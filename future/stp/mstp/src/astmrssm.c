/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmrssm.c,v 1.37 2017/11/21 13:06:50 siva Exp $
 *
 * Description: This file contains Port Role Selection State      
 *              Machine related MST functions.
 *
 *******************************************************************/

#ifdef MSTP_WANTED

#include "astminc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPRoleSelSmUpdtRolesDisabledTree         */
/*                                                                           */
/*    Description               : Function sets the Port role for all ports  */
/*                                for the given Tree or Instance.            */
/*                                                                           */
/*    Input(s)                  :  u2MstInst      - MST Instance Number for  */
/*                                                 which the SEM Operates.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/

#ifdef __STDC__
INT4
MstPRoleSelSmUpdtRolesDisabledTree (UINT2 u2MstInst)
#else
INT4
MstPRoleSelSmUpdtRolesDisabledTree (u2MstInst)
     UINT2               u2MstInst;
#endif
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    UINT2               u2PortNum = 0;

    tRstPortInfo       *pRstPortEntry = NULL;

    AST_SCAN_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList,
                       pRstPortEntry, tRstPortInfo *)
    {
        u2PortNum = pRstPortEntry->u2PortNum;

        if (u2PortNum == 0)
        {
            continue;
        }

        if ((AST_GET_PORTENTRY (u2PortNum)) == NULL)
        {
            continue;
        }
        pPerStPortInfo =
            AST_GET_PER_ST_CIST_MSTI_PORT_INFO (u2PortNum, u2MstInst);
        if (pPerStPortInfo != NULL)
        {
            AST_DBG_ARG2 (AST_RSSM_DBG,
                          "RSSM: Port %s: Inst %d: Port Role Selected as -> DISABLED\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

            pPerStPortInfo->u1SelectedPortRole = MST_PORT_ROLE_DISABLED;
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPRoleSelSmSetSelectedTree               */
/*                                                                           */
/*    Description               : Function Sets the Selected flag to True for*/
/*                                all ports of the given spanning Tree       */
/*                                                                           */
/*    Input(s)                  : u2MstInst      - MST Instance Number for   */
/*                                               which the SEM Operates.     */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
INT4
MstPRoleSelSmSetSelectedTree (UINT2 u2MstInst)
#else
INT4
MstPRoleSelSmSetSelectedTree (u2MstInst)
     UINT2               u2MstInst;
#endif
{
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    UINT2               u2PortNum = (UINT2) AST_INIT_VAL;
    UINT2               u2TmpRootPort = (UINT2) AST_INIT_VAL;
    UINT2               u2TempInst = 1;
    UINT2               u2MasterPort = AST_INIT_VAL;
    tPerStMstiOnlyInfo *pPerStMstiOnlyInfo = NULL;
    tAstPerStBridgeInfo *pCistBrgInfo = NULL;
    tAstPerStPortInfo  *pTmpPerStPortInfo = NULL;
    UINT2               u2TmpPortNum = 0;
    tAstBridgeId        MyBridgeId;

    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;

    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2MstInst);
    u2TmpRootPort = pPerStBrgInfo->u2RootPort;

    for (u2PortNum = 1; u2PortNum <= AST_MAX_NUM_PORTS; u2PortNum++)
    {

        /* Master Flag calculations for MSTIs */
        if (u2MstInst != MST_CIST_CONTEXT)
        {
            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);
            if (pPerStPortInfo == NULL)
            {
                /* Port not created or is not a member of this instance */
                continue;
            }
            if (pPerStPortInfo->u1SelectedPortRole == MST_PORT_ROLE_MASTER &&
                pPerStPortInfo->u1PortRole != MST_PORT_ROLE_MASTER)
            {
                /* Role change from non-master to master port - mstiMaster to 
                 * be set for all other root/designated ports of this 
                 * instance */
                AST_DBG_ARG2 (AST_RSSM_DBG,
                              "RSSM: Port %s: Inst %d: Master port detected. "
                              "Setting mstiMaster on all other active ports\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

                /* Master port cannot have mstiMaster set */
                pPerStMstiOnlyInfo =
                    AST_GET_PERST_MSTI_ONLY_INFO (pPerStPortInfo);
                pPerStMstiOnlyInfo->bMaster = MST_FALSE;

                for (u2TmpPortNum = 1; u2TmpPortNum <= AST_MAX_NUM_PORTS;
                     u2TmpPortNum++)
                {
                    if (u2TmpPortNum == u2PortNum)
                    {
                        continue;
                    }

                    pTmpPerStPortInfo =
                        AST_GET_PERST_PORT_INFO (u2TmpPortNum, u2MstInst);
                    if (pTmpPerStPortInfo == NULL)
                    {
                        /* Port not created or is not a member of this instance */
                        continue;
                    }

                    pPerStMstiOnlyInfo =
                        AST_GET_PERST_MSTI_ONLY_INFO (pTmpPerStPortInfo);

                    if ((pTmpPerStPortInfo->u1SelectedPortRole ==
                         AST_PORT_ROLE_ROOT)
                        || (pTmpPerStPortInfo->u1SelectedPortRole ==
                            AST_PORT_ROLE_DESIGNATED))
                    {
                        pPerStMstiOnlyInfo->bMaster = MST_TRUE;
                    }
                }
            }

            if ((pPerStPortInfo->u1PortRole == MST_PORT_ROLE_MASTER) &&
                (pPerStPortInfo->u1SelectedPortRole != MST_PORT_ROLE_MASTER) &&
                (u2MasterPort == AST_INIT_VAL))
            {
                /* Role change from master to non-master port - mstiMaster to 
                 * be reset for all ports of this instance */
                /* If u2MasterPort is non-zero there is a master port and hence
                 * MstiMaster need not be reset on all ports */
                AST_DBG_ARG2 (AST_RSSM_DBG,
                              "RSSM: Port %s: Inst %d: Master Port changes role\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                AST_DBG_ARG2 (AST_RSSM_DBG,
                              "RSSM: Port %s: Inst %d: Clearing mstiMaster for"
                              " all ports in this instance\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

                for (u2TmpPortNum = 1; u2TmpPortNum <= AST_MAX_NUM_PORTS;
                     u2TmpPortNum++)
                {
                    pTmpPerStPortInfo =
                        AST_GET_PERST_PORT_INFO (u2TmpPortNum, u2MstInst);
                    if (pTmpPerStPortInfo == NULL)
                    {
                        /* Port not created or is not a member of this instance */
                        continue;
                    }

                    pPerStMstiOnlyInfo =
                        AST_GET_PERST_MSTI_ONLY_INFO (pTmpPerStPortInfo);
                    pPerStMstiOnlyInfo->bMaster = MST_FALSE;
                }
            }

            if (((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ALTERNATE) ||
                 (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_DISABLED) ||
                 (pPerStPortInfo->u1PortRole == AST_PORT_ROLE_BACKUP)) &&
                ((pPerStPortInfo->u1SelectedPortRole == AST_PORT_ROLE_ROOT) ||
                 (pPerStPortInfo->u1SelectedPortRole ==
                  AST_PORT_ROLE_DESIGNATED)))
            {
                /* Non-Active port has become an Active port. */
                pPerStMstiOnlyInfo =
                    AST_GET_PERST_MSTI_ONLY_INFO (pPerStPortInfo);

                /* If this bridge has a master port or has any root or desig
                 * port with mstiMastered set then MstiMaster has to be set now */
                pCistBrgInfo = AST_GET_PERST_BRG_INFO (MST_CIST_CONTEXT);
                MyBridgeId.u2BrgPriority = pCistBrgInfo->u2BrgPriority;
                AST_MEMCPY (&(MyBridgeId.BridgeAddr),
                            &((AST_GET_BRGENTRY ())->BridgeAddr),
                            AST_MAC_ADDR_SIZE);

                if (((AST_COMPARE_BRGID (&(pCistBrgInfo->RootId),
                                         &MyBridgeId) != AST_BRGID1_SAME) &&
                     (AST_COMPARE_BRGID
                      (&((AST_GET_MST_BRGENTRY ())->RegionalRootId),
                       &MyBridgeId) == AST_BRGID1_SAME))
                    || (pPerStBrgInfo->u2MasteredCount > 0))
                {
                    AST_DBG_ARG2 (AST_RSSM_DBG,
                                  "RSSM: Port %s: Inst %d: Role change:"
                                  " Non-active->Active. Bridge has a master"
                                  " port. Hence setting mstiMaster\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                    pPerStMstiOnlyInfo->bMaster = MST_TRUE;
                }

                /* The MstiMastered previously received on this port has to counted. */
                if (pPerStMstiOnlyInfo->bMastered == MST_TRUE)
                {
                    pPerStBrgInfo->u2MasteredCount++;
                    MstProcessMstiMasteredFlag (pPerStPortInfo, u2MstInst);
                }
            }

            if (((pPerStPortInfo->u1SelectedPortRole == AST_PORT_ROLE_ALTERNATE)
                 || (pPerStPortInfo->u1SelectedPortRole ==
                     AST_PORT_ROLE_DISABLED)
                 || (pPerStPortInfo->u1SelectedPortRole ==
                     AST_PORT_ROLE_BACKUP))
                && ((pPerStPortInfo->u1PortRole == AST_PORT_ROLE_ROOT)
                    || (pPerStPortInfo->u1PortRole ==
                        AST_PORT_ROLE_DESIGNATED)))
            {
                /* Active port has become a Non-Active port. */
                pPerStMstiOnlyInfo =
                    AST_GET_PERST_MSTI_ONLY_INFO (pPerStPortInfo);

                /* The MstiMastered received on this port has to be excluded 
                 * from the count. */
                if ((pPerStMstiOnlyInfo->bMastered == MST_TRUE) &&
                    (pPerStBrgInfo->u2MasteredCount > 0))
                {
                    pPerStBrgInfo->u2MasteredCount--;

                    if (pPerStBrgInfo->u2MasteredCount == 0)
                    {
                        MstProcessMstiMasteredFlag (pPerStPortInfo, u2MstInst);
                    }
                }

                /* MstiMaster has to be cleared now */
                AST_DBG_ARG2 (AST_RSSM_DBG,
                              "RSSM: Port %s: Inst %d: Role change:"
                              " Active->Non-active. Clearing mstiMaster\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                pPerStMstiOnlyInfo->bMaster = MST_FALSE;
            }
        }                        /* End of Master flag calciulations */

        /* Call the Role Transition State Machine or
         * Port Information State Machine for all the ports 
         * other than Root Port. 
         */
        if (u2PortNum != u2TmpRootPort)
        {
            if ((AST_GET_PORTENTRY (u2PortNum)) != NULL)
            {
                pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);
                if (pPerStPortInfo != NULL)
                {
                    if (pPerStPortInfo->u1SelectedPortRole ==
                        (UINT1) MST_PORT_ROLE_MASTER)
                    {
                        u2MasterPort = pPerStPortInfo->u2PortNo;
                    }
                    else
                    {
                        pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
                        pRstPortInfo->bSelected = (tAstBoolean) MST_TRUE;
                        MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                               MST_SYNC_BSELECT_UPDATE);

                        AST_DBG_ARG2 (AST_RSSM_DBG,
                                      "RSSM: Port %s: Inst %d: SELECTED variable SET\n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      u2MstInst);

                        if (pRstPortInfo->bUpdtInfo == (tAstBoolean) MST_FALSE)
                        {
                            pPortInfo = AST_GET_PORTENTRY (u2PortNum);
                            if (pPortInfo != NULL)
                            {
                                pPortInfo->bAllTransmitReady = MST_TRUE;
                            }

                            if (MstPortRoleTransitMachine
                                (MST_PROLETRSM_EV_SELECTED_SET, pPerStPortInfo,
                                 u2MstInst) != MST_SUCCESS)
                            {

                                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                                              AST_ALL_FAILURE_TRC,
                                              "RSSM: Port %s: Inst %d: MstPortRoleTransitMachine function returned FAILURE for SELECTED_SET event!\n",
                                              AST_GET_IFINDEX_STR
                                              (pPerStPortInfo->u2PortNo),
                                              u2MstInst);
                                AST_DBG_ARG2 (AST_RSSM_DBG |
                                              AST_ALL_FAILURE_DBG,
                                              "RSSM: Port %s: Inst %d: MstPortRoleTransitMachine function returned FAILURE for SELECTED_SET event!\n",
                                              AST_GET_IFINDEX_STR
                                              (pPerStPortInfo->u2PortNo),
                                              u2MstInst);
                                return MST_FAILURE;
                            }
                        }
                        else
                        {
                            if (MstPortInfoMachine
                                ((UINT2) MST_PINFOSM_EV_UPDATEINFO,
                                 pPerStPortInfo, (tMstBpdu *) AST_NO_VAL,
                                 u2MstInst) != MST_SUCCESS)
                            {

                                AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                                              AST_ALL_FAILURE_TRC,
                                              "RSSM: Port %s: Inst %d: MstPortInfoMachine function returned FAILURE for UPDATE_INFO event!\n",
                                              AST_GET_IFINDEX_STR
                                              (pPerStPortInfo->u2PortNo),
                                              u2MstInst);
                                AST_DBG_ARG2 (AST_RSSM_DBG |
                                              AST_ALL_FAILURE_DBG,
                                              "RSSM: Port %s: Inst %d: MstPortInfoMachine function returned FAILURE for UPDATE_INFO event!\n",
                                              AST_GET_IFINDEX_STR
                                              (pPerStPortInfo->u2PortNo),
                                              u2MstInst);
                                return MST_FAILURE;
                            }
                        }
                    }
                }
            }
        }
    }

    /* The Master Port, like the Root Port, is handled after handling all the
     * other Designated, Alternate and Backup ports, so as to ensure that the
     * allSynced Event is True when the Master Port Role Transition is being
     * processed, thus enabling it to transition to forwarding. */
    if (u2MasterPort != AST_INIT_VAL)
    {
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2MasterPort, u2MstInst);
        if (pPerStPortInfo != NULL)
        {
            pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
            pRstPortInfo->bSelected = (tAstBoolean) MST_TRUE;
            MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                   MST_SYNC_BSELECT_UPDATE);

            AST_DBG_ARG2 (AST_RSSM_DBG,
                          "RSSM: Port %s: Inst %d: SELECTED variable SET\n",
                          AST_GET_IFINDEX_STR (pPerStPortInfo->u2PortNo),
                          u2MstInst);

            if (pRstPortInfo->bUpdtInfo == (tAstBoolean) MST_FALSE)
            {
                pPortInfo = AST_GET_PORTENTRY (u2MasterPort);
                if (pPortInfo != NULL)
                {
                    pPortInfo->bAllTransmitReady = MST_TRUE;
                }

                if (MstPortRoleTransitMachine
                    (MST_PROLETRSM_EV_SELECTED_SET, pPerStPortInfo,
                     u2MstInst) != MST_SUCCESS)
                {

                    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "RSSM: Port %s: Inst %d: MstPortRoleTransitMachine function returned FAILURE for SELECTED_SET event!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst);
                    AST_DBG_ARG2 (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RSSM: Port %s: Inst %d: MstPortRoleTransitMachine function returned FAILURE for SELECTED_SET event!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst);
                    return MST_FAILURE;
                }
            }
            else
            {
                if (MstPortInfoMachine
                    ((UINT2) MST_PINFOSM_EV_UPDATEINFO, pPerStPortInfo,
                     (tMstBpdu *) AST_NO_VAL, u2MstInst) != MST_SUCCESS)
                {

                    AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                                  AST_ALL_FAILURE_TRC,
                                  "RSSM: Port %s: Inst %d: MstPortInfoMachine function returned FAILURE for UPDATE_INFO event!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst);
                    AST_DBG_ARG2 (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                                  "RSSM: Port %s: Inst %d: MstPortInfoMachine function returned FAILURE for UPDATE_INFO event!\n",
                                  AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                       u2PortNo), u2MstInst);
                    return MST_FAILURE;
                }
            }
        }
    }

    /* Now Call the Role Transition State Machine or
     * Port Information State Machine for Root Port.
     */
    if (u2TmpRootPort != AST_INIT_VAL)
    {
        u2PortNum = pPerStBrgInfo->u2RootPort;
        if ((AST_GET_PORTENTRY (u2PortNum)) != NULL)
        {
            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);
            if (pPerStPortInfo != NULL)
            {
                pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
                pRstPortInfo->bSelected = (tAstBoolean) MST_TRUE;
                MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                       MST_SYNC_BSELECT_UPDATE);

                AST_DBG_ARG1 (AST_RSSM_DBG,
                              "RSSM: Port %s: SELECTED variable SET\n",
                              AST_GET_IFINDEX_STR (u2PortNum));

                if (pRstPortInfo->bUpdtInfo == (tAstBoolean) MST_FALSE)
                {
                    pPortInfo = AST_GET_PORTENTRY (u2PortNum);
                    if (pPortInfo != NULL)
                    {
                        pPortInfo->bAllTransmitReady = MST_TRUE;
                    }

                    if (MstPortRoleTransitMachine
                        (MST_PROLETRSM_EV_SELECTED_SET, pPerStPortInfo,
                         u2MstInst) != MST_SUCCESS)
                    {

                        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RSSM: Port %s: Inst %d: MstPortRoleTransitMachine function returned FAILURE for SELECTED_SET event!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      u2MstInst);
                        AST_DBG_ARG2 (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                                      "RSSM: Port %s: Inst %d: MstPortRoleTransitMachine function returned FAILURE for SELECTED_SET event!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      u2MstInst);
                        return MST_FAILURE;
                    }
                }
                else
                {
                    if (MstPortInfoMachine ((UINT2) MST_PINFOSM_EV_UPDATEINFO,
                                            pPerStPortInfo,
                                            (tMstBpdu *) AST_NO_VAL,
                                            u2MstInst) != MST_SUCCESS)
                    {

                        AST_TRC_ARG2 (AST_CONTROL_PATH_TRC |
                                      AST_ALL_FAILURE_TRC,
                                      "RSSM: Port %s: Inst %d: MstPortInfoMachine function returned FAILURE for UPDATE_INFO event!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      u2MstInst);
                        AST_DBG_ARG2 (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                                      "RSSM: Port %s: Inst %d: MstPortInfoMachine function returned FAILURE for UPDATE_INFO event!\n",
                                      AST_GET_IFINDEX_STR (pPerStPortInfo->
                                                           u2PortNo),
                                      u2MstInst);
                        return MST_FAILURE;
                    }
                }
            }
        }
    }

    /* Trigger is added apart from Std 802.1s.
     * There are 5 possible scenarios in which MSTI roles should be 
     * recalculated because of changes in CIST:
     * (1) Any port that received bpdus internal to the region now receives
     * bpdus from outside the region or vice versa, or,
     * (2) This Bridge may have previously been the CIST Regional Root
     * Bridge and may now no longer be so, or,
     * (3) This Bridge may not have been the CIST Regional Root Bridge 
     * earlier but may now be newly selected as the CIST Regional Root, or,
     * (4) There is a change in CIST roles for one or more ports and this 
     * bridge is the regional root, or else,
     * (5) There is a change in CIST roles for one or more boundary ports. */
    if ((u2MstInst == MST_CIST_CONTEXT) &&
        (AST_GET_INST_TRIGGER_FLAG == MST_TRUE))
    {
        AST_GET_INST_TRIGGER_FLAG = MST_FALSE;

        AST_DBG (AST_RSSM_DBG,
                 "RSSM: CIST: Triggering role selection for all instances!\n");

        AST_GET_NEXT_BUF_INSTANCE (u2TempInst, pPerStInfo)
        {
            if (pPerStInfo == NULL)
            {
                continue;
            }
            if (RstPortRoleSelectionMachine
                (RST_PROLESELSM_EV_RESELECT, u2TempInst) != RST_SUCCESS)
            {
                AST_TRC_ARG1 (AST_CONTROL_PATH_TRC | AST_ALL_FAILURE_TRC,
                              "RSSM: Inst %d: Role Selection machine returned FAILURE for RESELECT event!\n",
                              u2MstInst);
                AST_DBG_ARG1 (AST_RSSM_DBG | AST_ALL_FAILURE_DBG,
                              "RSSM: Inst %d: Role selection machine returned FAILURE for RESELECT event!\n",
                              u2MstInst);
                continue;
            }
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPRoleSelSmUpdtRoles                     */
/*                                                                           */
/*    Description               : Function updates the roles for the Ports   */
/*                                                                           */
/*                                                                           */
/*    Input(s)                  :  u2MstInst      - MST Instance Number for  */
/*                                                 which the SEM Operates.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPRoleSelSmUpdtRoles (UINT2 u2MstInst)
{
    INT4                i4RetVal = 0;

    if (u2MstInst == MST_CIST_CONTEXT)
    {
        i4RetVal = MstPRoleSelSmUpdtRolesCist (u2MstInst);
    }
    else
    {
        i4RetVal = MstPRoleSelSmUpdtRolesMsti (u2MstInst);
    }

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPRoleSelSmUpdtRolesCist                 */
/*                                                                           */
/*    Description               : Function updates the roles for the Ports   */
/*                                for the CIST context                       */
/*                                                                           */
/*    Input(s)                  :  u2MstInst      - MST Instance Number for  */
/*                                                 which the SEM Operates.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/
INT4
MstPRoleSelSmUpdtRolesCist (UINT2 u2MstInst)
{
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tPerStCistMstiCommInfo *pPerStCistMstiPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstMstBridgeEntry *pMstBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstTimes          *pPortTimes = NULL;
    tAstTimes          *pRootTimes = NULL;
    tRstPortInfo       *pRstPortEntry = NULL;
    UINT1               au1RootBrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1OldRootBrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1OldRole[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1NewRole[AST_BRG_ADDR_DIS_LEN];
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];

    tAstBridgeId        MyBrgId;
    tAstBridgeId        TmpRootId;
    tAstBridgeId        TmpDesgBrgId;
    UINT4               u4TmpRootCost = 0;
    UINT2               u2TmpDesgPort = 0;
    UINT4               u4Val = 0;
    UINT2               u2PortNum = 0;
    UINT2               u2TmpRootPort = 0;
    INT4                i4RetVal = 0;
    UINT2               u2Val = 0;
    tCistMstiPortInfo  *pCistMstiPortInfo = NULL;
    tAstMstBridgeEntry *pMstBridgeEntry = NULL;
    tAstBridgeId        TmpRegionalRootId;
    UINT4               u4TmpIntRootCost = 0;
    UINT2               u2TmpRootPortId = 0;
    UINT2               u2PortId = 0;
    tAstBoolean         bRegionalRoot = MST_FALSE;
    tAstBoolean         bBoundaryPort = MST_FALSE;
    tAstBoolean         bSyncMaster = MST_FALSE;

    tAstBridgeId        CurRegionalRootId;
    UINT4               u4CurIntRootCost = 0;
    tAstBridgeId        CurRootId;
    tAstBridgeId        CurDesgBrgId;
    UINT4               u4CurRootCost = 0;
    UINT2               u2CurDesgPort = 0;
    UINT2               u2CurRootPort = 0;
    UINT2               u2UpdateFlag = MST_TRUE;
    UINT1               u1SelectionLogic = AST_INIT_VAL;
    UINT1               u1RotateLoop = MST_TRUE;

    AST_MEMSET (&MyBrgId, AST_INIT_VAL, sizeof (MyBrgId));
    AST_MEMSET (&TmpRootId, AST_INIT_VAL, sizeof (TmpRootId));
    AST_MEMSET (&TmpDesgBrgId, AST_INIT_VAL, sizeof (TmpDesgBrgId));
    AST_MEMSET (au1RootBrgAddr, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);
    AST_MEMSET (au1OldRootBrgAddr, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);
    AST_MEMSET (au1OldRole, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);
    AST_MEMSET (au1NewRole, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);
    AST_MEMSET (au1TimeStr, AST_INIT_VAL, AST_BRG_ID_DIS_LEN);

    pBrgInfo = AST_GET_BRGENTRY ();
    pMstBrgInfo = AST_GET_MST_BRGENTRY ();
    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2MstInst);
    TmpRootId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
    AST_MEMCPY (&(TmpRootId.BridgeAddr),
                &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
    AST_MEMSET (&TmpRegionalRootId, AST_INIT_VAL, sizeof (TmpRegionalRootId));
    pMstBridgeEntry = &(pBrgInfo->MstBridgeEntry);

    TmpRegionalRootId = TmpRootId;
    u4TmpIntRootCost = 0;
    u4TmpRootCost = 0;
    TmpDesgBrgId = TmpRootId;
    u2TmpDesgPort = 0;
    u2TmpRootPort = 0;

    MyBrgId = TmpRootId;

    CurRegionalRootId = TmpRegionalRootId;
    u4CurIntRootCost = u4TmpIntRootCost;
    CurRootId = TmpRootId;
    CurDesgBrgId = TmpDesgBrgId;
    u4CurRootCost = 0;
    u2CurDesgPort = 0;
    u2CurRootPort = 0;

    if (AST_IS_MST_ENABLED () && (gu2AstRecvPort != AST_INIT_VAL))
    {
        if ((pPortInfo = AST_GET_PORTENTRY (gu2AstRecvPort)) != NULL)
        {
            if ((pPerStPortInfo =
                 AST_GET_PERST_PORT_INFO (gu2AstRecvPort, u2MstInst)) != NULL)
            {
                if ((pRstPortInfo =
                     AST_GET_RST_PORT_INFO (pPerStPortInfo)) != NULL)
                {
                    if ((pRstPortInfo->u1InfoIs == (UINT1) RST_INFOIS_RECEIVED))
                    {
                        pCistMstiPortInfo =
                            AST_GET_CIST_MSTI_PORT_INFO (gu2AstRecvPort);
                        {
                            u1SelectionLogic = MST_TRUE;
                            u2CurRootPort = pPerStBrgInfo->u2RootPort;
                            CurRootId = pPerStBrgInfo->RootId;
                            CurDesgBrgId = pPerStBrgInfo->DesgBrgId;
                            u2CurDesgPort = pPerStBrgInfo->u2DesgPort;
                            u4CurRootCost = pPerStBrgInfo->u4RootCost;
                            u4CurIntRootCost =
                                pPerStBrgInfo->u4CistInternalRootCost;
                            CurRegionalRootId = pMstBrgInfo->RegionalRootId;
                        }
                    }
                }
            }
        }
    }

    if (u1SelectionLogic == MST_TRUE)
    {
        /* Port Message is a Received Message */
        if (AST_MEMCMP (&(MyBrgId.BridgeAddr[0]),
                        &(pPerStPortInfo->DesgBrgId.BridgeAddr[0]),
                        AST_MAC_ADDR_SIZE) == AST_BRGID1_SAME)
        {
            u2TmpRootPort = u2CurRootPort;
            TmpRegionalRootId = CurRegionalRootId;
            u4TmpIntRootCost = u4CurIntRootCost;
            TmpRootId = CurRootId;
            TmpDesgBrgId = CurDesgBrgId;
            u4TmpRootCost = u4CurRootCost;
            u2TmpDesgPort = u2CurDesgPort;

        }
        else
        {

            i4RetVal = AST_COMPARE_BRGID (&(pPerStPortInfo->RootId),
                                          &(CurRootId));
            switch (i4RetVal)
            {
                case RST_BRGID1_SUPERIOR:
                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %s: CIST: Root BrgId received by Port is SUPERIOR\n",
                                  AST_GET_IFINDEX_STR (gu2AstRecvPort));

                    TmpRootId = pPerStPortInfo->RootId;
                    if (pCistMstiPortInfo->bInfoInternal == MST_FALSE)
                    {
                        u4TmpRootCost = pPerStPortInfo->u4RootCost +
                            pPerStPortInfo->u4PortPathCost;
                        TmpRegionalRootId = MyBrgId;
                        u4TmpIntRootCost = 0;
                    }
                    else
                    {
                        u4TmpRootCost = pPerStPortInfo->u4RootCost;
                        TmpRegionalRootId = pPerStPortInfo->RegionalRootId;
                        u4TmpIntRootCost =
                            pPerStPortInfo->u4IntRootPathCost +
                            pPerStPortInfo->u4PortPathCost;
                    }
                    TmpDesgBrgId = pPerStPortInfo->DesgBrgId;
                    u2TmpDesgPort = pPerStPortInfo->u2DesgPortId;
                    u2TmpRootPort = gu2AstRecvPort;
                    break;

                case RST_BRGID1_SAME:
                    TmpRootId = pPerStPortInfo->RootId;
                    if (pCistMstiPortInfo->bInfoInternal == MST_FALSE)
                    {
                        u4Val =
                            pPerStPortInfo->u4RootCost +
                            pPerStPortInfo->u4PortPathCost;
                    }
                    else
                    {
                        u4Val = pPerStPortInfo->u4RootCost;
                    }
                    if (u4Val < u4CurRootCost)
                    {
                        AST_DBG_ARG1 (AST_RSSM_DBG,
                                      "RSSM: Port %s: CIST: External Root Pathcost received by Port is SUPERIOR\n",
                                      AST_GET_IFINDEX_STR (gu2AstRecvPort));

                        u4TmpRootCost = u4Val;

                        if (pCistMstiPortInfo->bInfoInternal == MST_FALSE)
                        {
                            TmpRegionalRootId = MyBrgId;
                            u4TmpIntRootCost = 0;
                        }
                        else
                        {
                            TmpRegionalRootId = pPerStPortInfo->RegionalRootId;
                            u4TmpIntRootCost =
                                pPerStPortInfo->u4IntRootPathCost +
                                pPerStPortInfo->u4PortPathCost;
                        }

                        TmpDesgBrgId = pPerStPortInfo->DesgBrgId;
                        u2TmpDesgPort = pPerStPortInfo->u2DesgPortId;
                        u2TmpRootPort = gu2AstRecvPort;
                    }
                    else if (u4Val == u4CurRootCost)
                    {
                        u4TmpRootCost = u4Val;
                        if (pCistMstiPortInfo->bInfoInternal == MST_FALSE)
                        {
                            i4RetVal = AST_COMPARE_BRGID (&(MyBrgId),
                                                          &(CurRegionalRootId));
                        }
                        else
                        {
                            i4RetVal =
                                AST_COMPARE_BRGID (&
                                                   (pPerStPortInfo->
                                                    RegionalRootId),
                                                   &(CurRegionalRootId));
                        }
                        switch (i4RetVal)
                        {
                            case RST_BRGID1_SUPERIOR:

                                AST_DBG_ARG1 (AST_RSSM_DBG,
                                              "RSSM: Port %s: CIST: Regional RootId received by Port is SUPERIOR\n",
                                              AST_GET_IFINDEX_STR
                                              (gu2AstRecvPort));
                                if (pCistMstiPortInfo->bInfoInternal ==
                                    MST_FALSE)
                                {
                                    TmpRegionalRootId = MyBrgId;
                                    u4TmpIntRootCost = 0;
                                }
                                else
                                {
                                    TmpRegionalRootId =
                                        pPerStPortInfo->RegionalRootId;
                                    u4TmpIntRootCost =
                                        pPerStPortInfo->u4IntRootPathCost +
                                        pPerStPortInfo->u4PortPathCost;
                                }
                                TmpDesgBrgId = pPerStPortInfo->DesgBrgId;
                                u2TmpDesgPort = pPerStPortInfo->u2DesgPortId;
                                u2TmpRootPort = gu2AstRecvPort;
                                TmpRootId = pPerStPortInfo->RootId;
                                break;

                            case RST_BRGID1_SAME:
                                if (pCistMstiPortInfo->bInfoInternal ==
                                    MST_FALSE)
                                {
                                    TmpRegionalRootId = MyBrgId;
                                }
                                else
                                {
                                    TmpRegionalRootId =
                                        pPerStPortInfo->RegionalRootId;
                                }

                                if (pCistMstiPortInfo->bInfoInternal ==
                                    MST_FALSE)
                                {
                                    u4Val = 0;
                                }
                                else
                                {
                                    u4Val =
                                        pPerStPortInfo->u4IntRootPathCost +
                                        pPerStPortInfo->u4PortPathCost;
                                }
                                if (u4Val < u4CurIntRootCost)
                                {
                                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                                  "RSSM: Port %s: CIST: Internal RootPathcost received by Port is SUPERIOR\n",
                                                  AST_GET_IFINDEX_STR
                                                  (gu2AstRecvPort));
                                    u4TmpIntRootCost = u4Val;
                                    TmpRegionalRootId =
                                        pPerStPortInfo->RegionalRootId;
                                    TmpDesgBrgId = pPerStPortInfo->DesgBrgId;
                                    u2TmpDesgPort =
                                        pPerStPortInfo->u2DesgPortId;
                                    u2TmpRootPort = gu2AstRecvPort;
                                }
                                else if (u4Val == u4CurIntRootCost)
                                {
                                    u4TmpIntRootCost = u4Val;
                                    i4RetVal =
                                        AST_COMPARE_BRGID (&
                                                           (pPerStPortInfo->
                                                            DesgBrgId),
                                                           &(CurDesgBrgId));
                                    switch (i4RetVal)
                                    {

                                        case RST_BRGID1_SUPERIOR:
                                            AST_DBG_ARG1 (AST_RSSM_DBG,
                                                          "RSSM: Port %s: CIST: Desg BrgId received by Port is SUPERIOR\n",
                                                          AST_GET_IFINDEX_STR
                                                          (gu2AstRecvPort));
                                            TmpDesgBrgId =
                                                pPerStPortInfo->DesgBrgId;
                                            u2TmpDesgPort =
                                                pPerStPortInfo->u2DesgPortId;
                                            u2TmpRootPort = gu2AstRecvPort;
                                            break;

                                        case RST_BRGID1_SAME:
                                            TmpDesgBrgId =
                                                pPerStPortInfo->DesgBrgId;
                                            if (pPerStPortInfo->u2DesgPortId <
                                                u2CurDesgPort)
                                            {
                                                AST_DBG_ARG1 (AST_RSSM_DBG,
                                                              "RSSM: Port %s: CIST: Desg PortId received by Port is SUPERIOR\n",
                                                              AST_GET_IFINDEX_STR
                                                              (gu2AstRecvPort));
                                                u2TmpDesgPort =
                                                    pPerStPortInfo->
                                                    u2DesgPortId;
                                                u2TmpRootPort = gu2AstRecvPort;
                                            }
                                            else if (pPerStPortInfo->
                                                     u2DesgPortId ==
                                                     u2CurDesgPort)
                                            {
                                                u2TmpDesgPort =
                                                    pPerStPortInfo->
                                                    u2DesgPortId;
                                                u2TmpRootPortId =
                                                    (UINT2)
                                                    (AST_GET_PERST_PORT_INFO
                                                     (u2CurRootPort,
                                                      u2MstInst)->
                                                     u1PortPriority);
                                                u2TmpRootPortId =
                                                    (UINT2) (u2TmpRootPortId <<
                                                             8);
                                                u2TmpRootPortId =
                                                    u2TmpRootPortId |
                                                    AST_GET_LOCAL_PORT
                                                    (u2CurRootPort);

                                                u2PortId =
                                                    (UINT2)
                                                    (pPerStPortInfo->
                                                     u1PortPriority);
                                                u2PortId =
                                                    (UINT2) (u2PortId << 8);
                                                u2PortId =
                                                    u2PortId |
                                                    AST_GET_LOCAL_PORT
                                                    (gu2AstRecvPort);

                                                if (u2PortId <= u2TmpRootPortId)
                                                {
                                                    AST_DBG_ARG1
                                                        (AST_RSSM_DBG,
                                                         "RSSM: Port %s: CIST: Recvd PortId is SUPERIOR\n",
                                                         AST_GET_IFINDEX_STR
                                                         (gu2AstRecvPort));
                                                    u2TmpRootPort =
                                                        gu2AstRecvPort;
                                                }
                                                else
                                                    u2UpdateFlag = AST_INIT_VAL;
                                            }
                                            else
                                                u2UpdateFlag = AST_INIT_VAL;
                                            break;
                                        default:
                                            u2UpdateFlag = AST_INIT_VAL;
                                            break;
                                    }    /* End switch(i4RetVal) */
                                }    /* End of u4TmpIntRootCost */
                                else
                                    u2UpdateFlag = AST_INIT_VAL;
                                break;

                            default:
                                u2UpdateFlag = AST_INIT_VAL;
                                break;
                        }        /* End switch(i4RetVal) */
                    }
                    else
                        u2UpdateFlag = AST_INIT_VAL;
                    break;
                case RST_BRGID1_INFERIOR:
                /** Selection logic:
                 ** If the inferior info (other than previous ROOT info) is received in a ROOT port (particularly),
                 ** then the bridge advertises itself as ROOT bridge.
                 ** Normally this scenario will happen after the max-age expiry of old ROOT information.
                 **/
                    if ((pPerStPortInfo->u1PortRole == MST_PORT_ROLE_ROOT))
                    {

                        TmpRootId = MyBrgId;
                        u4TmpRootCost = 0;
                        TmpRegionalRootId = MyBrgId;
                        u4TmpIntRootCost = 0;
                        TmpDesgBrgId = MyBrgId;
                        u2TmpDesgPort = 0;
                        u2TmpRootPort = 0;
                        pPerStBrgInfo->u2InferiorRcvdRootPort = gu2AstRecvPort;
                    }
                    else if ((pPerStPortInfo->u1PortRole ==
                              AST_PORT_ROLE_ALTERNATE)
                             && (pCistMstiPortInfo->bInfoInternal == MST_FALSE))
                    {
                        TmpRootId = MyBrgId;
                        u4TmpRootCost = 0;
                        TmpRegionalRootId = MyBrgId;
                        u4TmpIntRootCost = 0;
                        TmpDesgBrgId = MyBrgId;
                        u2TmpDesgPort = 0;
                        u2TmpRootPort = 0;
                    }
                    else
                        u2UpdateFlag = AST_INIT_VAL;
                    break;
                default:
                    u2UpdateFlag = AST_INIT_VAL;
                    break;
            }                    /* End switch(i4RetVal) */
        }
        /* update the global values of Root */
        if (u2UpdateFlag == MST_TRUE)
        {
            u2CurRootPort = u2TmpRootPort;
            CurRegionalRootId = TmpRegionalRootId;
            u4CurIntRootCost = u4TmpIntRootCost;
            CurRootId = TmpRootId;
            CurDesgBrgId = TmpDesgBrgId;
            u4CurRootCost = u4TmpRootCost;
            u2CurDesgPort = u2TmpDesgPort;
            u1RotateLoop = MST_FALSE;

        }
        else if ((u2UpdateFlag == AST_INIT_VAL)
                 && (pPerStPortInfo->u1PortRole != MST_PORT_ROLE_ROOT))
        {
            u2TmpRootPort = u2CurRootPort;
            TmpRegionalRootId = CurRegionalRootId;
            u4TmpIntRootCost = u4CurIntRootCost;
            TmpRootId = CurRootId;
            TmpDesgBrgId = CurDesgBrgId;
            u4TmpRootCost = u4CurRootCost;
            u2TmpDesgPort = u2CurDesgPort;
            u1RotateLoop = MST_FALSE;

        }

    }

    if (u1RotateLoop == MST_TRUE)
    {
        AST_MEMSET (&MyBrgId, AST_INIT_VAL, sizeof (MyBrgId));
        AST_MEMSET (&TmpRootId, AST_INIT_VAL, sizeof (TmpRootId));
        AST_MEMSET (&TmpDesgBrgId, AST_INIT_VAL, sizeof (TmpDesgBrgId));

        pBrgInfo = AST_GET_BRGENTRY ();
        pMstBrgInfo = AST_GET_MST_BRGENTRY ();
        pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2MstInst);
        TmpRootId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
        AST_MEMCPY (&(TmpRootId.BridgeAddr),
                    &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
        AST_MEMSET (&TmpRegionalRootId, AST_INIT_VAL,
                    sizeof (TmpRegionalRootId));
        pMstBridgeEntry = &(pBrgInfo->MstBridgeEntry);

        TmpRegionalRootId = TmpRootId;
        u4TmpIntRootCost = 0;
        u4TmpRootCost = 0;
        TmpDesgBrgId = TmpRootId;
        u2TmpDesgPort = 0;
        u2TmpRootPort = 0;

        MyBrgId = TmpRootId;
        AST_SCAN_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList,
                           pRstPortEntry, tRstPortInfo *)
        {
            u2PortNum = pRstPortEntry->u2PortNum;

            if (u2PortNum == 0)
            {
                continue;
            }

            if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) == NULL)
            {
                continue;
            }
            if (AST_IS_MST_ENABLED ())
            {
                pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (u2PortNum);
                pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);
                if (pPerStPortInfo == NULL)
                {
                    continue;
                }

                /* Clear the Reselect flag for all  */
                /*port belonging to the given Spanning Tree context. */

                AST_DBG_ARG2 (AST_RSSM_DBG,
                              "RSSM: Port %s: Inst %d: Cleared RESELECT variable \n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                pPerStPortInfo->PerStRstPortInfo.bReSelect = MST_FALSE;

                pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);
                if (pRstPortInfo->bPortEnabled == (tAstBoolean) MST_FALSE)
                {
                    continue;
                }

                /* If restrictedRole is set, then don't consider this port
                 * for bridge root priority vector calculations. */
                if (AST_PORT_RESTRICTED_ROLE (pPortInfo) ==
                    (tAstBoolean) RST_TRUE)
                {
                    continue;
                }

                if (pRstPortInfo->u1InfoIs == (UINT1) RST_INFOIS_RECEIVED)
                {
                    /* Port Message is a Received Message */

                    if (AST_MEMCMP (&(MyBrgId.BridgeAddr[0]),
                                    &(pPerStPortInfo->DesgBrgId.BridgeAddr[0]),
                                    AST_MAC_ADDR_SIZE) == RST_BRGID1_SAME)
                    {
                        continue;
                    }

                    i4RetVal = AST_COMPARE_BRGID (&(pPerStPortInfo->RootId),
                                                  &(TmpRootId));
                    switch (i4RetVal)
                    {
                        case RST_BRGID1_SUPERIOR:
                            AST_DBG_ARG1 (AST_RSSM_DBG,
                                          "RSSM: Port %s: CIST: Root BrgId received by Port is SUPERIOR\n",
                                          AST_GET_IFINDEX_STR (u2PortNum));

                            TmpRootId = pPerStPortInfo->RootId;
                            if (pCistMstiPortInfo->bInfoInternal == MST_FALSE)
                            {
                                u4TmpRootCost = pPerStPortInfo->u4RootCost +
                                    pPerStPortInfo->u4PortPathCost;
                                TmpRegionalRootId = MyBrgId;
                                u4TmpIntRootCost = 0;
                            }
                            else
                            {
                                u4TmpRootCost = pPerStPortInfo->u4RootCost;
                                TmpRegionalRootId =
                                    pPerStPortInfo->RegionalRootId;
                                u4TmpIntRootCost =
                                    pPerStPortInfo->u4IntRootPathCost +
                                    pPerStPortInfo->u4PortPathCost;
                            }
                            TmpDesgBrgId = pPerStPortInfo->DesgBrgId;
                            u2TmpDesgPort = pPerStPortInfo->u2DesgPortId;
                            u2TmpRootPort = u2PortNum;
                            break;

                        case RST_BRGID1_SAME:
                            if (pCistMstiPortInfo->bInfoInternal == MST_FALSE)
                            {
                                u4Val =
                                    pPerStPortInfo->u4RootCost +
                                    pPerStPortInfo->u4PortPathCost;
                            }
                            else
                            {
                                u4Val = pPerStPortInfo->u4RootCost;
                            }
                            if (u4Val < u4TmpRootCost)
                            {
                                AST_DBG_ARG1 (AST_RSSM_DBG,
                                              "RSSM: Port %s: CIST: External Root Pathcost received by Port is SUPERIOR\n",
                                              AST_GET_IFINDEX_STR (u2PortNum));

                                u4TmpRootCost = u4Val;

                                if (pCistMstiPortInfo->bInfoInternal ==
                                    MST_FALSE)
                                {
                                    TmpRegionalRootId = MyBrgId;
                                    u4TmpIntRootCost = 0;
                                }
                                else
                                {
                                    TmpRegionalRootId =
                                        pPerStPortInfo->RegionalRootId;
                                    u4TmpIntRootCost =
                                        pPerStPortInfo->u4IntRootPathCost +
                                        pPerStPortInfo->u4PortPathCost;
                                }

                                TmpDesgBrgId = pPerStPortInfo->DesgBrgId;
                                u2TmpDesgPort = pPerStPortInfo->u2DesgPortId;
                                u2TmpRootPort = u2PortNum;
                            }
                            else if (u4Val == u4TmpRootCost)
                            {
                                if (pCistMstiPortInfo->bInfoInternal ==
                                    MST_FALSE)
                                {
                                    i4RetVal = AST_COMPARE_BRGID (&(MyBrgId),
                                                                  &
                                                                  (TmpRegionalRootId));
                                }
                                else
                                {
                                    i4RetVal =
                                        AST_COMPARE_BRGID (&
                                                           (pPerStPortInfo->
                                                            RegionalRootId),
                                                           &
                                                           (TmpRegionalRootId));
                                }
                                switch (i4RetVal)
                                {
                                    case RST_BRGID1_SUPERIOR:

                                        AST_DBG_ARG1 (AST_RSSM_DBG,
                                                      "RSSM: Port %s: CIST: Regional RootId received by Port is SUPERIOR\n",
                                                      AST_GET_IFINDEX_STR
                                                      (u2PortNum));
                                        if (pCistMstiPortInfo->bInfoInternal ==
                                            MST_FALSE)
                                        {
                                            TmpRegionalRootId = MyBrgId;
                                            u4TmpIntRootCost = 0;
                                        }
                                        else
                                        {
                                            TmpRegionalRootId =
                                                pPerStPortInfo->RegionalRootId;
                                            u4TmpIntRootCost =
                                                pPerStPortInfo->
                                                u4IntRootPathCost +
                                                pPerStPortInfo->u4PortPathCost;
                                        }
                                        TmpDesgBrgId =
                                            pPerStPortInfo->DesgBrgId;
                                        u2TmpDesgPort =
                                            pPerStPortInfo->u2DesgPortId;
                                        u2TmpRootPort = u2PortNum;
                                        break;

                                    case RST_BRGID1_SAME:

                                        /* Now the Regional Root Id is same */
                                        if (pCistMstiPortInfo->bInfoInternal ==
                                            MST_FALSE)
                                        {
                                            u4Val = 0;
                                        }
                                        else
                                        {
                                            u4Val =
                                                pPerStPortInfo->
                                                u4IntRootPathCost +
                                                pPerStPortInfo->u4PortPathCost;
                                        }

                                        if (u4Val < u4TmpIntRootCost)
                                        {
                                            AST_DBG_ARG1 (AST_RSSM_DBG,
                                                          "RSSM: Port %s: CIST: Internal RootPathcost received by Port is SUPERIOR\n",
                                                          AST_GET_IFINDEX_STR
                                                          (u2PortNum));
                                            u4TmpIntRootCost = u4Val;
                                            TmpDesgBrgId =
                                                pPerStPortInfo->DesgBrgId;
                                            u2TmpDesgPort =
                                                pPerStPortInfo->u2DesgPortId;
                                            u2TmpRootPort = u2PortNum;
                                        }
                                        else if (u4Val == u4TmpIntRootCost)
                                        {
                                            i4RetVal =
                                                AST_COMPARE_BRGID (&
                                                                   (pPerStPortInfo->
                                                                    DesgBrgId),
                                                                   &
                                                                   (TmpDesgBrgId));
                                            switch (i4RetVal)
                                            {

                                                case RST_BRGID1_SUPERIOR:
                                                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                                                  "RSSM: Port %s: CIST: Desg BrgId received by Port is SUPERIOR\n",
                                                                  AST_GET_IFINDEX_STR
                                                                  (u2PortNum));
                                                    TmpDesgBrgId =
                                                        pPerStPortInfo->
                                                        DesgBrgId;
                                                    u2TmpDesgPort =
                                                        pPerStPortInfo->
                                                        u2DesgPortId;
                                                    u2TmpRootPort = u2PortNum;
                                                    break;

                                                case RST_BRGID1_SAME:
                                                    if (pPerStPortInfo->
                                                        u2DesgPortId <
                                                        u2TmpDesgPort)
                                                    {
                                                        AST_DBG_ARG1
                                                            (AST_RSSM_DBG,
                                                             "RSSM: Port %s: CIST: Desg PortId received by Port is SUPERIOR\n",
                                                             AST_GET_IFINDEX_STR
                                                             (u2PortNum));
                                                        u2TmpDesgPort =
                                                            pPerStPortInfo->
                                                            u2DesgPortId;
                                                        u2TmpRootPort =
                                                            u2PortNum;
                                                    }
                                                    else if (pPerStPortInfo->
                                                             u2DesgPortId ==
                                                             u2TmpDesgPort)
                                                    {
                                                        u2TmpRootPortId =
                                                            (UINT2)
                                                            (AST_GET_PERST_PORT_INFO
                                                             (u2TmpRootPort,
                                                              u2MstInst)->
                                                             u1PortPriority);
                                                        u2TmpRootPortId =
                                                            (UINT2)
                                                            (u2TmpRootPortId <<
                                                             8);
                                                        u2TmpRootPortId =
                                                            u2TmpRootPortId |
                                                            AST_GET_LOCAL_PORT
                                                            (u2TmpRootPort);

                                                        u2PortId =
                                                            (UINT2)
                                                            (pPerStPortInfo->
                                                             u1PortPriority);
                                                        u2PortId =
                                                            (UINT2) (u2PortId <<
                                                                     8);
                                                        u2PortId =
                                                            u2PortId |
                                                            AST_GET_LOCAL_PORT
                                                            (u2PortNum);

                                                        if (u2PortId <
                                                            u2TmpRootPortId)
                                                        {
                                                            AST_DBG_ARG1
                                                                (AST_RSSM_DBG,
                                                                 "RSSM: Port %s: CIST: Recvd PortId is SUPERIOR\n",
                                                                 AST_GET_IFINDEX_STR
                                                                 (u2PortNum));
                                                            u2TmpRootPort =
                                                                u2PortNum;
                                                        }
                                                    }
                                                    break;

                                                default:
                                                    break;
                                            }    /* End switch(i4RetVal) */
                                        }    /* End of u4TmpIntRootCost */
                                        break;

                                    default:
                                        break;
                                }    /* End switch(i4RetVal) */
                            }
                            break;

                        default:
                            break;
                    }            /* End switch(i4RetVal) */
                }                /* End if(pRstPortInfo->u1InfoIs ..)... */
            }                    /* End if (AST_IS_RST_ENABLED())... */
        }                        /* End for (u2PortNum = 1; ...) */
    }
    if (u2TmpRootPort != AST_INIT_VAL)
    {

        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                      "RSSM: << ----Port %s selected as Root Port---- >>\n",
                      AST_GET_IFINDEX_STR (u2TmpRootPort));

        AST_DBG_ARG1 (AST_RSSM_DBG,
                      "RSSM: << ----Port %s selected as Root Port---- >>\n",
                      AST_GET_IFINDEX_STR (u2TmpRootPort));

        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2TmpRootPort, u2MstInst);
        pPortInfo = AST_GET_PORTENTRY (u2TmpRootPort);

        /* Calculate if there is any change in Root Bridge */

        i4RetVal =
            AST_MEMCMP (&(pPerStBrgInfo->RootId.BridgeAddr),
                        &(TmpRootId.BridgeAddr), AST_MAC_ADDR_LEN);
        if (i4RetVal != 0)
        {
            /* Store the OldRootId */
            pPerStBrgInfo->OldRootId = pPerStBrgInfo->RootId;
            /* Incrementing the New Root Bridge Count and generate Trap */
            AST_INCR_NEW_ROOTBRIDGE_COUNT (MST_CIST_CONTEXT);
            PrintMacAddress (pPerStBrgInfo->RootId.BridgeAddr,
                             au1OldRootBrgAddr);
            PrintMacAddress (TmpRootId.BridgeAddr, au1RootBrgAddr);
            UtlGetTimeStr (au1TimeStr);
            AST_SYS_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                              "Instance:%u Old Root:%s New Root:%s Time Stamp: %s\r\n",
                              u2MstInst, au1OldRootBrgAddr, au1RootBrgAddr,
                              au1TimeStr);

            AstNewRootTrap (pPerStBrgInfo->RootId, TmpRootId, u2MstInst,
                            (INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);

        }
        pBrgInfo->RootTimes = pPortInfo->PortTimes;
        pPerStBrgInfo->u1RootRemainingHops =
            pPerStPortInfo->PerStCistMstiCommPortInfo.u1PortRemainingHops;
        pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (u2TmpRootPort);

        /* According to updtRoles () Procedure of 802.1Q, when the sueprior
         * message is received, increment the message age if rcvdInternal is 
         * FALSE and decrement hopcount if rcvdInternal is TRUE
         * */
        if (pCistMstiPortInfo->bRcvdInternal == MST_FALSE)
        {
            pPerStBrgInfo->u1RootRemainingHops = pMstBrgInfo->u1MaxHopCount;
            pBrgInfo->RootTimes.u2MsgAgeOrHopCount =
                (UINT2) (pBrgInfo->RootTimes.u2MsgAgeOrHopCount +
                         (1 * AST_CENTI_SECONDS));
        }
        else
        {
            pPerStBrgInfo->u1RootRemainingHops =
                (UINT1) (pPerStBrgInfo->u1RootRemainingHops - 1);
        }
    }
    else
    {

        AST_DBG (AST_RSSM_DBG,
                 "RSSM: << ----This Bridge is the Root Bridge---- >>\n");
        /* Calculate if there is any change in Root Bridge */
        i4RetVal =
            AST_MEMCMP (&(pPerStBrgInfo->RootId.BridgeAddr),
                        &(TmpRootId.BridgeAddr), AST_MAC_ADDR_LEN);
        if (i4RetVal != 0)
        {
            /* Store the OldRootId */
            pPerStBrgInfo->OldRootId = pPerStBrgInfo->RootId;
            /* Incrementing the New Root Bridge Count and generate Trap */
            AST_INCR_NEW_ROOTBRIDGE_COUNT (MST_CIST_CONTEXT);
            AST_MEMSET (au1RootBrgAddr, 0, sizeof (au1RootBrgAddr));
            AST_MEMSET (au1OldRootBrgAddr, 0, sizeof (au1OldRootBrgAddr));

            PrintMacAddress (pPerStBrgInfo->RootId.BridgeAddr,
                             au1OldRootBrgAddr);
            PrintMacAddress (TmpRootId.BridgeAddr, au1RootBrgAddr);
            UtlGetTimeStr (au1TimeStr);
            AST_SYS_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                              "Instance:%u Old Root:%s New Root:%s Time Stamp: %s\r\n",
                              u2MstInst, au1OldRootBrgAddr, au1RootBrgAddr,
                              au1TimeStr);

            AstNewRootTrap (pPerStBrgInfo->RootId, TmpRootId, u2MstInst,
                            (INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);

        }
        pBrgInfo->RootTimes = pBrgInfo->BridgeTimes;
        pPerStBrgInfo->u1RootRemainingHops = pPerStBrgInfo->u1BrgRemainingHops;
    }

    /* As per 802.1Q Rev, SyncMaster will be called whenever
     * there is a change in the Regional Root and has or had a non-zero
     * CIST external root path cost.*/

    i4RetVal =
        AST_COMPARE_BRGID (&(pMstBridgeEntry->RegionalRootId),
                           &TmpRegionalRootId);
    if ((i4RetVal != RST_BRGID1_SAME)
        && ((u4TmpRootCost != 0) || (pPerStBrgInfo->u4RootCost != 0)))
    {
        bSyncMaster = MST_TRUE;
        AST_DBG (AST_RSSM_DBG,
                 "RSSM: SyncMaster set to TRUE due to change in CIST Regional Root with non-zero"
                 " CIST external root path cost.\n");
    }

    /* InstTrigger flag is set if any of the following is true
     * 1) If this bridge was previously the regional root and is no longer so.
     * 2) If this bridge was previously not the regional root and is newly 
     *    selected as regional root.
     */
    i4RetVal = AST_COMPARE_BRGID (&(pMstBridgeEntry->RegionalRootId), &MyBrgId);

    if (i4RetVal == RST_BRGID1_SAME)
    {
        i4RetVal = AST_COMPARE_BRGID (&(pMstBridgeEntry->RegionalRootId),
                                      &TmpRegionalRootId);
        if (i4RetVal != RST_BRGID1_SAME)
        {
            AST_DBG (AST_RSSM_DBG,
                     "RSSM: This Bridge is no longer the CIST Regional Root Bridge\n");
            AST_GET_INST_TRIGGER_FLAG = MST_TRUE;
        }
    }
    else
    {
        i4RetVal = AST_COMPARE_BRGID (&MyBrgId, &TmpRegionalRootId);
        if (i4RetVal == RST_BRGID1_SAME)
        {
            AST_DBG (AST_RSSM_DBG,
                     "RSSM: This Bridge is newly selected as the CIST Regional Root Bridge\n");
            AST_GET_INST_TRIGGER_FLAG = MST_TRUE;
            bRegionalRoot = MST_TRUE;    /* This bridge is the regional root */

        }
    }

    /* Now the Chosen Root Priority Vector is copied into global
     * Root Priority Vector*/

    pPerStBrgInfo->RootId = TmpRootId;
    pPerStBrgInfo->u4RootCost = u4TmpRootCost;

    pPerStBrgInfo->DesgBrgId = TmpDesgBrgId;
    pPerStBrgInfo->u2DesgPort = u2TmpDesgPort;

    pMstBridgeEntry->RegionalRootId = TmpRegionalRootId;
    pPerStBrgInfo->u4CistInternalRootCost = u4TmpIntRootCost;
    pPerStBrgInfo->u2RootPort = u2TmpRootPort;

    AST_SCAN_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList,
                       pRstPortEntry, tRstPortInfo *)
    {
        u2PortNum = pRstPortEntry->u2PortNum;

        if (u2PortNum == 0)
        {
            continue;
        }

        if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) == NULL)
        {
            /* Port is not yet created */
            continue;
        }
        if (AST_IS_MST_ENABLED ())
        {
            /* As per 802.1Q Copying the RootTimes to DesgTimes on all
             * the ports
             * */
            pPortInfo->DesgTimes.u2MaxAge = pBrgInfo->RootTimes.u2MaxAge;
            pPortInfo->DesgTimes.u2ForwardDelay =
                pBrgInfo->RootTimes.u2ForwardDelay;
            pPortInfo->DesgTimes.u2MsgAgeOrHopCount =
                pBrgInfo->RootTimes.u2MsgAgeOrHopCount;

            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);

            if (pPerStPortInfo == NULL)
            {
                continue;
            }

            /* Clear the Reselect flag for all  */
            /*port belonging to the given Spanning Tree context. */

            AST_DBG_ARG2 (AST_RSSM_DBG,
                          "RSSM: Port %s: Inst %d: Cleared RESELECT variable \n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
            pPerStPortInfo->PerStRstPortInfo.bReSelect = MST_FALSE;

            pPerStPortInfo->PerStCistMstiCommPortInfo.u1DesgRemainingHops
                = pPerStBrgInfo->u1RootRemainingHops;

            pRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);
            pPerStCistMstiPortInfo =
                MST_GET_CIST_MSTI_PORT_INFO (pPerStPortInfo);

            pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (u2PortNum);
            (pCistMstiPortInfo->bRcvdInternal == MST_TRUE) ?
                (bBoundaryPort = MST_FALSE) : (bBoundaryPort = MST_TRUE);
            switch (pRstPortInfo->u1InfoIs)
            {
                case RST_INFOIS_DISABLED:

                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %s: InfoIs is DISABLED\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));

                    pPerStPortInfo->u1SelectedPortRole =
                        (UINT1) AST_PORT_ROLE_DISABLED;
                    MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                           MST_SYNC_ROLE_UPDATE);
                    pPortInfo->CistMstiPortInfo.bCistDesignatedPort = MST_FALSE;
                    pPortInfo->CistMstiPortInfo.bCistRootPort = MST_FALSE;
                    AST_DBG_ARG2 (AST_RSSM_DBG,
                                  "RSSM: Port %s: Inst %d Port Role Selected as -> DISABLED\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                    break;

                case RST_INFOIS_AGED:

                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %s: InfoIs is AGED\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    pRstPortInfo->bUpdtInfo = MST_TRUE;
                    MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                           MST_SYNC_BSELECT_UPDATE);
                    pPortInfo->bAllTransmitReady = MST_FALSE;
                    pPerStPortInfo->u1SelectedPortRole =
                        (UINT1) AST_PORT_ROLE_DESIGNATED;
                    MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                           MST_SYNC_ROLE_UPDATE);
                    pPortInfo->CistMstiPortInfo.bCistDesignatedPort = MST_TRUE;
                    pPortInfo->CistMstiPortInfo.bCistRootPort = MST_FALSE;

                    AST_DBG_ARG2 (AST_RSSM_DBG,
                                  "RSSM: Port %s: Inst %d Port Role Selected as -> DESIGNATED\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                    break;

                case RST_INFOIS_MINE:

                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %s: InfoIs is MINE\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    pPerStPortInfo->u1SelectedPortRole =
                        (UINT1) AST_PORT_ROLE_DESIGNATED;
                    MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                           MST_SYNC_ROLE_UPDATE);
                    pPortInfo->CistMstiPortInfo.bCistDesignatedPort = MST_TRUE;
                    pPortInfo->CistMstiPortInfo.bCistRootPort = MST_FALSE;

                    AST_DBG_ARG2 (AST_RSSM_DBG,
                                  "RSSM: Port %s: Inst %d Port Role Selected as -> DESIGNATED\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

                    i4RetVal =
                        MstIsDesgPrBetterThanPortPr (u2PortNum, u2MstInst);

                    if (i4RetVal == RST_SAME_MSG)
                    {

                        pPortTimes = &(pPortInfo->PortTimes);
                        pRootTimes = &(pBrgInfo->RootTimes);

                        if ((pPortTimes->u2MaxAge != pRootTimes->u2MaxAge) ||
                            (pPortTimes->u2HelloTime !=
                             pPortInfo->DesgTimes.u2HelloTime) ||
                            (pPortTimes->u2ForwardDelay !=
                             pRootTimes->u2ForwardDelay) ||
                            (pPortTimes->u2MsgAgeOrHopCount !=
                             pRootTimes->u2MsgAgeOrHopCount) ||
                            (pPerStCistMstiPortInfo->u1PortRemainingHops !=
                             pPerStBrgInfo->u1RootRemainingHops))
                        {
                            pRstPortInfo->bUpdtInfo = MST_TRUE;
                            MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                                   MST_SYNC_BSELECT_UPDATE);
                            pPortInfo->bAllTransmitReady = MST_FALSE;
                        }
                    }
                    else
                    {
                        pRstPortInfo->bUpdtInfo = MST_TRUE;
                        MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                               MST_SYNC_BSELECT_UPDATE);
                        pPortInfo->bAllTransmitReady = MST_FALSE;
                    }

                    break;

                case RST_INFOIS_RECEIVED:

                    AST_DBG_ARG1 (AST_RSSM_DBG,
                                  "RSSM: Port %s: InfoIs is RECEIVED\n",
                                  AST_GET_IFINDEX_STR (u2PortNum));
                    if (u2TmpRootPort == u2PortNum)
                    {

                        pPerStPortInfo->u1SelectedPortRole =
                            (UINT1) AST_PORT_ROLE_ROOT;
                        MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                               MST_SYNC_ROLE_UPDATE);
                        pPortInfo->CistMstiPortInfo.bCistRootPort = MST_TRUE;
                        pPortInfo->CistMstiPortInfo.bCistDesignatedPort =
                            MST_FALSE;

                        AST_DBG_ARG2 (AST_RSSM_DBG,
                                      "RSSM: Port %s: Inst %d Port Role Selected as -> ROOT PORT\n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      u2MstInst);

                        pRstPortInfo->bUpdtInfo = MST_FALSE;
                        MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                               MST_SYNC_BSELECT_UPDATE);
                        if (pRstPortInfo->bSelected == MST_TRUE)
                        {
                            pPortInfo->bAllTransmitReady = MST_TRUE;
                        }
                    }
                    else
                    {
                        i4RetVal =
                            MstIsDesgPrBetterThanPortPr (u2PortNum, u2MstInst);
                        if (i4RetVal != RST_BETTER_MSG)
                        {
                            if (AST_MEMCMP
                                (&(pPerStPortInfo->DesgBrgId.BridgeAddr[0]),
                                 &(MyBrgId.BridgeAddr[0]),
                                 AST_MAC_ADDR_SIZE) != RST_BRGID1_SAME)
                            {

                                pPerStPortInfo->u1SelectedPortRole
                                    = (UINT1) AST_PORT_ROLE_ALTERNATE;
                                pPortInfo->CistMstiPortInfo.
                                    bCistDesignatedPort = MST_FALSE;
                                pPortInfo->CistMstiPortInfo.bCistRootPort =
                                    MST_FALSE;

                                AST_DBG_ARG2 (AST_RSSM_DBG,
                                              "RSSM: Port %s: Inst %d Port Role Selected as -> ALTERNATE\n",
                                              AST_GET_IFINDEX_STR (u2PortNum),
                                              u2MstInst);

                                pRstPortInfo->bUpdtInfo = MST_FALSE;
                                MstPUpdtAllSyncedFlag (u2MstInst,
                                                       pPerStPortInfo,
                                                       MST_SYNC_BSELECT_UPDATE);
                                if (pRstPortInfo->bSelected == MST_TRUE)
                                {
                                    pPortInfo->bAllTransmitReady = MST_TRUE;
                                }

                            }
                            else
                            {

                                u2Val = (UINT2) (pPerStPortInfo->u2DesgPortId &
                                                 (UINT2) AST_PORTNUM_MASK);
                                if (u2Val != u2PortNum)
                                {

                                    pPerStPortInfo->u1SelectedPortRole =
                                        (UINT1) AST_PORT_ROLE_BACKUP;
                                    MstPUpdtAllSyncedFlag (u2MstInst,
                                                           pPerStPortInfo,
                                                           MST_SYNC_ROLE_UPDATE);

                                    AST_DBG_ARG2 (AST_RSSM_DBG,
                                                  "RSSM: Port %s: Inst %d Port Role Selected as -> BACKUP\n",
                                                  AST_GET_IFINDEX_STR
                                                  (u2PortNum), u2MstInst);

                                    pRstPortInfo->bUpdtInfo = MST_FALSE;
                                    MstPUpdtAllSyncedFlag (u2MstInst,
                                                           pPerStPortInfo,
                                                           MST_SYNC_BSELECT_UPDATE);
                                    if (pRstPortInfo->bSelected == MST_TRUE)
                                    {
                                        pPortInfo->bAllTransmitReady = MST_TRUE;
                                    }

                                }
                                else
                                {
                                    /* Such a condition may occur when a BDPU is 
                                     * received that contains all the values 
                                     * that are the same as that would be 
                                     * transmitted from this port, except the 
                                     * designated port priority. For example, if 
                                     * this port is a designated port, and 
                                     * before a BPDU is received from the Root 
                                     * Port attached to this LAN, the Port 
                                     * Priority of this port is made worse. In 
                                     * this case, the values received from the 
                                     * Root Port will be the same as sent out by 
                                     * this Designated port, but with the old 
                                     * port priority value (which will be 
                                     * superior). In this case, the Port, which 
                                     * will still be a Designated Port, should 
                                     * update its designated and port priority
                                     * vector and transmit a BPDU with the new 
                                     * information. */
                                    pPerStPortInfo->u1SelectedPortRole =
                                        (UINT1) AST_PORT_ROLE_DESIGNATED;
                                    MstPUpdtAllSyncedFlag (u2MstInst,
                                                           pPerStPortInfo,
                                                           MST_SYNC_ROLE_UPDATE);

                                    AST_DBG_ARG2 (AST_RSSM_DBG,
                                                  "RSSM: Port %s: Inst %d Port Role Selected as -> DESIGNATED\n",
                                                  AST_GET_IFINDEX_STR
                                                  (u2PortNum), u2MstInst);

                                    pRstPortInfo->bUpdtInfo = MST_TRUE;
                                    MstPUpdtAllSyncedFlag (u2MstInst,
                                                           pPerStPortInfo,
                                                           MST_SYNC_BSELECT_UPDATE);
                                    pPortInfo->bAllTransmitReady = MST_FALSE;
                                }
                            }
                        }
                        else
                        {

                            pPerStPortInfo->u1SelectedPortRole =
                                (UINT1) AST_PORT_ROLE_DESIGNATED;
                            MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                                   MST_SYNC_ROLE_UPDATE);

                            AST_DBG_ARG2 (AST_RSSM_DBG,
                                          "RSSM: Port %s: Inst %d Port Role Selected as -> DESIGNATED\n",
                                          AST_GET_IFINDEX_STR (u2PortNum),
                                          u2MstInst);

                            pRstPortInfo->bUpdtInfo = MST_TRUE;
                            MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                                   MST_SYNC_BSELECT_UPDATE);
                            pPortInfo->bAllTransmitReady = MST_FALSE;
                        }
                    }
                    break;

                default:
                    return MST_FAILURE;
            }                    /* End Switch */

            if ((pPerStPortInfo->u1PortRole !=
                 pPerStPortInfo->u1SelectedPortRole))
            {
                /* Role Changed
                 * Send trap for role Change */
                AstNewPortRoleTrap ((UINT2) AST_GET_IFINDEX (u2PortNum),
                                    pPerStPortInfo->u1SelectedPortRole,
                                    pPerStPortInfo->u1PortRole,
                                    u2MstInst, (INT1 *) AST_MST_TRAPS_OID,
                                    AST_MST_TRAPS_OID_LEN);

                /* Set InstTriggerFlag 
                 * if (BoundaryPort or RegionalRoot) */
                AstGetRoleStr (pPerStPortInfo->u1PortRole, au1OldRole);
                AstGetRoleStr (pPerStPortInfo->u1SelectedPortRole, au1NewRole);
                AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
                UtlGetTimeStr (au1TimeStr);
                AST_SYS_TRC_ARG5 (AST_CONTROL_PATH_TRC,
                                  "Port:%s Instance:%u Old Role:%s New Role:%s Time Stamp: %s\r\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst,
                                  au1OldRole, au1NewRole, au1TimeStr);
                AST_MEMSET (au1OldRole, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);
                AST_MEMSET (au1NewRole, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);

                /* Set InstTriggerFlag 
                 * if (BoundaryPort or RegionalRoot) */
                if (bBoundaryPort == MST_TRUE || bRegionalRoot == MST_TRUE)
                {
                    AST_GET_INST_TRIGGER_FLAG = MST_TRUE;
                }
            }
        }
    }
    if (bSyncMaster == MST_TRUE)
    {
        /* SyncMaster should be called. */
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                      "RSSM: Inst %d: Calling SyncMaster Routine\n", u2MstInst);
        if (MstPRoleSelSmSyncMaster () != MST_SUCCESS)
        {

            AST_TRC_ARG1 (AST_CONTROL_PATH_TRC |
                          AST_ALL_FAILURE_TRC,
                          "RSSM: Inst %d: MstPRoleSelSmSyncMaster returned Failure!\n",
                          u2MstInst);
            return MST_FAILURE;
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstPRoleSelSmUpdtRolesMsti                 */
/*                                                                           */
/*    Description               : Function updates the Role for the ports for*/
/*                                given MSTI                                 */
/*                                                                           */
/*    Input(s)                  :  u2MstInst      - MST Instance Number for  */
/*                                                 which the SEM Operates.   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : MST_SUCCESS On Success.                    */
/*                                MST_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/

#ifdef __STDC__
INT4
MstPRoleSelSmUpdtRolesMsti (UINT2 u2MstInst)
#else
INT4
MstPRoleSelSmUpdtRolesMsti (u2MstInst)
     UINT2               u2MstInst;
#endif
{
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstPerStRstPortInfo *pRstPortInfo = NULL;
    tAstPerStBridgeInfo *pPerStCistBrgInfo = NULL;
    tAstPerStPortInfo  *pCistPerStPortInfo = NULL;
    tPerStCistMstiCommInfo *pPerStCistMstiPortInfo = NULL;
    tCistMstiPortInfo  *pCistMstiPortInfo = NULL;
    tRstPortInfo       *pRstPortEntry = NULL;
    tAstCommPortInfo   *pCommPortInfo = NULL;
    UINT1               au1RootBrgAddr[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1OldRootBrgAddr[AST_BRG_ADDR_DIS_LEN];
    CHR1                au1TimeStr[AST_BRG_ID_DIS_LEN];
    tAstBridgeId        MyBrgId;
    tAstBridgeId        TmpRootId;
    tAstBridgeId        TmpDesgBrgId;
    tAstBridgeId        TmpBrgId;
    UINT4               u4TmpRootCost = 0;
    UINT2               u2TmpDesgPort = 0;
    UINT4               u4Val = 0;
    UINT2               u2PortNum = 0;
    UINT2               u2TmpRootPort = 0;
    INT4                i4RetVal = 0;
    UINT2               u2Val = 0;
    UINT2               u2TmpRootPortId = 0;
    UINT2               u2PortId = 0;

    tAstBridgeId        CurRootId;
    tAstBridgeId        CurDesgBrgId;
    UINT4               u4CurRootCost = 0;
    UINT2               u2CurDesgPort = 0;
    UINT2               u2CurRootPort = 0;
    UINT1               u1SelectionLogic = AST_INIT_VAL;
    UINT1               u2UpdateFlag = MST_TRUE;
    UINT1               u1RotateLoop = MST_TRUE;
    UINT1               au1OldRole[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1NewRole[AST_BRG_ADDR_DIS_LEN];
    UINT1               au1OldState[AST_BRG_ADDR_DIS_LEN];
    AST_MEMSET (au1OldRole, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);
    AST_MEMSET (au1NewRole, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);

    AST_MEMSET (&TmpBrgId, AST_INIT_VAL, sizeof (TmpBrgId));

    AST_MEMSET (&MyBrgId, AST_INIT_VAL, sizeof (MyBrgId));
    AST_MEMSET (&TmpRootId, AST_INIT_VAL, sizeof (TmpRootId));
    AST_MEMSET (&TmpDesgBrgId, AST_INIT_VAL, sizeof (TmpDesgBrgId));
    AST_MEMSET (au1RootBrgAddr, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);
    AST_MEMSET (au1OldRootBrgAddr, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);
    AST_MEMSET (au1TimeStr, AST_INIT_VAL, AST_BRG_ID_DIS_LEN);
    AST_MEMSET (au1OldState, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);

    pBrgInfo = AST_GET_BRGENTRY ();
    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2MstInst);
    TmpRootId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
    AST_MEMCPY (&(TmpRootId.BridgeAddr),
                &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);
    u4TmpRootCost = 0;
    TmpDesgBrgId = TmpRootId;
    u2TmpDesgPort = 0;
    u2TmpRootPort = 0;

    pPerStCistBrgInfo = AST_GET_PERST_BRG_INFO (MST_CIST_CONTEXT);
    TmpBrgId.u2BrgPriority = pPerStCistBrgInfo->u2BrgPriority;
    AST_MEMCPY (&(TmpBrgId.BridgeAddr),
                &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);

    MyBrgId = TmpRootId;

    CurDesgBrgId = TmpDesgBrgId;
    u2CurDesgPort = u2TmpDesgPort;
    CurRootId = TmpRootId;
    u4CurRootCost = u4TmpRootCost;
    u2CurRootPort = u2TmpRootPort;

    if (AST_IS_MST_ENABLED () && (gu2AstRecvPort != AST_INIT_VAL))
    {
        if ((pPortInfo = AST_GET_PORTENTRY (gu2AstRecvPort)) != NULL)
        {
            if ((pPerStPortInfo =
                 AST_GET_PERST_PORT_INFO (gu2AstRecvPort, u2MstInst)) != NULL)
            {
                if ((pRstPortInfo =
                     AST_GET_RST_PORT_INFO (pPerStPortInfo)) != NULL)
                {
                    if ((pRstPortInfo->u1InfoIs == (UINT1) RST_INFOIS_RECEIVED))
                    {
                        u1SelectionLogic = MST_TRUE;
                        pCistMstiPortInfo =
                            AST_GET_CIST_MSTI_PORT_INFO (gu2AstRecvPort);
                        if (pCistMstiPortInfo->bInfoInternal != MST_FALSE)
                        {
                            u2CurRootPort = pPerStBrgInfo->u2RootPort;
                            CurRootId = pPerStBrgInfo->RootId;
                            CurDesgBrgId = pPerStBrgInfo->DesgBrgId;
                            u2CurDesgPort = pPerStBrgInfo->u2DesgPort;
                            u4CurRootCost = pPerStBrgInfo->u4RootCost;

                        }
                    }
                }
            }
        }
    }
    if (u1SelectionLogic == MST_TRUE)
    {
        /* Port Message is a Received Message */
        if (AST_MEMCMP (&(MyBrgId.BridgeAddr[0]),
                        &(pPerStPortInfo->DesgBrgId.BridgeAddr[0]),
                        AST_MAC_ADDR_SIZE) == AST_BRGID1_SAME)
        {
            u2TmpRootPort = u2CurRootPort;
            TmpRootId = CurRootId;
            TmpDesgBrgId = CurDesgBrgId;
            u4TmpRootCost = u4CurRootCost;
            u2TmpDesgPort = u2CurDesgPort;
        }
        else
        {
            i4RetVal = AST_COMPARE_BRGID (&(pPerStPortInfo->RootId),
                                          &(CurRootId));
            switch (i4RetVal)
            {
                case AST_BRGID1_SUPERIOR:
                    TmpRootId = pPerStPortInfo->RootId;
                    u4TmpRootCost = pPerStPortInfo->u4RootCost +
                        pPerStPortInfo->u4PortPathCost;
                    TmpDesgBrgId = pPerStPortInfo->DesgBrgId;
                    u2TmpDesgPort = pPerStPortInfo->u2DesgPortId;
                    u2TmpRootPort = gu2AstRecvPort;
                    break;

                case AST_BRGID1_SAME:
                    TmpRootId = pPerStPortInfo->RootId;
                    u4Val =
                        pPerStPortInfo->u4RootCost +
                        pPerStPortInfo->u4PortPathCost;
                    if (u4Val < u4CurRootCost)
                    {
                        u4TmpRootCost = u4Val;
                        TmpDesgBrgId = pPerStPortInfo->DesgBrgId;
                        u2TmpDesgPort = pPerStPortInfo->u2DesgPortId;
                        u2TmpRootPort = gu2AstRecvPort;
                    }
                    else if (u4Val == u4CurRootCost)
                    {
                        u4TmpRootCost = u4Val;
                        i4RetVal =
                            AST_COMPARE_BRGID (&(pPerStPortInfo->DesgBrgId),
                                               &(CurDesgBrgId));
                        switch (i4RetVal)
                        {
                            case AST_BRGID1_SUPERIOR:
                                TmpDesgBrgId = pPerStPortInfo->DesgBrgId;
                                u2TmpDesgPort = pPerStPortInfo->u2DesgPortId;
                                u2TmpRootPort = gu2AstRecvPort;
                                break;

                            case AST_BRGID1_SAME:
                                TmpDesgBrgId = pPerStPortInfo->DesgBrgId;
                                if (pPerStPortInfo->u2DesgPortId <
                                    u2CurDesgPort)
                                {
                                    u2TmpDesgPort =
                                        pPerStPortInfo->u2DesgPortId;
                                    u2TmpRootPort = gu2AstRecvPort;
                                }
                                else if (pPerStPortInfo->u2DesgPortId ==
                                         u2CurDesgPort)
                                {
                                    u2TmpDesgPort =
                                        pPerStPortInfo->u2DesgPortId;

                                    u2TmpRootPortId = (UINT2)
                                        (AST_GET_PERST_PORT_INFO
                                         (u2CurRootPort, u2MstInst)->
                                         u1PortPriority);
                                    u2TmpRootPortId =
                                        (UINT2) (u2TmpRootPortId << 8);
                                    u2TmpRootPortId =
                                        u2TmpRootPortId |
                                        AST_GET_PROTOCOL_PORT (u2CurRootPort);
                                    u2PortId =
                                        (UINT2) (pPerStPortInfo->
                                                 u1PortPriority);
                                    u2PortId = (UINT2) (u2PortId << 8);

                                    u2PortId = u2PortId |
                                        AST_GET_PROTOCOL_PORT (gu2AstRecvPort);

                                    if (u2PortId <= u2TmpRootPortId)
                                    {
                                        u2TmpRootPort = gu2AstRecvPort;
                                    }
                                    else
                                        u2UpdateFlag = AST_INIT_VAL;
                                }
                                else
                                    u2UpdateFlag = AST_INIT_VAL;
                                break;

                            default:
                                u2UpdateFlag = AST_INIT_VAL;
                                break;
                        }        /* End switch(i4RetVal) */
                    }
                    else
                        u2UpdateFlag = AST_INIT_VAL;
                    break;

                case RST_BRGID1_INFERIOR:
                    if ((pPerStPortInfo->u1PortRole == MST_PORT_ROLE_ROOT)
                        || (pPerStPortInfo->u1PortRole == MST_PORT_ROLE_MASTER))
                    {

                        TmpRootId = MyBrgId;
                        u4TmpRootCost = 0;
                        TmpDesgBrgId = MyBrgId;
                        u2TmpDesgPort = 0;
                        u2TmpRootPort = 0;
                        pPerStBrgInfo->u2InferiorRcvdRootPort = gu2AstRecvPort;
                    }
                    else
                        u2UpdateFlag = AST_INIT_VAL;
                    break;

                default:
                    u2UpdateFlag = AST_INIT_VAL;
                    break;
            }
        }

        /*Update the global info */
        if (u2UpdateFlag == MST_TRUE)
        {
            CurDesgBrgId = TmpDesgBrgId;
            u2CurDesgPort = u2TmpDesgPort;
            CurRootId = TmpRootId;
            u4CurRootCost = u4TmpRootCost;
            u2CurRootPort = u2TmpRootPort;
            u1RotateLoop = MST_FALSE;
        }
        else if (u2UpdateFlag == AST_INIT_VAL)
        {
            if (!((pPerStPortInfo->u1PortRole == MST_PORT_ROLE_ROOT)
                  || (pPerStPortInfo->u1PortRole == MST_PORT_ROLE_MASTER)))
            {
                TmpDesgBrgId = CurDesgBrgId;
                u2TmpDesgPort = u2CurDesgPort;
                TmpRootId = CurRootId;
                u4TmpRootCost = u4CurRootCost;
                u2TmpRootPort = u2CurRootPort;
                u1RotateLoop = MST_FALSE;
            }
            else
            {
                if ((pPerStPortInfo->u1PortRole == MST_PORT_ROLE_ROOT)
                    || (pPerStPortInfo->u1PortRole == MST_PORT_ROLE_MASTER))
                {
                    TmpRootId = MyBrgId;
                    u4TmpRootCost = 0;
                    TmpDesgBrgId = MyBrgId;
                    u2TmpDesgPort = 0;
                    u2TmpRootPort = 0;
                    pPerStBrgInfo->u2InferiorRcvdRootPort = gu2AstRecvPort;

                }
            }

        }
    }

    if (u1RotateLoop == MST_TRUE)
    {

        AST_SCAN_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList,
                           pRstPortEntry, tRstPortInfo *)
        {
            u2PortNum = pRstPortEntry->u2PortNum;

            if (u2PortNum == 0)
            {
                continue;
            }

            if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) == NULL)
            {
                continue;
            }

            if (AST_IS_MST_ENABLED ())
            {
                pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);

                if (pPerStPortInfo == NULL)
                {
                    continue;
                }

                pRstPortInfo = AST_GET_RST_PORT_INFO (pPerStPortInfo);

                if (pRstPortInfo->bPortEnabled == (tAstBoolean) MST_FALSE)
                {
                    continue;
                }

                /* If restrictedRole is set, then don't consider this port
                 * for bridge root priority vector calculations. */
                if (AST_PORT_RESTRICTED_ROLE (pPortInfo) ==
                    (tAstBoolean) RST_TRUE)
                {
                    continue;
                }

                /* This check is added apart from 802.1s to avoid information 
                 * received from outside the region from being included in role 
                 * selection and to select a new Msti Root as soon as any change 
                 * in the region occurs */
                pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (u2PortNum);
                if (pCistMstiPortInfo->bInfoInternal == MST_FALSE)
                {
                    continue;
                }

                if (pRstPortInfo->u1InfoIs == (UINT1) RST_INFOIS_RECEIVED)
                {
                    /* Port Message is a Received Message */
                    if (AST_MEMCMP (&(MyBrgId.BridgeAddr[0]),
                                    &(pPerStPortInfo->DesgBrgId.BridgeAddr[0]),
                                    AST_MAC_ADDR_SIZE) == AST_BRGID1_SAME)
                    {
                        continue;
                    }

                    i4RetVal = AST_COMPARE_BRGID (&(pPerStPortInfo->RootId),
                                                  &(TmpRootId));
                    switch (i4RetVal)
                    {
                        case AST_BRGID1_SUPERIOR:
                            TmpRootId = pPerStPortInfo->RootId;
                            u4TmpRootCost = pPerStPortInfo->u4RootCost +
                                pPerStPortInfo->u4PortPathCost;
                            TmpDesgBrgId = pPerStPortInfo->DesgBrgId;
                            u2TmpDesgPort = pPerStPortInfo->u2DesgPortId;
                            u2TmpRootPort = u2PortNum;
                            break;

                        case AST_BRGID1_SAME:
                            u4Val =
                                pPerStPortInfo->u4RootCost +
                                pPerStPortInfo->u4PortPathCost;
                            if (u4Val < u4TmpRootCost)
                            {
                                u4TmpRootCost = u4Val;
                                TmpDesgBrgId = pPerStPortInfo->DesgBrgId;
                                u2TmpDesgPort = pPerStPortInfo->u2DesgPortId;
                                u2TmpRootPort = u2PortNum;
                            }
                            else if (u4Val == u4TmpRootCost)
                            {
                                i4RetVal =
                                    AST_COMPARE_BRGID (&
                                                       (pPerStPortInfo->
                                                        DesgBrgId),
                                                       &(TmpDesgBrgId));
                                switch (i4RetVal)
                                {
                                    case AST_BRGID1_SUPERIOR:
                                        TmpDesgBrgId =
                                            pPerStPortInfo->DesgBrgId;
                                        u2TmpDesgPort =
                                            pPerStPortInfo->u2DesgPortId;
                                        u2TmpRootPort = u2PortNum;
                                        break;

                                    case AST_BRGID1_SAME:
                                        if (pPerStPortInfo->u2DesgPortId <
                                            u2TmpDesgPort)
                                        {
                                            u2TmpDesgPort =
                                                pPerStPortInfo->u2DesgPortId;
                                            u2TmpRootPort = u2PortNum;
                                        }
                                        else if (pPerStPortInfo->u2DesgPortId ==
                                                 u2TmpDesgPort)
                                        {
                                            u2TmpRootPortId = (UINT2)
                                                (AST_GET_PERST_PORT_INFO
                                                 (u2TmpRootPort, u2MstInst)->
                                                 u1PortPriority);
                                            u2TmpRootPortId =
                                                (UINT2) (u2TmpRootPortId << 8);
                                            u2TmpRootPortId =
                                                u2TmpRootPortId |
                                                AST_GET_LOCAL_PORT
                                                (u2TmpRootPort);
                                            u2PortId =
                                                (UINT2) (pPerStPortInfo->
                                                         u1PortPriority);
                                            u2PortId = (UINT2) (u2PortId << 8);

                                            u2PortId = u2PortId |
                                                AST_GET_LOCAL_PORT (u2PortNum);

                                            if (u2PortId < u2TmpRootPortId)
                                            {
                                                u2TmpRootPort = u2PortNum;
                                            }
                                        }
                                        break;

                                    default:
                                        break;
                                }    /* End switch(i4RetVal) */
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }                        /* End for (u2PortNum = 1; ...) */
    }

    if (u2TmpRootPort != AST_INIT_VAL)
    {
        AST_TRC_ARG1 (AST_CONTROL_PATH_TRC,
                      "RSSM: << ----Port %s selected as Root Port---- >>\n",
                      AST_GET_IFINDEX_STR (u2TmpRootPort));

        AST_DBG_ARG1 (AST_RSSM_DBG,
                      "RSSM: << ----Port %s selected as Root Port---- >>\n",
                      AST_GET_IFINDEX_STR (u2TmpRootPort));

        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2TmpRootPort, u2MstInst);
        /* Calculate if there is any change in Root Bridge */
        i4RetVal =
            AST_MEMCMP (&(pPerStBrgInfo->RootId.BridgeAddr),
                        &(TmpRootId.BridgeAddr), AST_MAC_ADDR_LEN);
        if (i4RetVal != 0)
        {
            /* Store the OldRootId */
            pPerStBrgInfo->OldRootId = pPerStBrgInfo->RootId;
            /* Incrementing the New Root Bridge Count and generate Trap */
            AST_INCR_NEW_ROOTBRIDGE_COUNT (u2MstInst);
            PrintMacAddress (pPerStBrgInfo->RootId.BridgeAddr,
                             au1OldRootBrgAddr);
            PrintMacAddress (TmpRootId.BridgeAddr, au1RootBrgAddr);
            UtlGetTimeStr (au1TimeStr);
            AST_SYS_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                              "Instance:%u Old Root:%s New Root:%s Time Stamp: %s\r\n",
                              u2MstInst, au1OldRootBrgAddr, au1RootBrgAddr,
                              au1TimeStr);

            AstNewRootTrap (pPerStBrgInfo->RootId, TmpRootId, u2MstInst,
                            (INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);

        }

        /* As per 802.1Q std, update the value of rootremainingHops to
         * portRemainingHops - 1, provided chosen RPV is not equal to BPV
         * */
        pPerStBrgInfo->u1RootRemainingHops
            = (UINT1) (pPerStPortInfo->PerStCistMstiCommPortInfo.
                       u1PortRemainingHops - 1);
    }
    else
    {

        AST_DBG (AST_RSSM_DBG,
                 "RSSM: << ----This Bridge is the Root Bridge---- >>\n");
        /* Calculate if there is any change in Root Bridge */
        i4RetVal =
            AST_MEMCMP (&(pPerStBrgInfo->RootId.BridgeAddr),
                        &(TmpRootId.BridgeAddr), AST_MAC_ADDR_LEN);
        if (i4RetVal != 0)
        {
            /* Store the OldRootId */
            pPerStBrgInfo->OldRootId = pPerStBrgInfo->RootId;
            /* Incrementing the New Root Bridge Count and generate Trap */
            AST_INCR_NEW_ROOTBRIDGE_COUNT (u2MstInst);
            PrintMacAddress (pPerStBrgInfo->RootId.BridgeAddr,
                             au1OldRootBrgAddr);
            PrintMacAddress (TmpRootId.BridgeAddr, au1RootBrgAddr);
            UtlGetTimeStr (au1TimeStr);
            AST_SYS_TRC_ARG4 (AST_CONTROL_PATH_TRC,
                              "Instance:%u Old Root:%s New Root:%s Time Stamp: %s\r\n",
                              u2MstInst, au1OldRootBrgAddr, au1RootBrgAddr,
                              au1TimeStr);

            AstNewRootTrap (pPerStBrgInfo->RootId, TmpRootId, u2MstInst,
                            (INT1 *) AST_MST_TRAPS_OID, AST_MST_TRAPS_OID_LEN);
        }
        pPerStBrgInfo->u1RootRemainingHops = pPerStBrgInfo->u1BrgRemainingHops;
    }

    /* Now the Chosen Root Priority Vector is copied into global 
     * Root Priority Vector
     */
    pPerStBrgInfo->u2RootPort = u2TmpRootPort;
    pPerStBrgInfo->RootId = TmpRootId;
    pPerStBrgInfo->u4RootCost = u4TmpRootCost;

    pPerStBrgInfo->DesgBrgId = TmpDesgBrgId;
    pPerStBrgInfo->u2DesgPort = u2TmpDesgPort;

    AST_SCAN_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList,
                       pRstPortEntry, tRstPortInfo *)
    {
        u2PortNum = pRstPortEntry->u2PortNum;

        if (u2PortNum == 0)
        {
            continue;
        }

        if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) == NULL)
        {
            /* Port is not yet created */
            continue;
        }

        pCistPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum,
                                                      MST_CIST_CONTEXT);
        pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst);
        pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
        if (pPerStPortInfo == NULL)
        {
            /* This port is not a member of this instance */
            continue;
        }

        /* Clear the Reselect flag for all  */
        /*port belonging to the given Spanning Tree context. */

        AST_DBG_ARG2 (AST_RSSM_DBG,
                      "RSSM: Port %s: Inst %d: Cleared RESELECT variable \n",
                      AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        pPerStPortInfo->PerStRstPortInfo.bReSelect = MST_FALSE;

        pPerStPortInfo->PerStCistMstiCommPortInfo.u1DesgRemainingHops
            = pPerStBrgInfo->u1RootRemainingHops;

        pRstPortInfo = &(pPerStPortInfo->PerStRstPortInfo);
        pPerStCistMstiPortInfo = MST_GET_CIST_MSTI_PORT_INFO (pPerStPortInfo);

        if (pRstPortInfo->u1InfoIs == MST_INFOIS_DISABLED)
        {
            AST_DBG_ARG1 (AST_RSSM_DBG,
                          "RSSM: Port %s: InfoIs is DISABLED\n",
                          AST_GET_IFINDEX_STR (u2PortNum));

            pPerStPortInfo->u1SelectedPortRole = (UINT1) MST_PORT_ROLE_DISABLED;
            MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                   MST_SYNC_ROLE_UPDATE);

            AST_DBG_ARG2 (AST_RSSM_DBG,
                          "RSSM: Port %s: Inst %d Port Role Selected as -> DISABLED\n",
                          AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
        }
        else
        {
            if ((pCistPerStPortInfo->u1SelectedPortRole == MST_PORT_ROLE_ROOT)
                && (pCistPerStPortInfo->PerStRstPortInfo.u1InfoIs ==
                    MST_INFOIS_RECEIVED)
                && (pPortInfo->CistMstiPortInfo.bInfoInternal == MST_FALSE))
            {

                AST_DBG_ARG1 (AST_RSSM_DBG,
                              "RSSM: Port %s: InfoIs is RECEIVED\n",
                              AST_GET_IFINDEX_STR (u2PortNum));

                pPerStPortInfo->u1SelectedPortRole =
                    (UINT1) MST_PORT_ROLE_MASTER;
                MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                       MST_SYNC_ROLE_UPDATE);

                AST_DBG_ARG2 (AST_RSSM_DBG,
                              "RSSM: Port %s: Inst %d Port Role Selected as -> MASTER\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);
                i4RetVal = MstIsDesgPrBetterThanPortPr (u2PortNum, u2MstInst);
                if (i4RetVal == RST_SAME_MSG)
                {
                    if ((pPerStCistMstiPortInfo->u1PortRemainingHops !=
                         pPerStBrgInfo->u1RootRemainingHops))
                    {
                        pRstPortInfo->bUpdtInfo = MST_TRUE;
                        MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                               MST_SYNC_BSELECT_UPDATE);
                        pPortInfo->bAllTransmitReady = MST_FALSE;
                    }
                }
                else
                {
                    pRstPortInfo->bUpdtInfo = MST_TRUE;
                    MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                           MST_SYNC_BSELECT_UPDATE);
                    pPortInfo->bAllTransmitReady = MST_FALSE;
                }
            }
            else if ((pCistPerStPortInfo->u1SelectedPortRole ==
                      MST_PORT_ROLE_ALTERNATE)
                     &&
                     (pCistPerStPortInfo->PerStRstPortInfo.u1InfoIs ==
                      MST_INFOIS_RECEIVED)
                     && (pPortInfo->CistMstiPortInfo.bInfoInternal ==
                         MST_FALSE))
            {

                AST_DBG_ARG1 (AST_RSSM_DBG,
                              "RSSM: Port %s: InfoIs is RECEIVED\n",
                              AST_GET_IFINDEX_STR (u2PortNum));

                pPerStPortInfo->u1SelectedPortRole =
                    (UINT1) MST_PORT_ROLE_ALTERNATE;
                MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                       MST_SYNC_ROLE_UPDATE);

                AST_DBG_ARG2 (AST_RSSM_DBG,
                              "RSSM: Port %s: Inst %d Port Role Selected as -> ALTERNATE\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

                i4RetVal = MstIsDesgPrBetterThanPortPr (u2PortNum, u2MstInst);
                if (i4RetVal == RST_SAME_MSG)
                {
                    if ((pPerStCistMstiPortInfo->u1PortRemainingHops !=
                         pPerStBrgInfo->u1RootRemainingHops))
                    {

                        pRstPortInfo->bUpdtInfo = MST_TRUE;
                        MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                               MST_SYNC_BSELECT_UPDATE);
                        pPortInfo->bAllTransmitReady = MST_FALSE;
                    }
                }
                else
                {

                    pRstPortInfo->bUpdtInfo = MST_TRUE;
                    MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                           MST_SYNC_BSELECT_UPDATE);
                    pPortInfo->bAllTransmitReady = MST_FALSE;
                }
            }

            /* When BPDU received from a Designated Port on the same bridge
             * and the information on the Selected Port is received from the Designated Port,
             * the port role should be BackUp - IEEE 802.1q Clause 13.26.23 updtRolesTree() - (l)*/
            else if ((pCistPerStPortInfo->u1SelectedPortRole ==
                      MST_PORT_ROLE_BACKUP)
                     &&
                     (pCistPerStPortInfo->PerStRstPortInfo.u1InfoIs ==
                      MST_INFOIS_RECEIVED)
                     && (pPortInfo->CistMstiPortInfo.bInfoInternal ==
                         MST_FALSE))
            {

                AST_DBG_ARG1 (AST_RSSM_DBG,
                              "RSSM: Port %s: InfoIs is RECEIVED\n",
                              AST_GET_IFINDEX_STR (u2PortNum));

                pPerStPortInfo->u1SelectedPortRole =
                    (UINT1) MST_PORT_ROLE_BACKUP;
                MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                       MST_SYNC_ROLE_UPDATE);

                AST_DBG_ARG2 (AST_RSSM_DBG,
                              "RSSM: Port %s: Inst %d Port Role Selected as -> BACKUP\n",
                              AST_GET_IFINDEX_STR (u2PortNum), u2MstInst);

                i4RetVal = MstIsDesgPrBetterThanPortPr (u2PortNum, u2MstInst);
                if (i4RetVal == RST_SAME_MSG)
                {
                    if ((pPerStCistMstiPortInfo->u1PortRemainingHops !=
                         pPerStBrgInfo->u1RootRemainingHops))
                    {

                        pRstPortInfo->bUpdtInfo = MST_TRUE;
                        MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                               MST_SYNC_BSELECT_UPDATE);
                        pPortInfo->bAllTransmitReady = MST_FALSE;
                    }
                }
                else
                {

                    pRstPortInfo->bUpdtInfo = MST_TRUE;
                    MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                           MST_SYNC_BSELECT_UPDATE);
                    pPortInfo->bAllTransmitReady = MST_FALSE;
                }
            }

            else if ((pCistPerStPortInfo->PerStRstPortInfo.u1InfoIs !=
                      MST_INFOIS_RECEIVED)
                     || (pPortInfo->CistMstiPortInfo.bInfoInternal == MST_TRUE))
            {
                switch (pRstPortInfo->u1InfoIs)
                {

                    case RST_INFOIS_AGED:
                        AST_DBG_ARG1 (AST_RSSM_DBG,
                                      "RSSM: Port %s: InfoIs is AGED\n",
                                      AST_GET_IFINDEX_STR (u2PortNum));

                        pPerStPortInfo->u1SelectedPortRole =
                            (UINT1) MST_PORT_ROLE_DESIGNATED;
                        MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                               MST_SYNC_ROLE_UPDATE);
                        pRstPortInfo->bUpdtInfo = MST_TRUE;
                        MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                               MST_SYNC_BSELECT_UPDATE);
                        pPortInfo->bAllTransmitReady = MST_FALSE;
                        AST_DBG_ARG2 (AST_RSSM_DBG,
                                      "RSSM: Port %s: Inst %d Port Role Selected as -> DESIGNATED\n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      u2MstInst);
                        break;

                    case RST_INFOIS_MINE:

                        AST_DBG_ARG1 (AST_RSSM_DBG,
                                      "RSSM: Port %s: InfoIs is MINE\n",
                                      AST_GET_IFINDEX_STR (u2PortNum));
                        if ((pPortInfo->bLoopGuard == RST_TRUE) &&
                            (pPerStPortInfo->bLoopGuardStatus == RST_TRUE) &&
                            (pPortInfo->bOperPointToPoint == RST_TRUE)
                            && ((pCommPortInfo->pEdgeDelayWhileTmr != NULL) &&
                                (pPerStPortInfo->PerStRstPortInfo.
                                 pRcvdInfoTmr == NULL)))
                        {
                            break;
                        }

                        pPerStPortInfo->u1SelectedPortRole =
                            (UINT1) MST_PORT_ROLE_DESIGNATED;
                        MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                               MST_SYNC_ROLE_UPDATE);
                        AST_DBG_ARG2 (AST_RSSM_DBG,
                                      "RSSM: Port %s: Inst %d Port Role Selected as -> DESIGNATED\n",
                                      AST_GET_IFINDEX_STR (u2PortNum),
                                      u2MstInst);

                        i4RetVal =
                            MstIsDesgPrBetterThanPortPr (u2PortNum, u2MstInst);
                        if (i4RetVal == RST_SAME_MSG)
                        {

                            if ((pPerStCistMstiPortInfo->u1PortRemainingHops !=
                                 pPerStBrgInfo->u1RootRemainingHops))
                            {

                                pRstPortInfo->bUpdtInfo = MST_TRUE;
                                MstPUpdtAllSyncedFlag (u2MstInst,
                                                       pPerStPortInfo,
                                                       MST_SYNC_BSELECT_UPDATE);
                                pPortInfo->bAllTransmitReady = MST_FALSE;
                            }
                        }
                        else
                        {

                            pRstPortInfo->bUpdtInfo = MST_TRUE;
                            MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                                   MST_SYNC_BSELECT_UPDATE);
                            pPortInfo->bAllTransmitReady = MST_FALSE;
                        }

                        break;

                    case RST_INFOIS_RECEIVED:

                        AST_DBG_ARG1 (AST_RSSM_DBG,
                                      "RSSM: Port %s: InfoIs is RECEIVED\n",
                                      AST_GET_IFINDEX_STR (u2PortNum));
                        if (u2TmpRootPort == u2PortNum)
                        {

                            pPerStPortInfo->u1SelectedPortRole =
                                (UINT1) AST_PORT_ROLE_ROOT;
                            MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                                   MST_SYNC_ROLE_UPDATE);
                            pPortInfo->CistMstiPortInfo.bCistRootPort =
                                MST_TRUE;
                            pPortInfo->CistMstiPortInfo.bCistDesignatedPort =
                                MST_FALSE;

                            AST_DBG_ARG2 (AST_RSSM_DBG,
                                          "RSSM: Port %s: Inst %d Port Role Selected as -> ROOT PORT\n",
                                          AST_GET_IFINDEX_STR (u2PortNum),
                                          u2MstInst);
                            pRstPortInfo->bUpdtInfo = MST_FALSE;
                            MstPUpdtAllSyncedFlag (u2MstInst, pPerStPortInfo,
                                                   MST_SYNC_BSELECT_UPDATE);
                            if (pRstPortInfo->bSelected == MST_TRUE)
                            {
                                pPortInfo->bAllTransmitReady = MST_TRUE;
                            }
                        }
                        else
                        {

                            i4RetVal =
                                MstIsDesgPrBetterThanPortPr (u2PortNum,
                                                             u2MstInst);
                            if (i4RetVal != RST_BETTER_MSG)
                            {
                                if (AST_MEMCMP
                                    (&(pPerStPortInfo->DesgBrgId.BridgeAddr[0]),
                                     &(MyBrgId.BridgeAddr[0]),
                                     AST_MAC_ADDR_SIZE) != AST_BRGID1_SAME)
                                {

                                    pPerStPortInfo->u1SelectedPortRole
                                        = (UINT1) AST_PORT_ROLE_ALTERNATE;
                                    pPortInfo->CistMstiPortInfo.
                                        bCistDesignatedPort = MST_FALSE;
                                    pPortInfo->CistMstiPortInfo.
                                        bCistRootPort = MST_FALSE;

                                    AST_DBG_ARG2 (AST_RSSM_DBG,
                                                  "RSSM: Port %s: Inst %d Port Role Selected as -> ALTERNATE\n",
                                                  AST_GET_IFINDEX_STR
                                                  (u2PortNum), u2MstInst);
                                    pRstPortInfo->bUpdtInfo = MST_FALSE;
                                    MstPUpdtAllSyncedFlag (u2MstInst,
                                                           pPerStPortInfo,
                                                           MST_SYNC_BSELECT_UPDATE);
                                    if (pRstPortInfo->bSelected == MST_TRUE)
                                    {
                                        pPortInfo->bAllTransmitReady = MST_TRUE;
                                    }

                                }
                                else
                                {

                                    u2Val =
                                        (UINT2) (pPerStPortInfo->
                                                 u2DesgPortId & (UINT2)
                                                 AST_PORTNUM_MASK);
                                    if (u2Val != u2PortNum)
                                    {

                                        pPerStPortInfo->u1SelectedPortRole =
                                            (UINT1) AST_PORT_ROLE_BACKUP;
                                        MstPUpdtAllSyncedFlag (u2MstInst,
                                                               pPerStPortInfo,
                                                               MST_SYNC_ROLE_UPDATE);

                                        AST_DBG_ARG2 (AST_RSSM_DBG,
                                                      "RSSM: Port %s: Inst %d Port Role Selected as -> BACKUP\n",
                                                      AST_GET_IFINDEX_STR
                                                      (u2PortNum), u2MstInst);
                                        pRstPortInfo->bUpdtInfo = MST_FALSE;
                                        MstPUpdtAllSyncedFlag (u2MstInst,
                                                               pPerStPortInfo,
                                                               MST_SYNC_BSELECT_UPDATE);
                                        if (pRstPortInfo->bSelected == MST_TRUE)
                                        {
                                            pPortInfo->bAllTransmitReady =
                                                MST_TRUE;
                                        }

                                    }
                                    else
                                    {

                                        /* This should not happen since the port priority
                                         * vector cannot have the designated bridgeid and
                                         * designated portid as its own bridgeid and 
                                         * port id along with the InfoIs as RECEIVED
                                         */
                                        return MST_FAILURE;
                                    }
                                }
                            }
                            else
                            {

                                pPerStPortInfo->u1SelectedPortRole =
                                    (UINT1) AST_PORT_ROLE_DESIGNATED;
                                MstPUpdtAllSyncedFlag (u2MstInst,
                                                       pPerStPortInfo,
                                                       MST_SYNC_ROLE_UPDATE);

                                AST_DBG_ARG2 (AST_RSSM_DBG,
                                              "RSSM: Port %s: Inst %d Port Role Selected as -> DESIGNATED\n",
                                              AST_GET_IFINDEX_STR (u2PortNum),
                                              u2MstInst);
                                pRstPortInfo->bUpdtInfo = MST_TRUE;
                                MstPUpdtAllSyncedFlag (u2MstInst,
                                                       pPerStPortInfo,
                                                       MST_SYNC_BSELECT_UPDATE);
                                pPortInfo->bAllTransmitReady = MST_FALSE;
                            }
                        }
                        break;

                    default:
                        return MST_FAILURE;
                }                /* End Switch */
            }
        }
        if (pPerStPortInfo->u1SelectedPortRole != pPerStPortInfo->u1PortRole)
        {
            /* Role Changed
             * Send trap for role Change */
            if (MstIsPortMemberOfInst
                ((INT4) (AST_GET_IFINDEX ((INT4) u2PortNum)),
                 (INT4) u2MstInst) != MST_FALSE)
            {
                AstNewPortRoleTrap ((UINT2) AST_GET_IFINDEX (u2PortNum),
                                    pPerStPortInfo->u1SelectedPortRole,
                                    pPerStPortInfo->u1PortRole,
                                    u2MstInst, (INT1 *) AST_MST_TRAPS_OID,
                                    AST_MST_TRAPS_OID_LEN);
                AstGetRoleStr (pPerStPortInfo->u1PortRole, au1OldRole);
                AstGetRoleStr (pPerStPortInfo->u1SelectedPortRole, au1NewRole);
                AST_MEMSET (au1TimeStr, 0, AST_BRG_ID_DIS_LEN);
                UtlGetTimeStr (au1TimeStr);
                AST_SYS_TRC_ARG5 (AST_CONTROL_PATH_TRC,
                                  "Port:%s Instance:%u Old Role:%s New Role:%s Time Stamp: %s\r\n",
                                  AST_GET_IFINDEX_STR (u2PortNum), u2MstInst,
                                  au1OldRole, au1NewRole, au1TimeStr);
                AST_MEMSET (au1OldRole, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);
                AST_MEMSET (au1NewRole, AST_INIT_VAL, AST_BRG_ADDR_DIS_LEN);
            }
        }
    }
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstPRoleSelSmSyncMaster                              */
/*                                                                           */
/* Description        : This performs the routine 'syncMaster' of 802.1Q     */
/*                      It clears the agree, agreed and synced variables     */
/*                      and sets the sync variable                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_SUCCESS / MST_FAILURE                            */
/*****************************************************************************/
INT4
MstPRoleSelSmSyncMaster (VOID)
{
    tAstPerStRstPortInfo *pPerStRstPortInfo = NULL;
    tAstPerStInfo      *pPerStInfo = NULL;
    tAstPortEntry      *pPortInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tCistMstiPortInfo  *pCistMstiPortInfo = NULL;
    tRstPortInfo       *pRstPortEntry = NULL;
    UINT2               u2Inst = 1;
    UINT2               u2PortNum = 0;

    AST_GET_NEXT_BUF_INSTANCE (u2Inst, pPerStInfo)
    {
        if (pPerStInfo == NULL)
        {
            continue;
        }

        AST_SCAN_PORT_BUF (&(AST_CURR_CONTEXT_INFO ())->PortList,
                           pRstPortEntry, tRstPortInfo *)
        {
            u2PortNum = pRstPortEntry->u2PortNum;

            if (u2PortNum == 0)
            {
                continue;
            }

            if ((pPortInfo = AST_GET_PORTENTRY (u2PortNum)) == NULL)
            {
                continue;
            }
            pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2Inst);

            if (pPerStPortInfo == NULL)
            {
                continue;
            }
            if (pPortInfo->u1EntryStatus != (UINT1) AST_PORT_OPER_UP)
            {
                continue;
            }

            pPerStRstPortInfo = AST_GET_PERST_RST_PORT_INFO (u2PortNum, u2Inst);
            if (pPerStRstPortInfo == NULL)
            {
                /* Port Entry not present
                 * */
                continue;

            }
            pCistMstiPortInfo = AST_GET_CIST_MSTI_PORT_INFO (u2PortNum);
            /* As per IEEE 802.1Q sync Master acts upon the ports internal to the
             * region.
             */
            if (pCistMstiPortInfo->bInfoInternal == MST_TRUE)
            {
                pPerStRstPortInfo->bAgreed = MST_FALSE;
                pPerStRstPortInfo->bAgree = MST_FALSE;
                pPerStRstPortInfo->bSynced = MST_FALSE;
                MstPUpdtAllSyncedFlag (u2Inst, pPerStPortInfo,
                                       MST_SYNC_BSYNCED_UPDATE);
                pPerStRstPortInfo->bSync = MST_TRUE;

                if (AST_GET_INST_TRIGGER_FLAG == MST_FALSE)
                {
                    if (MstPortRoleTransitMachine (MST_PROLETRSM_EV_SYNC_SET,
                                                   pPerStPortInfo,
                                                   u2Inst) != MST_SUCCESS)
                    {
                        AST_DBG_ARG2 (AST_RTSM_DBG | AST_RTSM_DBG |
                                      AST_ALL_FAILURE_DBG,
                                      "RTSM: Port %s: Inst %d: Port Role Transition Machine returned FAILURE for SYNC_SET event!\n",
                                      AST_GET_IFINDEX_STR (u2PortNum), u2Inst);
                        return MST_FAILURE;
                    }
                }
            }
        }
    }

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstIsDesgPrBetterThanPortPr                          */
/*                                                                           */
/* Description        : This routine compares the Designated Priority vector */
/*                      and the Port Priority vector and returns if the      */
/*                      Designated Priority vector is BETTER THAN or INFERIOR*/
/*                      TO or SAME AS the Port Priority vector.              */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port Number                          */
/*                      u2InstanceId - Instance Id of the Spanning Tree      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
MstIsDesgPrBetterThanPortPr (UINT2 u2PortNum, UINT2 u2InstanceId)
{
    tAstPerStBridgeInfo *pPerStBrgInfo = NULL;
    tAstPerStPortInfo  *pPerStPortInfo = NULL;
    tAstBridgeEntry    *pBrgInfo = NULL;
    tAstMstBridgeEntry *pMstBridgeEntry = NULL;
    tAstBridgeId        MyBrgId;
    UINT2               u2Val = (UINT2) AST_INIT_VAL;
    INT4                i4RetVal = (INT4) AST_INIT_VAL;
    tAstCommPortInfo   *pCommPortInfo = NULL;

    pCommPortInfo = AST_GET_COMM_PORT_INFO (u2PortNum);
    AST_MEMSET (&MyBrgId, AST_INIT_VAL, sizeof (MyBrgId));

    pBrgInfo = AST_GET_BRGENTRY ();
    pPerStBrgInfo = AST_GET_PERST_BRG_INFO (u2InstanceId);
    pPerStPortInfo = AST_GET_PERST_PORT_INFO (u2PortNum, u2InstanceId);
    pMstBridgeEntry = AST_GET_MST_BRGENTRY ();

    MyBrgId.u2BrgPriority = pPerStBrgInfo->u2BrgPriority;
    AST_MEMCPY (&(MyBrgId.BridgeAddr),
                &(pBrgInfo->BridgeAddr), AST_MAC_ADDR_SIZE);

    if (u2InstanceId == MST_CIST_CONTEXT)
    {
        i4RetVal = AST_COMPARE_BRGID (&(pPerStBrgInfo->RootId),
                                      &(pPerStPortInfo->RootId));
        switch (i4RetVal)
        {
            case AST_BRGID1_SUPERIOR:
                return RST_BETTER_MSG;

            case AST_BRGID1_INFERIOR:
                return RST_INFERIOR_MSG;

            default:
                break;
        }
        /* Same Root Id */
        if (pPerStBrgInfo->u4RootCost < pPerStPortInfo->u4RootCost)
        {
            return RST_BETTER_MSG;
        }
        else if (pPerStBrgInfo->u4RootCost > pPerStPortInfo->u4RootCost)
        {
            return RST_INFERIOR_MSG;
        }

        /* Same External Root Path Cost */
        if (pCommPortInfo->bSendRstp == RST_FALSE)
        {
            /* If a port is connected to a legacy STP bridge, the port's 
             * DesignatedPriorityVector will have it's own BridgeId in the 
             * RegionalRootId field - Sec 13.10 of 802.1s */
            i4RetVal = AST_COMPARE_BRGID (&(MyBrgId),
                                          &(pPerStPortInfo->RegionalRootId));
        }
        else
        {
            i4RetVal = AST_COMPARE_BRGID (&(pMstBridgeEntry->RegionalRootId),
                                          &(pPerStPortInfo->RegionalRootId));
        }

        switch (i4RetVal)
        {
            case AST_BRGID1_SUPERIOR:
                return RST_BETTER_MSG;

            case AST_BRGID1_INFERIOR:
                return RST_INFERIOR_MSG;

            default:
                break;
        }

        /* Same Regional Root Id */
        if (pPerStBrgInfo->u4CistInternalRootCost <
            pPerStPortInfo->u4IntRootPathCost)
        {
            return RST_BETTER_MSG;
        }
        else if (pPerStBrgInfo->u4CistInternalRootCost >
                 pPerStPortInfo->u4IntRootPathCost)
        {
            return RST_INFERIOR_MSG;
        }

        /* Same Internal Root Path Cost */
        i4RetVal = AST_COMPARE_BRGID (&(MyBrgId), &(pPerStPortInfo->DesgBrgId));
        switch (i4RetVal)
        {
            case AST_BRGID1_SUPERIOR:
                return RST_BETTER_MSG;

            case AST_BRGID1_INFERIOR:
                return RST_INFERIOR_MSG;

            default:
                /* Same DesgBrgId */
                break;
        }
        u2Val = (UINT2) pPerStPortInfo->u1PortPriority;
        u2Val = (UINT2) (u2Val << AST_PORTPRIORITY_BIT_OFFSET);
        u2Val = u2Val | AST_GET_LOCAL_PORT (u2PortNum);

        if (u2Val < pPerStPortInfo->u2DesgPortId)
        {
            return RST_BETTER_MSG;
        }
        else if (u2Val > pPerStPortInfo->u2DesgPortId)
        {
            return RST_INFERIOR_MSG;
        }
        return RST_SAME_MSG;
    }
    else                        /* MSTI Context */
    {
        i4RetVal = AST_COMPARE_BRGID (&(pPerStBrgInfo->RootId),
                                      &(pPerStPortInfo->RootId));
        switch (i4RetVal)
        {
            case AST_BRGID1_SUPERIOR:
                return RST_BETTER_MSG;

            case AST_BRGID1_INFERIOR:
                return RST_INFERIOR_MSG;

            default:
                break;
        }
        /* Same MSTI Root Id */
        if (pPerStBrgInfo->u4RootCost < pPerStPortInfo->u4RootCost)
        {
            return RST_BETTER_MSG;
        }
        else if (pPerStBrgInfo->u4RootCost > pPerStPortInfo->u4RootCost)
        {
            return RST_INFERIOR_MSG;
        }

        /* Same MSTI Internal Path Cost */
        i4RetVal = AST_COMPARE_BRGID (&(MyBrgId), &(pPerStPortInfo->DesgBrgId));
        switch (i4RetVal)
        {
            case AST_BRGID1_SUPERIOR:
                return RST_BETTER_MSG;

            case AST_BRGID1_INFERIOR:
                return RST_INFERIOR_MSG;

            default:
                /* Same DesgBrgId */
                break;
        }
        u2Val = (UINT2) pPerStPortInfo->u1PortPriority;
        u2Val = (UINT2) (u2Val << AST_PORTPRIORITY_BIT_OFFSET);
        u2Val = u2Val | AST_GET_LOCAL_PORT (u2PortNum);

        if (u2Val < pPerStPortInfo->u2DesgPortId)
        {
            return RST_BETTER_MSG;
        }
        else if (u2Val > pPerStPortInfo->u2DesgPortId)
        {
            return RST_INFERIOR_MSG;
        }
        return RST_SAME_MSG;
    }
}

/*****************************************************************************/
/* Function Name      : MstInitPortRoleSelectionMachine                      */
/*                                                                           */
/* Description        : Initialises the Port Role Selection State Machine    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
MstInitPortRoleSelectionMachine (VOID)
{
    RstInitPortRoleSelectionMachine ();
}

#endif /* MSTP_WANTED */
