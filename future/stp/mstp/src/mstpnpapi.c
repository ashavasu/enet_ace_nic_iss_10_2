/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved]
 *
 * $Id: mstpnpapi.c,v 1.3 2013/11/14 11:34:37 siva Exp $
 *
 * Description:This file contains the wrapper for
 *              for Hardware API's w.r.t MSTP
 *              <Part of dual node stacking model>
 *
  *******************************************************************/

#ifndef __MSTP_NP_API_C
#define __MSTP_NP_API_C

#include "astminc.h"
#include "mstpnpwr.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : MstpFsMiMstpNpCreateInstance                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiMstpNpCreateInstance
 *                                                                          
 *    Input(s)            : Arguments of FsMiMstpNpCreateInstance
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MstpFsMiMstpNpCreateInstance (UINT4 u4ContextId, UINT2 u2InstId)
{
    tFsHwNp             FsHwNp;
    tMstpNpModInfo     *pMstpNpModInfo = NULL;
    tMstpNpWrFsMiMstpNpCreateInstance *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MSTP_MOD,    /* Module ID */
                         FS_MI_MSTP_NP_CREATE_INSTANCE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMstpNpModInfo = &(FsHwNp.MstpNpModInfo);
    pEntry = &pMstpNpModInfo->MstpNpFsMiMstpNpCreateInstance;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u2InstId = u2InstId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MstpFsMiMstpNpAddVlanInstMapping                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiMstpNpAddVlanInstMapping
 *                                                                          
 *    Input(s)            : Arguments of FsMiMstpNpAddVlanInstMapping
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MstpFsMiMstpNpAddVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId,
                                  UINT2 u2InstId)
{
    tFsHwNp             FsHwNp;
    tMstpNpModInfo     *pMstpNpModInfo = NULL;
    tMstpNpWrFsMiMstpNpAddVlanInstMapping *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MSTP_MOD,    /* Module ID */
                         FS_MI_MSTP_NP_ADD_VLAN_INST_MAPPING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMstpNpModInfo = &(FsHwNp.MstpNpModInfo);
    pEntry = &pMstpNpModInfo->MstpNpFsMiMstpNpAddVlanInstMapping;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->u2InstId = u2InstId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MstpFsMiMstpNpAddVlanListInstMapping                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiMstpNpAddVlanListInstMapping
 *                                                                          
 *    Input(s)            : Arguments of FsMiMstpNpAddVlanListInstMapping
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MstpFsMiMstpNpAddVlanListInstMapping (UINT4 u4ContextId, UINT1 *pu1VlanList,
                                      UINT2 u2InstId, UINT2 u2NumVlans,
                                      UINT2 *pu2LastVlan)
{
    tFsHwNp             FsHwNp;
    tMstpNpModInfo     *pMstpNpModInfo = NULL;
    tMstpNpWrFsMiMstpNpAddVlanListInstMapping *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MSTP_MOD,    /* Module ID */
                         FS_MI_MSTP_NP_ADD_VLAN_LIST_INST_MAPPING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMstpNpModInfo = &(FsHwNp.MstpNpModInfo);
    pEntry = &pMstpNpModInfo->MstpNpFsMiMstpNpAddVlanListInstMapping;

    pEntry->u4ContextId = u4ContextId;
    pEntry->pu1VlanList = pu1VlanList;
    pEntry->u2InstId = u2InstId;
    pEntry->u2NumVlans = u2NumVlans;
    pEntry->pu2LastVlan = pu2LastVlan;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MstpFsMiMstpNpDeleteInstance                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiMstpNpDeleteInstance
 *                                                                          
 *    Input(s)            : Arguments of FsMiMstpNpDeleteInstance
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MstpFsMiMstpNpDeleteInstance (UINT4 u4ContextId, UINT2 u2InstId)
{
    tFsHwNp             FsHwNp;
    tMstpNpModInfo     *pMstpNpModInfo = NULL;
    tMstpNpWrFsMiMstpNpDeleteInstance *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MSTP_MOD,    /* Module ID */
                         FS_MI_MSTP_NP_DELETE_INSTANCE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMstpNpModInfo = &(FsHwNp.MstpNpModInfo);
    pEntry = &pMstpNpModInfo->MstpNpFsMiMstpNpDeleteInstance;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u2InstId = u2InstId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MstpFsMiMstpNpDelVlanInstMapping                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiMstpNpDelVlanInstMapping
 *                                                                          
 *    Input(s)            : Arguments of FsMiMstpNpDelVlanInstMapping
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MstpFsMiMstpNpDelVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId,
                                  UINT2 u2InstId)
{
    tFsHwNp             FsHwNp;
    tMstpNpModInfo     *pMstpNpModInfo = NULL;
    tMstpNpWrFsMiMstpNpDelVlanInstMapping *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MSTP_MOD,    /* Module ID */
                         FS_MI_MSTP_NP_DEL_VLAN_INST_MAPPING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMstpNpModInfo = &(FsHwNp.MstpNpModInfo);
    pEntry = &pMstpNpModInfo->MstpNpFsMiMstpNpDelVlanInstMapping;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->u2InstId = u2InstId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MstpFsMiMstpNpDelVlanListInstMapping                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiMstpNpDelVlanListInstMapping
 *                                                                          
 *    Input(s)            : Arguments of FsMiMstpNpDelVlanListInstMapping
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MstpFsMiMstpNpDelVlanListInstMapping (UINT4 u4ContextId, UINT1 *pu1VlanList,
                                      UINT2 u2InstId, UINT2 u2NumVlans,
                                      UINT2 *pu2LastVlan)
{
    tFsHwNp             FsHwNp;
    tMstpNpModInfo     *pMstpNpModInfo = NULL;
    tMstpNpWrFsMiMstpNpDelVlanListInstMapping *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MSTP_MOD,    /* Module ID */
                         FS_MI_MSTP_NP_DEL_VLAN_LIST_INST_MAPPING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMstpNpModInfo = &(FsHwNp.MstpNpModInfo);
    pEntry = &pMstpNpModInfo->MstpNpFsMiMstpNpDelVlanListInstMapping;

    pEntry->u4ContextId = u4ContextId;
    pEntry->pu1VlanList = pu1VlanList;
    pEntry->u2InstId = u2InstId;
    pEntry->u2NumVlans = u2NumVlans;
    pEntry->pu2LastVlan = pu2LastVlan;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MstpFsMiMstpNpSetInstancePortState                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiMstpNpSetInstancePortState
 *                                                                          
 *    Input(s)            : Arguments of FsMiMstpNpSetInstancePortState
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MstpFsMiMstpNpSetInstancePortState (UINT4 u4ContextId, UINT4 u4IfIndex,
                                    UINT2 u2InstanceId, UINT1 u1PortState)
{
    tFsHwNp             FsHwNp;
    tMstpNpModInfo     *pMstpNpModInfo = NULL;
    tMstpNpWrFsMiMstpNpSetInstancePortState *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MSTP_MOD,    /* Module ID */
                         FS_MI_MSTP_NP_SET_INSTANCE_PORT_STATE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMstpNpModInfo = &(FsHwNp.MstpNpModInfo);
    pEntry = &pMstpNpModInfo->MstpNpFsMiMstpNpSetInstancePortState;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u2InstanceId = u2InstanceId;
    pEntry->u1PortState = u1PortState;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MstpFsMiMstpNpInitHw                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiMstpNpInitHw
 *                                                                          
 *    Input(s)            : Arguments of FsMiMstpNpInitHw
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MstpFsMiMstpNpInitHw (UINT4 u4ContextId)
{
    tFsHwNp             FsHwNp;
    tMstpNpModInfo     *pMstpNpModInfo = NULL;
    tMstpNpWrFsMiMstpNpInitHw *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MSTP_MOD,    /* Module ID */
                         FS_MI_MSTP_NP_INIT_HW,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMstpNpModInfo = &(FsHwNp.MstpNpModInfo);
    pEntry = &pMstpNpModInfo->MstpNpFsMiMstpNpInitHw;

    pEntry->u4ContextId = u4ContextId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MstpFsMiMstpNpDeInitHw                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiMstpNpDeInitHw
 *                                                                          
 *    Input(s)            : Arguments of FsMiMstpNpDeInitHw
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MstpFsMiMstpNpDeInitHw (UINT4 u4ContextId)
{
    tFsHwNp             FsHwNp;
    tMstpNpModInfo     *pMstpNpModInfo = NULL;
    tMstpNpWrFsMiMstpNpDeInitHw *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MSTP_MOD,    /* Module ID */
                         FS_MI_MSTP_NP_DE_INIT_HW,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMstpNpModInfo = &(FsHwNp.MstpNpModInfo);
    pEntry = &pMstpNpModInfo->MstpNpFsMiMstpNpDeInitHw;

    pEntry->u4ContextId = u4ContextId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MstpFsMiMstNpGetPortState                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiMstNpGetPortState
 *                                                                          
 *    Input(s)            : Arguments of FsMiMstNpGetPortState
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MstpFsMiMstNpGetPortState (UINT4 u4ContextId, UINT2 u2InstanceId,
                           UINT4 u4IfIndex, UINT1 *pu1Status)
{
    tFsHwNp             FsHwNp;
    tMstpNpModInfo     *pMstpNpModInfo = NULL;
    tMstpNpWrFsMiMstNpGetPortState *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MSTP_MOD,    /* Module ID */
                         FS_MI_MST_NP_GET_PORT_STATE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMstpNpModInfo = &(FsHwNp.MstpNpModInfo);
    pEntry = &pMstpNpModInfo->MstpNpFsMiMstNpGetPortState;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u2InstanceId = u2InstanceId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pu1Status = pu1Status;

    return (NpUtilHwProgram (&FsHwNp));
}

#ifdef  MBSM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : MstpFsMiMstpMbsmNpCreateInstance                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiMstpMbsmNpCreateInstance
 *                                                                          
 *    Input(s)            : Arguments of FsMiMstpMbsmNpCreateInstance
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MstpFsMiMstpMbsmNpCreateInstance (UINT4 u4ContextId, UINT2 u2InstId,
                                  tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMstpNpModInfo     *pMstpNpModInfo = NULL;
    tMstpNpWrFsMiMstpMbsmNpCreateInstance *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MSTP_MOD,    /* Module ID */
                         FS_MI_MSTP_MBSM_NP_CREATE_INSTANCE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMstpNpModInfo = &(FsHwNp.MstpNpModInfo);
    pEntry = &pMstpNpModInfo->MstpNpFsMiMstpMbsmNpCreateInstance;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u2InstId = u2InstId;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MstpFsMiMstpMbsmNpAddVlanInstMapping                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiMstpMbsmNpAddVlanInstMapping
 *                                                                          
 *    Input(s)            : Arguments of FsMiMstpMbsmNpAddVlanInstMapping
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MstpFsMiMstpMbsmNpAddVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId,
                                      UINT2 u2InstId, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMstpNpModInfo     *pMstpNpModInfo = NULL;
    tMstpNpWrFsMiMstpMbsmNpAddVlanInstMapping *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MSTP_MOD,    /* Module ID */
                         FS_MI_MSTP_MBSM_NP_ADD_VLAN_INST_MAPPING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMstpNpModInfo = &(FsHwNp.MstpNpModInfo);
    pEntry = &pMstpNpModInfo->MstpNpFsMiMstpMbsmNpAddVlanInstMapping;

    pEntry->u4ContextId = u4ContextId;
    pEntry->VlanId = VlanId;
    pEntry->u2InstId = u2InstId;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MstpFsMiMstpMbsmNpSetInstancePortState                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiMstpMbsmNpSetInstancePortState
 *                                                                          
 *    Input(s)            : Arguments of FsMiMstpMbsmNpSetInstancePortState
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MstpFsMiMstpMbsmNpSetInstancePortState (UINT4 u4ContextId, UINT4 u4IfIndex,
                                        UINT2 u2InstanceId, UINT1 u1PortState,
                                        tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMstpNpModInfo     *pMstpNpModInfo = NULL;
    tMstpNpWrFsMiMstpMbsmNpSetInstancePortState *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MSTP_MOD,    /* Module ID */
                         FS_MI_MSTP_MBSM_NP_SET_INSTANCE_PORT_STATE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMstpNpModInfo = &(FsHwNp.MstpNpModInfo);
    pEntry = &pMstpNpModInfo->MstpNpFsMiMstpMbsmNpSetInstancePortState;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u2InstanceId = u2InstanceId;
    pEntry->u1PortState = u1PortState;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MstpFsMiMstpMbsmNpInitHw                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiMstpMbsmNpInitHw
 *                                                                          
 *    Input(s)            : Arguments of FsMiMstpMbsmNpInitHw
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MstpFsMiMstpMbsmNpInitHw (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMstpNpModInfo     *pMstpNpModInfo = NULL;
    tMstpNpWrFsMiMstpMbsmNpInitHw *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MSTP_MOD,    /* Module ID */
                         FS_MI_MSTP_MBSM_NP_INIT_HW,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMstpNpModInfo = &(FsHwNp.MstpNpModInfo);
    pEntry = &pMstpNpModInfo->MstpNpFsMiMstpMbsmNpInitHw;

    pEntry->u4ContextId = u4ContextId;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* MBSM_WANTED */

#endif /* __MSTP_NP_API_C */
