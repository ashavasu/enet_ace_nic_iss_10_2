#####################################################################
#### Copyright (C) 2006 Aricent Inc . All Rights Reserved                    ####
# $Id: make.h,v 1.8 2014/05/28 12:15:59 siva Exp $
#####################################################################
##|                                                               |##
##|###############################################################|##
##|                                                               |##
##|    FILE NAME               ::  make.h                         |##
##|                                                               |##
##|    PRINCIPAL    AUTHOR     ::  Aricent Inc.      |##
##|                                                               |##
##|    SUBSYSTEM NAME          ::  MST                            |##
##|                                                               |##
##|    MODULE NAME             ::  MST                            |##
##|                                                               |##
##|    TARGET ENVIRONMENT      ::  ANY                            |##
##|                                                               |##
##|    DATE OF FIRST RELEASE   ::  26 Mar 2002                    |##
##|                                                               |##
##|    DESCRIPTION             ::  Include file for the MST       |##
##|                                Makefile.                      |## 
##|                                                               |##
##|###############################################################|##

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

MST_SWITCHES = -Di386 -DRSTP_DEBUG -DMSTP_TRAP_WANTED -UIEEE_8021Y_Z12 


TOTAL_OPNS = ${MST_SWITCHES} $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

MST_BASE_DIR    = ${BASE_DIR}/stp/mstp
PVRST_BASE_DIR  = ${BASE_DIR}/stp/pvrst
RST_BASE_DIR    = ${BASE_DIR}/stp/rstp
MST_SRC_DIR     = ${MST_BASE_DIR}/src
MST_INC_DIR     = ${MST_BASE_DIR}/inc
MST_OBJ_DIR     = ${MST_BASE_DIR}/obj
PVRST_INC_DIR   = ${PVRST_BASE_DIR}/inc
RST_INC_DIR     = ${RST_BASE_DIR}/inc
CFA_INCD        = ${CFA_BASE_DIR}/inc

BRIDGE_INCL_DIR = ${BASE_DIR}/bridge/inc

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${MST_INC_DIR} -I${BRIDGE_INCL_DIR} -I${PVRST_INC_DIR} -I${RST_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
