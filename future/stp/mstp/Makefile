#####################################################################
#### Copyright (C) 2006 Aricent Inc . All Rights Reserved                    ####
#### Copyright (C) 2006 Aricent Inc . All Rights Reserved          ####
#####################################################################
##|                                                               |##
##|###############################################################|##
##|                                                               |##
##|    FILE NAME               ::  Makefile                       |##
##|                                                               |##
##|    PRINCIPAL    AUTHOR     ::  Aricent Inc.      |##
##|                                                               |##
##|    SUBSYSTEM NAME          ::  MST                            |##
##|                                                               |##
##|    MODULE NAME             ::  MST                            |##
##|                                                               |##
##|    TARGET ENVIRONMENT      ::  ANY                            |##
##|                                                               |##
##|    DATE OF FIRST RELEASE   ::  26 Mar 2002                    |##
##|                                                               |##
##|    DESCRIPTION             ::  MAKEFILE for FutureMSTP        |##
##|                                Final Object                   |## 
##|                                                               |##
##|###############################################################|##

.PHONY  : clean
include ../../LR/make.h
include ../../LR/make.rule
include ./make.h

ISS_C_FLAGS             = ${CC_FLAGS} ${TOTAL_OPNS} ${INCLUDES}
MST_FINAL_OBJ     = ${MST_OBJ_DIR}/FutureMSTP.o

#object module list

MST_OBJS = \
         ${MST_OBJ_DIR}/astmsnmp.o \
         ${MST_OBJ_DIR}/astmprsm.o \
         ${MST_OBJ_DIR}/astmplow.o \
         ${MST_OBJ_DIR}/astmimplw.o \
         ${MST_OBJ_DIR}/astmtcsm.o \
         ${MST_OBJ_DIR}/astmpism.o \
         ${MST_OBJ_DIR}/astmrssm.o \
         ${MST_OBJ_DIR}/astmrtsm.o \
         ${MST_OBJ_DIR}/astmsys.o  \
         ${MST_OBJ_DIR}/astmtxsm.o  \
         ${MST_OBJ_DIR}/astmutil.o  \
         ${MST_OBJ_DIR}/astmtrap.o \
         ${MST_OBJ_DIR}/mstpapi.o \
         ${MST_OBJ_DIR}/astmsisp.o 

ifeq ($(L2RED), YES)
MST_OBJS += \
${MST_OBJ_DIR}/astmred.o
else
MST_OBJS += \
       ${MST_OBJ_DIR}/astmrdstb.o
endif

ifeq (${SNMP_2}, YES)
MST_OBJS += \
${MST_OBJ_DIR}/std1s1lw.o \
${MST_OBJ_DIR}/std1s1wr.o\
${MST_OBJ_DIR}/fsmstwr.o \
${MST_OBJ_DIR}/fsmpmswr.o
endif

ifeq (${CLI}, YES)
MST_OBJS += \
       ${MST_OBJ_DIR}/mstpcli.o
endif

ifeq (DNP_BACKWD_COMPATIBILITY, $(findstring DNP_BACKWD_COMPATIBILITY, $(SYSTEM_COMPILATION_SWITCHES)))
ifeq ($(NPAPI), YES)
ifneq (${TARGET_ASIC},NPSIM)
MST_OBJS += ${MST_OBJ_DIR}/msminpwr.o
endif
endif
endif

ifeq ($(NPAPI), YES)
MST_OBJS += \
       ${MST_OBJ_DIR}/astmhwwr.o
MST_OBJS += \
       ${MST_OBJ_DIR}/mstpnpwr.o \
       ${MST_OBJ_DIR}/mstpnpapi.o
endif
ifeq ($(NETCONF), YES)
MST_OBJS += \
  ${MST_OBJ_DIR}/fsmstnc.o
endif

#object module dependency
${MST_FINAL_OBJ} : obj ${MST_OBJS}
	${LD} ${LD_FLAGS} ${CC_COMMON_OPTIONS} -o ${MST_FINAL_OBJ} ${MST_OBJS} 

#compilation file dependecies 
EXTERNAL_DEPENDENCIES =  \
	$(COMN_INCL_DIR)/lr.h \
	$(COMN_INCL_DIR)/cfa.h \
        $(COMN_INCL_DIR)/bridge.h \
        $(COMN_INCL_DIR)/mstp.h \
        $(COMN_INCL_DIR)/mstpnpwr.h \
        $(COMN_INCL_DIR)/rstp.h \
	$(SNMP_INCL_DIR)/snmccons.h \
	$(SNMP_INCL_DIR)/snmctdfs.h

ifeq (${MBSM}, YES)
EXTERNAL_DEPENDENCIES +=${COMN_INCL_DIR}/mbsm.h
endif

ifeq (${NPAPI}, YES)
EXTERNAL_DEPENDENCIES +=$(COMN_INCL_DIR)/nputil.h 
endif


INTERNAL_DEPENDENCIES = \
        ${MST_BASE_DIR}/make.h \
        ${MST_BASE_DIR}/Makefile \
        ${MST_INC_DIR}/astminc.h \
        ${MST_INC_DIR}/astmcons.h \
        ${MST_INC_DIR}/astmmacr.h \
        ${MST_INC_DIR}/astmprot.h \
        ${MST_INC_DIR}/astmtdfs.h \
        ${MST_INC_DIR}/astmtrc.h \
        ${MST_INC_DIR}/astmlow.h \
        ${MST_INC_DIR}/fsmpmslw.h \
        ${MST_INC_DIR}/astmextn.h \
        ${MST_INC_DIR}/astmtrap.h \
        ${MST_INC_DIR}/astmhwwr.h \
        ${MST_INC_DIR}/std1s1lw.h \
        ${MST_INC_DIR}/std1s1wr.h \
        ${RST_INC_DIR}/astctdfs.h \
        ${RST_INC_DIR}/astconst.h \
        ${RST_INC_DIR}/astglob.h \
        ${RST_INC_DIR}/astmacr.h \
        ${RST_INC_DIR}/astrtrap.h \
        ${RST_INC_DIR}/asttdfs.h
 
DEPENDENCIES =  \
    ${EXTERNAL_DEPENDENCIES} \
	 ${INTERNAL_DEPENDENCIES} \
	 ${COMMON_DEPENDENCIES}
ifeq ($(L2RED), YES)
INTERNAL_DEPENDENCIES += \
        ${MST_INC_DIR}/astmred.h \
        ${RST_INC_DIR}/astpred.h
else
INTERNAL_DEPENDENCIES += \
        ${MST_INC_DIR}/astmrdstb.h
endif
ifeq ($(NETCONF), YES)
INTERNAL_DEPENDENCIES += \
        ${MST_INC_DIR}/fsmstnc.h
endif

obj :
	$(MKDIR) $(MKDIR_FLAGS) $(MST_OBJ_DIR)

$(MST_OBJ_DIR)/astmsnmp.o : \
   $(MST_SRC_DIR)/astmsnmp.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(MST_SRC_DIR)/astmsnmp.c -o $(MST_OBJ_DIR)/astmsnmp.o

$(MST_OBJ_DIR)/astmprsm.o : \
   $(MST_SRC_DIR)/astmprsm.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(MST_SRC_DIR)/astmprsm.c -o $(MST_OBJ_DIR)/astmprsm.o

$(MST_OBJ_DIR)/astmtcsm.o : \
   $(MST_SRC_DIR)/astmtcsm.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(MST_SRC_DIR)/astmtcsm.c -o $(MST_OBJ_DIR)/astmtcsm.o

$(MST_OBJ_DIR)/astmplow.o : \
   $(MST_SRC_DIR)/astmplow.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(MST_SRC_DIR)/astmplow.c -o $(MST_OBJ_DIR)/astmplow.o

$(MST_OBJ_DIR)/astmimplw.o : \
   $(MST_SRC_DIR)/astmimplw.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(MST_SRC_DIR)/astmimplw.c -o $(MST_OBJ_DIR)/astmimplw.o

$(MST_OBJ_DIR)/fsmstwr.o : \
   $(MST_SRC_DIR)/fsmstwr.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(MST_SRC_DIR)/fsmstwr.c -o $(MST_OBJ_DIR)/fsmstwr.o

$(MST_OBJ_DIR)/fsmpmswr.o : \
   $(MST_SRC_DIR)/fsmpmswr.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(MST_SRC_DIR)/fsmpmswr.c -o $(MST_OBJ_DIR)/fsmpmswr.o

$(MST_OBJ_DIR)/astmpism.o : \
   $(MST_SRC_DIR)/astmpism.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(MST_SRC_DIR)/astmpism.c -o $(MST_OBJ_DIR)/astmpism.o

$(MST_OBJ_DIR)/astmrssm.o : \
   $(MST_SRC_DIR)/astmrssm.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(MST_SRC_DIR)/astmrssm.c -o $(MST_OBJ_DIR)/astmrssm.o

$(MST_OBJ_DIR)/astmrtsm.o : \
   $(MST_SRC_DIR)/astmrtsm.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(MST_SRC_DIR)/astmrtsm.c -o $(MST_OBJ_DIR)/astmrtsm.o

$(MST_OBJ_DIR)/astmsys.o : \
   $(MST_SRC_DIR)/astmsys.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(MST_SRC_DIR)/astmsys.c -o $(MST_OBJ_DIR)/astmsys.o

$(MST_OBJ_DIR)/astmtxsm.o : \
   $(MST_SRC_DIR)/astmtxsm.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(MST_SRC_DIR)/astmtxsm.c -o $(MST_OBJ_DIR)/astmtxsm.o

$(MST_OBJ_DIR)/astmtrap.o : \
   $(MST_SRC_DIR)/astmtrap.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(MST_SRC_DIR)/astmtrap.c -o $(MST_OBJ_DIR)/astmtrap.o

$(MST_OBJ_DIR)/astmhwwr.o : \
   $(MST_SRC_DIR)/astmhwwr.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(MST_SRC_DIR)/astmhwwr.c -o $(MST_OBJ_DIR)/astmhwwr.o

$(MST_OBJ_DIR)/mstpnpwr.o : \
   $(MST_SRC_DIR)/mstpnpwr.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(MST_SRC_DIR)/mstpnpwr.c -o $(MST_OBJ_DIR)/mstpnpwr.o

$(MST_OBJ_DIR)/mstpnpapi.o : \
   $(MST_SRC_DIR)/mstpnpapi.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(MST_SRC_DIR)/mstpnpapi.c -o $(MST_OBJ_DIR)/mstpnpapi.o

$(MST_OBJ_DIR)/astmred.o : \
   $(MST_SRC_DIR)/astmred.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(MST_SRC_DIR)/astmred.c -o $(MST_OBJ_DIR)/astmred.o

$(MST_OBJ_DIR)/astmrdstb.o : \
   $(MST_SRC_DIR)/astmrdstb.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(MST_SRC_DIR)/astmrdstb.c -o $(MST_OBJ_DIR)/astmrdstb.o


$(MST_OBJ_DIR)/astmutil.o : \
   $(MST_SRC_DIR)/astmutil.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(MST_SRC_DIR)/astmutil.c -o $(MST_OBJ_DIR)/astmutil.o

$(MST_OBJ_DIR)/mstpcli.o : \
   $(MST_SRC_DIR)/mstpcli.c  \
   $(SNMP_INCL_DIR)/snmcdefn.h  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(MST_SRC_DIR)/mstpcli.c -o $(MST_OBJ_DIR)/mstpcli.o
   
$(MST_OBJ_DIR)/mstpapi.o : \
   $(MST_SRC_DIR)/mstpapi.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS) $(MST_SRC_DIR)/mstpapi.c -o $(MST_OBJ_DIR)/mstpapi.o

$(MST_OBJ_DIR)/msminpwr.o : \
   $(MST_SRC_DIR)/msminpwr.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(MST_SRC_DIR)/msminpwr.c -o $(MST_OBJ_DIR)/msminpwr.o

$(MST_OBJ_DIR)/minpmstb.o : \
   $(MST_SRC_DIR)/minpmstb.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS)  $(MST_SRC_DIR)/minpmstb.c -o $(MST_OBJ_DIR)/minpmstb.o

$(MST_OBJ_DIR)/astmsisp.o : \
   $(MST_SRC_DIR)/astmsisp.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS) $(MST_SRC_DIR)/astmsisp.c -o $(MST_OBJ_DIR)/astmsisp.o

$(MST_OBJ_DIR)/std1s1lw.o : \
   $(MST_SRC_DIR)/std1s1lw.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS) $(MST_SRC_DIR)/std1s1lw.c -o $(MST_OBJ_DIR)/std1s1lw.o

$(MST_OBJ_DIR)/std1s1wr.o : \
   $(MST_SRC_DIR)/std1s1wr.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS) $(MST_SRC_DIR)/std1s1wr.c -o $(MST_OBJ_DIR)/std1s1wr.o
ifeq ($(NETCONF), YES)
$(MST_OBJ_DIR)/fsmstnc.o : \
  $(MST_SRC_DIR)/fsmstnc.c  \
	${DEPENDENCIES}
	$(CC) $(ISS_C_FLAGS) $(MST_SRC_DIR)/fsmstnc.c -o $(MST_OBJ_DIR)/fsmstnc.o
endif

clean:
	$(RM) $(RM_FLAGS) $(MST_OBJ_DIR)/*.o

pkg:
	CUR_PWD=${PWD};\
        cd ${BASE_DIR}/../..;\
        tar cvzf ${ISS_PKG_CREATE_DIR}/mstp.tgz -T ${BASE_DIR}/stp/mstp/FILES.NEW;\
        cd ${CUR_PWD};

