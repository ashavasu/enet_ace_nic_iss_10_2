/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1s1lw.h,v 1.1 2011/11/03 09:14:06 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for Ieee8021MstpCistTable. */
INT1
nmhValidateIndexInstanceIeee8021MstpCistTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021MstpCistTable  */

INT1
nmhGetFirstIndexIeee8021MstpCistTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021MstpCistTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021MstpCistBridgeIdentifier ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021MstpCistTopologyChange ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpCistRegionalRootIdentifier ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021MstpCistPathCost ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetIeee8021MstpCistMaxHops ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021MstpCistMaxHops ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021MstpCistMaxHops ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021MstpCistTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021MstpTable. */
INT1
nmhValidateIndexInstanceIeee8021MstpTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021MstpTable  */

INT1
nmhGetFirstIndexIeee8021MstpTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021MstpTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021MstpBridgeId ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021MstpTimeSinceTopologyChange ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021MstpTopologyChanges ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIeee8021MstpTopologyChange ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpDesignatedRoot ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021MstpRootPathCost ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpRootPort ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021MstpBridgePriority ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpVids0 ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021MstpVids1 ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021MstpVids2 ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021MstpVids3 ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021MstpRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021MstpBridgePriority ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021MstpRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021MstpBridgePriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021MstpRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021MstpTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021MstpCistPortTable. */
INT1
nmhValidateIndexInstanceIeee8021MstpCistPortTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021MstpCistPortTable  */

INT1
nmhGetFirstIndexIeee8021MstpCistPortTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021MstpCistPortTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021MstpCistPortUptime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021MstpCistPortAdminPathCost ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpCistPortDesignatedRoot ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021MstpCistPortTopologyChangeAck ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpCistPortHelloTime ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpCistPortAdminEdgePort ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpCistPortOperEdgePort ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpCistPortMacEnabled ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpCistPortMacOperational ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpCistPortRestrictedRole ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpCistPortRestrictedTcn ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpCistPortRole ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpCistPortDisputed ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpCistPortCistRegionalRootId ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021MstpCistPortCistPathCost ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021MstpCistPortProtocolMigration ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpCistPortEnableBPDURx ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpCistPortEnableBPDUTx ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpCistPortPseudoRootId ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021MstpCistPortIsL2Gp ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021MstpCistPortAdminPathCost ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021MstpCistPortAdminEdgePort ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021MstpCistPortMacEnabled ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021MstpCistPortRestrictedRole ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021MstpCistPortRestrictedTcn ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021MstpCistPortProtocolMigration ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021MstpCistPortEnableBPDURx ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021MstpCistPortEnableBPDUTx ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021MstpCistPortPseudoRootId ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021MstpCistPortIsL2Gp ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021MstpCistPortAdminPathCost ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021MstpCistPortAdminEdgePort ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021MstpCistPortMacEnabled ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021MstpCistPortRestrictedRole ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021MstpCistPortRestrictedTcn ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021MstpCistPortProtocolMigration ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021MstpCistPortEnableBPDURx ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021MstpCistPortEnableBPDUTx ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021MstpCistPortPseudoRootId ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Ieee8021MstpCistPortIsL2Gp ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021MstpCistPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021MstpPortTable. */
INT1
nmhValidateIndexInstanceIeee8021MstpPortTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021MstpPortTable  */

INT1
nmhGetFirstIndexIeee8021MstpPortTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021MstpPortTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021MstpPortUptime ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021MstpPortState ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpPortPriority ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpPortPathCost ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpPortDesignatedRoot ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021MstpPortDesignatedCost ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpPortDesignatedBridge ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021MstpPortDesignatedPort ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021MstpPortRole ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpPortDisputed ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021MstpPortPriority ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021MstpPortPathCost ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021MstpPortPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021MstpPortPathCost ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021MstpPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021MstpFidToMstiTable. */
INT1
nmhValidateIndexInstanceIeee8021MstpFidToMstiTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021MstpFidToMstiTable  */

INT1
nmhGetFirstIndexIeee8021MstpFidToMstiTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021MstpFidToMstiTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021MstpFidToMstiMstId ARG_LIST((UINT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021MstpFidToMstiMstId ARG_LIST((UINT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021MstpFidToMstiMstId ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021MstpFidToMstiTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021MstpVlanTable. */
INT1
nmhValidateIndexInstanceIeee8021MstpVlanTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021MstpVlanTable  */

INT1
nmhGetFirstIndexIeee8021MstpVlanTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021MstpVlanTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021MstpVlanMstId ARG_LIST((UINT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for Ieee8021MstpConfigIdTable. */
INT1
nmhValidateIndexInstanceIeee8021MstpConfigIdTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021MstpConfigIdTable  */

INT1
nmhGetFirstIndexIeee8021MstpConfigIdTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021MstpConfigIdTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021MstpConfigIdFormatSelector ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021MstpConfigurationName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021MstpRevisionLevel ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetIeee8021MstpConfigurationDigest ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021MstpConfigIdFormatSelector ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetIeee8021MstpConfigurationName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021MstpRevisionLevel ARG_LIST((UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021MstpConfigIdFormatSelector ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021MstpConfigurationName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Ieee8021MstpRevisionLevel ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021MstpConfigIdTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
