/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpmsdb.h,v 1.28 2017/09/20 06:32:06 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMPMSDB_H
#define _FSMPMSDB_H

UINT1 FsMIDot1sFutureMstTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIMstMstiBridgeTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIMstVlanInstanceMappingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIMstCistPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIMstMstiPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIMstPortExtTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIDot1sFsMstTrapsControlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIMstPortTrapNotificationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIMstPortRoleTrapNotificationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fsmpms [] ={1,3,6,1,4,1,2076,118};
tSNMP_OID_TYPE fsmpmsOID = {8, fsmpms};


UINT4 FsMIMstGlobalTrace [ ] ={1,3,6,1,4,1,2076,118,1,1};
UINT4 FsMIMstGlobalDebug [ ] ={1,3,6,1,4,1,2076,118,1,2};
UINT4 FsMIDot1sFutureMstContextId [ ] ={1,3,6,1,4,1,2076,118,1,3,1,1};
UINT4 FsMIMstSystemControl [ ] ={1,3,6,1,4,1,2076,118,1,3,1,2};
UINT4 FsMIMstModuleStatus [ ] ={1,3,6,1,4,1,2076,118,1,3,1,3};
UINT4 FsMIMstMaxMstInstanceNumber [ ] ={1,3,6,1,4,1,2076,118,1,3,1,4};
UINT4 FsMIMstNoOfMstiSupported [ ] ={1,3,6,1,4,1,2076,118,1,3,1,5};
UINT4 FsMIMstMaxHopCount [ ] ={1,3,6,1,4,1,2076,118,1,3,1,6};
UINT4 FsMIMstBrgAddress [ ] ={1,3,6,1,4,1,2076,118,1,3,1,7};
UINT4 FsMIMstCistRoot [ ] ={1,3,6,1,4,1,2076,118,1,3,1,8};
UINT4 FsMIMstCistRegionalRoot [ ] ={1,3,6,1,4,1,2076,118,1,3,1,9};
UINT4 FsMIMstCistRootCost [ ] ={1,3,6,1,4,1,2076,118,1,3,1,10};
UINT4 FsMIMstCistRegionalRootCost [ ] ={1,3,6,1,4,1,2076,118,1,3,1,11};
UINT4 FsMIMstCistRootPort [ ] ={1,3,6,1,4,1,2076,118,1,3,1,12};
UINT4 FsMIMstCistBridgePriority [ ] ={1,3,6,1,4,1,2076,118,1,3,1,13};
UINT4 FsMIMstCistBridgeMaxAge [ ] ={1,3,6,1,4,1,2076,118,1,3,1,14};
UINT4 FsMIMstCistBridgeForwardDelay [ ] ={1,3,6,1,4,1,2076,118,1,3,1,15};
UINT4 FsMIMstCistHoldTime [ ] ={1,3,6,1,4,1,2076,118,1,3,1,16};
UINT4 FsMIMstCistMaxAge [ ] ={1,3,6,1,4,1,2076,118,1,3,1,17};
UINT4 FsMIMstCistForwardDelay [ ] ={1,3,6,1,4,1,2076,118,1,3,1,18};
UINT4 FsMIMstMstpUpCount [ ] ={1,3,6,1,4,1,2076,118,1,3,1,19};
UINT4 FsMIMstMstpDownCount [ ] ={1,3,6,1,4,1,2076,118,1,3,1,20};
UINT4 FsMIMstTrace [ ] ={1,3,6,1,4,1,2076,118,1,3,1,22};
UINT4 FsMIMstDebug [ ] ={1,3,6,1,4,1,2076,118,1,3,1,23};
UINT4 FsMIMstForceProtocolVersion [ ] ={1,3,6,1,4,1,2076,118,1,3,1,24};
UINT4 FsMIMstTxHoldCount [ ] ={1,3,6,1,4,1,2076,118,1,3,1,25};
UINT4 FsMIMstMstiConfigIdSel [ ] ={1,3,6,1,4,1,2076,118,1,3,1,26};
UINT4 FsMIMstMstiRegionName [ ] ={1,3,6,1,4,1,2076,118,1,3,1,27};
UINT4 FsMIMstMstiRegionVersion [ ] ={1,3,6,1,4,1,2076,118,1,3,1,28};
UINT4 FsMIMstMstiConfigDigest [ ] ={1,3,6,1,4,1,2076,118,1,3,1,29};
UINT4 FsMIMstBufferOverFlowCount [ ] ={1,3,6,1,4,1,2076,118,1,3,1,30};
UINT4 FsMIMstMemAllocFailureCount [ ] ={1,3,6,1,4,1,2076,118,1,3,1,31};
UINT4 FsMIMstRegionConfigChangeCount [ ] ={1,3,6,1,4,1,2076,118,1,3,1,32};
UINT4 FsMIMstCistBridgeRoleSelectionSemState [ ] ={1,3,6,1,4,1,2076,118,1,3,1,33};
UINT4 FsMIMstCistTimeSinceTopologyChange [ ] ={1,3,6,1,4,1,2076,118,1,3,1,34};
UINT4 FsMIMstCistTopChanges [ ] ={1,3,6,1,4,1,2076,118,1,3,1,35};
UINT4 FsMIMstCistNewRootBridgeCount [ ] ={1,3,6,1,4,1,2076,118,1,3,1,36};
UINT4 FsMIMstCistHelloTime [ ] ={1,3,6,1,4,1,2076,118,1,3,1,37};
UINT4 FsMIMstCistBridgeHelloTime [ ] ={1,3,6,1,4,1,2076,118,1,3,1,38};
UINT4 FsMIMstCistDynamicPathcostCalculation [ ] ={1,3,6,1,4,1,2076,118,1,3,1,39};
UINT4 FsMIMstContextName [ ] ={1,3,6,1,4,1,2076,118,1,3,1,40};
UINT4 FsMIMstCalcPortPathCostOnSpeedChg [ ] ={1,3,6,1,4,1,2076,118,1,3,1,41};
UINT4 FsMIMstClearBridgeStats [ ] ={1,3,6,1,4,1,2076,118,1,3,1,42};
UINT4 FsMIMstRcvdEvent [ ] ={1,3,6,1,4,1,2076,118,1,3,1,43};
UINT4 FsMIMstRcvdEventSubType [ ] ={1,3,6,1,4,1,2076,118,1,3,1,44};
UINT4 FsMIMstRcvdEventTimeStamp [ ] ={1,3,6,1,4,1,2076,118,1,3,1,45};
UINT4 FsMIMstPortStateChangeTimeStamp [ ] ={1,3,6,1,4,1,2076,118,1,3,1,46};
UINT4 FsMIMstFlushInterval [ ] ={1,3,6,1,4,1,2076,118,1,3,1,47};
UINT4 FsMIMstCistFlushIndicationThreshold [ ] ={1,3,6,1,4,1,2076,118,1,3,1,48};
UINT4 FsMIMstCistTotalFlushCount [ ] ={1,3,6,1,4,1,2076,118,1,3,1,49};
UINT4 FsMIMstBpduGuard [ ] ={1,3,6,1,4,1,2076,118,1,3,1,50};
UINT4 FsMIMstStpPerfStatus [ ] ={1,3,6,1,4,1,2076,118,1,3,1,51};
UINT4 FsMIMstInstPortsMap [ ] ={1,3,6,1,4,1,2076,118,1,3,1,52};
UINT4 FsMIMstMstiInstanceIndex [ ] ={1,3,6,1,4,1,2076,118,1,4,1,1};
UINT4 FsMIMstMstiBridgeRegionalRoot [ ] ={1,3,6,1,4,1,2076,118,1,4,1,2};
UINT4 FsMIMstMstiBridgePriority [ ] ={1,3,6,1,4,1,2076,118,1,4,1,3};
UINT4 FsMIMstMstiRootCost [ ] ={1,3,6,1,4,1,2076,118,1,4,1,4};
UINT4 FsMIMstMstiRootPort [ ] ={1,3,6,1,4,1,2076,118,1,4,1,5};
UINT4 FsMIMstMstiTimeSinceTopologyChange [ ] ={1,3,6,1,4,1,2076,118,1,4,1,6};
UINT4 FsMIMstMstiTopChanges [ ] ={1,3,6,1,4,1,2076,118,1,4,1,7};
UINT4 FsMIMstMstiNewRootBridgeCount [ ] ={1,3,6,1,4,1,2076,118,1,4,1,8};
UINT4 FsMIMstMstiBridgeRoleSelectionSemState [ ] ={1,3,6,1,4,1,2076,118,1,4,1,9};
UINT4 FsMIMstInstanceUpCount [ ] ={1,3,6,1,4,1,2076,118,1,4,1,10};
UINT4 FsMIMstInstanceDownCount [ ] ={1,3,6,1,4,1,2076,118,1,4,1,11};
UINT4 FsMIMstOldDesignatedRoot [ ] ={1,3,6,1,4,1,2076,118,1,4,1,12};
UINT4 FsMIMstMstiClearBridgeStats [ ] ={1,3,6,1,4,1,2076,118,1,4,1,13};
UINT4 FsMIMstMstiFlushIndicationThreshold [ ] ={1,3,6,1,4,1,2076,118,1,4,1,14};
UINT4 FsMIMstMstiTotalFlushCount [ ] ={1,3,6,1,4,1,2076,118,1,4,1,15};
UINT4 FsMIMstiRootPriority [ ] ={1,3,6,1,4,1,2076,118,1,4,1,16};
UINT4 FsMIMstInstanceIndex [ ] ={1,3,6,1,4,1,2076,118,1,5,1,1};
UINT4 FsMIMstMapVlanIndex [ ] ={1,3,6,1,4,1,2076,118,1,5,1,2};
UINT4 FsMIMstUnMapVlanIndex [ ] ={1,3,6,1,4,1,2076,118,1,5,1,3};
UINT4 FsMIMstSetVlanList [ ] ={1,3,6,1,4,1,2076,118,1,5,1,4};
UINT4 FsMIMstResetVlanList [ ] ={1,3,6,1,4,1,2076,118,1,5,1,5};
UINT4 FsMIMstInstanceVlanMapped [ ] ={1,3,6,1,4,1,2076,118,1,5,1,6};
UINT4 FsMIMstInstanceVlanMapped2k [ ] ={1,3,6,1,4,1,2076,118,1,5,1,7};
UINT4 FsMIMstInstanceVlanMapped3k [ ] ={1,3,6,1,4,1,2076,118,1,5,1,8};
UINT4 FsMIMstInstanceVlanMapped4k [ ] ={1,3,6,1,4,1,2076,118,1,5,1,9};
UINT4 FsMIMstCistPort [ ] ={1,3,6,1,4,1,2076,118,1,6,1,1};
UINT4 FsMIMstCistPortPathCost [ ] ={1,3,6,1,4,1,2076,118,1,6,1,2};
UINT4 FsMIMstCistPortPriority [ ] ={1,3,6,1,4,1,2076,118,1,6,1,3};
UINT4 FsMIMstCistPortDesignatedRoot [ ] ={1,3,6,1,4,1,2076,118,1,6,1,4};
UINT4 FsMIMstCistPortDesignatedBridge [ ] ={1,3,6,1,4,1,2076,118,1,6,1,5};
UINT4 FsMIMstCistPortDesignatedPort [ ] ={1,3,6,1,4,1,2076,118,1,6,1,6};
UINT4 FsMIMstCistPortAdminP2P [ ] ={1,3,6,1,4,1,2076,118,1,6,1,7};
UINT4 FsMIMstCistPortOperP2P [ ] ={1,3,6,1,4,1,2076,118,1,6,1,8};
UINT4 FsMIMstCistPortAdminEdgeStatus [ ] ={1,3,6,1,4,1,2076,118,1,6,1,9};
UINT4 FsMIMstCistPortOperEdgeStatus [ ] ={1,3,6,1,4,1,2076,118,1,6,1,10};
UINT4 FsMIMstCistPortProtocolMigration [ ] ={1,3,6,1,4,1,2076,118,1,6,1,11};
UINT4 FsMIMstCistPortState [ ] ={1,3,6,1,4,1,2076,118,1,6,1,12};
UINT4 FsMIMstCistForcePortState [ ] ={1,3,6,1,4,1,2076,118,1,6,1,13};
UINT4 FsMIMstCistPortForwardTransitions [ ] ={1,3,6,1,4,1,2076,118,1,6,1,14};
UINT4 FsMIMstCistPortRxMstBpduCount [ ] ={1,3,6,1,4,1,2076,118,1,6,1,15};
UINT4 FsMIMstCistPortRxRstBpduCount [ ] ={1,3,6,1,4,1,2076,118,1,6,1,16};
UINT4 FsMIMstCistPortRxConfigBpduCount [ ] ={1,3,6,1,4,1,2076,118,1,6,1,17};
UINT4 FsMIMstCistPortRxTcnBpduCount [ ] ={1,3,6,1,4,1,2076,118,1,6,1,18};
UINT4 FsMIMstCistPortTxMstBpduCount [ ] ={1,3,6,1,4,1,2076,118,1,6,1,19};
UINT4 FsMIMstCistPortTxRstBpduCount [ ] ={1,3,6,1,4,1,2076,118,1,6,1,20};
UINT4 FsMIMstCistPortTxConfigBpduCount [ ] ={1,3,6,1,4,1,2076,118,1,6,1,21};
UINT4 FsMIMstCistPortTxTcnBpduCount [ ] ={1,3,6,1,4,1,2076,118,1,6,1,22};
UINT4 FsMIMstCistPortInvalidMstBpduRxCount [ ] ={1,3,6,1,4,1,2076,118,1,6,1,23};
UINT4 FsMIMstCistPortInvalidRstBpduRxCount [ ] ={1,3,6,1,4,1,2076,118,1,6,1,24};
UINT4 FsMIMstCistPortInvalidConfigBpduRxCount [ ] ={1,3,6,1,4,1,2076,118,1,6,1,25};
UINT4 FsMIMstCistPortInvalidTcnBpduRxCount [ ] ={1,3,6,1,4,1,2076,118,1,6,1,26};
UINT4 FsMIMstCistPortTransmitSemState [ ] ={1,3,6,1,4,1,2076,118,1,6,1,27};
UINT4 FsMIMstCistPortReceiveSemState [ ] ={1,3,6,1,4,1,2076,118,1,6,1,28};
UINT4 FsMIMstCistPortProtMigrationSemState [ ] ={1,3,6,1,4,1,2076,118,1,6,1,29};
UINT4 FsMIMstCistProtocolMigrationCount [ ] ={1,3,6,1,4,1,2076,118,1,6,1,30};
UINT4 FsMIMstCistPortDesignatedCost [ ] ={1,3,6,1,4,1,2076,118,1,6,1,31};
UINT4 FsMIMstCistPortRegionalRoot [ ] ={1,3,6,1,4,1,2076,118,1,6,1,32};
UINT4 FsMIMstCistPortRegionalPathCost [ ] ={1,3,6,1,4,1,2076,118,1,6,1,33};
UINT4 FsMIMstCistSelectedPortRole [ ] ={1,3,6,1,4,1,2076,118,1,6,1,34};
UINT4 FsMIMstCistCurrentPortRole [ ] ={1,3,6,1,4,1,2076,118,1,6,1,35};
UINT4 FsMIMstCistPortInfoSemState [ ] ={1,3,6,1,4,1,2076,118,1,6,1,36};
UINT4 FsMIMstCistPortRoleTransitionSemState [ ] ={1,3,6,1,4,1,2076,118,1,6,1,37};
UINT4 FsMIMstCistPortStateTransitionSemState [ ] ={1,3,6,1,4,1,2076,118,1,6,1,38};
UINT4 FsMIMstCistPortTopologyChangeSemState [ ] ={1,3,6,1,4,1,2076,118,1,6,1,39};
UINT4 FsMIMstCistPortHelloTime [ ] ={1,3,6,1,4,1,2076,118,1,6,1,40};
UINT4 FsMIMstCistPortOperVersion [ ] ={1,3,6,1,4,1,2076,118,1,6,1,41};
UINT4 FsMIMstCistPortEffectivePortState [ ] ={1,3,6,1,4,1,2076,118,1,6,1,42};
UINT4 FsMIMstCistPortAutoEdgeStatus [ ] ={1,3,6,1,4,1,2076,118,1,6,1,43};
UINT4 FsMIMstCistPortRestrictedRole [ ] ={1,3,6,1,4,1,2076,118,1,6,1,44};
UINT4 FsMIMstCistPortRestrictedTCN [ ] ={1,3,6,1,4,1,2076,118,1,6,1,45};
UINT4 FsMIMstCistPortAdminPathCost [ ] ={1,3,6,1,4,1,2076,118,1,6,1,46};
UINT4 FsMIMstCistPortEnableBPDURx [ ] ={1,3,6,1,4,1,2076,118,1,6,1,47};
UINT4 FsMIMstCistPortEnableBPDUTx [ ] ={1,3,6,1,4,1,2076,118,1,6,1,48};
UINT4 FsMIMstCistPortPseudoRootId [ ] ={1,3,6,1,4,1,2076,118,1,6,1,49};
UINT4 FsMIMstCistPortIsL2Gp [ ] ={1,3,6,1,4,1,2076,118,1,6,1,50};
UINT4 FsMIMstCistPortLoopGuard [ ] ={1,3,6,1,4,1,2076,118,1,6,1,51};
UINT4 FsMIMstCistPortClearStats [ ] ={1,3,6,1,4,1,2076,118,1,6,1,52};
UINT4 FsMIMstCistPortRcvdEvent [ ] ={1,3,6,1,4,1,2076,118,1,6,1,53};
UINT4 FsMIMstCistPortRcvdEventSubType [ ] ={1,3,6,1,4,1,2076,118,1,6,1,54};
UINT4 FsMIMstCistPortRcvdEventTimeStamp [ ] ={1,3,6,1,4,1,2076,118,1,6,1,55};
UINT4 FsMIMstCistLoopInconsistentState [ ] ={1,3,6,1,4,1,2076,118,1,6,1,56};
UINT4 FsMIMstCistPortBpduGuard [ ] ={1,3,6,1,4,1,2076,118,1,6,1,57};
UINT4 FsMIMstCistPortRootGuard [ ] ={1,3,6,1,4,1,2076,118,1,6,1,58};
UINT4 FsMIMstCistPortRootInconsistentState [ ] ={1,3,6,1,4,1,2076,118,1,6,1,59};
UINT4 FsMIMstCistPortErrorRecovery [ ] ={1,3,6,1,4,1,2076,118,1,6,1,60};
UINT4 FsMIMstPortBpduInconsistentState [ ] ={1,3,6,1,4,1,2076,118,1,6,1,61};
UINT4 FsMIMstPortBpduGuardAction [ ] ={1,3,6,1,4,1,2076,118,1,6,1,62};
UINT4 FsMIMstCistPortTCDetectedCount [ ] ={1,3,6,1,4,1,2076,118,1,6,1,63};
UINT4 FsMIMstCistPortTCReceivedCount [ ] ={1,3,6,1,4,1,2076,118,1,6,1,64};
UINT4 FsMIMstCistPortTCDetectedTimeStamp [ ] ={1,3,6,1,4,1,2076,118,1,6,1,65};
UINT4 FsMIMstCistPortTCReceivedTimeStamp [ ] ={1,3,6,1,4,1,2076,118,1,6,1,66};
UINT4 FsMIMstCistPortRcvInfoWhileExpCount [ ] ={1,3,6,1,4,1,2076,118,1,6,1,67};
UINT4 FsMIMstCistPortRcvInfoWhileExpTimeStamp [ ] ={1,3,6,1,4,1,2076,118,1,6,1,68};
UINT4 FsMIMstCistPortProposalPktsSent [ ] ={1,3,6,1,4,1,2076,118,1,6,1,69};
UINT4 FsMIMstCistPortProposalPktsRcvd [ ] ={1,3,6,1,4,1,2076,118,1,6,1,70};
UINT4 FsMIMstCistPortProposalPktSentTimeStamp [ ] ={1,3,6,1,4,1,2076,118,1,6,1,71};
UINT4 FsMIMstCistPortProposalPktRcvdTimeStamp [ ] ={1,3,6,1,4,1,2076,118,1,6,1,72};
UINT4 FsMIMstCistPortAgreementPktSent [ ] ={1,3,6,1,4,1,2076,118,1,6,1,73};
UINT4 FsMIMstCistPortAgreementPktRcvd [ ] ={1,3,6,1,4,1,2076,118,1,6,1,74};
UINT4 FsMIMstCistPortAgreementPktSentTimeStamp [ ] ={1,3,6,1,4,1,2076,118,1,6,1,75};
UINT4 FsMIMstCistPortAgreementPktRcvdTimeStamp [ ] ={1,3,6,1,4,1,2076,118,1,6,1,76};
UINT4 FsMIMstCistPortImpStateOccurCount [ ] ={1,3,6,1,4,1,2076,118,1,6,1,77};
UINT4 FsMIMstCistPortImpStateOccurTimeStamp [ ] ={1,3,6,1,4,1,2076,118,1,6,1,78};
UINT4 FsMIMstCistPortOldPortState [ ] ={1,3,6,1,4,1,2076,118,1,6,1,79};
UINT4 FsMIMstMstiPort [ ] ={1,3,6,1,4,1,2076,118,1,7,1,1};
UINT4 FsMIMstMstiPortPathCost [ ] ={1,3,6,1,4,1,2076,118,1,7,1,2};
UINT4 FsMIMstMstiPortPriority [ ] ={1,3,6,1,4,1,2076,118,1,7,1,3};
UINT4 FsMIMstMstiPortDesignatedRoot [ ] ={1,3,6,1,4,1,2076,118,1,7,1,4};
UINT4 FsMIMstMstiPortDesignatedBridge [ ] ={1,3,6,1,4,1,2076,118,1,7,1,5};
UINT4 FsMIMstMstiPortDesignatedPort [ ] ={1,3,6,1,4,1,2076,118,1,7,1,6};
UINT4 FsMIMstMstiPortState [ ] ={1,3,6,1,4,1,2076,118,1,7,1,7};
UINT4 FsMIMstMstiForcePortState [ ] ={1,3,6,1,4,1,2076,118,1,7,1,8};
UINT4 FsMIMstMstiPortForwardTransitions [ ] ={1,3,6,1,4,1,2076,118,1,7,1,9};
UINT4 FsMIMstMstiPortReceivedBPDUs [ ] ={1,3,6,1,4,1,2076,118,1,7,1,10};
UINT4 FsMIMstMstiPortTransmittedBPDUs [ ] ={1,3,6,1,4,1,2076,118,1,7,1,11};
UINT4 FsMIMstMstiPortInvalidBPDUsRcvd [ ] ={1,3,6,1,4,1,2076,118,1,7,1,12};
UINT4 FsMIMstMstiPortDesignatedCost [ ] ={1,3,6,1,4,1,2076,118,1,7,1,13};
UINT4 FsMIMstMstiSelectedPortRole [ ] ={1,3,6,1,4,1,2076,118,1,7,1,14};
UINT4 FsMIMstMstiCurrentPortRole [ ] ={1,3,6,1,4,1,2076,118,1,7,1,15};
UINT4 FsMIMstMstiPortInfoSemState [ ] ={1,3,6,1,4,1,2076,118,1,7,1,16};
UINT4 FsMIMstMstiPortRoleTransitionSemState [ ] ={1,3,6,1,4,1,2076,118,1,7,1,17};
UINT4 FsMIMstMstiPortStateTransitionSemState [ ] ={1,3,6,1,4,1,2076,118,1,7,1,18};
UINT4 FsMIMstMstiPortTopologyChangeSemState [ ] ={1,3,6,1,4,1,2076,118,1,7,1,19};
UINT4 FsMIMstMstiPortEffectivePortState [ ] ={1,3,6,1,4,1,2076,118,1,7,1,20};
UINT4 FsMIMstMstiPortAdminPathCost [ ] ={1,3,6,1,4,1,2076,118,1,7,1,21};
UINT4 FsMIMstMstiPortPseudoRootId [ ] ={1,3,6,1,4,1,2076,118,1,7,1,22};
UINT4 FsMIMstMstiPortClearStats [ ] ={1,3,6,1,4,1,2076,118,1,7,1,23};
UINT4 FsMIMstMstiPortStateChangeTimeStamp [ ] ={1,3,6,1,4,1,2076,118,1,7,1,24};
UINT4 FsMIMstMstiPortRootInconsistentState [ ] ={1,3,6,1,4,1,2076,118,1,7,1,25};
UINT4 FsMIMstMstiPortLoopInconsistentState [ ] ={1,3,6,1,4,1,2076,118,1,7,1,26};
UINT4 FsMIMstMstiPortTCDetectedCount [ ] ={1,3,6,1,4,1,2076,118,1,7,1,27};
UINT4 FsMIMstMstiPortTCReceivedCount [ ] ={1,3,6,1,4,1,2076,118,1,7,1,28};
UINT4 FsMIMstMstiPortTCDetectedTimeStamp [ ] ={1,3,6,1,4,1,2076,118,1,7,1,29};
UINT4 FsMIMstMstiPortTCReceivedTimeStamp [ ] ={1,3,6,1,4,1,2076,118,1,7,1,30};
UINT4 FsMIMstMstiPortProposalPktsSent [ ] ={1,3,6,1,4,1,2076,118,1,7,1,31};
UINT4 FsMIMstMstiPortProposalPktsRcvd [ ] ={1,3,6,1,4,1,2076,118,1,7,1,32};
UINT4 FsMIMstMstiPortProposalPktSentTimeStamp [ ] ={1,3,6,1,4,1,2076,118,1,7,1,33};
UINT4 FsMIMstMstiPortProposalPktRcvdTimeStamp [ ] ={1,3,6,1,4,1,2076,118,1,7,1,34};
UINT4 FsMIMstMstiPortAgreementPktSent [ ] ={1,3,6,1,4,1,2076,118,1,7,1,35};
UINT4 FsMIMstMstiPortAgreementPktRcvd [ ] ={1,3,6,1,4,1,2076,118,1,7,1,36};
UINT4 FsMIMstMstiPortAgreementPktSentTimeStamp [ ] ={1,3,6,1,4,1,2076,118,1,7,1,37};
UINT4 FsMIMstMstiPortAgreementPktRcvdTimeStamp [ ] ={1,3,6,1,4,1,2076,118,1,7,1,38};
UINT4 FsMIMstMstiPortOldPortState [ ] ={1,3,6,1,4,1,2076,118,1,7,1,39};
UINT4 FsMIMstPort [ ] ={1,3,6,1,4,1,2076,118,1,8,1,1};
UINT4 FsMIMstPortRowStatus [ ] ={1,3,6,1,4,1,2076,118,1,8,1,2};
UINT4 FsMIDot1sFsMstSetGlobalTrapOption [ ] ={1,3,6,1,4,1,2076,118,2,1};
UINT4 FsMIMstGlobalErrTrapType [ ] ={1,3,6,1,4,1,2076,118,2,2};
UINT4 FsMIMstSetTraps [ ] ={1,3,6,1,4,1,2076,118,2,3,1,1};
UINT4 FsMIMstGenTrapType [ ] ={1,3,6,1,4,1,2076,118,2,3,1,2};
UINT4 FsMIMstPortTrapIndex [ ] ={1,3,6,1,4,1,2076,118,2,4,1,1};
UINT4 FsMIMstPortMigrationType [ ] ={1,3,6,1,4,1,2076,118,2,4,1,2};
UINT4 FsMIMstPktErrType [ ] ={1,3,6,1,4,1,2076,118,2,4,1,3};
UINT4 FsMIMstPktErrVal [ ] ={1,3,6,1,4,1,2076,118,2,4,1,4};
UINT4 FsMIMstPortRoleType [ ] ={1,3,6,1,4,1,2076,118,2,5,1,1};
UINT4 FsMIMstOldRoleType [ ] ={1,3,6,1,4,1,2076,118,2,5,1,2};




tMbDbEntry fsmpmsMibEntry[]= {

{{10,FsMIMstGlobalTrace}, NULL, FsMIMstGlobalTraceGet, FsMIMstGlobalTraceSet, FsMIMstGlobalTraceTest, FsMIMstGlobalTraceDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsMIMstGlobalDebug}, NULL, FsMIMstGlobalDebugGet, FsMIMstGlobalDebugSet, FsMIMstGlobalDebugTest, FsMIMstGlobalDebugDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsMIDot1sFutureMstContextId}, GetNextIndexFsMIDot1sFutureMstTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstSystemControl}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstSystemControlGet, FsMIMstSystemControlSet, FsMIMstSystemControlTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstModuleStatus}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstModuleStatusGet, FsMIMstModuleStatusSet, FsMIMstModuleStatusTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstMaxMstInstanceNumber}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstMaxMstInstanceNumberGet, FsMIMstMaxMstInstanceNumberSet, FsMIMstMaxMstInstanceNumberTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstNoOfMstiSupported}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstNoOfMstiSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstMaxHopCount}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstMaxHopCountGet, FsMIMstMaxHopCountSet, FsMIMstMaxHopCountTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, "2000"},

{{12,FsMIMstBrgAddress}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstBrgAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistRoot}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstCistRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistRegionalRoot}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstCistRegionalRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistRootCost}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstCistRootCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistRegionalRootCost}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstCistRegionalRootCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistRootPort}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstCistRootPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistBridgePriority}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstCistBridgePriorityGet, FsMIMstCistBridgePrioritySet, FsMIMstCistBridgePriorityTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, "32768"},

{{12,FsMIMstCistBridgeMaxAge}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstCistBridgeMaxAgeGet, FsMIMstCistBridgeMaxAgeSet, FsMIMstCistBridgeMaxAgeTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, "2000"},

{{12,FsMIMstCistBridgeForwardDelay}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstCistBridgeForwardDelayGet, FsMIMstCistBridgeForwardDelaySet, FsMIMstCistBridgeForwardDelayTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, "1500"},

{{12,FsMIMstCistHoldTime}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstCistHoldTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistMaxAge}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstCistMaxAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistForwardDelay}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstCistForwardDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstMstpUpCount}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstMstpUpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstMstpDownCount}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstMstpDownCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstTrace}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstTraceGet, FsMIMstTraceSet, FsMIMstTraceTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, "0"},

{{12,FsMIMstDebug}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstDebugGet, FsMIMstDebugSet, FsMIMstDebugTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, "0"},

{{12,FsMIMstForceProtocolVersion}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstForceProtocolVersionGet, FsMIMstForceProtocolVersionSet, FsMIMstForceProtocolVersionTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, "3"},

{{12,FsMIMstTxHoldCount}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstTxHoldCountGet, FsMIMstTxHoldCountSet, FsMIMstTxHoldCountTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, "6"},

{{12,FsMIMstMstiConfigIdSel}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstMstiConfigIdSelGet, FsMIMstMstiConfigIdSelSet, FsMIMstMstiConfigIdSelTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstMstiRegionName}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstMstiRegionNameGet, FsMIMstMstiRegionNameSet, FsMIMstMstiRegionNameTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstMstiRegionVersion}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstMstiRegionVersionGet, FsMIMstMstiRegionVersionSet, FsMIMstMstiRegionVersionTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstMstiConfigDigest}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstMstiConfigDigestGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstBufferOverFlowCount}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstBufferOverFlowCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstMemAllocFailureCount}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstMemAllocFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstRegionConfigChangeCount}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstRegionConfigChangeCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistBridgeRoleSelectionSemState}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstCistBridgeRoleSelectionSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistTimeSinceTopologyChange}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstCistTimeSinceTopologyChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistTopChanges}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstCistTopChangesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistNewRootBridgeCount}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstCistNewRootBridgeCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistHelloTime}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstCistHelloTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistBridgeHelloTime}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstCistBridgeHelloTimeGet, FsMIMstCistBridgeHelloTimeSet, FsMIMstCistBridgeHelloTimeTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistDynamicPathcostCalculation}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstCistDynamicPathcostCalculationGet, FsMIMstCistDynamicPathcostCalculationSet, FsMIMstCistDynamicPathcostCalculationTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, "2"},

{{12,FsMIMstContextName}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstContextNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCalcPortPathCostOnSpeedChg}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstCalcPortPathCostOnSpeedChgGet, FsMIMstCalcPortPathCostOnSpeedChgSet, FsMIMstCalcPortPathCostOnSpeedChgTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, "2"},

{{12,FsMIMstClearBridgeStats}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstClearBridgeStatsGet, FsMIMstClearBridgeStatsSet, FsMIMstClearBridgeStatsTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstRcvdEvent}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstRcvdEventGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstRcvdEventSubType}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstRcvdEventSubTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstRcvdEventTimeStamp}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstRcvdEventTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstPortStateChangeTimeStamp}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstPortStateChangeTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstFlushInterval}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstFlushIntervalGet, FsMIMstFlushIntervalSet, FsMIMstFlushIntervalTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, "0"},

{{12,FsMIMstCistFlushIndicationThreshold}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstCistFlushIndicationThresholdGet, FsMIMstCistFlushIndicationThresholdSet, FsMIMstCistFlushIndicationThresholdTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, "0"},

{{12,FsMIMstCistTotalFlushCount}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstCistTotalFlushCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstBpduGuard}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstBpduGuardGet, FsMIMstBpduGuardSet, FsMIMstBpduGuardTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstStpPerfStatus}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstStpPerfStatusGet, FsMIMstStpPerfStatusSet, FsMIMstStpPerfStatusTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstInstPortsMap}, GetNextIndexFsMIDot1sFutureMstTable, FsMIMstInstPortsMapGet, FsMIMstInstPortsMapSet, FsMIMstInstPortsMapTest, FsMIDot1sFutureMstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1sFutureMstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstMstiInstanceIndex}, GetNextIndexFsMIMstMstiBridgeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIMstMstiBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiBridgeRegionalRoot}, GetNextIndexFsMIMstMstiBridgeTable, FsMIMstMstiBridgeRegionalRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIMstMstiBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiBridgePriority}, GetNextIndexFsMIMstMstiBridgeTable, FsMIMstMstiBridgePriorityGet, FsMIMstMstiBridgePrioritySet, FsMIMstMstiBridgePriorityTest, FsMIMstMstiBridgeTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIMstMstiBridgeTableINDEX, 2, 0, 0, "32768"},

{{12,FsMIMstMstiRootCost}, GetNextIndexFsMIMstMstiBridgeTable, FsMIMstMstiRootCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIMstMstiBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiRootPort}, GetNextIndexFsMIMstMstiBridgeTable, FsMIMstMstiRootPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIMstMstiBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiTimeSinceTopologyChange}, GetNextIndexFsMIMstMstiBridgeTable, FsMIMstMstiTimeSinceTopologyChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIMstMstiBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiTopChanges}, GetNextIndexFsMIMstMstiBridgeTable, FsMIMstMstiTopChangesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstMstiBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiNewRootBridgeCount}, GetNextIndexFsMIMstMstiBridgeTable, FsMIMstMstiNewRootBridgeCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstMstiBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiBridgeRoleSelectionSemState}, GetNextIndexFsMIMstMstiBridgeTable, FsMIMstMstiBridgeRoleSelectionSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstMstiBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstInstanceUpCount}, GetNextIndexFsMIMstMstiBridgeTable, FsMIMstInstanceUpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstMstiBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstInstanceDownCount}, GetNextIndexFsMIMstMstiBridgeTable, FsMIMstInstanceDownCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstMstiBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstOldDesignatedRoot}, GetNextIndexFsMIMstMstiBridgeTable, FsMIMstOldDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIMstMstiBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiClearBridgeStats}, GetNextIndexFsMIMstMstiBridgeTable, FsMIMstMstiClearBridgeStatsGet, FsMIMstMstiClearBridgeStatsSet, FsMIMstMstiClearBridgeStatsTest, FsMIMstMstiBridgeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstMstiBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiFlushIndicationThreshold}, GetNextIndexFsMIMstMstiBridgeTable, FsMIMstMstiFlushIndicationThresholdGet, FsMIMstMstiFlushIndicationThresholdSet, FsMIMstMstiFlushIndicationThresholdTest, FsMIMstMstiBridgeTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIMstMstiBridgeTableINDEX, 2, 0, 0, "0"},

{{12,FsMIMstMstiTotalFlushCount}, GetNextIndexFsMIMstMstiBridgeTable, FsMIMstMstiTotalFlushCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstMstiBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstiRootPriority}, GetNextIndexFsMIMstMstiBridgeTable, FsMIMstiRootPriorityGet, FsMIMstiRootPrioritySet, FsMIMstiRootPriorityTest, FsMIMstMstiBridgeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstMstiBridgeTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstInstanceIndex}, GetNextIndexFsMIMstVlanInstanceMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIMstVlanInstanceMappingTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMapVlanIndex}, GetNextIndexFsMIMstVlanInstanceMappingTable, FsMIMstMapVlanIndexGet, FsMIMstMapVlanIndexSet, FsMIMstMapVlanIndexTest, FsMIMstVlanInstanceMappingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstVlanInstanceMappingTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstUnMapVlanIndex}, GetNextIndexFsMIMstVlanInstanceMappingTable, FsMIMstUnMapVlanIndexGet, FsMIMstUnMapVlanIndexSet, FsMIMstUnMapVlanIndexTest, FsMIMstVlanInstanceMappingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstVlanInstanceMappingTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstSetVlanList}, GetNextIndexFsMIMstVlanInstanceMappingTable, FsMIMstSetVlanListGet, FsMIMstSetVlanListSet, FsMIMstSetVlanListTest, FsMIMstVlanInstanceMappingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIMstVlanInstanceMappingTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstResetVlanList}, GetNextIndexFsMIMstVlanInstanceMappingTable, FsMIMstResetVlanListGet, FsMIMstResetVlanListSet, FsMIMstResetVlanListTest, FsMIMstVlanInstanceMappingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIMstVlanInstanceMappingTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstInstanceVlanMapped}, GetNextIndexFsMIMstVlanInstanceMappingTable, FsMIMstInstanceVlanMappedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIMstVlanInstanceMappingTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstInstanceVlanMapped2k}, GetNextIndexFsMIMstVlanInstanceMappingTable, FsMIMstInstanceVlanMapped2kGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIMstVlanInstanceMappingTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstInstanceVlanMapped3k}, GetNextIndexFsMIMstVlanInstanceMappingTable, FsMIMstInstanceVlanMapped3kGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIMstVlanInstanceMappingTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstInstanceVlanMapped4k}, GetNextIndexFsMIMstVlanInstanceMappingTable, FsMIMstInstanceVlanMapped4kGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIMstVlanInstanceMappingTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstCistPort}, GetNextIndexFsMIMstCistPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortPathCost}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortPathCostGet, FsMIMstCistPortPathCostSet, FsMIMstCistPortPathCostTest, FsMIMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortPriority}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortPriorityGet, FsMIMstCistPortPrioritySet, FsMIMstCistPortPriorityTest, FsMIMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIMstCistPortTableINDEX, 1, 0, 0, "128"},

{{12,FsMIMstCistPortDesignatedRoot}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortDesignatedBridge}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortDesignatedBridgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortDesignatedPort}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortDesignatedPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortAdminP2P}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortAdminP2PGet, FsMIMstCistPortAdminP2PSet, FsMIMstCistPortAdminP2PTest, FsMIMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortOperP2P}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortOperP2PGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortAdminEdgeStatus}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortAdminEdgeStatusGet, FsMIMstCistPortAdminEdgeStatusSet, FsMIMstCistPortAdminEdgeStatusTest, FsMIMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortOperEdgeStatus}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortOperEdgeStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortProtocolMigration}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortProtocolMigrationGet, FsMIMstCistPortProtocolMigrationSet, FsMIMstCistPortProtocolMigrationTest, FsMIMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortState}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistForcePortState}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistForcePortStateGet, FsMIMstCistForcePortStateSet, FsMIMstCistForcePortStateTest, FsMIMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortForwardTransitions}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortForwardTransitionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortRxMstBpduCount}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortRxMstBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortRxRstBpduCount}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortRxRstBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortRxConfigBpduCount}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortRxConfigBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortRxTcnBpduCount}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortRxTcnBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortTxMstBpduCount}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortTxMstBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortTxRstBpduCount}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortTxRstBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortTxConfigBpduCount}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortTxConfigBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortTxTcnBpduCount}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortTxTcnBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortInvalidMstBpduRxCount}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortInvalidMstBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortInvalidRstBpduRxCount}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortInvalidRstBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortInvalidConfigBpduRxCount}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortInvalidConfigBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortInvalidTcnBpduRxCount}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortInvalidTcnBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortTransmitSemState}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortTransmitSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortReceiveSemState}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortReceiveSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortProtMigrationSemState}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortProtMigrationSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistProtocolMigrationCount}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistProtocolMigrationCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortDesignatedCost}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortDesignatedCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortRegionalRoot}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortRegionalRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortRegionalPathCost}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortRegionalPathCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistSelectedPortRole}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistSelectedPortRoleGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistCurrentPortRole}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistCurrentPortRoleGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortInfoSemState}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortInfoSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortRoleTransitionSemState}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortRoleTransitionSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortStateTransitionSemState}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortStateTransitionSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortTopologyChangeSemState}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortTopologyChangeSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortHelloTime}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortHelloTimeGet, FsMIMstCistPortHelloTimeSet, FsMIMstCistPortHelloTimeTest, FsMIMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortOperVersion}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortOperVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortEffectivePortState}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortEffectivePortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortAutoEdgeStatus}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortAutoEdgeStatusGet, FsMIMstCistPortAutoEdgeStatusSet, FsMIMstCistPortAutoEdgeStatusTest, FsMIMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortRestrictedRole}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortRestrictedRoleGet, FsMIMstCistPortRestrictedRoleSet, FsMIMstCistPortRestrictedRoleTest, FsMIMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortRestrictedTCN}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortRestrictedTCNGet, FsMIMstCistPortRestrictedTCNSet, FsMIMstCistPortRestrictedTCNTest, FsMIMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortAdminPathCost}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortAdminPathCostGet, FsMIMstCistPortAdminPathCostSet, FsMIMstCistPortAdminPathCostTest, FsMIMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortEnableBPDURx}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortEnableBPDURxGet, FsMIMstCistPortEnableBPDURxSet, FsMIMstCistPortEnableBPDURxTest, FsMIMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstCistPortTableINDEX, 1, 0, 0, "1"},

{{12,FsMIMstCistPortEnableBPDUTx}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortEnableBPDUTxGet, FsMIMstCistPortEnableBPDUTxSet, FsMIMstCistPortEnableBPDUTxTest, FsMIMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstCistPortTableINDEX, 1, 0, 0, "1"},

{{12,FsMIMstCistPortPseudoRootId}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortPseudoRootIdGet, FsMIMstCistPortPseudoRootIdSet, FsMIMstCistPortPseudoRootIdTest, FsMIMstCistPortTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortIsL2Gp}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortIsL2GpGet, FsMIMstCistPortIsL2GpSet, FsMIMstCistPortIsL2GpTest, FsMIMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstCistPortTableINDEX, 1, 0, 0, "2"},

{{12,FsMIMstCistPortLoopGuard}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortLoopGuardGet, FsMIMstCistPortLoopGuardSet, FsMIMstCistPortLoopGuardTest, FsMIMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstCistPortTableINDEX, 1, 0, 0, "2"},

{{12,FsMIMstCistPortClearStats}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortClearStatsGet, FsMIMstCistPortClearStatsSet, FsMIMstCistPortClearStatsTest, FsMIMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortRcvdEvent}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortRcvdEventGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortRcvdEventSubType}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortRcvdEventSubTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortRcvdEventTimeStamp}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortRcvdEventTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistLoopInconsistentState}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistLoopInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, "2"},

{{12,FsMIMstCistPortBpduGuard}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortBpduGuardGet, FsMIMstCistPortBpduGuardSet, FsMIMstCistPortBpduGuardTest, FsMIMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortRootGuard}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortRootGuardGet, FsMIMstCistPortRootGuardSet, FsMIMstCistPortRootGuardTest, FsMIMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortRootInconsistentState}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortRootInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, "2"},

{{12,FsMIMstCistPortErrorRecovery}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortErrorRecoveryGet, FsMIMstCistPortErrorRecoverySet, FsMIMstCistPortErrorRecoveryTest, FsMIMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstCistPortTableINDEX, 1, 0, 0, "30000"},

{{12,FsMIMstPortBpduInconsistentState}, GetNextIndexFsMIMstCistPortTable, FsMIMstPortBpduInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, "2"},

{{12,FsMIMstPortBpduGuardAction}, GetNextIndexFsMIMstCistPortTable, FsMIMstPortBpduGuardActionGet, FsMIMstPortBpduGuardActionSet, FsMIMstPortBpduGuardActionTest, FsMIMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstCistPortTableINDEX, 1, 0, 0, "1"},

{{12,FsMIMstCistPortTCDetectedCount}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortTCDetectedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortTCReceivedCount}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortTCReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortTCDetectedTimeStamp}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortTCDetectedTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortTCReceivedTimeStamp}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortTCReceivedTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortRcvInfoWhileExpCount}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortRcvInfoWhileExpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortRcvInfoWhileExpTimeStamp}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortRcvInfoWhileExpTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortProposalPktsSent}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortProposalPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortProposalPktsRcvd}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortProposalPktsRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortProposalPktSentTimeStamp}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortProposalPktSentTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortProposalPktRcvdTimeStamp}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortProposalPktRcvdTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortAgreementPktSent}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortAgreementPktSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortAgreementPktRcvd}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortAgreementPktRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortAgreementPktSentTimeStamp}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortAgreementPktSentTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortAgreementPktRcvdTimeStamp}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortAgreementPktRcvdTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortImpStateOccurCount}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortImpStateOccurCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortImpStateOccurTimeStamp}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortImpStateOccurTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstCistPortOldPortState}, GetNextIndexFsMIMstCistPortTable, FsMIMstCistPortOldPortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstMstiPort}, GetNextIndexFsMIMstMstiPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortPathCost}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortPathCostGet, FsMIMstMstiPortPathCostSet, FsMIMstMstiPortPathCostTest, FsMIMstMstiPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortPriority}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortPriorityGet, FsMIMstMstiPortPrioritySet, FsMIMstMstiPortPriorityTest, FsMIMstMstiPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIMstMstiPortTableINDEX, 2, 0, 0, "128"},

{{12,FsMIMstMstiPortDesignatedRoot}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortDesignatedBridge}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortDesignatedBridgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortDesignatedPort}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortDesignatedPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortState}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiForcePortState}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiForcePortStateGet, FsMIMstMstiForcePortStateSet, FsMIMstMstiForcePortStateTest, FsMIMstMstiPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortForwardTransitions}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortForwardTransitionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortReceivedBPDUs}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortReceivedBPDUsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortTransmittedBPDUs}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortTransmittedBPDUsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortInvalidBPDUsRcvd}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortInvalidBPDUsRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortDesignatedCost}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortDesignatedCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiSelectedPortRole}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiSelectedPortRoleGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiCurrentPortRole}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiCurrentPortRoleGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortInfoSemState}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortInfoSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortRoleTransitionSemState}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortRoleTransitionSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortStateTransitionSemState}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortStateTransitionSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortTopologyChangeSemState}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortTopologyChangeSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortEffectivePortState}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortEffectivePortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortAdminPathCost}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortAdminPathCostGet, FsMIMstMstiPortAdminPathCostSet, FsMIMstMstiPortAdminPathCostTest, FsMIMstMstiPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortPseudoRootId}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortPseudoRootIdGet, FsMIMstMstiPortPseudoRootIdSet, FsMIMstMstiPortPseudoRootIdTest, FsMIMstMstiPortTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortClearStats}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortClearStatsGet, FsMIMstMstiPortClearStatsSet, FsMIMstMstiPortClearStatsTest, FsMIMstMstiPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortStateChangeTimeStamp}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortStateChangeTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortRootInconsistentState}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortRootInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, "2"},

{{12,FsMIMstMstiPortLoopInconsistentState}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortLoopInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, "2"},

{{12,FsMIMstMstiPortTCDetectedCount}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortTCDetectedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortTCReceivedCount}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortTCReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortTCDetectedTimeStamp}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortTCDetectedTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortTCReceivedTimeStamp}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortTCReceivedTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortProposalPktsSent}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortProposalPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortProposalPktsRcvd}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortProposalPktsRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortProposalPktSentTimeStamp}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortProposalPktSentTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortProposalPktRcvdTimeStamp}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortProposalPktRcvdTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortAgreementPktSent}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortAgreementPktSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortAgreementPktRcvd}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortAgreementPktRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortAgreementPktSentTimeStamp}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortAgreementPktSentTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortAgreementPktRcvdTimeStamp}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortAgreementPktRcvdTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstMstiPortOldPortState}, GetNextIndexFsMIMstMstiPortTable, FsMIMstMstiPortOldPortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstPort}, GetNextIndexFsMIMstPortExtTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIMstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstPortRowStatus}, GetNextIndexFsMIMstPortExtTable, FsMIMstPortRowStatusGet, FsMIMstPortRowStatusSet, FsMIMstPortRowStatusTest, FsMIMstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIMstPortExtTableINDEX, 1, 0, 1, NULL},

{{10,FsMIDot1sFsMstSetGlobalTrapOption}, NULL, FsMIDot1sFsMstSetGlobalTrapOptionGet, FsMIDot1sFsMstSetGlobalTrapOptionSet, FsMIDot1sFsMstSetGlobalTrapOptionTest, FsMIDot1sFsMstSetGlobalTrapOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsMIMstGlobalErrTrapType}, NULL, FsMIMstGlobalErrTrapTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsMIMstSetTraps}, GetNextIndexFsMIDot1sFsMstTrapsControlTable, FsMIMstSetTrapsGet, FsMIMstSetTrapsSet, FsMIMstSetTrapsTest, FsMIDot1sFsMstTrapsControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1sFsMstTrapsControlTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstGenTrapType}, GetNextIndexFsMIDot1sFsMstTrapsControlTable, FsMIMstGenTrapTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIDot1sFsMstTrapsControlTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstPortTrapIndex}, GetNextIndexFsMIMstPortTrapNotificationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIMstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstPortMigrationType}, GetNextIndexFsMIMstPortTrapNotificationTable, FsMIMstPortMigrationTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstPktErrType}, GetNextIndexFsMIMstPortTrapNotificationTable, FsMIMstPktErrTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstPktErrVal}, GetNextIndexFsMIMstPortTrapNotificationTable, FsMIMstPktErrValGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIMstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsMIMstPortRoleType}, GetNextIndexFsMIMstPortRoleTrapNotificationTable, FsMIMstPortRoleTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstPortRoleTrapNotificationTableINDEX, 2, 0, 0, NULL},

{{12,FsMIMstOldRoleType}, GetNextIndexFsMIMstPortRoleTrapNotificationTable, FsMIMstOldRoleTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIMstPortRoleTrapNotificationTableINDEX, 2, 0, 0, NULL},
};
tMibData fsmpmsEntry = { 200, fsmpmsMibEntry };

#endif /* _FSMPMSDB_H */

