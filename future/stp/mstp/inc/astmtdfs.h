/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmtdfs.h,v 1.8 2009/09/24 14:31:02 prabuc Exp $
 *
 * Description: This file contains type definitions of the data 
 *              structures used in MSTP Module.                
 *
 *******************************************************************/


#ifndef _ASTMTDFS_H
#define _ASTMTDFS_H


typedef UINT1 tAstVlanList [MST_VLANMAP_LIST_SIZE];
typedef UINT1 tAstMstVlanList [MST_VLAN_LIST_SIZE];
typedef UINT1 tAstMstPortInstList [MST_INST_LIST_SIZE];

/**********************************************************************/
/*                MST Configuration  Identifier Information           */
/**********************************************************************/
typedef struct MstConfigIdInfo {

   UINT1                       au1ConfigName[MST_CONFIG_NAME_LEN];
                                                    /* Configuration Name */

   UINT1                       au1ConfigDigest[MST_CONFIG_DIGEST_LEN]; 
                                                    /* Configuration 
                                                     * Digest for this
                                                     * Configuration Id*/

   UINT2                       u2ConfigRevLevel;    /* Revision Level
                                                     * Used in the Region */

   UINT1                       u1ConfigurationId;   /* Configuration 
                                                     *  Identifier 
                                                     *  Format Selector */
   UINT1                       u1Reserved;

}tMstConfigIdInfo;

/******************************************************************************/
/*                        Mst-specific Bridge Information                     */
/******************************************************************************/
      
typedef struct AstMstBridgeEntry {

   tAstBridgeId               RegionalRootId;    /* MST Regional Root Identifier
                                                  * for the MST region.
                                                  */
   tMstConfigIdInfo           MstConfigIdInfo;    /*  Structure MST configuration
                                                  * ID  */
   
   UINT4                      u4RegionCfgChangeCount; /* Number of times Buffer 
			                               * Failure has occured */

   UINT1                      *pu1DigestInp;     /* Pointer use for MD5 Digest
                                                  * Calculation
                                                  */  
   UINT2                      u2MaxMstInstances; /* Maximum number of spanning
                                                  * tree instances supported */
   UINT2                      u2NoOfActiveMsti;  /* Number of active spanning
                                                  * tree instances currently
                                                  * active */
   tAstBoolean                bFlagInstTrigger;  /* Trigger the instance
                                                  * role selection */
   tAstBoolean                bExtendedSysId;    /* Indicates whether to use 
                                                    Extended System Id or not */
   UINT1                      u1MaxHopCount;     /* MaxHopCount Value */ 
   
   UINT1                      au1Reserved[3];
}tAstMstBridgeEntry;   
      

/**********************************************************************/
/*               CIST and MST Specific Port Information               */
/**********************************************************************/
typedef struct CistMstiPortInfo {

   tAstBoolean                  bInfoInternal;       /* Set to TRUE if the  
                                                      * Designated Bridge is 
                                                      * in the same MST Region
                                                      * as receving Bridge */

   tAstBoolean                  bNewInfo;             /* Set to TRUE Indicates 
                                                      * New CIST Information 
                                                      * is there to Transmit */

   tAstBoolean                  bNewInfoMsti;        /* Set to TRUE Indicates
                                                      * New MSTI Information
                                                      * is there to Transmit */
   
   tAstBoolean                  bRcvdInternal;       /* Set to TRUE by Receive 
                                                      * Machine if the BPDU 
                                                      * received was transmitted
                                                      * by a Bridge in the same
                                                      * MST Region */

   tAstBoolean                  bCist;               /* Set to TRUE only for 
                                                      * CIST state machines */

   tAstBoolean                  bCistRootPort;       /* Set to TRUE if the CIST 
                                                      * role for the given Port 
                                                      * is RootPort */

   tAstBoolean                  bCistDesignatedPort; /* Set to TRUE if the CIST
                                                      * role for the given Port 
                                                      * is Designated Port */

   tAstBoolean                  bMstiRootPort;       /* Set to TRUE if the role
                                                      * for any MSTI for the 
                                                      * given port is RootPort*/

   tAstBoolean                  bMstiDesignatedPort; /* Set to TRUE if the role
                                                      * for any MSTI for the
                                                      * given port is Designated
                                                      * Port */

   tAstBoolean                  bRcvdAnyMsg;         /* Set to TRUE if the
                                                      * rcvdMsg is TRUE for
                                                      * CIST or any MSTI for 
                                                      * that Port */

   tAstBoolean                  bRcvdCistInfo;       /* Set to TRUE if and
                                                      * only if rcvdMsg is
                                                      * TRUE for the CIST 
                                                      * for that Port */

   tAstBoolean                  bUpdtCistInfo;       /* Set to TRUE if and
                                                      * only if updtInfo is 
                                                      * TRUE for the CIST
                                                      * for that Port */
}tCistMstiPortInfo;


/**********************************************************************/
/*                        MSTI Specific Information                   */
/**********************************************************************/
typedef struct PerStMstiOnlyInfo {

   tAstBoolean                   bRcvdMstiInfo;       /* Set to TRUE for 
                                                       * given Port and MSTI
                                                       * if and only if 
                                                       * rcvdMsg is FALSE for
                                                       * the CIST for that 
                                                       * port and rcvdMsg is 
                                                       * TRUE for the MSTI for
                                                       * that Port */

   tAstBoolean                   bUpdtMstiInfo;       /* Set to TRUE for 
                                                       * given Port and MSTI
                                                       * if and only of 
                                                       * updtInfo is TRUE for
                                                       * the MSTI for that 
                                                       * port or either updtInfo
                                                       * or selected are TRUE
                                                       * for the CIST for that
                                                       * Port */
   tAstBoolean                   bMaster;              /* Set to TRUE for 
                                                       * given Port and MSTI
                                                       * if role is root or
                                                       * Desig and Bridge has 
                                                       * selected one of its 
                                                       * port as Master port for
                                                       * this MSTI or 
                                                       * MstiMastered
                                                       * is set for any 
                                                       * other root
                                                       * or designated port
                                                       */
    tAstBoolean                  bMastered;            /* Used to record the   
                                                       * value of Master 
                                                       * flag for 
                                                       * this MSTI and port 
                                                       * in MST BPDUs received
                                                       * from the attached LAN
                                                       */
 
}tPerStMstiOnlyInfo;


/**********************************************************************/
/*               Information for MSTI and CIST                        */
/**********************************************************************/
typedef struct PerStCistMstiCommInfo {
   

   tAstBoolean                 bAllSynced;     /* Set to TRUE if and 
                                                * only if the bSynced is
                                                * TRUE for all ports for
                                                * the given tree */
   
   tAstBoolean                 bReRooted;      /* Set to TRUE if the rrWhile
                                                * Timer is clear for all
                                                * ports for the tree other
                                                * the given Port */

   UINT1                       u1PortRemainingHops;    
   UINT1                       u1DesgRemainingHops;    
                                               /* Remaining Hops Value used    
                                                * to discard the received    
                                                * information.*/
   UINT1                       au1Reserved[2];     /* Added for alignment
                                                * purpose
                                                */

}tPerStCistMstiCommInfo;

/**********************************************************************/
/*                    MST Configuration Message Information           */
/**********************************************************************/
typedef struct MstConfigMsg {

   tAstBridgeId              MstiRegionalRootId;    /* The unique Identifier
                                                     * of the Bridge assumed 
                                                     * to be the MSTI
                                                     * Regional Root for that 
                                                     * instance
                                                     * by the bridge 
                                                     * transmitting
                                                     * the BPDU */

   UINT4                     u4MstiIntRootPathCost; /* The cost of the Path to
                                                     * MSTI Regional Root bridge
                                                     * from the transmitting 
                                                     * Bridge for that 
                                                     * Instance */

   UINT1                     u1MstiFlags;           /* Contains all the MSTI
                                                     * Flags which are 
                                                     * encoded bit-wise */

   UINT1                     u1MstiBrgPriority;     /* Priority value of the 
                                                     * Bridge */

   UINT1                     u1MstiPortPriority;    /* Priority value of 
                                                     * the Port */

   UINT1                     u1MstiRemainingHops;   /* Remaining Hops value */

   UINT1                     u1ValidFlag;           /* Flag to indicate whether
                                                     * this MstConfigMsg is 
                                                     * valid */
   UINT1                     au1Reserved[3];       

}tMstConfigMsg;

/**********************************************************************/
/*                    MST BPDU Information                            */
/**********************************************************************/
typedef struct MstBpdu {

   tAstBridgeId               CistRootId;           /* The unique Identifier 
                                                     * of the bridge assumed 
                                                     * to be the CIST Root
                                                     * by the bridge 
                                                     * transmitting the Bpdu */
   
   UINT4                      u4CistExtPathCost;    /* The cost of the path 
                                                     * to the CIST Root Bridge
                                                     * denoted by the
                                                     * CIST Root Identifier,
                                                     * from the 
                                                     * transmitting bridge */
   
   tAstBridgeId               CistRegionalRootId;   /* The unique Bridge 
                                                     * Identifier
                                                     * of the Bridge assumed 
                                                     * to be the CIST 
                                                     * Regional Root by the 
                                                     * Bridge transmitting 
                                                     * the Bpdu */

   UINT4                      u4CistIntRootPathCost; /* The cost of the Path to
                                                      * CIST Regional Root 
                                                      * bridge from the 
                                                      * transmitting Bridge */

   tAstBridgeId               CistBridgeId;          /* The Bridge Identifier 
                                                      * of the Transmitting 
                                                      * Bridge 
                                                      */

   
   tMacAddr                   DestAddr;
   tMacAddr                   SrcAddr;
   UINT2                      u2Length;
   UINT1                      au1LlcHeader[3];
   
   UINT1                      u1Version;            /* Protocol Version 
                                                     * Identifier
                                                     * which takes the value of
                                                     * 00000011 (Version 3) */

   UINT1                      u1BpduType;           /* Takes the value of 
                                                     * 00000010
                                                     * which denotes a Rapid
                                                     * Spanning Tree BPDU */

   UINT1                      u1CistFlags;          /* Contains all the CIST 
                                                     * Flags which are 
                                                     * encoded bit-wise */
   UINT2                      u2ProtocolId;         /* Protocol Identifier 
                                                     * which takes the value 
                                                     * of 0 identifying 
                                                     * Algorithm
                                                     * and Protocol */

   
   UINT2                      u2CistPortId;         /* The Port Identifier of
                                                     * the Port on the 
                                                     * transmitting Bridge 
                                                     * through which the Bpdu
                                                     * was transmitted */
   
   UINT2                      u2MessageAge;         /* The age of the MST 
                                                     * Message, being the 
                                                     * time since the
                                                     * generation of the 
                                                     * Mst Bpdu by the Root 
                                                     * that initiated
                                                     * the generation of 
                                                     * this Bpdu */
   
   UINT2                      u2MaxAge;             /* A Timeout value to be 
                                                     * used by all Bridges in 
                                                     * the Bridged LAN. This 
                                                     * value is set by the
                                                     * Root. It is used to 
                                                     * test the age of stored
                                                     * information */
   
   UINT2                      u2HelloTime;          /* The time interval 
                                                     * between the generation 
                                                     * of Rst Bpdus by 
                                                     * the Root */
   
   UINT2                      u2FwdDelay;           /* A Timeout value to be 
                                                     * used by all Bridges in 
                                                     * the Bridged LAN. This 
                                                     * value is set by the
                                                     * Root. Its used when 
                                                     * changing the Port state
                                                     * to Forwarding
                                                     * and for ageing Filtering
                                                     * database entries */
   
   UINT1                      u1Version1Len;        /* Takes the value of 0 
                                                     * which indicates that 
                                                     * there is no Version 1 
                                                     * Protocol information
                                                     * present */

   UINT1                      u1CistRemainingHops;   /* Remaining Hops value */
   
   UINT2                      u2Version3Len;        /* Takes the value of length
                                                     * of Version3 Information 
                                                     */

   tMstConfigIdInfo           MstConfigIdInfo;       /* MST Configuration 
                                                      * Identifier Information 
                                                      */


   tMstConfigMsg              aMstConfigMsg[AST_MAX_MST_INSTANCES];        
                                                     /* MST Configuration 
                                                      * Message Specific to 
                                                      * MSTI */
   
}tMstBpdu;

#endif /* _ASTMTDFS_H */

