/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmhwwr.h
 *
 * Description: This file contains all NPAPI wrapper prototype used by
 *              MSTP module.
 *
 *******************************************************************/

#ifndef __ASTMHWWR_H__
#define __ASTMHWWR_H__

INT4
FsMiMstpNpWrSetInstancePortState (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  UINT2 u2InstanceId, UINT1 u1PortState);

#endif
