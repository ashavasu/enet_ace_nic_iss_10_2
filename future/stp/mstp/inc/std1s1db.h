/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1s1db.h,v 1.1 2011/11/03 09:14:04 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STD1S1DB_H
#define _STD1S1DB_H

UINT1 Ieee8021MstpCistTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021MstpTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021MstpCistPortTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021MstpPortTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021MstpFidToMstiTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021MstpVlanTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021MstpConfigIdTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};

UINT4 std1s1 [] ={1,3,111,2,802,1,1,6};
tSNMP_OID_TYPE std1s1OID = {8, std1s1};


UINT4 Ieee8021MstpCistComponentId [ ] ={1,3,111,2,802,1,1,6,1,1,1,1};
UINT4 Ieee8021MstpCistBridgeIdentifier [ ] ={1,3,111,2,802,1,1,6,1,1,1,2};
UINT4 Ieee8021MstpCistTopologyChange [ ] ={1,3,111,2,802,1,1,6,1,1,1,3};
UINT4 Ieee8021MstpCistRegionalRootIdentifier [ ] ={1,3,111,2,802,1,1,6,1,1,1,4};
UINT4 Ieee8021MstpCistPathCost [ ] ={1,3,111,2,802,1,1,6,1,1,1,5};
UINT4 Ieee8021MstpCistMaxHops [ ] ={1,3,111,2,802,1,1,6,1,1,1,6};
UINT4 Ieee8021MstpComponentId [ ] ={1,3,111,2,802,1,1,6,1,2,1,1};
UINT4 Ieee8021MstpId [ ] ={1,3,111,2,802,1,1,6,1,2,1,2};
UINT4 Ieee8021MstpBridgeId [ ] ={1,3,111,2,802,1,1,6,1,2,1,3};
UINT4 Ieee8021MstpTimeSinceTopologyChange [ ] ={1,3,111,2,802,1,1,6,1,2,1,4};
UINT4 Ieee8021MstpTopologyChanges [ ] ={1,3,111,2,802,1,1,6,1,2,1,5};
UINT4 Ieee8021MstpTopologyChange [ ] ={1,3,111,2,802,1,1,6,1,2,1,6};
UINT4 Ieee8021MstpDesignatedRoot [ ] ={1,3,111,2,802,1,1,6,1,2,1,7};
UINT4 Ieee8021MstpRootPathCost [ ] ={1,3,111,2,802,1,1,6,1,2,1,8};
UINT4 Ieee8021MstpRootPort [ ] ={1,3,111,2,802,1,1,6,1,2,1,9};
UINT4 Ieee8021MstpBridgePriority [ ] ={1,3,111,2,802,1,1,6,1,2,1,10};
UINT4 Ieee8021MstpVids0 [ ] ={1,3,111,2,802,1,1,6,1,2,1,11};
UINT4 Ieee8021MstpVids1 [ ] ={1,3,111,2,802,1,1,6,1,2,1,12};
UINT4 Ieee8021MstpVids2 [ ] ={1,3,111,2,802,1,1,6,1,2,1,13};
UINT4 Ieee8021MstpVids3 [ ] ={1,3,111,2,802,1,1,6,1,2,1,14};
UINT4 Ieee8021MstpRowStatus [ ] ={1,3,111,2,802,1,1,6,1,2,1,15};
UINT4 Ieee8021MstpCistPortComponentId [ ] ={1,3,111,2,802,1,1,6,1,3,1,1};
UINT4 Ieee8021MstpCistPortNum [ ] ={1,3,111,2,802,1,1,6,1,3,1,2};
UINT4 Ieee8021MstpCistPortUptime [ ] ={1,3,111,2,802,1,1,6,1,3,1,3};
UINT4 Ieee8021MstpCistPortAdminPathCost [ ] ={1,3,111,2,802,1,1,6,1,3,1,4};
UINT4 Ieee8021MstpCistPortDesignatedRoot [ ] ={1,3,111,2,802,1,1,6,1,3,1,5};
UINT4 Ieee8021MstpCistPortTopologyChangeAck [ ] ={1,3,111,2,802,1,1,6,1,3,1,6};
UINT4 Ieee8021MstpCistPortHelloTime [ ] ={1,3,111,2,802,1,1,6,1,3,1,7};
UINT4 Ieee8021MstpCistPortAdminEdgePort [ ] ={1,3,111,2,802,1,1,6,1,3,1,8};
UINT4 Ieee8021MstpCistPortOperEdgePort [ ] ={1,3,111,2,802,1,1,6,1,3,1,9};
UINT4 Ieee8021MstpCistPortMacEnabled [ ] ={1,3,111,2,802,1,1,6,1,3,1,10};
UINT4 Ieee8021MstpCistPortMacOperational [ ] ={1,3,111,2,802,1,1,6,1,3,1,11};
UINT4 Ieee8021MstpCistPortRestrictedRole [ ] ={1,3,111,2,802,1,1,6,1,3,1,12};
UINT4 Ieee8021MstpCistPortRestrictedTcn [ ] ={1,3,111,2,802,1,1,6,1,3,1,13};
UINT4 Ieee8021MstpCistPortRole [ ] ={1,3,111,2,802,1,1,6,1,3,1,14};
UINT4 Ieee8021MstpCistPortDisputed [ ] ={1,3,111,2,802,1,1,6,1,3,1,15};
UINT4 Ieee8021MstpCistPortCistRegionalRootId [ ] ={1,3,111,2,802,1,1,6,1,3,1,16};
UINT4 Ieee8021MstpCistPortCistPathCost [ ] ={1,3,111,2,802,1,1,6,1,3,1,17};
UINT4 Ieee8021MstpCistPortProtocolMigration [ ] ={1,3,111,2,802,1,1,6,1,3,1,18};
UINT4 Ieee8021MstpCistPortEnableBPDURx [ ] ={1,3,111,2,802,1,1,6,1,3,1,19};
UINT4 Ieee8021MstpCistPortEnableBPDUTx [ ] ={1,3,111,2,802,1,1,6,1,3,1,20};
UINT4 Ieee8021MstpCistPortPseudoRootId [ ] ={1,3,111,2,802,1,1,6,1,3,1,21};
UINT4 Ieee8021MstpCistPortIsL2Gp [ ] ={1,3,111,2,802,1,1,6,1,3,1,22};
UINT4 Ieee8021MstpPortComponentId [ ] ={1,3,111,2,802,1,1,6,1,4,1,1};
UINT4 Ieee8021MstpPortMstId [ ] ={1,3,111,2,802,1,1,6,1,4,1,2};
UINT4 Ieee8021MstpPortNum [ ] ={1,3,111,2,802,1,1,6,1,4,1,3};
UINT4 Ieee8021MstpPortUptime [ ] ={1,3,111,2,802,1,1,6,1,4,1,4};
UINT4 Ieee8021MstpPortState [ ] ={1,3,111,2,802,1,1,6,1,4,1,5};
UINT4 Ieee8021MstpPortPriority [ ] ={1,3,111,2,802,1,1,6,1,4,1,6};
UINT4 Ieee8021MstpPortPathCost [ ] ={1,3,111,2,802,1,1,6,1,4,1,7};
UINT4 Ieee8021MstpPortDesignatedRoot [ ] ={1,3,111,2,802,1,1,6,1,4,1,8};
UINT4 Ieee8021MstpPortDesignatedCost [ ] ={1,3,111,2,802,1,1,6,1,4,1,9};
UINT4 Ieee8021MstpPortDesignatedBridge [ ] ={1,3,111,2,802,1,1,6,1,4,1,10};
UINT4 Ieee8021MstpPortDesignatedPort [ ] ={1,3,111,2,802,1,1,6,1,4,1,11};
UINT4 Ieee8021MstpPortRole [ ] ={1,3,111,2,802,1,1,6,1,4,1,12};
UINT4 Ieee8021MstpPortDisputed [ ] ={1,3,111,2,802,1,1,6,1,4,1,13};
UINT4 Ieee8021MstpFidToMstiComponentId [ ] ={1,3,111,2,802,1,1,6,1,5,1,1};
UINT4 Ieee8021MstpFidToMstiFid [ ] ={1,3,111,2,802,1,1,6,1,5,1,2};
UINT4 Ieee8021MstpFidToMstiMstId [ ] ={1,3,111,2,802,1,1,6,1,5,1,3};
UINT4 Ieee8021MstpVlanComponentId [ ] ={1,3,111,2,802,1,1,6,1,6,1,1};
UINT4 Ieee8021MstpVlanId [ ] ={1,3,111,2,802,1,1,6,1,6,1,2};
UINT4 Ieee8021MstpVlanMstId [ ] ={1,3,111,2,802,1,1,6,1,6,1,3};
UINT4 Ieee8021MstpConfigIdComponentId [ ] ={1,3,111,2,802,1,1,6,1,7,1,1};
UINT4 Ieee8021MstpConfigIdFormatSelector [ ] ={1,3,111,2,802,1,1,6,1,7,1,2};
UINT4 Ieee8021MstpConfigurationName [ ] ={1,3,111,2,802,1,1,6,1,7,1,3};
UINT4 Ieee8021MstpRevisionLevel [ ] ={1,3,111,2,802,1,1,6,1,7,1,4};
UINT4 Ieee8021MstpConfigurationDigest [ ] ={1,3,111,2,802,1,1,6,1,7,1,5};




tMbDbEntry std1s1MibEntry[]= {

{{12,Ieee8021MstpCistComponentId}, GetNextIndexIeee8021MstpCistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021MstpCistTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021MstpCistBridgeIdentifier}, GetNextIndexIeee8021MstpCistTable, Ieee8021MstpCistBridgeIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021MstpCistTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021MstpCistTopologyChange}, GetNextIndexIeee8021MstpCistTable, Ieee8021MstpCistTopologyChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021MstpCistTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021MstpCistRegionalRootIdentifier}, GetNextIndexIeee8021MstpCistTable, Ieee8021MstpCistRegionalRootIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021MstpCistTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021MstpCistPathCost}, GetNextIndexIeee8021MstpCistTable, Ieee8021MstpCistPathCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021MstpCistTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021MstpCistMaxHops}, GetNextIndexIeee8021MstpCistTable, Ieee8021MstpCistMaxHopsGet, Ieee8021MstpCistMaxHopsSet, Ieee8021MstpCistMaxHopsTest, Ieee8021MstpCistTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ieee8021MstpCistTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021MstpComponentId}, GetNextIndexIeee8021MstpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021MstpTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpId}, GetNextIndexIeee8021MstpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021MstpTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpBridgeId}, GetNextIndexIeee8021MstpTable, Ieee8021MstpBridgeIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021MstpTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpTimeSinceTopologyChange}, GetNextIndexIeee8021MstpTable, Ieee8021MstpTimeSinceTopologyChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Ieee8021MstpTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpTopologyChanges}, GetNextIndexIeee8021MstpTable, Ieee8021MstpTopologyChangesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Ieee8021MstpTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpTopologyChange}, GetNextIndexIeee8021MstpTable, Ieee8021MstpTopologyChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021MstpTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpDesignatedRoot}, GetNextIndexIeee8021MstpTable, Ieee8021MstpDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021MstpTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpRootPathCost}, GetNextIndexIeee8021MstpTable, Ieee8021MstpRootPathCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ieee8021MstpTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpRootPort}, GetNextIndexIeee8021MstpTable, Ieee8021MstpRootPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021MstpTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpBridgePriority}, GetNextIndexIeee8021MstpTable, Ieee8021MstpBridgePriorityGet, Ieee8021MstpBridgePrioritySet, Ieee8021MstpBridgePriorityTest, Ieee8021MstpTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ieee8021MstpTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpVids0}, GetNextIndexIeee8021MstpTable, Ieee8021MstpVids0Get, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021MstpTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpVids1}, GetNextIndexIeee8021MstpTable, Ieee8021MstpVids1Get, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021MstpTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpVids2}, GetNextIndexIeee8021MstpTable, Ieee8021MstpVids2Get, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021MstpTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpVids3}, GetNextIndexIeee8021MstpTable, Ieee8021MstpVids3Get, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021MstpTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpRowStatus}, GetNextIndexIeee8021MstpTable, Ieee8021MstpRowStatusGet, Ieee8021MstpRowStatusSet, Ieee8021MstpRowStatusTest, Ieee8021MstpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021MstpTableINDEX, 2, 0, 1, NULL},

{{12,Ieee8021MstpCistPortComponentId}, GetNextIndexIeee8021MstpCistPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021MstpCistPortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpCistPortNum}, GetNextIndexIeee8021MstpCistPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021MstpCistPortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpCistPortUptime}, GetNextIndexIeee8021MstpCistPortTable, Ieee8021MstpCistPortUptimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Ieee8021MstpCistPortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpCistPortAdminPathCost}, GetNextIndexIeee8021MstpCistPortTable, Ieee8021MstpCistPortAdminPathCostGet, Ieee8021MstpCistPortAdminPathCostSet, Ieee8021MstpCistPortAdminPathCostTest, Ieee8021MstpCistPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ieee8021MstpCistPortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpCistPortDesignatedRoot}, GetNextIndexIeee8021MstpCistPortTable, Ieee8021MstpCistPortDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021MstpCistPortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpCistPortTopologyChangeAck}, GetNextIndexIeee8021MstpCistPortTable, Ieee8021MstpCistPortTopologyChangeAckGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021MstpCistPortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpCistPortHelloTime}, GetNextIndexIeee8021MstpCistPortTable, Ieee8021MstpCistPortHelloTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ieee8021MstpCistPortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpCistPortAdminEdgePort}, GetNextIndexIeee8021MstpCistPortTable, Ieee8021MstpCistPortAdminEdgePortGet, Ieee8021MstpCistPortAdminEdgePortSet, Ieee8021MstpCistPortAdminEdgePortTest, Ieee8021MstpCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021MstpCistPortTableINDEX, 2, 0, 0, "1"},

{{12,Ieee8021MstpCistPortOperEdgePort}, GetNextIndexIeee8021MstpCistPortTable, Ieee8021MstpCistPortOperEdgePortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021MstpCistPortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpCistPortMacEnabled}, GetNextIndexIeee8021MstpCistPortTable, Ieee8021MstpCistPortMacEnabledGet, Ieee8021MstpCistPortMacEnabledSet, Ieee8021MstpCistPortMacEnabledTest, Ieee8021MstpCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021MstpCistPortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpCistPortMacOperational}, GetNextIndexIeee8021MstpCistPortTable, Ieee8021MstpCistPortMacOperationalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021MstpCistPortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpCistPortRestrictedRole}, GetNextIndexIeee8021MstpCistPortTable, Ieee8021MstpCistPortRestrictedRoleGet, Ieee8021MstpCistPortRestrictedRoleSet, Ieee8021MstpCistPortRestrictedRoleTest, Ieee8021MstpCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021MstpCistPortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpCistPortRestrictedTcn}, GetNextIndexIeee8021MstpCistPortTable, Ieee8021MstpCistPortRestrictedTcnGet, Ieee8021MstpCistPortRestrictedTcnSet, Ieee8021MstpCistPortRestrictedTcnTest, Ieee8021MstpCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021MstpCistPortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpCistPortRole}, GetNextIndexIeee8021MstpCistPortTable, Ieee8021MstpCistPortRoleGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021MstpCistPortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpCistPortDisputed}, GetNextIndexIeee8021MstpCistPortTable, Ieee8021MstpCistPortDisputedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021MstpCistPortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpCistPortCistRegionalRootId}, GetNextIndexIeee8021MstpCistPortTable, Ieee8021MstpCistPortCistRegionalRootIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021MstpCistPortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpCistPortCistPathCost}, GetNextIndexIeee8021MstpCistPortTable, Ieee8021MstpCistPortCistPathCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021MstpCistPortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpCistPortProtocolMigration}, GetNextIndexIeee8021MstpCistPortTable, Ieee8021MstpCistPortProtocolMigrationGet, Ieee8021MstpCistPortProtocolMigrationSet, Ieee8021MstpCistPortProtocolMigrationTest, Ieee8021MstpCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021MstpCistPortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpCistPortEnableBPDURx}, GetNextIndexIeee8021MstpCistPortTable, Ieee8021MstpCistPortEnableBPDURxGet, Ieee8021MstpCistPortEnableBPDURxSet, Ieee8021MstpCistPortEnableBPDURxTest, Ieee8021MstpCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021MstpCistPortTableINDEX, 2, 0, 0, "2"},

{{12,Ieee8021MstpCistPortEnableBPDUTx}, GetNextIndexIeee8021MstpCistPortTable, Ieee8021MstpCistPortEnableBPDUTxGet, Ieee8021MstpCistPortEnableBPDUTxSet, Ieee8021MstpCistPortEnableBPDUTxTest, Ieee8021MstpCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021MstpCistPortTableINDEX, 2, 0, 0, "2"},

{{12,Ieee8021MstpCistPortPseudoRootId}, GetNextIndexIeee8021MstpCistPortTable, Ieee8021MstpCistPortPseudoRootIdGet, Ieee8021MstpCistPortPseudoRootIdSet, Ieee8021MstpCistPortPseudoRootIdTest, Ieee8021MstpCistPortTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Ieee8021MstpCistPortTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpCistPortIsL2Gp}, GetNextIndexIeee8021MstpCistPortTable, Ieee8021MstpCistPortIsL2GpGet, Ieee8021MstpCistPortIsL2GpSet, Ieee8021MstpCistPortIsL2GpTest, Ieee8021MstpCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021MstpCistPortTableINDEX, 2, 0, 0, "2"},

{{12,Ieee8021MstpPortComponentId}, GetNextIndexIeee8021MstpPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021MstpPortTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021MstpPortMstId}, GetNextIndexIeee8021MstpPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021MstpPortTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021MstpPortNum}, GetNextIndexIeee8021MstpPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021MstpPortTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021MstpPortUptime}, GetNextIndexIeee8021MstpPortTable, Ieee8021MstpPortUptimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Ieee8021MstpPortTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021MstpPortState}, GetNextIndexIeee8021MstpPortTable, Ieee8021MstpPortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021MstpPortTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021MstpPortPriority}, GetNextIndexIeee8021MstpPortTable, Ieee8021MstpPortPriorityGet, Ieee8021MstpPortPrioritySet, Ieee8021MstpPortPriorityTest, Ieee8021MstpPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ieee8021MstpPortTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021MstpPortPathCost}, GetNextIndexIeee8021MstpPortTable, Ieee8021MstpPortPathCostGet, Ieee8021MstpPortPathCostSet, Ieee8021MstpPortPathCostTest, Ieee8021MstpPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ieee8021MstpPortTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021MstpPortDesignatedRoot}, GetNextIndexIeee8021MstpPortTable, Ieee8021MstpPortDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021MstpPortTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021MstpPortDesignatedCost}, GetNextIndexIeee8021MstpPortTable, Ieee8021MstpPortDesignatedCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ieee8021MstpPortTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021MstpPortDesignatedBridge}, GetNextIndexIeee8021MstpPortTable, Ieee8021MstpPortDesignatedBridgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021MstpPortTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021MstpPortDesignatedPort}, GetNextIndexIeee8021MstpPortTable, Ieee8021MstpPortDesignatedPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021MstpPortTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021MstpPortRole}, GetNextIndexIeee8021MstpPortTable, Ieee8021MstpPortRoleGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021MstpPortTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021MstpPortDisputed}, GetNextIndexIeee8021MstpPortTable, Ieee8021MstpPortDisputedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021MstpPortTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021MstpFidToMstiComponentId}, GetNextIndexIeee8021MstpFidToMstiTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021MstpFidToMstiTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpFidToMstiFid}, GetNextIndexIeee8021MstpFidToMstiTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021MstpFidToMstiTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpFidToMstiMstId}, GetNextIndexIeee8021MstpFidToMstiTable, Ieee8021MstpFidToMstiMstIdGet, Ieee8021MstpFidToMstiMstIdSet, Ieee8021MstpFidToMstiMstIdTest, Ieee8021MstpFidToMstiTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021MstpFidToMstiTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpVlanComponentId}, GetNextIndexIeee8021MstpVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021MstpVlanTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpVlanId}, GetNextIndexIeee8021MstpVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021MstpVlanTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpVlanMstId}, GetNextIndexIeee8021MstpVlanTable, Ieee8021MstpVlanMstIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021MstpVlanTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021MstpConfigIdComponentId}, GetNextIndexIeee8021MstpConfigIdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021MstpConfigIdTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021MstpConfigIdFormatSelector}, GetNextIndexIeee8021MstpConfigIdTable, Ieee8021MstpConfigIdFormatSelectorGet, Ieee8021MstpConfigIdFormatSelectorSet, Ieee8021MstpConfigIdFormatSelectorTest, Ieee8021MstpConfigIdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ieee8021MstpConfigIdTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021MstpConfigurationName}, GetNextIndexIeee8021MstpConfigIdTable, Ieee8021MstpConfigurationNameGet, Ieee8021MstpConfigurationNameSet, Ieee8021MstpConfigurationNameTest, Ieee8021MstpConfigIdTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Ieee8021MstpConfigIdTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021MstpRevisionLevel}, GetNextIndexIeee8021MstpConfigIdTable, Ieee8021MstpRevisionLevelGet, Ieee8021MstpRevisionLevelSet, Ieee8021MstpRevisionLevelTest, Ieee8021MstpConfigIdTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021MstpConfigIdTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021MstpConfigurationDigest}, GetNextIndexIeee8021MstpConfigIdTable, Ieee8021MstpConfigurationDigestGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021MstpConfigIdTableINDEX, 1, 0, 0, NULL},
};
tMibData std1s1Entry = { 67, std1s1MibEntry };

#endif /* _STD1S1DB_H */

