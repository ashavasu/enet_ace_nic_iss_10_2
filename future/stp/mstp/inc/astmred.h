/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmred.h,v 1.8 2011/08/24 06:13:45 siva Exp $
 *
 * Description: This file contains Prototype definitions of functions
 *              used in redundancy support for MSTP Module.                                 * 
 *
 *******************************************************************/

#ifndef _ASTMRED_H
#define _ASTMRED_H

INT4 MstRedStorePduInActive (UINT2 , tMstBpdu *, UINT2, UINT2);

INT4 MstRedStorePduInStandby (UINT2, VOID *, UINT2);


INT4 MstRedSyncUpPdu (UINT2 , tMstBpdu* , UINT2);
INT4 MstRedHandleMstPdus (VOID *, UINT4 *, UINT2);

INT4 MstRedClearAllBpdusOnActive(VOID);

INT4 MstRedProtocolRestart (UINT2, UINT2);

INT4 MstRedClearAllSyncUpDataInInst(UINT2);                               

INT4 MstRedClearPduOnActive(UINT2);

VOID RedDumpMstPdu (VOID);

VOID RedDumpMstPduOnPort (UINT2 u2PortIfIndex);
            
INT4 MstRedClearInstanceOnActive (UINT2, UINT2);
    
#endif /* _ASTMRED_H */
