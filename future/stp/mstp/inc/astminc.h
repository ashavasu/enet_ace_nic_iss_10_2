/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astminc.h,v 1.19 2015/10/12 11:08:58 siva Exp $
 *
 * Description: This file contains header files to be included   
 *              by all source files of MSTP Module.              
 *
 *******************************************************************/



#ifndef _ASTMINC_H
#define _ASTMINC_H

#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "snmputil.h"
#include "bridge.h"
#include "fsvlan.h"
#include "msr.h"
#ifdef PBB_WANTED
#include "pbb.h"
#endif

#ifdef ICCH_WANTED
#include "icch.h"
#endif /* ICCH_WANTED */

#include "asttrc.h"
#include "srmbuf.h"
#include "rstp.h" 
#include "mstp.h"
#include "l2iwf.h"
#include "ecfm.h"
#ifdef MBSM_WANTED
#include "mbsm.h"
#endif
#include "la.h"
#include "iss.h"
#include "l2iwf.h"
#include "vcm.h"
#include "garp.h"

#include "issu.h"

#include "stpcli.h"
/* MST Related Files */

#include "astconst.h"
#include "astmcons.h"
#include "astmacr.h"
#include "astctdfs.h"
#include "astmtdfs.h"
#ifdef L2RED_WANTED 
#include "astpred.h"
#else
#include "astprdstb.h"
#endif

#include "asttdfs.h"
#include "astrtrap.h"
#include "astglob.h"
#include "astmmacr.h"
#include "astprot.h"
#include "astmprot.h"
#include "astmred.h"
#include "astmtrc.h"
#include "astdbg.h"
#include "astmlow.h"
#include "fsmpmslw.h"
#include "astmextn.h"
#include "astmtrap.h"
#include "astpbprot.h"

#ifdef NPAPI_WANTED
#include "nputil.h"
#endif

#endif /* _ASTMINC_H */

