/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: astmlow.h,v 1.22 2017/09/20 06:32:06 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/
#ifndef _ASTMLOW_H
#define _ASTMLOW_H


/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMstSystemControl ARG_LIST((INT4 *));

INT1
nmhGetFsMstModuleStatus ARG_LIST((INT4 *));

INT1
nmhGetFsMstMaxMstInstanceNumber ARG_LIST((INT4 *));

INT1
nmhGetFsMstNoOfMstiSupported ARG_LIST((INT4 *));

INT1
nmhGetFsMstMaxHopCount ARG_LIST((INT4 *));

INT1
nmhGetFsMstBrgAddress ARG_LIST((tMacAddr * ));

INT1
nmhGetFsMstCistRoot ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMstCistRegionalRoot ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMstCistRootCost ARG_LIST((INT4 *));

INT1
nmhGetFsMstCistRegionalRootCost ARG_LIST((INT4 *));

INT1
nmhGetFsMstCistRootPort ARG_LIST((INT4 *));

INT1
nmhGetFsMstCistBridgePriority ARG_LIST((INT4 *));

INT1
nmhGetFsMstCistBridgeMaxAge ARG_LIST((INT4 *));

INT1
nmhGetFsMstCistBridgeForwardDelay ARG_LIST((INT4 *));

INT1
nmhGetFsMstCistHoldTime ARG_LIST((INT4 *));

INT1
nmhGetFsMstCistMaxAge ARG_LIST((INT4 *));

INT1
nmhGetFsMstCistForwardDelay ARG_LIST((INT4 *));

INT1
nmhGetFsMstMstpUpCount ARG_LIST((UINT4 *));

INT1
nmhGetFsMstMstpDownCount ARG_LIST((UINT4 *));

INT1
nmhGetFsMstTrace ARG_LIST((INT4 *));

INT1
nmhGetFsMstDebug ARG_LIST((INT4 *));

INT1
nmhGetFsMstForceProtocolVersion ARG_LIST((INT4 *));

INT1
nmhGetFsMstTxHoldCount ARG_LIST((INT4 *));

INT1
nmhGetFsMstMstiConfigIdSel ARG_LIST((INT4 *));

INT1
nmhGetFsMstMstiRegionName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMstMstiRegionVersion ARG_LIST((INT4 *));

INT1
nmhGetFsMstMstiConfigDigest ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMstBufferOverFlowCount ARG_LIST((UINT4 *));

INT1
nmhGetFsMstMemAllocFailureCount ARG_LIST((UINT4 *));

INT1
nmhGetFsMstRegionConfigChangeCount ARG_LIST((UINT4 *));

INT1
nmhGetFsMstCistBridgeRoleSelectionSemState ARG_LIST((INT4 *));

INT1
nmhGetFsMstCistTimeSinceTopologyChange ARG_LIST((UINT4 *));

INT1
nmhGetFsMstCistTopChanges ARG_LIST((UINT4 *));

INT1
nmhGetFsMstCistNewRootBridgeCount ARG_LIST((UINT4 *));

INT1
nmhGetFsMstCistHelloTime ARG_LIST((INT4 *));

INT1
nmhGetFsMstCistBridgeHelloTime ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMstSystemControl ARG_LIST((INT4 ));

INT1
nmhSetFsMstModuleStatus ARG_LIST((INT4 ));

INT1
nmhSetFsMstMaxMstInstanceNumber ARG_LIST((INT4 ));

INT1
nmhSetFsMstMaxHopCount ARG_LIST((INT4 ));

INT1
nmhSetFsMstCistBridgePriority ARG_LIST((INT4 ));

INT1
nmhSetFsMstCistBridgeMaxAge ARG_LIST((INT4 ));

INT1
nmhSetFsMstCistBridgeForwardDelay ARG_LIST((INT4 ));

INT1
nmhSetFsMstTrace ARG_LIST((INT4 ));

INT1
nmhSetFsMstDebug ARG_LIST((INT4 ));

INT1
nmhSetFsMstForceProtocolVersion ARG_LIST((INT4 ));

INT1
nmhSetFsMstTxHoldCount ARG_LIST((INT4 ));

INT1
nmhSetFsMstMstiConfigIdSel ARG_LIST((INT4 ));

INT1
nmhSetFsMstMstiRegionName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMstMstiRegionVersion ARG_LIST((INT4 ));

INT1
nmhSetFsMstCistBridgeHelloTime ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMstSystemControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMstModuleStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMstMaxMstInstanceNumber ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMstMaxHopCount ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMstCistBridgePriority ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMstCistBridgeMaxAge ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMstCistBridgeForwardDelay ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMstTrace ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMstDebug ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMstForceProtocolVersion ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMstTxHoldCount ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMstMstiConfigIdSel ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMstMstiRegionName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMstMstiRegionVersion ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMstCistBridgeHelloTime ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMstSystemControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMstModuleStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMstMaxMstInstanceNumber ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMstMaxHopCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMstCistBridgePriority ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMstCistBridgeMaxAge ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMstCistBridgeForwardDelay ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMstPathCostDefaultType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMstTrace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMstDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMstForceProtocolVersion ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMstTxHoldCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMstMstiConfigIdSel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMstMstiRegionName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMstMstiRegionVersion ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMstCistBridgeHelloTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMstMstiBridgeTable. */
INT1
nmhValidateIndexInstanceFsMstMstiBridgeTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMstMstiBridgeTable  */

INT1
nmhGetFirstIndexFsMstMstiBridgeTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMstMstiBridgeTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMstMstiBridgeRegionalRoot ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMstMstiBridgePriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstMstiRootCost ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstMstiRootPort ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstMstiTimeSinceTopologyChange ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstMstiTopChanges ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstMstiNewRootBridgeCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstMstiBridgeRoleSelectionSemState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstInstanceUpCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstInstanceDownCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstOldDesignatedRoot ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMstMstiBridgePriority ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMstMstiBridgePriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMstMstiBridgeTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMstVlanInstanceMappingTable. */
INT1
nmhValidateIndexInstanceFsMstVlanInstanceMappingTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMstVlanInstanceMappingTable  */

INT1
nmhGetFirstIndexFsMstVlanInstanceMappingTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMstVlanInstanceMappingTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMstMapVlanIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstUnMapVlanIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstSetVlanList ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMstResetVlanList ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMstInstanceVlanMapped ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMstInstanceVlanMapped2k ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMstInstanceVlanMapped3k ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMstInstanceVlanMapped4k ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMstMapVlanIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMstUnMapVlanIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMstSetVlanList ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMstResetVlanList ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMstMapVlanIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT4
MstTestMstMapVlanIndex (UINT4 *pu4ErrorCode,
                        INT4 i4FsMstInstanceIndex,
                        INT4 i4TestValFsMstMapVlanIndex);
extern INT4
MstSetMstMapVlanIndex (INT4 i4FsMstInstanceIndex,
                       INT4 i4SetValFsMstMapVlanIndex);

extern INT4
MstTestMstUnMapVlanIndex (UINT4 *pu4ErrorCode,
                        INT4 i4FsMstInstanceIndex,
                        INT4 i4TestValFsMstMapVlanIndex);
extern INT4
MstSetMstUnMapVlanIndex (INT4 i4FsMstInstanceIndex,
                       INT4 i4SetValFsMstMapVlanIndex);

INT1
nmhTestv2FsMstUnMapVlanIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMstSetVlanList ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMstResetVlanList ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMstVlanInstanceMappingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMstCistPortTable. */
INT1
nmhValidateIndexInstanceFsMstCistPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMstCistPortTable  */

INT1
nmhGetFirstIndexFsMstCistPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMstCistPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMstCistPortPathCost ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortDesignatedRoot ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMstCistPortDesignatedBridge ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMstCistPortDesignatedPort ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMstCistPortAdminP2P ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortOperP2P ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortAdminEdgeStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortOperEdgeStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortProtocolMigration ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistForcePortState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortForwardTransitions ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortRxMstBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortRxRstBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortRxConfigBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortRxTcnBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortTxMstBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortTxRstBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortTxConfigBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortTxTcnBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortInvalidMstBpduRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortInvalidRstBpduRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortInvalidConfigBpduRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortInvalidTcnBpduRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortTransmitSemState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortReceiveSemState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortProtMigrationSemState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistProtocolMigrationCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortDesignatedCost ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortRegionalRoot ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMstCistPortRegionalPathCost ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistSelectedPortRole ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistCurrentPortRole ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortInfoSemState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortRoleTransitionSemState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortStateTransitionSemState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortTopologyChangeSemState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortHelloTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortOperVersion ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortEffectivePortState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortAutoEdgeStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortRestrictedRole ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortRestrictedTCN ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortAdminPathCost ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortEnableBPDURx ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortEnableBPDUTx ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortPseudoRootId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMstCistPortIsL2Gp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortLoopGuard ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortRcvdEvent ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortRcvdEventSubType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortRcvdEventTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortBpduGuard ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortErrorRecovery ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstPortBpduInconsistentState ARG_LIST((INT4 ,INT4 *));

INT1 
nmhTestv2FsMstPortBpduGuardAction(UINT4 *pu4ErrorCode , 
          INT4 i4FsMstCistPort , INT4 i4TestValFsMstPortBpduGuardAction);

INT1 
nmhSetFsMstPortBpduGuardAction(INT4 i4FsMstCistPort , INT4 i4SetValFsMstPortBpduGuardAction);

INT1 
nmhGetFsMstPortBpduGuardAction(INT4 i4FsMstCistPort , INT4 *pi4RetValFsMstPortBpduGuardAction);

INT1
nmhGetFsMstCistPortTCDetectedCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortTCReceivedCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortTCDetectedTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortTCReceivedTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortRcvInfoWhileExpCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortRcvInfoWhileExpTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortProposalPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortProposalPktsRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortProposalPktSentTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortProposalPktRcvdTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortAgreementPktSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortAgreementPktRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortAgreementPktSentTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortAgreementPktRcvdTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortImpStateOccurCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortImpStateOccurTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMstCistPortOldPortState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortLoopInconsistentState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortRootGuard ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstCistPortRootInconsistentState ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMstCistPortPathCost ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMstCistPortPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMstCistPortAdminP2P ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMstCistPortAdminEdgeStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMstCistPortProtocolMigration ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMstCistForcePortState ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMstCistPortHelloTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMstCistPortAutoEdgeStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMstCistPortRestrictedRole ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMstCistPortRestrictedTCN ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMstCistPortAdminPathCost ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMstCistPortEnableBPDURx ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMstCistPortEnableBPDUTx ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMstCistPortPseudoRootId ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMstCistPortIsL2Gp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMstCistPortLoopGuard ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMstCistPortBpduGuard ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMstCistPortErrorRecovery ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMstCistPortRootGuard ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMstCistPortPathCost ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMstCistPortPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMstCistPortAdminP2P ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMstCistPortAdminEdgeStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMstCistPortProtocolMigration ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMstCistForcePortState ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMstCistPortHelloTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMstCistPortAutoEdgeStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMstCistPortRestrictedRole ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMstCistPortRestrictedTCN ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMstCistPortAdminPathCost ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMstCistPortEnableBPDURx ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMstCistPortEnableBPDUTx ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMstCistPortPseudoRootId ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMstCistPortIsL2Gp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMstCistPortLoopGuard ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMstCistPortBpduGuard ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMstCistPortErrorRecovery ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMstCistPortRootGuard ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMstCistPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMstMstiPortTable. */
INT1
nmhValidateIndexInstanceFsMstMstiPortTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMstMstiPortTable  */

INT1
nmhGetFirstIndexFsMstMstiPortTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMstMstiPortTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMstMstiPortPathCost ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMstMstiPortPriority ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMstMstiPortDesignatedRoot ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMstMstiPortDesignatedBridge ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMstMstiPortDesignatedPort ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMstMstiPortState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMstMstiForcePortState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMstMstiPortForwardTransitions ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMstMstiPortReceivedBPDUs ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMstMstiPortTransmittedBPDUs ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMstMstiPortInvalidBPDUsRcvd ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMstMstiPortDesignatedCost ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMstMstiSelectedPortRole ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMstMstiCurrentPortRole ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMstMstiPortInfoSemState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMstMstiPortRoleTransitionSemState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMstMstiPortStateTransitionSemState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMstMstiPortTopologyChangeSemState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMstMstiPortEffectivePortState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMstMstiPortAdminPathCost ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMstMstiPortPseudoRootId ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMstMstiPortStateChangeTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMstMstiPortRootInconsistentState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMstMstiPortOldPortState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMstMstiPortLoopInconsistentState ARG_LIST((INT4  , INT4 ,INT4 *));


INT1
nmhGetFsMstMstiPortTCDetectedCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMstMstiPortTCReceivedCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMstMstiPortTCDetectedTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMstMstiPortTCReceivedTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMstMstiPortProposalPktsSent ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMstMstiPortProposalPktsRcvd ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMstMstiPortProposalPktSentTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMstMstiPortProposalPktRcvdTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMstMstiPortAgreementPktSent ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMstMstiPortAgreementPktRcvd ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMstMstiPortAgreementPktSentTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMstMstiPortAgreementPktRcvdTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMstMstiPortPathCost ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMstMstiPortPriority ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMstMstiForcePortState ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMstMstiPortAdminPathCost ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMstMstiPortPseudoRootId ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMstMstiPortPathCost ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMstMstiPortPriority ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMstMstiForcePortState ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMstMstiPortAdminPathCost ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMstMstiPortPseudoRootId ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMstMstiPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMstCistDynamicPathcostCalculation ARG_LIST((INT4 *));

INT1
nmhGetFsMstCalcPortPathCostOnSpeedChg ARG_LIST((INT4 *));

INT1
nmhGetFsMstRcvdEvent ARG_LIST((INT4 *));

INT1
nmhGetFsMstRcvdEventSubType ARG_LIST((INT4 *));

INT1
nmhGetFsMstRcvdEventTimeStamp ARG_LIST((UINT4 *));

INT1
nmhGetFsMstPortStateChangeTimeStamp ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMstCistDynamicPathcostCalculation ARG_LIST((INT4 ));

INT1
nmhSetFsMstCalcPortPathCostOnSpeedChg ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMstCistDynamicPathcostCalculation ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMstCalcPortPathCostOnSpeedChg ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMstCistDynamicPathcostCalculation ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMstCalcPortPathCostOnSpeedChg ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMstPortExtTable. */
INT1
nmhValidateIndexInstanceFsMstPortExtTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMstPortExtTable  */

INT1
nmhGetFirstIndexFsMstPortExtTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMstPortExtTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMstPortRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMstPortRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMstPortRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMstPortExtTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMstSetTraps ARG_LIST((INT4 *));

INT1
nmhGetFsMstGenTrapType ARG_LIST((INT4 *));

INT1
nmhGetFsMstErrTrapType ARG_LIST((INT4 *));

INT1
nmhGetFsMstTrapMstiInstance ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMstSetTraps ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMstSetTraps ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMstSetTraps ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMstPortTrapNotificationTable. */
INT1
nmhValidateIndexInstanceFsMstPortTrapNotificationTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMstPortTrapNotificationTable  */

INT1
nmhGetFirstIndexFsMstPortTrapNotificationTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMstPortTrapNotificationTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMstPortMigrationType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstPktErrType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMstPktErrVal ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for FsMstPortRoleTrapNotificationTable. */
INT1
nmhValidateIndexInstanceFsMstPortRoleTrapNotificationTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMstPortRoleTrapNotificationTable  */

INT1
nmhGetFirstIndexFsMstPortRoleTrapNotificationTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMstPortRoleTrapNotificationTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMstPortRoleType ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMstOldPortRoleType ARG_LIST((INT4  , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMstBpduGuard ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMstBpduGuard ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMstBpduGuard ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMstBpduGuard ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhGetFsMstStpPerfStatus ARG_LIST((INT4 *));

INT1
nmhSetFsMstStpPerfStatus ARG_LIST((INT4 ));

INT1
nmhTestv2FsMstStpPerfStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhDepv2FsMstStpPerfStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhGetFsMstInstPortsMap ARG_LIST((INT4 *));

INT1
nmhSetFsMstInstPortsMap ARG_LIST((INT4 ));

INT1
nmhTestv2FsMstInstPortsMap ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhDepv2FsMstInstPortsMap ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

#endif /*_ASTMLOW_H  */
