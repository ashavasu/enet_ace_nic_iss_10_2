/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsmstdb.h,v 1.23 2017/09/20 06:32:07 siva Exp $
 *
 * Description: Protocol Mib Data base
 * *********************************************************************/
#ifndef _FSMSTDB_H
#define _FSMSTDB_H

UINT1 FsMstMstiBridgeTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMstVlanInstanceMappingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMstCistPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMstMstiPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMstPortExtTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMstPortTrapNotificationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMstPortRoleTrapNotificationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fsmst [] ={1,3,6,1,4,1,2076,80};
tSNMP_OID_TYPE fsmstOID = {8, fsmst};


UINT4 FsMstSystemControl [ ] ={1,3,6,1,4,1,2076,80,1,1};
UINT4 FsMstModuleStatus [ ] ={1,3,6,1,4,1,2076,80,1,2};
UINT4 FsMstMaxMstInstanceNumber [ ] ={1,3,6,1,4,1,2076,80,1,3};
UINT4 FsMstNoOfMstiSupported [ ] ={1,3,6,1,4,1,2076,80,1,4};
UINT4 FsMstMaxHopCount [ ] ={1,3,6,1,4,1,2076,80,1,5};
UINT4 FsMstBrgAddress [ ] ={1,3,6,1,4,1,2076,80,1,6};
UINT4 FsMstCistRoot [ ] ={1,3,6,1,4,1,2076,80,1,7};
UINT4 FsMstCistRegionalRoot [ ] ={1,3,6,1,4,1,2076,80,1,8};
UINT4 FsMstCistRootCost [ ] ={1,3,6,1,4,1,2076,80,1,9};
UINT4 FsMstCistRegionalRootCost [ ] ={1,3,6,1,4,1,2076,80,1,10};
UINT4 FsMstCistRootPort [ ] ={1,3,6,1,4,1,2076,80,1,11};
UINT4 FsMstCistBridgePriority [ ] ={1,3,6,1,4,1,2076,80,1,12};
UINT4 FsMstCistBridgeMaxAge [ ] ={1,3,6,1,4,1,2076,80,1,13};
UINT4 FsMstCistBridgeForwardDelay [ ] ={1,3,6,1,4,1,2076,80,1,14};
UINT4 FsMstCistHoldTime [ ] ={1,3,6,1,4,1,2076,80,1,15};
UINT4 FsMstCistMaxAge [ ] ={1,3,6,1,4,1,2076,80,1,16};
UINT4 FsMstCistForwardDelay [ ] ={1,3,6,1,4,1,2076,80,1,17};
UINT4 FsMstMstpUpCount [ ] ={1,3,6,1,4,1,2076,80,1,18};
UINT4 FsMstMstpDownCount [ ] ={1,3,6,1,4,1,2076,80,1,19};
UINT4 FsMstPathCostDefaultType [ ] ={1,3,6,1,4,1,2076,80,1,20};
UINT4 FsMstTrace [ ] ={1,3,6,1,4,1,2076,80,1,21};
UINT4 FsMstDebug [ ] ={1,3,6,1,4,1,2076,80,1,22};
UINT4 FsMstForceProtocolVersion [ ] ={1,3,6,1,4,1,2076,80,1,23};
UINT4 FsMstTxHoldCount [ ] ={1,3,6,1,4,1,2076,80,1,24};
UINT4 FsMstMstiConfigIdSel [ ] ={1,3,6,1,4,1,2076,80,1,25};
UINT4 FsMstMstiRegionName [ ] ={1,3,6,1,4,1,2076,80,1,26};
UINT4 FsMstMstiRegionVersion [ ] ={1,3,6,1,4,1,2076,80,1,27};
UINT4 FsMstMstiConfigDigest [ ] ={1,3,6,1,4,1,2076,80,1,28};
UINT4 FsMstBufferOverFlowCount [ ] ={1,3,6,1,4,1,2076,80,1,29};
UINT4 FsMstMemAllocFailureCount [ ] ={1,3,6,1,4,1,2076,80,1,30};
UINT4 FsMstRegionConfigChangeCount [ ] ={1,3,6,1,4,1,2076,80,1,31};
UINT4 FsMstCistBridgeRoleSelectionSemState [ ] ={1,3,6,1,4,1,2076,80,1,32};
UINT4 FsMstCistTimeSinceTopologyChange [ ] ={1,3,6,1,4,1,2076,80,1,33};
UINT4 FsMstCistTopChanges [ ] ={1,3,6,1,4,1,2076,80,1,34};
UINT4 FsMstCistNewRootBridgeCount [ ] ={1,3,6,1,4,1,2076,80,1,35};
UINT4 FsMstCistHelloTime [ ] ={1,3,6,1,4,1,2076,80,1,36};
UINT4 FsMstCistBridgeHelloTime [ ] ={1,3,6,1,4,1,2076,80,1,37};
UINT4 FsMstMstiInstanceIndex [ ] ={1,3,6,1,4,1,2076,80,1,38,1,1};
UINT4 FsMstMstiBridgeRegionalRoot [ ] ={1,3,6,1,4,1,2076,80,1,38,1,2};
UINT4 FsMstMstiBridgePriority [ ] ={1,3,6,1,4,1,2076,80,1,38,1,3};
UINT4 FsMstMstiRootCost [ ] ={1,3,6,1,4,1,2076,80,1,38,1,4};
UINT4 FsMstMstiRootPort [ ] ={1,3,6,1,4,1,2076,80,1,38,1,5};
UINT4 FsMstMstiTimeSinceTopologyChange [ ] ={1,3,6,1,4,1,2076,80,1,38,1,6};
UINT4 FsMstMstiTopChanges [ ] ={1,3,6,1,4,1,2076,80,1,38,1,7};
UINT4 FsMstMstiNewRootBridgeCount [ ] ={1,3,6,1,4,1,2076,80,1,38,1,8};
UINT4 FsMstMstiBridgeRoleSelectionSemState [ ] ={1,3,6,1,4,1,2076,80,1,38,1,9};
UINT4 FsMstInstanceUpCount [ ] ={1,3,6,1,4,1,2076,80,1,38,1,10};
UINT4 FsMstInstanceDownCount [ ] ={1,3,6,1,4,1,2076,80,1,38,1,11};
UINT4 FsMstOldDesignatedRoot [ ] ={1,3,6,1,4,1,2076,80,1,38,1,12};
UINT4 FsMstInstanceIndex [ ] ={1,3,6,1,4,1,2076,80,1,39,1,1};
UINT4 FsMstMapVlanIndex [ ] ={1,3,6,1,4,1,2076,80,1,39,1,2};
UINT4 FsMstUnMapVlanIndex [ ] ={1,3,6,1,4,1,2076,80,1,39,1,3};
UINT4 FsMstSetVlanList [ ] ={1,3,6,1,4,1,2076,80,1,39,1,4};
UINT4 FsMstResetVlanList [ ] ={1,3,6,1,4,1,2076,80,1,39,1,5};
UINT4 FsMstInstanceVlanMapped [ ] ={1,3,6,1,4,1,2076,80,1,39,1,6};
UINT4 FsMstInstanceVlanMapped2k [ ] ={1,3,6,1,4,1,2076,80,1,39,1,7};
UINT4 FsMstInstanceVlanMapped3k [ ] ={1,3,6,1,4,1,2076,80,1,39,1,8};
UINT4 FsMstInstanceVlanMapped4k [ ] ={1,3,6,1,4,1,2076,80,1,39,1,9};
UINT4 FsMstCistPort [ ] ={1,3,6,1,4,1,2076,80,1,40,1,1};
UINT4 FsMstCistPortPathCost [ ] ={1,3,6,1,4,1,2076,80,1,40,1,2};
UINT4 FsMstCistPortPriority [ ] ={1,3,6,1,4,1,2076,80,1,40,1,3};
UINT4 FsMstCistPortDesignatedRoot [ ] ={1,3,6,1,4,1,2076,80,1,40,1,4};
UINT4 FsMstCistPortDesignatedBridge [ ] ={1,3,6,1,4,1,2076,80,1,40,1,5};
UINT4 FsMstCistPortDesignatedPort [ ] ={1,3,6,1,4,1,2076,80,1,40,1,6};
UINT4 FsMstCistPortAdminP2P [ ] ={1,3,6,1,4,1,2076,80,1,40,1,7};
UINT4 FsMstCistPortOperP2P [ ] ={1,3,6,1,4,1,2076,80,1,40,1,8};
UINT4 FsMstCistPortAdminEdgeStatus [ ] ={1,3,6,1,4,1,2076,80,1,40,1,9};
UINT4 FsMstCistPortOperEdgeStatus [ ] ={1,3,6,1,4,1,2076,80,1,40,1,10};
UINT4 FsMstCistPortProtocolMigration [ ] ={1,3,6,1,4,1,2076,80,1,40,1,11};
UINT4 FsMstCistPortState [ ] ={1,3,6,1,4,1,2076,80,1,40,1,12};
UINT4 FsMstCistForcePortState [ ] ={1,3,6,1,4,1,2076,80,1,40,1,13};
UINT4 FsMstCistPortForwardTransitions [ ] ={1,3,6,1,4,1,2076,80,1,40,1,14};
UINT4 FsMstCistPortRxMstBpduCount [ ] ={1,3,6,1,4,1,2076,80,1,40,1,15};
UINT4 FsMstCistPortRxRstBpduCount [ ] ={1,3,6,1,4,1,2076,80,1,40,1,16};
UINT4 FsMstCistPortRxConfigBpduCount [ ] ={1,3,6,1,4,1,2076,80,1,40,1,17};
UINT4 FsMstCistPortRxTcnBpduCount [ ] ={1,3,6,1,4,1,2076,80,1,40,1,18};
UINT4 FsMstCistPortTxMstBpduCount [ ] ={1,3,6,1,4,1,2076,80,1,40,1,19};
UINT4 FsMstCistPortTxRstBpduCount [ ] ={1,3,6,1,4,1,2076,80,1,40,1,20};
UINT4 FsMstCistPortTxConfigBpduCount [ ] ={1,3,6,1,4,1,2076,80,1,40,1,21};
UINT4 FsMstCistPortTxTcnBpduCount [ ] ={1,3,6,1,4,1,2076,80,1,40,1,22};
UINT4 FsMstCistPortInvalidMstBpduRxCount [ ] ={1,3,6,1,4,1,2076,80,1,40,1,23};
UINT4 FsMstCistPortInvalidRstBpduRxCount [ ] ={1,3,6,1,4,1,2076,80,1,40,1,24};
UINT4 FsMstCistPortInvalidConfigBpduRxCount [ ] ={1,3,6,1,4,1,2076,80,1,40,1,25};
UINT4 FsMstCistPortInvalidTcnBpduRxCount [ ] ={1,3,6,1,4,1,2076,80,1,40,1,26};
UINT4 FsMstCistPortTransmitSemState [ ] ={1,3,6,1,4,1,2076,80,1,40,1,27};
UINT4 FsMstCistPortReceiveSemState [ ] ={1,3,6,1,4,1,2076,80,1,40,1,28};
UINT4 FsMstCistPortProtMigrationSemState [ ] ={1,3,6,1,4,1,2076,80,1,40,1,29};
UINT4 FsMstCistProtocolMigrationCount [ ] ={1,3,6,1,4,1,2076,80,1,40,1,30};
UINT4 FsMstCistPortDesignatedCost [ ] ={1,3,6,1,4,1,2076,80,1,40,1,31};
UINT4 FsMstCistPortRegionalRoot [ ] ={1,3,6,1,4,1,2076,80,1,40,1,32};
UINT4 FsMstCistPortRegionalPathCost [ ] ={1,3,6,1,4,1,2076,80,1,40,1,33};
UINT4 FsMstCistSelectedPortRole [ ] ={1,3,6,1,4,1,2076,80,1,40,1,34};
UINT4 FsMstCistCurrentPortRole [ ] ={1,3,6,1,4,1,2076,80,1,40,1,35};
UINT4 FsMstCistPortInfoSemState [ ] ={1,3,6,1,4,1,2076,80,1,40,1,36};
UINT4 FsMstCistPortRoleTransitionSemState [ ] ={1,3,6,1,4,1,2076,80,1,40,1,37};
UINT4 FsMstCistPortStateTransitionSemState [ ] ={1,3,6,1,4,1,2076,80,1,40,1,38};
UINT4 FsMstCistPortTopologyChangeSemState [ ] ={1,3,6,1,4,1,2076,80,1,40,1,39};
UINT4 FsMstCistPortHelloTime [ ] ={1,3,6,1,4,1,2076,80,1,40,1,40};
UINT4 FsMstCistPortOperVersion [ ] ={1,3,6,1,4,1,2076,80,1,40,1,41};
UINT4 FsMstCistPortEffectivePortState [ ] ={1,3,6,1,4,1,2076,80,1,40,1,42};
UINT4 FsMstCistPortAutoEdgeStatus [ ] ={1,3,6,1,4,1,2076,80,1,40,1,43};
UINT4 FsMstCistPortRestrictedRole [ ] ={1,3,6,1,4,1,2076,80,1,40,1,44};
UINT4 FsMstCistPortRestrictedTCN [ ] ={1,3,6,1,4,1,2076,80,1,40,1,45};
UINT4 FsMstCistPortAdminPathCost [ ] ={1,3,6,1,4,1,2076,80,1,40,1,46};
UINT4 FsMstCistPortEnableBPDURx [ ] ={1,3,6,1,4,1,2076,80,1,40,1,47};
UINT4 FsMstCistPortEnableBPDUTx [ ] ={1,3,6,1,4,1,2076,80,1,40,1,48};
UINT4 FsMstCistPortPseudoRootId [ ] ={1,3,6,1,4,1,2076,80,1,40,1,49};
UINT4 FsMstCistPortIsL2Gp [ ] ={1,3,6,1,4,1,2076,80,1,40,1,50};
UINT4 FsMstCistPortLoopGuard [ ] ={1,3,6,1,4,1,2076,80,1,40,1,51};
UINT4 FsMstCistPortRcvdEvent [ ] ={1,3,6,1,4,1,2076,80,1,40,1,52};
UINT4 FsMstCistPortRcvdEventSubType [ ] ={1,3,6,1,4,1,2076,80,1,40,1,53};
UINT4 FsMstCistPortRcvdEventTimeStamp [ ] ={1,3,6,1,4,1,2076,80,1,40,1,54};
UINT4 FsMstCistPortBpduGuard [ ] ={1,3,6,1,4,1,2076,80,1,40,1,55};
UINT4 FsMstCistPortRootGuard [ ] ={1,3,6,1,4,1,2076,80,1,40,1,56};
UINT4 FsMstCistPortRootInconsistentState [ ] ={1,3,6,1,4,1,2076,80,1,40,1,57};
UINT4 FsMstCistPortErrorRecovery [ ] ={1,3,6,1,4,1,2076,80,1,40,1,58};
UINT4 FsMstPortBpduInconsistentState [ ] ={1,3,6,1,4,1,2076,80,1,40,1,59};
UINT4 FsMstPortBpduGuardAction [ ] ={1,3,6,1,4,1,2076,80,1,40,1,60};
UINT4 FsMstCistPortTCDetectedCount [ ] ={1,3,6,1,4,1,2076,80,1,40,1,61};
UINT4 FsMstCistPortTCReceivedCount [ ] ={1,3,6,1,4,1,2076,80,1,40,1,62};
UINT4 FsMstCistPortTCDetectedTimeStamp [ ] ={1,3,6,1,4,1,2076,80,1,40,1,63};
UINT4 FsMstCistPortTCReceivedTimeStamp [ ] ={1,3,6,1,4,1,2076,80,1,40,1,64};
UINT4 FsMstCistPortRcvInfoWhileExpCount [ ] ={1,3,6,1,4,1,2076,80,1,40,1,65};
UINT4 FsMstCistPortRcvInfoWhileExpTimeStamp [ ] ={1,3,6,1,4,1,2076,80,1,40,1,66};
UINT4 FsMstCistPortProposalPktsSent [ ] ={1,3,6,1,4,1,2076,80,1,40,1,67};
UINT4 FsMstCistPortProposalPktsRcvd [ ] ={1,3,6,1,4,1,2076,80,1,40,1,68};
UINT4 FsMstCistPortProposalPktSentTimeStamp [ ] ={1,3,6,1,4,1,2076,80,1,40,1,69};
UINT4 FsMstCistPortProposalPktRcvdTimeStamp [ ] ={1,3,6,1,4,1,2076,80,1,40,1,70};
UINT4 FsMstCistPortAgreementPktSent [ ] ={1,3,6,1,4,1,2076,80,1,40,1,71};
UINT4 FsMstCistPortAgreementPktRcvd [ ] ={1,3,6,1,4,1,2076,80,1,40,1,72};
UINT4 FsMstCistPortAgreementPktSentTimeStamp [ ] ={1,3,6,1,4,1,2076,80,1,40,1,73};
UINT4 FsMstCistPortAgreementPktRcvdTimeStamp [ ] ={1,3,6,1,4,1,2076,80,1,40,1,74};
UINT4 FsMstCistPortImpStateOccurCount [ ] ={1,3,6,1,4,1,2076,80,1,40,1,75};
UINT4 FsMstCistPortImpStateOccurTimeStamp [ ] ={1,3,6,1,4,1,2076,80,1,40,1,76};
UINT4 FsMstCistPortOldPortState [ ] ={1,3,6,1,4,1,2076,80,1,40,1,77};
UINT4 FsMstCistPortLoopInconsistentState [ ] ={1,3,6,1,4,1,2076,80,1,40,1,78};
UINT4 FsMstMstiPort [ ] ={1,3,6,1,4,1,2076,80,1,41,1,1};
UINT4 FsMstMstiPortPathCost [ ] ={1,3,6,1,4,1,2076,80,1,41,1,2};
UINT4 FsMstMstiPortPriority [ ] ={1,3,6,1,4,1,2076,80,1,41,1,3};
UINT4 FsMstMstiPortDesignatedRoot [ ] ={1,3,6,1,4,1,2076,80,1,41,1,4};
UINT4 FsMstMstiPortDesignatedBridge [ ] ={1,3,6,1,4,1,2076,80,1,41,1,5};
UINT4 FsMstMstiPortDesignatedPort [ ] ={1,3,6,1,4,1,2076,80,1,41,1,6};
UINT4 FsMstMstiPortState [ ] ={1,3,6,1,4,1,2076,80,1,41,1,7};
UINT4 FsMstMstiForcePortState [ ] ={1,3,6,1,4,1,2076,80,1,41,1,8};
UINT4 FsMstMstiPortForwardTransitions [ ] ={1,3,6,1,4,1,2076,80,1,41,1,9};
UINT4 FsMstMstiPortReceivedBPDUs [ ] ={1,3,6,1,4,1,2076,80,1,41,1,10};
UINT4 FsMstMstiPortTransmittedBPDUs [ ] ={1,3,6,1,4,1,2076,80,1,41,1,11};
UINT4 FsMstMstiPortInvalidBPDUsRcvd [ ] ={1,3,6,1,4,1,2076,80,1,41,1,12};
UINT4 FsMstMstiPortDesignatedCost [ ] ={1,3,6,1,4,1,2076,80,1,41,1,13};
UINT4 FsMstMstiSelectedPortRole [ ] ={1,3,6,1,4,1,2076,80,1,41,1,14};
UINT4 FsMstMstiCurrentPortRole [ ] ={1,3,6,1,4,1,2076,80,1,41,1,15};
UINT4 FsMstMstiPortInfoSemState [ ] ={1,3,6,1,4,1,2076,80,1,41,1,16};
UINT4 FsMstMstiPortRoleTransitionSemState [ ] ={1,3,6,1,4,1,2076,80,1,41,1,17};
UINT4 FsMstMstiPortStateTransitionSemState [ ] ={1,3,6,1,4,1,2076,80,1,41,1,18};
UINT4 FsMstMstiPortTopologyChangeSemState [ ] ={1,3,6,1,4,1,2076,80,1,41,1,19};
UINT4 FsMstMstiPortEffectivePortState [ ] ={1,3,6,1,4,1,2076,80,1,41,1,20};
UINT4 FsMstMstiPortAdminPathCost [ ] ={1,3,6,1,4,1,2076,80,1,41,1,21};
UINT4 FsMstMstiPortPseudoRootId [ ] ={1,3,6,1,4,1,2076,80,1,41,1,22};
UINT4 FsMstMstiPortStateChangeTimeStamp [ ] ={1,3,6,1,4,1,2076,80,1,41,1,23};
UINT4 FsMstMstiPortRootInconsistentState [ ] ={1,3,6,1,4,1,2076,80,1,41,1,24};
UINT4 FsMstMstiPortOldPortState [ ] ={1,3,6,1,4,1,2076,80,1,41,1,25};
UINT4 FsMstMstiPortLoopInconsistentState [ ] ={1,3,6,1,4,1,2076,80,1,41,1,26};
UINT4 FsMstMstiPortTCDetectedCount [ ] ={1,3,6,1,4,1,2076,80,1,41,1,27};
UINT4 FsMstMstiPortTCReceivedCount [ ] ={1,3,6,1,4,1,2076,80,1,41,1,28};
UINT4 FsMstMstiPortTCDetectedTimeStamp [ ] ={1,3,6,1,4,1,2076,80,1,41,1,29};
UINT4 FsMstMstiPortTCReceivedTimeStamp [ ] ={1,3,6,1,4,1,2076,80,1,41,1,30};
UINT4 FsMstMstiPortProposalPktsSent [ ] ={1,3,6,1,4,1,2076,80,1,41,1,31};
UINT4 FsMstMstiPortProposalPktsRcvd [ ] ={1,3,6,1,4,1,2076,80,1,41,1,32};
UINT4 FsMstMstiPortProposalPktSentTimeStamp [ ] ={1,3,6,1,4,1,2076,80,1,41,1,33};
UINT4 FsMstMstiPortProposalPktRcvdTimeStamp [ ] ={1,3,6,1,4,1,2076,80,1,41,1,34};
UINT4 FsMstMstiPortAgreementPktSent [ ] ={1,3,6,1,4,1,2076,80,1,41,1,35};
UINT4 FsMstMstiPortAgreementPktRcvd [ ] ={1,3,6,1,4,1,2076,80,1,41,1,36};
UINT4 FsMstMstiPortAgreementPktSentTimeStamp [ ] ={1,3,6,1,4,1,2076,80,1,41,1,37};
UINT4 FsMstMstiPortAgreementPktRcvdTimeStamp [ ] ={1,3,6,1,4,1,2076,80,1,41,1,38};
UINT4 FsMstCistDynamicPathcostCalculation [ ] ={1,3,6,1,4,1,2076,80,1,42};
UINT4 FsMstCalcPortPathCostOnSpeedChg [ ] ={1,3,6,1,4,1,2076,80,1,43};
UINT4 FsMstRcvdEvent [ ] ={1,3,6,1,4,1,2076,80,1,44};
UINT4 FsMstRcvdEventSubType [ ] ={1,3,6,1,4,1,2076,80,1,45};
UINT4 FsMstRcvdEventTimeStamp [ ] ={1,3,6,1,4,1,2076,80,1,46};
UINT4 FsMstPortStateChangeTimeStamp [ ] ={1,3,6,1,4,1,2076,80,1,47};
UINT4 FsMstPort [ ] ={1,3,6,1,4,1,2076,80,1,48,1,1};
UINT4 FsMstPortRowStatus [ ] ={1,3,6,1,4,1,2076,80,1,48,1,2};
UINT4 FsMstBpduGuard [ ] ={1,3,6,1,4,1,2076,80,1,49};
UINT4 FsMstStpPerfStatus [ ] ={1,3,6,1,4,1,2076,80,1,50};
UINT4 FsMstInstPortsMap [ ] ={1,3,6,1,4,1,2076,80,1,51};
UINT4 FsMstSetTraps [ ] ={1,3,6,1,4,1,2076,80,2,1};
UINT4 FsMstGenTrapType [ ] ={1,3,6,1,4,1,2076,80,2,2};
UINT4 FsMstErrTrapType [ ] ={1,3,6,1,4,1,2076,80,2,3};
UINT4 FsMstPortTrapIndex [ ] ={1,3,6,1,4,1,2076,80,2,4,1,1};
UINT4 FsMstPortMigrationType [ ] ={1,3,6,1,4,1,2076,80,2,4,1,2};
UINT4 FsMstPktErrType [ ] ={1,3,6,1,4,1,2076,80,2,4,1,3};
UINT4 FsMstPktErrVal [ ] ={1,3,6,1,4,1,2076,80,2,4,1,4};
UINT4 FsMstPortRoleType [ ] ={1,3,6,1,4,1,2076,80,2,5,1,1};
UINT4 FsMstOldRoleType [ ] ={1,3,6,1,4,1,2076,80,2,5,1,2};




tMbDbEntry fsmstMibEntry[]= {

{{10,FsMstSystemControl}, NULL, FsMstSystemControlGet, FsMstSystemControlSet, FsMstSystemControlTest, FsMstSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsMstModuleStatus}, NULL, FsMstModuleStatusGet, FsMstModuleStatusSet, FsMstModuleStatusTest, FsMstModuleStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsMstMaxMstInstanceNumber}, NULL, FsMstMaxMstInstanceNumberGet, FsMstMaxMstInstanceNumberSet, FsMstMaxMstInstanceNumberTest, FsMstMaxMstInstanceNumberDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsMstNoOfMstiSupported}, NULL, FsMstNoOfMstiSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstMaxHopCount}, NULL, FsMstMaxHopCountGet, FsMstMaxHopCountSet, FsMstMaxHopCountTest, FsMstMaxHopCountDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "2000"},

{{10,FsMstBrgAddress}, NULL, FsMstBrgAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstCistRoot}, NULL, FsMstCistRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstCistRegionalRoot}, NULL, FsMstCistRegionalRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstCistRootCost}, NULL, FsMstCistRootCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstCistRegionalRootCost}, NULL, FsMstCistRegionalRootCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstCistRootPort}, NULL, FsMstCistRootPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstCistBridgePriority}, NULL, FsMstCistBridgePriorityGet, FsMstCistBridgePrioritySet, FsMstCistBridgePriorityTest, FsMstCistBridgePriorityDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "32768"},

{{10,FsMstCistBridgeMaxAge}, NULL, FsMstCistBridgeMaxAgeGet, FsMstCistBridgeMaxAgeSet, FsMstCistBridgeMaxAgeTest, FsMstCistBridgeMaxAgeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2000"},

{{10,FsMstCistBridgeForwardDelay}, NULL, FsMstCistBridgeForwardDelayGet, FsMstCistBridgeForwardDelaySet, FsMstCistBridgeForwardDelayTest, FsMstCistBridgeForwardDelayDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1500"},

{{10,FsMstCistHoldTime}, NULL, FsMstCistHoldTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstCistMaxAge}, NULL, FsMstCistMaxAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstCistForwardDelay}, NULL, FsMstCistForwardDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstMstpUpCount}, NULL, FsMstMstpUpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstMstpDownCount}, NULL, FsMstMstpDownCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstTrace}, NULL, FsMstTraceGet, FsMstTraceSet, FsMstTraceTest, FsMstTraceDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FsMstDebug}, NULL, FsMstDebugGet, FsMstDebugSet, FsMstDebugTest, FsMstDebugDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FsMstForceProtocolVersion}, NULL, FsMstForceProtocolVersionGet, FsMstForceProtocolVersionSet, FsMstForceProtocolVersionTest, FsMstForceProtocolVersionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{10,FsMstTxHoldCount}, NULL, FsMstTxHoldCountGet, FsMstTxHoldCountSet, FsMstTxHoldCountTest, FsMstTxHoldCountDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "6"},

{{10,FsMstMstiConfigIdSel}, NULL, FsMstMstiConfigIdSelGet, FsMstMstiConfigIdSelSet, FsMstMstiConfigIdSelTest, FsMstMstiConfigIdSelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsMstMstiRegionName}, NULL, FsMstMstiRegionNameGet, FsMstMstiRegionNameSet, FsMstMstiRegionNameTest, FsMstMstiRegionNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsMstMstiRegionVersion}, NULL, FsMstMstiRegionVersionGet, FsMstMstiRegionVersionSet, FsMstMstiRegionVersionTest, FsMstMstiRegionVersionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsMstMstiConfigDigest}, NULL, FsMstMstiConfigDigestGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstBufferOverFlowCount}, NULL, FsMstBufferOverFlowCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstMemAllocFailureCount}, NULL, FsMstMemAllocFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstRegionConfigChangeCount}, NULL, FsMstRegionConfigChangeCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstCistBridgeRoleSelectionSemState}, NULL, FsMstCistBridgeRoleSelectionSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstCistTimeSinceTopologyChange}, NULL, FsMstCistTimeSinceTopologyChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstCistTopChanges}, NULL, FsMstCistTopChangesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstCistNewRootBridgeCount}, NULL, FsMstCistNewRootBridgeCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstCistHelloTime}, NULL, FsMstCistHelloTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstCistBridgeHelloTime}, NULL, FsMstCistBridgeHelloTimeGet, FsMstCistBridgeHelloTimeSet, FsMstCistBridgeHelloTimeTest, FsMstCistBridgeHelloTimeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsMstMstiInstanceIndex}, GetNextIndexFsMstMstiBridgeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMstMstiBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMstMstiBridgeRegionalRoot}, GetNextIndexFsMstMstiBridgeTable, FsMstMstiBridgeRegionalRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMstMstiBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMstMstiBridgePriority}, GetNextIndexFsMstMstiBridgeTable, FsMstMstiBridgePriorityGet, FsMstMstiBridgePrioritySet, FsMstMstiBridgePriorityTest, FsMstMstiBridgeTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMstMstiBridgeTableINDEX, 1, 0, 0, "32768"},

{{12,FsMstMstiRootCost}, GetNextIndexFsMstMstiBridgeTable, FsMstMstiRootCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMstMstiBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMstMstiRootPort}, GetNextIndexFsMstMstiBridgeTable, FsMstMstiRootPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMstMstiBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMstMstiTimeSinceTopologyChange}, GetNextIndexFsMstMstiBridgeTable, FsMstMstiTimeSinceTopologyChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMstMstiBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMstMstiTopChanges}, GetNextIndexFsMstMstiBridgeTable, FsMstMstiTopChangesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstMstiBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMstMstiNewRootBridgeCount}, GetNextIndexFsMstMstiBridgeTable, FsMstMstiNewRootBridgeCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstMstiBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMstMstiBridgeRoleSelectionSemState}, GetNextIndexFsMstMstiBridgeTable, FsMstMstiBridgeRoleSelectionSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstMstiBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMstInstanceUpCount}, GetNextIndexFsMstMstiBridgeTable, FsMstInstanceUpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstMstiBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMstInstanceDownCount}, GetNextIndexFsMstMstiBridgeTable, FsMstInstanceDownCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstMstiBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMstOldDesignatedRoot}, GetNextIndexFsMstMstiBridgeTable, FsMstOldDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMstMstiBridgeTableINDEX, 1, 0, 0, NULL},

{{12,FsMstInstanceIndex}, GetNextIndexFsMstVlanInstanceMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMstVlanInstanceMappingTableINDEX, 1, 0, 0, NULL},

{{12,FsMstMapVlanIndex}, GetNextIndexFsMstVlanInstanceMappingTable, FsMstMapVlanIndexGet, FsMstMapVlanIndexSet, FsMstMapVlanIndexTest, FsMstVlanInstanceMappingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMstVlanInstanceMappingTableINDEX, 1, 0, 0, NULL},

{{12,FsMstUnMapVlanIndex}, GetNextIndexFsMstVlanInstanceMappingTable, FsMstUnMapVlanIndexGet, FsMstUnMapVlanIndexSet, FsMstUnMapVlanIndexTest, FsMstVlanInstanceMappingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMstVlanInstanceMappingTableINDEX, 1, 0, 0, NULL},

{{12,FsMstSetVlanList}, GetNextIndexFsMstVlanInstanceMappingTable, FsMstSetVlanListGet, FsMstSetVlanListSet, FsMstSetVlanListTest, FsMstVlanInstanceMappingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMstVlanInstanceMappingTableINDEX, 1, 0, 0, NULL},

{{12,FsMstResetVlanList}, GetNextIndexFsMstVlanInstanceMappingTable, FsMstResetVlanListGet, FsMstResetVlanListSet, FsMstResetVlanListTest, FsMstVlanInstanceMappingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMstVlanInstanceMappingTableINDEX, 1, 0, 0, NULL},

{{12,FsMstInstanceVlanMapped}, GetNextIndexFsMstVlanInstanceMappingTable, FsMstInstanceVlanMappedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMstVlanInstanceMappingTableINDEX, 1, 0, 0, NULL},

{{12,FsMstInstanceVlanMapped2k}, GetNextIndexFsMstVlanInstanceMappingTable, FsMstInstanceVlanMapped2kGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMstVlanInstanceMappingTableINDEX, 1, 0, 0, NULL},

{{12,FsMstInstanceVlanMapped3k}, GetNextIndexFsMstVlanInstanceMappingTable, FsMstInstanceVlanMapped3kGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMstVlanInstanceMappingTableINDEX, 1, 0, 0, NULL},

{{12,FsMstInstanceVlanMapped4k}, GetNextIndexFsMstVlanInstanceMappingTable, FsMstInstanceVlanMapped4kGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMstVlanInstanceMappingTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPort}, GetNextIndexFsMstCistPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortPathCost}, GetNextIndexFsMstCistPortTable, FsMstCistPortPathCostGet, FsMstCistPortPathCostSet, FsMstCistPortPathCostTest, FsMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortPriority}, GetNextIndexFsMstCistPortTable, FsMstCistPortPriorityGet, FsMstCistPortPrioritySet, FsMstCistPortPriorityTest, FsMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMstCistPortTableINDEX, 1, 0, 0, "128"},

{{12,FsMstCistPortDesignatedRoot}, GetNextIndexFsMstCistPortTable, FsMstCistPortDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortDesignatedBridge}, GetNextIndexFsMstCistPortTable, FsMstCistPortDesignatedBridgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortDesignatedPort}, GetNextIndexFsMstCistPortTable, FsMstCistPortDesignatedPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortAdminP2P}, GetNextIndexFsMstCistPortTable, FsMstCistPortAdminP2PGet, FsMstCistPortAdminP2PSet, FsMstCistPortAdminP2PTest, FsMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortOperP2P}, GetNextIndexFsMstCistPortTable, FsMstCistPortOperP2PGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortAdminEdgeStatus}, GetNextIndexFsMstCistPortTable, FsMstCistPortAdminEdgeStatusGet, FsMstCistPortAdminEdgeStatusSet, FsMstCistPortAdminEdgeStatusTest, FsMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortOperEdgeStatus}, GetNextIndexFsMstCistPortTable, FsMstCistPortOperEdgeStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortProtocolMigration}, GetNextIndexFsMstCistPortTable, FsMstCistPortProtocolMigrationGet, FsMstCistPortProtocolMigrationSet, FsMstCistPortProtocolMigrationTest, FsMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortState}, GetNextIndexFsMstCistPortTable, FsMstCistPortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistForcePortState}, GetNextIndexFsMstCistPortTable, FsMstCistForcePortStateGet, FsMstCistForcePortStateSet, FsMstCistForcePortStateTest, FsMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortForwardTransitions}, GetNextIndexFsMstCistPortTable, FsMstCistPortForwardTransitionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortRxMstBpduCount}, GetNextIndexFsMstCistPortTable, FsMstCistPortRxMstBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortRxRstBpduCount}, GetNextIndexFsMstCistPortTable, FsMstCistPortRxRstBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortRxConfigBpduCount}, GetNextIndexFsMstCistPortTable, FsMstCistPortRxConfigBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortRxTcnBpduCount}, GetNextIndexFsMstCistPortTable, FsMstCistPortRxTcnBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortTxMstBpduCount}, GetNextIndexFsMstCistPortTable, FsMstCistPortTxMstBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortTxRstBpduCount}, GetNextIndexFsMstCistPortTable, FsMstCistPortTxRstBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortTxConfigBpduCount}, GetNextIndexFsMstCistPortTable, FsMstCistPortTxConfigBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortTxTcnBpduCount}, GetNextIndexFsMstCistPortTable, FsMstCistPortTxTcnBpduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortInvalidMstBpduRxCount}, GetNextIndexFsMstCistPortTable, FsMstCistPortInvalidMstBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortInvalidRstBpduRxCount}, GetNextIndexFsMstCistPortTable, FsMstCistPortInvalidRstBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortInvalidConfigBpduRxCount}, GetNextIndexFsMstCistPortTable, FsMstCistPortInvalidConfigBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortInvalidTcnBpduRxCount}, GetNextIndexFsMstCistPortTable, FsMstCistPortInvalidTcnBpduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortTransmitSemState}, GetNextIndexFsMstCistPortTable, FsMstCistPortTransmitSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortReceiveSemState}, GetNextIndexFsMstCistPortTable, FsMstCistPortReceiveSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortProtMigrationSemState}, GetNextIndexFsMstCistPortTable, FsMstCistPortProtMigrationSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistProtocolMigrationCount}, GetNextIndexFsMstCistPortTable, FsMstCistProtocolMigrationCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortDesignatedCost}, GetNextIndexFsMstCistPortTable, FsMstCistPortDesignatedCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortRegionalRoot}, GetNextIndexFsMstCistPortTable, FsMstCistPortRegionalRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortRegionalPathCost}, GetNextIndexFsMstCistPortTable, FsMstCistPortRegionalPathCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistSelectedPortRole}, GetNextIndexFsMstCistPortTable, FsMstCistSelectedPortRoleGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistCurrentPortRole}, GetNextIndexFsMstCistPortTable, FsMstCistCurrentPortRoleGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortInfoSemState}, GetNextIndexFsMstCistPortTable, FsMstCistPortInfoSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortRoleTransitionSemState}, GetNextIndexFsMstCistPortTable, FsMstCistPortRoleTransitionSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortStateTransitionSemState}, GetNextIndexFsMstCistPortTable, FsMstCistPortStateTransitionSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortTopologyChangeSemState}, GetNextIndexFsMstCistPortTable, FsMstCistPortTopologyChangeSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortHelloTime}, GetNextIndexFsMstCistPortTable, FsMstCistPortHelloTimeGet, FsMstCistPortHelloTimeSet, FsMstCistPortHelloTimeTest, FsMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortOperVersion}, GetNextIndexFsMstCistPortTable, FsMstCistPortOperVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortEffectivePortState}, GetNextIndexFsMstCistPortTable, FsMstCistPortEffectivePortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortAutoEdgeStatus}, GetNextIndexFsMstCistPortTable, FsMstCistPortAutoEdgeStatusGet, FsMstCistPortAutoEdgeStatusSet, FsMstCistPortAutoEdgeStatusTest, FsMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortRestrictedRole}, GetNextIndexFsMstCistPortTable, FsMstCistPortRestrictedRoleGet, FsMstCistPortRestrictedRoleSet, FsMstCistPortRestrictedRoleTest, FsMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortRestrictedTCN}, GetNextIndexFsMstCistPortTable, FsMstCistPortRestrictedTCNGet, FsMstCistPortRestrictedTCNSet, FsMstCistPortRestrictedTCNTest, FsMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortAdminPathCost}, GetNextIndexFsMstCistPortTable, FsMstCistPortAdminPathCostGet, FsMstCistPortAdminPathCostSet, FsMstCistPortAdminPathCostTest, FsMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortEnableBPDURx}, GetNextIndexFsMstCistPortTable, FsMstCistPortEnableBPDURxGet, FsMstCistPortEnableBPDURxSet, FsMstCistPortEnableBPDURxTest, FsMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMstCistPortTableINDEX, 1, 0, 0, "1"},

{{12,FsMstCistPortEnableBPDUTx}, GetNextIndexFsMstCistPortTable, FsMstCistPortEnableBPDUTxGet, FsMstCistPortEnableBPDUTxSet, FsMstCistPortEnableBPDUTxTest, FsMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMstCistPortTableINDEX, 1, 0, 0, "1"},

{{12,FsMstCistPortPseudoRootId}, GetNextIndexFsMstCistPortTable, FsMstCistPortPseudoRootIdGet, FsMstCistPortPseudoRootIdSet, FsMstCistPortPseudoRootIdTest, FsMstCistPortTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortIsL2Gp}, GetNextIndexFsMstCistPortTable, FsMstCistPortIsL2GpGet, FsMstCistPortIsL2GpSet, FsMstCistPortIsL2GpTest, FsMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMstCistPortTableINDEX, 1, 0, 0, "2"},

{{12,FsMstCistPortLoopGuard}, GetNextIndexFsMstCistPortTable, FsMstCistPortLoopGuardGet, FsMstCistPortLoopGuardSet, FsMstCistPortLoopGuardTest, FsMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMstCistPortTableINDEX, 1, 0, 0, "2"},

{{12,FsMstCistPortRcvdEvent}, GetNextIndexFsMstCistPortTable, FsMstCistPortRcvdEventGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortRcvdEventSubType}, GetNextIndexFsMstCistPortTable, FsMstCistPortRcvdEventSubTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortRcvdEventTimeStamp}, GetNextIndexFsMstCistPortTable, FsMstCistPortRcvdEventTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortBpduGuard}, GetNextIndexFsMstCistPortTable, FsMstCistPortBpduGuardGet, FsMstCistPortBpduGuardSet, FsMstCistPortBpduGuardTest, FsMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMstCistPortTableINDEX, 1, 0, 0, "0"},

{{12,FsMstCistPortRootGuard}, GetNextIndexFsMstCistPortTable, FsMstCistPortRootGuardGet, FsMstCistPortRootGuardSet, FsMstCistPortRootGuardTest, FsMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortRootInconsistentState}, GetNextIndexFsMstCistPortTable, FsMstCistPortRootInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, "2"},

{{12,FsMstCistPortErrorRecovery}, GetNextIndexFsMstCistPortTable, FsMstCistPortErrorRecoveryGet, FsMstCistPortErrorRecoverySet, FsMstCistPortErrorRecoveryTest, FsMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMstCistPortTableINDEX, 1, 0, 0, "30000"},

{{12,FsMstPortBpduInconsistentState}, GetNextIndexFsMstCistPortTable, FsMstPortBpduInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, "2"},

{{12,FsMstPortBpduGuardAction}, GetNextIndexFsMstCistPortTable, FsMstPortBpduGuardActionGet, FsMstPortBpduGuardActionSet, FsMstPortBpduGuardActionTest, FsMstCistPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMstCistPortTableINDEX, 1, 0, 0, "1"},

{{12,FsMstCistPortTCDetectedCount}, GetNextIndexFsMstCistPortTable, FsMstCistPortTCDetectedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortTCReceivedCount}, GetNextIndexFsMstCistPortTable, FsMstCistPortTCReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortTCDetectedTimeStamp}, GetNextIndexFsMstCistPortTable, FsMstCistPortTCDetectedTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortTCReceivedTimeStamp}, GetNextIndexFsMstCistPortTable, FsMstCistPortTCReceivedTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortRcvInfoWhileExpCount}, GetNextIndexFsMstCistPortTable, FsMstCistPortRcvInfoWhileExpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortRcvInfoWhileExpTimeStamp}, GetNextIndexFsMstCistPortTable, FsMstCistPortRcvInfoWhileExpTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortProposalPktsSent}, GetNextIndexFsMstCistPortTable, FsMstCistPortProposalPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortProposalPktsRcvd}, GetNextIndexFsMstCistPortTable, FsMstCistPortProposalPktsRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortProposalPktSentTimeStamp}, GetNextIndexFsMstCistPortTable, FsMstCistPortProposalPktSentTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortProposalPktRcvdTimeStamp}, GetNextIndexFsMstCistPortTable, FsMstCistPortProposalPktRcvdTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortAgreementPktSent}, GetNextIndexFsMstCistPortTable, FsMstCistPortAgreementPktSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortAgreementPktRcvd}, GetNextIndexFsMstCistPortTable, FsMstCistPortAgreementPktRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortAgreementPktSentTimeStamp}, GetNextIndexFsMstCistPortTable, FsMstCistPortAgreementPktSentTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortAgreementPktRcvdTimeStamp}, GetNextIndexFsMstCistPortTable, FsMstCistPortAgreementPktRcvdTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortImpStateOccurCount}, GetNextIndexFsMstCistPortTable, FsMstCistPortImpStateOccurCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortImpStateOccurTimeStamp}, GetNextIndexFsMstCistPortTable, FsMstCistPortImpStateOccurTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortOldPortState}, GetNextIndexFsMstCistPortTable, FsMstCistPortOldPortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMstCistPortLoopInconsistentState}, GetNextIndexFsMstCistPortTable, FsMstCistPortLoopInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstCistPortTableINDEX, 1, 0, 0, "2"},

{{12,FsMstMstiPort}, GetNextIndexFsMstMstiPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortPathCost}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortPathCostGet, FsMstMstiPortPathCostSet, FsMstMstiPortPathCostTest, FsMstMstiPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortPriority}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortPriorityGet, FsMstMstiPortPrioritySet, FsMstMstiPortPriorityTest, FsMstMstiPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMstMstiPortTableINDEX, 2, 0, 0, "128"},

{{12,FsMstMstiPortDesignatedRoot}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortDesignatedBridge}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortDesignatedBridgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortDesignatedPort}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortDesignatedPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortState}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiForcePortState}, GetNextIndexFsMstMstiPortTable, FsMstMstiForcePortStateGet, FsMstMstiForcePortStateSet, FsMstMstiForcePortStateTest, FsMstMstiPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortForwardTransitions}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortForwardTransitionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortReceivedBPDUs}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortReceivedBPDUsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortTransmittedBPDUs}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortTransmittedBPDUsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortInvalidBPDUsRcvd}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortInvalidBPDUsRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortDesignatedCost}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortDesignatedCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiSelectedPortRole}, GetNextIndexFsMstMstiPortTable, FsMstMstiSelectedPortRoleGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiCurrentPortRole}, GetNextIndexFsMstMstiPortTable, FsMstMstiCurrentPortRoleGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortInfoSemState}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortInfoSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortRoleTransitionSemState}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortRoleTransitionSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortStateTransitionSemState}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortStateTransitionSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortTopologyChangeSemState}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortTopologyChangeSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortEffectivePortState}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortEffectivePortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortAdminPathCost}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortAdminPathCostGet, FsMstMstiPortAdminPathCostSet, FsMstMstiPortAdminPathCostTest, FsMstMstiPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortPseudoRootId}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortPseudoRootIdGet, FsMstMstiPortPseudoRootIdSet, FsMstMstiPortPseudoRootIdTest, FsMstMstiPortTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortStateChangeTimeStamp}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortStateChangeTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortRootInconsistentState}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortRootInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, "2"},

{{12,FsMstMstiPortOldPortState}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortOldPortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortLoopInconsistentState}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortLoopInconsistentStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, "2"},

{{12,FsMstMstiPortTCDetectedCount}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortTCDetectedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortTCReceivedCount}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortTCReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortTCDetectedTimeStamp}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortTCDetectedTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortTCReceivedTimeStamp}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortTCReceivedTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortProposalPktsSent}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortProposalPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortProposalPktsRcvd}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortProposalPktsRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortProposalPktSentTimeStamp}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortProposalPktSentTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortProposalPktRcvdTimeStamp}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortProposalPktRcvdTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortAgreementPktSent}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortAgreementPktSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortAgreementPktRcvd}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortAgreementPktRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortAgreementPktSentTimeStamp}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortAgreementPktSentTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{12,FsMstMstiPortAgreementPktRcvdTimeStamp}, GetNextIndexFsMstMstiPortTable, FsMstMstiPortAgreementPktRcvdTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMstMstiPortTableINDEX, 2, 0, 0, NULL},

{{10,FsMstCistDynamicPathcostCalculation}, NULL, FsMstCistDynamicPathcostCalculationGet, FsMstCistDynamicPathcostCalculationSet, FsMstCistDynamicPathcostCalculationTest, FsMstCistDynamicPathcostCalculationDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsMstCalcPortPathCostOnSpeedChg}, NULL, FsMstCalcPortPathCostOnSpeedChgGet, FsMstCalcPortPathCostOnSpeedChgSet, FsMstCalcPortPathCostOnSpeedChgTest, FsMstCalcPortPathCostOnSpeedChgDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsMstRcvdEvent}, NULL, FsMstRcvdEventGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstRcvdEventSubType}, NULL, FsMstRcvdEventSubTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstRcvdEventTimeStamp}, NULL, FsMstRcvdEventTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstPortStateChangeTimeStamp}, NULL, FsMstPortStateChangeTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsMstPort}, GetNextIndexFsMstPortExtTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMstPortExtTableINDEX, 1, 0, 0, NULL},

{{12,FsMstPortRowStatus}, GetNextIndexFsMstPortExtTable, FsMstPortRowStatusGet, FsMstPortRowStatusSet, FsMstPortRowStatusTest, FsMstPortExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMstPortExtTableINDEX, 1, 0, 1, NULL},

{{10,FsMstBpduGuard}, NULL, FsMstBpduGuardGet, FsMstBpduGuardSet, FsMstBpduGuardTest, FsMstBpduGuardDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsMstStpPerfStatus}, NULL, FsMstStpPerfStatusGet, FsMstStpPerfStatusSet, FsMstStpPerfStatusTest, FsMstStpPerfStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsMstInstPortsMap}, NULL, FsMstInstPortsMapGet, FsMstInstPortsMapSet, FsMstInstPortsMapTest, FsMstInstPortsMapDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsMstSetTraps}, NULL, FsMstSetTrapsGet, FsMstSetTrapsSet, FsMstSetTrapsTest, FsMstSetTrapsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsMstGenTrapType}, NULL, FsMstGenTrapTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMstErrTrapType}, NULL, FsMstErrTrapTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsMstPortTrapIndex}, GetNextIndexFsMstPortTrapNotificationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsMstPortMigrationType}, GetNextIndexFsMstPortTrapNotificationTable, FsMstPortMigrationTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsMstPktErrType}, GetNextIndexFsMstPortTrapNotificationTable, FsMstPktErrTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsMstPktErrVal}, GetNextIndexFsMstPortTrapNotificationTable, FsMstPktErrValGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMstPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsMstPortRoleType}, GetNextIndexFsMstPortRoleTrapNotificationTable, FsMstPortRoleTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstPortRoleTrapNotificationTableINDEX, 2, 0, 0, NULL},

{{12,FsMstOldRoleType}, GetNextIndexFsMstPortRoleTrapNotificationTable, FsMstOldRoleTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMstPortRoleTrapNotificationTableINDEX, 2, 0, 0, NULL},
};
tMibData fsmstEntry = { 193, fsmstMibEntry };

#endif /* _FSMSTDB_H */

