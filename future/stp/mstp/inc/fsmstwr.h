
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmstwr.h,v 1.21 2017/09/20 06:32:07 siva Exp $
*
* Description:  Function Prototype for SNMP in RSTP/MSTP
*********************************************************************/
#ifndef _FSMSTWR_H
#define _FSMSTWR_H

VOID RegisterFSMST(VOID);

VOID UnRegisterFSMST(VOID);
INT4 FsMstSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsMstModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMaxMstInstanceNumberGet(tSnmpIndex *, tRetVal *);
INT4 FsMstNoOfMstiSupportedGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMaxHopCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstBrgAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistRootGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistRegionalRootGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistRootCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistRegionalRootCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistRootPortGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistBridgePriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistBridgeMaxAgeGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistBridgeForwardDelayGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistHoldTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistMaxAgeGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistForwardDelayGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstpUpCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstpDownCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstTraceGet(tSnmpIndex *, tRetVal *);
INT4 FsMstDebugGet(tSnmpIndex *, tRetVal *);
INT4 FsMstForceProtocolVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsMstTxHoldCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiConfigIdSelGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiRegionNameGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiRegionVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiConfigDigestGet(tSnmpIndex *, tRetVal *);
INT4 FsMstBufferOverFlowCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMemAllocFailureCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstRegionConfigChangeCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistBridgeRoleSelectionSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistTimeSinceTopologyChangeGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistTopChangesGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistNewRootBridgeCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistHelloTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistBridgeHelloTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMstSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsMstModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMstMaxMstInstanceNumberSet(tSnmpIndex *, tRetVal *);
INT4 FsMstMaxHopCountSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistBridgePrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistBridgeMaxAgeSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistBridgeForwardDelaySet(tSnmpIndex *, tRetVal *);
INT4 FsMstTraceSet(tSnmpIndex *, tRetVal *);
INT4 FsMstDebugSet(tSnmpIndex *, tRetVal *);
INT4 FsMstForceProtocolVersionSet(tSnmpIndex *, tRetVal *);
INT4 FsMstTxHoldCountSet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiConfigIdSelSet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiRegionNameSet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiRegionVersionSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistBridgeHelloTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsMstSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstMaxMstInstanceNumberTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstMaxHopCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistBridgePriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistBridgeMaxAgeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistBridgeForwardDelayTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstTraceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstDebugTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstForceProtocolVersionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstTxHoldCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstMstiConfigIdSelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstMstiRegionNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstMstiRegionVersionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistBridgeHelloTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstSystemControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMstModuleStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMstMaxMstInstanceNumberDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMstMaxHopCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMstCistBridgePriorityDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMstCistBridgeMaxAgeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMstCistBridgeForwardDelayDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMstPathCostDefaultTypeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMstTraceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMstDebugDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMstForceProtocolVersionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMstTxHoldCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMstMstiConfigIdSelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMstMstiRegionNameDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMstMstiRegionVersionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMstCistBridgeHelloTimeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
















INT4 GetNextIndexFsMstMstiBridgeTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMstMstiBridgeRegionalRootGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiBridgePriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiRootCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiRootPortGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiTimeSinceTopologyChangeGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiTopChangesGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiNewRootBridgeCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiBridgeRoleSelectionSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstInstanceUpCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstInstanceDownCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstOldDesignatedRootGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiBridgePrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiBridgePriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstMstiBridgeTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsMstVlanInstanceMappingTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMstMapVlanIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMstUnMapVlanIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMstSetVlanListGet(tSnmpIndex *, tRetVal *);
INT4 FsMstResetVlanListGet(tSnmpIndex *, tRetVal *);
INT4 FsMstInstanceVlanMappedGet(tSnmpIndex *, tRetVal *);
INT4 FsMstInstanceVlanMapped2kGet(tSnmpIndex *, tRetVal *);
INT4 FsMstInstanceVlanMapped3kGet(tSnmpIndex *, tRetVal *);
INT4 FsMstInstanceVlanMapped4kGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMapVlanIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMstUnMapVlanIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMstSetVlanListSet(tSnmpIndex *, tRetVal *);
INT4 FsMstResetVlanListSet(tSnmpIndex *, tRetVal *);
INT4 FsMstMapVlanIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstUnMapVlanIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstSetVlanListTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstResetVlanListTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstVlanInstanceMappingTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




INT4 GetNextIndexFsMstCistPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMstCistPortPathCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortDesignatedRootGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortDesignatedBridgeGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortDesignatedPortGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortAdminP2PGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortOperP2PGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortAdminEdgeStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortOperEdgeStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortProtocolMigrationGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistForcePortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortForwardTransitionsGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortRxMstBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortRxRstBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortRxConfigBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortRxTcnBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortTxMstBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortTxRstBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortTxConfigBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortTxTcnBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortInvalidMstBpduRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortInvalidRstBpduRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortInvalidConfigBpduRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortInvalidTcnBpduRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortTransmitSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortReceiveSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortProtMigrationSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistProtocolMigrationCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortDesignatedCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortRegionalRootGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortRegionalPathCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistSelectedPortRoleGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistCurrentPortRoleGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortInfoSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortRoleTransitionSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortStateTransitionSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortTopologyChangeSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortHelloTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortOperVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortEffectivePortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortAutoEdgeStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortRestrictedRoleGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortRestrictedTCNGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortAdminPathCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortEnableBPDURxGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortEnableBPDUTxGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortPseudoRootIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortIsL2GpGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortLoopGuardGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortRcvdEventGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortRcvdEventSubTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortRcvdEventTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortBpduGuardGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortErrorRecoveryGet(tSnmpIndex *, tRetVal *);
INT4 FsMstPortBpduInconsistentStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstPortBpduGuardActionGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortTCDetectedCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortTCReceivedCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortTCDetectedTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortTCReceivedTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortRcvInfoWhileExpCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortRcvInfoWhileExpTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortProposalPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortProposalPktsRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortProposalPktSentTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortProposalPktRcvdTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortAgreementPktSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortAgreementPktRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortAgreementPktSentTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortAgreementPktRcvdTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortImpStateOccurCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortImpStateOccurTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortOldPortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortLoopInconsistentStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortRootGuardGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortRootInconsistentStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortPathCostSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortAdminP2PSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortAdminEdgeStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortProtocolMigrationSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistForcePortStateSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortHelloTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortAutoEdgeStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortRestrictedRoleSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortRestrictedTCNSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortAdminPathCostSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortEnableBPDURxSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortEnableBPDUTxSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortPseudoRootIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortIsL2GpSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortLoopGuardSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortBpduGuardSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortErrorRecoverySet(tSnmpIndex *, tRetVal *);
INT4 FsMstPortBpduGuardActionSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortRootGuardSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortPathCostTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortAdminP2PTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortAdminEdgeStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortProtocolMigrationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistForcePortStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortHelloTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortAutoEdgeStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortRestrictedRoleTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortRestrictedTCNTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortAdminPathCostTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortEnableBPDURxTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortEnableBPDUTxTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortPseudoRootIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortIsL2GpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortLoopGuardTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortBpduGuardTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortErrorRecoveryTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstPortBpduGuardActionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortRootGuardTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);















INT4 GetNextIndexFsMstMstiPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMstMstiPortPathCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortDesignatedRootGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortDesignatedBridgeGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortDesignatedPortGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiForcePortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortForwardTransitionsGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortReceivedBPDUsGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortTransmittedBPDUsGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortInvalidBPDUsRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortDesignatedCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiSelectedPortRoleGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiCurrentPortRoleGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortInfoSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortRoleTransitionSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortStateTransitionSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortTopologyChangeSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortEffectivePortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortAdminPathCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortPseudoRootIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortStateChangeTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortRootInconsistentStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortOldPortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortLoopInconsistentStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortTCDetectedCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortTCReceivedCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortTCDetectedTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortTCReceivedTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortProposalPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortProposalPktsRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortProposalPktSentTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortProposalPktRcvdTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortAgreementPktSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortAgreementPktRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortAgreementPktSentTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortAgreementPktRcvdTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortPathCostSet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiForcePortStateSet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortAdminPathCostSet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortPseudoRootIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortPathCostTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstMstiForcePortStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortAdminPathCostTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortPseudoRootIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstMstiPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);





INT4 FsMstCistDynamicPathcostCalculationGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCalcPortPathCostOnSpeedChgGet(tSnmpIndex *, tRetVal *);
INT4 FsMstRcvdEventGet(tSnmpIndex *, tRetVal *);
INT4 FsMstRcvdEventSubTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMstRcvdEventTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMstPortStateChangeTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistDynamicPathcostCalculationSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCalcPortPathCostOnSpeedChgSet(tSnmpIndex *, tRetVal *);
INT4 FsMstCistDynamicPathcostCalculationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCalcPortPathCostOnSpeedChgTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstCistDynamicPathcostCalculationDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMstCalcPortPathCostOnSpeedChgDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsMstPortExtTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMstPortRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMstPortRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMstPortRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstPortExtTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsMstBpduGuardGet(tSnmpIndex *, tRetVal *);
INT4 FsMstBpduGuardSet(tSnmpIndex *, tRetVal *);
INT4 FsMstBpduGuardTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstBpduGuardDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMstStpPerfStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMstStpPerfStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMstStpPerfStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstStpPerfStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMstInstPortsMapGet(tSnmpIndex *, tRetVal *);
INT4 FsMstInstPortsMapSet(tSnmpIndex *, tRetVal *);
INT4 FsMstInstPortsMapTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstInstPortsMapDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 FsMstSetTrapsGet(tSnmpIndex *, tRetVal *);
INT4 FsMstGenTrapTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMstErrTrapTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMstSetTrapsSet(tSnmpIndex *, tRetVal *);
INT4 FsMstSetTrapsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMstSetTrapsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 GetNextIndexFsMstPortTrapNotificationTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMstPortMigrationTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMstPktErrTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMstPktErrValGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMstPortRoleTrapNotificationTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMstPortRoleTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMstOldRoleTypeGet(tSnmpIndex *, tRetVal *);
#endif /* _FSMSTWR_H */
