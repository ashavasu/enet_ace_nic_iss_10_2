/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpmswr.h,v 1.26 2017/09/20 06:32:07 siva Exp $
*
* Description:  Function Prototype for SNMP in RSTP/MSTP
*********************************************************************/
#ifndef _FSMPMSWR_H
#define _FSMPMSWR_H

VOID RegisterFSMPMS(VOID);

VOID UnRegisterFSMPMS(VOID);
INT4 FsMIMstGlobalTraceGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstGlobalDebugGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstGlobalTraceSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstGlobalDebugSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstGlobalTraceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstGlobalDebugTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstGlobalTraceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMIMstGlobalDebugDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
/* Mid Level SET Routine for All Objects (for Incremental Save only).  */
INT4 GetNextIndexFsMIDot1sFutureMstTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIMstSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMaxMstInstanceNumberGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstNoOfMstiSupportedGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMaxHopCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstBrgAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistRootGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistRegionalRootGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistRootCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistRegionalRootCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistRootPortGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistBridgePriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistBridgeMaxAgeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistBridgeForwardDelayGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistHoldTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistMaxAgeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistForwardDelayGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstpUpCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstpDownCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstTraceGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstDebugGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstForceProtocolVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstTxHoldCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiConfigIdSelGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiRegionNameGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiRegionVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiConfigDigestGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstBufferOverFlowCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMemAllocFailureCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstRegionConfigChangeCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistBridgeRoleSelectionSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistTimeSinceTopologyChangeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistTopChangesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistNewRootBridgeCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistHelloTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistBridgeHelloTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistDynamicPathcostCalculationGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstContextNameGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCalcPortPathCostOnSpeedChgGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstRcvdEventGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstRcvdEventSubTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstRcvdEventTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstPortStateChangeTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstFlushIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistFlushIndicationThresholdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistTotalFlushCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstBpduGuardGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstStpPerfStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstInstPortsMapGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMaxMstInstanceNumberSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMaxHopCountSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistBridgePrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistBridgeMaxAgeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistBridgeForwardDelaySet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstTraceSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstDebugSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstForceProtocolVersionSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstTxHoldCountSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiConfigIdSelSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiRegionNameSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiRegionVersionSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistBridgeHelloTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistDynamicPathcostCalculationSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCalcPortPathCostOnSpeedChgSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstFlushIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistFlushIndicationThresholdSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstBpduGuardSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstStpPerfStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstInstPortsMapSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstMaxMstInstanceNumberTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstMaxHopCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistBridgePriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistBridgeMaxAgeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistBridgeForwardDelayTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstTraceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstDebugTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstForceProtocolVersionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstTxHoldCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiConfigIdSelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiRegionNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiRegionVersionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistBridgeHelloTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistDynamicPathcostCalculationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCalcPortPathCostOnSpeedChgTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstFlushIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistFlushIndicationThresholdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstBpduGuardTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstStpPerfStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstInstPortsMapTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1sFutureMstTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsMIMstClearBridgeStatsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstClearBridgeStatsSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstClearBridgeStatsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortClearStatsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortClearStatsSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortBpduGuardSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortErrorRecoverySet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortRootGuardSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortClearStatsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortBpduGuardTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortErrorRecoveryTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortRootGuardTest(UINT4 *, tSnmpIndex *, tRetVal *);
















INT4 GetNextIndexFsMIMstMstiBridgeTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIMstMstiBridgeRegionalRootGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiBridgePriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiRootCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiRootPortGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiTimeSinceTopologyChangeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiTopChangesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiNewRootBridgeCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiBridgeRoleSelectionSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstInstanceUpCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstInstanceDownCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstOldDesignatedRootGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiFlushIndicationThresholdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiTotalFlushCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstiRootPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiBridgePrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiFlushIndicationThresholdSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstiRootPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiBridgePriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiFlushIndicationThresholdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstiRootPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiBridgeTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
/* Mid Level SET Routine for All Objects (for Incremental Save only).  */
INT4 GetNextIndexFsMIMstVlanInstanceMappingTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIMstMapVlanIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstUnMapVlanIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstSetVlanListGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstResetVlanListGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstInstanceVlanMappedGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstInstanceVlanMapped2kGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstInstanceVlanMapped3kGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstInstanceVlanMapped4kGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMapVlanIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstUnMapVlanIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstSetVlanListSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstResetVlanListSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMapVlanIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstUnMapVlanIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstSetVlanListTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstResetVlanListTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstVlanInstanceMappingTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




INT4 GetNextIndexFsMIMstCistPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIMstCistPortPathCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortDesignatedRootGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortDesignatedBridgeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortDesignatedPortGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortAdminP2PGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortOperP2PGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortAdminEdgeStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortOperEdgeStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortProtocolMigrationGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistForcePortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortForwardTransitionsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortRxMstBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortRxRstBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortRxConfigBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortRxTcnBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortTxMstBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortTxRstBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortTxConfigBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortTxTcnBpduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortInvalidMstBpduRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortInvalidRstBpduRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortInvalidConfigBpduRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortInvalidTcnBpduRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortTransmitSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortReceiveSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortProtMigrationSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistProtocolMigrationCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortDesignatedCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortRegionalRootGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortRegionalPathCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistSelectedPortRoleGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistCurrentPortRoleGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortInfoSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortRoleTransitionSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortStateTransitionSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortTopologyChangeSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortHelloTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortOperVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortEffectivePortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortAutoEdgeStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortRestrictedRoleGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortRestrictedTCNGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortAdminPathCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortEnableBPDURxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortEnableBPDUTxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortPseudoRootIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortIsL2GpGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortLoopGuardGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortRcvdEventGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortRcvdEventSubTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortRcvdEventTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistLoopInconsistentStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortBpduGuardGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortErrorRecoveryGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstPortBpduGuardActionGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortTCDetectedCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortTCReceivedCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortTCDetectedTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortTCReceivedTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortRcvInfoWhileExpCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortRcvInfoWhileExpTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortProposalPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortProposalPktsRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortProposalPktSentTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortProposalPktRcvdTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortAgreementPktSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortAgreementPktRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortAgreementPktSentTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortAgreementPktRcvdTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortImpStateOccurCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortImpStateOccurTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortOldPortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstPortBpduInconsistentStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortRootGuardGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortRootInconsistentStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortPathCostSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortAdminP2PSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortAdminEdgeStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortProtocolMigrationSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistForcePortStateSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortHelloTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortAutoEdgeStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortRestrictedRoleSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortRestrictedTCNSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortAdminPathCostSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortEnableBPDURxSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortEnableBPDUTxSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortPseudoRootIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortIsL2GpSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortLoopGuardSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstPortBpduGuardActionSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortPathCostTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortAdminP2PTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortAdminEdgeStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortProtocolMigrationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistForcePortStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortHelloTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortAutoEdgeStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortRestrictedRoleTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortRestrictedTCNTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortAdminPathCostTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortEnableBPDURxTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortEnableBPDUTxTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortPseudoRootIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortIsL2GpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortLoopGuardTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstPortBpduGuardActionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstCistPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);















INT4 GetNextIndexFsMIMstMstiPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIMstMstiPortPathCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortDesignatedRootGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortDesignatedBridgeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortDesignatedPortGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiForcePortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortForwardTransitionsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortReceivedBPDUsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortTransmittedBPDUsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortInvalidBPDUsRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortDesignatedCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiSelectedPortRoleGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiCurrentPortRoleGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortInfoSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortRoleTransitionSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortStateTransitionSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortTopologyChangeSemStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortEffectivePortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortAdminPathCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortPseudoRootIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortStateChangeTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortRootInconsistentStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortLoopInconsistentStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortTCDetectedCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortTCReceivedCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortTCDetectedTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortTCReceivedTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortProposalPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortProposalPktsRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortProposalPktSentTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortProposalPktRcvdTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortAgreementPktSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortAgreementPktRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortAgreementPktSentTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortAgreementPktRcvdTimeStampGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortOldPortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortPathCostSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiForcePortStateSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortAdminPathCostSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortPseudoRootIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortPathCostTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiForcePortStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortAdminPathCostTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortPseudoRootIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIMstPortExtTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIMstPortRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstPortRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstPortRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstPortExtTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsMIDot1sFsMstSetGlobalTrapOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstGlobalErrTrapTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1sFsMstSetGlobalTrapOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1sFsMstSetGlobalTrapOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1sFsMstSetGlobalTrapOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 GetNextIndexFsMIDot1sFsMstTrapsControlTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIMstSetTrapsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstGenTrapTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstSetTrapsSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstSetTrapsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1sFsMstTrapsControlTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsMIMstPortTrapNotificationTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIMstPortMigrationTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstPktErrTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstPktErrValGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIMstPortRoleTrapNotificationTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIMstPortRoleTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstOldRoleTypeGet(tSnmpIndex *, tRetVal *);


INT4 FsMIMstMstiClearBridgeStatsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiClearBridgeStatsSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiClearBridgeStatsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortClearStatsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortClearStatsSet(tSnmpIndex *, tRetVal *);
INT4 FsMIMstMstiPortClearStatsTest(UINT4 *, tSnmpIndex *, tRetVal *);


#endif /* _FSMPMSWR_H */
