/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmtrc.h,v 1.4 2007/02/13 06:38:02 iss Exp $
 *
 * Description:  This file contains Trace and Debug related 
 *               macros used in MSTP Module.              
 *
 *******************************************************************/



#ifndef _ASTMTRC_H
#define _ASTMTRC_H

#ifdef  TRACE_WANTED

#define MSTP_TRACE_WANTED

#endif

#define   MST_INIT_SHUT_TRC      INIT_SHUT_TRC     
#define   MST_MGMT_TRC           MGMT_TRC          
#define   MST_DATA_PATH_TRC      DATA_PATH_TRC     
#define   MST_CONTROL_PATH_TRC   CONTROL_PLANE_TRC 
#define   MST_DUMP_TRC           DUMP_TRC          
#define   MST_OS_RESOURCE_TRC    OS_RESOURCE_TRC   
#define   MST_ALL_FAILURE_TRC    ALL_FAILURE_TRC   
#define   MST_BUFFER_TRC         BUFFER_TRC        

/* Trace definitions */
#ifdef  TRACE_WANTED

#define MST_PKT_DUMP(TraceType, pBuf, Length, Str)                           \
        MOD_PKT_DUMP(AST_TRC_FLAG, TraceType, AST_MOD_NAME, pBuf, Length, Str)

#define MST_TRC(TraceType, Str)                                              \
        MOD_TRC(AST_TRC_FLAG, TraceType, AST_MOD_NAME, Str)

#define MST_TRC_ARG1(TraceType, Str, Arg1)                                   \
        MOD_TRC_ARG1(AST_TRC_FLAG, TraceType, AST_MOD_NAME, Str, Arg1)

#define MST_TRC_ARG2(TraceType, Str, Arg1, Arg2)                             \
        MOD_TRC_ARG2(AST_TRC_FLAG, TraceType, AST_MOD_NAME, Str, Arg1, Arg2)

#define MST_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)                       \
        MOD_TRC_ARG3(AST_TRC_FLAG, TraceType, AST_MOD_NAME, Str, Arg1, Arg2, Arg3)
   
#define   MST_DEBUG(x) {x}

#else  /* TRACE_WANTED */

#define MST_PKT_DUMP(TraceType, pBuf, Length, Str)
#define MST_TRC(TraceType, Str)
#define MST_TRC_ARG1(TraceType, Str, Arg1)
#define MST_TRC_ARG2(TraceType, Str, Arg1, Arg2)
#define MST_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)

#define   MST_DEBUG(x)

#endif /* TRACE_WANTED */
   
#endif /* _ASTMTRC_H */

