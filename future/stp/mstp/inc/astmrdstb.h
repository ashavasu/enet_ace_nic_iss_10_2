/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmrdstb.h,v 1.6 2007/09/11 11:04:14 iss Exp $
 *
 * Description: This file contains Prototype definitions of functions
 *              used in redundancy support for MSTP Module.                                 * 
 *
 *******************************************************************/

#ifndef _ASTM_RD_STB_H
#define _ASTM_RD_STB_H

INT4 MstRedStorePduInActive (UINT2 , tMstBpdu *, UINT2, UINT2);

INT4 MstRedSyncUpPdu (UINT2 , tMstBpdu* , UINT2);

INT4 MstRedHandleMstPdus (VOID *, UINT4 *, UINT2);

INT4 MstRedClearAllBpdusOnActive(VOID);

INT4 MstRedProtocolRestart (UINT2, UINT2);

INT4 MstRedClearAllSyncUpDataInInst(UINT2);                               

INT4 MstRedClearPduOnActive(UINT2);

VOID RedDumpMstPdu (VOID);

VOID RedDumpMstPduOnPort (UINT2 u2PortIfIndex);

INT4 MstRedClearInstanceOnActive (UINT2 u2InstanceId, UINT2 u2PortNum);
    
#endif /* _ASTMRED_H */
