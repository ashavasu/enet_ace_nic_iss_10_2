/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmprot.h,v 1.51 2017/11/21 13:06:45 siva Exp $
 *
 * Description:  This file contains MSTP Module Prototype         
 *               and definitions.                             
 *
 *******************************************************************/

#ifndef _ASTMPROT_H
#define _ASTMPROT_H

/******************************************************************************/
/*                                   astmsys.c                                */
/******************************************************************************/

/* SYSTEM MODULE */
INT4 MstModuleInit (VOID);
INT4 MstModuleEnable (VOID);
INT4 MstModuleDisable (VOID);
INT4 MstComponentInit (VOID);
INT4 MstComponentShutdown (VOID);
INT4 MstComponentEnable (VOID);
INT4 MstComponentDisable (VOID);
INT4 MstComponentDeletePort (UINT2 u2PortNum);
INT4 MstUpdtProtoPortStateInL2Iwf (UINT2 u2Port, UINT1 u1TrigType);
INT4 MstCreateInstance (UINT2 u2MstInst, tAstMstVlanList MstVlanList);
INT4 MstDeleteInstance (UINT2 u2MstInst);
INT4 MstCreatePort (UINT4 u4IfIndex, UINT2 u2PortNum);
INT4 MstCreatePerStPortEntry (UINT2 u2PortNum, UINT2 u2MstInst,UINT1 u1IsStartSems);
INT4 MstDeletePort (UINT2 u2PortNum);
INT4 MstDeletePerStPortEntry (UINT2 u2PortNum, UINT2 u2MstInst);
INT4 MstEnablePort (UINT2 u2PortNum, UINT2 u2MstInst, UINT1 u1TrigType);
INT4 MstDisablePort (UINT2 u2PortNum, UINT2 u2MstInst, UINT1 u1TrigType);
VOID MstInitGlobalBrgInfo (VOID);
VOID MstInitPerStBrgInfo (tAstPerStBridgeInfo * pPerStBrgInfo);
VOID MstInitGlobalPortInfo (UINT2 u2PortNum, tAstPortEntry * pPortInfo);
VOID MstInitPerStPortInfo (UINT2 u2PortNum, UINT2 u2InstanceId,
                           tAstPerStPortInfo * pPerStPortInfo);
INT4 MstInitMstBridgeEntry (VOID);
INT4 MstStartSemsForPort (UINT2 u2PortNum, UINT2 u2MstInst);

INT4 MstAssertBegin (VOID);

VOID MstReInitMstInfo (VOID);
INT4 MstHwReInit (VOID);
INT4 MstHwDeInit (VOID);
VOID MstInitConfigName (tAstBoolean bOverWrite);
INT4 MstVlanIdInstUnMap (UINT2 u2MstInst, UINT2 u2VlanId);
INT4 MstVlanIdInstMap (UINT2 u2MstInst, UINT2 u2VlanId);
INT4 MstVlanInstUnMap (UINT2 u2MstInst);
VOID MstInitStateMachines (VOID);
INT4 MstInitInstance (UINT2 u2MstInst);
VOID MstInitPortMigrationMachine (VOID);
VOID MstInitPortStateTrMachine (VOID);
VOID MstInitBrgDetectionStateMachine (VOID);
INT4 MstCreateInstancesForPort (UINT2 u2PortNum);
INT4 MstEnableInstancesForPort (UINT2 u2PortNum);
INT4 MstDisableInstancesForPort (UINT2 u2PortNum, UINT1 u1TrigType);
VOID MstEnableExtendedSysId (VOID);
VOID MstDisableExtendedSysId (VOID);
VOID MstResetCounters (VOID);
VOID MstResetPerStCounters (UINT2 u2InstIndex);
VOID MstResetPerStPortCounters (UINT2 u2InstIndex , UINT2 u2PortNum);
VOID MstResetPortCounters(UINT2 u2PortNum);
VOID MstInitPseudoInfoMachine (VOID);
UINT1 MstIsPortMemberOfInst (INT4 i4PortNum, INT4 i4MstInst);

#ifdef NPAPI_WANTED
VOID MstpRetryNpapiForAllInstances (tAstPortEntry * pPortEntry);
#endif
INT4 MstReCalculatePathcost (UINT4);
INT4 MstSetPortBPDUGuard (UINT4 u4PortNo,UINT4 u4GuardType, UINT4 u4BpduGuardStatus);
INT4
MstSetPortBPDUGuardAction (UINT4 u4PortNo, UINT1 u1BpduGuardAction);
/******************************************************************************/
/*                                   astmprsm.c                               */
/******************************************************************************/

/* PORT RECEIVE STATE MACHINE */
INT4 MstHandleInBpdu (tMstBpdu * pRcvBpdu, UINT2 u2PortNum);
VOID MstPortRcvdFlagsStatsUpdt (tMstBpdu *pRcvdBpdu, UINT2 u2PortNum,
                           UINT2 u2InstanceId);
INT4 MstPortReceiveMachine (UINT1 u1Event,
                            tMstBpdu * pRcvBpdu, UINT2 u2PortNum);
INT4 MstPortRcvSmMakeDiscard (tMstBpdu * pRcvBpdu, UINT2 u2PortNum);

VOID MstPortRcvClearAllRcvdMsgs (UINT2 u2PortNum);

INT4 MstPortRcvSmMakeReceive (tMstBpdu * pRcvBpdu, UINT2 u2PortNum);
VOID MstPortRcvUpdtBpduVersion (tMstBpdu * pRcvBpdu, UINT2 u2PortNum);
tAstBoolean MstPortRcvFromSameRegion (tMstBpdu * pRcvBpdu, 
                                      UINT2 u2PortNum);
INT4 MstPortRcvSetRcvdMsgs (tMstBpdu * pRcvBpdu, UINT2 u2PortNum);
INT4 MstPortRcvSmChkRcvdBpdu (tMstBpdu * pRcvBpdu, UINT2 u2PortNum);
INT4 MstPortRcvSmChkRcvdNoMsg (tMstBpdu * pRcvBpdu, UINT2 u2PortNum);
INT4 MstPortRcvSmChkPortEnabled (tMstBpdu * pRcvBpdu, UINT2 u2PortNum);
VOID MstInitPortRxStateMachine (VOID);

/******************************************************************************/
/*                                   astmpism.c                               */
/******************************************************************************/

/* PORT INFORMATION STATE MACHINE */

INT4 MstPortInfoMachine (UINT1 u1Event, tAstPerStPortInfo * pPerStPortInfo,
                         tMstBpdu * pRcvdBpdu, UINT2 u2MstInst);
INT4 MstPortInfoSmMakeDisabled (tAstPerStPortInfo * pPerStPortInfo,
                                tMstBpdu * pRcvdBpdu, UINT2 u2MstInst);
INT4 MstPortInfoSmMakeAged (tAstPerStPortInfo * pPerStPortInfo,
                            tMstBpdu * pRcvdBpdu, UINT2 u2MstInst);
INT4 MstPortInfoSmMakeUpdate (tAstPerStPortInfo * pPerStPortInfo,
                              tMstBpdu * pRcvdBpdu, UINT2 u2MstInst);
INT4 MstPortInfoSmMakeCurrent (tAstPerStPortInfo * pPerStPortInfo,
                               tMstBpdu * pRcvdBpdu, UINT2 u2MstInst);
INT4 MstPortInfoSmMakeSuperiorDesig (tAstPerStPortInfo * pPerStPortInfo,
                                     tMstBpdu * pRcvdBpdu, UINT2 u2MstInst);
INT4 MstPortInfoSmMakeRepeatDesig (tAstPerStPortInfo * pPerStPortInfo,
                                   tMstBpdu * pRcvdBpdu, UINT2 u2MstInst);
INT4 MstPortInfoSmMakeInferiorDesig (tAstPerStPortInfo * pPerStPortInfo,
                                     tMstBpdu * pRcvdBpdu, UINT2 u2MstInst);

INT4 MstPortInfoSmMakeNotDesig (tAstPerStPortInfo * pPerStPortInfo,
                            tMstBpdu * pRcvdBpdu, UINT2 u2MstInst);
INT4 MstPortInfoSmMakeOther (tAstPerStPortInfo * pPerStPortInfo,
                             tMstBpdu * pRcvdBpdu, UINT2 u2MstInst);
INT4 MstPortInfoSmMakeReceive (tAstPerStPortInfo * pPerStPortInfo,
                               tMstBpdu * pRcvdBpdu, UINT2 u2MstInst);
INT4 MstPortInfoSmRiWhileExpCurrent (tAstPerStPortInfo * pPerStPortInfo,
                                     tMstBpdu * pRcvdBpdu, UINT2 u2MstInst);
INT4 MstPortInfoSmUpdtInfoCurrent (tAstPerStPortInfo * pPerStPortInfo,
                                   tMstBpdu * pRcvdBpdu, UINT2 u2MstInst);
INT4 MstPortInfoSmUpdtInfoAged (tAstPerStPortInfo * pPerStPortInfo,
                                tMstBpdu * pRcvdBpdu, UINT2 u2MstInst);

            /* 802.1Q Updations */ 

VOID MstPortInfoSmRecordTimes (tAstPerStPortInfo * pPerStPortInfo,
                               tMstBpdu * pRcvdBpdu, UINT2 u2MstInst);
    
INT4 MstPortInfoSmClearedBegin (tAstPerStPortInfo * pPerStPortInfo,
                                tMstBpdu * pRcvdBpdu, UINT2 u2MstInst);

VOID MstPortInfoSmRecordPriority (tAstPerStPortInfo * pPerStPortInfo,
                                  tMstBpdu * pRcvdBpdu, UINT2 u2MstInst);

INT4 MstPortInfoSmUpdtRcvdInfoWhile (tAstPerStPortInfo * pPerStPortInfo,
                                     tMstBpdu * pRcvdBpdu, UINT2 u2MstInst);

tAstBoolean MstPortInfoSmBetterOrSameInfo (tAstPerStPortInfo * pPerStPortInfo,
                                           tMstBpdu * pRcvBpdu, 
                                           UINT2 u2MstInst);

UINT1 MstPortInfoSmRcvInfo (tAstPerStPortInfo * pPerStPortInfo,
                           tMstBpdu * pRcvBpdu, UINT2 u2MstInst);

INT4 MstPortInfoSmGetMsgType (tAstPerStPortInfo * pPerStPortInfo,
                              tMstBpdu * pRcvdBpdu, UINT2 u2MstInst);

VOID MstPortInfoSmRecordMastered (tAstPerStPortInfo * pPerStPortInfo,
                                  tMstBpdu * pRcvdBpdu, UINT2 u2MstInst);

INT4 MstPortInfoSmSetTcFlags (tAstPerStPortInfo * pPerStPortInfo,
                              tMstBpdu * pRcvdBpdu, UINT2 u2MstInst);

INT4 MstPortInfoSmEventImpossible (tAstPerStPortInfo * pPerStPortInfo,
                                   tMstBpdu * pRcvdBpdu, UINT2 u2MstInst);

VOID MstPortInfoSmRecordAgreement (tAstPerStPortInfo * pPerStPortInfo,
                                   tMstBpdu * pRcvBpdu, UINT2 u2MstInst);
VOID MstPortInfoSmRecordProposal (tAstPerStPortInfo * pPerStPortInfo,
                                  tMstBpdu * pRcvBpdu, UINT2 u2MstInst);
INT4 MstPortInfoSmRecordDispute (tAstPerStPortInfo * pPerStPortInfo,
                                  tMstBpdu * pRcvBpdu, UINT2 u2MstInst);
VOID MstProcessMstiMasteredFlag (tAstPerStPortInfo *pPerStPortInfo, UINT2 u2MstInst);

VOID MstInitPortInfoMachine (VOID);

/******************************************************************************/
/*                                  astmrssm.c                                */
/******************************************************************************/

/* ROLE SELECTION STATE MACHINE */
INT4 MstPRoleSelSmUpdtRolesDisabledTree (UINT2 u2MstInst);
INT4 MstPRoleSelSmClearReselectTree (UINT2 u2MstInst);
INT4 MstPRoleSelSmSetSelectedTree (UINT2 u2MstInst);
INT4 MstPRoleSelSmUpdtRolesCist (UINT2 u2MstInst);
INT4 MstPRoleSelSmUpdtRolesMsti (UINT2 u2MstInst);
INT4 MstIsDesgPrBetterThanPortPr (UINT2 u2PortNum, UINT2 u2InstanceId);
VOID MstInitPortRoleSelectionMachine (VOID);

          /* 802.1Q changes */
INT4 MstPRoleSelSmSyncMaster (VOID);

INT4 MstPRoleSelSmUpdtRoles (UINT2 u2MstInst);
                      
/******************************************************************************/
/*                                  astmrtsm.c                                */
/******************************************************************************/

/* ROLE TRANSITION STATE MACHINE */
INT4 MstPortRoleTransitMachine (UINT1 u1Event, 
                                tAstPerStPortInfo * pPerStPortInfo, 
                                UINT2 u2MstInst);
INT4 MstPRoleTrSmUpdateInfoReset (tAstPerStPortInfo * pPerStPortInfo,
                                  UINT2 u2MstInst);
INT4 MstPRoleTrSmRerootResetActivePort (tAstPerStPortInfo * pPerStPortInfo,
                                        UINT2 u2MstInst);
INT4 MstPRoleTrSmSyncSetActivePort (tAstPerStPortInfo * pPerStPortInfo,
                                    UINT2 u2MstInst);

/* Port Role Transition State M/c  802.1Q Prototypes */
INT4 MstPRoleTrSmAllSyncedSetDesigPort (tAstPerStPortInfo * pPerStPortInfo,
                                        UINT2 u2MstInst);

INT4 MstProleTrSmAgreedSetRootPort (tAstPerStPortInfo * pPerStPortInfo,
                                    UINT2 u2MstInst);

INT4 MstProleTrSmAgreedSetMasterPort (tAstPerStPortInfo * pPerStPortInfo,
                                      UINT2 u2MstInst);


INT4 MstPRoleTrSmMakeBlockPort (tAstPerStPortInfo * pPerStPortInfo,
                                UINT2 u2MstInst);

INT4 MstPRoleTrSmRoleChanged (tAstPerStPortInfo * pPerStPortInfo,
                              UINT2 u2MstInst);

INT4 MstPRoleTrSmMakeMasterPort (tAstPerStPortInfo * pPerStPortInfo,
                                 UINT2 u2MstInst);

INT4 MstPRoleTrSmMakeDesigPort (tAstPerStPortInfo * pPerStPortInfo,
                                UINT2 u2MstInst);

INT4 MstProleTrSmIndicateAllSyncedSet (UINT2 u2PortNo, UINT2 u2MstInst);

INT4 MstProleTrSmProposedSetAlternatePort (tAstPerStPortInfo * pPerStPortInfo,
                                           UINT2 u2MstInst);

INT4 MstProleTrSmDesigPortTransitions (tAstPerStPortInfo * pPerStPortInfo,
                                       tAstBoolean bAllSyncedEvent,
                                       UINT2 u2MstInst);

INT4 MstProleTrSmMasterPortTransitions (tAstPerStPortInfo * pPerStPortInfo,
                                        tAstBoolean bAllSyncedEvent,
                                        UINT2 u2MstInst);

INT4 MstPRoleTrSmReRootSetDesigOrMasterPort (tAstPerStPortInfo * pPerStPortInfo,
                                        UINT2 u2MstInst);

INT4 MstPRoleTrSmRerootedSetRootPort (tAstPerStPortInfo * pPerStPortInfo,
                                      UINT2 u2MstInst);

INT4 MstProleTrSmTransitionToRootFwding (tAstPerStPortInfo * pPerStPortInfo,
                                         UINT2 u2MstInst);

INT4 MstProleTrSmMakeDesignatedorMasterSynced 
                     (tAstPerStPortInfo * pPerStPortInfo, UINT2 u2MstInst);
                     
INT4 MstProleTrSmMakeDesignatedorMasterLearnorFwd
                    (tAstPerStPortInfo * pPerStPortInfo, UINT2 u2MstInst);

INT4 MstPRoleTrSmRootPortTransitions (tAstPerStPortInfo * pPerStPortInfo,
                                      tAstBoolean bAllSyncedEvent, 
                                      UINT2 u2MstInst);

INT4 MstPRoleTrSmSyncSetDesigPort (tAstPerStPortInfo * pPerStPortInfo,
                                   UINT2 u2MstInst);

INT4 MstPRoleTrSmSyncSetMasterPort (tAstPerStPortInfo * pPerStPortInfo,
                                    UINT2 u2MstInst);

INT4 MstProleTrSmSyncSetRootPort (tAstPerStPortInfo * pPerStPortInfo,
                                  UINT2 u2MstInst);

INT4 MstPRoleTrSmDisputedSetDesigOrMasterPort 
                       (tAstPerStPortInfo * pPerStPortInfo, UINT2 u2MstInst);
                       
INT4 MstPRoleTrSmOperEdgeSetDesigOrMasterPort 
                       (tAstPerStPortInfo * pPerStPortInfo, UINT2 u2MstInst);
                       
INT4 MstPRoleTrSmMakeRootPort (tAstPerStPortInfo * pPerStPortInfo,
                               UINT2 u2MstInst);

INT4 MstPRoleTrSmMakeAlternatePort (tAstPerStPortInfo * pPerStPortInfo,
                                    UINT2 u2MstInst);

INT4 MstPRoleTrSmMakeDisablePort (tAstPerStPortInfo * pPerStPortInfo,
                                  UINT2 u2MstInst);

INT4 MstPRoleTrSmFwdExpRootPort (tAstPerStPortInfo * pPerStPortInfo,
                                 UINT2 u2MstInst);

INT4 MstProleTrSmAltPortTransitions (tAstPerStPortInfo * pPerStPortInfo,
                                     tAstBoolean bAllSyncedEvent, 
                                     UINT2 u2MstInst);

INT4 MstPRoleTrSmRrWhileExpDesigPort (tAstPerStPortInfo * pPerStPortInfo,
                                      UINT2 u2MstInst);

INT4 MstPRoleTrSmRbWhileExpRootPort (tAstPerStPortInfo * pPerStPortInfo,
                                     UINT2 u2MstInst);

INT4 MstPRoleTrSmMakeInitPort (tAstPerStPortInfo * pPerStPortInfo, 
                               UINT2 u2MstInst);

INT4 MstPRoleTrSmMakeDiasblePort (tAstPerStPortInfo * pPerStPortInfo, 
                                UINT2 u2MstInst);

INT4 MstPRoleTrSmMakeDisabledPort (tAstPerStPortInfo * pPerStPortInfo,
                                   UINT2 u2MstInst);

INT4 MstPRoleTrSmMakeForward (tAstPerStPortInfo * pPerStPortInfo, 
                              UINT2 u2MstInst);

INT4 MstPRoleTrSmMakeLearn (tAstPerStPortInfo * pPerStPortInfo, 
                            UINT2 u2MstInst);

INT4 MstProleTrSmMakeDesignatedorMasterDiscard
      (tAstPerStPortInfo * pPerStPortInfo, UINT2 u2MstInst);
      
INT4 MstPRoleTrSmFwdExpDesigOrMasterPort(tAstPerStPortInfo * pPerStPortInfo, 
                                          UINT2 u2MstInst);

INT4 MstPRoleTrSmSetSyncTree (UINT2 u2PortNo, UINT2 u2MstInst);

INT4 MstPRoleTrSmSetRerootTree (UINT2 u2MstInst);

INT4 MstProleTrSmProposedSetRootPort (tAstPerStPortInfo * pPerStPortInfo,
                                      UINT2 u2MstInst);

INT4 MstProleTrSmProposedSetMasterPort (tAstPerStPortInfo * pPerStPortInfo,
                                        UINT2 u2MstInst);

INT4 MstProleTrSmAgreedSetDesigPort (tAstPerStPortInfo * pPerStPortInfo,
                                     UINT2 u2MstInst);

INT4 MstPRoleTrSmAllSyncedSetRootPort (tAstPerStPortInfo * pPerStPortInfo,
                                       UINT2 u2MstInst);

INT4 MstPRoleTrSmAllSyncedSetMasterPort (tAstPerStPortInfo * pPerStPortInfo,
                                         UINT2 u2MstInst);

INT4 MstPRoleTrSmAllSyncedSetAltPort (tAstPerStPortInfo * pPerStPortInfo,
                                      UINT2 u2MstInst);

tAstBoolean MstIsAllOtherPortsSynced (UINT2 u2PortNum, UINT2 u2MstInst);

INT4 MstPRoleTrSmIsReRooted (UINT2 u2PortNum, UINT2 u2MstInst);

INT4
MstProleTrSmLearningFwdingReset (tAstPerStPortInfo * pPerStPortInfo,
                                 UINT2 u2MstInst);

VOID MstInitPortRoleTrMachine (VOID);

/******************************************************************************/
/*                                  astmtcsm.c                                */
/******************************************************************************/

/* TOPOLOGY CHANGE STATE MACHINE */
INT4 MstTopologyChMachine (UINT1 u1Event,
                           UINT2 u2InstanceId, 
                           tAstPerStPortInfo * pPerStPortInfo);

INT4 MstTopoChSmMakeInActive (UINT2 u2InstanceId, 
                              tAstPerStPortInfo * pPerStPortInfo);

INT4 MstTopoChSmEventImpossible (UINT2 u2InstanceId,
                                 tAstPerStPortInfo * pPerStPortInfo);

INT4 MstTopologyChSmMakeDetected (UINT2 u2InstanceId,
                                  tAstPerStPortInfo * pPerStPortInfo);

INT4 MstTopoChSmChkInactive (UINT2 u2InstanceId, 
                             tAstPerStPortInfo * pPerStPortInfo);

INT4 MstTopoChSmChkLearning (UINT2 u2InstanceId,
                             tAstPerStPortInfo * pPerStPortInfo);
    
    
INT4 MstTopoChSmChkDetected (UINT2 u2InstanceId,
                             tAstPerStPortInfo * pPerStPortInfo);

INT4 MstTopoChSmMakeLearning (UINT2 u2InstanceId,
                              tAstPerStPortInfo * pPerStPortInfo);

INT4 MstTopologyChNewTcWhile (UINT2 u2InstanceId, 
                              tAstPerStPortInfo * pPerStPortInfo);
#ifdef MRP_WANTED
INT4 MstTopologyChNewTcDetected (UINT2 u2InstanceId, 
                                 tAstPerStPortInfo * pPerStPortInfo);
#endif
VOID MstTopologyChSetTcPropTree (UINT2 u2InstanceId,
                                 tAstPerStPortInfo * pPerStPortInfo);
INT4 MstTopologyChSmMakeNotifiedTcn (UINT2 u2InstanceId,
                                     tAstPerStPortInfo * pPerStPortInfo);
INT4 MstTopologyChSmMakeNotifiedTc (UINT2 u2InstanceId,
                                    tAstPerStPortInfo * pPerStPortInfo);
INT4 MstTopologyChSmMakePropagating (UINT2 u2InstanceId,
                                     tAstPerStPortInfo * pPerStPortInfo);
INT4 MstTopologyChSmMakeAcknowledged (UINT2 u2InstanceId,
                                      tAstPerStPortInfo * pPerStPortInfo);
INT4 MstTopologyChSmMakeActive (UINT2 u2InstanceId,
                                tAstPerStPortInfo * pPerStPortInfo);
INT4 MstTopologyChSmNoOperEdgeInActive (UINT2 u2InstanceId,
                                        tAstPerStPortInfo * pPerStPortInfo);
INT4 MstTopologyChSmForwardInActive (UINT2 u2InstanceId,
                                     tAstPerStPortInfo * pPerStPortInfo);
VOID MstTopologyChSmFlushPort (tAstPerStPortInfo * pPerStPortInfo); 
VOID MstTopologyChSmFlushFdb (tAstPerStPortInfo * pPerStPortInfo, 
                              UINT2 u2InstanceId, INT4 i4OptimizeFlag);
VOID MstInitTopoChStateMachine (VOID);
VOID AstTopologyChSmFlushEntries (tAstPerStPortInfo * pPerStPortInfo);

/******************************************************************************/
/*                                  astmtxsm.c                                */
/******************************************************************************/

/* PORT TRANSMIT STATE MACHINE */
INT4 MstPortTxSmTxMstp (tAstPortEntry * pAstPortEntry, UINT2 u2MstInst);
INT4 MstFormBpdu (tAstPortEntry * pAstPortEntry, UINT2 u2InstanceId,
                  UINT1 u1MsgType, tAstBufChainHeader * pBuf, 
                  UINT4 *pu4DataLength , UINT1 *pu1Flag);
VOID MstFillBpdu (tAstPerStPortInfo * pPerStPortInfo, UINT1 **ppu1Bpdu);
VOID MstInitPortTxStateMachine (VOID);

/******************************************************************************/
/*                                  astmutil.c                                */
/******************************************************************************/

/* UTILITY ROUTINES */
INT4 MstUtlChkPriority (tAstPerStPortInfo * pPerStPortInfo, tMstBpdu * pRcvdBpdu,
                        UINT2 u2MstInst);
#ifdef RSTP_DEBUG 
VOID MstDumpPerStPortInfo (UINT2 u2PortNum, UINT2 u2InstanceId);
#endif
INT4 MstTmrExpiryHandler (tAstTimer *pAstTimer);

tAstBoolean MstMstiDesignatedOrTcPropagatingRoot (UINT2 u2PortNum);

tAstBoolean MstPortTxSmAllTransmitReady (UINT2 u2PortNum);

VOID MstGetVlanList (UINT2 u2MstInst, tAstVlanList VlanList, INT4 i4Flag);
VOID MstGetVfiVlanList (UINT2 u2MstInst, tAstMstVlanList VlanList);
VOID MstTriggerDigestCalc (VOID);

INT4 MstPreparePseudoInfo (UINT2 u2PortNum, tMstBpdu **ppPeudoMstBpdu);
INT4 MstCheckBPDUConsistency (UINT2 u2PortNum, tMstBpdu *pRcvdBpdu);

PUBLIC VOID
MstUtilTicksToDate (UINT4 u4Ticks, UINT1 *pu1DateFormat);
/******************************************************************************/
/*                                  astmsnmp.c                                */
/******************************************************************************/

/* SNMP INTERFACE ROUTINES */
INT4 MstSetMaxInstance (UINT2 u2MaxInstance);
INT4 MstSetMaxHopCount (UINT1 u1MaxHopCount);
INT4 MstSetConfigId (UINT1 u1ConfigId);
INT4 MstSetRegionName (UINT1 *pu1ConfigName, UINT2 u2NameLen);
INT4 MstSetRegionVersion (UINT2 u2RegionVersion);
INT4 MstSetBridgePriority (UINT2 u2InstanceId, UINT2 u2Priority);
INT4 MstSetDynamicPathcostCalc (UINT1 u1DynamicPathcostCalc);
INT4 MstSetDynaPathcostCalcLagg (UINT1 u1DynamicPathcostCalc);
INT4 MstSetPortPathCost (UINT2 u2PortNo, UINT2 u2InstanceId, UINT4 u4PathCost);
INT4 MstUpdatePortPathCost (UINT2 u2PortNum, UINT2 u2InstId, UINT4 u4PathCost);
INT4 MstSetPortPriority (UINT2 u2InstanceId, UINT2 u2PortNo, UINT1 u1Priority);
INT4 MstSetAdminPointToPoint (UINT2 u2PortNo, UINT1 u1AdminPToP);
INT4 MstSetAdminEdgePort (UINT2 u2PortNo, tAstBoolean bAdminEdgePort);
INT4 MstMapVlanList (tAstMstVlanList MstVlanList, UINT2 u2InstanceId);
INT4 MstUnMapVlanList (tAstMstVlanList MstVlanList, UINT2 u2InstanceId);
INT4 MstSetPortHelloTime (UINT2 u2PortNo, UINT2 u2HelloTime);
INT4 MstSetPortErrorRecovery (UINT2 u2PortNo, UINT4 u2ErrorRecovery);
INT4 MstSetAutoEdgePort (UINT2 u2PortNum, tAstBoolean bStatus);
INT4  MstSetPortRestrictedRole(tAstPortEntry *pPortEntry, tAstBoolean  bStatus);
INT4  MstSetPortRootGuard(tAstPortEntry *pPortEntry, tAstBoolean  bStatus);
INT4 MstSetZeroPortPathCost (UINT2 u2PortNo, UINT2 u2InstanceId);
INT4 MstSetPortLoopGuard (UINT2 u2PortNum, tAstBoolean bStatus);

/******************************************************************************/
/*                                  astmtrap.c                                */
/******************************************************************************/

/* MSTP TRAP ROUTINES */
VOID AstMstInstUpTrap (UINT2 u2InstId, INT1 *pi1TrapsOid, UINT1 u1TrapOidLen);
VOID AstMstInstDownTrap (UINT2 u2InstId, INT1 *pi1TrapsOid, UINT1 u1TrapOidLen);
VOID AstMstRegionConfigChangeTrap (tMstConfigIdInfo * pMstConfigIdInfo,
                                   INT1 *pi1TrapsOid, UINT1 u1TrapOidLen);
VOID MstSnmpIfSendTrap (UINT1 u1TrapId, INT1 *pi1TrapOid, UINT1 u1OidLen,
                        VOID *pTrapInfo);
tSNMP_OID_TYPE     * MstMakeObjIdFromDotNew (INT1 *pi1TextStr);
INT1 * MstParseSubIdNew (INT1 *pi1TmpPtr, UINT4 *pu4Value);

/******************************************************************************/
/*                                  astmplow.c                                */
/******************************************************************************/

/* SNMP LOW-LEVEL VALIDATE UTILITY ROUTINES */
INT4 MstValidateInstanceEntry (INT4 i4FsMstInstanceIndex);

/* MSTP CLI RELATED UTILITY ROUTINES */
VOID MstConvertOctetToVlan (UINT1 *pOctet, UINT4 u4OctetLen,
                            UINT1 *pu1Temp, INT4 i4Flag);

INT4 MstConvertStrToVlanList (UINT1 *pu1String, UINT1 *au1PortArray);
UINT2 MstCliCheckVlanId (UINT1 *pu1Temp);

/******************************************************************************/
/*                                  mstpmbsm.c                                */
/******************************************************************************/
#ifdef MBSM_WANTED
PUBLIC INT1 FsMiMstpMbsmNpCreateInstance (UINT4 ,UINT2 ,tMbsmSlotInfo *);
PUBLIC INT1 FsMiMstpMbsmNpAddVlanInstMapping (UINT4 ,tVlanId ,UINT2 , 
                                              tMbsmSlotInfo *);
INT1 FsMiMstpMbsmNpSetInstancePortState (UINT4 ,UINT4 ,UINT2 ,UINT1 ,
                                         tMbsmSlotInfo * );
#endif

INT4 MstMiValidateInstanceEntry (UINT4 u4ContextId, INT4 i4FsMstInstanceIndex);
INT4 MstMiValidateMstiPortEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                 INT4 i4MstInst);

/******************************************************************************/
/*                                  astmsisp.c                                */
/******************************************************************************/
INT4 MstSispEnableSispOnInterface (UINT2 u2PortNum);

INT4 MstSispDisableSispOnInterface (UINT2 u2PortNum);

INT4 MstSispHandleVlanAddPort (tAstMsgNode * pMsgNode);

INT4 MstSispValInstPortRestrict (UINT2 u2PortNum, UINT2 u2MstInst);

INT1 MstSispIsInstanceListSet (UINT4  u4PhyPort, UINT2 u2MstInst);

INT4 MstSispHandleVlanDelPort (tAstMsgNode * pMsgNode);

INT4 MstSispValidateVlantoInstanceMapping (tVlanId VlanId, UINT2 u2MstInst);

INT4 MstSispHandleBpduInSisp (tMstBpdu *pRcvdBpdu, UINT2 u2PortNum);

INT4 MstSispDeriveSispDefaults (UINT2 u2PortNum);

INT4 MstSispUpdatePortInstList (UINT2 u2PortNum, UINT2 u2MstInst, 
    UINT1 u1Action);

VOID
MstSispExtractInstInfo (tMstBpdu * pRcvdBpdu);

INT4 MstSispHandleUpdateVlanPortList (tAstMsgNode *pMsgNode);

INT4 MstSispVlanAddPort (UINT2  u2PortNum, tVlanId VlanId);

INT4 MstSispVlanDelPort (UINT2  u2PortNum, tVlanId VlanId);

BOOL1
MstSispIsVlansPresentInInst (UINT2 u2PortNum, UINT2 u2MstInst,
                             tSNMP_OCTET_STRING_TYPE * pVlanIdList,
                             tAstMstVlanList NewVlanList);
VOID
MstSispMakeCistInfoInferior (tMstBpdu * pRcvdBpdu);


INT4 AstConvertToLocalPortList (tPortList IfPortList,
    tLocalPortList LocalPortList);
INT4
MstSispIsPortPresentInInst (UINT2 u2PortNum, UINT2 u2MstInst);

VOID MstFlushFdbOnInstance (UINT2 u2InstId);

INT4 
MstGetInstPortLoopInconsistentState (UINT2 u2PortNum, UINT2 u2InstanceId, INT4 *pi4InstPortLoopIncStatus); 

INT1
MstConfigBridgePriority (INT4 i4InstId ,
                      INT4 i4BridgePriority);


#endif /* _ASTMPROT_H */

