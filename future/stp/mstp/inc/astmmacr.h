/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmmacr.h,v 1.18 2017/09/20 06:32:06 siva Exp $
 *
 * Description: This file contains definitions of macros used  
 *              in the MSTP Module.                            
 *
 *******************************************************************/


#ifndef _ASTMMACR_H
#define _ASTMMACR_H

/* SEM Related Macros */
#define MST_PORT_ROLE_SEL_MACHINE gAstGlobalInfo.aaPortRoleSelMachine
#define MST_PORT_STATE_TR_MACHINE gAstGlobalInfo.aaPortStateTrMachine
#define MST_PORT_MIG_MACHINE      gAstGlobalInfo.aaPortMigMachine
#define MST_PORT_TX_MACHINE       gAstGlobalInfo.aaPortTxMachine

#define MST_TOPO_CH_MACHINE       gAstGlobalInfo.aaMstTopoChMachine
#define MST_PORT_ROLE_TR_MACHINE  gAstGlobalInfo.aaMstPortRoleTrMachine
#define MST_PORT_RECEIVE_MACHINE  gAstGlobalInfo.aaMstPortRcvMachine
#define MST_PORT_INFO_MACHINE     gAstGlobalInfo.aaMstPortInfoMachine

/* Macros for accessing Global Structure fields */
#define AST_GET_MST_BRGENTRY()   &((AST_CURR_CONTEXT_INFO ())->BridgeEntry.MstBridgeEntry)

#define AST_GET_MAX_MST_INSTANCES    \
      (AST_GET_MST_BRGENTRY ())->u2MaxMstInstances
      
#define AST_GET_NO_OF_ACTIVE_INSTANCES   \
      (AST_CURR_CONTEXT_INFO ())->BridgeEntry.MstBridgeEntry.u2NoOfActiveMsti

#define AST_GET_MST_DIGEST_INPUT()  \
           (AST_CURR_CONTEXT_INFO ())->BridgeEntry.MstBridgeEntry.pu1DigestInp

#define AST_GET_CIST_MSTI_PORT_INFO(u2PortNum) \
          (&(AST_GET_PORTENTRY(u2PortNum)->CistMstiPortInfo))


#define AST_GET_PER_ST_CIST_MSTI_COMM_INFO(u2PortNum, u2MstInst) \
          &((AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst))->PerStCistMstiCommPortInfo)

#define AST_GET_MST_CONFIGID_ENTRY() \
           &((AST_CURR_CONTEXT_INFO ())->BridgeEntry.MstBridgeEntry.MstConfigIdInfo)

#define AST_GET_INST_TRIGGER_FLAG ((AST_CURR_CONTEXT_INFO ())->BridgeEntry.MstBridgeEntry.bFlagInstTrigger)

#define MST_GET_CIST_MSTI_PORT_INFO(pPerStPortInfo)   \
          &(pPerStPortInfo->PerStCistMstiCommPortInfo)

#define MST_GET_CIST_MSTI_PORT_INFO_PTR(pPerStPortInfo)   \
          (pPerStPortInfo->PerStCistMstiCommPortInfo)

#define AST_GET_PERST_MSTI_ONLY_INFO(pPerStPortInfo)   \
           &(pPerStPortInfo->PerStMstiOnlyInfo)

#define AST_GET_PER_ST_CIST_MSTI_PORT_INFO_PTR(u2PortNum, u2MstInst) \
           &(AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst))

#define AST_INST_UP_COUNT(u2MstInst) \
            AST_CURR_CONTEXT_INFO()->pInstanceUpCount[u2MstInst]

                          
#define AST_GET_PER_ST_CIST_MSTI_PORT_INFO(u2PortNum, u2MstInst) \
           (AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst))


#define AST_GET_PER_ST_INFO_PTR(u2MstInst) \
           &(AST_GET_PERST_INFO(u2MstInst))

#define AST_GET_PER_ST_BRG_INFO(u2InstanceId) \
          (AST_GET_PERST_INFO(u2InstanceId)->PerStBridgeInfo)

   
#define MST_IS_NOT_SELECTED(pRstPortInfo) \
        ((pRstPortInfo->bSelected != MST_TRUE) || \
               (pRstPortInfo->bUpdtInfo != MST_FALSE))

#define MST_UP_TRAPS_GEN() \
          &((AST_CURR_CONTEXT_INFO ())->BridgeEntry)->u4NumStpUpTrapsGen++;

#define MST_DOWN_TRAPS_GEN() \
         &((AST_CURR_CONTEXT_INFO ())->BridgeEntry)->u4NumStpDownTrapsGen++;

#define AST_INCR_MST_REGION_CONFIG_CHANGE_COUNT() \
      ((AST_CURR_CONTEXT_INFO ())->BridgeEntry.MstBridgeEntry.u4RegionCfgChangeCount)++

#define AST_INCR_INSTANCE_UP_COUNT(u2InstanceId) \
      ((AST_CURR_CONTEXT_INFO())->pInstanceUpCount[u2InstanceId])++ 


#define AST_GET_CONFIG_NAME(Value, pu1PktBuf)  \
        do {                                       \
           AST_MEMCPY (&Value, (pu1PktBuf), MST_CONFIG_NAME_LEN);    \
           (pu1PktBuf) += MST_CONFIG_NAME_LEN;                       \
        }while(0)

#define AST_GET_CONFIG_DIGEST(Value, pu1PktBuf)  \
        do {                                       \
           AST_MEMCPY (&Value, (pu1PktBuf), MST_CONFIG_DIGEST_LEN);    \
           (pu1PktBuf) += MST_CONFIG_DIGEST_LEN;                       \
        }while(0)

#define AST_PUT_CONFIG_NAME(pu1PktBuf, Value)   \
        do {                                      \
           AST_MEMCPY (pu1PktBuf, &Value, MST_CONFIG_NAME_LEN);     \
           pu1PktBuf += MST_CONFIG_NAME_LEN;                        \
        }while(0)

#define AST_PUT_CONFIG_DIGEST(pu1PktBuf, Value)   \
        do {                                      \
           AST_MEMCPY (pu1PktBuf, &Value, MST_CONFIG_DIGEST_LEN);     \
           pu1PktBuf += MST_CONFIG_DIGEST_LEN;                        \
        }while(0)

#define MST_SET_PORT_PRIORITY(u1Priority, u1PortPriority) \
{\
      u1PortPriority = (UINT1) (u1PortPriority & 0x0F); \
      u1Priority = (UINT1) (u1Priority & 0xF0); \
      u1PortPriority = (UINT1) (u1Priority | u1PortPriority);\
}

#define MST_SET_BRIDGE_PRIORITY(u2Priority, u2BridgePriority) \
{\
      u2BridgePriority = (UINT2) (u2BridgePriority & 0x0FFF); \
         u2Priority = (UINT2) (u2Priority & 0xF000); \
      u2BridgePriority = (UINT2) (u2BridgePriority | u2Priority);\
}

#define MST_GET_INSTANCE_ID(u2BridgePriority) \
         (UINT2) (u2BridgePriority & 0x0FFF)

#define MST_GET_BRIDGE_PRIORITY(u2BridgePriority, u2Priority) \
{\
      u2Priority = (UINT2) (u2BridgePriority & 0xF000); \
}
         
#define MST_GET_EXT_BRIDGE_PRIORITY(u2BridgePriority, u2Priority) \
{\
      u2Priority = (u2BridgePriority & 0xFFFF); \
}
         
#define MST_GET_PORT_PRIORITY(u1PortPriority, u1Priority)\
{\
      u1Priority = (UINT1) (u1PortPriority & 0xF0); \
}

#define MstCliPrintTestFnErrorMsg RstCliPrintTestFnErrorMsg
#define AST_COMPARE_BRGID RstCompareBrgId

#define MST_IS_INSTANCE_VALID(i4FsMstInstanceIndex) \
    ((((i4FsMstInstanceIndex >= AST_MIN_MST_INSTANCES) && (i4FsMstInstanceIndex < AST_MAX_MST_INSTANCES)) || (i4FsMstInstanceIndex == AST_TE_MSTID)) ? MST_TRUE:MST_FALSE)   

#define MST_IS_VALID_VLANID(Vid) \
    (((Vid < MST_MIN_VLAN_ID) || (Vid > AST_MAX_NUM_VLANS))? MST_FALSE:MST_TRUE)

/* Macro for checking vlan list is nonempty*/
#define IS_MST_VLANLIST_NONEMPTY(pu1VlanList, i4IsResult) \
{\
    UINT2 u2Count = 0;\
    i4IsResult = MST_FALSE;\
    for (u2Count = 0; u2Count < MST_VLAN_LIST_SIZE; u2Count++)\
    {\
        if (pu1VlanList [u2Count] != 0)\
        {\
            i4IsResult = MST_TRUE;\
            break;\
        }\
    }\
}
/* Macros for accessing the port bit array */

#define MST_SET_MEMBER_PORT(au1PortArray, u2Port) \
{\
    UINT2 u2PortBytePos;\
    UINT2 u2PortBitPos;\
    if (u2Port)\
    {\
        u2PortBytePos = (UINT2)(u2Port / MST_PORTORVLAN_PER_BYTE);\
        u2PortBitPos  = (UINT2)(u2Port % MST_PORTORVLAN_PER_BYTE);\
        if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
        \
        au1PortArray[u2PortBytePos] =\
        (UINT1)(au1PortArray[u2PortBytePos] \
                | gau1MstPortBitMaskMap[u2PortBitPos]);\
    }\
}    
#define MST_IS_MEMBER_VLAN(au1VlanArray, u2VlanId, u1Result) \
{\
    UINT2 u2VlanBytePos;\
    UINT2 u2VlanBitPos;\
    if (MST_IS_VALID_VLANID(u2VlanId) == MST_FALSE) \
    {u1Result = MST_FALSE;} \
    else {\
        u2VlanBytePos = (UINT2)(u2VlanId / MST_PORTORVLAN_PER_BYTE);\
        u2VlanBitPos  = (UINT2)(u2VlanId % MST_PORTORVLAN_PER_BYTE);\
        if (u2VlanBitPos  == 0) {u2VlanBytePos -= 1;} \
        if(u2VlanBytePos < sizeof(au1VlanArray)) \
        { \
        if ((au1VlanArray[u2VlanBytePos] \
             & gau1MstPortBitMaskMap[u2VlanBitPos]) != 0) {\
            \
            u1Result = MST_TRUE;\
        }\
        else {\
            \
            u1Result = MST_FALSE; \
        } \
       } \
    }\
}
#define MST_IS_MEMBER_FDB(au1FdbArray, u2FdbId, u1Result) \
MST_IS_MEMBER_VLAN(au1FdbArray, u2FdbId, u1Result)

#define AST_INCR_TX_MST_BPDU_COUNT(u2MstInst, u2PortNum) \
   (AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst))->u4NumBpdusTx++;

#define AST_INCR_RX_MST_BPDU_COUNT(u2MstInst, u2PortNum) \
   (AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst))->u4NumBpdusRx++;

#define AST_INCR_INVALID_MST_BPDU_COUNT(u2MstInst, u2PortNum) \
   (AST_GET_PERST_PORT_INFO (u2PortNum, u2MstInst))->u4NumInvalidBpdusRx++;

#define MST_INCR_TC_DETECTED_COUNT(u2PortNum,u2InstanceId) \
       ((AST_GET_PERST_PORT_INFO(u2PortNum, u2InstanceId))->u4TcDetectedCount)++

#define MST_INCR_TC_RX_COUNT(u2PortNum,u2InstanceId) \
       ((AST_GET_PERST_PORT_INFO(u2PortNum, u2InstanceId))->u4TcRcvdCount)++

 #define MST_INCR_RX_PROPOSAL_COUNT(u2PortNum,u2InstanceId) \
        ((AST_GET_PERST_PORT_INFO(u2PortNum, u2InstanceId))->u4ProposalRcvdCount)++

 #define MST_INCR_RX_AGREEMENT_COUNT(u2PortNum,u2InstanceId) \
        ((AST_GET_PERST_PORT_INFO(u2PortNum, u2InstanceId))->u4AgreementRcvdCount)++

#define MST_INCR_TX_PROPOSAL_COUNT(u2PortNum,u2InstanceId) \
       ((AST_GET_PERST_PORT_INFO(u2PortNum, u2InstanceId))->u4ProposalTxCount)++

#define MST_INCR_TX_AGREEMENT_COUNT(u2PortNum,u2InstanceId) \
       ((AST_GET_PERST_PORT_INFO(u2PortNum, u2InstanceId))->u4AgreementTxCount)++

#define MST_CALCULATE_DIGEST arHmac_MD5

/* This macro will be used to compare whether the set of vlans that a port
 * is member of and the set of vlans that are mapped to a particular inst
 * match. If even any one of the vlans match, then it updates u1Result as
 * MST_TRUE
 * */
#define AST_IS_VLANS_MATCH(au1List1, au1List2, u1Result) \
{\
    UINT2 u2ByteIndex = 0;\
        \
        for (u2ByteIndex = 0;\
             u2ByteIndex < MST_VLAN_LIST_SIZE;\
             u2ByteIndex++) {\
             if ((au1List1[u2ByteIndex]) & (au1List2[u2ByteIndex])) \
   {u1Result = MST_TRUE;\
       break;}\
   \
        }\
}


/*!!!!!!!!!!!!!!!!WARNING:- INSTANCE ID SHOULD BE VALIDATED B4 CALLING!!!!! 
 * This macro will update u1Result as MST_TRUE, if the given instance is set
 * in the instance list
 * */
#define AST_IS_INST_SET_IN_LIST(au1InstList, u2MstInst, i4ArraySize, bResult) \
    OSIX_BITLIST_IS_BIT_SET (au1InstList, u2MstInst, i4ArraySize, bResult)

#define AST_SISP_SET_INST_LIST(au1InstList, u2MstInst) \
     OSIX_BITLIST_SET_BIT (au1InstList, u2MstInst, MST_INST_LIST_SIZE) 

#define AST_SISP_RESET_INST_LIST(au1InstList, u2MstInst) \
    OSIX_BITLIST_RESET_BIT (au1InstList, u2MstInst, MST_INST_LIST_SIZE)

#define AST_IS_SISP_LOGICAL(u2PortNum) \
    ((AST_GET_SISP_STATUS (u2PortNum) == MST_TRUE)?MST_TRUE:MST_FALSE)

#define  MST_SISP_INVALID_INST     0xFF

#endif /* _ASTMMACR_H */

