#ifndef _MSMINPWR_H
#define _MSMINPWR_H
PUBLIC INT1
FsMiMstpNpCreateInstance (UINT4 u4ContextId, UINT2 u2InstId);


PUBLIC INT1
FsMiMstpNpAddVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2InstId);


PUBLIC INT1
FsMiMstpNpAddVlanListInstMapping (UINT4 u4ContextId, UINT1 *pu1VlanList, 
                                  UINT2 u2InstId, UINT2 u2NumVlans, 
                                  UINT2 *pu2LastVlan);

PUBLIC INT1
FsMiMstpNpDeleteInstance (UINT4 u4ContextId, UINT2 u2InstId);


PUBLIC INT1
FsMiMstpNpDelVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2InstId);

PUBLIC INT1
FsMiMstpNpDelVlanListInstMapping (UINT4 u4ContextId, UINT1 *pu1VlanList, 
                                  UINT2 u2InstId, UINT2 u2NumVlans, 
                                  UINT2 *pu2LastVlan);
INT4
FsMiMstpNpWrSetInstancePortState (UINT4 u4ContextId, UINT4 u4IfIndex,
                                UINT2 u2InstanceId, UINT1 u1PortState);

VOID
FsMiMstpNpInitHw (UINT4 u4ContextId);


VOID
FsMiMstpNpDeInitHw (UINT4 u4ContextId);

INT1
FsMiMstNpGetPortState (UINT4 u4ContextId, UINT2 u2InstanceId, UINT4 u4IfIndex,
                       UINT1 *pu1Status);
#ifdef MBSM_WANTED
INT1 FsMiMstpMbsmNpInitHw PROTO ((UINT4 , tMbsmSlotInfo *));
#endif
#endif
