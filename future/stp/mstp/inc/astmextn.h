/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmextn.h,v 1.6 2014/04/16 12:51:18 siva Exp $
 *
 * Description: This file contains External Declarations    
 *              for the MSTP Module                        
 *
 *******************************************************************/


#ifndef _ASTMEXTN_H
#define _ASTMEXTN_H


/* TRAP Related */



VOID RstCliPrintTestFnErrorMsg (UINT4 u4ErrCode, UINT1 *pu1Msg, UINT1 **ppu1OutMsg);
extern VOID
arHmac_MD5 (UINT1 *pu1Buf, INT4 i4BufLen, UINT1 *pu1Key, INT4 i4KeyLen,
         UINT1 *pu1Digest);
extern UINT2         gu2AstRecvPort;
#endif /* _ASTMEXTN_H */

