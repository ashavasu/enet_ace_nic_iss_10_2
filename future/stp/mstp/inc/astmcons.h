/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: astmcons.h,v 1.36 2017/12/29 09:31:21 siva Exp $
 *
 * Description: This file contains constant definitions used in 
 *              MSTP Module.                                     
 *
 *******************************************************************/



#ifndef _ASTMCONS_H
#define _ASTMCONS_H

/******************************************************************************/
/*                             Mst Protocol Constants                         */
/******************************************************************************/


#define MST_TRUE                       AST_TRUE
#define MST_FALSE                      AST_FALSE

#define MST_FORCE_STATE_DISABLED       0
#define MST_FORCE_STATE_ENABLED        1

#define MST_PORT_OPER_ENABLED          1
#define MST_PORT_OPER_DISABLED         2

#define MST_EXT_SYS_ID_ENABLED         1
#define MST_EXT_SYS_ID_DISABLED        2

#define MST_PATHCOST_TYPE_16           1
#define MST_PATHCOST_TYPE_32           2

/* HELLO, FWD and AGE are in Centi-Seconds */
#define MST_DEFAULT_BRG_HELLO_TIME     (2 * AST_CENTI_SECONDS)
#define MST_DEFAULT_BRG_FWD_DELAY      (15 * AST_CENTI_SECONDS)
#define MST_DEFAULT_BRG_MAX_AGE        (20 * AST_CENTI_SECONDS)
#define MST_DEFAULT_BRG_TX_LIMIT       6 
#define MST_DEFAULT_BRG_PRIORITY       32768
#define MST_DEFAULT_PORT_PRIORITY      128
#define MST_DEFAULT_FLUSH_INTERVAL     0
#define MST_DEFAULT_FLUSH_IND_THRESHOLD 0
#define MST_DEFAULT_OPT_FLUSH_IND_THRESHOLD 5

#define MST_PORT_ROLE_DISABLED         0x0
#define MST_PORT_ROLE_ALTERNATE        0x1
#define MST_PORT_ROLE_BACKUP           0x2
#define MST_PORT_ROLE_ROOT             0x3
#define MST_PORT_ROLE_DESIGNATED       0x4

/******************************************************************************/
/*                         Port Information SEM Constants                     */
/******************************************************************************/
#define MST_PINFOSM_MAX_STATES            9

#define MST_PINFOSM_STATE_DISABLED        0
#define MST_PINFOSM_STATE_AGED            1
#define MST_PINFOSM_STATE_UPDATE          2
#define MST_PINFOSM_STATE_SUPERIOR        3
#define MST_PINFOSM_STATE_REPEAT          4
#define MST_PINFOSM_STATE_INFERIOR_DESG   5 
#define MST_PINFOSM_STATE_NOT_DESG        6
#define MST_PINFOSM_STATE_CURRENT         7
#define MST_PINFOSM_STATE_RECEIVE         8
/* This will be used for info only and no events will occur in this state */
#define MST_PINFOSM_STATE_OTHER           9

#define MST_PINFOSM_MAX_EVENTS            7
#define MST_PINFOSM_EV_BEGIN              0
#define MST_PINFOSM_EV_BEGIN_CLEARED      1
#define MST_PINFOSM_EV_PORT_ENABLED       2
#define MST_PINFOSM_EV_PORT_DISABLED      3
#define MST_PINFOSM_EV_RCVD_BPDU          4
#define MST_PINFOSM_EV_RCVDINFOWHILE_EXP  5
#define MST_PINFOSM_EV_UPDATEINFO         6 

#define MST_SUPERIOR_DESG_MSG             1
#define MST_REPEATED_DESG_MSG             2
#define MST_INFERIOR_ROOT_ALT_MSG         3
#define MST_PINFOSM_OTHER_MSG             4
#define MST_INFERIOR_DESG_MSG             5
#define MST_OTHER_MSG                     6


/******************************************************************************/
/*                         Port Role Transition SEM Constants                 */
/******************************************************************************/

#define MST_PROLETRSM_MAX_STATES          8

#define MST_PROLETRSM_STATE_INIT_PORT     0
#define MST_PROLETRSM_STATE_DISABLE_PORT   1
#define MST_PROLETRSM_STATE_DISABLED_PORT  2
#define MST_PROLETRSM_STATE_ROOT_PORT      3
#define MST_PROLETRSM_STATE_DESIG_PORT     4
#define MST_PROLETRSM_STATE_ALTERNATE_PORT 5
#define MST_PROLETRSM_STATE_MASTER_PORT    6
#define MST_PROLETRSM_STATE_BLOCK_PORT     7

#define MST_PROLETRSM_MAX_EVENTS          14
#define MST_PROLETRSM_EV_BEGIN            0
#define MST_PROLETRSM_EV_FDWHILE_EXPIRED  1 
#define MST_PROLETRSM_EV_RRWHILE_EXPIRED  2
#define MST_PROLETRSM_EV_RBWHILE_EXPIRED  3
#define MST_PROLETRSM_EV_SYNC_SET         4
#define MST_PROLETRSM_EV_REROOT_SET       5
#define MST_PROLETRSM_EV_SELECTED_SET     6
#define MST_PROLETRSM_EV_UPDTINFO_SET     MST_PROLETRSM_EV_SELECTED_SET
#define MST_PROLETRSM_EV_PROPOSED_SET     7 
#define MST_PROLETRSM_EV_AGREED_SET       8 
#define MST_PROLETRSM_EV_ALLSYNCED_SET    9 
#define MST_PROLETRSM_EV_REROOTED_SET     10
#define MST_PROLETRSM_EV_OPEREDGE_SET     11
#define MST_PROLETRSM_EV_DISPUTED_SET     12
#define MST_PROLETRSM_EV_LEARNING_FWDING_RESET     13

/******************************************************************************/
/*                         Port Receive SEM Constants                         */
/******************************************************************************/

#define MST_PRCVSM_MAX_STATES                 2
#define MST_PRCVSM_STATE_DISCARD              0
#define MST_PRCVSM_STATE_RECEIVE              1

#define MST_PRCVSM_MAX_EVENTS                 4
#define MST_PRCVSM_EV_BEGIN                   0
#define MST_PRCVSM_EV_RCVD_BPDU               1
#define MST_PRCVSM_EV_PORT_ENABLED            2
#define MST_PRCVSM_EV_NOT_RCVD_ANY_MSG        3

/******************************************************************************/
/*                         Topology Change SEM Constants                      */
/******************************************************************************/

#define MST_TOPOCHSM_MAX_STATES              8
#define MST_TOPOCHSM_STATE_INACTIVE          0
#define MST_TOPOCHSM_STATE_LEARNING          1
#define MST_TOPOCHSM_STATE_DETECTED          2
#define MST_TOPOCHSM_STATE_ACTIVE            3
#define MST_TOPOCHSM_STATE_NOTIFIED_TCN      4
#define MST_TOPOCHSM_STATE_NOTIFIED_TC       5
#define MST_TOPOCHSM_STATE_PROPAGATING       6
#define MST_TOPOCHSM_STATE_ACKNOWLEDGED      7


#define MST_TOPOCHSM_MAX_EVENTS              10
#define MST_TOPOCHSM_EV_BEGIN                0
#define MST_TOPOCHSM_EV_LEARN_SET            1
#define MST_TOPOCHSM_EV_NOT_DESG_ROOT        2
#define MST_TOPOCHSM_EV_FORWARD              3 
#define MST_TOPOCHSM_EV_OPEREDGE_RESET       4
#define MST_TOPOCHSM_EV_OPEREDGE_SET         5
#define MST_TOPOCHSM_EV_RCVDTC               6
#define MST_TOPOCHSM_EV_RCVDTCN              7
#define MST_TOPOCHSM_EV_RCVDTCACK            8
#define MST_TOPOCHSM_EV_TCPROP               9

/******************************************************************************/
/*                         General Constants related to MSTP                  */
/******************************************************************************/
#define MSTP_STD_BIN                       2
#define MSTP_STD_OCT                       8
#define MSTP_STD_SQUAD                     4


#define MST_CONFIG_DIGEST_LEN             16
#define MST_CONFIG_INFO_OFFSET            38
#define MST_CIST_BRGID_OFFSET             76


#define MST_MAX_INST_NUMBER_MSG        27
#define MST_MAX_HOP_COUNT_MSG           28
#define MST_CONFIG_ID_MSG           29
#define MST_REGION_NAME_MSG           30
#define MST_REGION_VERSION_MSG    31
#define MST_INST_VLANID_MAP_MSG    32
#define MST_INST_VLANID_UNMAP_MSG   33
#define MST_INST_VLANLIST_MAP_MSG         34
#define MST_INST_VLANLIST_UNMAP_MSG       35
#define MST_PORT_HELLO_TIME_MSG           36

/* MST INSTANCE MAPPING */
#define MST_INST_MAP_DISABLED              2
#define MST_INST_MAP_ENABLED               1

#define AST_MIN_MST_INSTANCES             1

#define AST_MIN_IEEE_HOP_COUNT            100   /* Hop Count as defined in IEEE standard mib - std1s1ap.mib */
#define AST_MIN_HOP_COUNT                 600
#define AST_MAX_HOP_COUNT                 4000

/* Values defined in IEEE standard mib - std1s1ap.mib */
#define AST_PORT_ROLE_IEEE_ROOT        0x1
#define AST_PORT_ROLE_IEEE_ALTERNATE   0x2
#define AST_PORT_ROLE_IEEE_DESIGNATED  0x3
#define AST_PORT_ROLE_IEEE_BACKUP      0x4

#define AST_PORT_STATE_IEEE_DISABLED   1 
#define AST_PORT_STATE_IEEE_LISTENING  2
#define AST_PORT_STATE_IEEE_LEARNING   3
#define AST_PORT_STATE_IEEE_FORWARDING 4
#define AST_PORT_STATE_IEEE_BLOCKING   5

#define AST_MIN_BRIDGE_PRIORITY           0x0000
#define AST_MAX_BRIDGE_PRIORITY           0xffff

#define MST_MIN_CONFIG_ID                 0
#define MST_MAX_CONFIG_ID                 0xff

#define MST_MIN_CONFIG_NAME_LEN           0
#define MST_MAX_CONFIG_NAME_LEN           MST_CONFIG_NAME_LEN

#define MST_MIN_REGION_VERSION            0
#define MST_MAX_REGION_VERSION            0xffff

#define AST_MIN_PORT_PATH_COST            1
#define AST_MAX_PORT_PATH_COST            200000000

#define AST_MIN_PORT_PRIORITY             0
#define AST_MAX_PORT_PRIORITY             0xff

#define MST_BRIDGE_PRIORITY_SHIFT         12
#define MST_PORT_PRIORITY_SHIFT           4

#define MST_BPDU_CIST_LENGTH              102
#define MST_BPDU_CONFIG_INFO_LENGTH       64
#define MST_BPDU_MSTI_LENGTH              16

#define MST_DEFAULT_MAX_HOPCOUNT          20
#define MST_DEFAULT_CONFIG_ID             0
#define MST_DEFAULT_CONFIG_LEVEL          0

#define MST_INFERIOR_MSG      RST_INFERIOR_MSG              
#define MST_BETTER_MSG        RST_BETTER_MSG         
#define MST_SAME_MSG          RST_SAME_MSG         

#define MST_INFOIS_DISABLED               RST_INFOIS_DISABLED
#define MST_INFOIS_AGED                   RST_INFOIS_AGED
#define MST_INFOIS_MINE                   RST_INFOIS_MINE
#define MST_INFOIS_RECEIVED               RST_INFOIS_RECEIVED
#define MST_INFOIS_INVALID                0 

#define MST_SAME_INFO                     1
#define MST_SUPERIOR_INFO                 2
#define MST_INFERIOR_INFO                 3
#define MST_PRIORITY_SAME                 1
#define MST_PRIORITY_SUPERIOR             2
#define MST_PRIORITY_INFERIOR             3
#define MST_AGREEMENT_FLAG_MASK           0x0f00
#define MST_PROPOSAL_FLAG_MASK            0xf000 
#define AST_BRGID1_SUPERIOR               RST_BRGID1_SUPERIOR
#define AST_BRGID1_INFERIOR               RST_BRGID1_INFERIOR
#define AST_BRGID1_SAME                   RST_BRGID1_SAME

#define AST_MIN_BPDU_LENGTH_MST           102
#define AST_BPDU_LENGTH_MST             \
(AST_MAX_MST_INSTANCES > 64 ? (MST_BPDU_CIST_LENGTH + (MST_BPDU_MSTI_LENGTH * AST_MAX_MST_INSTANCES)) : 1500)

#define MST_MAX_BPDU_SIZE                 AST_BPDU_LENGTH_MST 

#define MST_SET_CONFIG_TOPOCH_ACK_FLAG    0x80
#define MST_SET_MST_TOPOCH_ACK_FLAG       0x00
#define MST_SET_AGREEMENT_FLAG            0x40
#define MST_SET_FORWARDING_FLAG           0x20
#define MST_SET_LEARNING_FLAG             0x10
#define MST_SET_DISABLED_PROLE_FLAG       0x00
#define MST_SET_ALTBACK_PROLE_FLAG        0x04
#define MST_SET_ROOT_PROLE_FLAG           0x08
#define MST_SET_DESG_PROLE_FLAG           0x0C
#define MST_SET_PROPOSAL_FLAG             0x02
#define MST_SET_TOPOCH_FLAG               0x01
#define MST_SET_MASTER_PROLE_FLAG         0x00
#define MST_SET_MASTER_FLAG               0x80
#define MST_FLAG_MASK_PORT_ROLE           0x0C

#define MST_RESET_FORWARD_FLAG            0xDF
#define MST_RESET_LEARN_FLAG              0xEF
#define MST_MIN_VLAN_ID                   VLAN_DEV_MIN_VLAN_ID

#define MST_RCVD_TC                       1
#define MST_RCVD_TCACK                    2
#define MST_RCVD_TCN                      3

#define MST_FLAG_MASK_MASTER              0x80         
#define MST_FLAG_MASK_TC                  0x01         
#define MST_FLAG_MASK_TCACK               0x80

#define MST_BIT8                          0x80
#define MST_VLANMAP_LIST_SIZE             128
#define MST_VLAN_LIST_SIZE                ((VLAN_DEV_MAX_VLAN_ID + 31)/32 * 4)
#define MST_VLANS_RANGE_1K                1024
#define MST_VLANS_RANGE_2K                2048
#define MST_VLANS_RANGE_3K                3072
#define MST_VLANS_RANGE_4K                4094

#define MST_VLANS_INSTANCE_1K             1
#define MST_VLANS_INSTANCE_2K             2
#define MST_VLANS_INSTANCE_3K             3
#define MST_VLANS_INSTANCE_4K             4
#define MST_VLANS_INSTANCE_8K             8

#define MST_VERSION3LENGTH_FIELD_SIZE     2
/* New CLI constants */
#define MST_DEFAULT_MAX_INST              AST_MAX_MST_INSTANCES - 1
#define MST_FDB_LIST_SIZE                 MST_VLAN_LIST_SIZE
#define MST_FDB_PER_BYTE                  MST_PORTORVLAN_PER_BYTE

/* SISP constants */
#define MST_INST_LIST_SIZE                ((AST_MAX_MST_INSTANCES + 31)/32 * 4)

#endif /* _ASTMCONS_H */

