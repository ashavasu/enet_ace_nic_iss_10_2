/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpmslw.h,v 1.26 2017/09/20 06:32:07 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIMstGlobalTrace ARG_LIST((INT4 *));

INT1
nmhGetFsMIMstGlobalDebug ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIMstGlobalTrace ARG_LIST((INT4 ));

INT1
nmhSetFsMIMstGlobalDebug ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIMstGlobalTrace ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMIMstGlobalDebug ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIMstGlobalTrace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIMstGlobalDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIDot1sFutureMstTable. */
INT1
nmhValidateIndexInstanceFsMIDot1sFutureMstTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIDot1sFutureMstTable  */

INT1
nmhGetFirstIndexFsMIDot1sFutureMstTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDot1sFutureMstTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIMstSystemControl ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstModuleStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstMaxMstInstanceNumber ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstNoOfMstiSupported ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstMaxHopCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstBrgAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsMIMstCistRoot ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIMstCistRegionalRoot ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIMstCistRootCost ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistRegionalRootCost ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistRootPort ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistBridgePriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistBridgeMaxAge ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistBridgeForwardDelay ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistHoldTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistMaxAge ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistForwardDelay ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstpUpCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstMstpDownCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstTrace ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstDebug ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstForceProtocolVersion ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstTxHoldCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstiConfigIdSel ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstiRegionName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIMstMstiRegionVersion ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstiConfigDigest ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIMstBufferOverFlowCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstMemAllocFailureCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstRegionConfigChangeCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistBridgeRoleSelectionSemState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistTimeSinceTopologyChange ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistTopChanges ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistNewRootBridgeCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistHelloTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistBridgeHelloTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistDynamicPathcostCalculation ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstClearBridgeStats ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstContextName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIMstCalcPortPathCostOnSpeedChg ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortClearStats ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstRcvdEvent ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstRcvdEventSubType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstRcvdEventTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstPortStateChangeTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstFlushInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistFlushIndicationThreshold ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistTotalFlushCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstBpduGuard ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsMIMstStpPerfStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstInstPortsMap ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIMstSystemControl ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstModuleStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstMaxMstInstanceNumber ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstMaxHopCount ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistBridgePriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistBridgeMaxAge ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistBridgeForwardDelay ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstTrace ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstDebug ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstForceProtocolVersion ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstTxHoldCount ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstMstiConfigIdSel ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstMstiRegionName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIMstMstiRegionVersion ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistBridgeHelloTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistDynamicPathcostCalculation ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCalcPortPathCostOnSpeedChg ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistPortClearStats ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistPortBpduGuard ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistPortErrorRecovery ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistPortRootGuard ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstClearBridgeStats ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstFlushInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistFlushIndicationThreshold ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstBpduGuard ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstStpPerfStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstInstPortsMap ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIMstSystemControl ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstModuleStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstMaxMstInstanceNumber ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstMaxHopCount ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistBridgePriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistBridgeMaxAge ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistBridgeForwardDelay ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstTrace ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstDebug ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstForceProtocolVersion ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstTxHoldCount ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstMstiConfigIdSel ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstMstiRegionName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIMstMstiRegionVersion ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistBridgeHelloTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistDynamicPathcostCalculation ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstClearBridgeStats ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCalcPortPathCostOnSpeedChg ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistPortClearStats ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistPortBpduGuard ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistPortErrorRecovery ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistPortRootGuard ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstFlushInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistFlushIndicationThreshold ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstBpduGuard ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstStpPerfStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstInstPortsMap ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1sFutureMstTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIMstMstiBridgeTable. */
INT1
nmhValidateIndexInstanceFsMIMstMstiBridgeTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIMstMstiBridgeTable  */

INT1
nmhGetFirstIndexFsMIMstMstiBridgeTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIMstMstiBridgeTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIMstMstiBridgeRegionalRoot ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIMstMstiBridgePriority ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstiRootCost ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstiRootPort ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstiTimeSinceTopologyChange ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstMstiTopChanges ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstMstiNewRootBridgeCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstMstiBridgeRoleSelectionSemState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstInstanceUpCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstInstanceDownCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstOldDesignatedRoot ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIMstMstiFlushIndicationThreshold ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstiTotalFlushCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstiRootPriority ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIMstMstiBridgePriority ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIMstMstiFlushIndicationThreshold ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIMstiRootPriority ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIMstMstiBridgePriority ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstMstiFlushIndicationThreshold ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstiRootPriority ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIMstMstiBridgeTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIMstVlanInstanceMappingTable. */
INT1
nmhValidateIndexInstanceFsMIMstVlanInstanceMappingTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIMstVlanInstanceMappingTable  */

INT1
nmhGetFirstIndexFsMIMstVlanInstanceMappingTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIMstVlanInstanceMappingTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIMstMapVlanIndex ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstUnMapVlanIndex ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstSetVlanList ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIMstResetVlanList ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIMstInstanceVlanMapped ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIMstInstanceVlanMapped2k ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIMstInstanceVlanMapped3k ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIMstInstanceVlanMapped4k ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIMstMapVlanIndex ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIMstUnMapVlanIndex ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIMstSetVlanList ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIMstResetVlanList ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIMstMapVlanIndex ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstUnMapVlanIndex ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstSetVlanList ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIMstResetVlanList ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIMstVlanInstanceMappingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIMstCistPortTable. */
INT1
nmhValidateIndexInstanceFsMIMstCistPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIMstCistPortTable  */

INT1
nmhGetFirstIndexFsMIMstCistPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIMstCistPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIMstCistPortPathCost ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortDesignatedRoot ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIMstCistPortDesignatedBridge ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIMstCistPortDesignatedPort ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIMstCistPortAdminP2P ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortOperP2P ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortAdminEdgeStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortOperEdgeStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortProtocolMigration ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistForcePortState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortForwardTransitions ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortRxMstBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortRxRstBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortRxConfigBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortRxTcnBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortTxMstBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortTxRstBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortTxConfigBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortTxTcnBpduCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortInvalidMstBpduRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortInvalidRstBpduRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortInvalidConfigBpduRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortInvalidTcnBpduRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortTransmitSemState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortReceiveSemState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortProtMigrationSemState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistProtocolMigrationCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortDesignatedCost ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortRegionalRoot ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIMstCistPortRegionalPathCost ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistSelectedPortRole ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistCurrentPortRole ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortInfoSemState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortRoleTransitionSemState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortStateTransitionSemState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortTopologyChangeSemState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortHelloTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortOperVersion ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortEffectivePortState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortAutoEdgeStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortRestrictedRole ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortRestrictedTCN ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortAdminPathCost ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortEnableBPDURx ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortEnableBPDUTx ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortPseudoRootId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIMstCistPortIsL2Gp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortLoopGuard ARG_LIST((INT4 ,INT4 *));


INT1
nmhGetFsMIMstCistPortRcvdEvent ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortRcvdEventSubType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortRcvdEventTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1 
nmhGetFsMIMstCistLoopInconsistentState ARG_LIST((INT4 ,INT4 *)); 

INT1
nmhGetFsMIMstCistPortBpduGuard ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortErrorRecovery ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstPortBpduInconsistentState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstPortBpduGuardAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortRootGuard ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortRootInconsistentState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstCistPortTCDetectedCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortTCReceivedCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortTCDetectedTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortTCReceivedTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortRcvInfoWhileExpCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortRcvInfoWhileExpTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortProposalPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortProposalPktsRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortProposalPktSentTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortProposalPktRcvdTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortAgreementPktSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortAgreementPktRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortAgreementPktSentTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortAgreementPktRcvdTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortImpStateOccurCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortImpStateOccurTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIMstCistPortOldPortState ARG_LIST((INT4 ,INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIMstCistPortPathCost ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistPortPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistPortAdminP2P ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistPortAdminEdgeStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistPortProtocolMigration ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistForcePortState ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistPortHelloTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistPortAutoEdgeStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistPortRestrictedRole ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistPortRestrictedTCN ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistPortAdminPathCost ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistPortEnableBPDURx ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistPortEnableBPDUTx ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistPortPseudoRootId ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIMstCistPortIsL2Gp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstCistPortLoopGuard ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIMstPortBpduGuardAction ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIMstCistPortPathCost ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistPortPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistPortAdminP2P ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistPortAdminEdgeStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistPortProtocolMigration ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistForcePortState ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistPortHelloTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistPortAutoEdgeStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistPortRestrictedRole ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistPortRestrictedTCN ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistPortAdminPathCost ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistPortEnableBPDURx ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistPortEnableBPDUTx ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistPortPseudoRootId ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIMstCistPortIsL2Gp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstCistPortLoopGuard ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstPortBpduGuardAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIMstCistPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIMstMstiPortTable. */
INT1
nmhValidateIndexInstanceFsMIMstMstiPortTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIMstMstiPortTable  */

INT1
nmhGetFirstIndexFsMIMstMstiPortTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIMstMstiPortTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIMstMstiPortPathCost ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstiPortPriority ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstiPortDesignatedRoot ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIMstMstiPortDesignatedBridge ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIMstMstiPortDesignatedPort ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIMstMstiPortState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstiForcePortState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstiPortForwardTransitions ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstMstiPortReceivedBPDUs ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstMstiPortTransmittedBPDUs ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstMstiPortInvalidBPDUsRcvd ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstMstiPortDesignatedCost ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstiSelectedPortRole ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstiCurrentPortRole ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstiPortInfoSemState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstiPortRoleTransitionSemState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstiPortStateTransitionSemState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstiPortTopologyChangeSemState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstiPortEffectivePortState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstiPortAdminPathCost ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstiPortPseudoRootId ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIMstMstiPortStateChangeTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstMstiPortRootInconsistentState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstiPortLoopInconsistentState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstMstiPortTCDetectedCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstMstiPortTCReceivedCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstMstiPortTCDetectedTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstMstiPortTCReceivedTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstMstiPortProposalPktsSent ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstMstiPortProposalPktsRcvd ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstMstiPortProposalPktSentTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstMstiPortProposalPktRcvdTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstMstiPortAgreementPktSent ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstMstiPortAgreementPktRcvd ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstMstiPortAgreementPktSentTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstMstiPortAgreementPktRcvdTimeStamp ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIMstMstiPortOldPortState ARG_LIST((INT4  , INT4 ,INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIMstMstiPortPathCost ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIMstMstiPortPriority ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIMstMstiForcePortState ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIMstMstiPortAdminPathCost ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIMstMstiPortPseudoRootId ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIMstMstiPortPathCost ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstMstiPortPriority ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstMstiForcePortState ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstMstiPortAdminPathCost ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstMstiPortPseudoRootId ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIMstMstiPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIMstPortExtTable. */

INT1
nmhValidateIndexInstanceFsMIMstPortExtTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIMstPortExtTable  */

INT1
nmhGetFirstIndexFsMIMstPortExtTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIMstPortExtTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIMstPortRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIMstPortRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIMstPortRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIMstPortExtTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDot1sFsMstSetGlobalTrapOption ARG_LIST((INT4 *));

INT1
nmhGetFsMIMstGlobalErrTrapType ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDot1sFsMstSetGlobalTrapOption ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDot1sFsMstSetGlobalTrapOption ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1sFsMstSetGlobalTrapOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIDot1sFsMstTrapsControlTable. */
INT1
nmhValidateIndexInstanceFsMIDot1sFsMstTrapsControlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIDot1sFsMstTrapsControlTable  */

INT1
nmhGetFirstIndexFsMIDot1sFsMstTrapsControlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDot1sFsMstTrapsControlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIMstSetTraps ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstGenTrapType ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIMstSetTraps ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIMstSetTraps ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1sFsMstTrapsControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIMstPortTrapNotificationTable. */
INT1
nmhValidateIndexInstanceFsMIMstPortTrapNotificationTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIMstPortTrapNotificationTable  */

INT1
nmhGetFirstIndexFsMIMstPortTrapNotificationTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIMstPortTrapNotificationTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIMstPortMigrationType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstPktErrType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIMstPktErrVal ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIMstPortRoleTrapNotificationTable. */
INT1
nmhValidateIndexInstanceFsMIMstPortRoleTrapNotificationTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIMstPortRoleTrapNotificationTable  */

INT1
nmhGetFirstIndexFsMIMstPortRoleTrapNotificationTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIMstPortRoleTrapNotificationTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIMstPortRoleType ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIMstOldRoleType ARG_LIST((INT4  , INT4 ,INT4 *));


INT1
nmhGetFsMIMstMstiClearBridgeStats ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhSetFsMIMstMstiClearBridgeStats ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstMstiClearBridgeStats ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhGetFsMIMstMstiPortClearStats ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhSetFsMIMstMstiPortClearStats ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIMstMstiPortClearStats ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));
