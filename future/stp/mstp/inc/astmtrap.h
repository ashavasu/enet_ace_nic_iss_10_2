/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: astmtrap.h,v 1.14 2017/09/20 06:32:06 siva Exp $
 *
 * Description: All macros and structure used for traps for the MSTP module
 *
 *******************************************************************/
#ifndef _MST_TRAP_H_
#define _MST_TRAP_H_

/* BRG */
#define AST_MST_TRAP_TYPE (AST_CURR_CONTEXT_INFO ())->u4ContextTrapOption

/* Type code for TRAP */
#define MST_BRG_GEN_TRAP_VAL 1 
#define MST_BRG_ERR_TRAP_VAL 2
#define MST_NEW_ROOT_TRAP_VAL 3
#define MST_TOPOLOGY_CHG_TRAP_VAL 4
#define MST_PROTOCOL_MIGRATION_TRAP_VAL 5
#define MST_INVALID_BPDU_RXD_TRAP_VAL 6
#define MST_REGION_CONFIG_CHANGE_TRAP_VAL 7
#define MST_NEW_PORTROLE_TRAP_VAL 8
#define MST_CIST_HW_FAIL_TRAP 9
#define MST_MSTI_HW_FAIL_TRAP 10
#define MST_CIST_LOOP_INCSTATE_TRAP 11
#define MST_MSTI_LOOP_INCSTATE_TRAP 12
#define MST_CIST_BPDU_INCSTATE_TRAP 13
#define MST_CIST_ROOT_INCSTATE_TRAP 14
#define MST_MSTI_ROOT_INCSTATE_TRAP 15
#define MST_CIST_STATE_CHG_TRAP 16
#define MST_MSTI_STATE_CHG_TRAP 17

#define AST_INST_UP_TRAP   3
#define AST_INST_DOWN_TRAP 4

/* Type code for abnormal trap type */

/* type code for protocol migration trap */
#define AST_TRAP_SEND_MSTP 2   

#define AST_MST_TRAP_MAX  3
#define AST_MST_TRAPS_OID_LEN  10 
#define IS_AST_MST_GEN_EVNT() ((AST_CURR_CONTEXT_INFO ())->u4TrapOption & 0x00000001) 
#define IS_AST_MST_ERR_EVNT() ((AST_CURR_CONTEXT_INFO ())->u4TrapOption & 0x00000002)


typedef struct _AstMstRegionConfigChangeTrap{
  UINT1         au1RegionName[MST_CONFIG_NAME_LEN];
  UINT1         au1ConfigDigest[MST_CONFIG_DIGEST_LEN];
  tAstMacAddr   BrgAddr;
  UINT2         u2RevisionLevel;
  UINT1         u1ConfigId;
  UINT1         au1Reserved[3];
} tAstMstRegionConfigChangeTrap;    

#define MST_MIB_OBJ_CONTEXT_NAME        "fsMIMstContextName"

#define MST_MIB_OBJ_BASE_BRIDGE_ADDR_MI    "fsMIMstBrgAddress"
#define MST_MIB_OBJ_GEN_TRAP_TYPE_MI       "fsMIMstGenTrapType"
#define MST_MIB_OBJ_ERR_TRAP_TYPE_MI       "fsMIMstGlobalErrTrapType"
#define MST_MIB_OBJ_MSTI_INST_INDEX_MI     "fsMIMstTrapMstiInstanceIndex"
#define MST_MIB_OBJ_OLD_DESIG_ROOT_MI      "fsMIMstOldDesignatedRoot"
#define MST_MIB_OBJ_MSTI_REG_ROOT_MI       "fsMIMstMstiBridgeRegionalRoot"
#define MST_MIB_OBJ_PORT_TRAP_INDEX_MI     "fsMIMstPortTrapIndex"
#define MST_MIB_OBJ_FORCE_PROT_VERSION_MI  "fsMIMstForceProtocolVersion"
#define MST_MIB_OBJ_PORT_MIGRATION_TYPE_MI "fsMIMstPortMigrationType"
#define MST_MIB_OBJ_PKT_ERR_TYPE_MI        "fsMIMstPktErrType"
#define MST_MIB_OBJ_PKT_ERR_VAL_MI         "fsMIMstPktErrVal"
#define MST_MIB_OBJ_MSTI_CONFIG_ID_SEL_MI  "fsMIMstMstiConfigIdSel"
#define MST_MIB_OBJ_MSTI_REGION_NAME_MI    "fsMIMstMstiRegionName"
#define MST_MIB_OBJ_MSTI_REGION_VERSION_MI "fsMIMstMstiRegionVersion"
#define MST_MIB_OBJ_MSTI_CONFIG_DIGEST_MI  "fsMIMstMstiConfigDigest"
#define MST_MIB_OBJ_MSTI_INST_MI           "fsMIMstInstanceIndex"
#define MST_MIB_OBJ_PREVIOUS_ROLE_MI       "fsMIMstOldRoleType" 
#define MST_MIB_OBJ_SELECTED_ROLE_MI       "fsMIMstPortRoleType"
#define MST_MIB_OBJ_TOPOLOGY_CHGS_MI       "fsMIMstMstiTopChanges"
#define MST_MIB_OBJ_INSTUP_COUNT_MI        "fsMIMstInstanceUpCount"
#define MST_MIB_OBJ_INSTDOWN_COUNT_MI      "fsMIMstInstanceDownCount"
#define MST_MIB_OBJ_CIST_PORT_STATE_MI     "fsMIMstCistPortState"
#define MST_MIB_OBJ_MSTI_PORT_STATE_MI     "fsMIMstMstiPortState"
#define MST_MIB_OBJ_CIST_LOOP_INCSTATE_MI  "fsMIMstCistLoopInconsistentState"
#define MST_MIB_OBJ_MSTI_LOOP_INCSTATE_MI  "fsMIMstMstiPortLoopInconsistentState"
#define MST_MIB_OBJ_CIST_ROOT_INCSTATE_MI  "fsMIMstCistPortRootInconsistentState"
#define MST_MIB_OBJ_MSTI_ROOT_INCSTATE_MI  "fsMIMstMstiPortRootInconsistentState"
#define MST_MIB_OBJ_CIST_BPDU_INCSTATE_MI  "fsMIMstPortBpduInconsistentState"
#define MST_MIB_OBJ_CIST_OLDPORT_STATE_MI  "fsMIMstCistPortOldPortState"
#define MST_MIB_OBJ_MSTI_OLDPORT_STATE_MI  "fsMIMstMstiPortOldPortState"
#define MST_MIB_OBJ_BASE_BRIDGE_ADDR_SI    "fsMstBrgAddress"
#define MST_MIB_OBJ_GEN_TRAP_TYPE_SI       "fsMstGenTrapType"
#define MST_MIB_OBJ_ERR_TRAP_TYPE_SI       "fsMstErrTrapType"
#define MST_MIB_OBJ_MSTI_INST_INDEX_SI     "fsMstTrapMstiInstance"
#define MST_MIB_OBJ_OLD_DESIG_ROOT_SI      "fsMstOldDesignatedRoot"
#define MST_MIB_OBJ_MSTI_REG_ROOT_SI       "fsMstMstiBridgeRegionalRoot"
#define MST_MIB_OBJ_PORT_TRAP_INDEX_SI     "fsMstPortTrapIndex"
#define MST_MIB_OBJ_FORCE_PROT_VERSION_SI  "fsMstForceProtocolVersion"
#define MST_MIB_OBJ_PORT_MIGRATION_TYPE_SI "fsMstPortMigrationType"
#define MST_MIB_OBJ_PKT_ERR_TYPE_SI        "fsMstPktErrType"
#define MST_MIB_OBJ_PKT_ERR_VAL_SI         "fsMstPktErrVal"
#define MST_MIB_OBJ_MSTI_CONFIG_ID_SEL_SI  "fsMstMstiConfigIdSel"
#define MST_MIB_OBJ_MSTI_REGION_NAME_SI    "fsMstMstiRegionName"
#define MST_MIB_OBJ_MSTI_REGION_VERSION_SI "fsMstMstiRegionVersion"
#define MST_MIB_OBJ_MSTI_CONFIG_DIGEST_SI  "fsMstMstiConfigDigest"
#define MST_MIB_OBJ_MSTI_INST_SI           "fsMstInstanceIndex"
#define MST_MIB_OBJ_PREVIOUS_ROLE_SI       "fsMstOldRoleType" 
#define MST_MIB_OBJ_SELECTED_ROLE_SI       "fsMstPortRoleType"
#define MST_MIB_OBJ_TOPOLOGY_CHGS_SI       "fsMstMstiTopChanges"
#define MST_MIB_OBJ_INSTUP_COUNT_SI        "fsMstInstanceUpCount"
#define MST_MIB_OBJ_INSTDOWN_COUNT_SI      "fsMstInstanceDownCount"
#define MST_MIB_OBJ_CIST_PORT_STATE_SI     "fsMstCistPortState"
#define MST_MIB_OBJ_MSTI_PORT_STATE_SI     "fsMstMstiPortState"
#define MST_MIB_OBJ_CIST_LOOP_INCSTATE_SI  "fsMstCistPortLoopInconsistentState"
#define MST_MIB_OBJ_MSTI_LOOP_INCSTATE_SI  "fsMstMstiPortLoopInconsistentState"
#define MST_MIB_OBJ_CIST_ROOT_INCSTATE_SI  "fsMstCistPortRootInconsistentState"
#define MST_MIB_OBJ_MSTI_ROOT_INCSTATE_SI  "fsMstMstiPortRootInconsistentState"
#define MST_MIB_OBJ_CIST_BPDU_INCSTATE_SI  "fsMstPortBpduInconsistentState"
#define MST_MIB_OBJ_CIST_OLDPORT_STATE_SI  "fsMstCistPortOldPortState"
#define MST_MIB_OBJ_MSTI_OLDPORT_STATE_SI  "fsMstMstiPortOldPortState"

#define MST_MIB_OBJ_BASE_BRIDGE_ADDR \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  MST_MIB_OBJ_BASE_BRIDGE_ADDR_SI:  MST_MIB_OBJ_BASE_BRIDGE_ADDR_MI)

#define MST_MIB_OBJ_GEN_TRAP_TYPE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  MST_MIB_OBJ_GEN_TRAP_TYPE_SI:  MST_MIB_OBJ_GEN_TRAP_TYPE_MI)

#define MST_MIB_OBJ_ERR_TRAP_TYPE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  MST_MIB_OBJ_ERR_TRAP_TYPE_SI:  MST_MIB_OBJ_ERR_TRAP_TYPE_MI)

#define MST_MIB_OBJ_MSTI_INST_INDEX \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  MST_MIB_OBJ_MSTI_INST_INDEX_SI:  MST_MIB_OBJ_MSTI_INST_INDEX_MI)

#define MST_MIB_OBJ_OLD_DESIG_ROOT \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  MST_MIB_OBJ_OLD_DESIG_ROOT_SI:  MST_MIB_OBJ_OLD_DESIG_ROOT_MI)

#define MST_MIB_OBJ_MSTI_REG_ROOT \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  MST_MIB_OBJ_MSTI_REG_ROOT_SI:  MST_MIB_OBJ_MSTI_REG_ROOT_MI)

#define MST_MIB_OBJ_PORT_TRAP_INDEX \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  MST_MIB_OBJ_PORT_TRAP_INDEX_SI:  MST_MIB_OBJ_PORT_TRAP_INDEX_MI)

#define MST_MIB_OBJ_FORCE_PROT_VERSION \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  MST_MIB_OBJ_FORCE_PROT_VERSION_SI:  MST_MIB_OBJ_FORCE_PROT_VERSION_MI)

#define MST_MIB_OBJ_PORT_MIGRATION_TYPE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  MST_MIB_OBJ_PORT_MIGRATION_TYPE_SI:  MST_MIB_OBJ_PORT_MIGRATION_TYPE_MI) 

#define MST_MIB_OBJ_PKT_ERR_TYPE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  MST_MIB_OBJ_PKT_ERR_TYPE_SI:  MST_MIB_OBJ_PKT_ERR_TYPE_MI)

#define MST_MIB_OBJ_PKT_ERR_VAL \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  MST_MIB_OBJ_PKT_ERR_VAL_SI:  MST_MIB_OBJ_PKT_ERR_VAL_MI)

#define MST_MIB_OBJ_MSTI_CONFIG_ID_SEL \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  MST_MIB_OBJ_MSTI_CONFIG_ID_SEL_SI:  MST_MIB_OBJ_MSTI_CONFIG_ID_SEL_MI)
 
#define MST_MIB_OBJ_MSTI_REGION_NAME \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  MST_MIB_OBJ_MSTI_REGION_NAME_SI:  MST_MIB_OBJ_MSTI_REGION_NAME_MI)

#define MST_MIB_OBJ_MSTI_REGION_VERSION \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  MST_MIB_OBJ_MSTI_REGION_VERSION_SI:  MST_MIB_OBJ_MSTI_REGION_VERSION_MI)

#define MST_MIB_OBJ_MSTI_CONFIG_DIGEST \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  MST_MIB_OBJ_MSTI_CONFIG_DIGEST_SI:  MST_MIB_OBJ_MSTI_CONFIG_DIGEST_MI)

#define MST_MIB_OBJ_MSTI_INST \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ? MST_MIB_OBJ_MSTI_INST_SI:  MST_MIB_OBJ_MSTI_INST_MI)

#define MST_MIB_OBJ_PREVIOUS_ROLE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  MST_MIB_OBJ_PREVIOUS_ROLE_SI:  MST_MIB_OBJ_PREVIOUS_ROLE_MI)

#define MST_MIB_OBJ_SELECTED_ROLE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  MST_MIB_OBJ_SELECTED_ROLE_SI:  MST_MIB_OBJ_SELECTED_ROLE_MI)

#define MST_MIB_OBJ_TOPOLOGY_CHGS \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  MST_MIB_OBJ_TOPOLOGY_CHGS_SI:  MST_MIB_OBJ_TOPOLOGY_CHGS_MI)

#define MST_MIB_OBJ_INSTUP_COUNT \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  MST_MIB_OBJ_INSTUP_COUNT_SI:  MST_MIB_OBJ_INSTUP_COUNT_MI)

#define MST_MIB_OBJ_INSTDOWN_COUNT \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  MST_MIB_OBJ_INSTDOWN_COUNT_SI:  MST_MIB_OBJ_INSTDOWN_COUNT_MI)

#define MST_MIB_OBJ_CIST_PORT_STATE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  MST_MIB_OBJ_CIST_PORT_STATE_SI:  MST_MIB_OBJ_CIST_PORT_STATE_MI)

#define MST_MIB_OBJ_MSTI_PORT_STATE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?  MST_MIB_OBJ_MSTI_PORT_STATE_SI:  MST_MIB_OBJ_MSTI_PORT_STATE_MI)

#define MST_MIB_OBJ_CIST_LOOP_INCSTATE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ? MST_MIB_OBJ_CIST_LOOP_INCSTATE_SI: MST_MIB_OBJ_CIST_LOOP_INCSTATE_MI)

#define MST_MIB_OBJ_MSTI_LOOP_INCSTATE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ? MST_MIB_OBJ_MSTI_LOOP_INCSTATE_SI: MST_MIB_OBJ_MSTI_LOOP_INCSTATE_MI)

#define MST_MIB_OBJ_CIST_ROOT_INCSTATE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ? MST_MIB_OBJ_CIST_ROOT_INCSTATE_SI: MST_MIB_OBJ_CIST_ROOT_INCSTATE_MI) 

#define MST_MIB_OBJ_MSTI_ROOT_INCSTATE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ? MST_MIB_OBJ_MSTI_ROOT_INCSTATE_SI: MST_MIB_OBJ_MSTI_ROOT_INCSTATE_MI)

#define MST_MIB_OBJ_CIST_BPDU_INCSTATE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ? MST_MIB_OBJ_CIST_BPDU_INCSTATE_SI: MST_MIB_OBJ_CIST_BPDU_INCSTATE_MI)

#define MST_MIB_OBJ_CIST_OLDPORT_STATE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ? MST_MIB_OBJ_CIST_OLDPORT_STATE_SI: MST_MIB_OBJ_CIST_OLDPORT_STATE_MI)

#define MST_MIB_OBJ_MSTI_OLDPORT_STATE \
   ((VcmGetSystemModeExt (AST_PROTOCOL_ID) == VCM_SI_MODE) ?MST_MIB_OBJ_MSTI_OLDPORT_STATE_SI: MST_MIB_OBJ_MSTI_OLDPORT_STATE_MI)

#endif

