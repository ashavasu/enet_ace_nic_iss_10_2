/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsssldb.h,v 1.6 2012/01/04 09:57:04 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSSSLDB_H
#define _FSSSLDB_H


UINT4 fsssl [] ={1,3,6,1,4,1,2076,96};
tSNMP_OID_TYPE fssslOID = {8, fsssl};


UINT4 SslSecureHttpStatus [ ] ={1,3,6,1,4,1,2076,96,1,2};
UINT4 SslPort [ ] ={1,3,6,1,4,1,2076,96,1,3};
UINT4 SslTrace [ ] ={1,3,6,1,4,1,2076,96,1,4};
UINT4 SslVersion [ ] ={1,3,6,1,4,1,2076,96,1,5};
UINT4 SslCipherList [ ] ={1,3,6,1,4,1,2076,96,2,1};
UINT4 SslDefaultCipherList [ ] ={1,3,6,1,4,1,2076,96,2,2};




tMbDbEntry fssslMibEntry[]= {

{{10,SslSecureHttpStatus}, NULL, SslSecureHttpStatusGet, SslSecureHttpStatusSet, SslSecureHttpStatusTest, SslSecureHttpStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,SslPort}, NULL, SslPortGet, SslPortSet, SslPortTest, SslPortDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "443"},

{{10,SslTrace}, NULL, SslTraceGet, SslTraceSet, SslTraceTest, SslTraceDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,SslVersion}, NULL, SslVersionGet, SslVersionSet, SslVersionTest, SslVersionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{10,SslCipherList}, NULL, SslCipherListGet, SslCipherListSet, SslCipherListTest, SslCipherListDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "76"},

{{10,SslDefaultCipherList}, NULL, SslDefaultCipherListGet, SslDefaultCipherListSet, SslDefaultCipherListTest, SslDefaultCipherListDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData fssslEntry = { 6, fssslMibEntry };

#endif /* _FSSSLDB_H */

