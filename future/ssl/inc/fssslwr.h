/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssslwr.h,v 1.4 2011/02/17 05:32:32 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

#ifndef _FSSSLWR_H
#define _FSSSLWR_H

VOID RegisterFSSSL(VOID);

VOID UnRegisterFSSSL(VOID);
INT4 SslSecureHttpStatusGet(tSnmpIndex *, tRetVal *);
INT4 SslPortGet(tSnmpIndex *, tRetVal *);
INT4 SslTraceGet(tSnmpIndex *, tRetVal *);
INT4 SslVersionGet(tSnmpIndex *, tRetVal *);
INT4 SslSecureHttpStatusSet(tSnmpIndex *, tRetVal *);
INT4 SslPortSet(tSnmpIndex *, tRetVal *);
INT4 SslTraceSet(tSnmpIndex *, tRetVal *);
INT4 SslVersionSet(tSnmpIndex *, tRetVal *);
INT4 SslSecureHttpStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SslPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SslTraceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SslVersionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SslSecureHttpStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 SslPortDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 SslTraceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 SslVersionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 SslCipherListGet(tSnmpIndex *, tRetVal *);
INT4 SslDefaultCipherListGet(tSnmpIndex *, tRetVal *);
INT4 SslCipherListSet(tSnmpIndex *, tRetVal *);
INT4 SslDefaultCipherListSet(tSnmpIndex *, tRetVal *);
INT4 SslCipherListTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SslDefaultCipherListTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SslCipherListDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 SslDefaultCipherListDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _FSSSLWR_H */
