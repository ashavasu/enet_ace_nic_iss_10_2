/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsssllw.h,v 1.4 2011/02/17 05:32:32 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSslSecureHttpStatus ARG_LIST((INT4 *));

INT1
nmhGetSslPort ARG_LIST((INT4 *));

INT1
nmhGetSslTrace ARG_LIST((INT4 *));

INT1
nmhGetSslVersion ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetSslSecureHttpStatus ARG_LIST((INT4 ));

INT1
nmhSetSslPort ARG_LIST((INT4 ));

INT1
nmhSetSslTrace ARG_LIST((INT4 ));

INT1
nmhSetSslVersion ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2SslSecureHttpStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2SslPort ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2SslTrace ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2SslVersion ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2SslSecureHttpStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SslPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SslTrace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SslVersion ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSslCipherList ARG_LIST((INT4 *));

INT1
nmhGetSslDefaultCipherList ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetSslCipherList ARG_LIST((INT4 ));

INT1
nmhSetSslDefaultCipherList ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2SslCipherList ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2SslDefaultCipherList ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2SslCipherList ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SslDefaultCipherList ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
