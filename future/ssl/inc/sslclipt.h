/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: sslclipt.h,v 1.4 2011/02/17 05:32:32 siva Exp $
 *
 * Description: This has prototype definitions for SSL CLI submodule
 *
 ***********************************************************************/

#ifndef __SSLCLIPT_H__
#define __SSLCLIPT_H__

/* Prototype declarations for SSL CLI */

INT4 SslCliGenCertReq (tCliHandle CliHandle, UINT1 *pu1SubjectName);
INT4 SslCliServerCert (tCliHandle CliHandle, INT1 *pi1ServerCert);
INT4 SslCliSetCipherList (tCliHandle CliHandle, INT4 i4SslCipherList, UINT1 u1Flag);
INT4 SslCliTrace (tCliHandle CliHandle, INT4 i4TraceLevel, UINT1 u1TraceFlag);
INT4 SslCliSetVersion (tCliHandle CliHandle, INT4 i4Version);
INT4 SslCliGenRsaKeyPair (tCliHandle CliHandle, INT4 i4RsaKeys);
INT4 SslCliHttpsStatus (tCliHandle CliHandle, INT4 i4HttpsStatus);
INT4 SslCliShowServerCert (tCliHandle CliHandle);
INT4 SslCliShowServerStatus (tCliHandle CliHandle);
INT4 SslShowRunningConfig (tCliHandle CliHandle);
VOID IssSslShowDebugging (tCliHandle CliHandle);
/*extern VOID  SslCtrlGenRsaKey(tSSLCtrlMsg *pSslCtrlMsg);*/

#endif /* __SSLCLIPT_H__ */
