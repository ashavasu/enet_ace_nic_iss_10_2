/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: sslapi.h,v 1.10 2015/01/02 12:05:51 siva Exp $
*
* Description: contains the Proto types 
*********************************************************************/

#ifndef __SSLAPI_H__
#define __SSLAPI_H__

/*#ifdef OPENSSL_WANTED
#include <openssl/evp.h>
typedef EVP_PKEY tEvpPkey;
#else
typedef VOID tEvpPkey;
#endif 
*/

INT1 SSLInit (VOID);
INT1 SSLStart (VOID);
signed char SSLFlush (void *pSslConnId);
VOID * SSLAccept (INT4 i4SockFd);
VOID * SSLConnect (INT4 i4SockFd);
INT1
SSLRead (VOID *pSslConnId, UINT1 *pu1Buf, UINT4 *pu4NumBytes);
INT1
SSLWrite (VOID *pConnId, UINT1 *pu1Buf, UINT4 *pu4NumBytes);
INT1
SSLWriteImmediate (VOID *pConnId, UINT1 *pu1Buf, UINT4 *pu4NumBytes);
VOID
SSLLibInit(VOID);
VOID
SSLClose (VOID *pSslConnId);
VOID
SSLProcessCtrlMsgs (VOID);
VOID
SSLClearSessions (VOID);
INT4 SSLGetCtrlSockFd (VOID);
UINT2 SSLGetHttpsPort (VOID);
UINT2 SSLGetHttpsVersion (VOID);
unsigned short
SSLGetCfgCipherList (void);
char
SSLIsServerCertConfigured (void);
signed char
SSLSetCtrlMsgCipherList (int i4Cipherlist);
signed char
SSLSetCtrlMsgHttpsStatus (int i4SslSecureHttpStatus);
signed char
SSLSetCtrlMsgSslPort (int i4SslPort);
signed char
SSLSetCtrlMsgSslVersion (int i4SslVersion);
void
SSLGenRsaKeyPair (int i4RsaKeys);
void
SslResetRsaKeyPair (void);
signed char
SSLGenCertReq (unsigned char *pu1SubjectName, unsigned char **pu1CertReq);
signed char
SSLGetServerCert (unsigned char **ppu1ASCII,
                  unsigned int *pu4ASCIILen, signed char *pi1CertStatus);
signed char
SSLGetDerServerCert (unsigned char **ppu1Cert,
                  int *pi4CertLen);
signed char
SSLSetServerCert (signed char *pi1ServerCert);
signed char
SSLRsaSignReq (int u4Len, unsigned char *pu1Sha1Digest, unsigned char *pu1RsaSign, UINT4 *u4SignLen);
signed char
SSLRsaSignVer (void *, int u4Len, unsigned char *pu1RsaInSign, unsigned char *pu1MsgDigest, UINT4 *u4DigestLen);
signed char
SSLGetDerRsaPubKey (unsigned char **pu1DerPubKey, unsigned short *pu2DerLen);
signed char
SSLGetRsaPubKey (const unsigned char **ppu1DerPubKey, int pu2DerLen, void **);
signed char
SSLGetMaxRsaSignSize (const unsigned char **ppu1DerPubKey, int pu2DerLen, int *pu4MaxSignLen);
signed char
SSLServerCertNameVerify (const unsigned char **pu1DerName, int i4TAOptLen);
int
SSLGetTrustAnchorName (unsigned char **ppu1DerTAName, int *pDerLen);
int
SSLVerifyCertificate(const unsigned char **ppu1DerCert, int i4CertLen);
void SSLRsaFree (void *pRsa);
unsigned short
SSLGetRsaBits (void);
int
SSLCheckRsaKey (void);
unsigned int
SSLGetSslTrace (void);
void
SSLSetSslTrace (int i4SslTrace);
signed char
SslSaveServerCert (void);
void
SSLRestoreCert (void);
int SSLSetFipsMode (void);
int SSLFipsSelfTestRsa (void);
int SSLFipsSelfTestDsa (void);
#endif
