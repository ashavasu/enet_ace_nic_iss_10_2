/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: sslcmn.h,v 1.7 2015/01/02 12:05:51 siva Exp $
*
* Description: contains the header file inclusions and macro definitions
*********************************************************************/

#ifndef __SSLCMN_H__
#define __SSLCMN_H__

#include "lr.h"
#include "cust.h"
#include "httpssl.h"
#include "rmgr.h"

#define SSL_SNMP_TRUE                      1
#define SSL_SNMP_FALSE                     2


#define SSL_STATUS_DISABLED                2


#define SSL_SERV_SSL3_VERSION                 0x0300
#define SSL_SERV_TLS1_VERSION                 0x0301


#define SSL_MAX_CIPHERS              11
#define SSL_MAX_CIPHER_LIST_LENGTH   256

/* Standard SSL port */
#define SSL_STD_PORT                    443
#define SSL_MAX_NAME_LENGTH             64


#define SSL_MEM_MALLOC          MEM_MALLOC
#define SSL_MEM_FREE            MEM_FREE
#define SSL_BZERO(ptr,len)      MEMSET(ptr,0,len)
#define SSL_STRCPY              STRCPY
#define SSL_STRNCPY             STRNCPY
#define SSL_MEMCPY              MEMCPY
#define SSL_SPRINTF             SPRINTF
#define SSL_STRLEN              STRLEN

#define SSL_HTONS               OSIX_HTONS
#define SSL_HTONL               OSIX_HTONL
#define SSL_NTOHS               OSIX_NTOHS
#define SSL_NTOHL               OSIX_NTOHL
/* Trace related definitions */
#define SSL_DISABLE                     0
#define SSL_ENABLE                      1
#define SSL_INIT_SHUT                   2
#define SSL_MGMT                        3
#define SSL_DATA_PATH                   4
#define SSL_CNTRL_PATH                  5
#define SSL_DUMP                        6
#define SSL_OS_RESOURCE                 7
#define SSL_ALL_FAILURE                 8
#define SSL_BUFFER                      9


/*#ifdef TRACE_WANTED   FS_SSL*/
#define SSL_MOD_TRC  UtlTrcLog
/*#else
 * #define SSL_MOD_TRC
 * #endif*/

/* Control Message handler related definitions */
#define SSL_INVALID_SOCKET              -1
#define SSL_LOOPBACK_ADDR               0x7f000001
#define SSL_CTRL_PORT                   9000

#define SSL_MAX_CERT_REQ_LEN            4096
#define SSL_SUBJ_NAME                   16

/* RFC suggests 24 hrs., but OpenSSL suggests using 2 hrs. as
 *  * 24 hrs is too long
 *   */
#define SSL_SESSION_LIFETIME            7200 /* 2 hrs */

#define SSL_ERR_MSG_SIZE                128
#define SSL_DISPLAY_SIZE                256
#define SSL_UNUSED_PARAM(x)             x=x

#define SSL_READ_WAIT_TIMEOUT             10
#define SSL_MAX_WRITE_RETRIES              5
#define SSL_STATUS_DISABLED                2

#define DEFAULT_CIPHERLIST                 76
#define RSA512                             512
#define RSA1024                            1024
#define SSL_SNMP_TRUE                      1
#define SSL_SNMP_FALSE                     2

#define SSL_CTRL_SET_PORT              1
#define SSL_CTRL_SET_SERVER_CERT       2
#define SSL_CTRL_SET_CIPHER_LIST       3
#define SSL_CTRL_GEN_RSAKEY            4
#define SSL_CTRL_GEN_CERT_REQ          5
#define SSL_CTRL_HTTPS_STATUS          6
#define SSL_CTRL_TIMER_EXPIRY          7
#define SSL_CTRL_SET_VERSION           8
#define SSL_MAX_CERT_SIZE               4096



#endif
