/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsssllw.c,v 1.11 2015/08/20 12:36:57 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsssllw.h"
#include   "sslcli.h"
#include "cust.h"
#include "iss.h"
#include "sslcmn.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSslSecureHttpStatus
 Input       :  The Indices

                The Object 
                retValSslSecureHttpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSslSecureHttpStatus (INT4 *pi4RetValSslSecureHttpStatus)
{
#ifdef WEBNM_WANTED
    HttpGetHttpsStatus (pi4RetValSslSecureHttpStatus);
    if (*pi4RetValSslSecureHttpStatus == SSL_FALSE)
    {
        *pi4RetValSslSecureHttpStatus = SSL_STATUS_DISABLED;
    }
#else
    *pi4RetValSslSecureHttpStatus = SSL_STATUS_DISABLED;
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSslPort
 Input       :  The Indices

                The Object 
                retValSslPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSslPort (INT4 *pi4RetValSslPort)
{
    *pi4RetValSslPort = SslArGetHttpsPort ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSslTrace
 Input       :  The Indices

                The Object 
                retValSslTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSslTrace (INT4 *pi4RetValSslTrace)
{
    *pi4RetValSslTrace = SslArGetSslTrace ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSslVersion
Input       :  The Indices
                The Object
                retValSslVersion
  Output      :  The Get Low Lev Routine Take the Indices &
                    store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSslVersion (INT4 *pi4RetValSslVersion)
{
    *pi4RetValSslVersion = SslArGetHttpsVersion ();
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetSslSecureHttpStatus
 Input       :  The Indices

                The Object 
                setValSslSecureHttpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSslSecureHttpStatus (INT4 i4SetValSslSecureHttpStatus)
{
    if (i4SetValSslSecureHttpStatus == SSL_STATUS_DISABLED)
    {
        i4SetValSslSecureHttpStatus = SSL_FALSE;
    }

    if (SslArSetCtrlMsgHttpsStatus (i4SetValSslSecureHttpStatus) == SSL_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSslPort
 Input       :  The Indices
 
 
                The Object 
                setValSslPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSslPort (INT4 i4SetValSslPort)
{
    if (SslArSetCtrlMsgSslPort (i4SetValSslPort) == SSL_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetSslTrace
 Input       :  The Indices

                The Object 
                setValSslTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSslTrace (INT4 i4SetValSslTrace)
{
    SslArSetSslTrace (i4SetValSslTrace);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSslVersion
Input       :  The Indices

                The Object
                setValSslVersion
Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSslVersion (INT4 i4SetValSslVersion)
{
    if (SslArSetCtrlMsgSslVersion (i4SetValSslVersion) == SSL_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2SslSecureHttpStatus
Input       :  The Indices

               The Object 
               testValSslSecureHttpStatus
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SslSecureHttpStatus (UINT4 *pu4ErrorCode,
                              INT4 i4TestValSslSecureHttpStatus)
{
    if ((i4TestValSslSecureHttpStatus != TRUE) &&
        (i4TestValSslSecureHttpStatus != SSL_STATUS_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SSL_INV_HTTPS_STATUS);
        return SNMP_FAILURE;
    }
    if ((SslArIsServerCertConfigured () == SSL_FALSE) &&
        (i4TestValSslSecureHttpStatus == TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_SSL_SRV_CERT_NOT_CONFIG);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SslPort
 Input       :  The Indices

                The Object 
                testValSslPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SslPort (UINT4 *pu4ErrorCode, INT4 i4TestValSslPort)
{
    if ((i4TestValSslPort <= 0) || (i4TestValSslPort > 65535))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SslTrace
 Input       :  The Indices

                The Object 
                testValSslTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SslTrace (UINT4 *pu4ErrorCode, INT4 i4TestValSslTrace)
{
    if ((i4TestValSslTrace < SSL_DISABLE_ALL_TRC_MASK) ||
        (i4TestValSslTrace > SSL_ENABLE_ALL_TRC_MASK))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SSL_INVALID_TRC);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2SslVersion
Input       :  The Indices

                The Object
                testValSslVersion
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
                Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SslVersion (UINT4 *pu4ErrorCode, INT4 i4TestValSslVersion)
{
    if ((i4TestValSslVersion != SSL_VERSION_ALL) &&
        (i4TestValSslVersion != SSL_VERSION_SSLV3) &&
        (i4TestValSslVersion != SSL_VERSION_TLSV1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (i4TestValSslVersion != SSL_VERSION_TLSV1)
    {
        if (SSL_CALLBACK_FN[SSL_CUST_MODE_CHK].pSslCustCheckCustMode != NULL)
        {
            if (SSL_CALLBACK_FN[SSL_CUST_MODE_CHK].pSslCustCheckCustMode ()
                == SSL_TRUE)

            {
                return SNMP_FAILURE;
            }
        }

    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2SslSecureHttpStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SslSecureHttpStatus (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SslPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SslPort (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SslTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SslTrace (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhDepv2SslVersion
Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SslVersion (UINT4 *pu4ErrorCode,
                    tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSslCipherList
 Input       :  The Indices

                The Object 
                retValSslCipherList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSslCipherList (INT4 *pi4RetValSslCipherList)
{
    *pi4RetValSslCipherList = SslArGetCfgCipherList ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSslDefaultCipherList
 Input       :  The Indices

                The Object 
                retValSslDefaultCipherList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSslDefaultCipherList (INT4 *pi4RetValSslDefaultCipherList)
{
    *pi4RetValSslDefaultCipherList = SSL_SNMP_FALSE;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetSslCipherList
 Input       :  The Indices

                The Object 
                setValSslCipherList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSslCipherList (INT4 i4SetValSslCipherList)
{
    if ((SslArSetCtrlMsgCipherList (i4SetValSslCipherList)) == SSL_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetSslDefaultCipherList
 Input       :  The Indices

                The Object 
                setValSslDefaultCipherList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSslDefaultCipherList (INT4 i4SetValSslDefaultCipherList)
{
    INT4                i4DefCipherList = 0;

    if (i4SetValSslDefaultCipherList == SSL_SNMP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    i4DefCipherList = (TLS_RSA_3DES_SHA1 | TLS_RSA_DES_SHA1 |
                       TLS_RSA_EXP1024_DES_SHA1);

    if ((SslArSetCtrlMsgCipherList (i4DefCipherList)) == SSL_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2SslCipherList
 Input       :  The Indices

                The Object 
                testValSslCipherList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SslCipherList (UINT4 *pu4ErrorCode, INT4 i4TestValSslCipherList)
{
    /* Here the maximum value of Cipher-list is checked agains 0x7ff. This is because,
     * the cipher list is 10-bits wide and the equivalent hex. when all the bits are
     * set is 0x7ff.
     */

    if ((i4TestValSslCipherList <= 0) || (i4TestValSslCipherList > 0x7ff))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SSL_INV_CIPHER_LIST);
        return SNMP_FAILURE;
    }

    if (SSL_CALLBACK_FN[SSL_CUST_MODE_CHK].pSslCustCheckCustMode != NULL)
    {
        if (SSL_CALLBACK_FN[SSL_CUST_MODE_CHK].pSslCustCheckCustMode () ==
            SSL_TRUE)
        {

            if ((i4TestValSslCipherList & TLS_RSA_NULL_MD5) ||
                (i4TestValSslCipherList & TLS_RSA_NULL_SHA1) ||
                (i4TestValSslCipherList & TLS_RSA_DES_SHA1) ||
                (i4TestValSslCipherList & TLS_DHE_RSA_DES_SHA1) ||
                (i4TestValSslCipherList & TLS_RSA_3DES_SHA1) ||
                (i4TestValSslCipherList & TLS_DHE_RSA_3DES_SHA1) ||
                (i4TestValSslCipherList & TLS_RSA_EXP1024_DES_SHA1))
            {
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SslDefaultCipherList
 Input       :  The Indices

                The Object 
                testValSslDefaultCipherList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SslDefaultCipherList (UINT4 *pu4ErrorCode,
                               INT4 i4TestValSslDefaultCipherList)
{
    if ((i4TestValSslDefaultCipherList != SSL_SNMP_TRUE)
        && (i4TestValSslDefaultCipherList != SSL_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2SslCipherList
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SslCipherList (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SslDefaultCipherList
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SslDefaultCipherList (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
