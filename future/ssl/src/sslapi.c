/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: sslapi.c,v 1.17 2015/02/11 10:40:50 siva Exp $
*
* Description: contains the functions for API's
*********************************************************************/

#include "sslcmn.h"
#include "sslapi.h"
#include "cust.h"
#include "httpssl.h"
unSslCallBackEntry  gaSslCallBack[SSL_MAX_CALLBACK_EVENTS];
INT1
SslArInit (VOID)
{
#ifdef OPENSSL_WANTED
    if (SSLInit () == SSL_FAILURE)
    {
        return (SSL_FAILURE);
    }
    return SSL_SUCCESS;
#else
    return SSL_SUCCESS;
#endif
}

INT1
SslArStart (VOID)
{
#ifdef OPENSSL_WANTED
    if (SSLStart () == SSL_FAILURE)
    {
        return (SSL_FAILURE);
    }
    return SSL_SUCCESS;
#else
    return SSL_SUCCESS;
#endif
}

INT1
SslArFlush (VOID *pSslConnId)
{
#ifdef OPENSSL_WANTED
    if (SSLFlush (pSslConnId) == SSL_FAILURE)
    {
        return (SSL_FAILURE);
    }
    return SSL_SUCCESS;
#else
    UNUSED_PARAM (pSslConnId);
    return SSL_SUCCESS;
#endif
}

VOID               *
SslArAccept (INT4 i4SockFd)
{
#ifdef OPENSSL_WANTED
    return (SSLAccept (i4SockFd));

#else
    UNUSED_PARAM (i4SockFd);
    return 0;
#endif
}

VOID
SslArLibInit (VOID)
{
#ifdef OPENSSL_WANTED
    return (SSLLibInit ());

#else
    return;
#endif
}

VOID               *
SslArConnect (INT4 i4SockFd)
{
#ifdef OPENSSL_WANTED
    return (SSLConnect (i4SockFd));

#else
    UNUSED_PARAM (i4SockFd);
    return 0;
#endif
}

INT1
SslArRead (VOID *pSslConnId, UINT1 *pu1Buf, UINT4 *pu4NumBytes)
{
#ifdef OPENSSL_WANTED
    return (SSLRead (pSslConnId, pu1Buf, pu4NumBytes));

#else
    UNUSED_PARAM (pSslConnId);
    UNUSED_PARAM (pu1Buf);
    UNUSED_PARAM (pu4NumBytes);
    return SSL_SUCCESS;
#endif
}

INT1
SslArWrite (VOID *pConnId, UINT1 *pu1Buf, UINT4 *pu4NumBytes)
{
#ifdef OPENSSL_WANTED
    return (SSLWrite (pConnId, pu1Buf, pu4NumBytes));

#else
    UNUSED_PARAM (pConnId);
    UNUSED_PARAM (pu1Buf);
    UNUSED_PARAM (pu4NumBytes);
    return SSL_SUCCESS;
#endif

}

INT1
SslArWriteImmediate (VOID *pConnId, UINT1 *pu1Buf, UINT4 *pu4NumBytes)
{
#ifdef OPENSSL_WANTED
    return (SSLWriteImmediate (pConnId, pu1Buf, pu4NumBytes));

#else
    UNUSED_PARAM (pConnId);
    UNUSED_PARAM (pu1Buf);
    UNUSED_PARAM (pu4NumBytes);
    return SSL_SUCCESS;
#endif

}

VOID
SslArClose (VOID *pSslConnId)
{
#ifdef OPENSSL_WANTED
    SSLClose (pSslConnId);

#else
    UNUSED_PARAM (pSslConnId);
    return;
#endif

}

VOID
SslArProcessCtrlMsgs (VOID)
{
#ifdef OPENSSL_WANTED
    SSLProcessCtrlMsgs ();

#else
    return;
#endif

}

VOID
SslArClearSessions (VOID)
{
#ifdef OPENSSL_WANTED
    SSLClearSessions ();

#else
    return;
#endif

}

INT4
SslArGetCtrlSockFd (VOID)
{
#ifdef OPENSSL_WANTED
    return (SSLGetCtrlSockFd ());

#else
    return 0;
#endif

}

UINT2
SslArGetHttpsPort (VOID)
{
#ifdef OPENSSL_WANTED
    return (SSLGetHttpsPort ());

#else
    return 0;
#endif
}

UINT2
SslArGetCfgCipherList (VOID)
{
#ifdef OPENSSL_WANTED
    return (SSLGetCfgCipherList ());

#else
    return 0;
#endif
}

BOOLEAN
SslArIsServerCertConfigured (VOID)
{
#ifdef OPENSSL_WANTED
    return (SSLIsServerCertConfigured ());

#else
    return 0;
#endif

}

INT1
SslArSetCtrlMsgCipherList (INT4 i4Cipherlist)
{
#ifdef OPENSSL_WANTED
    return (SSLSetCtrlMsgCipherList (i4Cipherlist));

#else
    UNUSED_PARAM (i4Cipherlist);
    return SSL_SUCCESS;
#endif

}

INT1
SslArSetCtrlMsgHttpsStatus (INT4 i4SslHttpsStatus)
{
#ifdef OPENSSL_WANTED
    return (SSLSetCtrlMsgHttpsStatus (i4SslHttpsStatus));

#else
    UNUSED_PARAM (i4SslHttpsStatus);
    return SSL_SUCCESS;
#endif

}

INT1
SslArSetCtrlMsgSslPort (INT4 i4SslPort)
{
#ifdef OPENSSL_WANTED
    return (SSLSetCtrlMsgSslPort (i4SslPort));

#else
    UNUSED_PARAM (i4SslPort);
    return SSL_SUCCESS;
#endif
}

UINT2
SslArGetHttpsVersion (VOID)
{
#ifdef OPENSSL_WANTED
    return (SSLGetHttpsVersion ());
#else
    return 0;
#endif
}

INT1
SslArSetCtrlMsgSslVersion (INT4 i4SslVersion)
{
#ifdef OPENSSL_WANTED
    return (SSLSetCtrlMsgSslVersion (i4SslVersion));
#else
    UNUSED_PARAM (i4SslVersion);
    return SSL_SUCCESS;
#endif
}

VOID
SslArGenRsaKeyPair (INT4 i4RsaKeys)
{
#ifdef OPENSSL_WANTED
    SSLGenRsaKeyPair (i4RsaKeys);

#else
    UNUSED_PARAM (i4RsaKeys);
    return;
#endif
}

VOID
SslArResetRsaKeyPair (VOID)
{
#ifdef OPENSSL_WANTED
    SslResetRsaKeyPair ();
#else
    return;
#endif
}

INT1
SslArGenCertReq (UINT1 *pu1SubjectName, UINT1 **ppu1CertReq)
{
#ifdef OPENSSL_WANTED
    return (SSLGenCertReq (pu1SubjectName, ppu1CertReq));

#else
    UNUSED_PARAM (ppu1CertReq);
    UNUSED_PARAM (pu1SubjectName);
    return SSL_SUCCESS;
#endif
}

INT1
SslArGetServerCert (UINT1 **ppu1ASCII, UINT4 *pu4ASCIILen, INT1 *i1CertStatus)
{
#ifdef OPENSSL_WANTED
    return (SSLGetServerCert (ppu1ASCII, pu4ASCIILen, i1CertStatus));

#else
    UNUSED_PARAM (ppu1ASCII);
    UNUSED_PARAM (pu4ASCIILen);
    UNUSED_PARAM (i1CertStatus);
    return SSL_SUCCESS;
#endif
}

INT1
SslArGetDerServerCert (UINT1 **ppu1Cert, INT4 *pi4CertLen)
{
#ifdef OPENSSL_WANTED
    return (SSLGetDerServerCert (ppu1Cert, pi4CertLen));

#else
    UNUSED_PARAM (ppu1Cert);
    UNUSED_PARAM (pi4CertLen);
    return SSL_FAILURE;
#endif
}

INT1
SslArSetServerCert (INT1 *pi1ServerCert)
{
#ifdef OPENSSL_WANTED
    return (SSLSetServerCert (pi1ServerCert));

#else
    UNUSED_PARAM (pi1ServerCert);
    return SSL_SUCCESS;
#endif
}

UINT4
SslArGetSslTrace (VOID)
{
#ifdef OPENSSL_WANTED
    return (SSLGetSslTrace ());
#else
    return 0;
#endif
}

VOID
SslArSetSslTrace (INT4 i4SslTrace)
{
#ifdef OPENSSL_WANTED
    SSLSetSslTrace (i4SslTrace);
#else
    UNUSED_PARAM (i4SslTrace);
    return;
#endif
}

UINT2
SslArGetRsaBits (VOID)
{
#ifdef OPENSSL_WANTED
    return (SSLGetRsaBits ());

#else
    return 0;
#endif

}

INT1
SslArSaveServerCert (VOID)
{
#ifdef OPENSSL_WANTED
    if (SSL_SUCCESS == SslSaveServerCert ())
	{
	   SslRmEnqCertGenMsgToRm ();
	   return SSL_SUCCESS;
	}
	return SSL_FAILURE;
#else
    return SSL_SUCCESS;
#endif

}

INT1
SslArCheckRsaKey (VOID)
{
#ifdef OPENSSL_WANTED
    return ((INT1)SSLCheckRsaKey ());

#else
    return SSL_SUCCESS;
#endif

}

/*****************************************************************************/
/* Function Name      : SslRegisterCallBackforSSLModule                      */
/*                                                                           */
/* Description        : This function is invoked to register the call backs  */
/*                      from  application.                                   */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                      pFsCbInfo - Call Back Function                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SSL__SUCCESS/SSL__FAILURE                            */
/*****************************************************************************/

INT4
SslRegisterCallBackforSSLModule (UINT4 u4Event, tFsCbInfo * pFsCbInfo)
{
    INT4                i4RetVal = SSL_SUCCESS;
    switch (u4Event)
    {
        case SSL_CUST_MODE_CHK:
            SSL_CALLBACK_FN[u4Event].pSslCustCheckCustMode =
                pFsCbInfo->pIssCustCheckSslCustMode;
            break;
        case SSL_CUST_CERTIFICATE_PROCESS:
            SSL_CALLBACK_FN[u4Event].pSslCustCertificateProcess =
                pFsCbInfo->pIssCustSecureProcess;
            break;
        default:
            i4RetVal = SSL_FAILURE;
            break;
    }
    return i4RetVal;
}

/******************************************************************************/
/*  Function Name   : SslArSetFipsMode                                        */
/*  Description     : This function is a wrapper function to                  */
/*                    enable FIPS mode in crypto library                      */
/*  Input(s)        : None                                                    */
/*  Output(s)       : None                                                    */
/*  Returns         : SSL_FAILURE / SSL_SUCCESS                               */
/******************************************************************************/

INT4
SslArSetFipsMode (VOID)
{
#ifdef OPENSSL_WANTED
    if (SSLSetFipsMode () != SSL_SUCCESS)
    {
        return SSL_FAILURE;
    }
#endif

    return SSL_SUCCESS;
}

/******************************************************************************/
/*  Function Name   : SslArSelfTestRsa                                        */
/*  Description     : This function is a wrapper function to                  */
/*                    invoke FIPS supported RSA self test                     */
/*  Input(s)        : None                                                    */
/*  Output(s)       : None                                                    */
/*  Returns         : SSL_FAILURE / SSL_SUCCESS                               */
/******************************************************************************/

INT4
SslArSelfTestRsa (VOID)
{
#ifdef OPENSSL_WANTED
    if (SSLFipsSelfTestRsa () != SSL_SUCCESS)
    {
        return SSL_FAILURE;
    }
#endif

    return SSL_SUCCESS;
}

/******************************************************************************/
/*  Function Name   : SslArSelfTestDsa                                        */
/*  Description     : This function is a wrapper function to                  */
/*                    invoke FIPS supported DSA self test                     */
/*  Input(s)        : None                                                    */
/*  Output(s)       : None                                                    */
/*  Returns         : SSL_FAILURE / SSL_SUCCESS                               */
/******************************************************************************/

INT4
SslArSelfTestDsa (VOID)
{
#ifdef OPENSSL_WANTED
    if (SSLFipsSelfTestDsa () != SSL_SUCCESS)
    {
        return SSL_FAILURE;
    }
#endif
    return SSL_SUCCESS;
}

/********************************************************************
 * DESCRIPTION : This function is a wrapper to function which 
 *               generates the signature for digest passed.
 *
 * INPUTS      : u4DigestLen - Length of the diget
 *               pu1Sha1Digest - Pointer to the digest
 *
 * OUTPUTS     : pu1RsaSign - Pointer to the RSA signature
 *               pu4SignLen - Pointer to the Signature length
 *
 * RETURNS     : SSL_SUCCESS/SSL_FAILURE
 *******************************************************************/
INT1
SslArRsaSignReq (UINT4 u4DigestLen, UINT1 *pu1Sha1Digest,
                 UINT1 *pu1RsaSign, UINT4 *pu4SignLen)
{
#ifdef OPENSSL_WANTED
        return (SSLRsaSignReq ((INT4) u4DigestLen, pu1Sha1Digest,
                pu1RsaSign, pu4SignLen));

#else
    UNUSED_PARAM (u4DigestLen);
    UNUSED_PARAM (pu1Sha1Digest);
    UNUSED_PARAM (pu1RsaSign);
    UNUSED_PARAM (pu4SignLen);
    return SSL_SUCCESS;
#endif
}

/********************************************************************
 * DESCRIPTION : This function is a wrapper to function which 
 *               verifies the signature for digest passed.
 *
 * INPUTS      : pRsa - Pointer to Rsa
 *               u4SignLen - Length of the signature length
 *               pu1RsaSign - Pointer to the RSA signature
 *               pu1Sha1Digest - Pointer to the digest
 *               pu4DigestLen - Pointer to the digest length
 *
 * OUTPUTS     : pu1RsaSign - Pointer to the RSA signature
 *
 * RETURNS     : SSL_SUCCESS/SSL_FAILURE
 *******************************************************************/
INT1
SslArRsaSignVer (VOID *pRsa, UINT4 u4SignLen, UINT1 *pu1RsaSign,
                 UINT1 *pu1Sha1Digest, UINT4 *pu4DigestLen)
{
#ifdef OPENSSL_WANTED
            return (SSLRsaSignVer (pRsa, u4SignLen, pu1RsaSign,
                    pu1Sha1Digest, pu4DigestLen));

#else
    UNUSED_PARAM (pRsa);
    UNUSED_PARAM (u4SignLen);
    UNUSED_PARAM (pu1RsaSign);
    UNUSED_PARAM (pu1Sha1Digest);
    UNUSED_PARAM (pu4DigestLen);
    return SSL_SUCCESS;
#endif
}

/********************************************************************
 * DESCRIPTION : This function is a wrapper to function which gets
 *               the DER encoded public key 
 *
 * INPUTS      : None
 *
 * OUTPUTS     : ppu1DerPubKey - Address of the pointer for DER
 *                               encoded public key
 *               pu2DerLen - Pointer to the DER encoded key length
 *
 * RETURNS     : SSL_SUCCESS/SSL_FAILURE
 *******************************************************************/
INT1
SslArGetDerRsaPubKey (UINT1 **ppu1DerPubKey, UINT2 *pu2DerLen)
{
#ifdef OPENSSL_WANTED
    return (SSLGetDerRsaPubKey (ppu1DerPubKey, pu2DerLen));
#else
    UNUSED_PARAM (ppu1DerPubKey);
    UNUSED_PARAM (pu2DerLen);
    return SSL_SUCCESS;
#endif
}

/********************************************************************
 * DESCRIPTION : This function is a wrapper to function which gets
 *               the decoded public key from DER encoding.
 *
 * INPUTS      : ppu1DerPubKey - Address of the pointer for DER
 *                               encoded public key
 *               u4DerLen - DER encoded key length
 *
 * OUTPUTS     : ppRsa - Address of the pointer to RSA 
 *
 * RETURNS     : SSL_SUCCESS/SSL_FAILURE
 *******************************************************************/
INT1
SslArGetRsaPubKey (UINT1 **ppu1DerPubKey, UINT4 u4DerLen,
                   VOID **ppRsa)
{
#ifdef OPENSSL_WANTED
    return (SSLGetRsaPubKey ((CONST UINT1 **) (VOID **)ppu1DerPubKey, 
                             (INT4) u4DerLen, ppRsa));
#else
    UNUSED_PARAM (ppu1DerPubKey);
    UNUSED_PARAM (u4DerLen);
    UNUSED_PARAM (ppRsa);
    return SSL_SUCCESS;
#endif
}

/********************************************************************
 * DESCRIPTION : This function is a wrapper to function which gets
 *               maximum RSA signature
 *
 * INPUTS      : ppu1DerPubKey - Address of the pointer for DER
 *                               encoded public key
 *               u4DerLen - DER encoded key length
 *
 * OUTPUTS     : pi4MaxSignLen - Pointer to Maximum signature length
 *
 * RETURNS     : SSL_SUCCESS/SSL_FAILURE
 *******************************************************************/
INT1
SslArGetMaxRsaSignSize (UINT1 **ppu1DerPubKey, UINT4 u4DerLen,
                        INT4 *pi4MaxSignLen)
{
#ifdef OPENSSL_WANTED
    return (SSLGetMaxRsaSignSize ((CONST UINT1 ** ) (VOID **)ppu1DerPubKey,
                                  (INT4)u4DerLen, pi4MaxSignLen));
#else
    UNUSED_PARAM (u4DerLen);
    UNUSED_PARAM (ppu1DerPubKey);
    UNUSED_PARAM (pi4MaxSignLen);
    return SSL_SUCCESS;
#endif
}

/********************************************************************
 * DESCRIPTION : This function is a wrapper to function which 
 *               verifies whether the TA name is same as OpenSSL 
 *
 * INPUTS      : pu1DerName - Pointer for DER encoded name
 *
 * OUTPUTS     : None 
 *
 * RETURNS     : SSL_SUCCESS/SSL_FAILURE
 *******************************************************************/
INT1
SSLArTACertNameVerify (UINT1 **pu1DerName, INT4 i4TAOptLen)
{
#ifdef OPENSSL_WANTED
        return (SSLServerCertNameVerify (
               (const unsigned char **) (VOID **)pu1DerName,
		        i4TAOptLen));
#else
    UNUSED_PARAM (i4TAOptLen);
    UNUSED_PARAM (pu1DerName);
    return SSL_FAILURE;
#endif
}

/********************************************************************
 * DESCRIPTION : Wrapper function to release the memory allocated by
 *               OpenSSL 
 *
 * INPUTS      : pMem - Pointer to the memory to be released
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *******************************************************************/
VOID
SslArMemFree (UINT1 *pMem)
{
#ifdef OPENSSL_WANTED
    SSL_MEM_FREE (pMem);
#else
    UNUSED_PARAM (pMem);
#endif
}

/********************************************************************
 * DESCRIPTION : Wrapper function to release the RSA memory 
 *
 * INPUTS      : pRsa - Pointer to RSA
 *
 * OUTPUTS     : None 
 *
 * RETURNS     : None 
 *******************************************************************/
void
SSLArRsaFree (void *pRsa)
{
#ifdef OPENSSL_WANTED
    SSLRsaFree(pRsa);
#else
    UNUSED_PARAM (pRsa);
#endif
}

/********************************************************************
 * DESCRIPTION : This function is a wrapper to function which
 *               verifies whether the TA name is same as OpenSSL
 *
 * INPUTS      : pu1DerName - Pointer for DER encoded name
 *
 * OUTPUTS     : None
 *
 * RETURNS     : SSL_SUCCESS/SSL_FAILURE
 *******************************************************************/
INT1
SSLArGetDerTAName (UINT1 **ppu1DerTAName, INT4 *pi4DerLen)
{
#ifdef OPENSSL_WANTED
        return ((INT1) SSLGetTrustAnchorName (ppu1DerTAName, pi4DerLen));

#else
    UNUSED_PARAM (pi4DerLen);
    UNUSED_PARAM (ppu1DerTAName);
    return SSL_SUCCESS;
#endif
}

/********************************************************************
 * DESCRIPTION : This function is a wrapper to function which
 *               verifies whether the TA name is same as OpenSSL
 *
 * INPUTS      : pu1DerName - Pointer for DER encoded name
 *
 * OUTPUTS     : None
 *
 * RETURNS     : SSL_SUCCESS/SSL_FAILURE
 *******************************************************************/
INT1
SSLArVerifyCert (UINT1 **ppu1DerCert, INT4 i4CertLen)
{
#ifdef OPENSSL_WANTED
        return ((INT1) SSLVerifyCertificate (
               (const unsigned char **)(VOID **)ppu1DerCert, 
	            i4CertLen));

#else
    UNUSED_PARAM (i4CertLen);
    UNUSED_PARAM (ppu1DerCert);
    return SSL_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : SslRmEnqCertGenMsgToRm                                 */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SSL_SUCCESS/SSL_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT4
SslRmEnqCertGenMsgToRm (VOID)
{
#ifdef RM_WANTED
    if ((RmEnqCertGenMsgToRmFromApp ()) == RM_FAILURE)
    {
        return SSL_FAILURE;
    }
	return SSL_SUCCESS;
#else
    return SSL_SUCCESS;
#endif
}

/********************************************************************
 * DESCRIPTION : Wrapper function to restore the RSA key and Certificate
 *
 * INPUTS      : None 
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *******************************************************************/
VOID
SSLArRestoreCert (VOID)
{
#ifdef OPENSSL_WANTED
    SSLRestoreCert ();
#endif
}

