/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: sslcli.c,v 1.19 2015/01/02 12:05:51 siva Exp $
 *
 * Description: This has functions for SSL CLI SubModule
 *
 ***********************************************************************/

#ifndef __SSLCLI_C__
#define __SSLCLI_C__

#include "sslcmn.h"
#include "cli.h"
#include "fsssllw.h"
#include "fssslwr.h"
#include "sslcli.h"
#include "sslclipt.h"
#include "fssslcli.h"
#include "iss.h"
static INT1         ai1ServerCert[SSL_MAX_CERT_SIZE + 1];
/*extern tSSLGlbCfg   gSslGlbCfg;*/

/**************************************************************************/
/*  Function Name   : cli_process_ssl_cmd                                 */
/*  Description     : Protocol CLI message handler function               */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    u4Command - Command identifier                      */
/*                    ... -Variable command argument list                 */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
cli_process_ssl_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[SSL_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode;
    UINT4               u4CmdType;
    INT4                i4CipherSuites;
    INT4                i4RespStatus = CLI_SUCCESS;
    INT1                ai1Prompt[] = "Enter Cert: ";
    UINT1              *pu1Inst;

    UNUSED_PARAM (pu1Inst);

    va_start (ap, u4Command);

    /* second arguement is always (interface name/index) NULL for ssl module cmds */
    pu1Inst = va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguements and store in args array. 
     * Store SSL_CLI_MAX_ARGS arguements at the max. 
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == SSL_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    switch (u4Command)
    {
        case CLI_SSL_GEN_CERT_REQ:
        {
            /* args[0] -  Subject name to be used in certificate creation */
            i4RespStatus = SslCliGenCertReq (CliHandle, args[0]);
            break;
        }
        case CLI_SSL_SHOW_SERVER_CERT:
        {
            /* No addition command arguments, so ignore the args[] array */
            i4RespStatus = SslCliShowServerCert (CliHandle);
            break;
        }
        case CLI_SSL_SERVER_CERT:
        {
            /* No addition command arguments, so ignore the args[] array */
            MEMSET (ai1ServerCert, 0, sizeof (ai1ServerCert));
            if (CliReadLine (ai1Prompt, ai1ServerCert, SSL_MAX_CERT_SIZE) ==
                CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r %%  Input read error  \r\n");
                i4RespStatus = CLI_FAILURE;
            }
            else
            {
                i4RespStatus = SslCliServerCert (CliHandle, ai1ServerCert);
            }
            break;
        }
        case CLI_SSL_SHOW_SERVER_STATUS:
        {
            /* No addition command arguments, so ignore the args[] array */
            i4RespStatus = SslCliShowServerStatus (CliHandle);
            break;
        }

        case CLI_SSL_DEBUG:
        {
            /* args[0] -  Debug Trace level to set */
            i4RespStatus = SslCliTrace (CliHandle, CLI_PTR_TO_I4 (args[0]),
                                        CLI_ENABLE);
            break;
        }
        case CLI_SSL_NO_DEBUG:
        {
            /* args[0] -  Debug Trace level to re-set */

            i4RespStatus = SslCliTrace (CliHandle, CLI_PTR_TO_I4 (args[0]),
                                        CLI_DISABLE);
            break;
        }
        case CLI_SSL_HTTPS:
        {
            /* args[0] -  CmdType for 'ip http secure' command.
             * args[1] -  Will contain the following values based on the given CmdTypes
             *            Value               --    CmdType
             *            Cipher Suites Type        CLI_SSL_CIPHER_SUITES
             *            Crypto Key values         CLI_SSL_CRYPTO_KEY
             */

            u4CmdType = CLI_PTR_TO_U4 (args[0]);

            if (u4CmdType == CLI_HTTPS_CONFIG)
            {
                i4RespStatus = SslCliHttpsStatus (CliHandle, TRUE);
            }
            else if (u4CmdType == CLI_SSL_CIPHER_SUITES)
            {
                i4RespStatus =
                    SslCliSetCipherList (CliHandle, CLI_PTR_TO_I4 (args[1]),
                                         CLI_ENABLE);
            }
            else if (u4CmdType == CLI_SSL_CRYPTO_KEY)
            {
                i4RespStatus = SslCliGenRsaKeyPair (CliHandle,
                                                    CLI_PTR_TO_I4 (args[1]));
            }

            break;
        }
        case CLI_SSL_KEY_GEN:
        {
            /* args[0] -  CmdType for 'ip http secure' command.
             * args[1] -  Will contain the following values based on the given CmdTypes
             *            Value               --    CmdType
             *            Cipher Suites Type        CLI_SSL_CIPHER_SUITES
             *            Crypto Key values         CLI_SSL_CRYPTO_KEY
             */

            u4CmdType = CLI_PTR_TO_U4 (args[0]);

            if (u4CmdType == CLI_HTTPS_CONFIG)
            {
                i4RespStatus = SslCliHttpsStatus (CliHandle, TRUE);
            }
            else if (u4CmdType == CLI_SSL_CIPHER_SUITES)
            {
                i4RespStatus =
                    SslCliSetCipherList (CliHandle, CLI_PTR_TO_I4 (args[1]),
                                         CLI_ENABLE);
            }
            else if (u4CmdType == CLI_SSL_CRYPTO_KEY)
            {
                i4RespStatus = SslCliGenRsaKeyPair (CliHandle,
                                                    CLI_PTR_TO_I4 (args[1]));
            }

            break;
        }

        case CLI_SSL_SERV_VERSION:
        {
            i4RespStatus =
                SslCliSetVersion (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;
        }
        case CLI_SSL_NO_HTTPS:
        {
            /* args[0] -  CmdType for 'no ip http secure' command.
             * args[1] -  Will contain the following values based on the given CmdTypes
             *            Value               --    CmdType
             *            Cipher Suites Type        CLI_SSL_CIPHER_SUITES
             *            to re-set
             */

            u4CmdType = CLI_PTR_TO_U4 (args[0]);

            if (u4CmdType == CLI_HTTPS_CONFIG)
            {
                i4RespStatus =
                    SslCliHttpsStatus (CliHandle, SSL_STATUS_DISABLED);
            }
            else if (u4CmdType == CLI_SSL_CIPHER_SUITES)
            {
                i4CipherSuites = CLI_PTR_TO_I4 (args[1]);

                i4RespStatus =
                    SslCliSetCipherList (CliHandle, i4CipherSuites,
                                         CLI_DISABLE);
            }

            break;
        }

        default:
            CliPrintf (CliHandle, "\r%% Invalid Command\r\n");
            i4RespStatus = CLI_FAILURE;
            break;
    }
    if ((i4RespStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_SSL_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", SslCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4)i4RespStatus);

    return i4RespStatus;
}

/**************************************************************************/
/*  Function Name   : SslCliServerCert                                    */
/*  Description     : This function configures the Server Cert            */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    pi1ServerCert - Server Cert to configure            */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
SslCliServerCert (tCliHandle CliHandle, INT1 *pi1ServerCert)
{
/*    tSSLCtrlMsg         SslCtrlMsg;
    UINT1         au1ServerCert [SSL_MAX_CERT_SIZE];*/

    if (SslArSetServerCert (pi1ServerCert) == SSL_SUCCESS)
        /* Check if key is present */
/*    if(SslArGetRsaKey() != NULL)
    if (gSslGlbCfg.pRsaKey != NULL)*/
    {
        /*SslArSetCtrlMsgType(SSL_CTRL_SET_SERVER_CERT);
           SslCtrlMsg.u4MsgType = SSL_CTRL_SET_SERVER_CERT;
           SSL_BZERO (au1ServerCert, SSL_MAX_CERT_SIZE);
           SSL_STRCPY (au1ServerCert, pi1ServerCert);

           SslArSetServerCertificate(au1ServerCert);
           SslSetCertificate (&SslCtrlMsg); */

        /* Save the server certificate */
        if (SslArSaveServerCert () == SSL_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%% Unable to save certificate\r\n");
            return (CLI_FAILURE);
        }
        CliPrintf (CliHandle, "\r\n");
        return CLI_SUCCESS;
    }
    CliPrintf (CliHandle, "\r\n%% Key not generated\r\n");
    return CLI_FAILURE;
}

/**************************************************************************/
/*  Function Name   : SslCliSetCipherList                                 */
/*  Description     : This function configures the Cipher List            */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4SslCipherList - Cipher list to configure          */
/*                    u1Flag          - CLI_ENABLE/CLI_DISABLE            */
/*                                                                        */
/*  Output(s)       : NONE                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
SslCliSetCipherList (tCliHandle CliHandle, INT4 i4SslCipherList, UINT1 u1Flag)
{
    UINT4               u4ErrCode;
    INT4                i4Value;

    UNUSED_PARAM (CliHandle);
    if (u1Flag == CLI_DISABLE)
    {
        nmhGetSslCipherList (&i4Value);
        /* Reset the bit positions, for which the value represents */
        i4SslCipherList = i4Value & (~i4SslCipherList);
    }

    /* If the cipher suite is NULL , set the cipher suite to
     * default values.
     */

    if (i4SslCipherList == 0)
    {
        i4SslCipherList = (TLS_RSA_3DES_SHA1 | TLS_RSA_DES_SHA1 |
                           TLS_RSA_EXP1024_DES_SHA1);
        if (SSL_CALLBACK_FN[SSL_CUST_MODE_CHK].pSslCustCheckCustMode != NULL)
        {
            if (SSL_CALLBACK_FN[SSL_CUST_MODE_CHK].pSslCustCheckCustMode ()
                == SSL_TRUE)
            {
                i4SslCipherList = (TLS_RSA_WITH_AES_128_CBC_SHA |
                                   TLS_RSA_WITH_AES_256_CBC_SHA |
                                   TLS_DHE_RSA_WITH_AES_128_CBC_SHA |
                                   TLS_DHE_RSA_WITH_AES_256_CBC_SHA);
            }
        }
    }

    if (nmhTestv2SslCipherList (&u4ErrCode, i4SslCipherList) == SNMP_SUCCESS)
    {
        if (nmhSetSslCipherList (i4SslCipherList) == SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
    }

    CLI_SET_ERR (CLI_SSL_INV_CIPHER_LIST);
    return (CLI_FAILURE);
}

/**************************************************************************/
/*  Function Name   : SslCliGenRsaKeyPair                                 */
/*  Description     : This function is used to geneate the RSA key pair   */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4RsaKeys - RsaKey pair bits                        */
/*                                                                        */
/*  Output(s)       : NONE                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
SslCliGenRsaKeyPair (tCliHandle CliHandle, INT4 i4RsaKeys)
{
    UNUSED_PARAM (CliHandle);

    if (SSL_CALLBACK_FN[SSL_CUST_MODE_CHK].pSslCustCheckCustMode != NULL)
    {
        if (SSL_CALLBACK_FN[SSL_CUST_MODE_CHK].pSslCustCheckCustMode ()
            == SSL_TRUE)
        {
            if (0 == i4RsaKeys)
            {
                i4RsaKeys = RSA_BITS1024;
            }
            if (RSA_BITS512 == i4RsaKeys)
            {
                CLI_SET_ERR (CLI_SSL_INV_RSA);
                return CLI_FAILURE;
            }
        }
    }

    if (i4RsaKeys == 0)
    {
        i4RsaKeys = RSA_BITS512;
    }

    SslArGenRsaKeyPair (i4RsaKeys);

    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : SslCliGenCertReq                                    */
/*  Description     : This function is used to generate the Cert Req      */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    pu1SubjectName - Subject Name to configure          */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
SslCliGenCertReq (tCliHandle CliHandle, UINT1 *pu1SubjectName)
{
    UINT1              *pu1CertReq = NULL;

    if (SslArGenCertReq (pu1SubjectName, &pu1CertReq) == SSL_SUCCESS)
        /*if (gSslGlbCfg.pRsaKey != NULL) */
    {
        /* SslArGenerateCertificateRequest (pu1SubjectName, NULL, &pu1CertReq);
           SslGenerateCertificateRequest (gSslGlbCfg.pRsaKey,
           pu1SubjectName, NULL, &pu1CertReq); */
        CliPrintf (CliHandle, "\r%s\r\n", pu1CertReq);
        SSL_MEM_FREE (pu1CertReq);
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r%% Key not generated\r\n");
    return CLI_FAILURE;
}

/**************************************************************************/
/*  Function Name   : SslCliTrace                                         */
/*  Description     : This function configures SSL Trace level            */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4TraceLevel - Trace level to set                   */
/*                    u1TraceFlag  - flag to check for set/re-set         */
/*                                                                        */
/*  Output(s)       : NONE                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
SslCliTrace (tCliHandle CliHandle, INT4 i4TraceLevel, UINT1 u1TraceFlag)
{
    UINT4               u4ErrCode;
    INT4                i4SslTrace;

    if (u1TraceFlag == CLI_DISABLE)
    {
        nmhGetSslTrace (&i4SslTrace);
        i4TraceLevel = (i4SslTrace & (~i4TraceLevel));
    }

    if (nmhTestv2SslTrace (&u4ErrCode, i4TraceLevel) == SNMP_SUCCESS)
    {
        if (nmhSetSslTrace (i4TraceLevel) == SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return (CLI_FAILURE);
}

/**************************************************************************/
/*  Function Name   : SslCliSetVersion                                    */
/*  Description     : This function configures SSL Version                */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4TraceLevel - Version to be configured             */
/*                                                                        */
/*  Output(s)       : NONE                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
SslCliSetVersion (tCliHandle CliHandle, INT4 i4ServVersion)
{
    UINT4               u4ErrCode;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2SslVersion (&u4ErrCode, i4ServVersion) == SNMP_SUCCESS)
    {

        if (nmhSetSslVersion (i4ServVersion) == SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
    }
    CLI_SET_ERR (CLI_SSL_INV_VERSION);
    return (CLI_FAILURE);
}

/**************************************************************************/
/*  Function Name   : SslCliHttpsStatus                                   */
/*  Description     : This function configures the HTTPS Status           */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4HttpsStatus - TRUE/SSL_STATUS_DISABLED            */
/*                                                                        */
/*  Output(s)       : NONE                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
SslCliHttpsStatus (tCliHandle CliHandle, INT4 i4HttpsStatus)
{
    UINT4               u4ErrCode;

    if (nmhTestv2SslSecureHttpStatus (&u4ErrCode, i4HttpsStatus) ==
        SNMP_SUCCESS)
    {
        if (nmhSetSslSecureHttpStatus (i4HttpsStatus) == SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return (CLI_FAILURE);
}

/**************************************************************************/
/*  Function Name   : SslCliShowServerStatus                              */
/*  Description     : To display SSL status and configuration information */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
SslCliShowServerStatus (tCliHandle CliHandle)
{
    INT4                i4CipherSuites;
    INT4                i4Version;
    INT4                i4HttpsStatus;
    UINT2               u2RsaBits;

    nmhGetSslSecureHttpStatus (&i4HttpsStatus);

    CliPrintf (CliHandle, "\r\nHTTP secure server status       : ");
    if (i4HttpsStatus == SSL_STATUS_DISABLED)
    {
        CliPrintf (CliHandle, "Disabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Enabled\r\n");
    }

    nmhGetSslCipherList (&i4CipherSuites);

    CliPrintf (CliHandle, "HTTP secure server ciphersuite  : ");

    if ((i4CipherSuites & TLS_RSA_NULL_MD5) == TLS_RSA_NULL_MD5)
    {
        CliPrintf (CliHandle, "RSA-NULL-MD5:");
    }
    if ((i4CipherSuites & TLS_RSA_NULL_SHA1) == TLS_RSA_NULL_SHA1)
    {
        CliPrintf (CliHandle, "RSA-NULL-SHA:");
    }
    if ((i4CipherSuites & TLS_RSA_DES_SHA1) == TLS_RSA_DES_SHA1)
    {
        CliPrintf (CliHandle, "RSA-DES-SHA:");
    }
    if ((i4CipherSuites & TLS_RSA_3DES_SHA1) == TLS_RSA_3DES_SHA1)
    {
        CliPrintf (CliHandle, "RSA-3DES-SHA:");
    }
    if ((i4CipherSuites & TLS_DHE_RSA_DES_SHA1) == TLS_DHE_RSA_DES_SHA1)
    {
        CliPrintf (CliHandle, "DH-RSA-DES-SHA:");
    }
    if ((i4CipherSuites & TLS_DHE_RSA_3DES_SHA1) == TLS_DHE_RSA_3DES_SHA1)
    {
        CliPrintf (CliHandle, "DH-RSA-3DES-SHA:");
    }
    if ((i4CipherSuites & TLS_RSA_EXP1024_DES_SHA1) == TLS_RSA_EXP1024_DES_SHA1)
    {
        CliPrintf (CliHandle, "RSA-EXP1024-DES-SHA:");
    }
    if ((i4CipherSuites & TLS_RSA_WITH_AES_128_CBC_SHA) ==
        TLS_RSA_WITH_AES_128_CBC_SHA)
    {
        CliPrintf (CliHandle, "RSA_WITH_AES_128_CBC_SHA:");
    }
    if ((i4CipherSuites & TLS_RSA_WITH_AES_256_CBC_SHA) ==
        TLS_RSA_WITH_AES_256_CBC_SHA)
    {
        CliPrintf (CliHandle, "RSA_WITH_AES_256_CBC_SHA:");
    }
    if ((i4CipherSuites & TLS_DHE_RSA_WITH_AES_128_CBC_SHA) ==
        TLS_DHE_RSA_WITH_AES_128_CBC_SHA)
    {
        CliPrintf (CliHandle, "DHE_RSA_WITH_AES_128_CBC_SHA:");
    }
    if ((i4CipherSuites & TLS_DHE_RSA_WITH_AES_256_CBC_SHA) ==
        TLS_DHE_RSA_WITH_AES_256_CBC_SHA)
    {
        CliPrintf (CliHandle, "DHE_RSA_WITH_AES_256_CBC_SHA:");
    }

    if (i4CipherSuites == 0)
    {
        CliPrintf (CliHandle, "No Cipher suites configured");
    }

    CliPrintf (CliHandle, "\r\n");

    u2RsaBits = SslArGetRsaBits ();
    if (u2RsaBits != 0)
    {
        CliPrintf (CliHandle, "HTTP crypto key rsa %d\r\n", u2RsaBits);
    }
    nmhGetSslVersion (&i4Version);
    switch (i4Version)
    {
        case SSL_VERSION_ALL:
            CliPrintf (CliHandle, "HTTP Secure Server Version : All \r\n");
            break;
        case SSL_VERSION_SSLV3:
            CliPrintf (CliHandle, "HTTP Secure Server Version : Ssl v3 \r\n");
            break;
        case SSL_VERSION_TLSV1:    /*Intensional fall-through */
        default:
            CliPrintf (CliHandle, "HTTP Secure Server Version : Tls v1 \r\n");
            break;
    }
    return (CLI_SUCCESS);
}

/**************************************************************************/
/*  Function Name   : SslCliShowServerCert                                */
/*  Description     : Displays  ssl server certificate                    */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
SslCliShowServerCert (tCliHandle CliHandle)
{
    UINT1              *pu1ASCII = NULL;
    UINT4               u4ASCIILen;
    INT1                i1CertStatus;

    if (SslArGetServerCert (&pu1ASCII, &u4ASCIILen, &i1CertStatus) ==
        SSL_SUCCESS)
    {
/*    if (SslArGetServerCert() != NULL)
    if (gSslGlbCfg.pServerCert != NULL)*/

        /* display the server certificate */

        /*if (SslArConvertX509ToASCII (&pu1ASCII, &u4ASCIILen) == SSL_SUCCESS)
           if (SslConvertX509ToASCII (gSslGlbCfg.pServerCert,
           &pu1ASCII, &u4ASCIILen) == SSL_SUCCESS) */
        CliPrintf (CliHandle, "\r\n%s\r\n\r\n", pu1ASCII);
        if (pu1ASCII != NULL)
        {
            SSL_MEM_FREE (pu1ASCII);
        }
        return CLI_SUCCESS;
    }
    if (i1CertStatus == SSL_TRUE)
    {
        CliPrintf (CliHandle, "\r %% Unable to read certificate\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r %% Server Certificate not configured\r\n");
    }
    if (pu1ASCII != NULL)
    {
        SSL_MEM_FREE (pu1ASCII);
    }

    return CLI_FAILURE;
}

/**************************************************************************/
/*  Function Name   : SslShowRunningConfig                                */
/*                                                                        */
/*  Description     : This function shows current running configuration   */
/*                    of SSL module                                          */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/
INT4
SslShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4RetVal = 0;

    nmhGetSslSecureHttpStatus (&i4RetVal);

    if (i4RetVal != SSL_STATUS_DISABLED)
    {
        CliPrintf (CliHandle, "ip http secure server\r\n");
    }
    i4RetVal = 0;
    nmhGetSslVersion (&i4RetVal);

    if (i4RetVal == SSL_VERSION_ALL)
    {
        CliPrintf (CliHandle, "version all\n");

    }
    else if (i4RetVal == SSL_VERSION_SSLV3)
    {
        CliPrintf (CliHandle, "version ssl3\n");
    }

    nmhGetSslCipherList (&i4RetVal);

    if (i4RetVal != DEFAULT_CIPHERLIST)
    {
        CliPrintf (CliHandle, "ip http secure ciphersuite");

        if ((i4RetVal & TLS_RSA_NULL_MD5) == TLS_RSA_NULL_MD5)
        {
            CliPrintf (CliHandle, " rsa-null-md5");
        }
        if ((i4RetVal & TLS_RSA_NULL_SHA1) == TLS_RSA_NULL_SHA1)
        {
            CliPrintf (CliHandle, " rsa-null-sha");
        }
        if ((i4RetVal & TLS_DHE_RSA_DES_SHA1) == TLS_DHE_RSA_DES_SHA1)
        {
            CliPrintf (CliHandle, " dh-rsa-des-sha");
        }
        if ((i4RetVal & TLS_DHE_RSA_3DES_SHA1) == TLS_DHE_RSA_3DES_SHA1)
        {
            CliPrintf (CliHandle, " dh-rsa-3des-sha");
        }
        if ((i4RetVal & TLS_RSA_DES_SHA1) == TLS_RSA_DES_SHA1)
        {
            CliPrintf (CliHandle, " rsa-des-sha");
        }
        if ((i4RetVal & TLS_RSA_3DES_SHA1) == TLS_RSA_3DES_SHA1)
        {
            CliPrintf (CliHandle, " rsa-3des-sha");
        }
        if ((i4RetVal & TLS_RSA_EXP1024_DES_SHA1) == TLS_RSA_EXP1024_DES_SHA1)
        {
            CliPrintf (CliHandle, " rsa-exp1024-des-sha");
        }
        if ((i4RetVal & TLS_RSA_WITH_AES_128_CBC_SHA) ==
            TLS_RSA_WITH_AES_128_CBC_SHA)
        {
            CliPrintf (CliHandle, " rsa-with-aes-128-cbc-sha");
        }
        if ((i4RetVal & TLS_RSA_WITH_AES_256_CBC_SHA) ==
            TLS_RSA_WITH_AES_256_CBC_SHA)
        {
            CliPrintf (CliHandle, " rsa-with-aes-256-cbc-sha");
        }
        if ((i4RetVal & TLS_DHE_RSA_WITH_AES_128_CBC_SHA) ==
            TLS_DHE_RSA_WITH_AES_128_CBC_SHA)
        {
            CliPrintf (CliHandle, " dhe-rsa-with-aes-128-cbc-sha");
        }
        if ((i4RetVal & TLS_DHE_RSA_WITH_AES_256_CBC_SHA) ==
            TLS_DHE_RSA_WITH_AES_256_CBC_SHA)
        {
            CliPrintf (CliHandle, " dhe-rsa-with-aes-256-cbc-sha");
        }
        CliPrintf (CliHandle, "\r\n");
    }

    if (SslArGetRsaBits () == RSA512)
        /*if (gSslGlbCfg.u2RsaBits == RSA512) */
    {
        CliPrintf (CliHandle, "ip http secure crypto key rsa 512\r\n");
    }
    else if (SslArGetRsaBits () == RSA1024)
    {
        CliPrintf (CliHandle, "ip http secure crypto key rsa 1024\r\n");
    }

    CliPrintf (CliHandle, "!\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssSslShowDebugging                                */
/*                                                                           */
/*     DESCRIPTION      : This function prints the SSL debug level           */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
IssSslShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DbgLevel = 0;

    nmhGetSslTrace (&i4DbgLevel);
    if (i4DbgLevel == 0)
    {
        return;
    }
    CliPrintf (CliHandle, "\rSSL :");

    if ((i4DbgLevel & SSL_INIT_SHUT_TRC_MASK) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SSL init and shutdown debugging is on");
    }
    if ((i4DbgLevel & SSL_MGMT_TRC_MASK) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SSL management debugging is on");
    }
    if ((i4DbgLevel & SSL_DATA_PATH_TRC_MASK) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SSL data path debugging is on");
    }
    if ((i4DbgLevel & SSL_CNTRL_PLANE_TRC_MASK) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SSL control path debugging is on");
    }
    if ((i4DbgLevel & SSL_DUMP_TRC_MASK) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SSL packet dump debugging is on");
    }
    if ((i4DbgLevel & SSL_OS_RESOURCE_TRC_MASK) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SSL resources debugging is on");
    }
    if ((i4DbgLevel & SSL_ALL_FAILURE_TRC_MASK) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SSL error debugging is on");
    }
    if ((i4DbgLevel & SSL_BUFFER_TRC_MASK) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SSL buffer debugging is on");
    }

    CliPrintf (CliHandle, "\r\n");
    return;
}

#endif /* __SSLCLI_C__ */
