/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssslwr.c,v 1.4 2011/02/17 05:32:34 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "fsssllw.h"
# include  "fssslwr.h"
# include  "fsssldb.h"

VOID
RegisterFSSSL ()
{
    SNMPRegisterMib (&fssslOID, &fssslEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fssslOID, (const UINT1 *) "fsssl");
}

VOID
UnRegisterFSSSL ()
{
    SNMPUnRegisterMib (&fssslOID, &fssslEntry);
    SNMPDelSysorEntry (&fssslOID, (const UINT1 *) "fsssl");
}

INT4
SslSecureHttpStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSslSecureHttpStatus (&(pMultiData->i4_SLongValue)));
}

INT4
SslPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSslPort (&(pMultiData->i4_SLongValue)));
}

INT4
SslTraceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSslTrace (&(pMultiData->i4_SLongValue)));
}

INT4
SslVersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSslVersion (&(pMultiData->i4_SLongValue)));
}

INT4
SslSecureHttpStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSslSecureHttpStatus (pMultiData->i4_SLongValue));
}

INT4
SslPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSslPort (pMultiData->i4_SLongValue));
}

INT4
SslTraceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSslTrace (pMultiData->i4_SLongValue));
}

INT4
SslVersionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSslVersion (pMultiData->i4_SLongValue));

}

INT4
SslSecureHttpStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SslSecureHttpStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SslPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SslPort (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SslTraceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SslTrace (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SslVersionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SslVersion (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SslSecureHttpStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SslSecureHttpStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SslPortDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SslPort (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SslTraceDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SslTrace (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SslVersionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SslVersion (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SslCipherListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSslCipherList (&(pMultiData->i4_SLongValue)));
}

INT4
SslDefaultCipherListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSslDefaultCipherList (&(pMultiData->i4_SLongValue)));
}

INT4
SslCipherListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSslCipherList (pMultiData->i4_SLongValue));
}

INT4
SslDefaultCipherListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSslDefaultCipherList (pMultiData->i4_SLongValue));
}

INT4
SslCipherListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SslCipherList (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SslDefaultCipherListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SslDefaultCipherList
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SslCipherListDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SslCipherList (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SslDefaultCipherListDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SslDefaultCipherList
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
