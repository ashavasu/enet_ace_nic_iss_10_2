/*****************************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: fmapi.c,v 1.4 2012/09/21 10:30:20 siva Exp $ 
 *
 * Description: This file contains the callback function API of Aricent FM
 *              module
 *****************************************************************************/
#ifndef FM_API_C
#define FM_API_C
#include "fminc.h"

tFmRegisterInfo    gFmRegistrationTable [ISS_FM_MAX_MODULES];

/******************************************************************************
 * Function Name      : FmApiNotifyFaults
 *
 * Description        : This API is called by external modules to notify about 
 *                      the protocol faults. The fault message is Logged 
 *                      through SYSLOG, and a TRAP message is sent to the 
 *                      Network Manager.
 *
 * Input(s)           : pFmMsg - Pointer to the Fault message structure.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
FmApiNotifyFaults (tFmFaultMsg * pFmMsg)
{
    INT4                i4SysLogId = 0;
    if (pFmMsg == NULL)
    {
        return OSIX_FAILURE;
    }
    /* Send the SYSLOG Message */
    if (pFmMsg->pSyslogMsg != NULL)
    {
	   /* If the syslog message is given and FM has not yet registred with
		* syslog , then make an attempt to register with syslog.
		*/
    if (gFmGlblInfo.u4SysLogId == 0)
    {
        i4SysLogId =
            SYS_LOG_REGISTER ((CONST UINT1 *) "FM", SYSLOG_ALERT_LEVEL);
        if (i4SysLogId <= 0)
        {
            return OSIX_FAILURE;
        }
        gFmGlblInfo.u4SysLogId = (UINT4) i4SysLogId;
    }
       SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gFmGlblInfo.u4SysLogId,
                      "[FM - %s] : %s", gapc1FmModule[pFmMsg->u4ModuleId],
                      pFmMsg->pSyslogMsg));
    }

    if (pFmMsg->pTrapMsg != NULL)
    {
#ifdef SNMP_2_WANTED
        /* Send the SNMP TRAP Message */
        if (pFmMsg->pCxtName == NULL)
        {
            /* If the Context Name is NULL means, this call is from a 
             * non-MI module. So call the normal SNMP API for sending TRAP. */
            SNMP_AGT_RIF_Notify_v2Trap (pFmMsg->pTrapMsg);
        }
        else
        {
            /* Call the SNMP API which is capable of sending trap 
             * with Context Name option. */
            SNMP_Context_Notify_Trap (pFmMsg->pEnterpriseOid,
                                      pFmMsg->u4GenTrapType,
                                      pFmMsg->u4SpecTrapType,
                                      pFmMsg->pTrapMsg, pFmMsg->pCxtName);
        }
#endif
    }
    FmIndicateEventToProto(pFmMsg);
    return OSIX_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : FmCallbackRegister                                   */
/*                                                                           */
/* Description        : This function registers the call backs from          */
/*                      application                                          */
/*                                                                           */
/* Input(s)           : pFsCbInfo - Call Back Function                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
INT4 FmCallbackRegister(tFsCbInfo * pFsCbInfo)
{
    tFmRegisterInfo    FmRegParams;

    MEMSET (&FmRegParams, 0, sizeof (tFmRegisterInfo));

    FmRegParams.pFmCallBack = pFsCbInfo->pFmCallBack;
    if (FmRegisterForCriticalEvent (&FmRegParams) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}


/*****************************************************************************/
/* Function Name      : FmRegisterForCriticalEvent                           */
/*                                                                           */
/* Description        : This function is to register the call back function  */
/*                      with FM module. On critical events the call back is  */
/*                      is invoked with the critical event information.      */
/*                                                                           */
/* Input(s)           : pFmRegInfo - Pointer to tFmRegisterInfo structure    */
/*						contains registration parematers.					 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
INT4 FmRegisterForCriticalEvent(tFmRegisterInfo *pFmRegInfo)
{
    UINT4               u4Index;

    for (u4Index = 0; u4Index < ISS_FM_MAX_MODULES; u4Index++)
    {
        if (gFmRegistrationTable[u4Index].u4Flag == OSIX_FALSE)
        {
            gFmRegistrationTable[u4Index].u4Flag = OSIX_TRUE;
            gFmRegistrationTable[u4Index].pFmCallBack =
                pFmRegInfo->pFmCallBack;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function Name      : FmIndicateEventToProto                               */
/*                                                                           */
/* Description        : This funtion is to indicate the critical event to all*/
/*                      registered modules.                                  */
/*                                                                           */
/* Input(s)           : pFmMsg -Pointer to structure tFmFaultMsg.pFmMsg holds*/
/*                                  the following information                */
/*                      ModuleName - Name of the module where the event      */
/*                                   occured.                                */
/*                      u4ModuleId - Id of the module where event occured.   */
/*                      u4EventType- Type of event triggered                 */
/*                      u4CriticalInfo - Holds status of the critical  events*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS                                         */
/*****************************************************************************/
INT4
FmIndicateEventToProto (tFmFaultMsg *pFmMsg)
{
    UINT4               u4Index;

    for (u4Index = 0; u4Index < ISS_FM_MAX_MODULES; u4Index++)
    {
        if ((gFmRegistrationTable[u4Index].u4Flag == OSIX_TRUE)
            && gFmRegistrationTable[u4Index].pFmCallBack != NULL)
        {
            gFmRegistrationTable[u4Index].pFmCallBack ((VOID *)pFmMsg);
        }
    }
    return OSIX_SUCCESS;
}
#endif /*FM_API_C */
