/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: fmtdfs.h,v 1.1.1.1 2008/06/03 14:34:14 iss Exp $
 *
 * Description: This file contains data structures defined for
 *              FM module.
 *********************************************************************/

#ifndef _FMTDFS_H_
#define _FMTDFS_H_


typedef struct FmGlblInfo
{
    UINT4              u4SysLogId;
} tFmGlblInfo;

#endif /*FMTDFS_H*/
/*-----------------------------------------------------------------------*/
/*                       End of the file  fmtdfs.h                      */
/*-----------------------------------------------------------------------*/
