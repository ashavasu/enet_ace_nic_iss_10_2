
/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: fminc.h,v 1.2 2010/12/12 11:39:59 siva Exp $
 *
 * Description: This file contains header files included in 
 *              FM module.
 *********************************************************************/
#ifndef _FMINC_H_
#define _FMINC_H_

#include "lr.h"
#include "fssnmp.h"
#include "fmtdfs.h"
#include "fmglob.h"
#include "fmextern.h"
#include "fssyslog.h"
#include "fm.h"
#endif   /* _FMINC_H_  */
/*-----------------------------------------------------------------------*/
/*                       End of the file  fminc.h                        */
/*-----------------------------------------------------------------------*/
