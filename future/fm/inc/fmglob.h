/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: fmglob.h,v 1.1.1.1 2008/06/03 14:34:14 iss Exp $
 *
 * Description: This file contains global variables used in FM.
 *********************************************************************/
#ifndef _FMGLB_H_
#define _FMGLB_H_

tFmGlblInfo  gFmGlblInfo;

#endif /*_FMGLB_H_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  fmglob.h                       */
/*-----------------------------------------------------------------------*/
