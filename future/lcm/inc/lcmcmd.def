/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved               
*                                                                    
* $Id: lcmcmd.def,v 1.4 2014/11/04 13:14:11 siva Exp $                                                         
*                                                                    
*********************************************************************/

   DEFINE GROUP: LCM_PRV_EXEC_GRP
   
   COMMAND: show ethernet evc detail
   ACTION : cli_process_lcm_cmd(CliHandle, CLI_LCM_SHOW_ETHER_SERVICE_EVC, NULL);
   SYNTAX : show ethernet evc detail
   PRVID  : 15
   HELP   : This command will display all the evc configured on this switch,
            use the show ethernet service evc command in privileged EXEC mode.
  CXT_HELP: show Displays the configuration / statistics / general information|
            ethernet Ethernet related configuration|
            evc Ethernet virtual connection related configuration|
            detail Detailed information|
            <CR> This command will display all the evc configured on this switch,use the show ethernet service evc command in privileged EXEC mode.


   COMMAND: show ethernet service instance {detail | idefp-identifier interface interface-id | interface
            interface-id}
   ACTION : cli_process_lcm_cmd(CliHandle, CLI_LCM_SHOW_ETH_SER_INSTANCE, NULL);
   SYNTAX : show ethernet service instance {detail | idefp-identifier interface interface-id | interface
            interface-id}
   PRVID  : 15
   HELP   : This command is used to display information about Ethernet customer service instances, use the show ethernet service instance command in privileged EXEC mode.
  CXT_HELP: show Displays the configuration / statistics / general information|
            ethernet Ethernet related configuration|
            service Service type related configuration|
            instance Customer service instance related configuration|
            detail Detailed information|
            idefp-identifier Ethernet service instance identifier related configuration|
            interface Interface related configuration|
            interface-id Interface id related configuration|
            interface Interface related configuration|
            interface-id Interface id related configuration|
            <CR> This command is used to display information about Ethernet customer service instances, use the show ethernet service instance command in privileged EXEC mode.


   END GROUP

   DEFINE GROUP: LCM_INT_CFG_GRP

   COMMAND: service instance efp-identifier <integer> ethernet evc-id <string(100)>
   ACTION : cli_process_lcm_cmd(CliHandle, CLI_LCM_SET_ETH_SER_INSTANCE, NULL,$3,$6);
   SYNTAX : service instance efp-identifier <integer(1-4294967295)> ethernet evc-id <string of maxmium length 100>
   PRVID  : 15
   HELP   : This command is used to Configure an Ethernet service instance (EFP) on the
            interface, and enter ethernet service configuration mode.
  CXT_HELP: service Configures service type related information|
            instance Customer service instance related configuration|
            efp-identifier Ethernet service instance identifier related configuration|
            <integer> EFP identifier value|
            ethernet Ethernet related configuration|
            evc-id Ethernet virtual connection id related configuration|
            <string(100)> Ethernet virtual connection id|
            <CR> This command is used to configure an Ethernet service instance (EFP) on the interface, and enter ethernet service configuration mode.

   COMMAND: no service instance efp-identifier <integer> ethernet evc-id <string(100)>
   ACTION : cli_process_lcm_cmd(CliHandle, CLI_LCM_SET_NO_ETH_SER_INSTANCE, NULL,$4,$7);
   SYNTAX : no service instance efp-identifier <integer(1-4294967295)> ethernet evc-id <string of maxmium length 100>
   PRVID  : 15
   HELP   : To delete a service instance, use the no form of this command.
  CXT_HELP: no Disables the configuration / deletes the entry / resets to default value|
            service Service type related configuration|
            instance Customer service instance related configuration|
            efp-identifier Ethernet service instance identifier related configuration|
            <integer> EFP identifier value|
            ethernet Ethernet related configuration|
            evc-id Ethernet virtual connection ID related configuration|
            <string(100)> Ethernet virtual connection id|
            <CR> To delete a service instance, use the no form of this command.

    
    
    END GROUP

   DEFINE GROUP:LCM_EVC_CFG_GRP

   COMMAND: oam protocol cfm svlan-id <integer(1-1000)> domain-name <string(100)>
   ACTION : cli_process_lcm_cmd(CliHandle, CLI_LCM_SET_OAM_CFM, NULL,$4,$6);
   SYNTAX : oam protocol cfm svlan-id <integer value of vlanid> domain-name<string of max 100 length>
   PRVID  : 15
   HELP   : This command is used to Configure the EVC OAM protocol as CFM,
            and identify the service provider VLAN-ID (S-VLAN-ID) for the CFM
            domain maintenance level.If the CFM domain does not exist, this command is
            rejected, and an error message is displayed.
  CXT_HELP: oam Configures oam related configuration|
            protocol Protocol related information|
            cfm CFM related configuration|
            svlan-id Service vlan id related configuration| 
            (1-1000) Service vlan id|
            domain-name Domain name related configuration|
            <string(100)> Domain name|
            <CR> This command is used to configure the EVC OAM protocol as CFM,and identify the service provider VLAN-ID (S-VLAN-ID) for the CFM domain maintenance level.If the CFM domain does not exist, this command is rejected, and an error message is displayed.
   END GROUP
