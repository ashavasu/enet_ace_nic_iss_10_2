#ifndef _LCM_GLOB_H
#define _LCM_GLOB_H

#ifdef _LCMAPI_C_
/***************** Required By Stubs *************************/

tLcmGlobalInfo      gLcmGlobalInfo;
tLcmPromptInfo      LcmPromptInfo;

#else
/***************** Required By Stubs *************************/

extern tLcmGlobalInfo      gLcmGlobalInfo;
extern tLcmPromptInfo      LcmPromptInfo;

#endif

#endif
