#ifndef _LCMCONST_H
#define _LCMCONST_H

#define LCM_OSIX_SUCCESS               OSIX_SUCCESS
#define LCM_OSIX_FAILURE               OSIX_FAILURE
#define LCM_MEM_SUCCESS                MEM_SUCCESS
#define LCM_MEM_FAILURE                MEM_FAILURE
#define LCM_MEMSET                     MEMSET
#define LCM_INIT_VAL                        0

#define     LCM_FALSE                           0
#define     LCM_TRUE                            1

#define LCM_MAX_NUM_PORT_PER_CONTEXT   SYS_DEF_MAX_PORTS_PER_CONTEXT
#define LCM_MAX_NUM_PORTS              (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG)

#define NUM_BW_ELEMENTS                 8
#define LCM_MAX_CONTEXTS        1 
#define LCM_MAP_VLAN 1
#define LCM_NO_MAP_VLAN 2

#define LCM_MUT_EXCL_SEM_NAME          (const UINT1 *) "EVSM" 
#define LCM_SEM_INIT_COUNT             1

#endif
