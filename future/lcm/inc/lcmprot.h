/* $Id: lcmprot.h,v 1.3 2011/12/22 11:18:37 siva Exp $*/

#ifndef _LCMPROT_H
#define _LCMPROT_H

INT2 LcmGetNoOfEvc(VOID);
INT2 LcmGenerateEvcLocalIndex(VOID);
VOID LcmDeleteEvcLocalNo(UINT2 u2LocalNo);
INT2 LcmGenerateEvcRefId(UINT2 u2PortNo, UINT2 *pu2RefId);
VOID LcmDeleteEvcRefId(UINT2 u2PortNo,UINT2 u2RefId);
tLcmEvcEntryInPort *LcmGetEvcEntryInPortInfo (
            UINT2 u2PortNo, UINT2 u2EvcRefId);
tLcmEvcEntryInPort *LcmGetFirstEvcEntryInPortInfo (UINT2 u2PortNo);
tLcmEvcEntryInPort *LcmGetNextEvcEntryInPortInfo (
            tLcmEvcEntryInPort * pLcmEvcEntryInPort,UINT2 u2PortNo);
tLcmEvcEntryInPort *LcmGetNextEvcEntryInPortInfoBasedOnEvcRefId (
            UINT2 u2PortNo,UINT2 u2EvcRefId);
INT4 LcmEvcEntryInPortInfoTblCmpFn (tRBElem * pKey1, tRBElem * pKey2);
INT4 LcmFreeEvcEntryInPortInfo (tRBElem * pElem, UINT4 u4Arg);
INT4 LcmAddToEvcEntryInPortInfoTable (
            tLcmEvcEntryInPort * pLcmEvcEntryInPort,UINT2 u2PortNo);
INT4 LcmRemoveFromEvcEntryInPortInfoTable (
            tLcmEvcEntryInPort * pLcmEvcEntryInPort,UINT2 u2PortNo);
tLcmPortEntryInEvc *LcmGetPortEntryInEvcInfo (
            tLcmLocalEvcInfo *pLcmLocalEvcInfo, UINT2 u2PortNo);
tLcmPortEntryInEvc*
LcmGetFirstPortEntryInEvcInfo (tLcmLocalEvcInfo *pLcmLocalEvcInfo);
tLcmPortEntryInEvc *
LcmGetNextPortEntryInEvcInfo (tLcmPortEntryInEvc * pLcmPortEntryInEvc,
                              tLcmLocalEvcInfo *pLcmLocalEvcInfo);
tLcmPortEntryInEvc *LcmGetNextPortEntryInEvcInfoBasedOnPortNo (
            tLcmLocalEvcInfo *pLcmLocalEvcInfo,UINT2 u2PortNo);
INT4 LcmPortEntryInEvcInfoTblCmpFn (tRBElem * pKey1, tRBElem * pKey2);
INT4 LcmFreePortEntryInEvcInfo (tRBElem * pElem, UINT4 u4Arg);
INT4 LcmAddToPortEntryInEvcInfoTable (
            tLcmPortEntryInEvc * pLcmPortEntryInEvc,tLcmLocalEvcInfo *pLcmLocalEvcInfo);
INT4 LcmRemoveFromPortEntryInEvcInfoTable (
            tLcmPortEntryInEvc * pLcmPortEntryInEvc,tLcmLocalEvcInfo *pLcmLocalEvcInfo);

tLcmLocalEvcInfo *
LcmGetEvcEntryInContextInfo (UINT2 u2ContextId,UINT1 *EvcId);
tLcmLocalEvcInfo *
LcmGetFirstEvcEntryInContextInfo (UINT2 u2ContextId);
tLcmLocalEvcInfo *
LcmGetNextEvcEntryInContextInfo (tLcmLocalEvcInfo * pLcmLocalEvcInfo,
                                UINT2 u2ContextId);
tLcmLocalEvcInfo *
LcmGetNextEvcEntryInContextInfoBasedOnEvcId (UINT2 u2ContextId,UINT1* EvcId);
INT4
LcmEvcEntryInContextInfoTblCmpFn (tRBElem * pKey1, tRBElem * pKey2);
INT4
LcmFreeEvcEntryInContextInfo (tLcmLocalEvcInfo * pLcmLocalEvcInfo, UINT4 u4Arg);
INT4
LcmAddToEvcEntryInContextInfoTable (tLcmLocalEvcInfo * pLcmLocalEvcInfo,
                                    UINT2 u2ContextId);
INT4
LcmRemoveFromEvcEntryInContextInfoTable (
                       tLcmLocalEvcInfo * pLcmLocalEvcInfo,
                       UINT2 u2ContextId);
#endif
