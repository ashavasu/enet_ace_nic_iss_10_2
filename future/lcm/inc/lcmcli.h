#ifndef _LCMCLI_H
#define _LCMCLI_H

INT4    CliHandleProtocolMap(UINT1 *pu1EvcId,UINT1 u1OamProtocol,UINT2 u2SVlan,UINT1 *pu1DomainName);
INT4    CliHandleServiceInstanceCreation(UINT2 u2PortNo,UINT2 u2EtherServiceInstance,UINT1 *pu1EvcId);
INT4    CliHandleServiceInstanceDeletion(UINT2 u2PortNo,UINT2 u2EtherServiceInstance,UINT1 *pu1EvcId);
INT4    CliHandleShowEvcInfo(tCliHandle CliHandle);

#endif
