#ifndef _LCMINC_H
#define _LCMINC_H

#include "lr.h"
#include "cfa.h"
#include "uni.h"
#include "snmctdfs.h"
#include "snmccons.h" 
#include "fssyslog.h"
#include "snmputil.h" 
#include "fsvlan.h"
#ifdef PBB_WANTED
#include "pbb.h"
#endif
#include "l2iwf.h"
#include "iss.h"
#include "cli.h"
#include "vcm.h"

#include "evcpro.h"
#include "elm.h"
#include "lcm.h"
#include "lcmconst.h"
#include "lcmmacr.h"
#include "lcmtdfs.h"
#include "lcmcli.h"
#include "lcmglob.h"
#include "lcmprot.h"
#include "lcmcfmstubs.h"

#endif

