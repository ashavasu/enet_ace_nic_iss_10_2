/* $Id: lcmcfmstubs.h,v 1.3 2011/10/13 10:09:12 siva Exp $ */
#ifndef _LCMCFMSTUBS_H
#define _LCMCFMSTUBS_H

#define MAX_NUM_MA_SUPPORTED       1000
#define CFM_MAX_CONTEXTS           1

#define CFM_UNUSED(x)  {x = x;}

/**************************************************************/
/*                  For CFM                                   */

typedef struct CfmMaInfo{
    UINT2                   u2MepCount;/*MAX_NUMBER_MEP_PER_EVC - 1024 as per   
                                        Cisco */
    UINT1                   u1CustomerServiceInstance; /* MA Identifier*/
    UINT1                   u1Level;
    UINT1                   au1DomainName[CFM_MAX_LENGTH_DOMAIN_NAME];
}tCfmMaInfo;


typedef struct CfmContextInfo {
    tCfmMaInfo            *apCfmMaInfo[MAX_NUM_MA_SUPPORTED];
}tCfmContextInfo;


typedef struct CfmGlobalInfo {
   tCfmContextInfo         *apContextInfo [CFM_MAX_CONTEXTS];
}tCfmGlobalInfo;

/**************************************************************/
#ifdef _LCMCFMSTUBS_C
tCfmGlobalInfo      gCfmGlobalInfo;
#else
extern tCfmGlobalInfo      gCfmGlobalInfo;
#endif



#endif /* _LCMCFMSTUBS_H */
