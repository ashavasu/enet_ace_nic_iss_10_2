/* $Id: lcmtdfs.h,v 1.4 2011/04/28 12:21:15 siva Exp $ */
#ifndef _LCMTDFS_H
#define _LCMTDFS_H

typedef struct LcmPortEntryInEvc{
     tRBNodeEmbd            LcmPortEntryInEvcNode;
     UINT4                  u4EtherServInstance;
     UINT2                  u2PortNo;
     UINT2                  u2NoOfVlansMapped;
     UINT2                  u2FirstVlan;
     UINT2                  u2LastVlan;
     UINT2                  u2EvcReferenceID;
     tIssVlanList           CeVlanMapInfo;
     UINT1                  u1EvcStatus;
     UINT1      u1Pad;
}tLcmPortEntryInEvc;

typedef struct LcmLocalEvcInfo{/*Indexed using Local EVC Number*/
     tRBNodeEmbd            LcmEvcEntryInContextNode;
     tRBTree                LcmPortRBTree;/*Indexed using Port Number */
     tBwProfile             EvcBwProfile[8];
     UINT2                  u2EvcLocalIndex;
     UINT2                  u2SVlan;
     UINT2                  u2UniCount;
     UINT1                  u1EvcType;
     UINT1                  u1Level;
     UINT1                  EvcIdentifier[LCM_MAX_EVC_IDENTIFIER_LENGTH]; 
     UINT1                  au1DomainName[CFM_MAX_LENGTH_DOMAIN_NAME];
     UINT1                  u1CfaCustomerServiceInstance;
     UINT1                  u1EvcStatus;
     UINT1                  u1OAMProtocolUsed;
     UINT1                  u1ChangedFlag;
     UINT1                  u1NoOfCos;
     UINT1                  u1BwBitMap[3];
}tLcmLocalEvcInfo;

typedef struct LcmEvcEntryInPort
{
    tRBNodeEmbd    LcmEvcEntryInPortNode;
    UINT4          u4EtherServInstance;
    UINT2          u2EvcRefId;
    UINT1          u1EvcStatus;
    UINT1          u1Pad;
    tLcmLocalEvcInfo   *pLcmLocalEvcInfo;
}tLcmEvcEntryInPort;

typedef struct LcmPortEntry
{
     tRBTree                LcmPortEvcEntries;
     tLcmLocalEvcInfo       *pLcmDefaultLocalEvcInfo;
     UINT2                  u2PortNo;
     UINT1                  u1NoOfEvc;
     UINT1                  u1RefIdTable[EVCPRO_MAX_EVC_PER_PORT + 1];
                            /* One extra element included in array since 
                            EVC ref ID cannot take ZERO value*/
}tLcmPortEntry;

typedef struct LcmContextInfo {
    tRBTree                LcmEvcEntries;
    tLcmPortEntry          aPortEntry [LCM_MAX_NUM_PORTS];
}tLcmContextInfo;

typedef struct LcmGlobalInfo {
   tLcmContextInfo          aContextInfo [LCM_MAX_CONTEXTS];/*ll be used 
                                                  in case of MI testing*/
   tLcmOsixSemId            SemId;   /* Lcm Sem4 Id */
   tLcmMemPoolId            EvcEntryInPortNodeMemPoolId;
   tLcmMemPoolId            LcmEvcInfoNodeMemPoolId;
   tLcmMemPoolId            PortEntryInEvcNodeMemPoolId;

}tLcmGlobalInfo;

/**************************************************************/
#endif
