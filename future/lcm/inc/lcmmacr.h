#ifndef _LCM_MACR_H
#define _LCM_MACR_H

#define LCM_UNUSED(x)  {x = x;}

#define LCM_CREATE_SEM(pu1Sem, u4Count, u4Flags, pSemId) \
            OsixCreateSem ((pu1Sem), (u4Count), (u4Flags), (pSemId))

#define LCM_DELETE_SEM                 OsixSemDel
#define LCM_TAKE_SEM                   OsixSemTake
#define LCM_GIVE_SEM                   OsixSemGive


#define tLcmMemPoolId                  tMemPoolId
#define tLcmOsixSemId                  tOsixSemId

#define LCM_CREATE_MEM_POOL(BlockSize,NumOfBlocks,pPoolId) \
           MemCreateMemPool((UINT4)(BlockSize),(UINT4)(NumOfBlocks), \
                (UINT4)(MEM_DEFAULT_MEMORY_TYPE),(tLcmMemPoolId*)(pPoolId))

#define LCM_ALLOC_EVC_INFO_MEM_BLOCK(pLcmEvcInfo) \
        (pLcmEvcInfo = (tLcmLocalEvcInfo *) \
         MemAllocMemBlk(gLcmGlobalInfo.LcmEvcInfoNodeMemPoolId))


#define LCM_ALLOC_PORTNODE_IN_EVC_INFO_MEM_BLOCK(pLcmPortEntryInEvc) \
        (pLcmPortEntryInEvc = (tLcmPortEntryInEvc *) \
          MemAllocMemBlk(gLcmGlobalInfo.PortEntryInEvcNodeMemPoolId))

#define LCM_ALLOC_EVCNODE_IN_PORT_INFO_MEM_BLOCK(pLcmEvcEntryInPort) \
        (pLcmEvcEntryInPort = (tLcmEvcEntryInPort *) \
         MemAllocMemBlk(gLcmGlobalInfo.EvcEntryInPortNodeMemPoolId))

#define LCM_RELEASE_EVC_INFO_MEM_BLOCK(pLcmEvcInfo) \
           MemReleaseMemBlock(gLcmGlobalInfo.LcmEvcInfoNodeMemPoolId, \
           (UINT1 *)pLcmEvcInfo)

#define LCM_RELEASE_PORTNODE_IN_EVC_INFO_MEM_BLOCK(pLcmPortEntryInEvc) \
           MemReleaseMemBlock(gLcmGlobalInfo.PortEntryInEvcNodeMemPoolId,\
           (UINT1 *)pLcmPortEntryInEvc)

#define LCM_RELEASE_EVCNODE_IN_PORT_INFO_MEM_BLOCK(pLcmEvcEntryInPort) \
           MemReleaseMemBlock(gLcmGlobalInfo.EvcEntryInPortNodeMemPoolId, \
           (UINT1 *)pLcmEvcEntryInPort)

#define LCM_GET_PORTENTRY(u2PortNo) \
        (((u2PortNo <=0)||(u2PortNo >LCM_MAX_NUM_PORTS))?NULL:(&(gLcmGlobalInfo.aContextInfo[0].aPortEntry[u2PortNo - 1])))
#define LCM_GET_EVCINFO(u2EvcLocalIndex) \
        (gLcmGlobalInfo.aContextInfo[0].apEvcEntry[u2EvcLocalIndex])


#endif
