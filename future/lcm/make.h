#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                     |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX ( Slackware 1.2.1 )                     |
# |                                                                          |
# |   DATE                   : 07 March 2002                                 |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


LCM_SWITCHES = -UDEBUG_WANTED 

TOTAL_OPNS = ${LCM_SWITCHES} $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

LCM_BASE_DIR = ${BASE_DIR}/lcm
LCM_SRC_DIR  = ${LCM_BASE_DIR}/src
LCM_INC_DIR  = ${LCM_BASE_DIR}/inc
LCM_OBJ_DIR  = ${LCM_BASE_DIR}/obj
COMMON_INC_DIR  = ${BASE_DIR}/inc

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${LCM_INC_DIR} -I${COMMON_INC_DIR} 
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
