#include "lcminc.h"
#include "lcmglobcli.h"

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CliHandleProtocolMap                               */
/*                                                                           */
/*     DESCRIPTION      : This function handles the updation of Protocol map */
/*                                                                           */
/*                                                                           */
/*     INPUT            : pu1EvcId - EVC ID                                  */
/*                        u1OamProtocol - OAM protocol                       */
/*                        pu1DomainName - Domain name                        */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CliHandleProtocolMap(UINT1 *pu1EvcId,UINT1 u1OamProtocol,UINT2 u2SVlan,UINT1 
*pu1DomainName)
{

   if ( LcmUpdateEvcOamProtocolMap(pu1EvcId,u1OamProtocol,
                                   u2SVlan,pu1DomainName) != LCM_SUCCESS )
   {
       return CLI_FAILURE;
   }

   return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CliHandleServiceInstanceCreation                   */
/*                                                                           */
/*     DESCRIPTION      : This function handles the creation of Ethernet     */
/*                        Service Instance                                   */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u2PortNo - Port no for creation                    */
/*                        u2EtherServiceInstance - Service instance to be    */
/*                          created                                          */
/*                        pu1EvcId - EVC id for the service instance         */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CliHandleServiceInstanceCreation(UINT2 u2PortNo,UINT2 
u2EtherServiceInstance,UINT1 *pu1EvcId)
{
   if (LcmUpdateEvcEtherServiceInstance
       (u2PortNo,u2EtherServiceInstance,pu1EvcId,0) != LCM_SUCCESS)
   {
       return CLI_FAILURE;
   }
   return CLI_SUCCESS;
}


/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CliHandleServiceInstanceDeletion                   */
/*                                                                           */
/*     DESCRIPTION      : This function handles the deletion of Ethernet     */
/*                        Service Instance                                   */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u2PortNo - Port no for deletion                    */
/*                        u2EtherServiceInstance - Service instance to be    */
/*                          deleted                                          */
/*                        pu1EvcId - EVC id for the service instance         */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CliHandleServiceInstanceDeletion(UINT2 u2PortNo,UINT2 
u2EtherServiceInstance,UINT1 *pu1EvcId)
{
   LcmDeleteEtherServiceInstance
       (u2PortNo,u2EtherServiceInstance,pu1EvcId);
   return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CliHandleShowEvcInfo                               */
/*                                                                           */
/*     DESCRIPTION      : This function handles the EVC show command         */
/*                                                                           */
/*                                                                           */
/*     INPUT            : CliHandle - Handle for Cli Commands                */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CliHandleShowEvcInfo(tCliHandle CliHandle)
{
   UINT2 u2NoOfEvc = 0;

   u2NoOfEvc = LcmGetNoOfEvc();

   CliPrintf (CliHandle, "\r NoOf Evc's Configured = %d !\r\n ",u2NoOfEvc);
   return CLI_SUCCESS;
}

