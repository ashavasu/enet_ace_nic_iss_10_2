#define _LCMAPI_C_

#include "lcminc.h"
/* $Id: lcmapi.c,v 1.12 2015/02/09 12:59:27 siva Exp $*/
/* Required by ELMI Module*/
/*****************************************************************************/
/* Function Name      : LcmGetEvcStatusInfo                                  */
/*                                                                           */
/* Description        : This routines fetches the status information for EVC */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcRefId - Refernce ID of the EVC whose status has */
/*                          to be fetched                                    */
/*                      pLcmEvcStatusInfo - EVC status info structure to be  */
/*                          filled with the status info                      */
/*                                                                           */
/* Output(s)          : pLcmEvcStatusInfo - EVC status info structure        */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
INT4
LcmGetEvcStatusInfo (UINT2 u2PortNo, UINT2 u2EvcRefId,
                     tLcmEvcStatusInfo * pLcmEvcStatusInfo)
{
    tLcmEvcEntryInPort *pLcmEvcEntryInPort = NULL;

    pLcmEvcEntryInPort = LcmGetEvcEntryInPortInfo (u2PortNo, u2EvcRefId);

    if (pLcmEvcEntryInPort == NULL)
    {
        return LCM_FAILURE;
    }

    pLcmEvcStatusInfo->u1NoOfCos =
        pLcmEvcEntryInPort->pLcmLocalEvcInfo->u1NoOfCos;

    MEMCPY (pLcmEvcStatusInfo->BwProfile,
            pLcmEvcEntryInPort->pLcmLocalEvcInfo->EvcBwProfile,
            NUM_BW_ELEMENTS * sizeof (tBwProfile));

    pLcmEvcStatusInfo->u2EvcReferenceId = u2EvcRefId;

    pLcmEvcStatusInfo->u1EvcStatus = pLcmEvcEntryInPort->u1EvcStatus;
    pLcmEvcEntryInPort->u1EvcStatus &= 0xFE;

    pLcmEvcStatusInfo->u1EvcType =
        pLcmEvcEntryInPort->pLcmLocalEvcInfo->u1EvcType;

    MEMCPY (pLcmEvcStatusInfo->au1EvcIdentifier,
            pLcmEvcEntryInPort->pLcmLocalEvcInfo->EvcIdentifier,
            LCM_MAX_EVC_IDENTIFIER_LENGTH);

    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LcmGetNextEvcStatusInfo                              */
/*                                                                           */
/* Description        : This routines fetches the status information for     */
/*                      at max 10 evcs at a time.                            */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcRefId - Refernce ID of the EVC after which the  */
/*                          status of the EVCs are to be fetched             */
/*                      pLcmEvcStatusInfo - EVC status info structure to be  */
/*                          filled with the status info                      */
/*                      pu1NoEvcInfo - No of EVCs whose status got fetched   */
/*                                                                           */
/* Output(s)          : pLcmEvcStatusInfo - EVC status info structure        */
/*                      pu1NoEvcInfo - No of EVCs whose status got fetched   */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
INT4
LcmGetNextEvcStatusInfo (UINT2 u2PortNo, UINT2 u2EvcRefId,
                         tLcmEvcStatusInfo * pLcmEvcStatusInfo,
                         UINT1 *pu1NoEvcInfo)
{
    tLcmEvcEntryInPort *pLcmEvcEntryInPort = NULL;

    *pu1NoEvcInfo = 0;

    while (*pu1NoEvcInfo < 10)
    {
        if (u2EvcRefId == 0)
        {
            pLcmEvcEntryInPort = LcmGetFirstEvcEntryInPortInfo (u2PortNo);
        }
        else
        {
            pLcmEvcEntryInPort = LcmGetNextEvcEntryInPortInfoBasedOnEvcRefId
                (u2PortNo, u2EvcRefId);
        }

        if (pLcmEvcEntryInPort == NULL)
        {
            return LCM_SUCCESS;
        }

        /* This is error case */
        if (pLcmEvcEntryInPort->pLcmLocalEvcInfo == NULL)
        {
            return LCM_FAILURE;
        }

        pLcmEvcStatusInfo[*pu1NoEvcInfo].u2EvcReferenceId =
            pLcmEvcEntryInPort->u2EvcRefId;

        pLcmEvcStatusInfo[*pu1NoEvcInfo].u1EvcStatus =
            pLcmEvcEntryInPort->u1EvcStatus;
        pLcmEvcEntryInPort->u1EvcStatus &= 0xFE;

        pLcmEvcStatusInfo[*pu1NoEvcInfo].u1EvcType =
            pLcmEvcEntryInPort->pLcmLocalEvcInfo->u1EvcType;

        pLcmEvcStatusInfo[*pu1NoEvcInfo].u1NoOfCos =
            pLcmEvcEntryInPort->pLcmLocalEvcInfo->u1NoOfCos;

        MEMCPY (pLcmEvcStatusInfo[*pu1NoEvcInfo].BwProfile,
                pLcmEvcEntryInPort->pLcmLocalEvcInfo->EvcBwProfile,
                (NUM_BW_ELEMENTS * sizeof (tBwProfile)));
        MEMCPY (pLcmEvcStatusInfo[*pu1NoEvcInfo].au1EvcIdentifier,
                pLcmEvcEntryInPort->pLcmLocalEvcInfo->EvcIdentifier,
                LCM_MAX_EVC_IDENTIFIER_LENGTH);

        u2EvcRefId = pLcmEvcEntryInPort->u2EvcRefId;

        (*pu1NoEvcInfo)++;
        pLcmEvcEntryInPort = NULL;
    }

    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LcmGetEvcCeVlanInfo                                  */
/*                                                                           */
/* Description        : This routines fetches CE vlan info for the given EVC */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcRefId - Refernce ID of the EVC whose CEVLAN info*/
/*                          has to be fetched                                */
/*                      pLcmEvcCeVlanInfo - CE VLAN info structure to be     */
/*                          filled with the info                             */
/*                                                                           */
/* Output(s)          : pLcmEvcCeVlanInfo- CE Vlan info structure            */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
INT4
LcmGetEvcCeVlanInfo (UINT2 u2PortNo, UINT2 u2EvcRefId,
                     tLcmEvcCeVlanInfo * pLcmEvcCeVlanInfo)
{
    tLcmEvcEntryInPort *pLcmEvcEntryInPort = NULL;
    tLcmPortEntryInEvc *pLcmPortEntryInEvc = NULL;

    pLcmEvcEntryInPort = LcmGetEvcEntryInPortInfo (u2PortNo, u2EvcRefId);

    if (pLcmEvcEntryInPort == NULL)
    {
        return LCM_FAILURE;
    }

    pLcmPortEntryInEvc = LcmGetPortEntryInEvcInfo
        (pLcmEvcEntryInPort->pLcmLocalEvcInfo, u2PortNo);

    pLcmEvcCeVlanInfo->u2EvcReferenceId = u2EvcRefId;

    if (pLcmPortEntryInEvc == NULL)
    {
        return LCM_FAILURE;
    }

    pLcmEvcCeVlanInfo->u2FirstVlanId = pLcmPortEntryInEvc->u2FirstVlan;

    pLcmEvcCeVlanInfo->u2LastVlanId = pLcmPortEntryInEvc->u2LastVlan;

    pLcmEvcCeVlanInfo->u2NoOfVlansMapped =
        pLcmPortEntryInEvc->u2NoOfVlansMapped;

    MEMCPY (pLcmEvcCeVlanInfo->CeVlanMapInfo,
            pLcmPortEntryInEvc->CeVlanMapInfo, sizeof (tIssVlanList));

    pLcmEvcCeVlanInfo->u1UntagPriTag = LCM_INIT_VAL;

    pLcmEvcCeVlanInfo->u1DefaultEvc = LCM_INIT_VAL;

    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LcmGetNextEvcCeVlanInfo                              */
/*                                                                           */
/* Description        : This routines fetches CE vlan info for maximum of 10 */
/*                           EVCs                                            */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcRefId - Refernce ID of the EVC after which      */
/*                          CEVLAN info has to be fetched                    */
/*                      pLcmEvcCeVlanInfo - CE VLAN info structure to be     */
/*                          filled with the info                             */
/*                      pu1NoEvcInfo - No of EVCs whose CE Vlan info is      */
/*                          fetched                                          */
/*                                                                           */
/* Output(s)          : pLcmEvcCeVlanInfo- CE Vlan info structure            */
/*                      pu1NoEvcInfo - No of EVCs whose CE Vlan info is      */
/*                          fetched                                          */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
INT4
LcmGetNextEvcCeVlanInfo (UINT2 u2PortNo, UINT2 u2EvcRefId,
                         tLcmEvcCeVlanInfo * pLcmEvcCeVlanInfo,
                         UINT1 *pu1NoEvcInfo)
{
    tLcmEvcEntryInPort *pLcmEvcEntryInPort = NULL;
    tLcmPortEntryInEvc *pLcmPortEntryInEvc = NULL;

    *pu1NoEvcInfo = 0;

    while (*pu1NoEvcInfo < 10)
    {

        if (u2EvcRefId == 0)
        {
            pLcmEvcEntryInPort = LcmGetFirstEvcEntryInPortInfo (u2PortNo);
        }
        else
        {
            pLcmEvcEntryInPort = LcmGetNextEvcEntryInPortInfoBasedOnEvcRefId
                (u2PortNo, u2EvcRefId);
        }

        if (pLcmEvcEntryInPort == NULL)
        {
            return LCM_SUCCESS;
        }

        pLcmPortEntryInEvc = LcmGetPortEntryInEvcInfo
            (pLcmEvcEntryInPort->pLcmLocalEvcInfo, u2PortNo);

        if (pLcmPortEntryInEvc == NULL)
        {
            u2EvcRefId = pLcmEvcEntryInPort->u2EvcRefId;
            pLcmEvcEntryInPort = NULL;
            pLcmPortEntryInEvc = NULL;
            continue;
        }

        pLcmEvcCeVlanInfo[*pu1NoEvcInfo].u2EvcReferenceId =
            pLcmEvcEntryInPort->u2EvcRefId;

        pLcmEvcCeVlanInfo[*pu1NoEvcInfo].u2FirstVlanId =
            pLcmPortEntryInEvc->u2FirstVlan;

        pLcmEvcCeVlanInfo[*pu1NoEvcInfo].u2LastVlanId =
            pLcmPortEntryInEvc->u2LastVlan;

        pLcmEvcCeVlanInfo[*pu1NoEvcInfo].u2NoOfVlansMapped =
            pLcmPortEntryInEvc->u2NoOfVlansMapped;

        MEMCPY (pLcmEvcCeVlanInfo[*pu1NoEvcInfo].CeVlanMapInfo,
                pLcmPortEntryInEvc->CeVlanMapInfo, sizeof (tIssVlanList));

        pLcmEvcCeVlanInfo[*pu1NoEvcInfo].u1UntagPriTag = LCM_INIT_VAL;

        pLcmEvcCeVlanInfo[*pu1NoEvcInfo].u1DefaultEvc = LCM_INIT_VAL;

        u2EvcRefId = pLcmEvcEntryInPort->u2EvcRefId;
        (*pu1NoEvcInfo)++;
        pLcmEvcEntryInPort = NULL;
        pLcmPortEntryInEvc = NULL;
    }

    return LCM_SUCCESS;
}

/* Required by EVC Provisioning Module */

/*****************************************************************************/
/* Function Name      : LcmFindEvcId                                         */
/*                                                                           */
/* Description        : This routines searches for the given EVC             */
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC to be searched                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
INT4
LcmFindEvcId (UINT1 *pu1EvcId)
{
    if (LcmGetEvcEntryInContextInfo (0, pu1EvcId) == NULL)
    {
        return LCM_FAILURE;
    }

    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LcmCreateEvc                                         */
/*                                                                           */
/* Description        : This routines creates an EVC                         */
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC to be created                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
INT4
LcmCreateEvc (UINT1 *pu1EvcId)
{
    tLcmLocalEvcInfo   *pLcmLocalEvcInfo = NULL;
    UINT1               Offset = 0;
    UINT2               u2Len = 0;

    if (LCM_ALLOC_EVC_INFO_MEM_BLOCK (pLcmLocalEvcInfo) == NULL)
    {
        /* CLI_PRINTF("\n Max EVC created cannot create more EVC"); */
        return LCM_FAILURE;
    }

    LCM_MEMSET (pLcmLocalEvcInfo, LCM_INIT_VAL, sizeof (tLcmLocalEvcInfo));

    pLcmLocalEvcInfo->LcmPortRBTree =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tLcmPortEntryInEvc,
                                             LcmPortEntryInEvcNode),
                              (tRBCompareFn) LcmPortEntryInEvcInfoTblCmpFn);

    Offset = FSAP_OFFSETOF (tLcmLocalEvcInfo, LcmPortRBTree);
    UNUSED_PARAM (Offset);
    if (pLcmLocalEvcInfo->LcmPortRBTree == NULL)
    {
        LCM_RELEASE_EVC_INFO_MEM_BLOCK (pLcmLocalEvcInfo);
        return LCM_FAILURE;
    }

    pLcmLocalEvcInfo->u1EvcStatus = 0x03;

    /* ISSUE BEGIN: Bandwidth Profile for Priority 0 is hardcoded */
    pLcmLocalEvcInfo->u1NoOfCos = 1;
    pLcmLocalEvcInfo->EvcBwProfile[0].u1PerCosBit = 1;
    /* ISSUE END  */
    u2Len =
        (UINT2) (STRLEN(pu1EvcId) <
                (sizeof(pLcmLocalEvcInfo->EvcIdentifier)-1) ?
                STRLEN(pu1EvcId): (sizeof(pLcmLocalEvcInfo->EvcIdentifier)-1));

    STRNCPY (pLcmLocalEvcInfo->EvcIdentifier, pu1EvcId,
             u2Len);
    pLcmLocalEvcInfo->EvcIdentifier[u2Len] = '\0';

    if (LcmAddToEvcEntryInContextInfoTable (pLcmLocalEvcInfo, 0) == LCM_FAILURE)
    {
        RBTreeDelete (pLcmLocalEvcInfo->LcmPortRBTree);
        LCM_RELEASE_EVC_INFO_MEM_BLOCK (pLcmLocalEvcInfo);
        return LCM_FAILURE;
    }

    KW_FALSEPOSITIVE_FIX (pLcmLocalEvcInfo->LcmPortRBTree->SemId);
    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LcmDeleteEtherServiceInstance                        */
/*                                                                           */
/* Description        : This routines deletes the Ethernet Service Instance  */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EtherServiceInstance - Service instance to be      */
/*                          deleted                                          */
/*                      pu1EvcId - EVC identifier                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
VOID 
     
     
     
     
     
     
     
    LcmDeleteEtherServiceInstance
    (UINT2 u2PortNo, UINT2 u2EtherServiceInstance, UINT1 *pu1EvcId)
{
    tLcmEvcEntryInPort *pLcmEvcEntryInPort = NULL;
    tLcmPortEntryInEvc *pLcmPortEntryInEvc = NULL;

    LCM_UNUSED (u2EtherServiceInstance);

    pLcmEvcEntryInPort = LcmGetFirstEvcEntryInPortInfo (u2PortNo);

    if (pLcmEvcEntryInPort == NULL)
    {
        printf ("\n No Evc configured on this port");
        return;
    }

    if (STRCMP (pLcmEvcEntryInPort->pLcmLocalEvcInfo->EvcIdentifier, pu1EvcId)
        == 0)
    {
        pLcmPortEntryInEvc =
            LcmGetPortEntryInEvcInfo (pLcmEvcEntryInPort->pLcmLocalEvcInfo,
                                      u2PortNo);

        if (pLcmPortEntryInEvc == NULL)
        {
            printf
                ("Error removing PortEntry from Evc Port Not present in EVC ");
        }

        LcmDeleteEvcRefId (u2PortNo, pLcmEvcEntryInPort->u2EvcRefId);
        if (LcmRemoveFromPortEntryInEvcInfoTable (pLcmPortEntryInEvc,
                                                  pLcmEvcEntryInPort->
                                                  pLcmLocalEvcInfo) ==
            LCM_FAILURE)
        {
            printf ("Error removing Port Entry from Evc");
        }

        LCM_RELEASE_PORTNODE_IN_EVC_INFO_MEM_BLOCK (pLcmPortEntryInEvc);

        if (LcmRemoveFromEvcEntryInPortInfoTable (pLcmEvcEntryInPort, u2PortNo)
            == LCM_FAILURE)
        {
            printf ("Error removing Evc Entry from Port");
        }

        LCM_RELEASE_EVCNODE_IN_PORT_INFO_MEM_BLOCK (pLcmEvcEntryInPort);

        if (ElmGetPortMode (u2PortNo) == ELM_NETWORK_SIDE)
        {
            ElmEvcInformationChangeIndication (u2PortNo);
        }
        return;
    }

    while ((pLcmEvcEntryInPort =
            LcmGetNextEvcEntryInPortInfo (pLcmEvcEntryInPort,
                                          u2PortNo)) != NULL)
    {
        if (STRCMP
            (pLcmEvcEntryInPort->pLcmLocalEvcInfo->EvcIdentifier,
             pu1EvcId) == 0)
        {
            pLcmPortEntryInEvc =
                LcmGetPortEntryInEvcInfo (pLcmEvcEntryInPort->pLcmLocalEvcInfo,
                                          u2PortNo);
            LcmDeleteEvcRefId (u2PortNo, pLcmEvcEntryInPort->u2EvcRefId);
            if (LcmRemoveFromPortEntryInEvcInfoTable (pLcmPortEntryInEvc,
                                                      pLcmEvcEntryInPort->
                                                      pLcmLocalEvcInfo) ==
                LCM_FAILURE)
            {
                printf ("Error removing Port Entry from Evc");
            }

            LCM_RELEASE_PORTNODE_IN_EVC_INFO_MEM_BLOCK (pLcmPortEntryInEvc);

            if (LcmRemoveFromEvcEntryInPortInfoTable
                (pLcmEvcEntryInPort, u2PortNo) == LCM_FAILURE)
            {
                printf ("Error removing Evc Entry from Port");
            }

            LCM_RELEASE_EVCNODE_IN_PORT_INFO_MEM_BLOCK (pLcmEvcEntryInPort);

            if (ElmGetPortMode (u2PortNo) == ELM_NETWORK_SIDE)
            {
                ElmEvcInformationChangeIndication (u2PortNo);
            }

            return;
        }
    }

    return;
}

/*****************************************************************************/
/* Function Name      : LcmDeleteEvc                                         */
/*                                                                           */
/* Description        : This routine deletes the EVC                         */
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC to be deleted                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
VOID
LcmDeleteEvc (UINT1 *pu1EvcId)
{
    tLcmPortEntryInEvc *pLcmPortEntryInEvc = NULL;
    tLcmEvcEntryInPort *pLcmEvcEntryInPort = NULL;
    tLcmLocalEvcInfo   *pLcmLocalEvcInfo = NULL;
    tLcmPortEntry      *pLcmPortEntry = NULL;
    UINT2               u2TempPortNo = 0;

    pLcmLocalEvcInfo = LcmGetEvcEntryInContextInfo (0, pu1EvcId);

    if (pLcmLocalEvcInfo != NULL)
    {
        while ((pLcmPortEntryInEvc =
                LcmGetFirstPortEntryInEvcInfo (pLcmLocalEvcInfo)) != NULL)
        {
            u2TempPortNo = pLcmPortEntryInEvc->u2PortNo;
            pLcmEvcEntryInPort =
                LcmGetEvcEntryInPortInfo (pLcmPortEntryInEvc->u2PortNo,
                                          pLcmPortEntryInEvc->u2EvcReferenceID);
            if (pLcmEvcEntryInPort == NULL)
            {
                return;
            }
            LcmDeleteEvcRefId (pLcmPortEntryInEvc->u2PortNo,
                               pLcmEvcEntryInPort->u2EvcRefId);
            if (LcmRemoveFromEvcEntryInPortInfoTable
                (pLcmEvcEntryInPort,
                 pLcmPortEntryInEvc->u2PortNo) == LCM_FAILURE)
            {
                printf ("Error removing Evc Entry from Port");
            }
            LCM_RELEASE_EVCNODE_IN_PORT_INFO_MEM_BLOCK (pLcmEvcEntryInPort);
            pLcmPortEntry = LCM_GET_PORTENTRY (u2TempPortNo);
            if (pLcmPortEntry == NULL)
            {
                return;
            }
            pLcmPortEntry->u1NoOfEvc--;

            if (LcmRemoveFromPortEntryInEvcInfoTable (pLcmPortEntryInEvc,
                                                      pLcmLocalEvcInfo) ==
                LCM_FAILURE)
            {
                printf ("Error removing Port Entry from Evc");
            }

            LCM_RELEASE_PORTNODE_IN_EVC_INFO_MEM_BLOCK (pLcmPortEntryInEvc);

            ElmEvcInformationChangeIndication (u2TempPortNo);
        }

        LcmRemoveFromEvcEntryInContextInfoTable (pLcmLocalEvcInfo, 0);
        LcmFreeEvcEntryInContextInfo (pLcmLocalEvcInfo, 0);
    }

    /* Delete entry in EVC Table */
    /* Indicate ELMI module */
    return;
}

/*****************************************************************************/
/* Function Name      : LcmUpdateEvcAttributes                               */
/*                                                                           */
/* Description        : This routines updates the attributes of the given EVC*/
/*                                                                           */
/* Input(s)           : u2RefId - Evc Reference ID                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
VOID
LcmUpdateEvcAttributes (UINT2 u2RefId)
{
    LCM_UNUSED (u2RefId);
    /* Update the received Information for this EVC */
    /* Indicate ELMI module */
    return;
}

/*****************************************************************************/
/* Function Name      : LcmUpdateEvcOamProtocolMap                           */
/*                                                                           */
/* Description        : This routines updates the protocol map of the EVC    */
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC identifier                            */
/*                      u1OamProtocol - Oam Protocol to be mapped            */
/*                      u2SVlan - SVLAN for the EVC                          */
/*                      pu1DomainName - Domain name                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
INT4
LcmUpdateEvcOamProtocolMap (UINT1 *pu1EvcId, UINT1 u1OamProtocol, UINT2
                            u2SVlan, UINT1 *pu1DomainName)
{
    tLcmLocalEvcInfo   *pLcmLocalEvcInfo = NULL;
    UINT2               u2Len = 0;

    if (CfmFindDomain (pu1DomainName) != LCM_SUCCESS)
    {
        /*        CLI_PRINTF("Domain doest not exist,First Create Domain before issuing 
           "
           "this command");
         */
        return LCM_FAILURE;
    }

    pLcmLocalEvcInfo = LcmGetEvcEntryInContextInfo (0, pu1EvcId);

    if (pLcmLocalEvcInfo != NULL)
    {
        pLcmLocalEvcInfo->u2SVlan = u2SVlan;

        u2Len =
            (UINT2) (STRLEN(pu1DomainName) <
                    (sizeof(pLcmLocalEvcInfo->au1DomainName)-1) ?
                    STRLEN(pu1DomainName) : (sizeof(pLcmLocalEvcInfo->au1DomainName)-1));

        STRNCPY (pLcmLocalEvcInfo->au1DomainName, pu1DomainName,
                 u2Len);

        pLcmLocalEvcInfo->au1DomainName[u2Len] = '\0';
        pLcmLocalEvcInfo->u1OAMProtocolUsed = u1OamProtocol;
        if (CfmCreateMA (pu1EvcId, u2SVlan, pu1DomainName) != LCM_SUCCESS)
        {
            return LCM_FAILURE;
        }

        return LCM_SUCCESS;
    }
    return LCM_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LcmUpdateEvcUniCount                                 */
/*                                                                           */
/* Description        : This routine updates the UNI count for the EVC       */
/*                                                                           */
/* Input(s)           : pu1EvcId -EVC identifier whose count is to be updated*/
/*                      u2UniCount - No of attached UNIs                     */
/*                      u4UniType - UNI type                                 */
/*                                                                           */
/* Output(s)          : pLcmEvcStatusInfo - EVC status info structure        */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
INT4
LcmUpdateEvcUniCount (UINT1 *pu1EvcId, UINT2 u2UniCount, UINT4 u4UniType)
{
    tLcmLocalEvcInfo   *pLcmLocalEvcInfo = NULL;

    LCM_UNUSED (u4UniType);

    pLcmLocalEvcInfo = LcmGetEvcEntryInContextInfo (0, pu1EvcId);

    if (pLcmLocalEvcInfo != NULL)
    {
        pLcmLocalEvcInfo->u2UniCount = u2UniCount;
        if (CfmCreateMeps (pu1EvcId, u2UniCount) != LCM_SUCCESS)
        {
            return LCM_FAILURE;
        }
        return LCM_SUCCESS;
    }
    return LCM_FAILURE;
}

/* Required by Vlan Module */

/*****************************************************************************/
/* Function Name      : LcmUpdateEvcEtherServiceInstance                     */
/*                                                                           */
/* Description        : This routine updates the Service Instance for an EVC */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EtherServiceInstance - New Service Instance        */
/*                      pu1EvcId - EVC Id                                    */
/*                      u2AssignedEvcRefId - Assigned EVC ref ID             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
INT4
LcmUpdateEvcEtherServiceInstance (UINT2 u2PortNo,
                                  UINT2 u2EtherServiceInstance,
                                  UINT1 *pu1EvcId, UINT2 u2AssignedEvcRefId)
{
    tLcmPortEntryInEvc *pLcmPortEntryInEvc = NULL;
    tLcmEvcEntryInPort *pLcmEvcEntryInPort = NULL;
    tLcmPortEntry      *pLcmPortEntry = NULL;
    tLcmLocalEvcInfo   *pLcmLocalEvcInfo = NULL;
    UINT2               u2EvcRefId = 0;

    /* If pu1EvcId is NULL then Map this service instance to default 
       EVC */
    if (pu1EvcId == NULL)
    {
        pLcmPortEntry = LCM_GET_PORTENTRY (u2PortNo);

        if (pLcmPortEntry == NULL)
        {
            return LCM_FAILURE;
        }

        /* Check if this Service instance is already mapped to this EVC if yes 
           then return LCM_SUCCESS, Loop through Port Info for this EVC Node */
        pLcmPortEntryInEvc =
            LcmGetFirstPortEntryInEvcInfo (pLcmPortEntry->
                                           pLcmDefaultLocalEvcInfo);

        if (LcmAddToPortEntryInEvcInfoTable
            (pLcmPortEntryInEvc,
             pLcmPortEntry->pLcmDefaultLocalEvcInfo) != LCM_SUCCESS)
        {
            return LCM_FAILURE;
        }

        /* Allocate EvcNode from Mempool */
        if (LCM_ALLOC_EVCNODE_IN_PORT_INFO_MEM_BLOCK (pLcmEvcEntryInPort) ==
            NULL)
        {
            return LCM_FAILURE;
        }

        LCM_MEMSET (pLcmEvcEntryInPort, LCM_INIT_VAL,
                    sizeof (tLcmEvcEntryInPort));

        /* Use Default Reference ID for this EVC */
        pLcmEvcEntryInPort->u2EvcRefId = u2AssignedEvcRefId;
        pLcmEvcEntryInPort->pLcmLocalEvcInfo =
            pLcmPortEntry->pLcmDefaultLocalEvcInfo;

        /* Add EvcNode pointer to LcmPortEntry */
        if (LcmAddToEvcEntryInPortInfoTable
            (pLcmEvcEntryInPort, u2PortNo) != LCM_SUCCESS)
        {
            return LCM_FAILURE;
        }

        if (ElmGetPortMode (u2PortNo) == ELM_NETWORK_SIDE)
        {
            ElmEvcInformationChangeIndication (u2PortNo);
        }
        return LCM_SUCCESS;
    }
    /* else ( pu1EvcId is not NULL) */
    else
    {
        pLcmEvcEntryInPort = LcmGetFirstEvcEntryInPortInfo (u2PortNo);

        pLcmPortEntry = LCM_GET_PORTENTRY (u2PortNo);
        if (pLcmPortEntry == NULL)
        {
            return LCM_FAILURE;
        }

        if (pLcmEvcEntryInPort != NULL)
        {
            if (pLcmEvcEntryInPort->u4EtherServInstance ==
                u2EtherServiceInstance)
            {
                if (STRCMP
                    (pLcmEvcEntryInPort->pLcmLocalEvcInfo->EvcIdentifier,
                     pu1EvcId) == 0)
                {
                    return LCM_SUCCESS;
                }
                else
                {
                    return LCM_FAILURE;
                }
            }

            while ((pLcmEvcEntryInPort =
                    LcmGetNextEvcEntryInPortInfo (pLcmEvcEntryInPort,
                                                  u2PortNo)) != NULL)
            {
                if (pLcmEvcEntryInPort->u4EtherServInstance ==
                    u2EtherServiceInstance)
                {
                    if (STRCMP
                        (pLcmEvcEntryInPort->pLcmLocalEvcInfo->EvcIdentifier,
                         pu1EvcId) == 0)
                    {
                        return LCM_SUCCESS;
                    }
                    else
                    {
                        return LCM_FAILURE;
                    }
                }
            }
            pLcmEvcEntryInPort = NULL;
        }

        pLcmLocalEvcInfo = LcmGetEvcEntryInContextInfo (0, pu1EvcId);

        if (pLcmLocalEvcInfo != NULL)
        {
            /* Check if this Service instance is already mapped to this 
               EVC if yes then return LCM_SUCCESS, For this Loop through 
               Port Info for this EVC Node */
            pLcmPortEntryInEvc =
                LcmGetPortEntryInEvcInfo (pLcmLocalEvcInfo, u2PortNo);

            if (pLcmPortEntryInEvc != NULL)
            {
                if (pLcmPortEntryInEvc->u4EtherServInstance ==
                    u2EtherServiceInstance)
                {
                    return LCM_SUCCESS;
                }
                else
                {
                    return LCM_FAILURE;
                }
            }

            if (pLcmPortEntry->u1NoOfEvc == EVCPRO_MAX_EVC_PER_PORT)
            {
                return LCM_FAILURE;
            }

            /* Allocate EvcPortNode from Mempool */
            if (LCM_ALLOC_PORTNODE_IN_EVC_INFO_MEM_BLOCK (pLcmPortEntryInEvc)
                == NULL)
            {
                return LCM_FAILURE;
            }

            LCM_MEMSET (pLcmPortEntryInEvc, LCM_INIT_VAL,
                        sizeof (tLcmPortEntryInEvc));

            /* Add this node to Default EVC node and update 
               u2EtherServiceInstance 
             */
            pLcmPortEntryInEvc->u4EtherServInstance = u2EtherServiceInstance;
            pLcmPortEntryInEvc->u2PortNo = u2PortNo;

            if (u2AssignedEvcRefId == LCM_INIT_VAL)
            {
                if (LcmGenerateEvcRefId (u2PortNo, &u2EvcRefId) != LCM_SUCCESS)
                {
                    LCM_RELEASE_PORTNODE_IN_EVC_INFO_MEM_BLOCK
                        (pLcmPortEntryInEvc);
                    return LCM_FAILURE;
                }
            }
            else
            {
                u2EvcRefId = u2AssignedEvcRefId;
            }

            pLcmPortEntryInEvc->u2EvcReferenceID = u2EvcRefId;

            if (LcmAddToPortEntryInEvcInfoTable
                (pLcmPortEntryInEvc, pLcmLocalEvcInfo) != LCM_SUCCESS)
            {
                LCM_RELEASE_PORTNODE_IN_EVC_INFO_MEM_BLOCK (pLcmPortEntryInEvc);
                return LCM_FAILURE;
            }

            /* Allocate EvcNode from Mempool */
            if (LCM_ALLOC_EVCNODE_IN_PORT_INFO_MEM_BLOCK (pLcmEvcEntryInPort)
                == NULL)
            {
                return LCM_FAILURE;
            }

            LCM_MEMSET (pLcmEvcEntryInPort, LCM_INIT_VAL,
                        sizeof (tLcmEvcEntryInPort));

            /* Use Default Reference ID for this EVC */
            pLcmEvcEntryInPort->u2EvcRefId = u2EvcRefId;
            pLcmEvcEntryInPort->u4EtherServInstance = u2EtherServiceInstance;
            pLcmEvcEntryInPort->u1EvcStatus = pLcmLocalEvcInfo->u1EvcStatus;
            pLcmEvcEntryInPort->pLcmLocalEvcInfo = pLcmLocalEvcInfo;

            /* Add EvcNode pointer to LcmPortEntry */
            if (LcmAddToEvcEntryInPortInfoTable
                (pLcmEvcEntryInPort, u2PortNo) != LCM_SUCCESS)
            {
                LCM_RELEASE_EVCNODE_IN_PORT_INFO_MEM_BLOCK (pLcmEvcEntryInPort);
                return LCM_FAILURE;
            }

            pLcmPortEntry->u1NoOfEvc++;

            if (ElmGetPortMode (u2PortNo) == ELM_NETWORK_SIDE)
            {
                ElmEvcInformationChangeIndication (u2PortNo);
            }
            return LCM_SUCCESS;
        }
    }
    return LCM_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LcmUpdateCVlanEvcMap                                 */
/*                                                                           */
/* Description        : This routine updates the CVLAN map for an EVC        */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      pu1EvcId - EVC Id                                    */
/*                      u4Vlan - Vlan ID                                     */
/*                      u4CommandType - Type of command: To map or unmap     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
INT4
LcmUpdateCVlanEvcMap (UINT2 u2PortNo, UINT1 *pu1EvcId,
                      UINT4 u4Vlan, UINT4 u4CommandType)
{
    /*tLcmVlanList *pLcmVlanList; */
    tLcmPortEntryInEvc *pLcmPortEntryInEvc = NULL;
    tLcmPortEntry      *pLcmPortEntry = NULL;
    tLcmLocalEvcInfo   *pLcmLocalEvcInfo = NULL;
    UINT2               u2TempVlanId = 0;
    UINT2               u2TempPortNo = 0;
    BOOL1               bResult = OSIX_FALSE;

    if (pu1EvcId == NULL)
    {

        /* If pu1EvcId is NULL then Map this service instance to default 
           EVC */
        pLcmPortEntry = LCM_GET_PORTENTRY (u2PortNo);
        if (pLcmPortEntry == NULL)
        {
            return LCM_FAILURE;
        }

        /* Search for EvcPortNode for u2PortNo */
        /* If not found then return LCM_FAILURE */
        pLcmPortEntryInEvc = LcmGetPortEntryInEvcInfo
            (pLcmPortEntry->pLcmDefaultLocalEvcInfo, u2PortNo);

        if (pLcmPortEntryInEvc == NULL)
        {
            return LCM_FAILURE;
        }
        /* else Update the port node with received Cvlan List using BIT Map 
           macros */

        for (u2TempVlanId = pLcmPortEntryInEvc->u2FirstVlan;
             u2TempVlanId <= pLcmPortEntryInEvc->u2LastVlan; u2TempVlanId++)
        {
            OSIX_BITLIST_IS_BIT_SET (pLcmPortEntryInEvc->CeVlanMapInfo,
                                     u2TempVlanId, LCM_CE_VLAN_SIZE, bResult);

            if (bResult == OSIX_FALSE && u4CommandType == LCM_MAP_VLAN)
            {
                OSIX_BITLIST_SET_BIT (pLcmPortEntryInEvc->CeVlanMapInfo,
                                      u2TempVlanId, LCM_CE_VLAN_SIZE);
            }
        }

        /* for all ports in this EVC call */
        pLcmPortEntryInEvc = LcmGetFirstPortEntryInEvcInfo
            (pLcmPortEntry->pLcmDefaultLocalEvcInfo);

        if (pLcmPortEntryInEvc != NULL)
        {
            if (ElmGetPortMode (u2PortNo) == ELM_NETWORK_SIDE)
            {
                ElmEvcInformationChangeIndication (pLcmPortEntryInEvc->
                                                   u2PortNo);
            }

            u2TempPortNo = pLcmPortEntryInEvc->u2PortNo;
            pLcmPortEntryInEvc = NULL;

            while ((pLcmPortEntryInEvc =
                    LcmGetNextPortEntryInEvcInfoBasedOnPortNo
                    (pLcmPortEntry->pLcmDefaultLocalEvcInfo,
                     u2TempPortNo)) != NULL)
            {
                if (ElmGetPortMode (u2PortNo) == ELM_NETWORK_SIDE)
                {
                    ElmEvcInformationChangeIndication (pLcmPortEntryInEvc->
                                                       u2PortNo);
                }

                u2TempPortNo = pLcmPortEntryInEvc->u2PortNo;
                pLcmPortEntryInEvc = NULL;
            }
        }

        return LCM_SUCCESS;
    }

    /* else ( pu1EvcId is not NULL) */
    pLcmLocalEvcInfo = LcmGetEvcEntryInContextInfo (0, pu1EvcId);

    /* GET_NODE From EVC RBTree */
    if (pLcmLocalEvcInfo != NULL)
    {
        /* If same then Search for EvcPortNode for u2PortNo */
        pLcmPortEntryInEvc = LcmGetPortEntryInEvcInfo
            (pLcmLocalEvcInfo, u2PortNo);

        if (pLcmPortEntryInEvc == NULL)
        {
            /* If not found then return LCM_FAILURE */
            return LCM_FAILURE;
        }
        /* else Update the port node with received Cvlan List using 
           BIT  Map macros */
        OSIX_BITLIST_IS_BIT_SET (pLcmPortEntryInEvc->CeVlanMapInfo,
                                 (UINT2) u4Vlan, LCM_CE_VLAN_SIZE, bResult);

        if (bResult == OSIX_FALSE && u4CommandType == LCM_MAP_VLAN)
        {
            pLcmPortEntryInEvc->u2NoOfVlansMapped++;
            OSIX_BITLIST_SET_BIT (pLcmPortEntryInEvc->CeVlanMapInfo,
                                  (UINT2) u4Vlan, LCM_CE_VLAN_SIZE);
            if (((UINT2) u4Vlan <= pLcmPortEntryInEvc->u2FirstVlan)
                || (pLcmPortEntryInEvc->u2FirstVlan == 0))
            {
                pLcmPortEntryInEvc->u2FirstVlan = (UINT2) u4Vlan;
            }

            if (((UINT2) u4Vlan > pLcmPortEntryInEvc->u2LastVlan) ||
                (pLcmPortEntryInEvc->u2LastVlan == 0))
            {
                pLcmPortEntryInEvc->u2LastVlan = (UINT2) u4Vlan;
            }

            /* for all ports in this EVC call */
            pLcmPortEntryInEvc = LcmGetFirstPortEntryInEvcInfo
                (pLcmLocalEvcInfo);

            if (pLcmPortEntryInEvc != NULL)
            {
                if (ElmGetPortMode (u2PortNo) == ELM_NETWORK_SIDE)
                {
                    ElmEvcInformationChangeIndication (pLcmPortEntryInEvc->
                                                       u2PortNo);
                }

                u2TempPortNo = pLcmPortEntryInEvc->u2PortNo;
                pLcmPortEntryInEvc = NULL;

                while ((pLcmPortEntryInEvc =
                        LcmGetNextPortEntryInEvcInfoBasedOnPortNo
                        (pLcmLocalEvcInfo, u2TempPortNo)) != NULL)
                {
                    ElmEvcInformationChangeIndication (pLcmPortEntryInEvc->
                                                       u2PortNo);
                    u2TempPortNo = pLcmPortEntryInEvc->u2PortNo;
                    pLcmPortEntryInEvc = NULL;
                }
            }
        }
        else if (bResult == OSIX_TRUE && u4CommandType == LCM_NO_MAP_VLAN)
        {
            pLcmPortEntryInEvc->u2NoOfVlansMapped--;
            OSIX_BITLIST_RESET_BIT (pLcmPortEntryInEvc->CeVlanMapInfo,
                                    (UINT2) u4Vlan, LCM_CE_VLAN_SIZE);

            if ((UINT2) u4Vlan == pLcmPortEntryInEvc->u2FirstVlan)
            {
                for (u2TempVlanId = pLcmPortEntryInEvc->u2FirstVlan;
                     u2TempVlanId <= pLcmPortEntryInEvc->u2LastVlan;
                     u2TempVlanId++)
                {
                    OSIX_BITLIST_IS_BIT_SET (pLcmPortEntryInEvc->CeVlanMapInfo,
                                             u2TempVlanId, LCM_CE_VLAN_SIZE,
                                             bResult);

                    if (bResult == OSIX_TRUE)
                    {
                        pLcmPortEntryInEvc->u2FirstVlan = u2TempVlanId;
                        break;
                    }
                }
            }

            if ((UINT2) u4Vlan == pLcmPortEntryInEvc->u2LastVlan)
            {
                for (u2TempVlanId = pLcmPortEntryInEvc->u2LastVlan;
                     u2TempVlanId >= pLcmPortEntryInEvc->u2FirstVlan;
                     u2TempVlanId--)
                {
                    OSIX_BITLIST_IS_BIT_SET (pLcmPortEntryInEvc->CeVlanMapInfo,
                                             u2TempVlanId, LCM_CE_VLAN_SIZE,
                                             bResult);

                    if (bResult == OSIX_TRUE)
                    {
                        pLcmPortEntryInEvc->u2LastVlan = u2TempVlanId;
                        break;
                    }
                }
            }

            /* for all ports in this EVC call */
            pLcmPortEntryInEvc = LcmGetFirstPortEntryInEvcInfo
                (pLcmLocalEvcInfo);

            if (pLcmPortEntryInEvc != NULL)
            {
                if (ElmGetPortMode (u2PortNo) == ELM_NETWORK_SIDE)
                {
                    ElmEvcInformationChangeIndication (pLcmPortEntryInEvc->
                                                       u2PortNo);
                }

                u2TempPortNo = pLcmPortEntryInEvc->u2PortNo;
                pLcmPortEntryInEvc = NULL;

                while ((pLcmPortEntryInEvc =
                        LcmGetNextPortEntryInEvcInfoBasedOnPortNo
                        (pLcmLocalEvcInfo, u2TempPortNo)) != NULL)
                {
                    ElmEvcInformationChangeIndication (pLcmPortEntryInEvc->
                                                       u2PortNo);
                    u2TempPortNo = pLcmPortEntryInEvc->u2PortNo;
                    pLcmPortEntryInEvc = NULL;
                }
            }
        }
        return LCM_SUCCESS;
    }
    /*else (If EVC ID not Present) */
    return LCM_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LcmSetEvcBwProfile                                   */
/*                                                                           */
/* Description        : This routine updates the Bandwidth Profile of an EVC */
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC Id                                    */
/*                      u4Cm - Coloring Mode                                 */
/*                      u4Cf - Coupling Flag                                 */
/*                      u4PerCosBit - Per Cos Bit                            */
/*                      u4CirMagnitude - CIR Magnitude                       */
/*                      u4CirMultiplier - CIR Multiplier                     */
/*                      u4CbsMagnitude - CBS Magnitude                       */
/*                      u4CbsMultiplier - CBS Multiplier                     */
/*                      u4EirMagnitude - EIR Magnitude                       */
/*                      u4EirMultiplier - EIR Multiplier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
INT4
LcmSetEvcBwProfile (UINT1 *pu1EvcId, UINT4 u4Cm, UINT4 u4Cf, UINT4 u4PerCosBit,
                    UINT4 u4CirMagnitude, UINT4 u4CirMultiplier,
                    UINT4 u4CbsMagnitude, UINT4 u4CbsMultiplier,
                    UINT4 u4EirMagnitude, UINT4 u4EirMultiplier,
                    UINT4 u4EbsMagnitude, UINT4 u4EbsMultiplier,
                    UINT4 u4PriorityBits)
{
    tLcmLocalEvcInfo   *pLcmLocalEvcInfo = NULL;
    tLcmPortEntryInEvc *pLcmPortEntryInEvc = NULL;
    UINT2               u2TempPortNo = 0;
    BOOL1               bResult = OSIX_FALSE;

    pLcmLocalEvcInfo = LcmGetEvcEntryInContextInfo (0, pu1EvcId);

    /* GET_NODE From EVC RBTree */
    if (pLcmLocalEvcInfo != NULL)
    {
        pLcmLocalEvcInfo->EvcBwProfile[u4PriorityBits].u1Cm = (UINT1) u4Cm;
        pLcmLocalEvcInfo->EvcBwProfile[u4PriorityBits].u1Cf = (UINT1) u4Cf;
        pLcmLocalEvcInfo->EvcBwProfile[u4PriorityBits].u1CirMagnitude =
            (UINT1) u4CirMagnitude;
        pLcmLocalEvcInfo->EvcBwProfile[u4PriorityBits].u2CirMultiplier =
            (UINT2) u4CirMultiplier;
        pLcmLocalEvcInfo->EvcBwProfile[u4PriorityBits].u1CbsMagnitude =
            (UINT1) u4CbsMagnitude;
        pLcmLocalEvcInfo->EvcBwProfile[u4PriorityBits].u1CbsMultiplier =
            (UINT1) u4CbsMultiplier;
        pLcmLocalEvcInfo->EvcBwProfile[u4PriorityBits].u1EirMagnitude =
            (UINT1) u4EirMagnitude;
        pLcmLocalEvcInfo->EvcBwProfile[u4PriorityBits].u2EirMultiplier =
            (UINT2) u4EirMultiplier;
        pLcmLocalEvcInfo->EvcBwProfile[u4PriorityBits].u1EbsMagnitude =
            (UINT1) u4EbsMagnitude;
        pLcmLocalEvcInfo->EvcBwProfile[u4PriorityBits].u1EbsMultiplier =
            (UINT1) u4EbsMultiplier;
        pLcmLocalEvcInfo->EvcBwProfile[u4PriorityBits].u1PriorityBits =
            (UINT1) u4PriorityBits;

        pLcmLocalEvcInfo->EvcBwProfile[u4PriorityBits].u1PerCosBit =
            u4PerCosBit;

        OSIX_BITLIST_IS_BIT_SET (pLcmLocalEvcInfo->u1BwBitMap,
                                 (UINT1) u4PriorityBits, 1, bResult);
        if (bResult == OSIX_FALSE)
        {
            pLcmLocalEvcInfo->u1NoOfCos++;
            OSIX_BITLIST_SET_BIT (pLcmLocalEvcInfo->u1BwBitMap,
                                  (UINT1) u4PriorityBits, 1);
        }

        /* for all ports in this EVC call */
        pLcmPortEntryInEvc = LcmGetFirstPortEntryInEvcInfo (pLcmLocalEvcInfo);

        if (pLcmPortEntryInEvc != NULL)
        {
            if (ElmGetPortMode (pLcmPortEntryInEvc->u2PortNo) ==
                ELM_NETWORK_SIDE)
            {
                ElmEvcInformationChangeIndication (pLcmPortEntryInEvc->
                                                   u2PortNo);
            }

            u2TempPortNo = pLcmPortEntryInEvc->u2PortNo;
            pLcmPortEntryInEvc = NULL;

            while ((pLcmPortEntryInEvc =
                    LcmGetNextPortEntryInEvcInfoBasedOnPortNo (pLcmLocalEvcInfo,
                                                               u2TempPortNo)) !=
                   NULL)
            {
                ElmEvcInformationChangeIndication (pLcmPortEntryInEvc->
                                                   u2PortNo);
                u2TempPortNo = pLcmPortEntryInEvc->u2PortNo;
                pLcmPortEntryInEvc = NULL;
            }
        }

        return LCM_SUCCESS;
    }

    return LCM_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LcmSetDefaultEvc                                     */
/*                                                                           */
/* Description        : This routine sets an EVC as default for all the Vlans*/
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC Id                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
VOID
LcmSetDefaultEvc (UINT1 *pu1EvcId)
{
    LCM_UNUSED (pu1EvcId);
    /* Update the received Information for this EVC */
    /* Indicate ELMI module */
}

/* Required by CFM module */
/*****************************************************************************/
/* Function Name      : LcmEvcStatusChange                                   */
/*                                                                           */
/* Description        : This routine changes the status of an configured EVC */
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC Id                                    */
/*                      u1Status - Changed Status of the EVC                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
VOID
LcmEvcStatusChange (UINT1 *pu1EvcId, UINT1 u1Status)
{
    tLcmLocalEvcInfo   *pLcmLocalEvcInfo = NULL;
    tLcmPortEntryInEvc *pLcmPortEntryInEvc = NULL;
    tLcmEvcEntryInPort *pLcmEvcEntryInPort = NULL;
    UINT2               u2TempPortNo = 0;

    if (pu1EvcId == NULL)
    {
        return;
    }

    pLcmLocalEvcInfo = LcmGetEvcEntryInContextInfo (0, pu1EvcId);

    /* GET_NODE From EVC Table */
    /* Compare received EvcId with Nodes Id */
    if (pLcmLocalEvcInfo != NULL)
    {
        /* If Status received is different from existing Status then
           Indicate ELMI for EVC status Change */
        if (pLcmLocalEvcInfo->u1EvcStatus != u1Status)
        {
            pLcmLocalEvcInfo->u1EvcStatus = u1Status;

            /* for all ports in this EVC call */
            pLcmPortEntryInEvc = LcmGetFirstPortEntryInEvcInfo
                (pLcmLocalEvcInfo);

            if (pLcmPortEntryInEvc != NULL)
            {
                pLcmEvcEntryInPort =
                    LcmGetEvcEntryInPortInfo (pLcmPortEntryInEvc->u2PortNo,
                                              pLcmPortEntryInEvc->
                                              u2EvcReferenceID);
                if (pLcmEvcEntryInPort == NULL)
                {
                    return;
                }
                pLcmEvcEntryInPort->u1EvcStatus = u1Status;
                pLcmPortEntryInEvc->u1EvcStatus = u1Status;

                if (ElmGetPortMode (pLcmPortEntryInEvc->u2PortNo) ==
                    ELM_NETWORK_SIDE)
                {
                    ElmEvcStatusChangeIndication
                        (pLcmPortEntryInEvc->u2PortNo,
                         pLcmPortEntryInEvc->u2EvcReferenceID, u1Status);
                }
                u2TempPortNo = pLcmPortEntryInEvc->u2PortNo;
                pLcmPortEntryInEvc = NULL;

                while ((pLcmPortEntryInEvc =
                        LcmGetNextPortEntryInEvcInfoBasedOnPortNo
                        (pLcmLocalEvcInfo, u2TempPortNo)) != NULL)
                {
                    pLcmEvcEntryInPort =
                        LcmGetEvcEntryInPortInfo (pLcmPortEntryInEvc->u2PortNo,
                                                  pLcmPortEntryInEvc->
                                                  u2EvcReferenceID);
                    if (pLcmEvcEntryInPort == NULL)
                    {
                        return;
                    }
                    pLcmEvcEntryInPort->u1EvcStatus = u1Status;
                    pLcmPortEntryInEvc->u1EvcStatus = u1Status;
                    if (ElmGetPortMode (pLcmPortEntryInEvc->u2PortNo) ==
                        ELM_NETWORK_SIDE)
                    {
                        ElmEvcStatusChangeIndication
                            (pLcmPortEntryInEvc->u2PortNo,
                             pLcmPortEntryInEvc->u2EvcReferenceID, u1Status);
                    }
                    u2TempPortNo = pLcmPortEntryInEvc->u2PortNo;
                    pLcmPortEntryInEvc = NULL;
                }
            }
        }
        return;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : LcmElmiOperStatusIndication                          */
/*                                                                           */
/* Description        : This routine receives indication of  any change in   */
/*                      the Operational Status of ELMI                       */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u1ElmOperStatus - Operational Status of ELMI         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LcmElmiOperStatusIndication (UINT2 u2PortNo, UINT1 u1ElmOperStatus)
{
    UNUSED_PARAM (u2PortNo);

    UNUSED_PARAM (u1ElmOperStatus);
    return;
}

/*****************************************************************************/
/* Function Name      : LcmPassCeVlanInfo                                    */
/*                                                                           */
/* Description        : This routine is used to pass the CVLan information   */
/*                      received from the ELMI UNI-C                         */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      pElmCeVlanInfom - CE Vlan Info structure             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LcmPassCeVlanInfo (UINT2 u2PortNo, tLcmEvcCeVlanInfo * pElmCeVlanInfo)
{
    tLcmEvcEntryInPort *pLcmEvcEntryInPort = NULL;
    tLcmPortEntryInEvc *pLcmPortEntryInEvc = NULL;

    pLcmEvcEntryInPort =
        LcmGetEvcEntryInPortInfo (u2PortNo, pElmCeVlanInfo->u2EvcReferenceId);

    if (pLcmEvcEntryInPort == NULL)
    {
        return;
    }

    pLcmPortEntryInEvc =
        LcmGetPortEntryInEvcInfo (pLcmEvcEntryInPort->pLcmLocalEvcInfo,
                                  u2PortNo);

    if (pLcmPortEntryInEvc == NULL)
    {
        return;
    }

    pLcmPortEntryInEvc->u2FirstVlan = pElmCeVlanInfo->u2FirstVlanId;
    pLcmPortEntryInEvc->u2LastVlan = pElmCeVlanInfo->u2LastVlanId;
    pLcmPortEntryInEvc->u2NoOfVlansMapped = pElmCeVlanInfo->u2NoOfVlansMapped;

    MEMCPY (pLcmPortEntryInEvc->CeVlanMapInfo, pElmCeVlanInfo->CeVlanMapInfo,
            sizeof (tIssVlanList));

    return;
}

/*****************************************************************************/
/* Function Name      : LcmGetEvcInfo                                        */
/*                                                                           */
/* Description        : This routine is used to get information about single */
/*                      EVC                                                  */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcReferenceId - EVC refernce ID                   */
/*                      pLcmLocalEvcInfo - EVC information structure              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS/LCM_FAILURE                              */
/*****************************************************************************/
INT4
LcmGetEvcInfo (UINT2 u2PortNo, UINT2 u2EvcReferenceId,
               tLcmEvcInfo * pLcmLocalEvcInfo)
{
    tLcmEvcEntryInPort *pLcmEvcEntryInPort = NULL;

    pLcmEvcEntryInPort = LcmGetEvcEntryInPortInfo (u2PortNo, u2EvcReferenceId);

    if (pLcmEvcEntryInPort == NULL)
    {
        return LCM_FAILURE;
    }

    pLcmLocalEvcInfo->LcmEvcStatusInfo.u1EvcStatus =
        pLcmEvcEntryInPort->pLcmLocalEvcInfo->u1EvcStatus;
    return LCM_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LcmGetEvcInfo                                        */
/*                                                                           */
/* Description        : This routine is used to get information about single */
/*                      EVC, next to the passed EVC                          */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcReferenceId - EVC refernce ID                   */
/*                      pLcmLocalEvcInfo - EVC information structure              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS/LCM_FAILURE                              */
/*****************************************************************************/
INT4
LcmGetNextEvcInfo (UINT2 u2PortNo, UINT2 u2EvcReferenceId,
                   tLcmEvcInfo * pLcmLocalEvcInfo)
{
    tLcmEvcEntryInPort *pLcmEvcEntryInPort = NULL;

    if (u2EvcReferenceId == 0)
    {
        pLcmEvcEntryInPort = LcmGetFirstEvcEntryInPortInfo (u2PortNo);
        if (pLcmEvcEntryInPort == NULL)
        {
            return LCM_SUCCESS;
        }
        pLcmLocalEvcInfo->LcmEvcStatusInfo.u1EvcStatus =
            pLcmEvcEntryInPort->pLcmLocalEvcInfo->u1EvcStatus;
        pLcmLocalEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId =
            pLcmEvcEntryInPort->u2EvcRefId;
        MEMCPY (pLcmLocalEvcInfo->LcmEvcStatusInfo.au1EvcIdentifier,
                pLcmEvcEntryInPort->pLcmLocalEvcInfo->EvcIdentifier,
                LCM_MAX_EVC_IDENTIFIER_LENGTH);
        return LCM_SUCCESS;
    }
    else
    {
        pLcmEvcEntryInPort =
            LcmGetEvcEntryInPortInfo (u2PortNo, u2EvcReferenceId);
        if (pLcmEvcEntryInPort == NULL)
        {
            pLcmEvcEntryInPort = LcmGetFirstEvcEntryInPortInfo (u2PortNo);
            if (pLcmEvcEntryInPort != NULL)
            {
                if (pLcmEvcEntryInPort->u2EvcRefId > u2EvcReferenceId)
                {
                    pLcmLocalEvcInfo->LcmEvcStatusInfo.u1EvcStatus =
                        pLcmEvcEntryInPort->pLcmLocalEvcInfo->u1EvcStatus;
                    pLcmLocalEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId =
                        pLcmEvcEntryInPort->u2EvcRefId;
                    MEMCPY (pLcmLocalEvcInfo->LcmEvcStatusInfo.au1EvcIdentifier,
                            pLcmEvcEntryInPort->pLcmLocalEvcInfo->EvcIdentifier,
                            LCM_MAX_EVC_IDENTIFIER_LENGTH);
                    return LCM_SUCCESS;
                }

                while ((pLcmEvcEntryInPort =
                        LcmGetNextEvcEntryInPortInfo (pLcmEvcEntryInPort,
                                                      u2PortNo)) != NULL)
                {
                    if (pLcmEvcEntryInPort->u2EvcRefId > u2EvcReferenceId)
                    {
                        pLcmLocalEvcInfo->LcmEvcStatusInfo.u1EvcStatus =
                            pLcmEvcEntryInPort->pLcmLocalEvcInfo->u1EvcStatus;
                        pLcmLocalEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId =
                            pLcmEvcEntryInPort->u2EvcRefId;
                        MEMCPY (pLcmLocalEvcInfo->LcmEvcStatusInfo.
                                au1EvcIdentifier,
                                pLcmEvcEntryInPort->pLcmLocalEvcInfo->
                                EvcIdentifier, LCM_MAX_EVC_IDENTIFIER_LENGTH);
                        return LCM_SUCCESS;
                    }
                }
                pLcmLocalEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId =
                    LCM_INIT_VAL;
                return LCM_SUCCESS;
            }
            pLcmLocalEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId = LCM_INIT_VAL;
            return LCM_SUCCESS;
        }
    }

    pLcmEvcEntryInPort =
        LcmGetNextEvcEntryInPortInfo (pLcmEvcEntryInPort, u2PortNo);
    if (pLcmEvcEntryInPort == NULL)
    {
        pLcmLocalEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId = LCM_INIT_VAL;
        return LCM_SUCCESS;
    }

    pLcmLocalEvcInfo->LcmEvcStatusInfo.u1EvcStatus =
        pLcmEvcEntryInPort->pLcmLocalEvcInfo->u1EvcStatus;
    pLcmLocalEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId =
        pLcmEvcEntryInPort->u2EvcRefId;
    MEMCPY (pLcmLocalEvcInfo->LcmEvcStatusInfo.au1EvcIdentifier,
            pLcmEvcEntryInPort->pLcmLocalEvcInfo->EvcIdentifier,
            LCM_MAX_EVC_IDENTIFIER_LENGTH);
    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LcmEvcStatusChangeIndication                         */
/*                                                                           */
/* Description        : This routine is called whenever status of an EVC     */
/*                      changes                                              */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcReferenceId - EVC refernce ID                   */
/*                      u1EvcStatus - Changed Status of the EVC              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LcmEvcStatusChangeIndication (UINT2 u2PortNo, UINT2 u2EvcReferenceId,
                              UINT1 u1EvcStatus)
{
    tLcmEvcEntryInPort *pLcmEvcEntryInPort = NULL;

    pLcmEvcEntryInPort = LcmGetEvcEntryInPortInfo (u2PortNo, u2EvcReferenceId);
    if (pLcmEvcEntryInPort == NULL)
    {
        return;
    }

    LcmEvcStatusChange (pLcmEvcEntryInPort->pLcmLocalEvcInfo->EvcIdentifier,
                        u1EvcStatus);

    return;
}

/*****************************************************************************/
/* Function Name      : LcmCreateNewEvcIndication                            */
/*                                                                           */
/* Description        : This routine is called whenever an EVC is created    */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      pLcmEvcStatusInfo - EVC status information structure */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LcmCreateNewEvcIndication (UINT2 u2PortNo,
                           tLcmEvcStatusInfo * pLcmEvcStatusInfo)
{

    if (LcmFindEvcId ((UINT1 *) (pLcmEvcStatusInfo->au1EvcIdentifier)) ==
        LCM_SUCCESS)
    {
        return;
    }

    if (LcmCreateEvc ((UINT1 *) (pLcmEvcStatusInfo->au1EvcIdentifier)) ==
        LCM_FAILURE)
    {
        return;
    }

    LcmUpdateEvcEtherServiceInstance (u2PortNo,
                                      pLcmEvcStatusInfo->u2EvcReferenceId,
                                      (UINT1 *) (pLcmEvcStatusInfo->
                                                 au1EvcIdentifier),
                                      pLcmEvcStatusInfo->u2EvcReferenceId);

    return;
}

/*****************************************************************************/
/* Function Name      : LcmDeleteEvcIndication                               */
/*                                                                           */
/* Description        : This routine is called when an EVC is to be deleted  */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcReferenceId - Ref Id of the EVC to be deleted   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LcmDeleteEvcIndication (UINT2 u2PortNo, UINT2 u2EvcReferenceId)
{
    tLcmEvcEntryInPort *pLcmEvcEntryInPort = NULL;

    pLcmEvcEntryInPort = LcmGetEvcEntryInPortInfo (u2PortNo, u2EvcReferenceId);
    if (pLcmEvcEntryInPort == NULL)
    {
        return;
    }

    LcmDeleteEvc (pLcmEvcEntryInPort->pLcmLocalEvcInfo->EvcIdentifier);

    return;

}

/*****************************************************************************/
/* Function Name      : LcmDatabaseUpdateIndication                          */
/*                                                                           */
/* Description        : This routine is used to indicate any change in the   */
/*                      LCM database                                         */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      pLcmEvcStatusInfo - EVC status information structure */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LcmDatabaseUpdateIndication (UINT2 u2PortNo,
                             tLcmEvcStatusInfo * pLcmEvcStatusInfo)
{
    tLcmEvcEntryInPort *pLcmEvcEntryInPort = NULL;

    pLcmEvcEntryInPort =
        LcmGetEvcEntryInPortInfo (u2PortNo,
                                  pLcmEvcStatusInfo->u2EvcReferenceId);

    if (pLcmEvcEntryInPort == NULL)
    {
        return;
    }

    MEMCPY (pLcmEvcEntryInPort->pLcmLocalEvcInfo->EvcBwProfile,
            pLcmEvcStatusInfo->BwProfile,
            NUM_BW_ELEMENTS * sizeof (tBwProfile));

    pLcmEvcEntryInPort->u4EtherServInstance =
        pLcmEvcStatusInfo->u4EtherServInstance;

    pLcmEvcEntryInPort->u1EvcStatus = pLcmEvcStatusInfo->u1EvcStatus;

    pLcmEvcEntryInPort->pLcmLocalEvcInfo->u1EvcType =
        pLcmEvcStatusInfo->u1EvcType;

    MEMCPY (pLcmEvcEntryInPort->pLcmLocalEvcInfo->EvcIdentifier,
            pLcmEvcStatusInfo->au1EvcIdentifier, LCM_MAX_EVC_IDENTIFIER_LENGTH);

    pLcmEvcEntryInPort->pLcmLocalEvcInfo->u1NoOfCos =
        pLcmEvcStatusInfo->u1NoOfCos;

    return;
}

/*****************************************************************************/
/* Function Name      : LcmLock                                             */
/*                                                                           */
/* Description        : This function is used to take the LCM mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS or LCM_FAILURE                           */
/*                                                                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/*                                                                           */
/* Calling Function   :  SNMP,                                               */
/*****************************************************************************/
INT4
LcmLock (VOID)
{
    if (LCM_TAKE_SEM (gLcmGlobalInfo.SemId) != LCM_OSIX_SUCCESS)
    {
        return LCM_FAILURE;
    }
    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LcmUnLock                                           */
/*                                                                           */
/* Description        : This function is used to give the LCM mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS or LCM_FAILURE                           */
/*                                                                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/*                                                                           */
/* Calling Function   :  SNMP                                                */
/*****************************************************************************/
INT4
LcmUnLock (VOID)
{
    LCM_GIVE_SEM (gLcmGlobalInfo.SemId);
    return LCM_SUCCESS;
}
