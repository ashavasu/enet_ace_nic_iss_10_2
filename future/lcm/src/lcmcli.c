/* SOURCE FILE HEADER :
*
*  $Id: lcmcli.c,v 1.6 2015/02/09 12:59:27 siva Exp $
*  ---------------------------------------------------------------------------
* |  FILE NAME             : lcmcli.c                                       |
* |                                                                           |
* |  PRINCIPAL AUTHOR      : Aricent Inc.                             |
* |                                                                           |
* |  SUBSYSTEM NAME        : CLI                                              |
* |                                                                           |
* |  MODULE NAME           : LCM                                     |
* |                                                                           |
* |  LANGUAGE              : C                                                |
* |                                                                           |
* |  TARGET ENVIRONMENT    :                                                  |
* |                                                                           |
* |  DATE OF FIRST RELEASE :                                                  |
* |                                                                           |
* |  DESCRIPTION           : Action routines for CLI LCM Commands       |
* |                                                                           |
*  ---------------------------------------------------------------------------
* |   1    | 25th Jun 2007   |    Original                                    |
* |        |                 |                                                |
*  ---------------------------------------------------------------------------
*   
*/

#include "lcminc.h"
#include "lcmglobcli.h"

INT4
cli_process_lcm_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{

    /*Third Argument is always passed as Interface Index */

    va_list             ap;
    UINT1              *args[LCM_CLI_MAX_ARGS];
    INT1                argno = (UINT1) LCM_INIT_VAL;
    UINT4               u4Index = LCM_INIT_VAL;
    INT4                i4RetVal = CLI_FAILURE;
    UINT4               u4CfgCmdFound = CLI_SUCCESS;
    UINT4               u4EfpIdentifier = LCM_INIT_VAL;
    UINT1              *pu1EvcName = NULL;
    UINT1              *pu1DomainName = NULL;
    UINT4               u4SerVlanId = LCM_INIT_VAL;
    UINT1               uProtocoltype = (UINT1) LCM_INIT_VAL;
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT2               u2Len = 0;

    MEMSET (au1Cmd, LCM_INIT_VAL, MAX_PROMPT_LEN);

    va_start (ap, u4Command);

    /* Third argument is always Interface Name Index */

    u4Index = va_arg (ap, UINT4);

    u4Index = CLI_GET_IFINDEX ();

    /* Walk through the rest of the arguments and store in args array. 
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == LCM_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    /* CliRegisterLock (CliHandle, ElmLock, ElmUnLock);
     */
    /* ELM_LOCK (); */
    if (u4Command == CLI_LCM_SET_OAM_CFM)
    {
        u4SerVlanId = *(UINT4 *) (VOID *) (args[0]);
        pu1DomainName = args[1];
        uProtocoltype = OAM_CFA_PROTOCOL_TYPE;
        pu1EvcName = (UINT1 *) &(LcmPromptInfo.EvcName);
        /*Evc id is taken from the mode */
        i4RetVal =
            CliHandleProtocolMap (pu1EvcName, uProtocoltype, u4SerVlanId,
                                  pu1DomainName);
    }
    else if (u4Command == CLI_LCM_SET_ETH_SER_INSTANCE)
    {
        u4EfpIdentifier = *(UINT4 *) (VOID *) (args[0]);
        pu1EvcName = (UINT1 *) args[1];

        /* Validate the range of the Efp Identifier value */
        if ((u4EfpIdentifier < LCM_MIN_EFP_IDENTIFIER_VALUE) ||
            (u4EfpIdentifier > LCM_MAX_EFP_IDENTIFIER_VALUE))
        {
            CliPrintf (CliHandle, "\r%% Value out of "
                       "range (1-4294967295) \r\n");
            return CLI_FAILURE;
        }

        i4RetVal =
            CliHandleServiceInstanceCreation (u4Index, u4EfpIdentifier,
                                              pu1EvcName);
        if (i4RetVal == CLI_SUCCESS)
        {
            SPRINTF ((CHR1 *) au1Cmd, "%s", CLI_ESI_MODE);
            if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to enter into Service Instance configuration mode\r\n");
                return CLI_FAILURE;
            }
            LcmPromptInfo.u4ServiceInstanceNo = u4EfpIdentifier;
            u2Len =
                (UINT2) (STRLEN(pu1EvcName) <
                        (sizeof(LcmPromptInfo.EvcName)-1) ?
                        STRLEN(pu1EvcName) : (sizeof(LcmPromptInfo.EvcName)-1));

            STRNCPY (&(LcmPromptInfo.EvcName), pu1EvcName,
                    u2Len);
            LcmPromptInfo.EvcName[u2Len] = '\0';
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r%% Error, Cannot enter Service Instance configuration mode \r\n");
            return CLI_FAILURE;
        }
    }

    else if (u4Command == CLI_LCM_SET_NO_ETH_SER_INSTANCE)
    {
        u4EfpIdentifier = *(UINT4 *) (VOID *) (args[0]);
        pu1EvcName = (UINT1 *) args[1];
        i4RetVal =
            CliHandleServiceInstanceDeletion (u4Index, u4EfpIdentifier,
                                              (UINT1 *) &(LcmPromptInfo.
                                                          EvcName));
        MEMSET (&(LcmPromptInfo.EvcName), 0x00, LCM_MAX_EVC_IDENTIFIER_LENGTH);
        LcmPromptInfo.u4ServiceInstanceNo = 0;
    }

    else if (u4Command == CLI_LCM_SHOW_ETHER_SERVICE_EVC)
    {
        i4RetVal = CliHandleShowEvcInfo (CliHandle);
    }
    else if (u4Command == CLI_LCM_SHOW_ETH_SER_INSTANCE)
    {
        CLI_SET_CMD_STATUS (i4RetVal);

        CliUnRegisterLock (CliHandle);

        return i4RetVal;

    }
    else
    {
        if (u4CfgCmdFound == CLI_FAILURE)
        {
            /* Given command does not match with any of the SET or SHOW 
               commands */
            /*CliPrintf (CliHandle, "\r%% Invalid Command !\r\n "); */
            i4RetVal = CLI_FAILURE;
        }
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetVal);

    CliUnRegisterLock (CliHandle);

    return i4RetVal;
}

/*******************************************************************************
 *
 *     FUNCTION NAME    : LcmGetEsiConfigPrompt
 *
 *     DESCRIPTION      : This function Checks for domain validity and
 *                        returns the prompt to be displayed.
 *
 *     INPUT            : pi1ModeName - Mode to be configured.
 *
 *     OUTPUT           : pi1DispStr  - Prompt to be displayed.
 *
 *     RETURNS          : EVCPRO_TRUE or EVCPRO_FALSE
 *
*******************************************************************************/

INT1
LcmGetEsiConfigPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (CLI_ESI_MODE);

    if (!pi1DispStr || !pi1ModeName)
    {
        return LCM_FALSE;
    }

    if (STRNCMP (pi1ModeName, CLI_ESI_MODE, u4Len) != 0)
    {
        return LCM_FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;
    /*u4Index = CLI_ATOI (pi1ModeName);
     *
     * No need to take lock here, since it is taken by
     * Cli in cli_process_ecfm_cmd.
     */

    STRCPY (pi1DispStr, "(config-if-esi)#");

    return LCM_TRUE;
}
