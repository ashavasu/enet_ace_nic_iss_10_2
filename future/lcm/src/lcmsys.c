#include "lcminc.h"

/* $Id: lcmsys.c,v 1.9 2015/02/09 12:59:27 siva Exp $*/
/*******For LCM Start **********/

/*****************************************************************************/
/* Function Name      : LcmGetNoOfEvc                                        */
/*                                                                           */
/* Description        : This routime fetches the No of EVCs provisioned      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : INT2                                                 */
/*****************************************************************************/
INT2
LcmGetNoOfEvc (VOID)
{
    tLcmLocalEvcInfo   *pLcmLocalEvcInfo = NULL;
    UINT2               u2NoOfEvc = 0;

    pLcmLocalEvcInfo = LcmGetFirstEvcEntryInContextInfo (0);

    if (pLcmLocalEvcInfo == NULL)
    {
        return u2NoOfEvc;
    }

    while (pLcmLocalEvcInfo != NULL)
    {
        u2NoOfEvc++;
        pLcmLocalEvcInfo =
            LcmGetNextEvcEntryInContextInfo (pLcmLocalEvcInfo, 0);
    }

    return u2NoOfEvc;
}

/* Local Routines */

/*****************************************************************************/
/* Function Name      : LcmGenerateEvcRefId                                  */
/*                                                                           */
/* Description        : This routine generates Reference ID for an EVC       */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      pu2RefId - generated reference Id                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS/LCM_FAILURE                              */
/*****************************************************************************/
INT2
LcmGenerateEvcRefId (UINT2 u2PortNo, UINT2 *pu2RefId)
{
    tLcmPortEntry      *pLcmPortEntry = NULL;
    UINT2               u2TempRefId = 0;

    /* Return a unique value which is in range 1-1024 from EvcRefId Table */
    pLcmPortEntry = LCM_GET_PORTENTRY (u2PortNo);
    if (pLcmPortEntry == NULL)
    {
        return LCM_FAILURE;
    }

    for (u2TempRefId = 1; u2TempRefId <= EVCPRO_MAX_EVC_PER_PORT; u2TempRefId++)
    {
        if (pLcmPortEntry->u1RefIdTable[u2TempRefId] == 0)
        {
            pLcmPortEntry->u1RefIdTable[u2TempRefId] = 1;
            *pu2RefId = u2TempRefId;
            return LCM_SUCCESS;
        }
    }

    return LCM_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LcmDeleteEvcRefId                                    */
/*                                                                           */
/* Description        : This routine deletes Reference ID for an EVC         */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      pu2RefId - generated reference Id                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS/LCM_FAILURE                              */
/*****************************************************************************/
VOID
LcmDeleteEvcRefId (UINT2 u2PortNo, UINT2 u2RefId)
{
    tLcmPortEntry      *pLcmPortEntry = NULL;

    /* Update EVCRefId Table */
    pLcmPortEntry = LCM_GET_PORTENTRY (u2PortNo);
    if (pLcmPortEntry == NULL)
    {
        return;
    }
    if (u2RefId >= EVCPRO_MAX_EVC_PER_PORT)
    {
        return;
    }

    pLcmPortEntry->u1RefIdTable[u2RefId] = 0;
    return;
}

/*****************************************************************************/
/* Function Name      : LcmInitialize                                        */
/*                                                                           */
/* Description        : This routine initializes the LCM module              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS/LCM_FAILURE                              */
/*****************************************************************************/
INT4
LcmInitialize (INT1 *pi1Dummy)
{
    UINT2               u2PortNo = LCM_INIT_VAL;

    UNUSED_PARAM (pi1Dummy);

    MEMSET (gLcmGlobalInfo.aContextInfo, LCM_INIT_VAL,
            sizeof (tLcmContextInfo));

    if (LCM_CREATE_SEM (LCM_MUT_EXCL_SEM_NAME, LCM_SEM_INIT_COUNT, LCM_INIT_VAL,
                        &gLcmGlobalInfo.SemId) != LCM_OSIX_SUCCESS)
    {
        return LCM_FAILURE;
    }

    /* Memory Pool for EvcNodeInPortEntry */
    if (LCM_CREATE_MEM_POOL (sizeof (tLcmEvcEntryInPort),
                             LCM_MAX_NUM_PORTS *
                             EVCPRO_MAX_EVC_PER_PORT,
                             &(gLcmGlobalInfo.EvcEntryInPortNodeMemPoolId))
        == LCM_MEM_FAILURE)
    {
        return LCM_FAILURE;
    }

    /* Memory Pool for EvcInfoEntries */
    if (LCM_CREATE_MEM_POOL (sizeof (tLcmLocalEvcInfo),
                             EVCPRO_MAX_NUM_EVC_SUPPORTED,
                             &(gLcmGlobalInfo.LcmEvcInfoNodeMemPoolId))
        == LCM_MEM_FAILURE)
    {
        return LCM_FAILURE;
    }

    /* Memory Pool for Lcm Port Entries */
    if (LCM_CREATE_MEM_POOL (sizeof (tLcmPortEntryInEvc),
                             LCM_MAX_NUM_PORTS *
                             EVCPRO_MAX_EVC_PER_PORT,
                             &(gLcmGlobalInfo.PortEntryInEvcNodeMemPoolId))
        == LCM_MEM_FAILURE)
    {
        return LCM_FAILURE;
    }

    gLcmGlobalInfo.aContextInfo[0].LcmEvcEntries =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tLcmLocalEvcInfo,
                                             LcmEvcEntryInContextNode),
                              (tRBCompareFn) LcmEvcEntryInContextInfoTblCmpFn);

    for (u2PortNo = 1; u2PortNo < LCM_MAX_NUM_PORTS; u2PortNo++)
    {
        (LCM_GET_PORTENTRY (u2PortNo))->LcmPortEvcEntries =
            RBTreeCreateEmbedded (FSAP_OFFSETOF (tLcmEvcEntryInPort,
                                                 LcmEvcEntryInPortNode),
                                  (tRBCompareFn) LcmEvcEntryInPortInfoTblCmpFn);

        if ((LCM_GET_PORTENTRY (u2PortNo))->LcmPortEvcEntries == NULL)
        {
            return LCM_FAILURE;
        }
    }

    lrInitComplete (OSIX_SUCCESS);
    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LcmGetEvcEntryInPortInfo                             */
/*                                                                           */
/* Description        : This routine gets the EVC entry per port             */
/*                                                                           */
/* Input(s)           : u2PortNo - Port no                                   */
/*                      u2EvcRefId - EVC refernce ID                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : tLcmEvcEntryInPort - EVC Entry per port info         */
/*****************************************************************************/
tLcmEvcEntryInPort *
LcmGetEvcEntryInPortInfo (UINT2 u2PortNo, UINT2 u2EvcRefId)
{
    tLcmEvcEntryInPort  DummyEntry;
    tLcmPortEntry      *pPortEntry = NULL;

    pPortEntry = LCM_GET_PORTENTRY (u2PortNo);

    if (pPortEntry != NULL)
    {
        DummyEntry.u2EvcRefId = u2EvcRefId;

        return (tLcmEvcEntryInPort *)
            RBTreeGet (pPortEntry->LcmPortEvcEntries, (tRBElem *) & DummyEntry);
    }
    return NULL;
}

/*****************************************************************************/
/* Function Name      : LcmGetEvcEntryInPortInfo                             */
/*                                                                           */
/* Description        : This routine gets the first EVC entry in the         */
/*                      port info                                            */
/*                                                                           */
/* Input(s)           : u2PortNo - Port no                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : tLcmEvcEntryInPort - EVC Entry per port info         */
/*****************************************************************************/
tLcmEvcEntryInPort *
LcmGetFirstEvcEntryInPortInfo (UINT2 u2PortNo)
{
    tLcmPortEntry      *pPortEntry = NULL;
    pPortEntry = LCM_GET_PORTENTRY (u2PortNo);

    if (pPortEntry != NULL)
    {
        return (tLcmEvcEntryInPort *) RBTreeGetFirst (pPortEntry->
                                                      LcmPortEvcEntries);
    }
    return NULL;
}

/*****************************************************************************/
/* Function Name      : LcmGetNextEvcEntryInPortInfo                         */
/*                                                                           */
/* Description        : This routine gets the next EVC entry in the          */
/*                      port info                                            */
/*                                                                           */
/* Input(s)           : pLcmEvcEntryInPort - EVC Entry per port info         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : tLcmEvcEntryInPort - EVC Entry per port info         */
/*****************************************************************************/
tLcmEvcEntryInPort *
LcmGetNextEvcEntryInPortInfo (tLcmEvcEntryInPort * pLcmEvcEntryInPort,
                              UINT2 u2PortNo)
{
    tLcmPortEntry      *pPortEntry = NULL;
    pPortEntry = LCM_GET_PORTENTRY (u2PortNo);

    if (pPortEntry != NULL)
    {

        return (tLcmEvcEntryInPort *) RBTreeGetNext
            (pPortEntry->LcmPortEvcEntries, (tRBElem *) pLcmEvcEntryInPort,
             NULL);
    }
    return NULL;
}

/*****************************************************************************/
/* Function Name      : LcmGetNextEvcEntryInPortInfoBasedOnEvcRefId          */
/*                                                                           */
/* Description        : This routine gets the next EVC entry in port info    */
/*                      based on EVC reference ID                            */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcRefId - EVC reference ID                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LcmEvcEntryInPort                                    */
/*****************************************************************************/
tLcmEvcEntryInPort *
LcmGetNextEvcEntryInPortInfoBasedOnEvcRefId (UINT2 u2PortNo, UINT2 u2EvcRefId)
{
    tLcmEvcEntryInPort  Dummy;

    tLcmPortEntry      *pPortEntry = NULL;
    pPortEntry = LCM_GET_PORTENTRY (u2PortNo);

    if (pPortEntry != NULL)
    {
        LCM_MEMSET (&Dummy, LCM_INIT_VAL, sizeof (Dummy));
        Dummy.u2EvcRefId = u2EvcRefId;
        return (tLcmEvcEntryInPort *) RBTreeGetNext
            (pPortEntry->LcmPortEvcEntries, (tRBElem *) & Dummy, NULL);
    }
    return NULL;
}

/*****************************************************************************/
/* Function Name      : LcmEvcEntryInPortInfoTblCmpFn                        */
/*                                                                           */
/* Description        : This routine compares the EVC reference IDs          */
/*                                                                           */
/* Input(s)           : pKey1 - First element for comparison                 */
/*                      pKey2 - Second element for comparison                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1:- EVC refId for pKey1 is greater than that of pKey2*/
/*                     -1:- EVC refId for pKey2 is greater than that of pKey1*/
/*                      0:- When both the refernce ids are same              */
/*****************************************************************************/
INT4
LcmEvcEntryInPortInfoTblCmpFn (tRBElem * pKey1, tRBElem * pKey2)
{
    tLcmEvcEntryInPort *pLcmEvcEntryInPort1 = (tLcmEvcEntryInPort *) pKey1;
    tLcmEvcEntryInPort *pLcmEvcEntryInPort2 = (tLcmEvcEntryInPort *) pKey2;

    if (pLcmEvcEntryInPort1->u2EvcRefId > pLcmEvcEntryInPort2->u2EvcRefId)
    {
        return 1;
    }
    else if (pLcmEvcEntryInPort1->u2EvcRefId < pLcmEvcEntryInPort2->u2EvcRefId)
    {
        return -1;
    }

    return 0;
}

/*****************************************************************************/
/* Function Name      : LcmFreeEvcEntryInPortInfo                            */
/*                                                                           */
/* Description        : This routine frees the EVC entry in port             */
/*                                                                           */
/* Input(s)           : pElem - Element in RbTree                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS/LCM_FAILURE                              */
/*****************************************************************************/
INT4
LcmFreeEvcEntryInPortInfo (tRBElem * pElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    LCM_RELEASE_EVCNODE_IN_PORT_INFO_MEM_BLOCK ((tLcmEvcEntryInPort *) pElem);

    return 1;
}

/*****************************************************************************/
/* Function Name      : LcmAddToEvcEntryInPortInfoTable                      */
/*                                                                           */
/* Description        : This routine adds an EVC entry in Port Info table    */
/*                                                                           */
/* Input(s)           : pLcmEvcEntryInPort - EVC entry in Port info          */
/*                      u2PortNo - Port No                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS/LCM_FAILURE                              */
/*****************************************************************************/
INT4
LcmAddToEvcEntryInPortInfoTable (tLcmEvcEntryInPort * pLcmEvcEntryInPort,
                                 UINT2 u2PortNo)
{
    tLcmPortEntry      *pPortEntry = NULL;
    pPortEntry = LCM_GET_PORTENTRY (u2PortNo);
    if (pPortEntry != NULL)
    {
        if (RBTreeAdd (pPortEntry->LcmPortEvcEntries,
                       (tRBElem *) pLcmEvcEntryInPort) == RB_FAILURE)
        {
            return LCM_FAILURE;
        }
        return LCM_SUCCESS;
    }
    return LCM_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LcmRemoveFromEvcEntryInPortInfoTable                 */
/*                                                                           */
/* Description        : This routine deletes an EVC entry in Port Info table */
/*                                                                           */
/* Input(s)           : pLcmEvcEntryInPort - EVC entry in Port info          */
/*                      u2PortNo - Port No                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS/LCM_FAILURE                              */
/*****************************************************************************/
INT4
LcmRemoveFromEvcEntryInPortInfoTable (tLcmEvcEntryInPort * pLcmEvcEntryInPort,
                                      UINT2 u2PortNo)
{
    tLcmPortEntry      *pPortEntry = NULL;
    pPortEntry = LCM_GET_PORTENTRY (u2PortNo);
    if (pPortEntry != NULL)
    {
        if (RBTreeRemove (pPortEntry->LcmPortEvcEntries,
                          (tRBElem *) pLcmEvcEntryInPort) == RB_FAILURE)
        {
            return LCM_FAILURE;
        }
        return LCM_SUCCESS;
    }
    return LCM_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LcmGetPortEntryInEvcInfo                             */
/*                                                                           */
/* Description        : This routine gets an EVC entry in Port Info          */
/*                                                                           */
/* Input(s)           : pLcmLocalEvcInfo - Local index of an EVC             */
/*                      u2PortNo - Port No                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : tLcmPortEntryInEvc                                   */
/*****************************************************************************/
tLcmPortEntryInEvc *
LcmGetPortEntryInEvcInfo (tLcmLocalEvcInfo * pLcmLocalEvcInfo, UINT2 u2PortNo)
{
    tLcmPortEntryInEvc *pDummyEntry = NULL;
    tLcmPortEntryInEvc *pLcmPortEntryInEvc = NULL;

    /* Allocate EvcPortNode from Mempool */
    if (LCM_ALLOC_PORTNODE_IN_EVC_INFO_MEM_BLOCK (pDummyEntry) == NULL)
    {
        return LCM_FAILURE;
    }

    pDummyEntry->u2PortNo = u2PortNo;

    if (pLcmLocalEvcInfo == NULL)
    {
        LCM_RELEASE_PORTNODE_IN_EVC_INFO_MEM_BLOCK (pDummyEntry);
        return NULL;
    }
    pLcmPortEntryInEvc = (tLcmPortEntryInEvc *)
        RBTreeGet (pLcmLocalEvcInfo->LcmPortRBTree, (tRBElem *) pDummyEntry);

    LCM_RELEASE_PORTNODE_IN_EVC_INFO_MEM_BLOCK (pDummyEntry);

    return (pLcmPortEntryInEvc);
}

/*****************************************************************************/
/* Function Name      : LcmGetFirstPortEntryInEvcInfo                        */
/*                                                                           */
/* Description        : This routine gets the first port entry in EVC info   */
/*                                                                           */
/* Input(s)           : pLcmLocalEvcInfo - Local index of an EVC             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : tLcmPortEntryInEvc                                   */
/*****************************************************************************/
tLcmPortEntryInEvc *
LcmGetFirstPortEntryInEvcInfo (tLcmLocalEvcInfo * pLcmLocalEvcInfo)
{
    if (pLcmLocalEvcInfo == NULL)
    {
        return NULL;
    }

    return (tLcmPortEntryInEvc *) RBTreeGetFirst
        (pLcmLocalEvcInfo->LcmPortRBTree);
}

/*****************************************************************************/
/* Function Name      : LcmGetNextPortEntryInEvcInfo                         */
/*                                                                           */
/* Description        : This routine gets the next port entry in EVC info    */
/*                                                                           */
/* Input(s)           : pLcmPortEntryInEvc  - LCM Port entry in EVC          */
/*                      pLcmLocalEvcInfo - Local index of an EVC             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : tLcmPortEntryInEvc                                   */
/*****************************************************************************/
tLcmPortEntryInEvc *
LcmGetNextPortEntryInEvcInfo (tLcmPortEntryInEvc * pLcmPortEntryInEvc,
                              tLcmLocalEvcInfo * pLcmLocalEvcInfo)
{
    if (pLcmLocalEvcInfo == NULL)
    {
        return NULL;
    }
    return (tLcmPortEntryInEvc *) RBTreeGetNext
        (pLcmLocalEvcInfo->LcmPortRBTree, (tRBElem *) pLcmPortEntryInEvc, NULL);
}

/*****************************************************************************/
/* Function Name      : LcmGetNextPortEntryInEvcInfoBasedOnPortNo            */
/*                                                                           */
/* Description        : This routine gets the next port entry in EVC info    */
/*                      based on port number                                 */
/*                                                                           */
/* Input(s)           : pLcmLocalEvcInfo - Local index of an EVC             */
/*                      u2PortNo - Port number                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : tLcmPortEntryInEvc                                   */
/*****************************************************************************/
tLcmPortEntryInEvc *
LcmGetNextPortEntryInEvcInfoBasedOnPortNo (tLcmLocalEvcInfo * pLcmLocalEvcInfo,
                                           UINT2 u2PortNo)
{
    tLcmPortEntryInEvc *pDummy = NULL;
    tLcmPortEntryInEvc *pLcmPortEntryInEvc = NULL;

    /* Allocate EvcPortNode from Mempool */
    if (LCM_ALLOC_PORTNODE_IN_EVC_INFO_MEM_BLOCK (pDummy) == NULL)
    {
        return LCM_FAILURE;
    }

    LCM_MEMSET (pDummy, LCM_INIT_VAL, sizeof (tLcmPortEntryInEvc));

    pDummy->u2PortNo = u2PortNo;

    if (pLcmLocalEvcInfo == NULL)
    {
        LCM_RELEASE_PORTNODE_IN_EVC_INFO_MEM_BLOCK (pDummy);
        return NULL;
    }

    pLcmPortEntryInEvc = (tLcmPortEntryInEvc *) RBTreeGetNext
        (pLcmLocalEvcInfo->LcmPortRBTree, (tRBElem *) pDummy, NULL);

    LCM_RELEASE_PORTNODE_IN_EVC_INFO_MEM_BLOCK (pDummy);

    return (pLcmPortEntryInEvc);
}

/*****************************************************************************/
/* Function Name      : LcmPortEntryInEvcInfoTblCmpFn                        */
/*                                                                           */
/* Description        : This routine compares the port numbers               */
/*                                                                           */
/* Input(s)           : pKey1 - First element for comparison                 */
/*                      pKey2 - Second element for comparison                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1:- Port No for pKey1 is greater than that of pKey2  */
/*                     -1:- Port No for pKey2 is greater than that of pKey1  */
/*                      0:- When both the Port Numbers are same              */
/*****************************************************************************/
INT4
LcmPortEntryInEvcInfoTblCmpFn (tRBElem * pKey1, tRBElem * pKey2)
{
    tLcmPortEntryInEvc *pLcmPortEntryInEvc1 = (tLcmPortEntryInEvc *) pKey1;
    tLcmPortEntryInEvc *pLcmPortEntryInEvc2 = (tLcmPortEntryInEvc *) pKey2;

    if (pLcmPortEntryInEvc1->u2PortNo > pLcmPortEntryInEvc2->u2PortNo)
    {
        return 1;
    }
    else if (pLcmPortEntryInEvc1->u2PortNo < pLcmPortEntryInEvc2->u2PortNo)
    {
        return -1;
    }

    return 0;
}

/*****************************************************************************/
/* Function Name      : LcmFreePortEntryInEvcInfo                            */
/*                                                                           */
/* Description        : This routine frees the port entry in EVC info        */
/*                                                                           */
/* Input(s)           : pElem - Element to be freed                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : INT4                                                 */
/*****************************************************************************/
INT4
LcmFreePortEntryInEvcInfo (tRBElem * pElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    LCM_RELEASE_PORTNODE_IN_EVC_INFO_MEM_BLOCK ((tLcmPortEntryInEvc *) pElem);

    return 1;
}

/*****************************************************************************/
/* Function Name      : LcmAddToPortEntryInEvcInfoTable                      */
/*                                                                           */
/* Description        : This routine adds a port entry in EVC info           */
/*                                                                           */
/* Input(s)           : pLcmPortEntryInEvc - Port Entry in EVC               */
/*                      pLcmLocalEvcInfo - Local index of an EVC             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS/LCM_FAILURE                              */
/*****************************************************************************/
INT4
LcmAddToPortEntryInEvcInfoTable (tLcmPortEntryInEvc * pLcmPortEntryInEvc,
                                 tLcmLocalEvcInfo * pLcmLocalEvcInfo)
{
    if (pLcmLocalEvcInfo == NULL)
    {
        return LCM_FAILURE;
    }

    if (RBTreeAdd (pLcmLocalEvcInfo->LcmPortRBTree,
                   (tRBElem *) pLcmPortEntryInEvc) == RB_FAILURE)
    {
        return LCM_FAILURE;
    }

    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LcmRemoveFromPortEntryInEvcInfoTable                 */
/*                                                                           */
/* Description        : This routine deletes a port entry in EVC info        */
/*                                                                           */
/* Input(s)           : pLcmPortEntryInEvc - Port Entry in EVC               */
/*                      pLcmLocalEvcInfo - Local index of an EVC             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS/LCM_FAILURE                              */
/*****************************************************************************/
INT4
LcmRemoveFromPortEntryInEvcInfoTable (tLcmPortEntryInEvc * pLcmPortEntryInEvc,
                                      tLcmLocalEvcInfo * pLcmLocalEvcInfo)
{
    INT4                i4RetVal = LCM_SUCCESS;

    if (RBTreeRemove (pLcmLocalEvcInfo->LcmPortRBTree,
                      (tRBElem *) pLcmPortEntryInEvc) == RB_FAILURE)
    {
        i4RetVal = LCM_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : LcmGetEvcEntryInContextInfo                          */
/*                                                                           */
/* Description        : This routine gets the EVC entry from global list of  */
/*                      EVC's                                                */
/*                                                                           */
/* Input(s)           : u2ContextId - Context no                             */
/*                      EvcId - EVC Name(String not exceeding 100 alphabets) */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : tLcmLocalEvcInfo - EVC Entry per port info           */
/*****************************************************************************/
tLcmLocalEvcInfo   *
LcmGetEvcEntryInContextInfo (UINT2 u2ContextId, UINT1 *EvcId)
{
    tLcmLocalEvcInfo    DummyEntry;
    UINT2               u2Len = 0;

    u2Len =
        (UINT2) (STRLEN(EvcId) <
                (sizeof(DummyEntry.EvcIdentifier)-1) ?
                STRLEN(EvcId)  : (sizeof(DummyEntry.EvcIdentifier)-1));

    STRNCPY (DummyEntry.EvcIdentifier, EvcId, u2Len);
    DummyEntry.EvcIdentifier[u2Len] = '\0';

    return (tLcmLocalEvcInfo *)
        RBTreeGet
        (gLcmGlobalInfo.aContextInfo[u2ContextId].LcmEvcEntries,
         (tRBElem *) & DummyEntry);
}

/*****************************************************************************/
/* Function Name      : LcmGetFirstEvcEntryInContextInfo                     */
/*                                                                           */
/* Description        : This routine gets the first EVC entry from the global*/
/*                      EVC entries                                          */
/*                                                                           */
/* Input(s)           : u2ContextId - Context no                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : tLcmLocalEvcInfo - EVC Entry in switch               */
/*****************************************************************************/
tLcmLocalEvcInfo   *
LcmGetFirstEvcEntryInContextInfo (UINT2 u2ContextId)
{
    return (tLcmLocalEvcInfo *) RBTreeGetFirst
        (gLcmGlobalInfo.aContextInfo[u2ContextId].LcmEvcEntries);
}

/*****************************************************************************/
/* Function Name      : LcmGetNextEvcEntryInContextInfo                      */
/*                                                                           */
/* Description        : This routine gets the next EVC entry from the        */
/*                      global EVC entries                                   */
/*                                                                           */
/* Input(s)           : pLcmLocalEvcInfo - EVC Entry per port info         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : tLcmLocalEvcInfo - EVC Entry per port info         */
/*****************************************************************************/
tLcmLocalEvcInfo   *
LcmGetNextEvcEntryInContextInfo (tLcmLocalEvcInfo * pLcmLocalEvcInfo,
                                 UINT2 u2ContextId)
{
    return (tLcmLocalEvcInfo *) RBTreeGetNext
        (gLcmGlobalInfo.aContextInfo[u2ContextId].LcmEvcEntries,
         (tRBElem *) pLcmLocalEvcInfo, NULL);
}

/*****************************************************************************/
/* Function Name      : LcmGetNextEvcEntryInContextInfoBasedOnEvcId          */
/*                                                                           */
/* Description        : This routine gets the next EVC entry in context info */
/*                      based on EVC Identifier                              */
/*                                                                           */
/* Input(s)           : u2ContextId - Context No                             */
/*                      EvcId - EVC Indentifier                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LcmLocalEvcInfo                                      */
/*****************************************************************************/
tLcmLocalEvcInfo   *
LcmGetNextEvcEntryInContextInfoBasedOnEvcId (UINT2 u2ContextId, UINT1 *EvcId)
{
    tLcmLocalEvcInfo    Dummy;
    UINT2               u2Len = 0;

    LCM_MEMSET (&Dummy, 0, sizeof (Dummy));
   
    u2Len =
        (UINT2) (STRLEN(EvcId)  <
                (sizeof(Dummy.EvcIdentifier)-1) ?
                STRLEN(EvcId)  : (sizeof(Dummy.EvcIdentifier)-1));

    STRNCPY (Dummy.EvcIdentifier, EvcId, u2Len);

    Dummy.EvcIdentifier[u2Len] = '\0';

    return (tLcmLocalEvcInfo *) RBTreeGetNext
        (gLcmGlobalInfo.aContextInfo[u2ContextId].LcmEvcEntries,
         (tRBElem *) & Dummy, NULL);
}

/*****************************************************************************/
/* Function Name      : LcmEvcEntryInContextInfoTblCmpFn                     */
/*                                                                           */
/* Description        : This routine compares the EVC Identifier             */
/*                                                                           */
/* Input(s)           : pKey1 - First element for comparison                 */
/*                      pKey2 - Second element for comparison                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1:- EVC Identifier for pKey1 is greater than that of */
/*                      pKey2                                                */
/*                      -1:- EVC Identifier for pKey2 is greater than that of*/
/*                      pKey1                                                */
/*                      0:- When both the refernce ids are same              */
/*****************************************************************************/
INT4
LcmEvcEntryInContextInfoTblCmpFn (tRBElem * pKey1, tRBElem * pKey2)
{
    tLcmLocalEvcInfo   *pLcmLocalEvcInfo1 = (tLcmLocalEvcInfo *) pKey1;
    tLcmLocalEvcInfo   *pLcmLocalEvcInfo2 = (tLcmLocalEvcInfo *) pKey2;

    return STRCMP (pLcmLocalEvcInfo1->EvcIdentifier,
                   pLcmLocalEvcInfo2->EvcIdentifier);
}

/*****************************************************************************/
/* Function Name      : LcmFreeEvcEntryInContextInfo                         */
/*                                                                           */
/* Description        : This routine frees the EVC entry from global EVC     */
/*                      entries                                              */
/*                                                                           */
/* Input(s)           : pLcmLocalEvcInfo - MemBlock to be released           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS/LCM_FAILURE                              */
/*****************************************************************************/
INT4
LcmFreeEvcEntryInContextInfo (tLcmLocalEvcInfo * pLcmLocalEvcInfo, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pLcmLocalEvcInfo != NULL)
    {
        RBTreeDelete (pLcmLocalEvcInfo->LcmPortRBTree);
        LCM_RELEASE_EVC_INFO_MEM_BLOCK (pLcmLocalEvcInfo);
    }

    return 1;
}

/*****************************************************************************/
/* Function Name      : LcmAddToEvcEntryInContextInfoTable                   */
/*                                                                           */
/* Description        : This routine adds an EVC entry in Global EVC entries */
/*                                                                           */
/* Input(s)           : pLcmLocalEvcInfo - EVC entry in Port info            */
/*                      u2ContextId - Port No                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS/LCM_FAILURE                              */
/*****************************************************************************/
INT4
LcmAddToEvcEntryInContextInfoTable (tLcmLocalEvcInfo * pLcmLocalEvcInfo,
                                    UINT2 u2ContextId)
{
    if (RBTreeAdd (gLcmGlobalInfo.aContextInfo[u2ContextId].LcmEvcEntries,
                   (tRBElem *) pLcmLocalEvcInfo) == RB_FAILURE)
    {
        return LCM_FAILURE;
    }

    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LcmRemoveFromEvcEntryInContextInfoTable              */
/*                                                                           */
/* Description        : This routine deletes EVC entry in Global EVC entries */
/*                                                                           */
/* Input(s)           : pLcmLocalEvcInfo - EVC entry in Port info            */
/*                      u2ContextId - Port No                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS/LCM_FAILURE                              */
/*****************************************************************************/
INT4
LcmRemoveFromEvcEntryInContextInfoTable (tLcmLocalEvcInfo * pLcmLocalEvcInfo,
                                         UINT2 u2ContextId)
{
    INT4                i4RetVal = LCM_SUCCESS;

    if (RBTreeRemove (gLcmGlobalInfo.aContextInfo[u2ContextId].LcmEvcEntries,
                      (tRBElem *) pLcmLocalEvcInfo) == RB_FAILURE)
    {
        i4RetVal = LCM_FAILURE;
    }

    return i4RetVal;
}

/************** Stubs Code End *********************************/
