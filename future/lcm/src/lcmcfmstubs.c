
#define _LCMCFMSTUBS_C

#include "lcminc.h"
#include "lcmcfmstubs.h"

/*******For CFM Start **********/
/*****************************************************************************/
/* Function Name      : CfmCreateMA                                          */
/*                                                                           */
/* Description        : This routines creates a Maintenance Association      */
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC identifier                            */
/*                      u2SVlan - SVLAN for the EVC                          */
/*                      pu1DomainName - Domain in which MA needs to be       */
/*                       created                                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/
INT4
CfmCreateMA (UINT1 *pu1EvcId, UINT2 u2SVlan, UINT1 *pu1DomainName)
{
    CFM_UNUSED (pu1EvcId);
    CFM_UNUSED (u2SVlan);
    CFM_UNUSED (pu1DomainName);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfmCreateMeps                                        */
/*                                                                           */
/* Description        : This routines creates the desired no of MEPs         */
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC identifier                            */
/*                      u2NumberOfMeps - No of MEPs to be created            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/
INT4
CfmCreateMeps (UINT1 *pu1EvcId, UINT2 u2NumberOfMeps)
{
    CFM_UNUSED (pu1EvcId);
    CFM_UNUSED (u2NumberOfMeps);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfmMepDown                                           */
/*                                                                           */
/* Description        : This routines is called when MEP goes down           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/
INT4
CfmMepDown (VOID)
{
    UINT1              *pu1EvcId = NULL;
    UINT1               u1Status = 0;
    /* Update VCD database */
    LcmEvcStatusChange (pu1EvcId, u1Status);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfmFindDomain                                        */
/*                                                                           */
/* Description        : This routines searches for the given domain          */
/*                                                                           */
/* Input(s)           : pu1DomainName - Domain name to be searched           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/
INT4
CfmFindDomain (UINT1 *pu1DomainName)
{
    CFM_UNUSED (pu1DomainName);

    /*Check if domain exists, if domain present return SUCCESS else failure */
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfmInitialize                                        */
/*                                                                           */
/* Description        : This routines initializes the CFM module             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/
INT4
CfmInitialize (VOID)
{
    return CLI_SUCCESS;
}

/*******For CFM End **********/
