/***********failure reason error string**********************/
#define ALL_FAIL                       255
#define PCI_TEST_FAIL                  1
#define SCHANNEL_TEST_FAIL             2
#define COUNTER_RW_TEST_FAIL           4 
#define COUNTER_WIDTH_TEST_FAIL             8
#define MAC_LOOPBACK_TEST_FAIL              16
#define IBIT_TEST_FAIL                 32
#define CBIT_TEST_FAIL                 64                                
#define CONFIG_RESTORE_FAILURE         128
#define BITE_FAILURE_REASON_MAX_LENGTH       100
                               /* Pci and S-Channel test failure                        	3
                                Pci and counter read/write tests failure              	5
                                Pci and counter width tests failure                   	9
                                Pci and Mac loopback tests failure                    	17
                                Pci and IBIT test failure                                                33
                                Pci and CBIT test failure                                              65
                                Pci and configuration restore failure                            129
                                S-channel and counter read/write tests failure        	 6
                                S-channel and counter width tests failure             	10
                                S-channel and Mac loopback tests failure             	18
                                S-channel and IBIT test failure                                    	34
                                S-channel and CBIT test failure                                   66
                                S-channel and configuration restore failure                 130
                                Counter read/write and counter width tests failure    	12
                                Counter read/write and Mac loopback tests failure     	20
                                Counter read/write and IBIT test failure                     	36
                                Counter read/write and CBIT test failure                     	68
                                Counter read/write and Configuration restore failure 	132   
                                Counter width and Mac loopback tests failure          	24 
                                Counter width and IBIT test failure                              	40
                                Counter width and CBIT test failure                             72
                                Counter width and Configuration restore failure          136
                                 Mac Loopback and IBIT test failure                            48
                                 Mac Loopback and CBIT test failure                           80
                                 Mac Loopback and configuration restore failure        	144 */



/***************************self test error string***************************/

#define PCI_TEST_FAIL                  1
#define SCHANNEL_TEST_FAIL             2
#define CPU_BENCHMARK_TEST_FAIL        4
#define ALL_FAIL                       7
#define BITE_IBIT_FAILURE_MAX_LENGTH         100
                               /* Pci and channel tests failure                                   	3
                                Pci and CPU benchmark tests failure                     	5
                                S-channel and CPU benchmark tests failure          	6
                                All self tests failed                        	7*/
/****************************cont self test err string **************/
#define INTERFACE_STATISTICS_COUNTER_FAIL                        	 1
#define IP_STATISTICS_COUNTER_FAIL                                    	 2
#define ICMP_STATISTICS_COUNTER_FAIL                             	 4
#define TCP_STATISTICS_COUNTER_FAIL                                 	 8
#define UDP_STATISTICS_COUNTER_FAIL                                      16
#define ALL_FAIL                                                         31
#define BITE_CBIT_FAILURE_MAX_LENGTH
                           /* Interface and Ip statistics counter failure              	 3
                            Interface and Icmp statistics counter failure         	 5
                            Interface and Tcp statistics counter failure          	 9
                            Interface and Udp statistics counter failure          	17              
                            Ip and Icmp statistics counter failure                    	 6
                            Ip and Tcp statistics counter failure                     	10       
                            Ip and Udp statistics counter failure                     	18      
                            Icmp and Tcp statistics counter failure                 	12       
                            Icmp and Udp statistics counter failure                	20
                            Tcp and udp statistics counter failure                  	24   
                            All statistics counters failed                                  	31*/


/*******************cont self test err string for interface**********************/
#define INTERFACE_OPER_STATUS_FAIL               1
#define INTERFACE_IN_DISCARDS_FAIL               2
#define INTERFACE_IN_ERRORS_FAIL                 4
#define INTERFACE_IN_UNKNOWN_PROTOS_FAIL         8
#define INTERFACE_OUT_DISCARDS_FAIL              16
#define INTERFACE_OUT_ERRORS_FAIL                32
#define ALL_FAIL                                 63
#define BITE_IF_CBIT_FAILURE_MAX_LENGTH          100
                            /*Interface OperStatus and InDiscards statistics failure                   	3 
                            Interface OperStatus and InErrors statistics failure                        	5         
                            Interface OperStatus and UnknownProtos statistics failure           	9           
                            Interface OperStatus and OutDiscards statistics failure                	17           
                            Interface OperStatus and OutErrors statistics failure                    	33          
                            Interface InDiscards and InErrors statistics failure                         	 6        
                            Interface InDiscards and UnknownProtos statistics failure           	10          
                            Interface InDiscards and OutDiscards statistics failure                 	18          
                            Interface InDiscards and OutErrors statistics failure                     	34         
                            Interface InErrors and UnknownProtos statistics failure               	12          
                            Interface InErrors and OutDiscards statistics failure                     	20       
                            Interface InErrors and OutErrors statistics failure                         	36           
                            Interface UnknownProtos and OutDiscards statistics failure        	24            
                            Interface UnknownProtos and OutErrors statistics failure            	40
                            Interface OutDiscards and OutErrors statistics failure                  	48        
                            All Interface statistics counters failed                                            	63

*/


#define IP_IN_HDR_ERR_FAIL            1
#define IP_IN_ADDR_FAIL               2
#define IP_IN_UNKNOWN_PROTO_FAIL      4
#define IP_IN_DISCARDS_FAIL            8
#define IP_OUT_DISCARDS_FAIL          16
#define IP_REASM_FAIL                 32
#define IP_FRAG_FAIL                  64

#define ICMP_IN_ERR_FAIL            1
#define ICMP_OUT_ERR_FAIL           2
