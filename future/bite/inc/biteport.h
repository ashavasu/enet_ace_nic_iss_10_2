/***************************Functions in File :biteport.c ******************************************************/
/*******************have included for BCM*************/
typedef int bcmx_lport_t;
/* have to remove */
typedef char *parse_key_t;

typedef struct args_s {
    parse_key_t a_cmd;               
    char        *a_argv[1024];      
    char        a_buffer[4096]; 
    int         a_argc;                 
    int         a_arg;                  
} args_t;


typedef int     (*test_ifunc_t)(int, args_t *, void **);
typedef int     (*test_dfunc_t)(int, void *);
typedef int     (*test_func_t)(int, args_t *, void *);

typedef struct test_s {
    char        *t_name;                 
    UINT4      t_flags;                
#   define      T_F_SEL_ALL     (1 << 0) 
#   define      T_F_SEL_CHIP    (1 << 1) 
#   define      T_F_SELECT      (T_F_SEL_ALL | T_F_SEL_CHIP)
#   define      T_F_ACTIVE      (1 << 2)
#   define      T_F_STOP        (1 << 3) 
#   define      T_F_ERROR       (1 << 4) 
#   define      T_F_RC          (1 << 5) 



#   define      TSEL            T_F_SEL_CHIP

#   define      T5600           (1 << 16)
#   define      T5605           (1 << 17)
#   define      T5615           (1 << 18)
#   define      T5625           (T5615)
#   define      T5645           (T5615)
#   define      T5665           (1 << 19)
#   define      T5670           (1 << 20)
#   define      T5673           (1 << 21)
#   define      T5674           (T5673)
#   define      T5680           (1 << 22)
#   define      T5690           (1 << 23)
#   define      T5695           (1 << 24)
#   define      T5675           (1 << 25)
#   define      T56601          (1 << 26)
#   define      T56504          (1 << 27)

#   define      TSTRATA         (T5600 | T5605 | T5615 | T5625 | \
                                 T5645 | T5680)
#   define      TXGS3SW         (T56601 | T56504)
#   define      TXGSSW          (T5673 | T5690 | T5665 | T5695 | TXGS3SW)
#   define      TALL            (T5600 | T5605 | T5615 | T5625 | T5645 | \
                         T5670 | T5673 | T5680 | T5690 | T5665 | \
                         T5695 | T5675 | T56601 | T56504)
#   define      TRC             (T_F_RC)

    int         t_test;                 
    int         t_loops;                
    test_ifunc_t t_init_f;              
    test_dfunc_t t_done_f;              
    test_func_t t_test_f;               

    char        *t_default_string;      
    char        *t_override_string;     

    

    int         t_runs;                 
    int         t_success;           
    int         t_fail;                 
} test_t;


/*have to remove completed */
PUBLIC INT1 BitePortGetStatisticsCounter (UINT4  u4CounterType , UINT4 u4IfNum, UINT4  *pRetVal);

PUBLIC test_t  *BitePortGetTestEntry(char *);



