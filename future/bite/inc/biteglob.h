/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 *
 * Description: This file contains all the global varaiables to be
 *              used by BITE module
 *
 *******************************************************************/

#ifndef _BITEGLOB_H_
#define _BITEGLOB_H_
#ifdef _BITEINIT_C_
tBiteGlobalInfo          gBiteGlobalInfo;
#else
extern tBiteGlobalInfo          gBiteGlobalInfo;
#endif
#ifdef _BITETST_C_
char gFsTestsToRun [TEST_NAMES_SIZE]; /* FS_DY4_HWTEST */
#else
extern char gFsTestsToRun [TEST_NAMES_SIZE]; /* FS_DY4_HWTEST */
#endif

tTimerListId gBiteOnesecTimerLst;
tTmrAppTimer gBiteOnesecTimerNode;

UINT4  gu4BiteSysCntrl;
UINT4  gu4BiteMsrStatus;
UINT4  gu4MaxIfcs;

#ifdef _BITETEST_C_
UINT1 gu1Flag = FALSE;
#else
extern  gu1Flag;
#endif


#endif/*_BITEGLOB_H_*/
