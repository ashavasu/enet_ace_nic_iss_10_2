/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsbitedb.h,v 1.1.1.1 2011/07/21 14:06:33 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSBITEDB_H
#define _FSBITEDB_H

UINT1 LruIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsbite [] ={1,3,6,1,4,1,25545,5,1};
tSNMP_OID_TYPE fsbiteOID = {9, fsbite};


UINT4 LruOverallStatus [ ] ={1,3,6,1,4,1,25545,5,1,2,1};
UINT4 LruPowerOnSelfTest [ ] ={1,3,6,1,4,1,25545,5,1,2,2};
UINT4 LruContinuousSelfTest [ ] ={1,3,6,1,4,1,25545,5,1,2,3};
UINT4 LruFailureType [ ] ={1,3,6,1,4,1,25545,5,1,2,4};
UINT4 LruFailureReason [ ] ={1,3,6,1,4,1,25545,5,1,2,5};
UINT4 LruSelfTestCommand [ ] ={1,3,6,1,4,1,25545,5,1,3,1};
UINT4 LruSelfTestUpTime [ ] ={1,3,6,1,4,1,25545,5,1,3,2};
UINT4 LruLastResultSelfTest [ ] ={1,3,6,1,4,1,25545,5,1,3,3};
UINT4 LruSelfTestReturnCode [ ] ={1,3,6,1,4,1,25545,5,1,3,4};
UINT4 LruContinuousSelfTestCommand [ ] ={1,3,6,1,4,1,25545,5,1,3,5};
UINT4 LruThresholdIpInAddrErrors [ ] ={1,3,6,1,4,1,25545,5,1,3,6};
UINT4 LruThresholdIpInHdrErrors [ ] ={1,3,6,1,4,1,25545,5,1,3,7};
UINT4 LruThresholdIpInUnknownProtos [ ] ={1,3,6,1,4,1,25545,5,1,3,8};
UINT4 LruThresholdIpInDiscards [ ] ={1,3,6,1,4,1,25545,5,1,3,9};
UINT4 LruThresholdIpOutDiscards [ ] ={1,3,6,1,4,1,25545,5,1,3,10};
UINT4 LruThresholdIpReasmFails [ ] ={1,3,6,1,4,1,25545,5,1,3,11};
UINT4 LruThresholdIpFragFails [ ] ={1,3,6,1,4,1,25545,5,1,3,12};
UINT4 LruThresholdIcmpInErrors [ ] ={1,3,6,1,4,1,25545,5,1,3,13};
UINT4 LruThresholdIcmpOutErrors [ ] ={1,3,6,1,4,1,25545,5,1,3,14};
UINT4 LruThresholdTcpInErrors [ ] ={1,3,6,1,4,1,25545,5,1,3,15};
UINT4 LruThresholdUdpInErrors [ ] ={1,3,6,1,4,1,25545,5,1,3,16};
UINT4 LruIfIndex [ ] ={1,3,6,1,4,1,25545,5,1,3,17,1,1};
UINT4 LruIfThresholdOperStatus [ ] ={1,3,6,1,4,1,25545,5,1,3,17,1,2};
UINT4 LruIfThresholdInDiscards [ ] ={1,3,6,1,4,1,25545,5,1,3,17,1,3};
UINT4 LruIfThresholdInErrors [ ] ={1,3,6,1,4,1,25545,5,1,3,17,1,4};
UINT4 LruIfThresholdInUnknownProtos [ ] ={1,3,6,1,4,1,25545,5,1,3,17,1,5};
UINT4 LruIfThresholdOutDiscards [ ] ={1,3,6,1,4,1,25545,5,1,3,17,1,6};
UINT4 LruIfThresholdOutErrors [ ] ={1,3,6,1,4,1,25545,5,1,3,17,1,7};
UINT4 LruIfContinuousSelfTestStatus [ ] ={1,3,6,1,4,1,25545,5,1,3,17,1,8};
UINT4 LruIfContinuousSelfTestReturnCode [ ] ={1,3,6,1,4,1,25545,5,1,3,17,1,9};
UINT4 LruIfRowStatus [ ] ={1,3,6,1,4,1,25545,5,1,3,17,1,10};
UINT4 LruThresholdCounterReset [ ] ={1,3,6,1,4,1,25545,5,1,3,18};
UINT4 LruContinuousSelfTestReturnCode [ ] ={1,3,6,1,4,1,25545,5,1,3,19};
UINT4 LruSwitchName [ ] ={1,3,6,1,4,1,25545,5,1,1,1};
UINT4 LruHardwarePartNumber [ ] ={1,3,6,1,4,1,25545,5,1,1,2};
UINT4 LruSoftwarePartNumber [ ] ={1,3,6,1,4,1,25545,5,1,1,3};
UINT4 LruSerialNumber [ ] ={1,3,6,1,4,1,25545,5,1,1,4};


tMbDbEntry fsbiteMibEntry[]= {

{{11,LruSwitchName}, NULL, LruSwitchNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, "FireBlade"},

{{11,LruHardwarePartNumber}, NULL, LruHardwarePartNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,LruSoftwarePartNumber}, NULL, LruSoftwarePartNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,LruSerialNumber}, NULL, LruSerialNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,LruOverallStatus}, NULL, LruOverallStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, "0"},

{{11,LruPowerOnSelfTest}, NULL, LruPowerOnSelfTestGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, "0"},

{{11,LruContinuousSelfTest}, NULL, LruContinuousSelfTestGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, "0"},

{{11,LruFailureType}, NULL, LruFailureTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, "0"},

{{11,LruFailureReason}, NULL, LruFailureReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,LruSelfTestCommand}, NULL, LruSelfTestCommandGet, LruSelfTestCommandSet, LruSelfTestCommandTest, LruSelfTestCommandDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,LruSelfTestUpTime}, NULL, LruSelfTestUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, "0"},

{{11,LruLastResultSelfTest}, NULL, LruLastResultSelfTestGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, "1"},

{{11,LruSelfTestReturnCode}, NULL, LruSelfTestReturnCodeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,LruContinuousSelfTestCommand}, NULL, LruContinuousSelfTestCommandGet, LruContinuousSelfTestCommandSet, LruContinuousSelfTestCommandTest, LruContinuousSelfTestCommandDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,LruThresholdIpInAddrErrors}, NULL, LruThresholdIpInAddrErrorsGet, LruThresholdIpInAddrErrorsSet, LruThresholdIpInAddrErrorsTest, LruThresholdIpInAddrErrorsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "32767"},

{{11,LruThresholdIpInHdrErrors}, NULL, LruThresholdIpInHdrErrorsGet, LruThresholdIpInHdrErrorsSet, LruThresholdIpInHdrErrorsTest, LruThresholdIpInHdrErrorsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "32767"},

{{11,LruThresholdIpInUnknownProtos}, NULL, LruThresholdIpInUnknownProtosGet, LruThresholdIpInUnknownProtosSet, LruThresholdIpInUnknownProtosTest, LruThresholdIpInUnknownProtosDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "32767"},

{{11,LruThresholdIpInDiscards}, NULL, LruThresholdIpInDiscardsGet, LruThresholdIpInDiscardsSet, LruThresholdIpInDiscardsTest, LruThresholdIpInDiscardsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "32767"},

{{11,LruThresholdIpOutDiscards}, NULL, LruThresholdIpOutDiscardsGet, LruThresholdIpOutDiscardsSet, LruThresholdIpOutDiscardsTest, LruThresholdIpOutDiscardsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "32767"},

{{11,LruThresholdIpReasmFails}, NULL, LruThresholdIpReasmFailsGet, LruThresholdIpReasmFailsSet, LruThresholdIpReasmFailsTest, LruThresholdIpReasmFailsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "32767"},

{{11,LruThresholdIpFragFails}, NULL, LruThresholdIpFragFailsGet, LruThresholdIpFragFailsSet, LruThresholdIpFragFailsTest, LruThresholdIpFragFailsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "32767"},

{{11,LruThresholdIcmpInErrors}, NULL, LruThresholdIcmpInErrorsGet, LruThresholdIcmpInErrorsSet, LruThresholdIcmpInErrorsTest, LruThresholdIcmpInErrorsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "32767"},

{{11,LruThresholdIcmpOutErrors}, NULL, LruThresholdIcmpOutErrorsGet, LruThresholdIcmpOutErrorsSet, LruThresholdIcmpOutErrorsTest, LruThresholdIcmpOutErrorsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "32767"},

{{11,LruThresholdTcpInErrors}, NULL, LruThresholdTcpInErrorsGet, LruThresholdTcpInErrorsSet, LruThresholdTcpInErrorsTest, LruThresholdTcpInErrorsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "32767"},

{{11,LruThresholdUdpInErrors}, NULL, LruThresholdUdpInErrorsGet, LruThresholdUdpInErrorsSet, LruThresholdUdpInErrorsTest, LruThresholdUdpInErrorsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "32767"},

{{13,LruIfIndex}, GetNextIndexLruIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, LruIfTableINDEX, 1, 0, 0, NULL},

{{13,LruIfThresholdOperStatus}, GetNextIndexLruIfTable, LruIfThresholdOperStatusGet, LruIfThresholdOperStatusSet, LruIfThresholdOperStatusTest, LruIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, LruIfTableINDEX, 1, 0, 0, "32767"},

{{13,LruIfThresholdInDiscards}, GetNextIndexLruIfTable, LruIfThresholdInDiscardsGet, LruIfThresholdInDiscardsSet, LruIfThresholdInDiscardsTest, LruIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, LruIfTableINDEX, 1, 0, 0, "32767"},

{{13,LruIfThresholdInErrors}, GetNextIndexLruIfTable, LruIfThresholdInErrorsGet, LruIfThresholdInErrorsSet, LruIfThresholdInErrorsTest, LruIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, LruIfTableINDEX, 1, 0, 0, "32767"},

{{13,LruIfThresholdInUnknownProtos}, GetNextIndexLruIfTable, LruIfThresholdInUnknownProtosGet, LruIfThresholdInUnknownProtosSet, LruIfThresholdInUnknownProtosTest, LruIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, LruIfTableINDEX, 1, 0, 0, "32767"},

{{13,LruIfThresholdOutDiscards}, GetNextIndexLruIfTable, LruIfThresholdOutDiscardsGet, LruIfThresholdOutDiscardsSet, LruIfThresholdOutDiscardsTest, LruIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, LruIfTableINDEX, 1, 0, 0, "32767"},

{{13,LruIfThresholdOutErrors}, GetNextIndexLruIfTable, LruIfThresholdOutErrorsGet, LruIfThresholdOutErrorsSet, LruIfThresholdOutErrorsTest, LruIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, LruIfTableINDEX, 1, 0, 0, "32767"},

{{13,LruIfContinuousSelfTestStatus}, GetNextIndexLruIfTable, LruIfContinuousSelfTestStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LruIfTableINDEX, 1, 0, 0, NULL},

{{13,LruIfContinuousSelfTestReturnCode}, GetNextIndexLruIfTable, LruIfContinuousSelfTestReturnCodeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LruIfTableINDEX, 1, 0, 0, NULL},

{{13,LruIfRowStatus}, GetNextIndexLruIfTable, LruIfRowStatusGet, LruIfRowStatusSet, LruIfRowStatusTest, LruIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LruIfTableINDEX, 1, 0, 1, NULL},

{{11,LruThresholdCounterReset}, NULL, LruThresholdCounterResetGet, LruThresholdCounterResetSet, LruThresholdCounterResetTest, LruThresholdCounterResetDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,LruContinuousSelfTestReturnCode}, NULL, LruContinuousSelfTestReturnCodeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData fsbiteEntry = { 37, fsbiteMibEntry };
#endif /* _FSBITEDB_H */

