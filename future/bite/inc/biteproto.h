#ifndef __BITE_PROTO_H__
#define __BITE_PROTO_H__
/***********************Functionss in the file : biteinit.c*****************************************************/


INT4 BiteMainInit (VOID);
INT4 BiteMainDeInit (VOID);
/***********************Functionss in the file : biteif.c*****************************************************/

INT4 BiteIfCreateAll (VOID);
tBiteIfInfo  *BiteIfCreate(UINT4 u4IfNum);
tBiteIfInfo  *BiteIfFind(UINT4 u4IfNum);
INT4 BiteIfDelete(UINT4 u4IfNum);
VOID BiteIfProcessStatusChange(UINT4 u4IfNum, UINT1 u1Status);
VOID BiteIfInitialize(tBiteIfInfo *pBiteIfInfo,UINT4 u4IfNum);
VOID BiteIfResetOperStatusCounter (VOID);
/***********************Functionss in the file : bitetmr.c*****************************************************/

 INT4 BiteTmrInit (VOID);
 INT4 BiteTmrDeInit (VOID);
 INT4 BiteTmrStartTmr (VOID);
 INT4 BiteTmrStopTmr (VOID);
 VOID BiteTmrExpHandler ( VOID );
/***********************Functionss in the file : bitetest.c*****************************************************/

VOID BitetestProcessMsrStatus (UINT4 u4Status);
INT4 BitetestStartIbitTest(VOID);
VOID BitetestStopCbitTest(VOID);
VOID  BitetestStartCbitTest(VOID);
VOID BitetestUpdatePbitStatus(VOID);
INT4 BitetestUpdateIbitStatus(VOID);
VOID BitetestMonitorCounterStatistics(UINT1 u1BaseFlag);
VOID BitetestResetThreshold(UINT4 u4OverallBitMask,UINT4 u4BitMask,UINT4 u4IfNum);
VOID BitetestGetErrCode(UINT4 u4TestType,UINT4 *pRetErrCode,UINT4 u4IfNum);
VOID BitetestGetHwNum(UINT1 *pRetValString);
VOID BitetestGetSwNum(UINT1 *pRetValString);
VOID BitetestGetSerialNum(UINT1 *pRetValString);
VOID BitetestLogFile(UINT4 u4TestType, UINT4 u4ErrCode);
VOID BiteErrIfCbitFailure(UINT4 u4IfNum,UINT4 u4Errcode,UINT1  *pRetErrstring);



/***********************Functionss in the file : biteque.c*****************************************************/

INT4 BiteQueEnqMsg (tBiteQMsg * pBiteQMsg);
INT4 BiteQuePostEventToBiteTask (INT4 i4Event);
VOID BiteQueMsgHandler (VOID);



/***********************Functionss in the file : biteerr.c*****************************************************/


VOID BiteErrFailureReason(UINT4 u4ErrCode, UINT1 *pRetErrString);
VOID BiteErrIbitFailure(UINT4 u4Err,UINT1 *pRetErr);
VOID BiteErrCbitFailure(UINT4 u4Errcode,UINT1 *pRetErrstring);
VOID BiteErrLogCbitFailure(UINT4 u4Errcode,UINT1 *pRetErrstring);
/******************functions related to bitesrc.c***************/
INT4 BiteSrcScalars  (tCliHandle CliHandle);
VOID BiteSrcInterfaces (tCliHandle CliHandle);
/****************************functions in biteapi.c ******************************/
 INT4 BiteApiLock(VOID);
INT4 BiteApiUnLock(VOID);


#endif
