#ifndef __BITE_INC_H__
#define __BITE_INC_H__

/*global header*/
#include "lr.h"
#include "fssnmp.h"
#include "cli.h"
#include "../../inc/bcm.h"

/*module header*/
#include "biteconst.h"
#include "bitetdfs.h"
#include "biteglob.h"
#include "biteproto.h"
#include "biteport.h"
#include "biteapi.h"
#include "fsbitelw.h"
#include "fsbitewr.h"
#include "bitecli.h"
#include "cfa.h"
#include "ip.h"
#ifdef NPAPI_WANTED
#include "npapi.h"
#endif
#endif/*__BITE_INC_H__*/

