/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsbitelw.h,v 1.1.1.1 2011/07/21 14:06:33 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLruOverallStatus ARG_LIST((INT4 *));

INT1
nmhGetLruPowerOnSelfTest ARG_LIST((INT4 *));

INT1
nmhGetLruContinuousSelfTest ARG_LIST((INT4 *));

INT1
nmhGetLruFailureType ARG_LIST((INT4 *));

INT1
nmhGetLruFailureReason ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLruSelfTestCommand ARG_LIST((INT4 *));

INT1
nmhGetLruSelfTestUpTime ARG_LIST((INT4 *));

INT1
nmhGetLruLastResultSelfTest ARG_LIST((INT4 *));

INT1
nmhGetLruSelfTestReturnCode ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLruContinuousSelfTestCommand ARG_LIST((INT4 *));

INT1
nmhGetLruThresholdIpInAddrErrors ARG_LIST((INT4 *));

INT1
nmhGetLruThresholdIpInHdrErrors ARG_LIST((INT4 *));

INT1
nmhGetLruThresholdIpInUnknownProtos ARG_LIST((INT4 *));

INT1
nmhGetLruThresholdIpInDiscards ARG_LIST((INT4 *));

INT1
nmhGetLruThresholdIpOutDiscards ARG_LIST((INT4 *));

INT1
nmhGetLruThresholdIpReasmFails ARG_LIST((INT4 *));

INT1
nmhGetLruThresholdIpFragFails ARG_LIST((INT4 *));

INT1
nmhGetLruThresholdIcmpInErrors ARG_LIST((INT4 *));

INT1
nmhGetLruThresholdIcmpOutErrors ARG_LIST((INT4 *));

INT1
nmhGetLruThresholdTcpInErrors ARG_LIST((INT4 *));

INT1
nmhGetLruThresholdUdpInErrors ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLruSelfTestCommand ARG_LIST((INT4 ));

INT1
nmhSetLruContinuousSelfTestCommand ARG_LIST((INT4 ));

INT1
nmhSetLruThresholdIpInAddrErrors ARG_LIST((INT4 ));

INT1
nmhSetLruThresholdIpInHdrErrors ARG_LIST((INT4 ));

INT1
nmhSetLruThresholdIpInUnknownProtos ARG_LIST((INT4 ));

INT1
nmhSetLruThresholdIpInDiscards ARG_LIST((INT4 ));

INT1
nmhSetLruThresholdIpOutDiscards ARG_LIST((INT4 ));

INT1
nmhSetLruThresholdIpReasmFails ARG_LIST((INT4 ));

INT1
nmhSetLruThresholdIpFragFails ARG_LIST((INT4 ));

INT1
nmhSetLruThresholdIcmpInErrors ARG_LIST((INT4 ));

INT1
nmhSetLruThresholdIcmpOutErrors ARG_LIST((INT4 ));

INT1
nmhSetLruThresholdTcpInErrors ARG_LIST((INT4 ));

INT1
nmhSetLruThresholdUdpInErrors ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LruSelfTestCommand ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2LruContinuousSelfTestCommand ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2LruThresholdIpInAddrErrors ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2LruThresholdIpInHdrErrors ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2LruThresholdIpInUnknownProtos ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2LruThresholdIpInDiscards ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2LruThresholdIpOutDiscards ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2LruThresholdIpReasmFails ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2LruThresholdIpFragFails ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2LruThresholdIcmpInErrors ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2LruThresholdIcmpOutErrors ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2LruThresholdTcpInErrors ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2LruThresholdUdpInErrors ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LruSelfTestCommand ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2LruContinuousSelfTestCommand ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2LruThresholdIpInAddrErrors ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2LruThresholdIpInHdrErrors ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2LruThresholdIpInUnknownProtos ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2LruThresholdIpInDiscards ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2LruThresholdIpOutDiscards ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2LruThresholdIpReasmFails ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2LruThresholdIpFragFails ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2LruThresholdIcmpInErrors ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2LruThresholdIcmpOutErrors ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2LruThresholdTcpInErrors ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2LruThresholdUdpInErrors ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LruIfTable. */
INT1
nmhValidateIndexInstanceLruIfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LruIfTable  */

INT1
nmhGetFirstIndexLruIfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLruIfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLruIfThresholdOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLruIfThresholdInDiscards ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLruIfThresholdInErrors ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLruIfThresholdInUnknownProtos ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLruIfThresholdOutDiscards ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLruIfThresholdOutErrors ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLruIfContinuousSelfTestStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLruIfContinuousSelfTestReturnCode ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLruIfRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLruIfThresholdOperStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetLruIfThresholdInDiscards ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetLruIfThresholdInErrors ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetLruIfThresholdInUnknownProtos ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetLruIfThresholdOutDiscards ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetLruIfThresholdOutErrors ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetLruIfRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LruIfThresholdOperStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2LruIfThresholdInDiscards ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2LruIfThresholdInErrors ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2LruIfThresholdInUnknownProtos ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2LruIfThresholdOutDiscards ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2LruIfThresholdOutErrors ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2LruIfRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LruIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLruThresholdCounterReset ARG_LIST((INT4 *));

INT1
nmhGetLruContinuousSelfTestReturnCode ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLruThresholdCounterReset ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LruThresholdCounterReset ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LruThresholdCounterReset ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLruSwitchName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLruHardwarePartNumber ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLruSoftwarePartNumber ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetLruSerialNumber ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
