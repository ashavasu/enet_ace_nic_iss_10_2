/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * Description: This file contains all the typedefs used by the BITE
 *              module.
 *******************************************************************/
#ifndef _BITETDFS_H
#define _BITETDFS_H



typedef struct _tBiteIfInfo
{
    UINT4 u4IfIndex;                       /*This represents the interface 
                                              index of the port to be monitored 
                                              as part of CBIT test*/
    UINT4 u4OperStatusCounter;               /*This represents counter that 
                                             gets incremented every time if 
                                             there is an operation status 
                                             change*/
    UINT4 u4OperStatus;                    /*operation status CFA_UP/CFA_DOWN*/
    UINT4 u4ThresholdIfOperStatus;        /*This represents the threshold 
                                             for operation status change*/
    UINT4 u4IfThresholdInDiscards;        /*This represents the threshold 
                                             for incoming packets discarded
                                             on the interface being monitored*/
    UINT4 u4IfInDisbaseval;        /*This represents the base value 
                                             of IfInDiscards counter to be 
                                             monitored as part of CBIT test*/
    UINT4 u4IfThresholdInErrors;          /*This represents the threshold 
                                             for incoming packets with errors 
                                             for the interface being monitored*/
    UINT4 u4IfInerrbaseval;          /*This represents the base value 
                                             of IfInErrors counter to be 
                                             monitored as part of CBIT test*/ 
    UINT4 u4IfThresholdInUnknownProtos;   /*This represents the threshold for 
                                             incoming packets with unknown 
                                             protocols on the interface 
                                             being monitored*/
    UINT4 u4IfInUnknwnProtbaseval;   /*This represents the base value 
                                             of IfInUnknownProtos counter to 
                                             be monitored as part of 
                                             CBIT test*/
    UINT4 u4IfThresholdOutDiscards;       /*This represents the threshold 
                                             for outgoing packets discardedon 
                                             the interface being monitored*/
    UINT4 u4IfOutDisbaseval;       /*This represents the base value 
                                             of IfOutDiscards counter to be 
                                             monitored as part of CBIT test*/
    UINT4 u4IfThresholdOutErrors;         /*This represents the threshold 
                                             for outgoing packets with errors 
                                             on the interface being monitored*/
    UINT4 u4IfOuterrbaseval;         /*This represents the base value 
                                             of IfOutErrors counter to be 
                                             monitored as part of CBIT test*/
    UINT4 u4IfContSelfTestStatus;   /*This represents the status of 
                                             the continuous self test of the  
                                             interface*/
    UINT4 u4IfContSelfTestRetCode;/*This represents the reason 
                                              for the failure of the last 
                                              executed CBIT test on the 
                                              interface*/
    UINT4 u4RowStatus;                     
}tBiteIfInfo;


/* This is the main BITE  queue message structure */
typedef struct BiteQMsg {
    UINT4                  u4IfNum;
    UINT1                  u1InterfaceStatus;
    UINT4                  u4MsgType;
    INT4                   i4MsrStatus;
    
}tBiteQMsg;

typedef struct _tBiteGlobalInfo
{

    tOsixTaskId BiteTaskId;            /* BITE Task identifier */
    tOsixSemId BiteSemId;              /*Semaphore Identifier */
     tMemPoolId BiteMemIfPoolId;          /* This corresponds to the memory pool
                                       identifier of interface database memory pool*/
    tMemPoolId BiteQueMemPoolId;          /* This corresponds to the memory pool for the Task queue */

    tTimerListId BiteTmrListId;        /*Timer Identifier of BITE timers*/
    tOsixQId BiteMsgQueId;             /*Queue identifier used for messages handling*/
    UINT4 u4OverallStatus;          /*This represents the overall status of the 
                                        device*/
    UINT4 u4PowerOnSelfTest;        /*This represents the result of power on 
                                       self test(PBIT)*/
    UINT4 u4ContSelfTest;             /*This represents the result of 
                                       continuous self test(CBIT)*/
    UINT4 u4FailureType;            /*This represents the failure type       
                                       due to PBIT,IBIT or CBIT   failure*/
    UINT4 u4FailureReason;          /*This represents the reason for the overall 
                                       health status failure */
    UINT4 u4SelfTestCommand;       /*This represents the Initiated built in test 
                                      status such as the IBIT test is on, off, 
                                      or in progress*/
    UINT4 u4SelfTestUpTime;        /*This represents the time since the las
                                      t Initiated built in test was activated*/
    UINT4 u4LastResultSelfTest;    /*This represents the result of last 
                                      executed Initiated built in test */
    UINT4 u4SelfTestReturnCode;    /*This represents the reason for     
                                         failure of the last executed  I
                                         nitiated built in test command*/
    UINT4 u4ContSelfTestCmd; /*This represents the continuous 
                                          built in test status such as the 
                                         CBIT test is on, off, or in progress*/
    UINT4 u4ThresholdIpInAddrErrors; /*This represents the threshold for 
                                         incoming ip packets  with address 
                                         errors*/

    UINT4 u4IpInAdderrbaseval;  /*This represents the base value of 
                                         IpInAddrErrors  counter to be 
                                         monitored as part of CBIT test*/
    UINT4 u4ThresholdIpInHdrErrors;  /*This represents the threshold for 
                                         incoming ip packets with header 
                                         errors*/
    UINT4 u4IpInHdrerrbaseval; /*This represents the base value of 
                                       IpInHdrErrors  counter to be 
                                        monitored as part of CBIT test*/
    UINT4 u4ThresholdIpInUnknownProtos; /*This represents the threshold 
                                            for incoming ip packets with 
                                            unknown protocols*/
    UINT4 u4IpInUnknwnProbaseval;/*This represents the base value of 
                                          IpInUnknownProtos counter to be 
                                          monitored as part of CBIT test*/
    UINT4 u4ThresholdIpInDiscards; /*This represents the threshold for i
                                         ncoming ip packets Discarded*/
    UINT4 u4IpInDisbaseval; /*This represents the base value of 
                                       IpInDiscards counter to be monitored 
                                       as part of CBIT test*/
    UINT4 u4ThresholdIpOutDiscards; /*This represents the threshold 
                                        for outgoing  ip packets Discarded*/
    UINT4 u4IpOutDisbaseval; /*This represents the base value of 
                                       IpOutDiscards counter to be monitored 
                                       as part of CBIT test*/
    UINT4 u4ThresholdIpReasmFails; /*This represents the threshold 
                                       for ip packets with reassembly failures*/
    UINT4 u4IpReasmFailbaseval; /*This represents the base value of 
                                      IpReasmFails counter to be monitored as 
                                      part of CBIT test*/
    UINT4 u4ThresholdIpFragFails; /*This represents the threshold for  
                                     ip packets with  fragmentation failures*/
    UINT4 u4IpFragFailbaseval; /*This represents the base value of 
                                     IpFragFails counter to be monitored as 
                                      part of CBIT test*/
    UINT4 u4ThresholdIcmpInErrors; /*This represents the threshold 
                                      for incoming icmp packets with errors*/
    UINT4 u4IcmpInerrbaseval; /*This represents the base value of 
                                      IcmpInErrors counter to be monitored as 
                                      part of CBIT test*/
    UINT4 u4ThresholdIcmpOutErrors; /*This represents the threshold for 
                                       outgoing icmp packets with errors*/
    UINT4 u4IcmpOuterrbaseval; /*This represents the base value of 
                                       IcmpOutErrors counter to be monitored 
                                       as part of CBIT test*/
    UINT4 u4ThresholdTcpInErrors;  /*This represents the threshold 
                                      for incoming tcp packets with  errors*/
    UINT4 u4TcpInerrbaseval; /*This represents the base value of  
                                      TcpInErrors counter to be monitored 
                                      as part of CBIT test*/
    UINT4 u4ThresholdUdpInErrors; /*This represents the threshold for 
                                     incoming udp packets with errors*/
    UINT4 u4UdpInerrbaseval; /* This represents the base value 
                                        of  UdpInErrors counter to be 
                                        monitored as part of CBIT test*/
    UINT4 u4CounterReset;         /* resetting the counter to base val                                             ues*/
    UINT4 u4ContSelfTestRetCode; /*This represents the reason 
                                             for the failure of the last 
                                             executed CBIT test*/
    UINT4 u4IpStatsErrCode; /*IP statisyics counter error code */
    UINT4 u4IcmpStatsErrCode; /*ICMP statistics counter error code */
    INT4 i4PciRunCount;   /*Number of runs of pci test */
    INT4 i4PciFailCount;  /*number of  fails in pci test*/
    INT4 i4PciSuccessCount;/*number of successes in pci */
    INT4 i4SchRunCount; /*number of runs in schannel */
    INT4 i4SchFailCount; /*number of fails in schannel*/  
    INT4 i4SchSuccessCount;/*number of successes in schannel */
    INT4 i4CpuRunCount; /*number of runs in cpu benchmark test */
    INT4 i4CpuFailCount; /*number of fails in cpu benchmark*/  
    INT4 i4CpuSuccessCount;/*number of successes in cpu benchmark */
    UINT1 au1HwPartNum[BITE_HW_NUM_MAX_LEN];/*hw num*/
    UINT1 au1SerialNum[BITE_SERIAL_NUM_MAX_LEN];/*serial num*/
    UINT1 au1SwVerNum[BITE_SW_NUM_MAX_LEN];/*sw version num*/
    tBiteIfInfo *au1BiteIfInfo[SYS_DEF_MAX_POSSIBLE_INTERFACES];/*This represents the array structure for storing the interface information of each port*/
}tBiteGlobalInfo;


#endif
