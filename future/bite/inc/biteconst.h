#ifndef __BITE_CONST_H__
#define __BITE_CONST_H__
#include "msr.h"
#include "biteinc.h"
/**********************************constants in fsbitelw.c **************************************/
#define BITE_MIN_THRESHOLD_VALUE 0
#define BITE_MAX_THRESHOLD_VALUE 65535
#define BITE_HW_NUM_MAX_LEN 20
#define BITE_SERIAL_NUM_MAX_LEN 12
#define BITE_SW_NUM_MAX_LEN 20
#define SOC_MIN_NUM_DEVICES 1
#define BITE_COUNTER_RESET_ON 1
#define BITE_COUNTER_RESET_OFF 2
#define SYS_DEF_MAX_POSSIBLE_INTERFACES 24
/****************constants for logging info in bitlog file***********************************************/
#define BITE_PBIT_TEST 1
#define BITE_IBIT_TEST 2
#define BITE_CBIT_TEST 3
#define BITE_CONFIG_RESTORE 4
#define BITE_OVERALL_FAIL 5
#define BITE_INTERFACE_FAIL 6
#define BITE_MAX_LOG_FILE_SIZE 5242880
/* constants in biteinit.c */
#define BITE_IBIT_TEST_EVT         1
#define BITE_TMR_EXP_EVENT         2
#define BITE_CONF_MSG_ENQ_EVENT    3
#define BITE_ALWAYS                1
#define MEM_DEFAULT_MEMORY_TYPE    0x00
#define BITE_TASK                  ((UINT1 *)"BITE")
#define BITE_PDU_QUEUE_NAME        ((UINT1 *)"BITQ")
#define BITE_SEM_NAME              ((UINT1 *)"BITS")
#define BITE_PDU_Q_DEPTH           12
#define BITE_PASS                  1
#define BITE_FAILED                2
#define BITE_RESERVED              0
#define BITE_NONE                  0
#define BITE_ZERO                  0
#define BITE_ONE                   1
#define BITE_THRESHOLD_DEFAULT_VALUE 32767

/*constants in biteif.c*/
#define BITE_IBIT_TEST_ON 1
#define BITE_IBIT_TEST_OFF 2
#define BITE_IBIT_TEST_INPRGS 3
#define BITE_CBIT_TEST_ON 1
#define BITE_CBIT_TEST_OFF 2
#define BITE_CBIT_TEST_INPRGS 3
#define BITE_HARDWARE_FAILURE 1
#define BITE_SOFTWARE_FAILURE 2


/* constanats in bitetest.c */
#define BITE_TESTNAME_LEN          5 
#define BITE_MAX_LINE_LEN          200
#define BITE_SCRIPT_DIR            "flash:scripts/"
#define BITE_EOF                   2
#define BITE_RDONLY                1
#define BITE_WRONLY                2
#define BITE_CREATE                4
#define BITE_WRONLY                2
#define  BITE_MIB_RESTORE_IN_PROGRESS      MIB_RESTORE_IN_PROGRESS      
#define  BITE_LAST_MIB_RESTORE_SUCCESSFUL  LAST_MIB_RESTORE_SUCCESSFUL  
#define  BITE_LAST_MIB_RESTORE_FAILED      LAST_MIB_RESTORE_FAILED      
#define  BITE_MIB_RESTORE_NOT_INITIATED    MIB_RESTORE_NOT_INITIATED    

#define BITE_ERR_LOG_MAX_LEN  BITE_FAILURE_REASON_MAX_LEN
#define BITE_LOG_FILE  "flash:/bite.log"
#define CWHWCFG_FILE "flash:default/cwhwcfg.txt"
#define CWSWCFG_FILE "flash:default/cwswcfg.txt"

/* error code bit mask */

/***********failure reason error string**********************/
#define PCI_TEST_FAIL                  1
#define SCHANNEL_TEST_FAIL             2
#define COUNTER_RW_TEST_FAIL           4
#define COUNTER_WIDTH_TEST_FAIL        8
#define PHY_LOOPBACK_TEST_FAIL         16
#define IBIT_TEST_FAIL                 32
#define CBIT_TEST_FAIL                 64
#define CONFIG_RESTORE_FAILURE         128

/*****************************ibit test err string ***********************/
#define PCI_TEST_FAIL                  1
#define SCHANNEL_TEST_FAIL             2
#define CPU_BENCHMARK_TEST_FAIL        4
/****************************cont self test err string **************/
#define INTERFACE_STATISTICS_COUNTER_FAIL                                1
#define IP_STATISTICS_COUNTER_FAIL                                       2
#define ICMP_STATISTICS_COUNTER_FAIL                                     4
#define TCP_STATISTICS_COUNTER_FAIL                                      8
#define UDP_STATISTICS_COUNTER_FAIL                                      16


/*******************cont self test err string for interface**********************/
#define INTERFACE_OPER_STATUS_FAIL               1
#define INTERFACE_IN_DISCARDS_FAIL               2
#define INTERFACE_IN_ERRORS_FAIL                 4
#define INTERFACE_IN_UNKNOWN_PROTOS_FAIL         8
#define INTERFACE_OUT_DISCARDS_FAIL              16
#define INTERFACE_OUT_ERRORS_FAIL                32

/***********************IP error code bit mask***********************************/
#define IP_IN_HDR_ERR_FAIL            1
#define IP_IN_ADDR_FAIL               2
#define IP_IN_UNKNOWN_PROTO_FAIL      4
#define IP_IN_DISCARDS_FAIL            8
#define IP_OUT_DISCARDS_FAIL          16
#define IP_REASM_FAIL                 32
#define IP_FRAG_FAIL                  64

/*******************************ICMP error code bitmask **************************/
#define ICMP_IN_ERR_FAIL            1
#define ICMP_OUT_ERR_FAIL           2

#define BITE_ONE_MASK 		    0xFFFE	
#define BITE_TWO_MASK               0xFFFD
#define BITE_FOUR_MASK              0xFFFB 
#define BITE_EIGHT_MASK             0xFFF7 
#define BITE_SIXTEEN_MASK           0xFFEF
#define BITE_THIRTYTWO_MASK         0xFFDF
#define BITE_SIXTY_MASK             0xFFBF

/****constants in biteque.c*********/

#define BITE_CFA_MSG 1/*msg from cfa */
#define BITE_MSR_MSG 2/* msg from msr */
/*************************************included to run IBIT test bcmtestrun()****************/
#define TEST_NAMES_SIZE         16 * 33   /* FS_DY4_HWTEST */
/****************constants in biteport.c******************************************************/
               /* IP counters */
#define BITE_IP_IN_HDR_ERRORS      1
#define BITE_IP_IN_ADDR_ERRORS     2
#define BITE_IP_IN_UNKNOWN_PROTOS  3 
#define BITE_IP_IN_DISCARDS        4
#define BITE_IP_OUT_DISCARDS       5
#define BITE_IP_REASM_FAILS        6
#define BITE_IP_FRAG_FAILS         7
#define BITE_ICMP_IN_ERRORS        8
#define BITE_ICMP_OUT_ERRORS       9
#define BITE_TCP_IN_ERRORS         10
#define BITE_UDP_IN_ERRORS         11
#define BITE_IF_IN_DISCARDS        12  
#define BITE_IF_IN_ERRORS          13 
#define BITE_IF_IN_UNKNOWN_PROTOS  14
#define BITE_IF_OUT_DISCARDS       15  
#define BITE_IF_OUT_ERRORS         16 

#endif
