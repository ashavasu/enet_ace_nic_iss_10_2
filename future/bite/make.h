#
# (C) 2001 Future Software Pvt. Ltd.
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Future Software                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : VXWORKS                                       |
# |                                                                          |
# |   DATE                   :                                               |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            included for building the BITE                |
# |                            Module .                                      |
# +--------------------------------------------------------------------------+
#
#     CHANGE RECORD :
# +--------------------------------------------------------------------------+
# | VERSION | AUTHOR/    | DESCRIPTION OF CHANGE                             |
# |         | DATE       |                                                   |
# +---------|------------|---------------------------------------------------+
# +--------------------------------------------------------------------------+



# Set the BITE_BASE_DIR as the directory where you untar the project files

BITE_NAME = bite
BITE_BASE_DIR = ${BASE_DIR}/bite
BITE_SRC_DIR = ${BITE_BASE_DIR}/src
BITE_CO_INC_DIR = ${BITE_BASE_DIR}/inc
BITE_OBJ_DIR = ${BITE_BASE_DIR}/obj
BITE_MAIN_INC_DIR = $(BASE_DIR)/inc
BITE_CLI_INC_DIR = $(BASE_DIR)/inc/cli


# Specify the project include directories and dependencies
BITE_CO_INC_FILES =   $(BITE_CO_INC_DIR)/fsbitelw.h \
                      $(BITE_CO_INC_DIR)/fsbitewr.h \
                      $(BITE_CO_INC_DIR)/fsbitedb.h \
                      $(BITE_CO_INC_DIR)/biteinc.h \
                      $(BITE_CO_INC_DIR)/biteglob.h \
                      $(BITE_CO_INC_DIR)/bitetdfs.h \
                      $(BITE_CO_INC_DIR)/biteconst.h \
                      $(BITE_CO_INC_DIR)/biteproto.h \
                      $(BITE_CO_INC_DIR)/biteapi.h \
                      $(BITE_CO_INC_DIR)/biteport.h \
                      $(BITE_MAIN_INC_DIR)/bite.h \
                      $(BITE_CLI_INC_DIR)/bitecli.h


BITE_ALL_INC_FILES = $(BITE_CO_INC_FILES)

BITE_INC_DIR = -I$(BITE_CO_INC_DIR)


BITE_FINAL_INCLUDES_DIRS = $(BITE_INC_DIR) \
                               $(COMMON_INCLUDE_DIRS)

BITE_DEPENDENCIES = $(COMMON_DEPENDENCIES) \
                        $(BITE_ALL_INC_FILES) \
                        $(BITE_BASE_DIR)/Makefile \
                        $(BITE_BASE_DIR)/make.h \
                        $(BITE_MAIN_INC_DIR)/bite.h

                         
