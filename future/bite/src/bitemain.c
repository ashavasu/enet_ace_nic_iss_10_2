/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: bitemain.c,v 1.1.1.1 2011/07/21 14:06:33 siva Exp $
 *
 * Description: This file contains BITE task main loop and initialization
 *              routines.
 *********************************************************************/
#ifndef _BITEINIT_C_
#define _BITEINIT_C_
#include "biteinc.h"

VOID                BiteMainInitializeGlobInfo (VOID);
VOID                BiteMainTask (VOID);
extern INT4         BiteApiLock (VOID);
extern INT4         BiteApiUnLock (VOID);
extern UINT4	IssGetFrontPanelPortCountFromNvRam(VOID);
/*****************************************************************************/
/* Function                  : BiteMainTask                                  */
/*                                                                           */
/* Description               : This is the initialization function for the   */
/*                             BITE module called during boot up time. This  */
/*                             function will wait for the various events     */
/*                             that are being triggered by external modules  */
/*                             and process those events once triggered.      */
/*                                                                           */
/* Input                     : None                                          */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gBiteGlobalInfo.BiteTaskId                    */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
VOID
BiteMainTask (VOID)
{
    UINT4               u4Events = 0;


	gu4MaxIfcs = IssGetFrontPanelPortCountFromNvRam();
    if (BiteMainInit () == OSIX_FAILURE)
    {
        /* Indicate the status of initialization to the main routine */
        lrInitComplete (OSIX_FAILURE);
    }
    /* An empty log file is created during the bite task creation */
    BitetestOpenFile ();

    BitetestUpdatePbitStatus ();    /*PBIT test status is updated */

    BiteIfCreateAll ();            /*Intefaces to a maximum of SOC_MAX_NUM_DEVICEScreated */
    BitetestStartCbitTest ();    /*CBIT started for monitoring the countrs */

    /* Register the MIBs with SNMP Agent */
    RegisterFSBITE ();

    lrInitComplete (OSIX_SUCCESS);
    gu4BiteSysCntrl = TRUE;
    while (BITE_ALWAYS)
    {
        if (OsixReceiveEvent (BITE_TMR_EXP_EVENT |
                              BITE_CONF_MSG_ENQ_EVENT | BITE_IBIT_TEST_EVT,
                              OSIX_EV_ANY, (UINT4) NULL,
                              (UINT4 *) &u4Events) == OSIX_SUCCESS)
        {
	
            /* Acquire the lock before processing every event */
            BiteApiLock ();
            /*timer event */
            if (u4Events & BITE_TMR_EXP_EVENT)
            {
                BiteTmrExpHandler ();
            }
            /*queue event */
            if (u4Events & BITE_CONF_MSG_ENQ_EVENT)
            {
                BiteQueMsgHandler ();
            }
            /* Event posted to the BITE main task till the  IBIT test stops */
            if (u4Events & BITE_IBIT_TEST_EVT)
            {
                BitetestStartIbitTest ();
            }
            /* Release the lock after processing every event */
            BiteApiUnLock ();
        }
    }                            /*end of while */
}

/*****************************************************************************/
/* Function                  : BiteMainInit                                   */
/*                                                                           */
/* Description               : This routine takes care of initializing the   */
/*                             BITE module data structures and variables. This*/
/*                             routine creates the task queue, semaphores and*/
/*                             allocates Mempools.                           */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : gBiteGlobalInfo.BiteMsgQueId, 
                               gBiteGlobalInfo.BiteTaskId                    */
/*                             gBiteGlobalInfo.BiteMemPoolId,                */
/*                             gBiteGlobalInfo.BiteSemId                     */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
INT4
BiteMainInit (VOID)
{
    if (OsixSemCrt (BITE_SEM_NAME, &(gBiteGlobalInfo.BiteSemId)) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    /* Semaphore is by default created with initial value 0. So GiveSem
     * is called before using the semaphore. */
    OsixSemGive (gBiteGlobalInfo.BiteSemId);
    /* Queue created for storing messages received from CFA and MSR into 
       bite queue */
    if (OsixQueCrt (BITE_PDU_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    BITE_PDU_Q_DEPTH,
                    &(gBiteGlobalInfo.BiteMsgQueId)) == OSIX_FAILURE)
    {
        /* delete semaphore if queue creation failed */
        OsixSemDel (gBiteGlobalInfo.BiteSemId);
        return OSIX_FAILURE;
    }
    /* MEMPOOL created for allocating memory for the queue */
    if (MemCreateMemPool ((sizeof (tBiteQMsg)),
                          BITE_PDU_Q_DEPTH,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(gBiteGlobalInfo.BiteQueMemPoolId)) == MEM_FAILURE)
    {
        /*delete queue and semaphore if mempool creation for queue failed */
        OsixSemDel (gBiteGlobalInfo.BiteSemId);
        OsixQueDel (gBiteGlobalInfo.BiteMsgQueId);
        return OSIX_FAILURE;
    }
    /* MEMPOOL created for allocating memory for the interface structure */
    if (MemCreateMemPool (sizeof (tBiteIfInfo), gu4MaxIfcs,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &gBiteGlobalInfo.BiteMemIfPoolId) == MEM_FAILURE)
    {
        /* delate queue,semaphore and meory pool creation for queue
           if memory pool creation for interface structure failed */
        OsixSemDel (gBiteGlobalInfo.BiteSemId);
        OsixQueDel (gBiteGlobalInfo.BiteMsgQueId);
        MemDeleteMemPool (gBiteGlobalInfo.BiteQueMemPoolId);
        return OSIX_FAILURE;
    }
    /*initialize the timer */
    if (BiteTmrInit () == OSIX_FAILURE)
    {
        /* delete all created resources if timer initialization failed */
        OsixSemDel (gBiteGlobalInfo.BiteSemId);
        OsixQueDel (gBiteGlobalInfo.BiteMsgQueId);
        MemDeleteMemPool (gBiteGlobalInfo.BiteQueMemPoolId);
        MemDeleteMemPool (gBiteGlobalInfo.BiteMemIfPoolId);
    }
    /*Initialize all the global variables to default */
    BiteMainInitializeGlobInfo ();
    return OSIX_SUCCESS;

}

  /*Function for initialising all the variables in the global structure to 
     their default values */
VOID
BiteMainInitializeGlobInfo (VOID)
{
    gBiteGlobalInfo.u4OverallStatus = BITE_PASS;
    gBiteGlobalInfo.u4ContSelfTest = BITE_RESERVED;
    gBiteGlobalInfo.u4PowerOnSelfTest = BITE_RESERVED;
    gBiteGlobalInfo.u4FailureType = BITE_NONE;
    gBiteGlobalInfo.u4SelfTestUpTime = BITE_ZERO;
    gBiteGlobalInfo.u4SelfTestCommand = BITE_IBIT_TEST_OFF;
    gBiteGlobalInfo.u4ContSelfTestCmd = BITE_CBIT_TEST_OFF;
    gBiteGlobalInfo.u4LastResultSelfTest = BITE_PASS;
    gBiteGlobalInfo.u4SelfTestReturnCode = BITE_ZERO;
    gBiteGlobalInfo.u4ContSelfTestRetCode = BITE_ZERO;
    gBiteGlobalInfo.u4ThresholdIpInAddrErrors = BITE_THRESHOLD_DEFAULT_VALUE;
    gBiteGlobalInfo.u4ThresholdIpInHdrErrors = BITE_THRESHOLD_DEFAULT_VALUE;
    gBiteGlobalInfo.u4ThresholdIpInUnknownProtos = BITE_THRESHOLD_DEFAULT_VALUE;
    gBiteGlobalInfo.u4ThresholdIpInDiscards = BITE_THRESHOLD_DEFAULT_VALUE;
    gBiteGlobalInfo.u4ThresholdIpOutDiscards = BITE_THRESHOLD_DEFAULT_VALUE;
    gBiteGlobalInfo.u4ThresholdIpReasmFails = BITE_THRESHOLD_DEFAULT_VALUE;
    gBiteGlobalInfo.u4ThresholdIpFragFails = BITE_THRESHOLD_DEFAULT_VALUE;
    gBiteGlobalInfo.u4ThresholdIcmpInErrors = BITE_THRESHOLD_DEFAULT_VALUE;
    gBiteGlobalInfo.u4ThresholdIcmpOutErrors = BITE_THRESHOLD_DEFAULT_VALUE;
    gBiteGlobalInfo.u4ThresholdTcpInErrors = BITE_THRESHOLD_DEFAULT_VALUE;
    gBiteGlobalInfo.u4ThresholdUdpInErrors = BITE_THRESHOLD_DEFAULT_VALUE;
    gBiteGlobalInfo.u4IpInAdderrbaseval = BITE_ZERO;
    gBiteGlobalInfo.u4IpInHdrerrbaseval = BITE_ZERO;
    gBiteGlobalInfo.u4IpInUnknwnProbaseval = BITE_ZERO;
    gBiteGlobalInfo.u4IpInDisbaseval = BITE_ZERO;
    gBiteGlobalInfo.u4IpOutDisbaseval = BITE_ZERO;
    gBiteGlobalInfo.u4IpReasmFailbaseval = BITE_ZERO;
    gBiteGlobalInfo.u4IpFragFailbaseval = BITE_ZERO;
    gBiteGlobalInfo.u4IcmpInerrbaseval = BITE_ZERO;
    gBiteGlobalInfo.u4IcmpOuterrbaseval = BITE_ZERO;
    gBiteGlobalInfo.u4TcpInerrbaseval = BITE_ZERO;
    gBiteGlobalInfo.u4UdpInerrbaseval = BITE_ZERO;
    return;
}

/*****************************************************************************/
/* Function     : BiteMainDeInit                                              */
/*                                                                           */
/* Description  : This routine takes care of de-initializing the BITE module  */
/*                data structures and variables. This routine deletes the    */
/*                task queue, semaphores and de-allocates Mempools.          */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : gPtpGlobalInfo                                             */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/
INT4
BiteMainDeInit (VOID)
{
    /* deallocate all the resources during the system shutdown */
    if (MemDeleteMemPool (gBiteGlobalInfo.BiteQueMemPoolId) == MEM_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (MemDeleteMemPool (gBiteGlobalInfo.BiteMemIfPoolId) == MEM_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (gBiteGlobalInfo.BiteSemId != 0)
    {
        OsixSemDel (gBiteGlobalInfo.BiteSemId);

    }

    OsixQueDel (gBiteGlobalInfo.BiteMsgQueId);

    BiteTmrDeInit ();
    return OSIX_SUCCESS;
}

#endif /*_BITEINIT_C_ */
