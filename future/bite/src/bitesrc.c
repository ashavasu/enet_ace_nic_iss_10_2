#ifndef _BITESRC_C_
#define _BITESRC_C_
#include "biteinc.h"
/***************************************************************************
 * FUNCTION NAME             : BiteSrcScalars
 *
 * DESCRIPTION               : This function displays scalars
 *                             for show running comfiguration command
 *
 * INPUT                     :
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
INT4 BiteSrcScalars (tCliHandle CliHandle)
{
    INT4 i4SelfTestCommand;
    INT4 i4ContinuousSelfTestCommand;
    INT4 i4ThresholdIpInAddrErrors;
    INT4 i4ThresholdIpInHdrErrors;
    INT4 i4ThresholdIpInUnknownProtos;
    INT4 i4ThresholdIpInDiscards;
    INT4 i4ThresholdIpOutDiscards;
    INT4 i4ThresholdIpReasmFails;
    INT4 i4ThresholdIpFragFails;
    INT4 i4ThresholdIcmpInErrors;
    INT4 i4ThresholdIcmpOutErrors;
    INT4 i4ThresholdTcpInErrors;
    INT4 i4ThresholdUdpInErrors;
    INT4 i4ThresholdCounterReset;

    nmhGetLruSelfTestCommand(&i4SelfTestCommand);
    if (i4SelfTestCommand != SELFTEST_IN_PRGS)
    {
        CliPrintf (CliHandle, "diagnostic test non-disruptive\r\n");
    }

    nmhGetLruContinuousSelfTestCommand(&i4ContinuousSelfTestCommand);
    if (i4ContinuousSelfTestCommand != CONTSELFTEST_ON)
    {
        CliPrintf (CliHandle, " no diagnostic test continuous\r\n");
    }

    nmhGetLruThresholdIpInAddrErrors(&i4ThresholdIpInAddrErrors);
    if (i4ThresholdIpInAddrErrors != BITE_THRESHOLD_DEFAULT_VALUE)
    {
        if (i4ThresholdIpInAddrErrors != BITE_ZERO)
        {
            CliPrintf (CliHandle, "set threshold ipInAddrErrors %d \r\n",
                                       i4ThresholdIpInAddrErrors);
        }
        else
        {
             CliPrintf (CliHandle, " no set threshold ipInAddrErrors %d \r\n",
                                       i4ThresholdIpInAddrErrors);
        }
    }

    nmhGetLruThresholdIpInHdrErrors(&i4ThresholdIpInHdrErrors);
    if (i4ThresholdIpInHdrErrors != BITE_THRESHOLD_DEFAULT_VALUE)
    {
        if (i4ThresholdIpInHdrErrors != BITE_ZERO)
        {    
            CliPrintf (CliHandle, "set threshold ipInHdrErrors %d \r\n",
                                        i4ThresholdIpInHdrErrors);
        }
        else
        {
             CliPrintf (CliHandle, "no set threshold ipInHdrErrors %d \r\n",
                                        i4ThresholdIpInHdrErrors);
        }
    }

    nmhGetLruThresholdIpInUnknownProtos( &i4ThresholdIpInUnknownProtos);
    if (i4ThresholdIpInUnknownProtos != BITE_THRESHOLD_DEFAULT_VALUE)
    {
        if (i4ThresholdIpInUnknownProtos != BITE_ZERO)
        {
            CliPrintf (CliHandle, "set threshold ipInUnknownProtos %d \r\n",
                                       i4ThresholdIpInUnknownProtos);
        } 
        else
        {
             CliPrintf (CliHandle, "no set threshold ipInUnknownProtos %d \r\n",
                                       i4ThresholdIpInUnknownProtos);
        }  
    }
    nmhGetLruThresholdIpInDiscards( &i4ThresholdIpInDiscards);
    if (i4ThresholdIpInDiscards != BITE_THRESHOLD_DEFAULT_VALUE)
    {
        if (i4ThresholdIpInDiscards != BITE_ZERO)
        {
            CliPrintf (CliHandle, "set threshold ipInDiscards %d \r\n",
                                       i4ThresholdIpInDiscards);
        }
        else
        {
            CliPrintf (CliHandle, " no set threshold ipInDiscards %d \r\n",
                                       i4ThresholdIpInDiscards);
        }
    }
    nmhGetLruThresholdIpOutDiscards( &i4ThresholdIpOutDiscards);
    if (i4ThresholdIpOutDiscards != BITE_THRESHOLD_DEFAULT_VALUE)
    {
        if (i4ThresholdIpOutDiscards != BITE_ZERO)
        {
            CliPrintf (CliHandle, "set threshold ipInDiscards %d \r\n",
                                       i4ThresholdIpInDiscards);
        }
        else
        {
             CliPrintf (CliHandle, "no set threshold ipInDiscards %d \r\n",
                                       i4ThresholdIpInDiscards);
        }
    }

    nmhGetLruThresholdIpReasmFails( &i4ThresholdIpReasmFails);
    if (i4ThresholdIpReasmFails != BITE_THRESHOLD_DEFAULT_VALUE)
    {
        if (i4ThresholdIpReasmFails != BITE_ZERO)
        {
            CliPrintf (CliHandle, "set threshold ipReasmFails %d \r\n",
                                       i4ThresholdIpReasmFails);
        }
        else
        {
            CliPrintf (CliHandle, "no set threshold ipReasmFails %d \r\n",
                                       i4ThresholdIpReasmFails);
        }
    }
    nmhGetLruThresholdIpFragFails( &i4ThresholdIpFragFails);
    if (i4ThresholdIpFragFails != BITE_THRESHOLD_DEFAULT_VALUE)
    {
        if (i4ThresholdIpFragFails != BITE_ZERO)
        {
            CliPrintf (CliHandle, "set threshold ipFragFails %d \r\n",
                                       i4ThresholdIpFragFails);
        }
        else
        {
            CliPrintf (CliHandle, "no set threshold ipFragFails %d \r\n",
                                       i4ThresholdIpFragFails);
        }
    }
    nmhGetLruThresholdIcmpInErrors( &i4ThresholdIcmpInErrors);
    if (i4ThresholdIcmpInErrors != BITE_THRESHOLD_DEFAULT_VALUE)
    {
        if (i4ThresholdIcmpInErrors != BITE_ZERO)
        {
            CliPrintf (CliHandle, "set threshold icmpInErrors %d \r\n",
                                       i4ThresholdIcmpInErrors);
        }
        else
        {
             CliPrintf (CliHandle, "no set threshold icmpInErrors %d \r\n",
                                       i4ThresholdIcmpInErrors);
        }
    }
    nmhGetLruThresholdIcmpOutErrors( &i4ThresholdIcmpOutErrors);
    if (i4ThresholdIcmpOutErrors != BITE_THRESHOLD_DEFAULT_VALUE)
    {
        if (i4ThresholdIcmpOutErrors != BITE_ZERO)
        {
            CliPrintf (CliHandle, "set threshold icmpOutErrors %d \r\n",
                                       i4ThresholdIcmpOutErrors);
        }
        else
        {
            CliPrintf (CliHandle, "no set threshold icmpOutErrors %d \r\n",
                                       i4ThresholdIcmpOutErrors);
        }
    }
    nmhGetLruThresholdTcpInErrors( &i4ThresholdTcpInErrors);
    if ( i4ThresholdTcpInErrors != BITE_THRESHOLD_DEFAULT_VALUE)
    {
        if (i4ThresholdTcpInErrors != BITE_ZERO)
        {
            CliPrintf (CliHandle, "set threshold tcpInErrors %d \r\n",
                                       i4ThresholdTcpInErrors);
        }
        else
        {
            CliPrintf (CliHandle, "no set threshold tcpInErrors %d \r\n",
                                       i4ThresholdTcpInErrors);
        }
    }
    nmhGetLruThresholdUdpInErrors( &i4ThresholdUdpInErrors);
    if (i4ThresholdUdpInErrors != BITE_THRESHOLD_DEFAULT_VALUE)
    {
        CliPrintf (CliHandle, "set threshold udpInErrors %d \r\n",
                                       i4ThresholdUdpInErrors);
    }

    nmhGetLruThresholdCounterReset( &i4ThresholdCounterReset);
    if (i4ThresholdCounterReset != BITE_RESET_OFF)
    {
            
        CliPrintf (CliHandle, "clear diagnostic test counters  %d \r\n",
                                       i4ThresholdCounterReset);
    }    
    return CLI_SUCCESS;   
}
/***************************************************************************
* FUNCTION NAME             : BiteSrcInterfaces
 *
 * DESCRIPTION               : This function displays interface objects
 *                             for show running comfiguration command
 *
 * INPUT                     :
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLII_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
VOID BiteSrcInterfaces(tCliHandle CliHandle)
{
    INT4 i4RetVal;
    INT4 i4FirstIfIndex;
    INT4 i4NextIfIndex;
    INT4 i4IfThresholdOperStatus;
    INT4 i4IfThresholdInDiscards;
    INT4 i4IfThresholdInErrors;
    INT4 i4IfThresholdInUnknownProtos;
    INT4 i4IfThresholdOutDiscards;
    INT4 i4IfThresholdOutErrors;
    INT4 i4IfRowStatus;
    i4RetVal = nmhGetFirstIndexLruIfTable(&i4FirstIfIndex);
    if (i4RetVal == SNMP_FAILURE)
    {
        return;
    }
    i4NextIfIndex = i4FirstIfIndex;

    do
    {
        nmhGetLruIfRowStatus(i4FirstIfIndex ,&i4IfRowStatus);
        if (i4IfRowStatus == DESTROY)
        {
            CliPrintf (CliHandle, "no monitor interface  %d\r\n",
                       i4FirstIfIndex);
        }

        nmhGetLruIfThresholdOperStatus(i4FirstIfIndex,
                                         &i4IfThresholdOperStatus);
        if (i4IfThresholdOperStatus != BITE_THRESHOLD_DEFAULT_VALUE)
        {
            if (i4IfThresholdOperStatus != BITE_ZERO)
            {
                CliPrintf (CliHandle, "set threshold IfOperStatus %d  %d\r\n",
                       i4FirstIfIndex,i4IfThresholdOperStatus);
            }
            else
            {
                CliPrintf (CliHandle, "no set threshold IfOperStatus %d  %d\r\n",
                       i4FirstIfIndex,i4IfThresholdOperStatus);
            }
        }
        nmhGetLruIfThresholdInDiscards(i4FirstIfIndex,
                                        &i4IfThresholdInDiscards);
        if (i4IfThresholdInDiscards != BITE_THRESHOLD_DEFAULT_VALUE)
        {
            if (i4IfThresholdInDiscards != BITE_ZERO)
            {
                CliPrintf (CliHandle, "set threshold IfInDiscards %d %d\r\n",
                       i4FirstIfIndex,i4IfThresholdInDiscards);
            }
            else
            {
                CliPrintf (CliHandle, "no set threshold IfInDiscards %d %d\r\n",
                       i4FirstIfIndex,i4IfThresholdInDiscards);
            }
        }
        nmhGetLruIfThresholdInErrors(i4FirstIfIndex,&i4IfThresholdInErrors);
        if (i4IfThresholdInErrors != BITE_THRESHOLD_DEFAULT_VALUE)
        {
            if (i4IfThresholdInErrors != BITE_ZERO)
            {
                CliPrintf (CliHandle, "set threshold IfInErrors %d %d\r\n",
                               i4FirstIfIndex,i4IfThresholdInErrors);
            }
            else
            {
                 CliPrintf (CliHandle, "set threshold IfInErrors %d %d\r\n",
                               i4FirstIfIndex,i4IfThresholdInErrors);
            }
        }
        nmhGetLruIfThresholdInUnknownProtos(i4FirstIfIndex,
                                         &i4IfThresholdInUnknownProtos);
        if (i4IfThresholdInUnknownProtos != BITE_THRESHOLD_DEFAULT_VALUE)
        {
            if (i4IfThresholdInUnknownProtos != BITE_ZERO)
            {
                CliPrintf (CliHandle, "set threshold IfInUnknownProtos %d %d\r\n", 
                           i4FirstIfIndex, i4IfThresholdInUnknownProtos);
            }
            else
            {
                 CliPrintf (CliHandle, "no set threshold IfInUnknownProtos %d %d\r\n",
                            i4FirstIfIndex,i4IfThresholdInUnknownProtos);
            }
        }
        nmhGetLruIfThresholdOutDiscards(i4FirstIfIndex,
                                         &i4IfThresholdOutDiscards);
        if (i4IfThresholdOutDiscards != BITE_THRESHOLD_DEFAULT_VALUE)
        {
           if (i4IfThresholdOutDiscards != BITE_ZERO)
           {
               CliPrintf (CliHandle, "set threshold IfOutDiscards %d %d\r\n",
                           i4FirstIfIndex,i4IfThresholdOutDiscards);
           }
           else
           {
                CliPrintf (CliHandle, "no set threshold IfOutDiscards %d %d\r\n",
                           i4FirstIfIndex,i4IfThresholdOutDiscards);
           }
           nmhGetLruIfThresholdOutErrors(i4FirstIfIndex,&i4IfThresholdOutErrors);
           if (i4IfThresholdOutErrors != BITE_THRESHOLD_DEFAULT_VALUE)
           {
               if (i4IfThresholdOutErrors != BITE_ZERO)
               {
                   CliPrintf (CliHandle, "set threshold IfOutErrors %d %d\r\n",
                              i4FirstIfIndex,i4IfThresholdOutErrors);
               }
               else
               {
                   CliPrintf (CliHandle, "no set threshold IfOutErrors %d %d\r\n",
                              i4FirstIfIndex,i4IfThresholdOutErrors);
               }
           }
         }
    }
    while(nmhGetNextIndexLruIfTable(i4FirstIfIndex ,&i4NextIfIndex ) == 
                                                    SNMP_SUCCESS);
}
#endif
