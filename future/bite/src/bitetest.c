/********************************************************************
 * * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 * *
 * * $Id: bitetest.c,v 1.1.1.1 2011/07/21 14:06:33 siva Exp $
 * *
 * * Description: This file contains functions related to BITE core
 * *********************************************************************/

#ifndef _BITETEST_C_
#define _BITETEST_C_
#include "biteinc.h"

extern void         bcmTestRun ();
extern VOID         IpDeleteAllLogicalInterfaces (VOID);
extern INT4         CliOpenFile (CONST CHR1 * pi1Filename, INT4 i4Flag);
extern INT1         CliGetLine (INT4, INT1 *);
extern INT2         CliCloseFile (INT2 i2FileFd);
FILE               *gLogFile = NULL;
extern UINT1        gau1OperPrevState[200];
extern UINT1        gau1OperCurrState[200];
extern UINT1        gau1InDisPrevState[200];
extern UINT1        gau1InDisCurrState[200];
extern UINT1        gau1InErrPrevState[200];
extern UINT1        gau1InErrCurrState[200];
extern UINT1        gau1InUnKnwPrevState[200];
extern UINT1        gau1InUnKnwCurrState[200];
extern UINT1        gau1OutDisPrevState[200];
extern UINT1        gau1OutDisCurrState[200];
extern UINT1        gau1OutErrPrevState[200];
extern UINT1        gau1OutErrCurrState[200];
extern UINT4        gu4IpCurrState;
extern UINT4        gu4IcmpCurrState;
extern UINT4        gu4TcpCurrState;
extern UINT4        gu4UdpCurrState;
/*****************************************************************************/
/* Function                  : BitetestProcessMsrStatus                      */
/*                                                                           */
/* Description               : This function process the configuration 
                               restore status message posted in the 
                               BITE queue from the MSR module.               */
/*                                                                           */
/* Input                     : u1Status - status of configuration restore    */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/

VOID
BitetestProcessMsrStatus (UINT4 u4Status)
{
    /* If the status from the MSR is configuration restore failure,then update 
       the overall status as failed,failure type as software failure and failure 
       reason as config restore failure */
    if ((u4Status != BITE_LAST_MIB_RESTORE_SUCCESSFUL)
        && (u4Status != BITE_MIB_RESTORE_NOT_INITIATED))
    {
        gBiteGlobalInfo.u4FailureType = BITE_SOFTWARE_FAILURE;
        gBiteGlobalInfo.u4OverallStatus = BITE_FAILED;
        gBiteGlobalInfo.u4FailureReason = CONFIG_RESTORE_FAILURE;
        BitetestLogFile (BITE_CONFIG_RESTORE, gBiteGlobalInfo.u4FailureReason);
    }
    return;
}

/*****************************************************************************/
/* Function                  : BitetestStartIbitTest                         */
/*                                                                           */
/* Description               : This function used to initiate the IBIT test.
                               The function calls BitePortGetTestEntry to get 
                               the test entry for the following tests 
                                   Pci test 
                                   Pci S-channel 
                                   Cpu Bench mark 
                                                                             */
/*                                                                           */
/* Input                     : None                                          */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/

INT4
BitetestStartIbitTest (VOID)
{
    UINT1               key[3];
    UINT4               u4CurrentTime = 0;
    INT4                i4ScriptFd = 0;
    UINT1               au1ScriptName[BITE_TESTNAME_LEN];
    UINT1               au1ScriptFile[BITE_TESTNAME_LEN + 20];
    UINT1               au1TempStr[BITE_MAX_LINE_LEN];
    UINT1               au1Ibittest[BITE_TESTNAME_LEN];
    INT1                i1RetVal = 0;
    UINT2               u2Index1 = 0;
    UINT2               u2Index2 = 0;
    test_t             *test = NULL;
    STRCPY (au1Ibittest, "ibit");
    SPRINTF ((CHR1 *) key, "%d", 2);
    MEMSET (key, 0, sizeof (key));
    MEMSET (au1ScriptName, 0, BITE_TESTNAME_LEN);
    /* initiated Built in Test is run from the ibit.soc script file present
       in the flash:scripts path */
    for (u2Index1 = 0; u2Index1 < STRLEN (au1Ibittest); u2Index1++)
    {
        if (au1Ibittest[u2Index1] == '\0')
        {
            MEMSET (au1TempStr, 0, BITE_MAX_LINE_LEN);
            MEMSET (au1ScriptFile, 0, BITE_TESTNAME_LEN + 20);
            STRCPY (au1ScriptFile, BITE_SCRIPT_DIR);
            STRCAT (au1ScriptFile, au1ScriptName);
            STRCAT (au1ScriptFile, ".soc");
            /* If unable to open the ibit.soc file return failure  */
            if ((i4ScriptFd =
                 CliOpenFile ((CHR1 *) au1ScriptFile, BITE_RDONLY)) < 0)
            {
                return OSIX_FAILURE;

            }
            /* If the script file is empty return failure */
            i1RetVal = CliGetLine (i4ScriptFd, (INT1 *) au1TempStr);
            if (i1RetVal == BITE_EOF)
            {
                return OSIX_FAILURE;
            }
            /* If the script file format is unsupported return failure */
            if (STRNCMP (au1TempStr, "#tl ", 4))
            {
                return OSIX_FAILURE;
            }
            /* If Error in closing the file return failure */
            if (CliCloseFile (i4ScriptFd) == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            MEMSET (au1ScriptName, 0, BITE_TESTNAME_LEN);
            u2Index2 = 0;
        }
        else
        {
            au1ScriptName[u2Index2] = au1Ibittest[u2Index1];
            u2Index2++;
        }
    }

    STRCPY (gFsTestsToRun, au1Ibittest);

    /* this must be done for running loopback tests also */
    /* deletion of LogFileical interfaces is taken from  */
    /* declassification routine                          */
    if ((STRCMP (gFsTestsToRun, "runbist") == 0) ||
        (STRCMP (gFsTestsToRun, "phydevice") == 0) ||
        (STRCMP (gFsTestsToRun, "mac") == 0) ||
        (STRCMP (gFsTestsToRun, "cpu") == 0))
    {
        IpDeleteAllLogicalInterfaces ();
    }
    /* call the test_find() function in bcm for running the pci test
       and get the updated test structure */
    test = BitePortGetTestEntry ((char *) key);
    gBiteGlobalInfo.i4PciRunCount = test->t_runs;
    gBiteGlobalInfo.i4PciFailCount = test->t_fail;
    gBiteGlobalInfo.i4PciSuccessCount = test->t_success;
    /* call the test_find() function in bcm for running the s-channel test
       and get the updated test structure */
    SPRINTF ((CHR1 *) key, "%d", 4);
    test = BitePortGetTestEntry ((char *) key);
    gBiteGlobalInfo.i4SchRunCount = test->t_runs;
    gBiteGlobalInfo.i4SchFailCount = test->t_fail;
    gBiteGlobalInfo.i4SchSuccessCount = test->t_success;
    /* call the test_find() function in bcm for running the cpu benchmark 
       test and get the updated test structure */
    SPRINTF ((CHR1 *) key, "%d", 21);
    test = BitePortGetTestEntry ((char *) key);
    gBiteGlobalInfo.i4CpuRunCount = test->t_runs;
    gBiteGlobalInfo.i4CpuFailCount = test->t_fail;
    gBiteGlobalInfo.i4CpuSuccessCount = test->t_success;

    /* Get the current time in secs */
    /* This is Updating the self test uptime(time since the IBIT test was 
       activated) */
    OsixGetSysTime (&u4CurrentTime);
    u4CurrentTime = (u4CurrentTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
    gBiteGlobalInfo.u4SelfTestUpTime = u4CurrentTime;
    gBiteGlobalInfo.u4SelfTestCommand = BITE_IBIT_TEST_ON;
    gBiteGlobalInfo.u4LastResultSelfTest = BITE_RESERVED;
    gBiteGlobalInfo.u4SelfTestReturnCode = BITE_ZERO;
    /* call bcmtestrun() in bcm sdk for running the IBIT test */

    gBiteGlobalInfo.u4SelfTestCommand = BITE_IBIT_TEST_INPRGS;

    bcmTestRun ();
    /* Update the IBIT test status as PASS/FAILED after running the IBIT test */
    BitetestUpdateIbitStatus ();
    gBiteGlobalInfo.u4SelfTestCommand = BITE_IBIT_TEST_OFF;
    return OSIX_SUCCESS;

}

/*****************************************************************************/
/* Function                  :BitetestUpdatePbitStatus                       */
/*                                                                           */
/* Description               : This function used to update the status of    */
/*                               PBIT test                                   */
/*                             The function calls BitePortGetTestEntry to get*/
/*                               the test entry for the following tests      */
/*                                   Pci test                                */
/*                                   Pci S-channel                           */
/*                                   Counter read/write                      */
/*                                   counter width                           */
/*                                   Mac loopback                            */
/* Input                     : None                                          */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/

VOID
BitetestUpdatePbitStatus (VOID)
{
    UINT1               key[5];
    UINT1               u1Flag = FALSE;
    UINT4               u4ErrCode = 0;
    test_t             *test;
    MEMSET (key, 0, sizeof (key));
    SPRINTF ((CHR1 *) key, "%d", 2);
    /* call the test_find() function in bcm for running the pci test
       and get the updated test structure */
    test = BitePortGetTestEntry ((char *) key);
	/*test->t_fail = 1;*/
    if ((test == NULL) || ((test->t_runs != 0) && (test->t_fail > 0)))
    {
        u1Flag = TRUE;
        u4ErrCode |= PCI_TEST_FAIL;
    }
    SPRINTF ((CHR1 *) key, "%d", 4);
    /* call the test_find() function in bcm for running the s-channel test
       and get the updated test structure */
    test = BitePortGetTestEntry ((char *) key);
	/*test->t_fail = 1;*/
    if ((test == NULL) || ((test->t_runs != 0) && (test->t_fail > 0)))
    {
        u1Flag = TRUE;
        u4ErrCode |= SCHANNEL_TEST_FAIL;
    }

    SPRINTF ((CHR1 *) key, "%d", 30);
    /* call the test_find() function in bcm for running the counter RW test
       and get the updated test structure */
    test = BitePortGetTestEntry ((char *) key);
	/*test->t_fail = 1;*/
    if ((test == NULL) || ((test->t_runs != 0) && (test->t_fail > 0)))
    {
        u1Flag = TRUE;
        u4ErrCode |= COUNTER_RW_TEST_FAIL;
    }

    SPRINTF ((CHR1 *) key, "%d", 31);
    /* call the test_find() function in bcm for running the counter width test
       and get the updated test structure */
    test = BitePortGetTestEntry ((char *) key);
	/*test->t_fail = 1;*/
    if ((test == NULL) || ((test->t_runs != 0) && (test->t_fail > 0)))
    {
        u1Flag = TRUE;
        u4ErrCode |= COUNTER_WIDTH_TEST_FAIL;
    }

    SPRINTF ((CHR1 *) key, "%d", 19);
    /* call the test_find() function in bcm for running the phy device test
       and get the updated test structure */
    test = BitePortGetTestEntry ((char *) key);
	/*test->t_fail = 56;*/
	/*test->t_runs = 2;*/
    if ((test == NULL) || ((test->t_runs != 0) && (test->t_fail > 0)))
    {
        u1Flag = TRUE;
        u4ErrCode |= PHY_LOOPBACK_TEST_FAIL;
    }

    gBiteGlobalInfo.u4PowerOnSelfTest = BITE_PASS;
    /* If any of the PBIT tests failed,update the overall status as failed,
       power on self test as failed,failure type as hardware with failure
       reason */
    if (u1Flag == TRUE)
    {
        gBiteGlobalInfo.u4FailureType = BITE_HARDWARE_FAILURE;
        gBiteGlobalInfo.u4OverallStatus = BITE_FAILED;
        gBiteGlobalInfo.u4FailureReason = u4ErrCode;
        gBiteGlobalInfo.u4PowerOnSelfTest = BITE_FAILED;
        /* Log the tests failed in bite.log File */
        BitetestLogFile (BITE_PBIT_TEST, u4ErrCode);
    }
    return;
}

/*****************************************************************************/
/* Function                  : BitetestStartCbitTest                         */
/*                                                                           */
/* Description               : This function initiates the CBIT test
                               for monitoring the IP and interface counters  */
/*                                                                           */
/* Input                     : None                                          */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/

VOID
BitetestStartCbitTest (VOID)
{
    /* This starts the monitoring of counter statistics and
       set the CBIT command as INPROGRESS */

    gBiteGlobalInfo.u4ContSelfTest = BITE_PASS;
    gBiteGlobalInfo.u4ContSelfTestCmd = BITE_IBIT_TEST_ON;

    BitetestMonitorCounterStatistics (TRUE);

    gBiteGlobalInfo.u4ContSelfTestCmd = BITE_CBIT_TEST_INPRGS;

    return;
}

/*****************************************************************************/
/* Function                  : BitetestStopCbitTest                         */
/*                                                                           */
/* Description               : This function stops the CBIT test
                               from monitoring the IP and interface counters  */
/*                                                                           */
/* Input                     : None                                          */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/

VOID
BitetestStopCbitTest (VOID)
{
    UINT4               u4IfNum;
    tBiteIfInfo        *pBiteIfInfo = NULL;
    /* This stops the monitoring of all the counters */
    for (u4IfNum = 1; u4IfNum <= gu4MaxIfcs; u4IfNum++)
    {
        pBiteIfInfo = BiteIfFind (u4IfNum);
        if (pBiteIfInfo == NULL)
        {
            continue;
        }
        /* Initialize the interface variables to their default values */
        BiteIfInitialize (pBiteIfInfo, u4IfNum);
    }
    /* stop the one second timer running if the CBIT is stopped */
    BiteTmrStopTmr ();
    gBiteGlobalInfo.u4ContSelfTestCmd = BITE_CBIT_TEST_OFF;
    return;
}

/*****************************************************************************/
/* Function                  : BitetestMonitorCounterStatistics              */
/*                                                                           */
/* Description               : This function does the monitoring of counters
                               of the expiry of one second timer             */
/*                                                                           */
/* Input                     : None                                          */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/

VOID
BitetestMonitorCounterStatistics (UINT1 u1BaseFlag)
{

    UINT4               u4RetValue = 0;
    UINT1               u1Flag = FALSE;
    UINT1               u1OverallFlag = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4RetVal = 0;
    UINT4               u4IfNum;
    tBiteIfInfo        *pBiteIfInfo;
    /* Get the values for all the IP counters from IP */
    /* Get the value for IpInHdrErrors from IP */

    if (BitePortGetStatisticsCounter (BITE_IP_IN_HDR_ERRORS, 0, &u4RetVal) !=
        OSIX_FAILURE)
    {
        /* Initially set the base value to the value returned for 
           IpInHdrErrors from IP */
        if (u1BaseFlag == TRUE)
        {
            gBiteGlobalInfo.u4IpInHdrerrbaseval = u4RetVal;
        }
        if (gBiteGlobalInfo.u4ThresholdIpInHdrErrors != 0)
        {
            /* check for if the value exceeded the threshold,then update the
               error code as IpInHdrErrors fail */
            if (((u4RetVal) - (gBiteGlobalInfo.u4IpInHdrerrbaseval)) >
                (gBiteGlobalInfo.u4ThresholdIpInHdrErrors))
            {
                u1Flag = TRUE;
                gBiteGlobalInfo.u4IpStatsErrCode |= IP_IN_HDR_ERR_FAIL;
            }
        }
    }
    /* Get the value for IpInAddrErrors from IP */
    if (BitePortGetStatisticsCounter (BITE_IP_IN_ADDR_ERRORS, 0, &u4RetVal) !=
        OSIX_FAILURE)
    {                            /* Initially set the base value to the value returned for 
                                   IpInAddrErrors from IP */
        if (u1BaseFlag == TRUE)
        {

            gBiteGlobalInfo.u4IpInAdderrbaseval = u4RetVal;
        }

        if (gBiteGlobalInfo.u4ThresholdIpInAddrErrors != 0)
        {
            /* check for if the value exceeded the threshold,then update the
               error code as IpInAddrErrors fail */
            if (((u4RetVal) - (gBiteGlobalInfo.u4IpInAdderrbaseval)) >
                (gBiteGlobalInfo.u4ThresholdIpInAddrErrors))
            {
                gBiteGlobalInfo.u4IpStatsErrCode |= IP_IN_ADDR_FAIL;
                u1Flag = TRUE;
            }
        }
    }
    /* Get the value for IpInunknownProtos from IP */
    if (BitePortGetStatisticsCounter (BITE_IP_IN_UNKNOWN_PROTOS, 0, &u4RetVal)
        != OSIX_FAILURE)
    {

        /* Initially set the base value to the value
         * returned for IpInUnknownProtos from IP */

        if (u1BaseFlag == TRUE)
        {
            gBiteGlobalInfo.u4IpInUnknwnProbaseval = u4RetVal;
        }
        if (gBiteGlobalInfo.u4ThresholdIpInUnknownProtos != 0)
        {
            /* check for if the value exceeded the threshold,
             * then update the error code as IpInunknownProtos fail */

            if (((u4RetVal) - (gBiteGlobalInfo.u4IpInUnknwnProbaseval)) >
                (gBiteGlobalInfo.u4ThresholdIpInUnknownProtos))
            {
                u1Flag = TRUE;
                gBiteGlobalInfo.u4IpStatsErrCode |= IP_IN_UNKNOWN_PROTO_FAIL;
            }
        }
    }
    /* Get the value for IpInDiscards from IP */
    if (BitePortGetStatisticsCounter (BITE_IP_IN_DISCARDS, 0, &u4RetVal) !=
        OSIX_FAILURE)
    {                            /* Initially set the base value to the value returned for 
                                   IpInDiscards from IP */
        if (u1BaseFlag == TRUE)
        {
            gBiteGlobalInfo.u4IpInDisbaseval = u4RetVal;
        }
        if (gBiteGlobalInfo.u4ThresholdIpInDiscards != 0)
        {                        /* check for if the value exceeded the threshold,then update the
                                   error code as IpInDiscards fail */
            if (((u4RetVal) - (gBiteGlobalInfo.u4IpInDisbaseval)) >
                (gBiteGlobalInfo.u4ThresholdIpInDiscards))
            {
                u1Flag = TRUE;
                gBiteGlobalInfo.u4IpStatsErrCode |= IP_IN_DISCARDS_FAIL;
            }
        }
    }
    /* Get the value for IpOutDiscards from IP */
    if (BitePortGetStatisticsCounter (BITE_IP_OUT_DISCARDS, 0, &u4RetVal) !=
        OSIX_FAILURE)
    {                            /* Initially set the base value to the value returned for 
                                   IpOutdiscards from IP */
        if (u1BaseFlag == TRUE)
        {
            gBiteGlobalInfo.u4IpOutDisbaseval = u4RetVal;
        }
        if (gBiteGlobalInfo.u4ThresholdIpOutDiscards != 0)
        {                        /* check for if the value exceeded the threshold,then update the
                                   error code as IpOutDiscards fail */
            if (((u4RetVal) - (gBiteGlobalInfo.u4IpOutDisbaseval)) >
                (gBiteGlobalInfo.u4ThresholdIpOutDiscards))
            {
                u1Flag = TRUE;
                gBiteGlobalInfo.u4IpStatsErrCode |= IP_OUT_DISCARDS_FAIL;
            }
        }
    }
    /* Get the value for IpReasmFails from IP */
    if (BitePortGetStatisticsCounter (BITE_IP_REASM_FAILS, 0, &u4RetVal) !=
        OSIX_FAILURE)
    {                            /* Initially set the base value to the value returned for 
                                   IpReasmFails from IP */
        if (u1BaseFlag == TRUE)
        {
            gBiteGlobalInfo.u4IpReasmFailbaseval = u4RetVal;
        }
        if (gBiteGlobalInfo.u4ThresholdIpReasmFails != 0)
        {                        /* check for if the value exceeded the threshold,then update the
                                   error code as IpReasmFails fail */
            if (((u4RetVal) - (gBiteGlobalInfo.u4IpReasmFailbaseval)) >
                (gBiteGlobalInfo.u4ThresholdIpReasmFails))
            {
                u1Flag = TRUE;
                gBiteGlobalInfo.u4IpStatsErrCode |= IP_REASM_FAIL;
            }
        }
    }
    /* Get the value for IpFragFails from IP */
    if (BitePortGetStatisticsCounter (BITE_IP_FRAG_FAILS, 0, &u4RetVal) !=
        OSIX_FAILURE)
    {                            /* Initially set the base value to the value returned for 
                                   IpFragFails from IP */
        if (u1BaseFlag == TRUE)
        {
            gBiteGlobalInfo.u4IpFragFailbaseval = u4RetVal;
        }
        if (gBiteGlobalInfo.u4ThresholdIpFragFails != 0)
        {                        /* check for if the value exceeded the threshold,then update the
                                   error code as IpFragFails fail */
            if (((u4RetVal) - (gBiteGlobalInfo.u4IpFragFailbaseval)) >
                (gBiteGlobalInfo.u4ThresholdIpFragFails))
            {
                u1Flag = TRUE;
                gBiteGlobalInfo.u4IpStatsErrCode |= IP_FRAG_FAIL;
            }
        }
    }

    if (u1Flag == TRUE)
    {
        u1OverallFlag = u1Flag;
        u4ErrCode = IP_STATISTICS_COUNTER_FAIL;
    }
    u1Flag = FALSE;
    /* Get the value for IcmpInErrors from IP */
    if (BitePortGetStatisticsCounter (BITE_ICMP_IN_ERRORS, 0, &u4RetVal) !=
        OSIX_FAILURE)
    {
        /* Initially set the base value to the value returned 
         * for IcmpInErrors from IP */
        if (u1BaseFlag == TRUE)
        {
            gBiteGlobalInfo.u4IcmpInerrbaseval = u4RetVal;
        }
        if (gBiteGlobalInfo.u4ThresholdIcmpInErrors != 0)
        {                        /* check for if the value exceeded the threshold,then update the
                                   error code as IcmpInErrors fail */
            if (((u4RetVal) - (gBiteGlobalInfo.u4IcmpInerrbaseval)) >
                (gBiteGlobalInfo.u4ThresholdIcmpInErrors))
            {
                u1Flag = TRUE;
                gBiteGlobalInfo.u4IcmpStatsErrCode |= ICMP_IN_ERR_FAIL;
            }
        }
    }
    /* Get the value for IcmpOutErrors from IP */
    if (BitePortGetStatisticsCounter (BITE_ICMP_OUT_ERRORS, 0, &u4RetVal) !=
        OSIX_FAILURE)
    {                            /* Initially set the base value to the value returned for 
                                   IcmpOutErrors from IP */
        if (u1BaseFlag == TRUE)
        {
            gBiteGlobalInfo.u4IcmpOuterrbaseval = u4RetVal;
        }
        if (gBiteGlobalInfo.u4ThresholdIcmpOutErrors != 0)
        {                        /* check for if the value exceeded the threshold,then update the
                                   error code as IcmpOutErrors fail */
            if (((u4RetVal) - (gBiteGlobalInfo.u4IcmpOuterrbaseval)) >
                (gBiteGlobalInfo.u4ThresholdIcmpOutErrors))
            {
                u1Flag = TRUE;
                gBiteGlobalInfo.u4IcmpStatsErrCode |= ICMP_OUT_ERR_FAIL;
            }
        }
    }
    if (u1Flag == TRUE)
    {
        u1OverallFlag = u1Flag;
        u4ErrCode = u4ErrCode | ICMP_STATISTICS_COUNTER_FAIL;
    }

    u1Flag = FALSE;
    /* Get the value for TcpInErrors from IP */
    if (BitePortGetStatisticsCounter (BITE_TCP_IN_ERRORS, 0, &u4RetVal) !=
        OSIX_FAILURE)
    {                            /* Initially set the base value to the value returned for 
                                   TcpInErrors from IP */
        if (u1BaseFlag == TRUE)
        {
            gBiteGlobalInfo.u4TcpInerrbaseval = u4RetVal;
        }
        if (gBiteGlobalInfo.u4ThresholdTcpInErrors != 0)
        {                        /* check for if the value exceeded the threshold,then update the
                                   error code as TcpInErrors fail */

            if (((u4RetVal) - (gBiteGlobalInfo.u4TcpInerrbaseval)) >
                (gBiteGlobalInfo.u4ThresholdTcpInErrors))
            {
                u1Flag = TRUE;
            }
        }
    }
    if (u1Flag == TRUE)
    {
        u1OverallFlag = u1Flag;
        u4ErrCode = u4ErrCode | TCP_STATISTICS_COUNTER_FAIL;
    }

    u1Flag = FALSE;
    /* Get the value for UdpInErrors from IP */
    if (BitePortGetStatisticsCounter (BITE_UDP_IN_ERRORS, 0, &u4RetVal) !=
        OSIX_FAILURE)
    {                            /* Initially set the base value to the value returned for 
                                   UdpInErrors from IP */
        if (u1BaseFlag == TRUE)
        {
            gBiteGlobalInfo.u4UdpInerrbaseval = u4RetVal;
        }
        if (gBiteGlobalInfo.u4ThresholdUdpInErrors != 0)
        {                        /* check for if the value exceeded the threshold,then update the
                                   error code as UdpInErrors fail */
            if (((u4RetVal) - (gBiteGlobalInfo.u4UdpInerrbaseval)) >
                (gBiteGlobalInfo.u4ThresholdUdpInErrors))
            {
                u1Flag = TRUE;
            }
        }
    }
    if (u1Flag == TRUE)
    {
        u1OverallFlag = u1Flag;
        u4ErrCode = u4ErrCode | UDP_STATISTICS_COUNTER_FAIL;
    }

    u1Flag = FALSE;

    for (u4IfNum = 1; u4IfNum <= gu4MaxIfcs; u4IfNum++)
    {
        pBiteIfInfo = BiteIfFind (u4IfNum);
        if (pBiteIfInfo == NULL)
        {
            continue;
        }
        if (((pBiteIfInfo->u4IfContSelfTestRetCode) &
             INTERFACE_OPER_STATUS_FAIL) != 0)
        {
            u1Flag = TRUE;
        }
        /* Get the value for UdpInErrors from IP */
        BitePortGetStatisticsCounter (BITE_IF_IN_DISCARDS, u4IfNum, &u4RetVal);
        /* Initially set the base value to the value returned 
         * for UdpInErrors from IP */
        if (u1BaseFlag == TRUE)
        {
            pBiteIfInfo->u4IfInDisbaseval = u4RetVal;
        }
        /* check for if the value exceeded the threshold,then update 
         * the error code as UdpInErrors fail */

        if (pBiteIfInfo->u4IfThresholdInDiscards != 0)
        {
            if ((u4RetVal - (pBiteIfInfo->u4IfInDisbaseval)) >
                (pBiteIfInfo->u4IfThresholdInDiscards))
            {
                u1Flag = TRUE;
                pBiteIfInfo->u4IfContSelfTestStatus = BITE_FAILED;
                pBiteIfInfo->u4IfContSelfTestRetCode |=
                    INTERFACE_IN_DISCARDS_FAIL;
            }
        }
        /* Get the value for UdpInErrors from IP */
        BitePortGetStatisticsCounter (BITE_IF_IN_ERRORS, u4IfNum, &u4RetVal);
        /* Initially set the base value to the value returned 
         * for UdpInErrors from IP */

        if (u1BaseFlag == TRUE)
        {
            pBiteIfInfo->u4IfInerrbaseval = u4RetVal;
        }
        if (pBiteIfInfo->u4IfThresholdInErrors != 0)
        {
            /* check for if the value exceeded the threshold,
             * then update the error code as UdpInErrors fail */

            if ((u4RetVal - (pBiteIfInfo->u4IfInerrbaseval)) >
                (pBiteIfInfo->u4IfThresholdInErrors))
            {

                u1Flag = TRUE;
                pBiteIfInfo->u4IfContSelfTestStatus = BITE_FAILED;
                pBiteIfInfo->u4IfContSelfTestRetCode |=
                    INTERFACE_IN_ERRORS_FAIL;
            }
        }
        /* Get the value for UdpInErrors from IP */
        BitePortGetStatisticsCounter (BITE_IF_IN_UNKNOWN_PROTOS,
                                      u4IfNum, &u4RetVal);
        /* Initially set the base value to the value returned
         *  for UdpInErrors from IP */
        if (u1BaseFlag == TRUE)
        {
            pBiteIfInfo->u4IfInUnknwnProtbaseval = u4RetVal;
        }
        if (pBiteIfInfo->u4IfThresholdInUnknownProtos != 0)
        {
            /* check for if the value exceeded the threshold,then update the
               error code as UdpInErrors fail */
            if ((u4RetVal - (pBiteIfInfo->u4IfInUnknwnProtbaseval)) >
                (pBiteIfInfo->u4IfThresholdInUnknownProtos))
            {
                u1Flag = TRUE;
                pBiteIfInfo->u4IfContSelfTestStatus = BITE_FAILED;
                pBiteIfInfo->u4IfContSelfTestRetCode |=
                    INTERFACE_IN_UNKNOWN_PROTOS_FAIL;
            }
        }
        /* Get the value for UdpInErrors from IP */
        BitePortGetStatisticsCounter (BITE_IF_OUT_DISCARDS, u4IfNum, &u4RetVal);
        /* Initially set the base value to the value returned 
         * for UdpInErrors from IP */
        if (u1BaseFlag == TRUE)
        {
            pBiteIfInfo->u4IfOutDisbaseval = u4RetVal;
        }
        if (pBiteIfInfo->u4IfThresholdOutDiscards != 0)
        {
            /* check for if the value exceeded the threshold,then update the
               error code as UdpInErrors fail */
            if ((u4RetVal - (pBiteIfInfo->u4IfOutDisbaseval)) >
                (pBiteIfInfo->u4IfThresholdOutDiscards))
            {
                u1Flag = TRUE;
                pBiteIfInfo->u4IfContSelfTestStatus = BITE_FAILED;
                pBiteIfInfo->u4IfContSelfTestRetCode |=
                    INTERFACE_OUT_DISCARDS_FAIL;
            }
        }
        /* Get the value for UdpInErrors from IP */
        BitePortGetStatisticsCounter (BITE_IF_OUT_ERRORS, u4IfNum, &u4RetVal);
        /* Initially set the base value to the value returned 
         * for UdpInErrors from IP */
        if (u1BaseFlag == TRUE)
        {
            pBiteIfInfo->u4IfOuterrbaseval = u4RetVal;
        }
        if (pBiteIfInfo->u4IfThresholdOutErrors != 0)
        {
            /* check for if the value exceeded the threshold,then update
             *  the error code as UdpInErrors fail */
            if ((u4RetVal - (pBiteIfInfo->u4IfOuterrbaseval)) >
                (pBiteIfInfo->u4IfThresholdOutErrors))
            {
                u1Flag = TRUE;
                pBiteIfInfo->u4IfContSelfTestStatus = BITE_FAILED;
                pBiteIfInfo->u4IfContSelfTestRetCode |=
                    INTERFACE_OUT_ERRORS_FAIL;
            }
        }

        if (u1Flag == TRUE)
        {
            u1OverallFlag = u1Flag;
            u4ErrCode = u4ErrCode | INTERFACE_STATISTICS_COUNTER_FAIL;
        }

    }
    /* Update Overall Status */
    if (u1OverallFlag == TRUE)
    {
        gBiteGlobalInfo.u4FailureType = BITE_HARDWARE_FAILURE;
        gBiteGlobalInfo.u4OverallStatus = BITE_FAILED;
        gBiteGlobalInfo.u4FailureReason = CBIT_TEST_FAIL;
        gBiteGlobalInfo.u4ContSelfTest = BITE_FAILED;
        gBiteGlobalInfo.u4ContSelfTestRetCode = u4ErrCode;
        BitetestLogFile (BITE_CBIT_TEST, u4ErrCode);
    }
    BiteTmrStartTmr ();
    return;
}

/*****************************************************************************/
/* Function                  : BitetestUpdateIbitStatus                      */
/*                                                                           */
/* Description               : This function will update the IBIT status 
                               based based on the fail count in test pu1Structure*/
/* 
                                                                             */
/*                                                                           */
/* Input                     : None                                          */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
INT4
BitetestUpdateIbitStatus (VOID)
{
    UINT1               key[3];
    test_t             *pcitest = NULL;
    test_t             *schtest = NULL;
    test_t             *cputest = NULL;
    UINT4               u4ErrCode = 0;
    UINT1               u1Flag = FALSE;
    MEMSET (key, 0, sizeof (key));
    SPRINTF ((CHR1 *) key, "%d", 2);
    pcitest = BitePortGetTestEntry ((char *) key);
    SPRINTF ((CHR1 *) key, "%d", 4);
    schtest = BitePortGetTestEntry ((char *) key);
    SPRINTF ((CHR1 *) key, "%d", 21);
    cputest = BitePortGetTestEntry ((char *) key);
    /* post event to the BITE main task till the Initiated Built in
       test gets completed */
    if (((gBiteGlobalInfo.i4PciRunCount + 1) != pcitest->t_runs) ||
        ((gBiteGlobalInfo.i4SchRunCount + 1) != schtest->t_runs) ||
        ((gBiteGlobalInfo.i4CpuRunCount + 1) != cputest->t_runs))
    {
        if (OsixEvtSend (gBiteGlobalInfo.BiteTaskId, BITE_IBIT_TEST_EVT) ==
            OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    if ((pcitest->t_runs != (pcitest->t_fail + pcitest->t_success)) ||
        (schtest->t_runs != (schtest->t_fail + schtest->t_success)) ||
        (cputest->t_runs != (cputest->t_fail + cputest->t_success)))
    {
        if (OsixEvtSend (gBiteGlobalInfo.BiteTaskId, BITE_IBIT_TEST_EVT) ==
            OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    /* IF the fail count in Pci is greater than Zero,then update the
       error code for IBIT as pci test fail */
	/*pcitest->t_fail = 1;*/
    if (pcitest->t_fail > gBiteGlobalInfo.i4PciFailCount)
    {
        u4ErrCode |= PCI_TEST_FAIL;
        u1Flag = TRUE;
    }
    /* IF the fail count in s-channel is greater than Zero,then update the
       error code for IBIT as s-channel test fail */
	/*schtest->t_fail = 1;*/
    if (schtest->t_fail > gBiteGlobalInfo.i4SchFailCount)
    {
        u4ErrCode |= SCHANNEL_TEST_FAIL;
        u1Flag = TRUE;
    }
    /* IF the fail count in cpu benchmark is greater than Zero,then update the
       error code for IBIT as cpu benchmark test fail */
	/*cputest->t_fail = 1;*/
    if (cputest->t_fail > gBiteGlobalInfo.i4CpuFailCount)
    {
        u4ErrCode |= CPU_BENCHMARK_TEST_FAIL;
        u1Flag = TRUE;
    }
    gBiteGlobalInfo.u4LastResultSelfTest = BITE_PASS;
    /* If any of the tests in IBIT failed,update the overall status as failed
       failure reason as IBIT,failure type as hardware */
    if (u1Flag == TRUE)
    {
        gBiteGlobalInfo.u4FailureType = BITE_HARDWARE_FAILURE;
        gBiteGlobalInfo.u4OverallStatus = BITE_FAILED;
        gBiteGlobalInfo.u4FailureReason = IBIT_TEST_FAIL;
        gBiteGlobalInfo.u4LastResultSelfTest = BITE_FAILED;
        gBiteGlobalInfo.u4SelfTestReturnCode = u4ErrCode;
        BitetestLogFile (BITE_IBIT_TEST, u4ErrCode);
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : BitetestResetThreshold                        */
/*                                                                           */
/* Description               : This function does the resetting of threshold */
/*                               value for a particular counter              */
/*                                                                           */
/* Input                     : u4BitMask - Bit mask for each threshold       */
/*                               u4OverallBitMask - Bit mask for all types   */
/*                               of counters                                 */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
VOID
BitetestResetThreshold (UINT4 u4OverallBitMask, UINT4 u4BitMask, UINT4 u4IfNum)
{
    tBiteIfInfo        *pBiteIfInfo = NULL;
    UINT4               u4RetVal = 0;
    UINT4               u4LocIfNum = 0;

    pBiteIfInfo = BiteIfFind (u4IfNum);

    if (u4OverallBitMask == IP_STATISTICS_COUNTER_FAIL)

    {
        switch (u4BitMask)
        {
            case IP_IN_HDR_ERR_FAIL:
                BitePortGetStatisticsCounter (BITE_IP_IN_HDR_ERRORS,
                                              0, &u4RetVal);
                gBiteGlobalInfo.u4IpInHdrerrbaseval = u4RetVal;
                break;
            case IP_IN_ADDR_FAIL:
                BitePortGetStatisticsCounter (BITE_IP_IN_ADDR_ERRORS,
                                              0, &u4RetVal);
                gBiteGlobalInfo.u4IpInAdderrbaseval = u4RetVal;
                break;

            case IP_IN_UNKNOWN_PROTO_FAIL:
                BitePortGetStatisticsCounter
                    (BITE_IP_IN_UNKNOWN_PROTOS, 0, &u4RetVal);
                gBiteGlobalInfo.u4IpInUnknwnProbaseval = u4RetVal;
                break;

            case IP_IN_DISCARDS_FAIL:
                BitePortGetStatisticsCounter (BITE_IP_IN_DISCARDS,
                                              0, &u4RetVal);
                gBiteGlobalInfo.u4IpInDisbaseval = u4RetVal;
                break;

            case IP_OUT_DISCARDS_FAIL:
                BitePortGetStatisticsCounter (BITE_IP_OUT_DISCARDS,
                                              0, &u4RetVal);
                gBiteGlobalInfo.u4IpOutDisbaseval = u4RetVal;
                break;

            case IP_REASM_FAIL:
                BitePortGetStatisticsCounter (BITE_IP_REASM_FAILS,
                                              0, &u4RetVal);
                gBiteGlobalInfo.u4IpReasmFailbaseval = u4RetVal;
                break;

            case IP_FRAG_FAIL:
                BitePortGetStatisticsCounter (BITE_IP_FRAG_FAILS, 0, &u4RetVal);
                gBiteGlobalInfo.u4IpFragFailbaseval = u4RetVal;
                break;

            default:
                break;
        }
    }

    if (u4OverallBitMask == INTERFACE_STATISTICS_COUNTER_FAIL)
    {
        switch (u4BitMask)
        {

            case INTERFACE_OPER_STATUS_FAIL:
                pBiteIfInfo->u4OperStatusCounter = BITE_ZERO;
                break;
            case INTERFACE_IN_DISCARDS_FAIL:
                BitePortGetStatisticsCounter (BITE_IF_IN_DISCARDS, u4IfNum,
                                              &u4RetVal);
                pBiteIfInfo->u4IfInDisbaseval = u4RetVal;
                break;
            case INTERFACE_IN_ERRORS_FAIL:
                BitePortGetStatisticsCounter (BITE_IF_IN_ERRORS, u4IfNum,
                                              &u4RetVal);
                pBiteIfInfo->u4IfInerrbaseval = u4RetVal;
                break;
            case INTERFACE_IN_UNKNOWN_PROTOS_FAIL:
                BitePortGetStatisticsCounter (BITE_IF_IN_UNKNOWN_PROTOS,
                                              u4IfNum, &u4RetVal);
                pBiteIfInfo->u4IfInUnknwnProtbaseval = u4RetVal;
                break;
            case INTERFACE_OUT_DISCARDS_FAIL:
                BitePortGetStatisticsCounter (BITE_IF_OUT_DISCARDS, u4IfNum,
                                              &u4RetVal);
                pBiteIfInfo->u4IfOutDisbaseval = u4RetVal;
                break;
            case INTERFACE_OUT_ERRORS_FAIL:
                BitePortGetStatisticsCounter (BITE_IF_OUT_ERRORS, u4IfNum,
                                              &u4RetVal);
                pBiteIfInfo->u4IfOuterrbaseval = u4RetVal;
                break;

            default:
                break;
        }
    }
    if (u4OverallBitMask == ICMP_STATISTICS_COUNTER_FAIL)
    {
        switch (u4BitMask)
        {

            case ICMP_IN_ERR_FAIL:
                BitePortGetStatisticsCounter (BITE_ICMP_IN_ERRORS,
                                              0, &u4RetVal);
                gBiteGlobalInfo.u4IcmpInerrbaseval = u4RetVal;
                break;

            case ICMP_OUT_ERR_FAIL:
                BitePortGetStatisticsCounter (BITE_ICMP_OUT_ERRORS,
                                              0, &u4RetVal);
                gBiteGlobalInfo.u4IcmpOuterrbaseval = u4RetVal;
                break;

            default:
                break;

        }
    }
    if (u4OverallBitMask == TCP_STATISTICS_COUNTER_FAIL)
    {

        BitePortGetStatisticsCounter (BITE_TCP_IN_ERRORS, 0, &u4RetVal);
        gBiteGlobalInfo.u4TcpInerrbaseval = u4RetVal;

    }
    if (u4OverallBitMask == UDP_STATISTICS_COUNTER_FAIL)
    {

        BitePortGetStatisticsCounter (BITE_UDP_IN_ERRORS, 0, &u4RetVal);
        gBiteGlobalInfo.u4UdpInerrbaseval = u4RetVal;
    }

    switch (u4OverallBitMask)
    {
        case IP_STATISTICS_COUNTER_FAIL:
            if (u4BitMask == IP_IN_HDR_ERR_FAIL)
            {
                gBiteGlobalInfo.u4IpStatsErrCode &= BITE_ONE_MASK;
            }
            if (u4BitMask == IP_IN_ADDR_FAIL)
            {
                gBiteGlobalInfo.u4IpStatsErrCode &= BITE_TWO_MASK;
            }
            if (u4BitMask == IP_IN_UNKNOWN_PROTO_FAIL)
            {
                gBiteGlobalInfo.u4IpStatsErrCode &= BITE_FOUR_MASK;
            }
            if (u4BitMask == IP_IN_DISCARDS_FAIL)
            {
                gBiteGlobalInfo.u4IpStatsErrCode &= BITE_EIGHT_MASK;
            }
            if (u4BitMask == IP_OUT_DISCARDS_FAIL)
            {
                gBiteGlobalInfo.u4IpStatsErrCode &= BITE_SIXTEEN_MASK;
            }
            if (u4BitMask == IP_REASM_FAIL)
            {
                gBiteGlobalInfo.u4IpStatsErrCode &= BITE_THIRTYTWO_MASK;
            }
            if (u4BitMask == IP_FRAG_FAIL)
            {
                gBiteGlobalInfo.u4IpStatsErrCode &= BITE_SIXTY_MASK;
            }
            if (gBiteGlobalInfo.u4IpStatsErrCode == 0)
            {
                gBiteGlobalInfo.u4ContSelfTestRetCode &= BITE_TWO_MASK;
            }
            break;

        case INTERFACE_STATISTICS_COUNTER_FAIL:

            if (u4BitMask == INTERFACE_OPER_STATUS_FAIL)
            {
                pBiteIfInfo->u4IfContSelfTestRetCode &= BITE_ONE_MASK;
            }
            if (u4BitMask == INTERFACE_IN_DISCARDS_FAIL)
            {

                pBiteIfInfo->u4IfContSelfTestRetCode &= BITE_TWO_MASK;
            }
            if (u4BitMask == INTERFACE_IN_ERRORS_FAIL)
            {
                pBiteIfInfo->u4IfContSelfTestRetCode &= BITE_FOUR_MASK;
            }
            if (u4BitMask == INTERFACE_IN_UNKNOWN_PROTOS_FAIL)
            {
                pBiteIfInfo->u4IfContSelfTestRetCode &= BITE_EIGHT_MASK;
            }
            if (u4BitMask == INTERFACE_OUT_DISCARDS_FAIL)
            {
                pBiteIfInfo->u4IfContSelfTestRetCode &= BITE_SIXTEEN_MASK;
            }
            if (u4BitMask == INTERFACE_OUT_ERRORS_FAIL)
            {
                pBiteIfInfo->u4IfContSelfTestRetCode &= BITE_THIRTYTWO_MASK;
            }
            if (pBiteIfInfo->u4IfContSelfTestRetCode == 0)
            {
                pBiteIfInfo->u4IfContSelfTestStatus = BITE_PASS;
            }
            for (u4LocIfNum = 1; u4LocIfNum <= gu4MaxIfcs; u4LocIfNum++)
            {
                pBiteIfInfo = BiteIfFind (u4LocIfNum);
                if ((pBiteIfInfo != NULL) &
                    (pBiteIfInfo->u4IfContSelfTestRetCode != 0))
                {

                    break;
                }
            }
            gBiteGlobalInfo.u4ContSelfTestRetCode &= BITE_ONE_MASK;
            break;
        case ICMP_STATISTICS_COUNTER_FAIL:
            if (u4BitMask == ICMP_IN_ERR_FAIL)
            {
                gBiteGlobalInfo.u4IcmpStatsErrCode &= BITE_ONE_MASK;
            }

            if (u4BitMask == ICMP_OUT_ERR_FAIL)
            {
                gBiteGlobalInfo.u4IcmpStatsErrCode &= BITE_TWO_MASK;
            }
            if (gBiteGlobalInfo.u4IcmpStatsErrCode == 0)
            {
                gBiteGlobalInfo.u4ContSelfTestRetCode &= BITE_FOUR_MASK;
            }
            break;
        case TCP_STATISTICS_COUNTER_FAIL:
            gBiteGlobalInfo.u4ContSelfTestRetCode &= BITE_EIGHT_MASK;
            break;

        case UDP_STATISTICS_COUNTER_FAIL:
            gBiteGlobalInfo.u4ContSelfTestRetCode &= BITE_SIXTEEN_MASK;
            break;
        default:
            break;
    }

    if (gBiteGlobalInfo.u4ContSelfTestRetCode == 0)
    {
        gBiteGlobalInfo.u4ContSelfTest = BITE_PASS;
        gBiteGlobalInfo.u4ContSelfTestRetCode = BITE_ZERO;
        if ((gBiteGlobalInfo.u4FailureReason & CBIT_TEST_FAIL) ==
            CBIT_TEST_FAIL)
        {
            gBiteGlobalInfo.u4FailureReason = BITE_ZERO;
            gBiteGlobalInfo.u4FailureType = BITE_RESERVED;
            gBiteGlobalInfo.u4OverallStatus = BITE_PASS;
        }
    }

}
VOID
BitetestOpenFile ()
{
    gLogFile = fopen (BITE_LOG_FILE, "a");
    fclose (gLogFile);
}

/*****************************************************************************/
/* Function                  : BitetestLogFile                               */
/*                                                                           */
/* Description               : This function used to LogFile the failures into*/
/*                               bite LogFile file whenever any test fails   */
/*                                                                           */
/* Input                     :                                               */
/*                             pErrString - failure pu1String                */
/*                                                                           */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/

VOID
BitetestLogFile (UINT4 u4TestType, UINT4 u4ErrCode)
{
    FILE               *gLogFile = NULL;
    UINT1               au1ErrString[BITE_ERR_LOG_MAX_LEN];
    UINT1               u1Flag = FALSE;
    INT4                WrSize = 0;
    UINT4               u4Size = 0;
    UINT4               u4CbitErrCode = 0;
    UINT4               u4IfIndex;
    tBiteIfInfo        *pbiteIfInfo;
    UINT1               tmp[11];
    UINT1               au1ReturnCode[BITE_FAILURE_REASON_MAX_LEN];

    MEMSET (au1ReturnCode, 0, BITE_FAILURE_REASON_MAX_LEN);

    MEMSET (au1ErrString, '\0', BITE_ERR_LOG_MAX_LEN);

    gLogFile = fopen (BITE_LOG_FILE, "a");

    /* OPEN the bite.log file for logging the failure info */

    if (gLogFile == NULL)
    {
        return;                    /* come out if we can't gLogFile */
    }

    /* WrSize = lseek(gLogFile,0,SEEK_END); */

    if (u4ErrCode > 0)
    {
        /* Log the tests failed based on the error code and test type */

        switch (u4TestType)
        {
                /* log the test failed as PBIT,IBIT or CBIT based on the test type
                   and tests which failed in PBIT,IBIT,CBIT based on the error code */
            case BITE_PBIT_TEST:
            case BITE_IBIT_TEST:
                if (u4TestType == BITE_PBIT_TEST)
                {
                    STRCAT (au1ErrString, "PBIT test failed:");
                }
                if (u4TestType == BITE_IBIT_TEST)
                {
                    STRCAT (au1ErrString, "IBIT test failed:");
                }
                if (u4ErrCode & PCI_TEST_FAIL)
                {
                    STRCAT (au1ErrString, "Pci ");
                    u1Flag = TRUE;
                }
                if (u4ErrCode & SCHANNEL_TEST_FAIL)
                {
                    if (u1Flag == TRUE)
                    {
                        STRCAT (au1ErrString, ", ");
                    }
                    STRCAT (au1ErrString, "S-channel ");
                    u1Flag = TRUE;
                }
                if (u4TestType == BITE_IBIT_TEST)
                {
                    if (u4ErrCode & CPU_BENCHMARK_TEST_FAIL)
                    {
                        if (u1Flag == TRUE)
                        {
                            STRCAT (au1ErrString, ", ");
                        }
                        STRCAT (au1ErrString, "Cpu benchmark ");
                        u1Flag = TRUE;
                    }
                }
                if (u4TestType == BITE_PBIT_TEST)
                {
                    if (u4ErrCode & COUNTER_RW_TEST_FAIL)
                    {
                        if (u1Flag == TRUE)
                        {
                            STRCAT (au1ErrString, ", ");
                        }
                        STRCAT (au1ErrString, "Counter Read/write ");
                        u1Flag = TRUE;
                    }
                    if (u4ErrCode & COUNTER_WIDTH_TEST_FAIL)
                    {
                        if (u1Flag == TRUE)
                        {
                            STRCAT (au1ErrString, ", ");
                        }
                        STRCAT (au1ErrString, "Counter width ");
                        u1Flag = TRUE;
                    }
                    if (u4ErrCode & PHY_LOOPBACK_TEST_FAIL)
                    {
                        if (u1Flag == TRUE)
                        {
                            STRCAT (au1ErrString, ", ");
                        }
                        STRCAT (au1ErrString, "Phydevice loopback ");
                        u1Flag = TRUE;
                    }
                }
                STRCAT (au1ErrString, "test failed \r\n");
                STRCAT (au1ErrString, "Overall Health Status: Failed \r\n");
                STRCAT (au1ErrString, "Overall Failure Type: Hardware \r\n");

                if (u4TestType == BITE_PBIT_TEST)
                {
                    STRCAT (au1ErrString,
                            "Overall Failure Reason: PBIT Test failed \r\n");
                }
                if (u4TestType == BITE_IBIT_TEST)
                {
                    STRCAT (au1ErrString,
                            "Overall Failure Reason: IBIT Test failed \r\n");
                }
                break;
            case BITE_CBIT_TEST:
                BitetestGetErrCode (BITE_CBIT_TEST, &u4CbitErrCode, 0);
                if (u4CbitErrCode != 0)
                {
                    BiteErrLogCbitFailure (u4CbitErrCode, au1ReturnCode);
                }
                STRCAT (au1ErrString, au1ReturnCode);

                break;
            case BITE_CONFIG_RESTORE:
                STRCAT (au1ErrString, "Configuration Restore Failed \r\n");

                STRCAT (au1ErrString, "Overall Health Status: Failed \r\n");

                STRCAT (au1ErrString, "Overall Failure Type: Software \r\n");

                STRCAT (au1ErrString,
                        "Overall Failure Reason: Configuration Restore Failed \r\n");

                break;
            default:
                fclose (gLogFile);
                return;
                break;
        }

    }
    u4Size = ftell (gLogFile);
    if ((u4Size + strlen (au1ErrString)) <= BITE_MAX_LOG_FILE_SIZE)
    {
        WrSize =
            fwrite (au1ErrString, strlen (au1ErrString), sizeof (char),
                    gLogFile);
        gu1Flag = FALSE;
    }
    else if (gu1Flag == FALSE)
    {
        PRINTF ("\r\nBite Log File exceeded the Maximum Limit %d(Bytes)\r\n",
                BITE_MAX_LOG_FILE_SIZE);
        gu1Flag = TRUE;
    }

    u4Size = ftell (gLogFile);
    if (u4Size == 0)
    {
        MEMSET (gau1OperPrevState, 0, sizeof (gau1OperPrevState));
        MEMSET (gau1OperCurrState, 0, sizeof (gau1OperCurrState));
        MEMSET (gau1InDisPrevState, 0, sizeof (gau1InDisPrevState));
        MEMSET (gau1InDisCurrState, 0, sizeof (gau1InDisCurrState));
        MEMSET (gau1InErrPrevState, 0, sizeof (gau1InErrPrevState));
        MEMSET (gau1InErrCurrState, 0, sizeof (gau1InErrCurrState));
        MEMSET (gau1InUnKnwPrevState, 0, sizeof (gau1InUnKnwPrevState));
        MEMSET (gau1InUnKnwCurrState, 0, sizeof (gau1InUnKnwCurrState));
        MEMSET (gau1OutDisPrevState, 0, sizeof (gau1OutDisPrevState));
        MEMSET (gau1OutDisCurrState, 0, sizeof (gau1OutDisCurrState));
        MEMSET (gau1OutErrPrevState, 0, sizeof (gau1OutErrPrevState));
        MEMSET (gau1OutErrCurrState, 0, sizeof (gau1OutErrCurrState));
        gu4IpCurrState = 0;
        gu4IcmpCurrState = 0;
        gu4TcpCurrState = 0;
        gu4UdpCurrState = 0;
    }

    fclose (gLogFile);
    return;
}

/*****************************************************************************/
/* Function                  : BitetestGetHwNum                           */
/*                                                                           */
/* Description               : This functionis used to get the hardware part */
/*                              num from cwhwcfg.txt
                                update the hardware num                      */
/* Input                     :                                               */
/*                             pErrString - failure pu1String   */
/*                                                                           */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
VOID
BitetestGetHwNum (UINT1 *pRetValString)
{

    FILE               *Stream = NULL;
    UINT1               au1Buffer[200];
    UINT1              *pu1Str = NULL;

    MEMSET (au1Buffer, '\0', sizeof (au1Buffer));
    /* open the file cwhwcfg.txt which contains the hardware part number */
    if ((Stream = fopen (CWHWCFG_FILE, "r")) == NULL)
    {
        exit (0);
    }
    /* copy the entire contents of the file into a temporary buffer */
    fread (au1Buffer, 1, (sizeof (au1Buffer) - 1), Stream);
    au1Buffer[sizeof (au1Buffer) - 1] = '\0';
    /* tokenize the contents of the buffer based on the token "=" */
    pu1Str = (UINT1 *) STRTOK (au1Buffer, " = \n ");
    if (pu1Str == NULL)
    {
        exit (0);
    }

    while (1)
    {
        /*Loop till the token matches HW_PART_NUM since the file
           contains HW_PART_NUM = <hwnum> and then get the next 
           token which contains the hw num and return thet string */
        if (STRCMP (pu1Str, "HW_PART_NUM") == 0)
        {
            pu1Str = (UINT1 *) STRTOK (NULL, " = \n ");
            if (pu1Str == NULL)
            {
                exit (0);
            }
            STRCPY (pRetValString, pu1Str);
            break;

        }

        pu1Str = (UINT1 *) STRTOK (NULL, " = \n ");
        if (pu1Str == NULL)
        {
            exit (0);
        }

    }                            /* end of while loop */
    fclose (Stream);

    return;
}

/*****************************************************************************/
/* Function                  : BitetestGetSerialNum                           */
/*                                                                           */
/* Description               : This functionis used to get the hardware part */
/*                              num from cwhwcfg.txt
                                update the hardware num                      */
/* Input                     :                                               */
/*                             pErrString - failure pu1String   */
/*                                                                           */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
VOID
BitetestGetSerialNum (UINT1 *pRetValString)
{

    FILE               *Stream = NULL;
    UINT1               au1Buffer[200];
    UINT1              *pu1Str = NULL;
    MEMSET (au1Buffer, '\0', sizeof (au1Buffer));
    /* open the file cwhwcfg.txt which contains the serial number */
    if ((Stream = fopen (CWHWCFG_FILE, "r")) == NULL)
    {
        exit (0);
    }
    /* copy the entire contents of the cwhwcfg.txt file into a temporary 
       buffer */
    fread (au1Buffer, 1, (sizeof (au1Buffer) - 1), Stream);
    au1Buffer[sizeof (au1Buffer) - 1] = '\0';
    /* Tokenize the contents of the buffer based on the token "=" */
    pu1Str = (UINT1 *) STRTOK (au1Buffer, " = \n ");
    if (pu1Str == NULL)
    {
        exit (0);
    }

    while (1)
    {
        /*Loop till the token matches SERIAL_NUM since the file
           contains SERIAL_NUM = <serialnum> and then get the next 
           token which contains the serial num and return thet string */
        if (STRCMP (pu1Str, "SERIAL_NUM") == 0)
        {
            pu1Str = (UINT1 *) STRTOK (NULL, " = \n ");
            if (pu1Str == NULL)
            {
                exit (0);
            }

            STRCPY (pRetValString, pu1Str);
            break;

        }
        pu1Str = (UINT1 *) STRTOK (NULL, " = \n ");
        if (pu1Str == NULL)
        {
            exit (0);
        }

    }
    fclose (Stream);
    return;

}

/*****************************************************************************/
/* Function                  : BitetestGetSwNum                           */
/*                                                                           */
/* Description               : This functionis used to get the hardware part */
/*                              num from cwhwcfg.txt
                                update the hardware num                      */
/* Input                     :                                               */
/*                             pErrString - failure pu1String   */
/*                                                                           */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
VOID
BitetestGetSwNum (UINT1 *pRetValString)
{

    FILE               *Stream = NULL;
    UINT1               au1Buffer[200];
    UINT1              *pu1Str = NULL;
    MEMSET (au1Buffer, '\0', sizeof (au1Buffer));
    /* Open the cwswcfg.txt file which contains the sw number */
    if ((Stream = fopen (CWSWCFG_FILE, "r")) == NULL)
    {
        exit (0);
    }
    /* copy the entire contents of the cwswcfg.txt into a temp buffer */
    fread (au1Buffer, 1, sizeof (au1Buffer), Stream);
    au1Buffer[sizeof (au1Buffer) - 1] = '\0';
    /* Tokenize the contents of the buffer based on the token "=" */
    pu1Str = (UINT1 *) STRTOK (au1Buffer, " = \n ");
    if (pu1Str == NULL)
    {
        exit (0);
    }

    while (1)
    {
        /*Loop till the token matches CW_CEC_VER since the file
           contains CW_CEC_VER = <swvernum> and then get the next 
           token which contains the sw num and return thet string */
        if (STRCMP (pu1Str, "CWCEC_SW_VER") == 0)
        {
            pu1Str = (UINT1 *) STRTOK (NULL, " = \n ");
            if (pu1Str == NULL)
            {
                exit (0);
            }

            STRCPY (pRetValString, pu1Str);
            break;
        }

        pu1Str = (UINT1 *) STRTOK (NULL, " = \n ");
        if (pu1Str == NULL)
        {
            exit (0);
        }

    }
    fclose (Stream);
    return;

}

/*******************function for getting err code**********************************************/
VOID
BitetestGetErrCode (UINT4 u4TestType, UINT4 *pRetErrCode, UINT4 u4IfNum)
{
    tBiteIfInfo        *pBiteIfInfo = NULL;

    switch (u4TestType)
    {
        case BITE_OVERALL_FAIL:
            *pRetErrCode = gBiteGlobalInfo.u4FailureReason;
            break;
        case BITE_IBIT_TEST:
            *pRetErrCode = gBiteGlobalInfo.u4SelfTestReturnCode;
            break;
        case BITE_CBIT_TEST:
            *pRetErrCode = gBiteGlobalInfo.u4ContSelfTestRetCode;
            break;
        case BITE_INTERFACE_FAIL:
            pBiteIfInfo = BiteIfFind (u4IfNum);
            *pRetErrCode = pBiteIfInfo->u4IfContSelfTestRetCode;
    }

    return;
}
#endif
