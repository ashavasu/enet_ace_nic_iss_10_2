/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 *
 * Description: This file contains the portable routines for BITE module
 *              that calls APIs given by other modules.
 *********************************************************************/
#ifndef _BITEPORT_C_
#define _BITEPORT_C_
#include "biteinc.h"
#include "../../netip/ipmgmt/inc/stdiplow.h"
#include "../../netip/ipmgmt/inc/stdtclow.h"
#include "../include/bcm/error.h"
#include "../include/bcm/stat.h"
extern test_t      *test_find (char *);
extern int          bcmx_stat_get32 (bcmx_lport_t port, bcm_stat_val_t type,
                                     UINT4 *value);

/*****************************************************************************/
/* Function     : BitePortGetIpStatisticsCounter                             */
/*                                                                           */
/* Description  : This function is called during the expiry of one second 
                  timer or from the function BitetestStartCbitTest() 
                  for getting the Ip counter statistics from the external
                  module IP                                                  */
/*                                                                           */
/* Input        : u4CounterType - Type of IP counters from 1 to 11                                                                                                                             */
/*                                                                           */
/* Output       : pRetVal - statistics of the counter based on counter type  */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT1
BitePortGetStatisticsCounter (UINT4 u4CounterType, UINT4 u4IfNum,
                              UINT4 *pRetVal)
{

    *pRetVal = 0;

    switch (u4CounterType)
    {
            /* Get the statistics fot IpInHdrErrors from IP */
        case BITE_IP_IN_HDR_ERRORS:
            IPFWD_PROT_LOCK ();
            if (nmhGetIpInHdrErrors (pRetVal) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }
            IPFWD_PROT_UNLOCK ();
            break;
            /* Get the statistics fot IpInAddrErrors from IP */
        case BITE_IP_IN_ADDR_ERRORS:
            IPFWD_PROT_LOCK ();
            if (nmhGetIpInAddrErrors (pRetVal) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }

            IPFWD_PROT_UNLOCK ();
            break;
            /* Get the statistics fot IpInUnknownprotos from IP */
        case BITE_IP_IN_UNKNOWN_PROTOS:
            IPFWD_PROT_LOCK ();
            if (nmhGetIpInUnknownProtos (pRetVal) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }

            IPFWD_PROT_UNLOCK ();
            break;
            /* Get the statistics fot IpInDiscards from IP */
        case BITE_IP_IN_DISCARDS:
            IPFWD_PROT_LOCK ();
            if (nmhGetIpInDiscards (pRetVal) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }

            IPFWD_PROT_UNLOCK ();
            break;
            /* Get the statistics fot IpoutDiscards from IP */
        case BITE_IP_OUT_DISCARDS:
            IPFWD_PROT_LOCK ();
            if (nmhGetIpOutDiscards (pRetVal) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }

            IPFWD_PROT_UNLOCK ();
            break;
            /* Get the statistics fot IpReasmFails from IP */
        case BITE_IP_REASM_FAILS:
            IPFWD_PROT_LOCK ();
            if (nmhGetIpReasmFails (pRetVal) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }
            IPFWD_PROT_UNLOCK ();
            break;
            /* Get the statistics fot IpFragfails from IP */
        case BITE_IP_FRAG_FAILS:
            IPFWD_PROT_LOCK ();
            if (nmhGetIpFragFails (pRetVal) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }
            IPFWD_PROT_UNLOCK ();
            break;
            /* Get the statistics fot IcmpInErrors from IP */
        case BITE_ICMP_IN_ERRORS:
            IPFWD_PROT_LOCK ();
            if (nmhGetIcmpInErrors (pRetVal) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }
            IPFWD_PROT_UNLOCK ();
            break;
            /* Get the statistics fot IcmpOutErrors from IP */
        case BITE_ICMP_OUT_ERRORS:
            IPFWD_PROT_LOCK ();
            if (nmhGetIcmpOutErrors (pRetVal) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }
            IPFWD_PROT_UNLOCK ();
            break;
            /* Get the statistics fot TcpInErrors from IP */
        case BITE_TCP_IN_ERRORS:
            if (nmhGetTcpInErrs (pRetVal) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }
            break;
            /* Get the statistics fot UdpInErrors from IP */
        case BITE_UDP_IN_ERRORS:
            if (nmhGetUdpInErrors (pRetVal) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }
            break;
        case BITE_IF_IN_DISCARDS:
            CFA_LOCK ();
            if (nmhGetIfInDiscards (u4IfNum, pRetVal) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }
            CFA_UNLOCK ();
            break;
        case BITE_IF_IN_ERRORS:
            CFA_LOCK ();
            if (nmhGetIfInErrors (u4IfNum, pRetVal) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }
            CFA_UNLOCK ();
            break;
        case BITE_IF_IN_UNKNOWN_PROTOS:
            CFA_LOCK ();
            if (nmhGetIfInUnknownProtos (u4IfNum, pRetVal) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }
            CFA_UNLOCK ();
            break;
        case BITE_IF_OUT_DISCARDS:
            CFA_LOCK ();
            if (nmhGetIfOutDiscards (u4IfNum, pRetVal) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }
            CFA_UNLOCK ();
            break;
        case BITE_IF_OUT_ERRORS:
            CFA_LOCK ();
            if (nmhGetIfOutErrors (u4IfNum, pRetVal) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }
            CFA_UNLOCK ();
            break;

        default:
            break;
    }
    return OSIX_SUCCESS;

}

/*****************************************************************************/
/* Function     : BitePortGetTestEntry                                       */
/*                                                                           */
/* Description  : This function is used to get the test entry for a given 
                  test id or test name                                       */
/*                                                                           */
/* Input        : key - key to locate the test based on name or test number
                  If key is all digits (0-9), it is assumed top be a test
                  Number otherwise it is a test name                         */
/*                                                                           */
/* Output       :  None                                                    
                                                                             */
/* Returns      : Pointer to entry if found
                  NULL if no match found
                                                                             */
/*                                                                           */
/*****************************************************************************/

test_t             *
BitePortGetTestEntry (char *key)
{

    test_t             *test = NULL;
    /* call the test_find() function in BCM sdk with the Key for getting 
       the updated test structure */
    test = test_find (key);
    return test;

}

#endif
