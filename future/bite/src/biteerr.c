/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 *
 * Description: This file contains BITE error string display routines
 *       
 *********************************************************************/
#ifndef _BITEERR_C_
#define _BITEERR_C_
#include "biteinc.h"
UINT1               gau1OperPrevState[200];
UINT1               gau1OperCurrState[200];
UINT1               gau1InDisPrevState[200];
UINT1               gau1InDisCurrState[200];
UINT1               gau1InErrPrevState[200];
UINT1               gau1InErrCurrState[200];
UINT1               gau1InUnKnwPrevState[200];
UINT1               gau1InUnKnwCurrState[200];
UINT1               gau1OutDisPrevState[200];
UINT1               gau1OutDisCurrState[200];
UINT1               gau1OutErrPrevState[200];
UINT1               gau1OutErrCurrState[200];
UINT4               gu4IpCurrState = 0;
UINT4               gu4IcmpCurrState = 0;
UINT4               gu4TcpCurrState = 0;
UINT4               gu4UdpCurrState = 0;
/*****************************************************************************/
/* Function                  : BiteErrFailureReason                          */
/*                                                                           */
/* Description               : This function returns the failure reason 
                               for the error code                            */
/*                                                                           */
/* Input                     : u4ErrCode - error code                        */
/*                                                                           */
/* Output                    : pRetErrString - error string for error code
                                                                             */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None                                          */
/*                                                                           */
/*****************************************************************************/

VOID
BiteErrFailureReason (UINT4 u4ErrCode, UINT1 *pRetErrString)
{
    UINT4               u4IfIndex;
    tBiteIfInfo        *pbiteIfInfo;
    UINT1               tmp[11];
    /* If the error code is zero,all the tests passed so display 
       failure reason as None */
    if (u4ErrCode == 0)
    {
        STRCPY (pRetErrString, "None \r\n");
        return;
    }
    /* If the Initiated Built in test failed dispaly the failure reason as 
       IBIT test failed */
    else if ((IBIT_TEST_FAIL & u4ErrCode) != 0)
    {
        STRCAT (pRetErrString, "IBIT test failed\r\n");
    }
    /* If the Continuous Built in test failed dispaly the failure reason as 
       CBIT test failed */
    else if ((CBIT_TEST_FAIL & u4ErrCode) != 0)
    {
        STRCAT (pRetErrString, "CBIT test failed:Ports causes the failure:");
        /* If the CBIT test failed identify the port that caudes the CBIT failure and
           dispaly in failure reason */

        for (u4IfIndex = 1; u4IfIndex <= gu4MaxIfcs; u4IfIndex++)
        {
            pbiteIfInfo = BiteIfFind (u4IfIndex);
            if ((pbiteIfInfo->u4IfContSelfTestStatus) == BITE_FAILED)
            {
                SPRINTF ((CHR1 *) tmp, "%ld", pbiteIfInfo->u4IfIndex);
                STRCAT (pRetErrString, tmp);
                STRCAT (pRetErrString, " ");
            }
        }

    }
    /* If the Configuration restore failed,dispaly the failure reason as
       Configuration restore failed */
    else if ((CONFIG_RESTORE_FAILURE & u4ErrCode) != 0)
    {
        STRCAT (pRetErrString, "configuration restore failed\r\n");
    }
    /* If the PBIT test failed,dispaly the failure reason as PBIT test failed
       and identify the tests in PBIT that caused PBIT failure */
    else
    {

        STRCAT (pRetErrString, "PBIT Test Failed- ");
        /* If the Pci in PBIT test failed,dispaly the failure reason as Pci */
        if ((PCI_TEST_FAIL & u4ErrCode) != 0)
        {
            STRCAT (pRetErrString, "Pci");
        }
        /* If the S-Channel in PBIT test failed,dispaly the failure reason as 
           S-Channel */
        if ((SCHANNEL_TEST_FAIL & u4ErrCode) != 0)
        {

            STRCAT (pRetErrString, "   S-channel");

        }
        /* If the Counter RW in PBIT test failed,dispaly the failure reason as 
           Counter RW */
        if ((COUNTER_RW_TEST_FAIL & u4ErrCode) != 0)
        {
            STRCAT (pRetErrString, "   counter RW ");
        }
        /* If the Counter width in PBIT test failed,dispaly the failure reason as 
           counter width */
        if ((COUNTER_WIDTH_TEST_FAIL & u4ErrCode) != 0)
        {
            STRCAT (pRetErrString, "   Counter width");

        }
        /* If the Physical loopback in PBIT test failed,dispaly the failure reason as */
        /*Phy device */
        if ((PHY_LOOPBACK_TEST_FAIL & u4ErrCode) != 0)
        {
            STRCAT (pRetErrString, "   PhyDevice loopback");
        }
    }
    return;
}

/*****************************************************************************/
/* Function                  : BiteErrIbitFailure                            */
/*                                                                           */
/* Description               : This function returns the failure reason
                               for the error code                            */
/*                                                                           */
/* Input                     : u4ErrCode - error code                        */
/*                                                                           */
/* Output                    : pRetErrString - error string for error code
                                                                             */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None                                          */
/*                                                                           */
/*****************************************************************************/

VOID
BiteErrIbitFailure (UINT4 u4Err, UINT1 *pRetErrStr)
{
    /* If the IBIT test failed,dispaly the failure reason as IBIT test failed
       and identify the tests in IBIT that caused IBIT failure */
    STRCAT (pRetErrStr, "IBIT Test Failed-");
    /* If the pci test failed dispaly the IBIT failure reason as pci */
    if ((PCI_TEST_FAIL & u4Err) != 0)
    {
        STRCAT (pRetErrStr, "PCI");
    }
    /* If the s-channel test failed dispaly the IBIT failure reason as 
       s-channel */
    if ((SCHANNEL_TEST_FAIL & u4Err) != 0)
    {
        STRCAT (pRetErrStr, "   S-CHANNEL");

    }
    /* If the cpu benchmark test failed dispaly the IBIT failure reason as 
       cpu benchmark */
    if ((CPU_BENCHMARK_TEST_FAIL & u4Err) != 0)
    {
        STRCAT (pRetErrStr, "   CPU BENCHMARK");
    }
    return;

}

/*****************************************************************************/
/* Function                  : BiteErrCbitFailure                            */
/*                                                                           */
/* Description               : This function returns the failure reason
                               for the error code                            */
/*                                                                           */
/* Input                     : u4ErrCode - error code                        */
/*                                                                           */
/* Output                    : pRetErrString - error string for error code
                                                                             */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None                                          */
/*                                                                           */
/*****************************************************************************/

VOID
BiteErrCbitFailure (UINT4 u4Errcode, UINT1 *pRetErrstring)
{
    UINT4               u4IfNum;
    tBiteIfInfo        *pBiteIfInfo;
    UINT1               au1TempIfNum[30];
    UINT1               au1OperIfNum[200];
    UINT1               au1InErrIfNum[200];
    UINT1               au1InDisIfNum[200];
    UINT1               au1OutErrIfNum[200];
    UINT1               au1OutDisIfNum[200];
    UINT1               au1InUnkwnProtIfNum[200];
    UINT4               u4IfContSelfTestRetCode;
    UINT4               u4IpStatsErrCode;
    UINT4               u4IcmpStatsErrCode;
    UINT4               u4IfErrCode = 0;

    u4IpStatsErrCode = gBiteGlobalInfo.u4IpStatsErrCode;
    u4IcmpStatsErrCode = gBiteGlobalInfo.u4IcmpStatsErrCode;
    MEMSET (au1TempIfNum, 0, sizeof (au1TempIfNum));
    /* Identify the counter that caused the continuous built in Test failure */
    MEMSET (au1OperIfNum, 0, sizeof (au1OperIfNum));
    MEMSET (au1InErrIfNum, 0, sizeof (au1InErrIfNum));
    MEMSET (au1InDisIfNum, 0, sizeof (au1InDisIfNum));
    MEMSET (au1OutErrIfNum, 0, sizeof (au1OutErrIfNum));
    MEMSET (au1OutDisIfNum, 0, sizeof (au1OutDisIfNum));
    MEMSET (au1InUnkwnProtIfNum, 0, sizeof (au1InUnkwnProtIfNum));

    if ((INTERFACE_STATISTICS_COUNTER_FAIL & u4Errcode) != 0)
    {
        for (u4IfNum = 1; u4IfNum <= gu4MaxIfcs; u4IfNum++)
        {
            pBiteIfInfo = BiteIfFind (u4IfNum);
            if (pBiteIfInfo == NULL)
            {
                continue;
            }
            u4IfContSelfTestRetCode = pBiteIfInfo->u4IfContSelfTestRetCode;
            /* If the interface oper status fails dispaly the CBIT failure as
               IfInOperStatus failure */
            if (((pBiteIfInfo->u4IfContSelfTestRetCode) &
                 INTERFACE_OPER_STATUS_FAIL) != 0)
            {
                u4IfErrCode = u4IfErrCode | INTERFACE_OPER_STATUS_FAIL;
                SPRINTF ((CHR1 *) au1TempIfNum, "%ld", u4IfNum);
                STRCAT ((CHR1 *) au1OperIfNum, au1TempIfNum);
                STRCAT ((CHR1 *) au1OperIfNum, " ");
                MEMSET (au1TempIfNum, 0, sizeof (au1TempIfNum));
            }
            /* If the IfInDiscards fails dispaly the CBIT failure as
               IfInDiscards failure */
            if (((pBiteIfInfo->u4IfContSelfTestRetCode) &
                 INTERFACE_IN_DISCARDS_FAIL) != 0)
            {
                u4IfErrCode = u4IfErrCode | INTERFACE_IN_DISCARDS_FAIL;
                SPRINTF ((CHR1 *) au1TempIfNum, "%ld", u4IfNum);
                STRCAT ((CHR1 *) au1InDisIfNum, au1TempIfNum);
                STRCAT ((CHR1 *) au1InDisIfNum, " ");
                MEMSET (au1TempIfNum, 0, sizeof (au1TempIfNum));

            }
            /* If the IfInErrors fails dispaly the CBIT failure as
               IfInErrors failure */
            if (((pBiteIfInfo->u4IfContSelfTestRetCode) &
                 INTERFACE_IN_ERRORS_FAIL) != 0)
            {
                u4IfErrCode = u4IfErrCode | INTERFACE_IN_ERRORS_FAIL;
                SPRINTF ((CHR1 *) au1TempIfNum, "%ld", u4IfNum);
                STRCAT ((CHR1 *) au1InErrIfNum, au1TempIfNum);
                STRCAT ((CHR1 *) au1InErrIfNum, " ");
                MEMSET (au1TempIfNum, 0, sizeof (au1TempIfNum));

            }
            /* If the IfInUnknownProtos fails dispaly the CBIT failure as
               IfInUnknownProtos failure */
            if (((pBiteIfInfo->u4IfContSelfTestRetCode) &
                 INTERFACE_IN_UNKNOWN_PROTOS_FAIL) != 0)
            {
                u4IfErrCode = u4IfErrCode | INTERFACE_IN_UNKNOWN_PROTOS_FAIL;
                SPRINTF ((CHR1 *) au1TempIfNum, "%ld", u4IfNum);
                STRCAT ((CHR1 *) au1InUnkwnProtIfNum, au1TempIfNum);
                STRCAT ((CHR1 *) au1InUnkwnProtIfNum, " ");
                MEMSET (au1TempIfNum, 0, sizeof (au1TempIfNum));
            }
            /* If the IfOutDiscards  fails dispaly the CBIT failure as
               IfOutDiscards failure */
            if (((pBiteIfInfo->u4IfContSelfTestRetCode) &
                 INTERFACE_OUT_DISCARDS_FAIL) != 0)
            {
                u4IfErrCode = u4IfErrCode | INTERFACE_OUT_DISCARDS_FAIL;
                SPRINTF ((CHR1 *) au1TempIfNum, "%ld", u4IfNum);
                STRCAT ((CHR1 *) au1OutDisIfNum, au1TempIfNum);
                STRCAT ((CHR1 *) au1OutDisIfNum, " ");
                MEMSET (au1TempIfNum, 0, sizeof (au1TempIfNum));

            }
            /* If the IfOutErrors fails dispaly the CBIT failure as
               IfOutErrors failure */
            if (((pBiteIfInfo->u4IfContSelfTestRetCode) &
                 INTERFACE_OUT_ERRORS_FAIL) != 0)
            {
                u4IfErrCode = u4IfErrCode | INTERFACE_OUT_ERRORS_FAIL;
                SPRINTF ((CHR1 *) au1TempIfNum, "%ld", u4IfNum);
                STRCAT ((CHR1 *) au1OutErrIfNum, au1TempIfNum);
                STRCAT ((CHR1 *) au1OutErrIfNum, " ");
                MEMSET (au1TempIfNum, 0, sizeof (au1TempIfNum));
            }
        }
    }
    if (u4IfErrCode != 0)
    {
        if ((u4IfErrCode & INTERFACE_OPER_STATUS_FAIL) != 0)
        {
            STRCAT (pRetErrstring,
                    "IfInOperStatus exceeded the threshold on the ports:");
            STRCAT (pRetErrstring, (CHR1 *) au1OperIfNum);
            STRCAT (pRetErrstring, "\r\n");
        }
        if ((u4IfErrCode & INTERFACE_IN_DISCARDS_FAIL) != 0)
        {
            STRCAT (pRetErrstring,
                    "IfInDiscard exceeded the threshold on the ports:");
            STRCAT (pRetErrstring, (CHR1 *) au1InDisIfNum);
            STRCAT (pRetErrstring, "\r\n");
        }
        if ((u4IfErrCode & INTERFACE_IN_ERRORS_FAIL) != 0)
        {
            STRCAT (pRetErrstring,
                    "IfInErrors exceeded the threshold on the ports:");
            STRCAT (pRetErrstring, (CHR1 *) au1InErrIfNum);
            STRCAT (pRetErrstring, "\r\n");
        }
        if ((u4IfErrCode & INTERFACE_IN_UNKNOWN_PROTOS_FAIL) != 0)
        {
            STRCAT (pRetErrstring,
                    "IfInUnknownProtos exceeded the threshold on the ports:");
            STRCAT (pRetErrstring, (CHR1 *) au1InUnkwnProtIfNum);
            STRCAT (pRetErrstring, "\r\n");
        }
        if ((u4IfErrCode & INTERFACE_OUT_DISCARDS_FAIL) != 0)
        {
            STRCAT (pRetErrstring,
                    "IfOutDiscard exceeded the threshold on the ports:");
            STRCAT (pRetErrstring, (CHR1 *) au1OutDisIfNum);
            STRCAT (pRetErrstring, "\r\n");
        }
        if ((u4IfErrCode & INTERFACE_OUT_ERRORS_FAIL) != 0)
        {
            STRCAT (pRetErrstring,
                    "IfOutErrors exceeded the threshold on the ports:");
            STRCAT (pRetErrstring, (CHR1 *) au1OutErrIfNum);
            STRCAT (pRetErrstring, "\r\n");
        }
        STRCAT (pRetErrstring, " \r\n");
    }
    if ((IP_STATISTICS_COUNTER_FAIL & u4Errcode) != 0)
    {
        /* If the IpInHdrErrors  fails dispaly the CBIT failure as
           IpInHdrErrors failure */
        STRCAT (pRetErrstring, "IP Statistics Counters: ");
        if ((IP_IN_HDR_ERR_FAIL & u4IpStatsErrCode) != 0)
        {
            STRCAT (pRetErrstring, "  IpInHdrErrors");
        }
        /* If the IpInAddrErrors  fails dispaly the CBIT failure as
           IpInAddrErrors failure */
        if ((IP_IN_ADDR_FAIL & u4IpStatsErrCode) != 0)
        {
            STRCAT (pRetErrstring, "  IpInAddrErrors");
        }
        /* If the IpInUnknownProtos fails dispaly the CBIT failure as
           IpInUnknownProtos failure */
        if ((IP_IN_UNKNOWN_PROTO_FAIL & u4IpStatsErrCode) != 0)
        {
            STRCAT (pRetErrstring, "  IpInUnknownProtos");

        }
        /* If the IpInDiscards  fails dispaly the CBIT failure as
           IpInDiscards failure */
        if ((IP_IN_DISCARDS_FAIL & u4IpStatsErrCode) != 0)
        {
            STRCAT (pRetErrstring, "  IpInDiscards");
        }
        /* If the IpOutDiscard  fails dispaly the CBIT failure as
           IpOutDiscard failure */
        if ((IP_OUT_DISCARDS_FAIL & u4IpStatsErrCode) != 0)
        {
            STRCAT (pRetErrstring, "  IpOutDiscards");

        }
        /* If the IpReasmFails  fails dispaly the CBIT failure as
           IpReasmFails failure */
        if ((IP_REASM_FAIL & u4IpStatsErrCode) != 0)
        {
            STRCAT (pRetErrstring, "  IpReasmFails");

        }
        /* If the IpFragFails fails dispaly the CBIT failure as
           IpFragFails failure */
        if ((IP_FRAG_FAIL & u4IpStatsErrCode) != 0)
        {
            STRCAT (pRetErrstring, "  IpFragFails");
        }
        STRCAT (pRetErrstring, " exceeded the threshold \r\n");
    }

    if ((ICMP_STATISTICS_COUNTER_FAIL & u4Errcode) != 0)
    {
        /* If the IcmpInErrors fails dispaly the CBIT failure as
           IcmpInErrors failure */
        STRCAT (pRetErrstring, "\r\n ICMP Statistics Counters: ");
        if ((ICMP_IN_ERR_FAIL & u4IcmpStatsErrCode) != 0)
        {
            STRCAT (pRetErrstring, "  IcmpInErrors");

        }
        /* If the IcmpOutErrorsErrors  fails dispaly the CBIT failure as
           IcmpOutErrorsErrors failure */
        if ((ICMP_OUT_ERR_FAIL & u4IcmpStatsErrCode) != 0)
        {
            STRCAT (pRetErrstring, "   IcmpOutErrors");

        }
        STRCAT (pRetErrstring, " exceeded the threshold \r\n");

    }
    /* If the TcpInErrors fails dispaly the CBIT failure as
       TcpInErrors failure */
    if ((TCP_STATISTICS_COUNTER_FAIL & u4Errcode) != 0)
    {
        STRCAT (pRetErrstring,
                "\r\n TCP statistics counter exceeded the threshold \r\n");
    }
    /* If the UdpInErrors fails dispaly the CBIT failure as
       UdpInErrors failure */
    if ((UDP_STATISTICS_COUNTER_FAIL & u4Errcode) != 0)
    {
        STRCAT (pRetErrstring,
                "\r\n UDP statistics counter exceeded the threshold \r\n");
    }

    return;

}

/*****************************************************************************/
/* Function                  : BiteErrIfCbitFailure                          */
/*                                                                           */
/* Description               : This function returns the failure reason
                               for the error code                            */
/*                                                                           */
/* Input                     : u4ErrCode - error code                        */
/*                                                                           */
/* Output                    : pRetErrString - error string for error code
                                                                             */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None                                          */
/*                                                                           */
/*****************************************************************************/

VOID
BiteErrIfCbitFailure (UINT4 u4IfNum, UINT4 u4Errcode, UINT1 *pRetErrstring)
{
    tBiteIfInfo        *pBiteIfInfo = NULL;
    pBiteIfInfo = BiteIfFind (u4IfNum);

    /* If the IfInOperStatus  fails dispaly the CBIT failure as
       IfInOperStatus failure */
    if ((u4Errcode & INTERFACE_OPER_STATUS_FAIL) != 0)
    {
        STRCAT (pRetErrstring, "IfInOperStatus");
        u4Errcode ^= INTERFACE_OPER_STATUS_FAIL;
        if (u4Errcode != 0)
        {
            STRCAT (pRetErrstring, ",");
        }
    }
    /* If the IfInDiscards   fails dispaly the CBIT failure as */
    /* IfInDiscards failure */
    if ((u4Errcode & INTERFACE_IN_DISCARDS_FAIL) != 0)
    {
        STRCAT (pRetErrstring, "IfInDiscards");
        u4Errcode ^= INTERFACE_IN_DISCARDS_FAIL;
        if (u4Errcode != 0)
        {
            STRCAT (pRetErrstring, ",");
        }
    }
    /* If the IfInerrors  fails dispaly the CBIT failure as
       IfOuterrors failure */
    if ((u4Errcode & INTERFACE_IN_ERRORS_FAIL) != 0)
    {
        STRCAT (pRetErrstring, "IfInErrors");
        u4Errcode ^= INTERFACE_IN_ERRORS_FAIL;
        if (u4Errcode != 0)
        {
            STRCAT (pRetErrstring, ",");
        }
    }
    /* If the IfInUnknownProtos   fails dispaly the CBIT failure as
       IfInUnknownProtos failure */
    if ((u4Errcode & INTERFACE_IN_UNKNOWN_PROTOS_FAIL) != 0)
    {
        STRCAT (pRetErrstring, "IfInUnknownProtos");
        u4Errcode ^= INTERFACE_IN_UNKNOWN_PROTOS_FAIL;
        if (u4Errcode != 0)
        {
            STRCAT (pRetErrstring, ",");
        }
    }
    /* If the IfOutDiscards  fails dispaly the CBIT failure as
       IfOutDiscards failure */
    if ((u4Errcode & INTERFACE_OUT_DISCARDS_FAIL) != 0)
    {
        STRCAT (pRetErrstring, "IfOutDiscards");
        u4Errcode ^= INTERFACE_OUT_DISCARDS_FAIL;
        if (u4Errcode != 0)
        {
            STRCAT (pRetErrstring, ",");
        }
    }
    /* If the IfOuterrors  fails dispaly the CBIT failure as
       IfOuterrors failure */
    if ((u4Errcode & INTERFACE_OUT_ERRORS_FAIL) != 0)
    {
        STRCAT (pRetErrstring, "IfOutErrors");
        u4Errcode ^= INTERFACE_OUT_ERRORS_FAIL;
        if (u4Errcode != 0)
        {
            STRCAT (pRetErrstring, ",");
        }
    }
    STRCAT (pRetErrstring, "  exceeded the threshold \r\n");
    return;

}

VOID
BiteErrLogCbitFailure (UINT4 u4Errcode, UINT1 *pRetErrstring)
{
    UINT4               u4IfNum;
    tBiteIfInfo        *pBiteIfInfo;
    UINT1               au1TempIfNum[30];
    UINT1               au1OperIfNum[200];
    UINT1               au1InErrIfNum[200];
    UINT1               au1InDisIfNum[200];
    UINT1               au1OutErrIfNum[200];
    UINT1               au1OutDisIfNum[200];
    UINT1               au1InUnkwnProtIfNum[200];
    UINT4               u4IfContSelfTestRetCode;
    UINT4               u4IpStatsErrCode;
    UINT4               u4IcmpStatsErrCode;
    UINT4               u4IfErrCode = 0;

    u4IpStatsErrCode = gBiteGlobalInfo.u4IpStatsErrCode;
    u4IcmpStatsErrCode = gBiteGlobalInfo.u4IcmpStatsErrCode;
    MEMSET (au1TempIfNum, 0, sizeof (au1TempIfNum));
    /* Identify the counter that caused the continuous built in Test failure */
    MEMSET (au1OperIfNum, 0, sizeof (au1OperIfNum));
    MEMSET (au1InErrIfNum, 0, sizeof (au1InErrIfNum));
    MEMSET (au1InDisIfNum, 0, sizeof (au1InDisIfNum));
    MEMSET (au1OutErrIfNum, 0, sizeof (au1OutErrIfNum));
    MEMSET (au1OutDisIfNum, 0, sizeof (au1OutDisIfNum));
    MEMSET (au1InUnkwnProtIfNum, 0, sizeof (au1InUnkwnProtIfNum));

    if ((INTERFACE_STATISTICS_COUNTER_FAIL & u4Errcode) != 0)
    {
        for (u4IfNum = 1; u4IfNum <= gu4MaxIfcs; u4IfNum++)
        {
            pBiteIfInfo = BiteIfFind (u4IfNum);
            if (pBiteIfInfo == NULL)
            {
                continue;
            }
            u4IfContSelfTestRetCode = pBiteIfInfo->u4IfContSelfTestRetCode;
            /* If the interface oper status fails dispaly the CBIT failure as
               IfInOperStatus failure */
            if (((pBiteIfInfo->u4IfContSelfTestRetCode) &
                 INTERFACE_OPER_STATUS_FAIL) != 0)
            {
                u4IfErrCode = u4IfErrCode | INTERFACE_OPER_STATUS_FAIL;
                SPRINTF ((CHR1 *) au1TempIfNum, "%ld", u4IfNum);
                STRCAT ((CHR1 *) au1OperIfNum, au1TempIfNum);
                STRCAT ((CHR1 *) au1OperIfNum, " ");
                MEMSET (au1TempIfNum, 0, sizeof (au1TempIfNum));
            }
            /* If the IfInDiscards fails dispaly the CBIT failure as
               IfInDiscards failure */
            if (((pBiteIfInfo->u4IfContSelfTestRetCode) &
                 INTERFACE_IN_DISCARDS_FAIL) != 0)
            {
                u4IfErrCode = u4IfErrCode | INTERFACE_IN_DISCARDS_FAIL;
                SPRINTF ((CHR1 *) au1TempIfNum, "%ld", u4IfNum);
                STRCAT ((CHR1 *) au1InDisIfNum, au1TempIfNum);
                STRCAT ((CHR1 *) au1InDisIfNum, " ");
                MEMSET (au1TempIfNum, 0, sizeof (au1TempIfNum));

            }
            /* If the IfInErrors fails dispaly the CBIT failure as
               IfInErrors failure */
            if (((pBiteIfInfo->u4IfContSelfTestRetCode) &
                 INTERFACE_IN_ERRORS_FAIL) != 0)
            {
                u4IfErrCode = u4IfErrCode | INTERFACE_IN_ERRORS_FAIL;
                SPRINTF ((CHR1 *) au1TempIfNum, "%ld", u4IfNum);
                STRCAT ((CHR1 *) au1InErrIfNum, au1TempIfNum);
                STRCAT ((CHR1 *) au1InErrIfNum, " ");
                MEMSET (au1TempIfNum, 0, sizeof (au1TempIfNum));

            }
            /* If the IfInUnknownProtos fails dispaly the CBIT failure as
               IfInUnknownProtos failure */
            if (((pBiteIfInfo->u4IfContSelfTestRetCode) &
                 INTERFACE_IN_UNKNOWN_PROTOS_FAIL) != 0)
            {
                u4IfErrCode = u4IfErrCode | INTERFACE_IN_UNKNOWN_PROTOS_FAIL;
                SPRINTF ((CHR1 *) au1TempIfNum, "%ld", u4IfNum);
                STRCAT ((CHR1 *) au1InUnkwnProtIfNum, au1TempIfNum);
                STRCAT ((CHR1 *) au1InUnkwnProtIfNum, " ");
                MEMSET (au1TempIfNum, 0, sizeof (au1TempIfNum));
            }
            /* If the IfOutDiscards  fails dispaly the CBIT failure as
               IfOutDiscards failure */
            if (((pBiteIfInfo->u4IfContSelfTestRetCode) &
                 INTERFACE_OUT_DISCARDS_FAIL) != 0)
            {
                u4IfErrCode = u4IfErrCode | INTERFACE_OUT_DISCARDS_FAIL;
                SPRINTF ((CHR1 *) au1TempIfNum, "%ld", u4IfNum);
                STRCAT ((CHR1 *) au1OutDisIfNum, au1TempIfNum);
                STRCAT ((CHR1 *) au1OutDisIfNum, " ");
                MEMSET (au1TempIfNum, 0, sizeof (au1TempIfNum));

            }
            /* If the IfOutErrors fails dispaly the CBIT failure as
               IfOutErrors failure */
            if (((pBiteIfInfo->u4IfContSelfTestRetCode) &
                 INTERFACE_OUT_ERRORS_FAIL) != 0)
            {
                u4IfErrCode = u4IfErrCode | INTERFACE_OUT_ERRORS_FAIL;
                SPRINTF ((CHR1 *) au1TempIfNum, "%ld", u4IfNum);
                STRCAT ((CHR1 *) au1OutErrIfNum, au1TempIfNum);
                STRCAT ((CHR1 *) au1OutErrIfNum, " ");
                MEMSET (au1TempIfNum, 0, sizeof (au1TempIfNum));
            }
        }
    }
    if (u4IfErrCode != 0)
    {
        if ((u4IfErrCode & INTERFACE_OPER_STATUS_FAIL) != 0)
        {
            STRCPY (gau1OperCurrState, au1OperIfNum);
            if (STRCMP (gau1OperPrevState, gau1OperCurrState) != 0)
            {
                STRCAT (pRetErrstring,
                        "IfInOperStatus exceeded the threshold on the ports:");
                STRCAT (pRetErrstring, (CHR1 *) au1OperIfNum);
                STRCAT (pRetErrstring, "\r\n");
                STRCAT (pRetErrstring, "Overall Health Status: Failed \r\n");
                STRCAT (pRetErrstring, "Overall Failure Type: Hardware \r\n");
                STRCAT (pRetErrstring,
                        "Overall Failure Reason: CBIT Test Failed \r\n");
                STRCPY (gau1OperPrevState, gau1OperCurrState);
            }
        }
        if ((u4IfErrCode & INTERFACE_IN_DISCARDS_FAIL) != 0)
        {
            STRCPY (gau1InDisCurrState, au1InDisIfNum);
            if (STRCMP (gau1InDisPrevState, gau1InDisCurrState) != 0)
            {
                STRCAT (pRetErrstring,
                        "IfInDiscard exceeded the threshold on the ports:");
                STRCAT (pRetErrstring, (CHR1 *) au1InDisIfNum);
                STRCAT (pRetErrstring, "\r\n");
                STRCAT (pRetErrstring, "Overall Health Status: Failed \r\n");
                STRCAT (pRetErrstring, "Overall Failure Type: Hardware \r\n");
                STRCAT (pRetErrstring,
                        "Overall Failure Reason: CBIT Test Failed \r\n");
                STRCPY (gau1InDisPrevState, gau1InDisCurrState);
            }
        }
        if ((u4IfErrCode & INTERFACE_IN_ERRORS_FAIL) != 0)
        {

            STRCPY (gau1InErrCurrState, au1InErrIfNum);
            if (STRCMP (gau1InErrPrevState, gau1InErrCurrState) != 0)
            {
                STRCAT (pRetErrstring,
                        "IfInErrors exceeded the threshold on the ports:");
                STRCAT (pRetErrstring, (CHR1 *) au1InErrIfNum);
                STRCAT (pRetErrstring, "\r\n");
                STRCAT (pRetErrstring, "Overall Health Status: Failed \r\n");
                STRCAT (pRetErrstring, "Overall Failure Type: Hardware \r\n");
                STRCAT (pRetErrstring,
                        "Overall Failure Reason: CBIT Test Failed \r\n");
                STRCPY (gau1InErrPrevState, gau1InErrCurrState);
            }
        }
        if ((u4IfErrCode & INTERFACE_IN_UNKNOWN_PROTOS_FAIL) != 0)
        {
            STRCPY (gau1InUnKnwCurrState, au1InUnkwnProtIfNum);
            if (STRCMP (gau1InUnKnwPrevState, gau1InUnKnwCurrState) != 0)
            {
                STRCAT (pRetErrstring,
                        "IfInUnknownProtos exceeded the threshold on the ports:");
                STRCAT (pRetErrstring, (CHR1 *) au1InUnkwnProtIfNum);
                STRCAT (pRetErrstring, "\r\n");
                STRCAT (pRetErrstring, "Overall Health Status: Failed \r\n");
                STRCAT (pRetErrstring, "Overall Failure Type: Hardware \r\n");
                STRCAT (pRetErrstring,
                        "Overall Failure Reason: CBIT Test Failed \r\n");
                STRCPY (gau1InUnKnwPrevState, gau1InUnKnwCurrState);
            }
        }
        if ((u4IfErrCode & INTERFACE_OUT_DISCARDS_FAIL) != 0)
        {

            STRCPY (gau1OutDisCurrState, au1OutDisIfNum);
            if (STRCMP (gau1OutDisPrevState, gau1OutDisCurrState) != 0)
            {
                STRCAT (pRetErrstring,
                        "IfOutDiscard exceeded the threshold on the ports:");
                STRCAT (pRetErrstring, (CHR1 *) au1OutDisIfNum);
                STRCAT (pRetErrstring, "\r\n");
                STRCAT (pRetErrstring, "Overall Health Status: Failed \r\n");
                STRCAT (pRetErrstring, "Overall Failure Type: Hardware \r\n");
                STRCAT (pRetErrstring,
                        "Overall Failure Reason: CBIT Test Failed \r\n");
                STRCPY (gau1OutDisPrevState, gau1OutDisCurrState);
            }
        }
        if ((u4IfErrCode & INTERFACE_OUT_ERRORS_FAIL) != 0)
        {
            STRCPY (gau1OutErrCurrState, au1OutErrIfNum);
            if (STRCMP (gau1OutDisPrevState, gau1OutDisCurrState) != 0)
            {
                STRCAT (pRetErrstring,
                        "IfOutErrors exceeded the threshold on the ports:");
                STRCAT (pRetErrstring, (CHR1 *) au1OutErrIfNum);
                STRCAT (pRetErrstring, "\r\n");
                STRCAT (pRetErrstring, "Overall Health Status: Failed \r\n");
                STRCAT (pRetErrstring, "Overall Failure Type: Hardware \r\n");
                STRCAT (pRetErrstring,
                        "Overall Failure Reason: CBIT Test Failed \r\n");
                STRCPY (gau1OutDisPrevState, gau1OutDisCurrState);
            }
        }
        /*STRCAT (pRetErrstring, " \r\n"); */
    }
    if ((IP_STATISTICS_COUNTER_FAIL & u4Errcode) != 0)
    {
        /* If the IpInHdrErrors  fails dispaly the CBIT failure as
           IpInHdrErrors failure */
        if ((IP_IN_HDR_ERR_FAIL & u4IpStatsErrCode) != 0)
        {
            if ((gu4IpCurrState & IP_IN_HDR_ERR_FAIL) == 0)
            {
                STRCAT (pRetErrstring, "IP Statistics Counters: ");
                STRCAT (pRetErrstring, "  IpInHdrErrors");
                STRCAT (pRetErrstring, " exceeded the threshold \r\n");
                STRCAT (pRetErrstring, "Overall Health Status: Failed \r\n");
                STRCAT (pRetErrstring, "Overall Failure Type: Hardware \r\n");
                STRCAT (pRetErrstring,
                        "Overall Failure Reason: CBIT Test Failed \r\n");
                gu4IpCurrState = gu4IpCurrState | IP_IN_HDR_ERR_FAIL;
            }
        }
        /* If the IpInAddrErrors  fails dispaly the CBIT failure as
           IpInAddrErrors failure */
        if ((IP_IN_ADDR_FAIL & u4IpStatsErrCode) != 0)
        {
            if ((gu4IpCurrState & IP_IN_ADDR_FAIL) == 0)
            {
                STRCAT (pRetErrstring, "IP Statistics Counters: ");
                STRCAT (pRetErrstring, "  IpInAddrErrors");
                STRCAT (pRetErrstring, " exceeded the threshold \r\n");
                STRCAT (pRetErrstring, "Overall Health Status: Failed \r\n");
                STRCAT (pRetErrstring, "Overall Failure Type: Hardware \r\n");
                STRCAT (pRetErrstring,
                        "Overall Failure Reason: CBIT Test Failed \r\n");
                gu4IpCurrState = gu4IpCurrState | IP_IN_ADDR_FAIL;
            }
        }
        /* If the IpInUnknownProtos fails dispaly the CBIT failure as
           IpInUnknownProtos failure */
        if ((IP_IN_UNKNOWN_PROTO_FAIL & u4IpStatsErrCode) != 0)
        {
            if ((gu4IpCurrState & IP_IN_UNKNOWN_PROTO_FAIL) == 0)
            {
                STRCAT (pRetErrstring, "IP Statistics Counters: ");
                STRCAT (pRetErrstring, "  IpInUnknownProtos");
                STRCAT (pRetErrstring, " exceeded the threshold \r\n");
                STRCAT (pRetErrstring, "Overall Health Status: Failed \r\n");
                STRCAT (pRetErrstring, "Overall Failure Type: Hardware \r\n");
                STRCAT (pRetErrstring,
                        "Overall Failure Reason: CBIT Test Failed \r\n");
                gu4IpCurrState = gu4IpCurrState | IP_IN_UNKNOWN_PROTO_FAIL;
            }
        }
        /* If the IpInDiscards  fails dispaly the CBIT failure as
           IpInDiscards failure */
        if ((IP_IN_DISCARDS_FAIL & u4IpStatsErrCode) != 0)
        {
            if ((gu4IpCurrState & IP_IN_DISCARDS_FAIL) == 0)
            {
                STRCAT (pRetErrstring, "IP Statistics Counters: ");
                STRCAT (pRetErrstring, "  IpInDiscards");
                STRCAT (pRetErrstring, " exceeded the threshold \r\n");
                STRCAT (pRetErrstring, "Overall Health Status: Failed \r\n");
                STRCAT (pRetErrstring, "Overall Failure Type: Hardware \r\n");
                STRCAT (pRetErrstring,
                        "Overall Failure Reason: CBIT Test Failed \r\n");
                gu4IpCurrState = gu4IpCurrState | IP_IN_DISCARDS_FAIL;
            }
        }
        /* If the IpOutDiscard  fails dispaly the CBIT failure as
           IpOutDiscard failure */
        if ((IP_OUT_DISCARDS_FAIL & u4IpStatsErrCode) != 0)
        {
            if ((gu4IpCurrState & IP_OUT_DISCARDS_FAIL) == 0)
            {
                STRCAT (pRetErrstring, "IP Statistics Counters: ");
                STRCAT (pRetErrstring, "  IpOutDiscards");
                STRCAT (pRetErrstring, " exceeded the threshold \r\n");
                STRCAT (pRetErrstring, "Overall Health Status: Failed \r\n");
                STRCAT (pRetErrstring, "Overall Failure Type: Hardware \r\n");
                STRCAT (pRetErrstring,
                        "Overall Failure Reason: CBIT Test Failed \r\n");
                gu4IpCurrState = gu4IpCurrState | IP_OUT_DISCARDS_FAIL;
            }

        }
        /* If the IpReasmFails  fails dispaly the CBIT failure as
           IpReasmFails failure */
        if ((IP_REASM_FAIL & u4IpStatsErrCode) != 0)
        {
            if ((gu4IpCurrState & IP_REASM_FAIL) == 0)
            {
                STRCAT (pRetErrstring, "IP Statistics Counters: ");
                STRCAT (pRetErrstring, "  IpReasmFails");
                STRCAT (pRetErrstring, " exceeded the threshold \r\n");
                STRCAT (pRetErrstring, "Overall Health Status: Failed \r\n");
                STRCAT (pRetErrstring, "Overall Failure Type: Hardware \r\n");
                STRCAT (pRetErrstring,
                        "Overall Failure Reason: CBIT Test Failed \r\n");
                gu4IpCurrState = gu4IpCurrState | IP_REASM_FAIL;
            }

        }
        /* If the IpFragFails fails dispaly the CBIT failure as
           IpFragFails failure */
        if ((IP_FRAG_FAIL & u4IpStatsErrCode) != 0)
        {
            if ((gu4IpCurrState & IP_FRAG_FAIL) == 0)
            {
                STRCAT (pRetErrstring, "IP Statistics Counters: ");
                STRCAT (pRetErrstring, "  IpFragFails");
                STRCAT (pRetErrstring, " exceeded the threshold \r\n");
                STRCAT (pRetErrstring, "Overall Health Status: Failed \r\n");
                STRCAT (pRetErrstring, "Overall Failure Type: Hardware \r\n");
                STRCAT (pRetErrstring,
                        "Overall Failure Reason: CBIT Test Failed \r\n");
                gu4IpCurrState = gu4IpCurrState | IP_FRAG_FAIL;
            }
        }
    }

    if ((ICMP_STATISTICS_COUNTER_FAIL & u4Errcode) != 0)
    {
        /* If the IcmpInErrors fails dispaly the CBIT failure as
           IcmpInErrors failure */
        if ((ICMP_IN_ERR_FAIL & u4IcmpStatsErrCode) != 0)
        {
            if ((gu4IcmpCurrState & ICMP_IN_ERR_FAIL) == 0)
            {
                STRCAT (pRetErrstring, "\r\n ICMP Statistics Counters: ");
                STRCAT (pRetErrstring, "  IcmpInErrors");
                STRCAT (pRetErrstring, " exceeded the threshold \r\n");
                STRCAT (pRetErrstring, "Overall Health Status: Failed \r\n");
                STRCAT (pRetErrstring, "Overall Failure Type: Hardware \r\n");
                STRCAT (pRetErrstring,
                        "Overall Failure Reason: CBIT Test Failed \r\n");
                gu4IcmpCurrState = gu4IcmpCurrState | ICMP_IN_ERR_FAIL;
            }

        }
        /* If the IcmpOutErrorsErrors  fails dispaly the CBIT failure as
           IcmpOutErrorsErrors failure */
        if ((ICMP_OUT_ERR_FAIL & u4IcmpStatsErrCode) != 0)
        {
            if ((gu4IcmpCurrState & ICMP_OUT_ERR_FAIL) == 0)
            {
                STRCAT (pRetErrstring, "\r\n ICMP Statistics Counters: ");
                STRCAT (pRetErrstring, "   IcmpOutErrors");
                STRCAT (pRetErrstring, " exceeded the threshold \r\n");
                STRCAT (pRetErrstring, "Overall Health Status: Failed \r\n");
                STRCAT (pRetErrstring, "Overall Failure Type: Hardware \r\n");
                STRCAT (pRetErrstring,
                        "Overall Failure Reason: CBIT Test Failed \r\n");
                gu4IcmpCurrState = gu4IcmpCurrState | ICMP_OUT_ERR_FAIL;
            }
        }

    }
    /* If the TcpInErrors fails dispaly the CBIT failure as
       TcpInErrors failure */
    if ((TCP_STATISTICS_COUNTER_FAIL & u4Errcode) != 0)
    {
        if ((gu4TcpCurrState & TCP_STATISTICS_COUNTER_FAIL) == 0)
        {
            STRCAT (pRetErrstring,
                    "\r\n TCP statistics counter exceeded the threshold \r\n");
            STRCAT (pRetErrstring, "Overall Health Status: Failed \r\n");
            STRCAT (pRetErrstring, "Overall Failure Type: Hardware \r\n");
            STRCAT (pRetErrstring,
                    "Overall Failure Reason: CBIT Test Failed \r\n");
            gu4TcpCurrState = gu4TcpCurrState | TCP_STATISTICS_COUNTER_FAIL;
        }

    }
    /* If the UdpInErrors fails dispaly the CBIT failure as
       UdpInErrors failure */
    if ((UDP_STATISTICS_COUNTER_FAIL & u4Errcode) != 0)
    {
        if ((gu4UdpCurrState & UDP_STATISTICS_COUNTER_FAIL) == 0)
        {
            STRCAT (pRetErrstring,
                    "\r\n UDP statistics counter exceeded the threshold \r\n");
            STRCAT (pRetErrstring, "Overall Health Status: Failed \r\n");
            STRCAT (pRetErrstring, "Overall Failure Type: Hardware \r\n");
            STRCAT (pRetErrstring,
                    "Overall Failure Reason: CBIT Test Failed \r\n");
            gu4UdpCurrState = gu4UdpCurrState | UDP_STATISTICS_COUNTER_FAIL;
        }
    }

    return;

}
#endif
