/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsbitelw.c,v 1.1.1.1 2011/07/21 14:06:33 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h" 
# include  "fssnmp.h" 
# include  "fsbitelw.h"
# include  "biteinc.h"
extern UINT1        gau1OperPrevState[200];
extern UINT1        gau1OperCurrState[200];
extern UINT1        gau1InDisPrevState[200];
extern UINT1        gau1InDisCurrState[200];
extern UINT1        gau1InErrPrevState[200];
extern UINT1        gau1InErrCurrState[200];
extern UINT1        gau1InUnKnwPrevState[200];
extern UINT1        gau1InUnKnwCurrState[200];
extern UINT1        gau1OutDisPrevState[200];
extern UINT1        gau1OutDisCurrState[200];
extern UINT1        gau1OutErrPrevState[200];
extern UINT1        gau1OutErrCurrState[200];
extern UINT4        gu4IpCurrState;
extern UINT4        gu4IcmpCurrState;
extern UINT4        gu4TcpCurrState;
extern UINT4        gu4UdpCurrState;
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLruOverallStatus
 Input       :  The Indices

                The Object 
                retValLruOverallStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLruOverallStatus (INT4 *pi4RetValLruOverallStatus)
{
    *pi4RetValLruOverallStatus = gBiteGlobalInfo.u4OverallStatus;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetLruPowerOnSelfTest
 Input       :  The Indices

                The Object 
                retValLruPowerOnSelfTest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLruPowerOnSelfTest (INT4 *pi4RetValLruPowerOnSelfTest)
{
    *pi4RetValLruPowerOnSelfTest = gBiteGlobalInfo.u4PowerOnSelfTest;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetLruContinuousSelfTest
 Input       :  The Indices

                The Object 
                retValLruContinuousSelfTest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLruContinuousSelfTest (INT4 *pi4RetValLruContinuousSelfTest)
{
    *pi4RetValLruContinuousSelfTest = gBiteGlobalInfo.u4ContSelfTest;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetLruFailureType
 Input       :  The Indices

                The Object 
                retValLruFailureType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruFailureType(INT4 *pi4RetValLruFailureType)
{
    *pi4RetValLruFailureType = gBiteGlobalInfo.u4FailureType;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetLruFailureReason
 Input       :  The Indices

                The Object 
                retValLruFailureReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruFailureReason(tSNMP_OCTET_STRING_TYPE * pRetValLruFailureReason)
{
    UINT1               au1RetValFailureReason[50];

    SPRINTF (au1RetValFailureReason, "%ld", gBiteGlobalInfo.u4FailureReason);
    pRetValLruFailureReason->i4_Length = STRLEN (au1RetValFailureReason);
    MEMCPY (pRetValLruFailureReason->pu1_OctetList, au1RetValFailureReason,
            pRetValLruFailureReason->i4_Length);
    return SNMP_SUCCESS;

}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLruSelfTestCommand
 Input       :  The Indices

                The Object 
                retValLruSelfTestCommand
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruSelfTestCommand(INT4 *pi4RetValLruSelfTestCommand)
{

    *pi4RetValLruSelfTestCommand = gBiteGlobalInfo.u4SelfTestCommand;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetLruSelfTestUpTime
 Input       :  The Indices

                The Object 
                retValLruSelfTestUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruSelfTestUpTime(INT4 *pi4RetValLruSelfTestUpTime)
{
    UINT4               u4CurrentTime = 0;
    if (gBiteGlobalInfo.u4SelfTestUpTime == 0)
    {
        *pi4RetValLruSelfTestUpTime = BITE_ZERO;
    }
    /* Get the current time in secs */
    else
    {
        OsixGetSysTime (&u4CurrentTime);
        u4CurrentTime = (u4CurrentTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
        *pi4RetValLruSelfTestUpTime = u4CurrentTime - (gBiteGlobalInfo.
                                                       u4SelfTestUpTime);
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetLruLastResultSelfTest
 Input       :  The Indices

                The Object 
                retValLruLastResultSelfTest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruLastResultSelfTest(INT4 *pi4RetValLruLastResultSelfTest)
{
    *pi4RetValLruLastResultSelfTest = gBiteGlobalInfo.u4LastResultSelfTest;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetLruSelfTestReturnCode
 Input       :  The Indices

                The Object 
                retValLruSelfTestReturnCode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruSelfTestReturnCode(tSNMP_OCTET_STRING_TYPE * pRetValLruSelfTestReturnCode)
{
    UINT1               au1RetValSelfTestRetCode[50];

    SPRINTF (au1RetValSelfTestRetCode, "%ld",
             gBiteGlobalInfo.u4SelfTestReturnCode);

    pRetValLruSelfTestReturnCode->i4_Length = STRLEN (au1RetValSelfTestRetCode);
    MEMCPY (pRetValLruSelfTestReturnCode->pu1_OctetList,
            au1RetValSelfTestRetCode, pRetValLruSelfTestReturnCode->i4_Length);

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetLruContinuousSelfTestCommand
 Input       :  The Indices

                The Object 
                retValLruContinuousSelfTestCommand
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruContinuousSelfTestCommand(INT4 *pi4RetValLruContinuousSelfTestCommand)
{
    *pi4RetValLruContinuousSelfTestCommand = gBiteGlobalInfo.u4ContSelfTestCmd;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetLruThresholdIpInAddrErrors
 Input       :  The Indices

                The Object 
                retValLruThresholdIpInAddrErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruThresholdIpInAddrErrors(INT4 *pi4RetValLruThresholdIpInAddrErrors)
{
    *pi4RetValLruThresholdIpInAddrErrors = gBiteGlobalInfo.
        u4ThresholdIpInAddrErrors;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetLruThresholdIpInHdrErrors
 Input       :  The Indices

                The Object 
                retValLruThresholdIpInHdrErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruThresholdIpInHdrErrors(INT4 *pi4RetValLruThresholdIpInHdrErrors)
{
    *pi4RetValLruThresholdIpInHdrErrors = gBiteGlobalInfo.
        u4ThresholdIpInHdrErrors;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetLruThresholdIpInUnknownProtos
 Input       :  The Indices

                The Object 
                retValLruThresholdIpInUnknownProtos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruThresholdIpInUnknownProtos(INT4 *pi4RetValLruThresholdIpInUnknownProtos)
{
    *pi4RetValLruThresholdIpInUnknownProtos = gBiteGlobalInfo.
        u4ThresholdIpInUnknownProtos;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetLruThresholdIpInDiscards
 Input       :  The Indices

                The Object 
                retValLruThresholdIpInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruThresholdIpInDiscards(INT4 *pi4RetValLruThresholdIpInDiscards)
{
    *pi4RetValLruThresholdIpInDiscards = gBiteGlobalInfo.
        u4ThresholdIpInDiscards;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetLruThresholdIpOutDiscards
 Input       :  The Indices

                The Object 
                retValLruThresholdIpOutDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruThresholdIpOutDiscards(INT4 *pi4RetValLruThresholdIpOutDiscards)
{
    *pi4RetValLruThresholdIpOutDiscards = gBiteGlobalInfo.
        u4ThresholdIpOutDiscards;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetLruThresholdIpReasmFails
 Input       :  The Indices

                The Object 
                retValLruThresholdIpReasmFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruThresholdIpReasmFails(INT4 *pi4RetValLruThresholdIpReasmFails)
{
    *pi4RetValLruThresholdIpReasmFails = gBiteGlobalInfo.
        u4ThresholdIpReasmFails;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetLruThresholdIpFragFails
 Input       :  The Indices

                The Object 
                retValLruThresholdIpFragFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruThresholdIpFragFails(INT4 *pi4RetValLruThresholdIpFragFails)
{
    *pi4RetValLruThresholdIpFragFails = gBiteGlobalInfo.u4ThresholdIpFragFails;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetLruThresholdIcmpInErrors
 Input       :  The Indices

                The Object 
                retValLruThresholdIcmpInErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruThresholdIcmpInErrors(INT4 *pi4RetValLruThresholdIcmpInErrors)
{
    *pi4RetValLruThresholdIcmpInErrors = gBiteGlobalInfo.
        u4ThresholdIcmpInErrors;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetLruThresholdIcmpOutErrors
 Input       :  The Indices

                The Object 
                retValLruThresholdIcmpOutErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruThresholdIcmpOutErrors(INT4 *pi4RetValLruThresholdIcmpOutErrors)
{
    *pi4RetValLruThresholdIcmpOutErrors = gBiteGlobalInfo.
        u4ThresholdIcmpOutErrors;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetLruThresholdTcpInErrors
 Input       :  The Indices

                The Object 
                retValLruThresholdTcpInErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruThresholdTcpInErrors(INT4 *pi4RetValLruThresholdTcpInErrors)
{
    *pi4RetValLruThresholdTcpInErrors = gBiteGlobalInfo.u4ThresholdTcpInErrors;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetLruThresholdUdpInErrors
 Input       :  The Indices

                The Object 
                retValLruThresholdUdpInErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruThresholdUdpInErrors(INT4 *pi4RetValLruThresholdUdpInErrors)
{

    *pi4RetValLruThresholdUdpInErrors = gBiteGlobalInfo.u4ThresholdUdpInErrors;
    return SNMP_SUCCESS;

}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLruSelfTestCommand
 Input       :  The Indices

                The Object 
                setValLruSelfTestCommand
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetLruSelfTestCommand(INT4 i4SetValLruSelfTestCommand)
{
    BiteApiUnLock ();
    if (gBiteGlobalInfo.u4SelfTestCommand != (UINT4) i4SetValLruSelfTestCommand)
    {
        if (i4SetValLruSelfTestCommand == BITE_IBIT_TEST_ON)
        {
            if (gBiteGlobalInfo.u4SelfTestCommand != BITE_IBIT_TEST_INPRGS)
            {
                if (OsixSendEvent (SELF, (const UINT1 *) BITE_TASK,
                                   BITE_IBIT_TEST_EVT) != OSIX_SUCCESS)
                {
                    gBiteGlobalInfo.u4SelfTestCommand = BITE_IBIT_TEST_OFF;
                    return SNMP_FAILURE;
                }
            }
        }
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetLruContinuousSelfTestCommand
 Input       :  The Indices

                The Object 
                setValLruContinuousSelfTestCommand
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetLruContinuousSelfTestCommand(INT4 i4SetValLruContinuousSelfTestCommand)
{

    if (gBiteGlobalInfo.u4ContSelfTestCmd !=
        (UINT4) i4SetValLruContinuousSelfTestCommand)
    {
        if (i4SetValLruContinuousSelfTestCommand == BITE_CBIT_TEST_ON)
        {
            if (gBiteGlobalInfo.u4ContSelfTestCmd != BITE_CBIT_TEST_INPRGS)
            {
                BitetestStartCbitTest ();
                return SNMP_SUCCESS;
            }
        }
        if (i4SetValLruContinuousSelfTestCommand == BITE_CBIT_TEST_OFF)
        {
            BitetestStopCbitTest ();
        }

    }

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetLruThresholdIpInAddrErrors
 Input       :  The Indices

                The Object 
                setValLruThresholdIpInAddrErrors
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetLruThresholdIpInAddrErrors(INT4 i4SetValLruThresholdIpInAddrErrors)
{
    gBiteGlobalInfo.u4ThresholdIpInAddrErrors =
        i4SetValLruThresholdIpInAddrErrors;
    BitetestResetThreshold (IP_STATISTICS_COUNTER_FAIL, IP_IN_ADDR_FAIL, 0);
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetLruThresholdIpInHdrErrors
 Input       :  The Indices

                The Object 
                setValLruThresholdIpInHdrErrors
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetLruThresholdIpInHdrErrors(INT4 i4SetValLruThresholdIpInHdrErrors)
{
    gBiteGlobalInfo.u4ThresholdIpInHdrErrors =
        i4SetValLruThresholdIpInHdrErrors;
    BitetestResetThreshold (IP_STATISTICS_COUNTER_FAIL, IP_IN_HDR_ERR_FAIL, 0);
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetLruThresholdIpInUnknownProtos
 Input       :  The Indices

                The Object 
                setValLruThresholdIpInUnknownProtos
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetLruThresholdIpInUnknownProtos(INT4 i4SetValLruThresholdIpInUnknownProtos)
{
    gBiteGlobalInfo.u4ThresholdIpInUnknownProtos =
        i4SetValLruThresholdIpInUnknownProtos;
    BitetestResetThreshold (IP_STATISTICS_COUNTER_FAIL,
                            IP_IN_UNKNOWN_PROTO_FAIL, 0);

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetLruThresholdIpInDiscards
 Input       :  The Indices

                The Object 
                setValLruThresholdIpInDiscards
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetLruThresholdIpInDiscards(INT4 i4SetValLruThresholdIpInDiscards)
{
    gBiteGlobalInfo.u4ThresholdIpInDiscards = i4SetValLruThresholdIpInDiscards;
    BitetestResetThreshold (IP_STATISTICS_COUNTER_FAIL, IP_IN_DISCARDS_FAIL, 0);

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetLruThresholdIpOutDiscards
 Input       :  The Indices

                The Object 
                setValLruThresholdIpOutDiscards
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetLruThresholdIpOutDiscards(INT4 i4SetValLruThresholdIpOutDiscards)
{
    gBiteGlobalInfo.u4ThresholdIpOutDiscards =
        i4SetValLruThresholdIpOutDiscards;
    BitetestResetThreshold (IP_STATISTICS_COUNTER_FAIL, IP_OUT_DISCARDS_FAIL,
                            0);

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetLruThresholdIpReasmFails
 Input       :  The Indices

                The Object 
                setValLruThresholdIpReasmFails
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetLruThresholdIpReasmFails(INT4 i4SetValLruThresholdIpReasmFails)
{
    gBiteGlobalInfo.u4ThresholdIpReasmFails = i4SetValLruThresholdIpReasmFails;
    BitetestResetThreshold (IP_STATISTICS_COUNTER_FAIL, IP_REASM_FAIL, 0);

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetLruThresholdIpFragFails
 Input       :  The Indices

                The Object 
                setValLruThresholdIpFragFails
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetLruThresholdIpFragFails(INT4 i4SetValLruThresholdIpFragFails)
{
    gBiteGlobalInfo.u4ThresholdIpFragFails = i4SetValLruThresholdIpFragFails;
    BitetestResetThreshold (IP_STATISTICS_COUNTER_FAIL, IP_FRAG_FAIL, 0);

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetLruThresholdIcmpInErrors
 Input       :  The Indices

                The Object 
                setValLruThresholdIcmpInErrors
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetLruThresholdIcmpInErrors(INT4 i4SetValLruThresholdIcmpInErrors)
{
    gBiteGlobalInfo.u4ThresholdIcmpInErrors = i4SetValLruThresholdIcmpInErrors;
    BitetestResetThreshold (ICMP_STATISTICS_COUNTER_FAIL, ICMP_IN_ERR_FAIL, 0);
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetLruThresholdIcmpOutErrors
 Input       :  The Indices

                The Object 
                setValLruThresholdIcmpOutErrors
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetLruThresholdIcmpOutErrors(INT4 i4SetValLruThresholdIcmpOutErrors)
{
    gBiteGlobalInfo.u4ThresholdIcmpOutErrors =
        i4SetValLruThresholdIcmpOutErrors;
    BitetestResetThreshold (ICMP_STATISTICS_COUNTER_FAIL, ICMP_OUT_ERR_FAIL, 0);

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetLruThresholdTcpInErrors
 Input       :  The Indices

                The Object 
                setValLruThresholdTcpInErrors
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetLruThresholdTcpInErrors(INT4 i4SetValLruThresholdTcpInErrors)
{
    gBiteGlobalInfo.u4ThresholdTcpInErrors = i4SetValLruThresholdTcpInErrors;
    BitetestResetThreshold (TCP_STATISTICS_COUNTER_FAIL,
                            TCP_STATISTICS_COUNTER_FAIL, 0);
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetLruThresholdUdpInErrors
 Input       :  The Indices

                The Object 
                setValLruThresholdUdpInErrors
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetLruThresholdUdpInErrors(INT4 i4SetValLruThresholdUdpInErrors)
{
    gBiteGlobalInfo.u4ThresholdUdpInErrors = i4SetValLruThresholdUdpInErrors;
    BitetestResetThreshold (UDP_STATISTICS_COUNTER_FAIL,
                            UDP_STATISTICS_COUNTER_FAIL, 0);
    return SNMP_SUCCESS;

}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LruSelfTestCommand
 Input       :  The Indices

                The Object 
                testValLruSelfTestCommand
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2LruSelfTestCommand(UINT4 *pu4ErrorCode , INT4 i4TestValLruSelfTestCommand)
{
    if (i4TestValLruSelfTestCommand != BITE_IBIT_TEST_ON)
    {
        CLI_SET_ERR (BITE_CLI_INVALID_IBIT_COMMAND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2LruContinuousSelfTestCommand
 Input       :  The Indices

                The Object 
                testValLruContinuousSelfTestCommand
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2LruContinuousSelfTestCommand(UINT4 *pu4ErrorCode , INT4 i4TestValLruContinuousSelfTestCommand)
{
    if (i4TestValLruContinuousSelfTestCommand != BITE_CBIT_TEST_ON &&
        i4TestValLruContinuousSelfTestCommand != BITE_CBIT_TEST_OFF)
    {
        CLI_SET_ERR (BITE_CLI_INVALID_CBIT_COMMAND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2LruThresholdIpInAddrErrors
 Input       :  The Indices

                The Object 
                testValLruThresholdIpInAddrErrors
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2LruThresholdIpInAddrErrors(UINT4 *pu4ErrorCode , INT4 i4TestValLruThresholdIpInAddrErrors)
{
    if ((i4TestValLruThresholdIpInAddrErrors < BITE_MIN_THRESHOLD_VALUE) ||
        (i4TestValLruThresholdIpInAddrErrors > BITE_MAX_THRESHOLD_VALUE))
    {
        CLI_SET_ERR (BITE_CLI_INVALID_THRESHOLD_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2LruThresholdIpInHdrErrors
 Input       :  The Indices

                The Object 
                testValLruThresholdIpInHdrErrors
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2LruThresholdIpInHdrErrors(UINT4 *pu4ErrorCode , INT4 i4TestValLruThresholdIpInHdrErrors)
{
    if ((i4TestValLruThresholdIpInHdrErrors < BITE_MIN_THRESHOLD_VALUE) ||
        (i4TestValLruThresholdIpInHdrErrors > BITE_MAX_THRESHOLD_VALUE))
    {
        CLI_SET_ERR (BITE_CLI_INVALID_THRESHOLD_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2LruThresholdIpInUnknownProtos
 Input       :  The Indices

                The Object 
                testValLruThresholdIpInUnknownProtos
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2LruThresholdIpInUnknownProtos(UINT4 *pu4ErrorCode , INT4 i4TestValLruThresholdIpInUnknownProtos)
{
    if ((i4TestValLruThresholdIpInUnknownProtos < BITE_MIN_THRESHOLD_VALUE) ||
        (i4TestValLruThresholdIpInUnknownProtos > BITE_MAX_THRESHOLD_VALUE))
    {
        CLI_SET_ERR (BITE_CLI_INVALID_THRESHOLD_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2LruThresholdIpInDiscards
 Input       :  The Indices

                The Object 
                testValLruThresholdIpInDiscards
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2LruThresholdIpInDiscards(UINT4 *pu4ErrorCode , INT4 i4TestValLruThresholdIpInDiscards)
{

    if ((i4TestValLruThresholdIpInDiscards < BITE_MIN_THRESHOLD_VALUE) ||
        (i4TestValLruThresholdIpInDiscards > BITE_MAX_THRESHOLD_VALUE))
    {
        CLI_SET_ERR (BITE_CLI_INVALID_THRESHOLD_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2LruThresholdIpOutDiscards
 Input       :  The Indices

                The Object 
                testValLruThresholdIpOutDiscards
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2LruThresholdIpOutDiscards(UINT4 *pu4ErrorCode , INT4 i4TestValLruThresholdIpOutDiscards)
{
    if ((i4TestValLruThresholdIpOutDiscards < BITE_MIN_THRESHOLD_VALUE) ||
        (i4TestValLruThresholdIpOutDiscards > BITE_MAX_THRESHOLD_VALUE))
    {
        CLI_SET_ERR (BITE_CLI_INVALID_THRESHOLD_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2LruThresholdIpReasmFails
 Input       :  The Indices

                The Object 
                testValLruThresholdIpReasmFails
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2LruThresholdIpReasmFails(UINT4 *pu4ErrorCode , INT4 i4TestValLruThresholdIpReasmFails)
{
    if ((i4TestValLruThresholdIpReasmFails < BITE_MIN_THRESHOLD_VALUE) ||
        (i4TestValLruThresholdIpReasmFails > BITE_MAX_THRESHOLD_VALUE))
    {
        CLI_SET_ERR (BITE_CLI_INVALID_THRESHOLD_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2LruThresholdIpFragFails
 Input       :  The Indices

                The Object 
                testValLruThresholdIpFragFails
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2LruThresholdIpFragFails(UINT4 *pu4ErrorCode , INT4 i4TestValLruThresholdIpFragFails)
{
    if ((i4TestValLruThresholdIpFragFails < BITE_MIN_THRESHOLD_VALUE) ||
        (i4TestValLruThresholdIpFragFails > BITE_MAX_THRESHOLD_VALUE))
    {
        CLI_SET_ERR (BITE_CLI_INVALID_THRESHOLD_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2LruThresholdIcmpInErrors
 Input       :  The Indices

                The Object 
                testValLruThresholdIcmpInErrors
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2LruThresholdIcmpInErrors(UINT4 *pu4ErrorCode , INT4 i4TestValLruThresholdIcmpInErrors)
{
    if ((i4TestValLruThresholdIcmpInErrors < BITE_MIN_THRESHOLD_VALUE) ||
        (i4TestValLruThresholdIcmpInErrors > BITE_MAX_THRESHOLD_VALUE))
    {
        CLI_SET_ERR (BITE_CLI_INVALID_THRESHOLD_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2LruThresholdIcmpOutErrors
 Input       :  The Indices

                The Object 
                testValLruThresholdIcmpOutErrors
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2LruThresholdIcmpOutErrors(UINT4 *pu4ErrorCode , INT4 i4TestValLruThresholdIcmpOutErrors)
{
    if ((i4TestValLruThresholdIcmpOutErrors < BITE_MIN_THRESHOLD_VALUE) ||
        (i4TestValLruThresholdIcmpOutErrors > BITE_MAX_THRESHOLD_VALUE))
    {
        CLI_SET_ERR (BITE_CLI_INVALID_THRESHOLD_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2LruThresholdTcpInErrors
 Input       :  The Indices

                The Object 
                testValLruThresholdTcpInErrors
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2LruThresholdTcpInErrors(UINT4 *pu4ErrorCode , INT4 i4TestValLruThresholdTcpInErrors)
{
    if ((i4TestValLruThresholdTcpInErrors < BITE_MIN_THRESHOLD_VALUE) ||
        (i4TestValLruThresholdTcpInErrors > BITE_MAX_THRESHOLD_VALUE))
    {
        CLI_SET_ERR (BITE_CLI_INVALID_THRESHOLD_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2LruThresholdUdpInErrors
 Input       :  The Indices

                The Object 
                testValLruThresholdUdpInErrors
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2LruThresholdUdpInErrors(UINT4 *pu4ErrorCode , INT4 i4TestValLruThresholdUdpInErrors)
{
    if ((i4TestValLruThresholdUdpInErrors < BITE_MIN_THRESHOLD_VALUE) ||
        (i4TestValLruThresholdUdpInErrors > BITE_MAX_THRESHOLD_VALUE))
    {
        CLI_SET_ERR (BITE_CLI_INVALID_THRESHOLD_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LruSelfTestCommand
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2LruSelfTestCommand(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2LruContinuousSelfTestCommand
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2LruContinuousSelfTestCommand(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2LruThresholdIpInAddrErrors
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2LruThresholdIpInAddrErrors(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2LruThresholdIpInHdrErrors
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2LruThresholdIpInHdrErrors(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2LruThresholdIpInUnknownProtos
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2LruThresholdIpInUnknownProtos(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2LruThresholdIpInDiscards
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2LruThresholdIpInDiscards(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2LruThresholdIpOutDiscards
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2LruThresholdIpOutDiscards(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2LruThresholdIpReasmFails
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2LruThresholdIpReasmFails(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2LruThresholdIpFragFails
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2LruThresholdIpFragFails(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2LruThresholdIcmpInErrors
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2LruThresholdIcmpInErrors(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2LruThresholdIcmpOutErrors
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2LruThresholdIcmpOutErrors(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2LruThresholdTcpInErrors
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2LruThresholdTcpInErrors(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2LruThresholdUdpInErrors
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2LruThresholdUdpInErrors(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : LruIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLruIfTable
 Input       :  The Indices
                LruIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceLruIfTable(INT4 i4LruIfIndex)
{
if ((i4LruIfIndex < BITE_ONE) || (i4LruIfIndex > gu4MaxIfcs))
    {
        CLI_SET_ERR (BITE_CLI_ERR_IF_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexLruIfTable
 Input       :  The Indices
                LruIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexLruIfTable(INT4 *pi4LruIfIndex)
{
tBiteIfInfo        *pBiteIfInfo = NULL;
    UINT4               u4IfNum = 0;

    for (u4IfNum = 1; u4IfNum <= gu4MaxIfcs; u4IfNum++)
    {
        pBiteIfInfo = BiteIfFind (u4IfNum);
        if (pBiteIfInfo != NULL)
        {
            *pi4LruIfIndex = (INT4) pBiteIfInfo->u4IfIndex;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}
/****************************************************************************
 Function    :  nmhGetNextIndexLruIfTable
 Input       :  The Indices
                LruIfIndex
                nextLruIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexLruIfTable(INT4 i4LruIfIndex ,INT4 *pi4NextLruIfIndex)
{tBiteIfInfo        *pBiteIfInfo = NULL;
    UINT4               u4IfNum = 0;

    for (u4IfNum = i4LruIfIndex + 1; u4IfNum <= gu4MaxIfcs; u4IfNum++)
    {
        pBiteIfInfo = BiteIfFind (u4IfNum);
        if (pBiteIfInfo != NULL)
        {
            *pi4NextLruIfIndex = (INT4) pBiteIfInfo->u4IfIndex;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLruIfThresholdOperStatus
 Input       :  The Indices
                LruIfIndex

                The Object 
                retValLruIfThresholdOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruIfThresholdOperStatus(INT4 i4LruIfIndex , INT4 *pi4RetValLruIfThresholdOperStatus)
{
    tBiteIfInfo        *pBiteIfInfo = NULL;
    pBiteIfInfo = BiteIfFind (i4LruIfIndex);
    if (pBiteIfInfo != NULL)
    {
        *pi4RetValLruIfThresholdOperStatus =
            pBiteIfInfo->u4ThresholdIfOperStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}
/****************************************************************************
 Function    :  nmhGetLruIfThresholdInDiscards
 Input       :  The Indices
                LruIfIndex

                The Object 
                retValLruIfThresholdInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruIfThresholdInDiscards(INT4 i4LruIfIndex , INT4 *pi4RetValLruIfThresholdInDiscards)
{
    tBiteIfInfo        *pBiteIfInfo = NULL;
    pBiteIfInfo = BiteIfFind (i4LruIfIndex);
    if (pBiteIfInfo != NULL)
    {
        *pi4RetValLruIfThresholdInDiscards =
            pBiteIfInfo->u4IfThresholdInDiscards;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}
/****************************************************************************
 Function    :  nmhGetLruIfThresholdInErrors
 Input       :  The Indices
                LruIfIndex

                The Object 
                retValLruIfThresholdInErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruIfThresholdInErrors(INT4 i4LruIfIndex , INT4 *pi4RetValLruIfThresholdInErrors)
{
    tBiteIfInfo        *pBiteIfInfo = NULL;
    pBiteIfInfo = BiteIfFind (i4LruIfIndex);
    if (pBiteIfInfo != NULL)
    {
        *pi4RetValLruIfThresholdInErrors = pBiteIfInfo->u4IfThresholdInErrors;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}
/****************************************************************************
 Function    :  nmhGetLruIfThresholdInUnknownProtos
 Input       :  The Indices
                LruIfIndex

                The Object 
                retValLruIfThresholdInUnknownProtos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruIfThresholdInUnknownProtos(INT4 i4LruIfIndex , INT4 *pi4RetValLruIfThresholdInUnknownProtos)
{
    tBiteIfInfo        *pBiteIfInfo = NULL;
    pBiteIfInfo = BiteIfFind (i4LruIfIndex);
    if (pBiteIfInfo != NULL)
    {
        *pi4RetValLruIfThresholdInUnknownProtos =
            pBiteIfInfo->u4IfThresholdInUnknownProtos;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}
/****************************************************************************
 Function    :  nmhGetLruIfThresholdOutDiscards
 Input       :  The Indices
                LruIfIndex

                The Object 
                retValLruIfThresholdOutDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruIfThresholdOutDiscards(INT4 i4LruIfIndex , INT4 *pi4RetValLruIfThresholdOutDiscards)
{
    tBiteIfInfo        *pBiteIfInfo = NULL;
    pBiteIfInfo = BiteIfFind (i4LruIfIndex);
    if (pBiteIfInfo != NULL)
    {
        *pi4RetValLruIfThresholdOutDiscards =
            pBiteIfInfo->u4IfThresholdOutDiscards;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}
/****************************************************************************
 Function    :  nmhGetLruIfThresholdOutErrors
 Input       :  The Indices
                LruIfIndex

                The Object 
                retValLruIfThresholdOutErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruIfThresholdOutErrors(INT4 i4LruIfIndex , INT4 *pi4RetValLruIfThresholdOutErrors)
{
    tBiteIfInfo        *pBiteIfInfo = NULL;
    pBiteIfInfo = BiteIfFind (i4LruIfIndex);
    if (pBiteIfInfo != NULL)
    {
        *pi4RetValLruIfThresholdOutErrors = pBiteIfInfo->u4IfThresholdOutErrors;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}
/****************************************************************************
 Function    :  nmhGetLruIfContinuousSelfTestStatus
 Input       :  The Indices
                LruIfIndex

                The Object 
                retValLruIfContinuousSelfTestStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruIfContinuousSelfTestStatus(INT4 i4LruIfIndex , INT4 *pi4RetValLruIfContinuousSelfTestStatus)
{
    tBiteIfInfo        *pBiteIfInfo = NULL;

    pBiteIfInfo = BiteIfFind (i4LruIfIndex);
    if (pBiteIfInfo != NULL)
    {
        *pi4RetValLruIfContinuousSelfTestStatus =
            pBiteIfInfo->u4IfContSelfTestStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}
/****************************************************************************
 Function    :  nmhGetLruIfContinuousSelfTestReturnCode
 Input       :  The Indices
                LruIfIndex

                The Object 
                retValLruIfContinuousSelfTestReturnCode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruIfContinuousSelfTestReturnCode(INT4 i4LruIfIndex , tSNMP_OCTET_STRING_TYPE * pRetValLruIfContinuousSelfTestReturnCode)
{
    tBiteIfInfo        *pBiteIfInfo;
    UINT1               au1RetValContSelfTest[50];
    pBiteIfInfo = BiteIfFind (i4LruIfIndex);
    if (pBiteIfInfo != NULL)
    {
        SPRINTF (au1RetValContSelfTest, "%ld",
                 pBiteIfInfo->u4IfContSelfTestRetCode);
        pRetValLruIfContinuousSelfTestReturnCode->i4_Length =
            STRLEN (au1RetValContSelfTest);
        MEMCPY (pRetValLruIfContinuousSelfTestReturnCode->pu1_OctetList,
                au1RetValContSelfTest,
                pRetValLruIfContinuousSelfTestReturnCode->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}
/****************************************************************************
 Function    :  nmhGetLruIfRowStatus
 Input       :  The Indices
                LruIfIndex

                The Object 
                retValLruIfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruIfRowStatus(INT4 i4LruIfIndex , INT4 *pi4RetValLruIfRowStatus)
{
    tBiteIfInfo        *pBiteIfInfo = NULL;
    pBiteIfInfo = BiteIfFind (i4LruIfIndex);
    if (pBiteIfInfo != NULL)
    {
        *pi4RetValLruIfRowStatus = pBiteIfInfo->u4RowStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLruIfThresholdOperStatus
 Input       :  The Indices
                LruIfIndex

                The Object 
                setValLruIfThresholdOperStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetLruIfThresholdOperStatus(INT4 i4LruIfIndex , INT4 i4SetValLruIfThresholdOperStatus)
{
    tBiteIfInfo        *pBiteIfInfo = NULL;
    pBiteIfInfo = BiteIfFind (i4LruIfIndex);
    if (pBiteIfInfo != NULL)
    {
        pBiteIfInfo->u4ThresholdIfOperStatus = i4SetValLruIfThresholdOperStatus;
        BitetestResetThreshold (INTERFACE_STATISTICS_COUNTER_FAIL,
                                INTERFACE_OPER_STATUS_FAIL, i4LruIfIndex);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}
/****************************************************************************
 Function    :  nmhSetLruIfThresholdInDiscards
 Input       :  The Indices
                LruIfIndex

                The Object 
                setValLruIfThresholdInDiscards
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetLruIfThresholdInDiscards(INT4 i4LruIfIndex , INT4 i4SetValLruIfThresholdInDiscards)
{
    tBiteIfInfo        *pBiteIfInfo = NULL;
    pBiteIfInfo = BiteIfFind (i4LruIfIndex);
    if (pBiteIfInfo != NULL)
    {

        pBiteIfInfo->u4IfThresholdInDiscards = i4SetValLruIfThresholdInDiscards;
        BitetestResetThreshold (INTERFACE_STATISTICS_COUNTER_FAIL,
                                INTERFACE_IN_DISCARDS_FAIL, i4LruIfIndex);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}
/****************************************************************************
 Function    :  nmhSetLruIfThresholdInErrors
 Input       :  The Indices
                LruIfIndex

                The Object 
                setValLruIfThresholdInErrors
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetLruIfThresholdInErrors(INT4 i4LruIfIndex , INT4 i4SetValLruIfThresholdInErrors)
{
    tBiteIfInfo        *pBiteIfInfo = NULL;
    pBiteIfInfo = BiteIfFind (i4LruIfIndex);
    if (pBiteIfInfo != NULL)
    {

        pBiteIfInfo->u4IfThresholdInErrors = i4SetValLruIfThresholdInErrors;
        BitetestResetThreshold (INTERFACE_STATISTICS_COUNTER_FAIL,
                                INTERFACE_IN_ERRORS_FAIL, i4LruIfIndex);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}
/****************************************************************************
 Function    :  nmhSetLruIfThresholdInUnknownProtos
 Input       :  The Indices
                LruIfIndex

                The Object 
                setValLruIfThresholdInUnknownProtos
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetLruIfThresholdInUnknownProtos(INT4 i4LruIfIndex , INT4 i4SetValLruIfThresholdInUnknownProtos)
{
    tBiteIfInfo        *pBiteIfInfo = NULL;
    pBiteIfInfo = BiteIfFind (i4LruIfIndex);
    if (pBiteIfInfo != NULL)
    {

        pBiteIfInfo->u4IfThresholdInUnknownProtos =
            i4SetValLruIfThresholdInUnknownProtos;
        BitetestResetThreshold (INTERFACE_STATISTICS_COUNTER_FAIL,
                                INTERFACE_IN_UNKNOWN_PROTOS_FAIL, i4LruIfIndex);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhSetLruIfThresholdOutDiscards
 Input       :  The Indices
                LruIfIndex

                The Object 
                setValLruIfThresholdOutDiscards
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetLruIfThresholdOutDiscards(INT4 i4LruIfIndex , INT4 i4SetValLruIfThresholdOutDiscards)
{
    tBiteIfInfo        *pBiteIfInfo = NULL;
    pBiteIfInfo = BiteIfFind (i4LruIfIndex);
    if (pBiteIfInfo != NULL)
    {

        pBiteIfInfo->u4IfThresholdOutDiscards =
            i4SetValLruIfThresholdOutDiscards;
        BitetestResetThreshold (INTERFACE_STATISTICS_COUNTER_FAIL,
                                INTERFACE_OUT_DISCARDS_FAIL, i4LruIfIndex);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}
/****************************************************************************
 Function    :  nmhSetLruIfThresholdOutErrors
 Input       :  The Indices
                LruIfIndex

                The Object 
                setValLruIfThresholdOutErrors
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetLruIfThresholdOutErrors(INT4 i4LruIfIndex , INT4 i4SetValLruIfThresholdOutErrors)
{
    tBiteIfInfo        *pBiteIfInfo = NULL;
    pBiteIfInfo = BiteIfFind (i4LruIfIndex);
    if (pBiteIfInfo != NULL)
    {
        pBiteIfInfo->u4IfThresholdOutErrors = i4SetValLruIfThresholdOutErrors;
        BitetestResetThreshold (INTERFACE_STATISTICS_COUNTER_FAIL,
                                INTERFACE_OUT_ERRORS_FAIL, i4LruIfIndex);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}
/****************************************************************************
 Function    :  nmhSetLruIfRowStatus
 Input       :  The Indices
                LruIfIndex

                The Object 
                setValLruIfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetLruIfRowStatus(INT4 i4LruIfIndex , INT4 i4SetValLruIfRowStatus)
{
    tBiteIfInfo        *pBiteIfInfo = NULL;

    switch (i4SetValLruIfRowStatus)
    {
        case CREATE_AND_GO:
            pBiteIfInfo = BiteIfCreate (i4LruIfIndex);
            if (pBiteIfInfo != NULL)
            {
                pBiteIfInfo->u4RowStatus = ACTIVE;
            }
            else
            {
                return SNMP_FAILURE;
            }
            break;

        case DESTROY:
            if (BiteIfDelete (i4LruIfIndex) == OSIX_FAILURE)
            {

                return SNMP_FAILURE;

            }
            break;
        default:
            /* only createandgo and destroy is supported.
               All other values are not suported */
            return SNMP_FAILURE;
            break;
    }

    return SNMP_SUCCESS;

}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LruIfThresholdOperStatus
 Input       :  The Indices
                LruIfIndex

                The Object 
                testValLruIfThresholdOperStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2LruIfThresholdOperStatus(UINT4 *pu4ErrorCode , INT4 i4LruIfIndex , INT4 i4TestValLruIfThresholdOperStatus)
{
    UNUSED_PARAM (i4LruIfIndex);
    if ((i4TestValLruIfThresholdOperStatus < BITE_MIN_THRESHOLD_VALUE) ||
        (i4TestValLruIfThresholdOperStatus > BITE_MAX_THRESHOLD_VALUE))
    {
        CLI_SET_ERR (BITE_CLI_INVALID_THRESHOLD_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2LruIfThresholdInDiscards
 Input       :  The Indices
                LruIfIndex

                The Object 
                testValLruIfThresholdInDiscards
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2LruIfThresholdInDiscards(UINT4 *pu4ErrorCode , INT4 i4LruIfIndex , INT4 i4TestValLruIfThresholdInDiscards)
{
    UNUSED_PARAM (i4LruIfIndex);

    if ((i4TestValLruIfThresholdInDiscards < BITE_MIN_THRESHOLD_VALUE) ||
        (i4TestValLruIfThresholdInDiscards > BITE_MAX_THRESHOLD_VALUE))
    {
        CLI_SET_ERR (BITE_CLI_INVALID_THRESHOLD_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2LruIfThresholdInErrors
 Input       :  The Indices
                LruIfIndex

                The Object 
                testValLruIfThresholdInErrors
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2LruIfThresholdInErrors(UINT4 *pu4ErrorCode , INT4 i4LruIfIndex , INT4 i4TestValLruIfThresholdInErrors)
{
    UNUSED_PARAM (i4LruIfIndex);

    if ((i4TestValLruIfThresholdInErrors < BITE_MIN_THRESHOLD_VALUE) ||
        (i4TestValLruIfThresholdInErrors > BITE_MAX_THRESHOLD_VALUE))
    {
        CLI_SET_ERR (BITE_CLI_INVALID_THRESHOLD_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2LruIfThresholdInUnknownProtos
 Input       :  The Indices
                LruIfIndex

                The Object 
                testValLruIfThresholdInUnknownProtos
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2LruIfThresholdInUnknownProtos(UINT4 *pu4ErrorCode , INT4 i4LruIfIndex , INT4 i4TestValLruIfThresholdInUnknownProtos)
{
    UNUSED_PARAM (i4LruIfIndex);

    if ((i4TestValLruIfThresholdInUnknownProtos < BITE_MIN_THRESHOLD_VALUE) ||
        (i4TestValLruIfThresholdInUnknownProtos > BITE_MAX_THRESHOLD_VALUE))
    {
        CLI_SET_ERR (BITE_CLI_INVALID_THRESHOLD_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2LruIfThresholdOutDiscards
 Input       :  The Indices
                LruIfIndex

                The Object 
                testValLruIfThresholdOutDiscards
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2LruIfThresholdOutDiscards(UINT4 *pu4ErrorCode , INT4 i4LruIfIndex , INT4 i4TestValLruIfThresholdOutDiscards)
{
    UNUSED_PARAM (i4LruIfIndex);

    if ((i4TestValLruIfThresholdOutDiscards < BITE_MIN_THRESHOLD_VALUE) ||
        (i4TestValLruIfThresholdOutDiscards > BITE_MAX_THRESHOLD_VALUE))
    {
        CLI_SET_ERR (BITE_CLI_INVALID_THRESHOLD_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2LruIfThresholdOutErrors
 Input       :  The Indices
                LruIfIndex

                The Object 
                testValLruIfThresholdOutErrors
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2LruIfThresholdOutErrors(UINT4 *pu4ErrorCode , INT4 i4LruIfIndex , INT4 i4TestValLruIfThresholdOutErrors)
{
    UNUSED_PARAM (i4LruIfIndex);

    if ((i4TestValLruIfThresholdOutErrors < BITE_MIN_THRESHOLD_VALUE) ||
        (i4TestValLruIfThresholdOutErrors > BITE_MAX_THRESHOLD_VALUE))
    {
        CLI_SET_ERR (BITE_CLI_INVALID_THRESHOLD_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2LruIfRowStatus
 Input       :  The Indices
                LruIfIndex

                The Object 
                testValLruIfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2LruIfRowStatus(UINT4 *pu4ErrorCode , INT4 i4LruIfIndex , INT4 i4TestValLruIfRowStatus)
{
    UNUSED_PARAM (i4LruIfIndex);

    if ((i4LruIfIndex < BITE_ONE) || (i4LruIfIndex > gu4MaxIfcs))
    {
        CLI_SET_ERR (BITE_CLI_ERR_STATUS_VALUE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
	 if ((i4TestValLruIfRowStatus != CREATE_AND_GO) &&
        (i4TestValLruIfRowStatus != ACTIVE)
        && (i4TestValLruIfRowStatus != DESTROY))
    {
        CLI_SET_ERR (BITE_CLI_ERR_STATUS_VALUE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2LruIfTable
 Input       :  The Indices
                LruIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2LruIfTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLruThresholdCounterReset
 Input       :  The Indices

                The Object 
                retValLruThresholdCounterReset
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruThresholdCounterReset(INT4 *pi4RetValLruThresholdCounterReset)
{
    *pi4RetValLruThresholdCounterReset = gBiteGlobalInfo.u4CounterReset;

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetLruContinuousSelfTestReturnCode
 Input       :  The Indices

                The Object 
                retValLruContinuousSelfTestReturnCode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruContinuousSelfTestReturnCode(tSNMP_OCTET_STRING_TYPE * pRetValLruContinuousSelfTestReturnCode)
{
    UINT1               au1RetValContSelftest[50];

    SPRINTF (au1RetValContSelftest, "%ld",
             gBiteGlobalInfo.u4ContSelfTestRetCode);
    pRetValLruContinuousSelfTestReturnCode->i4_Length =
        STRLEN (au1RetValContSelftest);
    MEMCPY (pRetValLruContinuousSelfTestReturnCode->pu1_OctetList,
            au1RetValContSelftest,
            pRetValLruContinuousSelfTestReturnCode->i4_Length);

    return SNMP_SUCCESS;

}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLruThresholdCounterReset
 Input       :  The Indices

                The Object 
                setValLruThresholdCounterReset
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetLruThresholdCounterReset(INT4 i4SetValLruThresholdCounterReset)
{
    MEMSET (gau1OperPrevState, 0, sizeof (gau1OperPrevState));
    MEMSET (gau1OperCurrState, 0, sizeof (gau1OperCurrState));
    MEMSET (gau1InDisPrevState, 0, sizeof (gau1InDisPrevState));
    MEMSET (gau1InDisCurrState, 0, sizeof (gau1InDisCurrState));
    MEMSET (gau1InErrPrevState, 0, sizeof (gau1InErrPrevState));
    MEMSET (gau1InErrCurrState, 0, sizeof (gau1InErrCurrState));
    MEMSET (gau1InUnKnwPrevState, 0, sizeof (gau1InUnKnwPrevState));
    MEMSET (gau1InUnKnwCurrState, 0, sizeof (gau1InUnKnwCurrState));
    MEMSET (gau1OutDisPrevState, 0, sizeof (gau1OutDisPrevState));
    MEMSET (gau1OutDisCurrState, 0, sizeof (gau1OutDisCurrState));
    MEMSET (gau1OutErrPrevState, 0, sizeof (gau1OutErrPrevState));
    MEMSET (gau1OutErrCurrState, 0, sizeof (gau1OutErrCurrState));
    gu4IpCurrState = 0;
    gu4IcmpCurrState = 0;
    gu4TcpCurrState = 0;
    gu4UdpCurrState = 0;
    gBiteGlobalInfo.u4CounterReset = i4SetValLruThresholdCounterReset;

    BiteIfResetOperStatusCounter ();
    gBiteGlobalInfo.u4ContSelfTest = BITE_PASS;
    gBiteGlobalInfo.u4ContSelfTestRetCode = 0;

    if ((gBiteGlobalInfo.u4FailureReason & CBIT_TEST_FAIL) != 0)
    {
        gBiteGlobalInfo.u4OverallStatus = BITE_PASS;
        gBiteGlobalInfo.u4FailureType = BITE_NONE;
        gBiteGlobalInfo.u4FailureReason = 0;
    }
    BitetestMonitorCounterStatistics (TRUE);
    gBiteGlobalInfo.u4CounterReset = BITE_COUNTER_RESET_OFF;
    return SNMP_SUCCESS;

}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LruThresholdCounterReset
 Input       :  The Indices

                The Object 
                testValLruThresholdCounterReset
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2LruThresholdCounterReset(UINT4 *pu4ErrorCode , INT4 i4TestValLruThresholdCounterReset)
{
if (i4TestValLruThresholdCounterReset != BITE_COUNTER_RESET_ON)
    {
        CLI_SET_ERR (BITE_CLI_INVALID_COUNTER_RESET_COMMAND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LruThresholdCounterReset
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2LruThresholdCounterReset(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLruSwitchName
 Input       :  The Indices

                The Object 
                retValLruSwitchName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruSwitchName(tSNMP_OCTET_STRING_TYPE * pRetValLruSwitchName)
{
    UINT1               u1SwitchName[50];
    STRCPY (u1SwitchName, IssGetSwitchName ());
    pRetValLruSwitchName->i4_Length = STRLEN (u1SwitchName);
    MEMCPY (pRetValLruSwitchName->pu1_OctetList,
            u1SwitchName, pRetValLruSwitchName->i4_Length);

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetLruHardwarePartNumber
 Input       :  The Indices

                The Object 
                retValLruHardwarePartNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruHardwarePartNumber(tSNMP_OCTET_STRING_TYPE * pRetValLruHardwarePartNumber)
{
    UINT1               au1RetValLruHardwarePartNumber[50];

    BitetestGetHwNum (au1RetValLruHardwarePartNumber);

    pRetValLruHardwarePartNumber->i4_Length =
        STRLEN (au1RetValLruHardwarePartNumber);
    MEMCPY (pRetValLruHardwarePartNumber->pu1_OctetList,
            au1RetValLruHardwarePartNumber,
            pRetValLruHardwarePartNumber->i4_Length);

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetLruSoftwarePartNumber
 Input       :  The Indices

                The Object 
                retValLruSoftwarePartNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruSoftwarePartNumber(tSNMP_OCTET_STRING_TYPE * pRetValLruSoftwarePartNumber)
{
    UINT1               au1RetValLruSoftwarePartNumber[50];

    BitetestGetSwNum (au1RetValLruSoftwarePartNumber);
    pRetValLruSoftwarePartNumber->i4_Length =
        STRLEN (au1RetValLruSoftwarePartNumber);
    MEMCPY (pRetValLruSoftwarePartNumber->pu1_OctetList,
            au1RetValLruSoftwarePartNumber,
            pRetValLruSoftwarePartNumber->i4_Length);

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetLruSerialNumber
 Input       :  The Indices

                The Object 
                retValLruSerialNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetLruSerialNumber(tSNMP_OCTET_STRING_TYPE * pRetValLruSerialNumber)
{
    UINT1               au1RetValLruSerialNumber[50];

    BitetestGetSerialNum (au1RetValLruSerialNumber);
    pRetValLruSerialNumber->i4_Length = STRLEN (au1RetValLruSerialNumber);
    MEMCPY (pRetValLruSerialNumber->pu1_OctetList, au1RetValLruSerialNumber,
            pRetValLruSerialNumber->i4_Length);
    return SNMP_SUCCESS;

}

