/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 *
 * Description: This file contains BITE task queue related routines
 *********************************************************************/
#ifndef _BITEQUE_C_
#define _BITEQUE_C_
#include "biteinc.h"

/*****************************************************************************/
/* Function                  : BiteQueMsgHandler                             */
/*                                                                           */
/* Description               : This routine receives the Bite messages that   */
/*                             have been posted into the PTP Queue. It       */
/*                             de-queues the PTP message and will invoke the */
/*                             corresponding message handlers based on the   */
/*                             received message.                             */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gBiteGlobalInfo.BiteMsgQueId                */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
BiteQueMsgHandler (VOID)
{
    tBiteQMsg          *pBiteQMsg = NULL;

    /* Dequeue the BITE Queue and handle the message for processing */
    while (OsixQueRecv (gBiteGlobalInfo.BiteMsgQueId, (UINT1 *) &pBiteQMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        switch (pBiteQMsg->u4MsgType)
        {
            case BITE_CFA_MSG:    /*message from CFA is handled to interface
                                   handler for processing intrface change */
                BiteIfProcessStatusChange (pBiteQMsg->u4IfNum,
                                           pBiteQMsg->u1InterfaceStatus);
                break;

            case BITE_MSR_MSG:    /* message from MSR is handled for checking the
                                   configuration restore status */
                BitetestProcessMsrStatus (pBiteQMsg->i4MsrStatus);
                break;

            default:
                break;

        }
        /*release the memory used by queue after message handling */
        MemReleaseMemBlock (gBiteGlobalInfo.BiteQueMemPoolId,
                            (UINT1 *) pBiteQMsg);

        return;
    }

}

/*****************************************************************************/
/* Function                  : BiteQueEnqMsg                                 */
/*                                                                           */
/* Description               : This routine enqueue the message in the BITE 
                               queue. This utility should be used to post    */
/*                             the message to BITE                           */
/*                                                                           */
/* Input                     : pBiteQMsg - Pointer to the message            */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gBiteGlobalInfo.BiteMsgQueId, 
                               gBiteGlobalInfo.BiteTaskId  */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
BiteQueEnqMsg (tBiteQMsg * pBiteQMsg)
{

    /*Messages from CFA and MSR queued into BITE msg queue */
    if (OsixQueSend (gBiteGlobalInfo.BiteMsgQueId, (UINT1 *) &pBiteQMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gBiteGlobalInfo.BiteQueMemPoolId,
                            (UINT1 *) &pBiteQMsg);
        return OSIX_FAILURE;
    }
    /*Event posted to the main task from the bite message queue */
    if (BiteQuePostEventToBiteTask (BITE_CONF_MSG_ENQ_EVENT) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gBiteGlobalInfo.BiteQueMemPoolId,
                            (UINT1 *) &pBiteQMsg);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : BiteQuePostEventToBiteTask                    */
/*                                                                           */
/* Description               : This routine posts the event to the Bite module*/
/*                                                                           */
/* Input                     : i4Event - Event to be posted to the module.   */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gBiteifGlobalInfo.BiteTaskId                  */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
BiteQuePostEventToBiteTask (INT4 i4Event)
{

    /*event posted from to the main task */
    if (OsixEvtSend (gBiteGlobalInfo.BiteTaskId, i4Event) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
#endif
