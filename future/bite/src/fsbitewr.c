# include  "lr.h" 
# include  "fssnmp.h" 
# include  "fsbitelw.h"
# include  "fsbitewr.h"
# include  "fsbitedb.h"


VOID RegisterFSBITE ()
{
	SNMPRegisterMib (&fsbiteOID, &fsbiteEntry, SNMP_MSR_TGR_FALSE);
	SNMPAddSysorEntry (&fsbiteOID, (const UINT1 *) "fsbite");
}



VOID UnRegisterFSBITE ()
{
	SNMPUnRegisterMib (&fsbiteOID, &fsbiteEntry);
	SNMPDelSysorEntry (&fsbiteOID, (const UINT1 *) "fsbite");
}

INT4 LruOverallStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruOverallStatus(&(pMultiData->i4_SLongValue)));
}
INT4 LruPowerOnSelfTestGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruPowerOnSelfTest(&(pMultiData->i4_SLongValue)));
}
INT4 LruContinuousSelfTestGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruContinuousSelfTest(&(pMultiData->i4_SLongValue)));
}
INT4 LruFailureTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruFailureType(&(pMultiData->i4_SLongValue)));
}
INT4 LruFailureReasonGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruFailureReason(pMultiData->pOctetStrValue));
}
INT4 LruSelfTestCommandGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruSelfTestCommand(&(pMultiData->i4_SLongValue)));
}
INT4 LruSelfTestUpTimeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruSelfTestUpTime(&(pMultiData->i4_SLongValue)));
}
INT4 LruLastResultSelfTestGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruLastResultSelfTest(&(pMultiData->i4_SLongValue)));
}
INT4 LruSelfTestReturnCodeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruSelfTestReturnCode(pMultiData->pOctetStrValue));
}
INT4 LruContinuousSelfTestCommandGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruContinuousSelfTestCommand(&(pMultiData->i4_SLongValue)));
}
INT4 LruThresholdIpInAddrErrorsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruThresholdIpInAddrErrors(&(pMultiData->i4_SLongValue)));
}
INT4 LruThresholdIpInHdrErrorsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruThresholdIpInHdrErrors(&(pMultiData->i4_SLongValue)));
}
INT4 LruThresholdIpInUnknownProtosGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruThresholdIpInUnknownProtos(&(pMultiData->i4_SLongValue)));
}
INT4 LruThresholdIpInDiscardsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruThresholdIpInDiscards(&(pMultiData->i4_SLongValue)));
}
INT4 LruThresholdIpOutDiscardsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruThresholdIpOutDiscards(&(pMultiData->i4_SLongValue)));
}
INT4 LruThresholdIpReasmFailsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruThresholdIpReasmFails(&(pMultiData->i4_SLongValue)));
}
INT4 LruThresholdIpFragFailsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruThresholdIpFragFails(&(pMultiData->i4_SLongValue)));
}
INT4 LruThresholdIcmpInErrorsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruThresholdIcmpInErrors(&(pMultiData->i4_SLongValue)));
}
INT4 LruThresholdIcmpOutErrorsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruThresholdIcmpOutErrors(&(pMultiData->i4_SLongValue)));
}
INT4 LruThresholdTcpInErrorsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruThresholdTcpInErrors(&(pMultiData->i4_SLongValue)));
}
INT4 LruThresholdUdpInErrorsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruThresholdUdpInErrors(&(pMultiData->i4_SLongValue)));
}
INT4 LruSelfTestCommandSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetLruSelfTestCommand(pMultiData->i4_SLongValue));
}


INT4 LruContinuousSelfTestCommandSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetLruContinuousSelfTestCommand(pMultiData->i4_SLongValue));
}


INT4 LruThresholdIpInAddrErrorsSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetLruThresholdIpInAddrErrors(pMultiData->i4_SLongValue));
}


INT4 LruThresholdIpInHdrErrorsSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetLruThresholdIpInHdrErrors(pMultiData->i4_SLongValue));
}


INT4 LruThresholdIpInUnknownProtosSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetLruThresholdIpInUnknownProtos(pMultiData->i4_SLongValue));
}


INT4 LruThresholdIpInDiscardsSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetLruThresholdIpInDiscards(pMultiData->i4_SLongValue));
}


INT4 LruThresholdIpOutDiscardsSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetLruThresholdIpOutDiscards(pMultiData->i4_SLongValue));
}


INT4 LruThresholdIpReasmFailsSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetLruThresholdIpReasmFails(pMultiData->i4_SLongValue));
}


INT4 LruThresholdIpFragFailsSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetLruThresholdIpFragFails(pMultiData->i4_SLongValue));
}


INT4 LruThresholdIcmpInErrorsSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetLruThresholdIcmpInErrors(pMultiData->i4_SLongValue));
}


INT4 LruThresholdIcmpOutErrorsSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetLruThresholdIcmpOutErrors(pMultiData->i4_SLongValue));
}


INT4 LruThresholdTcpInErrorsSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetLruThresholdTcpInErrors(pMultiData->i4_SLongValue));
}


INT4 LruThresholdUdpInErrorsSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetLruThresholdUdpInErrors(pMultiData->i4_SLongValue));
}


INT4 LruSelfTestCommandTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2LruSelfTestCommand(pu4Error, pMultiData->i4_SLongValue));
}


INT4 LruContinuousSelfTestCommandTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2LruContinuousSelfTestCommand(pu4Error, pMultiData->i4_SLongValue));
}


INT4 LruThresholdIpInAddrErrorsTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2LruThresholdIpInAddrErrors(pu4Error, pMultiData->i4_SLongValue));
}


INT4 LruThresholdIpInHdrErrorsTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2LruThresholdIpInHdrErrors(pu4Error, pMultiData->i4_SLongValue));
}


INT4 LruThresholdIpInUnknownProtosTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2LruThresholdIpInUnknownProtos(pu4Error, pMultiData->i4_SLongValue));
}


INT4 LruThresholdIpInDiscardsTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2LruThresholdIpInDiscards(pu4Error, pMultiData->i4_SLongValue));
}


INT4 LruThresholdIpOutDiscardsTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2LruThresholdIpOutDiscards(pu4Error, pMultiData->i4_SLongValue));
}


INT4 LruThresholdIpReasmFailsTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2LruThresholdIpReasmFails(pu4Error, pMultiData->i4_SLongValue));
}


INT4 LruThresholdIpFragFailsTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2LruThresholdIpFragFails(pu4Error, pMultiData->i4_SLongValue));
}


INT4 LruThresholdIcmpInErrorsTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2LruThresholdIcmpInErrors(pu4Error, pMultiData->i4_SLongValue));
}


INT4 LruThresholdIcmpOutErrorsTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2LruThresholdIcmpOutErrors(pu4Error, pMultiData->i4_SLongValue));
}


INT4 LruThresholdTcpInErrorsTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2LruThresholdTcpInErrors(pu4Error, pMultiData->i4_SLongValue));
}


INT4 LruThresholdUdpInErrorsTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2LruThresholdUdpInErrors(pu4Error, pMultiData->i4_SLongValue));
}


INT4 LruSelfTestCommandDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2LruSelfTestCommand(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 LruContinuousSelfTestCommandDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2LruContinuousSelfTestCommand(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 LruThresholdIpInAddrErrorsDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2LruThresholdIpInAddrErrors(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 LruThresholdIpInHdrErrorsDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2LruThresholdIpInHdrErrors(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 LruThresholdIpInUnknownProtosDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2LruThresholdIpInUnknownProtos(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 LruThresholdIpInDiscardsDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2LruThresholdIpInDiscards(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 LruThresholdIpOutDiscardsDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2LruThresholdIpOutDiscards(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 LruThresholdIpReasmFailsDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2LruThresholdIpReasmFails(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 LruThresholdIpFragFailsDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2LruThresholdIpFragFails(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 LruThresholdIcmpInErrorsDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2LruThresholdIcmpInErrors(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 LruThresholdIcmpOutErrorsDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2LruThresholdIcmpOutErrors(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 LruThresholdTcpInErrorsDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2LruThresholdTcpInErrors(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 LruThresholdUdpInErrorsDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2LruThresholdUdpInErrors(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexLruIfTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexLruIfTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexLruIfTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 LruIfThresholdOperStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLruIfTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLruIfThresholdOperStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LruIfThresholdInDiscardsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLruIfTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLruIfThresholdInDiscards(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LruIfThresholdInErrorsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLruIfTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLruIfThresholdInErrors(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LruIfThresholdInUnknownProtosGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLruIfTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLruIfThresholdInUnknownProtos(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LruIfThresholdOutDiscardsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLruIfTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLruIfThresholdOutDiscards(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LruIfThresholdOutErrorsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLruIfTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLruIfThresholdOutErrors(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LruIfContinuousSelfTestStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLruIfTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLruIfContinuousSelfTestStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LruIfContinuousSelfTestReturnCodeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLruIfTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLruIfContinuousSelfTestReturnCode(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 LruIfRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceLruIfTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetLruIfRowStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 LruIfThresholdOperStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetLruIfThresholdOperStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 LruIfThresholdInDiscardsSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetLruIfThresholdInDiscards(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 LruIfThresholdInErrorsSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetLruIfThresholdInErrors(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 LruIfThresholdInUnknownProtosSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetLruIfThresholdInUnknownProtos(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 LruIfThresholdOutDiscardsSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetLruIfThresholdOutDiscards(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 LruIfThresholdOutErrorsSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetLruIfThresholdOutErrors(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 LruIfRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetLruIfRowStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 LruIfThresholdOperStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2LruIfThresholdOperStatus(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 LruIfThresholdInDiscardsTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2LruIfThresholdInDiscards(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 LruIfThresholdInErrorsTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2LruIfThresholdInErrors(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 LruIfThresholdInUnknownProtosTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2LruIfThresholdInUnknownProtos(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 LruIfThresholdOutDiscardsTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2LruIfThresholdOutDiscards(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 LruIfThresholdOutErrorsTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2LruIfThresholdOutErrors(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 LruIfRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2LruIfRowStatus(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 LruIfTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2LruIfTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 LruThresholdCounterResetGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruThresholdCounterReset(&(pMultiData->i4_SLongValue)));
}
INT4 LruContinuousSelfTestReturnCodeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruContinuousSelfTestReturnCode(pMultiData->pOctetStrValue));
}
INT4 LruThresholdCounterResetSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetLruThresholdCounterReset(pMultiData->i4_SLongValue));
}


INT4 LruThresholdCounterResetTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2LruThresholdCounterReset(pu4Error, pMultiData->i4_SLongValue));
}


INT4 LruThresholdCounterResetDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2LruThresholdCounterReset(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 LruSwitchNameGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruSwitchName(pMultiData->pOctetStrValue));
}
INT4 LruHardwarePartNumberGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruHardwarePartNumber(pMultiData->pOctetStrValue));
}
INT4 LruSoftwarePartNumberGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruSoftwarePartNumber(pMultiData->pOctetStrValue));
}
INT4 LruSerialNumberGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetLruSerialNumber(pMultiData->pOctetStrValue));
}
