/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *
 * Description: This file contains the APIs exported by BITE module.
 *****************************************************************************/
#ifndef _BITEAPI_C_
#define _BITEAPI_C_
#include "biteinc.h"

/*****************************************************************************/
/* Function                  : BiteApiLock                                   */
/*                                                                           */
/* Description               : This API shall be used to take the mutual     */
/*                             protocol semaphore.                           */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gBiteGlobalInfo.BiteSemId                   */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : SNMP_SUCCESS/SNMP_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
INT4
BiteApiLock (VOID)
{
    /* semaphore is acquired before processing every event */
    if (OsixSemTake (gBiteGlobalInfo.BiteSemId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function                  : BiteApiUnLock                                  */
/*                                                                           */
/* Description               : This API shall be used to release the mutual  */
/*                             protocol semaphore.                           */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gBiteGlobalInfo.BiteSemId                   */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : SNMP_SUCCESS/SNMP_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
INT4
BiteApiUnLock (VOID)
{
    /* semaphore is released after processing every event */
    OsixSemGive (gBiteGlobalInfo.BiteSemId);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function                  : BiteApiMsrStatusHandler                       */
/*                                                                           */
/* Description               : This API should be invokedby the external     */
/*                             module MSR when the configuration restore
                               is complete and should send the sttaus
                               of configuration restore as success 
                               or failure                                    */
/* Input                     : i4MibRetStatus - status of configuration 
                               restore success/failure                       */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/

INT4
BiteApiMsrStatusHandler (INT4 i4MibRetStatus)
{
    BiteApiLock ();
    BitetestProcessMsrStatus (i4MibRetStatus);

    BiteApiUnLock ();

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : BiteApiIfStatusChangeHandler                  */
/*                                                                           */
/* Description               : This API should be invokedby the external     */
/*                             module CFA to send the operation status
                               as CFA_UP/CFA_DOWN for the interface with
                               index u4IfNum                                 */
/* Input                     : u4IfNum - index of the interface  
                               u1IfStatus - operation status 
                                            CFA_UP/CFA_DOWN                  */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/

INT4
BiteApiIfStatusChangeHandler (UINT4 u4IfNum, UINT1 u1IfStatus)
{

    tBiteQMsg          *pMsg = NULL;
    /* If the BITE module is not initialised return OSIX_FAILURE */

    if (gu4BiteSysCntrl == FALSE)
    {
        return OSIX_FAILURE;
    }
    /* Allocate memory for the queue msg structure for storing the status message from MSR */
    /* Return failure if memory allocation fails */
    if (MemAllocateMemBlock (gBiteGlobalInfo.BiteQueMemPoolId,
                             ((UINT1 **) &pMsg)) == MEM_FAILURE)
    {
        return OSIX_FAILURE;
    }
    /* Return failure if queue message structure is NULL after memory allocation */
    if (pMsg == NULL)
    {
        return OSIX_FAILURE;
    }
    /* message type is CFA */
    /* Interface status message and interface number from CFA copied into */
    /* BITE message queue structure */

    pMsg->u4MsgType = BITE_CFA_MSG;
    pMsg->u4IfNum = u4IfNum;
    pMsg->u1InterfaceStatus = u1IfStatus;
    /* Enque the message in BITE queue */
    if (BiteQueEnqMsg (pMsg) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
#endif
