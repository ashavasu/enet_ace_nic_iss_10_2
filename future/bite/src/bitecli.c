/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: bitecli.c,v 1.1.1.1 2011/07/21 14:06:33 siva Exp $
 *
 * *********************************************************************/

#ifndef _BITECLI_C_
#define _BITECLI_C_
#include "biteinc.h"
/***************************************************************************
 * FUNCTION NAME             : cli_process_bite_cmd
 *
 * DESCRIPTION               : This function is exported to CLI module to
 *                             handle the BITE cli commands to take the
 *                             corresponding action.
 *                             Only SNMP Low level routines are called
 *                             from CLI.
 *
 * INPUT                     : Variable arguments
 *
 * OUTPUT                    : None
   
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *

 
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

INT4
cli_process_bite_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *apu1args[BITE_CLI_MAX_ARGS];
    UINT1               u1Flag = FALSE;
    INT1                i1argno = BITE_ZERO;
    UINT4               u4Inst = BITE_ZERO;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT4               u4Test;
    UINT4               u4ErrCode = 0;
    UINT4               u4Value = 0;
    MEMSET (apu1args, BITE_ZERO, BITE_CLI_MAX_ARGS);

    /* To Parse the Variable number of arguments passed */
    va_start (ap, u4Command);
    u4Inst = va_arg (ap, UINT4);    /* Dummy Argument passed for backward
                                       Compatibility */
    /* Walking through the rest of the Arguments in a Infinite loop
     * which breaks when the number of arg read is equal to the
     * Maximum Cli Arguments that can be Passed */

    while (BITE_LOOP_FOREVER)
    {
        apu1args[i1argno++] = va_arg (ap, UINT1 *);
        if (i1argno == BITE_CLI_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);
    CliRegisterLock (CliHandle, BiteApiLock, BiteApiUnLock);
    BiteApiLock ();
    /* Calling proper Cli Functions using the 2 arg, u4Command */
    switch (u4Command)
    {

        case BITE_CLI_DIAGNOSTIC_NONDISRUPTIVE:
            u4Test = BITE_SELF_TEST;
            if (gBiteGlobalInfo.u4SelfTestCommand != BITE_IBIT_TEST_INPRGS)
            {
                CliUnRegisterLock (CliHandle);
                BiteApiUnLock ();

            }
            i4RetStatus = BiteCliSetDiagnosticTest (CliHandle,
                                                    u4Test, (INT4) apu1args[0]);
            break;

        case BITE_CLI_DIAGNOSTIC_CONTINUOUS:
            u4Test = BITE_CONT_SELF_TEST;
            if (gBiteGlobalInfo.u4SelfTestCommand != BITE_IBIT_TEST_INPRGS)
            {
                CliUnRegisterLock (CliHandle);
                BiteApiUnLock ();
            }
            i4RetStatus = BiteCliSetDiagnosticTest (CliHandle,
                                                    u4Test, (INT4) apu1args[0]);
            break;

        case BITE_CLI_CLEAR_COUNTER:
            i4RetStatus = BiteCliClearCounters (CliHandle, (INT4) apu1args[0]);
            break;

        case BITE_CLI_MONITOR_INTERFACE:
            i4RetStatus = BiteCliIfCreateDelete (CliHandle,
                                                 *(UINT4 *) apu1args[0],
                                                 (INT4) apu1args[1]);
            break;

        case BITE_CLI_IF_OPER_STATUS:
        case BITE_CLI_NO_IF_OPER_STATUS:
        case BITE_CLI_IF_OPER_STATUS_DEFAULT:
            if (u4Command == BITE_CLI_IF_OPER_STATUS_DEFAULT)
            {
                u4Value = BITE_THRESHOLD_DEFAULT_VALUE;
            }
            else if (u4Command == BITE_CLI_IF_OPER_STATUS)
            {
                u4Value = *(UINT4 *) apu1args[1];
            }
            i4RetStatus = BiteCliSetIfThreshold (CliHandle,
                                                 *(UINT4 *) apu1args[0],
                                                 BITE_CLI_IF_OPER_STATUS,
                                                 u4Value);
            break;

        case BITE_CLI_IF_IN_DISCARDS:
        case BITE_CLI_NO_IF_IN_DISCARDS:
        case BITE_CLI_IF_IN_DISCARDS_DEFAULT:
            if (u4Command == BITE_CLI_IF_IN_DISCARDS_DEFAULT)
            {
                u4Value = BITE_THRESHOLD_DEFAULT_VALUE;
            }
            else if (u4Command == BITE_CLI_IF_IN_DISCARDS)
            {
                u4Value = *(UINT4 *) apu1args[1];
            }
            i4RetStatus = BiteCliSetIfThreshold (CliHandle,
                                                 *(UINT4 *) apu1args[0],
                                                 BITE_CLI_IF_IN_DISCARDS,
                                                 u4Value);
            break;

        case BITE_CLI_IF_IN_ERRORS:
        case BITE_CLI_NO_IF_IN_ERRORS:
        case BITE_CLI_IF_IN_ERRORS_DEFAULT:
            if (u4Command == BITE_CLI_IF_IN_ERRORS_DEFAULT)
            {
                u4Value = BITE_THRESHOLD_DEFAULT_VALUE;
            }
            else if (u4Command == BITE_CLI_IF_IN_ERRORS)
            {
                u4Value = *(UINT4 *) apu1args[1];
            }
            i4RetStatus = BiteCliSetIfThreshold (CliHandle,
                                                 *(UINT4 *) apu1args[0],
                                                 BITE_CLI_IF_IN_ERRORS,
                                                 u4Value);
            break;

        case BITE_CLI_IF_IN_UNKNOWN_PROTOS:
        case BITE_CLI_NO_IF_IN_UNKNOWN_PROTOS:
        case BITE_CLI_IF_IN_UNKNOWN_PROTOS_DEFAULT:
            if (u4Command == BITE_CLI_IF_IN_UNKNOWN_PROTOS_DEFAULT)
            {
                u4Value = BITE_THRESHOLD_DEFAULT_VALUE;
            }
            else if (u4Command == BITE_CLI_IF_IN_UNKNOWN_PROTOS)
            {
                u4Value = *(UINT4 *) apu1args[1];
            }
            i4RetStatus = BiteCliSetIfThreshold (CliHandle,
                                                 *(UINT4 *) apu1args[0],
                                                 BITE_CLI_IF_IN_UNKNOWN_PROTOS,
                                                 u4Value);
            break;

        case BITE_CLI_IF_OUT_DISCARDS:
        case BITE_CLI_NO_IF_OUT_DISCARDS:
        case BITE_CLI_IF_OUT_DISCARDS_DEFAULT:
            if (u4Command == BITE_CLI_IF_OUT_DISCARDS_DEFAULT)
            {
                u4Value = BITE_THRESHOLD_DEFAULT_VALUE;
            }
            else if (u4Command == BITE_CLI_IF_OUT_DISCARDS)
            {
                u4Value = *(UINT4 *) apu1args[1];
            }
            i4RetStatus = BiteCliSetIfThreshold (CliHandle,
                                                 *(UINT4 *) apu1args[0],
                                                 BITE_CLI_IF_OUT_DISCARDS,
                                                 u4Value);
            break;

        case BITE_CLI_IF_OUT_ERRORS:
        case BITE_CLI_NO_IF_OUT_ERRORS:
        case BITE_CLI_IF_OUT_ERRORS_DEFAULT:
            if (u4Command == BITE_CLI_IF_OUT_ERRORS_DEFAULT)
            {
                u4Value = BITE_THRESHOLD_DEFAULT_VALUE;
            }
            else if (u4Command == BITE_CLI_IF_OUT_ERRORS)
            {
                u4Value = *(UINT4 *) apu1args[1];
            }
            i4RetStatus = BiteCliSetIfThreshold (CliHandle,
                                                 *(UINT4 *) apu1args[0],
                                                 BITE_CLI_IF_OUT_ERRORS,
                                                 u4Value);
            break;

        case BITE_CLI_IP_IN_HDR_ERRORS:
        case BITE_CLI_NO_IP_IN_HDR_ERRORS:
        case BITE_CLI_IP_IN_HDR_ERRORS_DEFAULT:
            if (u4Command == BITE_CLI_IP_IN_HDR_ERRORS_DEFAULT)
            {
                u4Value = BITE_THRESHOLD_DEFAULT_VALUE;
            }
            else if (u4Command == BITE_CLI_IP_IN_HDR_ERRORS)
            {
                u4Value = *(UINT4 *) apu1args[0];
            }
            i4RetStatus = BiteCliSetIPThreshold (CliHandle,
                                                 BITE_CLI_IP_IN_HDR_ERRORS,
                                                 u4Value);
            break;

        case BITE_CLI_IP_IN_ADDR_ERRORS:
        case BITE_CLI_NO_IP_IN_ADDR_ERRORS:
        case BITE_CLI_IP_IN_ADDR_ERRORS_DEFAULT:
            if (u4Command == BITE_CLI_IP_IN_ADDR_ERRORS_DEFAULT)
            {
                u4Value = BITE_THRESHOLD_DEFAULT_VALUE;
            }
            else if (u4Command == BITE_CLI_IP_IN_ADDR_ERRORS)
            {
                u4Value = *(UINT4 *) apu1args[0];
            }
            i4RetStatus = BiteCliSetIPThreshold (CliHandle,
                                                 BITE_CLI_IP_IN_ADDR_ERRORS,
                                                 u4Value);
            break;

        case BITE_CLI_IP_IN_UNKNOWN_PROTOS:
        case BITE_CLI_NO_IP_IN_UNKNOWN_PROTOS:
        case BITE_CLI_IP_IN_UNKNOWN_PROTOS_DEFAULT:
            if (u4Command == BITE_CLI_IP_IN_UNKNOWN_PROTOS_DEFAULT)
            {
                u4Value = BITE_THRESHOLD_DEFAULT_VALUE;
            }
            else if (u4Command == BITE_CLI_IP_IN_UNKNOWN_PROTOS)
            {
                u4Value = *(UINT4 *) apu1args[0];
            }
            i4RetStatus = BiteCliSetIPThreshold (CliHandle,
                                                 BITE_CLI_IP_IN_UNKNOWN_PROTOS,
                                                 u4Value);
            break;

        case BITE_CLI_IP_IN_DISCARDS:
        case BITE_CLI_NO_IP_IN_DISCARDS:
        case BITE_CLI_IP_IN_DISCARDS_DEFAULT:
            if (u4Command == BITE_CLI_IP_IN_DISCARDS_DEFAULT)
            {
                u4Value = BITE_THRESHOLD_DEFAULT_VALUE;
            }
            else if (u4Command == BITE_CLI_IP_IN_DISCARDS)
            {
                u4Value = *(UINT4 *) apu1args[0];
            }
            i4RetStatus = BiteCliSetIPThreshold (CliHandle,
                                                 BITE_CLI_IP_IN_DISCARDS,
                                                 u4Value);
            break;

        case BITE_CLI_IP_OUT_DISCARDS:
        case BITE_CLI_NO_IP_OUT_DISCARDS:
        case BITE_CLI_IP_OUT_DISCARDS_DEFAULT:
            if (u4Command == BITE_CLI_IP_OUT_DISCARDS_DEFAULT)
            {
                u4Value = BITE_THRESHOLD_DEFAULT_VALUE;
            }
            else if (u4Command == BITE_CLI_IP_OUT_DISCARDS)
            {
                u4Value = *(UINT4 *) apu1args[0];
            }
            i4RetStatus = BiteCliSetIPThreshold (CliHandle,
                                                 BITE_CLI_IP_OUT_DISCARDS,
                                                 u4Value);
            break;

        case BITE_CLI_IP_REASM_FAILS:
        case BITE_CLI_NO_IP_REASM_FAILS:
        case BITE_CLI_IP_REASM_FAILS_DEFAULT:
            if (u4Command == BITE_CLI_IP_REASM_FAILS_DEFAULT)
            {
                u4Value = BITE_THRESHOLD_DEFAULT_VALUE;
            }
            else if (u4Command == BITE_CLI_IP_REASM_FAILS)
            {
                u4Value = *(UINT4 *) apu1args[0];
            }
            i4RetStatus = BiteCliSetIPThreshold (CliHandle,
                                                 BITE_CLI_IP_REASM_FAILS,
                                                 u4Value);
            break;

        case BITE_CLI_IP_FRAG_FAILS:
        case BITE_CLI_NO_IP_FRAG_FAILS:
        case BITE_CLI_IP_FRAG_FAILS_DEFAULT:
            if (u4Command == BITE_CLI_IP_FRAG_FAILS_DEFAULT)
            {
                u4Value = BITE_THRESHOLD_DEFAULT_VALUE;
            }
            else if (u4Command == BITE_CLI_IP_FRAG_FAILS)
            {
                u4Value = *(UINT4 *) apu1args[0];
            }
            i4RetStatus = BiteCliSetIPThreshold (CliHandle,
                                                 BITE_CLI_IP_FRAG_FAILS,
                                                 u4Value);
            break;

        case BITE_CLI_ICMP_IN_ERRORS:
        case BITE_CLI_NO_ICMP_IN_ERRORS:
        case BITE_CLI_ICMP_IN_ERRORS_DEFAULT:
            if (u4Command == BITE_CLI_ICMP_IN_ERRORS_DEFAULT)
            {
                u4Value = BITE_THRESHOLD_DEFAULT_VALUE;
            }
            else if (u4Command == BITE_CLI_ICMP_IN_ERRORS)
            {
                u4Value = *(UINT4 *) apu1args[0];
            }
            i4RetStatus = BiteCliSetIPThreshold (CliHandle,
                                                 BITE_CLI_ICMP_IN_ERRORS,
                                                 u4Value);
            break;

        case BITE_CLI_ICMP_OUT_ERRORS:
        case BITE_CLI_NO_ICMP_OUT_ERRORS:
        case BITE_CLI_ICMP_OUT_ERRORS_DEFAULT:
            if (u4Command == BITE_CLI_ICMP_OUT_ERRORS_DEFAULT)
            {
                u4Value = BITE_THRESHOLD_DEFAULT_VALUE;
            }
            else if (u4Command == BITE_CLI_ICMP_OUT_ERRORS)
            {
                u4Value = *(UINT4 *) apu1args[0];
            }
            i4RetStatus = BiteCliSetIPThreshold (CliHandle,
                                                 BITE_CLI_ICMP_OUT_ERRORS,
                                                 u4Value);
            break;

        case BITE_CLI_TCP_IN_ERRORS:
        case BITE_CLI_NO_TCP_IN_ERRORS:
        case BITE_CLI_TCP_IN_ERRORS_DEFAULT:
            if (u4Command == BITE_CLI_TCP_IN_ERRORS_DEFAULT)
            {
                u4Value = BITE_THRESHOLD_DEFAULT_VALUE;
            }
            else if (u4Command == BITE_CLI_TCP_IN_ERRORS)
            {
                u4Value = *(UINT4 *) apu1args[0];
            }
            i4RetStatus = BiteCliSetIPThreshold (CliHandle,
                                                 BITE_CLI_TCP_IN_ERRORS,
                                                 u4Value);
            break;

        case BITE_CLI_UDP_IN_ERRORS:
        case BITE_CLI_NO_UDP_IN_ERRORS:
        case BITE_CLI_UDP_IN_ERRORS_DEFAULT:
            if (u4Command == BITE_CLI_TCP_IN_ERRORS_DEFAULT)
            {
                u4Value = BITE_THRESHOLD_DEFAULT_VALUE;
            }
            else if (u4Command == BITE_CLI_UDP_IN_ERRORS)
            {
                u4Value = *(UINT4 *) apu1args[0];
            }
            i4RetStatus = BiteCliSetIPThreshold (CliHandle,
                                                 BITE_CLI_UDP_IN_ERRORS,
                                                 u4Value);
            break;

        case BITE_CLI_HEALTH_STATUS:
            i4RetStatus = BiteCliShowOverallStatus (CliHandle);
            break;

        case BITE_CLI_DIAGNOSTIC_NONDISRUPTIVE_RESULT:
            i4RetStatus = BiteCliShowSelfTestResults (CliHandle);
            break;

        case BITE_CLI_CONTINUOUS_TEST_RESULT:

            i4RetStatus = BiteCliShowContSelfTestResults (CliHandle,
                                                          u1Flag,
                                                          *(UINT4 *)
                                                          apu1args[0]);
            break;
        case BITE_CLI_CONTINUOUS_TEST_RESULT_ALL:
            u1Flag = TRUE;
            i4RetStatus = BiteCliShowContSelfTestResults (CliHandle, u1Flag, 0);
            break;

        default:
            CliPrintf (CliHandle, "\r\n Wrong Command \r\n");
            break;

    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if (u4ErrCode > 0)
        {
/*            CliPrintf (CliHandle, "\r%% %s", BiteCliErrString[u4ErrCode]);*/
        }

        CLI_SET_ERR (BITE_ZERO);
    }

    CliUnRegisterLock (CliHandle);
    BiteApiUnLock ();
    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : BiteCliSetDiagnosticTest                             */
/* Description        : This Functions sets the Initiated Built In test if
                        u4Test = BITE_SELF_TEST
                        sets the Continuous Built In Test if
                        u4Test = BITE_CONT_SELF_TEST                         */
/* Input(s)           : CliHandle -  CliContext ID  
                        u4Test - BITE_SELF_TEST/BITE_CONT_SELF_TEST          */
/*                      i4Status  -   Enable/Disable                         */
/* Output(s)          : None.                                                */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/
INT4
BiteCliSetDiagnosticTest (tCliHandle CliHandle, UINT4 u4Test, INT4 i4Status)
{
    UINT4               u4ErrorCode;

    UNUSED_PARAM (CliHandle);
    if (u4Test == BITE_SELF_TEST)
    {

        /* Testing the self test mode  */
        if (nmhTestv2LruSelfTestCommand (&u4ErrorCode, i4Status) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /* Setting the Self test mode enable/disable */
        if (nmhSetLruSelfTestCommand (i4Status) == SNMP_FAILURE)
        {

            return CLI_FAILURE;
        }
    }

    if (u4Test == BITE_CONT_SELF_TEST)
    {
        /* Testing the continuous self test command */
        if (nmhTestv2LruContinuousSelfTestCommand (&u4ErrorCode, i4Status) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /* Setting the continuous Self test mode enable/disable */
        if (nmhSetLruContinuousSelfTestCommand (i4Status) == SNMP_FAILURE)
        {

            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : BiteCliClearCounters                                 */
/* Description        : This Functions resets the counter                    */
/* Input(s)           : CliHandle -  CliContext ID                           */
/*                      i4Status  -  counter status enable/disable           */
/* Output(s)          : None.                                                */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/
UINT4
BiteCliClearCounters (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrorCode;

    UNUSED_PARAM (CliHandle);
    /* Testing the clear counter command */
    if (nmhTestv2LruThresholdCounterReset (&u4ErrorCode, i4Status) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Clearing the counter enable/disable */
    if (nmhSetLruThresholdCounterReset (i4Status) == SNMP_FAILURE)
    {

        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************:w**********/
/* Function Name      : BiteCliShowOverallStatus                             */
/* Description        : This Functions Shows the overall status of           */
/*                      the device                                           */
/* Input(s)           : CliHandle        - CliContext ID                     */
/* Output(s)          : None.                                                */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/

INT4
BiteCliShowOverallStatus (tCliHandle CliHandle)
{

    INT4                i4LruOverallStatus;
    INT4                i4LruPowerOnSelfTest;
    INT4                i4LruContinuousSelfTest;
    INT4                i4LruFailureType;
    UINT4               u4ErrCode = 0;
    UINT1               au1FailureReason[BITE_FAILURE_REASON_MAX_LEN];

    MEMSET (au1FailureReason, 0, BITE_FAILURE_REASON_MAX_LEN);

    nmhGetLruOverallStatus (&i4LruOverallStatus);

    if (i4LruOverallStatus == BITE_PASS)
    {
        CliPrintf (CliHandle, " \r\n Overall Health status      :Pass\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " \r\n Overall health Status      :Fail\r\n");
    }

    nmhGetLruPowerOnSelfTest (&i4LruPowerOnSelfTest);

    if (i4LruPowerOnSelfTest == BITE_PASS)
    {
        CliPrintf (CliHandle, " \r\n Power on Self Test         :Pass\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " \r\n Power on self Test         :Fail\r\n");
    }

    nmhGetLruContinuousSelfTest (&i4LruContinuousSelfTest);

    if (i4LruContinuousSelfTest == BITE_PASS)
    {
        CliPrintf (CliHandle, " \r\n Continuous Self Test       :Pass\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " \r\n Continuous self Test       :Fail\r\n");
    }

    nmhGetLruFailureType (&i4LruFailureType);

    if (i4LruFailureType == BITE_HARDWARE_FAILURE)
    {
        CliPrintf (CliHandle,
                   " \r\n Failure Type               :Hardware Failure\r\n");
    }
    else if (i4LruFailureType == BITE_SOFTWARE_FAILURE)
    {
        CliPrintf (CliHandle,
                   " \r\n Failure Type               :Software Failure\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " \r\n Failure Type               :None\r\n");
    }

    BitetestGetErrCode (BITE_OVERALL_FAIL, &u4ErrCode, 0);
    BiteErrFailureReason (u4ErrCode, au1FailureReason);
    CliPrintf (CliHandle, " \r\n Failure Reason           :  \r\n");
    CliPrintf (CliHandle, "  %s \r\n", au1FailureReason);

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : BiteCliShowSelfTestResults                           */
/* Description        : This Functions Shows the results of                  */
/*                      Initiated built In Test                              */
/* Input(s)           : CliHandle        - CliContext ID                     */
/* Output(s)          : None.                                                */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/

INT4
BiteCliShowSelfTestResults (tCliHandle CliHandle)
{

    INT4                i4LruSelfTestUpTime;
    INT4                i4LruSelfTestCommand;
    INT4                i4LruLastResultSelfTest;
    UINT4               u4ErrCode = 0;
    UINT1               au1ReturnCode[BITE_FAILURE_REASON_MAX_LEN];

    MEMSET (au1ReturnCode, 0, BITE_FAILURE_REASON_MAX_LEN);

    nmhGetLruSelfTestCommand (&i4LruSelfTestCommand);

    if (i4LruSelfTestCommand == BITE_IBIT_TEST_ON)
    {
        CliPrintf (CliHandle, " \r\n  Self Test                 : On \r\n");
    }
    else if (i4LruSelfTestCommand == BITE_IBIT_TEST_INPRGS)
    {
        CliPrintf (CliHandle,
                   " \r\n  Self Test                 : In Progress \r\n");
    }
    else
    {
        CliPrintf (CliHandle, " \r\n  Self Test                 : Off \r\n");

    }

    nmhGetLruSelfTestUpTime (&i4LruSelfTestUpTime);
    CliPrintf (CliHandle, " \r\n  Self Test Up Time          : %d\r\n",
               i4LruSelfTestUpTime);
    nmhGetLruLastResultSelfTest (&i4LruLastResultSelfTest);

    if (i4LruLastResultSelfTest == BITE_PASS)
    {
        CliPrintf (CliHandle, " \r\n  Self Test Status          : Pass \r\n");
    }
    else
    {
        CliPrintf (CliHandle, " \r\n  Self Test                 : Fail \r\n");
    }

    BitetestGetErrCode (BITE_IBIT_TEST, &u4ErrCode, 0);
    if (u4ErrCode == 0)
    {
        CliPrintf (CliHandle, " \r\n  Self Test Failure Reason  : None \r\n");
    }
    else
    {
        BiteErrIbitFailure (u4ErrCode, au1ReturnCode);
        CliPrintf (CliHandle, " \r\n  Self Test Failure Reason  : \r\n");
        CliPrintf (CliHandle, " %s\r\n", au1ReturnCode);
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : BiteCliShowContSelfTestResults                       */
/* Description        : This Functions Shows the status of                   */
/*                      the Continuous Built In Test                         */
/* Input(s)           : CliHandle-cli context ID
                        u4IfNum-Index of the interface
                        u1Flag - If u1Flag equal to 1,then third argument 
                                 interface index is used.
                                 If u1Flag equal to 0,then the third 
                                 argument is a dummy argumaent               */
/* Outpu:523t(s)          : None.                                                */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/

INT4
BiteCliShowContSelfTestResults (tCliHandle CliHandle, UINT1 u1Flag,
                                UINT4 u4IfNum)
{

    INT4                i4LruFirstIfIndex = 0;
    INT4                i4LruNextIfIndex = 0;
    INT4                i4RetVal = 0;
    INT4                i4LruThresholdIpInAddrErrors = 0;
    INT4                i4LruThresholdIpInHdrErrors = 0;
    INT4                i4LruThresholdIpInUnknownProtos = 0;
    INT4                i4LruThresholdIpInDiscards = 0;
    INT4                i4LruThresholdIpOutDiscards = 0;
    INT4                i4LruThresholdIpReasmFails = 0;
    INT4                i4LruThresholdIpFragFails = 0;
    INT4                i4LruThresholdIcmpInErrors = 0;
    INT4                i4LruThresholdIcmpOutErrors = 0;
    INT4                i4LruThresholdTcpInErrors = 0;
    INT4                i4LruThresholdUdpInErrors = 0;
    INT4                i4LruIfThresholdOperStatus = 0;
    INT4                i4LruIfThresholdInDiscards = 0;
    INT4                i4LruIfThresholdInErrors = 0;
    INT4                i4LruIfThresholdInUnknownProtos = 0;
    INT4                i4LruIfThresholdOutDiscards = 0;
    INT4                i4LruIfThresholdOutErrors = 0;
    INT4                i4LruContSelfTestCmd = 0;
    INT4                i4LruContSelfTestStatus = 0;
    INT4                i4IfContSelfTestStatus = 0;
    UINT4               u4ErrCode = 0;
    UINT1               au1ReturnCode[BITE_FAILURE_REASON_MAX_LEN];
    tBiteIfInfo        *pBiteIfInfo = NULL;

    MEMSET (au1ReturnCode, 0, BITE_FAILURE_REASON_MAX_LEN);

    if ((u1Flag != TRUE) && (u4IfNum > gu4MaxIfcs))
    {
        CliPrintf (CliHandle, " \r\n Error: Wrong Interface \r\n");
        return CLI_SUCCESS;
    }
    if (u1Flag != TRUE)
    {
        pBiteIfInfo = BiteIfFind (u4IfNum);
        if (pBiteIfInfo == NULL)
        {
            CliPrintf (CliHandle,
                       " \r\n Error: Interface %d is not part of CBIT monitoring\r\n",
                       u4IfNum);
            return CLI_SUCCESS;
        }
    }
    nmhGetLruContinuousSelfTestCommand (&i4LruContSelfTestCmd);
    if (i4LruContSelfTestCmd == BITE_CBIT_TEST_ON)
    {
        CliPrintf (CliHandle,
                   " \r\n Continuous Self Test               : On \r\n");
    }
    else if (i4LruContSelfTestCmd == BITE_CBIT_TEST_INPRGS)
    {
        CliPrintf (CliHandle,
                   " \r\n Continuous Self Test               : In Progress \r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   " \r\n Continuous Self Test               : Off \r\n");
    }

    nmhGetLruContinuousSelfTest (&i4LruContSelfTestStatus);
    if (i4LruContSelfTestStatus == BITE_PASS)
    {
        CliPrintf (CliHandle,
                   " \r\n Continuous Self Test Status        : Passed \r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   " \r\n Continuous Self Test Status        : Failed \r\n");
    }
    BitetestGetErrCode (BITE_CBIT_TEST, &u4ErrCode, 0);

    if (u4ErrCode == 0)
    {
        CliPrintf (CliHandle,
                   " \r\n Continuous Self Test Failure Reason: None \r\n");
    }
    else
    {
        BiteErrCbitFailure (u4ErrCode, au1ReturnCode);
        CliPrintf (CliHandle,
                   " \r\n Continuous Self Test Failure Reason: \r\n");
        CliPrintf (CliHandle, " %s\r\n", au1ReturnCode);
    }

    if (u1Flag == TRUE)
    {
        CliPrintf (CliHandle, " \r\n .....Threshold for IP Counters..... \r\n");
        nmhGetLruThresholdIpInAddrErrors (&i4LruThresholdIpInAddrErrors);
        CliPrintf (CliHandle,
                   " \r\n  Threshold for IpInAddrErrors       : %d\r\n",
                   i4LruThresholdIpInAddrErrors);

        nmhGetLruThresholdIpInHdrErrors (&i4LruThresholdIpInHdrErrors);
        CliPrintf (CliHandle,
                   " \r\n  Threshold for IpInHdrErrors        : %d\r\n",
                   i4LruThresholdIpInHdrErrors);

        nmhGetLruThresholdIpInUnknownProtos (&i4LruThresholdIpInUnknownProtos);
        CliPrintf (CliHandle,
                   " \r\n  Threshold for IpInUnknownProtos    : %d\r\n",
                   i4LruThresholdIpInUnknownProtos);

        nmhGetLruThresholdIpInDiscards (&i4LruThresholdIpInDiscards);
        CliPrintf (CliHandle,
                   " \r\n  Threshold for IpInDiscards         : %d\r\n",
                   i4LruThresholdIpInDiscards);

        nmhGetLruThresholdIpOutDiscards (&i4LruThresholdIpOutDiscards);
        CliPrintf (CliHandle,
                   " \r\n  Threshold for IpOutDiscards        : %d\r\n",
                   i4LruThresholdIpOutDiscards);

        nmhGetLruThresholdIpReasmFails (&i4LruThresholdIpReasmFails);
        CliPrintf (CliHandle,
                   " \r\n  Threshold for IpReasmFails         : %d\r\n",
                   i4LruThresholdIpReasmFails);

        nmhGetLruThresholdIpFragFails (&i4LruThresholdIpFragFails);
        CliPrintf (CliHandle,
                   " \r\n  Threshold for IpFragFails          : %d\r\n",
                   i4LruThresholdIpFragFails);

        nmhGetLruThresholdIcmpInErrors (&i4LruThresholdIcmpInErrors);
        CliPrintf (CliHandle,
                   " \r\n  Threshold for IcmpInErrors         : %d\r\n",
                   i4LruThresholdIcmpInErrors);

        nmhGetLruThresholdIcmpOutErrors (&i4LruThresholdIcmpOutErrors);
        CliPrintf (CliHandle,
                   " \r\n  Threshold for IcmpOutErrors        : %d\r\n",
                   i4LruThresholdIcmpOutErrors);

        nmhGetLruThresholdTcpInErrors (&i4LruThresholdTcpInErrors);
        CliPrintf (CliHandle,
                   " \r\n  Threshold for TcpInErrors          : %d\r\n",
                   i4LruThresholdTcpInErrors);

        nmhGetLruThresholdUdpInErrors (&i4LruThresholdUdpInErrors);
        CliPrintf (CliHandle,
                   " \r\n  Threshold for UdpInErrors          : %d\r\n",
                   i4LruThresholdUdpInErrors);

        i4RetVal = nmhGetFirstIndexLruIfTable (&i4LruNextIfIndex);

        if (i4RetVal == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }

        CliPrintf (CliHandle,
                   " \r\n .....Statistics of Interface Counters..... \r\n");
        do
        {
            MEMSET (au1ReturnCode, 0, BITE_FAILURE_REASON_MAX_LEN);

            i4LruFirstIfIndex = i4LruNextIfIndex;

            CliPrintf (CliHandle, " \r\n  Port          : %d\r\n",
                       i4LruFirstIfIndex);

            nmhGetLruIfThresholdOperStatus (i4LruFirstIfIndex,
                                            &i4LruIfThresholdOperStatus);
            CliPrintf (CliHandle,
                       " \t\r\n Threshold for IfOperStatus         : %d\r\n",
                       i4LruIfThresholdOperStatus);

            nmhGetLruIfThresholdInDiscards (i4LruFirstIfIndex,
                                            &i4LruIfThresholdInDiscards);
            CliPrintf (CliHandle,
                       " \t\r\n Threshold for IfInDiscards         : %d\r\n",
                       i4LruIfThresholdInDiscards);

            nmhGetLruIfThresholdInErrors (i4LruFirstIfIndex,
                                          &i4LruIfThresholdInErrors);
            CliPrintf (CliHandle,
                       " \t\r\n Threshold for IfInErrors           : %d\r\n",
                       i4LruIfThresholdInErrors);

            nmhGetLruIfThresholdInUnknownProtos (i4LruFirstIfIndex,
                                                 &i4LruIfThresholdInUnknownProtos);
            CliPrintf (CliHandle,
                       " \t\r\n Threshold for IfInUnknownProtos    : %d\r\n",
                       i4LruIfThresholdInUnknownProtos);

            nmhGetLruIfThresholdOutDiscards (i4LruFirstIfIndex,
                                             &i4LruIfThresholdOutDiscards);
            CliPrintf (CliHandle,
                       " \t\r\n Threshold for IfOutDiscards        : %d\r\n",
                       i4LruIfThresholdOutDiscards);

            nmhGetLruIfThresholdOutErrors (i4LruFirstIfIndex,
                                           &i4LruIfThresholdOutErrors);
            CliPrintf (CliHandle,
                       " \t\r\n Threshold for IfOutErrors          : %d\r\n",
                       i4LruIfThresholdOutErrors);
            nmhGetLruIfContinuousSelfTestStatus (i4LruFirstIfIndex,
                                                 &i4IfContSelfTestStatus);
            if (i4IfContSelfTestStatus == BITE_PASS)
            {
                CliPrintf (CliHandle,
                           " \t\r\n Continuous self test status        : Passed \r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           " \t\r\n Continuous self test status        : Failed \r\n");
            }

            BitetestGetErrCode (BITE_INTERFACE_FAIL, &u4ErrCode,
                                i4LruFirstIfIndex);
            if (u4ErrCode == 0)
            {
                CliPrintf (CliHandle,
                           " \t\r\n Continuous Self Test Failure Reason: None \r\n");
            }
            else
            {
                BiteErrIfCbitFailure (i4LruFirstIfIndex, u4ErrCode,
                                      au1ReturnCode);
                CliPrintf (CliHandle,
                           " \t\r\n Continuous Self Test Failure Reason : \r\n");
                CliPrintf (CliHandle, " %s\r\n", au1ReturnCode);
            }
            CliPrintf (CliHandle, " \r\n \r\n");
            CliPrintf (CliHandle, " \r\n \r\n");
        }
        while (nmhGetNextIndexLruIfTable (i4LruFirstIfIndex, &i4LruNextIfIndex)
               == SNMP_SUCCESS);

    }

    else
    {
        u4ErrCode = 0;
        MEMSET (au1ReturnCode, 0, BITE_FAILURE_REASON_MAX_LEN);
        CliPrintf (CliHandle, " \r\n  Port      : %d\r\n", u4IfNum);

        nmhGetLruIfThresholdOperStatus (u4IfNum, &i4LruIfThresholdOperStatus);
        CliPrintf (CliHandle,
                   " \t\r\n Threshold for IfOperStatus         : %d\r\n",
                   i4LruIfThresholdOperStatus);

        nmhGetLruIfThresholdInDiscards (u4IfNum, &i4LruIfThresholdInDiscards);
        CliPrintf (CliHandle,
                   " \t\r\n Threshold for IfInDiscards         : %d\r\n",
                   i4LruIfThresholdInDiscards);

        nmhGetLruIfThresholdInErrors (u4IfNum, &i4LruIfThresholdInErrors);
        CliPrintf (CliHandle,
                   " \t\r\n Threshold for IfInErrors           : %d\r\n",
                   i4LruIfThresholdInErrors);

        nmhGetLruIfThresholdInUnknownProtos (u4IfNum,
                                             &i4LruIfThresholdInUnknownProtos);
        CliPrintf (CliHandle,
                   " \t\r\n Threshold for IfInUnknownProtos    : %d\r\n",
                   i4LruIfThresholdInUnknownProtos);

        nmhGetLruIfThresholdOutDiscards (u4IfNum, &i4LruIfThresholdOutDiscards);
        CliPrintf (CliHandle,
                   " \t\r\n Threshold for IfOutDisacards       : %d\r\n",
                   i4LruIfThresholdOutDiscards);

        nmhGetLruIfThresholdOutErrors (u4IfNum, &i4LruIfThresholdOutErrors);
        CliPrintf (CliHandle,
                   " \t\r\n Threshold for IfOutErrors          : %d\r\n",
                   i4LruIfThresholdOutErrors);

        nmhGetLruIfContinuousSelfTestStatus (u4IfNum, &i4IfContSelfTestStatus);

        if (i4IfContSelfTestStatus == BITE_PASS)
        {
            CliPrintf (CliHandle,
                       " \t\r\n Continuous self test status        : Passed \r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       " \t\r\n Continuous self test status        : Failed \r\n");
        }
        BitetestGetErrCode (BITE_INTERFACE_FAIL, &u4ErrCode, u4IfNum);
        if (u4ErrCode == 0)
        {
            CliPrintf (CliHandle,
                       " \t\r\n Continuous Self Test Failure Reason:None \r\n");
        }
        else
        {
            BiteErrIfCbitFailure (u4IfNum, u4ErrCode, au1ReturnCode);
            CliPrintf (CliHandle,
                       " \t\r\n Continuous Self Test Failure Reason: \r\n");
            CliPrintf (CliHandle, "  %s\r\n", au1ReturnCode);
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : BiteCliIfCreateDelete                                */
/* Description        : This Functions creates or deletes an interface
                        based on the third argument                          
                        if third argument = BITE_CLI_MONITOR_INTERFACE_ENABLE 
                        then interface is created                                 
                        if third argument = BITE_CLI_MONITOR_INTERFACE_DISABLE
                        then interface is deleted*/
/* Input(s)           : CliHandle-cli context ID
                        u4IfNum-Index of the interface
                        u1Operation-BITE_CLI_MONITOR_INTERFACE_ENABLE 
                                    BITE_CLI_MONITOR_INTERFACE_DISBLE        */

/* Output(s)          : None.                                                */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/

INT4
BiteCliIfCreateDelete (tCliHandle CliHandle, UINT4 u4IfNum, UINT4 u4Operation)
{

    UINT4               u4ErrCode = 0;
    tBiteIfInfo        *pBiteIfInfo = NULL;

    if ((u4IfNum < BITE_ONE) || (u4IfNum > gu4MaxIfcs))
    {
        CliPrintf (CliHandle, " \r\n Error: Invalid Interface \r\n");
        return CLI_FAILURE;
    }
    if (u4Operation == BITE_CLI_MONITOR_INTERFACE_ENABLE)
    {

        if (nmhTestv2LruIfRowStatus (&u4ErrCode, u4IfNum,
                                     CREATE_AND_GO) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        pBiteIfInfo = BiteIfFind (u4IfNum);
        if (pBiteIfInfo != NULL)

        {
            CliPrintf (CliHandle,
                       " \r\n Interface is already included for CBIT test monitoring \r\n");

            return CLI_FAILURE;
        }
        /* create the interface with index u4IfNum and
           set the row status to CREATE_AND_GO */
        if (nmhSetLruIfRowStatus (u4IfNum, CREATE_AND_GO) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (u4Operation == BITE_CLI_MONITOR_INTERFACE_DISABLE)
    {

        if (nmhTestv2LruIfRowStatus (&u4ErrCode, u4IfNum,
                                     DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        pBiteIfInfo = BiteIfFind (u4IfNum);

        if (pBiteIfInfo == NULL)
        {

            CliPrintf (CliHandle,
                       " \r\n Interface is already deleted from CBIT test monitoring : \r\n");

            return CLI_FAILURE;
        }
        /* Delete the interface with index u4IfNum and 
           set the row status to DESTROY  */
        if (nmhSetLruIfRowStatus (u4IfNum, DESTROY) == SNMP_FAILURE)
        {

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : BiteCliSetIfThreshold                                */
/* Description        : This Functions sets/resets the threshold values 
                        for interface counters
                        if fourth argument = enabled
                        then threshold value is set for that particular 
                        counter
                        if fourth argument = disabled
                        then threshold value is set to zero for that 
                        particular counter                                   */
/* Input(s)           : CliHandle-cli context ID
                        u4IfNum-Index of the interface
                        u4Type - Type of interface counter
                        i4Status- enabled/disabled                 
                        u4Value - Threshold value/default value              */

/* Output(s)          : None.                                                */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/

INT4
BiteCliSetIfThreshold (tCliHandle CliHandle, UINT4 u4IfNum, UINT4 u4Type,
                       UINT4 u4Value)
{

    UINT4               u4ErrorCode;

    if (nmhValidateIndexInstanceLruIfTable (u4IfNum) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Based on the counter type the low level routines to set the threshold values */
    switch (u4Type)
    {
        case BITE_CLI_IF_OPER_STATUS:

            if (nmhTestv2LruIfThresholdOperStatus
                (&u4ErrorCode, u4IfNum, u4Value) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetLruIfThresholdOperStatus (u4IfNum, u4Value) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            break;

        case BITE_CLI_IF_IN_DISCARDS:

            if (nmhTestv2LruIfThresholdInDiscards
                (&u4ErrorCode, u4IfNum, u4Value) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetLruIfThresholdInDiscards (u4IfNum, u4Value) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            break;

        case BITE_CLI_IF_IN_ERRORS:

            if (nmhTestv2LruIfThresholdInErrors (&u4ErrorCode, u4IfNum, u4Value)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetLruIfThresholdInErrors (u4IfNum, u4Value) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            break;

        case BITE_CLI_IF_IN_UNKNOWN_PROTOS:

            if (nmhTestv2LruIfThresholdInUnknownProtos
                (&u4ErrorCode, u4IfNum, u4Value) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetLruIfThresholdInUnknownProtos (u4IfNum, u4Value) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            break;

        case BITE_CLI_IF_OUT_DISCARDS:

            if (nmhTestv2LruIfThresholdOutDiscards
                (&u4ErrorCode, u4IfNum, u4Value) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetLruIfThresholdOutDiscards (u4IfNum, u4Value) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            break;

        case BITE_CLI_IF_OUT_ERRORS:

            if (nmhTestv2LruIfThresholdOutErrors
                (&u4ErrorCode, u4IfNum, u4Value) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetLruIfThresholdOutErrors (u4IfNum, u4Value) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            break;

        default:
            CliPrintf (CliHandle, "\r\n  Invalid Threshold value  \r\n");
            break;

    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : BiteCliSetIPThreshold                                */
/* Description        : This Functions sets/resets the threshold values
                        for IP  counters
                        if third argument = enabled
                        then threshold value is set for that particular
                        counter
                        if third argument = disabled
                        then threshold value is set to zero for that
                        particular counter                                   */
/* Input(s)           : CliHandle-cli context ID
                        u4Type - Type of IP counter
                        i4Status- enabled/disabled
                        u4Value - Threshold value/default value              */

/* Output(s)          : None.                                                */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/

INT4
BiteCliSetIPThreshold (tCliHandle CliHandle, UINT4 u4Type, UINT4 u4Value)
{
    UINT4               u4ErrorCode;
    /* Based on the counter type the low level routines are called to set 
       threshold values */
    switch (u4Type)
    {
        case BITE_CLI_IP_IN_HDR_ERRORS:

            if (nmhTestv2LruThresholdIpInHdrErrors (&u4ErrorCode, u4Value) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetLruThresholdIpInHdrErrors (u4Value) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            break;

        case BITE_CLI_IP_IN_ADDR_ERRORS:

            if (nmhTestv2LruThresholdIpInAddrErrors (&u4ErrorCode, u4Value) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetLruThresholdIpInAddrErrors (u4Value) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            break;

        case BITE_CLI_IP_IN_UNKNOWN_PROTOS:

            if (nmhTestv2LruThresholdIpInUnknownProtos (&u4ErrorCode, u4Value)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetLruThresholdIpInUnknownProtos (u4Value) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            break;

        case BITE_CLI_IP_IN_DISCARDS:

            if (nmhTestv2LruThresholdIpInDiscards (&u4ErrorCode, u4Value) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetLruThresholdIpInDiscards (u4Value) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            break;

        case BITE_CLI_IP_OUT_DISCARDS:

            if (nmhTestv2LruThresholdIpOutDiscards (&u4ErrorCode, u4Value) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetLruThresholdIpOutDiscards (u4Value) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            break;

        case BITE_CLI_IP_REASM_FAILS:
            if (nmhTestv2LruThresholdIpReasmFails (&u4ErrorCode, u4Value) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetLruThresholdIpReasmFails (u4Value) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            break;

        case BITE_CLI_IP_FRAG_FAILS:

            if (nmhTestv2LruThresholdIpFragFails (&u4ErrorCode, u4Value) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetLruThresholdIpFragFails (u4Value) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            break;

        case BITE_CLI_ICMP_IN_ERRORS:

            if (nmhTestv2LruThresholdIcmpInErrors (&u4ErrorCode, u4Value) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetLruThresholdIcmpInErrors (u4Value) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            break;

        case BITE_CLI_ICMP_OUT_ERRORS:

            if (nmhTestv2LruThresholdIcmpOutErrors (&u4ErrorCode, u4Value) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetLruThresholdIcmpOutErrors (u4Value) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            break;

        case BITE_CLI_TCP_IN_ERRORS:

            if (nmhTestv2LruThresholdTcpInErrors (&u4ErrorCode, u4Value) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetLruThresholdTcpInErrors (u4Value) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            break;

        case BITE_CLI_UDP_IN_ERRORS:

            if (nmhTestv2LruThresholdUdpInErrors (&u4ErrorCode, u4Value) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetLruThresholdUdpInErrors (u4Value) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            break;

        default:
            CliPrintf (CliHandle, "\r\n Invalid Threshold Value \r\n");
            break;
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : BiteShowRunningConfig                                 */
/*                                                                           */
/* Description        : Displays configurations done in BITE module           */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLI context                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*****************************************************************************/
INT4
BiteShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{
    if (BiteSrcScalars (CliHandle) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }                            /* displays configurations for global varaiables */

    if (u4Module == ISS_BITE_SHOW_RUNNING_CONFIG)
    {
        BiteSrcInterfaces (CliHandle);    /*dispalys interface specific
                                           configurations */
    }
    return (CLI_SUCCESS);
}

#endif
