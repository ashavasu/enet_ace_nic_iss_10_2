/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 *
 * Description: This file contains BITE interface related utilities
 *              implementation.
 *********************************************************************/
#ifndef _BITEIF_C_
#define _BITEIF_C_
#include "biteinc.h"

extern INT4 CfaGetIfOperStatus PROTO ((UINT4 u4IfIndex, UINT1 *pu1OperStatus));
/*****************************************************************************/
/* Function                  : BiteIfCreate                                  */
/*                                                                           */
/* Description               : This function is used to create entry in 
                               interface database for the given Interface 
                               number                                        */
/*                                                                           */
/* Input                     : u4IfNum - Index of the Interface              */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : tBiteIfInfo - Pointer to the created interface*/
/*                                                                           */
/*****************************************************************************/

tBiteIfInfo        *
BiteIfCreate (UINT4 u4IfNum)
{
    tBiteIfInfo        *pBiteIfInfo = NULL;
    /* Allocate memory for the interface structure with the index u4IfNum using the */
    /* memory pool */
    /* Return OSIX_FAILURE if memory allocation fails */
    if (MemAllocateMemBlock (gBiteGlobalInfo.BiteMemIfPoolId,
                             (UINT1 **) &pBiteIfInfo) == MEM_FAILURE)
    {
        return (tBiteIfInfo *) OSIX_FAILURE;
    }
    /* Return OSIX_FAILURE if the interface structure is NULL after memory allocation */
    if (pBiteIfInfo == NULL)
    {
        return (tBiteIfInfo *) OSIX_FAILURE;
    }

    gBiteGlobalInfo.au1BiteIfInfo[u4IfNum - 1] = pBiteIfInfo;
    /*initialize all the interface structure variables to default values */
    BiteIfInitialize (pBiteIfInfo, u4IfNum);
    return pBiteIfInfo;            /* return the created interface structure for u4IfNum */

}

/*****************************************************************************/
/* Function                  : BiteCreateIfAll                               */
/*                                                                           */
/* Description               : This function is used to create entry 
                               in interface database for all the interfaces 
                               (SOC_MAX_NUM_DEVICES) in the system.          */
/*                                                                           */
/* Input                     : None                                          */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/

INT4
BiteIfCreateAll (VOID)
{
    tBiteIfInfo        *pBiteIfInfo = NULL;
    UINT4               u4IfNum = 0;

    /* Create all the interfaces during the BITE main task */
    for (u4IfNum = 1; u4IfNum <= gu4MaxIfcs; u4IfNum++)
    {
        /*create interfaces to a maximum of SOC_MAX_NUM_DEVICES */
        /* Return OSIX_FAILURE if memory allocation for any of the interfaces fails */
        if (MemAllocateMemBlock (gBiteGlobalInfo.BiteMemIfPoolId,
                                 (UINT1 **) &pBiteIfInfo) == MEM_FAILURE)
        {
            return OSIX_FAILURE;
        }
        /* Return OSIX_FAILURE if interface structure is NULL after memory allocation */
        if (pBiteIfInfo == NULL)
        {
            return OSIX_FAILURE;
        }
        gBiteGlobalInfo.au1BiteIfInfo[u4IfNum - 1] = pBiteIfInfo;
        /* initialize all the interface structure varaiables to their default values */
        BiteIfInitialize (pBiteIfInfo, u4IfNum);
    }

    return OSIX_SUCCESS;
}

/* Function for initializing the interface structure varaibles to default 
values */
VOID
BiteIfInitialize (tBiteIfInfo * pBiteIfInfo, UINT4 u4IfNum)
{
    pBiteIfInfo->u4IfContSelfTestStatus = BITE_PASS;
    pBiteIfInfo->u4IfContSelfTestRetCode = BITE_ZERO;
    pBiteIfInfo->u4OperStatusCounter = BITE_ZERO;
    pBiteIfInfo->u4ThresholdIfOperStatus = BITE_THRESHOLD_DEFAULT_VALUE;
    pBiteIfInfo->u4IfThresholdInDiscards = BITE_THRESHOLD_DEFAULT_VALUE;
    pBiteIfInfo->u4IfThresholdInErrors = BITE_THRESHOLD_DEFAULT_VALUE;
    pBiteIfInfo->u4IfThresholdInUnknownProtos = BITE_THRESHOLD_DEFAULT_VALUE;
    pBiteIfInfo->u4IfThresholdOutDiscards = BITE_THRESHOLD_DEFAULT_VALUE;
    pBiteIfInfo->u4IfThresholdOutErrors = BITE_THRESHOLD_DEFAULT_VALUE;

    pBiteIfInfo->u4IfInDisbaseval = BITE_ZERO;
    pBiteIfInfo->u4IfInerrbaseval = BITE_ZERO;
    pBiteIfInfo->u4IfInUnknwnProtbaseval = BITE_ZERO;
    pBiteIfInfo->u4IfOutDisbaseval = BITE_ZERO;
    pBiteIfInfo->u4IfOuterrbaseval = BITE_ZERO;
    pBiteIfInfo->u4RowStatus = ACTIVE;
    pBiteIfInfo->u4IfIndex = u4IfNum;
    CfaGetIfOperStatus (u4IfNum, (UINT1 *) (pBiteIfInfo->u4OperStatus));
    return;
}

/*****************************************************************************/
/* Function                  : BiteIfFind                                    */
/*                                                                           */
/* Description               : This functions returns the interface entry 
                               from the interface database for the given 
                               interface number                              */
/*                                                                           */
/* Input                     : u4IfNum - Index of the interface              */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : tBiteIfInfo - Pointer to the entry            */
/*                                                                           */
/*****************************************************************************/

tBiteIfInfo        *
BiteIfFind (UINT4 u4IfNum)
{

    tBiteIfInfo        *pBiteIfInfo = NULL;

    pBiteIfInfo = gBiteGlobalInfo.au1BiteIfInfo[u4IfNum - 1];

    /* Return the interface strucrure for the interface index u4IfNum */
    return pBiteIfInfo;

}

/*****************************************************************************/
/* Function                  : BiteIfDelete                                  */
/*                                                                           */
/* Description               : This function used to delete the interface    */
/*                             entry of the given interface number from the  */
/*                             interface database                            */
/*                                                                           */
/* Input                     : u4IfNum - Index of the interface              */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/

INT4
BiteIfDelete (UINT4 u4IfNum)
{

    tBiteIfInfo        *pBiteIfInfo = NULL;

    /* search the interface struture that has to be deleted using interface index u4IfNum */
    pBiteIfInfo = BiteIfFind (u4IfNum);
    /* Release the memory allocated for that interface structure */
    /* Return OSIX_FAILURE if memory release for the interface structure fails */
    if (MemReleaseMemBlock (gBiteGlobalInfo.BiteMemIfPoolId,
                            (UINT1 *) pBiteIfInfo) == MEM_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gBiteGlobalInfo.au1BiteIfInfo[u4IfNum - 1] = NULL;
    return OSIX_SUCCESS;

}

/*****************************************************************************/
/* Function                  : BiteIfProcessStatusChange                     */
/*                                                                           */
/* Description               : This function is used to process the interface 
                                status change message from the CFA module.   */
/*                                                                           */
/* Input                     : u4IfNum - Index of the interface
                               u1Status - operation status
                                          CFA_UP/CFA_DOWN                    */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   :                                               */
/*                                                                           */
/*****************************************************************************/

VOID
BiteIfProcessStatusChange (UINT4 u4IfNum, UINT1 u1Status)
{
    tBiteIfInfo        *pBiteIfInfo = NULL;
    /* search the interface with the interface index u4IfNum */
    pBiteIfInfo = BiteIfFind (u4IfNum);
    /* Return if the interface structure is NULL */
    if (pBiteIfInfo == NULL)
    {
        return;
    }
    /* check if the interface operation status has changed
       If the status has changed from the initial operation status 
       store the status in a variable for further comparison and 
       increment the operation status counter */

    /*if the operation counter value exceeded the threshold the update the 
       status as failure for CBIT */
    /* overall status failed and failure is due to INTERFACE STATISTICS 
       COUNTER */
    if (pBiteIfInfo->u4OperStatus != u1Status)
    {
        pBiteIfInfo->u4OperStatus = u1Status;

        if ((gBiteGlobalInfo.u4ContSelfTestCmd == BITE_CBIT_TEST_ON) ||
            (gBiteGlobalInfo.u4ContSelfTestCmd == BITE_CBIT_TEST_INPRGS))
        {
            pBiteIfInfo->u4OperStatusCounter++;
            if ((pBiteIfInfo->u4ThresholdIfOperStatus != 0) &&
                (pBiteIfInfo->u4OperStatusCounter >
                 pBiteIfInfo->u4ThresholdIfOperStatus))
            {
                gBiteGlobalInfo.u4FailureType = BITE_HARDWARE_FAILURE;
                gBiteGlobalInfo.u4OverallStatus = BITE_FAILED;
                gBiteGlobalInfo.u4FailureReason = CBIT_TEST_FAIL;
                gBiteGlobalInfo.u4ContSelfTest = BITE_FAILED;
                gBiteGlobalInfo.u4ContSelfTestRetCode |=
                    INTERFACE_STATISTICS_COUNTER_FAIL;
                pBiteIfInfo->u4IfContSelfTestStatus = BITE_FAILED;
                pBiteIfInfo->u4IfContSelfTestRetCode |=
                    INTERFACE_OPER_STATUS_FAIL;
                BitetestLogFile (BITE_CBIT_TEST,
                                 gBiteGlobalInfo.u4ContSelfTestRetCode);
            }
        }
    }

    return;
}

VOID
BiteIfResetOperStatusCounter (VOID)
{
    tBiteIfInfo        *pBiteIfInfo = NULL;
    UINT4               u4IfNum = 0;

    /* Create all the interfaces during the BITE main task */
    for (u4IfNum = 1; u4IfNum <= gu4MaxIfcs; u4IfNum++)
    {
        pBiteIfInfo = BiteIfFind (u4IfNum);
        if (pBiteIfInfo == NULL)
        {
            continue;
        }
        pBiteIfInfo->u4IfContSelfTestStatus = BITE_PASS;
        pBiteIfInfo->u4OperStatusCounter = 0;
        pBiteIfInfo->u4IfContSelfTestRetCode = 0;
    }
    return;
}
#endif
