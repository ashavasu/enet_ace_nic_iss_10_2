/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 *
 * Description: This file contains BITE task timer related routines.
 *********************************************************************/
#ifndef _BITETMR_C_
#define _BITETMR_C_
#include "biteinc.h"

/*****************************************************************************/
/* Function                  : BiteTmrInit                                    */
/*                                                                           */
/* Description               : This routine intializes all the timer related */
/*                             BITE data.                                     */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gBiteGlobalInfo                             */
/*                                                                           */
/* Global Variables Modified : gBiteGlobalInfo.BiteTmrListId               */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/****************************i*************************************************/
INT4
BiteTmrInit (VOID)
{

    /* Create the timer list with the timer id for the one second timer */
    if (TmrCreateTimerList
        ((CONST UINT1 *) BITE_TASK, BITE_TMR_EXP_EVENT, NULL,
         (tTimerListId *) & (gBiteOnesecTimerLst)) != TMR_SUCCESS)
    {

        /* Return OSIX_FAILURE if the timer list creation fails */
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : BiteTmrDeInit                                  */
/*                                                                           */
/* Description               : This routine de-initzes all the timer related */
/*                             Bite data.                                    */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gBiteGlobalInfo                             */
/*                                                                           */
/* Global Variables Modified : gBiteGlobalInfo.BiteTmrListId               */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
INT4
BiteTmrDeInit (VOID)
{
    /* Delete timer list created for the one second timer when the continuous
       Built in Test is stopped */
    if (TmrDeleteTimerList (gBiteOnesecTimerLst) == TMR_FAILURE)
    {
        /* Return OSIX_FAILURE if timer list delation fails */
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : BiteTmrStartTmr                               */
/*                                                                           */
/* Description               : This routine starts the one second timer      */
/*                             in the PTP module. The timer type should also */
/*                             provided along with the timer node. When this */
/*                             routine is called from Expiry handler, it is  */
/*                             assumed that the timer would have been removed*/
/*                             and timer is stopped. When this               */
/*                             flag is set, Timer will be started without    */
/*                             validation.                                   */
/*                                                                           */
/* Input                     : pTmrBlk- Pointer to the structure whose timer */
/*                                      needs to be started                  */
/*                             u1TimeInterval - Duration for which the timer */
/*                                              needs to run.                */
/*                             u1TmrType      - Type of the timer.           */
/*                             u1IsExpired    - OSIX_TRUE indicates, that    */
/*                                              this function is invoked from*/
/*                                              timer expiry thread.         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gBiteGlobalInfo                             */
/*                                                                           */
/* Global Variables Modified : gBiteGlobalInfo.BiteTmrListId               */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_FAILURE/OSIX_SUCCESS                     */
/*                                                                           */
/*****************************************************************************/
INT4
BiteTmrStartTmr ()
{
    UINT4               u4TimeInterval = 1;

    /* start the one second timer for monitoring the counters when the
       Continuous Built in Test is initiated */
    if (TmrStartTimer (gBiteOnesecTimerLst, &(gBiteOnesecTimerNode),
                       (UINT4) u4TimeInterval) != TMR_SUCCESS)
    {
        /* If the timer start fails,delete the created timer list and return 
           OSIX_FAILURE */
        TmrDeleteTimerList (gBiteOnesecTimerLst);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : BiteTmrStopTmr                                */
/*                                                                           */
/* Description               : This routine stops the one second timer       */
/*                                                                           */
/* Input                     : pTmrBlk- Pointer to the structure whose timer */
/*                                      needs to be started                  */
/*                             u1TmrType      - Type of the timer.           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gBiteGlobalInfo                             */
/*                                                                           */
/* Global Variables Modified : gBiteGlobalInfo.BiteTmrListId               */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/

INT4
BiteTmrStopTmr ()
{
    /* stop the one second timer for monitoring the counters when the
       Continuous Built in Test is stopped */
    if (TmrStopTimer (gBiteOnesecTimerLst, &(gBiteOnesecTimerNode)) !=
        TMR_SUCCESS)
    {
        /* If the timer stop fails,delete the created timer list and return 
           OSIX_FAILURE */
        TmrDeleteTimerList (gBiteOnesecTimerLst);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/*****************************************************************************/
/* Function                  : BiteTmrExpHandler                             */
/*                                                                           */
/* Description               : This routine will be invoked whenever a 
                               timer expiry event is received by BITE        */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gBiteGlobalInfo                             */
/*                                                                           */
/* Global Variables Modified : gBiteGlobalInfo.BiteTmrListId               */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
VOID
BiteTmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;

    pExpiredTimers = TmrGetNextExpiredTimer (gBiteOnesecTimerLst);
    /* start monitoring the counters after every one second timer expiry */
    BitetestMonitorCounterStatistics (FALSE);

}

#endif
