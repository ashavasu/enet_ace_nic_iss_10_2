/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssysldb.h,v 1.13 2017/12/28 10:40:16 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSSYSLDB_H
#define _FSSYSLDB_H

UINT1 FsSyslogConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsSyslogFileTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsSyslogFwdTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsSyslogMailTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 fssysl [] ={1,3,6,1,4,1,2076,89};
tSNMP_OID_TYPE fssyslOID = {8, fssysl};


UINT4 FsSyslogLogging [ ] ={1,3,6,1,4,1,2076,89,1,1};
UINT4 FsSyslogTimeStamp [ ] ={1,3,6,1,4,1,2076,89,1,2};
UINT4 FsSyslogConsoleLog [ ] ={1,3,6,1,4,1,2076,89,1,3};
UINT4 FsSyslogSysBuffers [ ] ={1,3,6,1,4,1,2076,89,1,4};
UINT4 FsSyslogClearLog [ ] ={1,3,6,1,4,1,2076,89,1,5};
UINT4 FsSyslogConfigModule [ ] ={1,3,6,1,4,1,2076,89,1,6,1,1};
UINT4 FsSyslogConfigLogLevel [ ] ={1,3,6,1,4,1,2076,89,1,6,1,2};
UINT4 FsSyslogFacility [ ] ={1,3,6,1,4,1,2076,89,1,7};
UINT4 FsSyslogRole [ ] ={1,3,6,1,4,1,2076,89,1,8};
UINT4 FsSyslogLogFile [ ] ={1,3,6,1,4,1,2076,89,1,9};
UINT4 FsSyslogMail [ ] ={1,3,6,1,4,1,2076,89,1,10};
UINT4 FsSyslogProfile [ ] ={1,3,6,1,4,1,2076,89,1,11};
UINT4 FsSyslogRelayPort [ ] ={1,3,6,1,4,1,2076,89,1,12};
UINT4 FsSyslogRelayTransType [ ] ={1,3,6,1,4,1,2076,89,1,13};
UINT4 FsSyslogFileNameOne [ ] ={1,3,6,1,4,1,2076,89,1,14};
UINT4 FsSyslogFileNameTwo [ ] ={1,3,6,1,4,1,2076,89,1,15};
UINT4 FsSyslogFileNameThree [ ] ={1,3,6,1,4,1,2076,89,1,16};
UINT4 FsSyslogFilePriority [ ] ={1,3,6,1,4,1,2076,89,1,17,1,1};
UINT4 FsSyslogFileName [ ] ={1,3,6,1,4,1,2076,89,1,17,1,2};
UINT4 FsSyslogFileRowStatus [ ] ={1,3,6,1,4,1,2076,89,1,17,1,3};
UINT4 FsSyslogServerUpDownTrap [ ] ={1,3,6,1,4,1,2076,89,1,18};
UINT4 FsSyslogFileSize [ ] ={1,3,6,1,4,1,2076,89,1,19};
UINT4 FsSyslogLogSrvAddr [ ] ={1,3,6,1,4,1,2076,89,2,1};
UINT4 FsSyslogLogNoLogServer [ ] ={1,3,6,1,4,1,2076,89,2,2};
UINT4 FsSyslogFwdPriority [ ] ={1,3,6,1,4,1,2076,89,2,3,1,1};
UINT4 FsSyslogFwdAddressType [ ] ={1,3,6,1,4,1,2076,89,2,3,1,2};
UINT4 FsSyslogFwdServerIP [ ] ={1,3,6,1,4,1,2076,89,2,3,1,3};
UINT4 FsSyslogFwdPort [ ] ={1,3,6,1,4,1,2076,89,2,3,1,4};
UINT4 FsSyslogFwdTransType [ ] ={1,3,6,1,4,1,2076,89,2,3,1,5};
UINT4 FsSyslogFwdRowStatus [ ] ={1,3,6,1,4,1,2076,89,2,3,1,6};
UINT4 FsSyslogSmtpSrvAddr [ ] ={1,3,6,1,4,1,2076,89,3,1};
UINT4 FsSyslogSmtpRcvrMailId [ ] ={1,3,6,1,4,1,2076,89,3,2};
UINT4 FsSyslogSmtpSenderMailId [ ] ={1,3,6,1,4,1,2076,89,3,3};
UINT4 FsSyslogMailPriority [ ] ={1,3,6,1,4,1,2076,89,3,4,1,1};
UINT4 FsSyslogMailServAddType [ ] ={1,3,6,1,4,1,2076,89,3,4,1,2};
UINT4 FsSyslogMailServAdd [ ] ={1,3,6,1,4,1,2076,89,3,4,1,3};
UINT4 FsSyslogRxMailId [ ] ={1,3,6,1,4,1,2076,89,3,4,1,4};
UINT4 FsSyslogMailRowStatus [ ] ={1,3,6,1,4,1,2076,89,3,4,1,5};
UINT4 FsSyslogMailServUserName [ ] ={1,3,6,1,4,1,2076,89,3,4,1,6};
UINT4 FsSyslogMailServPassword [ ] ={1,3,6,1,4,1,2076,89,3,4,1,7};
UINT4 FsSyslogSmtpAuthMethod [ ] ={1,3,6,1,4,1,2076,89,3,5};
UINT4 SysLogSrvrUnreachEventTime [ ] ={1,3,6,1,4,1,2076,89,4,1,1};
UINT4 SysLogSrvrUnreachMessage [ ] ={1,3,6,1,4,1,2076,89,4,1,2};


tSNMP_OID_TYPE FsSyslogFileSizeOID = {10, FsSyslogFileSize};

tMbDbEntry fssyslMibEntry[]= {

{{10,FsSyslogLogging}, NULL, FsSyslogLoggingGet, FsSyslogLoggingSet, FsSyslogLoggingTest, FsSyslogLoggingDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsSyslogTimeStamp}, NULL, FsSyslogTimeStampGet, FsSyslogTimeStampSet, FsSyslogTimeStampTest, FsSyslogTimeStampDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 1, 0, "1"},

{{10,FsSyslogConsoleLog}, NULL, FsSyslogConsoleLogGet, FsSyslogConsoleLogSet, FsSyslogConsoleLogTest, FsSyslogConsoleLogDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsSyslogSysBuffers}, NULL, FsSyslogSysBuffersGet, FsSyslogSysBuffersSet, FsSyslogSysBuffersTest, FsSyslogSysBuffersDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "50"},

{{10,FsSyslogClearLog}, NULL, FsSyslogClearLogGet, FsSyslogClearLogSet, FsSyslogClearLogTest, FsSyslogClearLogDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{12,FsSyslogConfigModule}, GetNextIndexFsSyslogConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSyslogConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsSyslogConfigLogLevel}, GetNextIndexFsSyslogConfigTable, FsSyslogConfigLogLevelGet, FsSyslogConfigLogLevelSet, FsSyslogConfigLogLevelTest, FsSyslogConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSyslogConfigTableINDEX, 1, 0, 0, NULL},

{{10,FsSyslogFacility}, NULL, FsSyslogFacilityGet, FsSyslogFacilitySet, FsSyslogFacilityTest, FsSyslogFacilityDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "128"},

{{10,FsSyslogRole}, NULL, FsSyslogRoleGet, FsSyslogRoleSet, FsSyslogRoleTest, FsSyslogRoleDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsSyslogLogFile}, NULL, FsSyslogLogFileGet, FsSyslogLogFileSet, FsSyslogLogFileTest, FsSyslogLogFileDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsSyslogMail}, NULL, FsSyslogMailGet, FsSyslogMailSet, FsSyslogMailTest, FsSyslogMailDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsSyslogProfile}, NULL, FsSyslogProfileGet, FsSyslogProfileSet, FsSyslogProfileTest, FsSyslogProfileDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsSyslogRelayPort}, NULL, FsSyslogRelayPortGet, FsSyslogRelayPortSet, FsSyslogRelayPortTest, FsSyslogRelayPortDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "514"},

{{10,FsSyslogRelayTransType}, NULL, FsSyslogRelayTransTypeGet, FsSyslogRelayTransTypeSet, FsSyslogRelayTransTypeTest, FsSyslogRelayTransTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsSyslogFileNameOne}, NULL, FsSyslogFileNameOneGet, FsSyslogFileNameOneSet, FsSyslogFileNameOneTest, FsSyslogFileNameOneDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsSyslogFileNameTwo}, NULL, FsSyslogFileNameTwoGet, FsSyslogFileNameTwoSet, FsSyslogFileNameTwoTest, FsSyslogFileNameTwoDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsSyslogFileNameThree}, NULL, FsSyslogFileNameThreeGet, FsSyslogFileNameThreeSet, FsSyslogFileNameThreeTest, FsSyslogFileNameThreeDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsSyslogFilePriority}, GetNextIndexFsSyslogFileTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsSyslogFileTableINDEX, 2, 0, 0, NULL},

{{12,FsSyslogFileName}, GetNextIndexFsSyslogFileTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsSyslogFileTableINDEX, 2, 0, 0, NULL},

{{12,FsSyslogFileRowStatus}, GetNextIndexFsSyslogFileTable, FsSyslogFileRowStatusGet, FsSyslogFileRowStatusSet, FsSyslogFileRowStatusTest, FsSyslogFileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSyslogFileTableINDEX, 2, 0, 1, NULL},

{{10,FsSyslogServerUpDownTrap}, NULL, FsSyslogServerUpDownTrapGet, FsSyslogServerUpDownTrapSet, FsSyslogServerUpDownTrapTest, FsSyslogServerUpDownTrapDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsSyslogLogSrvAddr}, NULL, FsSyslogLogSrvAddrGet, FsSyslogLogSrvAddrSet, FsSyslogLogSrvAddrTest, FsSyslogLogSrvAddrDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 1, 0, NULL},

{{10,FsSyslogLogNoLogServer}, NULL, FsSyslogLogNoLogServerGet, FsSyslogLogNoLogServerSet, FsSyslogLogNoLogServerTest, FsSyslogLogNoLogServerDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 1, 0, NULL},

{{12,FsSyslogFwdPriority}, GetNextIndexFsSyslogFwdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsSyslogFwdTableINDEX, 3, 0, 0, NULL},

{{12,FsSyslogFwdAddressType}, GetNextIndexFsSyslogFwdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSyslogFwdTableINDEX, 3, 0, 0, NULL},

{{12,FsSyslogFwdServerIP}, GetNextIndexFsSyslogFwdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsSyslogFwdTableINDEX, 3, 0, 0, NULL},

{{12,FsSyslogFwdPort}, GetNextIndexFsSyslogFwdTable, FsSyslogFwdPortGet, FsSyslogFwdPortSet, FsSyslogFwdPortTest, FsSyslogFwdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsSyslogFwdTableINDEX, 3, 0, 0, "514"},

{{12,FsSyslogFwdTransType}, GetNextIndexFsSyslogFwdTable, FsSyslogFwdTransTypeGet, FsSyslogFwdTransTypeSet, FsSyslogFwdTransTypeTest, FsSyslogFwdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSyslogFwdTableINDEX, 3, 0, 0, "0"},

{{12,FsSyslogFwdRowStatus}, GetNextIndexFsSyslogFwdTable, FsSyslogFwdRowStatusGet, FsSyslogFwdRowStatusSet, FsSyslogFwdRowStatusTest, FsSyslogFwdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSyslogFwdTableINDEX, 3, 0, 1, NULL},

{{10,FsSyslogSmtpSrvAddr}, NULL, FsSyslogSmtpSrvAddrGet, FsSyslogSmtpSrvAddrSet, FsSyslogSmtpSrvAddrTest, FsSyslogSmtpSrvAddrDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 1, 0, NULL},

{{10,FsSyslogSmtpRcvrMailId}, NULL, FsSyslogSmtpRcvrMailIdGet, FsSyslogSmtpRcvrMailIdSet, FsSyslogSmtpRcvrMailIdTest, FsSyslogSmtpRcvrMailIdDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 1, 0, "admin@futsoft.com"},

{{10,FsSyslogSmtpSenderMailId}, NULL, FsSyslogSmtpSenderMailIdGet, FsSyslogSmtpSenderMailIdSet, FsSyslogSmtpSenderMailIdTest, FsSyslogSmtpSenderMailIdDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsSyslogMailPriority}, GetNextIndexFsSyslogMailTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsSyslogMailTableINDEX, 3, 0, 0, NULL},

{{12,FsSyslogMailServAddType}, GetNextIndexFsSyslogMailTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSyslogMailTableINDEX, 3, 0, 0, NULL},

{{12,FsSyslogMailServAdd}, GetNextIndexFsSyslogMailTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsSyslogMailTableINDEX, 3, 0, 0, NULL},

{{12,FsSyslogRxMailId}, GetNextIndexFsSyslogMailTable, FsSyslogRxMailIdGet, FsSyslogRxMailIdSet, FsSyslogRxMailIdTest, FsSyslogMailTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsSyslogMailTableINDEX, 3, 0, 0, NULL},

{{12,FsSyslogMailRowStatus}, GetNextIndexFsSyslogMailTable, FsSyslogMailRowStatusGet, FsSyslogMailRowStatusSet, FsSyslogMailRowStatusTest, FsSyslogMailTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSyslogMailTableINDEX, 3, 0, 1, NULL},

{{12,FsSyslogMailServUserName}, GetNextIndexFsSyslogMailTable, FsSyslogMailServUserNameGet, FsSyslogMailServUserNameSet, FsSyslogMailServUserNameTest, FsSyslogMailTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsSyslogMailTableINDEX, 3, 0, 0, NULL},

{{12,FsSyslogMailServPassword}, GetNextIndexFsSyslogMailTable, FsSyslogMailServPasswordGet, FsSyslogMailServPasswordSet, FsSyslogMailServPasswordTest, FsSyslogMailTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsSyslogMailTableINDEX, 3, 0, 0, NULL},

{{10,FsSyslogSmtpAuthMethod}, NULL, FsSyslogSmtpAuthMethodGet, FsSyslogSmtpAuthMethodSet, FsSyslogSmtpAuthMethodTest, FsSyslogSmtpAuthMethodDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,SysLogSrvrUnreachEventTime}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,SysLogSrvrUnreachMessage}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData fssyslEntry = { 42, fssyslMibEntry };


tMbDbEntry FsSyslogFileSizeMibEntry[]= {

{{10,FsSyslogFileSize}, NULL, FsSyslogFileSizeGet, FsSyslogFileSizeSet, FsSyslogFileSizeTest, FsSyslogFileSizeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData FsSyslogFileSizeEntry = { 1, FsSyslogFileSizeMibEntry };

#endif /* _FSSYSLDB_H */

