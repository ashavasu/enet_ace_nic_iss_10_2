/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: slogglob.h,v 1.9 2014/03/14 12:42:01 siva Exp $
 *
 * Description:File containing the global data accessible by syslog
 *
 *******************************************************************/
#ifndef SYSLOG_GLOB_H
#define SYSLOG_GLOB_H


INT4                gi4SysLogFd;
INT4                gi4SmtpInit = SMTP_DISABLE;
UINT4               gu4SysLogInit;
UINT4               gu4CliSysLogLevel;
UINT2               gu2SyslogTrcModule = 1;
UINT1               au1ErrorBuf[SMTP_ERROR_BUF_LEN];
UINT1               au1CmdBuf[SMTP_CMD_BUF_LEN];
UINT4               u4SmtpTskId;
INT4                gi4SlogSrvUpDownTrap = SNMP_TRAP_ENABLE;

tOsixQId            gSmtpQueId;
tOsixQId            gStdbyQueId;
tOsixQId            gSyslogTcpQueId;
tOsixTaskId         gSmtpTaskId;
tOsixSemId          gsSemId;

tSysLogGlobalInfo gSysLogGlobalInfo;
tSysLogEntry        gaSysLogTbl[MAX_SYSLOG_USERS_LIMIT + 1];
tSysUsrInfo         asUserInfo[MAX_SYSLOG_USERS_LIMIT];
tSysLogParams       gsSysLogParams;
tSysModInfo         asModInfo[SYSLOG_MAX_MODULES + 1];
tEmailAlertParams   gsSmtpAlertParams;
tSysLogRegister     gaSysLogRegister[SYSLOG_MAX_REGISTER];
UINT1               gu1SmtpAuthentication;

tSysLogCallBackEntry gSysLogCallBack[MAX_SYSLOG_EVT];


#endif /* SYSLOG_GLOB_H */

