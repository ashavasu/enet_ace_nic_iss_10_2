/**********************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: sysclipt.h,v 1.11 2017/12/28 10:40:16 siva Exp $ 
 *
 * Description: This has #defines & function proto types of Syslog-CLI sub module.
 *
 ***********************************************************************************/

#ifndef __SYSCLIPT_H__
#define __SYSCLIPT_H__


#define SYSLG_CLI_MAX_ARGS             10
#define SYSLG_INADDR_ANY       0x00000000

/* Prototype declarations for Syslog CLI */
INT4 SysLogShowLogs                PROTO ((tCliHandle));
INT4 SysLogShowLogBuffer           PROTO ((tCliHandle));
INT4 SysLogShowEMailAlerts         PROTO ((tCliHandle));
INT4 SyslgShowRunningConfig        PROTO ((tCliHandle));
INT4 SysLogSetConsoleLog           PROTO ((tCliHandle, INT4));
INT4 SysLogCfgMaxModLogBuff        PROTO ((tCliHandle, INT4));
INT4 SysLogCfgModLogLevel          PROTO ((tCliHandle, INT4));
INT4 SysLogSetLogging              PROTO ((tCliHandle, INT4));
INT4 SyslogEnableTrace             PROTO ((INT4,INT4));
INT4 SysLogSetClearLogs            PROTO ((tCliHandle, INT4));
INT4 SysLogSetFacility             PROTO ((tCliHandle, INT4));
INT4 SysLogSmtpSetMyDomainName     PROTO ((tCliHandle, UINT1 *));
INT4 SysLogCfgMaxUsrLogBuff        PROTO ((tCliHandle, UINT1 *, UINT4));


INT4 SysLogSetRole                 PROTO ((tCliHandle, INT4));
INT4 SysLogShowRole                PROTO ((tCliHandle));
INT4 SysLogSetMail                 PROTO ((tCliHandle, INT4));
INT4 SysLogShowMail                PROTO ((tCliHandle));
INT4 SysLogSetLocalStorage         PROTO ((tCliHandle, INT4));
INT4 SysLogShowLocalStorage        PROTO ((tCliHandle));
INT4 SysLogSetSysLogPort           PROTO ((tCliHandle, INT4));
INT4 SysLogShowSysLogPort          PROTO ((tCliHandle));
INT4 SysLogSetSysLogProfile        PROTO ((tCliHandle, CHR1 *));
INT4 SysLogShowSysLogProfile       PROTO ((tCliHandle));
INT4 SysLogShowInformation         PROTO ((tCliHandle));
INT4 SysLogSetRelayTransType       PROTO ((tCliHandle,INT4));
 
INT4 SysLogShowRelayTransType      PROTO ((tCliHandle));


INT4 SysLogSetFileTable            PROTO ((tCliHandle,INT4,UINT1 *));
INT4 SysLogSetFileSize             PROTO ((tCliHandle,INT4));
INT4 SysLogShowFileTable           PROTO ((tCliHandle));
INT4 SysLogDelFileTableEntry       PROTO ((tCliHandle,UINT4,UINT1 *));
INT4 SysLogSetFwdTable             PROTO ((tCliHandle,UINT4,UINT4,UINT1 *,UINT4,UINT4));  
INT4 SysLogShowFwdTable            PROTO ((tCliHandle));                          
INT4 SysLogDelFwdTable             PROTO ((tCliHandle,UINT4,UINT4,UINT1 *));
INT4 SysLogSetMailTable            PROTO ((tCliHandle,UINT4,UINT4,UINT1 *,UINT1 *, UINT1 *, UINT1 *));
INT4 SysLogShowMailTable           PROTO ((tCliHandle));
INT4 SysLogDelMailTable            PROTO ((tCliHandle,UINT4,UINT4,UINT1 *));

INT4 SysLogSetFileNameOne          PROTO ((tCliHandle,UINT1 *));
INT4 SysLogSetFileNameTwo          PROTO ((tCliHandle,UINT1 *));
INT4 SysLogSetFileNameThree        PROTO ((tCliHandle,UINT1 *));
INT4 SysLogShowFileName            PROTO ((tCliHandle)); 
INT4 SysLogSmtpSetAuthentication   PROTO ((tCliHandle, INT4));
INT4 SysLogSnmpSetTrap             PROTO ((tCliHandle, INT4));
#endif /* __SYSCLIPT_H__ */


