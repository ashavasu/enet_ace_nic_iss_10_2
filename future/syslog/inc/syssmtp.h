/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: syssmtp.h,v 1.12 2014/03/14 12:42:01 siva Exp $
 *
 * Description: Contain the constants, structure definitions for 
 *              SMTP and prototypes of all the functions used for 
 *              SMTP.     
 *
 *******************************************************************/

#ifndef __SMTP_ALERT__
#define __SMTP_ALERT__

#define SMTP_SUCCESS 1
#define SMTP_FAILURE 2

#define SMTP_ENABLE  1
#define SMTP_DISABLE 2

#define SMTP_SET     1
#define SMTP_NOT_SET 2

#define SMTP_Q_NAME        "SMQ"
#define STDB_Q_NAME        "STD"
#define SMTP_QUE_ID        gSmtpQueId
#define SYSLOG_TCP_MSG_QUE_NAME "STQ"
#define SYSLOG_TCP_MSG_QUE_ID        gSyslogTcpQueId
#define SMTP_SEM_ID        gsSemId 
#define SMTP_TASK_NAME     "SMT"
#define SMTP_TASK_PRIORITY 200


#define SMTP_ERROR_BUF_LEN 256
#define SMTP_CMD_BUF_LEN   1024
#define SMTP_RCV_BUF_LEN   512

#define SMTP_PORT          25

#define SMTP_SRV_READY_CODE "220"
#define SMTP_SRV_CLOSE_CODE "221"
#define SMTP_SRV_OK_CODE    "250"
#define SMTP_SRV_START_MAIL "354"
#define SMTP_SRV_AUTH_CODE  "334"  
#define SMTP_SRV_AUTH_SUCCESS "235"

#define SMTP_DOMAIN_NAME_LEN 100
#define SMTP_MAX_STRING      64

#define SMTP_INIT_COMPLETE(u4Status)  lrInitComplete(u4Status)

/* SMTP authentication types */
#define SMTP_NO_AUTHENTICATE    1
#define SMTP_AUTH_LOGIN         2
#define SMTP_AUTH_PLAIN         3
#define SMTP_AUTH_CRAM_MD5      4
#define SMTP_AUTH_DIGEST_MD5    5
typedef struct 
{
   UINT4 u4MyDomainName;     /* Flag that specifies whether our domain is set */
   UINT4 u4ServerDomainName; /* Flag that specifies whether server domain is
                                set */
   UINT4 u4MailServerIp;
   UINT1 au1SysDomainName[SMTP_DOMAIN_NAME_LEN];
   UINT1 au1RcvDomainName[SMTP_DOMAIN_NAME_LEN];
}tEmailAlertParams;

/***************************** Function Prototypes *************************/

INT4 SysLogSmtpInit(VOID);


/* Internal Functions */

INT4 SysLogSmtpInitialize(VOID);
VOID SysLogSmtpSendEmail (UINT1 *, UINT4 );
INT4 SysLogSmtpReadResponse (INT4 ,CONST UINT1 * );
INT4 SysLogSmtpSendCmd (INT4 ,UINT4 ); 
INT4 SysLogSmtpSendData (INT4 ,UINT1 *, UINT4 );
INT4 SysLogSmtpCheckAuthString (INT4, const UINT1 *, UINT1 *);
INT4 SysLogSmtpSendHeloRequest (INT4);
INT4 SysLogSmtpReadSrvStrResp (INT4, UINT1 *, UINT4 *);
INT4 SysLogSmtpReadSocket (INT4, UINT1 *);
#endif
