/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssyslg.h,v 1.4 2017/12/28 10:40:16 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/

/*
 * Automatically generated by FuturePostmosy
 * Do not Edit!!!
 */

#ifndef _SNMP_MIB_H
#define _SNMP_MIB_H

/* SNMP-MIB translation table. */

static struct MIB_OID orig_mib_oid_table[] = {
{"ccitt",        "0"},
{"iso",        "1"},
{"org",        "1.3"},
{"dod",        "1.3.6"},
{"internet",        "1.3.6.1"},
{"directory",        "1.3.6.1.1"},
{"mgmt",        "1.3.6.1.2"},
{"mib-2",        "1.3.6.1.2.1"},
{"transmission",        "1.3.6.1.2.1.10"},
{"dot1dBridge",        "1.3.6.1.2.1.17"},
{"dot1dTp",        "1.3.6.1.2.1.17.4"},
{"experimental",        "1.3.6.1.3"},
{"private",        "1.3.6.1.4"},
{"enterprises",        "1.3.6.1.4.1"},
{"fsSyslog",        "1.3.6.1.4.1.2076.89"},
{"fsSyslogGeneralGroup",        "1.3.6.1.4.1.2076.89.1"},
{"fsSyslogLogging",        "1.3.6.1.4.1.2076.89.1.1"},
{"fsSyslogTimeStamp",        "1.3.6.1.4.1.2076.89.1.2"},
{"fsSyslogConsoleLog",        "1.3.6.1.4.1.2076.89.1.3"},
{"fsSyslogSysBuffers",        "1.3.6.1.4.1.2076.89.1.4"},
{"fsSyslogClearLog",        "1.3.6.1.4.1.2076.89.1.5"},
{"fsSyslogConfigTable",        "1.3.6.1.4.1.2076.89.1.6"},
{"fsSyslogConfigEntry",        "1.3.6.1.4.1.2076.89.1.6.1"},
{"fsSyslogConfigModule",        "1.3.6.1.4.1.2076.89.1.6.1.1"},
{"fsSyslogConfigLogLevel",        "1.3.6.1.4.1.2076.89.1.6.1.2"},
{"fsSyslogFacility",        "1.3.6.1.4.1.2076.89.1.7"},
{"fsSyslogFileSize",        "1.3.6.1.4.1.2076.89.1.19"},
{"fsSyslogLogs",        "1.3.6.1.4.1.2076.89.2"},
{"fsSyslogLogSrvAddr",        "1.3.6.1.4.1.2076.89.2.1"},
{"fsSyslogLogNoLogServer",        "1.3.6.1.4.1.2076.89.2.2"},
{"fsSyslogSmtp",        "1.3.6.1.4.1.2076.89.3"},
{"fsSyslogSmtpSrvAddr",        "1.3.6.1.4.1.2076.89.3.1"},
{"fsSyslogSmtpRcvrMailId",        "1.3.6.1.4.1.2076.89.3.2"},
{"fsSyslogSmtpSenderMailId",        "1.3.6.1.4.1.2076.89.3.3"},
{"sysLogSrvrUnreachEventTime",      "1.3.6.1.4.1.2076.89.4.1.1"},
{"sysLogSrvrUnreachMessage",         "1.3.6.1.4.1.2076.89.4.1.2"},
{"security",        "1.3.6.1.5"},
{"snmpV2",        "1.3.6.1.6"},
{"snmpDomains",        "1.3.6.1.6.1"},
{"snmpProxys",        "1.3.6.1.6.2"},
{"snmpModules",        "1.3.6.1.6.3"},
{"joint-iso-ccitt",        "2"},
{0,0},
};

#endif /* _SNMP_MIB_H */
