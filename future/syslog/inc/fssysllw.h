/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssysllw.h,v 1.10 2017/12/28 10:40:16 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSyslogLogging ARG_LIST((INT4 *));

INT1
nmhGetFsSyslogTimeStamp ARG_LIST((INT4 *));

INT1
nmhGetFsSyslogConsoleLog ARG_LIST((INT4 *));

INT1
nmhGetFsSyslogSysBuffers ARG_LIST((INT4 *));

INT1
nmhGetFsSyslogClearLog ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSyslogLogging ARG_LIST((INT4 ));

INT1
nmhSetFsSyslogTimeStamp ARG_LIST((INT4 ));

INT1
nmhSetFsSyslogConsoleLog ARG_LIST((INT4 ));

INT1
nmhSetFsSyslogSysBuffers ARG_LIST((INT4 ));

INT1
nmhSetFsSyslogClearLog ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSyslogLogging ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSyslogTimeStamp ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSyslogConsoleLog ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSyslogSysBuffers ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSyslogClearLog ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSyslogLogging ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSyslogTimeStamp ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSyslogConsoleLog ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSyslogSysBuffers ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSyslogClearLog ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsSyslogConfigTable. */
INT1
nmhValidateIndexInstanceFsSyslogConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsSyslogConfigTable  */

INT1
nmhGetFirstIndexFsSyslogConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSyslogConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSyslogConfigLogLevel ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSyslogConfigLogLevel ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSyslogConfigLogLevel ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSyslogConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSyslogFacility ARG_LIST((INT4 *));

INT1
nmhGetFsSyslogRole ARG_LIST((INT4 *));

INT1
nmhGetFsSyslogLogFile ARG_LIST((INT4 *));

INT1
nmhGetFsSyslogMail ARG_LIST((INT4 *));

INT1
nmhGetFsSyslogProfile ARG_LIST((INT4 *));

INT1
nmhGetFsSyslogRelayPort ARG_LIST((INT4 *));

INT1
nmhGetFsSyslogRelayTransType ARG_LIST((INT4 *));

INT1
nmhGetFsSyslogFileNameOne ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSyslogFileNameTwo ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSyslogFileNameThree ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSyslogFacility ARG_LIST((INT4 ));

INT1
nmhSetFsSyslogRole ARG_LIST((INT4 ));

INT1
nmhSetFsSyslogLogFile ARG_LIST((INT4 ));

INT1
nmhSetFsSyslogMail ARG_LIST((INT4 ));

INT1
nmhSetFsSyslogProfile ARG_LIST((INT4 ));

INT1
nmhSetFsSyslogRelayPort ARG_LIST((INT4 ));

INT1
nmhSetFsSyslogRelayTransType ARG_LIST((INT4 ));

INT1
nmhSetFsSyslogFileNameOne ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsSyslogFileNameTwo ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsSyslogFileNameThree ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSyslogFacility ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSyslogRole ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSyslogLogFile ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSyslogMail ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSyslogProfile ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSyslogRelayPort ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSyslogRelayTransType ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSyslogFileNameOne ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsSyslogFileNameTwo ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsSyslogFileNameThree ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSyslogFacility ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSyslogRole ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSyslogLogFile ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSyslogMail ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSyslogProfile ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSyslogRelayPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSyslogRelayTransType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSyslogFileNameOne ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSyslogFileNameTwo ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSyslogFileNameThree ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsSyslogFileTable. */
INT1
nmhValidateIndexInstanceFsSyslogFileTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsSyslogFileTable  */

INT1
nmhGetFirstIndexFsSyslogFileTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSyslogFileTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSyslogFileRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSyslogFileRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSyslogFileRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSyslogFileTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSyslogServerUpDownTrap ARG_LIST((INT4 *));

INT1
nmhGetFsSyslogFileSize ARG_LIST((INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSyslogServerUpDownTrap ARG_LIST((INT4 ));

INT1
nmhSetFsSyslogFileSize ARG_LIST((INT4 ));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSyslogServerUpDownTrap ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSyslogFileSize ARG_LIST((UINT4 *  ,INT4 ));


/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSyslogServerUpDownTrap ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSyslogFileSize ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSyslogLogSrvAddr ARG_LIST((UINT4 *));

INT1
nmhGetFsSyslogLogNoLogServer ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSyslogLogSrvAddr ARG_LIST((UINT4 ));

INT1
nmhSetFsSyslogLogNoLogServer ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSyslogLogSrvAddr ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsSyslogLogNoLogServer ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSyslogLogSrvAddr ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSyslogLogNoLogServer ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsSyslogFwdTable. */
INT1
nmhValidateIndexInstanceFsSyslogFwdTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsSyslogFwdTable  */

INT1
nmhGetFirstIndexFsSyslogFwdTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSyslogFwdTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSyslogFwdPort ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsSyslogFwdTransType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsSyslogFwdRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSyslogFwdPort ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsSyslogFwdTransType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsSyslogFwdRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSyslogFwdPort ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsSyslogFwdTransType ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsSyslogFwdRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSyslogFwdTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSyslogSmtpSrvAddr ARG_LIST((UINT4 *));

INT1
nmhGetFsSyslogSmtpRcvrMailId ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSyslogSmtpSenderMailId ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSyslogSmtpSrvAddr ARG_LIST((UINT4 ));

INT1
nmhSetFsSyslogSmtpRcvrMailId ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsSyslogSmtpSenderMailId ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSyslogSmtpSrvAddr ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsSyslogSmtpRcvrMailId ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsSyslogSmtpSenderMailId ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSyslogSmtpSrvAddr ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSyslogSmtpRcvrMailId ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSyslogSmtpSenderMailId ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsSyslogMailTable. */
INT1
nmhValidateIndexInstanceFsSyslogMailTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsSyslogMailTable  */

INT1
nmhGetFirstIndexFsSyslogMailTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSyslogMailTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSyslogRxMailId ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSyslogMailRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsSyslogMailServUserName ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSyslogMailServPassword ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSyslogRxMailId ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsSyslogMailRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsSyslogMailServUserName ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsSyslogMailServPassword ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSyslogRxMailId ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsSyslogMailRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsSyslogMailServUserName ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsSyslogMailServPassword ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSyslogMailTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSyslogSmtpAuthMethod ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSyslogSmtpAuthMethod ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSyslogSmtpAuthMethod ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSyslogSmtpAuthMethod ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
