/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: slogprot.h,v 1.17 2017/12/28 10:40:16 siva Exp $
 *
 * Description:This file holds the prototypes which is to be
 *             ported to Target or different IP other than
 *             FutureIP
 *
 *******************************************************************/

#ifndef SYSLOG_PROT_H
#define SYSLOG_PROT_H

INT4 SysLogSetMaxLogBuff(UINT4 ,UINT4 );
VOID SysLogStr(UINT4 , UINT4 , UINT4 , const CHR1 *, va_list);
INT4 SysLogAddLogServerIp(UINT4 );
INT4 SysSendLog(UINT4 ,INT1 *);

INT4 SysLogSetModLogLevel(UINT4 , UINT4 );
VOID SysLogClearLogs PROTO((VOID));
INT4 SyslogGetSyslogCount          PROTO ((UINT4 *));
INT4 SyslogGetConsolelogCount      PROTO ((UINT4 *));
INT4 SyslogGetLogBufferCount       PROTO ((UINT4 *));
INT4 SysLogGetTimeStrAndYear       PROTO ((CHR1 []));
INT4 SysLogGetTimeStr              PROTO ((CHR1 []));
INT4 SysLogRegisterAllModules      PROTO ((VOID));

PUBLIC tSysLogFileInfo *  FileUtilFindEntry     PROTO ((INT4 i4FsSyslogPriority,
                                                        UINT1 *FileName));
INT4 SysLogFileInfoAddNodeToList   PROTO ((tSysLogFileInfo *pFileInfo));

INT4 SysLogFwdInfoAddNodeToList    PROTO ((tSysLogFwdInfo *pFwdInfo));

PUBLIC tSysLogFwdInfo *  FwdUtilFindEntry           PROTO ((INT4 i4FsSyslogFwdPriority,
                                          INT4 i4FsSyslogFwdAddressType,
                                          UINT1 * au1IpAddr));
PUBLIC tSysLogMailInfo *  MailUtilFindEntry           PROTO ((INT4 i4FsSyslogMailPriority,
                                          INT4 i4FsSyslogMailAddressType,
                                          UINT1 * au1IpAddr));

INT4 SysLogMailInfoAddNodeToList   PROTO  ((tSysLogMailInfo *pMailInfo));

INT4 SysLogStoreInFile    PROTO ((UINT4 u4Priority, UINT1 *pu1LogBuf, UINT1 *pu1ModName,UINT4 U4FileNo,UINT4 ModuleId));
INT4 SysLogParser PROTO((INT4 *,INT1 *,INT1 *,CHR1 *));
INT4 CheckTimeStamp PROTO((INT4 ,CHR1 *,CHR1 *));
INT4  CheckMonthAndDate PROTO((CHR1 *,INT4 ));

INT4 SendLogMsgV4UdpServer PROTO ((tSysLogFwdInfo *, INT1* ));

INT4 SendLogMsgV6UdpServer PROTO ((tSysLogFwdInfo *, INT1*));
#ifdef RM_WANTED
INT1 SendLogMsgToSyslog (tCRU_BUF_CHAIN_HEADER *, UINT4);
#endif
INT1 SendLogMsgToTcpServer PROTO ((UINT1 **pc1SysMsg));

INT4 SendLogMsgV4TcpServer PROTO ((tSysLogFwdInfo *, INT1* ));

INT4 SendLogMsgV6TcpServer PROTO ((tSysLogFwdInfo *, INT1*));

VOID SysLogProcessTcpRcvdMsg PROTO ((VOID));

VOID SysLogBeepRcvdMsg PROTO((VOID));



INT4 SysLogRelayEnable (VOID);
INT4 SysLogRelayDisable (VOID);
INT4 SysLogUdpV4SockInit(VOID);
#ifdef IP6_WANTED
INT4 SysLogUdpV6SockInit(VOID);
INT4 SysLogTcpV6SockInit(VOID);
VOID SysLogRcvdIpv6Pkt (UINT4);
INT4 SysLogMsgFromV6Client(UINT1*, UINT4);
#endif
INT4 SysLogTcpV4SockInit(VOID);
VOID SysLogPacketOnSocket (INT4);
VOID SysLogTcpPacketOnSocket(INT4);
VOID SysLogTcpPacketOnV6Socket(INT4);
VOID SysLogPacketOnSocket6 (INT4);
VOID SysLogRcvdIpv4Pkt (UINT4);
VOID SysLogTcpRcvdIpv4Pkt (UINT4);
VOID SysLogTcpRcvdIpv6Pkt (UINT4);
INT4 SysLogMsgFromV4Client(UINT1*, UINT4);
VOID SysLogProcessPkts(VOID);
VOID SysLogProcessStdbyMsg (VOID);
INT4 SysLogFromBeepServer (UINT1 *);
INT4 SysLogProfileUpdateAllTransModule (VOID);

INT4 SysSendLogStandby(INT1 *);

INT4 SysLogMemInit PROTO((VOID));
INT4 SysLogSendMailAlert(UINT4,UINT1 * );
INT4 SysLogFindRegisterEntry (UINT1);

INT4 SysLogFindFreeRegisterEntry (VOID);

VOID  SysLogUpdateTransModule (UINT1);
INT4  SysLogRoleUpdateTransModule(UINT1);
INT4  SysLogRoleUpdateAllTransModule(VOID);
INT4  SysLogProfileUpdateTransModule(UINT1);
INT4  SysLogMsgUpdateTransModule(UINT1, UINT1*);

/* Internal Functions */

VOID SyslogReplaceFileTableEntries(INT1 * pi1Filename, 
                                   tSNMP_OCTET_STRING_TYPE * pNewFilename);

UINT1 SyslogUtilValidateIpAddress PROTO ((UINT4 u4IpAddr));
/* Cust Call Back fucntion */
INT4 SysLogCustCallBack (UINT4, ...);

/* Trap Related Function Prototypes */
VOID
SyslogSendTrapMessage (tIPvXAddr);
VOID
SyslogTrapSendNotifications (tSyslogTrapMsg * pSyslogTrapMsg,tIPvXAddr ServerIpAddr);
tSNMP_OID_TYPE     *
SyslogMakeObjIdFromDotNew (INT1 *pi1TextStr);
UINT1              *
SyslogParseSubIdNew (UINT1 *pu1TempPtr, UINT4 *pu4Value);
UINT4 SyslogNotifyIfStatusChange (UINT4 u4IfIndex);
UINT4
SysLogGetLogFileNum(UINT4 u4ModuleId);
INT4 SysLogSetFileNum(UINT4 u4ModuleId,UINT4 u4FileNo);


#endif /* SYSLOG_PROT_H*/
