/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: slogtrc.h,v 1.2 2010/11/11 05:21:44 prabuc Exp $
 *
 * Description: syslog Trace macros 
 *
 *******************************************************************/

#ifndef _SYSLOG_TRACE_H_

#define _SYSLOG_TRACE_H_

/* #define SYSLOG_MAX_TRC_LEVEL  0xffffffff */


#ifdef TRACE_WANTED

#define   SYSLOG_DBG_FLAG     gsSysLogParams.u4SysLogTrcFlag
#define   SYSLOG_NAME         "SYSLOG"

/* General Macros */
#define SYSLOG_TRC(cmod, mode,mod,fmt) \
        UtlTrcLog(SYSLOG_DBG_FLAG, SYSLOG_NAME, mod, fmt)

#define SYSLOG_TRC_ARG1(cmod,mode,  mod, fmt, arg1) \
        UtlTrcLog(SYSLOG_DBG_FLAG, SYSLOG_NAME, mod, fmt, arg1)

#define SYSLOG_TRC_ARG2( cmod, mode, mod, fmt,arg1,arg2) \
        UtlTrcLog(SYSLOG_DBG_FLAG, SYSLOG_NAME, mod, fmt, arg1, arg2)

#define SYSLOG_TRC_ARG3( cmod, mode, mod, fmt,arg1,arg2,arg3) \
        UtlTrcLog(SYSLOG_DBG_FLAG, SYSLOG_NAME, mod, fmt, arg1, arg2,arg3)

#define SYSLOG_TRC_ARG4( cmod, mode, mod, fmt,arg1,arg2,arg3,arg4) \
        UtlTrcLog(SYSLOG_DBG_FLAG, SYSLOG_NAME, mod, fmt, arg1, arg2,arg3, arg4)

#define SYSLOG_TRC_ARG5( cmod, mode, mod, fmt,arg1,arg2,arg3,arg4,arg5) \
        UtlTrcLog(SYSLOG_DBG_FLAG, SYSLOG_NAME, mod, fmt, arg1, arg2,arg3, arg4, arg5)

#define SYSLOG_TRC_ARG6( cmod, mode, mod, fmt,arg1,arg2,arg3,arg4,arg5,arg6) \
        UtlTrcLog(SYSLOG_DBG_FLAG, SYSLOG_NAME, mod, fmt, arg1, arg2,arg3, arg4, arg5, arg6)

#define SYSLOG_DBG(Value, Fmt) \
      UtlTrcLog(SYSLOG_DBG_FLAG,Value,"SYSLOG", Fmt)


#else
#define SYSLOG_TRC(cmod,mask, mod, fmt)
#define SYSLOG_TRC_ARG1(cmod,mask, mod, fmt,arg1)
#define SYSLOG_TRC_ARG2(cmod,mask, mod, fmt,arg1,arg2)
#define SYSLOG_TRC_ARG3(cmod,mask, mod, fmt,arg1,arg2,arg3)
#define SYSLOG_TRC_ARG4(cmod,mask, mod, fmt,arg1,arg2,arg3,arg4)
#define SYSLOG_TRC_ARG5(cmod,mask, mod, fmt,arg1,arg2,arg3,arg4,arg5)
#define SYSLOG_TRC_ARG6(cmod,mask, mod, fmt,arg1,arg2,arg3,arg4,arg5,arg6)

#define SYSLOG_DBG(Value, Fmt) \
    UtlTrcLog(0,Value,"SYSLOG", Fmt)

#endif /* DEBUG_IP */



#define SYSLOG_DBG_ENTRY         0x00010000
#define SYSLOG_DBG_EXIT          0x00020000
#define SYSLOG_DBG_MEM           0x08000000
#define SYSLOG_DBG_FAILURE       0x10000000

#define SYSLOG_INIT_SHUT_TRC     INIT_SHUT_TRC
#define SYSLOG_MGMT_TRC          MGMT_TRC
#define SYSLOG_DATA_PATH_TRC     DATA_PATH_TRC
#define SYSLOG_CONTROL_PATH_TRC  CONTROL_PLANE_TRC
#define SYSLOG_DUMP_TRC          DUMP_TRC
#define SYSLOG_OS_RESOURCE_TRC   OS_RESOURCE_TRC
#define SYSLOG_ALL_FAILURE_TRC   ALL_FAILURE_TRC
#define SYSLOG_BUFFER_TRC        BUFFER_TRC

#endif /* _SYSLOG_TRACE_H_  */
