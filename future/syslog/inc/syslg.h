/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: syslg.h,v 1.14 2015/05/12 12:40:19 siva Exp $
 *
 * Description: Contain the constants, structure definitions for 
 *              SysLogs and prototypes of all the functions used for 
 *              SysLogs.     
 *
 *******************************************************************/

#ifndef __SYSLOGS_H_
#define __SYSLOGS_H_

#define SYSLOG_MAX_MODULES   120   /* Maximum no of modules */
#define SYSLOG_MAX_BUFFERS   200
#define SYSLOG_MIN_LOG_LEVEL 0
#define SYSLOG_MAX_LOG_LEVEL 7
#define SYSLOG_SEM_NAME      "SLG"

#define MAX_MOD_NAME         15   /* Length of module name */

#define SYSLOG_MEMALLOC_FAILURE  -1
#define SYSLOG_OS_FAILURE        -2
#define SYSLOG_INVALID_MODID     -3
#define SYSLOG_INVALID_USRID     -4
#define SYSLOG_INVALID_LEVEL     -5
#define SYSLOG_SOCK_FAILURE      -6
#define SYSLOG_INVALID_VALUE     -7

#define SYSLOG_SUCCESS            1
#define SYSLOG_CLI_SUCCESS        1

#define SYSLOG_PORT         514

#define DEF_SYSLOG_NUMCMD_BUFF    5
#define DEF_SYSLOG_MAX_BUFF       50
#define DEF_SYSLOG_LEVEL          SYSLOG_NOTICE_LEVEL

#define SYSLOG_TRUE               1
#define SYSLOG_FALSE              2

typedef struct {

     UINT4 u4SysLogStatus;
     UINT4 u4NumBuffers;
     UINT4 u4LogConsole;  /* The status of console logging (Enabled/Disabled)*/      
     UINT4 u4LogTelnet;
     UINT4 u4LogSession;
     UINT4 u4TimeStamp;
     UINT4 u4LogServerIp;
     UINT4 u4Facility;

     UINT4 u4TotalLogs;
     UINT4 u4ConsoleLogs; /* Number of log messages trapped to console*/
     UINT4 u4SyslogLogs;  /* Number of log messages send to a syslog server*/
     UINT4 u4Overruns;
     UINT4 u4ClearLogs;
     UINT4 u4FilterLogs; 
     UINT4 u4BufAllocs; 
}tSysLogParams;     

typedef struct sSysLogBuff
{
 INT1  ai1LogStr[SYSLOG_MAX_STRING];
 struct sSysLogBuff *pNext;
}tSysLogBuff;


typedef struct sSysLog 
{
    UINT4 u4MaxLogMsgs;
    UINT4 u4NumMsgLogs; 
    tSysLogBuff *pLogBuff;
    tSysLogBuff *pHead;
    tSysLogBuff *pLast;
    tSysLogBuff *pFree;
}tSysLogEntry;

typedef struct {
   UINT4 u4Status;
   UINT4 u4Level;
   UINT1 au1Name[MAX_MOD_NAME];
   UINT1 au1Reserved[2];
}tSysModInfo;  

/* New fields could be added in Future */

typedef struct 
{
   UINT4 u4Level;
}tSysUsrInfo;  

INT4 SysLogSetMaxLogBuff(UINT4 ,UINT4 );
VOID SysLogStr(UINT4 , UINT4 , UINT4 , const CHR1 *, va_list);
INT4 SysLogAddLogServerIp(UINT4 );
VOID SysSendLog(INT1 *);
INT4 SysLogSetModLogLevel(UINT4 , UINT4 );
INT4 SysLogSetUsrLogLevel(UINT4 , UINT4 );
VOID SysLogClearLogs PROTO((VOID));
INT4 SyslogGetSyslogCount          PROTO ((UINT4 *));
INT4 SyslogGetConsolelogCount      PROTO ((UINT4 *));
INT4 SyslogGetLogBufferCount       PROTO ((UINT4 *));
INT4 SysLogGetTimeStr              PROTO ((CHR1 []));
INT4 SysLogRegisterAllModules       PROTO ((VOID));


#define   SYSLOG_IS_ADDR_CLASS_A(u4Addr)   ((u4Addr & 0x80000000) == 0)  
#define   SYSLOG_IS_ADDR_CLASS_B(u4Addr)   ((u4Addr & 0xc0000000) == 0x80000000)
#define   SYSLOG_IS_ADDR_CLASS_C(u4Addr)   ((u4Addr & 0xe0000000) == 0xc0000000) 
#endif

