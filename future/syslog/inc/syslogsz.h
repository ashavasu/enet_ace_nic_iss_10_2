/*$Id: syslogsz.h,v 1.5 2014/03/14 12:42:01 siva Exp $*/
enum {
    MAX_SYSLOG_AUDIT_INFO_SIZING_ID,
    MAX_SYSLOG_CHR_PKT_SIZING_ID,
    MAX_SYSLOG_FILE_ENTRY_SIZING_ID,
    MAX_SYSLOG_FWD_ENTRY_SIZING_ID,
    MAX_SYSLOG_LOG_BUFF_USERS_SIZING_ID,
    MAX_SYSLOG_MAIL_ENTRY_SIZING_ID,
    MAX_SYSLOG_SMTPQ_DEPTH_SIZING_ID,
    MAX_SYSLOG_UINT_PKT_SIZING_ID,
    MAX_SYSLOG_STD_PKT_SIZING_ID,
    SYSLOG_MAX_SIZING_ID
};


#ifdef  _SYSLOGSZ_C
tMemPoolId SYSLOGMemPoolIds[ SYSLOG_MAX_SIZING_ID];
INT4  SyslogSizingMemCreateMemPools(VOID);
VOID  SyslogSizingMemDeleteMemPools(VOID);
INT4  SyslogSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _SYSLOGSZ_C  */
extern tMemPoolId SYSLOGMemPoolIds[ ];
extern INT4  SyslogSizingMemCreateMemPools(VOID);
extern VOID  SyslogSizingMemDeleteMemPools(VOID);
#endif /*  _SYSLOGSZ_C  */


#ifdef  _SYSLOGSZ_C
tFsModSizingParams FsSYSLOGSizingParams [] = {
{ "tAuditInfo", "MAX_SYSLOG_AUDIT_INFO", sizeof(tAuditInfo),MAX_SYSLOG_AUDIT_INFO, MAX_SYSLOG_AUDIT_INFO,0 },
{ "CHR1[SYSLOG_MESS_LENGTH+SYSLOG_FIFTY]", "MAX_SYSLOG_CHR_PKT", sizeof(CHR1[SYSLOG_MESS_LENGTH+SYSLOG_FIFTY]),MAX_SYSLOG_CHR_PKT, MAX_SYSLOG_CHR_PKT,0 },
{ "tSysLogFileInfo", "MAX_SYSLOG_FILE_ENTRY", sizeof(tSysLogFileInfo),MAX_SYSLOG_FILE_ENTRY, MAX_SYSLOG_FILE_ENTRY,0 },
{ "tSysLogFwdInfo", "MAX_SYSLOG_FWD_ENTRY", sizeof(tSysLogFwdInfo),MAX_SYSLOG_FWD_ENTRY, MAX_SYSLOG_FWD_ENTRY,0 },
{ "tSysLogBuffUserBlock", "MAX_SYSLOG_LOG_BUFF_USERS", sizeof(tSysLogBuffUserBlock),MAX_SYSLOG_LOG_BUFF_USERS, MAX_SYSLOG_LOG_BUFF_USERS,0 },
{ "tSysLogMailInfo", "MAX_SYSLOG_MAIL_ENTRY", sizeof(tSysLogMailInfo),MAX_SYSLOG_MAIL_ENTRY, MAX_SYSLOG_MAIL_ENTRY,0 },
{ "tSysQueMsgBlock", "MAX_SYSLOG_SMTPQ_DEPTH", sizeof(tSysQueMsgBlock),MAX_SYSLOG_SMTPQ_DEPTH, MAX_SYSLOG_SMTPQ_DEPTH,0 },
{ "UINT1[SYSLOG_MAX_PKT_LEN+SYSLOG_FIFTY]", "MAX_SYSLOG_UINT_PKT", sizeof(UINT1[SYSLOG_MAX_PKT_LEN+SYSLOG_FIFTY]),MAX_SYSLOG_UINT_PKT, MAX_SYSLOG_UINT_PKT,0 },
{ "(MAX_LOG_SIZE_STDBY*MAX_LOG_CHR_STDBY)", "MAX_SYSLOG_STD_PKT",(MAX_LOG_SIZE_STDBY*MAX_LOG_CHR_STDBY),MAX_SYSLOG_STD_PKT, MAX_SYSLOG_STD_PKT,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _SYSLOGSZ_C  */
extern tFsModSizingParams FsSYSLOGSizingParams [];
#endif /*  _SYSLOGSZ_C  */


