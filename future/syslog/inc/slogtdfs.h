/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: slogtdfs.h,v 1.16 2017/12/28 10:40:16 siva Exp $
 *
 * Description:File containing all the type declarations of
 *             the data structures used by syslog .
 *
 *******************************************************************/

#ifndef SYSLOG_TYPDFS_H
#define SYSLOG_TYPDFS_H
#include "dns.h"
/* syslog global structure   */
typedef struct
{
    INT1  ai1FileOne[SYS_MAX_FILE_NAME_LEN+3];
    INT1  ai1FileTwo[SYS_MAX_FILE_NAME_LEN+3];
    INT1  ai1FileThree[SYS_MAX_FILE_NAME_LEN+3];
    UINT4 u4SysLogStatus;
    UINT4 u4NumBuffers;
    UINT4 u4LogConsole;  /* The status of console logging (Enabled/Disabled)*/      
    UINT4 u4LogTelnet;
    UINT4 u4LogSession;
    UINT4 u4TimeStamp;
    UINT4 u4LogServerIp;
    UINT4 u4Facility;
    UINT4 u4TotalLogs;
    UINT4 u4ConsoleLogs; /* Number of log messages trapped to console*/
    UINT4 u4SyslogLogs;  /* Number of log messages send to a syslog server*/
    UINT4 u4Overruns;
    UINT4 u4ClearLogs;
    UINT4 u4FilterLogs; 
    UINT4 u4BufAllocs;
    UINT4 u4MailOption;
    UINT4 u4LocalStorage;
    UINT4 u4SysLogRole;
    UINT4 u4RelayTransType;
    UINT4 u4SysLogPort;
    UINT4 u4SysLogProfile;
    UINT4 u4SysLogTrcFlag;
    INT4  i4Sockv4Id;
    INT4  i4Sockv6Id;
    INT4  i4ConnSock;
    FILE  *flashFpOne;
    FILE  *flashFpTwo;
    FILE  *flashFpThree;
    FILE  *flashFp;

} tSysLogParams;     

typedef struct
{
    UINT4     u4ModuleId;
    CHR1      *pc1ModName;
} tModules;

typedef struct sSysLogBuff
{
 INT1  ai1LogStr[SYSLOG_MAX_STRING];
 struct sSysLogBuff *pNext;
}tSysLogBuff;

typedef struct sSysLog 
{
    UINT4 u4MaxLogMsgs;
    UINT4 u4NumMsgLogs; 
    tSysLogBuff *pLogBuff;
    tSysLogBuff *pHead;
    tSysLogBuff *pLast;
    tSysLogBuff *pFree;
}tSysLogEntry;

typedef struct {
    UINT4 u4ModuleId;
    UINT4 u4Status;
    UINT4 u4Level;
    UINT1 au1Name[MAX_MOD_NAME+1];
    UINT4 u4FileNo;
}tSysModInfo;  

/* New fields could be added in Future */

typedef struct
{
    UINT4 u4Level;
}tSysUsrInfo;  

typedef struct sSysLogFileInfo
{
    tTMO_SLL_NODE NextNode;
    UINT4         u4Priority;
    UINT1         au1FileName[SYS_MAX_FILE_NAME_LEN+3];
    UINT4         u4FileSize;
    INT4          i4FileRowStatus;       /* Row status of the file table */    
}tSysLogFileInfo;

typedef struct sSysLogFwdInfo
{
    tTMO_SLL_NODE        NextNode;
    tIPvXAddr            ServIpAddr; /*Server Address ipv4 or ipv6 */
    UINT4                u4Priority;
    UINT4                u4AddrType; /* MANGO: this can be removed */
    UINT4                u4Port;
    UINT4                u4TransType;
    INT4                 i4FwdRowStatus;
    INT4                 i4SockFd;          /* Socket File Descriptor to which
                                               the log messages has to be sent */
    UINT1                au1HostName[DNS_MAX_QUERY_LEN];
    UINT1                au1Pad[1];
}tSysLogFwdInfo;


typedef struct sSysLogMailInfo
{
     tTMO_SLL_NODE        NextNode;
     tIPvXAddr            MailServIpAddr; /*Server Address ipv4 or ipv6 */
     UINT4                u4Priority;
     UINT4                u4AddrType;
     INT4                 i4MailRowStatus;
     UINT1                au1RexMailId[SYSLOG_RXMAILID_LENGTH];
     UINT1                au1SmtpUserName[SMTP_MAX_STRING + SYSLOG_FOUR];
     UINT1                au1SmtpPasswd[SMTP_MAX_STRING + SYSLOG_FOUR];
     UINT1                au1HostName[DNS_MAX_QUERY_LEN];
     UINT1                au1Pad[1];
} tSysLogMailInfo;


typedef struct sSysLogGlobalInfo {
    tTMO_SLL      sysFileTblList;                  /* Head Pointer To SyslogfileTable list   */
    tMemPoolId    sysFilePoolId;                   /* Specifies file tbl memory poolid   */
    tTMO_SLL      sysFwdTblList;                  /* Head Pointer To SyslogfwdTable list   */
    tMemPoolId    sysFwdPoolId;                   /* Specifies fwd tbl memory poolid   */
    tTMO_SLL      sysMailTblList;                  /* Head Pointer To SyslogMailTable list   */
    tMemPoolId    sysMailPoolId;                   /* Specifies mail tbl memory poolid   */
    tMemPoolId    sysQueMsgPoolId;                 /* Specifies the Queue Msg Pool Id */
    tMemPoolId    sysLogBuffPoolId;                /* Specifies the Log Buffer Pool Id */
    tMemPoolId    SysLogUMemPoolId;
    tMemPoolId    SysLogCMemPoolId;
    tMemPoolId    SysLogTMemPoolId;
    tMemPoolId    SysLogStdbyPoolId;
} tSysLogGlobalInfo;

/* structure to maintain the req info about the Transport modules */
typedef struct
{
    UINT1          u1ModuleId;
    UINT1          u1Flag; /* to tell whether already registered or not */
    UINT2          u2AlignmentByte;
    VOID (*pTransFuncPtr) (UINT4,tSysLogTransMsg,UINT1*);
} tSysLogRegister;


/* Syslog Queue Msg MemPool Block */
typedef struct _tSysQueMsgBlock
{
    UINT1        au1SysQueMsg [SYSLOG_MAX_PKT_LEN + SYSLOG_FIFTY + SYSLOG_ONE];
    UINT1        au1Padding [1];
}tSysQueMsgBlock;

/* Syslog Log-Buffer User MemPool Block */
typedef struct _tSysLogBuffUserBlock
{
    tSysLogBuff   aSysLogBuff[SYSLOG_MAX_BUFFERS];
}tSysLogBuffUserBlock;

typedef union SysLogCallBackEntry {
INT4 (*pSysLogRegister) (CONST UINT1 *, UINT4, UINT4 *);
INT4 (*pSysLogDeRegister) (UINT4);
INT4 (*pSysLogMsg) (UINT4, UINT4, const CHR1 *, ...);
} tSysLogCallBackEntry;

enum {
    SYSLOG_REGISTER_EVT = 1,
    SYSLOG_DEREGISTER_EVT,
    SYSLOG_MSG_EVT,
    MAX_SYSLOG_EVT
};

enum {
    ISS_CUSTOM_CONTINUE_PROCESSING = 0,
    ISS_CUSTOM_SKIP_PROCESSING,
    ISS_CUSTOM_FAILURE
};


typedef struct SyslogTrapMsg
{
    CHR1        ac1DateTime[SYSLOG_TRAP_TIME_STR_LEN];    /* Event Time  */
    CHR1        ac1Message[SYSLOG_TRAP_TIME_STR_LEN];
    UINT4       u4Event;                                  /* Event Type  */
}tSyslogTrapMsg;

#define   SYSLOG_CALLBACK         gSysLogCallBack
#endif /* SYSLOG_TYPDFS_H */
