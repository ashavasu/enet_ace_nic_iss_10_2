
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: slogdefs.h,v 1.22 2017/12/28 10:40:16 siva Exp $
 *
 * Description: File containing the macros and constants used in SYSLOG
 *
 *******************************************************************/

#ifndef SYSLOG_DEFS_H
#define SYSLOG_DEFS_H


#define ISS_CUST_SYSLOG_CONFIG   "/var/log/"
#define SYSLOG_MAX_MODULES   120   /* Maximum no of modules */
#define SYSLOG_MAX_BUFFERS   200
#define SYSLOG_MIN_LOG_LEVEL 0
#define SYSLOG_MAX_LOG_LEVEL 7
#define SYSLOG_SEM_NAME      "SLG"
#define SYSLOGLOOP           1
#define SYSLOG_ZERO          0

#define SYSLOG_INVALID_REGISTER -1


#define SYSLOG_INPUT_Q_EVENT     0x00000001
#define SYSLOG_IPV4_PKT_EVENT    0x00000002
#define SYSLOG_IPV6_PKT_EVENT    0x00000004
#define SYSLOG_TCP_IPV4_MSG_EVENT 0x00000008
#define SYSLOG_TCP_IPV6_MSG_EVENT 0x00000010 
#define SYSLOG_BEEP_MSG_EVENT     0x00000020
#define SYSLOG_TCP_LOG_MSG_EVENT 0x00000040 
#define SYSLOG_SYS_LOG_MSG_EVENT 0x00000080 
#define SYSLOG_ALL_EVENTS        0xffffffff

#define MAX_MOD_NAME         15   /* Length of module name */

#define SYSLOG_MEMALLOC_FAILURE  -1
#define SYSLOG_OS_FAILURE        -2
#define SYSLOG_INVALID_MODID     -3
#define SYSLOG_INVALID_USRID     -4
#define SYSLOG_INVALID_LEVEL     -5
#define SYSLOG_SOCK_FAILURE      -6
#define SYSLOG_INVALID_VALUE     -7

#define SYSLOG_CLI_SUCCESS        1

#define SYSLOG_PORT         514
#define SYSLOG_MAX_PKT_LEN      SYSLOG_MAX_STRING

#define MAX_LOG_SIZE_STDBY 10
#define MAX_LOG_CHR_STDBY 80


#define SYSLOG_MAX_TIMESTR_SIZE   18
#define SYSLOG_MAX_TIMEYRSTR_SIZE 22

#define DEF_SYSLOG_NUMCMD_BUFF    5
#define DEF_SYSLOG_MAX_BUFF       50
#define DEF_SYSLOG_LEVEL          SYSLOG_NOTICE_LEVEL

#define SYSLOG_TRUE               1
#define SYSLOG_FALSE              2

#define   SYSLOG_NTOHL                      (UINT4 )OSIX_NTOHL
#define   SYSLOG_HTONL                      (UINT4 )OSIX_HTONL

#define SYSLOG_MAX_REGISTER       5
#define SYSLOG_UP                 1
#define SYSLOG_DOWN               2
#define SYSLOG_MAX_DNS_DOMAIN_NAME               64

#define SYSLOG_NO_SOCK_FD         -1

#define SYSLOG_INET_NTOHL(GrpIp1)\
{\
    UINT4   u4TmpGrpAddr = 0;\
    UINT4   u4Count = 0;\
    UINT1   u1Index = 0;\
    UINT1   au1TmpGrpIp[IPVX_MAX_INET_ADDR_LEN];\
      \
    MEMSET (au1TmpGrpIp, 0, IPVX_MAX_INET_ADDR_LEN);\
    \
    for (u4Count = 0, u1Index = 0; u4Count < IPVX_MAX_INET_ADDR_LEN; \
            u1Index++, u4Count = u4Count + 4)\
    {\
   MEMCPY (&u4TmpGrpAddr, (GrpIp1 + u4Count), sizeof(UINT4));\
        u4TmpGrpAddr = SYSLOG_NTOHL(u4TmpGrpAddr);\
   MEMCPY (&(au1TmpGrpIp[u4Count]), &u4TmpGrpAddr, sizeof(UINT4));\
    }\
   MEMCPY (GrpIp1, au1TmpGrpIp, IPVX_MAX_INET_ADDR_LEN);\
}


#define SYSLOG_INET_HTONL(GrpIp1)\
{\
    UINT4   u4TmpGrpAddr = 0;\
    UINT4   u4Count1 = 0;\
    UINT1   u1Index = 0;\
    UINT1   au1TmpGrpIp[IPVX_MAX_INET_ADDR_LEN];\
      \
    MEMSET (au1TmpGrpIp, 0, IPVX_MAX_INET_ADDR_LEN);\
    \
    for (u4Count1 = 0, u1Index = 0; u4Count1 < IPVX_MAX_INET_ADDR_LEN; \
            u1Index++, u4Count1 = u4Count1 + 4)\
    {\
   MEMCPY (&u4TmpGrpAddr, (GrpIp1 + u4Count1), sizeof(UINT4));\
        u4TmpGrpAddr = SYSLOG_HTONL(u4TmpGrpAddr);\
   MEMCPY (&(au1TmpGrpIp[u4Count1]), &u4TmpGrpAddr, sizeof(UINT4));\
    }\
    MEMCPY (GrpIp1, au1TmpGrpIp, IPVX_MAX_INET_ADDR_LEN);\
}

#define SYSLOG_REGISTER_WITH_BEEP          BeepRegisterSysLog
#define SYSLOG_DEREGISTER_WITH_BEEP        BeepDeRegisterSysLog
#define SYSLOG_APP_ID 1


#define SYSLOG_PRIORITY_MIN 0
#define SYSLOG_PRIORITY_MAX 191


#define SYSLOG_DEVICE_ROLE 1
#define SYSLOG_RELAY_ROLE  2

#define SYSLOG_RELAY_UDP 1
#define SYSLOG_RELAY_TCP 2

#define SYSLOG_FILE_MAX 3

#define SYSLOG_UDP   0
#define SYSLOG_TCP   1
#define SYSLOG_BEEP  2

#define MAX_NO_OF_CONN 5

#define SYSLOG_RXMAILID_LENGTH 100
#define SYSLOG_MESS_LENGTH SYSLOG_MAX_STRING
#define SYS_DELIM_SIZE 4

#define SYS_SET_PRI_FLAG 1
#define SYS_SET_TIME_FLAG 1
#define SYS_SET_MONTH_FLAG 1


#define SYSLOG_FILE1 "iss1"
#define SYSLOG_FILE2 "iss2"
#define SYSLOG_FILE3 "iss3"
#define SYS_MAX_FILE_NAME_SIZE 20
#define SYS_MAX_FILE_NAME_LEN (32 + 1) /* 1 Extra byte added for storing NULL character*/


#define RSYSLOG_RAW_PROFILE    1
#define RSYSLOG_COOKED_PROFILE 2
#define RSYSLOG_DEFAULT_PROFILE "raw"

#define BSD_SYSLOG_PORT 514
#define BSD_SYSLOG_PORT_MIN 1
#define BSD_SYSLOG_PORT_MAX 65535


/*BSD Syslog Parser*/
#define MONTH_IND 4
#define MINUS_ONE -1
#define SYSLOG_FIVE 5
#define DEFAULT_PRIORITY 13
#define SYS_MAX_DAY 31
#define SYS_FEB_MAX_DAY 29
#define SYSLOG_THIRTY 30
#define SYS_MAX_HOUR 23
#define SYS_MAX_MIN 59
#define SYS_MAX_SEC 59
#define SYS_MAX_MONTH 12
#define SYS_NUM_MONTH 11

#define SYSLOG_ONE 1
#define SYSLOG_TWO 2
#define SYSLOG_THREE 3
#define SYSLOG_FOUR 4
#define SYSLOG_FIVE 5
#define SYSLOG_SIX 6
#define SYSLOG_SEVEN 7
#define SYSLOG_EIGHT 8
#define SYSLOG_NINE 9
#define SYSLOG_TEN 10
#define SYSLOG_ELEVEN 11
#define SYSLOG_TWELVE 12
#define SYSLOG_THIRTEEN 13
#define SYSLOG_FOURTEEN 14
#define SYSLOG_FIFTY 50
#define SYSLOG_HUNDRED 100
#define SYSLOG_FORTYEIGHT 48

#define SYS_TAG_MAX_LENGTH 32

/* ROW STATES  */
#define SYS_ACTIVE              1
#define SYS_NOT_IN_SERVICE      2
#define SYS_NOT_READY           3
#define SYS_CREATE_AND_GO       4
#define SYS_CREATE_AND_WAIT     5
#define SYS_DESTROY             6


#define SYS_MEM_BLOCK_NUM       50

#define SysLogSetMem(xdst,xdata,xlen) memset((UINT1 *)xdst,xdata,xlen)


#define FILEENTRY_OFFSET(x,y) FSAP_OFFSETOF(x,y)
#define GET_FILEENTRY(x) ((tSysLogFileInfo *)(VOID *)((UINT1 *)x-FILEENTRY_OFFSET(tSysLogFileInfo,NextNode)))

#define FWDENTRY_OFFSET(x,y) FSAP_OFFSETOF(x,y)
#define GET_FWDENTRY(x) ((tSysLogFwdInfo *)(VOID *)((UINT1 *)x-FWDENTRY_OFFSET(tSysLogFwdInfo,NextNode)))


#define MAILENTRY_OFFSET(x,y) FSAP_OFFSETOF(x,y)
#define GET_MAILENTRY(x) ((tSysLogMailInfo *)(VOID *)((UINT1 *)x-MAILENTRY_OFFSET(tSysLogMailInfo,NextNode)))

#define REX_MAILID_LENGTH 50

#define SYSLOG_MAX_TRACE_LEN      256
#define SYSLOG_MAX_FLASH_FILESIZE (1024*1024)

#define SMTP_INIT_COMPLETE(u4Status)     lrInitComplete(u4Status)

#define   SYSLOG_LTD_B_CAST_ADDR           0xffffffff     /* Limited BroadCast address */
#define   SYSLOG_LOOP_BACK_ADDR_MASK       0xff000000     /* Loop back address 127.x.x.x range */
#define   SYSLOG_LOOP_BACK_ADDRESSES        0x7f000000

#define   SYSLOG_IS_ADDR_CLASS_A(u4Addr)   ((u4Addr & 0x80000000) == 0)  
#define   SYSLOG_IS_ADDR_CLASS_B(u4Addr)   ((u4Addr & 0xc0000000) == 0x80000000)
#define   SYSLOG_IS_ADDR_CLASS_C(u4Addr)   ((u4Addr & 0xe0000000) == 0xc0000000) 


#define SNMP_TRAP_ENABLE  1
#define SNMP_TRAP_DISABLE 2
#define SYSLOG_TRAP_TIME_STR_LEN     24

/* BroadCast address  x.x.x.255/ x.x.255.255/ x.255.255.255 range*/
#define SYSLOG_IS_BRD_CAST_ADDRESS(u4Addr) \
              ((((u4Addr & 0x000000ff) == 0x000000ff)|| \
              ((u4Addr & 0x0000ffff) == 0x0000ffff) || \
              ((u4Addr & 0x00ffffff) == 0x00ffffff)) ? 0 : 1)

#define SYSLOG_NETWORK_ADDR    0xffffff00

#endif /* SYSLOG_DEFS_H */


#define SYSLOG_MAX_SESSION 8


