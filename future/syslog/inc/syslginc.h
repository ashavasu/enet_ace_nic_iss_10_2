/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: syslginc.h,v 1.15 2012/04/18 14:15:39 siva Exp $
 *
 * Description: Contain the include files of SYSLOG Module
 *
 *******************************************************************/

#ifndef _SYSLGINC_H
#define _SYSLGINC_H


#include "lr.h"
#include "ip.h"
#include "fssocket.h"
#include "utilipvx.h"
#include "fssyslog.h"
#include "ipv6.h"

#include <stdarg.h>


#include "slogdefs.h"
#include "syssmtp.h"
#include "slogtdfs.h"
#include "slogprot.h"

#include "snmccons.h"
#include "snmcdefn.h"
#include "fssnmp.h" 
#include  "fssysllw.h"
#ifdef SNMP_2_WANTED
#include  "fssyslwr.h"
#endif
#include "msr.h"
#include "syslogsz.h"

#endif /* _SYSLGINC_H */

