#######################################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved                                 #
# ------------------------------------------                          #
# $Id: make.h,v 1.6 2011/02/01 08:15:08 siva Exp $
#    DESCRIPTION            : Specifies the options and modules to be #
#                             including for building the SYSLOG module#
#                                                                     #
#######################################################################


###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


TOTAL_OPNS = $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

SYSLOG_BASE_DIR = ${BASE_DIR}/syslog
SYSLOG_SRC_DIR  = ${SYSLOG_BASE_DIR}/src
SYSLOG_INC_DIR  = ${SYSLOG_BASE_DIR}/inc
SYSLOG_OBJ_DIR  = ${SYSLOG_BASE_DIR}/obj
AUTH_UTIL_INC_DIR = ${BASE_DIR}/util/auth 

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${SYSLOG_INC_DIR} -I${AUTH_UTIL_INC_DIR}
#ifdef SNMP_3_WANTED
SNMP_INCLUDES   =  -I${BASE_DIR}/snmpv3
#else
#ifdef SNMP_2_WANTED
SNMP_INCLUDES   =  -I${BASE_DIR}/snmp_2
#endif
#endif

INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS} ${SNMP2_INCLUDES}

#############################################################################
