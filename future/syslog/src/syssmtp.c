/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: syssmtp.c,v 1.38 2015/03/11 10:58:52 siva Exp $
 *
 * Description: Action routines for SMTP
 *
 *******************************************************************/

#include "syslginc.h"
#include "utilipvx.h"
#include "digmd5.h"
#include "syslgcli.h"
#include "slogtrc.h"

#ifdef SNMP_2_WANTED
#include "fssyslwr.h"
#endif

extern tOsixQId     gSmtpQueId;
extern tOsixQId     gStdbyQueId;
extern tOsixQId     gSyslogTcpQueId;
extern tOsixTaskId  gSmtpTaskId;
extern tSysLogGlobalInfo gSysLogGlobalInfo;

extern tSysLogParams gsSysLogParams;
extern INT4         gi4SmtpInit;
extern tEmailAlertParams gsSmtpAlertParams;

extern UINT1        au1ErrorBuf[SMTP_ERROR_BUF_LEN];
extern UINT1        au1CmdBuf[SMTP_CMD_BUF_LEN];
extern UINT1        gu1SmtpAuthentication;
static UINT1        gau1TmpBuf[SMTP_CMD_BUF_LEN];
static tDigestMd5Client DigestMd5Client;

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SysLogSmtpInit                                   */
/*                                                                          */
/*    Description        : This function is used to initialize the SMTP     */
/*                         protocol.                                        */
/*                                                                          */
/*    Input(s)           : pLogBuffer - pointer to the buffer to be sent    */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/

INT4
SysLogSmtpInit (VOID)
{

    if (OsixQueCrt
        ((UINT1 *) SMTP_Q_NAME, OSIX_MAX_Q_MSG_LEN,
         MAX_SYSLOG_SMTPQ_DEPTH, &gSmtpQueId) != OSIX_SUCCESS)
    {
        gi4SmtpInit = SMTP_DISABLE;
        return (OSIX_FAILURE);
    }

    if (OsixQueCrt
        ((UINT1 *) SYSLOG_TCP_MSG_QUE_NAME, OSIX_MAX_Q_MSG_LEN,
         MAX_SYSLOG_TCP_QUE_DEPTH, &gSyslogTcpQueId) != OSIX_SUCCESS)
    {
        gi4SmtpInit = SMTP_DISABLE;
        return (OSIX_FAILURE);
    }

    if (OsixQueCrt
        ((UINT1 *) STDB_Q_NAME, OSIX_MAX_Q_MSG_LEN,
         MAX_SYSLOG_SMTPQ_DEPTH, &gStdbyQueId) != OSIX_SUCCESS)
    {
        gi4SmtpInit = SMTP_DISABLE;
        return (OSIX_FAILURE);
    }
    gsSmtpAlertParams.u4MyDomainName = SMTP_NOT_SET;
    gsSmtpAlertParams.u4ServerDomainName = SMTP_SET;

    MEMSET (gsSmtpAlertParams.au1SysDomainName, 0, SMTP_DOMAIN_NAME_LEN);
    MEMSET (gsSmtpAlertParams.au1RcvDomainName, 0, SMTP_DOMAIN_NAME_LEN);

    gi4SmtpInit = SMTP_ENABLE;
    gu1SmtpAuthentication = SMTP_NO_AUTHENTICATE;

    return (OSIX_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SysLogSmtpMain                                   */
/*                                                                          */
/*    Description        : This function waits for messages from a queue    */
/*                         and invokes a routine to send the received       */
/*                         message to the server.                           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/

VOID
SysLogSmtpMain (INT1 *pParam)
{

    UINT4               u4Events;
    UNUSED_PARAM (pParam);

    if (OsixTskIdSelf (&gSmtpTaskId) != OSIX_SUCCESS)
    {
        SMTP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (SysLogInit () != SYSLOG_SUCCESS)
    {
        gi4SmtpInit = SMTP_DISABLE;
        /* Indicate the status of initialization to the main routine */
        SMTP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    else
    {
        /* Indicate the status of initialization to the main routine */
        SMTP_INIT_COMPLETE (OSIX_SUCCESS);
    }
#ifdef SNMP_2_WANTED
    RegisterFSSYSL ();
#endif

    /* only in Relay Role */
    if (gsSysLogParams.u4SysLogRole == SYSLOG_RELAY_ROLE)
    {
        SysLogRelayEnable ();
    }

    while (SYSLOGLOOP)
    {

        if (OsixEvtRecv (gSmtpTaskId, SYSLOG_ALL_EVENTS, OSIX_WAIT,
                         &u4Events) == OSIX_SUCCESS)
        {

            if (u4Events & SYSLOG_INPUT_Q_EVENT)
            {
                SysLogProcessPkts ();
            }
            if (u4Events & SYSLOG_SYS_LOG_MSG_EVENT)
            {
                SysLogProcessStdbyMsg ();
            }
            /* Tcp Packet received on ipv4 */
            if (u4Events & SYSLOG_TCP_IPV4_MSG_EVENT)
            {

                SysLogTcpRcvdIpv4Pkt (u4Events);

                if ((gsSysLogParams.i4Sockv4Id != -1)
                    && (u4Events & SYSLOG_TCP_IPV4_MSG_EVENT))
                {

                    if (SelAddFd
                        (gsSysLogParams.i4ConnSock,
                         SysLogTcpPacketOnSocket) != OSIX_SUCCESS)
                    {
                        
                        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC, ": Adding Socket Descriptor to Select utility Failed \r\n");
                    }
                }

            }
            /* Tcp Packet received on ipv6 */
            if (u4Events & SYSLOG_TCP_IPV6_MSG_EVENT)
            {
                SysLogTcpRcvdIpv6Pkt (u4Events);

                if ((gsSysLogParams.i4Sockv6Id != -1)
                    && (u4Events & SYSLOG_TCP_IPV6_MSG_EVENT))
                {

                    if (SelAddFd
                        (gsSysLogParams.i4ConnSock,
                         SysLogTcpPacketOnV6Socket) != OSIX_SUCCESS)
                    {
                        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC, ": Adding Socket Descriptor to Select utility Failed \r\n"); 
                    }
                }

            }

            /* Tcp Syslog messages from modules */
            if (u4Events & SYSLOG_TCP_LOG_MSG_EVENT)
            {
                SysLogProcessTcpRcvdMsg ();
            }

            /* Syslog Message from Beep */
            if (u4Events & SYSLOG_BEEP_MSG_EVENT)
            {
                SysLogBeepRcvdMsg ();
            }

            /* Syslog message from the other ipv4 clients */
            if (u4Events & SYSLOG_IPV4_PKT_EVENT)
            {
                SysLogRcvdIpv4Pkt (u4Events);
                /* Check to see the socket was deleted by CLI and is
                 * initialized to -1 */
                if ((gsSysLogParams.i4Sockv4Id != -1) &&
                    (u4Events & SYSLOG_IPV4_PKT_EVENT))
                {
                    if (SelAddFd
                        (gsSysLogParams.i4Sockv4Id,
                         SysLogPacketOnSocket) != OSIX_SUCCESS)
                    {
                     SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC, ": Adding Socket Descriptor to Select utility Failed \r\n");
                    }
                }
            }
            /* Syslog message from the other ipv6 clients */
#ifdef IP6_WANTED
            if (u4Events & SYSLOG_IPV6_PKT_EVENT)
            {
                SysLogRcvdIpv6Pkt (u4Events);
                /* Check to see the socket was deleted by CLI and is
                 * initialized to -1 */
                if ((gsSysLogParams.i4Sockv6Id != -1) &&
                    (u4Events & SYSLOG_IPV6_PKT_EVENT))
                {
                    if (SelAddFd (gsSysLogParams.i4Sockv6Id,
                                  SysLogPacketOnSocket) != OSIX_SUCCESS)
                    {
                       SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC, ": Adding Socket Descriptor to Select utility Failed \r\n");
                    }

                }
            }
#endif
        }

    }

    /* The code should never reach this point */

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SysLogSmtpSendEmail                              */
/*                                                                          */
/*    Description        : This function will send email alert messages to  */
/*                         the mail server.                                 */
/*                                                                          */
/*    Input(s)           : pLogBuffer - pointer to the buffer to be sent    */
/*                         u4Size     - number of words to be sent          */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/

VOID
SysLogSmtpSendEmail (UINT1 *pLogBuffer, UINT4 u4Size)
{
    INT4                i4SockFd = SYSLOG_ZERO;
    UINT4               u4Len = SYSLOG_ZERO;
    INT4                i4Index = SYSLOG_ZERO;
    INT4                i4PriIndex = SYSLOG_ZERO;
    INT4                i4FirNum = SYSLOG_ZERO;
    INT4                i4SecNum = SYSLOG_ZERO;
    INT4                i4ThirdNum = SYSLOG_ZERO;
    UINT4               u4Priority = SYSLOG_ZERO;
    UINT4               u4ServerIp = SYSLOG_ZERO;
    UINT4               u4StrLen = SYSLOG_ZERO;
    UINT4               u4AddrType = SYSLOG_ZERO;
    INT4                i4DnsRetVal = DNS_NOT_RESOLVED;
    tSysLogMailInfo    *pMailInfo = NULL;
    tSysLogMailInfo    *pnode = NULL;
    tDNSResolvedIpInfo  IpInfo;
    tIPvXAddr           ResolAddr;

    struct sockaddr_in6 Ipv6SrvAddr;
    struct sockaddr_in  MailServAddr;
    UINT1               u1AuthFlag = SMTP_NOT_SET;

    MEMSET (&IpInfo, SYSLOG_ZERO, sizeof (tDNSResolvedIpInfo));

    while (pLogBuffer[i4Index] != '>')
    {

        if (pLogBuffer[i4Index] != '<')
        {
            i4PriIndex++;
        }
        i4Index++;
    }

    if (i4PriIndex == SYSLOG_THREE)
    {
        i4FirNum = (UINT4) ((*(pLogBuffer + SYSLOG_ONE)) - SYSLOG_FORTYEIGHT);
        i4SecNum = (UINT4) ((*(pLogBuffer + SYSLOG_TWO)) - SYSLOG_FORTYEIGHT);
        i4ThirdNum =
            (UINT4) ((*(pLogBuffer + SYSLOG_THREE)) - SYSLOG_FORTYEIGHT);

        u4Priority =
            i4FirNum * SYSLOG_HUNDRED + i4SecNum * SYSLOG_TEN + i4ThirdNum;

    }
    else if (i4PriIndex == SYSLOG_TWO)
    {
        i4FirNum = (*(pLogBuffer + SYSLOG_ONE)) - SYSLOG_FORTYEIGHT;
        i4SecNum = (*(pLogBuffer + SYSLOG_TWO)) - SYSLOG_FORTYEIGHT;

        u4Priority = i4FirNum * SYSLOG_TEN + i4SecNum;
    }
    else if (i4PriIndex == SYSLOG_ONE)
    {
        i4FirNum = (*(pLogBuffer + SYSLOG_ONE)) - SYSLOG_FORTYEIGHT;
        u4Priority = i4FirNum;

    }

    /* Code for scanning Tbl */
    TMO_SLL_Scan (&gSysLogGlobalInfo.sysMailTblList, pnode, tSysLogMailInfo *)
    {
        IPVX_ADDR_CLEAR (&ResolAddr);
        pMailInfo = GET_MAILENTRY (pnode);
        u4AddrType = pMailInfo->u4AddrType;

        if (pMailInfo->u4Priority == u4Priority)
        {
            if (u4AddrType == IPVX_DNS_FAMILY)
            {
                if ((i4DnsRetVal =
                     FsUtlIPvXResolveHostName (pMailInfo->au1HostName,
                                               DNS_NONBLOCK,
                                               &IpInfo)) == DNS_RESOLVED)
                {
                    if ((IpInfo.Resolv4Addr.u1AddrLen != 0)
                        || (IpInfo.Resolv6Addr.u1AddrLen != 0))
                    {
                        if (IpInfo.Resolv6Addr.u1AddrLen != 0)
                        {
                            u4AddrType = IPVX_ADDR_FMLY_IPV6;
                            IPVX_ADDR_COPY (&ResolAddr, &(IpInfo.Resolv6Addr));
                        }
                        else
                        {
                            u4AddrType = IPVX_ADDR_FMLY_IPV4;
                            IPVX_ADDR_COPY (&ResolAddr, &(IpInfo.Resolv4Addr));

                        }
                    }
                }
                else
                {
                    switch (i4DnsRetVal)
                    {
                        case DNS_IN_PROGRESS:
                            SYSLOG_DBG (SYSLOG_MGMT_TRC | SYSLOG_CONTROL_PATH_TRC,
                                        "SysLogSmtpSendEmail: Host name resolution in Progress\n");
								break;
                        case DNS_CACHE_FULL:
                            SYSLOG_DBG (SYSLOG_MGMT_TRC | SYSLOG_CONTROL_PATH_TRC,
                                        "SysLogSmtpSendEmail: DNS cache full, cannot resolve at the moment\n");
                            break;
                        default:
                            SYSLOG_DBG (SYSLOG_MGMT_TRC | SYSLOG_CONTROL_PATH_TRC,
                                        "SysLogSmtpSendEmail: Cannot resolve the host name \n");
                            break;
                    }

                    continue;
                }

            }
            else
            {
                IPVX_ADDR_COPY (&ResolAddr, &(pMailInfo->MailServIpAddr));

            }
            if (u4AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                MEMSET (&MailServAddr, SYSLOG_ZERO,
                        sizeof (struct sockaddr_in));
                MEMCPY (&u4ServerIp, ResolAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
                MailServAddr.sin_family = AF_INET;
                MailServAddr.sin_addr.s_addr = (u4ServerIp);
                MailServAddr.sin_port = OSIX_HTONS (SMTP_PORT);
                MEMCPY (gsSmtpAlertParams.au1RcvDomainName,
                        pMailInfo->au1RexMailId,
                        STRLEN (pMailInfo->au1RexMailId));

                if ((i4SockFd = (socket (AF_INET,
                                         SOCK_STREAM,
                                         IPPROTO_TCP))) < SYSLOG_ZERO)
                {
                    return;
                }

                if (connect (i4SockFd, (struct sockaddr *) &MailServAddr,
                             sizeof (MailServAddr)) < SYSLOG_ZERO)
                {
                    close (i4SockFd);
                    return;
                }

            }
            else if (u4AddrType == IPVX_ADDR_FMLY_IPV6)
            {
                MEMSET (&Ipv6SrvAddr, SYSLOG_ZERO,
                        sizeof (struct sockaddr_in6));
                Ipv6SrvAddr.sin6_family = AF_INET6;
                Ipv6SrvAddr.sin6_port = OSIX_HTONS (SMTP_PORT);
                SYSLOG_INET_HTONL (ResolAddr.au1Addr);
                MEMCPY (&Ipv6SrvAddr.sin6_addr.s6_addr,
                        (ResolAddr.au1Addr), sizeof (tIp6Addr));
                MEMCPY (gsSmtpAlertParams.au1RcvDomainName,
                        pMailInfo->au1RexMailId,
                        STRLEN (pMailInfo->au1RexMailId));

                if ((i4SockFd = (socket (AF_INET6, SOCK_STREAM,
                                         IPPROTO_TCP))) < SYSLOG_ZERO)
                {
                    return;
                }

                if (connect (i4SockFd, (struct sockaddr *) &Ipv6SrvAddr,
                             sizeof (Ipv6SrvAddr)) < SYSLOG_ZERO)
                {
                    close (i4SockFd);
                    return;
                }

            }

            /*Ends */
            /* Check if the server is ready */

            if (SysLogSmtpReadResponse
                (i4SockFd, (const UINT1 *) SMTP_SRV_READY_CODE) == SMTP_FAILURE)
            {
                close (i4SockFd);
                return;
            }

            if (gsSmtpAlertParams.u4MyDomainName == SMTP_NOT_SET)
            {
                close (i4SockFd);
                return;
            }

            switch (gu1SmtpAuthentication)
            {
                case SMTP_AUTH_LOGIN:
                    /* Send the Ehlo Command to the server. */
                    MEMSET (au1CmdBuf, 0, SMTP_CMD_BUF_LEN);
                    u4Len = SPRINTF ((CHR1 *) au1CmdBuf, "EHLO SMTPALERT\r\n");
                    SysLogSmtpSendCmd (i4SockFd, u4Len);

                    if (SysLogSmtpReadResponse (i4SockFd,
                                                (const UINT1 *)
                                                SMTP_SRV_OK_CODE) ==
                        SMTP_FAILURE)
                    {
                        close (i4SockFd);
                        return;
                    }
                    /* Check if the string AUTH LOGIN is present in the
                     *  server response */
                    if (SysLogSmtpCheckAuthString (i4SockFd,
                                                   (const UINT1 *) "LOGIN",
                                                   &u1AuthFlag) == SMTP_FAILURE)
                    {
                        close (i4SockFd);
                        return;
                    }
                    /* Read 2 more server response strings to read the DELIVERBY
                     * and HELP responses */
                    if (SysLogSmtpReadResponse (i4SockFd,
                                                (const UINT1 *)
                                                SMTP_SRV_OK_CODE) ==
                        SMTP_FAILURE)
                    {
                        close (i4SockFd);
                        return;
                    }
                    if (SysLogSmtpReadResponse (i4SockFd,
                                                (const UINT1 *)
                                                SMTP_SRV_OK_CODE) ==
                        SMTP_FAILURE)
                    {
                        close (i4SockFd);
                        return;
                    }
                    if (u1AuthFlag == SMTP_NOT_SET)
                    {
                        /* The required authentication mode is not available
                         * send the email without any authentication */
                        if (SysLogSmtpSendHeloRequest (i4SockFd) ==
                            SMTP_FAILURE)
                        {
                            close (i4SockFd);
                            return;
                        }
                    }
                    else
                    {
                        MEMSET (au1CmdBuf, 0, SMTP_CMD_BUF_LEN);
                        u4Len = SPRINTF ((CHR1 *) au1CmdBuf, "AUTH LOGIN\r\n");
                        SysLogSmtpSendCmd (i4SockFd, u4Len);

                        MEMSET (au1CmdBuf, 0, SMTP_CMD_BUF_LEN);
                        SPRINTF ((CHR1 *) au1CmdBuf, "%s ", SMTP_SRV_AUTH_CODE);
                        u4Len = STRLEN ("Username:");
                        UtlBase64EncodeString ((UINT1 *) "Username:",
                                               &au1CmdBuf[SYSLOG_FOUR], &u4Len);
                        if (SysLogSmtpReadResponse (i4SockFd, au1CmdBuf)
                            == SMTP_FAILURE)
                        {
                            close (i4SockFd);
                            return;
                        }
                        MEMSET (au1CmdBuf, 0, SMTP_CMD_BUF_LEN);
                        u4Len = STRLEN (pMailInfo->au1SmtpUserName);
                        UtlBase64EncodeString (pMailInfo->au1SmtpUserName,
                                               au1CmdBuf, &u4Len);
                        STRCAT (au1CmdBuf, "\r\n");
                        u4Len += SYSLOG_TWO;
                        if (SysLogSmtpSendCmd (i4SockFd, u4Len) == SMTP_FAILURE)
                        {
                            close (i4SockFd);
                            return;
                        }

                        MEMSET (au1CmdBuf, 0, SMTP_CMD_BUF_LEN);
                        SPRINTF ((CHR1 *) au1CmdBuf, "%s ", SMTP_SRV_AUTH_CODE);
                        u4Len = STRLEN ("Password:");
                        UtlBase64EncodeString ((UINT1 *) "Password:",
                                               &au1CmdBuf[SYSLOG_FOUR], &u4Len);
                        if (SysLogSmtpReadResponse (i4SockFd, au1CmdBuf)
                            == SMTP_FAILURE)
                        {
                            close (i4SockFd);
                            return;
                        }
                        MEMSET (au1CmdBuf, 0, SMTP_CMD_BUF_LEN);
                        u4Len = STRLEN (pMailInfo->au1SmtpPasswd);
                        UtlBase64EncodeString (pMailInfo->au1SmtpPasswd,
                                               au1CmdBuf, &u4Len);
                        STRCAT (au1CmdBuf, "\r\n");
                        u4Len += SYSLOG_TWO;
                        SysLogSmtpSendCmd (i4SockFd, u4Len);

                        if (SysLogSmtpReadResponse (i4SockFd,
                                                    (const UINT1 *)
                                                    SMTP_SRV_AUTH_SUCCESS) ==
                            SMTP_FAILURE)
                        {
                            close (i4SockFd);
                            return;
                        }
                    }
                    break;

                case SMTP_AUTH_PLAIN:
                    /* Send the Ehlo Command to the server. */
                    MEMSET (au1CmdBuf, 0, SMTP_CMD_BUF_LEN);
                    u4Len = SPRINTF ((CHR1 *) au1CmdBuf, "EHLO SMTPALERT\r\n");
                    SysLogSmtpSendCmd (i4SockFd, u4Len);

                    if (SysLogSmtpReadResponse (i4SockFd,
                                                (const UINT1 *)
                                                SMTP_SRV_OK_CODE) ==
                        SMTP_FAILURE)
                    {
                        close (i4SockFd);
                        return;
                    }
                    /* Check if the string AUTH PLAIN is present in the
                     *  server response */
                    if (SysLogSmtpCheckAuthString (i4SockFd,
                                                   (const UINT1 *) "PLAIN",
                                                   &u1AuthFlag) == SMTP_FAILURE)
                    {
                        close (i4SockFd);
                        return;
                    }
                    /* Read 2 more server response strings to read the DELIVERBY
                     * and HELP responses */
                    if (SysLogSmtpReadResponse (i4SockFd,
                                                (const UINT1 *)
                                                SMTP_SRV_OK_CODE) ==
                        SMTP_FAILURE)
                    {
                        close (i4SockFd);
                        return;
                    }
                    if (SysLogSmtpReadResponse (i4SockFd,
                                                (const UINT1 *)
                                                SMTP_SRV_OK_CODE) ==
                        SMTP_FAILURE)
                    {
                        close (i4SockFd);
                        return;
                    }

                    if (u1AuthFlag == SMTP_NOT_SET)
                    {
                        /* The required authentication mode is not available
                         * send the email without any authentication */
                        if (SysLogSmtpSendHeloRequest (i4SockFd) ==
                            SMTP_FAILURE)
                        {
                            close (i4SockFd);
                            return;
                        }
                    }
                    else
                    {
                        MEMSET (au1CmdBuf, '\0', SMTP_CMD_BUF_LEN);

                        u4Len = STRLEN (pMailInfo->au1SmtpUserName);
                        MEMCPY (au1CmdBuf, pMailInfo->au1SmtpUserName, u4Len);
                        MEMCPY (&au1CmdBuf[u4Len + 1],
                                pMailInfo->au1SmtpUserName, u4Len);
                        u4Len += u4Len + SYSLOG_TWO;
                        u4StrLen = STRLEN (pMailInfo->au1SmtpPasswd);
                        MEMCPY (&au1CmdBuf[u4Len], pMailInfo->au1SmtpPasswd,
                                u4StrLen);
                        u4Len += u4StrLen;

                        MEMSET (gau1TmpBuf, 0, SMTP_CMD_BUF_LEN);
                        UtlBase64EncodeString (au1CmdBuf, gau1TmpBuf, &u4Len);
                        u4StrLen =
                            SNPRINTF ((CHR1 *) au1CmdBuf, SMTP_CMD_BUF_LEN,
                                      "AUTH PLAIN %s\r\n", gau1TmpBuf);
                        SysLogSmtpSendCmd (i4SockFd, u4StrLen);

                        if (SysLogSmtpReadResponse (i4SockFd,
                                                    (const UINT1 *)
                                                    SMTP_SRV_AUTH_SUCCESS) ==
                            SMTP_FAILURE)
                        {
                            close (i4SockFd);
                            return;
                        }
                    }
                    break;

                case SMTP_AUTH_CRAM_MD5:
                    /* Send the Ehlo Command to the server. */
                    MEMSET (au1CmdBuf, 0, SMTP_CMD_BUF_LEN);
                    u4Len = SPRINTF ((CHR1 *) au1CmdBuf, "EHLO SMTPALERT\r\n");
                    SysLogSmtpSendCmd (i4SockFd, u4Len);

                    if (SysLogSmtpReadResponse (i4SockFd,
                                                (const UINT1 *)
                                                SMTP_SRV_OK_CODE) ==
                        SMTP_FAILURE)
                    {
                        close (i4SockFd);
                        return;
                    }
                    /* Check if the string CRAM MD5 is present in the
                     *  server response */
                    if (SysLogSmtpCheckAuthString (i4SockFd,
                                                   (const UINT1 *) "CRAM-MD5",
                                                   &u1AuthFlag) == SMTP_FAILURE)
                    {
                        close (i4SockFd);
                        return;
                    }
                    /* Read 2 more server response strings to read the DELIVERBY
                     * and HELP responses */
                    if (SysLogSmtpReadResponse (i4SockFd,
                                                (const UINT1 *)
                                                SMTP_SRV_OK_CODE) ==
                        SMTP_FAILURE)
                    {
                        close (i4SockFd);
                        return;
                    }
                    if (SysLogSmtpReadResponse (i4SockFd,
                                                (const UINT1 *)
                                                SMTP_SRV_OK_CODE) ==
                        SMTP_FAILURE)
                    {
                        close (i4SockFd);
                        return;
                    }

                    if (u1AuthFlag == SMTP_NOT_SET)
                    {
                        /* The required authentication mode is not available
                         * send the email without any authentication */
                        if (SysLogSmtpSendHeloRequest (i4SockFd) ==
                            SMTP_FAILURE)
                        {
                            close (i4SockFd);
                            return;
                        }
                    }
                    else
                    {
                        MEMSET (au1CmdBuf, 0, SMTP_CMD_BUF_LEN);
                        u4Len =
                            SPRINTF ((CHR1 *) au1CmdBuf, "AUTH CRAM-MD5\r\n");
                        SysLogSmtpSendCmd (i4SockFd, u4Len);

                        MEMSET (gau1TmpBuf, 0, SMTP_CMD_BUF_LEN);
                        if (SysLogSmtpReadSrvStrResp (i4SockFd, gau1TmpBuf,
                                                      &u4StrLen) ==
                            SMTP_FAILURE)
                        {
                            close (i4SockFd);
                            return;
                        }

                        MEMSET (au1CmdBuf, 0, SMTP_CMD_BUF_LEN);
                        AuthCramMd5Digest (pMailInfo->au1SmtpUserName,
                                           pMailInfo->au1SmtpPasswd,
                                           (UINT2)
                                           STRLEN (pMailInfo->au1SmtpPasswd),
                                           gau1TmpBuf, &u4StrLen, au1CmdBuf);

                        STRCAT (au1CmdBuf, "\r\n");
                        u4StrLen += SYSLOG_TWO;
                        SysLogSmtpSendCmd (i4SockFd, u4StrLen);

                        if (SysLogSmtpReadResponse (i4SockFd,
                                                    (const UINT1 *)
                                                    SMTP_SRV_AUTH_SUCCESS) ==
                            SMTP_FAILURE)
                        {
                            close (i4SockFd);
                            return;
                        }
                    }
                    break;

                case SMTP_AUTH_DIGEST_MD5:
                    /* Send the Ehlo Command to the server. */
                    MEMSET (au1CmdBuf, 0, SMTP_CMD_BUF_LEN);
                    u4Len = SPRINTF ((CHR1 *) au1CmdBuf, "EHLO SMTPALERT\r\n");
                    SysLogSmtpSendCmd (i4SockFd, u4Len);

                    if (SysLogSmtpReadResponse (i4SockFd,
                                                (const UINT1 *)
                                                SMTP_SRV_OK_CODE) ==
                        SMTP_FAILURE)
                    {
                        close (i4SockFd);
                        return;
                    }
                    /* Check if the string DIGEST MD5 is present in the
                     *  server response */
                    if (SysLogSmtpCheckAuthString (i4SockFd,
                                                   (const UINT1 *) "DIGEST-MD5",
                                                   &u1AuthFlag) == SMTP_FAILURE)
                    {
                        close (i4SockFd);
                        return;
                    }
                    /* Read 2 more server response strings to read the DELIVERBY
                     * and HELP responses */
                    if (SysLogSmtpReadResponse (i4SockFd,
                                                (const UINT1 *)
                                                SMTP_SRV_OK_CODE) ==
                        SMTP_FAILURE)
                    {
                        close (i4SockFd);
                        return;
                    }
                    if (SysLogSmtpReadResponse (i4SockFd,
                                                (const UINT1 *)
                                                SMTP_SRV_OK_CODE) ==
                        SMTP_FAILURE)
                    {
                        close (i4SockFd);
                        return;
                    }
                    if (u1AuthFlag == SMTP_NOT_SET)
                    {
                        /* The required authentication mode is not available
                         * send the email without any authentication */
                        if (SysLogSmtpSendHeloRequest (i4SockFd) ==
                            SMTP_FAILURE)
                        {
                            close (i4SockFd);
                            return;
                        }
                    }
                    else
                    {
                        MEMSET (au1CmdBuf, 0, SMTP_CMD_BUF_LEN);
                        u4Len =
                            SPRINTF ((CHR1 *) au1CmdBuf, "AUTH DIGEST-MD5\r\n");
                        SysLogSmtpSendCmd (i4SockFd, u4Len);

                        MEMSET (gau1TmpBuf, 0, SMTP_CMD_BUF_LEN);
                        if (SysLogSmtpReadSrvStrResp (i4SockFd, gau1TmpBuf,
                                                      &u4StrLen) ==
                            SMTP_FAILURE)
                        {
                            close (i4SockFd);
                            return;
                        }

                        MEMSET (&DigestMd5Client, 0, sizeof (tDigestMd5Client));
                        /* Update the Username and the password to the
                         * DigestMd5Client structure */
                        STRNCPY (DigestMd5Client.au1UserName,
                                 pMailInfo->au1SmtpUserName,
                                 UTL_MAX_STRING_LEN);
                        DigestMd5Client.au1UserName[UTL_MAX_STRING_LEN - 1] =
                            '\0';
                        STRNCPY (DigestMd5Client.au1Passwd,
                                 pMailInfo->au1SmtpPasswd, UTL_MAX_STRING_LEN);
                        DigestMd5Client.au1Passwd[UTL_MAX_STRING_LEN - 1] =
                            '\0';
                        /* Parse the server challenge and formulate the digest
                         * response */
                        if (DigestMd5ParseSrvChallenge
                            (&DigestMd5Client, gau1TmpBuf,
                             u4StrLen) != UTL_AUTH_SUCCESS)
                        {
                            close (i4SockFd);
                            return;
                        }
                        if (DigestMd5ClientResponse (&DigestMd5Client)
                            == UTL_AUTH_FAILURE)
                        {
                            close (i4SockFd);
                            return;
                        }

                        MEMSET (au1CmdBuf, 0, SMTP_CMD_BUF_LEN);
                        DigestMd5Client.u4BufLen =
                            MEM_MAX_BYTES (DigestMd5Client.u4BufLen,
                                           (SMTP_CMD_BUF_LEN - SYSLOG_THREE));
                        MEMCPY (au1CmdBuf, DigestMd5Client.au1Buffer,
                                DigestMd5Client.u4BufLen);
                        SPRINTF ((CHR1 *) & au1CmdBuf[DigestMd5Client.u4BufLen],
                                 "\r\n");
                        DigestMd5Client.u4BufLen += SYSLOG_TWO;
                        if (SysLogSmtpSendCmd
                            (i4SockFd,
                             DigestMd5Client.u4BufLen) == SMTP_FAILURE)
                        {
                            close (i4SockFd);
                            return;
                        }
                        MEMSET (gau1TmpBuf, 0, SMTP_CMD_BUF_LEN);
                        if (SysLogSmtpReadSrvStrResp (i4SockFd, gau1TmpBuf,
                                                      &u4StrLen) ==
                            SMTP_FAILURE)
                        {
                            close (i4SockFd);
                            return;
                        }
                        if (DigestMd5ParseSrvChallenge (&DigestMd5Client,
                                                        gau1TmpBuf,
                                                        u4StrLen) ==
                            UTL_AUTH_ERROR)
                        {
                            STRNCPY (au1ErrorBuf,
                                    "\r\nAuthentication failed\r\n", STRLEN("\r\nAuthentication failed\r\n"));
			    au1ErrorBuf[STRLEN("\r\nAuthentication failed\r\n")] = '\0';
                            close (i4SockFd);
                            return;
                        }
                        MEMSET (au1CmdBuf, 0, SMTP_CMD_BUF_LEN);
                        u4Len = SPRINTF ((CHR1 *) au1CmdBuf, "\r\n");
                        SysLogSmtpSendCmd (i4SockFd, u4Len);

                        if (SysLogSmtpReadResponse (i4SockFd, (const UINT1 *)
                                                    SMTP_SRV_AUTH_SUCCESS)
                            == SMTP_FAILURE)
                        {
                            close (i4SockFd);
                            return;
                        }
                    }
                    break;

                case SMTP_NO_AUTHENTICATE:

                    if (SysLogSmtpSendHeloRequest (i4SockFd) == SMTP_FAILURE)
                    {
                        close (i4SockFd);
                        return;
                    }
                    break;

            }
            /* Send the MAIL command which identifies the sender
             * and its domain name */
            MEMSET (au1CmdBuf, 0, SMTP_CMD_BUF_LEN);
            u4Len =
                SPRINTF ((CHR1 *) au1CmdBuf, "MAIL FROM: <%s>\r\n",
                         gsSmtpAlertParams.au1SysDomainName);
            SysLogSmtpSendCmd (i4SockFd, u4Len);

            if (SysLogSmtpReadResponse (i4SockFd,
                                        (const UINT1 *) SMTP_SRV_OK_CODE) ==
                SMTP_FAILURE)
            {
                close (i4SockFd);
                return;
            }

            /* Though we check for the domain name in the SendEmailAlert() function
             * this check is repeated here as the user may had reset the domain name
             * variable.
             */

            if (gsSmtpAlertParams.u4ServerDomainName == SMTP_NOT_SET)
            {
                close (i4SockFd);
                return;
            }

            /* The RCPT command identifies the list of receivers. */

            MEMSET (au1CmdBuf, 0, SMTP_CMD_BUF_LEN);
            u4Len =
                SPRINTF ((CHR1 *) au1CmdBuf, "RCPT TO: <%s>\r\n",
                         gsSmtpAlertParams.au1RcvDomainName);
            SysLogSmtpSendCmd (i4SockFd, u4Len);

            if (SysLogSmtpReadResponse (i4SockFd,
                                        (const UINT1 *) SMTP_SRV_OK_CODE) ==
                SMTP_FAILURE)
            {
                close (i4SockFd);
                return;
            }

            /* Send the DATA command to initiate data transfer. */

            MEMSET (au1CmdBuf, 0, SMTP_CMD_BUF_LEN);
            u4Len = SPRINTF ((CHR1 *) au1CmdBuf, "DATA \r\n");
            SysLogSmtpSendCmd (i4SockFd, u4Len);

            if (SysLogSmtpReadResponse (i4SockFd,
                                        (const UINT1 *) SMTP_SRV_START_MAIL) ==
                SMTP_FAILURE)
            {
                close (i4SockFd);
                return;
            }

            SysLogSmtpSendData (i4SockFd, pLogBuffer, u4Size);

            if (SysLogSmtpReadResponse (i4SockFd,
                                        (const UINT1 *) SMTP_SRV_OK_CODE) ==
                SMTP_FAILURE)
            {
                close (i4SockFd);
                return;
            }

            MEMSET (au1CmdBuf, 0, SMTP_CMD_BUF_LEN);
            u4Len = SPRINTF ((CHR1 *) au1CmdBuf, "QUIT\r\n");
            SysLogSmtpSendCmd (i4SockFd, u4Len);

            if (SysLogSmtpReadResponse (i4SockFd,
                                        (const UINT1 *) SMTP_SRV_CLOSE_CODE) ==
                SMTP_FAILURE)
            {
                close (i4SockFd);
                return;
            }

            close (i4SockFd);
        }
    }                            /* Closing Scan */

    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SysLogSmtpReadResponse                           */
/*                                                                          */
/*    Description        : This function reads the SMTP responses sent      */
/*                         from the remote mail server.It takes a code      */
/*                         value as input and checks the server response    */
/*                         for the input code value. If the code is received*/
/*                         as success is returned. Otherwise it return a    */
/*                         failure and copies the error message onto the    */
/*                         error buffer                                     */
/*                                                                          */
/*    Input(s)           : i4SockFd - socket descriptor to the mail server  */
/*                         au1Code  - Expected Code.                        */
/*                                                                          */
/*    Output(s)          : au1ErrorBuf - Is changed in case of error        */
/*                                                                          */
/*    Returns            : SMTP_SUCCESS or SMTP_FAILURE                     */
/*                                                                          */
/****************************************************************************/

INT4
SysLogSmtpReadResponse (INT4 i4SockFd, const UINT1 *pu1Code)
{
    UINT1              *pStr = NULL;
    UINT1               au1RcvBuff[SMTP_RCV_BUF_LEN];

    MEMSET (au1RcvBuff, 0, SMTP_RCV_BUF_LEN);

    if (SysLogSmtpReadSocket (i4SockFd, au1RcvBuff) == SMTP_FAILURE)
    {
        return SMTP_FAILURE;
    }

    if (STRSTR (au1RcvBuff, pu1Code) != NULL)
    {
        return (SMTP_SUCCESS);
    }

    /* SMTP uses a three digit reply code and string along with the code in a 
     * response message. The string is user for interpretation and the three 
     * digit code is for the protocol. We skip the  first three bytes and get
     * the error string
     */

    pStr = &au1RcvBuff[SYSLOG_THREE];
    if (STRLEN (pStr) < SMTP_ERROR_BUF_LEN)
    {
        STRNCPY (au1ErrorBuf, pStr, STRLEN(pStr));
	au1ErrorBuf[STRLEN(pStr)] = '\0';
    }
    return (SMTP_FAILURE);

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SysLogSmtpSendCmd                                */
/*                                                                          */
/*    Description        : This function is used to send a command to the   */
/*                         the server.                                      */
/*                                                                          */
/*    Input(s)           : i4SockFd - socket descriptor to the mail server  */
/*                         u4Len    - Length of the buffer                  */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : The number of bytes written if successful and    */
/*                         SMTP_FAILURE otherwise.                          */
/****************************************************************************/

INT4
SysLogSmtpSendCmd (INT4 i4SockFd, UINT4 u4Len)
{
    INT4                i4BytesWritten;

    if ((i4BytesWritten = send (i4SockFd, au1CmdBuf, u4Len, 0)) > 0)
    {
        return i4BytesWritten;
    }
    else
    {
        return SMTP_FAILURE;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SysLogSmtpSendData                               */
/*                                                                          */
/*    Description        : This function is used to send the data (logs).   */
/*                                                                          */
/*    Input(s)           : pu1Data : pointer to the data to be sent         */
/*                         u4Size  : length of data to be sent              */
/*                         i4SockFd - socket descriptor to the mail server  */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : The number of bytes written if successful and    */
/*                         SMTP_FAILURE otherwise.                          */
/*                                                                          */
/****************************************************************************/

INT4
SysLogSmtpSendData (INT4 i4SockFd, UINT1 *pu1Data, UINT4 u4Size)
{
    INT4                i4BytesWritten;
    INT4                i4Len;
    UINT1               au1DataTermStr[] = "\r\n.\r\n";
    UINT1               au1Subject[] = "Subject: Email Alert \r\n";
    UINT1               au1Str[SMTP_DOMAIN_NAME_LEN + SMTP_DOMAIN_NAME_LEN +
                               SYSLOG_FIFTY];

    MEMSET (au1Str, 0, sizeof (au1Str));

    SPRINTF ((CHR1 *) au1Str, "From: %s\r\nTo: %s\r\n",
             gsSmtpAlertParams.au1SysDomainName,
             gsSmtpAlertParams.au1RcvDomainName);

    if ((i4BytesWritten =
         send (i4SockFd, au1Str, (INT4) STRLEN (au1Str), 0)) <= 0)
    {
        return SMTP_FAILURE;
    }

    if ((i4BytesWritten =
         send (i4SockFd, au1Subject, (INT4) STRLEN (au1Subject), 0)) <= 0)
    {
        return SMTP_FAILURE;
    }

    if ((i4BytesWritten = send (i4SockFd, pu1Data, (INT4) u4Size, 0)) <= 0)
    {
        return SMTP_FAILURE;
    }

    i4Len = STRLEN (au1DataTermStr);
    if (send (i4SockFd, au1DataTermStr, i4Len, 0) <= 0)
    {
        return SMTP_FAILURE;
    }
    else
    {
        return i4BytesWritten;
    }
}

/****************************************************************************/
/*    Function Name      : SysLogSmtpSendHeloRequest                        */
/*                                                                          */
/*    Description        : This function is used to send Helo request to    */
/*                          the server and verifies if the server accepts   */
/*                          the connection request                          */
/*                                                                          */
/*    Input(s)           : i4SockFd - Server socket                         */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : SMTP_SUCCESS / SMTP_FAILURE                      */
/****************************************************************************/
INT4
SysLogSmtpSendHeloRequest (INT4 i4SockFd)
{
    UINT4               u4Len = 0;
    /* Send the Helo Command to the server. 
     * The Helo command is used to identify the client 
     * ie. the host to the server. */
    MEMSET (au1CmdBuf, 0, SMTP_CMD_BUF_LEN);
    u4Len = SPRINTF ((CHR1 *) au1CmdBuf, "HELO SMTPALERT\r\n");
    SysLogSmtpSendCmd (i4SockFd, u4Len);

    if (SysLogSmtpReadResponse (i4SockFd,
                                (const UINT1 *) SMTP_SRV_OK_CODE)
        == SMTP_FAILURE)
    {
        return SMTP_FAILURE;
    }
    return SMTP_SUCCESS;
}

/****************************************************************************/
/*    Function Name      : SysLogSmtpCheckAuthString                        */
/*                                                                          */
/*    Description        : This function is used to read the authentication */
/*                          string from the server and verify if the given  */
/*                          authentication mechanism is present or not.     */
/*                                                                          */
/*    Input(s)           : i4SockFd - Server socket                         */
/*                         pu1AuthString - Authentication mechanism         */
/*                                                                          */
/*    Output(s)          : pu1AuthFlag - SMTP_SET / SMTP_NOT_SET            */
/*                                                                          */
/*    Returns            : SMTP_SUCCESS / SMTP_FAILURE                      */
/****************************************************************************/
INT4
SysLogSmtpCheckAuthString (INT4 i4SockFd, const UINT1 *pu1AuthString,
                           UINT1 *pu1AuthFlag)
{
    UINT1              *pStr = NULL;
    UINT1               au1RcvBuff[SMTP_RCV_BUF_LEN];

    while (1)
    {
        MEMSET (au1RcvBuff, 0, SMTP_RCV_BUF_LEN);

        if (SysLogSmtpReadSocket (i4SockFd, au1RcvBuff) == SMTP_FAILURE)
        {
            return SMTP_FAILURE;
        }

        if (STRSTR (au1RcvBuff, SMTP_SRV_OK_CODE) == NULL)
        {
            /* SMTP uses a three digit reply code and string along 
             * with the code in a response message. The string is user 
             * for interpretation and the three digit code is for the 
             * protocol. We skip the  first three bytes and get the 
             * error string */

            pStr = &au1RcvBuff[SYSLOG_THREE];
            if (STRLEN (pStr) < SMTP_ERROR_BUF_LEN)
            {
                STRNCPY (au1ErrorBuf, pStr, STRLEN(pStr));
		au1ErrorBuf[STRLEN(pStr)] = '\0';
            }
            return (SMTP_FAILURE);
        }
        pStr = &au1RcvBuff[SYSLOG_FOUR];
        if (STRSTR (pStr, "AUTH") != NULL)
        {
            break;
        }
    }

    pStr = &au1RcvBuff[SYSLOG_EIGHT];
    if (STRSTR (pStr, pu1AuthString) == NULL)
    {
        *pu1AuthFlag = SMTP_NOT_SET;
    }
    else
    {
        *pu1AuthFlag = SMTP_SET;
    }
    return SMTP_SUCCESS;
}

/****************************************************************************/
/*    Function Name      : SysLogSmtpReadSrvStrResp                         */
/*                                                                          */
/*    Description        : This function is used to read the server         */
/*                          challenge string used for the challenge/response*/
/*                          authentication mechanisms. The string is base64 */
/*                          decoded before returning.                       */
/*                                                                          */
/*    Input(s)           : i4SockFd - Server socket                         */
/*                                                                          */
/*    Output(s)          : pu1TmpBuf - Server challenge string              */
/*                         pu4Len    - Challenge string length              */
/*                                                                          */
/*    Returns            : SMTP_SUCCESS / SMTP_FAILURE                      */
/****************************************************************************/
INT4
SysLogSmtpReadSrvStrResp (INT4 i4SockFd, UINT1 *pu1TmpBuf, UINT4 *pu4Len)
{
    UINT1              *pStr = NULL;
    UINT1               au1RcvBuff[SMTP_RCV_BUF_LEN];
    UINT4               u4Len = 0;

    MEMSET (au1RcvBuff, 0, SMTP_RCV_BUF_LEN);

    if (SysLogSmtpReadSocket (i4SockFd, au1RcvBuff) == SMTP_FAILURE)
    {
        return SMTP_FAILURE;
    }

    if (STRSTR (au1RcvBuff, SMTP_SRV_AUTH_CODE) == NULL)
    {
        /* SMTP uses a three digit reply code and string along 
         * with the code in a response message. The string is user 
         * for interpretation and the three digit code is for the 
         * protocol. We skip the  first three bytes and get the 
         * error string */

        pStr = &au1RcvBuff[SYSLOG_THREE];
        if (STRLEN (pStr) < SMTP_ERROR_BUF_LEN)
        {
            STRNCPY (au1ErrorBuf, pStr, STRLEN(pStr));
	    au1ErrorBuf[STRLEN(pStr)] = '\0';
        }
        return (SMTP_FAILURE);
    }
    u4Len = STRLEN (&au1RcvBuff[SYSLOG_FOUR]);
    /* To consider the string without the \r\n characters */
    u4Len -= SYSLOG_TWO;
    UtlBase64DecodeString (&au1RcvBuff[SYSLOG_FOUR], pu1TmpBuf, &u4Len);
    *pu4Len = u4Len;
    return SMTP_SUCCESS;
}

/****************************************************************************/
/*    Function Name      : SysLogSmtpReadSocket                             */
/*                                                                          */
/*    Description        : This function is used to read the string         */
/*                          given by the smtp mail server.                  */
/*                                                                          */
/*    Input(s)           : i4SockFd - Server socket                         */
/*                                                                          */
/*    Output(s)          : pu1RcvBuff - Received string                     */
/*                                                                          */
/*    Returns            : SMTP_SUCCESS / SMTP_FAILURE                      */
/****************************************************************************/
INT4
SysLogSmtpReadSocket (INT4 i4SockFd, UINT1 *pu1RcvBuff)
{
    INT4                i4State = 0;    /* 0= msg, 1=\r, 2=\r\n(EOM) */
    UINT1               u1Value;
    UINT4               u4Len = 0;

    while (i4State != 2)
    {
        if (recv (i4SockFd, &u1Value, 1, 0) < 0)
        {
            STRNCPY (au1ErrorBuf, "Connection Closed ",STRLEN("Connection Closed "));
	    au1ErrorBuf[STRLEN("Connection Closed ")] = '\0';
            return (SMTP_FAILURE);
        }

        if (u4Len > SMTP_RCV_BUF_LEN - 1)
        {
            break;
        }

        if (u1Value != '\0')
        {
            pu1RcvBuff[u4Len++] = u1Value;
        }

        switch (i4State)
        {
            case 0:
                if (u1Value == '\r')
                {
                    i4State = 1;
                }
                break;
            case 1:
                if (u1Value == '\n')
                {
                    i4State = 2;
                }
                else
                {
                    i4State = 0;    /*reset to msg state */
                }
                break;
            default:
                break;
        }
    }

    if (u4Len <= SMTP_RCV_BUF_LEN - 1)
    {
        pu1RcvBuff[u4Len++] = '\0';
    }
    else
    {
        return (SMTP_FAILURE);
    }

    return (SMTP_SUCCESS);
}
