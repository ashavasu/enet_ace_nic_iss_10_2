/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: slogport.c,v 1.9 2016/03/18 13:06:43 siva Exp $
 *
 * Description:This file contains the routines required for
 *              porting  on different targets
 *
 *******************************************************************/

#include "syslginc.h"
#include "cust.h"

extern tSysLogRegister gaSysLogRegister[SYSLOG_MAX_REGISTER];
extern tSysLogParams gsSysLogParams;
extern tOsixSemId   gsSemId;
extern tOsixTaskId  gSmtpTaskId;
extern tOsixQId     gSmtpQueId;
extern tSysModInfo  asModInfo[SYSLOG_MAX_MODULES + 1];
extern INT4         gi4SmtpInit;
extern tEmailAlertParams gsSmtpAlertParams;
extern tSysLogGlobalInfo gSysLogGlobalInfo;

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogRegister                                         */
/*                                                                           */
/* Description      : This function is invoked by protocol modules to        */
/*                    register the module name and log level with syslog     */
/*                                                                           */
/* Input Parameters : pu1Name - Module Name                                  */
/*                    u4Level - Module Level                                 */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : Identifier - to be used with syslog for reference     */
/*                     -1 - On failure                                       */
/*****************************************************************************/

INT4
SysLogRegister (CONST UINT1 *pu1Name, UINT4 u4Level)
{
    UINT4               u4Count = 0;
    INT4                i4RetVal;

    i4RetVal =
        SysLogCustCallBack (SYSLOG_REGISTER_EVT, pu1Name, u4Level, &u4Count);

    if (ISS_CUSTOM_CONTINUE_PROCESSING == i4RetVal)
    {
        if (OsixSemTake (SMTP_SEM_ID) != OSIX_SUCCESS)
        {
            return (SYSLOG_OS_FAILURE);
        }
        for (u4Count = 1; u4Count <= SYSLOG_MAX_MODULES; ++u4Count)
        {
            if (STRCMP (asModInfo[u4Count].au1Name, pu1Name) == 0)
            {
                if (asModInfo[u4Count].u4Status == SYSLOG_ENABLE)
                {
                    if (asModInfo[u4Count].u4Level == u4Level)
                    {
                        OsixSemGive (SMTP_SEM_ID);
                        return ((INT4) u4Count);
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            else if (asModInfo[u4Count].u4Status == SYSLOG_DISABLE)
            {
                break;
            }

        }

        if (u4Count <= SYSLOG_MAX_MODULES)
        {
            STRNCPY (asModInfo[u4Count].au1Name, pu1Name,
                     sizeof (asModInfo[u4Count].au1Name));

        asModInfo[u4Count].au1Name[STRLEN(pu1Name)] = '\0';
            asModInfo[u4Count].u4Level = u4Level;
            asModInfo[u4Count].u4ModuleId = u4Count;
            asModInfo[u4Count].u4Status = SYSLOG_ENABLE;
            OsixSemGive (SMTP_SEM_ID);
            return ((INT4) u4Count);
        }

        OsixSemGive (SMTP_SEM_ID);
        return (-1);
    }
    return ((INT4) u4Count);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogDeRegister                                       */
/*                                                                           */
/* Description      : This function is invoked by protocol modules to        */
/*                    de-register the module from the syslog module          */
/*                                                                           */
/* Input Parameters : u4ModId - Module Identifier                            */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : SYSLOG_INVALID_MODID - If the module id is invalid    */
/*                     SYSLOG_SUCCESS - On Success                           */
/*****************************************************************************/

INT4
SysLogDeRegister (UINT4 u4ModId)
{
    INT4                i4RetVal;

    i4RetVal = SysLogCustCallBack (SYSLOG_DEREGISTER_EVT, u4ModId);

    if (ISS_CUSTOM_CONTINUE_PROCESSING == i4RetVal)
    {
        if (u4ModId > SYSLOG_MAX_MODULES)
        {
            return (SYSLOG_INVALID_MODID);
        }

        if (asModInfo[u4ModId].u4Status == SYSLOG_DISABLE)
            return (SYSLOG_SUCCESS);

        asModInfo[u4ModId].u4Status = SYSLOG_DISABLE;
    }
    return (SYSLOG_SUCCESS);
}

/*********************************************************************/
/* Function           :   SysLogRegisterTransModule                  */
/* Description        :   Register with Syslog                       */
/*                                                                   */
/*                                                                   */
/*                                                                   */
/*                                                                   */
/* Output(s)          : none                                         */
/*                                                                   */
/* Returns            : None                                         */
/*                                                                   */
/*********************************************************************/

INT4
SysLogRegisterTransModule (UINT1 u1ModuleId,
                           VOID (*pTransFuncPtr) (UINT4 u4MsgType,
                                                  tSysLogTransMsg pSysLogInfo,
                                                  UINT1 *pu1Msg))
{
    INT4                i4Index = SYSLOG_ZERO;

    i4Index = SysLogFindRegisterEntry (u1ModuleId);
    if (i4Index == SYSLOG_INVALID_REGISTER)
    {
        if ((i4Index = SysLogFindFreeRegisterEntry ())
            == SYSLOG_INVALID_REGISTER)
        {
            return SYSLOG_FAILURE;
        }
    }
    gaSysLogRegister[i4Index].u1ModuleId = u1ModuleId;
    gaSysLogRegister[i4Index].pTransFuncPtr = pTransFuncPtr;
    gaSysLogRegister[i4Index].u1Flag = SYSLOG_UP;

    /* update the current Role and Profile to the Register Transport Module */
    SysLogUpdateTransModule (u1ModuleId);

    return SYSLOG_SUCCESS;
}

/*********************************************************************/
/* Function           : SysLogDeRegisterTransType                    */
/*                                                                   */
/* Description        : De register with Syslog                      */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : None                                         */
/*                                                                   */
/*********************************************************************/

INT4
SysLogDeRegisterTransModule (UINT1 u1ModuleId)
{
    INT4                i4Index = SYSLOG_ZERO;

    i4Index = SysLogFindRegisterEntry (u1ModuleId);

    if (i4Index != SYSLOG_INVALID_REGISTER)
    {
        gaSysLogRegister[i4Index].u1ModuleId = SYSLOG_ZERO;
        gaSysLogRegister[i4Index].u1Flag = SYSLOG_DOWN;
        gaSysLogRegister[i4Index].pTransFuncPtr = NULL;

        return SYSLOG_SUCCESS;
    }

    return SYSLOG_FAILURE;
}

/*********************************************************************/
/* Function           :  SysLogFindRegisterEntry                     */
/*                                                                   */
/* Description        : Finds the entry in the given register        */
/*                                                                   */
/* Input(s)           : register table, keys.                        */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : returns -1 if found otherwise index          */
/*                                                                   */
/*********************************************************************/

INT4
SysLogFindRegisterEntry (UINT1 u1ModuleId)
{
    INT4                i4Index;

    for (i4Index = SYSLOG_ZERO; i4Index < SYSLOG_MAX_REGISTER; i4Index++)
    {
        if (gaSysLogRegister[i4Index].u1ModuleId == u1ModuleId)
        {
            return i4Index;
        }
    }

    return SYSLOG_FAILURE;
}

/*********************************************************************/
/* Function           : SysLogFindFreeRegisterEntry                  */
/*                                                                   */
/* Description        : Finds the free entry in the given register   */
/*                                                                   */
/* Input(s)           : Registration register                        */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : freeindex                                    */
/*                                                                   */
/*********************************************************************/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                  **/
/*********************************************************************/
INT4
SysLogFindFreeRegisterEntry (VOID)
{
    INT4                i4Index;

    for (i4Index = SYSLOG_ZERO; i4Index < SYSLOG_MAX_REGISTER; i4Index++)
    {
        if (gaSysLogRegister[i4Index].u1Flag == SYSLOG_DOWN)
        {
            return i4Index;
        }
    }

    return SYSLOG_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogUpdateTransModule                                */
/*                                                                           */
/* Description      : This function is invoked to initialize the command log */
/*                    data structures. This function should be invoked only  */
/*                    during system startup.                                 */
/*                                                                           */
/* Input Parameters : None                                                   */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  SYSLOG_SUCCESS - On successful initialization.        */
/*                     SYSLOG_MEMALLOC_FAILURE - When initialization fails   */
/*****************************************************************************/

VOID
SysLogUpdateTransModule (UINT1 u1ModuleId)
{

    SysLogRoleUpdateTransModule (u1ModuleId);

    SysLogProfileUpdateTransModule (u1ModuleId);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    :  SysLogRoleUpdateTransModule                           */
/*                                                                           */
/* Description      : This function is invoked to initialize the command log */
/*                    data structures. This function should be invoked only  */
/*                    during system startup.                                 */
/*                                                                           */
/* Input Parameters : None                                                   */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  SYSLOG_SUCCESS - On successful initialization.        */
/*                     SYSLOG_MEMALLOC_FAILURE - When initialization fails   */
/*****************************************************************************/

INT4
SysLogRoleUpdateTransModule (UINT1 u1ModuleId)
{
    INT4                i4Index;
    tSysLogTransMsg     SysLogTransMsg;

    SysLogSetMem (&SysLogTransMsg, SYSLOG_ZERO, sizeof (tSysLogTransMsg));
    for (i4Index = SYSLOG_ZERO; i4Index < SYSLOG_MAX_REGISTER; i4Index++)
    {
        if ((gaSysLogRegister[i4Index].u1ModuleId == u1ModuleId) &&
            (gaSysLogRegister[i4Index].u1Flag == SYSLOG_UP))
        {
            SysLogTransMsg.u.Role.u4SysLogRole = gsSysLogParams.u4SysLogRole;
            gaSysLogRegister[i4Index].pTransFuncPtr (SYSLOG_CURRENT_ROLE_EVENT,
                                                     SysLogTransMsg, NULL);
            return SYSLOG_SUCCESS;
        }
    }

    return SYSLOG_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    :  SysLogRoleUpdateAllTransModule                        */
/*                                                                           */
/* Description      : This function is invoked to initialize the command log */
/*                    data structures. This function should be invoked only  */
/*                    during system startup.                                 */
/*                                                                           */
/* Input Parameters : None                                                   */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  SYSLOG_SUCCESS - On successful initialization.        */
/*                     SYSLOG_MEMALLOC_FAILURE - When initialization fails   */
/*****************************************************************************/

INT4
SysLogRoleUpdateAllTransModule (VOID)
{
    INT4                i4Index;
    tSysLogTransMsg     SysLogTransMsg;

    SysLogSetMem (&SysLogTransMsg, SYSLOG_ZERO, sizeof (tSysLogTransMsg));
    for (i4Index = SYSLOG_ZERO; i4Index < SYSLOG_MAX_REGISTER; i4Index++)
    {
        if (gaSysLogRegister[i4Index].u1Flag == SYSLOG_UP)
        {
            SysLogTransMsg.u.Role.u4SysLogRole = gsSysLogParams.u4SysLogRole;
            gaSysLogRegister[i4Index].pTransFuncPtr (SYSLOG_CURRENT_ROLE_EVENT,
                                                     SysLogTransMsg, NULL);
            return SYSLOG_SUCCESS;
        }
    }

    return SYSLOG_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    :  SysLogProfileUpdateTransModule                        */
/*                                                                           */
/* Description      : This function is invoked to initialize the command log */
/*                    data structures. This function should be invoked only  */
/*                    during system startup.                                 */
/*                                                                           */
/* Input Parameters : None                                                   */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  SYSLOG_SUCCESS - On successful initialization.        */
/*                     SYSLOG_MEMALLOC_FAILURE - When initialization fails   */
/*****************************************************************************/

INT4
SysLogProfileUpdateTransModule (UINT1 u1ModuleId)
{
    INT4                i4Index;
    tSysLogTransMsg     SysLogTransMsg;

    SysLogSetMem (&SysLogTransMsg, SYSLOG_ZERO, sizeof (tSysLogTransMsg));
    for (i4Index = SYSLOG_ZERO; i4Index < SYSLOG_MAX_REGISTER; i4Index++)
    {
        if ((gaSysLogRegister[i4Index].u1ModuleId == u1ModuleId) &&
            (gaSysLogRegister[i4Index].u1Flag == SYSLOG_UP))
        {
            SysLogTransMsg.u.Profile.u4SysLogProfile =
                gsSysLogParams.u4SysLogProfile;
            gaSysLogRegister[i4Index].
                pTransFuncPtr (SYSLOG_CURRENT_PROFILE_EVENT, SysLogTransMsg,
                               NULL);
            return SYSLOG_SUCCESS;
        }
    }

    return SYSLOG_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    :  SysLogProfileUpdateAllTransModule                     */
/*                                                                           */
/* Description      : This function is invoked to initialize the command log */
/*                    data structures. This function should be invoked only  */
/*                    during system startup.                                 */
/*                                                                           */
/* Input Parameters : None                                                   */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  SYSLOG_SUCCESS - On successful initialization.        */
/*                     SYSLOG_MEMALLOC_FAILURE - When initialization fails   */
/*****************************************************************************/

INT4
SysLogProfileUpdateAllTransModule (VOID)
{
    INT4                i4Index;
    tSysLogTransMsg     SysLogTransMsg;

    SysLogSetMem (&SysLogTransMsg, SYSLOG_ZERO, sizeof (tSysLogTransMsg));
    for (i4Index = SYSLOG_ZERO; i4Index < SYSLOG_MAX_REGISTER; i4Index++)
    {
        if (gaSysLogRegister[i4Index].u1Flag == SYSLOG_UP)
        {
            SysLogTransMsg.u.Profile.u4SysLogProfile =
                gsSysLogParams.u4SysLogProfile;
            gaSysLogRegister[i4Index].
                pTransFuncPtr (SYSLOG_CURRENT_PROFILE_EVENT, SysLogTransMsg,
                               NULL);
            return SYSLOG_SUCCESS;
        }
    }

    return SYSLOG_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogMsgUpdateTransModule                             */
/*                                                                           */
/* Description      : This function is invoked to initialize the command log */
/*                    data structures. This function should be invoked only  */
/*                    during system startup.                                 */
/*                                                                           */
/* Input Parameters : None                                                   */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  SYSLOG_SUCCESS - On successful initialization.        */
/*                     SYSLOG_MEMALLOC_FAILURE - When initialization fails   */
/*****************************************************************************/

INT4
SysLogMsgUpdateTransModule (UINT1 u1ModuleId, UINT1 *u1LogMsg)
{
    INT4                i4Index;
    tSysLogTransMsg     SysLogTransMsg;

    for (i4Index = SYSLOG_ZERO; i4Index < SYSLOG_MAX_REGISTER; i4Index++)
    {
        if ((gaSysLogRegister[i4Index].u1ModuleId == u1ModuleId) &&
            (gaSysLogRegister[i4Index].u1Flag == SYSLOG_UP))
        {
            gaSysLogRegister[i4Index].pTransFuncPtr (SYSLOG_TRANS_MSG_EVENT,
                                                     SysLogTransMsg, u1LogMsg);
            return SYSLOG_SUCCESS;
        }
    }

    return SYSLOG_FAILURE;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SysLogSendMailAlert                              */
/*                                                                          */
/*    Description        : This function is used to send an email alert     */
/*                         message to the mail server.                      */
/*                                                                          */
/*    Input(s)           : pLogBuffer - pointer to the buffer to be sent    */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/

INT4
SysLogSendMailAlert (UINT4 u4Priority, UINT1 *pLogBuffer)
{
    UINT1              *pMessage = NULL;

    if (gi4SmtpInit == SMTP_DISABLE)
    {
        return (SMTP_FAILURE);
    }

    if (gsSmtpAlertParams.u4ServerDomainName == SMTP_NOT_SET)
    {
        return (SMTP_FAILURE);
    }

    if (gsSmtpAlertParams.u4MyDomainName == SMTP_NOT_SET)
    {
        return (SMTP_FAILURE);
    }

    pMessage = (UINT1 *) MemAllocMemBlk (gSysLogGlobalInfo.sysQueMsgPoolId);
    if (pMessage == NULL)
    {
        return (SNMP_FAILURE);
    }

    STRNCPY (pMessage, pLogBuffer, STRLEN(pLogBuffer));
    pMessage[STRLEN(pLogBuffer)] = '\0';

    /* enqueue syslog message to SMTP task */
    if (OsixQueSend (gSmtpQueId, (UINT1 *) &pMessage,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (gSysLogGlobalInfo.sysQueMsgPoolId,
                            (UINT1 *) pMessage);
        return (SMTP_FAILURE);
    }

    /* Send a EVENT to SMTP task */
    OsixEvtSend (gSmtpTaskId, SYSLOG_INPUT_Q_EVENT);

    UNUSED_PARAM (u4Priority);
    return (SMTP_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SysLogFromBeepServer                             */
/*                                                                          */
/*    Description        : This function is used to send an email alert     */
/*                         message to the mail server.                      */
/*                                                                          */
/*    Input(s)           : pLogBuffer - pointer to the buffer to be sent    */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/

INT4
SysLogFromBeepServer (UINT1 *pLogBuffer)
{
    UINT1              *pMessage = NULL;

    if (gsSysLogParams.u4SysLogRole != SYSLOG_RELAY_ROLE)
    {
        return SYSLOG_FAILURE;
    }

    pMessage = (UINT1 *) MemAllocMemBlk (gSysLogGlobalInfo.sysQueMsgPoolId);
    if (pMessage == NULL)
    {
        return (SNMP_FAILURE);
    }

    STRNCPY (pMessage, pLogBuffer, STRLEN(pLogBuffer));
    pMessage[STRLEN(pLogBuffer)] = '\0';

    /* enqueue syslog message to SMTP task */
    if (OsixQueSend (gSmtpQueId, (UINT1 *) &pMessage,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (gSysLogGlobalInfo.sysQueMsgPoolId,
                            (UINT1 *) pMessage);
        return (SMTP_FAILURE);
    }

    /* Send a EVENT to SMTP task */
    OsixEvtSend (gSmtpTaskId, SYSLOG_BEEP_MSG_EVENT);

    return (SMTP_SUCCESS);
}
