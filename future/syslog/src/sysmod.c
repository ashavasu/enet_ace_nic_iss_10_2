
/********************************************************************
 * Copyright (C) Future Sotware,2003-04
 *
 * $Id: sysmod.c,v 1.13 2015/06/06 11:13:46 siva Exp $
 *
 * Description: Action routines for Registering each modules and
 *              converting module name to module Id.
 *******************************************************************/

#include "syslginc.h"

extern tSysModInfo  asModInfo[SYSLOG_MAX_MODULES + 1];
tModules            sModules[] = {
    {0, "ARP"}
    ,
    {0, "BGP4"}
    ,
    {0, "BRG"}
    ,
    {0, "FM"}
    ,
    {0, "EMFM"}
    ,
    {0, "GDD"}
    ,
    {0, "CFA"}
    ,
    {0, "DHCP SRV"}
    ,
    {0, "DHCP CLIENT"}
    ,
    {0, "DHCP"}
    ,
    {0, "IKE"}
    ,
    {0, "IPSecv4"}
    ,
    {0, "IPSecv6"}
    ,
    {0, "ISS"}
    ,
    {0, "[LNTN]:"}
    ,
    {0, "MbsmMain:"}
    ,
    {0, "MbsmProto:"}
    ,
    {0, "MbsmTopo:"}
    ,
    {0, "MbsmUtil:"}
    ,
    {0, "MbsmInit:"}
    ,
    {0, "MFWD"}
    ,
    {0, "ICMP"}
    ,
    {0, "IP"}
    ,
    {0, "BOOTP"}
    ,
    {0, "UDP"}
    ,
    {0, "MbsmIp"}
    ,
    {0, "IP6"}
    ,
    {0, "ICMP6"}
    ,
    {0, "UDP6"}
    ,
    {0, "PING6"}
    ,
    {0, "MIPv6"}
    ,
    {0, "NETIP6"}
    ,
    {0, "MbsmIp6"}
    ,
    {0, "MLD"}
    ,
    {0, "RTM6"}
    ,
    {0, "OSPF"}
    ,
    {0, "OAS"}
    ,
    {0, "OSPFV3"}
    ,
    {0, "PNAC"}
    ,
    {0, "[LLDP] "}
    ,
    {0, "POE"}
    ,
    {0, "PPP"}
    ,
    {0, "PPPoE"}
    ,
    {0, "RIP"}
    ,
    {0, "RIP6"}
    ,
    {0, "RM"}
    ,
    {0, "RMON"}
    ,
    {0, "SNP:GBL"}
    ,
    {0, "SNP:GBL-MEM"}
    ,
    {0, "SNP:GBL-MGMT"}
    ,
    {0, "SNTP"}
    ,
    {0, "AST"}
    ,
    {0, "TCP"}
    ,
    {0, "VLAN:"}
    ,
    {0, "GARP:"}
    ,
    {0, "GVRP:"}
    ,
    {0, "GMRP:"}
    ,
    {0, "VRRP_INIT"}
    ,
    {0, "VRRP_PKTP"}
    ,
    {0, "VRRP_SEM"}
    ,
    {0, "VRRP_BUF"}
    ,
    {0, "VRRP_IPIF"}
    ,
    {0, "VRRP_TMR"}
    ,
    {0, "VRRP_OSI"}
    ,
    {0, "VRRP_CONF"}
    ,
    {0, "VRRP_COMP"}
    ,
    {0, "VRRP_ALL"}
    ,
    {0, "WEBNM"}
    ,
    {0, "WLAN"}
    ,
    {0, "BEEP_SRV"}
    ,
    {0, "BEEP_CLT"}
    ,
    {0, "SSH"}
    ,
    {0, "SSL"}
    ,
    {0, "[UTIL] "}
    ,
    {0, "FIPS"}
    ,
    {0, "FWUPGD"}
    ,
    {0, "D6RL"}
    ,
    {0, "IGMP"}
    ,
    {0, "PIM"}
    ,
    {0, "MLD"}
    ,
    {0, "SnpT"}
    ,
    {0, NULL}
};

/*****************************************************************************/
/*                                                                           */
/* Function Name     : SysLogRegisterAllModules                              */
/*                                                                           */
/* Description       : This function is invoked to register all the modules  */
/*                     with the syslog.                                      */
/*                                                                           */
/* Input Parameters  : None                                                  */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : SYSLOG_SUCCESS - On successful initialization.        */
/*                     SYSLOG_REG_FAILURE - When registeration fails         */
/*****************************************************************************/
INT4
SysLogRegisterAllModules ()
{
    INT4                i4Loop = 0;
    /* Registering all the modules with syslog */
    while (sModules[i4Loop].pc1ModName != NULL)
    {
        sModules[i4Loop].u4ModuleId =
            SysLogRegister ((const UINT1 *) sModules[i4Loop].pc1ModName,
                            SYSLOG_CRITICAL_LEVEL);
        i4Loop++;
    }
    return SYSLOG_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name     : SysLogMessage                                         */
/*                                                                           */
/* Description       : This function is will check whether the modules is    */
/*                     registered with syslog. If so appropriate function    */
/*                     will be called to log the message.                    */
/*                                                                           */
/* Input Parameters  : u4Level    - Message Log Level                        */
/*                     pi1ModName - Module Name                              */
/*                     pc1Buf     - Output buffer                            */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : None                                                  */
/*****************************************************************************/
VOID
SysLogMessage (UINT4 u4Level, const INT1 *pi1ModName, CHR1 * pc1Buf)
{
    UINT4               u4ModId = 0;
    INT4                i4Loop = 1;

    /* Sanity check */
    if ((pi1ModName == NULL) || (pc1Buf == NULL))
    {
        return;
    }

    /* Checking whether the module is registered with syslog */
    while (i4Loop <= SYSLOG_MAX_MODULES)
    {
        if (STRCMP (asModInfo[i4Loop].au1Name, pi1ModName) == 0)
        {
            u4ModId = asModInfo[i4Loop].u4ModuleId;
            break;
        }
        i4Loop++;
    }
    /* Calling the syslog function to log the message */
    SysLogMsg (u4Level, u4ModId, pc1Buf);

    return;
}
