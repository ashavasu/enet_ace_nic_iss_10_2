/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: syslgcli.c,v 1.82 2017/12/28 10:40:16 siva Exp $
 *
 * Description: Action routines of syslog module specific commands
 *
 *******************************************************************/
#ifndef __SYSLGCLI_C__
#define __SYSLGCLI_C__

#include "syslginc.h"
#include "syslgcli.h"
#include "sysclipt.h"
#include "fssyslcli.h"
#include "utilipvx.h"
#include "slogtrc.h"

extern INT4         gi4SysLogFd;
extern tSysLogParams gsSysLogParams;
extern tSysModInfo  asModInfo[SYSLOG_MAX_MODULES + 1];
extern tEmailAlertParams gsSmtpAlertParams;
extern UINT4        gu4SysLogInit;
extern tSysLogEntry gaSysLogTbl[MAX_SYSLOG_USERS_LIMIT + 1];
extern tSysLogGlobalInfo gSysLogGlobalInfo;

extern tSNMP_OCTET_STRING_TYPE *allocmem_octetstring PROTO ((INT4));

extern VOID free_octetstring PROTO ((tSNMP_OCTET_STRING_TYPE *));

extern tSNMP_OCTET_STRING_TYPE *SNMP_AGT_FormOctetString
PROTO ((UINT1 *, INT4));

extern VOID
        SNMP_AGT_FreeOctetString (tSNMP_OCTET_STRING_TYPE * pOctetString);
#ifdef BGP_WANTED
extern INT4         BgpSetTraceValue (INT4 i4TrapLevel, UINT1 u1LoggingCmd);
#endif
#ifdef VRRP_WANTED
extern INT4         VrrpSetTraceValue (INT4 i4TrapLevel, UINT1 u1LoggingCmd);
#endif
#ifdef RIP_WANTED
extern INT4         RipSetTraceValue (INT4 i4TrapLevel, UINT1 u1LoggingCmd);
#endif
#ifdef OSPF_WANTED
extern INT4         OspfSetTraceValue (INT4 i4TrapLevel, UINT1 u1LoggingCmd);
#endif
#ifdef OSPF3_WANTED
extern INT4         Ospf3SetTraceValue (INT4 i4TrapLevel, UINT1 u1LoggingCmd);
#endif
#ifdef NPAPI_WANTED
extern UINT1        IsssysNpSetTrace (UINT1 u1TraceModule, UINT1 u1TraceLevel);
#endif
extern tOsixSemId   gsSemId;
extern UINT4        gu4AuditLogSize;
extern UINT1        gu1LoggingCmd;

/**************************************************************************/
/*  Function Name   : cli_process_syslg_cmd                               */
/*  Description     : Protocol CLI message handler function               */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    u4Command - Command identifier                      */
/*                    ... -Variable command argument list                 */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
cli_process_syslg_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[SYSLG_CLI_MAX_ARGS];
    INT1                i1argno = 0;
    UINT4               u4ErrCode;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT4               u4CmdType;
    UINT1              *pu1Inst = NULL;

    INT4                i4Args;
    UINT4               u4Args;

    UINT4               u4IpFamily;
    UINT4               u4TransType;
    UINT4               u4RelayTransType;
    UINT4               u4Port;
    UINT1               au1FwdServAddr[DNS_MAX_QUERY_LEN];
    UINT4               u4IPAddr = 0;
    tDNSResolvedIpInfo  ResolvedIpInfo;

    MEMSET (&ResolvedIpInfo, SYSLOG_ZERO, sizeof (tDNSResolvedIpInfo));
    MEMSET (au1FwdServAddr, SYSLOG_ZERO, DNS_MAX_QUERY_LEN);

    va_start (ap, u4Command);

    /* Third arguement is always interface name/index */
    pu1Inst = va_arg (ap, UINT1 *);

    /* This third argument in the variable argument list should be always NULL
     * for commands that doesn't uses Interface argument and hence the following 
     * check is used to verify that this module doesn't uses the 3rd argument
     */
    if (pu1Inst != NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Passing interface argument which is unused\r\n");
        va_end (ap);
        return CLI_FAILURE;
    }

    /* Walk through the rest of the arguements and store in args array. 
     * Store SYSLG_CLI_MAX_ARGS arguements at the max. 
     */

    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT1 *);
        if (i1argno == SYSLG_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);
    switch (u4Command)
    {
        case CLI_SYSLG_SNMP_TRAP:
            i4Args = CLI_PTR_TO_I4 (args[0]);
            i4RetStatus = SysLogSnmpSetTrap (CliHandle, i4Args);
            break;

        case CLI_SYSLG_SHOW_LOGGING:
            /* No addition command arguments, so ignore the args[] array */
            /* Action routine shuld be modified later to use nmhGet routines instead of
             * accessing the global structure directly. 
             */
            i4RetStatus = SysLogShowLogs (CliHandle);
            break;
        case CLI_SYSLG_SHOW_EMAIL_ALERT:
            /* No addition command arguments, so ignore the args[] array */
            i4RetStatus = SysLogShowEMailAlerts (CliHandle);
            break;

        case CLI_SYSLG_SET_LOGGING:
            /* args[0] -  CmdType for Logging command.
             * args[1] -  Log buffer size/ severity level,
             *            if cmd type is LOGGING_BUFFER_SIZE/LOGGING_SEVERITY
             * args[2] -  Facilility value/Severity type,
             *            if cmd type is LOGGING_FACILITY/LOGGING_SEVERITY
             */
            gu1LoggingCmd = 1;
            u4CmdType = CLI_PTR_TO_U4 (args[0]);
            if (u4CmdType == LOGGING_BUFFER_SIZE)
            {
                MEMCPY (&i4Args, args[1], sizeof (INT4));
                i4RetStatus = SysLogCfgMaxModLogBuff (CliHandle, i4Args);
            }
            else if (u4CmdType == LOGGING_CONSOLE)
            {
                i4RetStatus = SysLogSetConsoleLog (CliHandle, SYSLOG_ENABLE);
            }
            else if (u4CmdType == LOGGING_FACILITY)
            {
                /* 
                 * args[2] -  Facility value
                 */
                i4RetStatus = SysLogSetFacility (CliHandle,
                                                 CLI_PTR_TO_I4 (args[2]));
            }
            else if (u4CmdType == LOGGING_SEVERITY)
            {
                CliPrintf (CliHandle,
                           "!! Traces for available modules are enabled now. if new module is enabled later, need to enable trace manually in that module\r\n");

                /* 
                 * args[1] -  Severity level value
                 * args[2] -  Severity type
                 */
                if (args[1] != NULL)
                {
                    MEMCPY (&i4Args, args[1], sizeof (INT4));
                    i4RetStatus = SysLogCfgModLogLevel (CliHandle, i4Args);
                }
                else
                {
                    i4RetStatus =
                        SysLogCfgModLogLevel (CliHandle,
                                              CLI_PTR_TO_I4 (args[2]));
                }

            }
            else if (u4CmdType == SYSLOG_ENABLE)
            {
                i4RetStatus = SysLogSetLogging (CliHandle, SYSLOG_ENABLE);
            }

            break;

        case CLI_SYSLG_NO_LOGGING:
            /* args[0] -  CmdType for No Logging command.
             */
            gu1LoggingCmd = 0;
            u4CmdType = CLI_PTR_TO_U4 (args[0]);
            if (u4CmdType == LOGGING_BUFFER_SIZE)
            {
                i4RetStatus =
                    SysLogCfgMaxModLogBuff (CliHandle,
                                            (INT4) DEF_SYSLOG_MAX_BUFF);
            }
            else if (u4CmdType == LOGGING_CONSOLE)
            {
                i4RetStatus = SysLogSetConsoleLog (CliHandle, SYSLOG_DISABLE);
            }
            else if (u4CmdType == LOGGING_FACILITY)
            {
                i4RetStatus =
                    SysLogSetFacility (CliHandle, DEF_SYSLOG_FACILITY);
            }
            else if (u4CmdType == LOGGING_SEVERITY)
            {
                i4RetStatus =
                    SysLogCfgModLogLevel (CliHandle, SYSLOG_CRITICAL_LEVEL);
            }
            else if (u4CmdType == SYSLOG_DISABLE)
            {
                i4RetStatus = SysLogSetLogging (CliHandle, SYSLOG_DISABLE);
            }

            break;
        case CLI_SYSLG_SET_SNDR_MAIL:
            /* args[0] - 
             * In Set    command  - Mail id string 
             */
            i4RetStatus = SysLogSmtpSetMyDomainName (CliHandle, args[0]);
            break;

        case CLI_SYSLG_NO_SNDR_MAIL:
            /* No addition command arguments, so ignore the args[] array */
            i4RetStatus = SysLogSmtpSetMyDomainName (CliHandle, NULL);
            break;
        case CLI_SYSLG_SET_CMDBUFF:
            /* args[0] - User Name
             * args[1] - No. of buffers
             */
            MEMCPY (&u4Args, args[1], sizeof (INT4));
            i4RetStatus = SysLogCfgMaxUsrLogBuff (CliHandle, args[0], u4Args);

            break;
        case CLI_SYSLG_CLEAR_LOGS:
            /* No addition command arguments, so ignore the args[] array */
            i4RetStatus = SysLogSetClearLogs (CliHandle, SYSLOG_TRUE);
            break;
        case CLI_SYSLG_ROLE:
            i4RetStatus = SysLogSetRole (CliHandle, SYSLOG_RELAY_ROLE);
            break;
        case CLI_NO_SYSLG_ROLE:

            i4RetStatus = SysLogSetRole (CliHandle, SYSLOG_DEVICE_ROLE);
            break;
        case CLI_SHW_SYSLG_ROLE:
            i4RetStatus = SysLogShowRole (CliHandle);
            break;

        case CLI_SYSLG_MAIL:
            i4RetStatus = SysLogSetMail (CliHandle, SYSLOG_ENABLE);
            break;
        case CLI_NO_SYSLG_MAIL:
            i4RetStatus = SysLogSetMail (CliHandle, SYSLOG_DISABLE);
            break;
        case CLI_SHW_SYSLG_MAIL:
            i4RetStatus = SysLogShowMail (CliHandle);
            break;
        case CLI_SYSLG_LOCALSTORAGE:
            i4RetStatus = SysLogSetLocalStorage (CliHandle, SYSLOG_ENABLE);
            break;
        case CLI_NO_SYSLG_LOCALSTORAGE:
            i4RetStatus = SysLogSetLocalStorage (CliHandle, SYSLOG_DISABLE);
            break;
        case CLI_SHW_SYSLG_LOCALSTORAGE:
            i4RetStatus = SysLogShowLocalStorage (CliHandle);
            break;
        case CLI_SYSLG_LOGGING_FILE:
            MEMCPY (&i4Args, args[0], sizeof (INT4));
            i4RetStatus = SysLogSetFileTable (CliHandle, i4Args, args[1]);
            break;
        case CLI_SYSLG_LOGGING_FILESIZE:
            MEMCPY (&i4Args, args[0], sizeof (INT4));
            i4RetStatus = SysLogSetFileSize (CliHandle, i4Args);
            break;
        case CLI_SHW_SYSLG_LOGGINGFILE:
            i4RetStatus = SysLogShowFileTable (CliHandle);
            break;
        case CLI_SYSLG_NO_LOGGING_FILE:
            MEMCPY (&u4Args, args[0], sizeof (INT4));
            i4RetStatus = SysLogDelFileTableEntry (CliHandle, u4Args, args[1]);
            break;

        case CLI_SYSLG_PORT:
            MEMCPY (&i4Args, args[0], sizeof (INT4));
            i4RetStatus = SysLogSetSysLogPort (CliHandle, i4Args);
            break;
        case CLI_NO_SYSLG_PORT:
            i4RetStatus = SysLogSetSysLogPort (CliHandle, BSD_SYSLOG_PORT);
            break;
        case CLI_SHOW_SYSLG_PORT:
            i4RetStatus = SysLogShowSysLogPort (CliHandle);
            break;
        case CLI_SYSLG_RELAY_TRANS_TYPE:
            u4RelayTransType = CLI_PTR_TO_U4 (args[0]);
            i4RetStatus = SysLogSetRelayTransType (CliHandle, u4RelayTransType);
            break;
        case CLI_SHOW_SYSLG_REL_TRANSTYPE:
            i4RetStatus = SysLogShowRelayTransType (CliHandle);
            break;
        case CLI_SYSLG_PROFILE:
            i4RetStatus = SysLogSetSysLogProfile (CliHandle, (CHR1 *) args[0]);
            break;
        case CLI_NO_SYSLG_PROFILE:
            i4RetStatus =
                SysLogSetSysLogProfile (CliHandle, RSYSLOG_DEFAULT_PROFILE);
            break;
        case CLI_SHOW_SYSLG_PROFILE:
            i4RetStatus = SysLogShowSysLogProfile (CliHandle);
            break;
        case CLI_SYSLG_LOGGING_SERVER:
            MEMCPY (&u4Args, args[0], sizeof (INT4));
            u4IpFamily = CLI_PTR_TO_U4 (args[1]);
            u4TransType = CLI_PTR_TO_U4 (args[6]);
            u4Port = CLI_PTR_TO_U4 (args[5]);
            if (u4IpFamily == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (au1FwdServAddr, (UINT1 *) args[2], IPVX_IPV4_ADDR_LEN);
            }
            else if (u4IpFamily == IPVX_ADDR_FMLY_IPV6)
            {
                /* get the string in args[3], remove ":" or "." from the 
                   addess string and copy it to the variable below */
                if (INET_ATON6 (args[3], au1FwdServAddr) == SYSLOG_ZERO)
                {
                    CLI_SET_ERR (CLI_SYSLG_INVALID_IP);
                    break;
                }
                SYSLOG_INET_NTOHL (au1FwdServAddr);
            }
            else
            {
                u4IpFamily = IPVX_DNS_FAMILY;
                STRNCPY (au1FwdServAddr, (UINT1 *) args[4],
                         MEM_MAX_BYTES (STRLEN (args[4]),
                                        DNS_MAX_QUERY_LEN - 1));
                au1FwdServAddr[STRLEN (args[4])] = '\0';
                *au1FwdServAddr = UtilStrToLower ((UINT1 *) au1FwdServAddr);
            }
            MEMCPY (&u4IPAddr, au1FwdServAddr, sizeof (UINT4));

            if ((u4IpFamily == IPVX_ADDR_FMLY_IPV4)
                && (SyslogUtilValidateIpAddress (u4IPAddr) != TRUE))
            {
                CliPrintf (CliHandle, "%% Invalid IP Address\r\n");
                return CLI_FAILURE;
            }
            i4RetStatus = SysLogSetFwdTable (CliHandle, u4Args, u4IpFamily,
                                             au1FwdServAddr, u4Port,
                                             u4TransType);
            break;
        case CLI_SHW_SYSLG_LOGGINGSERVER:
            i4RetStatus = SysLogShowFwdTable (CliHandle);
            break;
        case CLI_SYSLG_NO_LOGGING_SERVER:
            MEMCPY (&u4Args, args[0], sizeof (INT4));
            u4IpFamily = CLI_PTR_TO_U4 (args[1]);
            if (u4IpFamily == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (au1FwdServAddr, (UINT1 *) args[2], IPVX_IPV4_ADDR_LEN);
            }
            else if (u4IpFamily == IPVX_ADDR_FMLY_IPV6)
            {
                if (INET_ATON6 (args[3], au1FwdServAddr) == SYSLOG_ZERO)
                {
                    CLI_SET_ERR (CLI_SYSLG_INVALID_IP);
                    break;
                }

                SYSLOG_INET_NTOHL (au1FwdServAddr);
            }
            else
            {
                u4IpFamily = IPVX_DNS_FAMILY;
                STRNCPY (au1FwdServAddr, (UINT1 *) args[4],
                         MEM_MAX_BYTES (STRLEN (args[4]),
                                        DNS_MAX_QUERY_LEN - 1));
                au1FwdServAddr[STRLEN (args[4])] = '\0';
                *au1FwdServAddr = UtilStrToLower ((UINT1 *) au1FwdServAddr);
            }
            i4RetStatus = SysLogDelFwdTable (CliHandle, u4Args,
                                             u4IpFamily, au1FwdServAddr);
            break;

        case CLI_SYSLG_MAIL_SERVER:
            MEMCPY (&u4Args, args[0], sizeof (INT4));
            u4IpFamily = CLI_PTR_TO_U4 (args[1]);
            if (u4IpFamily == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (au1FwdServAddr, (UINT1 *) args[2], IPVX_IPV4_ADDR_LEN);
            }
            else if (u4IpFamily == IPVX_ADDR_FMLY_IPV6)
            {
                /* get the string in args[3], remove ":" or "." from the
                   addess string and copy it to the variable below */
                if (INET_ATON6 (args[3], au1FwdServAddr) == SYSLOG_ZERO)
                {
                    /*CLI_SET_ERR (CLI_SNOOP_INVALID_ADDRESS_ERR); */
                    break;
                }

                SYSLOG_INET_NTOHL (au1FwdServAddr);
            }
            else
            {
                u4IpFamily = IPVX_DNS_FAMILY;
                STRNCPY (au1FwdServAddr, (UINT1 *) args[4],
                         MEM_MAX_BYTES (STRLEN (args[4]),
                                        DNS_MAX_QUERY_LEN - 1));
                au1FwdServAddr[STRLEN (args[4])] = '\0';
            }
            i4RetStatus =
                SysLogSetMailTable (CliHandle, u4Args,
                                    u4IpFamily, au1FwdServAddr,
                                    args[5], args[6], args[7]);
            break;
        case CLI_SHW_SYSLG_MAILSERVER:
            i4RetStatus = SysLogShowMailTable (CliHandle);
            break;
        case CLI_SYSLG_NO_MAILSERVER:
            MEMCPY (&u4Args, args[0], sizeof (INT4));
            u4IpFamily = CLI_PTR_TO_U4 (args[1]);

            if (u4IpFamily == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (au1FwdServAddr, (UINT1 *) args[2], IPVX_IPV4_ADDR_LEN);
            }
            else if (u4IpFamily == IPVX_ADDR_FMLY_IPV6)
            {
                if (INET_ATON6 (args[3], au1FwdServAddr) == 0)
                {
                    /*CLI_SET_ERR (CLI_SNOOP_INVALID_ADDRESS_ERR); */
                    break;
                }

                SYSLOG_INET_NTOHL (au1FwdServAddr);
            }
            else
            {
                /* Currently resolving host name is supported 
                 * only for ipv4 address */
                u4IpFamily = IPVX_DNS_FAMILY;
                STRNCPY (au1FwdServAddr, (UINT1 *) args[4],
                         MEM_MAX_BYTES (STRLEN (args[4]),
                                        DNS_MAX_QUERY_LEN - 1));
                au1FwdServAddr[STRLEN (args[4])] = '\0';
            }
            i4RetStatus =
                SysLogDelMailTable (CliHandle, u4Args,
                                    u4IpFamily, au1FwdServAddr);
            break;

        case CLI_SYSLG_SMTP_AUTH:
            i4Args = CLI_PTR_TO_I4 (args[0]);
            i4RetStatus = SysLogSmtpSetAuthentication (CliHandle, i4Args);
            break;

        case CLI_SHOW_SYSLG_INFORMATION:
            i4RetStatus = SysLogShowInformation (CliHandle);
            break;
        case CLI_SYSLG_FILENAME_ONE:
            i4RetStatus = SysLogSetFileNameOne (CliHandle, args[0]);
            break;
        case CLI_SYSLG_FILENAME_TWO:
            i4RetStatus = SysLogSetFileNameTwo (CliHandle, args[0]);
            break;
        case CLI_SYSLG_FILENAME_THREE:
            i4RetStatus = SysLogSetFileNameThree (CliHandle, args[0]);
            break;
        case CLI_SHOW_SYSLG_FILE_NAME:
            i4RetStatus = SysLogShowFileName (CliHandle);
            break;
        default:
            CliPrintf (CliHandle, "\r\n%% Invalid Command\r\n");
            i4RetStatus = CLI_FAILURE;
            break;

    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_SYSLG_MAX_ERR))
        {
            CliPrintf (CliHandle, "%s", SysLogCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    return i4RetStatus;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogSetFacility                                      */
/*                                                                           */
/* Description      : This function is invoked to set syslog facility value  */
/*                                                                           */
/* Input Parameters : CliHandle   - CliContext ID                            */
/*                    i4Facility  - Syslog facility value to set             */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SysLogSetFacility (tCliHandle CliHandle, INT4 i4Facility)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsSyslogFacility (&u4ErrorCode, i4Facility) == SNMP_SUCCESS)
    {
        if (nmhSetFsSyslogFacility (i4Facility) == SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return (CLI_FAILURE);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogSetConsoleLog                                    */
/*                                                                           */
/* Description      : This function is invoked to set console logs           */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID                              */
/*                    i4LogStatus  - SYSLOG_ENABLE/SYSLOG_DISABLE            */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SysLogSetConsoleLog (tCliHandle CliHandle, INT4 i4LogStatus)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsSyslogConsoleLog (&u4ErrorCode, i4LogStatus) == SNMP_SUCCESS)
    {
        if (nmhSetFsSyslogConsoleLog (i4LogStatus) == SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return (CLI_FAILURE);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogShowEMailAlerts                                  */
/*                                                                           */
/* Description      : This function is invoked to display syslog's email     */
/*                    alert informations.                                    */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID                              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SysLogShowEMailAlerts (tCliHandle CliHandle)
{
    UINT1               au1MailId[SMTP_DOMAIN_NAME_LEN];
    tSNMP_OCTET_STRING_TYPE FsSyslogSmtpMailId;

    FsSyslogSmtpMailId.pu1_OctetList = au1MailId;

    nmhGetFsSyslogSmtpSenderMailId (&FsSyslogSmtpMailId);
    if (FsSyslogSmtpMailId.i4_Length)
    {
        CliPrintf (CliHandle, "\r\nSender email-id   : %s\r\n",
                   FsSyslogSmtpMailId.pu1_OctetList);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogShowLogs                                         */
/*                                                                           */
/* Description      : This function is invoked to display SysLog information */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID                              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SysLogShowLogs (tCliHandle CliHandle)
{
    UINT4               u4Count = 0;
    UINT4               u4SysLogStatus;
    UINT4               u4TimeStamp;
    UINT4               u4LogConsole;
    INT4                i4TrapCfgModuleId;
    INT4                i4NextTrapCfgModuleId;
    INT4                i4TrapLevel = -1;
    INT4                i4Facility;

    CliPrintf (CliHandle, "\r\nSystem Log Information\r\n");
    CliPrintf (CliHandle, "----------------------\r\n");

    /* If Protocol data structure Lock is available then UnLock the data 
       structure before calling CliPrintf and Lock the data structure after 
       CliPrintf. This is to avoid the protocol getting Locked when CLI 
       waits for input keys 'q' or 'enter' in paged output display whenever 
       the ouput is more than one page. This is needed mostly in case of 
       'show' commands where the output display spans more than one page. */

    nmhGetFsSyslogLogging ((INT4 *) &u4SysLogStatus);
    if (u4SysLogStatus == SYSLOG_ENABLE)
    {
        CliPrintf (CliHandle, "Syslog logging   : enabled");
        SyslogGetSyslogCount (&u4Count);
    }
    else
    {
        u4Count = 0;
        CliPrintf (CliHandle, "Syslog logging   : disabled");
    }
    CliPrintf (CliHandle, "(Number of messages %d)\r\n", u4Count);

    nmhGetFsSyslogConsoleLog ((INT4 *) &u4LogConsole);
    if (u4LogConsole == SYSLOG_ENABLE)
    {
        CliPrintf (CliHandle, "Console logging  : enabled");
        SyslogGetConsolelogCount (&u4Count);
    }
    else
    {
        u4Count = 0;
        CliPrintf (CliHandle, "Console logging  : disabled");
    }
    CliPrintf (CliHandle, "(Number of messages %d)\r\n", u4Count);

    nmhGetFsSyslogTimeStamp ((INT4 *) &u4TimeStamp);
    if (u4TimeStamp == SYSLOG_ENABLE)
    {
        CliPrintf (CliHandle, "TimeStamp option : enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "TimeStamp option : disabled\r\n");
    }

    CliPrintf (CliHandle, "Severity logging     : ");
    if (nmhGetFirstIndexFsSyslogConfigTable (&i4TrapCfgModuleId) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "disabled\r\n");
    }
    else
    {
        i4NextTrapCfgModuleId = i4TrapCfgModuleId;
        do
        {
            i4TrapCfgModuleId = i4NextTrapCfgModuleId;
            nmhGetFsSyslogConfigLogLevel (i4TrapCfgModuleId, &i4TrapLevel);
            if (STRCASECMP (asModInfo[i4TrapCfgModuleId].au1Name, "CLI") == 0)
            {
                switch (i4TrapLevel)
                {
                    case SYSLOG_EMERG_LEVEL:
                        CliPrintf (CliHandle, "Emergencies\r\n");
                        break;
                    case SYSLOG_ALERT_LEVEL:
                        CliPrintf (CliHandle, "Alerts\r\n");
                        break;
                    case SYSLOG_CRITICAL_LEVEL:
                        CliPrintf (CliHandle, "Critical\r\n");
                        break;
                    case SYSLOG_ERROR_LEVEL:
                        CliPrintf (CliHandle, "Errors\r\n");
                        break;
                    case SYSLOG_WARN_LEVEL:
                        CliPrintf (CliHandle, "Warnings\r\n");
                        break;
                    case SYSLOG_NOTICE_LEVEL:
                        CliPrintf (CliHandle, "Notification\r\n");
                        break;
                    case SYSLOG_INFO_LEVEL:
                        CliPrintf (CliHandle, "Informational\r\n");
                        break;
                    case SYSLOG_DEBUG_LEVEL:
                        CliPrintf (CliHandle, "Debugging\r\n");
                        break;
                    default:
                        CliPrintf (CliHandle, "Disabled\r\n");
                        break;
                }
            }
        }
        while (nmhGetNextIndexFsSyslogConfigTable
               (i4TrapCfgModuleId, &i4NextTrapCfgModuleId) == SNMP_SUCCESS);
    }

    CliPrintf (CliHandle, "Facility   \t : ");

    nmhGetFsSyslogFacility (&i4Facility);
    switch (i4Facility)
    {
        case SYSLG_FACILITY_LOCAL0:
            CliPrintf (CliHandle, "Default (local0)\r\n");
            break;
        case SYSLG_FACILITY_LOCAL1:
            CliPrintf (CliHandle, "local1\r\n");
            break;
        case SYSLG_FACILITY_LOCAL2:
            CliPrintf (CliHandle, "local2\r\n");
            break;
        case SYSLG_FACILITY_LOCAL3:
            CliPrintf (CliHandle, "local3\r\n");
            break;
        case SYSLG_FACILITY_LOCAL4:
            CliPrintf (CliHandle, "local4\r\n");
            break;
        case SYSLG_FACILITY_LOCAL5:
            CliPrintf (CliHandle, "local5\r\n");
            break;
        case SYSLG_FACILITY_LOCAL6:
            CliPrintf (CliHandle, "local6\r\n");
            break;
        case SYSLG_FACILITY_LOCAL7:
            CliPrintf (CliHandle, "local7\r\n");
            break;
    }

    nmhGetFsSyslogSysBuffers ((INT4 *) &u4Count);
    CliPrintf (CliHandle, "Buffered size    : %d Entries\r\n\r\n", u4Count);

    SyslogGetLogBufferCount (&u4Count);
    CliPrintf (CliHandle, "LogBuffer(%d Entries, %d bytes)\r\n",
               u4Count, u4Count * sizeof (tSysLogBuff));

    if (u4LogConsole == SYSLOG_ENABLE)
    {
        SysLogShowLogBuffer (CliHandle);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogSetFileNameOne                                   */
/*                                                                           */
/* Description      : This function is invoked to set    syslog file names   */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID and file name                */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
INT4
SysLogSetFileNameOne (tCliHandle CliHandle, UINT1 *pu1FileOne)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *FileOne = NULL;
    if (pu1FileOne != NULL)
    {
        FileOne = SNMP_AGT_FormOctetString (pu1FileOne, STRLEN (pu1FileOne));

        if (FileOne != NULL)
        {
            if (nmhTestv2FsSyslogFileNameOne (&u4ErrorCode, FileOne) ==
                SNMP_SUCCESS)
            {
                nmhSetFsSyslogFileNameOne (FileOne);
                SNMP_AGT_FreeOctetString (FileOne);
                return CLI_SUCCESS;
            }
        }
        SNMP_AGT_FreeOctetString (FileOne);
    }
    else if (pu1FileOne == NULL)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogSetFileNameTwo                                   */
/*                                                                           */
/* Description      : This function is invoked to set    syslog file names   */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID and file name                */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
INT4
SysLogSetFileNameTwo (tCliHandle CliHandle, UINT1 *pu1FileTwo)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *FileTwo = NULL;
    if (pu1FileTwo != NULL)
    {
        FileTwo = SNMP_AGT_FormOctetString (pu1FileTwo, STRLEN (pu1FileTwo));

        if (FileTwo != NULL)
        {
            if (nmhTestv2FsSyslogFileNameTwo (&u4ErrorCode, FileTwo) ==
                SNMP_SUCCESS)
            {
                nmhSetFsSyslogFileNameTwo (FileTwo);
                SNMP_AGT_FreeOctetString (FileTwo);
                return CLI_SUCCESS;
            }
        }
        SNMP_AGT_FreeOctetString (FileTwo);
    }
    else if (pu1FileTwo == NULL)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogSetFileNameThree                                 */
/*                                                                           */
/* Description      : This function is invoked to set    syslog file names   */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID and file name                */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
INT4
SysLogSetFileNameThree (tCliHandle CliHandle, UINT1 *pu1FileThree)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *FileThree = NULL;
    if (pu1FileThree != NULL)
    {
        FileThree =
            SNMP_AGT_FormOctetString (pu1FileThree, STRLEN (pu1FileThree));

        if (FileThree != NULL)
        {
            if (nmhTestv2FsSyslogFileNameThree (&u4ErrorCode, FileThree) ==
                SNMP_SUCCESS)
            {
                nmhSetFsSyslogFileNameThree (FileThree);
                SNMP_AGT_FreeOctetString (FileThree);
                return CLI_SUCCESS;
            }
        }
        SNMP_AGT_FreeOctetString (FileThree);
    }
    else if (pu1FileThree == NULL)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogShowFileName                                     */
/*                                                                           */
/* Description      : This function is invoked to display syslog file names   */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID                              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
INT4
SysLogShowFileName (tCliHandle CliHandle)
{

    tSNMP_OCTET_STRING_TYPE *pFileOne = NULL;
    tSNMP_OCTET_STRING_TYPE *pFileTwo = NULL;
    tSNMP_OCTET_STRING_TYPE *pFileThree = NULL;
    CliPrintf (CliHandle, "\r\nSyslog File Name\r\n");
    CliPrintf (CliHandle, "----------------------\r\n");
    /*Allocating memory */
    pFileOne =
        (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (SYS_MAX_FILE_NAME_LEN);
    if (pFileOne == NULL)
    {
        return CLI_FAILURE;
    }
    pFileTwo =
        (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (SYS_MAX_FILE_NAME_LEN);
    if (pFileTwo == NULL)
    {
        free_octetstring (pFileOne);
        return CLI_FAILURE;
    }
    pFileThree =
        (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (SYS_MAX_FILE_NAME_LEN);
    if (pFileThree == NULL)
    {
        free_octetstring (pFileTwo);
        free_octetstring (pFileOne);
        return CLI_FAILURE;
    }

    MEMSET (pFileOne->pu1_OctetList, SYSLOG_ZERO, SYS_MAX_FILE_NAME_LEN);
    nmhGetFsSyslogFileNameOne (pFileOne);
    CliPrintf (CliHandle, "Syslog File-One :%s \r\n\r\n",
               pFileOne->pu1_OctetList);

    MEMSET (pFileTwo->pu1_OctetList, SYSLOG_ZERO, SYS_MAX_FILE_NAME_LEN);
    nmhGetFsSyslogFileNameTwo (pFileTwo);
    CliPrintf (CliHandle, "Syslog File-Two :%s \r\n\r\n",
               pFileTwo->pu1_OctetList);

    MEMSET (pFileThree->pu1_OctetList, SYSLOG_ZERO, SYS_MAX_FILE_NAME_LEN);
    nmhGetFsSyslogFileNameThree (pFileThree);
    CliPrintf (CliHandle, "Syslog File-Three :%s \r\n\r\n",
               pFileThree->pu1_OctetList);

    free_octetstring (pFileOne);
    free_octetstring (pFileTwo);
    free_octetstring (pFileThree);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogShowInformation                                  */
/*                                                                           */
/* Description      : This function is invoked to display SysLog information */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID                              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SysLogShowInformation (tCliHandle CliHandle)
{
    INT4                i4LocalStorage = SYSLOG_ZERO;
    INT4                i4MailOption = SYSLOG_ZERO;
    INT4                i4SysLogPort = SYSLOG_ZERO;
    INT4                i4Role = SYSLOG_ZERO;
    INT4                i4Authenticate = SYSLOG_ZERO;

    CliPrintf (CliHandle, "\r\nSystem Log Information\r\n");
    CliPrintf (CliHandle, "----------------------\r\n");

    /* If Protocol data structure Lock is available then UnLock the data
       structure before calling CliPrintf and Lock the data structure after
       CliPrintf. This is to avoid the protocol getting Locked when CLI
       waits for input keys 'q' or 'enter' in paged output display whenever
       the ouput is more than one page. This is needed mostly in case of
       'show' commands where the output display spans more than one page. */

    SYSLOG_DBG (SYSLOG_DBG_ENTRY, "ShowLogs\n");
    nmhGetFsSyslogLogFile (&i4LocalStorage);
    if (i4LocalStorage)
    {
        if (SYSLOG_ENABLE == i4LocalStorage)
        {
            CliPrintf (CliHandle, "Syslog Localstorage   : Enabled\r\n\r\n");
        }
        else if (SYSLOG_DISABLE == i4LocalStorage)
        {
            CliPrintf (CliHandle, "Syslog Localstorage   : Disabled\r\n\r\n");
        }
    }

    nmhGetFsSyslogMail (&i4MailOption);
    if (i4MailOption)
    {
        if (SYSLOG_ENABLE == i4MailOption)
        {
            CliPrintf (CliHandle, "Syslog Mail Option    : Enabled\r\n\r\n");
        }
        else if (SYSLOG_DISABLE == i4MailOption)
        {
            CliPrintf (CliHandle, "Syslog Mail Option    : Disabled\r\n\r\n");
        }
    }

    nmhGetFsSyslogRelayPort (&i4SysLogPort);
    if (i4SysLogPort <= BSD_SYSLOG_PORT_MAX)
    {
        CliPrintf (CliHandle, "Syslog Port    : %d\r\n\r\n", i4SysLogPort);
    }

    nmhGetFsSyslogRole (&i4Role);
    if (i4Role)
    {
        if (SYSLOG_DEVICE_ROLE == i4Role)
        {
            CliPrintf (CliHandle, "Syslog Role    : Device\r\n\r\n");
        }
        else if (SYSLOG_RELAY_ROLE == i4Role)
        {
            CliPrintf (CliHandle, "Syslog Role    : Relay\r\n\r\n");
        }
    }
    nmhGetFsSyslogSmtpAuthMethod (&i4Authenticate);
    switch (i4Authenticate)
    {
        case SMTP_NO_AUTHENTICATE:
            CliPrintf (CliHandle, "Smtp Authentication   : None\r\n");
            break;
        case SMTP_AUTH_LOGIN:
            CliPrintf (CliHandle, "Smtp Authentication   : Auth Login\r\n");
            break;
        case SMTP_AUTH_PLAIN:
            CliPrintf (CliHandle, "Smtp Authentication   : Auth Plain\r\n");
            break;
        case SMTP_AUTH_CRAM_MD5:
            CliPrintf (CliHandle, "Smtp Authentication   : Cram-MD5\r\n");
            break;
        case SMTP_AUTH_DIGEST_MD5:
            CliPrintf (CliHandle, "Smtp Authentication   : Digest-MD5\r\n");
            break;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SyslgShowRunningConfig                                 */
/*                                                                           */
/* Description      : This function will dispaly the current running         */
/*                    configuration of  Syslog.                              */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID                              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
INT4
SyslgShowRunningConfig (tCliHandle CliHandle)
{
    UINT4               u4Count = SYSLOG_ZERO;
    UINT4               u4SysLogStatus = SYSLOG_ZERO;
    UINT4               u4TimeStamp = SYSLOG_ZERO;
    UINT4               u4SyslogServerUpDownTrap = SYSLOG_ZERO;
    UINT4               u4LocalStorage = SYSLOG_ZERO;
    UINT4               u4MailOption = SYSLOG_ZERO;
    UINT4               u4SysLogRole = SYSLOG_ZERO;
    INT4                i4RelayTransType = SYSLOG_ZERO;
    UINT4               u4TempIpv4 = SYSLOG_ZERO;
    INT4                i4SysLogPort = SYSLOG_ZERO;
    INT4                i4SysLogProfile = SYSLOG_ZERO;
    INT4                i4Priority = SYSLOG_ZERO;
    INT4                i4AddrType = SYSLOG_ZERO;
    INT4                i4Port = SYSLOG_ZERO;
    INT4                i4TransType = SYSLOG_ZERO;
    INT4                i4RowStatus = SYSLOG_ZERO;
    CHR1                au1Ip6Addr[IPVX_MAX_INET_ADDR_LEN];
    UINT4               u4LogConsole = SYSLOG_ZERO;
    UINT4               u4IpAddr = SYSLOG_ZERO;
    INT4                i4TrapCfgModuleId = SYSLOG_ZERO;
    INT4                i4NextTrapCfgModuleId = SYSLOG_ZERO;
    INT4                i4TrapLevel = -1;
    INT4                i4Facility = SYSLOG_ZERO;
    INT4                i4Authenticate = SYSLOG_ZERO;
    CHR1               *pu1String = NULL;
    UINT1               au1MailId[SMTP_DOMAIN_NAME_LEN];
    UINT1               au1UserName[SMTP_MAX_STRING];
    INT4                i4SyslogFileSize = SYSLOG_ZERO;

    tSNMP_OCTET_STRING_TYPE *pu1IpAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pu1RxMailId = NULL;
    tSNMP_OCTET_STRING_TYPE *pu1FileName = NULL;
    tSNMP_OCTET_STRING_TYPE FsSyslogSmtpMailId;
    tSNMP_OCTET_STRING_TYPE *pu1FileNameOne = NULL;
    tSNMP_OCTET_STRING_TYPE *pu1FileNameTwo = NULL;
    tSNMP_OCTET_STRING_TYPE *pu1FileNameThree = NULL;
    tSNMP_OCTET_STRING_TYPE UserName;

    UINT4               u4Args;

    if (OsixTakeSem
        (SELF, (const UINT1 *) SYSLOG_SEM_NAME, OSIX_WAIT,
         SYSLOG_ZERO) != OSIX_SUCCESS)
    {
        return (SYSLOG_OS_FAILURE);
    }

    nmhGetFsSyslogLogging ((INT4 *) &u4SysLogStatus);

    if (u4SysLogStatus != SYSLOG_ENABLE)
    {

        CliPrintf (CliHandle, "no logging on\r\n");
    }
    nmhGetFsSyslogServerUpDownTrap ((INT4 *) &u4SyslogServerUpDownTrap);
    if (u4SyslogServerUpDownTrap != SNMP_TRAP_ENABLE)
    {
        CliPrintf (CliHandle, "snmp trap syslog-server-status disabled\r\n");
    }

    nmhGetFsSyslogLogFile ((INT4 *) &u4LocalStorage);
    if (u4LocalStorage != SYSLOG_DISABLE)
    {
        CliPrintf (CliHandle, "syslog localstorage \r\n");
    }

    nmhGetFsSyslogMail ((INT4 *) &u4MailOption);
    if (u4MailOption != SYSLOG_DISABLE)
    {
        CliPrintf (CliHandle, "syslog mail \r\n");
    }

    nmhGetFsSyslogRole ((INT4 *) &u4SysLogRole);
    if (u4SysLogRole != SYSLOG_DEVICE_ROLE)
    {
        CliPrintf (CliHandle, "syslog relay \r\n");
    }

    nmhGetFsSyslogRelayPort (&i4SysLogPort);
    if (i4SysLogPort != BSD_SYSLOG_PORT)
    {
        CliPrintf (CliHandle, "syslog relay-port %d\r\n", i4SysLogPort);
    }

    nmhGetFsSyslogRelayTransType (&i4RelayTransType);
    if (i4RelayTransType != SYSLOG_RELAY_UDP)
    {
        CliPrintf (CliHandle, "syslog relay transport type tcp \r\n");
    }
    nmhGetFsSyslogProfile (&i4SysLogProfile);
    if (i4SysLogProfile != RSYSLOG_RAW_PROFILE)
    {
        CliPrintf (CliHandle, "syslog profile cooked\r\n");
    }
    /*SRC for configuring filename */
    pu1FileNameOne =
        (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (SYS_MAX_FILE_NAME_LEN);
    if (pu1FileNameOne == NULL)
    {
        OsixSemGive (SMTP_SEM_ID);
        return CLI_FAILURE;
    }
    MEMSET (pu1FileNameOne->pu1_OctetList, SYSLOG_ZERO, SYS_MAX_FILE_NAME_LEN);
    nmhGetFsSyslogFileNameOne (pu1FileNameOne);
    if (pu1FileNameOne->i4_Length != SYSLOG_ZERO)
    {
        CliPrintf (CliHandle, "syslog filename-one \"%s\"\r\n",
                   pu1FileNameOne->pu1_OctetList);
    }

    pu1FileNameTwo =
        (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (SYS_MAX_FILE_NAME_LEN);
    if (pu1FileNameTwo == NULL)
    {
        free_octetstring (pu1FileNameOne);
        OsixSemGive (SMTP_SEM_ID);
        return CLI_FAILURE;
    }
    MEMSET (pu1FileNameTwo->pu1_OctetList, SYSLOG_ZERO, SYS_MAX_FILE_NAME_LEN);
    nmhGetFsSyslogFileNameTwo (pu1FileNameTwo);
    if (pu1FileNameTwo->i4_Length != SYSLOG_ZERO)
    {
        CliPrintf (CliHandle, "syslog filename-two \"%s\"\r\n",
                   pu1FileNameTwo->pu1_OctetList);
    }

    pu1FileNameThree =
        (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (SYS_MAX_FILE_NAME_LEN);
    if (pu1FileNameThree == NULL)
    {
        free_octetstring (pu1FileNameOne);
        free_octetstring (pu1FileNameTwo);
        OsixSemGive (SMTP_SEM_ID);
        return CLI_FAILURE;
    }
    MEMSET (pu1FileNameThree->pu1_OctetList, SYSLOG_ZERO,
            SYS_MAX_FILE_NAME_LEN);
    nmhGetFsSyslogFileNameThree (pu1FileNameThree);
    if (pu1FileNameThree->i4_Length != SYSLOG_ZERO)
    {
        CliPrintf (CliHandle, "syslog filename-three \"%s\"\r\n",
                   pu1FileNameThree->pu1_OctetList);
    }

    nmhGetFsSyslogSmtpAuthMethod (&i4Authenticate);

    switch (i4Authenticate)
    {
        case SMTP_AUTH_LOGIN:
            CliPrintf (CliHandle, "smtp authentication auth-login\r\n");
            break;
        case SMTP_AUTH_PLAIN:
            CliPrintf (CliHandle, "smtp authentication auth-plain\r\n");
            break;
        case SMTP_AUTH_CRAM_MD5:
            CliPrintf (CliHandle, "smtp authentication cram-md5\r\n");
            break;
        case SMTP_AUTH_DIGEST_MD5:
            CliPrintf (CliHandle, "smtp authentication digest-md5\r\n");
            break;
    }

    /*Forwarding Table SRC */

    /*Allocating memory */
    pu1IpAddr =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (DNS_MAX_QUERY_LEN);

    /* Sanity check */
    if ((pu1IpAddr == NULL))
    {
        free_octetstring (pu1FileNameOne);
        free_octetstring (pu1FileNameTwo);
        free_octetstring (pu1FileNameThree);
        OsixSemGive (SMTP_SEM_ID);
        return CLI_FAILURE;
    }

    MEMSET (pu1IpAddr->pu1_OctetList, SYSLOG_ZERO, IPVX_IPV6_ADDR_LEN);

    if (nmhGetFirstIndexFsSyslogFwdTable (&i4Priority, &i4AddrType, pu1IpAddr)
        == SNMP_SUCCESS)
    {
        do
        {
            nmhGetFsSyslogFwdRowStatus (i4Priority, i4AddrType, pu1IpAddr,
                                        &i4RowStatus);
            if (i4RowStatus == SYS_ACTIVE)
            {
                CliPrintf (CliHandle, "logging-server %d", i4Priority);
                nmhGetFsSyslogFwdPort (i4Priority, i4AddrType, pu1IpAddr,
                                       &i4Port);

                nmhGetFsSyslogFwdTransType (i4Priority, i4AddrType, pu1IpAddr,
                                            &i4TransType);

                if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    SYSLOG_INET_HTONL (pu1IpAddr->pu1_OctetList);
                    MEMCPY (&u4TempIpv4, pu1IpAddr->pu1_OctetList,
                            IPVX_IPV4_ADDR_LEN);
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4TempIpv4);
                    CliPrintf (CliHandle, " ipv4 %s ", pu1String);
                }
                else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
                {
                    SYSLOG_INET_HTONL (pu1IpAddr->pu1_OctetList);
                    MEMCPY (au1Ip6Addr, pu1IpAddr->pu1_OctetList,
                            IPVX_IPV6_ADDR_LEN);
                    SYSLOG_INET_HTONL (au1Ip6Addr);
                    CliPrintf (CliHandle, " ipv6 %s ",
                               Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr));
                }
                else if (i4AddrType == IPVX_DNS_FAMILY)
                {
                    CliPrintf (CliHandle, " %s  ", pu1IpAddr->pu1_OctetList);
                }
                if (i4Port != SYSLOG_PORT)
                {
                    CliPrintf (CliHandle, "port  %d", i4Port);
                }

                if (i4TransType == SYSLOG_TCP)
                {
                    CliPrintf (CliHandle, " tcp\n");
                }
                else if (i4TransType == SYSLOG_BEEP)
                {
                    CliPrintf (CliHandle, " beep\n");
                }

                else if (i4TransType == SYSLOG_UDP)
                {
                    CliPrintf (CliHandle, "\n");
                }
            }

            if (i4AddrType != IPVX_DNS_FAMILY)
            {
                SYSLOG_INET_HTONL (pu1IpAddr->pu1_OctetList);
            }
        }
        while (nmhGetNextIndexFsSyslogFwdTable (i4Priority, &i4Priority,
                                                i4AddrType, &i4AddrType,
                                                pu1IpAddr, pu1IpAddr));
    }

    /*Ends for Fwd Table */

    /*Mail Table SRC */
    pu1RxMailId =
        (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (SYSLOG_RXMAILID_LENGTH);

    /* Sanity check */

    if ((pu1RxMailId == NULL))
    {
        free_octetstring (pu1FileNameOne);
        free_octetstring (pu1FileNameTwo);
        free_octetstring (pu1FileNameThree);
        free_octetstring (pu1IpAddr);
        OsixSemGive (SMTP_SEM_ID);
        return CLI_FAILURE;
    }
    UserName.pu1_OctetList = au1UserName;
    MEMSET (pu1IpAddr->pu1_OctetList, SYSLOG_ZERO, DNS_MAX_QUERY_LEN);
    MEMSET (pu1RxMailId->pu1_OctetList, SYSLOG_ZERO, SYSLOG_RXMAILID_LENGTH);

    /* Get the First Index of the File Table */

    if (nmhGetFirstIndexFsSyslogMailTable (&i4Priority, &i4AddrType, pu1IpAddr)
        == SNMP_SUCCESS)
    {
        do
        {
            nmhGetFsSyslogMailRowStatus (i4Priority, i4AddrType, pu1IpAddr,
                                         &i4RowStatus);
            if (i4RowStatus == SYS_ACTIVE)
            {
                MEMSET (au1UserName, 0, SMTP_MAX_STRING);
                CliPrintf (CliHandle, "mail-server %d", i4Priority);
                if (nmhGetFsSyslogRxMailId (i4Priority, i4AddrType,
                                            pu1IpAddr,
                                            pu1RxMailId) == SNMP_FAILURE)
                {
                    free_octetstring (pu1FileNameOne);
                    free_octetstring (pu1FileNameTwo);
                    free_octetstring (pu1FileNameThree);
                    free_octetstring (pu1IpAddr);
                    free_octetstring (pu1RxMailId);
                    OsixSemGive (SMTP_SEM_ID);
                    return CLI_FAILURE;
                }
                if (nmhGetFsSyslogMailServUserName (i4Priority, i4AddrType,
                                                    pu1IpAddr, &UserName)
                    == SNMP_FAILURE)
                {
                    free_octetstring (pu1FileNameOne);
                    free_octetstring (pu1FileNameTwo);
                    free_octetstring (pu1FileNameThree);
                    free_octetstring (pu1IpAddr);
                    free_octetstring (pu1RxMailId);
                    OsixSemGive (SMTP_SEM_ID);
                    return CLI_FAILURE;
                }

                if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    SYSLOG_INET_HTONL (pu1IpAddr->pu1_OctetList);
                    MEMCPY (&u4Args, pu1IpAddr->pu1_OctetList, sizeof (UINT4));
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Args);
                    if (UserName.i4_Length != 0)
                    {
                        CliPrintf (CliHandle, " ipv4 %s %s ", pu1String,
                                   pu1RxMailId->pu1_OctetList);
                    }
                    else
                    {
                        CliPrintf (CliHandle, " ipv4 %s %s\r\n", pu1String,
                                   pu1RxMailId->pu1_OctetList);
                    }
                }
                else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
                {
                    SYSLOG_INET_HTONL (pu1IpAddr->pu1_OctetList);
                    MEMCPY (au1Ip6Addr, pu1IpAddr->pu1_OctetList,
                            IPVX_IPV6_ADDR_LEN);
                    SYSLOG_INET_HTONL (au1Ip6Addr);
                    if (UserName.i4_Length != 0)
                    {
                        CliPrintf (CliHandle, " ipv6 %s %s ",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                 au1Ip6Addr),
                                   pu1RxMailId->pu1_OctetList);
                    }
                    else
                    {
                        CliPrintf (CliHandle, " ipv6 %s %s\r\n",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                 au1Ip6Addr),
                                   pu1RxMailId->pu1_OctetList);
                    }
                }
                else
                {
                    if (UserName.i4_Length != 0)
                    {
                        CliPrintf (CliHandle, "  %s %s ",
                                   pu1IpAddr->pu1_OctetList,
                                   pu1RxMailId->pu1_OctetList);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "  %s %s\r\n",
                                   pu1IpAddr->pu1_OctetList,
                                   pu1RxMailId->pu1_OctetList);
                    }
                }
                if (UserName.i4_Length != 0)
                {
                    CliPrintf (CliHandle, "user %s", au1UserName);
                }
            }
            MEMSET (pu1RxMailId->pu1_OctetList, SYSLOG_ZERO,
                    SYSLOG_RXMAILID_LENGTH);
            SYSLOG_INET_HTONL (pu1IpAddr->pu1_OctetList);
            CliPrintf (CliHandle, "\r\n");

        }
        while (nmhGetNextIndexFsSyslogMailTable (i4Priority, &i4Priority,
                                                 i4AddrType, &i4AddrType,
                                                 pu1IpAddr,
                                                 pu1IpAddr) == SNMP_SUCCESS);
    }
    CliPrintf (CliHandle, "\r\n");

    /*Mail Table SRC Ends */
    nmhGetFsSyslogFileSize (&i4SyslogFileSize);
    if ((i4SyslogFileSize >= SYSLOG_MIN_FILESIZE)
        && (i4SyslogFileSize <= SYSLOG_MAX_FILESIZE))
    {
        if (i4SyslogFileSize != AUDIT_MAX_FILE_SIZE)
        {
            CliPrintf (CliHandle, "logging-filesize %d\r\n", i4SyslogFileSize);
        }
    }

    /*File Table SRC Starts */

    /*Allocating memory */
    pu1FileName =
        (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (SYS_MAX_FILE_NAME_LEN);

    /* Sanity check */
    if ((pu1FileName == NULL))
    {
        free_octetstring (pu1FileNameOne);
        free_octetstring (pu1FileNameTwo);
        free_octetstring (pu1FileNameThree);
        free_octetstring (pu1IpAddr);
        free_octetstring (pu1RxMailId);
        OsixSemGive (SMTP_SEM_ID);
        return CLI_FAILURE;
    }

    MEMSET (pu1FileName->pu1_OctetList, SYSLOG_ZERO, SYS_MAX_FILE_NAME_SIZE);

    /* Get the First Index of the File Table */

    if (nmhGetFirstIndexFsSyslogFileTable (&i4Priority, pu1FileName) ==
        SNMP_SUCCESS)
    {
        do
        {
            CliPrintf (CliHandle, "logging-file %d  %s\r\n", i4Priority,
                       pu1FileName->pu1_OctetList);

        }
        while (nmhGetNextIndexFsSyslogFileTable
               (i4Priority, &i4Priority, pu1FileName,
                pu1FileName) == SNMP_SUCCESS);
    }

    /*File Table SRC Ends */

    nmhGetFsSyslogConsoleLog ((INT4 *) &u4LogConsole);

    if (u4LogConsole != SYSLOG_ENABLE)
    {
        CliPrintf (CliHandle, "no logging console\r\n");
    }

    nmhGetFsSyslogTimeStamp ((INT4 *) &u4TimeStamp);

    if (u4TimeStamp != SYSLOG_ENABLE)
    {
        CliPrintf (CliHandle, "no service timestamps\r\n");
    }

    nmhGetFsSyslogLogSrvAddr (&u4IpAddr);

    if (u4IpAddr != 0)
    {
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr);
        CliPrintf (CliHandle, "logging %s\r\n", pu1String);

    }

    nmhGetFsSyslogSysBuffers ((INT4 *) &u4Count);

    if ((u4Count != DEF_SYSLOG_NUMCMD_BUFF) && (u4Count != DEF_SYSLOG_MAX_BUFF))
    {
        CliPrintf (CliHandle, "logging buffered %d\r\n", u4Count);
    }

    nmhGetFsSyslogFacility (&i4Facility);
    switch (i4Facility)
    {
        case SYSLG_FACILITY_LOCAL1:
            CliPrintf (CliHandle, "logging facility local1\r\n");
            break;
        case SYSLG_FACILITY_LOCAL2:
            CliPrintf (CliHandle, "logging facility local2\r\n");
            break;
        case SYSLG_FACILITY_LOCAL3:
            CliPrintf (CliHandle, "logging facility local3\r\n");
            break;
        case SYSLG_FACILITY_LOCAL4:
            CliPrintf (CliHandle, "logging facility local4\r\n");
            break;
        case SYSLG_FACILITY_LOCAL5:
            CliPrintf (CliHandle, "logging facility local5\r\n");
            break;
        case SYSLG_FACILITY_LOCAL6:
            CliPrintf (CliHandle, "logging facility local6\r\n");
            break;
        case SYSLG_FACILITY_LOCAL7:
            CliPrintf (CliHandle, "logging facility local7\r\n");
            break;
    }

    u4Count = 0;

    if (nmhGetFirstIndexFsSyslogConfigTable (&i4TrapCfgModuleId) ==
        SNMP_SUCCESS)
    {
        i4NextTrapCfgModuleId = i4TrapCfgModuleId;

        do
        {
            i4TrapCfgModuleId = i4NextTrapCfgModuleId;

            nmhGetFsSyslogConfigLogLevel (i4TrapCfgModuleId, &i4TrapLevel);
            if (STRCASECMP (asModInfo[i4TrapCfgModuleId].au1Name, "CLI") == 0)

            {
                switch (i4TrapLevel)
                {
                    case SYSLOG_EMERG_LEVEL:
                        CliPrintf (CliHandle,
                                   "logging severity emergencies\r\n");
                        break;
                    case SYSLOG_ALERT_LEVEL:
                        CliPrintf (CliHandle, "logging severity alerts\r\n");
                        break;
                    case SYSLOG_ERROR_LEVEL:
                        CliPrintf (CliHandle, "logging severity errors\r\n");
                        break;
                    case SYSLOG_WARN_LEVEL:
                        CliPrintf (CliHandle, "logging severity warnings\r\n");
                        break;
                    case SYSLOG_NOTICE_LEVEL:
                        CliPrintf (CliHandle,
                                   "logging severity notification\r\n");
                        break;
                    case SYSLOG_INFO_LEVEL:
                        CliPrintf (CliHandle,
                                   "logging severity informational\r\n");
                        break;
                    case SYSLOG_DEBUG_LEVEL:
                        CliPrintf (CliHandle, "logging severity debugging\r\n");
                        break;

                }
            }

        }
        while (nmhGetNextIndexFsSyslogConfigTable (i4TrapCfgModuleId,
                                                   &i4NextTrapCfgModuleId)
               == SNMP_SUCCESS);
    }

    nmhGetFsSyslogSmtpSrvAddr (&u4IpAddr);

    if (u4IpAddr)
    {
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr);
        CliPrintf (CliHandle, "mailserver %s\r\n", pu1String);
    }

    FsSyslogSmtpMailId.pu1_OctetList = au1MailId;

    MEMSET (FsSyslogSmtpMailId.pu1_OctetList, 0, SMTP_DOMAIN_NAME_LEN);

    nmhGetFsSyslogSmtpSenderMailId (&FsSyslogSmtpMailId);

    if ((FsSyslogSmtpMailId.i4_Length != 0) &&
        (STRCMP (FsSyslogSmtpMailId.pu1_OctetList, "syslog@Aricent.com")) != 0)
    {
        CliPrintf (CliHandle, "sender mail-id %s\r\n",
                   FsSyslogSmtpMailId.pu1_OctetList);
    }

    FsSyslogSmtpMailId.pu1_OctetList = au1MailId;

    MEMSET (FsSyslogSmtpMailId.pu1_OctetList, 0, SMTP_DOMAIN_NAME_LEN);

    nmhGetFsSyslogSmtpRcvrMailId (&FsSyslogSmtpMailId);

    if ((FsSyslogSmtpMailId.i4_Length != 0) &&
        (STRCMP (FsSyslogSmtpMailId.pu1_OctetList, "admin@aricent.com")) != 0)
    {
        CliPrintf (CliHandle, "receiver mail-id \"%s\"\r\n",
                   FsSyslogSmtpMailId.pu1_OctetList);
    }

    free_octetstring (pu1FileNameOne);
    free_octetstring (pu1FileNameTwo);
    free_octetstring (pu1FileNameThree);
    free_octetstring (pu1IpAddr);
    free_octetstring (pu1RxMailId);
    free_octetstring (pu1FileName);

    OsixSemGive (SMTP_SEM_ID);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogCfgMaxModLogBuff                                 */
/*                                                                           */
/* Description      : This function is invoked to set the maximum module log */
/*                    buffers                                                */
/*                                                                           */
/* Input Parameters : CliHandle     - CliContext ID                          */
/*                    i4BuffSize    - Syslog SysBuffer size to set           */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS - always                                  */
/*****************************************************************************/

INT4
SysLogCfgMaxModLogBuff (tCliHandle CliHandle, INT4 i4BuffSize)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsSyslogSysBuffers (&u4ErrorCode, i4BuffSize) == SNMP_SUCCESS)
    {
        if (nmhSetFsSyslogSysBuffers (i4BuffSize) == SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return (CLI_FAILURE);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogCfgMaxUsrLogBuff                                 */
/*                                                                           */
/* Description      : This function is invoked to set number of              */
/*                    syslog buffers for a particular user                   */
/*                                                                           */
/* Input Parameters : CliHandle      - CliContext ID                         */
/*                    *pUser         - User Name                             */
/*                    u4Value        - maximum no. of buffers                */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS - always                                  */
/*****************************************************************************/

INT4
SysLogCfgMaxUsrLogBuff (tCliHandle CliHandle, UINT1 *pUser, UINT4 u4Value)
{
    INT4                i4Status;
    INT4                i4UserId = 0;

    i4UserId = mmi_search_user ((INT1 *) pUser);

    /* Check if the user name is valid and also if the given value is within
     * range.
     */

    if (i4UserId != CLI_ERROR)
    {
        i4Status = SysLogSetMaxLogBuff ((UINT4) i4UserId, (UINT4) u4Value);

        if (i4Status == SYSLOG_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        if (i4Status == SYSLOG_INVALID_VALUE)
        {
            CliPrintf (CliHandle,
                       "\r%% Invalid value for number of buffers \r\n");
        }
        else if (i4Status == SYSLOG_MEMALLOC_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    else
    {

        CliPrintf (CliHandle, "\r%% No such user\r\n");
    }
    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : CliSetModLogLevel                                      */
/*                                                                           */
/* Description      : This function is invoked to set the log level for all  */
/*                    modules registered with SysLog                         */
/*                                                                           */
/* Input Parameters : CliHandle - Cli Context ID                             */
/*                    i4TrapLevel - Trap level to set for all modules.       */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : SYSLOG_CLI_SUCCESS - always                           */
/*****************************************************************************/

INT4
SysLogCfgModLogLevel (tCliHandle CliHandle, INT4 i4TrapLevel)
{
    UINT4               u4ErrorCode;
    INT4                i4TrapCfgModuleId;

    if (nmhGetFirstIndexFsSyslogConfigTable (&i4TrapCfgModuleId) ==
        SNMP_FAILURE)
    {
        /* No module is registered */
        return CLI_SUCCESS;
    }

    do
    {
        if (nmhTestv2FsSyslogConfigLogLevel
            (&u4ErrorCode, i4TrapCfgModuleId, i4TrapLevel) != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetFsSyslogConfigLogLevel (i4TrapCfgModuleId, i4TrapLevel) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    while (nmhGetNextIndexFsSyslogConfigTable (i4TrapCfgModuleId,
                                               &i4TrapCfgModuleId) ==
           SNMP_SUCCESS);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SyslogEnableTrace                                      */
/*                                                                           */
/* Description      : This function is invoked to Enable the BGP Traces      */
/*                                                                           */
/* Input Parameters : i4TrapLevel - TrapLevel                                */
/*                                                                           */
/* Return Value     : CLI_SUCCESS/CLI_FAILURE                                */
/*****************************************************************************/

INT4
SyslogEnableTrace (INT4 i4TrapLevel, INT4 u4ModuleId)
{
#ifdef BGP_WANTED
    if (!(STRCMP (asModInfo[u4ModuleId].au1Name, "BGP4")))
    {
        if (BgpSetTraceValue (i4TrapLevel, gu1LoggingCmd) != CLI_SUCCESS)
        {
            return (CLI_FAILURE);
        }
    }
#endif

#ifdef VRRP_WANTED
    if (VrrpSetTraceValue (i4TrapLevel, gu1LoggingCmd) != CLI_SUCCESS)
    {
        return (CLI_FAILURE);
    }
#endif

#ifdef RIP_WANTED
    if (!(STRCMP (asModInfo[u4ModuleId].au1Name, "RIP")))
    {
        if (RipSetTraceValue (i4TrapLevel, gu1LoggingCmd) != CLI_SUCCESS)
        {
            return (CLI_FAILURE);
        }
    }
#endif

#ifdef OSPF_WANTED
    if (!(STRCMP (asModInfo[u4ModuleId].au1Name, "OSPF")))
    {
        if (OspfSetTraceValue (i4TrapLevel, gu1LoggingCmd) != CLI_SUCCESS)
        {
            return (CLI_FAILURE);
        }
    }
#endif

#ifdef OSPF3_WANTED
    if (!(STRCMP (asModInfo[u4ModuleId].au1Name, "OSPFV3")))
    {
        if (Ospf3SetTraceValue (i4TrapLevel, gu1LoggingCmd) != CLI_SUCCESS)
        {
            return (CLI_FAILURE);
        }
    }
#endif

#ifdef NPAPI_WANTED
    if (!(STRCMP (asModInfo[u4ModuleId].au1Name, "ISS")))
    {
        UINT4               u4Trace = 0;
        if (i4TrapLevel == SYSLOG_CRITICAL_LEVEL)
        {
            u4Trace =
                (CLI_ISS_NP_LOG_CRITICAL_LEVEL | CLI_ISS_NP_LOG_ERROR_LEVEL);
        }
        else if (i4TrapLevel == SYSLOG_INFO_LEVEL)
        {
            u4Trace = CLI_ISS_NP_LOG_INFO_LEVEL;
        }
        if (IsssysNpSetTrace (CLI_ISS_NP_MOD_BCMX, (UINT1) u4Trace) !=
            CLI_SUCCESS)
        {
            return (CLI_FAILURE);
        }
    }
#else
    UNUSED_PARAM (i4TrapLevel);
    UNUSED_PARAM (u4ModuleId);
#endif
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogSetLogging                                       */
/*                                                                           */
/* Description      : This function is invoked to Enable/Disable logging     */
/*                    feature.                                               */
/*                                                                           */
/* Input Parameters : CliHandle - Cli Context ID                             */
/*                    i4LogStatus - SYSLOG_ENABLE/SYSLOG_DISABLE             */
/*                                                                           */
/* Output Parameters: None                                                   */
/*                                                                           */
/* Return Value     : CLI_SUCCESS/CLI_FAILURE                                */
/*****************************************************************************/

INT4
SysLogSetLogging (tCliHandle CliHandle, INT4 i4LogStatus)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsSyslogLogging (&u4ErrorCode, i4LogStatus) == SNMP_SUCCESS)
    {
        if (nmhSetFsSyslogLogging (i4LogStatus) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return (CLI_FAILURE);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SysLogSmtpSetMyDomainName                        */
/*                                                                          */
/*    Description        : This function is used to set the domain name of  */
/*                         the mail client ( our domain name )              */
/*                                                                          */
/*    Input(s)           : CliHandle    - CliContext ID                     */
/*                         pDomain      - Pointer to the Domain Name        */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                          */
/****************************************************************************/

INT4
SysLogSmtpSetMyDomainName (tCliHandle CliHandle, UINT1 *pDomain)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE DomainName;
    UINT1               au1DomainName[SMTP_DOMAIN_NAME_LEN];

    if (pDomain != NULL)
    {
        DomainName.i4_Length = STRLEN (pDomain);
        DomainName.pu1_OctetList = pDomain;

        if (nmhTestv2FsSyslogSmtpSenderMailId (&u4ErrorCode, &DomainName) ==
            SNMP_SUCCESS)
        {
            if (nmhSetFsSyslogSmtpSenderMailId (&DomainName) == SNMP_SUCCESS)
            {
                return (CLI_SUCCESS);
            }
            else
            {
                CLI_FATAL_ERROR (CliHandle);
            }
        }

        /* Error code is set in Low level routine itself. Nothing to do here */
        return (CLI_FAILURE);
    }
    else
    {
        MEMSET (au1DomainName, 0, SMTP_DOMAIN_NAME_LEN);
        DomainName.pu1_OctetList = au1DomainName;
        DomainName.i4_Length = 0;

        if (nmhTestv2FsSyslogSmtpSenderMailId (&u4ErrorCode, &DomainName) ==
            SNMP_SUCCESS)
        {
            if (nmhSetFsSyslogSmtpSenderMailId (&DomainName) == SNMP_SUCCESS)
            {
                return (CLI_SUCCESS);
            }
            else
            {
                CLI_FATAL_ERROR (CliHandle);
            }
        }

        /* Error code is set in Low level routine itself. Nothing to do here */
        return (CLI_FAILURE);
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SysLogSetClearLogs                               */
/*                                                                          */
/*    Description        : This function is used to Clear the SysLog        */
/*                         buffers.                                         */
/*                                                                          */
/*    Input(s)           : CliHandle    - CliContext ID                     */
/*                         i4Status     - SYSLOG_TRUE always                */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                          */
/****************************************************************************/

INT4
SysLogSetClearLogs (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsSyslogClearLog (&u4ErrorCode, i4Status) == SNMP_SUCCESS)
    {
        if (nmhSetFsSyslogClearLog (i4Status) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return (CLI_FAILURE);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogShowLogBuffer                                    */
/*                                                                           */
/* Description      : This function is invoked from syslgcli.c to            */
/*                    display the contents of the log buffer                 */
/*                                                                           */
/* Input Parameters : tCliHandle CliHandle                                   */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SysLogShowLogBuffer (tCliHandle CliHandle)
{
    UINT4               u4Count = 0;
    tSysLogBuff        *pTempBuff = NULL;
    tSysLogEntry       *pSysLogEntry = NULL;

    pSysLogEntry = &gaSysLogTbl[MAX_SYSLOG_USERS_LIMIT];

    if (pSysLogEntry != NULL)
    {
        for (pTempBuff = pSysLogEntry->pHead; pTempBuff != NULL;
             pTempBuff = pTempBuff->pNext)
        {
            if (u4Count == pSysLogEntry->u4NumMsgLogs)
            {
                break;
            }
            u4Count++;
            CliPrintf (CliHandle, "%s \r\n", pTempBuff->ai1LogStr);
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogSetRole                                          */
/*                                                                           */
/* Description      : This function is invoked to set syslog in relay role   */
/*                                                                           */
/*                                                                           */
/* Input Parameters : CliHandle - Cli Context ID                             */
/*                    i4RelayRole - SYSLOG_DEVICE_ROLE/SYSLOG_RELAY_ROLE     */
/*                                                                           */
/* Output Parameters: None                                                   */
/*                                                                           */
/* Return Value     : CLI_SUCCESS/CLI_FAILURE                                */
/*****************************************************************************/

INT4
SysLogSetRole (tCliHandle CliHandle, INT4 i4RelayRole)
{
    UINT4               u4ErrorCode;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsSyslogRole (&u4ErrorCode, i4RelayRole) == SNMP_SUCCESS)
    {
        if (nmhSetFsSyslogRole (i4RelayRole) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }
    return (CLI_FAILURE);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogSetMail                                          */
/*                                                                           */
/* Description      : This function is invoked to set syslog mail option     */
/*                                                                           */
/*                                                                           */
/* Input Parameters : CliHandle - Cli Context ID                             */
/*                    i4MailOption - SYSLOG_ENABLE/SYSLOG_DISABLE            */
/*                                                                           */
/* Output Parameters: None                                                   */
/*                                                                           */
/* Return Value     : CLI_SUCCESS/CLI_FAILURE                                */
/*****************************************************************************/

INT4
SysLogSetMail (tCliHandle CliHandle, INT4 i4MailOption)
{
    UINT4               u4ErrorCode;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsSyslogMail (&u4ErrorCode, i4MailOption) == SNMP_SUCCESS)
    {
        if (nmhSetFsSyslogMail (i4MailOption) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }
    return (CLI_FAILURE);
}

/******************************************** ****************************************/
/*                                                                                   */
/* Function Name    : SysLogSetLocalStorage                                          */
/*                                                                                   */
/* Description      : This function is invoked to set syslog file storage option     */
/*                                                                                   */
/*                                                                                   */
/* Input Parameters : CliHandle - Cli Context ID                                     */
/*                    i4LocalStorage - SYSLOG_ENABLE/SYSLOG_DISABLE                  */
/*                                                                                   */
/* Output Parameters: None                                                           */
/*                                                                                   */
/* Return Value     : CLI_SUCCESS/CLI_FAILURE                                        */
/*********************************************************************************** */

INT4
SysLogSetLocalStorage (tCliHandle CliHandle, INT4 i4LocalStorage)
{
    UINT4               u4ErrorCode;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsSyslogLogFile (&u4ErrorCode, i4LocalStorage) == SNMP_SUCCESS)
    {
        if (nmhSetFsSyslogLogFile (i4LocalStorage) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }
    return (CLI_FAILURE);
}

/*************************************************************************************/
/*                                                                                   */
/* Function Name    : SysLogSetSysLogPort                                            */
/*                                                                                   */
/* Description      : This function is invoked to set syslog port                    */
/*                                                                                   */
/*                                                                                   */
/* Input Parameters : CliHandle - Cli Context ID                                     */
/*                    i4Port - Syslog Port                                           */
/*                                                                                   */
/* Output Parameters: None                                                           */
/*                                                                                   */
/* Return Value     : CLI_SUCCESS/CLI_FAILURE                                        */
/*************************************************************************************/

INT4
SysLogSetSysLogPort (tCliHandle CliHandle, INT4 i4Port)
{

    UINT4               u4ErrorCode;
    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsSyslogRelayPort (&u4ErrorCode, i4Port) == SNMP_SUCCESS)
    {
        if (nmhSetFsSyslogRelayPort (i4Port) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }
    return (CLI_FAILURE);
}

/*************************************************************************************/
/*                                                                                   */
/* Function Name    : SysLogSetSysLogProfile                                         */
/*                                                                                   */
/* Description      : This function is invoked to set syslog profile                 */
/*                                                                                   */
/*                                                                                   */
/* Input Parameters : CliHandle - Cli Context ID                                     */
/*                    i4Profile - Syslog Profile                                     */
/*                                                                                   */
/* Output Parameters: None                                                           */
/*                                                                                   */
/* Return Value     : CLI_SUCCESS/CLI_FAILURE                                        */
/*************************************************************************************/

INT4
SysLogSetSysLogProfile (tCliHandle CliHandle, CHR1 * pc1Profile)
{
    UINT4               u4ErrorCode;
    INT4                i4Profile = SYSLOG_ZERO;

    UNUSED_PARAM (CliHandle);

    if (STRCMP (pc1Profile, RSYSLOG_DEFAULT_PROFILE) == SYSLOG_ZERO)
    {
        i4Profile = RSYSLOG_RAW_PROFILE;
    }
    else if (STRCMP (pc1Profile, "cooked") == SYSLOG_ZERO)
    {
        i4Profile = RSYSLOG_COOKED_PROFILE;
    }

    if (nmhTestv2FsSyslogProfile (&u4ErrorCode, i4Profile) == SNMP_SUCCESS)
    {
        if (nmhSetFsSyslogProfile (i4Profile) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }
    return (CLI_FAILURE);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogShowRole                                         */
/*                                                                           */
/* Description      : This function is invoked to display syslog's role      */
/*                                                                           */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID                              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SysLogShowRole (tCliHandle CliHandle)
{

    INT4                i4Role;

    nmhGetFsSyslogRole (&i4Role);
    if (i4Role)
    {
        if (SYSLOG_DEVICE_ROLE == i4Role)
        {
            CliPrintf (CliHandle, "Syslog Role    : Device\r\n\r\n");
        }
        else if (SYSLOG_RELAY_ROLE == i4Role)
        {
            CliPrintf (CliHandle, "Syslog Role    : Relay\r\n\r\n");
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r%% Syslog Role is not configured\r\n");
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogShowMail                                         */
/*                                                                           */
/* Description      : This function is invoked to display whether
                      sysmail configured                                     */
/*                                                                           */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID                              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SysLogShowMail (tCliHandle CliHandle)
{

    INT4                i4MailOption;

    nmhGetFsSyslogMail (&i4MailOption);
    if (i4MailOption)
    {
        if (SYSLOG_ENABLE == i4MailOption)
        {
            CliPrintf (CliHandle, "Syslog Mail Option    : Enabled\r\n\r\n");
        }
        else if (SYSLOG_DISABLE == i4MailOption)
        {
            CliPrintf (CliHandle, "Syslog Mail Option    : Disabled\r\n\r\n");
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r%% Syslog Mail  is not configured\r\n");
    }
    return CLI_SUCCESS;
}

/*****************************************************************************************/
/*                                                                                       */
/* Function Name    : SysLogShowPort                                                     */
/*                                                                                       */
/* Description      : This function is invoked to display Syslog Port                    */
/*                                                                                       */
/*                                                                                       */
/* Input Parameters : CliHandle - CliContext ID                                          */
/*                                                                                       */
/* Output Parameters : None                                                              */
/*                                                                                       */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                           */
/*****************************************************************************************/

INT4
SysLogShowSysLogPort (tCliHandle CliHandle)
{

    INT4                i4SysLogPort;

    nmhGetFsSyslogRelayPort (&i4SysLogPort);
    /* If you choose to reconfigure the log port,
     * make sure that the port number you assign to syslog is
     * not more than INT32 [65535]]*/
    if (i4SysLogPort <= BSD_SYSLOG_PORT_MAX)
    {
        CliPrintf (CliHandle, "\nSyslog Port    : %d\r\n\r\n", i4SysLogPort);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************************/
/*                                                                                       */
/* Function Name    : SysLogShowRelayTransType                                           */
/*                                                                                       */
/* Description      : This function is invoked to display Syslog relay transport type    */
/*                                                                                       */
/*                                                                                       */
/* Input Parameters : CliHandle - CliContext ID                                          */
/*                                                                                       */
/* Output Parameters : None                                                              */
/*                                                                                       */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                           */
/*****************************************************************************************/
INT4
SysLogShowRelayTransType (tCliHandle CliHandle)
{
    INT4                i4SysRelTransType;

    nmhGetFsSyslogRelayTransType (&i4SysRelTransType);
    if (i4SysRelTransType == SYSLOG_RELAY_UDP)
    {
        CliPrintf (CliHandle, "\nSyslog Relay Transport type udp \r\n");
        return CLI_SUCCESS;
    }
    else if (i4SysRelTransType == SYSLOG_RELAY_TCP)
    {
        CliPrintf (CliHandle, "\nSyslog Relay Transport type tcp \r\n");
        return CLI_SUCCESS;
    }
    return CLI_FAILURE;
}

/*****************************************************************************************/
/*                                                                                       */
/* Function Name    : SysLogSetRelayTransType                                            */
/*                                                                                       */
/* Description      : This function is invoked to set syslog relay transport type        */
/*                                                                                       */
/*                                                                                       */
/* Input Parameters : CliHandle - CliContext ID and relay tranport type                  */
/*                                                                                       */
/* Output Parameters : None                                                              */
/*                                                                                       */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                           */
/*****************************************************************************************/

INT4
SysLogSetRelayTransType (tCliHandle CliHandle, INT4 i4RelayTransType)
{
    UINT4               u4ErrorCode;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsSyslogRelayTransType (&u4ErrorCode, i4RelayTransType) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsSyslogRelayTransType (i4RelayTransType) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }
    return (CLI_FAILURE);
}

/*****************************************************************************************/
/*                                                                                       */
/* Function Name    : SysLogShowProfile                                                  */
/*                                                                                       */
/* Description      : This function is invoked to display Syslog Profile                 */
/*                                                                                       */
/*                                                                                       */
/* Input Parameters : CliHandle - CliContext ID                                          */
/*                                                                                       */
/* Output Parameters : None                                                              */
/*                                                                                       */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                           */
/*****************************************************************************************/

INT4
SysLogShowSysLogProfile (tCliHandle CliHandle)
{

    INT4                i4SysLogProfile;

    nmhGetFsSyslogProfile (&i4SysLogProfile);
    if (i4SysLogProfile)
    {
        if (RSYSLOG_RAW_PROFILE == i4SysLogProfile)
        {
            CliPrintf (CliHandle, "\nSyslog Profile    : raw\r\n\r\n");
        }
        else if (RSYSLOG_COOKED_PROFILE == i4SysLogProfile)
        {
            CliPrintf (CliHandle, "\nSyslog Profile    : cooked\r\n\r\n");
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r%% Syslog Profile  is not configured\r\n");
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogShowLocalStorage                                 */
/*                                                                           */
/* Description      : This function is invoked to display syslog's 
                      local storage configuration                            */
/*                                                                           */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID                              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SysLogShowLocalStorage (tCliHandle CliHandle)
{

    INT4                i4LocalStorage;

    nmhGetFsSyslogLogFile (&i4LocalStorage);
    if (i4LocalStorage)
    {
        if (SYSLOG_ENABLE == i4LocalStorage)
        {
            CliPrintf (CliHandle, "Syslog Localstorage    : Enabled\r\n\r\n");
        }
        else if (SYSLOG_DISABLE == i4LocalStorage)
        {
            CliPrintf (CliHandle, "Syslog Localstorage    :Disabled\r\n\r\n");
        }
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r%% Syslog Local storage  is not configured\r\n");
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogSetFileTable                                     */
/*                                                                           */
/* Description      : This function is invoked to set the values in file table */
/*                                                                           */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID,
 *                    u4Priority- Priority
 *                    *pFileName- FileName
 *                    u4FileSize- FileSize*/
/*                                                                           */
/* Output Parameters :                                                       */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SysLogSetFileTable (tCliHandle CliHandle, INT4 u4Priority, UINT1 *pFileName)
{

    INT4                i4status = SYSLOG_ZERO;
    UINT4               u4ErrCode;

    tSNMP_OCTET_STRING_TYPE FsSyslogFileName;
    FsSyslogFileName.pu1_OctetList = pFileName;
    FsSyslogFileName.i4_Length = STRLEN (pFileName);

    if (nmhGetFsSyslogFileRowStatus (u4Priority, &FsSyslogFileName, &i4status)
        == SNMP_SUCCESS)
    {

        if (i4status == SYS_ACTIVE)
        {
            if (nmhTestv2FsSyslogFileRowStatus (&u4ErrCode, u4Priority,
                                                &FsSyslogFileName,
                                                SYS_NOT_IN_SERVICE) ==
                SNMP_SUCCESS)
            {

                if (nmhSetFsSyslogFileRowStatus
                    (u4Priority, &FsSyslogFileName,
                     SYS_NOT_IN_SERVICE) == SNMP_FAILURE)
                {

                    return CLI_FAILURE;
                }
            }
        }
    }

    else
    {

        if (nmhTestv2FsSyslogFileRowStatus (&u4ErrCode, u4Priority,
                                            &FsSyslogFileName,
                                            SYS_CREATE_AND_WAIT) ==
            SNMP_SUCCESS)
        {

            if ((nmhSetFsSyslogFileRowStatus
                 (u4Priority, &FsSyslogFileName,
                  SYS_CREATE_AND_WAIT)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\n\t  Syslog File Table Entry Cannot be added \n");
                return CLI_FAILURE;
            }
        }
        else
        {
            return CLI_FAILURE;
        }

    }

    /*Activate the file table entry status */
    if (nmhTestv2FsSyslogFileRowStatus (&u4ErrCode, u4Priority,
                                        &FsSyslogFileName,
                                        SYS_ACTIVE) == SNMP_SUCCESS)
    {

        if (nmhSetFsSyslogFileRowStatus
            (u4Priority, &FsSyslogFileName, SYS_ACTIVE) == SNMP_FAILURE)
        {

            return CLI_FAILURE;
        }

    }
    else
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*************************************************************************************/
/*                                                                                   */
/* Function Name    : SysLogSetFileSize                                              */
/*                                                                                   */
/* Description      : This function is invoked to set logging file size              */
/*                                                                                   */
/*                                                                                   */
/* Input Parameters : CliHandle - Cli Context ID                                     */
/*                    i4filesize - logging file size                                 */
/*                                                                                   */
/* Output Parameters: None                                                           */
/*                                                                                   */
/* Return Value     : CLI_SUCCESS/CLI_FAILURE                                        */
/*************************************************************************************/

INT4
SysLogSetFileSize (tCliHandle CliHandle, INT4 i4filesize)
{

    UINT4               u4ErrorCode;
    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsSyslogFileSize (&u4ErrorCode, i4filesize) == SNMP_SUCCESS)
    {
        if (nmhSetFsSyslogFileSize (i4filesize) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CliPrintf (CliHandle, "\n \t Unable to set the  File Size \n");
            return CLI_FAILURE;
        }
    }
    CliPrintf (CliHandle, "\n \t Unable to set the  File Size \n");
    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogShowFileTable                                    */
/*                                                                           */
/* Description      : This function is invoked to display syslog's filetbl   */
/*                                                                           */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID                              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SysLogShowFileTable (tCliHandle CliHandle)
{

    INT4                i4Priority = SYSLOG_ZERO;
    tSNMP_OCTET_STRING_TYPE *pau1FileName = NULL;
    /*Allocating memory */
    pau1FileName =
        (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (SYS_MAX_FILE_NAME_LEN);

    /* Sanity check */
    if ((pau1FileName == NULL))
    {
        return CLI_FAILURE;
    }

    MEMSET (pau1FileName->pu1_OctetList, SYSLOG_ZERO, SYS_MAX_FILE_NAME_LEN);

    /* Get the First Index of the File Table */

    if (nmhGetFirstIndexFsSyslogFileTable (&i4Priority, pau1FileName) ==
        SNMP_FAILURE)
    {
        free_octetstring (pau1FileName);
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\nSyslog File Table Information\n");
    CliPrintf (CliHandle, "\n----------------------------\r\n");
    CliPrintf (CliHandle, "Priority    File-Name  \r\n");
    CliPrintf (CliHandle, "--------    ----------\n ");

    do
    {
        CliPrintf (CliHandle, "\n%d \t     %s  \n", i4Priority,
                   pau1FileName->pu1_OctetList);
    }
    while (nmhGetNextIndexFsSyslogFileTable
           (i4Priority, &i4Priority, pau1FileName,
            pau1FileName) == SNMP_SUCCESS);

    free_octetstring (pau1FileName);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogDelFileTableEntry                                */
/*                                                                           */
/* Description      : This function is invoked to delete
                                        particular entry in Filetable        */
/*                                                                           */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID                              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SysLogDelFileTableEntry (tCliHandle CliHandle, UINT4 u4Priority,
                         UINT1 *au1FileName)
{
    UINT4               u4ErrCode = SYSLOG_ZERO;
    tSNMP_OCTET_STRING_TYPE FsSyslogFileName;

    FsSyslogFileName.pu1_OctetList = au1FileName;
    FsSyslogFileName.i4_Length = STRLEN (au1FileName);

    if (nmhTestv2FsSyslogFileRowStatus
        (&u4ErrCode, u4Priority, &FsSyslogFileName,
         SYS_DESTROY) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\n\t  Syslog File Table Info cannot be set to Destroy\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsSyslogFileRowStatus (u4Priority, &FsSyslogFileName, SYS_DESTROY)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\n\t  Syslog File Table Info cannot be set to Destroy\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogSetFwdTable                                      */
/*                                                                           */
/* Description      : This function is invoked to add
                                        particular entry in Forwardtable     */
/*                                                                           */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID,Priority,Address type,Address                                                  ,Port and Transport type                               */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SysLogSetFwdTable (tCliHandle CliHandle, UINT4 u4Priority, UINT4 u4Address_Type,
                   UINT1 *au1IpAddress, UINT4 u4Port, UINT4 u4Trans_Type)
{

    INT4                i4status = SYSLOG_ZERO;
    UINT4               u4ErrorCode = SYSLOG_ZERO;
    UINT1               au1Ip6Addr[IPVX_MAX_INET_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE FsSyslogIpAddr;

    MEMSET (au1Ip6Addr, SYSLOG_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (&FsSyslogIpAddr, SYSLOG_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (u4Address_Type == IPVX_ADDR_FMLY_IPV4)
    {
        FsSyslogIpAddr.pu1_OctetList = au1IpAddress;
        FsSyslogIpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;

    }
    else if (u4Address_Type == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (au1Ip6Addr, au1IpAddress, IPVX_IPV6_ADDR_LEN);
        FsSyslogIpAddr.pu1_OctetList = au1Ip6Addr;
        FsSyslogIpAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
    }
    else
    {
        FsSyslogIpAddr.pu1_OctetList = au1IpAddress;
        FsSyslogIpAddr.i4_Length = (INT4) STRLEN (au1IpAddress);
        /* Currently only V4 and V6 address families are supported */
    }

    if (u4Address_Type != IPVX_DNS_FAMILY)
    {
        SYSLOG_INET_HTONL (FsSyslogIpAddr.pu1_OctetList);
    }

    if (nmhGetFsSyslogFwdRowStatus
        (u4Priority, u4Address_Type, &FsSyslogIpAddr,
         &i4status) == SNMP_SUCCESS)
    {

        if (i4status == SYS_ACTIVE)
        {
            if (nmhTestv2FsSyslogFwdRowStatus (&u4ErrorCode, u4Priority,
                                               u4Address_Type, &FsSyslogIpAddr,
                                               SYS_NOT_IN_SERVICE) ==
                SNMP_SUCCESS)
            {
                if (nmhSetFsSyslogFwdRowStatus
                    (u4Priority, u4Address_Type, &FsSyslogIpAddr,
                     SYS_NOT_IN_SERVICE) == SNMP_FAILURE)
                {

                    return CLI_FAILURE;
                }
            }
            else
            {
                return CLI_FAILURE;
            }

        }
    }

    else
    {
        if (nmhTestv2FsSyslogFwdRowStatus (&u4ErrorCode, u4Priority,
                                           u4Address_Type, &FsSyslogIpAddr,
                                           SYS_CREATE_AND_WAIT) == SNMP_SUCCESS)
        {

            if ((nmhSetFsSyslogFwdRowStatus
                 (u4Priority, u4Address_Type, &FsSyslogIpAddr,
                  SYS_CREATE_AND_WAIT)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\n\t  Syslog Fwd Table Information Cannot be Added\n");
                return CLI_FAILURE;
            }
        }
        else
        {
            if (u4ErrorCode != SNMP_ERR_NO_CREATION)
            {
                CliPrintf (CliHandle,
                           "\n\t  Syslog Table Information Cannot be Added. May be due to longer length !\n");
                return CLI_FAILURE;
            }
        }

    }

    if (nmhTestv2FsSyslogFwdPort
        (&u4ErrorCode, u4Priority, u4Address_Type, &FsSyslogIpAddr,
         u4Port) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsSyslogFwdPort
        (u4Priority, u4Address_Type, &FsSyslogIpAddr, u4Port) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsSyslogFwdTransType
        (&u4ErrorCode, u4Priority, u4Address_Type, &FsSyslogIpAddr,
         u4Trans_Type) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsSyslogFwdTransType
        (u4Priority, u4Address_Type, &FsSyslogIpAddr,
         u4Trans_Type) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /*Activate the Forward table entry status */
    if (nmhTestv2FsSyslogFwdRowStatus (&u4ErrorCode, u4Priority,
                                       u4Address_Type, &FsSyslogIpAddr,
                                       SYS_ACTIVE) == SNMP_SUCCESS)
    {

        if (nmhSetFsSyslogFwdRowStatus
            (u4Priority, u4Address_Type, &FsSyslogIpAddr,
             SYS_ACTIVE) == SNMP_FAILURE)
        {

            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogShowFwdTable                                     */
/*                                                                           */
/* Description      : This function is invoked to display syslog's fwdtbl    */
/*                                                                           */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID                              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SysLogShowFwdTable (tCliHandle CliHandle)
{

    UINT4               u4TempIpv4 = SYSLOG_ZERO;
    INT4                i4Priority = SYSLOG_ZERO;
    INT4                i4AddrType = SYSLOG_ZERO;
    INT4                i4Port = SYSLOG_ZERO;
    INT4                i4TransType = SYSLOG_ZERO;
    CHR1                au1Ip6Addr[DNS_MAX_QUERY_LEN];
    CHR1               *pu1String = NULL;
    tSNMP_OCTET_STRING_TYPE *pu1IpAddr = NULL;

    /*Allocating memory */
    pu1IpAddr =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (DNS_MAX_QUERY_LEN);

    /* Sanity check */
    if ((pu1IpAddr == NULL))
    {
        return CLI_FAILURE;
    }

    MEMSET (pu1IpAddr->pu1_OctetList, SYSLOG_ZERO, DNS_MAX_QUERY_LEN);
    /* Get the First Index of the File Table */

    if (nmhGetFirstIndexFsSyslogFwdTable (&i4Priority, &i4AddrType, pu1IpAddr)
        == SNMP_FAILURE)
    {
        free_octetstring (pu1IpAddr);
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\nSyslog Forward Table Information\r\n");
    CliPrintf (CliHandle, "--------------------------------\r\n");
    CliPrintf (CliHandle, "\r\n%-10s%-15s%-40s%-7s%-5s\r\n",
               "Priority", "Address-Type", "IpAddress", "Port", "Trans-Type");
    CliPrintf (CliHandle, "\r\n%-10s%-15s%-40s%-7s%-5s\r\n",
               "--------", "------------", "---------", "----", "----------");

    do
    {

        MEMSET (au1Ip6Addr, 0, DNS_MAX_QUERY_LEN);
        if (nmhGetFsSyslogFwdPort (i4Priority, i4AddrType, pu1IpAddr, &i4Port)
            == SNMP_FAILURE)
        {
            free_octetstring (pu1IpAddr);
            return CLI_FAILURE;
        }

        if (nmhGetFsSyslogFwdTransType
            (i4Priority, i4AddrType, pu1IpAddr, &i4TransType) == SNMP_FAILURE)
        {
            free_octetstring (pu1IpAddr);
            return CLI_FAILURE;
        }

        CliPrintf (CliHandle, "\r\n%-10d", i4Priority);

        if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (&u4TempIpv4, (pu1IpAddr->pu1_OctetList),
                    IPVX_IPV4_ADDR_LEN);
            u4TempIpv4 = OSIX_HTONL (u4TempIpv4);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4TempIpv4);
            CliPrintf (CliHandle, "%-15s%-40s%-7d", "ipv4", pu1String, i4Port);
        }
        else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
        {

            MEMCPY (au1Ip6Addr, pu1IpAddr->pu1_OctetList, IPVX_IPV6_ADDR_LEN);
            CliPrintf (CliHandle, "%-15s%-40s%-7d", "ipv6",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr), i4Port);
        }
        else if (i4AddrType == IPVX_DNS_FAMILY)
        {
            MEMCPY (au1Ip6Addr, pu1IpAddr->pu1_OctetList, pu1IpAddr->i4_Length);
            CliPrintf (CliHandle, "%-15s%-40s%-7d", "host", (VOID *) au1Ip6Addr,
                       i4Port);
        }

        if (i4TransType == SYSLOG_UDP)
        {
            CliPrintf (CliHandle, "%-5s\r\n", "udp");
        }
        else if (i4TransType == SYSLOG_TCP)
        {
            CliPrintf (CliHandle, "%-5s\r\n", "tcp");
        }
        else if (i4TransType == SYSLOG_BEEP)
        {
            CliPrintf (CliHandle, "%-5s\r\n", "beep");
        }

    }
    while (nmhGetNextIndexFsSyslogFwdTable
           (i4Priority, &i4Priority, i4AddrType, &i4AddrType, pu1IpAddr,
            pu1IpAddr) == SNMP_SUCCESS);

    free_octetstring (pu1IpAddr);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                       */
/* Function Name    : SysLogDelFwdTableEntry                                 */
/*                                                                           */
/* Description      : This function is invoked to delete
                                        particular entry in Fwdtablei        */
/*                                                                           */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID                              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SysLogDelFwdTable (tCliHandle CliHandle, UINT4 u4Priority, UINT4 u4Address_Type,
                   UINT1 *au1IpAddress)
{
    UINT4               u4ErrCode = SYSLOG_ZERO;
    UINT1               au1Ip6Addr[IPVX_MAX_INET_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE FsSyslogIpAddr;

    MEMSET (au1Ip6Addr, SYSLOG_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (&FsSyslogIpAddr, SYSLOG_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (u4Address_Type == IPVX_ADDR_FMLY_IPV4)
    {
        FsSyslogIpAddr.pu1_OctetList = au1IpAddress;
        FsSyslogIpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;

    }
    else if (u4Address_Type == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (au1Ip6Addr, au1IpAddress, IPVX_IPV6_ADDR_LEN);
        FsSyslogIpAddr.pu1_OctetList = au1Ip6Addr;
        FsSyslogIpAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
    }
    else
    {
        FsSyslogIpAddr.pu1_OctetList = au1IpAddress;
        FsSyslogIpAddr.i4_Length = (INT4) STRLEN (au1IpAddress);
    }

    if (u4Address_Type != IPVX_DNS_FAMILY)
    {
        SYSLOG_INET_HTONL (FsSyslogIpAddr.pu1_OctetList);
    }

    if (nmhTestv2FsSyslogFwdRowStatus (&u4ErrCode, u4Priority, u4Address_Type,
                                       &FsSyslogIpAddr,
                                       SYS_DESTROY) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "%% Syslog Fwd Table Info cannot be set to Destroy\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsSyslogFwdRowStatus (u4Priority, u4Address_Type,
                                    &FsSyslogIpAddr,
                                    SYS_DESTROY) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\n\t  Syslog Fwd Table Information Cannot be Deleted\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogSetMailTable                                     */
/*                                                                           */
/* Description      : This function is invoked to add
                                        particular entry in Mail   table     */
/*                                                                           */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID,Priority,Address type,Address                                                  and mail-id                                            */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SysLogSetMailTable (tCliHandle CliHandle, UINT4 u4Priority,
                    UINT4 u4Address_Type, UINT1 *au1IpAddress,
                    UINT1 *au1RexMailId, UINT1 *pu1UserName, UINT1 *pu1Passwd)
{

    INT4                i4status = SYSLOG_ZERO;
    UINT4               u4ErrorCode = SYSLOG_ZERO;
    UINT1               au1Ip6Addr[IPVX_MAX_INET_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE FsSyslogIpAddr;
    tSNMP_OCTET_STRING_TYPE FsSyslogRxMailId;
    tSNMP_OCTET_STRING_TYPE FsSmtpUserName;
    tSNMP_OCTET_STRING_TYPE FsSmtpPassword;

    MEMSET (au1Ip6Addr, SYSLOG_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (&FsSyslogIpAddr, SYSLOG_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&FsSmtpUserName, SYSLOG_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&FsSmtpPassword, SYSLOG_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

    FsSyslogRxMailId.pu1_OctetList = au1RexMailId;
    FsSyslogRxMailId.i4_Length = STRLEN (au1RexMailId);

    if (u4Address_Type == IPVX_ADDR_FMLY_IPV4)
    {
        FsSyslogIpAddr.pu1_OctetList = au1IpAddress;
        FsSyslogIpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;

    }
    else if (u4Address_Type == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (au1Ip6Addr, au1IpAddress, IPVX_IPV6_ADDR_LEN);
        FsSyslogIpAddr.pu1_OctetList = au1Ip6Addr;
        FsSyslogIpAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
    }
    else
    {
        FsSyslogIpAddr.pu1_OctetList = au1IpAddress;
        FsSyslogIpAddr.i4_Length = (INT4) STRLEN (au1IpAddress);
    }

    if (pu1UserName != 0)
    {
        FsSmtpUserName.pu1_OctetList = pu1UserName;
        FsSmtpUserName.i4_Length = STRLEN (pu1UserName);

        FsSmtpPassword.pu1_OctetList = pu1Passwd;
        FsSmtpPassword.i4_Length = STRLEN (pu1Passwd);
    }

    if (u4Address_Type != IPVX_DNS_FAMILY)
    {
        SYSLOG_INET_HTONL (FsSyslogIpAddr.pu1_OctetList);
    }

    if (nmhGetFsSyslogMailRowStatus (u4Priority, u4Address_Type,
                                     &FsSyslogIpAddr,
                                     &i4status) == SNMP_SUCCESS)
    {
        if (i4status == SYS_ACTIVE)
        {
            if (nmhTestv2FsSyslogMailRowStatus
                (&u4ErrorCode, u4Priority, u4Address_Type, &FsSyslogIpAddr,
                 SYS_NOT_IN_SERVICE) == SNMP_SUCCESS)
            {
                if (nmhSetFsSyslogMailRowStatus
                    (u4Priority, u4Address_Type, &FsSyslogIpAddr,
                     SYS_NOT_IN_SERVICE) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
        }
    }
    else
    {
        if (nmhTestv2FsSyslogMailRowStatus
            (&u4ErrorCode, u4Priority, u4Address_Type, &FsSyslogIpAddr,
             SYS_CREATE_AND_WAIT) == SNMP_SUCCESS)
        {
            if ((nmhSetFsSyslogMailRowStatus
                 (u4Priority, u4Address_Type, &FsSyslogIpAddr,
                  SYS_CREATE_AND_WAIT)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\n\t  Syslog Mail Table Information Cannot be Added");
                return CLI_FAILURE;
            }
        }
    }

    /*Test and set the Mail-id */

    if (nmhTestv2FsSyslogRxMailId (&u4ErrorCode, u4Priority, u4Address_Type,
                                   &FsSyslogIpAddr,
                                   &FsSyslogRxMailId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (FsSmtpUserName.i4_Length != 0)
    {
        if (nmhTestv2FsSyslogMailServUserName (&u4ErrorCode, u4Priority,
                                               u4Address_Type, &FsSyslogIpAddr,
                                               &FsSmtpUserName) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhTestv2FsSyslogMailServPassword (&u4ErrorCode, u4Priority,
                                               u4Address_Type, &FsSyslogIpAddr,
                                               &FsSmtpPassword) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (nmhSetFsSyslogRxMailId (u4Priority, u4Address_Type, &FsSyslogIpAddr,
                                &FsSyslogRxMailId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (FsSmtpUserName.i4_Length != 0)
    {
        if (nmhSetFsSyslogMailServUserName (u4Priority, u4Address_Type,
                                            &FsSyslogIpAddr, &FsSmtpUserName)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsSyslogMailServPassword (u4Priority, u4Address_Type,
                                            &FsSyslogIpAddr, &FsSmtpPassword)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    /*Activate the Mail table entry status */
    if (nmhTestv2FsSyslogMailRowStatus
        (&u4ErrorCode, u4Priority, u4Address_Type, &FsSyslogIpAddr,
         SYS_ACTIVE) == SNMP_SUCCESS)
    {
        if (nmhSetFsSyslogMailRowStatus
            (u4Priority, u4Address_Type, &FsSyslogIpAddr,
             SYS_ACTIVE) == SNMP_FAILURE)
        {

            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogShowMailTable                                    */
/*                                                                           */
/* Description      : This function is invoked to display syslog's Mailtbl   */
/*                                                                           */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID                              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SysLogShowMailTable (tCliHandle CliHandle)
{
    UINT4               u4TempIpv4 = SYSLOG_ZERO;
    INT4                i4Priority = SYSLOG_ZERO;
    INT4                i4AddrType = SYSLOG_ZERO;
    UINT1               au1UserName[SMTP_MAX_STRING];
    CHR1                au1Ip6Addr[DNS_MAX_QUERY_LEN];
    CHR1               *pu1String = NULL;
    tSNMP_OCTET_STRING_TYPE *pu1IpAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pu1RxMailId = NULL;
    tSNMP_OCTET_STRING_TYPE UserName;

    UserName.pu1_OctetList = au1UserName;
    /*Allocating memory */
    pu1IpAddr =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (DNS_MAX_QUERY_LEN);
    /* Sanity check */
    if ((pu1IpAddr == NULL))
    {
        return CLI_FAILURE;
    }

    pu1RxMailId =
        (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (SYSLOG_RXMAILID_LENGTH);
    /* Sanity check */
    if ((pu1RxMailId == NULL))
    {
        free_octetstring (pu1IpAddr);
        return CLI_FAILURE;
    }

    MEMSET (pu1IpAddr->pu1_OctetList, SYSLOG_ZERO, DNS_MAX_QUERY_LEN);
    MEMSET (pu1RxMailId->pu1_OctetList, SYSLOG_ZERO, SYSLOG_RXMAILID_LENGTH);
    /* Get the First Index of the File Table */

    if (nmhGetFirstIndexFsSyslogMailTable (&i4Priority, &i4AddrType, pu1IpAddr)
        == SNMP_FAILURE)
    {
        free_octetstring (pu1IpAddr);
        free_octetstring (pu1RxMailId);
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\n Syslog Mail Table Information");
    CliPrintf (CliHandle, "\n ----------------------------\r\n");

    CliPrintf (CliHandle,
               "\nPriority  Address-Type   IpAddress       Receiver Mail-Id  UserName \r\n");
    CliPrintf (CliHandle,
               "--------  ------------   ---------       ----------------  -------- \n");

    do
    {

        if (nmhGetFsSyslogRxMailId
            (i4Priority, i4AddrType, pu1IpAddr, pu1RxMailId) == SNMP_FAILURE)
        {
            free_octetstring (pu1IpAddr);
            free_octetstring (pu1RxMailId);
            return CLI_FAILURE;
        }

        MEMSET (au1UserName, 0, SMTP_MAX_STRING);
        MEMSET (au1Ip6Addr, 0, DNS_MAX_QUERY_LEN);
        if (nmhGetFsSyslogMailServUserName
            (i4Priority, i4AddrType, pu1IpAddr, &UserName) == SNMP_FAILURE)
        {
            free_octetstring (pu1IpAddr);
            free_octetstring (pu1RxMailId);
            return CLI_FAILURE;
        }

        CliPrintf (CliHandle, "\n%d ", i4Priority);

        if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (&u4TempIpv4, (pu1IpAddr->pu1_OctetList),
                    IPVX_IPV4_ADDR_LEN);
            u4TempIpv4 = OSIX_HTONL (u4TempIpv4);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4TempIpv4);

            CliPrintf (CliHandle, "\t  ipv4 \t\t %s  %s  %s \n", pu1String,
                       pu1RxMailId->pu1_OctetList, UserName.pu1_OctetList);
        }
        else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            MEMCPY (au1Ip6Addr, pu1IpAddr->pu1_OctetList, IPVX_IPV6_ADDR_LEN);
            CliPrintf (CliHandle, "\t  ipv6 \t\t %s  %s  %s \n",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr),
                       pu1RxMailId->pu1_OctetList, UserName.pu1_OctetList);

        }
        else if (i4AddrType == IPVX_DNS_FAMILY)
        {
            MEMCPY (au1Ip6Addr, pu1IpAddr->pu1_OctetList, pu1IpAddr->i4_Length);
            CliPrintf (CliHandle, "\t  host \t\t %s  %s  %s \n",
                       au1Ip6Addr, pu1RxMailId->pu1_OctetList,
                       UserName.pu1_OctetList);
        }

        MEMSET (pu1RxMailId->pu1_OctetList, SYSLOG_ZERO,
                STRLEN (pu1RxMailId->pu1_OctetList));

    }
    while (nmhGetNextIndexFsSyslogMailTable
           (i4Priority, &i4Priority, i4AddrType, &i4AddrType, pu1IpAddr,
            pu1IpAddr) == SNMP_SUCCESS);

    free_octetstring (pu1IpAddr);
    free_octetstring (pu1RxMailId);
    return CLI_SUCCESS;

}

/******************************************************************************/
/*                                                                           */
/* Function Name    : SysLogDelMailTable                                */
/*                                                                           */
/* Description      : This function is invoked to delete
                                        particular entry in Fwdtable        */
/*                                                                           */
/*                                                                           */
/* Input Parameters : CliHandle - CliContext ID                              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
SysLogDelMailTable (tCliHandle CliHandle, UINT4 u4Priority,
                    UINT4 u4Address_Type, UINT1 *au1IpAddress)
{

    UINT4               u4ErrCode = SYSLOG_ZERO;
    UINT1               au1Ip6Addr[IPVX_MAX_INET_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE FsSyslogIpAddr;

    MEMSET (au1Ip6Addr, SYSLOG_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (&FsSyslogIpAddr, SYSLOG_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (u4Address_Type == IPVX_ADDR_FMLY_IPV4)
    {
        FsSyslogIpAddr.pu1_OctetList = au1IpAddress;
        FsSyslogIpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;

    }
    else if (u4Address_Type == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (au1Ip6Addr, au1IpAddress, IPVX_IPV6_ADDR_LEN);
        FsSyslogIpAddr.pu1_OctetList = au1Ip6Addr;
        FsSyslogIpAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
    }
    else
    {
        FsSyslogIpAddr.pu1_OctetList = au1IpAddress;
        FsSyslogIpAddr.i4_Length = (INT4) STRLEN (au1IpAddress);
    }
    if (u4Address_Type != IPVX_DNS_FAMILY)
    {
        SYSLOG_INET_HTONL (FsSyslogIpAddr.pu1_OctetList);
    }
    if (nmhTestv2FsSyslogMailRowStatus (&u4ErrCode, u4Priority,
                                        u4Address_Type, &FsSyslogIpAddr,
                                        SYS_DESTROY) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\t\t%%syslog Mail Table Info Row Status cannot be set Destroy");
        return CLI_FAILURE;
    }
    if (nmhSetFsSyslogMailRowStatus (u4Priority, u4Address_Type,
                                     &FsSyslogIpAddr,
                                     SYS_DESTROY) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\n\t  Syslog Mail Table Info Cannot be deleted");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*************************************************************************************/
/*                                                                                   */
/* Function Name    : SysLogSmtpSetAuthentication                                    */
/*                                                                                   */
/* Description      : This function is invoked to set smtp authentication            */
/*                                                                                   */
/*                                                                                   */
/* Input Parameters : CliHandle - Cli Context ID                                     */
/*                    i4Port - Syslog Port                                           */
/*                                                                                   */
/* Output Parameters: None                                                           */
/*                                                                                   */
/* Return Value     : CLI_SUCCESS/CLI_FAILURE                                        */
/*************************************************************************************/

INT4
SysLogSmtpSetAuthentication (tCliHandle CliHandle, INT4 i4Authenticate)
{

    UINT4               u4ErrorCode;
    if (nmhTestv2FsSyslogSmtpAuthMethod (&u4ErrorCode, i4Authenticate) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsSyslogSmtpAuthMethod (i4Authenticate) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }
    CliPrintf (CliHandle, "\r%% Unable to set the smtp authentication\r\n");
    return (CLI_FAILURE);
}

/*************************************************************************************/
/*                                                                                   */
/* Function Name    : SysLogSnmpSetTrap                                              */
/*                                                                                   */
/* Description      : This function is used to set SNMP trap as enable/disable       */
/*                    when Syslog server down status                                 */
/*                                                                                   */
/*                                                                                   */
/* Input Parameters : CliHandle - Cli Context ID                                     */
/*                    i4Port - Syslog Port                                           */
/*                                                                                   */
/* Output Parameters: None                                                           */
/*                                                                                   */
/* Return Value     : CLI_SUCCESS/CLI_FAILURE                                        */
/*************************************************************************************/

INT4
SysLogSnmpSetTrap (tCliHandle CliHandle, INT4 i4SnmpTrapValue)
{

    UINT4               u4ErrorCode;
    if (nmhTestv2FsSyslogServerUpDownTrap (&u4ErrorCode, i4SnmpTrapValue) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsSyslogServerUpDownTrap (i4SnmpTrapValue) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }
    CliPrintf (CliHandle, "\r%% Unable to set the snmp trap\r\n");
    return (CLI_FAILURE);
}

#endif /* __SYSLGCLI_C__ */
