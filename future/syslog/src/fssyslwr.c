/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssyslwr.c,v 1.6
*
* Description: Protocol Mid Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fssysllw.h"
# include  "fssyslwr.h"
# include  "fssysldb.h"

VOID
RegisterFSSYSL ()
{
    SNMPRegisterMib (&fssyslOID, &fssyslEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fssyslOID, (const UINT1 *) "fssyslg");
}

VOID
UnRegisterFSSYSL ()
{
    SNMPUnRegisterMib (&fssyslOID, &fssyslEntry);
    SNMPDelSysorEntry (&fssyslOID, (const UINT1 *) "fssyslg");
}

INT4
FsSyslogLoggingGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogLogging (&(pMultiData->i4_SLongValue)));
}

INT4
FsSyslogTimeStampGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogTimeStamp (&(pMultiData->i4_SLongValue)));
}

INT4
FsSyslogConsoleLogGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogConsoleLog (&(pMultiData->i4_SLongValue)));
}

INT4
FsSyslogSysBuffersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogSysBuffers (&(pMultiData->i4_SLongValue)));
}

INT4
FsSyslogClearLogGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogClearLog (&(pMultiData->i4_SLongValue)));
}

INT4
FsSyslogLoggingSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogLogging (pMultiData->i4_SLongValue));
}

INT4
FsSyslogTimeStampSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogTimeStamp (pMultiData->i4_SLongValue));
}

INT4
FsSyslogConsoleLogSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogConsoleLog (pMultiData->i4_SLongValue));
}

INT4
FsSyslogSysBuffersSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogSysBuffers (pMultiData->i4_SLongValue));
}

INT4
FsSyslogClearLogSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogClearLog (pMultiData->i4_SLongValue));
}

INT4
FsSyslogLoggingTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogLogging (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSyslogTimeStampTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogTimeStamp (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSyslogConsoleLogTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogConsoleLog (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSyslogSysBuffersTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogSysBuffers (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSyslogClearLogTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogClearLog (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSyslogLoggingDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogLogging (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSyslogTimeStampDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogTimeStamp
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSyslogConsoleLogDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogConsoleLog
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSyslogSysBuffersDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogSysBuffers
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSyslogClearLogDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogClearLog (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsSyslogConfigTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsSyslogConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsSyslogConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsSyslogConfigLogLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSyslogConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSyslogConfigLogLevel (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsSyslogConfigLogLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsSyslogConfigLogLevel (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsSyslogConfigLogLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsSyslogConfigLogLevel (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsSyslogConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSyslogFacilityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogFacility (&(pMultiData->i4_SLongValue)));
}

INT4
FsSyslogRoleGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogRole (&(pMultiData->i4_SLongValue)));
}

INT4
FsSyslogLogFileGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogLogFile (&(pMultiData->i4_SLongValue)));
}

INT4
FsSyslogMailGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogMail (&(pMultiData->i4_SLongValue)));
}

INT4
FsSyslogProfileGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogProfile (&(pMultiData->i4_SLongValue)));
}

INT4
FsSyslogRelayPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogRelayPort (&(pMultiData->i4_SLongValue)));
}

INT4
FsSyslogRelayTransTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogRelayTransType (&(pMultiData->i4_SLongValue)));
}

INT4
FsSyslogFileNameOneGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogFileNameOne (pMultiData->pOctetStrValue));
}

INT4
FsSyslogFileNameTwoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogFileNameTwo (pMultiData->pOctetStrValue));
}

INT4
FsSyslogFileNameThreeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogFileNameThree (pMultiData->pOctetStrValue));
}

INT4
FsSyslogFacilitySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogFacility (pMultiData->i4_SLongValue));
}

INT4
FsSyslogRoleSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogRole (pMultiData->i4_SLongValue));
}

INT4
FsSyslogLogFileSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogLogFile (pMultiData->i4_SLongValue));
}

INT4
FsSyslogMailSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogMail (pMultiData->i4_SLongValue));
}

INT4
FsSyslogProfileSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogProfile (pMultiData->i4_SLongValue));
}

INT4
FsSyslogRelayPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogRelayPort (pMultiData->i4_SLongValue));
}

INT4
FsSyslogRelayTransTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogRelayTransType (pMultiData->i4_SLongValue));
}

INT4
FsSyslogFileNameOneSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogFileNameOne (pMultiData->pOctetStrValue));
}

INT4
FsSyslogFileNameTwoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogFileNameTwo (pMultiData->pOctetStrValue));
}

INT4
FsSyslogFileNameThreeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogFileNameThree (pMultiData->pOctetStrValue));
}

INT4
FsSyslogFacilityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogFacility (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSyslogRoleTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogRole (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSyslogLogFileTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogLogFile (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSyslogMailTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogMail (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSyslogProfileTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogProfile (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSyslogRelayPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogRelayPort (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSyslogRelayTransTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogRelayTransType
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSyslogFileNameOneTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogFileNameOne
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsSyslogFileNameTwoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogFileNameTwo
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsSyslogFileNameThreeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogFileNameThree
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsSyslogFacilityDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogFacility (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSyslogRoleDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogRole (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSyslogLogFileDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogLogFile (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSyslogMailDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogMail (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSyslogProfileDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogProfile (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSyslogRelayPortDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogRelayPort
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSyslogRelayTransTypeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogRelayTransType
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSyslogFileNameOneDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogFileNameOne
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSyslogFileNameTwoDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogFileNameTwo
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSyslogFileNameThreeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogFileNameThree
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsSyslogFileTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsSyslogFileTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsSyslogFileTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsSyslogFileRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSyslogFileTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSyslogFileRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsSyslogFileRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsSyslogFileRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsSyslogFileSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogFileSize (&(pMultiData->i4_SLongValue)));
}

INT4
FsSyslogFileSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogFileSize (pMultiData->i4_SLongValue));
}

INT4
FsSyslogFileRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsSyslogFileRowStatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            pOctetStrValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsSyslogFileTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogFileTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSyslogFileSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogFileSize (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSyslogFileSizeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogFileSize (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSyslogServerUpDownTrapGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogServerUpDownTrap (&(pMultiData->i4_SLongValue)));
}

INT4
FsSyslogServerUpDownTrapSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogServerUpDownTrap (pMultiData->i4_SLongValue));
}

INT4
FsSyslogServerUpDownTrapTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogServerUpDownTrap
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSyslogServerUpDownTrapDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogServerUpDownTrap
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSyslogLogSrvAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogLogSrvAddr (&(pMultiData->u4_ULongValue)));
}

INT4
FsSyslogLogNoLogServerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogLogNoLogServer (&(pMultiData->i4_SLongValue)));
}

INT4
FsSyslogLogSrvAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogLogSrvAddr (pMultiData->u4_ULongValue));
}

INT4
FsSyslogLogNoLogServerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogLogNoLogServer (pMultiData->i4_SLongValue));
}

INT4
FsSyslogLogSrvAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogLogSrvAddr (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsSyslogLogNoLogServerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogLogNoLogServer
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSyslogLogSrvAddrDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogLogSrvAddr
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSyslogLogNoLogServerDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogLogNoLogServer
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsSyslogFwdTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsSyslogFwdTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsSyslogFwdTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsSyslogFwdPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSyslogFwdTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSyslogFwdPort (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   pMultiIndex->pIndex[2].pOctetStrValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FsSyslogFwdTransTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSyslogFwdTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSyslogFwdTransType (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiIndex->pIndex[2].pOctetStrValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsSyslogFwdRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSyslogFwdTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSyslogFwdRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiIndex->pIndex[2].pOctetStrValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsSyslogFwdPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsSyslogFwdPort (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   pMultiIndex->pIndex[2].pOctetStrValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FsSyslogFwdTransTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsSyslogFwdTransType (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiIndex->pIndex[2].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsSyslogFwdRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsSyslogFwdRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiIndex->pIndex[2].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsSyslogFwdPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsSyslogFwdPort (pu4Error,
                                      pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiIndex->pIndex[2].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsSyslogFwdTransTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsSyslogFwdTransType (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiIndex->pIndex[2].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsSyslogFwdRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsSyslogFwdRowStatus (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiIndex->pIndex[2].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsSyslogFwdTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogFwdTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSyslogSmtpSrvAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogSmtpSrvAddr (&(pMultiData->u4_ULongValue)));
}

INT4
FsSyslogSmtpRcvrMailIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogSmtpRcvrMailId (pMultiData->pOctetStrValue));
}

INT4
FsSyslogSmtpSenderMailIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogSmtpSenderMailId (pMultiData->pOctetStrValue));
}

INT4
FsSyslogSmtpSrvAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogSmtpSrvAddr (pMultiData->u4_ULongValue));
}

INT4
FsSyslogSmtpRcvrMailIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogSmtpRcvrMailId (pMultiData->pOctetStrValue));
}

INT4
FsSyslogSmtpSenderMailIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogSmtpSenderMailId (pMultiData->pOctetStrValue));
}

INT4
FsSyslogSmtpSrvAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogSmtpSrvAddr (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsSyslogSmtpRcvrMailIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogSmtpRcvrMailId
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsSyslogSmtpSenderMailIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogSmtpSenderMailId
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsSyslogSmtpSrvAddrDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogSmtpSrvAddr
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSyslogSmtpRcvrMailIdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogSmtpRcvrMailId
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSyslogSmtpSenderMailIdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogSmtpSenderMailId
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsSyslogMailTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsSyslogMailTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsSyslogMailTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsSyslogRxMailIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSyslogMailTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSyslogRxMailId (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiIndex->pIndex[2].pOctetStrValue,
                                    pMultiData->pOctetStrValue));

}

INT4
FsSyslogMailRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSyslogMailTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSyslogMailRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiIndex->pIndex[2].pOctetStrValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsSyslogMailServUserNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSyslogMailTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSyslogMailServUserName
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsSyslogMailServPasswordGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSyslogMailTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSyslogMailServPassword
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsSyslogRxMailIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsSyslogRxMailId (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiIndex->pIndex[2].pOctetStrValue,
                                    pMultiData->pOctetStrValue));

}

INT4
FsSyslogMailRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsSyslogMailRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiIndex->pIndex[2].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsSyslogMailServUserNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsSyslogMailServUserName
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsSyslogMailServPasswordSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsSyslogMailServPassword
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsSyslogRxMailIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsSyslogRxMailId (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].pOctetStrValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FsSyslogMailRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsSyslogMailRowStatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[2].
                                            pOctetStrValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsSyslogMailServUserNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsSyslogMailServUserName (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[2].
                                               pOctetStrValue,
                                               pMultiData->pOctetStrValue));

}

INT4
FsSyslogMailServPasswordTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsSyslogMailServPassword (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[2].
                                               pOctetStrValue,
                                               pMultiData->pOctetStrValue));

}

INT4
FsSyslogMailTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogMailTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSyslogSmtpAuthMethodGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSyslogSmtpAuthMethod (&(pMultiData->i4_SLongValue)));
}

INT4
FsSyslogSmtpAuthMethodSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSyslogSmtpAuthMethod (pMultiData->i4_SLongValue));
}

INT4
FsSyslogSmtpAuthMethodTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSyslogSmtpAuthMethod
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSyslogSmtpAuthMethodDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSyslogSmtpAuthMethod
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
