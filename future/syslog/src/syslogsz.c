/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: syslogsz.c,v 1.4 2013/11/29 11:04:15 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _SYSLOGSZ_C
#include "syslginc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
SyslogSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < SYSLOG_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsSYSLOGSizingParams[i4SizingId].u4StructSize,
                              FsSYSLOGSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(SYSLOGMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            SyslogSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
SyslogSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsSYSLOGSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, SYSLOGMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
SyslogSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < SYSLOG_MAX_SIZING_ID; i4SizingId++)
    {
        if (SYSLOGMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (SYSLOGMemPoolIds[i4SizingId]);
            SYSLOGMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
