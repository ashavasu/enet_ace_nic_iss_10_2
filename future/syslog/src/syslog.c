/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: syslog.c,v 1.85 2017/12/28 10:40:16 siva Exp $
 *
 * Description: Action routines for Logging System Events and
 *              cli commands.
 *
 *******************************************************************/

#include "syslginc.h"
#include "slogglob.h"
#include "slogtrc.h"
#include "fssocket.h"
#include "syslgcli.h"
#include "sysclipt.h"
#include "fssyslcli.h"
#include "iss.h"
#include "ipv6.h"
#include "msr.h"
#include "snmputil.h"
#include "fssocket.h"
#include "beepsrv.h"
#include "utilipvx.h"
#include "beepclt.h"
#include "cfanp.h"
#include "nputil.h"
#ifdef SNMP_3_WANTED
#include "fssyslg.h"
#endif
#include "fips.h"
#include <time.h>
tBpCltSyslogMsg     gsBpCltSyslogMsg;
UINT1               gau1SysMess[SYSLOG_MAX_STRING];
#ifdef SNMP_3_WANTED
static INT1         ai1TempBuffer[257];
#endif
extern INT4         gi4MibResStatus;
/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogInit                                             */
/*                                                                           */
/* Description      : This function is invoked to initialize the command log */
/*                    data structures. This function should be invoked only  */
/*                    during system startup.                                 */
/*                                                                           */
/* Input Parameters : None                                                   */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  SYSLOG_SUCCESS - On successful initialization.        */
/*                     SYSLOG_MEMALLOC_FAILURE - When initialization fails   */
/*****************************************************************************/

INT4
SysLogInit ()
{
    UINT4               u4Count;

    gu4SysLogInit = SYSLOG_ENABLE;
    gu4CliSysLogLevel = DEF_SYSLOG_LEVEL;

    if (OsixCreateSem
        ((const UINT1 *) SYSLOG_SEM_NAME, 1, OSIX_WAIT,
         &(SMTP_SEM_ID)) != OSIX_SUCCESS)
    {
        gu4SysLogInit = SYSLOG_DISABLE;
        return (SYSLOG_OS_FAILURE);
    }

    /*MEMORY POOL CREATION */
    MEMSET (&gSysLogGlobalInfo, 0, sizeof (tSysLogGlobalInfo));
    TMO_SLL_Init (&(gSysLogGlobalInfo.sysFileTblList));
    TMO_SLL_Init (&(gSysLogGlobalInfo.sysFwdTblList));
    TMO_SLL_Init (&(gSysLogGlobalInfo.sysMailTblList));

    if (SysLogMemInit () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    for (u4Count = 1; u4Count <= SYSLOG_MAX_MODULES; ++u4Count)
    {
        asModInfo[u4Count].u4Status = SYSLOG_DISABLE;
    }

    /* Default Parameter settings for syslogs. Logging is enabled on console
     * Telnet session login is disabled. Timestamp option is enabled.
     */

    gsSysLogParams.u4SysLogStatus = SYSLOG_ENABLE;
    gsSysLogParams.u4NumBuffers = DEF_SYSLOG_MAX_BUFF;

    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        /* By default, the messages logged to syslog server is also logged
         * in console. But in FIPS mode, the console won't be available.
         * Hence logging in console is blocked in FIPS mode.
         */
        gsSysLogParams.u4LogConsole = SYSLOG_DISABLE;
    }
    else
    {
        gsSysLogParams.u4LogConsole = SYSLOG_ENABLE;
    }

    gsSysLogParams.u4LogTelnet = SYSLOG_DISABLE;
    gsSysLogParams.u4LogSession = SYSLOG_ZERO;
    gsSysLogParams.u4TimeStamp = SYSLOG_ENABLE;
    gsSysLogParams.u4LogServerIp = SYSLOG_ZERO;
    gsSysLogParams.u4Facility = DEF_SYSLOG_FACILITY;

    gsSysLogParams.u4MailOption = SYSLOG_DISABLE;
    gsSysLogParams.u4LocalStorage = SYSLOG_DISABLE;
    gsSysLogParams.u4SysLogRole = SYSLOG_DEVICE_ROLE;
    gsSysLogParams.u4SysLogPort = BSD_SYSLOG_PORT;
    gsSysLogParams.u4RelayTransType = SYSLOG_RELAY_UDP;
    gsSysLogParams.u4SysLogProfile = RSYSLOG_RAW_PROFILE;
    gsSysLogParams.i4Sockv4Id = MINUS_ONE;
    gsSysLogParams.i4Sockv6Id = MINUS_ONE;

    gsSysLogParams.u4TotalLogs = SYSLOG_ZERO;
    gsSysLogParams.u4ConsoleLogs = SYSLOG_ZERO;
    gsSysLogParams.u4SyslogLogs = SYSLOG_ZERO;
    gsSysLogParams.u4Overruns = SYSLOG_ZERO;
    gsSysLogParams.u4ClearLogs = SYSLOG_ZERO;
    gsSysLogParams.u4FilterLogs = SYSLOG_ZERO;
    gsSysLogParams.u4BufAllocs = SYSLOG_ZERO;
    gsSysLogParams.flashFpOne = NULL;
    gsSysLogParams.flashFpTwo = NULL;
    gsSysLogParams.flashFpThree = NULL;
    gsSysLogParams.u4SysLogTrcFlag = 0;

    for (u4Count = 0; u4Count < MAX_SYSLOG_USERS_LIMIT; ++u4Count)
    {
        if (SysLogSetMaxLogBuff (u4Count, DEF_SYSLOG_NUMCMD_BUFF) ==
            SYSLOG_MEMALLOC_FAILURE)
        {
            gu4SysLogInit = SYSLOG_DISABLE;
            return (SYSLOG_MEMALLOC_FAILURE);
        }
    }

    if (SysLogSetMaxLogBuff
        (MAX_SYSLOG_USERS_LIMIT,
         gsSysLogParams.u4NumBuffers) == SYSLOG_MEMALLOC_FAILURE)
    {
        gu4SysLogInit = SYSLOG_DISABLE;
        return (SYSLOG_MEMALLOC_FAILURE);
    }

    SysLogRegisterAllModules ();
    SysLogSmtpInit ();

#ifdef BEEP_SERVER_WANTED
    BeepServerRegister (BPSRV_SYSLOG_ID, SysLogFromBeepServer);
#endif

    return (SYSLOG_SUCCESS);
}

/******************************************************************************/
/*                                                                            */
/*  Function    : SysLogMemInit                                               */
/*                                                                            */
/*  Description : Creates the memory pool                                     */
/*                                                                            */
/*  Input       : None                                                        */
/*                                                                            */
/*  Output      : None                                                        */
/*                                                                            */
/*  Returns     : OSIX_SUCCESS or OSIX_FAILURE                                */
/*                                                                            */
/******************************************************************************/

INT4
SysLogMemInit (VOID)
{
    /* Dummy pointers for system sizing during run time */
    tSysQueMsgBlock    *pSysQueMsgBlock = NULL;
    tSysLogBuffUserBlock *pSysLogBuffUserBlock = NULL;

    UNUSED_PARAM (pSysQueMsgBlock);
    UNUSED_PARAM (pSysLogBuffUserBlock);

    if (SyslogSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gSysLogGlobalInfo.sysFilePoolId =
        SYSLOGMemPoolIds[MAX_SYSLOG_FILE_ENTRY_SIZING_ID];
    gSysLogGlobalInfo.sysFwdPoolId =
        SYSLOGMemPoolIds[MAX_SYSLOG_FWD_ENTRY_SIZING_ID];
    gSysLogGlobalInfo.sysMailPoolId =
        SYSLOGMemPoolIds[MAX_SYSLOG_MAIL_ENTRY_SIZING_ID];
    gSysLogGlobalInfo.sysQueMsgPoolId =
        SYSLOGMemPoolIds[MAX_SYSLOG_SMTPQ_DEPTH_SIZING_ID];
    gSysLogGlobalInfo.sysLogBuffPoolId =
        SYSLOGMemPoolIds[MAX_SYSLOG_LOG_BUFF_USERS_SIZING_ID];
    gSysLogGlobalInfo.SysLogUMemPoolId =
        SYSLOGMemPoolIds[MAX_SYSLOG_UINT_PKT_SIZING_ID];
    gSysLogGlobalInfo.SysLogCMemPoolId =
        SYSLOGMemPoolIds[MAX_SYSLOG_CHR_PKT_SIZING_ID];
    gSysLogGlobalInfo.SysLogTMemPoolId =
        SYSLOGMemPoolIds[MAX_SYSLOG_AUDIT_INFO_SIZING_ID];
    gSysLogGlobalInfo.SysLogStdbyPoolId =
        SYSLOGMemPoolIds[MAX_SYSLOG_STD_PKT_SIZING_ID];
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogAllocBuff                                        */
/*                                                                           */
/* Description      : This function is invoked to allocate log buffers for   */
/*                    CLI command logs and modules                           */
/*                                                                           */
/* Input Parameters : UINT4 u4UserIndex - User index given by MMI            */
/*                    UINT4 u4NumCmds - Maximum number of commands that can  */
/*                                      can be logged                        */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  SYSLOG_SUCCESS  - On successful allocation            */
/*                     SYSLOG_MEMALLOC_FAILURE  - When memory allocation     */
/*                                                fails                      */
/*****************************************************************************/

INT4
SysLogSetMaxLogBuff (UINT4 u4UserIndex, UINT4 u4NumLogs)
{
    tSysLogEntry       *pSysLogEntry;
    tSysLogBuff        *pLogBuff;
    tSysLogBuff        *pTempBuff;
    tSysLogBuff        *pUnwanted = NULL;
    tSysLogBuff        *pSaveBuff;
    tSysLogBuff        *pSavedLast = NULL;
    tSysLogBuff        *pDestBuff;
    UINT4               u4Count = 0;
    UINT4               u4UnwantedCount = 0;
    if (gu4SysLogInit == SYSLOG_DISABLE)
    {
        return (SYSLOG_MEMALLOC_FAILURE);
    }
    if ((u4NumLogs == 0) || (u4NumLogs > SYSLOG_MAX_BUFFERS))
    {
        return (SYSLOG_INVALID_VALUE);
    }

    if (OsixSemTake (SMTP_SEM_ID) != OSIX_SUCCESS)
    {
        return (SYSLOG_OS_FAILURE);
    }

    pSysLogEntry = &gaSysLogTbl[u4UserIndex];

    if (pSysLogEntry->pLogBuff != NULL)
    {
        /* Already memory is allocated. Try to see if new buffers can be 
         * allocated. Otherwise let us live with existing memory.
         */
        pLogBuff =
            (tSysLogBuff *) MemAllocMemBlk (gSysLogGlobalInfo.sysLogBuffPoolId);
        if (pLogBuff == NULL)
        {
            OsixSemGive (SMTP_SEM_ID);
            return (SYSLOG_MEMALLOC_FAILURE);
        }

        pSaveBuff = pLogBuff;

        for (u4Count = 0; u4Count < (u4NumLogs - 1); ++u4Count)
        {
            pLogBuff->pNext = (pLogBuff + 1);
            pLogBuff = pLogBuff + 1;
            MEMSET (pLogBuff->ai1LogStr, 0, SYSLOG_MAX_STRING);
        }
        pLogBuff->pNext = NULL;

        if (pSysLogEntry->u4MaxLogMsgs < u4NumLogs)
        {
            /* More buffers are being allocated compared to existing log buffers
             * for the user.
             */
            pTempBuff = pSysLogEntry->pHead;
        }
        else
        {
            /* Number of buffers is reduced. We need to retain the last 'n'
             * commands. Traverse the list to find out the number of elements.
             */
            u4Count = 1;
            for (pTempBuff = pSysLogEntry->pHead;
                 pTempBuff != pSysLogEntry->pLast; pTempBuff = pTempBuff->pNext)
            {
                u4Count++;
            }

            if (u4Count < u4NumLogs)
            {
                /* Since the total number of log buffers is less than the newly 
                 * allocated memory we can copy all the logs even though the 
                 * number of log buffers are reduced.
                 */
                pTempBuff = pSysLogEntry->pHead;
                pSavedLast = pSysLogEntry->pLast;
            }
            else
            {
                pTempBuff = pSysLogEntry->pHead;
                pUnwanted = pTempBuff;
                u4Count = u4Count - u4NumLogs;
                u4UnwantedCount = u4Count;
                while (u4Count != 0)
                {
                    pTempBuff = pTempBuff->pNext;
                    u4Count--;
                    if (gsSysLogParams.u4LogConsole == SYSLOG_ENABLE)
                    {
                        gsSysLogParams.u4ConsoleLogs--;
                    }
                }

            }
        }

        pDestBuff = pSaveBuff;
        pSysLogEntry->u4NumMsgLogs = 0;
        for (; (pTempBuff != NULL) && (pDestBuff != NULL);
             pTempBuff = pTempBuff->pNext)
        {
            STRNCPY (pDestBuff->ai1LogStr, pTempBuff->ai1LogStr,
                     STRLEN (pTempBuff->ai1LogStr));
            pDestBuff->ai1LogStr[STRLEN (pTempBuff->ai1LogStr)] = '\0';
            pSysLogEntry->pLast = pDestBuff;
            pDestBuff = pDestBuff->pNext;
            if (pSysLogEntry->u4NumMsgLogs < pSysLogEntry->u4MaxLogMsgs)
            {
                pSysLogEntry->u4NumMsgLogs++;
            }
            if (pTempBuff == pSavedLast)
            {
                break;
            }
        }
        if (pDestBuff != NULL)
        {
            if (u4UnwantedCount != 0)
            {
                pTempBuff = pUnwanted;
                while (u4UnwantedCount != 1)
                {
                    pTempBuff = pTempBuff->pNext;
                    u4UnwantedCount--;
                }
                pTempBuff->pNext = pDestBuff;
                pSysLogEntry->pFree = pUnwanted;
            }
            else
            {
                pSysLogEntry->pFree = pDestBuff;
            }
        }
        else
        {
            pSysLogEntry->pFree = NULL;
        }
        pSysLogEntry->pHead = pSaveBuff;

        MemReleaseMemBlock (gSysLogGlobalInfo.sysLogBuffPoolId,
                            (UINT1 *) pSysLogEntry->pLogBuff);
        pSysLogEntry->pLogBuff = pSaveBuff;

        gsSysLogParams.u4BufAllocs++;
    }
    else
    {
        /* Memory has not been allocated before at all for the logs */
        pLogBuff =
            (tSysLogBuff *) MemAllocMemBlk (gSysLogGlobalInfo.sysLogBuffPoolId);
        if (pLogBuff == NULL)
        {
            OsixSemGive (SMTP_SEM_ID);
            return (SYSLOG_MEMALLOC_FAILURE);
        }

        pSysLogEntry->pLogBuff = pLogBuff;
        for (u4Count = 0; u4Count < (u4NumLogs - 1); ++u4Count)
        {
            pLogBuff->pNext = (pLogBuff + 1);
            pLogBuff = pLogBuff + 1;
            MEMSET (pLogBuff->ai1LogStr, 0, SYSLOG_MAX_STRING);
        }
        pLogBuff->pNext = NULL;
        pSysLogEntry->pHead = pSysLogEntry->pLast = NULL;
        pSysLogEntry->pFree = pSysLogEntry->pLogBuff;
        pSysLogEntry->u4NumMsgLogs = 0;

    }

    pSysLogEntry->u4MaxLogMsgs = u4NumLogs;
    OsixSemGive (SMTP_SEM_ID);
    return (SYSLOG_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogMsg                                              */
/*                                                                           */
/* Description      : This function is invoked to log a system event.        */
/*                                                                           */
/* Input Parameters : UINT4 u4Level  - Log level                             */
/*                    UINT4 u4ModuleId - Module Identifier                   */
/*                    INT1 *pFormat - Message Given by user                  */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  None                                                  */
/*****************************************************************************/

VOID
SysLogMsg (UINT4 u4Level, UINT4 u4ModuleId, const CHR1 * pFormat, ...)
{
    va_list             VarArgList;
    UINT1              *pu1TempStr = NULL;
    INT4                i4RetVal;

    /* if pool id equals zero, syslog init was not called and 
     * mempool is not created and so returns*/
    if (gSysLogGlobalInfo.SysLogUMemPoolId == 0)
    {
        return;
    }
    pu1TempStr = MemAllocMemBlk (gSysLogGlobalInfo.SysLogUMemPoolId);

    if (pu1TempStr == NULL)
    {
        return;
    }
    MEMSET (pu1TempStr, 0, SYSLOG_MAX_STRING + SYSLOG_FIFTY);

    va_start (VarArgList, pFormat);

    vsprintf ((CHR1 *) pu1TempStr, pFormat, VarArgList);

    va_end (VarArgList);

    i4RetVal = SysLogCustCallBack (SYSLOG_MSG_EVT, u4Level,
                                   u4ModuleId, (const CHR1 *) pu1TempStr);

    va_start (VarArgList, pFormat);
    if (ISS_CUSTOM_CONTINUE_PROCESSING == i4RetVal)
    {
        SysLogStr (SYSLOG_ZERO, u4Level, u4ModuleId, pFormat, VarArgList);
    }
    va_end (VarArgList);
    MemReleaseMemBlock (gSysLogGlobalInfo.SysLogUMemPoolId, pu1TempStr);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogCmd                                              */
/*                                                                           */
/* Description      : This function is invoked to log a CLI command.         */
/*                                                                           */
/* Input Parameters : UINT4 u4Level  - Log level                             */
/*                    UINT4 u4UserId - User Identifier                       */
/*                    INT1 *pFormat - Message Given by user                  */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  None                                                  */
/*****************************************************************************/

VOID
SysLogCmd (UINT4 u4Level, UINT4 u4UserId, const CHR1 * pFormat, ...)
{

    va_list             VarArgList;

    va_start (VarArgList, pFormat);
    SysLogStr (u4UserId, u4Level, 0, pFormat, VarArgList);
    va_end (VarArgList);

}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogStr                                              */
/*                                                                           */
/* Description      : This function is invoked to log either a CLI command   */
/*                    or a system event.                                     */
/*                                                                           */
/* Input Parameters : UINT4 u4UserId - User Id given by MMI                  */
/*                    UINT4 u4Level  - Log level                             */
/*                    UINT4 u4ModuleId - Module Identifier                   */
/*                    INT1 *pFormat - Message Given by user                  */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  None                                                  */
/*****************************************************************************/

VOID
SysLogStr (UINT4 u4UserId, UINT4 u4Level, UINT4 u4ModuleId,
           const CHR1 * pFormat, va_list VarArgList)
{
    tSysLogEntry       *pSysLogEntry;
    tSysLogBuff        *pLogBuff;
    INT1               *pStr;
    UINT4               u4Len = SYSLOG_ZERO;
    UINT4               u4CurLen = SYSLOG_ZERO;
    UINT4               u4TempLen = SYSLOG_ZERO;
    UINT1              *pu1TempStr = NULL;
    UINT4               u4Priority = SYSLOG_ZERO;
    UINT4               u4HostNameLen = SYSLOG_ZERO;
    CHR1               *pu1String = NULL;
    UINT4               u4IpAddr = SYSLOG_ZERO;
    CHR1                au1MyHostName[80];
#ifdef POSIX
    INT4                i4rc = SYSLOG_ZERO;
#endif

    MEMSET (au1MyHostName, 0, 80);
    if (gsSysLogParams.u4SysLogStatus != SYSLOG_ENABLE)
        return;

    if (gu4SysLogInit != SYSLOG_ENABLE)
        return;

    if (u4ModuleId != 0)
    {
        /* Check if Logging is enabled for the module. */

        if (u4ModuleId > SYSLOG_MAX_MODULES)
            return;

        if (asModInfo[u4ModuleId].u4Status == SYSLOG_DISABLE)
            return;

        if (asModInfo[u4ModuleId].u4Level < u4Level)
        {
            gsSysLogParams.u4FilterLogs++;
            return;
        }

        u4UserId = MAX_SYSLOG_USERS_LIMIT;
    }
    else
    {
        /* Validate the user id */

        if (u4UserId >= MAX_SYSLOG_USERS_LIMIT)
            return;

        /* Check if Logging is enabled for the user. */

        if (gu4CliSysLogLevel < u4Level)
        {
            gsSysLogParams.u4FilterLogs++;
            return;
        }
    }

    if (OsixSemTake (SMTP_SEM_ID) != OSIX_SUCCESS)
    {
        return;
    }

    pSysLogEntry = &gaSysLogTbl[u4UserId];

    pLogBuff = pSysLogEntry->pFree;

    if (pLogBuff == NULL)
    {
        /* No Elements in the free list. We need to take up an element
         * from the log buffer list. Since the first element is the oldest
         * log we use that and add the same to the end of the list
         */

        pLogBuff = pSysLogEntry->pHead;
        if (pLogBuff->pNext != NULL)
        {
            /* Check if this a one element list in which case the next 
             * pointer of the first element will be NULL.
             */

            pSysLogEntry->pHead = pLogBuff->pNext;
        }
        pLogBuff->pNext = NULL;

        if ((pSysLogEntry->pLast != NULL) && (pSysLogEntry->pLast != pLogBuff))
        {
            pSysLogEntry->pLast->pNext = pLogBuff;
        }

        pSysLogEntry->pLast = pLogBuff;
    }
    else
    {
        /* There are elements in the free list. We get an element from the 
         * free list and add the same to the end.
         */

        pSysLogEntry->pFree = pLogBuff->pNext;
        pLogBuff->pNext = NULL;

        if (pSysLogEntry->pLast != NULL)
        {
            pSysLogEntry->pLast->pNext = pLogBuff;
        }
        pSysLogEntry->pLast = pLogBuff;

        if (pSysLogEntry->pHead == NULL)
        {
            pSysLogEntry->pHead = pLogBuff;
        }
    }

    if (pSysLogEntry->u4NumMsgLogs < pSysLogEntry->u4MaxLogMsgs)
    {
        pSysLogEntry->u4NumMsgLogs++;
    }
    gsSysLogParams.u4TotalLogs++;

    MEMSET (pLogBuff->ai1LogStr, 0, SYSLOG_MAX_STRING);

    pStr = pLogBuff->ai1LogStr;

    /* Priority is attached to Syslog Message */

    u4Priority = gsSysLogParams.u4Facility | u4Level;

    u4Len = SNPRINTF ((CHR1 *) pStr, SYSLOG_MAX_STRING, "<%u>", u4Priority);
    pStr = pStr + u4Len;

    /* TimeStamp is attached to the syslog message */

    if (gsSysLogParams.u4TimeStamp == SYSLOG_ENABLE)
    {
        SysLogGetTimeStr ((CHR1 *) pStr);
        pStr = pStr + STRLEN (pStr);
    }

    /* Host name is attached to the syslog message */
#ifdef POSIX
    i4rc = gethostname (au1MyHostName, 80);
    if (i4rc == SYSLOG_ZERO)
    {
        SYSLOG_DBG (SYSLOG_MGMT_TRC | SYSLOG_CONTROL_PATH_TRC,
                    "SysSendLog: Invalid Host name !!!\n");
    }
    u4HostNameLen =
        SNPRINTF ((CHR1 *) pStr, SYSLOG_MAX_STRING, "%s ", au1MyHostName);
#else
    u4HostNameLen = SNPRINTF ((CHR1 *) pStr, SYSLOG_MAX_STRING, "%s ", "ISS");
#endif
    pStr = pStr + u4HostNameLen;

    if (u4HostNameLen <= SYSLOG_ONE)
    {
        pStr = pStr - SYSLOG_ONE;
        u4IpAddr = IssGetIpAddrFromNvRam ();
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr);
        u4HostNameLen = SNPRINTF ((CHR1 *) pStr, SYSLOG_MAX_STRING, "%s ",
                                  pu1String);
        pStr = pStr + u4HostNameLen;

    }

    if (u4ModuleId != SYSLOG_ZERO)
    {
        pu1TempStr = MemAllocMemBlk (gSysLogGlobalInfo.SysLogUMemPoolId);
        if (pu1TempStr == NULL)
        {
            OsixSemGive (SMTP_SEM_ID);
            return;
        }

        MEMSET (pu1TempStr, 0, SYSLOG_MAX_STRING + SYSLOG_FIFTY);

        u4Len = SNPRINTF ((CHR1 *) pStr, SYSLOG_MAX_STRING, "%.32s: ",
                          asModInfo[u4ModuleId].au1Name);
        pStr = pStr + u4Len;
        u4CurLen = STRLEN (pLogBuff->ai1LogStr);

        vsprintf ((CHR1 *) pu1TempStr, pFormat, VarArgList);
        u4TempLen = STRLEN (pu1TempStr);

        if (u4CurLen + u4TempLen >= SYSLOG_MAX_STRING)
        {
            MEMCPY (pStr, pu1TempStr, (SYSLOG_MAX_STRING - u4CurLen - 1));
        }
        else
        {
            MEMCPY (pStr, pu1TempStr, u4TempLen);
        }

        if (gsSysLogParams.u4LogConsole == SYSLOG_ENABLE)
        {
            SYSLOG_DBG (ALL_FAILURE_TRC, (CHR1 *) pLogBuff->ai1LogStr);
            gsSysLogParams.u4ConsoleLogs++;
        }

        OsixSemGive (SMTP_SEM_ID);
        SysSendLog (u4Priority, pLogBuff->ai1LogStr);
        OsixSemTake (SMTP_SEM_ID);
        MemReleaseMemBlock (gSysLogGlobalInfo.SysLogUMemPoolId, pu1TempStr);

    }
    else
    {
        /* The sub-string should be either console or telnet */

        vsprintf ((CHR1 *) pStr, pFormat, VarArgList);
        OsixSemGive (SMTP_SEM_ID);
        SysSendLog (u4Priority, pLogBuff->ai1LogStr);
        OsixSemTake (SMTP_SEM_ID);
    }

    if (pSysLogEntry->u4NumMsgLogs == pSysLogEntry->u4MaxLogMsgs)
    {
        gsSysLogParams.u4Overruns++;
    }

    if (gsSysLogParams.u4MailOption == SYSLOG_ENABLE)
    {
        SysLogSendMailAlert (u4Priority, (UINT1 *) pLogBuff->ai1LogStr);
    }
    if ((gsSysLogParams.u4LocalStorage == SYSLOG_ENABLE)
        && (gi4MibResStatus != MIB_RESTORE_IN_PROGRESS))
    {
        SysLogStoreInFile (u4Priority, (UINT1 *) pLogBuff->ai1LogStr,
                           asModInfo[u4ModuleId].au1Name,
                           asModInfo[u4ModuleId].u4FileNo, u4ModuleId);
    }

    OsixSemGive (SMTP_SEM_ID);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name     : SysLogStoreInFile                                     */
/*                                                                           */
/* Description       : This function is invoked to get file name             */
/*                     from file table                                       */
/*                                                                           */
/* Input Parameters  : pu1LogBuf and priority                                */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : SYSLOG_SUCCESS - On successful opening of socket      */
/*                     SYSLOG_FILE_FAILURE - On failure to open a file       */
/*****************************************************************************/
INT4
SysLogStoreInFile (UINT4 u4Priority, UINT1 *pu1LogBuf, UINT1 *pu1ModName,
                   UINT4 u4FileNo, UINT4 u4ModuleId)
{
    tSysLogFileInfo    *pFileInfo = NULL;
    tSysLogFileInfo    *pnode = NULL;
    tAuditInfo         *pTempAudit = NULL;
    CHR1               *pu1Temp = NULL;

    pTempAudit =
        (tAuditInfo *) MemAllocMemBlk (gSysLogGlobalInfo.SysLogTMemPoolId);
    if (pTempAudit == NULL)
    {
        return SYSLOG_FAILURE;
    }
    MEMSET (pTempAudit, 0, sizeof (tAuditInfo));
    /* Sanity check */
    if (pu1LogBuf == NULL)
    {
        MemReleaseMemBlock (gSysLogGlobalInfo.SysLogTMemPoolId,
                            (UINT1 *) pTempAudit);
        return SYSLOG_FAILURE;
    }

    TMO_SLL_Scan (&gSysLogGlobalInfo.sysFileTblList, pnode, tSysLogFileInfo *)
    {

        pFileInfo = GET_FILEENTRY (pnode);

        if (pFileInfo->u4Priority == u4Priority)
        {
            /* Memset the required buffers */
            MEMSET (pTempAudit->au1SyslogBuf, 0, SYSLOG_MAX_PKT_LEN);
            MEMSET (pTempAudit->au1IssLogFileName, 0, ISS_CONFIG_FILE_NAME_LEN);

            MEMCPY (pTempAudit->au1SyslogBuf, pu1LogBuf, 1024);
            MEMCPY (pTempAudit->au1IssLogFileName, pFileInfo->au1FileName,
                    STRLEN (pFileInfo->au1FileName));
            pTempAudit->i2Flag = AUDIT_SYSLOG_MSG;

            if (MSRSendAuditInfo (pTempAudit) != MSR_SUCCESS)
            {
                MemReleaseMemBlock (gSysLogGlobalInfo.SysLogTMemPoolId,
                                    (UINT1 *) pTempAudit);
                return SYSLOG_FAILURE;
            }
        }

    }

    if (pu1ModName != NULL)
    {
        MEMSET (pTempAudit->au1SyslogBuf, 0, SYSLOG_MAX_PKT_LEN);
        MEMSET (pTempAudit->au1IssLogFileName, 0, ISS_CONFIG_FILE_NAME_LEN);

        MEMCPY (pTempAudit->au1SyslogBuf, pu1LogBuf, 1024);
        MEMCPY (pTempAudit->au1IssLogFileName, pu1ModName, (MAX_MOD_NAME + 1));
        pTempAudit->u4ModuleId = u4ModuleId;
        if (u4FileNo != 0)
        {

            pu1Temp = STRTOK (pu1ModName, ".");
            SPRINTF ((CHR1 *) pTempAudit->au1IssLogFileName, "%s.%d.txt",
                     pu1Temp, u4FileNo);
        }
        else
        {
            SPRINTF ((CHR1 *) pTempAudit->au1IssLogFileName, "%s.txt",
                     pu1ModName);
        }
        pTempAudit->i2Flag = AUDIT_SYSLOG_MSG;

        if (MSRSendAuditInfo (pTempAudit) != MSR_SUCCESS)
        {
            MemReleaseMemBlock (gSysLogGlobalInfo.SysLogTMemPoolId,
                                (UINT1 *) pTempAudit);
            return SYSLOG_FAILURE;
        }
    }

    gsSysLogParams.u4SyslogLogs++;
    MemReleaseMemBlock (gSysLogGlobalInfo.SysLogTMemPoolId,
                        (UINT1 *) pTempAudit);
    return SYSLOG_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogAddLogServerIp                                   */
/*                                                                           */
/* Description      : This function is invoked to set the Log server IP      */
/*                    address.                                               */
/*                                                                           */
/* Input Parameters : UINT4 u4IpAddress - IP address of Log server           */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  SYSLOG_SUCCESS - On successful opening of socket      */
/*                     SYSLOG_SOCK_FAILURE - On failure to open a socket     */
/*****************************************************************************/

INT4
SysLogAddLogServerIp (UINT4 u4IpAddress)
{
    INT4                i4Fd, i4TempFd;

    /* Try to open a socket to the mail server. Once the socket is opened
     * we need to write the data to the mail server from the log buffer.
     */

    i4Fd = socket (AF_INET, SOCK_DGRAM, 0);

    if (i4Fd == -1)
    {
        return SYSLOG_SOCK_FAILURE;
    }

    i4TempFd = gi4SysLogFd;
    gi4SysLogFd = i4Fd;
    gsSysLogParams.u4LogServerIp = u4IpAddress;
    if (i4TempFd != 0)
    {
        if (close (i4TempFd) < 0)
        {
            return SYSLOG_SOCK_FAILURE;
        }
    }

    return SYSLOG_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysSendLog                                             */
/*                                                                           */
/* Description      : This function is invoked to log the data in the log    */
/*                    server.                                                */
/*                                                                           */
/* Input Parameters : Priority ,INT1 *pStr - String to logged in log server  */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  None                                                  */
/*****************************************************************************/

INT4
SysSendLog (UINT4 u4Priority, INT1 *pStr)
{

    tSysLogFwdInfo     *pFwdInfoNode = NULL;
    UINT1              *pu1SysMsg = NULL;
    INT4                i4DnsRetVal = DNS_NOT_RESOLVED;
    tDNSResolvedIpInfo  IpInfo;
    tSysLogFwdInfo      ResolAddr;

#ifdef RM_WANTED
    UINT4               u4StackingModel = 0;
#endif
    MEMSET (&IpInfo, SYSLOG_ZERO, sizeof (tDNSResolvedIpInfo));

#ifdef RM_WANTED
    u4StackingModel = ISS_GET_STACKING_MODEL ();

    if (u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL)
    {
        if (RmRetrieveNodeState () == RM_STANDBY)
        {
            if (SysSendLogStandby ((INT1 *) pStr) != SYSLOG_SUCCESS)
            {
                return SYSLOG_FAILURE;
            }
            return SYSLOG_SUCCESS;
        }
    }
#endif
    TMO_SLL_Scan (&gSysLogGlobalInfo.sysFwdTblList, pFwdInfoNode,
                  tSysLogFwdInfo *)
    {
        MEMSET (&ResolAddr, SYSLOG_ZERO, sizeof (tSysLogFwdInfo));
        pFwdInfoNode = GET_FWDENTRY (pFwdInfoNode);
        if (pFwdInfoNode == NULL)
        {
            return SYSLOG_FAILURE;
        }
        ResolAddr.u4Port = pFwdInfoNode->u4Port;
        ResolAddr.u4AddrType = pFwdInfoNode->u4AddrType;

        if (pFwdInfoNode->u4AddrType == IPVX_DNS_FAMILY)
        {
            if ((i4DnsRetVal =
                 FsUtlIPvXResolveHostName (pFwdInfoNode->au1HostName,
                                           DNS_NONBLOCK,
                                           &IpInfo)) == DNS_RESOLVED)
            {
                if ((IpInfo.Resolv4Addr.u1AddrLen != 0)
                    || (IpInfo.Resolv6Addr.u1AddrLen != 0))
                {
                    if (IpInfo.Resolv6Addr.u1AddrLen != 0)
                    {
                        ResolAddr.u4AddrType = IPVX_ADDR_FMLY_IPV6;
                        IPVX_ADDR_COPY (&(ResolAddr.ServIpAddr),
                                        &(IpInfo.Resolv6Addr));
                    }
                    else
                    {
                        ResolAddr.u4AddrType = IPVX_ADDR_FMLY_IPV4;
                        IPVX_ADDR_COPY (&(ResolAddr.ServIpAddr),
                                        &(IpInfo.Resolv4Addr));

                    }
                }
            }
            else
            {
                switch (i4DnsRetVal)
                {
                    case DNS_IN_PROGRESS:
                        SYSLOG_DBG (SYSLOG_MGMT_TRC | SYSLOG_CONTROL_PATH_TRC,
                                    "SysSendLog: Host name resolution in Progress\n");
                        break;
                    case DNS_CACHE_FULL:
                        SYSLOG_DBG (SYSLOG_MGMT_TRC | SYSLOG_CONTROL_PATH_TRC,
                                    "SysSendLog: DNS cache full, cannot resolve at the moment\n");
                        break;
                    default:
                        SYSLOG_DBG (SYSLOG_MGMT_TRC | SYSLOG_CONTROL_PATH_TRC,
                                    "SysSendLog: Cannot resolve the host name \n");
                        break;
                }
                continue;

            }

        }
        else
        {
            IPVX_ADDR_COPY (&(ResolAddr.ServIpAddr),
                            &(pFwdInfoNode->ServIpAddr));

        }

        if ((pFwdInfoNode->u4Priority) == u4Priority)
        {
            /* If the transport type is UDP */
            if ((pFwdInfoNode->u4TransType) == SYSLOG_UDP)
            {
                if (ResolAddr.u4AddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    SendLogMsgV4UdpServer (&ResolAddr, pStr);

                }
                else if (ResolAddr.u4AddrType == IPVX_ADDR_FMLY_IPV6)
                {
                    SendLogMsgV6UdpServer (&ResolAddr, pStr);
                }
            }

            /* If the transport type is TCP */

            if ((pFwdInfoNode->u4TransType) == SYSLOG_TCP)
            {
                pu1SysMsg = (UINT1 *) MemAllocMemBlk (gSysLogGlobalInfo.
                                                      SysLogCMemPoolId);
                if (pu1SysMsg == NULL)
                {
                    return SYSLOG_FAILURE;
                }

                MEMSET (pu1SysMsg, SYSLOG_ZERO,
                        SYSLOG_MESS_LENGTH + SYSLOG_FIFTY);

                STRNCPY (pu1SysMsg, pStr, STRLEN (pStr));
                pu1SysMsg[STRLEN (pStr)] = '\0';
                STRCAT (pu1SysMsg, "\r\n");
                SendLogMsgToTcpServer ((UINT1 **) &pu1SysMsg);
            }

            /* If the transport Type is BEEP */
            if ((pFwdInfoNode->u4TransType) == SYSLOG_BEEP)
            {
                /* Filling structure for Beep */
                gsBpCltSyslogMsg.u4MsgType = SYSLOG_MSG_EVENT;
                gsBpCltSyslogMsg.u4Role = gsSysLogParams.u4SysLogRole;
                gsBpCltSyslogMsg.i4Profile =
                    (UINT4) gsSysLogParams.u4SysLogProfile;
                gsBpCltSyslogMsg.i4Port = (UINT4) pFwdInfoNode->u4Port;

                if (ResolAddr.u4AddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    gsBpCltSyslogMsg.SrvAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
                    gsBpCltSyslogMsg.SrvAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
                    MEMCPY (gsBpCltSyslogMsg.SrvAddr.au1Addr,
                            ResolAddr.ServIpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
                }
                else if (ResolAddr.u4AddrType == IPVX_ADDR_FMLY_IPV6)
                {
                    gsBpCltSyslogMsg.SrvAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
                    gsBpCltSyslogMsg.SrvAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;
                    MEMCPY (gsBpCltSyslogMsg.SrvAddr.au1Addr,
                            ResolAddr.ServIpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
                }
#ifdef BEEP_CLIENT_WANTED
                BpCltSyslogMsgNotify (&gsBpCltSyslogMsg, (UINT1 *) pStr);
#endif
            }
        }
    }
    return SYSLOG_SUCCESS;
}

#ifdef RM_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysSendLogStandby                                      */
/*                                                                           */
/* Description      : This function is invoked to log the standby data       */
/*                    in the log server                                      */
/*                                                                           */
/* Input Parameters : Priority ,INT1 *pStr - String to logged in log server  */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  None                                                  */
/*****************************************************************************/

INT4
SysSendLogStandby (INT1 *pStr)
{
    static UINT1        au1Buf[MAX_LOG_SIZE_STDBY][MAX_LOG_CHR_STDBY];
    static INT1         i1count = 0;
    UINT4               u4PktLen = 0;
#ifdef RM_WANTED
    tHwPortInfo         HwPortInfo;
    UINT4               u4IfIndex = 0;
#endif
    INT4                i4len = 0;
    UINT4               u4RetVal = SYSLOG_SUCCESS;
    INT4                i4PriValue = SYSLOG_ZERO;
    INT1                i1PriFlag = SYSLOG_ZERO;
    INT1                i1TimeFlag = SYSLOG_ZERO;

    if (pStr == NULL)
    {
        return SYSLOG_FAILURE;
    }
#ifdef NPSIM_WANTED
    tHwInfo             HwInfo;
    MEMSET (&HwInfo, 0, sizeof (tHwInfo));
#endif
#ifdef RM_WANTED
    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
#endif

    u4RetVal = SysLogParser (&i4PriValue, &i1PriFlag, &i1TimeFlag,
                             (CHR1 *) pStr);
    if (u4RetVal != SYSLOG_SUCCESS)
    {
        return SYSLOG_FAILURE;
    }
    u4RetVal = (UINT4) i4PriValue;
    u4RetVal = OSIX_HTONL (u4RetVal);

    MEMSET (&au1Buf[i1count], 0, MAX_LOG_CHR_STDBY);
    i4len = STRLEN ("stby:");
    u4PktLen =
        (STRLEN (pStr) >
         (MAX_LOG_CHR_STDBY - 9)) ? (MAX_LOG_CHR_STDBY - 9) : STRLEN (pStr);
    MEMCPY (au1Buf[i1count], &u4RetVal, sizeof (UINT4));
    STRNCPY (&au1Buf[i1count][4], "stby:", i4len);
    STRNCPY (&au1Buf[i1count][9], pStr, u4PktLen);
    i1count++;

#ifdef RM_WANTED
    CfaGetLocalUnitPortInformation (&HwPortInfo);
    u4IfIndex = HwPortInfo.au4ConnectingPortIfIndex[0];
    UNUSED_PARAM (u4IfIndex);
#endif
    if (i1count == (MAX_LOG_SIZE_STDBY - 1))
    {
        i1count = SYSLOG_ZERO;
#ifdef NPSIM_WANTED
        HwInfo.u4IfIndex = u4IfIndex;
        HwInfo.u4PktSize = u4PktLen;
        HwInfo.u4MessageType = RM_STDBY_MSG;
        HwInfo.uHwMsgInfo.HwGenMsgInfo.pu1Data = (UINT1 *) &au1Buf;

        if (FsCfaHwSendIPCMsg (&HwInfo) == FNP_FAILURE)
        {
            return SYSLOG_FAILURE;
        }

#endif
    }
    return SYSLOG_SUCCESS;

}
#endif
/*****************************************************************************/
/*                                                                           */
/* Function Name    : SendLogMsgV4UdpServer                                  */
/*                                                                           */
/* Description      : This function is invoked to log the data in the log    */
/*                    server.                                                */
/*                                                                           */
/* Input Parameters : Pointer to Fwd table structure,
                      INT1 *pStr - String to logged in log server            */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  None                                                  */
/*****************************************************************************/

INT4
SendLogMsgV4UdpServer (tSysLogFwdInfo * pFwdInfo, INT1 *pStr)
{
    UINT4               u4ServerIp;
    INT4                i4WriteCount;
    struct sockaddr_in  sLogServer;
    INT4                i4SockFd = SYSLOG_ZERO;

    MEMSET (&sLogServer, SYSLOG_ZERO, sizeof (struct sockaddr_in));
    MEMCPY (&u4ServerIp, pFwdInfo->ServIpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
    sLogServer.sin_family = AF_INET;
    sLogServer.sin_addr.s_addr = (u4ServerIp);
    sLogServer.sin_port = OSIX_HTONS ((UINT2) pFwdInfo->u4Port);
    if ((i4SockFd = (socket (AF_INET, SOCK_DGRAM, SYSLOG_ZERO))) < SYSLOG_ZERO)
    {
        return SYSLOG_FAILURE;
    }

    if (connect (i4SockFd, (struct sockaddr *) &sLogServer,
                 sizeof (sLogServer)) < SYSLOG_ZERO)
    {

        SYSLOG_DBG (ALL_FAILURE_TRC, "udp client connect failed\n");
        SyslogSendTrapMessage (pFwdInfo->ServIpAddr);
        close (i4SockFd);
        return SYSLOG_FAILURE;
    }

    i4WriteCount = send (i4SockFd, pStr, STRLEN (pStr), SYSLOG_ZERO);

    if (i4WriteCount <= SYSLOG_ZERO)
    {
        SyslogSendTrapMessage (pFwdInfo->ServIpAddr);
        close (i4SockFd);
        return SYSLOG_FAILURE;
    }
    gsSysLogParams.u4SyslogLogs++;

    close (i4SockFd);
    return SYSLOG_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SendLogMsgV6UdpServer                                  */
/*                                                                           */
/* Description      : This function is invoked to log the data in the log    */
/*                    server.                                                */
/*                                                                           */
/* Input Parameters : Pointer Fwd Table structure,
                      INT1 *pStr - String to logged in log server            */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  None                                                  */
/*****************************************************************************/

INT4
SendLogMsgV6UdpServer (tSysLogFwdInfo * pFwdInfo, INT1 *pStr)
{
    INT4                i4WriteCount;
    struct sockaddr_in6 v6sendaddr;
    INT4                i4SockFd = SYSLOG_ZERO;
    UINT1               au1ipv6[IPVX_IPV6_ADDR_LEN];

    MEMSET (&v6sendaddr, SYSLOG_ZERO, sizeof (struct sockaddr_in6));
    v6sendaddr.sin6_family = AF_INET6;
    v6sendaddr.sin6_port = (UINT2) pFwdInfo->u4Port;
    MEMCPY (au1ipv6, pFwdInfo->ServIpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);

    MEMCPY ((&v6sendaddr.sin6_addr.s6_addr), (au1ipv6), sizeof (tIp6Addr));

    if ((i4SockFd = (socket (AF_INET6, SOCK_DGRAM, SYSLOG_ZERO))) < SYSLOG_ZERO)
    {
        return SYSLOG_FAILURE;
    }

    if (connect (i4SockFd, (struct sockaddr *) &v6sendaddr, sizeof (v6sendaddr))
        < SYSLOG_ZERO)
    {
        close (i4SockFd);
        return SYSLOG_FAILURE;
    }

    i4WriteCount = send (i4SockFd, pStr, STRLEN (pStr), SYSLOG_ZERO);

    if (i4WriteCount <= SYSLOG_ZERO)
    {
        close (i4SockFd);
        return SYSLOG_FAILURE;
    }
    gsSysLogParams.u4SyslogLogs++;

    close (i4SockFd);
    return SYSLOG_SUCCESS;
}

#ifdef RM_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function Name    : SendLogMsgToSyslog                                     */
/*                                                                           */
/* Description      : This function is used to post a log event to TcpSyslog */
/*                    server.                                                */
/*                                                                           */
/* Input Parameters : INT1 **pc1SysMsg - String to logged in log server      */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  SYSLOG_SUCCESS                                        */
/*****************************************************************************/
INT1
SendLogMsgToSyslog (tCRU_BUF_CHAIN_HEADER * pPkt, UINT4 u4Length)
{
    UINT1              *pu1Buf = NULL;
    UINT4               u4PacLen = 0;

    u4PacLen = (MAX_LOG_SIZE_STDBY) * (MAX_LOG_CHR_STDBY);
    if (u4Length > u4PacLen)
    {
        CRU_BUF_Release_MsgBufChain (pPkt, 0);
        return SYSLOG_FAILURE;
    }

    pu1Buf = MemAllocMemBlk (gSysLogGlobalInfo.SysLogStdbyPoolId);
    if (pu1Buf == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pPkt, 0);
        return SYSLOG_FAILURE;
    }

    CRU_BUF_Copy_FromBufChain ((tCRU_BUF_CHAIN_HEADER *) (VOID *) pPkt, pu1Buf,
                               0, u4Length);

    if (OsixQueSend (gStdbyQueId, (UINT1 *) &pu1Buf,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gSysLogGlobalInfo.SysLogStdbyPoolId, pu1Buf);
        CRU_BUF_Release_MsgBufChain (pPkt, 0);
        return SYSLOG_FAILURE;
    }

    OsixEvtSend (gSmtpTaskId, SYSLOG_SYS_LOG_MSG_EVENT);

    CRU_BUF_Release_MsgBufChain (pPkt, 0);
    return SYSLOG_SUCCESS;
}
#endif
/*****************************************************************************/
/*                                                                           */
/* Function Name    : SendLogMsgToTcpServer                                  */
/*                                                                           */
/* Description      : This function is used to post a log event to TcpSyslog */
/*                    server.                                                */
/*                                                                           */
/* Input Parameters : INT1 **pc1SysMsg - String to logged in log server      */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  SYSLOG_SUCCESS                                        */
/*****************************************************************************/
INT1
SendLogMsgToTcpServer (UINT1 **pc1SysMsg)
{
    /* posts the TCP syslog message in message queue */
    if (OsixQueSend (gSyslogTcpQueId, (UINT1 *) pc1SysMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gSysLogGlobalInfo.SysLogCMemPoolId,
                            (UINT1 *) *pc1SysMsg);
        return SYSLOG_FAILURE;
    }

    OsixEvtSend (gSmtpTaskId, SYSLOG_TCP_LOG_MSG_EVENT);

    /* A delay of one tick is added to make sure the message is
     * properly recieved by OsixQueRecv */
    OsixDelayTask (SYSLOG_ONE);
    return SYSLOG_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SendLogMsgV4TcpServer                                  */
/*                                                                           */
/* Description      : This function is invoked to log the data in the log    */
/*                    server.                                                */
/*                                                                           */
/* Input Parameters : Pointer to Fwd Table Structure,
                      INT1 *pStr - String to logged in log server            */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  None                                                  */
/*****************************************************************************/

INT4
SendLogMsgV4TcpServer (tSysLogFwdInfo * pFwdInfo, INT1 *pStr)
{
    UINT4               u4ServerIp;
    INT4                i4WriteCount;
    struct sockaddr_in  sLogServer;

    if (pFwdInfo->i4SockFd < SYSLOG_ZERO)
    {
        MEMSET (&sLogServer, SYSLOG_ZERO, sizeof (struct sockaddr_in));
        MEMCPY (&u4ServerIp, pFwdInfo->ServIpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
        sLogServer.sin_family = AF_INET;
        sLogServer.sin_addr.s_addr = (u4ServerIp);
        sLogServer.sin_port = OSIX_HTONS ((UINT2) pFwdInfo->u4Port);

        if ((pFwdInfo->i4SockFd =
             (socket (AF_INET, SOCK_STREAM, IPPROTO_TCP))) < SYSLOG_ZERO)
        {
            pFwdInfo->i4SockFd = SYSLOG_NO_SOCK_FD;
            return SYSLOG_FAILURE;
        }

        if (connect (pFwdInfo->i4SockFd, (struct sockaddr *) &sLogServer,
                     sizeof (sLogServer)) < SYSLOG_ZERO)
        {
            SyslogSendTrapMessage (pFwdInfo->ServIpAddr);
            SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC, "Tcp client  connect failed:");
            close (pFwdInfo->i4SockFd);
            pFwdInfo->i4SockFd = SYSLOG_NO_SOCK_FD;
            return SYSLOG_FAILURE;
        }
    }

    i4WriteCount = send (pFwdInfo->i4SockFd, pStr, STRLEN (pStr), SYSLOG_ZERO);

    if (i4WriteCount <= SYSLOG_ZERO)
    {
        SyslogSendTrapMessage (pFwdInfo->ServIpAddr);
        close (pFwdInfo->i4SockFd);
        pFwdInfo->i4SockFd = SYSLOG_NO_SOCK_FD;
        return SYSLOG_FAILURE;
    }
    gsSysLogParams.u4SyslogLogs++;

    return SYSLOG_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SendLogMsgV6TcpServer                                  */
/*                                                                           */
/* Description      : This function is invoked to log the data in the log    */
/*                    server.                                                */
/*                                                                           */
/* Input Parameters : Pointer Fwd Table structure,
                      INT1 *pStr - String to logged in log server            */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  None                                                  */
/*****************************************************************************/

INT4
SendLogMsgV6TcpServer (tSysLogFwdInfo * pFwdInfo, INT1 *pStr)
{
    INT4                i4WriteCount;
    struct sockaddr_in6 v6sendaddr;
    UINT1               au1ipv6[IPVX_IPV6_ADDR_LEN];

    if (pFwdInfo->i4SockFd < SYSLOG_ZERO)
    {
        MEMSET (&v6sendaddr, SYSLOG_ZERO, sizeof (struct sockaddr_in6));
        v6sendaddr.sin6_family = AF_INET6;
        v6sendaddr.sin6_port = (UINT2) pFwdInfo->u4Port;
        MEMCPY (au1ipv6, pFwdInfo->ServIpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
        SYSLOG_INET_HTONL (au1ipv6);
        MEMCPY ((&v6sendaddr.sin6_addr.s6_addr), (au1ipv6), sizeof (tIp6Addr));

        if ((pFwdInfo->i4SockFd =
             (socket (AF_INET6, SOCK_STREAM, IPPROTO_TCP))) < SYSLOG_ZERO)
        {
            pFwdInfo->i4SockFd = SYSLOG_NO_SOCK_FD;
            return SYSLOG_FAILURE;
        }

        if (connect (pFwdInfo->i4SockFd, (struct sockaddr *) &v6sendaddr,
                     sizeof (v6sendaddr)) < SYSLOG_ZERO)
        {
            SyslogSendTrapMessage (pFwdInfo->ServIpAddr);
            close (pFwdInfo->i4SockFd);
            pFwdInfo->i4SockFd = SYSLOG_NO_SOCK_FD;
            return SYSLOG_FAILURE;
        }
    }

    i4WriteCount = send (pFwdInfo->i4SockFd, pStr, STRLEN (pStr), SYSLOG_ZERO);
    if (i4WriteCount <= SYSLOG_ZERO)
    {
        SyslogSendTrapMessage (pFwdInfo->ServIpAddr);
        close (pFwdInfo->i4SockFd);
        pFwdInfo->i4SockFd = SYSLOG_NO_SOCK_FD;
        return SYSLOG_FAILURE;
    }
    gsSysLogParams.u4SyslogLogs++;

    return SYSLOG_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogSetModLogLevel                                   */
/*                                                                           */
/* Description      : This function is invoked to set the SysLog level       */
/*                                                                           */
/* Input Parameters : UINT4 u4Level - Log Level                              */
/*                    UINT4 u4ModuleId - Module Id                           */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  SYSLOG_SUCCCESS - On success                          */
/*                     SYSLOG_INVALID_MODID - If moduleid is invalid         */
/*                     SYSLOG_INVALID_LEVEL - If the level is invalid        */
/*****************************************************************************/

INT4
SysLogSetModLogLevel (UINT4 u4ModuleId, UINT4 u4Level)
{

    if (u4ModuleId > SYSLOG_MAX_MODULES)
    {
        return (SYSLOG_INVALID_MODID);
    }

    if (asModInfo[u4ModuleId].u4Status == SYSLOG_DISABLE)
    {
        return (SYSLOG_INVALID_MODID);
    }

    if (u4Level > SYSLOG_DEBUG_LEVEL)
    {
        return (SYSLOG_INVALID_LEVEL);
    }

    asModInfo[u4ModuleId].u4Level = u4Level;

    if (STRCASECMP (asModInfo[u4ModuleId].au1Name, "CLI") == 0)
        gu4CliSysLogLevel = u4Level;

    return (SYSLOG_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogClearLogs                                        */
/*                                                                           */
/* Description      : This function is invoked to clear console logs         */
/*                                                                           */
/* Input Parameters : None                                                   */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  None                                                  */
/*****************************************************************************/

VOID
SysLogClearLogs ()
{
    tSysLogEntry       *pSysLogEntry;

    if (gu4SysLogInit == SYSLOG_DISABLE)
    {
        return;
    }

    if (OsixSemTake (SMTP_SEM_ID) != OSIX_SUCCESS)
    {
        return;
    }

    pSysLogEntry = &gaSysLogTbl[MAX_SYSLOG_USERS_LIMIT];

    if (pSysLogEntry->pLast != NULL)
    {
        pSysLogEntry->pLast->pNext = pSysLogEntry->pFree;
    }
    if (pSysLogEntry->pHead != NULL)
    {
        pSysLogEntry->pFree = pSysLogEntry->pHead;
    }
    pSysLogEntry->pHead = pSysLogEntry->pLast = NULL;
    pSysLogEntry->u4NumMsgLogs = 0;

    OsixSemGive (SMTP_SEM_ID);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SyslogGetSyslogCount                                   */
/*                                                                           */
/* Description      : This function is invoked from syslgcli.c to            */
/*                    get the number of syslog messages sent to syslog       */
/*                    server                                                 */
/*                                                                           */
/* Input Parameters : UINT4 *pu4SyslogCount - sylog count                    */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : SYSLOG_SUCCESS                                        */
/*****************************************************************************/

INT4
SyslogGetSyslogCount (UINT4 *pu4SyslogCount)
{
    *pu4SyslogCount = gsSysLogParams.u4SyslogLogs;
    return SYSLOG_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SyslogGetConsolelogCount                               */
/*                                                                           */
/* Description      : This function is invoked from syslgcli.c to            */
/*                    get the number of console messages sent to syslog      */
/*                    server                                                 */
/*                                                                           */
/* Input Parameters : UINT4 *pu4ConsolelogCount - sylog count                */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : SYSLOG_SUCCESS                                        */
/*****************************************************************************/

INT4
SyslogGetConsolelogCount (UINT4 *pu4ConsolelogCount)
{
    *pu4ConsolelogCount = gsSysLogParams.u4ConsoleLogs;
    return SYSLOG_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SyslogGetLogBufferCount                                */
/*                                                                           */
/* Description      : This function is invoked from syslgcli.c to            */
/*                    get the number of messages stored in the log buffer    */
/*                                                                           */
/* Input Parameters : UINT4 *pu4LogBuffCount - log buffer count              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : SYSLOG_SUCCESS                                        */
/*****************************************************************************/

INT4
SyslogGetLogBufferCount (UINT4 *pu4LogBuffCount)
{
    tSysLogEntry       *pSysLogEntry = NULL;
    pSysLogEntry = &gaSysLogTbl[MAX_SYSLOG_USERS_LIMIT];
    *pu4LogBuffCount = 0;
    if (pSysLogEntry != NULL)
    {
        *pu4LogBuffCount = pSysLogEntry->u4NumMsgLogs;
    }
    return SYSLOG_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SysLogGetTimeStr                                 */
/*                                                                          */
/*    Description        : This function gets the time as a string.         */
/*                         The display string is 21 bytes long, including   */
/*                         null termination.                                */
/*                                                                          */
/*    Input(s)           : ac1TimeStr - Buffer of length atleast 21 bytes   */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : SYSLOG_SUCCESS                                   */
/*                                                                          */
/****************************************************************************/

INT4
SysLogGetTimeStr (CHR1 ac1TimeStr[])
{
    tUtlTm             *tm;

    const CHR1         *ac1Month[12] = {
        "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };

#ifndef OS_VXWORKS
    time_t              ti;
    time (&ti);
#else
    struct timespec     ti;
    clock_gettime (CLOCK_REALTIME, &ti);
#endif

    tm = (tUtlTm *) localtime ((time_t *) & ti);
    if (tm != NULL)
    {
        tm->tm_year += 1900;

        /* This fix is added to avoid False Positive Coverity warning */
        SNPRINTF ((CHR1 *) ac1TimeStr, SYSLOG_MAX_TIMESTR_SIZE,
                  "%s %2u %.2u:%.2u:%.2u ",
                  ac1Month[tm->tm_mon], tm->tm_mday, tm->tm_hour, tm->tm_min,
                  tm->tm_sec);
    }

    return SYSLOG_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SysLogGetTimeStrAndYear                          */
/*                                                                          */
/*    Description        : This function gets the time and year as a string */
/*                         The display string is 21 bytes long, including   */
/*                         null termination.                                */
/*                                                                          */
/*    Input(s)           : ac1TimeStr - Buffer of length atleast 21 bytes   */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : SYSLOG_SUCCESS                                   */
/*                                                                          */
/****************************************************************************/

INT4
SysLogGetTimeStrAndYear (CHR1 ac1TimeStr[])
{
    tUtlTm             *tm;

    const CHR1         *ac1Month[12] = {
        "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };

#ifndef OS_VXWORKS
    time_t              ti;
    time (&ti);
#else
    struct timespec     ti;
    clock_gettime (CLOCK_REALTIME, &ti);
#endif

    tm = (tUtlTm *) localtime ((time_t *) & ti);
    if (tm != NULL)
    {
        tm->tm_year += 1900;

        /* This fix is added to avoid False Positive Coverity warning */
        SNPRINTF ((CHR1 *) ac1TimeStr, SYSLOG_MAX_TIMEYRSTR_SIZE,
                  "%s %2u %.2u:%.2u:%.2u %4u ",
                  ac1Month[tm->tm_mon], tm->tm_mday, tm->tm_hour,
                  tm->tm_min, tm->tm_sec, tm->tm_year);

    }

    return SYSLOG_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SysLogValidateMailId                             */
/*                                                                          */
/*    Description        : This function is used to validate the given      */
/*                         mail id.                                         */
/*                                                                          */
/*    Input(s)           : pu1TestStr - Mail id to be validated             */
/*                         i4Length   - Length of the mail id               */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : SYSLOG_TRUE/SYSLOG_FALSE                         */
/*                                                                          */
/****************************************************************************/
UINT4
SysLogValidateMailId (UINT1 *pu1TestStr, INT4 i4Length)
{
    INT4                i4Len = 0;
    UINT1              *u1DotPos = NULL;

    if (i4Length == 0)
    {
        /* No mail id is configured */
        return SYSLOG_TRUE;
    }

    /* Mail id should contain @ character
     */
    if (STRSTR (pu1TestStr, "@") == NULL)
    {
        return SYSLOG_FALSE;
    }

    /* Mail id should not start with @ character
     */
    if (*pu1TestStr == '@')
    {
        return SYSLOG_FALSE;
    }

    for (i4Len = 0; i4Len < i4Length; i4Len++)
    {
        /* In mail id only special characters(@ _ . -) and alphanumerals
         * are allowed.
         */
        if ((pu1TestStr[i4Len] != '.') && (pu1TestStr[i4Len] != '-') &&
            (pu1TestStr[i4Len] != '_') && (!isalnum (pu1TestStr[i4Len])))
        {
            if (pu1TestStr[i4Len] == '@')
            {
                /*Mail id should not end with "@" and should not
                 * end with "@." 
                 */
                if ((pu1TestStr[i4Len + 1] == '\0') ||
                    (pu1TestStr[i4Len + 1] == '.'))
                {
                    return SYSLOG_FALSE;
                }

                /* Mail id should not contains more than one "@"
                 */
                if (STRSTR (&pu1TestStr[i4Len + 1], "@") != NULL)
                {
                    return SYSLOG_FALSE;
                }

                u1DotPos = (UINT1 *) STRSTR (&pu1TestStr[i4Len + 1], ".");

                /* Mail id should not end with "."
                 */
                if (u1DotPos != NULL && *(u1DotPos + 1) == '\0')
                {
                    return SYSLOG_FALSE;
                }

            }
            else
            {
                return SYSLOG_FALSE;
            }
        }
    }

    return SYSLOG_TRUE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FileUtilFindEntry                                */
/*                                                                          */
/*    Description        : This function is check whether entry is
 *                                                     already present      */
/*                                                                          */
/*                                                                          */
/*    Input(s)           : i4FsSyslogPriority- Priority value               */
/*                         *pFsSyslogFileName   - Pointer to the structure  */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : SYSLOG_TRUE/SYSLOG_FALSE                         */
/*                                                                          */
/****************************************************************************/
PUBLIC tSysLogFileInfo *
FileUtilFindEntry (INT4 i4FsSyslogPriority, UINT1 *pFileName)
{

    tSysLogFileInfo    *pFileInfo = NULL;
    tSysLogFileInfo    *pnode = NULL;

    TMO_SLL_Scan (&gSysLogGlobalInfo.sysFileTblList, pnode, tSysLogFileInfo *)
    {

        pFileInfo = GET_FILEENTRY (pnode);

        if ((pFileInfo->u4Priority == (UINT4) i4FsSyslogPriority) &&
            (STRCMP (pFileName, pFileInfo->au1FileName) == SYSLOG_ZERO))
        {
            return pFileInfo;
        }
    }

    return NULL;
}

/****************************************************************/
/* Function    : SysLogFileInfoAddNodeToList                    */
/* Description : add node of structure to linked list           */
/* Input       : pointer to File Info Structure                 */
/* Output      : None                                           */
/* Return      : SYSLOG_SUCCESS/SYSLOG_FAILURE                  */
/* **************************************************************/

PUBLIC INT4
SysLogFileInfoAddNodeToList (tSysLogFileInfo * pFileInfo)
{

    tSysLogFileInfo    *pTemp = NULL;
    tSysLogFileInfo    *pFileInfoTemp = NULL;

    TMO_SLL_Scan (&gSysLogGlobalInfo.sysFileTblList, pTemp, tSysLogFileInfo *)
    {
        pFileInfoTemp = GET_FILEENTRY (pTemp);

        /* Compare the priority */
        if (pFileInfoTemp->u4Priority > pFileInfo->u4Priority)
        {
            TMO_SLL_Insert (&gSysLogGlobalInfo.sysFileTblList,
                            TMO_SLL_Previous (&gSysLogGlobalInfo.sysFileTblList,
                                              &(pFileInfoTemp->NextNode)),
                            (tTMO_SLL_NODE *) (VOID *) pFileInfo);

            return SYSLOG_SUCCESS;
        }

        /* If priority equal */
        else if (pFileInfoTemp->u4Priority == pFileInfo->u4Priority)
        {
            /* Compare the length of the filename */
            if (STRLEN (pFileInfoTemp->au1FileName) >
                STRLEN (pFileInfo->au1FileName))
            {

                TMO_SLL_Insert (&gSysLogGlobalInfo.sysFileTblList,
                                TMO_SLL_Previous (&gSysLogGlobalInfo.
                                                  sysFileTblList,
                                                  &(pFileInfoTemp->NextNode)),
                                (tTMO_SLL_NODE *) (VOID *) pFileInfo);

                return SYSLOG_SUCCESS;

            }
            /* if length of the filenames equal compare the Filenames */
            else if (STRLEN (pFileInfoTemp->au1FileName) ==
                     STRLEN (pFileInfo->au1FileName))
            {

                if (STRCMP (pFileInfoTemp->au1FileName, pFileInfo->au1FileName)
                    > 0)
                {
                    TMO_SLL_Insert (&gSysLogGlobalInfo.sysFileTblList,
                                    TMO_SLL_Previous (&gSysLogGlobalInfo.
                                                      sysFileTblList,
                                                      &(pFileInfoTemp->
                                                        NextNode)),
                                    (tTMO_SLL_NODE *) (VOID *) pFileInfo);

                    return SYSLOG_SUCCESS;
                }

            }
        }
    }
    TMO_SLL_Add (&gSysLogGlobalInfo.sysFileTblList,
                 (tTMO_SLL_NODE *) (VOID *) pFileInfo);

    return SYSLOG_SUCCESS;

}

/*****************************************************************
 * Function    : SysLogFwdInfoAddNodeToList
 * Description : add node of structure to linked list
 * Input       : pointer to Fwd Info Structure
 * Output      : None
 * Return      : SYSLOG_SUCCESS/SYSLOG_FAILURE
 * ************************************************************/

PUBLIC INT4
SysLogFwdInfoAddNodeToList (tSysLogFwdInfo * pFwdInfo)
{

    tSysLogFwdInfo     *pTemp = NULL;
    tSysLogFwdInfo     *pFwdInfoTemp = NULL;
    INT4                i4Cmp;

    TMO_SLL_Scan (&gSysLogGlobalInfo.sysFwdTblList, pTemp, tSysLogFwdInfo *)
    {
        pFwdInfoTemp = GET_FWDENTRY (pTemp);
        if (pFwdInfoTemp->u4Priority > pFwdInfo->u4Priority)
        {
            TMO_SLL_Insert (&gSysLogGlobalInfo.sysFwdTblList,
                            TMO_SLL_Previous (&gSysLogGlobalInfo.sysFwdTblList,
                                              &(pFwdInfoTemp->NextNode)),
                            (tTMO_SLL_NODE *) (VOID *) pFwdInfo);

            return SYSLOG_SUCCESS;
        }
        else if (pFwdInfoTemp->u4Priority == pFwdInfo->u4Priority)
        {
            if (pFwdInfoTemp->u4AddrType > pFwdInfo->u4AddrType)
            {
                TMO_SLL_Insert (&gSysLogGlobalInfo.sysFwdTblList,
                                TMO_SLL_Previous (&gSysLogGlobalInfo.
                                                  sysFwdTblList,
                                                  &(pFwdInfoTemp->NextNode)),
                                (tTMO_SLL_NODE *) (VOID *) pFwdInfo);
                return SYSLOG_SUCCESS;
            }
            else if (pFwdInfoTemp->u4AddrType == pFwdInfo->u4AddrType)
            {
                i4Cmp =
                    MEMCMP (pFwdInfoTemp->ServIpAddr.au1Addr,
                            pFwdInfo->ServIpAddr.au1Addr,
                            IPVX_MAX_INET_ADDR_LEN);
                if (i4Cmp > 0)

                {
                    TMO_SLL_Insert (&gSysLogGlobalInfo.sysFwdTblList,
                                    TMO_SLL_Previous (&gSysLogGlobalInfo.
                                                      sysFwdTblList,
                                                      &(pFwdInfoTemp->
                                                        NextNode)),
                                    (tTMO_SLL_NODE *) (VOID *) pFwdInfo);
                    return SYSLOG_SUCCESS;
                }
                else if (i4Cmp == 0)
                {
                    return SYSLOG_SUCCESS;
                }
            }
        }
    }
    TMO_SLL_Add (&gSysLogGlobalInfo.sysFwdTblList,
                 (tTMO_SLL_NODE *) (VOID *) pFwdInfo);

    return SYSLOG_SUCCESS;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwdUtilFindEntry                                 */
/*                                                                          */
/*    Description        : This function is check whether entry is
 *                                                     already present      */
/*                                                                          */
/*                                                                          */
/*    Input(s)           : i4FsSyslogFwdPriority- Priority value  
                           i4FsSyslogFwdAddressType - Address Type          */
/*                         Ip Address   - Ip Address                        */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : SYSLOG_TRUE/SYSLOG_FALSE                         */
/*                                                                          */
/****************************************************************************/

PUBLIC tSysLogFwdInfo *
FwdUtilFindEntry (INT4 i4FsSyslogFwdPriority,
                  INT4 i4FsSyslogFwdAddressType, UINT1 *au1IpAddr)
{

    tSysLogFwdInfo     *pFwdInfo = NULL;
    tSysLogFwdInfo     *pnode = NULL;
    UINT1               u1AddrLen = SYSLOG_ZERO;

    if (i4FsSyslogFwdAddressType == IPVX_ADDR_FMLY_IPV4)
    {
        u1AddrLen = IPVX_IPV4_ADDR_LEN;
    }
    else if (i4FsSyslogFwdAddressType == IPVX_ADDR_FMLY_IPV6)
    {
        u1AddrLen = IPVX_IPV6_ADDR_LEN;
    }
    else
    {
        u1AddrLen = (UINT1) STRLEN (au1IpAddr);
    }

    TMO_SLL_Scan (&gSysLogGlobalInfo.sysFwdTblList, pnode, tSysLogFwdInfo *)
    {

        pFwdInfo = GET_FWDENTRY (pnode);

        if (i4FsSyslogFwdAddressType != IPVX_DNS_FAMILY)
        {

            if ((MEMCMP (au1IpAddr, pFwdInfo->ServIpAddr.au1Addr, u1AddrLen) ==
                 SYSLOG_ZERO)
                && (pFwdInfo->u4Priority == (UINT4) i4FsSyslogFwdPriority)
                && (pFwdInfo->u4AddrType == (UINT4) i4FsSyslogFwdAddressType))
            {
                return pFwdInfo;
            }
        }
        else
        {
            if ((STRNCMP (au1IpAddr, pFwdInfo->au1HostName, DNS_MAX_QUERY_LEN)
                 == SYSLOG_ZERO)
                && (pFwdInfo->u4Priority == (UINT4) i4FsSyslogFwdPriority)
                && (pFwdInfo->u4AddrType == (UINT4) i4FsSyslogFwdAddressType))
            {

                return pFwdInfo;
            }
        }
    }

    return NULL;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MailUtilFindEntry                                */
/*                                                                          */
/*    Description        : This function is check whether entry is
 *                                                     already present      */
/*                                                                          */
/*                                                                          */
/*    Input(s)           : i4FsSyslogMailPriority- Priority value
                           i4FsSyslogMailSrvAddType - Address Type          */
/*                         au1IpAddr   - Ip Address                        */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : SYSLOG_TRUE/SYSLOG_FALSE                         */
/*                                                                          */
/****************************************************************************/

PUBLIC tSysLogMailInfo *
MailUtilFindEntry (INT4 i4FsSyslogMailPriority,
                   INT4 i4FsSyslogMailAddressType, UINT1 *au1IpAddr)
{

    tSysLogMailInfo    *pMailInfo = NULL;
    tSysLogMailInfo    *pnode = NULL;
    UINT1               u1AddrLen = SYSLOG_ZERO;

    if (i4FsSyslogMailAddressType == (INT4) IPVX_ADDR_FMLY_IPV4)
    {
        u1AddrLen = (UINT1) IPVX_IPV4_ADDR_LEN;
    }
    else if (i4FsSyslogMailAddressType == (INT4) IPVX_ADDR_FMLY_IPV6)
    {
        u1AddrLen = (UINT1) IPVX_IPV6_ADDR_LEN;
    }
    else if (i4FsSyslogMailAddressType == (INT4) IPVX_DNS_FAMILY)
    {
        u1AddrLen = (UINT1) STRLEN (au1IpAddr);
    }

    TMO_SLL_Scan (&gSysLogGlobalInfo.sysMailTblList, pnode, tSysLogMailInfo *)
    {

        pMailInfo = GET_MAILENTRY (pnode);
        if (i4FsSyslogMailAddressType != IPVX_DNS_FAMILY)
        {
            if ((MEMCMP (au1IpAddr, pMailInfo->MailServIpAddr.au1Addr,
                         u1AddrLen) == SYSLOG_ZERO)
                && (pMailInfo->u4Priority == (UINT4) i4FsSyslogMailPriority)
                && (pMailInfo->u4AddrType == (UINT4) i4FsSyslogMailAddressType))
            {

                return pMailInfo;
            }
        }
        else
        {
            if ((STRNCMP (au1IpAddr, pMailInfo->au1HostName, u1AddrLen) ==
                 SYSLOG_ZERO)
                && (pMailInfo->u4Priority == (UINT4) i4FsSyslogMailPriority)
                && (pMailInfo->u4AddrType == (UINT4) i4FsSyslogMailAddressType))
            {

                return pMailInfo;
            }

        }
    }

    return NULL;
}

/****************************************************************
 * Function    : SysLogMailInfoAddNodeToList
 * Description : add node of structure to linked list
 * Input       : pointer to Mail Info Structure
 * Output      : None
 * Return      : SYSLOG_SUCCESS/SYSLOG_FAILURE
 * ************************************************************/

PUBLIC INT4
SysLogMailInfoAddNodeToList (tSysLogMailInfo * pMailInfo)
{

    tSysLogMailInfo    *pTemp = NULL;
    tSysLogMailInfo    *pMailInfoTemp = NULL;

    TMO_SLL_Scan (&gSysLogGlobalInfo.sysMailTblList, pTemp, tSysLogMailInfo *)
    {
        pMailInfoTemp = GET_MAILENTRY (pTemp);
        if (pMailInfoTemp->u4Priority > pMailInfo->u4Priority)
        {
            TMO_SLL_Insert (&gSysLogGlobalInfo.sysMailTblList,
                            TMO_SLL_Previous (&gSysLogGlobalInfo.sysMailTblList,
                                              &(pMailInfoTemp->NextNode)),
                            (tTMO_SLL_NODE *) (VOID *) pMailInfo);

            return SYSLOG_SUCCESS;
        }
    }
    TMO_SLL_Add (&gSysLogGlobalInfo.sysMailTblList,
                 (tTMO_SLL_NODE *) (VOID *) pMailInfo);

    return SYSLOG_SUCCESS;
}

/************************************************************************
 *  Function Name   : SysLogUdpV4SockInit
 *  Description     : Function to create BSD Server socket and bind to
 *                    Syslog port
 *  Input           : None
 *  Output          : None
 *  Returns         : Socket descriptor or FAILURE
 ************************************************************************/

INT4
SysLogUdpV4SockInit ()
{
    INT4                i4Sock = SYSLOG_ZERO;
    struct sockaddr_in  SockAddr;

    i4Sock = socket (AF_INET, SOCK_DGRAM, SYSLOG_ZERO);
    if (i4Sock < SYSLOG_ZERO)
    {
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                    "SysLog:Failed to create socket\r\n");
        return SYSLOG_FAILURE;
    }

    MEMSET (&SockAddr, SYSLOG_ZERO, sizeof (SockAddr));
    SockAddr.sin_family = AF_INET;
    SockAddr.sin_port = OSIX_HTONS ((UINT2) (gsSysLogParams.u4SysLogPort));
    SockAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);

    if (bind (i4Sock, (struct sockaddr *) &SockAddr, sizeof (SockAddr)) <
        SYSLOG_ZERO)
    {
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC, "SysLog:Unable to Bind Socket\r\n");
        return SYSLOG_FAILURE;
    }

    gsSysLogParams.i4Sockv4Id = i4Sock;

    if (SelAddFd (i4Sock, SysLogPacketOnSocket) != OSIX_SUCCESS)
    {
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                    "Adding Socket Descriptor to Select utility Failed \r\n");
        gsSysLogParams.i4Sockv4Id = MINUS_ONE;
        close (gsSysLogParams.i4Sockv4Id);
        return SYSLOG_FAILURE;
    }

    if (fcntl (i4Sock, F_SETFL, O_NONBLOCK) < SYSLOG_ZERO)
    {

        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                    " Adding Socket Descriptor to Select utility Failed \r\n");
        gsSysLogParams.i4Sockv4Id = MINUS_ONE;
        close (gsSysLogParams.i4Sockv4Id);
        return SYSLOG_FAILURE;
    }

    return SYSLOG_SUCCESS;
}

#ifdef IP6_WANTED
/************************************************************************
 *  Function Name   : SysLogUdpV6SockInit
 *  Description     : Function to create BSD Server socket and bind to
 *                    Syslog port
 *  Input           : None
 *  Output          : None
 *  Returns         : Socket descriptor or FAILURE
 ************************************************************************/

INT4
SysLogUdpV6SockInit ()
{
    INT4                i4Sock = SYSLOG_ZERO;
    INT4                i4OptVal = OSIX_TRUE;
    struct sockaddr_in6 SockAddr;

    i4Sock = socket (AF_INET6, SOCK_DGRAM, SYSLOG_ZERO);
    if (i4Sock < SYSLOG_ZERO)
    {
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                    "SysLog:Failed to create socket\r\n");
        return SYSLOG_FAILURE;
    }

    if (setsockopt (i4Sock, IPPROTO_IPV6, IPV6_V6ONLY, &i4OptVal,
                    sizeof (i4OptVal)))
    {
        close (i4Sock);
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                    "SysLog:Unable to set socket option IPV6_V6ONLY\r\n");
        return SYSLOG_FAILURE;
    }

    MEMSET (&SockAddr, SYSLOG_ZERO, sizeof (SockAddr));
    SockAddr.sin6_family = AF_INET6;
    SockAddr.sin6_port = OSIX_HTONS (SYSLOG_PORT);
    inet_pton (AF_INET6, (const CHR1 *) "0::0", &SockAddr.sin6_addr.s6_addr);

    if (bind (i4Sock, (struct sockaddr *) &SockAddr, sizeof (SockAddr)) <
        SYSLOG_ZERO)
    {
        close (i4Sock);
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC, "SysLog:Unable to Bind Socket\r\n");
        return SYSLOG_FAILURE;
    }

    gsSysLogParams.i4Sockv6Id = i4Sock;

    if (SelAddFd (i4Sock, SysLogPacketOnSocket6) != OSIX_SUCCESS)
    {
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                    "Adding Socket Descriptor to Select utility Failed \r\n");
        gsSysLogParams.i4Sockv6Id = MINUS_ONE;
        close (gsSysLogParams.i4Sockv6Id);
        return SYSLOG_FAILURE;
    }

    if (fcntl (i4Sock, F_SETFL, O_NONBLOCK) < SYSLOG_ZERO)
    {

        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                    "Adding Socket Descriptor to Select utility Failed \r\n");
        gsSysLogParams.i4Sockv6Id = MINUS_ONE;
        close (gsSysLogParams.i4Sockv6Id);
        return SYSLOG_FAILURE;
    }

    return SYSLOG_SUCCESS;
}
#endif

/************************************************************************
 *  Function Name   : SysLogTcpV4SockInit
 *  Description     : Function to create BSD Server socket and bind to
 *                    Syslog port
 *  Input           : None
 *  Output          : None
 *  Returns         : Socket descriptor or FAILURE
 ************************************************************************/

INT4
SysLogTcpV4SockInit ()
{
    INT4                i4Sock = SYSLOG_ZERO;
    struct sockaddr_in  SockAddr;

    i4Sock = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < SYSLOG_ZERO)
    {
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                    "SysLog:Failed to create socket\r\n");
        return SYSLOG_FAILURE;
    }

    MEMSET (&SockAddr, SYSLOG_ZERO, sizeof (SockAddr));
    SockAddr.sin_family = AF_INET;
    SockAddr.sin_port = OSIX_HTONS ((UINT2) (gsSysLogParams.u4SysLogPort));
    SockAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
    if (bind (i4Sock, (struct sockaddr *) &SockAddr, sizeof (SockAddr)) <
        SYSLOG_ZERO)
    {
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC, "SysLog:Unable to Bind Socket\r\n");
        return SYSLOG_FAILURE;
    }
    listen (i4Sock, MAX_NO_OF_CONN);

    gsSysLogParams.i4Sockv4Id = i4Sock;

    if (SelAddFd (i4Sock, SysLogTcpPacketOnSocket) != OSIX_SUCCESS)
    {
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                    "Adding Socket Descriptor to Select utility Failed \r\n");
        gsSysLogParams.i4Sockv4Id = MINUS_ONE;
        close (gsSysLogParams.i4Sockv4Id);
        return SYSLOG_FAILURE;
    }
    return SYSLOG_SUCCESS;
}

#ifdef IP6_WANTED
/************************************************************************
 *  Function Name   : SysLogTcpV6SockInit
 *  Description     : Function to create BSD Server socket and bind to
 *                    Syslog port
 *  Input           : None
 *  Output          : None
 *  Returns         : Socket descriptor or FAILURE
 ************************************************************************/

INT4
SysLogTcpV6SockInit ()
{
    INT4                i4Sock = SYSLOG_ZERO;
    INT4                i4OptVal = OSIX_TRUE;
    struct sockaddr_in6 SockAddr;

    i4Sock = socket (AF_INET6, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < SYSLOG_ZERO)
    {
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                    "SysLog:Failed to create socket\r\n");
        return SYSLOG_FAILURE;
    }

    if (setsockopt (i4Sock, IPPROTO_IPV6, IPV6_V6ONLY, (char *) &i4OptVal,
                    sizeof (i4OptVal)))
    {
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                    "SysLog:Unable to set socket option IPV6_V6ONLY\r\n");
        close (i4Sock);
        return SYSLOG_FAILURE;
    }

    MEMSET (&SockAddr, SYSLOG_ZERO, sizeof (SockAddr));
    SockAddr.sin6_family = AF_INET6;
    SockAddr.sin6_port = OSIX_HTONS (SYSLOG_PORT);
    inet_pton (AF_INET6, (const CHR1 *) "0::0", &SockAddr.sin6_addr.s6_addr);

    if (bind (i4Sock, (struct sockaddr *) &SockAddr, sizeof (SockAddr)) <
        SYSLOG_ZERO)
    {
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC, "SysLog:Unable to Bind Socket\r\n");
        close (i4Sock);
        return SYSLOG_FAILURE;
    }
    listen (i4Sock, MAX_NO_OF_CONN);

    gsSysLogParams.i4Sockv6Id = i4Sock;

    if (SelAddFd (i4Sock, SysLogTcpPacketOnV6Socket) != OSIX_SUCCESS)
    {
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                    "Adding Socket Descriptor to Select utility Failed \r\n");
        gsSysLogParams.i4Sockv6Id = MINUS_ONE;
        close (gsSysLogParams.i4Sockv6Id);
        return SYSLOG_FAILURE;
    }

    return SYSLOG_SUCCESS;
}
#endif

/************************************************************************
 *  Function Name   : SysLogRelayEnable
 *  Description     : Function to create BSD Server socket and bind to
 *                    Syslog port
 *  Input           : None
 *  Output          : None
 *  Returns         : Socket descriptor or FAILURE
 ************************************************************************/

INT4
SysLogRelayEnable ()
{

    if (gsSysLogParams.u4RelayTransType == SYSLOG_RELAY_UDP)
    {

        if (SysLogUdpV4SockInit () == SYSLOG_FAILURE)
        {
            return SYSLOG_FAILURE;
        }
#ifdef IP6_WANTED
        if (SysLogUdpV6SockInit () == SYSLOG_FAILURE)
        {
            return SYSLOG_FAILURE;
        }
#endif
    }
    else if (gsSysLogParams.u4RelayTransType == SYSLOG_RELAY_TCP)
    {
        if (SysLogTcpV4SockInit () == SYSLOG_FAILURE)
        {
            return SYSLOG_FAILURE;
        }

#ifdef IP6_WANTED
        if (SysLogTcpV6SockInit () == SYSLOG_FAILURE)
        {
            return SYSLOG_FAILURE;
        }
#endif

    }
    /*BeepServerRegister (SYSLOG_APP_ID, SysLogFromBeepServer); */

    return SYSLOG_SUCCESS;

}

/************************************************************************
 *  Function Name   : SysLogRelayDisable
 *  Description     : Function to create BSD Server socket and bind to
 *                    Syslog port
 *  Input           : None
 *  Output          : None
 *  Returns         : Socket descriptor or FAILURE
 ************************************************************************/

INT4
SysLogRelayDisable ()
{
    if (gsSysLogParams.i4Sockv4Id != MINUS_ONE)
    {
        SelRemoveFd (gsSysLogParams.i4Sockv4Id);
        close (gsSysLogParams.i4Sockv4Id);
    }
#if defined (IP6_WANTED)
    if (gsSysLogParams.i4Sockv6Id != MINUS_ONE)
    {
        SelRemoveFd (gsSysLogParams.i4Sockv6Id);
        close (gsSysLogParams.i4Sockv6Id);
    }
#endif /* IP6_WANTED */
    return SYSLOG_SUCCESS;
}

/******************************************************************************
 * Function Name   : SysLogPacketOnSocket
 * Description     : Call back function from SelAddFd(), when syslog packet is
 *                   received
 * Inputs          : None
ysLogParams.i4SockId
 * Returns         : None.
 ******************************************************************************/
VOID
SysLogTcpPacketOnSocket (INT4 i4SockFd)
{
    if (i4SockFd == gsSysLogParams.i4Sockv4Id)
    {
        if (OsixSendEvent
            (SELF, (const UINT1 *) SMTP_TASK_NAME,
             SYSLOG_TCP_IPV4_MSG_EVENT) != OSIX_SUCCESS)
        {
            SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC, " Event send Failed\r\n");
        }
    }
    return;
}

/******************************************************************************
 * Function Name   : SysLogPacketOnSocket
 * Description     : Call back function from SelAddFd(), when syslog packet is
 *                   received
 * Inputs          : None
 * Output          : Sends an event to Syslog task
 * Returns         : None.
 ******************************************************************************/
VOID
SysLogPacketOnSocket (INT4 i4SockFd)
{

    if (i4SockFd == gsSysLogParams.i4Sockv4Id)
    {
        if (OsixSendEvent
            (SELF, (const UINT1 *) SMTP_TASK_NAME,
             SYSLOG_IPV4_PKT_EVENT) != OSIX_SUCCESS)
        {
            SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC, " Event send Failed\r\n");
        }
    }

    return;
}

/******************************************************************************
 * Function Name   : SysLogPacketOnSocket6
 * Description     : Call back function from SelAddFd(), when syslog packet is
 *                   received
 * Inputs          : None
 * Output          : Sends an event to Syslog task
 * Returns         : None.
 ******************************************************************************/
VOID
SysLogPacketOnSocket6 (INT4 i4SockFd)
{

#ifdef IP6_WANTED
    if (i4SockFd == gsSysLogParams.i4Sockv6Id)
    {
        if (OsixSendEvent
            (SELF, (const UINT1 *) SMTP_TASK_NAME,
             SYSLOG_IPV6_PKT_EVENT) != OSIX_SUCCESS)
        {
            SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC, " Event send Failed\r\n");
        }
    }
#endif
    UNUSED_PARAM (i4SockFd);

}

/******************************************************************************
 * Function Name   : SysLogPacketOnSocket6
 * Description     : Call back function from SelAddFd(), when syslog packet is
 *                   received
 * Inputs          : None
 * Output          : Sends an event to Syslog task
 * Returns         : None.
 ******************************************************************************/
VOID
SysLogTcpPacketOnV6Socket (INT4 i4SockFd)
{

#ifdef IP6_WANTED
    if (i4SockFd == gsSysLogParams.i4Sockv6Id)
    {
        if (OsixSendEvent
            (SELF, (const UINT1 *) SMTP_TASK_NAME,
             SYSLOG_TCP_IPV6_MSG_EVENT) != OSIX_SUCCESS)
        {
            SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC, " Event send Failed\r\n");
        }
    }
#endif
    UNUSED_PARAM (i4SockFd);

}

/******************************************************************************
 * Function Name   : SysLogProcessPacket
 * Description     : Call back function from SelAddFd(), when syslog packet is
 *                   received
 * Inputs          : None
 * Output          : Sends an event to Syslog task
 * Returns         : None.
 ******************************************************************************/

VOID
SysLogProcessPkts (VOID)
{
    UINT1              *pBuf = NULL;

    while (OsixQueRecv (gSmtpQueId, (UINT1 *) &pBuf,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        SysLogSmtpSendEmail (pBuf, STRLEN (pBuf));
        MemReleaseMemBlock (gSysLogGlobalInfo.sysQueMsgPoolId, (UINT1 *) pBuf);
    }

}

/******************************************************************************
 * Function Name   : SysLogProcessStdbyMsg
 * Description     : This function is used to receive the message from active
 *                   and send to syslog server
 * Inputs          : None
 * Output          : Sends an event to Syslog task
 * Returns         : None.
 ******************************************************************************/

VOID
SysLogProcessStdbyMsg (VOID)
{
    UINT1              *pBuf;
    INT4                i4count = 0;
    INT4                i4PriValue = SYSLOG_ZERO;
    UINT4               u4Value = SYSLOG_ZERO;
    INT4                i4Len = 0;

    while (OsixQueRecv (gStdbyQueId, (UINT1 *) &pBuf,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        MEMCPY (&u4Value, (pBuf), sizeof (UINT4));
        u4Value = OSIX_NTOHL (u4Value);
        i4PriValue = (INT4) u4Value;
        for (i4count = 0; i4count < MAX_LOG_SIZE_STDBY; i4count++)
        {
            SysSendLog (i4PriValue, (INT1 *) (pBuf + i4Len + sizeof (UINT4)));
            i4Len += MAX_LOG_CHR_STDBY;

        }
        MemReleaseMemBlock (gSysLogGlobalInfo.SysLogStdbyPoolId, pBuf);
    }

}

/******************************************************************************
 * Function Name   : SysLogTcpRcvdIpv4Pkt
 * Description     : Call back function from SelAddFd(), when tcp syslog packet is
 *                   received
 * Inputs          : None
 * Output          : Sends an event to Syslog task
 * Returns         : None.
 ******************************************************************************/

VOID
SysLogTcpRcvdIpv4Pkt (UINT4 u4Events)
{
    UINT1              *pu1SysLogPkt = NULL;
    UINT1              *u1char = NULL;
    INT4                i4PktSize = SYSLOG_ZERO;
    INT4                i4Socket = SYSLOG_ZERO;
    INT4                iRetVal = SYSLOG_ZERO;
    INT4                i4ConnSock = SYSLOG_ZERO;
    INT4                i4len = SYSLOG_ZERO;
    INT4                i4Index = SYSLOG_ZERO;
    struct sockaddr_in  CliAddr;

    UNUSED_PARAM (u4Events);
    UNUSED_PARAM (iRetVal);

    pu1SysLogPkt = MemAllocMemBlk (gSysLogGlobalInfo.SysLogUMemPoolId);
    if (NULL == pu1SysLogPkt)
    {

        return;
    }
    MEMSET (pu1SysLogPkt, 0, SYSLOG_MAX_PKT_LEN + SYSLOG_FIFTY);

    i4Socket = gsSysLogParams.i4Sockv4Id;

    i4len = sizeof (CliAddr);

    i4ConnSock =
        accept (i4Socket, (struct sockaddr *) &CliAddr, (socklen_t *) & i4len);
    if (i4ConnSock < SYSLOG_ZERO)
    {
        MemReleaseMemBlock (gSysLogGlobalInfo.SysLogUMemPoolId, pu1SysLogPkt);
        return;
    }

    while (recv (i4ConnSock, &u1char, SYSLOG_ONE, SYSLOG_ZERO) > SYSLOG_ZERO)
    {
        if (u1char == (UINT1 *) '\r')
        {
            i4Index++;
            break;
        }
        else
        {
            MEMCPY (&pu1SysLogPkt[i4Index], (UINT1 *) &u1char, SYSLOG_ONE);
            i4Index++;
        }
    }

    pu1SysLogPkt[i4Index] = '\n';

    if (STRLEN (pu1SysLogPkt) != SYSLOG_ZERO)
    {
        iRetVal = SysLogMsgFromV4Client (pu1SysLogPkt, i4PktSize);
    }

    close (gsSysLogParams.i4Sockv4Id);
    SysLogTcpV4SockInit ();
    MemReleaseMemBlock (gSysLogGlobalInfo.SysLogUMemPoolId, pu1SysLogPkt);
    return;

}

/******************************************************************************
 * Function Name   : SysLogTcpRcvdIpv6Pkt
 * Description     : Call back function from SelAddFd(), when tcp syslog packet is
 *                   received
 * Inputs          : None
 * Output          : Sends an event to Syslog task
 * Returns         : None.
 ******************************************************************************/

VOID
SysLogTcpRcvdIpv6Pkt (UINT4 u4Events)
{
    UINT1              *pu1SysLogPkt = NULL;
    UINT1              *u1char = NULL;
    INT4                i4PktSize = SYSLOG_ZERO;
    INT4                i4Socket = SYSLOG_ZERO;
    INT4                iRetVal = SYSLOG_ZERO;
    INT4                i4ConnSock = SYSLOG_ZERO;
    INT4                i4len = SYSLOG_ZERO;
    INT4                i4Index = SYSLOG_ZERO;
    struct sockaddr_in  CliAddr;

    UNUSED_PARAM (u4Events);
    UNUSED_PARAM (iRetVal);
    pu1SysLogPkt = MemAllocMemBlk (gSysLogGlobalInfo.SysLogUMemPoolId);

    if (pu1SysLogPkt == NULL)
    {

        return;
    }
    MEMSET (pu1SysLogPkt, SYSLOG_ZERO, SYSLOG_MAX_PKT_LEN + SYSLOG_FIFTY);

    i4Socket = gsSysLogParams.i4Sockv6Id;

    i4len = sizeof (CliAddr);

    i4ConnSock =
        accept (i4Socket, (struct sockaddr *) &CliAddr, (socklen_t *) & i4len);
    if (i4ConnSock < SYSLOG_ZERO)
    {
        MemReleaseMemBlock (gSysLogGlobalInfo.SysLogUMemPoolId, pu1SysLogPkt);
        return;
    }
    while (recv (i4ConnSock, &u1char, SYSLOG_ONE, SYSLOG_ZERO) > SYSLOG_ZERO)
    {
        if (u1char == (UINT1 *) '\r')
        {
            i4Index++;
            break;
        }
        else
        {
            MEMCPY (&pu1SysLogPkt[i4Index], (UINT1 *) &u1char, SYSLOG_ONE);
            i4Index++;
        }
    }

    pu1SysLogPkt[i4Index] = '\n';

    if (STRLEN (pu1SysLogPkt) != SYSLOG_ZERO)
    {
        iRetVal = SysLogMsgFromV4Client (pu1SysLogPkt, i4PktSize);
    }

    close (gsSysLogParams.i4Sockv6Id);
#ifdef IP6_WANTED
    SysLogTcpV6SockInit ();
#endif
    MemReleaseMemBlock (gSysLogGlobalInfo.SysLogUMemPoolId, pu1SysLogPkt);
    return;

}

/******************************************************************************
 * Function Name   : SysLogRcvdIpv4Pkt
 * Description     : Call back function from SelAddFd(), when syslog packet is
 *                   received
 * Inputs          : None
 * Output          : Sends an event to Syslog task
 * Returns         : None.
 ******************************************************************************/

VOID
SysLogRcvdIpv4Pkt (UINT4 u4Events)
{
    UINT1              *pu1SysLogPkt = NULL;
    INT4                i4PktSize = SYSLOG_ZERO;
    INT4                i4Socket = SYSLOG_ZERO;
    INT4                iRetVal = SYSLOG_ZERO;
    UNUSED_PARAM (iRetVal);

    pu1SysLogPkt = MemAllocMemBlk (gSysLogGlobalInfo.SysLogUMemPoolId);
    if (pu1SysLogPkt == NULL)
    {
        return;
    }

    MEMSET (pu1SysLogPkt, SYSLOG_ZERO, SYSLOG_MAX_PKT_LEN + SYSLOG_FIFTY);

    if (u4Events & SYSLOG_IPV4_PKT_EVENT)
    {
        i4Socket = gsSysLogParams.i4Sockv4Id;
    }

    if ((i4PktSize =
         recv (i4Socket, pu1SysLogPkt, SYSLOG_MAX_PKT_LEN,
               SYSLOG_ZERO)) > SYSLOG_ZERO)

    {
        iRetVal = SysLogMsgFromV4Client (pu1SysLogPkt, i4PktSize);
    }
    MemReleaseMemBlock (gSysLogGlobalInfo.SysLogUMemPoolId, pu1SysLogPkt);
    return;

}

/******************************************************************************
 * Function Name   : SysLogMsgFromV4Client
 * Description     : Receives the syslog message from v4 client
 *
 * Inputs          : syslog message and packet length
 * Output          : parse the syslog message and displays on the console,forward,
                     mail the syslog message.
 * Returns         : syslog success or failure
 ******************************************************************************/

INT4
SysLogMsgFromV4Client (UINT1 *pu1SysLogPkt, UINT4 u4SyslogPktLen)
{
    INT1                i1PriFlag = SYSLOG_ZERO;
    INT1                i1TimeFlag = SYSLOG_ZERO;
    INT4                i4PriValue = SYSLOG_ZERO;
    INT4                i4MessLen = SYSLOG_ZERO;
    INT4                i4RetVal = SYSLOG_SUCCESS;
    UINT1              *pu1ModName = NULL;
    UINT4               u4FileNo = SYSLOG_ZERO;
    UINT4               u4ModuleId = SYSLOG_ZERO;

    /*pu1SysLogPkt = "ISS CLI Dec 35 14:53:31 2008 User root logged in"; */

    if (pu1SysLogPkt == NULL)
    {
        return SYSLOG_FAILURE;
    }

    i4RetVal =
        SysLogParser (&i4PriValue, &i1PriFlag, &i1TimeFlag,
                      (CHR1 *) pu1SysLogPkt);
    if (i4RetVal == SYSLOG_FAILURE)
    {
        return SYSLOG_FAILURE;
    }
    i4MessLen = STRLEN (pu1SysLogPkt);

    /*Check whether the message length is greater than 1024 */

    if (i4MessLen > SYSLOG_MAX_STRING)
    {
        MEMSET (gau1SysMess, SYSLOG_ZERO, SYSLOG_MAX_STRING);
        MEMCPY (gau1SysMess, pu1SysLogPkt, SYSLOG_MAX_STRING);
        MEMSET (pu1SysLogPkt, SYSLOG_ZERO, SYSLOG_MAX_STRING);
        MEMCPY (pu1SysLogPkt, gau1SysMess, SYSLOG_MAX_STRING);
    }

    if (gsSysLogParams.u4LogConsole == SYSLOG_ENABLE)
    {
        SYSLOG_DBG (ALL_FAILURE_TRC, (CHR1 *) pu1SysLogPkt);
        gsSysLogParams.u4ConsoleLogs++;
    }
    if (pu1SysLogPkt != NULL)
    {
        SysSendLog ((UINT4) i4PriValue, (INT1 *) pu1SysLogPkt);
    }
    if (gsSysLogParams.u4MailOption == SYSLOG_ENABLE)
    {
        SysLogSendMailAlert (i4PriValue, (UINT1 *) pu1SysLogPkt);
    }
    if (gsSysLogParams.u4LocalStorage == SYSLOG_ENABLE)
    {
        SysLogStoreInFile ((UINT4) i4PriValue, (UINT1 *) pu1SysLogPkt,
                           pu1ModName, u4FileNo, u4ModuleId);
    }

    UNUSED_PARAM (u4SyslogPktLen);

    return SYSLOG_SUCCESS;
}

#ifdef IP6_WANTED

/******************************************************************************
 * Function Name   : SysLogRcvdIpv6Pkt
 * Description     : Call back function from SelAddFd(), when syslog packet is
 *                   received
 * Inputs          : None
 * Output          : Sends an event to Syslog task
 * Returns         : None.
 ******************************************************************************/

VOID
SysLogRcvdIpv6Pkt (UINT4 u4Events)
{
    UINT1              *pu1SysLogPkt = NULL;
    UINT4               u4Pkt6Size = SYSLOG_ZERO;
    UINT4               u4Len6 = SYSLOG_ZERO;
    UINT4               u4PktSize = SYSLOG_ZERO;
    INT4                i4Socket = SYSLOG_ZERO;
    INT4                i4RetVal = SYSLOG_ZERO;
    struct sockaddr_in6 SysLogClientAddr6;

    UNUSED_PARAM (u4Pkt6Size);

    pu1SysLogPkt = MemAllocMemBlk (gSysLogGlobalInfo.SysLogUMemPoolId);
    if (pu1SysLogPkt == NULL)
    {
        return;
    }
    MEMSET (pu1SysLogPkt, 0, SYSLOG_MAX_PKT_LEN + SYSLOG_FIFTY);
    if (u4Events & SYSLOG_IPV6_PKT_EVENT)
    {
        i4Socket = gsSysLogParams.i4Sockv6Id;
    }

    MEMSET (&SysLogClientAddr6, SYSLOG_ZERO, sizeof (SysLogClientAddr6));
    SysLogClientAddr6.sin6_family = AF_INET6;
    SysLogClientAddr6.sin6_port = OSIX_HTONS (SYSLOG_ZERO);
    inet_pton (AF_INET6, (const CHR1 *) "0::0",
               &SysLogClientAddr6.sin6_addr.s6_addr);
    u4Len6 = sizeof (SysLogClientAddr6);

    while ((u4PktSize = recv (i4Socket, pu1SysLogPkt, SYSLOG_MAX_PKT_LEN,
                              SYSLOG_ZERO)) > SYSLOG_ZERO)
    {

        i4RetVal = SysLogMsgFromV6Client (pu1SysLogPkt, u4PktSize);
    }

    MemReleaseMemBlock (gSysLogGlobalInfo.SysLogUMemPoolId, pu1SysLogPkt);

    UNUSED_PARAM (i4RetVal);
    UNUSED_PARAM (u4Len6);
    return;
}

/******************************************************************************
 * Function Name   : SysLogMsgFromV6Client
 * Description     : Receives the syslog message from v6 client
 *                   
 * Inputs          : syslog message and packet length
 * Output          : parse the syslog message and displays on the console,forward,
                     mail the syslog message.                    
 * Returns         : syslog success or failure
 ******************************************************************************/

INT4
SysLogMsgFromV6Client (UINT1 *pu1SysLogPkt, UINT4 u4SyslogPktLen)
{
    INT1                i1PriFlag = SYSLOG_ZERO;
    INT1                i1TimeFlag = SYSLOG_ZERO;
    INT4                i4PriValue = SYSLOG_ZERO;
    INT4                i4MessLen = SYSLOG_ZERO;
    INT4                i4RetVal = SYSLOG_SUCCESS;
    UINT1              *pu1ModName = NULL;
    UINT4               u4FileNo = SYSLOG_ZERO;
    UINT4               u4ModuleId = SYSLOG_ZERO;
    UNUSED_PARAM (u4SyslogPktLen);

    i4RetVal =
        SysLogParser (&i4PriValue, &i1PriFlag, &i1TimeFlag,
                      (CHR1 *) pu1SysLogPkt);
    if (i4RetVal == SYSLOG_FAILURE)
    {
        return SYSLOG_FAILURE;
    }

    i4MessLen = STRLEN (pu1SysLogPkt);

    /*Check whether the message length is greater than 1024 */

    if (i4MessLen > SYSLOG_MAX_STRING)
    {
        MEMSET (gau1SysMess, SYSLOG_ZERO, SYSLOG_MAX_STRING);
        MEMCPY (gau1SysMess, pu1SysLogPkt, SYSLOG_MAX_STRING);
        MEMSET (pu1SysLogPkt, SYSLOG_ZERO, SYSLOG_MAX_STRING);
        MEMCPY (pu1SysLogPkt, gau1SysMess, SYSLOG_MAX_STRING);
    }

    if (gsSysLogParams.u4LogConsole == SYSLOG_ENABLE)
    {
        SYSLOG_DBG (ALL_FAILURE_TRC, (CHR1 *) pu1SysLogPkt);
        gsSysLogParams.u4ConsoleLogs++;
    }

    SysSendLog (i4PriValue, (INT1 *) pu1SysLogPkt);

    if (gsSysLogParams.u4MailOption == SYSLOG_ENABLE)
    {
        SysLogSendMailAlert (i4PriValue, (UINT1 *) pu1SysLogPkt);
    }

    if (gsSysLogParams.u4LocalStorage == SYSLOG_ENABLE)
    {
        SysLogStoreInFile ((UINT4) i4PriValue, (UINT1 *) pu1SysLogPkt,
                           pu1ModName, u4FileNo, u4ModuleId);
    }

    return SYSLOG_SUCCESS;
}

#endif

/******************************************************************************
 * Function Name   : SysLogProcessTcpRcvdMsg
 * Description     : This function processes the syslog message enqueued in the
 *                   queue.
 * Inputs          : None
 * Output          : None
 * Returns         : None
 ******************************************************************************/

VOID
SysLogProcessTcpRcvdMsg ()
{
    INT1                i1PriFlag = SYSLOG_ZERO;
    INT1                i1TimeFlag = SYSLOG_ZERO;
    INT4                i4PriValue = SYSLOG_ZERO;
    INT4                i4RetVal = SYSLOG_SUCCESS;
    INT4                i4DnsRetVal = DNS_NOT_RESOLVED;
    UINT1              *pMessage = NULL;
    tSysLogFwdInfo     *pFwdInfoNode = NULL;
    tDNSResolvedIpInfo  IpInfo;
    tSysLogFwdInfo      ResolAddr;

    MEMSET (&IpInfo, SYSLOG_ZERO, sizeof (tDNSResolvedIpInfo));

    while (OsixQueRecv
           (gSyslogTcpQueId, (UINT1 *) &pMessage, OSIX_DEF_MSG_LEN,
            OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pMessage == NULL)
        {
            continue;
        }

        /* Extracting the Message priority from the message */
        i4RetVal =
            SysLogParser (&i4PriValue, &i1PriFlag, &i1TimeFlag,
                          (CHR1 *) pMessage);
        if (i4RetVal == SYSLOG_FAILURE)
        {
            MemReleaseMemBlock (gSysLogGlobalInfo.SysLogCMemPoolId,
                                (UINT1 *) pMessage);
            continue;
        }

        /* Parsing the syslog global setting for server configuration */
        pFwdInfoNode = NULL;
        TMO_SLL_Scan (&gSysLogGlobalInfo.sysFwdTblList, pFwdInfoNode,
                      tSysLogFwdInfo *)
        {
            MEMSET (&ResolAddr, SYSLOG_ZERO, sizeof (tSysLogFwdInfo));
            pFwdInfoNode = GET_FWDENTRY (pFwdInfoNode);
            if (pFwdInfoNode == NULL)
            {
                continue;
            }
            ResolAddr.u4Port = pFwdInfoNode->u4Port;
            ResolAddr.i4SockFd = pFwdInfoNode->i4SockFd;
            ResolAddr.u4AddrType = pFwdInfoNode->u4AddrType;
            if (pFwdInfoNode->u4AddrType == IPVX_DNS_FAMILY)
            {
                if ((i4DnsRetVal =
                     FsUtlIPvXResolveHostName (pFwdInfoNode->au1HostName,
                                               DNS_NONBLOCK,
                                               &IpInfo)) == DNS_RESOLVED)
                {
                    if ((IpInfo.Resolv4Addr.u1AddrLen != 0)
                        || (IpInfo.Resolv6Addr.u1AddrLen != 0))
                    {
                        if (IpInfo.Resolv6Addr.u1AddrLen != 0)
                        {
                            ResolAddr.u4AddrType = IPVX_ADDR_FMLY_IPV6;
                            IPVX_ADDR_COPY (&(ResolAddr.ServIpAddr),
                                            &(IpInfo.Resolv6Addr));
                        }
                        else
                        {
                            ResolAddr.u4AddrType = IPVX_ADDR_FMLY_IPV4;
                            IPVX_ADDR_COPY (&(ResolAddr.ServIpAddr),
                                            &(IpInfo.Resolv4Addr));

                        }
                    }
                }
                else
                {
                    switch (i4DnsRetVal)
                    {
                        case DNS_IN_PROGRESS:
                            SYSLOG_DBG (SYSLOG_MGMT_TRC |
                                        SYSLOG_CONTROL_PATH_TRC,
                                        "SysLogProcessTcpRcvdMsg: Host name resolution in Progress\n");
                            break;
                        case DNS_CACHE_FULL:
                            SYSLOG_DBG (SYSLOG_MGMT_TRC |
                                        SYSLOG_CONTROL_PATH_TRC,
                                        "SysLogProcessTcpRcvdMsg: DNS cache full, cannot resolve at the moment\n");
                            break;
                        default:
                            SYSLOG_DBG (SYSLOG_MGMT_TRC |
                                        SYSLOG_CONTROL_PATH_TRC,
                                        "SysLogProcessTcpRcvdMsg: Cannot resolve the host name \n");
                            break;
                    }
                    continue;
                }

            }
            else
            {
                IPVX_ADDR_COPY (&(ResolAddr.ServIpAddr),
                                &(pFwdInfoNode->ServIpAddr));

            }

            if ((pFwdInfoNode->u4Priority) == (UINT4) i4PriValue)
            {

                if (ResolAddr.u4AddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    SendLogMsgV4TcpServer (&ResolAddr, (INT1 *) pMessage);
                }
                else if (ResolAddr.u4AddrType == IPVX_ADDR_FMLY_IPV6)
                {
                    SendLogMsgV6TcpServer (&ResolAddr, (INT1 *) pMessage);
                }

                break;
            }
        }
        MemReleaseMemBlock (gSysLogGlobalInfo.SysLogCMemPoolId,
                            (UINT1 *) pMessage);
    }
}

/******************************************************************************
 * Function Name   : SysLogBeepRcvdMsg
 * Description     : Receive the syslog message from beep server
 *
 * Inputs          : None
 * Output          :
 * Returns         : None.
 ******************************************************************************/
VOID
SysLogBeepRcvdMsg ()
{
    INT1                i1PriFlag = SYSLOG_ZERO;
    INT1                i1TimeFlag = SYSLOG_ZERO;
    INT4                i4PriValue = SYSLOG_ZERO;
    INT4                i4MessLen = SYSLOG_ZERO;
    INT4                i4RetVal = SYSLOG_SUCCESS;
    UINT1              *pMessage = NULL;
    UINT1              *pu1ModName = NULL;
    UINT4               u4FileNo = SYSLOG_ZERO;
    UINT4               u4ModuleId = SYSLOG_ZERO;

    while (OsixQueRecv
           (gSmtpQueId, (UINT1 *) &pMessage, OSIX_DEF_MSG_LEN,
            OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pMessage == NULL)
        {
            continue;
        }
        i4RetVal =
            SysLogParser (&i4PriValue, &i1PriFlag, &i1TimeFlag,
                          (CHR1 *) pMessage);
        if (i4RetVal == SYSLOG_FAILURE)
        {
            return;
        }
        i4MessLen = STRLEN (pMessage);

        /*Check whether the message length is greater than 1024 */

        if (i4MessLen > SYSLOG_MAX_STRING)
        {
            MEMSET (gau1SysMess, SYSLOG_ZERO, SYSLOG_MAX_STRING);
            MEMCPY (gau1SysMess, pMessage, SYSLOG_MAX_STRING);
            MEMSET (pMessage, SYSLOG_ZERO, SYSLOG_MAX_STRING);
            MEMCPY (pMessage, gau1SysMess, SYSLOG_MAX_STRING);
        }

        if (gsSysLogParams.u4LogConsole == SYSLOG_ENABLE)
        {
            SYSLOG_DBG (ALL_FAILURE_TRC, (CHR1 *) pMessage);
            gsSysLogParams.u4ConsoleLogs++;
        }
        if (pMessage != NULL)
        {
            SysSendLog (i4PriValue, (INT1 *) pMessage);
        }
        if (gsSysLogParams.u4MailOption == SYSLOG_ENABLE)
        {
            SysLogSendMailAlert (i4PriValue, (UINT1 *) pMessage);
        }
        if (gsSysLogParams.u4LocalStorage == SYSLOG_ENABLE)
        {
            SysLogStoreInFile ((UINT4) i4PriValue, (UINT1 *) pMessage,
                               pu1ModName, u4FileNo, u4ModuleId);
        }

    }
}

/******************************************************************************
 * Function Name   : SysLogParser
 * Description     : check whether the syslog message is in bsd format, if not 
                     convert the message to bsd format.
 *
 * Inputs          : pointer to priority value,priority flag,time flag and 
                     syslog message
 * Output          : parse the syslog message and conforms it the bsd format
 * Returns         : SYSLOG_SUCCESS or SYSLOG_FAILURE
 ******************************************************************************/

INT4
SysLogParser (INT4 *pi4PriValue, INT1 *pi1PriFlag,
              INT1 *pi1TimeFlag, CHR1 * pc1SysLogPkt)
{

    CHR1                ac1Month[MONTH_IND];
    CHR1               *pc1SysMsg = NULL;
    CHR1               *pc1TempSysMsg = NULL;
    INT4                i4Len = SYSLOG_ZERO;
    INT4                i4Index = SYSLOG_ZERO;
    INT4                i4Priority = MINUS_ONE;
    INT4                i4MonthIndex = SYSLOG_ZERO;
    INT4                i4LoopIndex = SYSLOG_ZERO;

    pc1SysMsg = (CHR1 *) MemAllocMemBlk (gSysLogGlobalInfo.SysLogCMemPoolId);
    if (pc1SysMsg == NULL)
    {
        return SYSLOG_FAILURE;
    }

    MEMSET (pc1SysMsg, SYSLOG_ZERO, SYSLOG_MESS_LENGTH + SYSLOG_FIFTY);
    MEMCPY (pc1SysMsg, pc1SysLogPkt, STRLEN (pc1SysLogPkt));

    /*To check for the month */
    for (i4LoopIndex = SYSLOG_ZERO; i4LoopIndex <= SYSLOG_FIVE; i4LoopIndex++)
    {
        if ((isalpha (pc1SysMsg[i4LoopIndex])) >= SYSLOG_TRUE)
        {
            i4MonthIndex = i4LoopIndex;
            break;
        }
    }
    if (i4MonthIndex == SYSLOG_ZERO)
    {
        ac1Month[SYSLOG_ZERO] = pc1SysMsg[i4MonthIndex];
        ac1Month[SYSLOG_ONE] = pc1SysMsg[i4MonthIndex + SYSLOG_ONE];
        ac1Month[SYSLOG_TWO] = pc1SysMsg[i4MonthIndex + SYSLOG_TWO];
        ac1Month[SYSLOG_THREE] = '\0';

    }
    if ((pc1SysMsg[SYSLOG_ZERO] != '<') && (pc1SysMsg[SYSLOG_TWO] != '>') &&
        (pc1SysMsg[SYSLOG_THREE] != '>') && (pc1SysMsg[SYSLOG_FOUR] != '>') &&
        (i4MonthIndex != SYSLOG_ZERO))

    {
        ac1Month[SYSLOG_ZERO] = pc1SysMsg[i4MonthIndex];
        ac1Month[SYSLOG_ONE] = pc1SysMsg[i4MonthIndex + SYSLOG_ONE];
        ac1Month[SYSLOG_TWO] = pc1SysMsg[i4MonthIndex + SYSLOG_TWO];
        ac1Month[SYSLOG_THREE] = '\0';

    }
    if ((pc1SysMsg[SYSLOG_ZERO] != '<'))
    {
        *pi1PriFlag = SYS_SET_PRI_FLAG;
    }

    else if ((pc1SysMsg[SYSLOG_TWO] != '>'))
    {
        if (pc1SysMsg[SYSLOG_THREE] != '>')
        {
            if (pc1SysMsg[SYSLOG_FOUR] != '>')
            {
                *pi1PriFlag = SYS_SET_PRI_FLAG;
                ac1Month[SYSLOG_ZERO] = pc1SysMsg[i4MonthIndex];
                ac1Month[SYSLOG_ONE] = pc1SysMsg[i4MonthIndex + SYSLOG_ONE];
                ac1Month[SYSLOG_TWO] = pc1SysMsg[i4MonthIndex + SYSLOG_TWO];
                ac1Month[SYSLOG_THREE] = '\0';

            }
        }
        if (pc1SysMsg[SYSLOG_THREE] == '>')
        {
            i4Index = SYSLOG_FOUR;
            if ((isalpha (pc1SysMsg[SYSLOG_ONE])) >= SYSLOG_TRUE)
            {
                *pi1PriFlag = SYS_SET_PRI_FLAG;
            }

            else if ((isalpha (pc1SysMsg[SYSLOG_TWO])) >= SYSLOG_TRUE)
            {
                *pi1PriFlag = SYS_SET_PRI_FLAG;
            }
            i4Priority =
                ((pc1SysMsg[SYSLOG_ONE] - '0') * SYSLOG_TEN) +
                (pc1SysMsg[SYSLOG_TWO] - '0');
            ac1Month[SYSLOG_ZERO] = pc1SysMsg[i4Index];
            ac1Month[SYSLOG_ONE] = pc1SysMsg[i4Index + SYSLOG_ONE];
            ac1Month[SYSLOG_TWO] = pc1SysMsg[i4Index + SYSLOG_TWO];
            ac1Month[SYSLOG_THREE] = '\0';
        }

        else if (pc1SysMsg[SYSLOG_FOUR] == '>')
        {
            i4Index = SYSLOG_FIVE;
            if ((isalpha (pc1SysMsg[SYSLOG_ONE])) >= SYSLOG_TRUE)
            {
                *pi1PriFlag = SYS_SET_PRI_FLAG;
            }
            else if ((isalpha (pc1SysMsg[SYSLOG_TWO])) >= SYSLOG_TRUE)
            {
                *pi1PriFlag = SYS_SET_PRI_FLAG;
            }
            else if ((isalpha (pc1SysMsg[SYSLOG_THREE])) >= SYSLOG_TRUE)
            {
                *pi1PriFlag = SYS_SET_PRI_FLAG;
            }
            i4Priority = ((pc1SysMsg[SYSLOG_ONE] - '0') * SYSLOG_HUNDRED) +
                ((pc1SysMsg[SYSLOG_TWO] - '0') * SYSLOG_TEN) +
                (pc1SysMsg[SYSLOG_THREE] - '0');
            ac1Month[SYSLOG_ZERO] = pc1SysMsg[i4Index];
            ac1Month[SYSLOG_ONE] = pc1SysMsg[i4Index + SYSLOG_ONE];
            ac1Month[SYSLOG_TWO] = pc1SysMsg[i4Index + SYSLOG_TWO];
            ac1Month[SYSLOG_THREE] = '\0';

        }

    }

    if (pc1SysMsg[SYSLOG_TWO] == '>')
    {
        i4Index = SYSLOG_THREE;
        if ((isalpha (pc1SysMsg[SYSLOG_ONE])) >= SYSLOG_TRUE)
        {
            *pi1PriFlag = SYS_SET_PRI_FLAG;
        }
        i4Priority = pc1SysMsg[SYSLOG_ONE] - '0';
        ac1Month[SYSLOG_ZERO] = pc1SysMsg[i4Index];
        ac1Month[SYSLOG_ONE] = pc1SysMsg[i4Index + SYSLOG_ONE];
        ac1Month[SYSLOG_TWO] = pc1SysMsg[i4Index + SYSLOG_TWO];
        ac1Month[SYSLOG_THREE] = '\0';
        i4MonthIndex = SYSLOG_THREE;

    }

    if (!
        ((i4Priority >= SYSLOG_PRIORITY_MIN)
         && (i4Priority <= SYSLOG_PRIORITY_MAX)))
    {
        *pi4PriValue = DEFAULT_PRIORITY;
        *pi1PriFlag = SYS_SET_PRI_FLAG;
    }
    else
    {
        *pi4PriValue = i4Priority;
    }
    *pi1TimeFlag = (INT1) CheckTimeStamp (i4MonthIndex, ac1Month, pc1SysLogPkt);
/*In Relay Case Priority,TimeStamp,Host name Not attached .just forwarding actual syslog message */
    if ((gsSysLogParams.u4RelayTransType != SYSLOG_RELAY_UDP) &&
        (gsSysLogParams.u4RelayTransType != SYSLOG_RELAY_TCP))
    {
        pc1TempSysMsg =
            (CHR1 *) MemAllocMemBlk (gSysLogGlobalInfo.SysLogCMemPoolId);
        if (pc1TempSysMsg == NULL)
        {
            MemReleaseMemBlock (gSysLogGlobalInfo.SysLogCMemPoolId,
                                (UINT1 *) pc1SysMsg);
            return SYSLOG_FAILURE;
        }
        MEMSET (pc1TempSysMsg, 0, SYSLOG_MAX_PKT_LEN + SYSLOG_FIFTY);

        if ((*pi1PriFlag == SYS_SET_PRI_FLAG)
            && (*pi1TimeFlag == SYS_SET_TIME_FLAG))
        {

            MEMCPY (pc1TempSysMsg, "<13>", STRLEN ("<13>"));
            i4Len = STRLEN (pc1TempSysMsg);

            SysLogGetTimeStr (pc1TempSysMsg + i4Len);
            i4Len = STRLEN (pc1TempSysMsg);

            MEMCPY (pc1TempSysMsg + i4Len, "ISS ", STRLEN ("ISS"));
            i4Len = STRLEN (pc1TempSysMsg);

            MEMCPY (pc1TempSysMsg + i4Len, pc1SysLogPkt, STRLEN (pc1SysLogPkt));
            MEMSET (pc1SysLogPkt, 0, STRLEN (pc1TempSysMsg));
            MEMCPY (pc1SysLogPkt, pc1TempSysMsg, STRLEN (pc1TempSysMsg));
        }
        else if (*pi1PriFlag == SYS_SET_PRI_FLAG)
        {

            MEMCPY (pc1TempSysMsg, "<13>", STRLEN ("<13>"));
            i4Len = STRLEN (pc1TempSysMsg);

            SysLogGetTimeStr (pc1TempSysMsg + i4Len);
            i4Len = STRLEN (pc1TempSysMsg);

            MEMCPY (pc1TempSysMsg + i4Len, "ISS ", STRLEN ("ISS"));
            i4Len = STRLEN (pc1TempSysMsg);

            MEMCPY (pc1TempSysMsg + i4Len, pc1SysLogPkt, STRLEN (pc1SysLogPkt));

            MEMSET (pc1SysLogPkt, 0, STRLEN (pc1TempSysMsg));
            MEMCPY (pc1SysLogPkt, pc1TempSysMsg, STRLEN (pc1TempSysMsg));

        }
        else if (*pi1TimeFlag == SYS_SET_TIME_FLAG)
        {
            MEMSET (pc1TempSysMsg, 0, SYSLOG_MAX_PKT_LEN + SYSLOG_FIFTY);
            MEMCPY (pc1TempSysMsg, pc1SysLogPkt, i4MonthIndex);
            pc1TempSysMsg[i4MonthIndex + 1] = '\0';

            i4Len = STRLEN (pc1TempSysMsg);

            SysLogGetTimeStr (pc1TempSysMsg + i4Len);
            i4Len = STRLEN (pc1TempSysMsg);

            MEMCPY (pc1TempSysMsg + i4Len, "ISS ", STRLEN ("ISS"));
            i4Len = STRLEN (pc1TempSysMsg);

            MEMCPY (pc1TempSysMsg + i4Len, pc1SysLogPkt, STRLEN (pc1SysLogPkt));

            MEMSET (pc1SysLogPkt, 0, STRLEN (pc1TempSysMsg));
            MEMCPY (pc1SysLogPkt, pc1TempSysMsg, STRLEN (pc1TempSysMsg));

        }
        MemReleaseMemBlock (gSysLogGlobalInfo.SysLogCMemPoolId,
                            (UINT1 *) pc1TempSysMsg);
    }
    MemReleaseMemBlock (gSysLogGlobalInfo.SysLogCMemPoolId,
                        (UINT1 *) pc1SysMsg);
    return SYSLOG_SUCCESS;
}

/******************************************************************************
 * Function Name   : CheckTimeStamp
 * Description     : check whether the time stamp in syslog message is in bsd format
                     ,if not convert the message to bsd format.
 *
 * Inputs          : monthindex,month and syslog message
 * Output          : Time stamp in syslog message conformance to the bsd format
 * Returns         : syslog success or failure
 ******************************************************************************/

INT4
CheckTimeStamp (INT4 i4MonthIndex, CHR1 * ac1Month, CHR1 * pc1SysMess)
{
    /*INT4 i4MessLen = STRLEN(pc1SysMess); */
    CHR1               *pc1SysMsg = NULL;
    INT4                i4Day = SYSLOG_ZERO;
    INT4                i4Hour = SYSLOG_ZERO;
    INT4                i4Min = SYSLOG_ZERO;
    INT4                i4Sec = SYSLOG_ZERO;
    INT1                i1TimeFlag = SYSLOG_ZERO;

    pc1SysMsg = (CHR1 *) MemAllocMemBlk (gSysLogGlobalInfo.SysLogCMemPoolId);
    if (pc1SysMsg == NULL)
    {
        return SYSLOG_FAILURE;
    }
    MEMSET (pc1SysMsg, SYSLOG_ZERO, SYSLOG_MESS_LENGTH + SYSLOG_FIFTY);
    MEMCPY (pc1SysMsg, pc1SysMess, STRLEN (pc1SysMess));
    pc1SysMsg[strlen (pc1SysMess)] = '\0';

    if ((((pc1SysMsg[i4MonthIndex + SYSLOG_NINE]) != ':') &&
         (pc1SysMsg[i4MonthIndex + SYSLOG_TWELVE]) != ':'))
    {
        i1TimeFlag = SYS_SET_TIME_FLAG;
        MemReleaseMemBlock (gSysLogGlobalInfo.SysLogCMemPoolId,
                            (UINT1 *) pc1SysMsg);
        return i1TimeFlag;

    }

    if (pc1SysMsg[i4MonthIndex + SYSLOG_FOUR] == ' ')
    {
        i4Day = (pc1SysMsg[i4MonthIndex + SYSLOG_FIVE] - '0');
    }
    else
    {
        i4Day = ((pc1SysMsg[i4MonthIndex + SYSLOG_FOUR] - '0') * SYSLOG_TEN) +
            (pc1SysMsg[i4MonthIndex + SYSLOG_FIVE] - '0');
    }
    i4Hour = ((pc1SysMsg[i4MonthIndex + SYSLOG_SEVEN] - '0') * SYSLOG_TEN) +
        (pc1SysMsg[i4MonthIndex + SYSLOG_EIGHT] - '0');

    i4Min = ((pc1SysMsg[i4MonthIndex + SYSLOG_TEN] - '0') * SYSLOG_TEN) +
        (pc1SysMsg[i4MonthIndex + SYSLOG_ELEVEN] - '0');

    i4Sec = ((pc1SysMsg[i4MonthIndex + SYSLOG_THIRTEEN] - '0') * SYSLOG_TEN) +
        (pc1SysMsg[i4MonthIndex + SYSLOG_FOURTEEN] - '0');

    if ((i4Day < SYSLOG_ZERO) || (i4Day > SYS_MAX_DAY))
    {
        i1TimeFlag = SYS_SET_TIME_FLAG;
        MemReleaseMemBlock (gSysLogGlobalInfo.SysLogCMemPoolId,
                            (UINT1 *) pc1SysMsg);
        return i1TimeFlag;
    }
    else if ((i4Hour < SYSLOG_ZERO) || (i4Hour > SYS_MAX_HOUR))
    {
        i1TimeFlag = SYS_SET_TIME_FLAG;
        MemReleaseMemBlock (gSysLogGlobalInfo.SysLogCMemPoolId,
                            (UINT1 *) pc1SysMsg);
        return i1TimeFlag;
    }
    else if ((i4Min < SYSLOG_ZERO) || (i4Min > SYS_MAX_MIN))
    {
        i1TimeFlag = SYS_SET_TIME_FLAG;
        MemReleaseMemBlock (gSysLogGlobalInfo.SysLogCMemPoolId,
                            (UINT1 *) pc1SysMsg);
        return i1TimeFlag;
    }
    else if ((i4Sec < SYSLOG_ZERO) || (i4Sec > SYS_MAX_SEC))
    {
        i1TimeFlag = SYS_SET_TIME_FLAG;
        MemReleaseMemBlock (gSysLogGlobalInfo.SysLogCMemPoolId,
                            (UINT1 *) pc1SysMsg);
        return i1TimeFlag;
    }
    else if (CheckMonthAndDate (ac1Month, i4Day) == SYS_SET_MONTH_FLAG)
    {
        i1TimeFlag = SYS_SET_TIME_FLAG;
        MemReleaseMemBlock (gSysLogGlobalInfo.SysLogCMemPoolId,
                            (UINT1 *) pc1SysMsg);
        return i1TimeFlag;
    }

    MemReleaseMemBlock (gSysLogGlobalInfo.SysLogCMemPoolId,
                        (UINT1 *) pc1SysMsg);
    return SYSLOG_ZERO;

}

/******************************************************************************
 * Function Name   : CheckMonthAndDate
 * Description     : check whether the month and date in syslog message is in 
                     bsd format ,if not convert the message to bsd format.
 *
 * Inputs          : month and day
 * Output          : Time stamp in syslog message conformance to the bsd format
 * Returns         : syslog success or failure
 ******************************************************************************/

INT4
CheckMonthAndDate (CHR1 * ac1Month, INT4 i4Day)
{
    INT4                i4Index;
    INT4                i4MaxDay = SYSLOG_ZERO;
    const CHR1         *pc1Month[SYS_MAX_MONTH] = {
        "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };

    if (ac1Month == NULL)
    {
        return SYS_SET_MONTH_FLAG;
    }

    for (i4Index = SYSLOG_ZERO; i4Index <= SYS_NUM_MONTH; i4Index++)
    {
        if (strcmp (pc1Month[i4Index], ac1Month) == SYSLOG_ZERO)
        {
            if ((i4Index == SYSLOG_ZERO) || (i4Index == SYSLOG_TWO)
                || (i4Index == SYSLOG_FOUR) || (i4Index == SYSLOG_SIX)
                || (i4Index == SYSLOG_SEVEN) || (i4Index == SYSLOG_NINE)
                || (i4Index == SYSLOG_ELEVEN))
            {
                i4MaxDay = SYS_MAX_DAY;
            }
            else if (i4Index == SYSLOG_ONE)
            {
                i4MaxDay = SYS_FEB_MAX_DAY;
            }
            else
            {
                i4MaxDay = SYSLOG_THIRTY;
            }
        }

    }

    if (i4Day > i4MaxDay)
    {
        return SYS_SET_MONTH_FLAG;
    }
    else
    {
        return SYSLOG_ZERO;
    }
}

/******************************************************************************
 *  Function Name   : SyslogReplaceFileTableEntries
 *  Description     : Function replace the filename in filetable entries when
 *                    syslog file changes
 *  
 *  Inputs          : pi1FileName - Exiting filename
 *                    pNewFilename - New filename to be replaced with
 *  Output          : Filetabel updated with new filename
 *  Returns         : None
 *******************************************************************************/
VOID
SyslogReplaceFileTableEntries (INT1 *pi1FileName,
                               tSNMP_OCTET_STRING_TYPE * pNewFileName)
{
    tSysLogFileInfo    *pFileInfo = NULL;

    pFileInfo =
        (tSysLogFileInfo *) TMO_SLL_First (&(gSysLogGlobalInfo.sysFileTblList));
    while (pFileInfo != NULL)
    {
        if (STRLEN (pFileInfo->au1FileName) == STRLEN (pi1FileName))
        {
            if ((MEMCMP (pFileInfo->au1FileName, pi1FileName,
                         STRLEN (pFileInfo->au1FileName))) == SYSLOG_ZERO)
            {
                MEMSET (pFileInfo->au1FileName, SYSLOG_ZERO,
                        SYS_MAX_FILE_NAME_LEN + 3);
                MEMCPY (pFileInfo->au1FileName, pNewFileName->pu1_OctetList,
                        pNewFileName->i4_Length);
            }
        }

        pFileInfo =
            (tSysLogFileInfo *)
            TMO_SLL_Next ((&gSysLogGlobalInfo.sysFileTblList),
                          &pFileInfo->NextNode);
    }

    return;
}

/*****************************************************************************/
/* Function Name      : SysLogCallBackRegister                               */
/*                                                                           */
/* Description        : This function registers the call backs from          */
/*                      application                                          */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                      pFsCbInfo - Call Back Function                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SYSLOG_SUCCESS/SYSLOG_FAILURE                        */
/*****************************************************************************/
INT4
SysLogCallBackRegister (UINT4 u4Event, tFsCbInfo * pFsCbInfo)
{
    int                 i4RetVal = SYSLOG_SUCCESS;

    switch (u4Event)
    {
        case SYSLOG_REGISTER_EVT:
            SYSLOG_CALLBACK[u4Event].pSysLogRegister =
                pFsCbInfo->pSysLogRegister;
            break;

        case SYSLOG_DEREGISTER_EVT:
            SYSLOG_CALLBACK[u4Event].pSysLogDeRegister =
                pFsCbInfo->pSysLogDeRegister;
            break;

        case SYSLOG_MSG_EVT:
            SYSLOG_CALLBACK[u4Event].pSysLogMsg = pFsCbInfo->pSysLogMsg;
            break;

        default:
            i4RetVal = SYSLOG_FAILURE;
            break;
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name        : SysLogCustCallBack                                 */
/*                                                                           */
/* Description          : This function calls the registered function        */
/*                        pointer for specific events.                       */
/*                                                                           */
/* Input(s)             : u4Event - Event for which Call back is invoked     */
/*                        Variable Args - Since each called function has a   */
/*                                        different parameter list           */
/* Global Variables                                                          */
/* Referred             : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Modified             : None                                               */
/*                                                                           */
/* Return Value(s)      : ISS_CUSTOM_CONTINUE_PROCESSING/                    */
/*                        ISS_CUSTOM_SKIP_PROCESSING/ISS_CUSTOM_FAILURE      */
/*                                                                           */
/*****************************************************************************/
INT4
SysLogCustCallBack (UINT4 u4Event, ...)
{
    INT4                i4RetVal = ISS_CUSTOM_CONTINUE_PROCESSING;
    va_list             VarArgList;
    const UINT1        *pu1Name;
    UINT4               u4Level;
    UINT4               u4ModId;
    UINT4              *pu4Count;
    UINT4               u4Count = 0;
    const CHR1         *au1TempStr;

    va_start (VarArgList, u4Event);

    switch (u4Event)
    {
        case SYSLOG_REGISTER_EVT:
            if (SYSLOG_CALLBACK[u4Event].pSysLogRegister != NULL)
            {
                pu1Name = va_arg (VarArgList, const UINT1 *);
                u4Level = va_arg (VarArgList, UINT4);
                pu4Count = va_arg (VarArgList, UINT4 *);
                i4RetVal =
                    SYSLOG_CALLBACK[u4Event].pSysLogRegister (pu1Name, u4Level,
                                                              &u4Count);
                *pu4Count = u4Count;
            }
            break;

        case SYSLOG_DEREGISTER_EVT:
            if (SYSLOG_CALLBACK[u4Event].pSysLogDeRegister != NULL)
            {
                u4ModId = va_arg (VarArgList, UINT4);
                i4RetVal = SYSLOG_CALLBACK[u4Event].pSysLogDeRegister (u4ModId);
            }
            break;

        case SYSLOG_MSG_EVT:
            if (SYSLOG_CALLBACK[u4Event].pSysLogMsg != NULL)
            {
                u4Level = va_arg (VarArgList, UINT4);
                u4ModId = va_arg (VarArgList, UINT4);
                au1TempStr = va_arg (VarArgList, const CHR1 *);

                i4RetVal = SYSLOG_CALLBACK[u4Event].pSysLogMsg
                    (u4Level, u4ModId, au1TempStr);
            }
            break;

        default:
            i4RetVal = ISS_CUSTOM_FAILURE;
            break;
    }
    va_end (VarArgList);

    return i4RetVal;

}

/*****************************************************************************/
/* Function Name        : SyslogUtilCheckServerConfig                        */
/*                                                                           */
/* Description          : This function checks whether the server is         */
/*                        configured for the particular severity type.       */
/*                                                                           */
/* Input(s)             : i4SeverityLevel - Severity level                   */
/*                                                                           */
/* Output(s)            : None.                                              */
/*                                                                           */
/* Return Value(s)      : ISS_TRUE if the the syslog server is configured    */
/*                        otherwise ISS_FALSE will be returned.              */
/*                                                                           */
/*****************************************************************************/
INT4
SyslogUtilCheckServerConfig (UINT4 u4SeverityLevel)
{
    UINT4               u4Priority = 0;
    tSysLogFwdInfo     *pFwdInfo = NULL;
    tSysLogFwdInfo     *pnode = NULL;

    if (SYSLOG_ENABLE == gsSysLogParams.u4SysLogStatus)
    {
        u4Priority = gsSysLogParams.u4Facility | u4SeverityLevel;

        /* Traversing the syslog server forwarding table */
        TMO_SLL_Scan (&gSysLogGlobalInfo.sysFwdTblList, pnode, tSysLogFwdInfo *)
        {
            pFwdInfo = GET_FWDENTRY (pnode);

            if (pFwdInfo->u4Priority == u4Priority)
            {
                return ISS_TRUE;
            }
        }
    }
    return ISS_FALSE;
}

/*****************************************************************************/
/* Function Name      : SyslogSendTrapMessage                                */
/*                                                                           */
/* Description        : This function is used to send the trap notification  */
/*                       whenever syslog server is down                      */
/*                                                                           */
/* Input(s)           : pu1SyslogServerIpAddr - IP address of the syslog     */
/*                      server                                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
SyslogSendTrapMessage (tIPvXAddr ServerIpAddr)
{
    tSyslogTrapMsg      sSyslogTrapMsg;
    MEMSET (&sSyslogTrapMsg, 0, sizeof (sSyslogTrapMsg));
    sSyslogTrapMsg.u4Event = 1;
    UtlGetTimeStr (sSyslogTrapMsg.ac1DateTime);
    SyslogTrapSendNotifications (&sSyslogTrapMsg, ServerIpAddr);

    return;
}

/******************************************************************************
 * Function Name      : SyslogTrapSendNotifications
 * 
 * Description        : This function is used to send notifications for
 *                      whenever syslog server is down
 *
 * Input(s)           : pSyslogTrapMsg - Pointer to the tIdsTrapMsg
 *                      structure
 *                      pu1SyslogServerIpAddr - IP Address syslog server
 * 
 * Output(s)          : None
 * Return Value(s)    : None
 ******************************************************************************/

VOID
SyslogTrapSendNotifications (tSyslogTrapMsg * pSyslogTrapMsg,
                             tIPvXAddr ServerIpAddr)
{
#ifdef SNMP_3_WANTED
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_OID_TYPE      PrefixTrapOid;
    tSNMP_OID_TYPE      SuffixTrapOid;
    tSNMP_OID_TYPE     *pTrapOid;
    UINT4               au4PrefixTrapOid[] = { 1, 3, 6, 1, 4, 1 };
    UINT4               au4SuffixTrapOid[] = { 89, 4, 1 };
    UINT4               au4SnmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT1               au1Buf[SNMP_MAX_OID_LENGTH];
    UINT1               au1Addr[SNMP_MAX_OID_LENGTH];
#endif /* SNMP_3_WANTED */
    if (gi4SlogSrvUpDownTrap == SNMP_TRAP_DISABLE)
    {
        return;
    }

    if (pSyslogTrapMsg == NULL)
    {
        return;
    }
#ifdef SNMP_3_WANTED
    PrefixTrapOid.pu4_OidList = au4PrefixTrapOid;
    PrefixTrapOid.u4_Length = sizeof (au4PrefixTrapOid) / sizeof (UINT4);

    SuffixTrapOid.pu4_OidList = au4SuffixTrapOid;
    SuffixTrapOid.u4_Length = sizeof (au4SuffixTrapOid) / sizeof (UINT4);

    if ((pTrapOid = alloc_oid (SNMP_MAX_OID_LENGTH)) == NULL)
    {
        return;
    }
    MEMSET (pTrapOid->pu4_OidList, 0, sizeof (UINT4) * SNMP_MAX_OID_LENGTH);

    pTrapOid->u4_Length = 0;

    if (SNMPAddEnterpriseOid (&PrefixTrapOid, &SuffixTrapOid,
                              pTrapOid) == OSIX_FAILURE)
    {
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                    "SyslogTrapSendNotifications: Adding EnterpriseOid failed \n");
        free_oid (pTrapOid);
        return;
    }
    MEMSET (au1Buf, 0, sizeof (SNMP_MAX_OID_LENGTH));

    pEnterpriseOid = alloc_oid (SNMP_MAX_OID_LENGTH);

    if (pEnterpriseOid == NULL)
    {
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                    "SyslogTrapSendNotifications: OID Memory Allocation Failed\n");
        free_oid (pTrapOid);
        return;
    }

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    MEMCPY (pEnterpriseOid->pu4_OidList, pTrapOid->pu4_OidList,
            pTrapOid->u4_Length * sizeof (UINT4));
    pEnterpriseOid->u4_Length = pTrapOid->u4_Length;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = 1;
    free_oid (pTrapOid);

    pSnmpTrapOid = alloc_oid (sizeof (au4SnmpTrapOid) / sizeof (UINT4));
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                    "SyslogTrapSendNotifications: OID Memory Allocation Failed\n");
        return;
    }
    MEMCPY (pSnmpTrapOid->pu4_OidList, au4SnmpTrapOid, sizeof (au4SnmpTrapOid));
    pSnmpTrapOid->u4_Length = sizeof (au4SnmpTrapOid) / sizeof (UINT4);

    pVbList = SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                    SNMP_DATA_TYPE_OBJECT_ID,
                                    0, 0, NULL, pEnterpriseOid, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                    "SyslogTrapSendNotifications: Variable Binding Failed\n");
        return;
    }
    pStartVb = pVbList;
/******* Getting the oid for SysLogSrvrUnreachEventTime */
    SPRINTF ((CHR1 *) au1Buf, "%s", "sysLogSrvrUnreachEventTime");

    pOid = SyslogMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                    "SyslogTrapSendNotifications: OID Not Found\r\n");

        return;
    }

    pOstring = SNMP_AGT_FormOctetString ((UINT1 *) pSyslogTrapMsg->ac1DateTime,
                                         (INT4) SYSLOG_TRAP_TIME_STR_LEN);

    if (pOstring == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                    "SyslogTrapSendNotifications: Form Octet string Failed\n");

        return;
    }

    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_OCTET_PRIM,
                                                  0, 0, pOstring,
                                                  NULL, SnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                    "SyslogTrapSendNotifications: Variable Binding Failed\r\n");

        return;
    }
    pVbList = pVbList->pNextVarBind;
/******* Getting the oid for SysLogSrvrUnreachMessagei */
    MEMSET (au1Addr, 0, SNMP_MAX_OID_LENGTH);

    if (ServerIpAddr.u1AddrLen == IPVX_IPV4_ADDR_LEN)
    {
        SPRINTF ((CHR1 *) au1Addr, "%d.%d.%d.%d",
                 (UINT1) ServerIpAddr.au1Addr[0],
                 (UINT1) ServerIpAddr.au1Addr[1],
                 (UINT1) ServerIpAddr.au1Addr[2],
                 (UINT1) ServerIpAddr.au1Addr[3]);
    }
    else if (ServerIpAddr.u1AddrLen == IPVX_IPV6_ADDR_LEN)
    {
        SPRINTF ((CHR1 *) au1Addr,
                 "%.2x%.2x:%.2x%.2x:%.2x%.2x:%.2x%.2x:%.2x%.2x:%.2x%.2x:%.2x%.2x:%.2x%.2x",
                 (UINT1) ServerIpAddr.au1Addr[3],
                 (UINT1) ServerIpAddr.au1Addr[2],
                 (UINT1) ServerIpAddr.au1Addr[1],
                 (UINT1) ServerIpAddr.au1Addr[0],
                 (UINT1) ServerIpAddr.au1Addr[7],
                 (UINT1) ServerIpAddr.au1Addr[6],
                 (UINT1) ServerIpAddr.au1Addr[5],
                 (UINT1) ServerIpAddr.au1Addr[4],
                 (UINT1) ServerIpAddr.au1Addr[11],
                 (UINT1) ServerIpAddr.au1Addr[10],
                 (UINT1) ServerIpAddr.au1Addr[9],
                 (UINT1) ServerIpAddr.au1Addr[8],
                 (UINT1) ServerIpAddr.au1Addr[15],
                 (UINT1) ServerIpAddr.au1Addr[14],
                 (UINT1) ServerIpAddr.au1Addr[13],
                 (UINT1) ServerIpAddr.au1Addr[12]);
    }

    SPRINTF ((CHR1 *) au1Buf, "%s", "sysLogSrvrUnreachMessage");

    pOid = SyslogMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                    "SyslogTrapSendNotifications: OID Not Found\r\n");

        return;
    }
    MEMSET (au1Buf, 0, SNMP_MAX_OID_LENGTH);

    SPRINTF ((CHR1 *) au1Buf, "%s:%s", "SYSLOG SERVER UNREACHABLE", au1Addr);

    pOstring =
        SNMP_AGT_FormOctetString ((UINT1 *) au1Buf, (INT4) STRLEN (au1Buf));

    if (pOstring == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                    "SyslogTrapSendNotifications: Form Octet string Failed\n");

        return;
    }

    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_OCTET_PRIM,
                                                  0, 0, pOstring,
                                                  NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                    "SyslogTrapSendNotifications: Variable Binding Failed\r\n");

        return;
    }
    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;
    /* sending the trap message */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
#else
    UNUSED_PARAM (ServerIpAddr);
#endif
    return;
}

#ifdef SNMP_3_WANTED
/******************************************************************************
 * * Function :   SyslogMakeObjIdFromDotNew
 * *
 * * Description: This Function retuns the OID  of the given string for the
 * *              proprietary MIB.
 * *
 * * Input    :   pi1TextStr - pointer to the string.
 * *
 * * Output   :   None.
 * *
 * * Returns  :   pOidPtr or NULL
 * *******************************************************************************/
tSNMP_OID_TYPE     *
SyslogMakeObjIdFromDotNew (INT1 *pi1TextStr)
{
    tSNMP_OID_TYPE     *pOidPtr = NULL;
    INT1               *pi1TempPtr = NULL;
    INT1               *pi1DotPtr = NULL;
    UINT2               u2Index;
    UINT2               u2DotCount;
    UINT4               u4Len = 0;

    /* see if there is an alpha descriptor at begining */
    if (isalpha (*pi1TextStr))
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pi1TempPtr = pi1TextStr;

        for (u2Index = 0; ((pi1TempPtr < pi1DotPtr) && (u2Index < 256));
             u2Index++)
        {
            ai1TempBuffer[u2Index] = *pi1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';
        for (u2Index = 0;
             ((u2Index <
               (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID)))
              && (orig_mib_oid_table[u2Index].pName != NULL)); u2Index++)
        {
            if ((STRCMP
                 (orig_mib_oid_table[u2Index].pName,
                  (INT1 *) ai1TempBuffer) == 0)
                && (STRLEN ((INT1 *) ai1TempBuffer) ==
                    STRLEN (orig_mib_oid_table[u2Index].pName)))
            {
                u4Len =
                    ((STRLEN (orig_mib_oid_table[u2Index].pNumber) <
                      sizeof (ai1TempBuffer)) ?
                     STRLEN (orig_mib_oid_table[u2Index].
                             pNumber) : sizeof (ai1TempBuffer) - 1);
                STRNCPY ((INT1 *) ai1TempBuffer,
                         orig_mib_oid_table[u2Index].pNumber, u4Len);
                ai1TempBuffer[u4Len] = '\0';
                break;
            }
        }
        if (u2Index < (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID)))
        {
            if (orig_mib_oid_table[u2Index].pName == NULL)
            {
                return (NULL);
            }
        }
        /* now concatenate the non-alpha part to the begining */
        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr,
                 MEM_MAX_BYTES (STRLEN (pi1DotPtr),
                                sizeof (ai1TempBuffer) - 1 -
                                STRLEN (ai1TempBuffer)));

    }
    else
    {                            /* is not alpha, so just copy into ai1TempBuffer */
        u4Len =
            ((STRLEN (pi1TextStr) <
              sizeof (ai1TempBuffer)) ? STRLEN (pi1TextStr) :
             sizeof (ai1TempBuffer) - 1);
        STRNCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr, u4Len);
        ai1TempBuffer[u4Len] = '\0';
    }
    u2DotCount = 0;
    for (u2Index = 0;
         ((u2Index < 256) && (ai1TempBuffer[u2Index] != '\0')); u2Index++)
    {
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }
    pOidPtr = alloc_oid (SNMP_TRAP_OID_LEN);
    if (pOidPtr == NULL)
    {
        return (NULL);
    }

    pOidPtr->u4_Length = u2DotCount + 1;

    /* now we convert number.number.... strings */
    pi1TempPtr = ai1TempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {
        if ((pi1TempPtr = (INT1 *) SyslogParseSubIdNew
             ((UINT1 *) pi1TempPtr, &(pOidPtr->pu4_OidList[u2Index]))) == NULL)
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if (*pi1TempPtr == '.')
        {
            pi1TempPtr++;        /* to skip over dot */
        }
        else if (*pi1TempPtr != '\0')
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
}

/******************************************************************************
 * * Function    : SyslogParseSubIdNew
 * *
 * * Description : Parse the string format in number.number..format.
 * *
 * * Input       : pu1TempPtr  - Pointer to the string.
 * *               pu4Value    - Pointer the OID List value.
 * *
 * * Output      : value of pu1TempPtr
 * *
 * * Returns     : OSIX_SUCCESS or OSIX_FAILURE
 * *******************************************************************************/

UINT1              *
SyslogParseSubIdNew (UINT1 *pu1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    UINT1              *pu1Tmp = NULL;

    for (pu1Tmp = pu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                               ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                               ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pu1Tmp & 0xf);
    }

    if (pu1TempPtr == pu1Tmp)
    {
        pu1Tmp = NULL;
    }
    *pu4Value = u4Value;
    return pu1Tmp;
}
#endif
/*****************************************************************************/
/* Function Name      : SyslogNotifyIfStatusChange                           */
/*                                                                           */
/* Description        : This function generates Trap message whenever there  */
/*                      is a connectivity loss to the syslog server. Ie.     */
/*                        when there is a link down trap generated for the     */
/*                        link connected to the syslog server.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Index of the interface                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SYSLOG_SUCCESS                                       */
/*****************************************************************************/
UINT4
SyslogNotifyIfStatusChange (UINT4 u4IfIndex)
{
    tSysLogFwdInfo     *pFwdInfo = NULL;
    tSysLogFwdInfo     *pnode = NULL;
    UINT4               u4ServerIp;
    tIpConfigInfo       IpIfInfo;
#ifdef IP6_WANTED
    INT4                i4RetVal = 0;
    UINT1               au1Addr[SNMP_MAX_OID_LENGTH];
    tNetIpv6AddrInfo    NetIpv6AddrInfo;
    tNetIpv6AddrInfo    NetIpv6NextAddrInfo;
    tIp6Addr            Ipv6SyslgAddress;
    UINT4               u4PrefixLen;
#endif
    if (CfaIpIfGetIfInfo (u4IfIndex, &IpIfInfo) == CFA_SUCCESS)
    {
        TMO_SLL_Scan (&gSysLogGlobalInfo.sysFwdTblList, pnode, tSysLogFwdInfo *)
        {
            pFwdInfo = GET_FWDENTRY (pnode);
            SYSLOG_INET_NTOHL (pFwdInfo->ServIpAddr.au1Addr);
            MEMCPY (&u4ServerIp, pFwdInfo->ServIpAddr.au1Addr,
                    IPVX_IPV4_ADDR_LEN);
            if ((IpIfInfo.u4Addr & IpIfInfo.u4NetMask) ==
                (u4ServerIp & IpIfInfo.u4NetMask))
            {
                SYSLOG_INET_HTONL (pFwdInfo->ServIpAddr.au1Addr);
                SyslogSendTrapMessage (pFwdInfo->ServIpAddr);
            }
        }

    }

#ifdef IP6_WANTED

    if (NetIpv6GetFirstIfAddr (u4IfIndex, &NetIpv6AddrInfo) == NETIPV6_SUCCESS)
    {
        for (;;)
        {
            u4PrefixLen = NetIpv6AddrInfo.u4PrefixLength;
            TMO_SLL_Scan (&gSysLogGlobalInfo.sysFwdTblList, pnode,
                          tSysLogFwdInfo *)
            {
                pFwdInfo = GET_FWDENTRY (pnode);
                SYSLOG_INET_HTONL (pFwdInfo->ServIpAddr.au1Addr);
                SPRINTF ((CHR1 *) au1Addr,
                         "%.02x%.02x:%.02x%.02x:%.02x%.02x:%.02x%.02x:%.02x%.02x:%.02x%.02x:%.02x%.02x:%.02x%.02x",
                         (UINT1) pFwdInfo->ServIpAddr.au1Addr[3],
                         (UINT1) pFwdInfo->ServIpAddr.au1Addr[2],
                         (UINT1) pFwdInfo->ServIpAddr.au1Addr[1],
                         (UINT1) pFwdInfo->ServIpAddr.au1Addr[0],
                         (UINT1) pFwdInfo->ServIpAddr.au1Addr[7],
                         (UINT1) pFwdInfo->ServIpAddr.au1Addr[6],
                         (UINT1) pFwdInfo->ServIpAddr.au1Addr[5],
                         (UINT1) pFwdInfo->ServIpAddr.au1Addr[4],
                         (UINT1) pFwdInfo->ServIpAddr.au1Addr[11],
                         (UINT1) pFwdInfo->ServIpAddr.au1Addr[10],
                         (UINT1) pFwdInfo->ServIpAddr.au1Addr[9],
                         (UINT1) pFwdInfo->ServIpAddr.au1Addr[8],
                         (UINT1) pFwdInfo->ServIpAddr.au1Addr[15],
                         (UINT1) pFwdInfo->ServIpAddr.au1Addr[14],
                         (UINT1) pFwdInfo->ServIpAddr.au1Addr[13],
                         (UINT1) pFwdInfo->ServIpAddr.au1Addr[12]);

                i4RetVal = INET_ATON6 (au1Addr, &Ipv6SyslgAddress);
                if (Ip6AddrMatch
                    (&Ipv6SyslgAddress, &NetIpv6AddrInfo.Ip6Addr,
                     (INT4) u4PrefixLen) == TRUE)
                {
                    SyslogSendTrapMessage (pFwdInfo->ServIpAddr);
                }
                /*  SYSLOG_INET_NTOHL (pFwdInfo->ServIpAddr.au1Addr); */

            }

            if (NetIpv6GetNextIfAddr (u4IfIndex, &NetIpv6AddrInfo,
                                      &NetIpv6NextAddrInfo) == NETIPV6_FAILURE)
            {
                break;
            }
            MEMCPY (&NetIpv6AddrInfo, &NetIpv6NextAddrInfo,
                    sizeof (tNetIpv6AddrInfo));
        }
    }

    UNUSED_PARAM (i4RetVal);

#endif

    return SYSLOG_SUCCESS;
}

/*****************************************************************************/
/* Function Name        : SyslogUtilValidateIpAddress                        */
/*                                                                           */
/* Description          : This Routine is used to validate IP Address        */
/*                                                                           */
/* Input(s)             : u4IpAddr - Ip Address                              */
/*                                                                           */
/* Output(s)            : None.                                              */
/*                                                                           */
/* Return Value(s)      : TRUE/FALSE                                         */
/*                                                                           */
/*****************************************************************************/

UINT1
SyslogUtilValidateIpAddress (UINT4 u4IpAddr)
{
    if (u4IpAddr == SYSLOG_ZERO)
    {
        return (FALSE);
    }
    if (u4IpAddr == SYSLOG_LTD_B_CAST_ADDR)
    {
        return (FALSE);
    }
    if ((u4IpAddr & SYSLOG_LOOP_BACK_ADDR_MASK) == SYSLOG_LOOP_BACK_ADDRESSES)
    {
        return (FALSE);
    }
    if ((u4IpAddr & SYSLOG_NETWORK_ADDR) == u4IpAddr)
    {
        return (FALSE);
    }
    if (SYSLOG_IS_BRD_CAST_ADDRESS (u4IpAddr) == SYSLOG_ZERO)
    {
        return (FALSE);
    }
    if (!((SYSLOG_IS_ADDR_CLASS_A (u4IpAddr)) ||
          (SYSLOG_IS_ADDR_CLASS_B (u4IpAddr))
          || (SYSLOG_IS_ADDR_CLASS_C (u4IpAddr))))
    {
        return (FALSE);
    }
    return (TRUE);                /*Valid Ip Address */
}

/*****************************************************************************/
/* Function Name        : SysLogGetLogFileNum                                */
/*                                                                           */
/* Description          : This Routine is used to Get the Log File Number    */
/*                                                                           */
/* Input(s)             : u4ModuleId                                         */
/*                                                                           */
/* Output(s)            : None.                                              */
/*                                                                           */
/* Return Value(s)      : Filenum                                            */
/*                                                                           */
/*****************************************************************************/

UINT4
SysLogGetLogFileNum (UINT4 u4ModuleId)
{
    UINT4               u4RetVal = 0;
    u4RetVal = asModInfo[u4ModuleId].u4FileNo;
    return u4RetVal;
}

/*****************************************************************************/
/* Function Name        : SysLogSetFileNum                                   */
/*                                                                           */
/* Description          : This Routine is used to set file number            */
/*                                                                           */
/* Input(s)             : u4ModuleId , u4FileNum                             */
/*                                                                           */
/* Output(s)            : None.                                              */
/*                                                                           */
/* Return Value(s)      : SYSLOG_SUCCESS                                     */
/*                                                                           */
/*****************************************************************************/

INT4
SysLogSetFileNum (UINT4 u4ModuleId, UINT4 u4FileNum)
{
    asModInfo[u4ModuleId].u4FileNo = u4FileNum;
    return SYSLOG_SUCCESS;
}

/*****************************************************************************/
/* Function Name        : SysLogGetMemPoolId                                 */
/*                                                                           */
/* Description          : This Routine is used to get mempool id of syslog   */
/*                                                                           */
/* Input(s)             : None                                               */
/*                                                                           */
/* Output(s)            : None.                                              */
/*                                                                           */
/* Return Value(s)      : Mempool Id used by syslog                          */
/*                                                                           */
/*****************************************************************************/
INT4
SysLogGetMemPoolId (VOID)
{
    return (gSysLogGlobalInfo.SysLogUMemPoolId);
}
