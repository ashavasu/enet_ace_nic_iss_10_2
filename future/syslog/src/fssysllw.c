/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssysllw.c,v 1.62 2017/12/28 10:40:16 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "syslginc.h"
# include "syslgcli.h"
# include "rmgr.h"
# include "slogtrc.h"

# include "fips.h"

extern tSysLogParams gsSysLogParams;
extern tSysModInfo  asModInfo[SYSLOG_MAX_MODULES + 1];

extern tSysLogGlobalInfo gSysLogGlobalInfo;
extern UINT1        gu1SmtpAuthentication;
extern UINT4        FsSyslogLogFile[SYSLOG_TEN];
extern UINT4        FsSyslogRole[SYSLOG_TEN];
extern UINT4        FsSyslogConfigLogLevel[SYSLOG_TWELVE];
extern UINT4        FsSyslogMail[SYSLOG_TEN];
extern UINT4        FsSyslogProfile[SYSLOG_TEN];
extern UINT4        FsSyslogRelayPort[SYSLOG_TEN];
extern UINT4        FsSyslogRelayTransType[SYSLOG_TEN];
extern UINT4        FsSyslogFwdRowStatus[SYSLOG_TWELVE];
extern UINT4        FsSyslogFwdPort[SYSLOG_TWELVE];
extern UINT4        FsSyslogFwdTransType[SYSLOG_TWELVE];
extern UINT4        FsSyslogRxMailId[SYSLOG_TWELVE];
extern UINT4        FsSyslogMailRowStatus[SYSLOG_TWELVE];
extern UINT4        FsSyslogMailServUserName[SYSLOG_TWELVE];
extern UINT4        FsSyslogMailServPassword[SYSLOG_TWELVE];
extern UINT4        FsSyslogFileRowStatus[SYSLOG_TWELVE];
extern UINT4        FsSyslogSmtpAuthMethod[SYSLOG_TEN];
extern UINT4        FsSyslogFileNameOne[SYSLOG_TEN];
extern UINT4        FsSyslogFileNameTwo[SYSLOG_TEN];
extern UINT4        FsSyslogFileNameThree[SYSLOG_TEN];
extern UINT4        FsSyslogSmtpSenderMailId[SYSLOG_TEN];

extern INT4         SyslogEnableTrace (INT4 i4TrapLevel, INT4 u4ModuleId);
extern tSysLogEntry gaSysLogTbl[MAX_SYSLOG_USERS_LIMIT + 1];
extern tEmailAlertParams gsSmtpAlertParams;
extern UINT4        gu4CliSysLogLevel;
extern INT4         gi4SysLogFd;
extern INT4         gi4SlogSrvUpDownTrap;
extern UINT4        gu4AuditLogSize;

UINT4               gSyslogMode = SYSLOG_DEFAULT_MODE;

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSyslogLogging
 Input       :  The Indices

                The Object 
                retValFsSyslogLogging
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogLogging (INT4 *pi4RetValFsSyslogLogging)
{
    *pi4RetValFsSyslogLogging = gsSysLogParams.u4SysLogStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSyslogTimeStamp
 Input       :  The Indices

                The Object 
                retValFsSyslogTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogTimeStamp (INT4 *pi4RetValFsSyslogTimeStamp)
{
    *pi4RetValFsSyslogTimeStamp = gsSysLogParams.u4TimeStamp;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSyslogConsoleLog
 Input       :  The Indices

                The Object 
                retValFsSyslogConsoleLog
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogConsoleLog (INT4 *pi4RetValFsSyslogConsoleLog)
{
    *pi4RetValFsSyslogConsoleLog = gsSysLogParams.u4LogConsole;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSyslogSysBuffers
 Input       :  The Indices

                The Object 
                retValFsSyslogSysBuffers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogSysBuffers (INT4 *pi4RetValFsSyslogSysBuffers)
{
    tSysLogEntry       *pSysLogEntry;
    pSysLogEntry = &gaSysLogTbl[MAX_SYSLOG_USERS_LIMIT];
    *pi4RetValFsSyslogSysBuffers = pSysLogEntry->u4MaxLogMsgs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSyslogClearLog
 Input       :  The Indices

                The Object 
                retValFsSyslogClearLog
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogClearLog (INT4 *pi4RetValFsSyslogClearLog)
{
    *pi4RetValFsSyslogClearLog = SYSLOG_FALSE;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSyslogLogging
 Input       :  The Indices

                The Object 
                setValFsSyslogLogging
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogLogging (INT4 i4SetValFsSyslogLogging)
{
    gsSysLogParams.u4SysLogStatus = i4SetValFsSyslogLogging;
    if (gsSysLogParams.u4SysLogStatus == SYSLOG_DISABLE)
    {
        gsSysLogParams.u4SyslogLogs = SYSLOG_ZERO;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSyslogTimeStamp
 Input       :  The Indices

                The Object 
                setValFsSyslogTimeStamp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogTimeStamp (INT4 i4SetValFsSyslogTimeStamp)
{
    gsSysLogParams.u4TimeStamp = i4SetValFsSyslogTimeStamp;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSyslogConsoleLog
 Input       :  The Indices

                The Object 
                setValFsSyslogConsoleLog
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogConsoleLog (INT4 i4SetValFsSyslogConsoleLog)
{
    /* In FIPS mode CLI console will be disabled.
     * So logging to CLI console is not possible. 
     * But when user sets logging console in non-fips mode, 
     * gives write startup config and switches to FIPS mode
     * the logging console option should not be enabled from
     * MSR. */
    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        gsSysLogParams.u4LogConsole = SYSLOG_DISABLE;
    }
    else
    {
        gsSysLogParams.u4LogConsole = i4SetValFsSyslogConsoleLog;
    }
    if (i4SetValFsSyslogConsoleLog == SYSLOG_DISABLE)
    {
        gsSysLogParams.u4ConsoleLogs = 0;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSyslogSysBuffers
 Input       :  The Indices

                The Object 
                setValFsSyslogSysBuffers
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogSysBuffers (INT4 i4SetValFsSyslogSysBuffers)
{
    tSysLogEntry       *pSysLogEntry;

    pSysLogEntry = &gaSysLogTbl[MAX_SYSLOG_USERS_LIMIT];
    if (pSysLogEntry->u4MaxLogMsgs == (UINT4) i4SetValFsSyslogSysBuffers)
    {
        return SNMP_SUCCESS;
    }

    if (SysLogSetMaxLogBuff (MAX_SYSLOG_USERS_LIMIT,
                             (UINT4) i4SetValFsSyslogSysBuffers) !=
        SYSLOG_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSyslogClearLog
 Input       :  The Indices

                The Object 
                setValFsSyslogClearLog
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogClearLog (INT4 i4SetValFsSyslogClearLog)
{
    if (i4SetValFsSyslogClearLog == SYSLOG_TRUE)
    {
        SysLogClearLogs ();
        gsSysLogParams.u4ConsoleLogs = 0;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSyslogLogging
 Input       :  The Indices

                The Object 
                testValFsSyslogLogging
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogLogging (UINT4 *pu4ErrorCode, INT4 i4TestValFsSyslogLogging)
{
    if ((i4TestValFsSyslogLogging != SYSLOG_ENABLE) &&
        (i4TestValFsSyslogLogging != SYSLOG_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INV_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSyslogTimeStamp
 Input       :  The Indices

                The Object 
                testValFsSyslogTimeStamp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogTimeStamp (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsSyslogTimeStamp)
{
    if ((i4TestValFsSyslogTimeStamp != SYSLOG_ENABLE) &&
        (i4TestValFsSyslogTimeStamp != SYSLOG_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INV_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSyslogConsoleLog
 Input       :  The Indices

                The Object 
                testValFsSyslogConsoleLog
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogConsoleLog (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsSyslogConsoleLog)
{
    if ((i4TestValFsSyslogConsoleLog != SYSLOG_ENABLE) &&
        (i4TestValFsSyslogConsoleLog != SYSLOG_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INV_STATUS);
        return SNMP_FAILURE;
    }

    /* In FIPS mode CLI console will be disabled.
     * So logging to CLI console is not possible. */
    if ((FIPS_MODE == FipsGetFipsCurrOperMode ()) &&
        (i4TestValFsSyslogConsoleLog == SYSLOG_ENABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INV_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSyslogSysBuffers
 Input       :  The Indices

                The Object 
                testValFsSyslogSysBuffers
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogSysBuffers (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsSyslogSysBuffers)
{
    if ((i4TestValFsSyslogSysBuffers == 0) ||
        (i4TestValFsSyslogSysBuffers > SYSLOG_MAX_BUFFERS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INV_BUF_SIZE);
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSyslogClearLog
 Input       :  The Indices

                The Object 
                testValFsSyslogClearLog
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogClearLog (UINT4 *pu4ErrorCode, INT4 i4TestValFsSyslogClearLog)
{
    if ((i4TestValFsSyslogClearLog != SYSLOG_TRUE) &&
        (i4TestValFsSyslogClearLog != SYSLOG_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INV_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSyslogLogging
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogLogging (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSyslogTimeStamp
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogTimeStamp (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSyslogConsoleLog
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogConsoleLog (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSyslogSysBuffers
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogSysBuffers (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSyslogClearLog
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogClearLog (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsSyslogConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsSyslogConfigTable
 Input       :  The Indices
                FsSyslogConfigModule
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsSyslogConfigTable (INT4 i4FsSyslogConfigModule)
{
    if ((i4FsSyslogConfigModule <= 0) ||
        (i4FsSyslogConfigModule > SYSLOG_MAX_MODULES))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsSyslogConfigTable
 Input       :  The Indices
                FsSyslogConfigModule
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsSyslogConfigTable (INT4 *pi4FsSyslogConfigModule)
{
    if (nmhGetNextIndexFsSyslogConfigTable (0, pi4FsSyslogConfigModule) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsSyslogConfigTable
 Input       :  The Indices
                FsSyslogConfigModule
                nextFsSyslogConfigModule
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsSyslogConfigTable (INT4 i4FsSyslogConfigModule,
                                    INT4 *pi4NextFsSyslogConfigModule)
{
    UINT4               u4ModuleId;

    if (i4FsSyslogConfigModule < 0)
    {
        /* To avoid -ve index, due to UINT to INT conversion in the SNMP agent */
        return SNMP_FAILURE;
    }

    u4ModuleId = (UINT4) i4FsSyslogConfigModule + 1;

    while (1)
    {
        if (u4ModuleId > SYSLOG_MAX_MODULES)
        {
            return SNMP_FAILURE;
        }
        if (asModInfo[u4ModuleId].u4Status == SYSLOG_DISABLE)
        {
            u4ModuleId = u4ModuleId + 1;
        }
        else
        {
            break;
        }
    }
    *pi4NextFsSyslogConfigModule = u4ModuleId;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSyslogConfigLogLevel
 Input       :  The Indices
                FsSyslogConfigModule

                The Object 
                retValFsSyslogConfigLogLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogConfigLogLevel (INT4 i4FsSyslogConfigModule,
                              INT4 *pi4RetValFsSyslogConfigLogLevel)
{
    UINT4               u4ModuleId;
    u4ModuleId = (UINT4) i4FsSyslogConfigModule;

    if (asModInfo[u4ModuleId].u4Status == SYSLOG_ENABLE)
    {
        *pi4RetValFsSyslogConfigLogLevel = asModInfo[u4ModuleId].u4Level;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSyslogConfigLogLevel
 Input       :  The Indices
                FsSyslogConfigModule

                The Object 
                setValFsSyslogConfigLogLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogConfigLogLevel (INT4 i4FsSyslogConfigModule,
                              INT4 i4SetValFsSyslogConfigLogLevel)
{
    UINT4               u4ModuleId;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SYSLOG_ZERO;

    MEMSET (&SnmpNotifyInfo, SYSLOG_ZERO, sizeof (tSnmpNotifyInfo));
    u4ModuleId = (UINT4) i4FsSyslogConfigModule;

    if (asModInfo[u4ModuleId].u4Status == SYSLOG_ENABLE)
    {
        asModInfo[u4ModuleId].u4Level = i4SetValFsSyslogConfigLogLevel;
        if (STRCASECMP (asModInfo[u4ModuleId].au1Name, "CLI") == 0)
        {
            gu4CliSysLogLevel = (UINT4) i4SetValFsSyslogConfigLogLevel;
        }
#ifdef CLI_WANTED
        if (gSyslogMode)
        {
            SyslogEnableTrace (i4SetValFsSyslogConfigLogLevel,
                               i4FsSyslogConfigModule);
        }
#endif

        RM_GET_SEQ_NUM (&u4SeqNum);
        SnmpNotifyInfo.pu4ObjectId = FsSyslogConfigLogLevel;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsSyslogConfigLogLevel) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NULL;
        SnmpNotifyInfo.pUnLockPointer = NULL;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i%i", i4FsSyslogConfigModule,
                          i4SetValFsSyslogConfigLogLevel));

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSyslogConfigLogLevel
 Input       :  The Indices
                FsSyslogConfigModule

                The Object 
                testValFsSyslogConfigLogLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogConfigLogLevel (UINT4 *pu4ErrorCode,
                                 INT4 i4FsSyslogConfigModule,
                                 INT4 i4TestValFsSyslogConfigLogLevel)
{
    UINT4               u4ModuleId;
    u4ModuleId = (UINT4) i4FsSyslogConfigModule;

    if (asModInfo[u4ModuleId].u4Status == SYSLOG_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsSyslogConfigLogLevel < SYSLOG_MIN_LOG_LEVEL) ||
        (i4TestValFsSyslogConfigLogLevel > SYSLOG_MAX_LOG_LEVEL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INV_SEVERITY_LEVEL);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSyslogConfigTable
 Input       :  The Indices
                FsSyslogConfigModule
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogConfigTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSyslogFacility
 Input       :  The Indices

                The Object 
                retValFsSyslogFacility
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogFacility (INT4 *pi4RetValFsSyslogFacility)
{
    *pi4RetValFsSyslogFacility = gsSysLogParams.u4Facility;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSyslogRole
 Input       :  The Indices

                The Object 
                retValFsSyslogRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogRole (INT4 *pi4RetValFsSyslogRole)
{
    *pi4RetValFsSyslogRole = gsSysLogParams.u4SysLogRole;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSyslogLogFile
 Input       :  The Indices

                The Object 
                retValFsSyslogLogFile
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogLogFile (INT4 *pi4RetValFsSyslogLogFile)
{
    *pi4RetValFsSyslogLogFile = gsSysLogParams.u4LocalStorage;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSyslogMail
 Input       :  The Indices

                The Object 
                retValFsSyslogMail
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogMail (INT4 *pi4RetValFsSyslogMail)
{
    *pi4RetValFsSyslogMail = gsSysLogParams.u4MailOption;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSyslogProfile
 Input       :  The Indices

                The Object 
                retValFsSyslogProfile
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogProfile (INT4 *pi4RetValFsSyslogProfile)
{
    *pi4RetValFsSyslogProfile = gsSysLogParams.u4SysLogProfile;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSyslogRelayPort
 Input       :  The Indices

                The Object 
                retValFsSyslogRelayPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogRelayPort (INT4 *pi4RetValFsSyslogRelayPort)
{
    *pi4RetValFsSyslogRelayPort = gsSysLogParams.u4SysLogPort;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSyslogRelayTransType
 Input       :  The Indices

                The Object 
                retValFsSyslogRelayTransType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogRelayTransType (INT4 *pi4RetValFsSyslogRelayTransType)
{
    *pi4RetValFsSyslogRelayTransType = gsSysLogParams.u4RelayTransType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSyslogFileNameOne
 Input       :  The Indices

                The Object 
                retValFsSyslogFileNameOne
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogFileNameOne (tSNMP_OCTET_STRING_TYPE * pRetValFsSyslogFileNameOne)
{
    MEMSET (pRetValFsSyslogFileNameOne->pu1_OctetList, SYSLOG_ZERO,
            SYS_MAX_FILE_NAME_LEN);
    MEMCPY (pRetValFsSyslogFileNameOne->pu1_OctetList,
            gsSysLogParams.ai1FileOne, STRLEN (gsSysLogParams.ai1FileOne));
    pRetValFsSyslogFileNameOne->i4_Length = STRLEN (gsSysLogParams.ai1FileOne);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSyslogFileNameTwo
 Input       :  The Indices

                The Object 
                retValFsSyslogFileNameTwo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogFileNameTwo (tSNMP_OCTET_STRING_TYPE * pRetValFsSyslogFileNameTwo)
{
    MEMSET (pRetValFsSyslogFileNameTwo->pu1_OctetList, SYSLOG_ZERO,
            SYS_MAX_FILE_NAME_LEN);
    MEMCPY (pRetValFsSyslogFileNameTwo->pu1_OctetList,
            gsSysLogParams.ai1FileTwo, STRLEN (gsSysLogParams.ai1FileTwo));
    pRetValFsSyslogFileNameTwo->i4_Length = STRLEN (gsSysLogParams.ai1FileTwo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSyslogFileNameThree
 Input       :  The Indices

                The Object 
                retValFsSyslogFileNameThree
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogFileNameThree (tSNMP_OCTET_STRING_TYPE *
                             pRetValFsSyslogFileNameThree)
{
    MEMSET (pRetValFsSyslogFileNameThree->pu1_OctetList, SYSLOG_ZERO,
            SYS_MAX_FILE_NAME_LEN);
    MEMCPY (pRetValFsSyslogFileNameThree->pu1_OctetList,
            gsSysLogParams.ai1FileThree, STRLEN (gsSysLogParams.ai1FileThree));
    pRetValFsSyslogFileNameThree->i4_Length =
        STRLEN (gsSysLogParams.ai1FileThree);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSyslogFacility
 Input       :  The Indices

                The Object 
                setValFsSyslogFacility
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogFacility (INT4 i4SetValFsSyslogFacility)
{
    gsSysLogParams.u4Facility = i4SetValFsSyslogFacility;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSyslogRole
 Input       :  The Indices

                The Object 
                setValFsSyslogRole
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogRole (INT4 i4SetValFsSyslogRole)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SYSLOG_ZERO;

    if (gsSysLogParams.u4SysLogRole == (UINT4) i4SetValFsSyslogRole)
    {
        return SNMP_SUCCESS;
    }
    /*If the already existing role is relay and the user trying to
       configure the role as device then close the socket */

    if ((gsSysLogParams.u4SysLogRole == SYSLOG_RELAY_ROLE) &&
        (i4SetValFsSyslogRole == SYSLOG_DEVICE_ROLE))
    {
        SysLogRelayDisable ();
    }
    /*If the already existing role is device and the user trying to
       configure the role as relay then open the socket */

    else if ((gsSysLogParams.u4SysLogRole == SYSLOG_DEVICE_ROLE) &&
             (i4SetValFsSyslogRole == SYSLOG_RELAY_ROLE))
    {
        SysLogRelayEnable ();
    }

    gsSysLogParams.u4SysLogRole = i4SetValFsSyslogRole;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSyslogRole;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSyslogRole) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SYSLOG_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsSyslogRole));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSyslogLogFile
 Input       :  The Indices

                The Object 
                setValFsSyslogLogFile
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogLogFile (INT4 i4SetValFsSyslogLogFile)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SYSLOG_ZERO;
    gsSysLogParams.u4LocalStorage = i4SetValFsSyslogLogFile;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSyslogLogFile;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSyslogLogFile) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SYSLOG_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsSyslogLogFile));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSyslogMail
 Input       :  The Indices

                The Object 
                setValFsSyslogMail
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogMail (INT4 i4SetValFsSyslogMail)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SYSLOG_ZERO;
    gsSysLogParams.u4MailOption = i4SetValFsSyslogMail;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSyslogMail;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSyslogMail) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SYSLOG_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsSyslogMail));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSyslogProfile
 Input       :  The Indices

                The Object 
                setValFsSyslogProfile
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogProfile (INT4 i4SetValFsSyslogProfile)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SYSLOG_ZERO;
    gsSysLogParams.u4SysLogProfile = i4SetValFsSyslogProfile;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSyslogProfile;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSyslogProfile) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SYSLOG_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsSyslogProfile));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSyslogRelayPort
 Input       :  The Indices

                The Object 
                setValFsSyslogRelayPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogRelayPort (INT4 i4SetValFsSyslogRelayPort)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SYSLOG_ZERO;

    /* If the Already existing value and configured value are same
       return success */
    if (gsSysLogParams.u4SysLogPort == (UINT4) i4SetValFsSyslogRelayPort)
    {
        return SNMP_SUCCESS;
    }
    gsSysLogParams.u4SysLogPort = i4SetValFsSyslogRelayPort;

    if ((gsSysLogParams.u4SysLogRole == SYSLOG_RELAY_ROLE) &&
        (i4SetValFsSyslogRelayPort != BSD_SYSLOG_PORT))
    {
        SysLogRelayDisable ();
        SysLogRelayEnable ();
    }
    else if ((gsSysLogParams.u4SysLogRole == SYSLOG_RELAY_ROLE) &&
             (i4SetValFsSyslogRelayPort == BSD_SYSLOG_PORT))
    {
        SysLogRelayDisable ();
        SysLogRelayEnable ();
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSyslogRelayPort;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSyslogRelayPort) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SYSLOG_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsSyslogRelayPort));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSyslogRelayTransType
 Input       :  The Indices

                The Object 
                setValFsSyslogRelayTransType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogRelayTransType (INT4 i4SetValFsSyslogRelayTransType)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SYSLOG_ZERO;

    /* If the Already existing value and configured value are same
       return success */
    if (gsSysLogParams.u4RelayTransType ==
        (UINT4) i4SetValFsSyslogRelayTransType)
    {
        return SNMP_SUCCESS;
    }
    /* If the already existing tranport type and the user configuring
       tranport type are different then set the global tranport type
       ,close the already existing socket and open the socket type as
       configured by user */

    gsSysLogParams.u4RelayTransType = i4SetValFsSyslogRelayTransType;
    SysLogRelayDisable ();
    SysLogRelayEnable ();

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSyslogRelayTransType;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSyslogRelayTransType) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SYSLOG_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsSyslogRelayTransType));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSyslogFileNameOne
 Input       :  The Indices

                The Object 
                setValFsSyslogFileNameOne
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogFileNameOne (tSNMP_OCTET_STRING_TYPE * pSetValFsSyslogFileNameOne)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SYSLOG_ZERO;

    if ((gsSysLogParams.ai1FileOne != NULL) &&
        (STRLEN (gsSysLogParams.ai1FileOne) != SYSLOG_ZERO))
    {
        /* When already a file exists for the file type, remove all entries 
         * in filetable and create new entries with new file name */
        SyslogReplaceFileTableEntries ((INT1 *) &(gsSysLogParams.ai1FileOne),
                                       pSetValFsSyslogFileNameOne);
    }

    MEMSET (gsSysLogParams.ai1FileOne, SYSLOG_ZERO, SYS_MAX_FILE_NAME_LEN + 3);
    MEMCPY (gsSysLogParams.ai1FileOne,
            pSetValFsSyslogFileNameOne->pu1_OctetList,
            pSetValFsSyslogFileNameOne->i4_Length);

    RM_GET_SEQ_NUM (&u4SeqNum);

    SnmpNotifyInfo.pu4ObjectId = FsSyslogFileNameOne;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSyslogFileNameOne) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SYSLOG_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s", pSetValFsSyslogFileNameOne));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSyslogFileNameTwo
 Input       :  The Indices

                The Object 
                setValFsSyslogFileNameTwo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogFileNameTwo (tSNMP_OCTET_STRING_TYPE * pSetValFsSyslogFileNameTwo)
{

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SYSLOG_ZERO;

    if ((gsSysLogParams.ai1FileTwo != NULL) &&
        (STRLEN (gsSysLogParams.ai1FileTwo) != SYSLOG_ZERO))
    {
        /* When already a file exists for the file type, remove all entries 
         * in filetable and create new entries with new file name */
        SyslogReplaceFileTableEntries ((INT1 *) &(gsSysLogParams.ai1FileTwo),
                                       pSetValFsSyslogFileNameTwo);
    }

    MEMSET (gsSysLogParams.ai1FileTwo, SYSLOG_ZERO, SYS_MAX_FILE_NAME_LEN + 3);
    MEMCPY (gsSysLogParams.ai1FileTwo,
            pSetValFsSyslogFileNameTwo->pu1_OctetList,
            pSetValFsSyslogFileNameTwo->i4_Length);

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSyslogFileNameTwo;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSyslogFileNameTwo) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SYSLOG_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s", pSetValFsSyslogFileNameTwo));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSyslogFileNameThree
 Input       :  The Indices

                The Object 
                setValFsSyslogFileNameThree
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogFileNameThree (tSNMP_OCTET_STRING_TYPE *
                             pSetValFsSyslogFileNameThree)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SYSLOG_ZERO;

    if ((gsSysLogParams.ai1FileThree != NULL) &&
        (STRLEN (gsSysLogParams.ai1FileThree) != SYSLOG_ZERO))
    {
        /* When already a file exists for the file type, remove all entries 
         * in filetable and create new entries with new file name */
        SyslogReplaceFileTableEntries ((INT1 *) &(gsSysLogParams.ai1FileThree),
                                       pSetValFsSyslogFileNameThree);
    }

    MEMSET (gsSysLogParams.ai1FileThree, SYSLOG_ZERO,
            SYS_MAX_FILE_NAME_LEN + 3);
    MEMCPY (gsSysLogParams.ai1FileThree,
            pSetValFsSyslogFileNameThree->pu1_OctetList,
            pSetValFsSyslogFileNameThree->i4_Length);

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSyslogFileNameThree;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSyslogFileNameThree) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SYSLOG_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s", pSetValFsSyslogFileNameThree));

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSyslogFacility
 Input       :  The Indices

                The Object 
                testValFsSyslogFacility
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogFacility (UINT4 *pu4ErrorCode, INT4 i4TestValFsSyslogFacility)
{
    if ((i4TestValFsSyslogFacility != SYSLG_FACILITY_LOCAL0) &&
        (i4TestValFsSyslogFacility != SYSLG_FACILITY_LOCAL1) &&
        (i4TestValFsSyslogFacility != SYSLG_FACILITY_LOCAL2) &&
        (i4TestValFsSyslogFacility != SYSLG_FACILITY_LOCAL3) &&
        (i4TestValFsSyslogFacility != SYSLG_FACILITY_LOCAL4) &&
        (i4TestValFsSyslogFacility != SYSLG_FACILITY_LOCAL5) &&
        (i4TestValFsSyslogFacility != SYSLG_FACILITY_LOCAL6) &&
        (i4TestValFsSyslogFacility != SYSLG_FACILITY_LOCAL7))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INV_FACILITY);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSyslogRole
 Input       :  The Indices

                The Object 
                testValFsSyslogRole
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogRole (UINT4 *pu4ErrorCode, INT4 i4TestValFsSyslogRole)
{
    if (gsSysLogParams.u4SysLogRole == (UINT4) i4TestValFsSyslogRole)
    {
        return SNMP_SUCCESS;
    }
    if ((i4TestValFsSyslogRole != SYSLOG_DEVICE_ROLE) &&
        (i4TestValFsSyslogRole != SYSLOG_RELAY_ROLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INV_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSyslogLogFile
 Input       :  The Indices

                The Object 
                testValFsSyslogLogFile
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogLogFile (UINT4 *pu4ErrorCode, INT4 i4TestValFsSyslogLogFile)
{
    if ((i4TestValFsSyslogLogFile != SYSLOG_ENABLE) &&
        (i4TestValFsSyslogLogFile != SYSLOG_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INV_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSyslogMail
 Input       :  The Indices

                The Object 
                testValFsSyslogMail
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogMail (UINT4 *pu4ErrorCode, INT4 i4TestValFsSyslogMail)
{
    if ((i4TestValFsSyslogMail != SYSLOG_ENABLE) &&
        (i4TestValFsSyslogMail != SYSLOG_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INV_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSyslogProfile
 Input       :  The Indices

                The Object 
                testValFsSyslogProfile
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogProfile (UINT4 *pu4ErrorCode, INT4 i4TestValFsSyslogProfile)
{
    if ((i4TestValFsSyslogProfile != RSYSLOG_RAW_PROFILE) &&
        (i4TestValFsSyslogProfile != RSYSLOG_COOKED_PROFILE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INV_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSyslogRelayPort
 Input       :  The Indices

                The Object 
                testValFsSyslogRelayPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogRelayPort (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsSyslogRelayPort)
{
    /* If the Already existing value and configured value are same
       return failure */
    if (gsSysLogParams.u4SysLogPort == (UINT4) i4TestValFsSyslogRelayPort)
    {
        return SNMP_SUCCESS;
    }
    if ((i4TestValFsSyslogRelayPort < BSD_SYSLOG_PORT_MIN) ||
        (i4TestValFsSyslogRelayPort > BSD_SYSLOG_PORT_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INV_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSyslogRelayTransType
 Input       :  The Indices

                The Object 
                testValFsSyslogRelayTransType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogRelayTransType (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsSyslogRelayTransType)
{
    if (gsSysLogParams.u4RelayTransType ==
        (UINT4) i4TestValFsSyslogRelayTransType)
    {
        return SNMP_SUCCESS;
    }

    if (gsSysLogParams.u4SysLogRole == SYSLOG_RELAY_ROLE)
    {
        if ((i4TestValFsSyslogRelayTransType == SYSLOG_RELAY_UDP) ||
            (i4TestValFsSyslogRelayTransType == SYSLOG_RELAY_TCP))
        {
            return SNMP_SUCCESS;
        }
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_SYSLG_INV_STATUS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsSyslogFileNameOne
 Input       :  The Indices

                The Object 
                testValFsSyslogFileNameOne
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogFileNameOne (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValFsSyslogFileNameOne)
{
    if (pTestValFsSyslogFileNameOne->pu1_OctetList == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INV_FILE_NAME);
        return SNMP_FAILURE;
    }

    if ((pTestValFsSyslogFileNameOne->i4_Length == SYSLOG_ZERO)
        || (pTestValFsSyslogFileNameOne->i4_Length >= SYS_MAX_FILE_NAME_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_SYSLG_INV_FILE_NAME);
        return SNMP_FAILURE;
    }

    if (STRLEN (gsSysLogParams.ai1FileTwo) != SYSLOG_ZERO)
    {
        if (pTestValFsSyslogFileNameOne->i4_Length ==
            (INT4) STRLEN (gsSysLogParams.ai1FileTwo))
        {
            if ((MEMCMP (pTestValFsSyslogFileNameOne->pu1_OctetList,
                         gsSysLogParams.ai1FileTwo,
                         SYS_MAX_FILE_NAME_LEN)) == SYSLOG_ZERO)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_SYSLG_INV_FILE_NAME);
                return SNMP_FAILURE;
            }
        }
    }

    if (STRLEN (gsSysLogParams.ai1FileThree) != SYSLOG_ZERO)
    {
        if (pTestValFsSyslogFileNameOne->i4_Length ==
            (INT4) STRLEN (gsSysLogParams.ai1FileThree))
        {
            if ((MEMCMP (pTestValFsSyslogFileNameOne->pu1_OctetList,
                         gsSysLogParams.ai1FileThree,
                         SYS_MAX_FILE_NAME_LEN)) == SYSLOG_ZERO)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_SYSLG_INV_FILE_NAME);
                return SNMP_FAILURE;
            }
        }
    }

    if (pTestValFsSyslogFileNameOne->i4_Length >= SYS_MAX_FILE_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_SYSLG_INV_FILE_NAME);
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValFsSyslogFileNameOne->pu1_OctetList,
                              pTestValFsSyslogFileNameOne->i4_Length)
        == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSyslogFileNameTwo
 Input       :  The Indices

                The Object 
                testValFsSyslogFileNameTwo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogFileNameTwo (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValFsSyslogFileNameTwo)
{
    if (pTestValFsSyslogFileNameTwo->pu1_OctetList == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INV_FILE_NAME);
        return SNMP_FAILURE;
    }

    if ((pTestValFsSyslogFileNameTwo->i4_Length == SYSLOG_ZERO)
        || (pTestValFsSyslogFileNameTwo->i4_Length >= SYS_MAX_FILE_NAME_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_SYSLG_INV_FILE_NAME);
        return SNMP_FAILURE;
    }

    if (STRLEN (gsSysLogParams.ai1FileOne) != SYSLOG_ZERO)
    {
        if (pTestValFsSyslogFileNameTwo->i4_Length ==
            (INT4) STRLEN (gsSysLogParams.ai1FileOne))
        {
            if ((MEMCMP (pTestValFsSyslogFileNameTwo->pu1_OctetList,
                         gsSysLogParams.ai1FileOne,
                         SYS_MAX_FILE_NAME_LEN)) == SYSLOG_ZERO)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_SYSLG_INV_FILE_NAME);
                return SNMP_FAILURE;
            }
        }
    }

    if (STRLEN (gsSysLogParams.ai1FileThree) != SYSLOG_ZERO)
    {
        if (pTestValFsSyslogFileNameTwo->i4_Length ==
            (INT4) STRLEN (gsSysLogParams.ai1FileThree))
        {
            if ((MEMCMP (pTestValFsSyslogFileNameTwo->pu1_OctetList,
                         gsSysLogParams.ai1FileThree,
                         SYS_MAX_FILE_NAME_LEN)) == SYSLOG_ZERO)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_SYSLG_INV_FILE_NAME);
                return SNMP_FAILURE;
            }
        }
    }

    if (pTestValFsSyslogFileNameTwo->i4_Length >= SYS_MAX_FILE_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_SYSLG_INV_FILE_NAME);
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValFsSyslogFileNameTwo->pu1_OctetList,
                              pTestValFsSyslogFileNameTwo->i4_Length)
        == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSyslogFileNameThree
 Input       :  The Indices

                The Object 
                testValFsSyslogFileNameThree
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogFileNameThree (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsSyslogFileNameThree)
{
    if (pTestValFsSyslogFileNameThree->pu1_OctetList == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INV_FILE_NAME);
        return SNMP_FAILURE;
    }

    if ((pTestValFsSyslogFileNameThree->i4_Length == SYSLOG_ZERO)
        || (pTestValFsSyslogFileNameThree->i4_Length > SYS_MAX_FILE_NAME_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_SYSLG_INV_FILE_NAME);
        return SNMP_FAILURE;
    }

    if (STRLEN (gsSysLogParams.ai1FileOne) != SYSLOG_ZERO)
    {
        if (pTestValFsSyslogFileNameThree->i4_Length ==
            (INT4) STRLEN (gsSysLogParams.ai1FileOne))
        {
            if ((MEMCMP (pTestValFsSyslogFileNameThree->pu1_OctetList,
                         gsSysLogParams.ai1FileOne,
                         SYS_MAX_FILE_NAME_LEN)) == SYSLOG_ZERO)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_SYSLG_INV_FILE_NAME);
                return SNMP_FAILURE;
            }
        }
    }

    if (STRLEN (gsSysLogParams.ai1FileTwo) != SYSLOG_ZERO)
    {
        if (pTestValFsSyslogFileNameThree->i4_Length ==
            (INT4) STRLEN (gsSysLogParams.ai1FileTwo))
        {
            if ((MEMCMP (pTestValFsSyslogFileNameThree->pu1_OctetList,
                         gsSysLogParams.ai1FileTwo,
                         SYS_MAX_FILE_NAME_LEN)) == SYSLOG_ZERO)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_SYSLG_INV_FILE_NAME);
                return SNMP_FAILURE;
            }
        }
    }

    if (pTestValFsSyslogFileNameThree->i4_Length >= SYS_MAX_FILE_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_SYSLG_INV_FILE_NAME);
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValFsSyslogFileNameThree->pu1_OctetList,
                              pTestValFsSyslogFileNameThree->i4_Length)
        == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSyslogFacility
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogFacility (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSyslogRole
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogRole (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSyslogLogFile
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogLogFile (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSyslogMail
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogMail (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSyslogProfile
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogProfile (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSyslogRelayPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogRelayPort (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSyslogRelayTransType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogRelayTransType (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSyslogFileNameOne
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogFileNameOne (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSyslogFileNameTwo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogFileNameTwo (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSyslogFileNameThree
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogFileNameThree (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsSyslogFileTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsSyslogFileTable
 Input       :  The Indices
                FsSyslogFilePriority
                FsSyslogFileName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsSyslogFileTable (INT4 i4FsSyslogFilePriority,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsSyslogFileName)
{
    if (((i4FsSyslogFilePriority >= SYSLOG_PRIORITY_MIN) &&
         (i4FsSyslogFilePriority <= SYSLOG_PRIORITY_MAX)) &&
        (pFsSyslogFileName != NULL))

    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsSyslogFileTable
 Input       :  The Indices
                FsSyslogFilePriority
                FsSyslogFileName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsSyslogFileTable (INT4 *pi4FsSyslogFilePriority,
                                   tSNMP_OCTET_STRING_TYPE * pFsSyslogFileName)
{
    tSysLogFileInfo    *pFileInfo = NULL;

    pFileInfo =
        (tSysLogFileInfo *) TMO_SLL_First (&(gSysLogGlobalInfo.sysFileTblList));
    if (pFileInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4FsSyslogFilePriority = (INT4) pFileInfo->u4Priority;

        pFsSyslogFileName->i4_Length = STRLEN (pFileInfo->au1FileName);
        MEMSET (pFsSyslogFileName->pu1_OctetList, SYSLOG_ZERO,
                STRLEN (pFileInfo->au1FileName));
        MEMCPY (pFsSyslogFileName->pu1_OctetList, pFileInfo->au1FileName,
                STRLEN (pFileInfo->au1FileName));

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsSyslogFileTable
 Input       :  The Indices
                FsSyslogFilePriority
                nextFsSyslogFilePriority
                FsSyslogFileName
                nextFsSyslogFileName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsSyslogFileTable (INT4 i4FsSyslogFilePriority,
                                  INT4 *pi4NextFsSyslogFilePriority,
                                  tSNMP_OCTET_STRING_TYPE * pFsSyslogFileName,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pNextFsSyslogFileName)
{
    tSysLogFileInfo    *pFileInfo = NULL;
    tSysLogFileInfo    *pFileInfoNext = NULL;
    UINT1               au1TempFileName[SYS_MAX_FILE_NAME_LEN];

    MEMSET (au1TempFileName, 0, SYS_MAX_FILE_NAME_LEN);

    MEMCPY (au1TempFileName, pFsSyslogFileName->pu1_OctetList,
            pFsSyslogFileName->i4_Length);

    pFileInfo = (tSysLogFileInfo *) FileUtilFindEntry (i4FsSyslogFilePriority,
                                                       au1TempFileName);
    if (pFileInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pFileInfoNext =
        (tSysLogFileInfo *) TMO_SLL_Next ((&gSysLogGlobalInfo.sysFileTblList),
                                          &pFileInfo->NextNode);

    if (pFileInfoNext == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsSyslogFilePriority = (INT4) pFileInfoNext->u4Priority;
    pNextFsSyslogFileName->i4_Length = STRLEN (pFileInfoNext->au1FileName);
    MEMSET (pNextFsSyslogFileName->pu1_OctetList, SYSLOG_ZERO,
            SYS_MAX_FILE_NAME_LEN);
    MEMCPY (pNextFsSyslogFileName->pu1_OctetList, pFileInfoNext->au1FileName,
            STRLEN (pFileInfoNext->au1FileName));
    pNextFsSyslogFileName->i4_Length = STRLEN (pFileInfoNext->au1FileName);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSyslogFileRowStatus
 Input       :  The Indices
                FsSyslogFilePriority
                FsSyslogFileName

                The Object 
                retValFsSyslogFileRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogFileRowStatus (INT4 i4FsSyslogFilePriority,
                             tSNMP_OCTET_STRING_TYPE * pFsSyslogFileName,
                             INT4 *pi4RetValFsSyslogFileRowStatus)
{
    tSysLogFileInfo    *pFileInfo = NULL;
    UINT1               au1TempFileName[SYS_MAX_FILE_NAME_LEN];

    MEMSET (au1TempFileName, 0, SYS_MAX_FILE_NAME_LEN);

    MEMCPY (au1TempFileName, pFsSyslogFileName->pu1_OctetList,
            pFsSyslogFileName->i4_Length);

    pFileInfo = (tSysLogFileInfo *) FileUtilFindEntry (i4FsSyslogFilePriority,
                                                       au1TempFileName);
    if (pFileInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsSyslogFileRowStatus = pFileInfo->i4FileRowStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSyslogFileRowStatus
 Input       :  The Indices
                FsSyslogFilePriority
                FsSyslogFileName

                The Object 
                setValFsSyslogFileRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogFileRowStatus (INT4 i4FsSyslogFilePriority,
                             tSNMP_OCTET_STRING_TYPE * pFsSyslogFileName,
                             INT4 i4SetValFsSyslogFileRowStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SYSLOG_ZERO;
    tSysLogFileInfo    *pFileInfo = NULL;
    UINT1               au1Name[SYS_MAX_FILE_NAME_LEN + 1];

    MEMSET (au1Name, 0, sizeof (au1Name));

    MEMCPY (au1Name, pFsSyslogFileName->pu1_OctetList,
            pFsSyslogFileName->i4_Length);
    pFileInfo =
        (tSysLogFileInfo *) FileUtilFindEntry (i4FsSyslogFilePriority, au1Name);

    if ((pFileInfo != NULL)
        && (i4SetValFsSyslogFileRowStatus == SYS_CREATE_AND_WAIT))
    {
        return SNMP_FAILURE;
    }
    if ((pFileInfo == NULL)
        && (i4SetValFsSyslogFileRowStatus != SYS_CREATE_AND_WAIT))
    {
        return SNMP_FAILURE;
    }
    switch (i4SetValFsSyslogFileRowStatus)
    {
        case SYS_CREATE_AND_WAIT:

            if ((pFileInfo = (tSysLogFileInfo *)
                 (MemAllocMemBlk (gSysLogGlobalInfo.sysFilePoolId))) == NULL)
            {
                return SNMP_FAILURE;
            }
            /* initialize the new fileentry structure */
            MEMSET (pFileInfo, SYSLOG_ZERO, sizeof (tSysLogFileInfo));

            /* initialize the linked list node for new file table entry */
            TMO_SLL_Init_Node (&pFileInfo->NextNode);

            pFileInfo->u4Priority = i4FsSyslogFilePriority;
            MEMCPY (pFileInfo->au1FileName, pFsSyslogFileName->pu1_OctetList,
                    pFsSyslogFileName->i4_Length);

            pFileInfo->i4FileRowStatus = SYS_NOT_IN_SERVICE;

            /* add the new entry  to linked list */
            SysLogFileInfoAddNodeToList (pFileInfo);

            break;
        case SYS_ACTIVE:

            pFileInfo->u4Priority = i4FsSyslogFilePriority;
            MEMCPY (pFileInfo->au1FileName, pFsSyslogFileName->pu1_OctetList,
                    pFsSyslogFileName->i4_Length);

            pFileInfo->i4FileRowStatus = SYS_ACTIVE;

            break;
        case SYS_NOT_IN_SERVICE:
            if ((pFileInfo != NULL)
                && (pFileInfo->i4FileRowStatus != SYS_ACTIVE))
            {
                return SNMP_FAILURE;
            }

            pFileInfo->i4FileRowStatus = SYS_NOT_IN_SERVICE;
            break;
        case SYS_DESTROY:
            if ((pFsSyslogFileName->pu1_OctetList) != NULL)
            {
                /* delink the node from the list */
                TMO_SLL_Delete (&(gSysLogGlobalInfo.sysFileTblList),
                                &pFileInfo->NextNode);

                /* release the memory for the list */
                if (MemReleaseMemBlock (gSysLogGlobalInfo.sysFilePoolId,
                                        (UINT1 *) pFileInfo) == MEM_FAILURE)
                {
                    return SYSLOG_FAILURE;
                }

            }
            break;
        default:
            return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSyslogFileRowStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSyslogFileRowStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SYSLOG_TWO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i", i4FsSyslogFilePriority,
                      pFsSyslogFileName, i4SetValFsSyslogFileRowStatus));

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSyslogFileRowStatus
 Input       :  The Indices
                FsSyslogFilePriority
                FsSyslogFileName

                The Object 
                testValFsSyslogFileRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogFileRowStatus (UINT4 *pu4ErrorCode,
                                INT4 i4FsSyslogFilePriority,
                                tSNMP_OCTET_STRING_TYPE * pFsSyslogFileName,
                                INT4 i4TestValFsSyslogFileRowStatus)
{
    tSysLogFileInfo    *pFileInfo = NULL;
    UINT1               au1Name[SYS_MAX_FILE_NAME_LEN + 1];

    MEMSET (au1Name, 0, sizeof (au1Name));

    MEMCPY (au1Name, pFsSyslogFileName->pu1_OctetList,
            pFsSyslogFileName->i4_Length);

    if (!((i4FsSyslogFilePriority >= SYSLOG_PRIORITY_MIN) &&
          (i4FsSyslogFilePriority <= SYSLOG_PRIORITY_MAX)))
    {
        return SNMP_FAILURE;

    }
    if ((STRLEN (gsSysLogParams.ai1FileOne) != SYSLOG_ZERO) &&
        (STRLEN (gsSysLogParams.ai1FileTwo) != SYSLOG_ZERO) &&
        (STRLEN (gsSysLogParams.ai1FileThree) != SYSLOG_ZERO))
    {
        if (((MEMCMP
              (au1Name, gsSysLogParams.ai1FileOne,
               SYS_MAX_FILE_NAME_LEN) != SYSLOG_ZERO)
             &&
             (MEMCMP
              (au1Name, gsSysLogParams.ai1FileTwo,
               SYS_MAX_FILE_NAME_LEN) != SYSLOG_ZERO)
             &&
             (MEMCMP
              (au1Name, gsSysLogParams.ai1FileThree,
               SYS_MAX_FILE_NAME_LEN) != SYSLOG_ZERO)))

        {
            CLI_SET_ERR (CLI_SYSLG_NO_MATCH_FILE_NAME);
            return SNMP_FAILURE;
        }
    }
    else if ((STRLEN (gsSysLogParams.ai1FileOne) != SYSLOG_ZERO) &&
             (STRLEN (gsSysLogParams.ai1FileTwo) != SYSLOG_ZERO))
    {
        if (((MEMCMP
              (au1Name, gsSysLogParams.ai1FileOne,
               SYS_MAX_FILE_NAME_LEN) != SYSLOG_ZERO)
             &&
             (MEMCMP
              (au1Name, gsSysLogParams.ai1FileTwo,
               SYS_MAX_FILE_NAME_LEN) != SYSLOG_ZERO)))

        {
            CLI_SET_ERR (CLI_SYSLG_NO_MATCH_FILE_NAME);
            return SNMP_FAILURE;
        }
    }
    else if ((STRLEN (gsSysLogParams.ai1FileOne) != SYSLOG_ZERO))
    {
        if (((MEMCMP
              (au1Name, gsSysLogParams.ai1FileOne,
               SYS_MAX_FILE_NAME_LEN) != SYSLOG_ZERO)))

        {
            CLI_SET_ERR (CLI_SYSLG_NO_MATCH_FILE_NAME);
            return SNMP_FAILURE;
        }
    }
    else
    {
        CLI_SET_ERR (CLI_SYSLG_NO_MATCH_FILE_NAME);
        return SNMP_FAILURE;
    }

    pFileInfo = (tSysLogFileInfo *) FileUtilFindEntry (i4FsSyslogFilePriority,
                                                       au1Name);
    switch (i4TestValFsSyslogFileRowStatus)
    {
        case SYS_ACTIVE:
        case SYS_NOT_IN_SERVICE:
        case SYS_NOT_READY:
        case SYS_DESTROY:

            if (pFileInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
                /*Cannot obtain node for deletion */
            }
            break;
        case SYS_CREATE_AND_GO:
        case SYS_CREATE_AND_WAIT:
            if (pFileInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
                /*Node already exists */
            }
            break;
        default:

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
            /*Invalid Row Status */
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSyslogFileTable
 Input       :  The Indices
                FsSyslogFilePriority
                FsSyslogFileName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogFileTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 *  Function    :  nmhGetFsSyslogServerUpDownTrap
 *  Input       :  The Indices
 *
 *                   The Object
 *                   retValFsSyslogServerUpDownTrap
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsSyslogServerUpDownTrap (INT4 *pi4RetValFsSyslogServerUpDownTrap)
{
    *pi4RetValFsSyslogServerUpDownTrap = gi4SlogSrvUpDownTrap;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSyslogFileSize
 Input       :  The Indices
                The Object
                retValFsSyslogFileSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogFileSize (INT4 *pi4RetValFsSyslogFileSize)
{
    *pi4RetValFsSyslogFileSize = IssGetSyslogFileSize ();
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 *  Function    :  nmhSetFsSyslogServerUpDownTrap
 *  Input       :  The Indices
 *
 *                   The Object
 *                   setValFsSyslogServerUpDownTrap
 *  Output      :  The Set Low Lev Routine Take the Indices &
 *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhSetFsSyslogServerUpDownTrap (INT4 i4SetValFsSyslogServerUpDownTrap)
{
    if ((i4SetValFsSyslogServerUpDownTrap == SNMP_TRAP_ENABLE)
        || (i4SetValFsSyslogServerUpDownTrap == SNMP_TRAP_DISABLE))
    {
        gi4SlogSrvUpDownTrap = i4SetValFsSyslogServerUpDownTrap;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsSyslogFileSize
 Input       :  The Indices
                The Object
                setValFsSyslogFileSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogFileSize (INT4 i4SetValFsSyslogFileSize)
{
    INT4                i4RetVal;

    i4RetVal = IssSetSyslogFileSize (i4SetValFsSyslogFileSize);
    if (i4RetVal == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 *  Function    :  nmhTestv2FsSyslogServerUpDownTrap
 *   Input       :  The Indices
 *
 *                   The Object
                 testValFsSyslogServerUpDownTrap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogServerUpDownTrap (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsSyslogServerUpDownTrap)
{
    if ((i4TestValFsSyslogServerUpDownTrap == SNMP_TRAP_ENABLE)
        || (i4TestValFsSyslogServerUpDownTrap == SNMP_TRAP_DISABLE))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INV_STATUS);
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsSyslogFileSize
 Input       :  The Indices

                The Object
                testValFsSyslogFileSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogFileSize (UINT4 *pu4ErrorCode, INT4 i4TestValFsSyslogFileSize)
{
    if (i4TestValFsSyslogFileSize >= SYSLOG_MIN_FILESIZE
        && i4TestValFsSyslogFileSize <= SYSLOG_MAX_FILESIZE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INV_SIZE);
        return SNMP_FAILURE;
    }

}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 *  Function    :  nmhDepv2FsSyslogServerUpDownTrap
 *   Output      :  The Dependency Low Lev Routine Take the Indices &
 *                   check whether dependency is met or not.
 *                  Stores the value of error code in the Return val
  Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogServerUpDownTrap (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSyslogFileSize
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogFileSize (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSyslogLogSrvAddr
 Input       :  The Indices

                The Object 
                retValFsSyslogLogSrvAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogLogSrvAddr (UINT4 *pu4RetValFsSyslogLogSrvAddr)
{
    *pu4RetValFsSyslogLogSrvAddr = OSIX_NTOHL (gsSysLogParams.u4LogServerIp);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSyslogLogNoLogServer
 Input       :  The Indices

                The Object 
                retValFsSyslogLogNoLogServer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogLogNoLogServer (INT4 *pi4RetValFsSyslogLogNoLogServer)
{
    *pi4RetValFsSyslogLogNoLogServer = SYSLOG_FALSE;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSyslogLogSrvAddr
 Input       :  The Indices

                The Object 
                setValFsSyslogLogSrvAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogLogSrvAddr (UINT4 u4SetValFsSyslogLogSrvAddr)
{
    if ((SysLogAddLogServerIp (OSIX_HTONL (u4SetValFsSyslogLogSrvAddr)) !=
         SYSLOG_SUCCESS))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSyslogLogNoLogServer
 Input       :  The Indices

                The Object 
                setValFsSyslogLogNoLogServer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogLogNoLogServer (INT4 i4SetValFsSyslogLogNoLogServer)
{
    if (i4SetValFsSyslogLogNoLogServer == SYSLOG_TRUE)
    {
        if (gsSysLogParams.u4LogServerIp > 0)
        {
            close (gi4SysLogFd);
            gsSysLogParams.u4LogServerIp = gi4SysLogFd = 0;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSyslogLogSrvAddr
 Input       :  The Indices

                The Object 
                testValFsSyslogLogSrvAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogLogSrvAddr (UINT4 *pu4ErrorCode,
                             UINT4 u4TestValFsSyslogLogSrvAddr)
{
    if (!((SYSLOG_IS_ADDR_CLASS_A (u4TestValFsSyslogLogSrvAddr)) ||
          (SYSLOG_IS_ADDR_CLASS_B (u4TestValFsSyslogLogSrvAddr)) ||
          (SYSLOG_IS_ADDR_CLASS_C (u4TestValFsSyslogLogSrvAddr))) ||
        (u4TestValFsSyslogLogSrvAddr == 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INVALID_IP);
        return SNMP_FAILURE;
    }
#ifdef IP_WANTED
    if (NetIpv4IfIsOurAddress (u4TestValFsSyslogLogSrvAddr) == NETIPV4_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INV_DIRECT_IP);
        return SNMP_FAILURE;
    }

    if (NetIpv4IpIsLocalNet (u4TestValFsSyslogLogSrvAddr) != NETIPV4_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INV_LOCAL_IP);
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSyslogLogNoLogServer
 Input       :  The Indices

                The Object 
                testValFsSyslogLogNoLogServer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogLogNoLogServer (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsSyslogLogNoLogServer)
{
    if ((i4TestValFsSyslogLogNoLogServer != SYSLOG_TRUE) &&
        (i4TestValFsSyslogLogNoLogServer != SYSLOG_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INV_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSyslogLogSrvAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogLogSrvAddr (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSyslogLogNoLogServer
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogLogNoLogServer (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsSyslogFwdTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsSyslogFwdTable
 Input       :  The Indices
                FsSyslogFwdPriority
                FsSyslogFwdAddressType
                FsSyslogFwdServerIP
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsSyslogFwdTable (INT4 i4FsSyslogFwdPriority,
                                          INT4 i4FsSyslogFwdAddressType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsSyslogFwdServerIP)
{
    if (((i4FsSyslogFwdPriority >= SYSLOG_PRIORITY_MIN) &&
         (i4FsSyslogFwdPriority <= SYSLOG_PRIORITY_MAX)) &&
        ((i4FsSyslogFwdAddressType == IPVX_ADDR_FMLY_IPV4) ||
         (i4FsSyslogFwdAddressType == IPVX_ADDR_FMLY_IPV6) ||
         (i4FsSyslogFwdAddressType == IPVX_DNS_FAMILY)) &&
        (pFsSyslogFwdServerIP != NULL))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsSyslogFwdTable
 Input       :  The Indices
                FsSyslogFwdPriority
                FsSyslogFwdAddressType
                FsSyslogFwdServerIP
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsSyslogFwdTable (INT4 *pi4FsSyslogFwdPriority,
                                  INT4 *pi4FsSyslogFwdAddressType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsSyslogFwdServerIP)
{
    tSysLogFwdInfo     *pFwdInfo = NULL;

    pFwdInfo =
        (tSysLogFwdInfo *) TMO_SLL_First (&(gSysLogGlobalInfo.sysFwdTblList));
    if (pFwdInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4FsSyslogFwdPriority = (INT4) pFwdInfo->u4Priority;
        *pi4FsSyslogFwdAddressType = (INT4) pFwdInfo->u4AddrType;
        if (*pi4FsSyslogFwdAddressType == (INT4) IPVX_ADDR_FMLY_IPV4)
        {
            pFsSyslogFwdServerIP->i4_Length = (INT4) IPVX_IPV4_ADDR_LEN;
        }
        else if (*pi4FsSyslogFwdAddressType == (INT4) IPVX_ADDR_FMLY_IPV6)
        {
            pFsSyslogFwdServerIP->i4_Length = (INT4) IPVX_IPV6_ADDR_LEN;
        }
        else
        {
            pFsSyslogFwdServerIP->i4_Length =
                (INT4) STRLEN (pFwdInfo->au1HostName);
        }
        if (*pi4FsSyslogFwdAddressType != (INT4) IPVX_DNS_FAMILY)
        {
            MEMCPY (pFsSyslogFwdServerIP->pu1_OctetList,
                    pFwdInfo->ServIpAddr.au1Addr,
                    pFsSyslogFwdServerIP->i4_Length);
        }
        else
        {
            STRNCPY (pFsSyslogFwdServerIP->pu1_OctetList,
                     pFwdInfo->au1HostName, STRLEN (pFwdInfo->au1HostName));

            pFsSyslogFwdServerIP->pu1_OctetList
                [STRLEN (pFwdInfo->au1HostName)] = '\0';
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsSyslogFwdTable
 Input       :  The Indices
                FsSyslogFwdPriority
                nextFsSyslogFwdPriority
                FsSyslogFwdAddressType
                nextFsSyslogFwdAddressType
                FsSyslogFwdServerIP
                nextFsSyslogFwdServerIP
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsSyslogFwdTable (INT4 i4FsSyslogFwdPriority,
                                 INT4 *pi4NextFsSyslogFwdPriority,
                                 INT4 i4FsSyslogFwdAddressType,
                                 INT4 *pi4NextFsSyslogFwdAddressType,
                                 tSNMP_OCTET_STRING_TYPE * pFsSyslogFwdServerIP,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNextFsSyslogFwdServerIP)
{
    tSysLogFwdInfo     *pFwdInfo = NULL;
    tSysLogFwdInfo     *pFwdInfoNext = NULL;
    UINT4               u4Addrlen = SYSLOG_ZERO;
    UINT1               au1ServerIP[DNS_MAX_QUERY_LEN];

    MEMSET (au1ServerIP, 0, DNS_MAX_QUERY_LEN);

    MEMCPY (au1ServerIP, pFsSyslogFwdServerIP->pu1_OctetList,
            pFsSyslogFwdServerIP->i4_Length);

    pFwdInfo =
        (tSysLogFwdInfo *) FwdUtilFindEntry (i4FsSyslogFwdPriority,
                                             i4FsSyslogFwdAddressType,
                                             au1ServerIP);
    if (pFwdInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pFwdInfoNext =
        (tSysLogFwdInfo *) TMO_SLL_Next ((&gSysLogGlobalInfo.sysFwdTblList),
                                         &pFwdInfo->NextNode);

    if (pFwdInfoNext == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pNextFsSyslogFwdServerIP->pu1_OctetList, SYSLOG_ZERO,
            IPVX_IPV6_ADDR_LEN);
    *pi4NextFsSyslogFwdPriority = (INT4) pFwdInfoNext->u4Priority;
    *pi4NextFsSyslogFwdAddressType = (INT4) pFwdInfoNext->u4AddrType;
    if (*pi4NextFsSyslogFwdAddressType == IPVX_ADDR_FMLY_IPV4)
    {
        u4Addrlen = IPVX_IPV4_ADDR_LEN;
        MEMCPY (pNextFsSyslogFwdServerIP->pu1_OctetList,
                pFwdInfoNext->ServIpAddr.au1Addr, u4Addrlen);

    }
    else if (*pi4NextFsSyslogFwdAddressType == IPVX_ADDR_FMLY_IPV6)
    {
        u4Addrlen = IPVX_IPV6_ADDR_LEN;
        MEMCPY (pNextFsSyslogFwdServerIP->pu1_OctetList,
                pFwdInfoNext->ServIpAddr.au1Addr, u4Addrlen);

    }
    else
    {
        u4Addrlen = STRLEN (pFwdInfoNext->au1HostName);

        MEMCPY (pNextFsSyslogFwdServerIP->pu1_OctetList,
                pFwdInfoNext->au1HostName, u4Addrlen);

    }
    pNextFsSyslogFwdServerIP->i4_Length = u4Addrlen;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSyslogFwdPort
 Input       :  The Indices
                FsSyslogFwdPriority
                FsSyslogFwdAddressType
                FsSyslogFwdServerIP

                The Object 
                retValFsSyslogFwdPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogFwdPort (INT4 i4FsSyslogFwdPriority,
                       INT4 i4FsSyslogFwdAddressType,
                       tSNMP_OCTET_STRING_TYPE * pFsSyslogFwdServerIP,
                       INT4 *pi4RetValFsSyslogFwdPort)
{
    tSysLogFwdInfo     *pFwdInfo = NULL;

    pFwdInfo =
        (tSysLogFwdInfo *) FwdUtilFindEntry (i4FsSyslogFwdPriority,
                                             i4FsSyslogFwdAddressType,
                                             pFsSyslogFwdServerIP->
                                             pu1_OctetList);
    if (pFwdInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsSyslogFwdPort = (INT4) pFwdInfo->u4Port;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSyslogFwdTransType
 Input       :  The Indices
                FsSyslogFwdPriority
                FsSyslogFwdAddressType
                FsSyslogFwdServerIP

                The Object 
                retValFsSyslogFwdTransType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogFwdTransType (INT4 i4FsSyslogFwdPriority,
                            INT4 i4FsSyslogFwdAddressType,
                            tSNMP_OCTET_STRING_TYPE * pFsSyslogFwdServerIP,
                            INT4 *pi4RetValFsSyslogFwdTransType)
{
    tSysLogFwdInfo     *pFwdInfo = NULL;

    pFwdInfo =
        (tSysLogFwdInfo *) FwdUtilFindEntry (i4FsSyslogFwdPriority,
                                             i4FsSyslogFwdAddressType,
                                             pFsSyslogFwdServerIP->
                                             pu1_OctetList);
    if (pFwdInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsSyslogFwdTransType = (INT4) pFwdInfo->u4TransType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSyslogFwdRowStatus
 Input       :  The Indices
                FsSyslogFwdPriority
                FsSyslogFwdAddressType
                FsSyslogFwdServerIP

                The Object 
                retValFsSyslogFwdRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogFwdRowStatus (INT4 i4FsSyslogFwdPriority,
                            INT4 i4FsSyslogFwdAddressType,
                            tSNMP_OCTET_STRING_TYPE * pFsSyslogFwdServerIP,
                            INT4 *pi4RetValFsSyslogFwdRowStatus)
{
    tSysLogFwdInfo     *pFwdInfo = NULL;

    pFwdInfo =
        (tSysLogFwdInfo *) FwdUtilFindEntry (i4FsSyslogFwdPriority,
                                             i4FsSyslogFwdAddressType,
                                             pFsSyslogFwdServerIP->
                                             pu1_OctetList);

    if (pFwdInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsSyslogFwdRowStatus = pFwdInfo->i4FwdRowStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSyslogFwdPort
 Input       :  The Indices
                FsSyslogFwdPriority
                FsSyslogFwdAddressType
                FsSyslogFwdServerIP

                The Object 
                setValFsSyslogFwdPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogFwdPort (INT4 i4FsSyslogFwdPriority,
                       INT4 i4FsSyslogFwdAddressType,
                       tSNMP_OCTET_STRING_TYPE * pFsSyslogFwdServerIP,
                       INT4 i4SetValFsSyslogFwdPort)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SYSLOG_ZERO;
    tSysLogFwdInfo     *pFwdInfo = NULL;

    pFwdInfo = (tSysLogFwdInfo *) FwdUtilFindEntry (i4FsSyslogFwdPriority,
                                                    i4FsSyslogFwdAddressType,
                                                    pFsSyslogFwdServerIP->
                                                    pu1_OctetList);
    if (pFwdInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pFwdInfo->u4Port = i4SetValFsSyslogFwdPort;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSyslogFwdPort;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSyslogFwdPort) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SYSLOG_THREE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %i", i4FsSyslogFwdPriority,
                      i4FsSyslogFwdAddressType, pFsSyslogFwdServerIP,
                      i4SetValFsSyslogFwdPort));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSyslogFwdTransType
 Input       :  The Indices
                FsSyslogFwdPriority
                FsSyslogFwdAddressType
                FsSyslogFwdServerIP

                The Object 
                setValFsSyslogFwdTransType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogFwdTransType (INT4 i4FsSyslogFwdPriority,
                            INT4 i4FsSyslogFwdAddressType,
                            tSNMP_OCTET_STRING_TYPE * pFsSyslogFwdServerIP,
                            INT4 i4SetValFsSyslogFwdTransType)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SYSLOG_ZERO;
    tSysLogFwdInfo     *pFwdInfo = NULL;

    pFwdInfo = (tSysLogFwdInfo *) FwdUtilFindEntry (i4FsSyslogFwdPriority,
                                                    i4FsSyslogFwdAddressType,
                                                    pFsSyslogFwdServerIP->
                                                    pu1_OctetList);
    if (pFwdInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pFwdInfo->u4TransType = i4SetValFsSyslogFwdTransType;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSyslogFwdTransType;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSyslogFwdTransType) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SYSLOG_THREE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %i", i4FsSyslogFwdPriority,
                      i4FsSyslogFwdAddressType, pFsSyslogFwdServerIP,
                      i4SetValFsSyslogFwdTransType));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSyslogFwdRowStatus
 Input       :  The Indices
                FsSyslogFwdPriority
                FsSyslogFwdAddressType
                FsSyslogFwdServerIP

                The Object 
                setValFsSyslogFwdRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogFwdRowStatus (INT4 i4FsSyslogFwdPriority,
                            INT4 i4FsSyslogFwdAddressType,
                            tSNMP_OCTET_STRING_TYPE * pFsSyslogFwdServerIP,
                            INT4 i4SetValFsSyslogFwdRowStatus)
{
    tSysLogFwdInfo     *pFwdInfo = NULL;
    UINT1               au1ServerIP[DNS_MAX_QUERY_LEN];

    MEMSET (au1ServerIP, 0, DNS_MAX_QUERY_LEN);

    MEMCPY (au1ServerIP, pFsSyslogFwdServerIP->pu1_OctetList,
            pFsSyslogFwdServerIP->i4_Length);

    pFwdInfo = (tSysLogFwdInfo *) FwdUtilFindEntry (i4FsSyslogFwdPriority,
                                                    i4FsSyslogFwdAddressType,
                                                    au1ServerIP);

    if ((pFwdInfo != NULL)
        && (i4SetValFsSyslogFwdRowStatus == (INT4) SYS_CREATE_AND_WAIT))
    {
        return SNMP_FAILURE;
    }
    if ((pFwdInfo == NULL)
        && (i4SetValFsSyslogFwdRowStatus != (INT4) SYS_CREATE_AND_WAIT))
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValFsSyslogFwdRowStatus)
    {
        case SYS_CREATE_AND_WAIT:

            if ((i4FsSyslogFwdAddressType != (INT4) IPVX_ADDR_FMLY_IPV4) &&
                (i4FsSyslogFwdAddressType != (INT4) IPVX_ADDR_FMLY_IPV6) &&
                (i4FsSyslogFwdAddressType != (INT4) IPVX_DNS_FAMILY))
            {
                return SNMP_FAILURE;
            }

            if ((pFwdInfo = (tSysLogFwdInfo *)
                 (MemAllocMemBlk (gSysLogGlobalInfo.sysFwdPoolId))) == NULL)
            {
                return SNMP_FAILURE;
            }
            /* Initialize the new fwd entry structure */
            MEMSET (pFwdInfo, SYSLOG_ZERO, sizeof (tSysLogFwdInfo));

            /* initialize the linked list node for new fwd table entry */
            TMO_SLL_Init_Node (&pFwdInfo->NextNode);

            pFwdInfo->u4Priority = (UINT4) i4FsSyslogFwdPriority;

            pFwdInfo->u4AddrType = (UINT4) i4FsSyslogFwdAddressType;

            if (i4FsSyslogFwdAddressType == (INT4) IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (pFwdInfo->ServIpAddr.au1Addr,
                        (pFsSyslogFwdServerIP->pu1_OctetList),
                        pFsSyslogFwdServerIP->i4_Length);
            }
            else if (i4FsSyslogFwdAddressType == IPVX_ADDR_FMLY_IPV6)
            {
                MEMCPY (pFwdInfo->ServIpAddr.au1Addr,
                        (pFsSyslogFwdServerIP->pu1_OctetList),
                        pFsSyslogFwdServerIP->i4_Length);

                IPVX_ADDR_INIT (pFwdInfo->ServIpAddr,
                                (UINT1) i4FsSyslogFwdAddressType,
                                pFsSyslogFwdServerIP->pu1_OctetList);
            }
            else
            {
                pFwdInfo->ServIpAddr.u1Afi = (UINT1) IPVX_DNS_FAMILY;
                STRNCPY (pFwdInfo->au1HostName,
                         (pFsSyslogFwdServerIP->pu1_OctetList),
                         pFsSyslogFwdServerIP->i4_Length);

                pFwdInfo->au1HostName[pFsSyslogFwdServerIP->i4_Length] = '\0';
            }

            pFwdInfo->u4Port = BSD_SYSLOG_PORT;

            pFwdInfo->u4TransType = SYSLOG_UDP;

            /* to be used incase of TCP syslog */
            pFwdInfo->i4SockFd = SYSLOG_NO_SOCK_FD;

            pFwdInfo->i4FwdRowStatus = SYS_NOT_IN_SERVICE;
            SysLogFwdInfoAddNodeToList (pFwdInfo);

            break;
        case SYS_ACTIVE:

            pFwdInfo->u4Priority = (UINT4) i4FsSyslogFwdPriority;

            pFwdInfo->u4AddrType = (UINT4) i4FsSyslogFwdAddressType;

            if (i4FsSyslogFwdAddressType != IPVX_DNS_FAMILY)
            {
                MEMCPY (pFwdInfo->ServIpAddr.au1Addr,
                        pFsSyslogFwdServerIP->pu1_OctetList,
                        pFsSyslogFwdServerIP->i4_Length);

                IPVX_ADDR_INIT (pFwdInfo->ServIpAddr,
                                (UINT1) i4FsSyslogFwdAddressType,
                                pFsSyslogFwdServerIP->pu1_OctetList);
            }
            else
            {
                pFwdInfo->ServIpAddr.u1Afi = (UINT1) IPVX_DNS_FAMILY;
                STRNCPY (pFwdInfo->au1HostName,
                         pFsSyslogFwdServerIP->pu1_OctetList,
                         pFsSyslogFwdServerIP->i4_Length);

                pFwdInfo->au1HostName[pFsSyslogFwdServerIP->i4_Length] = '\0';
            }

            /* Incase of TCP connection, 
             * the socket descriptor has to be taken care */
            if (pFwdInfo->u4TransType == SYSLOG_TCP)
            {
                /* if, there is already a socket descriptor available,
                 * just close it before updating */
                if (pFwdInfo->i4SockFd > SYSLOG_NO_SOCK_FD)
                {
                    close (pFwdInfo->i4SockFd);
                    pFwdInfo->i4SockFd = SYSLOG_NO_SOCK_FD;
                }
            }

            pFwdInfo->i4FwdRowStatus = SYS_ACTIVE;

            break;
        case SYS_NOT_IN_SERVICE:
            if ((pFwdInfo != NULL) && (pFwdInfo->i4FwdRowStatus != SYS_ACTIVE))
            {
                return SNMP_FAILURE;
            }

            pFwdInfo->i4FwdRowStatus = SYS_NOT_IN_SERVICE;
            break;
        case SYS_DESTROY:
            /* Incase of TCP connection, 
             * the socket descriptor has to be taken care */
            if (pFwdInfo->u4TransType == SYSLOG_TCP)
            {
                /* if, there is already a socket descriptor available,
                 * just close it before destroying */
                if (pFwdInfo->i4SockFd > SYSLOG_NO_SOCK_FD)
                {
                    close (pFwdInfo->i4SockFd);
                    pFwdInfo->i4SockFd = SYSLOG_NO_SOCK_FD;
                }
            }

            /* delink the node from the list */
            TMO_SLL_Delete (&(gSysLogGlobalInfo.sysFwdTblList),
                            &pFwdInfo->NextNode);

            /* release the memory for the list */
            if (MemReleaseMemBlock (gSysLogGlobalInfo.sysFwdPoolId,
                                    (UINT1 *) pFwdInfo) == MEM_FAILURE)
            {
                SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                            "\n\t  Syslog Fwd Table Information Cannot be Deleted\n");
                return SNMP_FAILURE;
            }
            break;
        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSyslogFwdPort
 Input       :  The Indices
                FsSyslogFwdPriority
                FsSyslogFwdAddressType
                FsSyslogFwdServerIP

                The Object 
                testValFsSyslogFwdPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogFwdPort (UINT4 *pu4ErrorCode, INT4 i4FsSyslogFwdPriority,
                          INT4 i4FsSyslogFwdAddressType,
                          tSNMP_OCTET_STRING_TYPE * pFsSyslogFwdServerIP,
                          INT4 i4TestValFsSyslogFwdPort)
{
    tSysLogFwdInfo     *pFwdInfo = NULL;

    pFwdInfo = (tSysLogFwdInfo *) FwdUtilFindEntry (i4FsSyslogFwdPriority,
                                                    i4FsSyslogFwdAddressType,
                                                    pFsSyslogFwdServerIP->
                                                    pu1_OctetList);

    if ((((i4TestValFsSyslogFwdPort >= BSD_SYSLOG_PORT_MIN) &&
          (i4TestValFsSyslogFwdPort <= BSD_SYSLOG_PORT_MAX)) &&
         (pFwdInfo != NULL)) == SYSLOG_TRUE)
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsSyslogFwdTransType
 Input       :  The Indices
                FsSyslogFwdPriority
                FsSyslogFwdAddressType
                FsSyslogFwdServerIP

                The Object 
                testValFsSyslogFwdTransType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogFwdTransType (UINT4 *pu4ErrorCode, INT4 i4FsSyslogFwdPriority,
                               INT4 i4FsSyslogFwdAddressType,
                               tSNMP_OCTET_STRING_TYPE * pFsSyslogFwdServerIP,
                               INT4 i4TestValFsSyslogFwdTransType)
{
    tSysLogFwdInfo     *pFwdInfo = NULL;

    pFwdInfo = (tSysLogFwdInfo *) FwdUtilFindEntry (i4FsSyslogFwdPriority,
                                                    i4FsSyslogFwdAddressType,
                                                    pFsSyslogFwdServerIP->
                                                    pu1_OctetList);

    if ((((i4TestValFsSyslogFwdTransType >= SYSLOG_UDP) &&
          (i4TestValFsSyslogFwdTransType <= SYSLOG_BEEP))
         && (pFwdInfo != NULL)) == SYSLOG_TRUE)
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsSyslogFwdRowStatus
 Input       :  The Indices
                FsSyslogFwdPriority
                FsSyslogFwdAddressType
                FsSyslogFwdServerIP

                The Object 
                testValFsSyslogFwdRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogFwdRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsSyslogFwdPriority,
                               INT4 i4FsSyslogFwdAddressType,
                               tSNMP_OCTET_STRING_TYPE * pFsSyslogFwdServerIP,
                               INT4 i4TestValFsSyslogFwdRowStatus)
{
    tSysLogFwdInfo     *pFwdInfo = NULL;
    tIp6Addr            Ip6Addr;

    if (!((i4FsSyslogFwdPriority >= SYSLOG_PRIORITY_MIN) &&
          (i4FsSyslogFwdPriority <= SYSLOG_PRIORITY_MAX)) &&
        ((i4FsSyslogFwdAddressType == IPVX_ADDR_FMLY_IPV4) ||
         (i4FsSyslogFwdAddressType == IPVX_ADDR_FMLY_IPV6) ||
         (i4FsSyslogFwdAddressType == IPVX_DNS_FAMILY)))
    {
        return SNMP_FAILURE;

    }

    if ((i4FsSyslogFwdAddressType == IPVX_ADDR_FMLY_IPV6) &&
        (pFsSyslogFwdServerIP->pu1_OctetList != NULL))
    {
        MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
        MEMCPY (&Ip6Addr, pFsSyslogFwdServerIP->pu1_OctetList,
                IPVX_MAX_INET_ADDR_LEN);
        if (IS_ADDR_LLOCAL (Ip6Addr))
        {
            return SNMP_FAILURE;
        }
    }

    if ((pFsSyslogFwdServerIP->pu1_OctetList != NULL) &&
        (TMO_SLL_Count (&(gSysLogGlobalInfo.sysFwdTblList)) <
         SYSLOG_MAX_SESSION))
    {
        pFwdInfo = (tSysLogFwdInfo *) FwdUtilFindEntry (i4FsSyslogFwdPriority,
                                                        i4FsSyslogFwdAddressType,
                                                        pFsSyslogFwdServerIP->
                                                        pu1_OctetList);
    }
    else
    {
        CLI_SET_ERR (CLI_SYSLG_CREATE_ERR);
        return SNMP_FAILURE;
    }
    switch (i4TestValFsSyslogFwdRowStatus)
    {
        case SYS_ACTIVE:
        case SYS_NOT_IN_SERVICE:
        case SYS_NOT_READY:
        case SYS_DESTROY:

            if (pFwdInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
                /*Cannot obtain node for deletion */
            }
            break;
        case SYS_CREATE_AND_GO:
        case SYS_CREATE_AND_WAIT:
            if (TMO_SLL_Count (&(gSysLogGlobalInfo.sysFwdTblList)) >=
                MAX_SYSLOG_LOGGING_ENTRY)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_SYSLG_MAX_LOGGING_SERVER_REACHED);
                return SNMP_FAILURE;
            }
            /*since domain is one of the index,max length of 255 causes problem while doing MSR
             * to address that restricting the host name size to 63*/
            if (pFsSyslogFwdServerIP->i4_Length >= SYSLOG_MAX_DNS_DOMAIN_NAME)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_SYSLG_MAX_DOMAIN_LEN);
                return SNMP_FAILURE;
            }

            if (pFwdInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_SYSLG_INVALID_IP_SERVER);
                return SNMP_FAILURE;
                /*Node already exists */
            }
            break;

        default:

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_SYSLG_INVALID_IP_SERVER);
            return SNMP_FAILURE;
            /*Invalid Row Status */
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSyslogFwdTable
 Input       :  The Indices
                FsSyslogFwdPriority
                FsSyslogFwdAddressType
                FsSyslogFwdServerIP
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogFwdTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSyslogSmtpSrvAddr
 Input       :  The Indices

                The Object 
                retValFsSyslogSmtpSrvAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogSmtpSrvAddr (UINT4 *pu4RetValFsSyslogSmtpSrvAddr)
{
    *pu4RetValFsSyslogSmtpSrvAddr = gsSmtpAlertParams.u4MailServerIp;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSyslogSmtpRcvrMailId
 Input       :  The Indices

                The Object 
                retValFsSyslogSmtpRcvrMailId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogSmtpRcvrMailId (tSNMP_OCTET_STRING_TYPE *
                              pRetValFsSyslogSmtpRcvrMailId)
{
    pRetValFsSyslogSmtpRcvrMailId->i4_Length =
        STRLEN (gsSmtpAlertParams.au1RcvDomainName);

    MEMCPY (pRetValFsSyslogSmtpRcvrMailId->pu1_OctetList,
            gsSmtpAlertParams.au1RcvDomainName,
            pRetValFsSyslogSmtpRcvrMailId->i4_Length);
    pRetValFsSyslogSmtpRcvrMailId->
        pu1_OctetList[pRetValFsSyslogSmtpRcvrMailId->i4_Length] = '\0';
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSyslogSmtpSenderMailId
 Input       :  The Indices

                The Object 
                retValFsSyslogSmtpSenderMailId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogSmtpSenderMailId (tSNMP_OCTET_STRING_TYPE *
                                pRetValFsSyslogSmtpSenderMailId)
{
    pRetValFsSyslogSmtpSenderMailId->i4_Length =
        STRLEN (gsSmtpAlertParams.au1SysDomainName);

    MEMCPY (pRetValFsSyslogSmtpSenderMailId->pu1_OctetList,
            gsSmtpAlertParams.au1SysDomainName,
            pRetValFsSyslogSmtpSenderMailId->i4_Length);
    pRetValFsSyslogSmtpSenderMailId->pu1_OctetList
        [pRetValFsSyslogSmtpSenderMailId->i4_Length] = '\0';
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSyslogSmtpSrvAddr
 Input       :  The Indices

                The Object 
                setValFsSyslogSmtpSrvAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogSmtpSrvAddr (UINT4 u4SetValFsSyslogSmtpSrvAddr)
{
    gsSmtpAlertParams.u4MailServerIp = u4SetValFsSyslogSmtpSrvAddr;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSyslogSmtpRcvrMailId
 Input       :  The Indices

                The Object 
                setValFsSyslogSmtpRcvrMailId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogSmtpRcvrMailId (tSNMP_OCTET_STRING_TYPE *
                              pSetValFsSyslogSmtpRcvrMailId)
{
    MEMCPY (gsSmtpAlertParams.au1RcvDomainName,
            pSetValFsSyslogSmtpRcvrMailId->pu1_OctetList,
            pSetValFsSyslogSmtpRcvrMailId->i4_Length);
    gsSmtpAlertParams.
        au1RcvDomainName[pSetValFsSyslogSmtpRcvrMailId->i4_Length] = '\0';
    gsSmtpAlertParams.u4ServerDomainName = SMTP_SET;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsSyslogSmtpSenderMailId
 Input       :  The Indices

                The Object 
                setValFsSyslogSmtpSenderMailId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogSmtpSenderMailId (tSNMP_OCTET_STRING_TYPE *
                                pSetValFsSyslogSmtpSenderMailId)
{
    UINT4               u4SeqNum = SYSLOG_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (pSetValFsSyslogSmtpSenderMailId->i4_Length != 0)
    {
        MEMCPY (gsSmtpAlertParams.au1SysDomainName,
                pSetValFsSyslogSmtpSenderMailId->pu1_OctetList,
                pSetValFsSyslogSmtpSenderMailId->i4_Length);
        gsSmtpAlertParams.
            au1SysDomainName[pSetValFsSyslogSmtpSenderMailId->i4_Length] = '\0';
        gsSmtpAlertParams.u4MyDomainName = SMTP_SET;
    }
    else
    {
        MEMSET (gsSmtpAlertParams.au1SysDomainName, 0, SMTP_DOMAIN_NAME_LEN);
        gsSmtpAlertParams.u4MyDomainName = SMTP_NOT_SET;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSyslogSmtpSenderMailId;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsSyslogSmtpSenderMailId) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SYSLOG_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s", pSetValFsSyslogSmtpSenderMailId));

    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSyslogSmtpSrvAddr
 Input       :  The Indices

                The Object 
                testValFsSyslogSmtpSrvAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogSmtpSrvAddr (UINT4 *pu4ErrorCode,
                              UINT4 u4TestValFsSyslogSmtpSrvAddr)
{
    if (!((SYSLOG_IS_ADDR_CLASS_A (u4TestValFsSyslogSmtpSrvAddr)) ||
          (SYSLOG_IS_ADDR_CLASS_B (u4TestValFsSyslogSmtpSrvAddr)) ||
          (SYSLOG_IS_ADDR_CLASS_C (u4TestValFsSyslogSmtpSrvAddr)) ||
          (u4TestValFsSyslogSmtpSrvAddr == 0)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INVALID_IP);
        return SNMP_FAILURE;
    }
#ifdef IP_WANTED
    if (NetIpv4IfIsOurAddress (u4TestValFsSyslogSmtpSrvAddr) == NETIPV4_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_INV_DIRECT_IP);
        return SNMP_FAILURE;
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSyslogSmtpRcvrMailId
 Input       :  The Indices

                The Object 
                testValFsSyslogSmtpRcvrMailId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogSmtpRcvrMailId (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValFsSyslogSmtpRcvrMailId)
{
    if ((pTestValFsSyslogSmtpRcvrMailId->i4_Length <= 0) ||
        (pTestValFsSyslogSmtpRcvrMailId->i4_Length > SMTP_DOMAIN_NAME_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_SYSLG_RCVR_MAIL_ERR);
        return SNMP_FAILURE;
    }

    if (SNMPCheckForNVTChars (pTestValFsSyslogSmtpRcvrMailId->pu1_OctetList,
                              pTestValFsSyslogSmtpRcvrMailId->i4_Length)
        == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_RCVR_MAIL_ERR);
        return SNMP_FAILURE;
    }

    if (SysLogValidateMailId (pTestValFsSyslogSmtpRcvrMailId->pu1_OctetList,
                              pTestValFsSyslogSmtpRcvrMailId->i4_Length)
        == SYSLOG_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_RCVR_MAIL_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSyslogSmtpSenderMailId
 Input       :  The Indices

                The Object 
                testValFsSyslogSmtpSenderMailId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogSmtpSenderMailId (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsSyslogSmtpSenderMailId)
{
    if ((pTestValFsSyslogSmtpSenderMailId->i4_Length < 0) ||
        (pTestValFsSyslogSmtpSenderMailId->i4_Length > SMTP_DOMAIN_NAME_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_SYSLG_SNDR_MAIL_ERR);
        return SNMP_FAILURE;
    }

    if (SNMPCheckForNVTChars (pTestValFsSyslogSmtpSenderMailId->pu1_OctetList,
                              pTestValFsSyslogSmtpSenderMailId->i4_Length)
        == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_SNDR_MAIL_ERR);
        return SNMP_FAILURE;
    }

    if (SysLogValidateMailId (pTestValFsSyslogSmtpSenderMailId->pu1_OctetList,
                              pTestValFsSyslogSmtpSenderMailId->i4_Length)
        == SYSLOG_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_SNDR_MAIL_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSyslogSmtpSrvAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogSmtpSrvAddr (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSyslogSmtpRcvrMailId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogSmtpRcvrMailId (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSyslogSmtpSenderMailId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogSmtpSenderMailId (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsSyslogMailTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsSyslogMailTable
 Input       :  The Indices
                FsSyslogMailPriority
                FsSyslogMailServAddType
                FsSyslogMailServAdd
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsSyslogMailTable (INT4 i4FsSyslogMailPriority,
                                           INT4 i4FsSyslogMailServAddType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsSyslogMailServAdd)
{
    if (((i4FsSyslogMailPriority >= SYSLOG_PRIORITY_MIN) &&
         (i4FsSyslogMailPriority <= SYSLOG_PRIORITY_MAX)) &&
        ((i4FsSyslogMailServAddType == IPVX_ADDR_FMLY_IPV4) ||
         (i4FsSyslogMailServAddType == IPVX_ADDR_FMLY_IPV6) ||
         (i4FsSyslogMailServAddType == IPVX_DNS_FAMILY)) &&
        (pFsSyslogMailServAdd != NULL))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsSyslogMailTable
 Input       :  The Indices
                FsSyslogMailPriority
                FsSyslogMailServAddType
                FsSyslogMailServAdd
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsSyslogMailTable (INT4 *pi4FsSyslogMailPriority,
                                   INT4 *pi4FsSyslogMailServAddType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsSyslogMailServAdd)
{
    tSysLogMailInfo    *pMailInfo = NULL;

    pMailInfo =
        (tSysLogMailInfo *) TMO_SLL_First (&(gSysLogGlobalInfo.sysMailTblList));
    if (pMailInfo == NULL)
    {
        return SNMP_FAILURE;
        /* *pi4FsSyslogPriority = -1 */ ;
    }
    else
    {
        *pi4FsSyslogMailPriority = (INT4) pMailInfo->u4Priority;
        *pi4FsSyslogMailServAddType = (INT4) pMailInfo->u4AddrType;
        if (*pi4FsSyslogMailServAddType == IPVX_ADDR_FMLY_IPV4)
        {
            pFsSyslogMailServAdd->i4_Length = IPVX_IPV4_ADDR_LEN;
        }
        else if (*pi4FsSyslogMailServAddType == IPVX_ADDR_FMLY_IPV6)
        {
            pFsSyslogMailServAdd->i4_Length = IPVX_IPV6_ADDR_LEN;
        }
        else if (*pi4FsSyslogMailServAddType == IPVX_DNS_FAMILY)
        {
            pFsSyslogMailServAdd->i4_Length =
                (INT4) STRLEN (pMailInfo->au1HostName);
        }

        if (*pi4FsSyslogMailServAddType != IPVX_DNS_FAMILY)
        {
            MEMCPY (pFsSyslogMailServAdd->pu1_OctetList,
                    pMailInfo->MailServIpAddr.au1Addr,
                    pFsSyslogMailServAdd->i4_Length);
        }
        else
        {
            STRNCPY (pFsSyslogMailServAdd->pu1_OctetList,
                     pMailInfo->au1HostName, pFsSyslogMailServAdd->i4_Length);

            pFsSyslogMailServAdd->pu1_OctetList
                [pFsSyslogMailServAdd->i4_Length] = '\0';

        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsSyslogMailTable
 Input       :  The Indices
                FsSyslogMailPriority
                nextFsSyslogMailPriority
                FsSyslogMailServAddType
                nextFsSyslogMailServAddType
                FsSyslogMailServAdd
                nextFsSyslogMailServAdd
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsSyslogMailTable (INT4 i4FsSyslogMailPriority,
                                  INT4 *pi4NextFsSyslogMailPriority,
                                  INT4 i4FsSyslogMailServAddType,
                                  INT4 *pi4NextFsSyslogMailServAddType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsSyslogMailServAdd,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pNextFsSyslogMailServAdd)
{
    tSysLogMailInfo    *pMailInfo = NULL;
    tSysLogMailInfo    *pMailInfoNext = NULL;
    UINT4               u4Addrlen = SYSLOG_ZERO;

    pMailInfo =
        (tSysLogMailInfo *) MailUtilFindEntry (i4FsSyslogMailPriority,
                                               i4FsSyslogMailServAddType,
                                               pFsSyslogMailServAdd->
                                               pu1_OctetList);
    if (pMailInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pMailInfoNext =
        (tSysLogMailInfo *) TMO_SLL_Next ((&gSysLogGlobalInfo.sysMailTblList),
                                          &pMailInfo->NextNode);

    if (pMailInfoNext == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pNextFsSyslogMailServAdd->pu1_OctetList, SYSLOG_ZERO,
            IPVX_IPV6_ADDR_LEN);
    *pi4NextFsSyslogMailPriority = (INT4) pMailInfoNext->u4Priority;
    *pi4NextFsSyslogMailServAddType = (INT4) pMailInfoNext->u4AddrType;
    if (*pi4NextFsSyslogMailServAddType == (INT4) IPVX_ADDR_FMLY_IPV4)
    {
        u4Addrlen = IPVX_IPV4_ADDR_LEN;
    }
    else if (*pi4NextFsSyslogMailServAddType == (INT4) IPVX_ADDR_FMLY_IPV6)
    {
        u4Addrlen = IPVX_IPV6_ADDR_LEN;
    }
    else if (*pi4NextFsSyslogMailServAddType == (INT4) IPVX_DNS_FAMILY)
    {
        u4Addrlen = (UINT4) STRLEN (pMailInfoNext->au1HostName);

    }
    if (*pi4NextFsSyslogMailServAddType != IPVX_DNS_FAMILY)
    {
        MEMCPY (pNextFsSyslogMailServAdd->pu1_OctetList,
                pMailInfoNext->MailServIpAddr.au1Addr, u4Addrlen);
    }
    else
    {
        STRNCPY (pNextFsSyslogMailServAdd->pu1_OctetList,
                 pMailInfoNext->au1HostName, u4Addrlen);

        pNextFsSyslogMailServAdd->pu1_OctetList[u4Addrlen] = '\0';
    }
    pNextFsSyslogMailServAdd->i4_Length = (INT4) u4Addrlen;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSyslogRxMailId
 Input       :  The Indices
                FsSyslogMailPriority
                FsSyslogMailServAddType
                FsSyslogMailServAdd

                The Object 
                retValFsSyslogRxMailId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogRxMailId (INT4 i4FsSyslogMailPriority,
                        INT4 i4FsSyslogMailServAddType,
                        tSNMP_OCTET_STRING_TYPE * pFsSyslogMailServAdd,
                        tSNMP_OCTET_STRING_TYPE * pRetValFsSyslogRxMailId)
{
    tSysLogMailInfo    *pMailInfo = NULL;

    pMailInfo = (tSysLogMailInfo *) MailUtilFindEntry (i4FsSyslogMailPriority,
                                                       i4FsSyslogMailServAddType,
                                                       pFsSyslogMailServAdd->
                                                       pu1_OctetList);
    if (pMailInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pRetValFsSyslogRxMailId->pu1_OctetList, pMailInfo->au1RexMailId,
            STRLEN (pMailInfo->au1RexMailId));
    pRetValFsSyslogRxMailId->i4_Length = STRLEN (pMailInfo->au1RexMailId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSyslogMailRowStatus
 Input       :  The Indices
                FsSyslogMailPriority
                FsSyslogMailServAddType
                FsSyslogMailServAdd

                The Object 
                retValFsSyslogMailRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogMailRowStatus (INT4 i4FsSyslogMailPriority,
                             INT4 i4FsSyslogMailServAddType,
                             tSNMP_OCTET_STRING_TYPE * pFsSyslogMailServAdd,
                             INT4 *pi4RetValFsSyslogMailRowStatus)
{
    tSysLogMailInfo    *pMailInfo = NULL;

    pMailInfo = (tSysLogMailInfo *) MailUtilFindEntry (i4FsSyslogMailPriority,
                                                       i4FsSyslogMailServAddType,
                                                       pFsSyslogMailServAdd->
                                                       pu1_OctetList);

    if (pMailInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsSyslogMailRowStatus = pMailInfo->i4MailRowStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSyslogMailServUserName
 Input       :  The Indices
                FsSyslogMailPriority
                FsSyslogMailServAddType
                FsSyslogMailServAdd

                The Object 
                retValFsSyslogMailServUserName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogMailServUserName (INT4 i4FsSyslogMailPriority,
                                INT4 i4FsSyslogMailServAddType,
                                tSNMP_OCTET_STRING_TYPE * pFsSyslogMailServAdd,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsSyslogMailServUserName)
{
    tSysLogMailInfo    *pMailInfo = NULL;

    pMailInfo = (tSysLogMailInfo *) MailUtilFindEntry (i4FsSyslogMailPriority,
                                                       i4FsSyslogMailServAddType,
                                                       pFsSyslogMailServAdd->
                                                       pu1_OctetList);
    if (pMailInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pRetValFsSyslogMailServUserName->i4_Length =
        STRLEN (pMailInfo->au1SmtpUserName);
    MEMCPY (pRetValFsSyslogMailServUserName->pu1_OctetList,
            pMailInfo->au1SmtpUserName,
            pRetValFsSyslogMailServUserName->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSyslogMailServPassword
 Input       :  The Indices
                FsSyslogMailPriority
                FsSyslogMailServAddType
                FsSyslogMailServAdd

                The Object 
                retValFsSyslogMailServPassword
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogMailServPassword (INT4 i4FsSyslogMailPriority,
                                INT4 i4FsSyslogMailServAddType,
                                tSNMP_OCTET_STRING_TYPE * pFsSyslogMailServAdd,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsSyslogMailServPassword)
{
    tSysLogMailInfo    *pMailInfo = NULL;

    pMailInfo = (tSysLogMailInfo *) MailUtilFindEntry (i4FsSyslogMailPriority,
                                                       i4FsSyslogMailServAddType,
                                                       pFsSyslogMailServAdd->
                                                       pu1_OctetList);
    if (pMailInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    /* Password is not displayed */
    pRetValFsSyslogMailServPassword->i4_Length = 0;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSyslogRxMailId
 Input       :  The Indices
                FsSyslogMailPriority
                FsSyslogMailServAddType
                FsSyslogMailServAdd

                The Object 
                setValFsSyslogRxMailId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogRxMailId (INT4 i4FsSyslogMailPriority,
                        INT4 i4FsSyslogMailServAddType,
                        tSNMP_OCTET_STRING_TYPE * pFsSyslogMailServAdd,
                        tSNMP_OCTET_STRING_TYPE * pSetValFsSyslogRxMailId)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SYSLOG_ZERO;
    tSysLogMailInfo    *pMailInfo = NULL;

    pMailInfo = (tSysLogMailInfo *) MailUtilFindEntry (i4FsSyslogMailPriority,
                                                       i4FsSyslogMailServAddType,
                                                       pFsSyslogMailServAdd->
                                                       pu1_OctetList);
    if (pMailInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pMailInfo->au1RexMailId, SYSLOG_ZERO, SYSLOG_RXMAILID_LENGTH);

    MEMCPY (pMailInfo->au1RexMailId,
            pSetValFsSyslogRxMailId->pu1_OctetList,
            pSetValFsSyslogRxMailId->i4_Length);

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSyslogRxMailId;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSyslogRxMailId) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SYSLOG_THREE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %s ", i4FsSyslogMailPriority,
                      i4FsSyslogMailServAddType, pFsSyslogMailServAdd,
                      pSetValFsSyslogRxMailId));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSyslogMailRowStatus
 Input       :  The Indices
                FsSyslogMailPriority
                FsSyslogMailServAddType
                FsSyslogMailServAdd

                The Object 
                setValFsSyslogMailRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogMailRowStatus (INT4 i4FsSyslogMailPriority,
                             INT4 i4FsSyslogMailServAddType,
                             tSNMP_OCTET_STRING_TYPE * pFsSyslogMailServAdd,
                             INT4 i4SetValFsSyslogMailRowStatus)
{
    tSysLogMailInfo    *pMailInfo = NULL;

    pMailInfo = (tSysLogMailInfo *) MailUtilFindEntry (i4FsSyslogMailPriority,
                                                       i4FsSyslogMailServAddType,
                                                       pFsSyslogMailServAdd->
                                                       pu1_OctetList);

    if ((pMailInfo != NULL)
        && (i4SetValFsSyslogMailRowStatus == SYS_CREATE_AND_WAIT))
    {
        return SNMP_FAILURE;
    }
    if ((pMailInfo == NULL)
        && (i4SetValFsSyslogMailRowStatus != SYS_CREATE_AND_WAIT))
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValFsSyslogMailRowStatus)
    {
        case SYS_CREATE_AND_WAIT:

            if ((pMailInfo = (tSysLogMailInfo *)
                 (MemAllocMemBlk (gSysLogGlobalInfo.sysMailPoolId))) == NULL)
            {
                return SNMP_FAILURE;
            }
            /* Initialize the new fwd entry structure */
            MEMSET (pMailInfo, SYSLOG_ZERO, sizeof (tSysLogMailInfo));

            /* initialize the linked list node for new fwd table entry */
            TMO_SLL_Init_Node (&pMailInfo->NextNode);

            pMailInfo->u4Priority = (UINT4) i4FsSyslogMailPriority;

            pMailInfo->u4AddrType = (UINT4) i4FsSyslogMailServAddType;

            if (i4FsSyslogMailServAddType == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (pMailInfo->MailServIpAddr.au1Addr,
                        (pFsSyslogMailServAdd->pu1_OctetList),
                        pFsSyslogMailServAdd->i4_Length);
            }
            else if (i4FsSyslogMailServAddType == IPVX_ADDR_FMLY_IPV6)
            {
                MEMCPY (pMailInfo->MailServIpAddr.au1Addr,
                        pFsSyslogMailServAdd->pu1_OctetList,
                        pFsSyslogMailServAdd->i4_Length);

            }
            else if (i4FsSyslogMailServAddType == IPVX_DNS_FAMILY)
            {
                STRNCPY (pMailInfo->au1HostName,
                         pFsSyslogMailServAdd->pu1_OctetList,
                         pFsSyslogMailServAdd->i4_Length);

                pMailInfo->au1HostName[pFsSyslogMailServAdd->i4_Length] = '\0';
            }

            if (i4FsSyslogMailServAddType != IPVX_DNS_FAMILY)
            {
                IPVX_ADDR_INIT (pMailInfo->MailServIpAddr,
                                (UINT1) i4FsSyslogMailServAddType,
                                pFsSyslogMailServAdd->pu1_OctetList);
            }

            pMailInfo->i4MailRowStatus = SYS_NOT_IN_SERVICE;

            SysLogMailInfoAddNodeToList (pMailInfo);

            break;
        case SYS_ACTIVE:

            pMailInfo->u4Priority = (UINT4) i4FsSyslogMailPriority;

            pMailInfo->u4AddrType = (UINT4) i4FsSyslogMailServAddType;

            if (i4FsSyslogMailServAddType != (INT4) IPVX_DNS_FAMILY)
            {
                MEMCPY (pMailInfo->MailServIpAddr.au1Addr,
                        pFsSyslogMailServAdd->pu1_OctetList,
                        pFsSyslogMailServAdd->i4_Length);

                IPVX_ADDR_INIT (pMailInfo->MailServIpAddr,
                                (UINT1) i4FsSyslogMailServAddType,
                                pFsSyslogMailServAdd->pu1_OctetList);
            }
            else
            {
                STRNCPY (pMailInfo->au1HostName,
                         pFsSyslogMailServAdd->pu1_OctetList,
                         pFsSyslogMailServAdd->i4_Length);

                pMailInfo->au1HostName[pFsSyslogMailServAdd->i4_Length] = '\0';

            }

            pMailInfo->i4MailRowStatus = (INT4) SYS_ACTIVE;

            break;
        case SYS_NOT_IN_SERVICE:
            if ((pMailInfo != NULL)
                && (pMailInfo->i4MailRowStatus != SYS_ACTIVE))
            {
                return SNMP_FAILURE;
            }
            pMailInfo->i4MailRowStatus = SYS_NOT_IN_SERVICE;
            break;
        case SYS_DESTROY:
            TMO_SLL_Delete (&(gSysLogGlobalInfo.sysMailTblList),
                            &pMailInfo->NextNode);

            /* release the memory for the list */
            if (MemReleaseMemBlock (gSysLogGlobalInfo.sysMailPoolId,
                                    (UINT1 *) pMailInfo) == MEM_FAILURE)
            {
                SYSLOG_DBG (SYSLOG_ALL_FAILURE_TRC,
                            "\n\t  Syslog Mail Table Information Cannot be deleted");
                return SYSLOG_FAILURE;
            }

            break;
        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSyslogMailServUserName
 Input       :  The Indices
                FsSyslogMailPriority
                FsSyslogMailServAddType
                FsSyslogMailServAdd

                The Object 
                setValFsSyslogMailServUserName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogMailServUserName (INT4 i4FsSyslogMailPriority,
                                INT4 i4FsSyslogMailServAddType,
                                tSNMP_OCTET_STRING_TYPE * pFsSyslogMailServAdd,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsSyslogMailServUserName)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SYSLOG_ZERO;
    tSysLogMailInfo    *pMailInfo = NULL;

    pMailInfo = (tSysLogMailInfo *) MailUtilFindEntry (i4FsSyslogMailPriority,
                                                       i4FsSyslogMailServAddType,
                                                       pFsSyslogMailServAdd->
                                                       pu1_OctetList);
    if (pMailInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pMailInfo->au1SmtpUserName, SYSLOG_ZERO,
            (SMTP_MAX_STRING + SYSLOG_FOUR));

    MEMCPY (pMailInfo->au1SmtpUserName,
            pSetValFsSyslogMailServUserName->pu1_OctetList,
            MEM_MAX_BYTES (pSetValFsSyslogMailServUserName->i4_Length,
                           SMTP_MAX_STRING));

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSyslogMailServUserName;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsSyslogMailServUserName) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SYSLOG_THREE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %s ", i4FsSyslogMailPriority,
                      i4FsSyslogMailServAddType, pFsSyslogMailServAdd,
                      pSetValFsSyslogMailServUserName));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSyslogMailServPassword
 Input       :  The Indices
                FsSyslogMailPriority
                FsSyslogMailServAddType
                FsSyslogMailServAdd

                The Object 
                setValFsSyslogMailServPassword
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogMailServPassword (INT4 i4FsSyslogMailPriority,
                                INT4 i4FsSyslogMailServAddType,
                                tSNMP_OCTET_STRING_TYPE * pFsSyslogMailServAdd,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsSyslogMailServPassword)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SYSLOG_ZERO;
    tSysLogMailInfo    *pMailInfo = NULL;

    pMailInfo = (tSysLogMailInfo *) MailUtilFindEntry (i4FsSyslogMailPriority,
                                                       i4FsSyslogMailServAddType,
                                                       pFsSyslogMailServAdd->
                                                       pu1_OctetList);
    if (pMailInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pMailInfo->au1SmtpPasswd, SYSLOG_ZERO,
            (SMTP_MAX_STRING + SYSLOG_FOUR));

    MEMCPY (pMailInfo->au1SmtpPasswd,
            pSetValFsSyslogMailServPassword->pu1_OctetList,
            MEM_MAX_BYTES (pSetValFsSyslogMailServPassword->i4_Length,
                           SMTP_MAX_STRING));

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSyslogMailServPassword;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsSyslogMailServPassword) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SYSLOG_THREE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %s ", i4FsSyslogMailPriority,
                      i4FsSyslogMailServAddType, pFsSyslogMailServAdd,
                      pSetValFsSyslogMailServPassword));
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSyslogRxMailId
 Input       :  The Indices
                FsSyslogMailPriority
                FsSyslogMailServAddType
                FsSyslogMailServAdd

                The Object 
                testValFsSyslogRxMailId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogRxMailId (UINT4 *pu4ErrorCode, INT4 i4FsSyslogMailPriority,
                           INT4 i4FsSyslogMailServAddType,
                           tSNMP_OCTET_STRING_TYPE * pFsSyslogMailServAdd,
                           tSNMP_OCTET_STRING_TYPE * pTestValFsSyslogRxMailId)
{
    tSysLogMailInfo    *pMailInfo = NULL;

    pMailInfo = (tSysLogMailInfo *) MailUtilFindEntry (i4FsSyslogMailPriority,
                                                       i4FsSyslogMailServAddType,
                                                       pFsSyslogMailServAdd->
                                                       pu1_OctetList);

    if (pMailInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pTestValFsSyslogRxMailId->i4_Length <= SYSLOG_ZERO) ||
        (pTestValFsSyslogRxMailId->i4_Length > SMTP_DOMAIN_NAME_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_SYSLG_RCVR_MAIL_ERR);
        return SNMP_FAILURE;
    }

    if (SNMPCheckForNVTChars (pTestValFsSyslogRxMailId->pu1_OctetList,
                              pTestValFsSyslogRxMailId->i4_Length)
        == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_RCVR_MAIL_ERR);
        return SNMP_FAILURE;
    }
    if (SysLogValidateMailId (pTestValFsSyslogRxMailId->pu1_OctetList,
                              pTestValFsSyslogRxMailId->i4_Length)
        == SYSLOG_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SYSLG_RCVR_MAIL_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsSyslogMailRowStatus
 Input       :  The Indices
                FsSyslogMailPriority
                FsSyslogMailServAddType
                FsSyslogMailServAdd

                The Object 
                testValFsSyslogMailRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogMailRowStatus (UINT4 *pu4ErrorCode,
                                INT4 i4FsSyslogMailPriority,
                                INT4 i4FsSyslogMailServAddType,
                                tSNMP_OCTET_STRING_TYPE * pFsSyslogMailServAdd,
                                INT4 i4TestValFsSyslogMailRowStatus)
{
    tSysLogMailInfo    *pMailInfo = NULL;

    if (!((i4FsSyslogMailPriority >= SYSLOG_PRIORITY_MIN) &&
          (i4FsSyslogMailPriority <= SYSLOG_PRIORITY_MAX)) &&
        ((i4FsSyslogMailServAddType == IPVX_ADDR_FMLY_IPV4) ||
         (i4FsSyslogMailServAddType == IPVX_ADDR_FMLY_IPV6) ||
         (i4FsSyslogMailServAddType == IPVX_DNS_FAMILY)) &&
        (pFsSyslogMailServAdd->pu1_OctetList != NULL))
    {
        return SNMP_FAILURE;

    }

    if (pFsSyslogMailServAdd->pu1_OctetList != NULL)
    {

        pMailInfo =
            (tSysLogMailInfo *) MailUtilFindEntry (i4FsSyslogMailPriority,
                                                   i4FsSyslogMailServAddType,
                                                   pFsSyslogMailServAdd->
                                                   pu1_OctetList);

    }
    switch (i4TestValFsSyslogMailRowStatus)
    {

        case SYS_ACTIVE:
        case SYS_NOT_IN_SERVICE:
        case SYS_NOT_READY:
        case SYS_DESTROY:

            if (pMailInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
                /*Cannot obtain node for deletion */
            }
            break;
        case SYS_CREATE_AND_GO:
        case SYS_CREATE_AND_WAIT:
            if (TMO_SLL_Count (&(gSysLogGlobalInfo.sysMailTblList)) >=
                MAX_SYSLOG_MAIL_ENTRY)
            {
                CLI_SET_ERR (CLI_SYSLG_MAX_MAIL_SERVER_REACHED);
                return SNMP_FAILURE;
            }
            if (pMailInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
                /*Node already exists */
            }
            break;

        default:

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
            /*Invalid Row Status */
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsSyslogMailServUserName
 Input       :  The Indices
                FsSyslogMailPriority
                FsSyslogMailServAddType
                FsSyslogMailServAdd

                The Object 
                testValFsSyslogMailServUserName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogMailServUserName (UINT4 *pu4ErrorCode,
                                   INT4 i4FsSyslogMailPriority,
                                   INT4 i4FsSyslogMailServAddType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsSyslogMailServAdd,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsSyslogMailServUserName)
{
    tSysLogMailInfo    *pMailInfo = NULL;

    pMailInfo = (tSysLogMailInfo *) MailUtilFindEntry (i4FsSyslogMailPriority,
                                                       i4FsSyslogMailServAddType,
                                                       pFsSyslogMailServAdd->
                                                       pu1_OctetList);

    if (pMailInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pTestValFsSyslogMailServUserName->i4_Length < SYSLOG_ZERO) ||
        (pTestValFsSyslogMailServUserName->i4_Length > SMTP_MAX_STRING))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (SNMPCheckForNVTChars (pTestValFsSyslogMailServUserName->pu1_OctetList,
                              pTestValFsSyslogMailServUserName->i4_Length)
        == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSyslogMailServPassword
 Input       :  The Indices
                FsSyslogMailPriority
                FsSyslogMailServAddType
                FsSyslogMailServAdd

                The Object 
                testValFsSyslogMailServPassword
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogMailServPassword (UINT4 *pu4ErrorCode,
                                   INT4 i4FsSyslogMailPriority,
                                   INT4 i4FsSyslogMailServAddType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsSyslogMailServAdd,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsSyslogMailServPassword)
{
    tSysLogMailInfo    *pMailInfo = NULL;

    pMailInfo = (tSysLogMailInfo *) MailUtilFindEntry (i4FsSyslogMailPriority,
                                                       i4FsSyslogMailServAddType,
                                                       pFsSyslogMailServAdd->
                                                       pu1_OctetList);

    if (pMailInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pTestValFsSyslogMailServPassword->i4_Length < SYSLOG_ZERO) ||
        (pTestValFsSyslogMailServPassword->i4_Length > SMTP_MAX_STRING))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSyslogMailTable
 Input       :  The Indices
                FsSyslogMailPriority
                FsSyslogMailServAddType
                FsSyslogMailServAdd
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogMailTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSyslogSmtpAuthMethod
 Input       :  The Indices

                The Object 
                retValFsSyslogSmtpAuthMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSyslogSmtpAuthMethod (INT4 *pi4RetValFsSyslogSmtpAuthMethod)
{
    *pi4RetValFsSyslogSmtpAuthMethod = gu1SmtpAuthentication;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSyslogSmtpAuthMethod
 Input       :  The Indices

                The Object 
                setValFsSyslogSmtpAuthMethod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSyslogSmtpAuthMethod (INT4 i4SetValFsSyslogSmtpAuthMethod)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = SYSLOG_ZERO;
    gu1SmtpAuthentication = (UINT1) i4SetValFsSyslogSmtpAuthMethod;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsSyslogSmtpAuthMethod;
    SnmpNotifyInfo.u4OidLen = sizeof (FsSyslogSmtpAuthMethod) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = SYSLOG_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsSyslogSmtpAuthMethod));

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSyslogSmtpAuthMethod
 Input       :  The Indices

                The Object 
                testValFsSyslogSmtpAuthMethod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSyslogSmtpAuthMethod (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsSyslogSmtpAuthMethod)
{
    if ((i4TestValFsSyslogSmtpAuthMethod >= SMTP_NO_AUTHENTICATE)
        && (i4TestValFsSyslogSmtpAuthMethod <= SMTP_AUTH_DIGEST_MD5))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSyslogSmtpAuthMethod
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSyslogSmtpAuthMethod (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
