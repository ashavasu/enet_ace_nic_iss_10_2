/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: dnsglob.h,v 1.2 2017/12/19 10:00:02 siva Exp $
*
* Description: This file contains global structures used by
*              DNS Module
*********************************************************************/


#ifndef _DNS_GLOB_H_
#define _DNS_GLOB_H_

#ifdef _DNS_MAIN_C_
tDnsGlobalInfo        gDnsGlobalInfo;
tDnsResolverParams    gDnsResolverParams;
tDnsStatistics        gDnsStatistics;
tDnsIndexMgrListInfo  gDnsIndexMgrListInfo;
tDnsCacheInfo         gDnsCacheInfo;
tDnsCapwapSRVRecord   gDnsCapwapSRVRecord;
#else
extern tDnsGlobalInfo        gDnsGlobalInfo;
extern tDnsResolverParams    gDnsResolverParams;
extern tDnsStatistics        gDnsStatistics;
extern tDnsIndexMgrListInfo  gDnsIndexMgrListInfo;
extern tDnsCacheInfo         gDnsCacheInfo;

extern tDnsCapwapSRVRecord   gDnsCapwapSRVRecord;
#endif /* _DNS_MAIN_C_ */

#endif /* _DNS_GLOB_H_ */
