/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdnswr.h,v 1.3 2014/10/13 12:08:07 siva Exp $
*
* Description: Proto types for Middle Level  Routines
*********************************************************************/
#ifndef _FSDNSWR_H
#define _FSDNSWR_H

VOID RegisterFSDNS(VOID);

VOID UnRegisterFSDNS(VOID);
INT4 FsDnsSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsDnsModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDnsTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsDnsQueryRetryCountGet(tSnmpIndex *, tRetVal *);
INT4 FsDnsQueryTimeOutGet(tSnmpIndex *, tRetVal *);
INT4 FsDnsResolverModeGet(tSnmpIndex *, tRetVal *);
INT4 FsDnsPreferentialTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsDnsSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsDnsModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDnsTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsDnsQueryRetryCountSet(tSnmpIndex *, tRetVal *);
INT4 FsDnsQueryTimeOutSet(tSnmpIndex *, tRetVal *);
INT4 FsDnsResolverModeSet(tSnmpIndex *, tRetVal *);
INT4 FsDnsPreferentialTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsDnsSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDnsModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDnsTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDnsQueryRetryCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDnsQueryTimeOutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDnsResolverModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDnsPreferentialTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDnsSystemControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsDnsModuleStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsDnsTraceOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsDnsQueryRetryCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsDnsQueryTimeOutDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsDnsResolverModeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsDnsPreferentialTypeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsDnsNameServerTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDnsServerIPAddressTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsDnsServerIPAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsDnsNameServerRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDnsServerIPAddressTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsDnsServerIPAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsDnsNameServerRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDnsServerIPAddressTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDnsServerIPAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDnsNameServerRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDnsNameServerTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsDnsDomainNameTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDnsDomainNameGet(tSnmpIndex *, tRetVal *);
INT4 FsDnsDomainNameRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDnsDomainNameSet(tSnmpIndex *, tRetVal *);
INT4 FsDnsDomainNameRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDnsDomainNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDnsDomainNameRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDnsDomainNameTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsDnsQueryTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDnsQueryNameGet(tSnmpIndex *, tRetVal *);
INT4 FsDnsQueryNSAddressTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsDnsQueryNSAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsDnsQueriesSentGet(tSnmpIndex *, tRetVal *);
INT4 FsDnsResponseReceivedGet(tSnmpIndex *, tRetVal *);
INT4 FsDnsDroppedResponseGet(tSnmpIndex *, tRetVal *);
INT4 FsDnsUnAnsweredQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsDnsFailedQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsDnsReTransQueriesGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsDnsResCacheRRTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDnsResCacheRRSourceGet(tSnmpIndex *, tRetVal *);
#endif /* _FSDNSWR_H */
