/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stddnsdb.h,v 1.1.1.1 2011/01/21 08:37:31 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDDNSDB_H
#define _STDDNSDB_H

UINT1 DnsResConfigSbeltTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 DnsResCounterByOpcodeTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 DnsResCounterByRcodeTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 DnsResLameDelegationTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 DnsResCacheRRTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 DnsResNCacheErrTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 stddns [] ={1,3,6,1,2,1,32,2};
tSNMP_OID_TYPE stddnsOID = {8, stddns};


UINT4 DnsResConfigImplementIdent [ ] ={1,3,6,1,2,1,32,2,1,1,1};
UINT4 DnsResConfigService [ ] ={1,3,6,1,2,1,32,2,1,1,2};
UINT4 DnsResConfigMaxCnames [ ] ={1,3,6,1,2,1,32,2,1,1,3};
UINT4 DnsResConfigSbeltAddr [ ] ={1,3,6,1,2,1,32,2,1,1,4,1,1};
UINT4 DnsResConfigSbeltName [ ] ={1,3,6,1,2,1,32,2,1,1,4,1,2};
UINT4 DnsResConfigSbeltRecursion [ ] ={1,3,6,1,2,1,32,2,1,1,4,1,3};
UINT4 DnsResConfigSbeltPref [ ] ={1,3,6,1,2,1,32,2,1,1,4,1,4};
UINT4 DnsResConfigSbeltSubTree [ ] ={1,3,6,1,2,1,32,2,1,1,4,1,5};
UINT4 DnsResConfigSbeltClass [ ] ={1,3,6,1,2,1,32,2,1,1,4,1,6};
UINT4 DnsResConfigSbeltStatus [ ] ={1,3,6,1,2,1,32,2,1,1,4,1,7};
UINT4 DnsResConfigUpTime [ ] ={1,3,6,1,2,1,32,2,1,1,5};
UINT4 DnsResConfigResetTime [ ] ={1,3,6,1,2,1,32,2,1,1,6};
UINT4 DnsResConfigReset [ ] ={1,3,6,1,2,1,32,2,1,1,7};
UINT4 DnsResCounterByOpcodeCode [ ] ={1,3,6,1,2,1,32,2,1,2,3,1,1};
UINT4 DnsResCounterByOpcodeQueries [ ] ={1,3,6,1,2,1,32,2,1,2,3,1,2};
UINT4 DnsResCounterByOpcodeResponses [ ] ={1,3,6,1,2,1,32,2,1,2,3,1,3};
UINT4 DnsResCounterByRcodeCode [ ] ={1,3,6,1,2,1,32,2,1,2,4,1,1};
UINT4 DnsResCounterByRcodeResponses [ ] ={1,3,6,1,2,1,32,2,1,2,4,1,2};
UINT4 DnsResCounterNonAuthDataResps [ ] ={1,3,6,1,2,1,32,2,1,2,5};
UINT4 DnsResCounterNonAuthNoDataResps [ ] ={1,3,6,1,2,1,32,2,1,2,6};
UINT4 DnsResCounterMartians [ ] ={1,3,6,1,2,1,32,2,1,2,7};
UINT4 DnsResCounterRecdResponses [ ] ={1,3,6,1,2,1,32,2,1,2,8};
UINT4 DnsResCounterUnparseResps [ ] ={1,3,6,1,2,1,32,2,1,2,9};
UINT4 DnsResCounterFallbacks [ ] ={1,3,6,1,2,1,32,2,1,2,10};
UINT4 DnsResLameDelegationOverflows [ ] ={1,3,6,1,2,1,32,2,1,3,1};
UINT4 DnsResLameDelegationSource [ ] ={1,3,6,1,2,1,32,2,1,3,2,1,1};
UINT4 DnsResLameDelegationName [ ] ={1,3,6,1,2,1,32,2,1,3,2,1,2};
UINT4 DnsResLameDelegationClass [ ] ={1,3,6,1,2,1,32,2,1,3,2,1,3};
UINT4 DnsResLameDelegationCounts [ ] ={1,3,6,1,2,1,32,2,1,3,2,1,4};
UINT4 DnsResLameDelegationStatus [ ] ={1,3,6,1,2,1,32,2,1,3,2,1,5};
UINT4 DnsResCacheStatus [ ] ={1,3,6,1,2,1,32,2,1,4,1};
UINT4 DnsResCacheMaxTTL [ ] ={1,3,6,1,2,1,32,2,1,4,2};
UINT4 DnsResCacheGoodCaches [ ] ={1,3,6,1,2,1,32,2,1,4,3};
UINT4 DnsResCacheBadCaches [ ] ={1,3,6,1,2,1,32,2,1,4,4};
UINT4 DnsResCacheRRName [ ] ={1,3,6,1,2,1,32,2,1,4,5,1,1};
UINT4 DnsResCacheRRClass [ ] ={1,3,6,1,2,1,32,2,1,4,5,1,2};
UINT4 DnsResCacheRRType [ ] ={1,3,6,1,2,1,32,2,1,4,5,1,3};
UINT4 DnsResCacheRRTTL [ ] ={1,3,6,1,2,1,32,2,1,4,5,1,4};
UINT4 DnsResCacheRRElapsedTTL [ ] ={1,3,6,1,2,1,32,2,1,4,5,1,5};
UINT4 DnsResCacheRRSource [ ] ={1,3,6,1,2,1,32,2,1,4,5,1,6};
UINT4 DnsResCacheRRData [ ] ={1,3,6,1,2,1,32,2,1,4,5,1,7};
UINT4 DnsResCacheRRStatus [ ] ={1,3,6,1,2,1,32,2,1,4,5,1,8};
UINT4 DnsResCacheRRIndex [ ] ={1,3,6,1,2,1,32,2,1,4,5,1,9};
UINT4 DnsResCacheRRPrettyName [ ] ={1,3,6,1,2,1,32,2,1,4,5,1,10};
UINT4 DnsResNCacheStatus [ ] ={1,3,6,1,2,1,32,2,1,5,1};
UINT4 DnsResNCacheMaxTTL [ ] ={1,3,6,1,2,1,32,2,1,5,2};
UINT4 DnsResNCacheGoodNCaches [ ] ={1,3,6,1,2,1,32,2,1,5,3};
UINT4 DnsResNCacheBadNCaches [ ] ={1,3,6,1,2,1,32,2,1,5,4};
UINT4 DnsResNCacheErrQName [ ] ={1,3,6,1,2,1,32,2,1,5,5,1,1};
UINT4 DnsResNCacheErrQClass [ ] ={1,3,6,1,2,1,32,2,1,5,5,1,2};
UINT4 DnsResNCacheErrQType [ ] ={1,3,6,1,2,1,32,2,1,5,5,1,3};
UINT4 DnsResNCacheErrTTL [ ] ={1,3,6,1,2,1,32,2,1,5,5,1,4};
UINT4 DnsResNCacheErrElapsedTTL [ ] ={1,3,6,1,2,1,32,2,1,5,5,1,5};
UINT4 DnsResNCacheErrSource [ ] ={1,3,6,1,2,1,32,2,1,5,5,1,6};
UINT4 DnsResNCacheErrCode [ ] ={1,3,6,1,2,1,32,2,1,5,5,1,7};
UINT4 DnsResNCacheErrStatus [ ] ={1,3,6,1,2,1,32,2,1,5,5,1,8};
UINT4 DnsResNCacheErrIndex [ ] ={1,3,6,1,2,1,32,2,1,5,5,1,9};
UINT4 DnsResNCacheErrPrettyName [ ] ={1,3,6,1,2,1,32,2,1,5,5,1,10};
UINT4 DnsResOptCounterReferals [ ] ={1,3,6,1,2,1,32,2,1,6,1};
UINT4 DnsResOptCounterRetrans [ ] ={1,3,6,1,2,1,32,2,1,6,2};
UINT4 DnsResOptCounterNoResponses [ ] ={1,3,6,1,2,1,32,2,1,6,3};
UINT4 DnsResOptCounterRootRetrans [ ] ={1,3,6,1,2,1,32,2,1,6,4};
UINT4 DnsResOptCounterInternals [ ] ={1,3,6,1,2,1,32,2,1,6,5};
UINT4 DnsResOptCounterInternalTimeOuts [ ] ={1,3,6,1,2,1,32,2,1,6,6};




tMbDbEntry stddnsMibEntry[]= {

{{11,DnsResConfigImplementIdent}, NULL, DnsResConfigImplementIdentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,DnsResConfigService}, NULL, DnsResConfigServiceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,DnsResConfigMaxCnames}, NULL, DnsResConfigMaxCnamesGet, DnsResConfigMaxCnamesSet, DnsResConfigMaxCnamesTest, DnsResConfigMaxCnamesDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{13,DnsResConfigSbeltAddr}, GetNextIndexDnsResConfigSbeltTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, DnsResConfigSbeltTableINDEX, 3, 0, 0, NULL},

{{13,DnsResConfigSbeltName}, GetNextIndexDnsResConfigSbeltTable, DnsResConfigSbeltNameGet, DnsResConfigSbeltNameSet, DnsResConfigSbeltNameTest, DnsResConfigSbeltTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, DnsResConfigSbeltTableINDEX, 3, 0, 0, NULL},

{{13,DnsResConfigSbeltRecursion}, GetNextIndexDnsResConfigSbeltTable, DnsResConfigSbeltRecursionGet, DnsResConfigSbeltRecursionSet, DnsResConfigSbeltRecursionTest, DnsResConfigSbeltTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DnsResConfigSbeltTableINDEX, 3, 0, 0, NULL},

{{13,DnsResConfigSbeltPref}, GetNextIndexDnsResConfigSbeltTable, DnsResConfigSbeltPrefGet, DnsResConfigSbeltPrefSet, DnsResConfigSbeltPrefTest, DnsResConfigSbeltTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DnsResConfigSbeltTableINDEX, 3, 0, 0, NULL},

{{13,DnsResConfigSbeltSubTree}, GetNextIndexDnsResConfigSbeltTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, DnsResConfigSbeltTableINDEX, 3, 0, 0, NULL},

{{13,DnsResConfigSbeltClass}, GetNextIndexDnsResConfigSbeltTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DnsResConfigSbeltTableINDEX, 3, 0, 0, NULL},

{{13,DnsResConfigSbeltStatus}, GetNextIndexDnsResConfigSbeltTable, DnsResConfigSbeltStatusGet, DnsResConfigSbeltStatusSet, DnsResConfigSbeltStatusTest, DnsResConfigSbeltTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DnsResConfigSbeltTableINDEX, 3, 0, 1, NULL},

{{11,DnsResConfigUpTime}, NULL, DnsResConfigUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,DnsResConfigResetTime}, NULL, DnsResConfigResetTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,DnsResConfigReset}, NULL, DnsResConfigResetGet, DnsResConfigResetSet, DnsResConfigResetTest, DnsResConfigResetDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{13,DnsResCounterByOpcodeCode}, GetNextIndexDnsResCounterByOpcodeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DnsResCounterByOpcodeTableINDEX, 1, 0, 0, NULL},

{{13,DnsResCounterByOpcodeQueries}, GetNextIndexDnsResCounterByOpcodeTable, DnsResCounterByOpcodeQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DnsResCounterByOpcodeTableINDEX, 1, 0, 0, NULL},

{{13,DnsResCounterByOpcodeResponses}, GetNextIndexDnsResCounterByOpcodeTable, DnsResCounterByOpcodeResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DnsResCounterByOpcodeTableINDEX, 1, 0, 0, NULL},

{{13,DnsResCounterByRcodeCode}, GetNextIndexDnsResCounterByRcodeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, DnsResCounterByRcodeTableINDEX, 1, 0, 0, NULL},

{{13,DnsResCounterByRcodeResponses}, GetNextIndexDnsResCounterByRcodeTable, DnsResCounterByRcodeResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DnsResCounterByRcodeTableINDEX, 1, 0, 0, NULL},

{{11,DnsResCounterNonAuthDataResps}, NULL, DnsResCounterNonAuthDataRespsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,DnsResCounterNonAuthNoDataResps}, NULL, DnsResCounterNonAuthNoDataRespsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,DnsResCounterMartians}, NULL, DnsResCounterMartiansGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,DnsResCounterRecdResponses}, NULL, DnsResCounterRecdResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,DnsResCounterUnparseResps}, NULL, DnsResCounterUnparseRespsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,DnsResCounterFallbacks}, NULL, DnsResCounterFallbacksGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,DnsResLameDelegationOverflows}, NULL, DnsResLameDelegationOverflowsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,DnsResLameDelegationSource}, GetNextIndexDnsResLameDelegationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, DnsResLameDelegationTableINDEX, 3, 0, 0, NULL},

{{13,DnsResLameDelegationName}, GetNextIndexDnsResLameDelegationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, DnsResLameDelegationTableINDEX, 3, 0, 0, NULL},

{{13,DnsResLameDelegationClass}, GetNextIndexDnsResLameDelegationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DnsResLameDelegationTableINDEX, 3, 0, 0, NULL},

{{13,DnsResLameDelegationCounts}, GetNextIndexDnsResLameDelegationTable, DnsResLameDelegationCountsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DnsResLameDelegationTableINDEX, 3, 0, 0, NULL},

{{13,DnsResLameDelegationStatus}, GetNextIndexDnsResLameDelegationTable, DnsResLameDelegationStatusGet, DnsResLameDelegationStatusSet, DnsResLameDelegationStatusTest, DnsResLameDelegationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DnsResLameDelegationTableINDEX, 3, 0, 1, NULL},

{{11,DnsResCacheStatus}, NULL, DnsResCacheStatusGet, DnsResCacheStatusSet, DnsResCacheStatusTest, DnsResCacheStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,DnsResCacheMaxTTL}, NULL, DnsResCacheMaxTTLGet, DnsResCacheMaxTTLSet, DnsResCacheMaxTTLTest, DnsResCacheMaxTTLDep, SNMP_DATA_TYPE_GAUGE32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,DnsResCacheGoodCaches}, NULL, DnsResCacheGoodCachesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,DnsResCacheBadCaches}, NULL, DnsResCacheBadCachesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,DnsResCacheRRName}, GetNextIndexDnsResCacheRRTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, DnsResCacheRRTableINDEX, 4, 0, 0, NULL},

{{13,DnsResCacheRRClass}, GetNextIndexDnsResCacheRRTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DnsResCacheRRTableINDEX, 4, 0, 0, NULL},

{{13,DnsResCacheRRType}, GetNextIndexDnsResCacheRRTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DnsResCacheRRTableINDEX, 4, 0, 0, NULL},

{{13,DnsResCacheRRTTL}, GetNextIndexDnsResCacheRRTable, DnsResCacheRRTTLGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DnsResCacheRRTableINDEX, 4, 0, 0, NULL},

{{13,DnsResCacheRRElapsedTTL}, GetNextIndexDnsResCacheRRTable, DnsResCacheRRElapsedTTLGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DnsResCacheRRTableINDEX, 4, 0, 0, NULL},

{{13,DnsResCacheRRSource}, GetNextIndexDnsResCacheRRTable, DnsResCacheRRSourceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, DnsResCacheRRTableINDEX, 4, 0, 0, NULL},

{{13,DnsResCacheRRData}, GetNextIndexDnsResCacheRRTable, DnsResCacheRRDataGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, DnsResCacheRRTableINDEX, 4, 0, 0, NULL},

{{13,DnsResCacheRRStatus}, GetNextIndexDnsResCacheRRTable, DnsResCacheRRStatusGet, DnsResCacheRRStatusSet, DnsResCacheRRStatusTest, DnsResCacheRRTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DnsResCacheRRTableINDEX, 4, 0, 1, NULL},

{{13,DnsResCacheRRIndex}, GetNextIndexDnsResCacheRRTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DnsResCacheRRTableINDEX, 4, 0, 0, NULL},

{{13,DnsResCacheRRPrettyName}, GetNextIndexDnsResCacheRRTable, DnsResCacheRRPrettyNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, DnsResCacheRRTableINDEX, 4, 0, 0, NULL},

{{11,DnsResNCacheStatus}, NULL, DnsResNCacheStatusGet, DnsResNCacheStatusSet, DnsResNCacheStatusTest, DnsResNCacheStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,DnsResNCacheMaxTTL}, NULL, DnsResNCacheMaxTTLGet, DnsResNCacheMaxTTLSet, DnsResNCacheMaxTTLTest, DnsResNCacheMaxTTLDep, SNMP_DATA_TYPE_GAUGE32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,DnsResNCacheGoodNCaches}, NULL, DnsResNCacheGoodNCachesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,DnsResNCacheBadNCaches}, NULL, DnsResNCacheBadNCachesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,DnsResNCacheErrQName}, GetNextIndexDnsResNCacheErrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, DnsResNCacheErrTableINDEX, 4, 0, 0, NULL},

{{13,DnsResNCacheErrQClass}, GetNextIndexDnsResNCacheErrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DnsResNCacheErrTableINDEX, 4, 0, 0, NULL},

{{13,DnsResNCacheErrQType}, GetNextIndexDnsResNCacheErrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DnsResNCacheErrTableINDEX, 4, 0, 0, NULL},

{{13,DnsResNCacheErrTTL}, GetNextIndexDnsResNCacheErrTable, DnsResNCacheErrTTLGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DnsResNCacheErrTableINDEX, 4, 0, 0, NULL},

{{13,DnsResNCacheErrElapsedTTL}, GetNextIndexDnsResNCacheErrTable, DnsResNCacheErrElapsedTTLGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DnsResNCacheErrTableINDEX, 4, 0, 0, NULL},

{{13,DnsResNCacheErrSource}, GetNextIndexDnsResNCacheErrTable, DnsResNCacheErrSourceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, DnsResNCacheErrTableINDEX, 4, 0, 0, NULL},

{{13,DnsResNCacheErrCode}, GetNextIndexDnsResNCacheErrTable, DnsResNCacheErrCodeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DnsResNCacheErrTableINDEX, 4, 0, 0, NULL},

{{13,DnsResNCacheErrStatus}, GetNextIndexDnsResNCacheErrTable, DnsResNCacheErrStatusGet, DnsResNCacheErrStatusSet, DnsResNCacheErrStatusTest, DnsResNCacheErrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DnsResNCacheErrTableINDEX, 4, 0, 1, NULL},

{{13,DnsResNCacheErrIndex}, GetNextIndexDnsResNCacheErrTable, DnsResNCacheErrIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, DnsResNCacheErrTableINDEX, 4, 0, 0, NULL},

{{13,DnsResNCacheErrPrettyName}, GetNextIndexDnsResNCacheErrTable, DnsResNCacheErrPrettyNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, DnsResNCacheErrTableINDEX, 4, 0, 0, NULL},

{{11,DnsResOptCounterReferals}, NULL, DnsResOptCounterReferalsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,DnsResOptCounterRetrans}, NULL, DnsResOptCounterRetransGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,DnsResOptCounterNoResponses}, NULL, DnsResOptCounterNoResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,DnsResOptCounterRootRetrans}, NULL, DnsResOptCounterRootRetransGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,DnsResOptCounterInternals}, NULL, DnsResOptCounterInternalsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,DnsResOptCounterInternalTimeOuts}, NULL, DnsResOptCounterInternalTimeOutsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData stddnsEntry = { 64, stddnsMibEntry };

#endif /* _STDDNSDB_H */

