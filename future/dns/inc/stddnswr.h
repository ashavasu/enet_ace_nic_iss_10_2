/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stddnswr.h,v 1.1.1.1 2011/01/21 08:37:31 siva Exp $
*
* Description: Proto types for Middle Level  Routines
*********************************************************************/
#ifndef _STDDNSWR_H
#define _STDDNSWR_H

VOID RegisterSTDDNS(VOID);

VOID UnRegisterSTDDNS(VOID);
INT4 DnsResConfigImplementIdentGet(tSnmpIndex *, tRetVal *);
INT4 DnsResConfigServiceGet(tSnmpIndex *, tRetVal *);
INT4 DnsResConfigMaxCnamesGet(tSnmpIndex *, tRetVal *);
INT4 DnsResConfigMaxCnamesSet(tSnmpIndex *, tRetVal *);
INT4 DnsResConfigMaxCnamesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DnsResConfigMaxCnamesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexDnsResConfigSbeltTable(tSnmpIndex *, tSnmpIndex *);
INT4 DnsResConfigSbeltNameGet(tSnmpIndex *, tRetVal *);
INT4 DnsResConfigSbeltRecursionGet(tSnmpIndex *, tRetVal *);
INT4 DnsResConfigSbeltPrefGet(tSnmpIndex *, tRetVal *);
INT4 DnsResConfigSbeltStatusGet(tSnmpIndex *, tRetVal *);
INT4 DnsResConfigSbeltNameSet(tSnmpIndex *, tRetVal *);
INT4 DnsResConfigSbeltRecursionSet(tSnmpIndex *, tRetVal *);
INT4 DnsResConfigSbeltPrefSet(tSnmpIndex *, tRetVal *);
INT4 DnsResConfigSbeltStatusSet(tSnmpIndex *, tRetVal *);
INT4 DnsResConfigSbeltNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DnsResConfigSbeltRecursionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DnsResConfigSbeltPrefTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DnsResConfigSbeltStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DnsResConfigSbeltTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 DnsResConfigUpTimeGet(tSnmpIndex *, tRetVal *);
INT4 DnsResConfigResetTimeGet(tSnmpIndex *, tRetVal *);
INT4 DnsResConfigResetGet(tSnmpIndex *, tRetVal *);
INT4 DnsResConfigResetSet(tSnmpIndex *, tRetVal *);
INT4 DnsResConfigResetTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DnsResConfigResetDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexDnsResCounterByOpcodeTable(tSnmpIndex *, tSnmpIndex *);
INT4 DnsResCounterByOpcodeQueriesGet(tSnmpIndex *, tRetVal *);
INT4 DnsResCounterByOpcodeResponsesGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDnsResCounterByRcodeTable(tSnmpIndex *, tSnmpIndex *);
INT4 DnsResCounterByRcodeResponsesGet(tSnmpIndex *, tRetVal *);
INT4 DnsResCounterNonAuthDataRespsGet(tSnmpIndex *, tRetVal *);
INT4 DnsResCounterNonAuthNoDataRespsGet(tSnmpIndex *, tRetVal *);
INT4 DnsResCounterMartiansGet(tSnmpIndex *, tRetVal *);
INT4 DnsResCounterRecdResponsesGet(tSnmpIndex *, tRetVal *);
INT4 DnsResCounterUnparseRespsGet(tSnmpIndex *, tRetVal *);
INT4 DnsResCounterFallbacksGet(tSnmpIndex *, tRetVal *);
INT4 DnsResLameDelegationOverflowsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDnsResLameDelegationTable(tSnmpIndex *, tSnmpIndex *);
INT4 DnsResLameDelegationCountsGet(tSnmpIndex *, tRetVal *);
INT4 DnsResLameDelegationStatusGet(tSnmpIndex *, tRetVal *);
INT4 DnsResLameDelegationStatusSet(tSnmpIndex *, tRetVal *);
INT4 DnsResLameDelegationStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DnsResLameDelegationTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 DnsResCacheStatusGet(tSnmpIndex *, tRetVal *);
INT4 DnsResCacheMaxTTLGet(tSnmpIndex *, tRetVal *);
INT4 DnsResCacheGoodCachesGet(tSnmpIndex *, tRetVal *);
INT4 DnsResCacheBadCachesGet(tSnmpIndex *, tRetVal *);
INT4 DnsResCacheStatusSet(tSnmpIndex *, tRetVal *);
INT4 DnsResCacheMaxTTLSet(tSnmpIndex *, tRetVal *);
INT4 DnsResCacheStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DnsResCacheMaxTTLTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DnsResCacheStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 DnsResCacheMaxTTLDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexDnsResCacheRRTable(tSnmpIndex *, tSnmpIndex *);
INT4 DnsResCacheRRTTLGet(tSnmpIndex *, tRetVal *);
INT4 DnsResCacheRRElapsedTTLGet(tSnmpIndex *, tRetVal *);
INT4 DnsResCacheRRSourceGet(tSnmpIndex *, tRetVal *);
INT4 DnsResCacheRRDataGet(tSnmpIndex *, tRetVal *);
INT4 DnsResCacheRRStatusGet(tSnmpIndex *, tRetVal *);
INT4 DnsResCacheRRPrettyNameGet(tSnmpIndex *, tRetVal *);
INT4 DnsResCacheRRStatusSet(tSnmpIndex *, tRetVal *);
INT4 DnsResCacheRRStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DnsResCacheRRTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 DnsResNCacheStatusGet(tSnmpIndex *, tRetVal *);
INT4 DnsResNCacheMaxTTLGet(tSnmpIndex *, tRetVal *);
INT4 DnsResNCacheGoodNCachesGet(tSnmpIndex *, tRetVal *);
INT4 DnsResNCacheBadNCachesGet(tSnmpIndex *, tRetVal *);
INT4 DnsResNCacheStatusSet(tSnmpIndex *, tRetVal *);
INT4 DnsResNCacheMaxTTLSet(tSnmpIndex *, tRetVal *);
INT4 DnsResNCacheStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DnsResNCacheMaxTTLTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DnsResNCacheStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 DnsResNCacheMaxTTLDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexDnsResNCacheErrTable(tSnmpIndex *, tSnmpIndex *);
INT4 DnsResNCacheErrTTLGet(tSnmpIndex *, tRetVal *);
INT4 DnsResNCacheErrElapsedTTLGet(tSnmpIndex *, tRetVal *);
INT4 DnsResNCacheErrSourceGet(tSnmpIndex *, tRetVal *);
INT4 DnsResNCacheErrCodeGet(tSnmpIndex *, tRetVal *);
INT4 DnsResNCacheErrStatusGet(tSnmpIndex *, tRetVal *);
INT4 DnsResNCacheErrIndexGet(tSnmpIndex *, tRetVal *);
INT4 DnsResNCacheErrPrettyNameGet(tSnmpIndex *, tRetVal *);
INT4 DnsResNCacheErrStatusSet(tSnmpIndex *, tRetVal *);
INT4 DnsResNCacheErrStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DnsResNCacheErrTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 DnsResOptCounterReferalsGet(tSnmpIndex *, tRetVal *);
INT4 DnsResOptCounterRetransGet(tSnmpIndex *, tRetVal *);
INT4 DnsResOptCounterNoResponsesGet(tSnmpIndex *, tRetVal *);
INT4 DnsResOptCounterRootRetransGet(tSnmpIndex *, tRetVal *);
INT4 DnsResOptCounterInternalsGet(tSnmpIndex *, tRetVal *);
INT4 DnsResOptCounterInternalTimeOutsGet(tSnmpIndex *, tRetVal *);
#endif /* _STDDNSWR_H */
