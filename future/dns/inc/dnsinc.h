/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dnsinc.h,v 1.4 2017/12/19 10:00:02 siva Exp $
 * 
 * Description: This file contains header files included in 
 *              DNS Resolver module.
 *********************************************************************/
#ifndef _DNSINC_H
#define _DNSINC_H

#include "lr.h"
#include "cli.h"
#include "cfa.h"
#include "tcp.h"
#include "fssocket.h"
#include "fswebnm.h"
#include "iss.h"
#include "msr.h"
#include "ip.h"
#include "fssnmp.h"
#include "dns.h"
#include "ip6util.h"
#include "dnsdefn.h"
#include "dnstdfs.h"
#include "dnsprot.h"
#include "dnsglob.h"

#include "fsdnslw.h"
#include "stddnslw.h"

#ifdef SNMP_2_WANTED
#include "fsdnswr.h"
#include "stddnswr.h"
#endif

#include "dnssz.h"
#include "dnscli.h"
#ifdef WTP_WANTED
#include "capwap.h"
#endif
#endif /* _DNSINC_H */
