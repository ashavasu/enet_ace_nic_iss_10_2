
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dnstdfs.h,v 1.9.2.1 2018/03/01 14:05:15 siva Exp $
 * 
 * Description: This file contains data structures used in 
 *              DNS Resolver module.
 *********************************************************************/
/*------------------------------------------------------------------------
 *           Common Enumerations
 *-----------------------------------------------------------------------*/
enum
{
   DNS_QRY_TMR = 0,        /*Query Expiry Timer */
   DNS_CACHE_TMR = 1,  /*Cache Expiry Timer */
   DNS_MAX_TMR_TYPES = 2
};

typedef struct _tDnsGlobalInfo
{
    UINT1          u1SystemCtrlStatus;
    UINT1          u1ModuleStatus;
    UINT1          au1Pad[2];
    tOsixTaskId    DnsTaskId;
    tOsixSemId     DnsSemId;
    tOsixQId       DnsTaskQId;
    tMemPoolId     NameServerPoolId;
    tMemPoolId     DomainNamePoolId;
    tMemPoolId     CacheDataPoolId;
    tMemPoolId     QueryRespMemPoolId;
 tMemPoolId    DnsSrvMemPoolId;
 /*Queue msg mempool Id*/
    tMemPoolId     QMsgMemPoolId;
 /*Pending query mempool Id*/
 tMemPoolId     PendQryMemPoolId;
    tRBTree        PendingQueryList;
 /* Variable storing Timer List*/
    tTimerListId   TimerListId;
 /* description of all timers are stored in this
  * array with TimerId as the array index. */
    tTmrDesc       aTmrDesc[DNS_MAX_TMR_TYPES];
    tTMO_SLL       NameServerList;
    tTMO_SLL       DomainNameList;
    INT4           i4ResolverSockFd;
    /* Variable storing IPv6 socket-id */
    INT4           i4ResolverSock6Fd;
    UINT4          u4TraceOption;
}tDnsGlobalInfo;

typedef struct _tDnsIndexMgrListInfo
{
    tIndexMgrId    QueryIndexList;
    tIndexMgrId    NameServerIndexList;
    tIndexMgrId    DomainNameIndexList;
    tIndexMgrId    CacheIndexList;
    UINT1          au1NameServerBitMap[MAX_DNS_BITMAP_SIZE];
    UINT1          au1DomainNameBitMap[MAX_DNS_BITMAP_SIZE];
    UINT1          au1QueryBitmap[MAX_DNS_BITMAP_SIZE];
    UINT1          au1CacheBitmap[MAX_DNS_BITMAP_SIZE];
}tDnsIndexMgrListInfo;

typedef struct _tDnsResolverParams
{
    UINT4          u4QueryRetryCount;
    UINT4          u4QueryTimeout;
    /* Variable storing Resolver Mode of querying*/
    INT4           i4QueryMode;
 /* Variable storing Resolving Preferential type*/
    INT4           i4QueryType; 
}tDnsResolverParams;

typedef struct _tDnsStatistics
{
    UINT4          u4TotalQuerySent;
    UINT4          u4TotalResponseReceived;
    UINT4          u4DroppedResponse;
    UINT4          u4UnansQueries;
    UINT4          u4FailedQueries;
    UINT4          u4RetransQueries;
}tDnsStatistics;

typedef struct _tDnsQueryInfo
{
    tRBNodeEmbd    QueryNode;
    /*Query expiry timer*/
    tTmrBlk        QryNodeTmrNode;
    UINT1          au1QueryName[DNS_MAX_QUERY_LEN + 1];
    tIPvXAddr      QueryNameServerIP;
    tIPvXAddr      QueryResolvedIP; 
    UINT1          au1ErrorNum[MAX_DNS_NAME_SERVERS]; 
    UINT4          u4DomainCount;
    UINT4          u4NameServerCount;
    UINT4          u4CurRetryCount;
    UINT4          u4QueryClass;
    UINT4          u4QueryType;
    UINT4          u4QueryTTL;
    UINT2          u2QueryIndex;
    UINT1          u1ToBeCached;
    UINT1          au1ResolveName[DNS_MAX_RESOLVER_LEN + 1];
    UINT1          au1Pad[4];
}tDnsQueryInfo;

typedef struct _tDnsNameServerInfo
{
    tTMO_SLL_NODE  NameServerNode;
    UINT4          u4NameServerIndex;
    tIPvXAddr      NameServerIP;
    UINT4          u4RowStatus;
}tDnsNameServerInfo;

typedef struct _tDnsDomainNameInfo
{
    tTMO_SLL_NODE  DomainNameNode;
    UINT4          u4DomainNameIndex;
    UINT1          au1DomainName[DNS_MAX_DOMAIN_NAME_LEN];
    UINT4          u4RowStatus;
}tDnsDomainNameInfo;

typedef struct _tDnsHeader
{
   UINT2           u2QId;
   UINT2           u2Flags;
   UINT2           u2QDCount;
   UINT2           u2ANCount;
   UINT2           u2NSCount;
   UINT2           u2ARCount;
}tDnsHeader;

typedef struct _tDnsCacheInfo
{
    UINT4          u4Status;
    UINT4          u4MaxTTL;
    tRBTree        CacheList;
    UINT4          u4TotalEntries;
}tDnsCacheInfo;


typedef struct _tDnsQMsg
{
    UINT1               au1QueryName[DNS_MAX_QUERY_LEN + 1];
    UINT4               u4QueryType;     /* Query Type.*/
}tDnsQMsg;
typedef struct _tDnsSrvInfo
{
    UINT4 u4Priority;
    UINT4 u4Port;
    UINT4 u4Weight;
    UINT4 u4TargetLen;
    UINT1 au1TargetName[DNS_MAX_DOMAIN_NAME_LEN];
}tDnsSrvInfo;

typedef struct _tDnsCapwapSRVRecord
{
    tIPvXAddr   ServerIp;
    tDnsSrvInfo DnsSrvInfo;
    UINT1       u1Dnslookup;
    UINT1          au1Pad[3];
}tDnsCapwapSRVRecord;

