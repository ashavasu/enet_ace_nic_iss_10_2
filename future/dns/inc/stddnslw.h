/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stddnslw.h,v 1.1.1.1 2011/01/21 08:37:31 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDnsResConfigImplementIdent ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDnsResConfigService ARG_LIST((INT4 *));

INT1
nmhGetDnsResConfigMaxCnames ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDnsResConfigMaxCnames ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DnsResConfigMaxCnames ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DnsResConfigMaxCnames ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DnsResConfigSbeltTable. */
INT1
nmhValidateIndexInstanceDnsResConfigSbeltTable ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for DnsResConfigSbeltTable  */

INT1
nmhGetFirstIndexDnsResConfigSbeltTable ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDnsResConfigSbeltTable ARG_LIST((UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDnsResConfigSbeltName ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDnsResConfigSbeltRecursion ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetDnsResConfigSbeltPref ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetDnsResConfigSbeltStatus ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDnsResConfigSbeltName ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDnsResConfigSbeltRecursion ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetDnsResConfigSbeltPref ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetDnsResConfigSbeltStatus ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DnsResConfigSbeltName ARG_LIST((UINT4 *  ,UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2DnsResConfigSbeltRecursion ARG_LIST((UINT4 *  ,UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2DnsResConfigSbeltPref ARG_LIST((UINT4 *  ,UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2DnsResConfigSbeltStatus ARG_LIST((UINT4 *  ,UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DnsResConfigSbeltTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDnsResConfigUpTime ARG_LIST((UINT4 *));

INT1
nmhGetDnsResConfigResetTime ARG_LIST((UINT4 *));

INT1
nmhGetDnsResConfigReset ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDnsResConfigReset ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DnsResConfigReset ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DnsResConfigReset ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DnsResCounterByOpcodeTable. */
INT1
nmhValidateIndexInstanceDnsResCounterByOpcodeTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for DnsResCounterByOpcodeTable  */

INT1
nmhGetFirstIndexDnsResCounterByOpcodeTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDnsResCounterByOpcodeTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDnsResCounterByOpcodeQueries ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDnsResCounterByOpcodeResponses ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for DnsResCounterByRcodeTable. */
INT1
nmhValidateIndexInstanceDnsResCounterByRcodeTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for DnsResCounterByRcodeTable  */

INT1
nmhGetFirstIndexDnsResCounterByRcodeTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDnsResCounterByRcodeTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDnsResCounterByRcodeResponses ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDnsResCounterNonAuthDataResps ARG_LIST((UINT4 *));

INT1
nmhGetDnsResCounterNonAuthNoDataResps ARG_LIST((UINT4 *));

INT1
nmhGetDnsResCounterMartians ARG_LIST((UINT4 *));

INT1
nmhGetDnsResCounterRecdResponses ARG_LIST((UINT4 *));

INT1
nmhGetDnsResCounterUnparseResps ARG_LIST((UINT4 *));

INT1
nmhGetDnsResCounterFallbacks ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDnsResLameDelegationOverflows ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for DnsResLameDelegationTable. */
INT1
nmhValidateIndexInstanceDnsResLameDelegationTable ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for DnsResLameDelegationTable  */

INT1
nmhGetFirstIndexDnsResLameDelegationTable ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDnsResLameDelegationTable ARG_LIST((UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDnsResLameDelegationCounts ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetDnsResLameDelegationStatus ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDnsResLameDelegationStatus ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DnsResLameDelegationStatus ARG_LIST((UINT4 *  ,UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DnsResLameDelegationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDnsResCacheStatus ARG_LIST((INT4 *));

INT1
nmhGetDnsResCacheMaxTTL ARG_LIST((UINT4 *));

INT1
nmhGetDnsResCacheGoodCaches ARG_LIST((UINT4 *));

INT1
nmhGetDnsResCacheBadCaches ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDnsResCacheStatus ARG_LIST((INT4 ));

INT1
nmhSetDnsResCacheMaxTTL ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DnsResCacheStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2DnsResCacheMaxTTL ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DnsResCacheStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DnsResCacheMaxTTL ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DnsResCacheRRTable. */
INT1
nmhValidateIndexInstanceDnsResCacheRRTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for DnsResCacheRRTable  */

INT1
nmhGetFirstIndexDnsResCacheRRTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDnsResCacheRRTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDnsResCacheRRTTL ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetDnsResCacheRRElapsedTTL ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetDnsResCacheRRSource ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetDnsResCacheRRData ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDnsResCacheRRStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetDnsResCacheRRPrettyName ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDnsResCacheRRStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DnsResCacheRRStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DnsResCacheRRTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDnsResNCacheStatus ARG_LIST((INT4 *));

INT1
nmhGetDnsResNCacheMaxTTL ARG_LIST((UINT4 *));

INT1
nmhGetDnsResNCacheGoodNCaches ARG_LIST((UINT4 *));

INT1
nmhGetDnsResNCacheBadNCaches ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDnsResNCacheStatus ARG_LIST((INT4 ));

INT1
nmhSetDnsResNCacheMaxTTL ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DnsResNCacheStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2DnsResNCacheMaxTTL ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DnsResNCacheStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DnsResNCacheMaxTTL ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DnsResNCacheErrTable. */
INT1
nmhValidateIndexInstanceDnsResNCacheErrTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for DnsResNCacheErrTable  */

INT1
nmhGetFirstIndexDnsResNCacheErrTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDnsResNCacheErrTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDnsResNCacheErrTTL ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetDnsResNCacheErrElapsedTTL ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetDnsResNCacheErrSource ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetDnsResNCacheErrCode ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetDnsResNCacheErrStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetDnsResNCacheErrPrettyName ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDnsResNCacheErrStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DnsResNCacheErrStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DnsResNCacheErrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDnsResOptCounterReferals ARG_LIST((UINT4 *));

INT1
nmhGetDnsResOptCounterRetrans ARG_LIST((UINT4 *));

INT1
nmhGetDnsResOptCounterNoResponses ARG_LIST((UINT4 *));

INT1
nmhGetDnsResOptCounterRootRetrans ARG_LIST((UINT4 *));

INT1
nmhGetDnsResOptCounterInternals ARG_LIST((UINT4 *));

INT1
nmhGetDnsResOptCounterInternalTimeOuts ARG_LIST((UINT4 *));
