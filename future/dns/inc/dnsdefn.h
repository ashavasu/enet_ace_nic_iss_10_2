/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: dnsdefn.h,v 1.9 2017/12/19 10:00:02 siva Exp $
*
* Description: This file contains all macro definition used by
*              DNS Module
*********************************************************************/

#define DNS_TASK_NAME              "DNSR"
#define DNS_SEM_NAME               "DNSS"
#define DNS_Q_NAME                 "DNSQ"

/* Events */
#define DNS_TMR_EXP_EVT      0x01
#define DNS_APP_REQ_EVT           0x02
#define DNS_IPV4_PKT_RXED_EVT            0x04
#define DNS_IPV6_PKT_RXED_EVT            0x08
#define DNS_ALL_EVENTS             0xff

#define DNS_MAX_PKT_LEN            1500
#define DNS_RESP_OFF_MASK          0x3FFF


/* DNS Packet Flags */
#define DNS_QUERY_FLAG                0x0000
#define DNS_RESPONSE_FLAG             0x8000
#define DNS_RECURSIVE_QUERY           0x0100
#define DNS_TRUNCATION_FLAG           0x0200
#define DNS_RCODE_MASK                0x000F
#define DNS_RCODE_NO_NAME             0x0003
#define DNS_RCODE_SERV_FAIL             0x0002
/* Default DNS Packet section count */
#define DNS_QDCOUNT             1
#define DNS_ANCOUNT             0
#define DNS_ARCOUNT             0
#define DNS_NSCOUNT             0

/* Default DNS Class and Type */
#define DNS_INTERNET_CLASS      1
#define DNS_HOST_ADDRESS        1
#define DNS_SRV_RECORD          0x21
#define DNS_PKT_OFFSET1         1
#define DNS_PKT_OFFSET2         2
#define DNS_PKT_OFFSET4         4
#define MAX_DNS_ANS_COUNT       20
#define DNS_AAAA_RECORD        28
/* DNS Packet Error codes */
#define DNS_NO_ERROR               0
#define DNS_FORMAT_ERROR           1
#define DNS_SERVER_FAILURE         2
#define DNS_NAME_SERVER_ERROR      3
#define DNS_NOT_IMPLEMENTED_ERROR  4
#define DNS_REFUSED_ERROR          5
#define DNS_TMR_EXPIRY_ERROR       6

/* Default DNS Port */
#define DNS_UDP_PORT               53

#define DNS_MAX_RETRY_COUNT        10

#define DNS_MIN_TIMEOUT            1
#define DNS_MAX_TIMEOUT            100

#define DNS_MIN_CACHE_TTL          1
#define DNS_MAX_CACHE_TTL          (60 * 60)  /* One Hour */

#define DNS_ZERO                   0
#define DNS_ONE                    1
#define DNS_TWO                    2
#define DNS_THIRTY                 30
#define DNS_MAX_UINT2_VALUE        65535

#define DNS_MAX_DOMAIN_NAME_LEN     64

/* DNS Packet field Length */
#define DNS_TYPE_AND_CLASS_LEN           4
#define DNS_RESPONSE_ANSWER_FLAG_LEN     4
#define DNS_RESPONSE_TTL_LEN             4
#define DNS_ANSWER_RDATA_LEN             2

/* Flags used while deleting Cache Record */
#define DNS_CACHE_RB_REM      1     /* Delete from RB Tree */
#define DNS_CACHE_NO_RB_REM   2     /* Do not delete from RB Tree */

/* Macros for DNS Resolver mode */
#define DNS_MODE_SIMUL          1
#define DNS_MODE_SEQUENCE       2

/* Minimum count of dots for adding the domain name list 
 * to the query to be resolved */
#define DNS_MIN_QUERY_DOTS    1

/* Default Query Retry Count */
#define DNS_DEFAULT_RETRY_COUNT   1

/* Default Query Retry timeout value */
#define DNS_DEFAULT_TIMEOUT       5

/*IP Resolved Status*/
#define DNSIP_NO_RESOLVED   -1 
#define DNSIP_RESOLVED      1
#define DNSIP_INPROGRESS     2

/* Trace Printing function */
#define  DNS_TRACE(x)        DnsTracePrint( __FILE__, __LINE__, DnsTrace x)

#define DNS_MAX_LOG_STR_LEN        2000
#define DNS_TRACE_FLAG             gDnsGlobalInfo.u4TraceOption
#define DNS_TMRLIST_ID             gDnsGlobalInfo.TimerListId
#define DNS_QUEUE_ID         gDnsGlobalInfo.DnsTaskQId
#define DNS_SEM_ID               gDnsGlobalInfo.DnsSemId
#define DNS_TASK_ID              gDnsGlobalInfo.DnsTaskId


#define DNS_GET_SYS_TIME(u4CurrentTime)  \
  u4CurrentTime = OsixGetSysUpTime ()

#define DNS_REMAIN_TIME(x,y)  \
  x = ((x > y) ? ((x) - (y)) : (0))

#define DNS_UNANS_DECR(value)  \
  value = ((value > 0) ? ((value)-(1)) : (0))

