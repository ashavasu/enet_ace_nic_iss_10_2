/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdnsdb.h,v 1.4 2014/10/13 12:08:07 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSDNSDB_H
#define _FSDNSDB_H

UINT1 FsDnsNameServerTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsDnsDomainNameTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsDnsQueryTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsDnsResCacheRRTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fsdns [] ={1,3,6,1,4,1,29601,2,99};
tSNMP_OID_TYPE fsdnsOID = {9, fsdns};


UINT4 FsDnsSystemControl [ ] ={1,3,6,1,4,1,29601,2,99,1,1};
UINT4 FsDnsModuleStatus [ ] ={1,3,6,1,4,1,29601,2,99,1,2};
UINT4 FsDnsTraceOption [ ] ={1,3,6,1,4,1,29601,2,99,1,3};
UINT4 FsDnsQueryRetryCount [ ] ={1,3,6,1,4,1,29601,2,99,1,4};
UINT4 FsDnsQueryTimeOut [ ] ={1,3,6,1,4,1,29601,2,99,1,5};
UINT4 FsDnsResolverMode [ ] ={1,3,6,1,4,1,29601,2,99,1,6};
UINT4 FsDnsPreferentialType [ ] ={1,3,6,1,4,1,29601,2,99,1,7};
UINT4 FsDnsNameServerIndex [ ] ={1,3,6,1,4,1,29601,2,99,2,1,1,1};
UINT4 FsDnsServerIPAddressType [ ] ={1,3,6,1,4,1,29601,2,99,2,1,1,2};
UINT4 FsDnsServerIPAddress [ ] ={1,3,6,1,4,1,29601,2,99,2,1,1,3};
UINT4 FsDnsNameServerRowStatus [ ] ={1,3,6,1,4,1,29601,2,99,2,1,1,4};
UINT4 FsDnsDomainNameIndex [ ] ={1,3,6,1,4,1,29601,2,99,3,1,1,1};
UINT4 FsDnsDomainName [ ] ={1,3,6,1,4,1,29601,2,99,3,1,1,2};
UINT4 FsDnsDomainNameRowStatus [ ] ={1,3,6,1,4,1,29601,2,99,3,1,1,3};
UINT4 FsDnsQueryIndex [ ] ={1,3,6,1,4,1,29601,2,99,4,1,1,1};
UINT4 FsDnsQueryName [ ] ={1,3,6,1,4,1,29601,2,99,4,1,1,2};
UINT4 FsDnsQueryNSAddressType [ ] ={1,3,6,1,4,1,29601,2,99,4,1,1,3};
UINT4 FsDnsQueryNSAddress [ ] ={1,3,6,1,4,1,29601,2,99,4,1,1,4};
UINT4 FsDnsQueriesSent [ ] ={1,3,6,1,4,1,29601,2,99,5,1};
UINT4 FsDnsResponseReceived [ ] ={1,3,6,1,4,1,29601,2,99,5,2};
UINT4 FsDnsDroppedResponse [ ] ={1,3,6,1,4,1,29601,2,99,5,3};
UINT4 FsDnsUnAnsweredQueries [ ] ={1,3,6,1,4,1,29601,2,99,5,4};
UINT4 FsDnsFailedQueries [ ] ={1,3,6,1,4,1,29601,2,99,5,5};
UINT4 FsDnsReTransQueries [ ] ={1,3,6,1,4,1,29601,2,99,5,6};
UINT4 FsDnsResCacheRRSource [ ] ={1,3,6,1,4,1,29601,2,99,6,1,1,1};




tMbDbEntry fsdnsMibEntry[]= {

{{11,FsDnsSystemControl}, NULL, FsDnsSystemControlGet, FsDnsSystemControlSet, FsDnsSystemControlTest, FsDnsSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsDnsModuleStatus}, NULL, FsDnsModuleStatusGet, FsDnsModuleStatusSet, FsDnsModuleStatusTest, FsDnsModuleStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsDnsTraceOption}, NULL, FsDnsTraceOptionGet, FsDnsTraceOptionSet, FsDnsTraceOptionTest, FsDnsTraceOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsDnsQueryRetryCount}, NULL, FsDnsQueryRetryCountGet, FsDnsQueryRetryCountSet, FsDnsQueryRetryCountTest, FsDnsQueryRetryCountDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsDnsQueryTimeOut}, NULL, FsDnsQueryTimeOutGet, FsDnsQueryTimeOutSet, FsDnsQueryTimeOutTest, FsDnsQueryTimeOutDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{11,FsDnsResolverMode}, NULL, FsDnsResolverModeGet, FsDnsResolverModeSet, FsDnsResolverModeTest, FsDnsResolverModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsDnsPreferentialType}, NULL, FsDnsPreferentialTypeGet, FsDnsPreferentialTypeSet, FsDnsPreferentialTypeTest, FsDnsPreferentialTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{13,FsDnsNameServerIndex}, GetNextIndexFsDnsNameServerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsDnsNameServerTableINDEX, 1, 0, 0, NULL},

{{13,FsDnsServerIPAddressType}, GetNextIndexFsDnsNameServerTable, FsDnsServerIPAddressTypeGet, FsDnsServerIPAddressTypeSet, FsDnsServerIPAddressTypeTest, FsDnsNameServerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDnsNameServerTableINDEX, 1, 0, 0, NULL},

{{13,FsDnsServerIPAddress}, GetNextIndexFsDnsNameServerTable, FsDnsServerIPAddressGet, FsDnsServerIPAddressSet, FsDnsServerIPAddressTest, FsDnsNameServerTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDnsNameServerTableINDEX, 1, 0, 0, NULL},

{{13,FsDnsNameServerRowStatus}, GetNextIndexFsDnsNameServerTable, FsDnsNameServerRowStatusGet, FsDnsNameServerRowStatusSet, FsDnsNameServerRowStatusTest, FsDnsNameServerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDnsNameServerTableINDEX, 1, 0, 1, NULL},

{{13,FsDnsDomainNameIndex}, GetNextIndexFsDnsDomainNameTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsDnsDomainNameTableINDEX, 1, 0, 0, NULL},

{{13,FsDnsDomainName}, GetNextIndexFsDnsDomainNameTable, FsDnsDomainNameGet, FsDnsDomainNameSet, FsDnsDomainNameTest, FsDnsDomainNameTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDnsDomainNameTableINDEX, 1, 0, 0, NULL},

{{13,FsDnsDomainNameRowStatus}, GetNextIndexFsDnsDomainNameTable, FsDnsDomainNameRowStatusGet, FsDnsDomainNameRowStatusSet, FsDnsDomainNameRowStatusTest, FsDnsDomainNameTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDnsDomainNameTableINDEX, 1, 0, 1, NULL},

{{13,FsDnsQueryIndex}, GetNextIndexFsDnsQueryTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsDnsQueryTableINDEX, 1, 0, 0, NULL},

{{13,FsDnsQueryName}, GetNextIndexFsDnsQueryTable, FsDnsQueryNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsDnsQueryTableINDEX, 1, 0, 0, NULL},

{{13,FsDnsQueryNSAddressType}, GetNextIndexFsDnsQueryTable, FsDnsQueryNSAddressTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDnsQueryTableINDEX, 1, 0, 0, NULL},

{{13,FsDnsQueryNSAddress}, GetNextIndexFsDnsQueryTable, FsDnsQueryNSAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsDnsQueryTableINDEX, 1, 0, 0, NULL},

{{11,FsDnsQueriesSent}, NULL, FsDnsQueriesSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsDnsResponseReceived}, NULL, FsDnsResponseReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsDnsDroppedResponse}, NULL, FsDnsDroppedResponseGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsDnsUnAnsweredQueries}, NULL, FsDnsUnAnsweredQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsDnsFailedQueries}, NULL, FsDnsFailedQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsDnsReTransQueries}, NULL, FsDnsReTransQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,FsDnsResCacheRRSource}, GetNextIndexFsDnsResCacheRRTable, FsDnsResCacheRRSourceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsDnsResCacheRRTableINDEX, 4, 0, 0, NULL},
};
tMibData fsdnsEntry = { 25, fsdnsMibEntry };

#endif /* _FSDNSDB_H */

