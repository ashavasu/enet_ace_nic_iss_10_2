/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dnsprot.h,v 1.9.2.1 2018/03/01 14:05:14 siva Exp $
 * 
 * Description: This file contains protoypes of funxtions used by 
 *              DNS Resolver module.
 *********************************************************************/


/* dnsmain.c */

PUBLIC INT4 DnsResolverInit (UINT1);
PUBLIC INT4 DnsResolverDeInit (UINT1);
PUBLIC INT4 DnsResolverInitGlobalInfo (UINT1);
PUBLIC INT4 DnsResolverMemInit (VOID);
PUBLIC INT4 DnsResolverSockInit (VOID);
#ifdef IP6_WANTED
PUBLIC INT4 DnsResolverSock6Init (VOID);
#endif
PUBLIC INT4 DnsResolverIndexMgrInit (VOID);
PUBLIC INT4 DnsModuleStart (VOID);
PUBLIC INT4 DnsModuleShutdown (VOID);
PUBLIC INT4 DnsResolverInitInfo (VOID);
PUBLIC INT4 DnsResolverShut (VOID);

/* dnsutil.c */
PUBLIC INT4 DnsFillNameServerIP (tDnsQueryInfo *);
PUBLIC INT4 DnsQueryIdCmp (tRBElem *, tRBElem *);
PUBLIC INT4 DnsAddDomainToQuery (tDnsQueryInfo *);
PUBLIC INT4 DnsAddNameServertoList (tDnsNameServerInfo *);
PUBLIC INT4 DnsAddDomainNametoList (tDnsDomainNameInfo *);
PUBLIC INT4 DnsCacheTreeCmp (tRBElem *, tRBElem *);
PUBLIC INT4 DnsCacheListCmp (tRBElem *, tRBElem *);
PUBLIC tDnsNameServerInfo * DnsGetNameServer (UINT4);
PUBLIC UINT4 DnsGetNameServerFromAdd (tIPvXAddr *);
PUBLIC VOID DnsDelNameServerFromList (tDnsNameServerInfo *);
PUBLIC INT4 DnsGetServerIndex (UINT4, UINT1 *);
PUBLIC tDnsDomainNameInfo * DnsGetDomainName (UINT4);
PUBLIC VOID DnsDelDomainNameFromList (tDnsDomainNameInfo *);
PUBLIC INT4 DnsGetDomainNameIndex (UINT1 *);
PUBLIC UINT4 DnsCountDotsFromName (UINT1 *);
PUBLIC UINT4 DnsDuplicateNSIP (UINT1 *);
PUBLIC UINT4 DnsDupDomainName (UINT1 *);
PUBLIC CHR1  *DnsTrace ( UINT4,  const char *, ...);
PUBLIC VOID  DnsTracePrint  ( const CHR1 *, UINT4, CHR1 *);


/* dnsquery.c */
PUBLIC INT4 DnsSendQueryToResolver (tDnsQueryInfo *);
PUBLIC INT4 DnsCreateQuery (tDnsQueryInfo *, UINT1 *);
PUBLIC INT4 DnsFillQueryHeader (UINT1 *, UINT2);
PUBLIC INT4 DnsFillQueryQues (tDnsQueryInfo *, UINT1 *); 
PUBLIC INT4 DnsSendQuery (UINT1 *,UINT4, tDnsQueryInfo *);
PUBLIC INT4 DNSResolveIPvXFromName PROTO ((UINT1 *, UINT1,
                                            tDNSResolvedIpInfo *));
PUBLIC INT4 DnsFillQueryIndex (tDnsQueryInfo *, UINT4 , UINT4 , UINT1 *);
PUBLIC INT4 DnsChkCache ( UINT4 , UINT4 , 
                         UINT1 *,INT4 , INT4 *, tDNSResolvedIpInfo *);
PUBLIC UINT1 DnsQrySendNameserver (tDnsQueryInfo *);
PUBLIC INT4 DnsUpdateResolIP (tDnsQueryInfo *);

PUBLIC INT4
DnsReqSendQuery  (tDnsQueryInfo *);
PUBLIC INT4
DNSResolSimulMode (tDnsQueryInfo *);
PUBLIC INT4
DNSResolSeqMode (tDnsQueryInfo *);

/* dnsresp.c */
PUBLIC INT4 DnsRcvRespFromServer (tDnsQueryInfo *);
PUBLIC INT4 DnsProcessResponsePkt (tDnsQueryInfo *, UINT1 *, UINT1 *);
PUBLIC UINT4 DnsSkipRespQuesSection (tDnsQueryInfo *, UINT1 *, UINT4, UINT2);
PUBLIC UINT4 DnsGetAddrFromAnsSection (tDnsQueryInfo *, UINT1 *, UINT4, UINT2);
PUBLIC UINT4 DnsSkipRespAuthSection (UINT1 *, UINT4, UINT2);
PUBLIC INT4
DnsRcvRespIPvXFromServer (INT4 );
PUBLIC INT4
DnsRcvRespIPvX (INT4 , UINT1 *);
PUBLIC INT4 DnsResReqQry (UINT2 , UINT1 *, tIPvXAddr *);


/* dnscache.c */
PUBLIC INT4 DnsResolverCacheInit (VOID);
PUBLIC INT4 DnsAddCacheData (UINT1 *, UINT4, UINT4, UINT4, tIPvXAddr, tIPvXAddr);
PUBLIC tDnsCacheRRInfo * DnsCacheLookup (UINT1 *, UINT4, UINT4);
PUBLIC VOID DnsCacheClear (VOID);
PUBLIC VOID DnsDeleteCacheData (tDnsCacheRRInfo *, UINT4);
PUBLIC tDnsCacheRRInfo *DnsGetCacheRR (UINT1 *, UINT4, UINT4, UINT4);
PUBLIC INT4 CacheFreeNode (tRBElem *, UINT4);
PUBLIC INT4 DnsCacheGetElappsedTTL (tDnsCacheRRInfo *);


/* dnstmr.c */
PUBLIC INT4 DnsTmrInit (VOID);
PUBLIC INT4 DnsTmrDeInit (VOID);
PUBLIC VOID DnsTmrExpHandler (VOID);
PUBLIC VOID DnsTmrStopTimer (tTmrBlk * );
PUBLIC UINT4 DnsTmrStart (tTmrBlk * , UINT1 , UINT4 );
PUBLIC VOID DnsTmrRestart (tTmrBlk * , UINT1 , UINT4 );

/* dnsreq.c */
PUBLIC INT4
DnsReqSendQ (UINT1 * ,UINT1 , INT4 *, INT4 *, tDNSResolvedIpInfo *);
PUBLIC INT4 DnsQueMsg (UINT1 *, UINT4);
PUBLIC VOID DnsQueueReqHandler (tDnsQMsg * );
PUBLIC INT4 DnsChkPendQry (UINT4 , UINT1 * );
PUBLIC VOID DnsPktInSocket (INT4);
PUBLIC VOID DnsPktInIpv6Socket (INT4);
PUBLIC VOID DnsDeleteQueryData (tDnsQueryInfo * );
PUBLIC UINT4 DnsProcessAdditionalRecord(UINT1 *, UINT4 , UINT2);
PUBLIC VOID  DnsSortSrvRecord(tDnsSrvInfo **,UINT4);
PUBLIC INT4
DnsCreateNameServer ( UINT4 u4Index, UINT4 u4Family,UINT1 *pu1IPAddr);
