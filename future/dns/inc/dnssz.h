/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
 $Id: dnssz.h,v 1.5.32.1 2018/03/01 14:05:15 siva Exp $

* Description: This file contains routines related with DNS
*              Sizing functionality
********************************************************************/

enum {
    MAX_DNS_CACHE_ENTRIES_SIZING_ID,
    MAX_DNS_DOMAINS_SIZING_ID,
    MAX_DNS_NAME_SERVERS_SIZING_ID,
    MAX_DNS_PKT_LEN_SIZING_ID,
    MAX_DNS_Q_MSG_SIZING_ID,
 MAX_DNS_PEND_QRY_SIZING_ID,
    MAX_DNS_SRV_SIZING_ID,
    DNS_MAX_SIZING_ID
};


#ifdef  _DNSSZ_C
tMemPoolId DNSMemPoolIds[ DNS_MAX_SIZING_ID];
INT4  DnsSizingMemCreateMemPools(VOID);
VOID  DnsSizingMemDeleteMemPools(VOID);
INT4   DnsSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _DNSSZ_C  */
extern tMemPoolId DNSMemPoolIds[ ];
extern INT4  DnsSizingMemCreateMemPools(VOID);
extern VOID  DnsSizingMemDeleteMemPools(VOID);
#endif /*  _DNSSZ_C  */


#ifdef  _DNSSZ_C
tFsModSizingParams FsDNSSizingParams [] = {
{ "tDnsCacheRRInfo", "MAX_DNS_CACHE_ENTRIES", sizeof(tDnsCacheRRInfo),MAX_DNS_CACHE_ENTRIES, MAX_DNS_CACHE_ENTRIES,0 },
{ "tDnsDomainNameInfo", "MAX_DNS_DOMAINS", sizeof(tDnsDomainNameInfo),MAX_DNS_DOMAINS, MAX_DNS_DOMAINS,0 },
{ "tDnsNameServerInfo", "MAX_DNS_NAME_SERVERS", sizeof(tDnsNameServerInfo),MAX_DNS_NAME_SERVERS, MAX_DNS_NAME_SERVERS,0 },
{ "UINT1[DNS_MAX_PKT_LEN]", "MAX_DNS_PKT_LEN", sizeof(UINT1[DNS_MAX_PKT_LEN]),MAX_DNS_PKT_LEN, MAX_DNS_PKT_LEN,0 },
{ "tDnsQMsg", "MAX_DNS_Q_MSG", sizeof(tDnsQMsg),MAX_DNS_Q_MSG, MAX_DNS_Q_MSG,0 },
{ "tDnsQueryInfo", "MAX_DNS_PENDING_QUERIES", sizeof(tDnsQueryInfo),MAX_DNS_PENDING_QUERIES, MAX_DNS_PENDING_QUERIES,0 },
{ "tDnsSrvInfo", "MAX_DNS_SRV", sizeof(tDnsSrvInfo),MAX_DNS_SRV, MAX_DNS_SRV,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _DNSSZ_C  */
extern tFsModSizingParams FsDNSSizingParams [];
#endif /*  _DNSSZ_C  */


