/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdnslw.h,v 1.3 2014/10/13 12:08:07 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDnsSystemControl ARG_LIST((INT4 *));

INT1
nmhGetFsDnsModuleStatus ARG_LIST((INT4 *));

INT1
nmhGetFsDnsTraceOption ARG_LIST((INT4 *));

INT1
nmhGetFsDnsQueryRetryCount ARG_LIST((UINT4 *));

INT1
nmhGetFsDnsQueryTimeOut ARG_LIST((UINT4 *));

INT1
nmhGetFsDnsResolverMode ARG_LIST((INT4 *));

INT1
nmhGetFsDnsPreferentialType ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDnsSystemControl ARG_LIST((INT4 ));

INT1
nmhSetFsDnsModuleStatus ARG_LIST((INT4 ));

INT1
nmhSetFsDnsTraceOption ARG_LIST((INT4 ));

INT1
nmhSetFsDnsQueryRetryCount ARG_LIST((UINT4 ));

INT1
nmhSetFsDnsQueryTimeOut ARG_LIST((UINT4 ));

INT1
nmhSetFsDnsResolverMode ARG_LIST((INT4 ));

INT1
nmhSetFsDnsPreferentialType ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDnsSystemControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsDnsModuleStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsDnsTraceOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsDnsQueryRetryCount ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsDnsQueryTimeOut ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsDnsResolverMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsDnsPreferentialType ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDnsSystemControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDnsModuleStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDnsTraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDnsQueryRetryCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDnsQueryTimeOut ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDnsResolverMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDnsPreferentialType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDnsNameServerTable. */
INT1
nmhValidateIndexInstanceFsDnsNameServerTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDnsNameServerTable  */

INT1
nmhGetFirstIndexFsDnsNameServerTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDnsNameServerTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDnsServerIPAddressType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsDnsServerIPAddress ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDnsNameServerRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDnsServerIPAddressType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsDnsServerIPAddress ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDnsNameServerRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDnsServerIPAddressType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsDnsServerIPAddress ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDnsNameServerRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDnsNameServerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDnsDomainNameTable. */
INT1
nmhValidateIndexInstanceFsDnsDomainNameTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDnsDomainNameTable  */

INT1
nmhGetFirstIndexFsDnsDomainNameTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDnsDomainNameTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDnsDomainName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDnsDomainNameRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDnsDomainName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDnsDomainNameRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDnsDomainName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDnsDomainNameRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDnsDomainNameTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDnsQueryTable. */
INT1
nmhValidateIndexInstanceFsDnsQueryTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDnsQueryTable  */

INT1
nmhGetFirstIndexFsDnsQueryTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDnsQueryTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDnsQueryName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDnsQueryNSAddressType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsDnsQueryNSAddress ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDnsQueriesSent ARG_LIST((UINT4 *));

INT1
nmhGetFsDnsResponseReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsDnsDroppedResponse ARG_LIST((UINT4 *));

INT1
nmhGetFsDnsUnAnsweredQueries ARG_LIST((UINT4 *));

INT1
nmhGetFsDnsFailedQueries ARG_LIST((UINT4 *));

INT1
nmhGetFsDnsReTransQueries ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for FsDnsResCacheRRTable. */
INT1
nmhValidateIndexInstanceFsDnsResCacheRRTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDnsResCacheRRTable  */

INT1
nmhGetFirstIndexFsDnsResCacheRRTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDnsResCacheRRTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDnsResCacheRRSource ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
