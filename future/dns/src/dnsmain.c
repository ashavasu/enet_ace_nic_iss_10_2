/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dnsmain.c,v 1.11.32.1 2018/03/01 14:05:18 siva Exp $
 *
 * Description: This file contains the init, deinit functions 
 *              for DNS Module
 *
 *******************************************************************/

#ifndef _DNS_MAIN_C_
#define _DNS_MAIN_C_

#include "dnsinc.h"

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsResolverMainTask
 *
 *    DESCRIPTION      : Entry point function of DNS Resolver Task.
 *
 *    INPUT            : pi1Arg - Pointer to the arguments
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/

PUBLIC VOID
DnsResolverMainTask (INT1 *pi1Arg)
{
    UINT4               u4Events = 0;
    UINT1               u1ModStart = OSIX_FALSE;
    tDnsQMsg           *pQMsg = NULL;

    UNUSED_PARAM (pi1Arg);

    DNS_TRACE ((DNS_INITSHUT_TRC, " Intialising Dns Module Task \r\n"));

    /* Initialises Resolver Module */
    if (DnsResolverInit (u1ModStart) == DNS_FAILURE)
    {
        DNS_TRACE ((DNS_INITSHUT_TRC | DNS_FAILURE_TRC,
                    " DNS Task Initialization Fails \r\n"));
        DnsResolverDeInit (u1ModStart);
        lrInitComplete (OSIX_FAILURE);
    }

#ifdef SNMP_2_WANTED
    RegisterSTDDNS ();
    RegisterFSDNS ();
#endif

    /* Indicate the status of initialization to main routine */
    lrInitComplete (OSIX_SUCCESS);

    DNS_TRACE ((DNS_INITSHUT_TRC, " DNS Module Successfully Initialised \r\n"));

    while (1)
    {
        if (OsixEvtRecv (gDnsGlobalInfo.DnsTaskId, DNS_ALL_EVENTS,
                         OSIX_WAIT, &u4Events) == DNS_SUCCESS)
        {
            DnsLock ();
            if (u4Events & DNS_TMR_EXP_EVT)
            {
                DnsTmrExpHandler ();

            }
            if (u4Events & DNS_APP_REQ_EVT)
            {
                while (OsixQueRecv
                       (gDnsGlobalInfo.DnsTaskQId, (UINT1 *) &pQMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
                {
                    DnsQueueReqHandler (pQMsg);
                    pQMsg = NULL;
                }

            }
            if (u4Events & DNS_IPV4_PKT_RXED_EVT)
            {

                DnsRcvRespIPvXFromServer (gDnsGlobalInfo.i4ResolverSockFd);
                if (SelAddFd (gDnsGlobalInfo.i4ResolverSockFd, DnsPktInSocket)
                    != OSIX_SUCCESS)
                {
                    DNS_TRACE ((DNS_RESP_TRC | DNS_FAILURE_TRC,
                                " Failed to initiate next DNS IP packet reception \r\n"));
                }

            }
#ifdef IP6_WANTED
            if (u4Events & DNS_IPV6_PKT_RXED_EVT)
            {

                DnsRcvRespIPvXFromServer (gDnsGlobalInfo.i4ResolverSock6Fd);
                if (SelAddFd
                    (gDnsGlobalInfo.i4ResolverSock6Fd,
                     DnsPktInIpv6Socket) != OSIX_SUCCESS)
                {
                    DNS_TRACE ((DNS_RESP_TRC | DNS_FAILURE_TRC,
                                " Failed to initiate next DNS IPv6 packet reception \r\n"));
                }

            }
#endif

        }
        DnsUnLock ();
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsResolverInit      
 *
 *    DESCRIPTION      : This function intialises DNS Resolver module
 *
 *    INPUT            : None.                               
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : DNS_SUCCESS / DNS_FAILURE
 *
 ****************************************************************************/

PUBLIC INT4
DnsResolverInit (UINT1 u1ModStart)
{
    DNS_TRACE ((DNS_INITSHUT_TRC, " Initializing Global Data structures \r\n"));
    if (DnsResolverInitGlobalInfo (u1ModStart) == DNS_FAILURE)
    {
        return DNS_FAILURE;
    }

    DNS_TRACE ((DNS_INITSHUT_TRC, " Initializing Memory Pools \r\n"));
    if (DnsResolverMemInit () == DNS_FAILURE)
    {
        return DNS_FAILURE;
    }

    DNS_TRACE ((DNS_INITSHUT_TRC, " Initializing DNS Socket Information \r\n"));
    if (DnsResolverSockInit () == DNS_FAILURE)
    {
        return DNS_FAILURE;
    }
#ifdef IP6_WANTED
    if (DnsResolverSock6Init () == DNS_FAILURE)
    {
        return DNS_FAILURE;
    }
#endif /* IP6_WANTED */
    DNS_TRACE ((DNS_INITSHUT_TRC, " Initializing DNS Index Manager \r\n"));
    if (DnsResolverIndexMgrInit () == DNS_FAILURE)
    {
        return DNS_FAILURE;
    }

    DNS_TRACE ((DNS_INITSHUT_TRC, " Initializing DNS Cache \r\n"));
    if (DnsResolverCacheInit () == DNS_FAILURE)
    {
        return DNS_FAILURE;
    }
    DNS_TRACE ((DNS_INITSHUT_TRC, " Initializing DNS Timer \r\n"));
    if (DnsTmrInit () != OSIX_SUCCESS)
    {
        return DNS_FAILURE;
    }
    return DNS_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsResolverInitGlobalInfo
 *
 *    DESCRIPTION      : This function intialises DNS global Information
 *
 *    INPUT            : None.                               
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : DNS_SUCCESS / DNS_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
DnsResolverInitGlobalInfo (UINT1 u1ModStart)
{
    UINT1               u1Offset = 0;

    MEMSET (&gDnsResolverParams, 0, sizeof (tDnsResolverParams));
    MEMSET (&gDnsStatistics, 0, sizeof (tDnsStatistics));

    if (!u1ModStart)
    {
        MEMSET (&gDnsGlobalInfo, 0, sizeof (tDnsGlobalInfo));
        OsixTskIdSelf (&(gDnsGlobalInfo.DnsTaskId));
    }
    gDnsGlobalInfo.u1SystemCtrlStatus = DNS_START;
    gDnsGlobalInfo.u1ModuleStatus = DNS_ENABLE;
    gDnsResolverParams.u4QueryRetryCount = DNS_DEFAULT_RETRY_COUNT;
    gDnsResolverParams.u4QueryTimeout = DNS_DEFAULT_TIMEOUT;
    gDnsResolverParams.i4QueryMode = DNS_MODE_SIMUL;
    gDnsResolverParams.i4QueryType = DNS_v4v6_RESOLVE;

    if (!u1ModStart)
    {
        if (OsixSemCrt ((UINT1 *) DNS_SEM_NAME,
                        &(gDnsGlobalInfo.DnsSemId)) == OSIX_FAILURE)
        {
            DNS_TRACE ((DNS_INITSHUT_TRC | DNS_FAILURE_TRC,
                        " Semaphore creation fails \r\n"));
            return DNS_FAILURE;
        }
        if (OsixQueCrt ((UINT1 *) DNS_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                        MAX_DNS_Q_MSG,
                        &(gDnsGlobalInfo.DnsTaskQId)) != OSIX_SUCCESS)
        {
            DNS_TRACE ((DNS_INITSHUT_TRC | DNS_FAILURE_TRC,
                        " Queue Creation Failed \r\n"));

            return DNS_FAILURE;
        }
    }
    DnsUnLock ();

    u1Offset = FSAP_OFFSETOF (tDnsQueryInfo, QueryNode);

    if ((gDnsGlobalInfo.PendingQueryList = RBTreeCreateEmbedded
         (u1Offset, DnsQueryIdCmp)) == NULL)
    {
        DNS_TRACE ((DNS_INITSHUT_TRC | DNS_FAILURE_TRC,
                    " RB-Tree creation of Pending Query List Fails \r\n"));
        return DNS_FAILURE;
    }

    TMO_SLL_Init (&gDnsGlobalInfo.NameServerList);
    TMO_SLL_Init (&gDnsGlobalInfo.DomainNameList);

    return DNS_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsResolverInitGlobalInfo
 *
 *    DESCRIPTION      : This function intialises DNS Index manager lists
 *
 *    INPUT            : None.                               
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : DNS_SUCCESS / DNS_FAILURE
 *
 ****************************************************************************/

PUBLIC INT4
DnsResolverIndexMgrInit (VOID)
{
    MEMSET (&gDnsIndexMgrListInfo, 0, sizeof (tDnsIndexMgrListInfo));

    if ((IndexManagerInitList
         (1, MAX_DNS_INDICES, gDnsIndexMgrListInfo.au1NameServerBitMap,
          INDEX_MGR_NON_INCR_TYPE,
          &gDnsIndexMgrListInfo.NameServerIndexList)) == INDEX_FAILURE)
    {
        DNS_TRACE ((DNS_INITSHUT_TRC | DNS_FAILURE_TRC,
                    " Creation of Name Server Index List Fails \r\n"));
        return DNS_FAILURE;
    }

    if ((IndexManagerInitList
         (1, MAX_DNS_INDICES, gDnsIndexMgrListInfo.au1DomainNameBitMap,
          INDEX_MGR_NON_INCR_TYPE,
          &gDnsIndexMgrListInfo.DomainNameIndexList)) == INDEX_FAILURE)
    {
        DNS_TRACE ((DNS_INITSHUT_TRC | DNS_FAILURE_TRC,
                    " Creation of Domain Name Index List Fails \r\n"));
        return DNS_FAILURE;
    }

    if ((IndexManagerInitList
         (1, MAX_DNS_INDICES, gDnsIndexMgrListInfo.au1QueryBitmap,
          INDEX_MGR_NON_INCR_TYPE,
          &gDnsIndexMgrListInfo.QueryIndexList)) == INDEX_FAILURE)
    {
        DNS_TRACE ((DNS_INITSHUT_TRC | DNS_FAILURE_TRC,
                    " Creation of Pending Query Index List Fails \r\n"));
        return DNS_FAILURE;
    }

    if ((IndexManagerInitList
         (1, MAX_DNS_INDICES, gDnsIndexMgrListInfo.au1CacheBitmap,
          INDEX_MGR_NON_INCR_TYPE,
          &gDnsIndexMgrListInfo.CacheIndexList)) == INDEX_FAILURE)
    {
        DNS_TRACE ((DNS_INITSHUT_TRC | DNS_FAILURE_TRC,
                    " Creation of Cache Record Index List Fails \r\n"));
        return DNS_FAILURE;
    }

    return DNS_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsResolverInitGlobalInfo
 *
 *    DESCRIPTION      : This function intialises DNS Memory Pools
 *
 *    INPUT            : None.                               
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : DNS_SUCCESS / DNS_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
DnsResolverMemInit (VOID)
{

    if (DnsSizingMemCreateMemPools () != OSIX_SUCCESS)
    {
        DNS_TRACE ((DNS_INITSHUT_TRC | DNS_FAILURE_TRC,
                    " MemPool Creation Fails \r\n"));
        return DNS_FAILURE;
    }
    gDnsGlobalInfo.NameServerPoolId =
        DNSMemPoolIds[MAX_DNS_NAME_SERVERS_SIZING_ID];
    gDnsGlobalInfo.DomainNamePoolId = DNSMemPoolIds[MAX_DNS_DOMAINS_SIZING_ID];
    gDnsGlobalInfo.CacheDataPoolId =
        DNSMemPoolIds[MAX_DNS_CACHE_ENTRIES_SIZING_ID];
    gDnsGlobalInfo.QueryRespMemPoolId =
        DNSMemPoolIds[MAX_DNS_PKT_LEN_SIZING_ID];
    gDnsGlobalInfo.QMsgMemPoolId = DNSMemPoolIds[MAX_DNS_Q_MSG_SIZING_ID];
    gDnsGlobalInfo.PendQryMemPoolId = DNSMemPoolIds[MAX_DNS_PEND_QRY_SIZING_ID];
    gDnsGlobalInfo.DnsSrvMemPoolId = DNSMemPoolIds[MAX_DNS_SRV_SIZING_ID];

    return DNS_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsResolverDeInit            
 *
 *    DESCRIPTION      : This function deletes all the memory allocated by 
 *                       DNS Module
 *
 *    INPUT            : None.                               
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : DNS_SUCCESS / DNS_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
DnsResolverDeInit (UINT1 u1modShut)
{
    DNS_TRACE ((DNS_INITSHUT_TRC, "Deleting DNS Mempools \r\n"));
    DnsSizingMemDeleteMemPools ();
    DnsTmrDeInit ();

    DNS_TRACE ((DNS_INITSHUT_TRC, "Deleting DNS Index Manager Lists \r\n"));
    if (gDnsIndexMgrListInfo.NameServerIndexList != 0)
    {
        IndexManagerDeInitList (gDnsIndexMgrListInfo.NameServerIndexList);
    }
    if (gDnsIndexMgrListInfo.DomainNameIndexList != 0)
    {
        IndexManagerDeInitList (gDnsIndexMgrListInfo.DomainNameIndexList);
    }
    if (gDnsIndexMgrListInfo.QueryIndexList != 0)
    {
        IndexManagerDeInitList (gDnsIndexMgrListInfo.QueryIndexList);
    }
    if (gDnsIndexMgrListInfo.CacheIndexList != 0)
    {
        IndexManagerDeInitList (gDnsIndexMgrListInfo.CacheIndexList);
    }

    DNS_TRACE ((DNS_INITSHUT_TRC, "Destroying Pending Query RB Tree \r\n"));
    if (gDnsGlobalInfo.PendingQueryList != NULL)
    {
        RBTreeDestroy (gDnsGlobalInfo.PendingQueryList, NULL, DNS_ZERO);
        gDnsGlobalInfo.PendingQueryList = NULL;
    }

    DNS_TRACE ((DNS_INITSHUT_TRC, "Deleting DNS Module Semaphore \r\n"));
    if (!u1modShut)
    {
        if (gDnsGlobalInfo.DnsSemId != 0)
        {
            OsixSemDel (gDnsGlobalInfo.DnsSemId);
        }
        if (gDnsGlobalInfo.DnsTaskQId != 0)
        {
            OsixQueDel (gDnsGlobalInfo.DnsTaskQId);
        }
    }
    DNS_TRACE ((DNS_INITSHUT_TRC, " Closing DNS Socket \r\n"));
    if (gDnsGlobalInfo.i4ResolverSockFd != 0)
    {
        SelRemoveFd (gDnsGlobalInfo.i4ResolverSockFd);
        close (gDnsGlobalInfo.i4ResolverSockFd);
    }
    if (gDnsGlobalInfo.i4ResolverSock6Fd != 0)
    {
        SelRemoveFd (gDnsGlobalInfo.i4ResolverSock6Fd);
        close (gDnsGlobalInfo.i4ResolverSock6Fd);
    }
    DNS_TRACE ((DNS_INITSHUT_TRC, "Destroying Cache RB Tree \r\n"));
    if (gDnsCacheInfo.CacheList != NULL)
    {
        RBTreeDestroy (gDnsCacheInfo.CacheList, NULL, DNS_ZERO);
    }

    return DNS_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsResolverSockInit         
 *
 *    DESCRIPTION      : This function intialises a DNS Resolver socket
 *
 *    INPUT            : None.                               
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : DNS_SUCCESS / DNS_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
DnsResolverSockInit ()
{
    INT4                i4Sock;
    struct sockaddr_in  SockAddr;

    MEMSET (&SockAddr, 0, sizeof (SockAddr));
    i4Sock = socket (AF_INET, SOCK_DGRAM, 0);

    if (i4Sock < 0)
    {
        DNS_TRACE ((DNS_INITSHUT_TRC | DNS_FAILURE_TRC,
                    " Unable to create Socket for ipv4 address family \r\n"));
        return DNS_FAILURE;
    }

    SockAddr.sin_family = AF_INET;
    SockAddr.sin_port = OSIX_HTONS (DNS_ZERO);
    SockAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);

    if (bind (i4Sock, (struct sockaddr *) &SockAddr, sizeof (SockAddr))
        < DNS_ZERO)
    {
        DNS_TRACE ((DNS_INITSHUT_TRC | DNS_FAILURE_TRC,
                    " Unable to bind DNS Socket \r\n"));
        close (i4Sock);
        return DNS_FAILURE;
    }

    if (fcntl (i4Sock, F_SETFL, O_NONBLOCK) < DNS_ZERO)
    {
        DNS_TRACE ((DNS_INITSHUT_TRC | DNS_FAILURE_TRC,
                    " Unable to set Socket as Non-Blocking \r\n"));
        close (i4Sock);
        return DNS_FAILURE;
    }

    gDnsGlobalInfo.i4ResolverSockFd = i4Sock;
    if (SelAddFd (gDnsGlobalInfo.i4ResolverSockFd,
                  DnsPktInSocket) != OSIX_SUCCESS)
    {
        /* SelAddFd failed for socket */
        return OSIX_FAILURE;
    }

    return DNS_SUCCESS;
}

/****************************************************************************
 **
 **    FUNCTION NAME    : DnsResolverSock6Init
 **
 **    DESCRIPTION      : This function intialises a DNS Resolver socket
 **
 **    INPUT            : None.
 **
 **    OUTPUT           : None
 **
 **    RETURNS          : DNS_SUCCESS / DNS_FAILURE
 **
 *****************************************************************************/
#ifdef IP6_WANTED
PUBLIC INT4
DnsResolverSock6Init ()
{
    INT4                i4Sock6;
    struct sockaddr_in6 SockAddr6;

    MEMSET (&SockAddr6, 0, sizeof (SockAddr6));

    i4Sock6 = socket (AF_INET6, SOCK_DGRAM, 0);
    if (i4Sock6 < 0)
    {
        DNS_TRACE ((DNS_INITSHUT_TRC | DNS_FAILURE_TRC,
                    " Unable to create Socket for ipv6 address family \r\n"));
        return DNS_FAILURE;
    }

    SockAddr6.sin6_family = AF_INET6;
    SockAddr6.sin6_port = OSIX_HTONS (DNS_ZERO);
    inet_pton (AF_INET6, (const CHR1 *) "0::0", &SockAddr6.sin6_addr.s6_addr);

    if (bind (i4Sock6, (struct sockaddr *) &SockAddr6, sizeof (SockAddr6))
        < DNS_ZERO)
    {
        DNS_TRACE ((DNS_INITSHUT_TRC | DNS_FAILURE_TRC,
                    " Unable to bind DNS Socket "
                    "for ipv6 address family \r\n"));
        close (i4Sock6);
        return DNS_FAILURE;
    }

    if (fcntl (i4Sock6, F_SETFL, O_NONBLOCK) < DNS_ZERO)
    {
        DNS_TRACE ((DNS_INITSHUT_TRC | DNS_FAILURE_TRC,
                    " Unable to set Socket as Non-Blocking "
                    "for ipv6 address family\r\n"));
        close (i4Sock6);
        return DNS_FAILURE;
    }

    gDnsGlobalInfo.i4ResolverSock6Fd = i4Sock6;
    if (SelAddFd (gDnsGlobalInfo.i4ResolverSock6Fd,
                  DnsPktInIpv6Socket) != OSIX_SUCCESS)
    {
        /* SelAddFd failed for socket */
        return OSIX_FAILURE;
    }

    return DNS_SUCCESS;
}
#endif /* IP6_WANTED */
/****************************************************************************
 *
 *    FUNCTION NAME    : DnsModuleStart             
 *
 *    DESCRIPTION      : This functios starts the DNS Module                     
 *
 *    INPUT            : None.                               
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : DNS_SUCCESS / DNS_FAILURE
 *
 ****************************************************************************/
INT4
DnsModuleStart (VOID)
{
    UINT1               u1ModStart = OSIX_TRUE;
    UINT1               u1ModShut = OSIX_FALSE;

    if (DnsResolverInit (u1ModStart) != DNS_SUCCESS)
    {
        DnsResolverDeInit (u1ModShut);
    }
    return DNS_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsResolverInitGlobalInfo
 *
 *    DESCRIPTION      : This functios shutdowns the DNS module                 
 *
 *    INPUT            : None.                               
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : DNS_SUCCESS / DNS_FAILURE
 *
 ****************************************************************************/
INT4
DnsModuleShutdown (VOID)
{
    UINT1               u1ModShut = OSIX_TRUE;

    DNS_TRACE ((DNS_INITSHUT_TRC, " Shutting down DNS Module \r\n"));
    gDnsGlobalInfo.u1ModuleStatus = DNS_DISABLE;

    DNS_TRACE ((DNS_INITSHUT_TRC,
                " Freeing Name server and Domain name List nodes \r\n"));
    TMO_SLL_FreeNodes (&gDnsGlobalInfo.NameServerList,
                       gDnsGlobalInfo.NameServerPoolId);
    TMO_SLL_FreeNodes (&gDnsGlobalInfo.DomainNameList,
                       gDnsGlobalInfo.DomainNamePoolId);

    DNS_TRACE ((DNS_INITSHUT_TRC, " Clearing all DNS Cache Records \r\n"));
    DnsCacheClear ();

    if (gDnsGlobalInfo.PendingQueryList != NULL)
    {
        DNS_TRACE ((DNS_INITSHUT_TRC, " Pending Query List nodes \r\n"));
        RBTreeDrain (gDnsGlobalInfo.PendingQueryList, NULL, DNS_ZERO);
    }

    DnsResolverDeInit (u1ModShut);

    return DNS_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsLock                   
 *
 *    DESCRIPTION      : This functios is used to take the DNS Module lock 
 *
 *    INPUT            : None.                               
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : DNS_SUCCESS / DNS_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
DnsLock (VOID)
{

    if (OsixSemTake (gDnsGlobalInfo.DnsSemId) == OSIX_FAILURE)
    {
        DNS_TRACE ((DNS_INITSHUT_TRC | DNS_FAILURE_TRC,
                    " DNS Locking Fails \r\n"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsUnLock                  
 *
 *    DESCRIPTION      : This functios is used to release the DNS Module lock
 *                       DNS Module
 *
 *    INPUT            : None.                               
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : DNS_SUCCESS / DNS_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
DnsUnLock (VOID)
{
    if (OsixSemGive (gDnsGlobalInfo.DnsSemId) == OSIX_FAILURE)
    {
        DNS_TRACE ((DNS_INITSHUT_TRC | DNS_FAILURE_TRC,
                    " DNS UnLocking Fails \r\n"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
#endif /* _DNS_MAIN_C_ */
