/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dnsreq.c,v 1.1 2014/10/13 12:12:29 siva Exp $
 *
 * Description: This file contains action routines for
 *              DNS Request proccessing
 *
 *******************************************************************/

#include "dnsinc.h"

/*****************************************************************************/
/*     FUNCTION NAME    : DnsReqSendQ                                      */
/*                                                                           */
/*     DESCRIPTION      : This function sends Request to Queue             */
/*                                                                           */
/*     INPUT            : pu1QueryName - Query Name.                         */
/*                        u1IsAsync - mode                                 */
/*                        pi4Resolv6Stat - IPv6 resolved status              */
/*                        pi4Resolv4Stat - IPv4 resolved status              */
/*                                                                           */
/*     OUTPUT           : pResolvedIpInfo - Resolved IP                      */
/*                                                                           */
/*     RETURNS          : DNSIP_RESOLVED / DNSIP_NO_RESOLVED/DNSIP_INPROGRESS*/
/*                        DNS_CACHE_FULL                                     */
/*****************************************************************************/

PUBLIC INT4
DnsReqSendQ (UINT1 *pu1QueryName, UINT1 u1IsAsync,
             INT4 *pi4Resolv6Stat, INT4 *pi4Resolv4Stat,
             tDNSResolvedIpInfo * pResolvedIpInfo)
{

    INT4                i4IsPoll = OSIX_TRUE;
    INT4                i4V6Stat = DNSIP_RESOLVED;
    INT4                i4V4Stat = DNSIP_RESOLVED;
    INT4                i4status = DNSIP_NO_RESOLVED;

    if (*pi4Resolv6Stat == DNSIP_NO_RESOLVED)
    {
        i4V6Stat = DNSIP_NO_RESOLVED;
        if ((DnsChkPendQry (DNS_AAAA_RECORD, pu1QueryName)) == OSIX_TRUE)
        {
            i4V6Stat = DNSIP_INPROGRESS;
        }
        else
        {
            if (gDnsCacheInfo.u4TotalEntries == MAX_DNS_CACHE_ENTRIES)
            {
                i4V6Stat = DNS_CACHE_FULL;
            }
        }
    }

    if (*pi4Resolv4Stat == DNSIP_NO_RESOLVED)
    {
        i4V4Stat = DNSIP_NO_RESOLVED;
        if (DnsChkPendQry (DNS_HOST_ADDRESS, pu1QueryName) == OSIX_TRUE)
        {
            i4V4Stat = DNSIP_INPROGRESS;
        }
        else
        {
            if (gDnsCacheInfo.u4TotalEntries == MAX_DNS_CACHE_ENTRIES)
            {
                i4V4Stat = DNS_CACHE_FULL;
            }

        }
    }

    if ((i4V6Stat == DNS_CACHE_FULL) || (i4V4Stat == DNS_CACHE_FULL))
    {
        return DNS_CACHE_FULL;
    }

    if (i4V6Stat == DNSIP_NO_RESOLVED)
    {
        DnsQueMsg (pu1QueryName, DNS_AAAA_RECORD);
        *pi4Resolv6Stat = DNSIP_INPROGRESS;
        i4V6Stat = DNSIP_INPROGRESS;
    }

    if (i4V4Stat == DNSIP_NO_RESOLVED)
    {
        DnsQueMsg (pu1QueryName, DNS_HOST_ADDRESS);
        *pi4Resolv4Stat = DNSIP_INPROGRESS;
        i4V4Stat = DNSIP_INPROGRESS;
    }

    if (!u1IsAsync)
    {
        do
        {
            DnsUnLock ();
            OsixTskDelay (10);
            DnsLock ();
            if (i4V6Stat == DNSIP_INPROGRESS)
            {
                DnsChkCache (DNS_INTERNET_CLASS,
                             DNS_AAAA_RECORD, pu1QueryName, i4IsPoll,
                             pi4Resolv6Stat, pResolvedIpInfo);
                if (*pi4Resolv6Stat == DNSIP_NO_RESOLVED)
                {
                    if (DnsChkPendQry (DNS_AAAA_RECORD, pu1QueryName) ==
                        OSIX_TRUE)
                    {
                        *pi4Resolv6Stat = DNSIP_INPROGRESS;
                    }
                    if (*pi4Resolv6Stat == DNSIP_NO_RESOLVED)
                    {
                        i4V6Stat = DNSIP_NO_RESOLVED;
                    }
                }
				else 
				{
					i4V6Stat = DNSIP_RESOLVED;

				}
            }
            if (i4V4Stat == DNSIP_INPROGRESS)
            {
                DnsChkCache (DNS_INTERNET_CLASS,
                             DNS_HOST_ADDRESS, pu1QueryName, i4IsPoll,
                             pi4Resolv4Stat, pResolvedIpInfo);
                if (*pi4Resolv4Stat == DNSIP_NO_RESOLVED)
                {
                    if (DnsChkPendQry (DNS_HOST_ADDRESS, pu1QueryName) ==
                        OSIX_TRUE)
                    {
                        *pi4Resolv4Stat = DNSIP_INPROGRESS;
                    }
                    if (*pi4Resolv4Stat == DNSIP_NO_RESOLVED)
                    {
                        i4V4Stat = DNSIP_NO_RESOLVED;
                    }
                }
				else
				{
					i4V4Stat = DNSIP_RESOLVED;
				}
            }
            if ((*pi4Resolv6Stat == DNSIP_RESOLVED)
                && (*pi4Resolv4Stat == DNSIP_RESOLVED))
            {
                return DNSIP_RESOLVED;
            }
        }
        while ((i4V6Stat == DNSIP_INPROGRESS)
               || (i4V4Stat == DNSIP_INPROGRESS));
    }
    if (gDnsResolverParams.i4QueryType == DNS_v4v6_RESOLVE)
    {
        if ((*pi4Resolv6Stat == DNSIP_RESOLVED)
            || (*pi4Resolv4Stat == DNSIP_RESOLVED))
        {
            i4status = DNSIP_RESOLVED;
        }
        else if ((*pi4Resolv6Stat == DNSIP_INPROGRESS)
                 || (*pi4Resolv4Stat == DNSIP_INPROGRESS))
        {
            i4status = DNSIP_INPROGRESS;
        }
        else
        {
            i4status = DNSIP_NO_RESOLVED;
        }

    }
    else
    {
        if ((*pi4Resolv6Stat == DNSIP_INPROGRESS)
            || (*pi4Resolv4Stat == DNSIP_INPROGRESS))
        {
            i4status = DNSIP_INPROGRESS;
        }
        else if ((*pi4Resolv6Stat == DNSIP_NO_RESOLVED)
                 || (*pi4Resolv4Stat == DNSIP_NO_RESOLVED))
        {
            i4status = DNSIP_NO_RESOLVED;
        }
    }
    return i4status;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsQueMsg                                      */
/*                                                                           */
/*     DESCRIPTION      : This function sends msg to Queue             */
/*                                                                           */
/*     INPUT            : pu1QueryName - Query Name.                         */
/*                        u4QueryType - Query Type                                 */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : DNS Cache Resource Record                          */
/*****************************************************************************/

PUBLIC INT4
DnsQueMsg (UINT1 *pu1QueryName, UINT4 u4QueryType)
{
    tDnsQMsg           *pQMsg = NULL;
    UINT4               u4Len = 0;

    /* Allocating MEM Block for the Message */
    if ((pQMsg =
         (tDnsQMsg *) MemAllocMemBlk (gDnsGlobalInfo.QMsgMemPoolId)) == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC,
                    "DnsQueMsg: Memory Allocation Failure. \r\n"));
        return OSIX_FALSE;
    }
    MEMSET (pQMsg, 0, sizeof (tDnsQMsg));

    u4Len = MEM_MAX_BYTES ((STRLEN (pu1QueryName)),
                           (sizeof (pQMsg->au1QueryName) - 1));
    MEMCPY (pQMsg->au1QueryName, pu1QueryName, u4Len);
    pQMsg->au1QueryName[u4Len] = '\0';

    pQMsg->u4QueryType = u4QueryType;
    if (OsixQueSend (DNS_QUEUE_ID, (UINT1 *) (&pQMsg),
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "DnsQueMsg: DNS Enque Failed. \r\n"));
        return OSIX_FAILURE;
    }

    /* Sending Event */
    OsixEvtSend (DNS_TASK_ID, DNS_APP_REQ_EVT);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsQueueReqHandler
 *
 *    DESCRIPTION      : This functions add Request 
 *                        PEND Query 
 *
 *    INPUT            : pQMsg - Message information received.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC VOID
DnsQueueReqHandler (tDnsQMsg * pQMsg)
{
    tDnsQueryInfo      *pDnsQueryInfo = NULL;

    if (DnsChkPendQry (pQMsg->u4QueryType, pQMsg->au1QueryName) == OSIX_FALSE)
    {
        /* Allocating MEM Block for the Message */
        if ((pDnsQueryInfo =
             (tDnsQueryInfo *) MemAllocMemBlk (gDnsGlobalInfo.
                                               PendQryMemPoolId)) == NULL)
        {
            DNS_TRACE ((DNS_FAILURE_TRC,
                        "DnsQueMsg: Memory Allocation Failure. \r\n"));
            return;
        }
    MEMSET (pDnsQueryInfo, 0, sizeof (tDnsQueryInfo));

        DnsFillQueryIndex
            (pDnsQueryInfo, DNS_INTERNET_CLASS, pQMsg->u4QueryType,
             pQMsg->au1QueryName);
        DnsReqSendQuery (pDnsQueryInfo);
    }

    MemReleaseMemBlock (gDnsGlobalInfo.QMsgMemPoolId, (UINT1 *) pQMsg);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsChkPendQry
 *
 *    DESCRIPTION      : This functions Check Pending query list for request
 *
 *    INPUT            : QueryName  - Host name
 *                       QueryType  - Type of Query
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_FALSE / OSIX_TRUE
 *
 ****************************************************************************/
PUBLIC INT4
DnsChkPendQry (UINT4 QryType, UINT1 *pu1ReqQryName)
{
    tDnsQueryInfo       DnsQueryInfo;
    tDnsQueryInfo      *pDnsQueryInfo = NULL;

    MEMSET (&DnsQueryInfo, 0, sizeof (tDnsQueryInfo));
    pDnsQueryInfo = RBTreeGetFirst (gDnsGlobalInfo.PendingQueryList);

    do
    {
        if (pDnsQueryInfo == NULL)
        {
            DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Pending Query Index \r\n"));
            return OSIX_FALSE;
        }

        if (STRLEN (pu1ReqQryName) == STRLEN (pDnsQueryInfo->au1QueryName))
        {
            if ((STRNCMP
                 (pu1ReqQryName, pDnsQueryInfo->au1QueryName,
                  STRLEN (pDnsQueryInfo->au1QueryName))) == 0)
            {
                if (pDnsQueryInfo->u4QueryType == QryType)
                    return OSIX_TRUE;
            }
        }
        DnsQueryInfo.u2QueryIndex = pDnsQueryInfo->u2QueryIndex;
    }
    while ((pDnsQueryInfo =
            (RBTreeGetNext
             (gDnsGlobalInfo.PendingQueryList, &DnsQueryInfo, NULL))) != NULL);

    return OSIX_FALSE;
}

/******************************************************************************
 * Function Name   : DnsPktInSocket
 * Description     : Call back function from SelAddFd(), when a DNS IPv4 packet
 *                   is received
 * Inputs          : i4SockFd
 * Output          : Sends an event to DNS task
 * Returns         : None.
 ******************************************************************************/
PUBLIC VOID
DnsPktInSocket (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);

    OsixEvtSend (gDnsGlobalInfo.DnsTaskId, DNS_IPV4_PKT_RXED_EVT);

    return;
}

/******************************************************************************
 * Function Name   : DnsPktInIpv6Socket
 * Description     : Call back function from SelAddFd(), when DNS IPv6 packet
 *                   is received
 * Global Varibles : gDnsTaskId
 * Inputs          : i4SockFd
 * Output          : Sends an event to DNS task
 * Returns         : None.
 ******************************************************************************/
PUBLIC VOID
DnsPktInIpv6Socket (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);

    OsixEvtSend (gDnsGlobalInfo.DnsTaskId, DNS_IPV6_PKT_RXED_EVT);

    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsDeleteQueryData                                 */
/*                                                                           */
/*     DESCRIPTION      : This function Deletes a DNS Query           */
/*                                                                           */
/*     INPUT            : pDnsQueryInfo                     */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : None.                                              */
/*****************************************************************************/

PUBLIC VOID
DnsDeleteQueryData (tDnsQueryInfo * pDnsQueryInfo)
{
    if (pDnsQueryInfo->u2QueryIndex > 0)
    {
        IndexManagerFreeIndex (gDnsIndexMgrListInfo.QueryIndexList,
                               (UINT4) pDnsQueryInfo->u2QueryIndex);

    }

    DnsTmrStopTimer (&pDnsQueryInfo->QryNodeTmrNode);
    RBTreeRem (gDnsGlobalInfo.PendingQueryList, (tRBElem *) pDnsQueryInfo);
    MemReleaseMemBlock (gDnsGlobalInfo.PendQryMemPoolId,
                        (UINT1 *) pDnsQueryInfo);
    DNS_TRACE ((DNS_QUERY_TRC, "Successfully Deleted the query Record \r\n"));
}
