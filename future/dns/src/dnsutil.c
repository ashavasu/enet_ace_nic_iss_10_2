/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dnsutil.c,v 1.12 2017/12/19 10:00:03 siva Exp $
 *
 * Description: This file contains utility functions used by
 *              DNS Module
 *
 *******************************************************************/

#include "dnsinc.h"

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsFillNameServerIP 
 *
 *    DESCRIPTION      : This function adds Name server in the Dns Query 
 *
 *    INPUT            : pDnsQueryInfo - pointer to DNS Query structure.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : DNS_SUCCESS / DNS_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
DnsFillNameServerIP (tDnsQueryInfo * pDnsQueryInfo)
{
    tDnsNameServerInfo *pDnsNameServerInfo = NULL;
    INT4                i4Return = DNS_SUCCESS;

    MEMSET (pDnsQueryInfo->au1ResolveName, 0, (DNS_MAX_RESOLVER_LEN + 1));
    STRCPY (pDnsQueryInfo->au1ResolveName, pDnsQueryInfo->au1QueryName);

    /* First for a name server try to resolve with various domain name combination */
    if ((DnsCountDotsFromName (pDnsQueryInfo->au1ResolveName) <
         DNS_MIN_QUERY_DOTS)
        && (TMO_SLL_Count (&gDnsGlobalInfo.DomainNameList) != 0))
    {
        i4Return = DnsAddDomainToQuery (pDnsQueryInfo);
        if (i4Return == DNS_FAILURE)
        {
            return DNS_FAILURE;
        }

        if (pDnsQueryInfo->u4DomainCount != 1)
        {
            return DNS_SUCCESS;
        }
    }

    pDnsQueryInfo->u4NameServerCount++;
    pDnsNameServerInfo =
        (tDnsNameServerInfo *) TMO_SLL_Nth (&gDnsGlobalInfo.NameServerList,
                                            pDnsQueryInfo->u4NameServerCount);

    if (pDnsNameServerInfo == NULL)
    {
        /* Retrying again from the first Name server */
        pDnsQueryInfo->u4CurRetryCount++;
        pDnsNameServerInfo =
            (tDnsNameServerInfo *)
            TMO_SLL_First (&gDnsGlobalInfo.NameServerList);

        pDnsQueryInfo->u4NameServerCount = 1;
        if (pDnsNameServerInfo == NULL)
        {
            return DNS_FAILURE;
        }
    }

    DNS_TRACE ((DNS_QUERY_TRC, " Adding Name Server IP to the Query \r\n"));
    MEMCPY (&pDnsQueryInfo->QueryNameServerIP,
            &pDnsNameServerInfo->NameServerIP, sizeof (tIPvXAddr));
    return i4Return;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsAddDomainToQuery 
 *
 *    DESCRIPTION      : This function adds Domain name in the Dns Query 
 *
 *    INPUT            : pDnsQueryInfo - pointer to DNS Query structure.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : DNS_SUCCESS / DNS_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
DnsAddDomainToQuery (tDnsQueryInfo * pDnsQueryInfo)
{
    tDnsDomainNameInfo *pDnsDomainNameInfo = NULL;

    pDnsQueryInfo->u4DomainCount++;
    pDnsDomainNameInfo =
        (tDnsDomainNameInfo *) TMO_SLL_Nth (&gDnsGlobalInfo.DomainNameList,
                                            pDnsQueryInfo->u4DomainCount);
    if (pDnsDomainNameInfo == NULL)
    {
        pDnsQueryInfo->u4DomainCount = 0;
        return DNS_FAILURE;
    }

    DNS_TRACE ((DNS_QUERY_TRC, "Adding Domain name to the DNS Query \r\n"));
    if ((STRLEN (pDnsQueryInfo->au1ResolveName) +
         STRLEN (pDnsDomainNameInfo->au1DomainName) + 1) >
        (DNS_MAX_RESOLVER_LEN + 1))
    {
        DNS_TRACE ((DNS_QUERY_TRC,
                    "Cannot add Domain name to the DNS Query as length exceeds the limit\r\n"));
        return DNS_SKIP;
    }
    STRNCAT (pDnsQueryInfo->au1ResolveName, ".",
             MEM_MAX_BYTES (STRLEN ("."),
                            sizeof (pDnsQueryInfo->au1ResolveName) - 1 -
                            STRLEN (pDnsQueryInfo->au1ResolveName)));
    STRNCAT (pDnsQueryInfo->au1ResolveName, pDnsDomainNameInfo->au1DomainName,
             MEM_MAX_BYTES (STRLEN (pDnsDomainNameInfo->au1DomainName),
                            sizeof (pDnsQueryInfo->au1ResolveName) - 1 -
                            STRLEN (pDnsQueryInfo->au1ResolveName)));
    return DNS_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsAddNameServertoList
 *
 *    DESCRIPTION      : This function adds a Name server entry to the  
 *                       Name server table.
 *
 *    INPUT            : pDnsNameServerInfo - pointer to name server structure
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : DNS_SUCCESS / DNS_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
DnsAddNameServertoList (tDnsNameServerInfo * pDnsNameServerInfo)
{
    tDnsNameServerInfo *pPrevNSNode = NULL;
    tDnsNameServerInfo *pNextNSNode = NULL;

    TMO_SLL_Scan (&gDnsGlobalInfo.NameServerList, pNextNSNode,
                  tDnsNameServerInfo *)
    {
        if (pDnsNameServerInfo->u4NameServerIndex ==
            pNextNSNode->u4NameServerIndex)
        {
            return DNS_FAILURE;
        }
        else if (pDnsNameServerInfo->u4NameServerIndex <
                 pNextNSNode->u4NameServerIndex)
        {
            break;
        }
        pPrevNSNode = pNextNSNode;
    }

    DNS_TRACE ((DNS_CTRL_TRC, "Added Name server node to the List \r\n"));
    TMO_SLL_Insert (&gDnsGlobalInfo.NameServerList,
                    &(pPrevNSNode->NameServerNode),
                    (tTMO_SLL_NODE *) (VOID *) pDnsNameServerInfo);
    return DNS_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsAddNameServertoList
 *
 *    DESCRIPTION      : This function adds a domain name entry to the  
 *                       Domain name table.
 *
 *    INPUT            : pDnsDomainNameInfo - pointer to domain name structure
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : DNS_SUCCESS / DNS_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
DnsAddDomainNametoList (tDnsDomainNameInfo * pDnsDomainNameInfo)
{
    tDnsDomainNameInfo *pPrevDomainNode = NULL;
    tDnsDomainNameInfo *pNextDomainNode = NULL;

    TMO_SLL_Scan (&gDnsGlobalInfo.DomainNameList, pNextDomainNode,
                  tDnsDomainNameInfo *)
    {
        if (pDnsDomainNameInfo->u4DomainNameIndex ==
            pNextDomainNode->u4DomainNameIndex)
        {
            return DNS_FAILURE;
        }
        else if (pDnsDomainNameInfo->u4DomainNameIndex <
                 pNextDomainNode->u4DomainNameIndex)
        {
            break;
        }
        pPrevDomainNode = pNextDomainNode;
    }

    DNS_TRACE ((DNS_CTRL_TRC, "Added Domain name node to the List \r\n"));

    TMO_SLL_Insert (&gDnsGlobalInfo.DomainNameList,
                    &(pPrevDomainNode->DomainNameNode),
                    (tTMO_SLL_NODE *) (VOID *) (pDnsDomainNameInfo));
    return DNS_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsQueryIdCmp            
 *
 *    DESCRIPTION      : Query RB Tree Comparison function.              
 *
 *    INPUT            : pRBElem1, pRBElem2 - RB node elements                
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : -1,0,1
 *
 ****************************************************************************/
PUBLIC INT4
DnsQueryIdCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT2               u2QueryIndex1 =
        ((tDnsQueryInfo *) pRBElem1)->u2QueryIndex;
    UINT2               u2QueryIndex2 =
        ((tDnsQueryInfo *) pRBElem2)->u2QueryIndex;

    if (u2QueryIndex1 < u2QueryIndex2)
    {
        return -1;
    }
    else if (u2QueryIndex1 > u2QueryIndex2)
    {
        return 1;
    }
    return 0;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsCacheListCmp          
 *
 *    DESCRIPTION      : Cache RB Tree Comparison function.              
 *
 *    INPUT            : pRBElem1, pRBElem2 - RB node elements                
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : -1,0,1
 *
 ****************************************************************************/
PUBLIC INT4
DnsCacheListCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDnsCacheRRInfo    *pDnsCacheRRInfo1 = (tDnsCacheRRInfo *) pRBElem1;
    tDnsCacheRRInfo    *pDnsCacheRRInfo2 = (tDnsCacheRRInfo *) pRBElem2;

    if (STRCMP (pDnsCacheRRInfo1->au1RRName, pDnsCacheRRInfo2->au1RRName) < 0)
    {
        return -1;
    }
    else if (STRCMP (pDnsCacheRRInfo1->au1RRName, pDnsCacheRRInfo2->au1RRName) >
             0)
    {
        return 1;
    }

    if (pDnsCacheRRInfo1->u4RRClass < pDnsCacheRRInfo2->u4RRClass)
    {
        return -1;
    }
    else if (pDnsCacheRRInfo1->u4RRClass > pDnsCacheRRInfo2->u4RRClass)
    {
        return 1;
    }

    if (pDnsCacheRRInfo1->u4RRType < pDnsCacheRRInfo2->u4RRType)
    {
        return -1;
    }
    else if (pDnsCacheRRInfo1->u4RRType > pDnsCacheRRInfo2->u4RRType)
    {
        return 1;
    }

    if (pDnsCacheRRInfo1->u4RRIndex < pDnsCacheRRInfo2->u4RRIndex)
    {
        return -1;
    }
    else if (pDnsCacheRRInfo1->u4RRIndex > pDnsCacheRRInfo2->u4RRIndex)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsGetNameServer         
 *
 *    DESCRIPTION      : This function retreives a Name Server from a 
 *                       Name server index.
 *
 *    INPUT            : u4NameServerIndex - Name server Index                
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : pointer to name server structure
 *
 ****************************************************************************/
PUBLIC tDnsNameServerInfo *
DnsGetNameServer (UINT4 u4NameServerIndex)
{
    tDnsNameServerInfo *pDnsNameServerInfo = NULL;

    TMO_SLL_Scan (&(gDnsGlobalInfo.NameServerList), pDnsNameServerInfo,
                  tDnsNameServerInfo *)
    {
        if (pDnsNameServerInfo->u4NameServerIndex == u4NameServerIndex)
        {
            return pDnsNameServerInfo;
        }
    }

    return NULL;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsDelNameServerFromList 
 *
 *    DESCRIPTION      : This function deletes a Name Server entry from a 
 *                       Name server table.
 *
 *    INPUT            : pDnsNameServerInfo - pointer to name server structure
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None.                             
 *
 ****************************************************************************/
PUBLIC VOID
DnsDelNameServerFromList (tDnsNameServerInfo * pDnsNameServerInfo)
{
    if (pDnsNameServerInfo->u4NameServerIndex > DNS_DEF_NAME_SERVER_INDEX)
    {
        IndexManagerFreeIndex (gDnsIndexMgrListInfo.NameServerIndexList,
                               pDnsNameServerInfo->u4NameServerIndex);
    }
    TMO_SLL_Delete (&gDnsGlobalInfo.NameServerList,
                    &pDnsNameServerInfo->NameServerNode);
    MemReleaseMemBlock (gDnsGlobalInfo.NameServerPoolId,
                        (UINT1 *) pDnsNameServerInfo);
    DNS_TRACE ((DNS_CTRL_TRC, "Deleted the Name server from the list \r\n"));
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsGetServerIndex        
 *
 *    DESCRIPTION      : This function retreives a Name Server Index from a 
 *                       IP Address
 *
 *    INPUT            : u4Family - Family   pu1IPAddr - IP Address           
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : Name server Index / DNS_ZERO
 *
 ****************************************************************************/
PUBLIC INT4
DnsGetServerIndex (UINT4 u4Family, UINT1 *pu1IPAddr)
{
    tDnsNameServerInfo *pDnsNameServerInfo = NULL;

    TMO_SLL_Scan (&(gDnsGlobalInfo.NameServerList), pDnsNameServerInfo,
                  tDnsNameServerInfo *)
    {
        if ((pDnsNameServerInfo->NameServerIP.u1Afi == u4Family) &&
            (MEMCMP (pDnsNameServerInfo->NameServerIP.au1Addr, pu1IPAddr,
                     pDnsNameServerInfo->NameServerIP.u1AddrLen) == DNS_ZERO))
        {
            return ((INT4) pDnsNameServerInfo->u4NameServerIndex);
        }
    }
    return DNS_ZERO;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsGetDomainName         
 *
 *    DESCRIPTION      : This function retreives a Domain name from a 
 *                       Domain name index.
 *
 *    INPUT            : u4DomainNameIndex - domain name Index                
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : pointer to domain name structure
 *
 ****************************************************************************/
PUBLIC tDnsDomainNameInfo *
DnsGetDomainName (UINT4 u4DomainNameIndex)
{
    tDnsDomainNameInfo *pDnsDomainNameInfo = NULL;

    TMO_SLL_Scan (&(gDnsGlobalInfo.DomainNameList), pDnsDomainNameInfo,
                  tDnsDomainNameInfo *)
    {
        if (pDnsDomainNameInfo->u4DomainNameIndex == u4DomainNameIndex)
        {
            return pDnsDomainNameInfo;
        }
    }

    return NULL;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsDelNameServerFromList 
 *
 *    DESCRIPTION      : This function deletes a domain name entry from a 
 *                       domain name table.
 *
 *    INPUT            : pDnsNameServerInfo - pointer to name server structure
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None.                             
 *
 ****************************************************************************/
PUBLIC VOID
DnsDelDomainNameFromList (tDnsDomainNameInfo * pDnsDomainNameInfo)
{
    if (pDnsDomainNameInfo->u4DomainNameIndex > DNS_DEF_DOMAIN_NAME_INDEX)
    {
        IndexManagerFreeIndex (gDnsIndexMgrListInfo.DomainNameIndexList,
                               pDnsDomainNameInfo->u4DomainNameIndex);
    }
    TMO_SLL_Delete (&gDnsGlobalInfo.DomainNameList,
                    &pDnsDomainNameInfo->DomainNameNode);
    MemReleaseMemBlock (gDnsGlobalInfo.DomainNamePoolId,
                        (UINT1 *) pDnsDomainNameInfo);
    DNS_TRACE ((DNS_CTRL_TRC, "Deleted the Domain name from the list \r\n"));
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsGetDomainNameIndex    
 *
 *    DESCRIPTION      : This function retreives a domain name index from the
 *                       domain name.
 *
 *    INPUT            : pu1DomainName - Domain name                          
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : Domain name Index / DNS_ZERO     
 *
 ****************************************************************************/
PUBLIC INT4
DnsGetDomainNameIndex (UINT1 *pu1DomainName)
{
    tDnsDomainNameInfo *pDnsDomainNameInfo = NULL;

    TMO_SLL_Scan (&(gDnsGlobalInfo.DomainNameList), pDnsDomainNameInfo,
                  tDnsDomainNameInfo *)
    {
        if (STRNCMP (pDnsDomainNameInfo->au1DomainName, pu1DomainName,
                     DNS_MAX_DOMAIN_NAME_LEN) == DNS_ZERO)
        {
            return ((INT4) pDnsDomainNameInfo->u4DomainNameIndex);
        }
    }
    return DNS_ZERO;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsCountDotsFromName      
 *
 *    DESCRIPTION      : This function is used to count the dots in the  
 *                       input domain name.                                  
 *
 *    INPUT            : pu1Name - Domain name.                               
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : u4DotCount - number of dots in domain name
 *
 ****************************************************************************/
PUBLIC UINT4
DnsCountDotsFromName (UINT1 *pu1Name)
{
    UINT4               u4Index, u4DotCount = 0;

    for (u4Index = 0; pu1Name[u4Index] != '\0'; u4Index++)
    {
        if (pu1Name[u4Index] == '.')
        {
            u4DotCount++;
        }
    }
    return u4DotCount;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsDuplicateNSIP          
 *
 *    DESCRIPTION      : This function is used to verify whether the input 
 *                       Name server IP matches any other name server entry
 *
 *    INPUT            : pu1IPAddr - Name server IP address                   
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : DNS_SUCCESS / DNS_FAILURE        
 *
 ****************************************************************************/
PUBLIC UINT4
DnsDuplicateNSIP (UINT1 *pu1IPAddr)
{
    tDnsNameServerInfo *pDnsNameServerInfo = NULL;

    TMO_SLL_Scan (&(gDnsGlobalInfo.NameServerList), pDnsNameServerInfo,
                  tDnsNameServerInfo *)
    {
        if ((MEMCMP (pDnsNameServerInfo->NameServerIP.au1Addr, pu1IPAddr,
                     IPVX_IPV6_ADDR_LEN) == DNS_ZERO))
        {
            return DNS_FAILURE;
        }
    }
    return DNS_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsDupDomainName          
 *
 *    DESCRIPTION      : This function is used to verify whether the input domain
 *                       name matches any other domain name entry.
 *
 *    INPUT            : pu1DomainName - Domain name                          
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : DNS_SUCCESS / DNS_FAILURE        
 *
 ****************************************************************************/
PUBLIC UINT4
DnsDupDomainName (UINT1 *pu1DomainName)
{
    tDnsDomainNameInfo *pDnsDomainNameInfo = NULL;

    TMO_SLL_Scan (&(gDnsGlobalInfo.DomainNameList), pDnsDomainNameInfo,
                  tDnsDomainNameInfo *)
    {
        if (STRCMP (pDnsDomainNameInfo->au1DomainName, pu1DomainName) ==
            DNS_ZERO)
        {
            return DNS_FAILURE;
        }
    }
    return DNS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DnsTracePrint                                        */
/* Description        : This Functions Prints the trace string along with    */
/*                      Filename and the line num                            */
/* Input(s)           : fname   - File name                                  */
/*                      u4Line  - Line no                                    */
/*                      s       - string to be printed                       */
/* Output(s)          : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
DnsTracePrint (const CHR1 * fname, UINT4 u4Line, CHR1 * s)
{

    tOsixSysTime        sysTime;
    const CHR1         *pc1Fname = fname;
    CHR1                ai1LogMsgBuf[UTL_MAX_LOG_LEN];

    if (s == NULL)
    {
        return;
    }

    while (*fname != '\0')
    {
        if (*fname == '/')
        {
            pc1Fname = (fname + 1);
        }
        fname++;
    }

    OsixGetSysTime (&sysTime);
    SNPRINTF (ai1LogMsgBuf, sizeof (ai1LogMsgBuf), "DNS: %d:%s:%d:   %s",
              sysTime, pc1Fname, u4Line, s);
    UtlTrcPrint ((const char *) ai1LogMsgBuf);
}

/*****************************************************************************/
/* Function Name      : DnsTrace                                             */
/* Description        : Converts the Variable Argument to a String depending */
/*                      on the Current Trace flag                            */
/* Input(s)           : u4Flags - Trace Flag                                 */
/*                      pc1fmt  - Format String                              */
/* Output(s)          : None.                                                */
/* Return Value(s)    : Returns a String                                     */
/*****************************************************************************/
CHR1               *
DnsTrace (UINT4 u4Flags, const CHR1 * pc1fmt, ...)
{
    va_list             ap;
#define DNS_TRACE_BUF_SIZE 2000
    static CHR1         ac1buf[DNS_TRACE_BUF_SIZE];

    if ((u4Flags & DNS_TRACE_FLAG) == 0)
    {
        return (NULL);
    }
    va_start (ap, pc1fmt);
    vsprintf (&ac1buf[0], pc1fmt, ap);
    va_end (ap);

    return (&ac1buf[0]);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsGetNameServerFromAdd
 *
 *    DESCRIPTION      : This function retreives a Name Server from a
 *                       Name server IP.
 *
 *    INPUT            : tIPvXAddr
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : pointer to name server structure
 *
 ****************************************************************************/

PUBLIC UINT4
DnsGetNameServerFromAdd (tIPvXAddr * pSrcAddr)
{
    tDnsNameServerInfo *pDnsNameServerInfo = NULL;
    UINT4               u4NsPosition = 0;

    TMO_SLL_Scan (&(gDnsGlobalInfo.NameServerList), pDnsNameServerInfo,
                  tDnsNameServerInfo *)
    {
        if (IPVX_ADDR_COMPARE (pDnsNameServerInfo->NameServerIP, (*pSrcAddr)) ==
            0)
        {
            u4NsPosition =
                TMO_SLL_Find (&gDnsGlobalInfo.NameServerList,
                              &pDnsNameServerInfo->NameServerNode);
            return u4NsPosition;
        }
    }

    return u4NsPosition;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsCreateNameServer                                */
/*                                                                           */
/*     DESCRIPTION      : This function adds a new entry to Name server      */
/*                        Table                                              */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Index - Name Server Index                        */
/*                        u4Family - Name Server Address Family              */
/*                        pu1IPAddr - Name Server IP                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : DNS_SUCCESS/DNS_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
DnsCreateNameServer (UINT4 u4Index, UINT4 u4Family, UINT1 *pu1IPAddr)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE NSIPAddr;

    MEMSET (&NSIPAddr, DNS_ZERO, sizeof (NSIPAddr));

    if (u4Family == IPVX_ADDR_FMLY_IPV4)
    {
        NSIPAddr.pu1_OctetList = pu1IPAddr;
        NSIPAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    }
    else if (u4Family == IPVX_ADDR_FMLY_IPV6)
    {
        NSIPAddr.pu1_OctetList = pu1IPAddr;
        NSIPAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
    }

    /* If we are adding a non-default name server, we get the index 
     * from the Index manager */
    if (u4Index == DNS_ZERO)
    {
        if (IndexManagerGetNextFreeIndex
            (gDnsIndexMgrListInfo.NameServerIndexList,
             &u4Index) == INDEX_FAILURE)
        {
            return DNS_FAILURE;
        }
    }
    if (nmhTestv2FsDnsNameServerRowStatus
        (&u4ErrorCode, u4Index, CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return DNS_FAILURE;
    }
    if (nmhSetFsDnsNameServerRowStatus (u4Index, CREATE_AND_WAIT) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (DNS_CLI_NO_MULTIPLE_NAME_SERVERS);
        return DNS_FAILURE;
    }

    if (nmhTestv2FsDnsServerIPAddressType
        (&u4ErrorCode, u4Index, (INT4) u4Family) == SNMP_FAILURE)
    {
        nmhSetFsDnsNameServerRowStatus (u4Index, DESTROY);
        return DNS_FAILURE;
    }

    if (nmhSetFsDnsServerIPAddressType (u4Index, (INT4) u4Family) ==
        SNMP_FAILURE)
    {
        nmhSetFsDnsNameServerRowStatus (u4Index, DESTROY);
        return DNS_FAILURE;
    }

    if (nmhTestv2FsDnsServerIPAddress (&u4ErrorCode, u4Index, &NSIPAddr) ==
        SNMP_FAILURE)
    {
        nmhSetFsDnsNameServerRowStatus (u4Index, DESTROY);
        return DNS_FAILURE;
    }
    if (nmhSetFsDnsServerIPAddress (u4Index, &NSIPAddr) == SNMP_FAILURE)
    {
        nmhSetFsDnsNameServerRowStatus (u4Index, DESTROY);
        return DNS_FAILURE;
    }
    if (nmhTestv2FsDnsNameServerRowStatus (&u4ErrorCode, u4Index, ACTIVE) ==
        SNMP_FAILURE)
    {
        nmhSetFsDnsNameServerRowStatus (u4Index, DESTROY);
        return DNS_FAILURE;
    }
    if (nmhSetFsDnsNameServerRowStatus (u4Index, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsDnsNameServerRowStatus (u4Index, DESTROY);
        return DNS_FAILURE;
    }
    return DNS_SUCCESS;
}
