/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dnsapi.c,v 1.9 2017/12/19 10:00:03 siva Exp $
 *
 * Description: This file contains DNS Functions used by 
 *              other modules.
 *
 *******************************************************************/

#include "dnsinc.h"

/*****************************************************************************/
/*     FUNCTION NAME    : DNSResolveIPFromName                               */
/*                                                                           */
/*     DESCRIPTION      : This function is used to resolve the IP Address    */
/*                        from the English name                              */
/*                                                                           */
/*     INPUT            : pu1QueryName - Name to be resolved                 */
/*                                                                           */
/*     OUTPUT           : pResolvedIP - Resolved IP                          */
/*                        pu4Error - Error Code                              */
/*                                                                           */
/*     RETURNS          : DNS_SUCCESS / DNS_FAILURE                          */
/*****************************************************************************/

PUBLIC INT4
DNSResolveIPFromName (UINT1 *pu1QueryName, tIPvXAddr * pResolvedIP,
                      UINT4 *pu4Error)
{

    tDNSResolvedIpInfo  ResolvedIpInfo;
    INT4                i4RetVal = DNS_ZERO;

    MEMSET (&ResolvedIpInfo, DNS_ZERO, sizeof (tDNSResolvedIpInfo));

    i4RetVal = DNSApiResolveIPvXFromName (pu1QueryName, DNS_HOST_ADDRESS,
                                          &ResolvedIpInfo, pu4Error);
    IPVX_ADDR_COPY (&(pResolvedIP), &(ResolvedIpInfo.Resolv4Addr));

    return i4RetVal;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DNSResolveIPFromNameServer                         */
/*                                                                           */
/*     DESCRIPTION      : This function is used to resolve the IP Address    */
/*                        from the domain server name                        */
/*                                                                           */
/*     INPUT            : pu1QueryName - Name to be resolved                 */
/*                                                                           */
/*     OUTPUT           : pResolvedIP - Resolved IP addresses                */
/*                                                                           */
/*     RETURNS          : DNS_SUCCESS / DNS_FAILURE                          */
/*****************************************************************************/
PUBLIC INT4
DNSResolveIPFromNameServer (UINT1 *pu1QueryName, UINT1 *pu1DnsSrvrIp)
{
    /* Start DNS resolution */
    gDnsCapwapSRVRecord.u1Dnslookup = TRUE;
    DnsQueMsg (pu1QueryName, DNS_SRV_RECORD);

    UNUSED_PARAM (pu1DnsSrvrIp);
    return DNS_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DNSApiResolveIPvXFromName                             */
/*                                                                           */
/*     DESCRIPTION      : This function is used to resolve the IP Address    */
/*                        from the English name                              */
/*                                                                           */
/*     INPUT            : pu1QueryName - Name to be resolved                 */
/*                        u1QueryType  - Query Type                          */
/*                                                                           */
/*     OUTPUT           : pResolvedIP - Resolved IPvX                        */
/*                        pu4Error - Error Code                              */
/*                                                                           */
/*     RETURNS          : DNS_SUCCESS / DNS_FAILURE                          */
/*****************************************************************************/

PUBLIC INT4
DNSApiResolveIPvXFromName (UINT1 *pu1QueryName, UINT1 u1QueryType,
                           tDNSResolvedIpInfo * pResolvedIpInfo,
                           UINT4 *pu4Error)
{
    INT4                i4RetVal = DNS_ZERO;
    INT4                u1IsAsync = OSIX_TRUE;

    if (gDnsGlobalInfo.u1ModuleStatus != DNS_ENABLE)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Dns Module is Shutdown \r\n"));
        return DNS_FAILURE;
    }
    i4RetVal = DNSApiResolverCacheAndAct (pu1QueryName, (UINT1) u1IsAsync,
                                          pResolvedIpInfo);

    UNUSED_PARAM (u1QueryType);
    UNUSED_PARAM (pu4Error);

    return i4RetVal;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DNSApiResolverCacheAndAct                          */
/*                                                                           */
/*     DESCRIPTION      : This function is used to resolve the IP Address    */
/*                        from the English name                              */
/*                                                                           */
/*     INPUT            : pu1QueryName - Name to be resolved                 */
/*                        u1QueryType  - Query Type                          */
/*                                                                           */
/*     OUTPUT           : pResolvedIP - Resolved IPvX                        */
/*                        pu4Error - Error Code                              */
/*                                                                           */
/*     RETURNS          : DNS_SUCCESS / DNS_FAILURE                          */
/*****************************************************************************/
PUBLIC INT4
DNSApiResolverCacheAndAct (UINT1 *pu1QueryName, UINT1 u1IsAsync,
                           tDNSResolvedIpInfo * pResolvedIpInfo)
{
    INT4                i4RetVal = DNS_ZERO;

    if (gDnsGlobalInfo.u1ModuleStatus != DNS_ENABLE)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Dns Module is Shutdown \r\n"));
        return DNS_FAILURE;
    }
    DnsLock ();
    i4RetVal =
        DNSResolveIPvXFromName (pu1QueryName, u1IsAsync, pResolvedIpInfo);

    DnsUnLock ();
    return i4RetVal;
}
