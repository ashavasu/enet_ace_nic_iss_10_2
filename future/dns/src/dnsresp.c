/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dnsresp.c,v 1.19.2.1 2018/03/01 14:05:18 siva Exp $
 *
 * Description: This file contains action routines for
 *              DNS Response Proccessing
 *
 *******************************************************************/

#include "dnsinc.h"

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsRcvRespFromServer      
 *
 *    DESCRIPTION      : This functions receives response from a Name Server
 *    
 *    INPUT            : pDnsQueryInfo - pointer to DNS Query structure
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : DNS_SUCCESS / DNS_FAILURE.
 *
 ****************************************************************************/
PUBLIC INT4
DnsRcvRespFromServer (tDnsQueryInfo * pDnsQueryInfo)
{
    INT4                i4ServerAddrLen = 0;
    INT4                i4PktSize = 0;
    fd_set              readFds;
    struct sockaddr_in  DnsServerAddr;
    struct timeval      stimeval;
    UINT2               u2QueryIndex;
    tDnsQueryInfo      *pTempDnsQueryInfo = NULL;
    tDnsQueryInfo       DnsQueryInfo;
    UINT4               u4IPAddr = 0;
    UINT1              *pu1RespPkt = NULL;
    UINT1               u1ErrCode = 0;

    pu1RespPkt = MemAllocMemBlk (gDnsGlobalInfo.QueryRespMemPoolId);
    if (pu1RespPkt == NULL)
    {
        DNS_TRACE ((DNS_RESP_TRC | DNS_FAILURE_TRC,
                    " DNS Memory Allocation Fails  \r\n"));
        return DNS_FAILURE;
    }
    do
    {
        MEMSET (&DnsServerAddr, 0, sizeof (DnsServerAddr));
        MEMSET (pu1RespPkt, 0, MAX_DNS_PKT_LEN);
        MEMSET (&DnsQueryInfo, 0, sizeof (DnsQueryInfo));
        MEMSET (&readFds, 0, sizeof (readFds));

        FD_ZERO (&readFds);
        FD_SET ((UINT4) (gDnsGlobalInfo.i4ResolverSockFd), &readFds);

        stimeval.tv_sec = (time_t) gDnsResolverParams.u4QueryTimeout;
        stimeval.tv_usec = 0;

        /* Waiting on select call till Query Timeout */
        if (select (gDnsGlobalInfo.i4ResolverSockFd + 1,
                    &readFds, NULL, NULL, &stimeval) <= DNS_ZERO)
        {
            DNS_TRACE ((DNS_RESP_TRC | DNS_FAILURE_TRC,
                        " Select Call Fails \r\n"));
            MemReleaseMemBlock (gDnsGlobalInfo.QueryRespMemPoolId, pu1RespPkt);
            return DNS_FAILURE;
        }

        i4PktSize =
            recvfrom (gDnsGlobalInfo.i4ResolverSockFd, pu1RespPkt,
                      DNS_MAX_PKT_LEN, DNS_ZERO,
                      (struct sockaddr *) &DnsServerAddr, &i4ServerAddrLen);

        if (i4PktSize < 0)
        {
            DNS_TRACE ((DNS_RESP_TRC | DNS_FAILURE_TRC,
                        " DNS Packet RecvFrom Fails  \r\n"));
            MemReleaseMemBlock (gDnsGlobalInfo.QueryRespMemPoolId, pu1RespPkt);
            return DNS_FAILURE;
        }

        /* To ensure that response is received only from the name server to which
         * it was queried */

        MEMCPY (&u4IPAddr, pDnsQueryInfo->QueryNameServerIP.au1Addr,
                sizeof (UINT4));

#ifdef LNXIP4_WANTED
        if (DnsServerAddr.sin_family != AF_UNSPEC)
        {
            if (u4IPAddr != DnsServerAddr.sin_addr.s_addr)
            {
                DNS_TRACE ((DNS_RESP_TRC | DNS_FAILURE_TRC,
                            " DNS Packet Source IP Addr Mismatch \r\n"));
                MemReleaseMemBlock (gDnsGlobalInfo.QueryRespMemPoolId,
                                    pu1RespPkt);
                return DNS_FAILURE;
            }
        }
#else
        if (u4IPAddr != DnsServerAddr.sin_addr.s_addr)
        {
            DNS_TRACE ((DNS_RESP_TRC | DNS_FAILURE_TRC,
                        " DNS Packet Source IP Addr Mismatch \r\n"));
            MemReleaseMemBlock (gDnsGlobalInfo.QueryRespMemPoolId, pu1RespPkt);
            return DNS_FAILURE;
        }
#endif
        gDnsStatistics.u4TotalResponseReceived++;

        MEMCPY (&u2QueryIndex, pu1RespPkt, sizeof (UINT2));
        u2QueryIndex = OSIX_HTONS (u2QueryIndex);

        /* If the response received is not for current Query */
        if (u2QueryIndex != pDnsQueryInfo->u2QueryIndex)
        {
            DNS_TRACE ((DNS_RESP_TRC,
                        " Response received for another DNS Query \r\n"));
            DnsQueryInfo.u2QueryIndex = u2QueryIndex;
            pTempDnsQueryInfo =
                RBTreeGet (gDnsGlobalInfo.PendingQueryList, &DnsQueryInfo);

            if (pTempDnsQueryInfo == NULL)
            {
                continue;
            }
            if (DnsProcessResponsePkt
                (pTempDnsQueryInfo, pu1RespPkt, &u1ErrCode) != DNS_SUCCESS)
            {
                DNS_TRACE ((DNS_RESP_TRC | DNS_FAILURE_TRC,
                            " DNS Response Packet Proccessing Fails \r\n"));
                gDnsStatistics.u4DroppedResponse++;
                MemReleaseMemBlock (gDnsGlobalInfo.QueryRespMemPoolId,
                                    pu1RespPkt);
                return DNS_FAILURE;
            }

            gDnsStatistics.u4UnansQueries--;
        }

    }
    while (u2QueryIndex != pDnsQueryInfo->u2QueryIndex);

    if (DnsProcessResponsePkt (pDnsQueryInfo, pu1RespPkt, &u1ErrCode) !=
        DNS_SUCCESS)
    {
        DNS_TRACE ((DNS_RESP_TRC | DNS_FAILURE_TRC,
                    " DNS Response Packet Proccessing Fails \r\n"));
        gDnsStatistics.u4DroppedResponse++;
        MemReleaseMemBlock (gDnsGlobalInfo.QueryRespMemPoolId, pu1RespPkt);
        return DNS_FAILURE;
    }

    gDnsStatistics.u4UnansQueries--;
    MemReleaseMemBlock (gDnsGlobalInfo.QueryRespMemPoolId, pu1RespPkt);
    return DNS_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsProcessResponsePkt     
 *
 *    DESCRIPTION      : This functions proccesses the response from a Name Server
 *    
 *    INPUT            : pDnsQueryInfo - pointer to DNS Query structure
 *                       pu1RespPkt - pointer to the response packet
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : DNS_SUCCESS / DNS_FAILURE.
 *
 ****************************************************************************/
PUBLIC INT4
DnsProcessResponsePkt (tDnsQueryInfo * pDnsQueryInfo, UINT1 *pu1RespPkt,
                       UINT1 *pErrNum)
{
    tDnsHeader          DnsHeader;
    UINT4               u4Offset = 0;

    MEMCPY (&DnsHeader, pu1RespPkt, sizeof (tDnsHeader));

    DnsHeader.u2QId = OSIX_NTOHS (DnsHeader.u2QId);

    DnsHeader.u2Flags = OSIX_NTOHS (DnsHeader.u2Flags);
    if ((DnsHeader.u2Flags & DNS_RESPONSE_FLAG) != DNS_RESPONSE_FLAG)
    {
        DNS_TRACE ((DNS_RESP_TRC | DNS_FAILURE_TRC,
                    " Invalid DNS Response Packet Format \r\n"));
        return DNS_FAILURE;
    }

    /* If the packet is truncated, we drop the response */
    if ((DnsHeader.u2Flags & DNS_TRUNCATION_FLAG) == DNS_TRUNCATION_FLAG)
    {
        DNS_TRACE ((DNS_RESP_TRC | DNS_FAILURE_TRC,
                    " DNS Response packet is truncated\r\n"));
        return DNS_FAILURE;
    }

    *pErrNum = (UINT1) (DnsHeader.u2Flags & DNS_RCODE_MASK);
    if (*pErrNum != DNS_ZERO)
    {
        DNS_TRACE ((DNS_RESP_TRC | DNS_FAILURE_TRC,
                    " Invalid DNS Response Packet Format \r\n"));
        return DNS_FAILURE;
    }
    u4Offset = sizeof (tDnsHeader);

    DNS_TRACE ((DNS_RESP_TRC, " Skipping DNS Response Question section \r\n"));
    DnsHeader.u2QDCount = OSIX_NTOHS (DnsHeader.u2QDCount);
    u4Offset +=
        DnsSkipRespQuesSection (pDnsQueryInfo, pu1RespPkt, u4Offset,
                                DnsHeader.u2QDCount);
    if (u4Offset <= DNS_ZERO)
    {
        DNS_TRACE ((DNS_RESP_TRC | DNS_FAILURE_TRC,
                    " Invalid DNS Response Question Section \r\n"));
        return DNS_FAILURE;
    }
    DnsHeader.u2ANCount = OSIX_NTOHS (DnsHeader.u2ANCount);
    if (DnsHeader.u2ANCount == 0)
    {
        pDnsQueryInfo->u1ToBeCached = OSIX_FALSE;
    }
    else
    {
        pDnsQueryInfo->u1ToBeCached = OSIX_TRUE;
    }

    DNS_TRACE ((DNS_RESP_TRC,
                " Retreiving IPAddr from DNS Response Answer section \r\n"));
    u4Offset +=
        DnsGetAddrFromAnsSection (pDnsQueryInfo, pu1RespPkt, u4Offset,
                                  DnsHeader.u2ANCount);

    DNS_TRACE ((DNS_RESP_TRC, " Skipping DNS Response Authority Section \r\n"));
    DnsHeader.u2NSCount = OSIX_NTOHS (DnsHeader.u2NSCount);
    u4Offset +=
        DnsSkipRespAuthSection (pu1RespPkt, u4Offset, DnsHeader.u2NSCount);

    DNS_TRACE ((DNS_RESP_TRC, " Processing Additional Record section \r\n"));
    DnsHeader.u2ARCount = OSIX_NTOHS (DnsHeader.u2ARCount);
    u4Offset +=
        DnsProcessAdditionalRecord (pu1RespPkt, u4Offset, DnsHeader.u2ARCount);

    return DNS_SUCCESS;
}

/****************************************************************************
 *
 *      FUNCTION NAME    : DnsProcessAdditionalRecord
 * 
 *      DESCRIPTION      : This functions returns the address from the
 *                         answer section.
 * 
 *      INPUT            : pu1RespPkt - pointer to DNS Response packet
 *                         u4PktOffset - Offset
 *                         u2ARCount - Result count
 *   
 *      OUTPUT           : None
 * 
 *      RETURNS          : Current packet offset
 *  
 *   
 **************************************************************************/
PUBLIC UINT4
DnsProcessAdditionalRecord (UINT1 *pu1RespPkt,
                            UINT4 u4PktOffset, UINT2 u2ARCount)
{
#ifdef WTP_WANTED
    UINT2               u2RdataLen = 0;
    UINT4               u4TTL;
    UINT4               u4Offset = 0;
    UINT2               u2Count = 0;
    UINT1              *pu1ARSec = pu1RespPkt + u4PktOffset;
    UINT1              *pu1TgtNameSec = NULL;
    UINT1               au1HostName[DNS_MAX_DOMAIN_NAME_LEN] = { 0 };

    for (u2Count = 0; u2Count < u2ARCount; u2Count++)
    {
        /* check the DNS Target name */
        pu1TgtNameSec = pu1RespPkt + pu1ARSec[1];

        pu1ARSec += DNS_PKT_OFFSET2;
        u4Offset += DNS_PKT_OFFSET2;
        /* Check the type of the DNS response TYPE */
        if ((pu1ARSec[0] == 0x00) && (pu1ARSec[1] == 0x01))
        {
            /* DNS response type is A record */

            /* Skip the Type and Class */
            pu1ARSec += DNS_TYPE_AND_CLASS_LEN;
            u4Offset += DNS_TYPE_AND_CLASS_LEN;
            /* Skip the TTL */
            MEMCPY (&u4TTL, pu1ARSec, DNS_RESPONSE_TTL_LEN);
            u4TTL = OSIX_HTONL (u4TTL);
            pu1ARSec += DNS_RESPONSE_TTL_LEN;
            u4Offset += DNS_RESPONSE_TTL_LEN;
            /* Get the IP Address Len */
            MEMCPY (&u2RdataLen, pu1ARSec, DNS_ANSWER_RDATA_LEN);
            u2RdataLen = OSIX_NTOHS (u2RdataLen);
            pu1ARSec += DNS_ANSWER_RDATA_LEN;
            u4Offset += DNS_ANSWER_RDATA_LEN;
            /* Fill the IP address */
            if (u2RdataLen == DNS_PKT_OFFSET4)
            {
                MEMCPY (au1HostName, pu1TgtNameSec,
                        gDnsCapwapSRVRecord.DnsSrvInfo.u4TargetLen);
                if ((gDnsCapwapSRVRecord.u1Dnslookup == 1) &&
                    (MEMCMP (gDnsCapwapSRVRecord.DnsSrvInfo.au1TargetName,
                             au1HostName,
                             gDnsCapwapSRVRecord.DnsSrvInfo.u4TargetLen) == 0))
                {

                    DNS_TRACE ((DNS_RESP_TRC,
                                " Successfully retreived IP Addr"
                                " from Response \r\n"));
                    gDnsCapwapSRVRecord.ServerIp.u1AddrLen = (UINT1) u2RdataLen;
                    MEMCPY (gDnsCapwapSRVRecord.ServerIp.au1Addr,
                            pu1ARSec, u2RdataLen);
                    CapwapSetAcIPAddressFromDns (gDnsCapwapSRVRecord.ServerIp.
                                                 u1AddrLen,
                                                 gDnsCapwapSRVRecord.ServerIp.
                                                 au1Addr,
                                                 gDnsCapwapSRVRecord.DnsSrvInfo.
                                                 u4Port);
                }

            }
            pu1ARSec += u2RdataLen;
            u4Offset += u2RdataLen;
        }

    }

    return u4Offset;

#else
    UNUSED_PARAM (pu1RespPkt);
    UNUSED_PARAM (u4PktOffset);
    UNUSED_PARAM (u2ARCount);
    return 0;
#endif
}

 /****************************************************************************
 *
 *    FUNCTION NAME    : DnsSkipRespQuesSection    
 *
 *    DESCRIPTION      : This functions skips the Question section from the       
 *                       response packet
 *    
 *    INPUT            : pu1RespPkt - pointer to the response packet
 *                       u4PktOffset - Offset
 *                       u2QuesCount - Question count
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : Current packet offset        
 *
 ****************************************************************************/
PUBLIC UINT4
DnsSkipRespQuesSection (tDnsQueryInfo * pDnsQueryInfo, UINT1 *pu1RespPkt,
                        UINT4 u4PktOffset, UINT2 u2QuesCount)
{
    UINT4               u4Offset = 0;
    UINT4               u4CurQuesCount = 0;
    UINT1              *pu1AnsSec = pu1RespPkt + u4PktOffset;
    UINT1               u1NameCnt = 0;
    UINT1               u1NameMatch = OSIX_FALSE;
    UINT1               u1Hostname = OSIX_FALSE;
    UINT1               au1Host[DNS_MAX_RESOLVER_LEN + 1];
    tDnsDomainNameInfo *pNextDomainNode = NULL;

    for (u4CurQuesCount = 0; u4CurQuesCount < u2QuesCount; u4CurQuesCount++)
    {
        u1NameCnt = 0;
        MEMSET (au1Host, 0, (DNS_MAX_RESOLVER_LEN + 1));

        while (pu1AnsSec[u4Offset] != 0)
        {
            u4Offset = (UINT4) (u4Offset + pu1AnsSec[u4Offset] + 1);
            if (u1NameCnt == 0)
            {
                STRNCAT (au1Host, (pu1AnsSec + 1), (u4Offset - 1));
                if (MEMCMP
                    (au1Host, pDnsQueryInfo->au1ResolveName,
                     MEM_MAX_BYTES (STRLEN (au1Host),
                                    STRLEN (pDnsQueryInfo->au1ResolveName))) ==
                    0)

                {
                    u1Hostname = OSIX_TRUE;
                }
            }
            else
            {
                STRCAT (au1Host, ".");
                STRNCAT (au1Host, (pu1AnsSec + u1NameCnt + 1),
                         ((INT1) u4Offset - (u1NameCnt + 1)));
            }
            u1NameCnt = (UINT1) u4Offset;
        }
        if (u1Hostname == OSIX_TRUE)
        {
            if (MEMCMP
                (au1Host, pDnsQueryInfo->au1ResolveName,
                 MEM_MAX_BYTES (STRLEN (au1Host),
                                STRLEN (pDnsQueryInfo->au1ResolveName))) == 0)

            {
                u1NameMatch = OSIX_TRUE;
            }

            else if (TMO_SLL_Count (&(gDnsGlobalInfo.DomainNameList)) > 0)
            {
                TMO_SLL_Scan (&gDnsGlobalInfo.DomainNameList, pNextDomainNode,
                              tDnsDomainNameInfo *)
                {
                    MEMSET (pDnsQueryInfo->au1ResolveName, 0,
                            DNS_MAX_QUERY_LEN);
                    STRNCPY (pDnsQueryInfo->au1ResolveName,
                             pDnsQueryInfo->au1QueryName,
                             STRLEN (pDnsQueryInfo->au1QueryName));

                    STRCAT (pDnsQueryInfo->au1ResolveName, ".");
                    STRNCAT (pDnsQueryInfo->au1ResolveName,
                             pNextDomainNode->au1DomainName,
                             STRLEN (pNextDomainNode->au1DomainName));

                    if (STRLEN (au1Host) !=
                        STRLEN (pDnsQueryInfo->au1ResolveName))
                    {
                        continue;
                    }
                    if (MEMCMP
                        (au1Host, pDnsQueryInfo->au1ResolveName,
                         STRLEN (au1Host)) == 0)

                    {
                        u1NameMatch = OSIX_TRUE;
                        break;
                    }

                }
            }
            else
            {
                u1NameMatch = OSIX_TRUE;
            }
        }
        u4Offset += 1;
        u4Offset += DNS_TYPE_AND_CLASS_LEN;
    }
    if (u1NameMatch == OSIX_FALSE)
    {
        return DNS_ZERO;
    }

    return u4Offset;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsGetAddrFromAnsSection  
 *
 *    DESCRIPTION      : This functions returns the address from the answer       
 *                       section.           
 *    
 *    INPUT            : pDnsQueryInfo - pointer to DNS Query structure
 *                       pu1RespPkt - pointer to DNS Response packet
 *                       u4PktOffset - Offset
 *                       u2QuesCount - Question count
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : Current packet offset        
 *
 ****************************************************************************/
PUBLIC UINT4
DnsGetAddrFromAnsSection (tDnsQueryInfo * pDnsQueryInfo, UINT1 *pu1RespPkt,
                          UINT4 u4PktOffset, UINT2 u2AnsCount)
{
    UINT2               u2RdataLen = 0;
    UINT4               u4TTL;
    UINT4               u4Offset = 0;
    UINT2               u2Count = 0;
    UINT1              *pu1AnsSec = NULL;
    UINT4               u4Weight = 0;
    UINT4               u4Priority = 0;
    UINT2               u4Port = 0;
    UINT2               u2Val = DNS_PKT_OFFSET2 + DNS_PKT_OFFSET4;
    UINT4               u4SrvRecordCount = 0;
    tDnsSrvInfo        *pDnsSrvRecord[MAX_DNS_ANS_COUNT];

    /*Below conditions has been added to solve
       tainted scalar error of coverity and f18 64 bit warnings */
    if ((pu1RespPkt == NULL) || (u2AnsCount > MAX_DNS_ANS_COUNT))
    {
        return DNS_FAILURE;
    }
    else
    {
        pu1AnsSec = pu1RespPkt + u4PktOffset;
    }
    if (u2AnsCount < DNS_MAX_UINT2_VALUE)
    {
        for (u2Count = 0; u2Count < u2AnsCount; u2Count++)
        {
            /* Skip the name in the answer section. The name would have 
             * the value 0xC00C as the value because of the 
             * DNS Packet compression. Hence moving only 2 bytes.
             */
            if (pu1AnsSec[0] != 0xc0)
            {
                return DNS_FAILURE;
            }

            /* Allocate memory for the DNS SRV structure. */
            pDnsSrvRecord[u4SrvRecordCount] = (tDnsSrvInfo *) MemAllocMemBlk
                (gDnsGlobalInfo.DnsSrvMemPoolId);
            if (pDnsSrvRecord[u4SrvRecordCount] == NULL)
            {
                return DNS_FAILURE;
            }

            pu1AnsSec += DNS_PKT_OFFSET2;
            u4Offset += DNS_PKT_OFFSET2;
            /* Check the type of the DNS response TYPE */
            if ((pu1AnsSec[0] == 0x00) && (pu1AnsSec[1] == DNS_SRV_RECORD))
            {
                /* DNS response is Service record */

                /* Skip the Type and Class */
                pu1AnsSec += DNS_TYPE_AND_CLASS_LEN;
                u4Offset += DNS_TYPE_AND_CLASS_LEN;
                /* Skip the TTL */
                MEMCPY (&u4TTL, pu1AnsSec, DNS_RESPONSE_TTL_LEN);
                pDnsQueryInfo->u4QueryTTL = OSIX_HTONL (u4TTL);
                pu1AnsSec += DNS_RESPONSE_TTL_LEN;
                u4Offset += DNS_RESPONSE_TTL_LEN;

                /* Get the data length */
                MEMCPY (&u2RdataLen, pu1AnsSec, DNS_ANSWER_RDATA_LEN);
                u2RdataLen = OSIX_NTOHS (u2RdataLen);
                pu1AnsSec += DNS_ANSWER_RDATA_LEN;
                u4Offset += DNS_ANSWER_RDATA_LEN;
                /* Get the Priority, Weight and Port */
                MEMCPY (&u4Priority, pu1AnsSec, DNS_PKT_OFFSET2);
                u4Priority = OSIX_NTOHL (u4Priority);
                pu1AnsSec += DNS_PKT_OFFSET2;
                u4Offset += DNS_PKT_OFFSET2;
                MEMCPY (&u4Weight, pu1AnsSec, DNS_PKT_OFFSET2);
                u4Weight = OSIX_NTOHS (u4Weight);
                pu1AnsSec += DNS_PKT_OFFSET2;
                u4Offset += DNS_PKT_OFFSET2;

                MEMCPY (&u4Port, pu1AnsSec, DNS_PKT_OFFSET2);
                u4Port = OSIX_NTOHS (u4Port);
                pu1AnsSec += DNS_PKT_OFFSET2;
                u4Offset += DNS_PKT_OFFSET2;
                pDnsSrvRecord[u4SrvRecordCount]->u4Priority = u4Priority;
                pDnsSrvRecord[u4SrvRecordCount]->u4Weight = u4Weight;
                pDnsSrvRecord[u4SrvRecordCount]->u4Port = u4Port;
                u2RdataLen = (UINT2) (u2RdataLen - u2Val);
                pDnsSrvRecord[u4SrvRecordCount]->u4TargetLen = u2RdataLen;
                MEMCPY (pDnsSrvRecord[u4SrvRecordCount]->au1TargetName,
                        pu1AnsSec,
                        pDnsSrvRecord[u4SrvRecordCount]->u4TargetLen);
                pu1AnsSec += u2RdataLen;
                u4Offset += u2RdataLen;
                u4SrvRecordCount++;
            }
            else if ((pu1AnsSec[0] == 0x00) && (pu1AnsSec[1] == 0x01))
            {
                /* DNS response type is A record */

                /* Skip the Type and Class */
                pu1AnsSec += DNS_TYPE_AND_CLASS_LEN;
                u4Offset += DNS_TYPE_AND_CLASS_LEN;

                /* Skip the TTL */
                MEMCPY (&u4TTL, pu1AnsSec, DNS_RESPONSE_TTL_LEN);
                pDnsQueryInfo->u4QueryTTL = OSIX_HTONL (u4TTL);
                pu1AnsSec += DNS_RESPONSE_TTL_LEN;
                u4Offset += DNS_RESPONSE_TTL_LEN;

                /* Get the IP Address Len */
                MEMCPY (&u2RdataLen, pu1AnsSec, DNS_ANSWER_RDATA_LEN);
                u2RdataLen = OSIX_NTOHS (u2RdataLen);
                pDnsQueryInfo->QueryResolvedIP.u1AddrLen = (UINT1) u2RdataLen;
                pu1AnsSec += DNS_ANSWER_RDATA_LEN;
                u4Offset += DNS_ANSWER_RDATA_LEN;

                /* Fill the IP address */
                if (u2RdataLen == DNS_PKT_OFFSET4)
                {
                    DNS_TRACE ((DNS_RESP_TRC,
                                " Successfully retreived IP Addr from Response \r\n"));
                    MEMCPY (pDnsQueryInfo->QueryResolvedIP.au1Addr, pu1AnsSec,
                            u2RdataLen);
                }
                pu1AnsSec += u2RdataLen;
                u4Offset += u2RdataLen;
            }
        }
    }
    if (u4SrvRecordCount > 0)
    {
        DnsSortSrvRecord (pDnsSrvRecord, u4SrvRecordCount);
        MEMCPY (&(gDnsCapwapSRVRecord.DnsSrvInfo), &pDnsSrvRecord[0],
                sizeof (tDnsSrvInfo));
        for (u2Count = 0; u2Count < u4SrvRecordCount; u2Count++)
        {
            MemReleaseMemBlock (gDnsGlobalInfo.DnsSrvMemPoolId,
                                (UINT1 *) pDnsSrvRecord[u2Count]);
        }
    }

    return u4Offset;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsSkipRespAuthSection    
 *
****************************************************************************/
VOID
DnsSortSrvRecord (tDnsSrvInfo * pDnsSrvRecord[], UINT4 u4Count)
{
    tDnsSrvInfo         DnsSrvInfo;
    UINT4               u4Index0 = 0;
    UINT4               u4Index1 = 0;

    MEMSET (&DnsSrvInfo, 0, sizeof (tDnsSrvInfo));

    /* As per RFC 2782, if the DNS response has multiple SRV records, select the best *
     *one based on priority, if priority is same for all the SRV records then the SRV Record *
     *with larget weight should be given higher probability of being selected */

    for (u4Index0 = 0; u4Index0 < u4Count - 1; u4Index0++)
    {
        for (u4Index1 = 0; u4Index1 < u4Count - u4Index0 - 1; u4Index1++)
        {
            if (pDnsSrvRecord[u4Index1]->u4Priority >
                pDnsSrvRecord[u4Index1 + 1]->u4Priority)
            {
                MEMCPY (&DnsSrvInfo, &pDnsSrvRecord[u4Index1],
                        sizeof (tDnsSrvInfo));
                MEMCPY (&pDnsSrvRecord[u4Index1], &pDnsSrvRecord[u4Index1 + 1],
                        sizeof (tDnsSrvInfo));
                MEMCPY (&pDnsSrvRecord[u4Index1 + 1], &DnsSrvInfo,
                        sizeof (tDnsSrvInfo));
            }
            else if (pDnsSrvRecord[u4Index1]->u4Priority ==
                     pDnsSrvRecord[u4Index1 + 1]->u4Priority)
            {
                if (pDnsSrvRecord[u4Index1]->u4Weight <
                    pDnsSrvRecord[u4Index1 + 1]->u4Weight)
                {
                    MEMCPY (&DnsSrvInfo, &pDnsSrvRecord[u4Index1],
                            sizeof (tDnsSrvInfo));
                    MEMCPY (&pDnsSrvRecord[u4Index1],
                            &pDnsSrvRecord[u4Index1 + 1], sizeof (tDnsSrvInfo));
                    MEMCPY (&pDnsSrvRecord[u4Index1 + 1], &DnsSrvInfo,
                            sizeof (tDnsSrvInfo));
                }
            }
        }
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsSkipRespAuthSection    
 *
 *    DESCRIPTION      : This functions skips the Auth section from the           
 *                       response packet.   
 *    
 *    INPUT            : pu1RespPkt - pointer to DNS Response packet
 *                       u4PktOffset - Offset
 *                       u2NSCount - Name server count
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : Current packet offset        
 *
 ****************************************************************************/
PUBLIC UINT4
DnsSkipRespAuthSection (UINT1 *pu1RespPkt, UINT4 u4PktOffset, UINT2 u2NSCount)
{
    UINT2               u2RdataLen = 0;
    UINT4               u4Offset = 0;
    UINT4               u4Count = 0;
    UINT1              *pu1NSSec = pu1RespPkt + u4PktOffset;

    for (u4Count = 0; u4Count < u2NSCount; u4Count++)
    {
        pu1NSSec += 2;
        u4Offset += 2;

        /* Skip the Type and Class */
        pu1NSSec += DNS_TYPE_AND_CLASS_LEN;
        u4Offset += DNS_TYPE_AND_CLASS_LEN;

        /* Skip the TTL */
        pu1NSSec += DNS_RESPONSE_TTL_LEN;
        u4Offset += DNS_RESPONSE_TTL_LEN;

        /* Get the IP Address Len */
        MEMCPY (&u2RdataLen, pu1NSSec, DNS_ANSWER_RDATA_LEN);
        u2RdataLen = OSIX_NTOHS (u2RdataLen);
        pu1NSSec += (DNS_ANSWER_RDATA_LEN + u2RdataLen);
        u4Offset = u4Offset + (UINT4) (DNS_ANSWER_RDATA_LEN + u2RdataLen);
    }
    return u4Offset;
}

/****************************************************************************
 **
 **    FUNCTION NAME    : DnsRcvRespIPvXFromServer
 **
 **    DESCRIPTION      : This functions receives response from a Name Server
 **
 **    INPUT            : pDnsQueryInfov4 - pointer to DNS Query structure of 
 **                                         Type A
 **                       pDnsQueryInfov6 - pointer to DNS Query structure of 
 **                                         Type AAAA
 **                       pV6Status- IPv6 resolved Status
 **                       pV4Status- IPv4 resolved Status
 **    OUTPUT           : None
 **
 **    RETURNS          : DNS_SUCCESS / DNS_FAILURE.
 **
 *****************************************************************************/

PUBLIC INT4
DnsRcvRespIPvXFromServer (INT4 i4SockId)
{
    INT4                i4RetVal = DNS_SUCCESS;
    UINT1              *pu1RespPkt = NULL;

    pu1RespPkt = MemAllocMemBlk (gDnsGlobalInfo.QueryRespMemPoolId);
    if (pu1RespPkt == NULL)
    {
        DNS_TRACE ((DNS_RESP_TRC | DNS_FAILURE_TRC,
                    " DNS Memory Allocation Fails  \r\n"));
        return DNS_FAILURE;
    }
    i4RetVal = DnsRcvRespIPvX (i4SockId, pu1RespPkt);
    MemReleaseMemBlock (gDnsGlobalInfo.QueryRespMemPoolId, pu1RespPkt);

    return i4RetVal;
}

/****************************************************************************
 **
 **    FUNCTION NAME    : DnsResReqQry
 **
 **    DESCRIPTION      : This function process the received packet
 **
 **    INPUT            : u2QueryIndex - Query Index
 **                       pu1RespPkt - packet storage
 **    OUTPUT           : None
 **
 **    RETURNS          : DNS_SUCCESS / DNS_FAILURE.
 **
 *******************************************************************************/

PUBLIC INT4
DnsResReqQry (UINT2 u2QueryIndex, UINT1 *pu1RespPkt, tIPvXAddr * pSrcAddr)
{
    INT4                i4RetVal = DNS_SUCCESS;
    UINT1               u1ErrCode = 0;
    UINT4               u4NsPosition = 0;
    tDnsQueryInfo      *pTempDnsQueryInfo = NULL;
    tDnsQueryInfo       DnsQueryInfo;

    MEMSET (&DnsQueryInfo, 0, sizeof (DnsQueryInfo));

    DNS_TRACE ((DNS_RESP_TRC, " Response received for DNS Query \r\n"));
    DnsQueryInfo.u2QueryIndex = u2QueryIndex;
    pTempDnsQueryInfo =
        RBTreeGet (gDnsGlobalInfo.PendingQueryList, &DnsQueryInfo);

    if (pTempDnsQueryInfo != NULL)
    {
        if (DnsProcessResponsePkt (pTempDnsQueryInfo,
                                   pu1RespPkt, &u1ErrCode) != DNS_SUCCESS)
        {
            DNS_TRACE ((DNS_RESP_TRC | DNS_FAILURE_TRC,
                        " DNS Response Packet Proccessing Fails \r\n"));
            gDnsStatistics.u4DroppedResponse++;
            u4NsPosition = DnsGetNameServerFromAdd (pSrcAddr);

            if ((u4NsPosition > 0) && (u4NsPosition < MAX_DNS_NAME_SERVERS + 1))
            {
                pTempDnsQueryInfo->au1ErrorNum[(u4NsPosition) - 1] = u1ErrCode;
            }

            if (gDnsResolverParams.i4QueryMode == DNS_MODE_SEQUENCE)
            {
                DNSResolSeqMode (pTempDnsQueryInfo);
            }
            i4RetVal = DNS_FAILURE;
        }
        else
        {
            DNS_UNANS_DECR (gDnsStatistics.u4UnansQueries);
            IPVX_ADDR_COPY (&(pTempDnsQueryInfo->QueryNameServerIP), pSrcAddr);
            DnsUpdateResolIP (pTempDnsQueryInfo);
        }
    }
    else
    {
        DNS_TRACE ((DNS_RESP_TRC,
                    " DNS Response Received No Pending Query to process \r\n"));
        gDnsStatistics.u4DroppedResponse++;
        i4RetVal = DNS_FAILURE;
    }
    return i4RetVal;
}

/****************************************************************************
 **
 **    FUNCTION NAME    : DnsRcvRespIPvX
 **
 **    DESCRIPTION      : This functions receives response from a Name Server
 **
 **    INPUT            : i4SockId -Socket ID
 **                       pu1RespPkt - packet storage
 **    OUTPUT           : None
 **
 **    RETURNS          : DNS_SUCCESS / DNS_FAILURE.
 **
 ******************************************************************************/

PUBLIC INT4
DnsRcvRespIPvX (INT4 i4SockId, UINT1 *pu1RespPkt)
{
    INT4                i4PktSize = 0;
    INT4                i4RetVal = DNS_SUCCESS;
    UINT2               u2QueryIndex = 0;
    struct sockaddr_in  DnsServerAddr;
    INT4                i4ServerAddrLen = sizeof (DnsServerAddr);
#ifdef IP6_WANTED
    struct sockaddr_in6 DnsServerAddr6;
#endif
    tIPvXAddr           SrcAddr;

    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&DnsServerAddr, 0, sizeof (DnsServerAddr));
#ifdef IP6_WANTED
    MEMSET (&DnsServerAddr6, 0, sizeof (DnsServerAddr6));
#endif
    MEMSET (pu1RespPkt, 0, MAX_DNS_PKT_LEN);

    if (i4SockId == gDnsGlobalInfo.i4ResolverSockFd)
    {
        while ((i4PktSize =
                recvfrom (gDnsGlobalInfo.i4ResolverSockFd, pu1RespPkt,
                          DNS_MAX_PKT_LEN, DNS_ZERO,
                          (struct sockaddr *) &DnsServerAddr,
                          &i4ServerAddrLen)) > 0)
        {
            MEMCPY (&(SrcAddr.au1Addr), &(DnsServerAddr.sin_addr.s_addr),
                    IPVX_IPV4_ADDR_LEN);
            SrcAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
            SrcAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
            gDnsStatistics.u4TotalResponseReceived++;
            MEMCPY (&u2QueryIndex, pu1RespPkt, sizeof (UINT2));
            u2QueryIndex = OSIX_HTONS (u2QueryIndex);

            DnsResReqQry (u2QueryIndex, pu1RespPkt, &SrcAddr);
            MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
            MEMSET (&DnsServerAddr, 0, sizeof (DnsServerAddr));
            MEMSET (pu1RespPkt, 0, MAX_DNS_PKT_LEN);

        }

    }
    else if (i4SockId == gDnsGlobalInfo.i4ResolverSock6Fd)
    {
#ifdef IP6_WANTED
        while ((i4PktSize =
                recvfrom (gDnsGlobalInfo.i4ResolverSock6Fd, pu1RespPkt,
                          DNS_MAX_PKT_LEN, DNS_ZERO,
                          (struct sockaddr *) &DnsServerAddr6,
                          &i4ServerAddrLen)) > 0)
        {
            MEMCPY (&(SrcAddr.au1Addr), &(DnsServerAddr6.sin6_addr.s6_addr),
                    IPVX_IPV6_ADDR_LEN);
            SrcAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
            SrcAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;
            gDnsStatistics.u4TotalResponseReceived++;
            MEMCPY (&u2QueryIndex, pu1RespPkt, sizeof (UINT2));
            u2QueryIndex = OSIX_HTONS (u2QueryIndex);

            DnsResReqQry (u2QueryIndex, pu1RespPkt, &SrcAddr);
            MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));
            MEMSET (&DnsServerAddr6, 0, sizeof (DnsServerAddr6));
            MEMSET (pu1RespPkt, 0, MAX_DNS_PKT_LEN);

        }
#endif
    }

    return i4RetVal;
}
