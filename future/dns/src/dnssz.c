/* $Id: dnssz.c,v 1.4 2013/11/29 11:04:12 siva Exp $*/
#define _DNSSZ_C
#include "dnsinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
DnsSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < DNS_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (FsDNSSizingParams[i4SizingId].u4StructSize,
                                     FsDNSSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(DNSMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            DnsSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
DnsSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsDNSSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, DNSMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
DnsSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < DNS_MAX_SIZING_ID; i4SizingId++)
    {
        if (DNSMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (DNSMemPoolIds[i4SizingId]);
            DNSMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
