/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdnslw.c,v 1.8 2014/11/19 10:54:41 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "dnsinc.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDnsSystemControl
 Input       :  The Indices

                The Object 
                retValFsDnsSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDnsSystemControl (INT4 *pi4RetValFsDnsSystemControl)
{
    *pi4RetValFsDnsSystemControl = gDnsGlobalInfo.u1SystemCtrlStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDnsModuleStatus
 Input       :  The Indices

                The Object 
                retValFsDnsModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDnsModuleStatus (INT4 *pi4RetValFsDnsModuleStatus)
{
    *pi4RetValFsDnsModuleStatus = gDnsGlobalInfo.u1ModuleStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDnsTraceOption
 Input       :  The Indices

                The Object 
                retValFsDnsTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDnsTraceOption (INT4 *pi4RetValFsDnsTraceOption)
{

    *pi4RetValFsDnsTraceOption = (INT4) gDnsGlobalInfo.u4TraceOption;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDnsQueryRetryCount
 Input       :  The Indices

                The Object 
                retValFsDnsQueryRetryCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDnsQueryRetryCount (UINT4 *pu4RetValFsDnsQueryRetryCount)
{
    *pu4RetValFsDnsQueryRetryCount = gDnsResolverParams.u4QueryRetryCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDnsQueryTimeOut
 Input       :  The Indices

                The Object 
                retValFsDnsQueryTimeOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDnsQueryTimeOut (UINT4 *pu4RetValFsDnsQueryTimeOut)
{
    *pu4RetValFsDnsQueryTimeOut = gDnsResolverParams.u4QueryTimeout;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDnsResolverMode
 Input       :  The Indices

                The Object 
                retValFsDnsResolverMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDnsResolverMode (INT4 *pi4RetValFsDnsResolverMode)
{
    *pi4RetValFsDnsResolverMode = gDnsResolverParams.i4QueryMode;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDnsPreferentialType
 Input       :  The Indices

                The Object 
                retValFsDnsPreferentialType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDnsPreferentialType (INT4 *pi4RetValFsDnsPreferentialType)
{
    *pi4RetValFsDnsPreferentialType = gDnsResolverParams.i4QueryType;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDnsSystemControl
 Input       :  The Indices

                The Object 
                setValFsDnsSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDnsSystemControl (INT4 i4SetValFsDnsSystemControl)
{

    if (gDnsGlobalInfo.u1SystemCtrlStatus == (UINT1) i4SetValFsDnsSystemControl)
    {
        return SNMP_SUCCESS;
    }
    if (i4SetValFsDnsSystemControl != DNS_SHUTDOWN)
    {
        if (DnsModuleStart () != DNS_SUCCESS)
        {
            DNS_TRACE ((DNS_FAILURE_TRC, "Failed to start DNS Module \r\n"));
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (DnsModuleShutdown () != DNS_SUCCESS)
        {
            DNS_TRACE ((DNS_FAILURE_TRC, "Failed to shutdown DNS Module \r\n"));
            return SNMP_FAILURE;
        }
    }

    gDnsGlobalInfo.u1SystemCtrlStatus = (UINT1) i4SetValFsDnsSystemControl;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDnsModuleStatus
 Input       :  The Indices

                The Object 
                setValFsDnsModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDnsModuleStatus (INT4 i4SetValFsDnsModuleStatus)
{
    if (gDnsGlobalInfo.u1ModuleStatus == (UINT1) i4SetValFsDnsModuleStatus)
    {
        return SNMP_SUCCESS;
    }

    gDnsGlobalInfo.u1ModuleStatus = (UINT1) i4SetValFsDnsModuleStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDnsTraceOption
 Input       :  The Indices

                The Object 
                setValFsDnsTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDnsTraceOption (INT4 i4SetValFsDnsTraceOption)
{
    if (gDnsGlobalInfo.u4TraceOption == (UINT4) i4SetValFsDnsTraceOption)
    {
        return SNMP_SUCCESS;
    }
    gDnsGlobalInfo.u4TraceOption = (UINT4) i4SetValFsDnsTraceOption;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDnsQueryRetryCount
 Input       :  The Indices

                The Object 
                setValFsDnsQueryRetryCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDnsQueryRetryCount (UINT4 u4SetValFsDnsQueryRetryCount)
{
    if (gDnsResolverParams.u4QueryRetryCount == u4SetValFsDnsQueryRetryCount)
    {
        return SNMP_SUCCESS;
    }

    gDnsResolverParams.u4QueryRetryCount = u4SetValFsDnsQueryRetryCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDnsQueryTimeOut
 Input       :  The Indices

                The Object 
                setValFsDnsQueryTimeOut
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDnsQueryTimeOut (UINT4 u4SetValFsDnsQueryTimeOut)
{
    if (gDnsResolverParams.u4QueryTimeout == u4SetValFsDnsQueryTimeOut)
    {
        return SNMP_SUCCESS;
    }
    gDnsResolverParams.u4QueryTimeout = u4SetValFsDnsQueryTimeOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDnsResolverMode
 Input       :  The Indices

                The Object 
                setValFsDnsResolverMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDnsResolverMode (INT4 i4SetValFsDnsResolverMode)
{
    gDnsResolverParams.i4QueryMode = i4SetValFsDnsResolverMode;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDnsPreferentialType
 Input       :  The Indices

                The Object 
                setValFsDnsPreferentialType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDnsPreferentialType (INT4 i4SetValFsDnsPreferentialType)
{
    gDnsResolverParams.i4QueryType = i4SetValFsDnsPreferentialType;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDnsSystemControl
 Input       :  The Indices

                The Object 
                testValFsDnsSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDnsSystemControl (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsDnsSystemControl)
{
    if ((i4TestValFsDnsSystemControl != DNS_START)
        && (i4TestValFsDnsSystemControl != DNS_SHUTDOWN))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (DNS_CLI_INVALID_SYSTEM_CONTROL);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDnsModuleStatus
 Input       :  The Indices

                The Object 
                testValFsDnsModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDnsModuleStatus (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsDnsModuleStatus)
{
    if (gDnsGlobalInfo.u1SystemCtrlStatus != DNS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (DNS_CLI_SHUTDOWN);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDnsModuleStatus != DNS_ENABLE) &&
        (i4TestValFsDnsModuleStatus != DNS_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (DNS_CLI_INVALID_MODULE_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDnsTraceOption
 Input       :  The Indices

                The Object 
                testValFsDnsTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDnsTraceOption (UINT4 *pu4ErrorCode, INT4 i4TestValFsDnsTraceOption)
{
    if (gDnsGlobalInfo.u1SystemCtrlStatus != DNS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (DNS_CLI_SHUTDOWN);
        return SNMP_FAILURE;
    }
    if (i4TestValFsDnsTraceOption > DNS_ALL_TRC)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (DNS_CLI_INVALID_TRACE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDnsQueryRetryCount
 Input       :  The Indices

                The Object 
                testValFsDnsQueryRetryCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDnsQueryRetryCount (UINT4 *pu4ErrorCode,
                               UINT4 u4TestValFsDnsQueryRetryCount)
{
    if (gDnsGlobalInfo.u1SystemCtrlStatus != DNS_START)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (DNS_CLI_SHUTDOWN);
        return SNMP_FAILURE;
    }
    if (u4TestValFsDnsQueryRetryCount > DNS_MAX_RETRY_COUNT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (DNS_CLI_INVALID_RETRY_COUNT);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDnsQueryTimeOut
 Input       :  The Indices

                The Object 
                testValFsDnsQueryTimeOut
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDnsQueryTimeOut (UINT4 *pu4ErrorCode,
                            UINT4 u4TestValFsDnsQueryTimeOut)
{
    if (gDnsGlobalInfo.u1SystemCtrlStatus != DNS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (DNS_CLI_SHUTDOWN);
        return SNMP_FAILURE;
    }
    if ((u4TestValFsDnsQueryTimeOut < DNS_MIN_TIMEOUT) ||
        (u4TestValFsDnsQueryTimeOut > DNS_MAX_TIMEOUT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (DNS_CLI_INVALID_TIMEOUT);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDnsResolverMode
 Input       :  The Indices

                The Object 
                testValFsDnsResolverMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDnsResolverMode (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsDnsResolverMode)
{
    if (gDnsGlobalInfo.u1SystemCtrlStatus != DNS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (DNS_CLI_SHUTDOWN);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDnsResolverMode != DNS_MODE_SIMUL)
        && (i4TestValFsDnsResolverMode != DNS_MODE_SEQUENCE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (DNS_CLI_INVALID_MODE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDnsPreferentialType
 Input       :  The Indices

                The Object 
                testValFsDnsPreferentialType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDnsPreferentialType (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsDnsPreferentialType)
{
    if (gDnsGlobalInfo.u1SystemCtrlStatus != DNS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (DNS_CLI_SHUTDOWN);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDnsPreferentialType < DNS_v4_RESOLVE)
        || (i4TestValFsDnsPreferentialType > DNS_v4v6_RESOLVE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (DNS_CLI_INVALID_TYPE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDnsSystemControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDnsSystemControl (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDnsModuleStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDnsModuleStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDnsTraceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDnsTraceOption (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDnsQueryRetryCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDnsQueryRetryCount (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDnsQueryTimeOut
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDnsQueryTimeOut (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDnsResolverMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDnsResolverMode (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDnsPreferentialType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDnsPreferentialType (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDnsNameServerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDnsNameServerTable
 Input       :  The Indices
                FsDnsNameServerIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDnsNameServerTable (UINT4 u4FsDnsNameServerIndex)
{
    if (u4FsDnsNameServerIndex <= 0)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Name Server Index \r\n"));
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDnsNameServerTable
 Input       :  The Indices
                FsDnsNameServerIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDnsNameServerTable (UINT4 *pu4FsDnsNameServerIndex)
{
    tDnsNameServerInfo *pDnsNameServerInfo = NULL;

    pDnsNameServerInfo = (tDnsNameServerInfo *)
        TMO_SLL_First (&(gDnsGlobalInfo.NameServerList));

    if (pDnsNameServerInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Name Server List is Empty \r\n"));
        return SNMP_FAILURE;
    }

    *pu4FsDnsNameServerIndex = pDnsNameServerInfo->u4NameServerIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDnsNameServerTable
 Input       :  The Indices
                FsDnsNameServerIndex
                nextFsDnsNameServerIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDnsNameServerTable (UINT4 u4FsDnsNameServerIndex,
                                     UINT4 *pu4NextFsDnsNameServerIndex)
{
    tDnsNameServerInfo *pDnsNameServerInfo = NULL;
    tDnsNameServerInfo *pNextDnsNameServerInfo = NULL;

    pDnsNameServerInfo = DnsGetNameServer (u4FsDnsNameServerIndex);
    if (pDnsNameServerInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Name server Index \r\n"));
        return SNMP_FAILURE;
    }

    pNextDnsNameServerInfo = TMO_SLL_Next (&(gDnsGlobalInfo.NameServerList),
                                           &
                                           (pDnsNameServerInfo->
                                            NameServerNode));

    if (pNextDnsNameServerInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4NextFsDnsNameServerIndex = pNextDnsNameServerInfo->u4NameServerIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDnsServerIPAddressType
 Input       :  The Indices
                FsDnsNameServerIndex

                The Object 
                retValFsDnsServerIPAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDnsServerIPAddressType (UINT4 u4FsDnsNameServerIndex,
                                INT4 *pi4RetValFsDnsServerIPAddressType)
{
    tDnsNameServerInfo *pDnsNameServerInfo = NULL;

    pDnsNameServerInfo = DnsGetNameServer (u4FsDnsNameServerIndex);
    if (pDnsNameServerInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Name server Index \r\n"));
        return SNMP_FAILURE;
    }

    *pi4RetValFsDnsServerIPAddressType = pDnsNameServerInfo->NameServerIP.u1Afi;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDnsServerIPAddress
 Input       :  The Indices
                FsDnsNameServerIndex

                The Object 
                retValFsDnsServerIPAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDnsServerIPAddress (UINT4 u4FsDnsNameServerIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsDnsServerIPAddress)
{
    tDnsNameServerInfo *pDnsNameServerInfo = NULL;

    pDnsNameServerInfo = DnsGetNameServer (u4FsDnsNameServerIndex);
    if (pDnsNameServerInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Name server Index \r\n"));
        return SNMP_FAILURE;
    }

    MEMSET (pRetValFsDnsServerIPAddress->pu1_OctetList, 0,
            IPVX_MAX_INET_ADDR_LEN);
    MEMCPY (pRetValFsDnsServerIPAddress->pu1_OctetList,
            pDnsNameServerInfo->NameServerIP.au1Addr,
            pDnsNameServerInfo->NameServerIP.u1AddrLen);
    pRetValFsDnsServerIPAddress->i4_Length =
        pDnsNameServerInfo->NameServerIP.u1AddrLen;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsDnsNameServerRowStatus
 Input       :  The Indices
                FsDnsNameServerIndex

                The Object 
                retValFsDnsNameServerRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDnsNameServerRowStatus (UINT4 u4FsDnsNameServerIndex,
                                INT4 *pi4RetValFsDnsNameServerRowStatus)
{
    tDnsNameServerInfo *pDnsNameServerInfo = NULL;

    pDnsNameServerInfo = DnsGetNameServer (u4FsDnsNameServerIndex);
    if (pDnsNameServerInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Name server Index \r\n"));
        return SNMP_FAILURE;
    }

    *pi4RetValFsDnsNameServerRowStatus = (INT4) pDnsNameServerInfo->u4RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDnsServerIPAddressType
 Input       :  The Indices
                FsDnsNameServerIndex

                The Object 
                setValFsDnsServerIPAddressType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDnsServerIPAddressType (UINT4 u4FsDnsNameServerIndex,
                                INT4 i4SetValFsDnsServerIPAddressType)
{
    tDnsNameServerInfo *pDnsNameServerInfo = NULL;

    pDnsNameServerInfo = DnsGetNameServer (u4FsDnsNameServerIndex);
    if (pDnsNameServerInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Name server Index \r\n"));
        return SNMP_FAILURE;
    }

    pDnsNameServerInfo->NameServerIP.u1Afi =
        (UINT1) i4SetValFsDnsServerIPAddressType;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsDnsServerIPAddress
 Input       :  The Indices
                FsDnsNameServerIndex

                The Object 
                setValFsDnsServerIPAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDnsServerIPAddress (UINT4 u4FsDnsNameServerIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsDnsServerIPAddress)
{
    tDnsNameServerInfo *pDnsNameServerInfo = NULL;

    pDnsNameServerInfo = DnsGetNameServer (u4FsDnsNameServerIndex);
    if (pDnsNameServerInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Name server Index \r\n"));
        return SNMP_FAILURE;
    }

    MEMSET (pDnsNameServerInfo->NameServerIP.au1Addr, 0,
            IPVX_MAX_INET_ADDR_LEN);
    MEMCPY (pDnsNameServerInfo->NameServerIP.au1Addr,
            pSetValFsDnsServerIPAddress->pu1_OctetList,
            pSetValFsDnsServerIPAddress->i4_Length);
    pDnsNameServerInfo->NameServerIP.u1AddrLen =
        (UINT1) pSetValFsDnsServerIPAddress->i4_Length;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDnsNameServerRowStatus
 Input       :  The Indices
                FsDnsNameServerIndex

                The Object 
                setValFsDnsNameServerRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDnsNameServerRowStatus (UINT4 u4FsDnsNameServerIndex,
                                INT4 i4SetValFsDnsNameServerRowStatus)
{
    tDnsNameServerInfo *pDnsNameServerInfo = NULL;

    switch (i4SetValFsDnsNameServerRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:

            IndexManagerSetIndex (gDnsIndexMgrListInfo.NameServerIndexList,
                                  u4FsDnsNameServerIndex);
            pDnsNameServerInfo =
                MemAllocMemBlk (gDnsGlobalInfo.NameServerPoolId);
            if (pDnsNameServerInfo == NULL)
            {
                DNS_TRACE ((DNS_FAILURE_TRC,
                            " Memory Allocation for Name Server Entry Fails \r\n"));
                return SNMP_FAILURE;
            }
            MEMSET (pDnsNameServerInfo, 0, sizeof (tDnsNameServerInfo));
            TMO_SLL_Init_Node (&pDnsNameServerInfo->NameServerNode);
            pDnsNameServerInfo->u4NameServerIndex = u4FsDnsNameServerIndex;
            if (DnsAddNameServertoList (pDnsNameServerInfo) == DNS_FAILURE)
            {
                MemReleaseMemBlock (gDnsGlobalInfo.NameServerPoolId,
                                    (UINT1 *) pDnsNameServerInfo);
                DNS_TRACE ((DNS_FAILURE_TRC,
                            " Addition of Name Server to List Fails \r\n"));
                return SNMP_FAILURE;
            }
            if (i4SetValFsDnsNameServerRowStatus == CREATE_AND_GO)
            {
                pDnsNameServerInfo->u4RowStatus = ACTIVE;
            }
            break;

        case ACTIVE:
        case NOT_IN_SERVICE:
        case NOT_READY:
            pDnsNameServerInfo = DnsGetNameServer (u4FsDnsNameServerIndex);
            if (pDnsNameServerInfo == NULL)
            {
                DNS_TRACE ((DNS_FAILURE_TRC,
                            " Invalid Name server Index \r\n"));
                return SNMP_FAILURE;
            }
            pDnsNameServerInfo->u4RowStatus =
                (UINT4) i4SetValFsDnsNameServerRowStatus;
            break;

        case DESTROY:
            pDnsNameServerInfo = DnsGetNameServer (u4FsDnsNameServerIndex);
            if (pDnsNameServerInfo == NULL)
            {
                DNS_TRACE ((DNS_FAILURE_TRC,
                            " Invalid Name server Index \r\n"));
                return SNMP_FAILURE;
            }
            DnsDelNameServerFromList (pDnsNameServerInfo);
            break;

        default:
            return SNMP_FAILURE;
            break;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDnsServerIPAddressType
 Input       :  The Indices
                FsDnsNameServerIndex

                The Object 
                testValFsDnsServerIPAddressType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDnsServerIPAddressType (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsDnsNameServerIndex,
                                   INT4 i4TestValFsDnsServerIPAddressType)
{
    tDnsNameServerInfo *pDnsNameServerInfo = NULL;

    if (gDnsGlobalInfo.u1SystemCtrlStatus != DNS_START)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Dns Module is Shutdown \r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (DNS_CLI_SHUTDOWN);
        return SNMP_FAILURE;
    }

    if (u4FsDnsNameServerIndex <= DNS_ZERO)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Invalid Name Server Index\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pDnsNameServerInfo = DnsGetNameServer (u4FsDnsNameServerIndex);
    if (pDnsNameServerInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Name server Index \r\n"));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (DNS_CLI_NO_NS_ENTRY);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDnsServerIPAddressType != IPVX_ADDR_FMLY_IPV4) &&
        (i4TestValFsDnsServerIPAddressType != IPVX_ADDR_FMLY_IPV6))
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Name server IP Addr type \r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (DNS_INVALID_NS_IP_TYPE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDnsServerIPAddress
 Input       :  The Indices
                FsDnsNameServerIndex

                The Object 
                testValFsDnsServerIPAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDnsServerIPAddress (UINT4 *pu4ErrorCode,
                               UINT4 u4FsDnsNameServerIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsDnsServerIPAddress)
{
    tDnsNameServerInfo *pDnsNameServerInfo = NULL;
    UINT4               u4IPAddr = 0;
    UINT1               au1TempIPAddr[IPVX_IPV6_ADDR_LEN];
    tIp6Addr            TempAddr;

    MEMSET (au1TempIPAddr, 0, IPVX_IPV6_ADDR_LEN);

    if (gDnsGlobalInfo.u1SystemCtrlStatus != DNS_START)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Dns Module is Shutdown \r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (DNS_CLI_SHUTDOWN);
        return SNMP_FAILURE;
    }

    if (u4FsDnsNameServerIndex <= DNS_ZERO)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Invalid Name Server Index\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pDnsNameServerInfo = DnsGetNameServer (u4FsDnsNameServerIndex);
    if (pDnsNameServerInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Name server Index \r\n"));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (DNS_CLI_NO_NS_ENTRY);
        return SNMP_FAILURE;
    }

    if (MEMCMP (pTestValFsDnsServerIPAddress->pu1_OctetList, au1TempIPAddr,
                pTestValFsDnsServerIPAddress->i4_Length) == 0)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Name server IP \r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (DNS_INVALID_NS_IP);
        return SNMP_FAILURE;
    }

    if (pTestValFsDnsServerIPAddress->i4_Length == IPVX_IPV4_ADDR_LEN)
    {
        PTR_FETCH4 (u4IPAddr, pTestValFsDnsServerIPAddress->pu1_OctetList);

        if (!
            (((ISS_IS_ADDR_CLASS_A (u4IPAddr))
              || (ISS_IS_ADDR_CLASS_B (u4IPAddr))
              || (ISS_IS_ADDR_CLASS_C (u4IPAddr)))
             && (ISS_IS_ADDR_VALID (u4IPAddr))))
        {
            DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Name server IP \r\n"));
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (DNS_INVALID_NS_IP);
            return SNMP_FAILURE;
        }

    }
    else if (pTestValFsDnsServerIPAddress->i4_Length == IPVX_IPV6_ADDR_LEN)
    {
        MEMCPY (&TempAddr, pTestValFsDnsServerIPAddress->pu1_OctetList,
                IPVX_IPV6_ADDR_LEN);
        if (IS_ADDR_UNSPECIFIED (TempAddr) || IS_ADDR_MULTI (TempAddr)
            || IS_ADDR_LOOPBACK (TempAddr))
        {
            CLI_SET_ERR (DNS_INVALID_NS_IP);
            return SNMP_FAILURE;
        }
    }
    else
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Name server IP \r\n"));
        CLI_SET_ERR (DNS_INVALID_NS_IP);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (DnsDuplicateNSIP (pTestValFsDnsServerIPAddress->pu1_OctetList) !=
        DNS_SUCCESS)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Duplicate Name Server IP Address \r\n"));
        CLI_SET_ERR (DNS_CLI_DUPLICATE_NS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsDnsNameServerRowStatus
 Input       :  The Indices
                FsDnsNameServerIndex

                The Object 
                testValFsDnsNameServerRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDnsNameServerRowStatus (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsDnsNameServerIndex,
                                   INT4 i4TestValFsDnsNameServerRowStatus)
{
    tDnsNameServerInfo *pDnsNameServerInfo = NULL;

    if (gDnsGlobalInfo.u1SystemCtrlStatus != DNS_START)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Dns Module is Shutdown \r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (DNS_CLI_SHUTDOWN);
        return SNMP_FAILURE;
    }

    if (u4FsDnsNameServerIndex <= DNS_ZERO)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Invalid Name Server Index\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pDnsNameServerInfo = DnsGetNameServer (u4FsDnsNameServerIndex);

    if ((i4TestValFsDnsNameServerRowStatus == CREATE_AND_GO) ||
        (i4TestValFsDnsNameServerRowStatus == CREATE_AND_WAIT))
    {
        if (pDnsNameServerInfo != NULL)
        {
            DNS_TRACE ((DNS_FAILURE_TRC,
                        " Name Server Entry already exists \r\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (DNS_CLI_INVALID_NS_ENTRY);
            return SNMP_FAILURE;
        }
    }
    else if ((i4TestValFsDnsNameServerRowStatus == ACTIVE) ||
             (i4TestValFsDnsNameServerRowStatus == NOT_READY) ||
             (i4TestValFsDnsNameServerRowStatus == NOT_IN_SERVICE) ||
             (i4TestValFsDnsNameServerRowStatus == DESTROY))
    {
        if (pDnsNameServerInfo == NULL)
        {
            DNS_TRACE ((DNS_FAILURE_TRC,
                        " Name Server Entry does not exists \r\n"));
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (DNS_CLI_NO_NS_ENTRY);
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (DNS_CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDnsNameServerTable
 Input       :  The Indices
                FsDnsNameServerIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDnsNameServerTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDnsDomainNameTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDnsDomainNameTable
 Input       :  The Indices
                FsDnsDomainNameIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDnsDomainNameTable (UINT4 u4FsDnsDomainNameIndex)
{
    if (u4FsDnsDomainNameIndex <= 0)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Domain name Index \r\n"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDnsDomainNameTable
 Input       :  The Indices
                FsDnsDomainNameIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDnsDomainNameTable (UINT4 *pu4FsDnsDomainNameIndex)
{
    tDnsDomainNameInfo *pDnsDomainNameInfo = NULL;

    pDnsDomainNameInfo = (tDnsDomainNameInfo *)
        TMO_SLL_First (&(gDnsGlobalInfo.DomainNameList));
    if (pDnsDomainNameInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Domain name List is empty \r\n"));
        return SNMP_FAILURE;
    }

    *pu4FsDnsDomainNameIndex = pDnsDomainNameInfo->u4DomainNameIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDnsDomainNameTable
 Input       :  The Indices
                FsDnsDomainNameIndex
                nextFsDnsDomainNameIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDnsDomainNameTable (UINT4 u4FsDnsDomainNameIndex,
                                     UINT4 *pu4NextFsDnsDomainNameIndex)
{
    tDnsDomainNameInfo *pDnsDomainNameInfo = NULL;
    tDnsDomainNameInfo *pDnsNextDomainNameInfo = NULL;

    pDnsDomainNameInfo = DnsGetDomainName (u4FsDnsDomainNameIndex);
    if (pDnsDomainNameInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Domain name Index \r\n"));
        return SNMP_FAILURE;
    }
    pDnsNextDomainNameInfo = TMO_SLL_Next (&(gDnsGlobalInfo.DomainNameList),
                                           &
                                           (pDnsDomainNameInfo->
                                            DomainNameNode));
    if (pDnsNextDomainNameInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4NextFsDnsDomainNameIndex = pDnsNextDomainNameInfo->u4DomainNameIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDnsDomainName
 Input       :  The Indices
                FsDnsDomainNameIndex

                The Object 
                retValFsDnsDomainName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDnsDomainName (UINT4 u4FsDnsDomainNameIndex,
                       tSNMP_OCTET_STRING_TYPE * pRetValFsDnsDomainName)
{
    tDnsDomainNameInfo *pDnsDomainNameInfo = NULL;

    pDnsDomainNameInfo = DnsGetDomainName (u4FsDnsDomainNameIndex);
    if (pDnsDomainNameInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Domain name Index \r\n"));
        return SNMP_FAILURE;
    }

    MEMSET (pRetValFsDnsDomainName->pu1_OctetList, 0, DNS_MAX_DOMAIN_NAME_LEN);
    MEMCPY (pRetValFsDnsDomainName->pu1_OctetList,
            pDnsDomainNameInfo->au1DomainName,
            STRLEN (pDnsDomainNameInfo->au1DomainName));
    pRetValFsDnsDomainName->i4_Length =
        (INT4) STRLEN (pDnsDomainNameInfo->au1DomainName);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDnsDomainNameRowStatus
 Input       :  The Indices
                FsDnsDomainNameIndex

                The Object 
                retValFsDnsDomainNameRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDnsDomainNameRowStatus (UINT4 u4FsDnsDomainNameIndex,
                                INT4 *pi4RetValFsDnsDomainNameRowStatus)
{
    tDnsDomainNameInfo *pDnsDomainNameInfo = NULL;

    pDnsDomainNameInfo = DnsGetDomainName (u4FsDnsDomainNameIndex);
    if (pDnsDomainNameInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Domain name Index \r\n"));
        return SNMP_FAILURE;
    }

    *pi4RetValFsDnsDomainNameRowStatus = (INT4) pDnsDomainNameInfo->u4RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDnsDomainName
 Input       :  The Indices
                FsDnsDomainNameIndex

                The Object 
                setValFsDnsDomainName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDnsDomainName (UINT4 u4FsDnsDomainNameIndex,
                       tSNMP_OCTET_STRING_TYPE * pSetValFsDnsDomainName)
{
    tDnsDomainNameInfo *pDnsDomainNameInfo = NULL;

    pDnsDomainNameInfo = DnsGetDomainName (u4FsDnsDomainNameIndex);
    if (pDnsDomainNameInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Domain name Index \r\n"));
        return SNMP_FAILURE;
    }

    MEMSET (pDnsDomainNameInfo->au1DomainName, 0, DNS_MAX_DOMAIN_NAME_LEN);
    MEMCPY (pDnsDomainNameInfo->au1DomainName,
            pSetValFsDnsDomainName->pu1_OctetList,
            pSetValFsDnsDomainName->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDnsDomainNameRowStatus
 Input       :  The Indices
                FsDnsDomainNameIndex

                The Object 
                setValFsDnsDomainNameRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDnsDomainNameRowStatus (UINT4 u4FsDnsDomainNameIndex,
                                INT4 i4SetValFsDnsDomainNameRowStatus)
{
    tDnsDomainNameInfo *pDnsDomainNameInfo = NULL;

    switch (i4SetValFsDnsDomainNameRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            IndexManagerSetIndex (gDnsIndexMgrListInfo.DomainNameIndexList,
                                  u4FsDnsDomainNameIndex);
            pDnsDomainNameInfo =
                MemAllocMemBlk (gDnsGlobalInfo.DomainNamePoolId);
            if (pDnsDomainNameInfo == NULL)
            {
                DNS_TRACE ((DNS_FAILURE_TRC,
                            " Memory Allocation of Domain name node Fails \r\n"));
                return SNMP_FAILURE;
            }

            MEMSET (pDnsDomainNameInfo, 0, sizeof (tDnsDomainNameInfo));
            TMO_SLL_Init_Node (&pDnsDomainNameInfo->DomainNameNode);
            pDnsDomainNameInfo->u4DomainNameIndex = u4FsDnsDomainNameIndex;
            pDnsDomainNameInfo->u4RowStatus =
                (UINT4) i4SetValFsDnsDomainNameRowStatus;
            if (DnsAddDomainNametoList (pDnsDomainNameInfo) == DNS_FAILURE)
            {
                DNS_TRACE ((DNS_FAILURE_TRC,
                            " Adding Domain name to the list fails \r\n"));
                MemReleaseMemBlock (gDnsGlobalInfo.DomainNamePoolId,
                                    (UINT1 *) pDnsDomainNameInfo);
                return SNMP_FAILURE;
            }
            if (i4SetValFsDnsDomainNameRowStatus == CREATE_AND_GO)
            {
                pDnsDomainNameInfo->u4RowStatus = ACTIVE;
            }
            break;

        case ACTIVE:
        case NOT_READY:
        case NOT_IN_SERVICE:
            pDnsDomainNameInfo = DnsGetDomainName (u4FsDnsDomainNameIndex);
            if (pDnsDomainNameInfo == NULL)
            {
                DNS_TRACE ((DNS_FAILURE_TRC,
                            " Invalid Domain name Index \r\n"));
                return SNMP_FAILURE;
            }
            pDnsDomainNameInfo->u4RowStatus =
                (UINT4) i4SetValFsDnsDomainNameRowStatus;
            break;
        case DESTROY:
            pDnsDomainNameInfo = DnsGetDomainName (u4FsDnsDomainNameIndex);
            if (pDnsDomainNameInfo == NULL)
            {
                DNS_TRACE ((DNS_FAILURE_TRC,
                            " Invalid Domain name Index \r\n"));
                return SNMP_FAILURE;
            }
            DnsDelDomainNameFromList (pDnsDomainNameInfo);
            break;
        default:
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDnsDomainName
 Input       :  The Indices
                FsDnsDomainNameIndex

                The Object 
                testValFsDnsDomainName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDnsDomainName (UINT4 *pu4ErrorCode, UINT4 u4FsDnsDomainNameIndex,
                          tSNMP_OCTET_STRING_TYPE * pTestValFsDnsDomainName)
{
    tDnsDomainNameInfo *pDnsDomainNameInfo = NULL;

    if (gDnsGlobalInfo.u1SystemCtrlStatus != DNS_START)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Dns Module is Shutdown \r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (DNS_CLI_SHUTDOWN);
        return SNMP_FAILURE;
    }

    if (u4FsDnsDomainNameIndex <= DNS_ZERO)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Invalid Domain Name Index\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pDnsDomainNameInfo = DnsGetDomainName (u4FsDnsDomainNameIndex);
    if (pDnsDomainNameInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Domain name Index \r\n"));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (DNS_CLI_NO_DOMAIN_NAME_ENTRY);
        return SNMP_FAILURE;
    }

    if (pTestValFsDnsDomainName->pu1_OctetList == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Domain name value\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (DNS_CLI_INVALID_DOMAIN_NAME);
        return SNMP_FAILURE;
    }

    if ((STRLEN (pTestValFsDnsDomainName->pu1_OctetList) == DNS_ZERO) ||
        (STRLEN (pTestValFsDnsDomainName->pu1_OctetList) >=
         DNS_MAX_DOMAIN_NAME_LEN))
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Domain name value\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (DNS_CLI_INVALID_DOMAIN_NAME);
        return SNMP_FAILURE;
    }

    if (DnsDupDomainName (pTestValFsDnsDomainName->pu1_OctetList) !=
        DNS_SUCCESS)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Domain name already exists\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (DNS_CLI_INVALID_DOMAIN_NAME_ENTRY);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsDnsDomainNameRowStatus
 Input       :  The Indices
                FsDnsDomainNameIndex

                The Object 
                testValFsDnsDomainNameRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDnsDomainNameRowStatus (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsDnsDomainNameIndex,
                                   INT4 i4TestValFsDnsDomainNameRowStatus)
{
    tDnsDomainNameInfo *pDnsDomainNameInfo = NULL;

    if (gDnsGlobalInfo.u1SystemCtrlStatus != DNS_START)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Dns Module is Shutdown \r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (DNS_CLI_SHUTDOWN);
        return SNMP_FAILURE;
    }

    if (u4FsDnsDomainNameIndex <= DNS_ZERO)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Invalid Domain Name Index\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pDnsDomainNameInfo = DnsGetDomainName (u4FsDnsDomainNameIndex);

    if ((i4TestValFsDnsDomainNameRowStatus == CREATE_AND_GO) ||
        (i4TestValFsDnsDomainNameRowStatus == CREATE_AND_WAIT))
    {
        if (pDnsDomainNameInfo != NULL)
        {
            DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Domain name Index \r\n"));
            CLI_SET_ERR (DNS_CLI_INVALID_DOMAIN_NAME_ENTRY);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    else if ((i4TestValFsDnsDomainNameRowStatus == ACTIVE) ||
             (i4TestValFsDnsDomainNameRowStatus == NOT_IN_SERVICE) ||
             (i4TestValFsDnsDomainNameRowStatus == NOT_READY) ||
             (i4TestValFsDnsDomainNameRowStatus == DESTROY))
    {
        if (pDnsDomainNameInfo == NULL)
        {
            DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Domain name Index \r\n"));
            CLI_SET_ERR (DNS_CLI_NO_DOMAIN_NAME_ENTRY);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    else
    {
        CLI_SET_ERR (DNS_CLI_INVALID_VALUE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDnsDomainNameTable
 Input       :  The Indices
                FsDnsDomainNameIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDnsDomainNameTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDnsQueryTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDnsQueryTable
 Input       :  The Indices
                FsDnsQueryIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDnsQueryTable (UINT4 u4FsDnsQueryIndex)
{
    if (u4FsDnsQueryIndex <= 0)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Pending Query Index \r\n"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDnsQueryTable
 Input       :  The Indices
                FsDnsQueryIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDnsQueryTable (UINT4 *pu4FsDnsQueryIndex)
{
    tDnsQueryInfo      *pDnsQueryInfo = NULL;

    pDnsQueryInfo = RBTreeGetFirst (gDnsGlobalInfo.PendingQueryList);
    if (pDnsQueryInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Pending Query Index \r\n"));
        return SNMP_FAILURE;
    }
    *pu4FsDnsQueryIndex = pDnsQueryInfo->u2QueryIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDnsQueryTable
 Input       :  The Indices
                FsDnsQueryIndex
                nextFsDnsQueryIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDnsQueryTable (UINT4 u4FsDnsQueryIndex,
                                UINT4 *pu4NextFsDnsQueryIndex)
{
    tDnsQueryInfo      *pDnsQueryInfo = NULL;
    tDnsQueryInfo       DnsQueryInfo;

    MEMSET (&DnsQueryInfo, 0, sizeof (tDnsQueryInfo));

    DnsQueryInfo.u2QueryIndex = (UINT2) u4FsDnsQueryIndex;

    pDnsQueryInfo =
        RBTreeGetNext (gDnsGlobalInfo.PendingQueryList, &DnsQueryInfo, NULL);
    if (pDnsQueryInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4NextFsDnsQueryIndex = pDnsQueryInfo->u2QueryIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDnsQueryName
 Input       :  The Indices
                FsDnsQueryIndex

                The Object 
                retValFsDnsQueryName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDnsQueryName (UINT4 u4FsDnsQueryIndex,
                      tSNMP_OCTET_STRING_TYPE * pRetValFsDnsQueryName)
{
    tDnsQueryInfo      *pDnsQueryInfo = NULL;
    tDnsQueryInfo       DnsQueryInfo;

    MEMSET (&DnsQueryInfo, 0, sizeof (tDnsQueryInfo));

    DnsQueryInfo.u2QueryIndex = (UINT2) u4FsDnsQueryIndex;

    pDnsQueryInfo = RBTreeGet (gDnsGlobalInfo.PendingQueryList, &DnsQueryInfo);
    if (pDnsQueryInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Pending Query Index \r\n"));
        return SNMP_FAILURE;
    }

    MEMSET (pRetValFsDnsQueryName->pu1_OctetList, 0, DNS_MAX_QUERY_LEN);
    MEMCPY (pRetValFsDnsQueryName->pu1_OctetList, pDnsQueryInfo->au1QueryName,
            STRLEN (pDnsQueryInfo->au1QueryName));
    pRetValFsDnsQueryName->i4_Length =
        (INT4) STRLEN (pDnsQueryInfo->au1QueryName);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDnsQueryNSAddressType
 Input       :  The Indices
                FsDnsQueryIndex

                The Object 
                retValFsDnsQueryNSAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDnsQueryNSAddressType (UINT4 u4FsDnsQueryIndex,
                               INT4 *pi4RetValFsDnsQueryNSAddressType)
{
    tDnsQueryInfo      *pDnsQueryInfo = NULL;
    tDnsQueryInfo       DnsQueryInfo;

    MEMSET (&DnsQueryInfo, 0, sizeof (tDnsQueryInfo));

    DnsQueryInfo.u2QueryIndex = (UINT2) u4FsDnsQueryIndex;

    pDnsQueryInfo = RBTreeGet (gDnsGlobalInfo.PendingQueryList, &DnsQueryInfo);
    if (pDnsQueryInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Pending Query Index \r\n"));
        return SNMP_FAILURE;
    }

    *pi4RetValFsDnsQueryNSAddressType = pDnsQueryInfo->QueryNameServerIP.u1Afi;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDnsQueryNSAddress
 Input       :  The Indices
                FsDnsQueryIndex

                The Object 
                retValFsDnsQueryNSAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDnsQueryNSAddress (UINT4 u4FsDnsQueryIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValFsDnsQueryNSAddress)
{
    tDnsQueryInfo      *pDnsQueryInfo = NULL;
    tDnsQueryInfo       DnsQueryInfo;

    MEMSET (&DnsQueryInfo, 0, sizeof (tDnsQueryInfo));

    DnsQueryInfo.u2QueryIndex = (UINT2) u4FsDnsQueryIndex;

    pDnsQueryInfo = RBTreeGet (gDnsGlobalInfo.PendingQueryList, &DnsQueryInfo);
    if (pDnsQueryInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Invalid Pending Query Index \r\n"));
        return SNMP_FAILURE;
    }

    MEMSET (pRetValFsDnsQueryNSAddress->pu1_OctetList, 0,
            IPVX_MAX_INET_ADDR_LEN);
    MEMCPY (pRetValFsDnsQueryNSAddress->pu1_OctetList,
            pDnsQueryInfo->QueryNameServerIP.au1Addr,
            pDnsQueryInfo->QueryNameServerIP.u1AddrLen);
    pRetValFsDnsQueryNSAddress->i4_Length =
        pDnsQueryInfo->QueryNameServerIP.u1AddrLen;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDnsQueriesSent
 Input       :  The Indices

                The Object 
                retValFsDnsQueriesSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDnsQueriesSent (UINT4 *pu4RetValFsDnsQueriesSent)
{
    *pu4RetValFsDnsQueriesSent = gDnsStatistics.u4TotalQuerySent;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDnsResponseReceived
 Input       :  The Indices

                The Object 
                retValFsDnsResponseReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDnsResponseReceived (UINT4 *pu4RetValFsDnsResponseReceived)
{
    *pu4RetValFsDnsResponseReceived = gDnsStatistics.u4TotalResponseReceived;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDnsDroppedResponse
 Input       :  The Indices

                The Object 
                retValFsDnsDroppedResponse
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDnsDroppedResponse (UINT4 *pu4RetValFsDnsDroppedResponse)
{
    *pu4RetValFsDnsDroppedResponse = gDnsStatistics.u4DroppedResponse;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDnsUnAnsweredQueries
 Input       :  The Indices

                The Object 
                retValFsDnsUnAnsweredQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDnsUnAnsweredQueries (UINT4 *pu4RetValFsDnsUnAnsweredQueries)
{
    *pu4RetValFsDnsUnAnsweredQueries = gDnsStatistics.u4UnansQueries;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDnsFailedQueries
 Input       :  The Indices

                The Object 
                retValFsDnsFailedQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDnsFailedQueries (UINT4 *pu4RetValFsDnsFailedQueries)
{
    *pu4RetValFsDnsFailedQueries = gDnsStatistics.u4FailedQueries;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDnsReTransQueries
 Input       :  The Indices

                The Object 
                retValFsDnsReTransQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDnsReTransQueries (UINT4 *pu4RetValFsDnsReTransQueries)
{
    *pu4RetValFsDnsReTransQueries = gDnsStatistics.u4RetransQueries;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDnsResCacheRRTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDnsResCacheRRTable
 Input       :  The Indices
                DnsResCacheRRName
                DnsResCacheRRClass
                DnsResCacheRRType
                DnsResCacheRRIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDnsResCacheRRTable (tSNMP_OCTET_STRING_TYPE
                                              * pDnsResCacheRRName,
                                              INT4 i4DnsResCacheRRClass,
                                              INT4 i4DnsResCacheRRType,
                                              INT4 i4DnsResCacheRRIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhValidateIndexInstanceDnsResCacheRRTable (pDnsResCacheRRName,
                                                           i4DnsResCacheRRClass,
                                                           i4DnsResCacheRRType,
                                                           i4DnsResCacheRRIndex);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDnsResCacheRRTable
 Input       :  The Indices
                DnsResCacheRRName
                DnsResCacheRRClass
                DnsResCacheRRType
                DnsResCacheRRIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDnsResCacheRRTable (tSNMP_OCTET_STRING_TYPE *
                                      pDnsResCacheRRName,
                                      INT4 *pi4DnsResCacheRRClass,
                                      INT4 *pi4DnsResCacheRRType,
                                      INT4 *pi4DnsResCacheRRIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFirstIndexDnsResCacheRRTable (pDnsResCacheRRName,
                                                   pi4DnsResCacheRRClass,
                                                   pi4DnsResCacheRRType,
                                                   pi4DnsResCacheRRIndex);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDnsResCacheRRTable
 Input       :  The Indices
                DnsResCacheRRName
                nextDnsResCacheRRName
                DnsResCacheRRClass
                nextDnsResCacheRRClass
                DnsResCacheRRType
                nextDnsResCacheRRType
                DnsResCacheRRIndex
                nextDnsResCacheRRIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDnsResCacheRRTable (tSNMP_OCTET_STRING_TYPE *
                                     pDnsResCacheRRName,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextDnsResCacheRRName,
                                     INT4 i4DnsResCacheRRClass,
                                     INT4 *pi4NextDnsResCacheRRClass,
                                     INT4 i4DnsResCacheRRType,
                                     INT4 *pi4NextDnsResCacheRRType,
                                     INT4 i4DnsResCacheRRIndex,
                                     INT4 *pi4NextDnsResCacheRRIndex)
{

    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetNextIndexDnsResCacheRRTable (pDnsResCacheRRName,
                                                  pNextDnsResCacheRRName,
                                                  i4DnsResCacheRRClass,
                                                  pi4NextDnsResCacheRRClass,
                                                  i4DnsResCacheRRType,
                                                  pi4NextDnsResCacheRRType,
                                                  i4DnsResCacheRRIndex,
                                                  pi4NextDnsResCacheRRIndex);
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDnsResCacheRRSource
 Input       :  The Indices
                DnsResCacheRRName
                DnsResCacheRRClass
                DnsResCacheRRType
                DnsResCacheRRIndex

                The Object
                retValFsDnsResCacheRRSource
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDnsResCacheRRSource (tSNMP_OCTET_STRING_TYPE * pDnsResCacheRRName,
                             INT4 i4DnsResCacheRRClass,
                             INT4 i4DnsResCacheRRType,
                             INT4 i4DnsResCacheRRIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsDnsResCacheRRSource)
{

    tDnsCacheRRInfo    *pDnsCacheRRInfo = NULL;

    pDnsCacheRRInfo =
        DnsGetCacheRR (pDnsResCacheRRName->pu1_OctetList,
                       (UINT4) i4DnsResCacheRRClass,
                       (UINT4) i4DnsResCacheRRType,
                       (UINT4) i4DnsResCacheRRIndex);
    if (pDnsCacheRRInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Invalid Cache record indices\r\n"));
        return SNMP_FAILURE;
    }
if (i4DnsResCacheRRType == IPVX_ADDR_FMLY_IPV4)
{
    MEMCPY (pRetValFsDnsResCacheRRSource->pu1_OctetList,
            pDnsCacheRRInfo->au1RRData,
            IPVX_IPV4_ADDR_LEN);
    pRetValFsDnsResCacheRRSource->i4_Length =
        IPVX_IPV4_ADDR_LEN;
}
else
{
        MEMCPY (pRetValFsDnsResCacheRRSource->pu1_OctetList,
                            pDnsCacheRRInfo->au1RRData,
                                        IPVX_IPV6_ADDR_LEN);
            pRetValFsDnsResCacheRRSource->i4_Length =
                        IPVX_IPV6_ADDR_LEN;

}
    return SNMP_SUCCESS;
}
