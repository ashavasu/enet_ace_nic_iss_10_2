/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dnscli.c,v 1.25 2016/07/28 07:49:38 siva Exp $
 *
 * Description: This file contains action routines for set/get objects 
 *              in stddns.mib and fsdns.mib
 *
 *******************************************************************/
#define _DNS_CLI_C_

#include "dnsinc.h"
#include "stddnscli.h"
#include "fsdnscli.h"

/*****************************************************************************/
/*     FUNCTION NAME    : cli_process_dns_cmd                                */
/*                                                                           */
/*     DESCRIPTION      : This function takes in variable no. of arguments   */
/*                        and process the commands for DNS module as         */
/*                         defined in dnscmd.def                             */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Command -  Command Identifier                    */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS / CLI_FAILURE                          */
/*****************************************************************************/
PUBLIC UINT4
cli_process_dns_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *apu1args[CLI_DNS_MAX_ARGS];
    INT1                i1argno = DNS_ZERO;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT4               u4ErrorCode = DNS_ZERO;
    UINT4               u4Family = DNS_ZERO;
    UINT4               u4DomainNameIndex = DNS_ZERO;
    UINT4               u4IPAddr = 0;
    UINT1               au1IPAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1DomainName[DNS_MAX_DOMAIN_NAME_LEN];
    INT4                i4TraceValue = 0;
    INT4                i4Args = 0;
    tIp6Addr            Ip6Addr;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    MEMSET (au1DomainName, 0, DNS_MAX_DOMAIN_NAME_LEN);
    MEMSET (au1IPAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    va_start (ap, u4Command);

    /* Third argument is always interface name/index */
    va_arg (ap, INT4);

    while (1)
    {
        apu1args[i1argno++] = va_arg (ap, UINT1 *);

        if (i1argno == CLI_DNS_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);
    DnsLock ();
    if ((((u4Command >= CLI_DNS_NAME_SERVER)
          && (u4Command <= CLI_DNS_CLEAR_CACHE))
         || (u4Command == CLI_DNS_CACHE_TTL))
        && (gDnsGlobalInfo.u1SystemCtrlStatus != DNS_START))
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Dns Module is Shutdown \r\n"));
        u4ErrorCode = DNS_CLI_SHUTDOWN;
        CliPrintf (CliHandle, "\r\n%% %s\r\n", DnsCliErrString[u4ErrorCode]);
        DnsUnLock ();
        return CLI_FAILURE;
    }
    switch (u4Command)
    {
        case CLI_DNS_SYS_CTRL:
            i4RetStatus = DnsCliSetSystemControl (CliHandle,
                                                  CLI_PTR_TO_I4 (apu1args[0]));
            break;

        case CLI_DNS_MOD_STAT:
            i4RetStatus = DnsCliSetModuleStatus (CliHandle,
                                                 CLI_PTR_TO_I4 (apu1args[0]));
            break;

        case CLI_DNS_RETRY:
            MEMCPY (&i4Args, apu1args[0], sizeof (INT4));
            i4RetStatus = DnsQueryRetryCount (CliHandle, (UINT4) i4Args);
            break;

        case CLI_DNS_TIMEOUT:
            MEMCPY (&i4Args, apu1args[0], sizeof (INT4));
            i4RetStatus = DnsQueryTimeOut (CliHandle, (UINT4) i4Args);
            break;

        case CLI_DNS_MODE:
            i4RetStatus = DnsResolverMode (CliHandle, DNS_MODE_SEQUENCE);
            break;

        case CLI_DNS_PREF_TYPE:
            i4RetStatus =
                DnsPreferentialType (CliHandle, CLI_PTR_TO_I4 (apu1args[0]));
            break;

        case CLI_DNS_NO_RETRY:
            i4RetStatus = DnsQueryRetryCount (CliHandle,
                                              DNS_DEFAULT_RETRY_COUNT);
            break;

        case CLI_DNS_NO_TIMEOUT:
            i4RetStatus = DnsQueryTimeOut (CliHandle, DNS_DEFAULT_TIMEOUT);
            break;

        case CLI_DNS_NO_MODE:
            i4RetStatus = DnsResolverMode (CliHandle, DNS_MODE_SIMUL);
            break;

        case CLI_DNS_NO_PREF_TYPE:
            i4RetStatus = DnsPreferentialType (CliHandle, DNS_v4v6_RESOLVE);
            break;

        case CLI_DNS_SHOW_INFO:
            i4RetStatus = DnsCliShowGlobalInfo (CliHandle);
            break;

        case CLI_DNS_CACHE:
            i4RetStatus = DnsCliSetCacheStatus (CliHandle,
                                                DNS_CACHE_ENABLE,
                                                DNS_CACHE_DEFAULT_TTL);
            break;

        case CLI_DNS_CACHE_TTL:
                MEMCPY (&i4Args, apu1args[0], sizeof (INT4));
                i4RetStatus = DnsCliSetCacheStatus (CliHandle,
                                                    DNS_CACHE_ENABLE, i4Args);
            break;

        case CLI_DNS_NO_CACHE_TTL:
                i4RetStatus =
                    DnsCliSetCacheStatus (CliHandle, DNS_CACHE_ENABLE, DNS_CACHE_DEFAULT_TTL);
                break;
                
        case CLI_DNS_NO_CACHE:
                i4RetStatus =
                    DnsCliSetCacheStatus (CliHandle, DNS_CACHE_DISABLE, DNS_CACHE_DEFAULT_TTL);

            break;

        case CLI_DNS_CLEAR_CACHE:
            i4RetStatus =
                DnsCliSetCacheStatus (CliHandle, DNS_CACHE_CLEAR, DNS_CACHE_DEFAULT_TTL);
            break;

        case CLI_DNS_SHOW_CACHE:
            i4RetStatus = DnsCliShowCacheInfo (CliHandle);
            break;

        case CLI_DNS_SHOW_CACHE_DETAIL:
            i4RetStatus = DnsCliShowDetailCacheInfo (CliHandle);
            break;

        case CLI_DNS_NAME_SERVER:
            u4Family = CLI_PTR_TO_U4 (apu1args[1]);

            if (u4Family == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (&u4IPAddr, apu1args[2], IPVX_IPV4_ADDR_LEN);
                u4IPAddr = OSIX_HTONL (u4IPAddr);
                MEMCPY (au1IPAddr, &u4IPAddr, IPVX_IPV4_ADDR_LEN);
            }
            else if (u4Family == IPVX_ADDR_FMLY_IPV6)
            {
                if (INET_ATON6 (apu1args[3], au1IPAddr) == 0)
                {
                    break;
                }
            }
            i4RetStatus =
                DnsCliCreateNameServer (CliHandle,
                                        CLI_PTR_TO_U4 (apu1args[0]),
                                        u4Family, au1IPAddr);

            break;

        case CLI_DNS_NO_NAME_SERVER:
            u4Family = CLI_PTR_TO_U4 (apu1args[1]);

            if (u4Family == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (&u4IPAddr, apu1args[2], IPVX_IPV4_ADDR_LEN);
                u4IPAddr = OSIX_HTONL (u4IPAddr);
                MEMCPY (au1IPAddr, &u4IPAddr, IPVX_IPV4_ADDR_LEN);
            }
            else if (u4Family == IPVX_ADDR_FMLY_IPV6)
            {
                if ((apu1args[3] != NULL)
                    && (INET_ATON6 (apu1args[3], au1IPAddr) == 0))
                {
                    CliPrintf (CliHandle,
                               "\r%% Incorrect Ipv6 Address Format\n");
                    break;
                }
            }
            i4RetStatus =
                DnsCliDeleteNameServer (CliHandle, CLI_PTR_TO_U4 (apu1args[0]),
                                        u4Family, au1IPAddr);
            break;

        case CLI_DNS_SHOW_NAME_SERVER:
            i4RetStatus = DnsCliShowNameServer (CliHandle);
            break;

        case CLI_DNS_DOMAIN_NAME:

            i4RetStatus =
                DnsCliCreateDomainName (CliHandle, CLI_PTR_TO_U4 (apu1args[0]),
                                        (UINT1 *) apu1args[1]);
            break;

        case CLI_DNS_NO_DOMAIN_NAME:
            u4DomainNameIndex = CLI_PTR_TO_U4 (apu1args[0]);
            /* silent ignore if default entry comes for deletion */
            if (apu1args[1] != NULL)
            {
                if (u4DomainNameIndex == DNS_ZERO)
                {
                    MEMCPY (au1DomainName, (UINT1 *) apu1args[1],
                            DNS_MAX_DOMAIN_NAME_LEN);
                }
                i4RetStatus = DnsCliDeleteDomainName (CliHandle,
                                                      CLI_PTR_TO_U4 (apu1args
                                                                     [0]),
                                                      au1DomainName);
            }
            break;

        case CLI_DNS_SHOW_DOMAIN_NAME:
            i4RetStatus = DnsCliShowDomainName (CliHandle);
            break;

        case CLI_DNS_SHOW_STATS:
            i4RetStatus = DnsCliShowStatistics (CliHandle);
            break;

        case CLI_DNS_SHOW_QUERY:
            i4RetStatus = DnsCliShowPendingQueries (CliHandle);
            break;

        case CLI_DNS_SET_TRACE:
            i4TraceValue = CLI_PTR_TO_I4 (apu1args[0]);
            i4RetStatus = DnsCliSetTraceOption (CliHandle, i4TraceValue);
            break;

        case CLI_DNS_RESET_TRACE:
            i4TraceValue = CLI_PTR_TO_I4 (apu1args[0]);
            i4RetStatus = DnsCliResetTraceOption (CliHandle, i4TraceValue);
            break;

        case CLI_DNS_SHOW_TRACE:
            i4RetStatus = DnsCliShowTraceOption (CliHandle);
            break;

        default:
            CliPrintf (CliHandle, "\r\nUnknown Command \r\n");
            DnsUnLock ();
            return CLI_FAILURE;
    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrorCode) == CLI_SUCCESS))
    {
        if (u4ErrorCode > DNS_ZERO)
        {
            CliPrintf (CliHandle, "\r\n%% %s\r\n",
                       DnsCliErrString[u4ErrorCode]);
        }
        CLI_SET_ERR (DNS_ZERO);
    }
    DnsUnLock ();
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsCliCreateDomainName                             */
/*                                                                           */
/*     DESCRIPTION      : This function adds a new Domain name entry         */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Index - Domain name index                        */
/*                        pu1DomainName - Domain name                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

PUBLIC INT4
DnsCliCreateDomainName (tCliHandle CliHandle, UINT4 u4Index,
                        UINT1 *pu1DomainName)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE DomainName;

    *pu1DomainName = UtilStrToLower ((UINT1 *)pu1DomainName);

    MEMSET (&DomainName, 0, sizeof (DomainName));
    DomainName.pu1_OctetList = pu1DomainName;
    DomainName.i4_Length = (INT4) STRLEN (pu1DomainName);

    /* If we are adding a non-default domain name, we get the index from the Index manager */

    if (u4Index == DNS_ZERO)
    {
        if (IndexManagerGetNextFreeIndex
            (gDnsIndexMgrListInfo.DomainNameIndexList,
             &u4Index) == INDEX_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsDnsDomainNameRowStatus
        (&u4ErrorCode, u4Index, CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        nmhSetFsDnsDomainNameRowStatus (u4Index, DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetFsDnsDomainNameRowStatus (u4Index, CREATE_AND_WAIT) ==
        SNMP_FAILURE)
    {
        nmhSetFsDnsDomainNameRowStatus (u4Index, DESTROY);
        CLI_SET_ERR (DNS_CLI_NO_MULTIPLE_DOMAINS);
        return CLI_FAILURE;
    }
    if (nmhTestv2FsDnsDomainName (&u4ErrorCode, u4Index, &DomainName) ==
        SNMP_FAILURE)
    {
        nmhSetFsDnsDomainNameRowStatus (u4Index, DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetFsDnsDomainName (u4Index, &DomainName) == SNMP_FAILURE)
    {
        nmhSetFsDnsDomainNameRowStatus (u4Index, DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDnsDomainNameRowStatus (&u4ErrorCode, u4Index, ACTIVE) ==
        SNMP_FAILURE)
    {
        nmhSetFsDnsDomainNameRowStatus (u4Index, DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetFsDnsDomainNameRowStatus (u4Index, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsDnsDomainNameRowStatus (u4Index, DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsCliDeleteDomainName                             */
/*                                                                           */
/*     DESCRIPTION      : This function deletes a  Domain name entry         */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Index - Domain name index                        */
/*                        pu1DomainName - Domain name                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

PUBLIC INT4
DnsCliDeleteDomainName (tCliHandle CliHandle, UINT4 u4Index,
                        UINT1 *pu1DomainName)
{
    UINT4               u4ErrorCode = 0;

    *pu1DomainName = UtilStrToLower ((UINT1 *)pu1DomainName);

    if (u4Index == DNS_ZERO)
    {
        u4Index = (UINT4) DnsGetDomainNameIndex (pu1DomainName);
        if (u4Index == DNS_DEF_DOMAIN_NAME_INDEX)
        {
            CliPrintf (CliHandle, "\r\n%% Invalid Domain Name Entry \r\n");
        }
    }

    if (nmhTestv2FsDnsDomainNameRowStatus (&u4ErrorCode, u4Index, DESTROY) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDnsDomainNameRowStatus (u4Index, DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsCliShowDomainName                               */
/*                                                                           */
/*     DESCRIPTION      : This function prints the entries in Domain name    */
/*                        table.                                             */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

PUBLIC INT4
DnsCliShowDomainName (tCliHandle CliHandle)
{
    UINT4               u4Index = 0;
    UINT4               u4PrevIndex = 0;
    UINT1               au1DomainName[DNS_MAX_DOMAIN_NAME_LEN];

    tSNMP_OCTET_STRING_TYPE DomainName;
    MEMSET (&DomainName, 0, sizeof (DomainName));
    DomainName.pu1_OctetList = au1DomainName;

    if (nmhGetFirstIndexFsDnsDomainNameTable (&u4Index) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    CliPrintf (CliHandle, "\r\n Domain Name Table \r\n");
    CliPrintf (CliHandle, " =================\r\n");
    CliPrintf (CliHandle, "  Index    Domain name \r\n");
    CliPrintf (CliHandle, "  ======   ============\r\n");
    do
    {
        nmhGetFsDnsDomainName (u4Index, &DomainName);
        CliPrintf (CliHandle, " %5d \t    %-30s\r\n", u4Index,
                   DomainName.pu1_OctetList);
        u4PrevIndex = u4Index;
    }
    while (nmhGetNextIndexFsDnsDomainNameTable (u4PrevIndex, &u4Index) !=
           SNMP_FAILURE);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsShowRunningConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function handles the show running config      */
/*                        framework for DNS Module.                          */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Module - Unused Param                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
DnsShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{
    INT4                i4DnsSysCtrl = 0;
    INT4                i4DnsModuleStatus = 0;
    INT4                i4IPAddr = 0;
    UINT4               u4DnsRetryCount = 0;
    UINT4               u4DnsTimeOut = 0;
    UINT4               u4Index = 0;
    UINT4               u4PrevIndex = 0;
    UINT4               u4DomainIndex = 0;
    UINT4               u4PrevDomainIndex = 0;
    INT4                i4AddrType = 0;
    UINT4               u4CacheTTL = 0;
    INT4                i4CacheStatus = 0;
    INT4                i4Mode = 0;
    INT4                i4Type = 0;
    UINT1               au1IPAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1DomainName[DNS_MAX_DOMAIN_NAME_LEN];
    CHR1               *pc1IPAddr = NULL;
    tSNMP_OCTET_STRING_TYPE IPAddr;
    tSNMP_OCTET_STRING_TYPE DomainName;

    UNUSED_PARAM (u4Module);

    MEMSET (&IPAddr, DNS_ZERO, sizeof (IPAddr));
    IPAddr.pu1_OctetList = au1IPAddr;

    MEMSET (&DomainName, DNS_ZERO, sizeof (DomainName));
    DomainName.pu1_OctetList = au1DomainName;

    MEMSET (au1DomainName, DNS_ZERO, DNS_MAX_DOMAIN_NAME_LEN);


    nmhGetFsDnsSystemControl (&i4DnsSysCtrl);
    if (i4DnsSysCtrl == DNS_SHUTDOWN)
    {
        CliPrintf (CliHandle, "shutdown dns\r\n");
        return CLI_SUCCESS;
    }

    nmhGetFsDnsModuleStatus (&i4DnsModuleStatus);
    if (i4DnsModuleStatus == DNS_DISABLE)
    {
        CliPrintf (CliHandle, "no ip domain lookup\r\n");
    }

    nmhGetFsDnsQueryRetryCount (&u4DnsRetryCount);
    if (u4DnsRetryCount != DNS_DEFAULT_RETRY_COUNT)
    {
        CliPrintf (CliHandle, "ip domain retry %d\r\n", u4DnsRetryCount);
    }

    nmhGetFsDnsQueryTimeOut (&u4DnsTimeOut);
    if (u4DnsTimeOut != DNS_DEFAULT_TIMEOUT)
    {
        CliPrintf (CliHandle, "ip domain timeout %d\r\n", u4DnsTimeOut);
    }

    nmhGetFsDnsResolverMode (&i4Mode);
    if (i4Mode != DNS_MODE_SIMUL)
    {
        CliPrintf (CliHandle, "ip domain resolver-mode sequential\r\n");
    }
    nmhGetFsDnsPreferentialType (&i4Type);
    switch (i4Type)
    {
        case DNS_v4_RESOLVE:
            CliPrintf (CliHandle, "\r\n ip domain preferential-type ipv4 \r\n");
            break;
        case DNS_v6_RESOLVE:
            CliPrintf (CliHandle, "\r\n ip domain preferential-type ipv6 \r\n");
            break;
        default:
            break;

    }
    nmhGetDnsResCacheStatus (&i4CacheStatus);
    if (i4CacheStatus != DNS_CACHE_ENABLE)
    {
        CliPrintf (CliHandle, "no ip domain cache\r\n");
    }
    else
    {
        nmhGetDnsResCacheMaxTTL (&u4CacheTTL);
        if (u4CacheTTL != DNS_CACHE_DEFAULT_TTL)
        {
            CliPrintf (CliHandle, "ip domain cache ttl %d\r\n", u4CacheTTL);
        }
    }

    if (nmhGetFirstIndexFsDnsNameServerTable (&u4Index) == SNMP_SUCCESS)
    {
        do
        {
            nmhGetFsDnsServerIPAddressType (u4Index, &i4AddrType);
            nmhGetFsDnsServerIPAddress (u4Index, &IPAddr);
            if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (&i4IPAddr, IPAddr.pu1_OctetList, sizeof (UINT4));
                i4IPAddr = (INT4) OSIX_HTONL ((UINT4) i4IPAddr);
                CLI_CONVERT_IPADDR_TO_STR (pc1IPAddr, (UINT4) i4IPAddr);
                CliPrintf (CliHandle, "domain name-server ipv4 %s\r\n", pc1IPAddr);
                u4PrevIndex = u4Index;
            }
            else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
            {
                CliPrintf (CliHandle, "domain name-server ipv6 %s\r\n",
                           Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                         IPAddr.pu1_OctetList));
            }

            u4PrevIndex = u4Index;
        }
        while (nmhGetNextIndexFsDnsNameServerTable (u4PrevIndex, &u4Index) !=
               SNMP_FAILURE);
    }

    if (nmhGetFirstIndexFsDnsDomainNameTable (&u4DomainIndex) == SNMP_SUCCESS)
    {
        do
        {
            nmhGetFsDnsDomainName (u4DomainIndex, &DomainName);
            CliPrintf (CliHandle, "domain name %s\r\n",
                       DomainName.pu1_OctetList);
            u4PrevDomainIndex = u4DomainIndex;
        }
        while (nmhGetNextIndexFsDnsDomainNameTable (u4PrevDomainIndex,
                                                    &u4DomainIndex) !=
               SNMP_FAILURE);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsCliSetSystemControl                             */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disabled the overall DNS     */
/*                        System control                                     */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        i4Status - System control value                    */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

PUBLIC INT4
DnsCliSetSystemControl (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrorCode = DNS_ZERO;

    if (nmhTestv2FsDnsSystemControl (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsDnsSystemControl (i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsCliSetModuleStatus                              */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disabled the overall DNS     */
/*                        Module Status                                      */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        i4Status - Module Status  value                    */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
DnsCliSetModuleStatus (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsDnsModuleStatus (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsDnsModuleStatus (i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsQueryRetryCount                                 */
/*                                                                           */
/*     DESCRIPTION      : This function configures the DNS Query retry count */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Count - Query retry count                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
DnsQueryRetryCount (tCliHandle CliHandle, UINT4 u4Count)
{
    UINT4               u4ErrorCode = 0;
    if (nmhTestv2FsDnsQueryRetryCount (&u4ErrorCode, u4Count) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsDnsQueryRetryCount (u4Count) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsQueryTimeOut                                    */
/*                                                                           */
/*     DESCRIPTION      : This function configures the DNS Query timeout.    */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Count - Query retry timeout                      */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
DnsQueryTimeOut (tCliHandle CliHandle, UINT4 u4Count)
{
    UINT4               u4ErrorCode = 0;
    if (nmhTestv2FsDnsQueryTimeOut (&u4ErrorCode, u4Count) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsDnsQueryTimeOut (u4Count) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsResolverMode                                    */
/*                                                                           */
/*     DESCRIPTION      : This function configures the DNS Resolver Mode.    */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Count - Resolver mode                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
DnsResolverMode (tCliHandle CliHandle, INT4 i4Mode)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4Ret = 0;

    if (nmhTestv2FsDnsResolverMode (&u4ErrorCode, i4Mode) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    i4Ret = nmhSetFsDnsResolverMode (i4Mode);

    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4Ret);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsPreferentialType                                    */
/*                                                                           */
/*     DESCRIPTION      : This function configures the DNS PreferentialType.    */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        i4Type - Resolver PreferentialType                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
DnsPreferentialType (tCliHandle CliHandle, INT4 i4Type)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4Ret = 0;
    if (nmhTestv2FsDnsPreferentialType (&u4ErrorCode, i4Type) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    i4Ret = nmhSetFsDnsPreferentialType (i4Type);
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4Ret);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsCliShowGlobalInfo                               */
/*                                                                           */
/*     DESCRIPTION      : This function shows the DNS System Information.    */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
DnsCliShowGlobalInfo (tCliHandle CliHandle)
{
    INT4                i4SystemControl;
    INT4                i4ModuleStatus;
    INT4                i4Mode = 0;
    INT4                i4Type = 0;
    UINT4               u4RetryCount = 0;
    UINT4               u4TimeOut = 0;

    nmhGetFsDnsSystemControl (&i4SystemControl);

    if (i4SystemControl == DNS_START)
    {
        CliPrintf (CliHandle, "\r\n System Control    :  START \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\n System Control    :  SHUTDOWN \r\n");
    }

    nmhGetFsDnsModuleStatus (&i4ModuleStatus);

    if (i4ModuleStatus == DNS_ENABLE)
    {
        CliPrintf (CliHandle, "\r\n Module Status     :  ENABLE \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\n Module Status     :  DISABLE \r\n");
    }

    nmhGetFsDnsQueryRetryCount (&u4RetryCount);
    CliPrintf (CliHandle, "\r\n Query Retry Count  : %d\r\n", u4RetryCount);

    nmhGetFsDnsQueryTimeOut (&u4TimeOut);
    CliPrintf (CliHandle, "\r\n Query Retry Time Out : %d\r\n", u4TimeOut);

    nmhGetFsDnsResolverMode (&i4Mode);
    if (i4Mode == DNS_MODE_SIMUL)
    {
        CliPrintf (CliHandle,
                   "\r\n Resolver Mode is Simultaneous - Sends Query Simultaneously"
                   " to all Configured Name Servers\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\n Resolver Mode is Sequential - Sends Query Sequentially"
                   " to all Configured Name Servers\r\n");
    }
    nmhGetFsDnsPreferentialType (&i4Type);
    switch (i4Type)
    {
        case DNS_v4_RESOLVE:
            CliPrintf (CliHandle, "\r\n Preferential Type :  IPv4 \r\n");
            break;
        case DNS_v6_RESOLVE:
            CliPrintf (CliHandle, "\r\n Preferential Type : IPv6 \r\n");
            break;
        case DNS_v4v6_RESOLVE:
            CliPrintf (CliHandle,
                       "\r\n Preferential Type : IPv4 and IPv6 \r\n");
            break;
        default:
            break;

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsCliShowNameServer                               */
/*                                                                           */
/*     DESCRIPTION      : This function prints the entries in Name server    */
/*                        Table                                              */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
DnsCliShowNameServer (tCliHandle CliHandle)
{
    UINT4               u4Index = 0;
    UINT4               u4PrevIndex = 0;
    INT4                i4IPAddr = 0;
    UINT1               au1IPAddr[IPVX_MAX_INET_ADDR_LEN];
    CHR1               *pc1IPddr = NULL;

    tSNMP_OCTET_STRING_TYPE IPAddr;
    MEMSET (au1IPAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (&IPAddr, DNS_ZERO, sizeof (IPAddr));
    IPAddr.pu1_OctetList = au1IPAddr;

    if (nmhGetFirstIndexFsDnsNameServerTable (&u4Index) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\n  Name Server IP Table \r\n");
    CliPrintf (CliHandle, "  ==================== \r\n");
    CliPrintf (CliHandle, "  Index  Address-Type   IP-Address \r\n");
    CliPrintf (CliHandle, "  =====  ============   ===========\r\n");
    do
    {
        nmhGetFsDnsServerIPAddress (u4Index, &IPAddr);
        if (IPAddr.i4_Length == IPVX_IPV4_ADDR_LEN)
        {
            MEMCPY (&i4IPAddr, IPAddr.pu1_OctetList, sizeof (UINT4));
            i4IPAddr = (INT4) OSIX_HTONL ((UINT4) i4IPAddr);
            CLI_CONVERT_IPADDR_TO_STR (pc1IPddr, (UINT4) i4IPAddr);
            CliPrintf (CliHandle, " %5d \t   ipv4\t\t %s\r\n", u4Index,
                       pc1IPddr);
        }
        else if (IPAddr.i4_Length == IPVX_IPV6_ADDR_LEN)
        {
            CliPrintf (CliHandle, " %5d \t   ipv6\t\t %s\r\n", u4Index,
                       Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                     IPAddr.pu1_OctetList));
        }
        u4PrevIndex = u4Index;
    }
    while (nmhGetNextIndexFsDnsNameServerTable (u4PrevIndex, &u4Index) !=
           SNMP_FAILURE);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsCliCreateNameServer                             */
/*                                                                           */
/*     DESCRIPTION      : This function adds a new entry to Name server      */
/*                        Table                                              */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Index - Name Server Index                        */
/*                        u4Family - Name Server Address Family              */
/*                        pu1IPAddr - Name Server IP                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
DnsCliCreateNameServer (tCliHandle CliHandle, UINT4 u4Index, UINT4 u4Family,
                        UINT1 *pu1IPAddr)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE NSIPAddr;

    MEMSET (&NSIPAddr, DNS_ZERO, sizeof (NSIPAddr));

    if (u4Family == IPVX_ADDR_FMLY_IPV4)
    {
        NSIPAddr.pu1_OctetList = pu1IPAddr;
        NSIPAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    }
    else if (u4Family == IPVX_ADDR_FMLY_IPV6)
    {
        NSIPAddr.pu1_OctetList = pu1IPAddr;
        NSIPAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
    }

    /* If we are adding a non-default name server, we get the index from the Index manager */
    if (u4Index == DNS_ZERO)
    {
        if (IndexManagerGetNextFreeIndex
            (gDnsIndexMgrListInfo.NameServerIndexList,
             &u4Index) == INDEX_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%% Cannot create more Domain Name Servers \r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsDnsNameServerRowStatus
        (&u4ErrorCode, u4Index, CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsDnsNameServerRowStatus (u4Index, CREATE_AND_WAIT) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (DNS_CLI_NO_MULTIPLE_NAME_SERVERS);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDnsServerIPAddressType
        (&u4ErrorCode, u4Index, (INT4) u4Family) == SNMP_FAILURE)
    {
        nmhSetFsDnsNameServerRowStatus (u4Index, DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetFsDnsServerIPAddressType (u4Index, u4Family) == SNMP_FAILURE)
    {
        nmhSetFsDnsNameServerRowStatus (u4Index, DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDnsServerIPAddress (&u4ErrorCode, u4Index, &NSIPAddr) ==
        SNMP_FAILURE)
    {
        nmhSetFsDnsNameServerRowStatus (u4Index, DESTROY);
        return CLI_FAILURE;
    }
    if (nmhSetFsDnsServerIPAddress (u4Index, &NSIPAddr) == SNMP_FAILURE)
    {
        nmhSetFsDnsNameServerRowStatus (u4Index, DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDnsNameServerRowStatus (&u4ErrorCode, u4Index, ACTIVE) ==
        SNMP_FAILURE)
    {
        nmhSetFsDnsNameServerRowStatus (u4Index, DESTROY);
        return CLI_FAILURE;
    }
    if (nmhSetFsDnsNameServerRowStatus (u4Index, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsDnsNameServerRowStatus (u4Index, DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsCliDeleteNameServer                             */
/*                                                                           */
/*     DESCRIPTION      : This function deletes an entry from  Name server   */
/*                        Table                                              */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Index - Name Server Index                        */
/*                        u4Family - Name Server Address Family              */
/*                        pu1IPAddr - Name Server IP                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
DnsCliDeleteNameServer (tCliHandle CliHandle, UINT4 u4Index, UINT4 u4Family,
                        UINT1 *pu1IPAddr)
{
    UINT4               u4ErrorCode = 0;

    if (u4Index == DNS_ZERO)
    {
        u4Index = (UINT4) DnsGetServerIndex (u4Family, pu1IPAddr);
        if (u4Index == DNS_DEF_NAME_SERVER_INDEX)
        {
            CliPrintf (CliHandle, "\r\n%%  Invalid Name Server Entry \r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsDnsNameServerRowStatus (&u4ErrorCode, u4Index, DESTROY) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsDnsNameServerRowStatus (u4Index, DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsCliSetCacheStatus                               */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables a DNS Cache status  */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        i4Status - Cache Status                            */
/*                        i4MaxTTL - Dns Max cache record TTL                */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
DnsCliSetCacheStatus (tCliHandle CliHandle, INT4 i4Status, INT4 i4MaxTTL)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2DnsResCacheStatus (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDnsResCacheStatus (i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2DnsResCacheMaxTTL (&u4ErrorCode, (UINT4) i4MaxTTL) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDnsResCacheMaxTTL (i4MaxTTL) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsCliShowCacheInfo                                */
/*                                                                           */
/*     DESCRIPTION      : This function displays all the cache records.      */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
DnsCliShowCacheInfo (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE RRName;
    INT4                i4RRClass = 0;
    INT4                i4RRType = 0;
    INT4                i4RRIndex = 0;
    UINT1               au1RRName[DNS_MAX_QUERY_LEN];
    tSNMP_OCTET_STRING_TYPE PrevRRName;
    INT4                i4PrevRRClass = 0;
    INT4                i4PrevRRType = 0;
    INT4                i4PrevRRIndex = 0;
    UINT1               au1PrevRRName[DNS_MAX_QUERY_LEN];
    tDnsCacheRRInfo    *pDnsCacheRRInfo = NULL;
    INT4                i4IPAddr = 0;
    CHR1               *pc1IPddr = NULL;
    INT4                i4Count = 0;
    UINT1               au1temp[DNS_THIRTY];

    MEMSET (&RRName, 0, sizeof (RRName));
    MEMSET (&PrevRRName, 0, sizeof (PrevRRName));
    MEMSET (au1RRName, 0, DNS_MAX_QUERY_LEN);
    MEMSET (au1PrevRRName, 0, DNS_MAX_QUERY_LEN);
    MEMSET (au1temp, 0, DNS_THIRTY);

    RRName.pu1_OctetList = au1RRName;
    PrevRRName.pu1_OctetList = au1PrevRRName;

    if (nmhGetFirstIndexDnsResCacheRRTable
        (&RRName, &i4RRClass, &i4RRType, &i4RRIndex) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, " \r\n DNS Cache Records \r\n");
    CliPrintf (CliHandle, " ================= \r\n");
    CliPrintf (CliHandle,"\r\n%-30s%-30s%-7s%s \r\n",
               "Query","Name-Server","TTL","Answer");
    CliPrintf (CliHandle,"\r\n%-30s%-30s%-7s%s \r\n",
               "=====","===========","===","======");

    do
    {
        i4Count++;
        /*here allocated column size is 30, to have space between query
         * and name server copying 29 characters.*/
        MEMCPY (au1temp, RRName.pu1_OctetList, (DNS_THIRTY - 1));
        if (((STRLEN (RRName.pu1_OctetList)) > (DNS_THIRTY - 1)))
        {
            /*if received string length is greater than 29 then * needs to 
             * be appended */
            au1temp[DNS_THIRTY - 2] = '*';
        }

        CliPrintf (CliHandle, "\r\n%-30s", au1temp);

        pDnsCacheRRInfo =
            DnsGetCacheRR (RRName.pu1_OctetList, (UINT4) i4RRClass,
                           (UINT4) i4RRType, (UINT4) i4RRIndex);
        if (pDnsCacheRRInfo == NULL)
        {
            return CLI_FAILURE;
        }

        if (pDnsCacheRRInfo->SourceIP.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (&i4IPAddr, &pDnsCacheRRInfo->SourceIP.au1Addr,
                    sizeof (INT4));
            i4IPAddr = (INT4) OSIX_HTONL ((UINT4) i4IPAddr);
            CLI_CONVERT_IPADDR_TO_STR (pc1IPddr, (UINT4) i4IPAddr);

            CliPrintf (CliHandle, "%-30s", pc1IPddr);
        }
        else if (pDnsCacheRRInfo->SourceIP.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            CliPrintf (CliHandle, "%-30s",
                       Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                     &(pDnsCacheRRInfo->SourceIP.au1Addr)));
        }
        CliPrintf (CliHandle, "%-7d", pDnsCacheRRInfo->u4RRTTL);
        if (i4RRType == DNS_HOST_ADDRESS)
        {
            MEMCPY (&i4IPAddr, pDnsCacheRRInfo->au1RRData, sizeof (INT4));
            i4IPAddr = (INT4) OSIX_HTONL ((UINT4) i4IPAddr);
            CLI_CONVERT_IPADDR_TO_STR (pc1IPddr, (UINT4) i4IPAddr);
            CliPrintf (CliHandle, "%s\r\n", pc1IPddr);
        }
        else if (i4RRType == DNS_AAAA_RECORD)
        {
            CliPrintf (CliHandle, "%s\r\n",
                       Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                     &(pDnsCacheRRInfo->au1RRData)));
        }

        STRCPY (PrevRRName.pu1_OctetList, RRName.pu1_OctetList);
        i4PrevRRClass = i4RRClass;
        i4PrevRRType = i4RRType;
        i4PrevRRIndex = i4RRIndex;
    }
    while (nmhGetNextIndexDnsResCacheRRTable
           (&PrevRRName, &RRName, i4PrevRRClass, &i4RRClass, i4PrevRRType,
            &i4RRType, i4PrevRRIndex, &i4RRIndex) != SNMP_FAILURE);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsCliShowStatistics                               */
/*                                                                           */
/*     DESCRIPTION      : This function displays DNS Statistics information  */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
DnsCliShowStatistics (tCliHandle CliHandle)
{
    UINT4               u4QueriesSent = 0;
    UINT4               u4ResReceived = 0;
    UINT4               u4DropResponse = 0;
    UINT4               u4UnAnsQueries = 0;
    UINT4               u4FailedQueries = 0;
    UINT4               u4ReTransQueries = 0;

    nmhGetFsDnsQueriesSent (&u4QueriesSent);
    nmhGetFsDnsResponseReceived (&u4ResReceived);
    nmhGetFsDnsDroppedResponse (&u4DropResponse);
    nmhGetFsDnsUnAnsweredQueries (&u4UnAnsQueries);
    nmhGetFsDnsFailedQueries (&u4FailedQueries);
    nmhGetFsDnsReTransQueries (&u4ReTransQueries);

    CliPrintf (CliHandle, "\r\n Dns Statistics \r\n");
    CliPrintf (CliHandle, "Queries Sent : %d \r\n", u4QueriesSent);
    CliPrintf (CliHandle, "Response Received : %d \r\n", u4ResReceived);
    CliPrintf (CliHandle, "Dropped Response  : %d \r\n", u4DropResponse);
    CliPrintf (CliHandle, "Unanswered Queries : %d \r\n", u4UnAnsQueries);
    CliPrintf (CliHandle, "Failed Queries : %d \r\n", u4FailedQueries);
    CliPrintf (CliHandle, "Retransmitted Queries : %d \r\n", u4ReTransQueries);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsCliShowPendingQueries                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays DNS Pending Query           */
/*                        Information                                        */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
DnsCliShowPendingQueries (tCliHandle CliHandle)
{
    UINT4               u4Index = 0;
    UINT4               u4PrevIndex = 0;
    UINT1               au1QueryName[DNS_MAX_QUERY_LEN];

    tSNMP_OCTET_STRING_TYPE QueryName;
    MEMSET (au1QueryName, 0, DNS_MAX_QUERY_LEN);
    MEMSET (&QueryName, 0, sizeof (QueryName));

    QueryName.pu1_OctetList = au1QueryName;

    if (nmhGetFirstIndexFsDnsQueryTable (&u4Index) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    CliPrintf (CliHandle, "\r\n Pending Query : \r\n");
    do
    {
        nmhGetFsDnsQueryName (u4Index, &QueryName);

        CliPrintf (CliHandle, " %5d. %-50s \r\n", u4Index,
                   QueryName.pu1_OctetList);
        u4PrevIndex = u4Index;
    }
    while (nmhGetNextIndexFsDnsQueryTable (u4PrevIndex, &u4Index) !=
           DNS_FAILURE);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsCliShowTraceOption                              */
/*                                                                           */
/*     DESCRIPTION      : This function displays DNS Trace Option            */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
DnsCliShowTraceOption (tCliHandle CliHandle)
{
    INT4                i4CurrentLevel = 0;

    nmhGetFsDnsTraceOption (&i4CurrentLevel);

    if (i4CurrentLevel == 0)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\rDNS :\n");

    if (i4CurrentLevel & DNS_INITSHUT_TRC)
    {
        CliPrintf (CliHandle, "  DNS Init-Shutdown Trace is enabled \r\n");
    }
    if (i4CurrentLevel & DNS_CTRL_TRC)
    {
        CliPrintf (CliHandle, "  DNS Control Trace is enabled \r\n");
    }
    if (i4CurrentLevel & DNS_QUERY_TRC)
    {
        CliPrintf (CliHandle, "  DNS Query Trace is enabled \r\n");
    }
    if (i4CurrentLevel & DNS_RESP_TRC)
    {
        CliPrintf (CliHandle, "  DNS Response Trace is enabled \r\n");
    }
    if (i4CurrentLevel & DNS_CACHE_TRC)
    {
        CliPrintf (CliHandle, "  DNS Cache Trace is enabled \r\n");
    }
    if (i4CurrentLevel & DNS_FAILURE_TRC)
    {
        CliPrintf (CliHandle, "  DNS Failure Trace is enabled \r\n");
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsCliSetTraceOption                               */
/*                                                                           */
/*     DESCRIPTION      : This Functions sets the Overall Trace Level.       */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        i4TraceValue - Trace Value                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
DnsCliSetTraceOption (tCliHandle CliHandle, INT4 i4TraceValue)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4CurrentLevel = 0;

    nmhGetFsDnsTraceOption (&i4CurrentLevel);

    i4TraceValue = i4CurrentLevel | i4TraceValue;

    if (nmhTestv2FsDnsTraceOption (&u4ErrorCode, i4TraceValue) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDnsTraceOption (i4TraceValue) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsCliResetTraceOption                             */
/*                                                                           */
/*     DESCRIPTION      : This Functions resets the Overall Trace Level.     */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        i4TraceValue - Trace Value                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
DnsCliResetTraceOption (tCliHandle CliHandle, INT4 i4TraceValue)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4CurrentLevel = 0;

    nmhGetFsDnsTraceOption (&i4CurrentLevel);

    i4TraceValue = i4CurrentLevel & (~i4TraceValue);

    if (nmhTestv2FsDnsTraceOption (&u4ErrorCode, i4TraceValue) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDnsTraceOption (i4TraceValue) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsCliShowDetailCacheInfo                          */
/*                                                                           */
/*     DESCRIPTION      : This function displays all the cache records.      */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
DnsCliShowDetailCacheInfo (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE RRName;
    INT4                i4RRClass = 0;
    INT4                i4RRType = 0;
    INT4                i4RRIndex = 0;
    UINT1               au1RRName[DNS_MAX_QUERY_LEN];
    tSNMP_OCTET_STRING_TYPE PrevRRName;
    INT4                i4PrevRRClass = 0;
    INT4                i4PrevRRType = 0;
    INT4                i4PrevRRIndex = 0;
    UINT1               au1PrevRRName[DNS_MAX_QUERY_LEN];
    tDnsCacheRRInfo    *pDnsCacheRRInfo = NULL;
    INT4                i4IPAddr = 0;
    CHR1               *pc1IPddr = NULL;
    INT4                i4Count = 0;

    MEMSET (&RRName, 0, sizeof (RRName));
    MEMSET (&PrevRRName, 0, sizeof (PrevRRName));
    MEMSET (au1RRName, 0, DNS_MAX_QUERY_LEN);
    MEMSET (au1PrevRRName, 0, DNS_MAX_QUERY_LEN);

    RRName.pu1_OctetList = au1RRName;
    PrevRRName.pu1_OctetList = au1PrevRRName;

    if (nmhGetFirstIndexDnsResCacheRRTable
        (&RRName, &i4RRClass, &i4RRType, &i4RRIndex) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, " \r\n DNS Cache Records \r\n");
    CliPrintf (CliHandle, " ================= \r\n");
    
    do
    {
        i4Count++;

        CliPrintf (CliHandle, "Query        : %s\r\n", RRName.pu1_OctetList);

        pDnsCacheRRInfo =
            DnsGetCacheRR (RRName.pu1_OctetList, (UINT4) i4RRClass,
                           (UINT4) i4RRType, (UINT4) i4RRIndex);
        if (pDnsCacheRRInfo == NULL)
        {
            return CLI_FAILURE;
        }

        if (pDnsCacheRRInfo->SourceIP.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (&i4IPAddr, &pDnsCacheRRInfo->SourceIP.au1Addr,
                    sizeof (INT4));
            i4IPAddr = (INT4) OSIX_HTONL ((UINT4) i4IPAddr);
            CLI_CONVERT_IPADDR_TO_STR (pc1IPddr, (UINT4) i4IPAddr);

            CliPrintf (CliHandle, "Name-Server  : %s\r\n", pc1IPddr);
        }
        else if (pDnsCacheRRInfo->SourceIP.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            CliPrintf (CliHandle, "Name-Server  : %s\r\n",
                       Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                     &(pDnsCacheRRInfo->SourceIP.au1Addr)));
        }
        CliPrintf (CliHandle, "TTL          : %d\r\n", pDnsCacheRRInfo->u4RRTTL);
        if (i4RRType == DNS_HOST_ADDRESS)
        {
            MEMCPY (&i4IPAddr, pDnsCacheRRInfo->au1RRData, sizeof (INT4));
            i4IPAddr = (INT4) OSIX_HTONL ((UINT4) i4IPAddr);
            CLI_CONVERT_IPADDR_TO_STR (pc1IPddr, (UINT4) i4IPAddr);
            CliPrintf (CliHandle, "Answer       : %s\r\n", pc1IPddr);
        }
        else if (i4RRType == DNS_AAAA_RECORD)
        {
            CliPrintf (CliHandle, "Answer       : %s\r\n",
                       Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                     &(pDnsCacheRRInfo->au1RRData)));
        }

        STRCPY (PrevRRName.pu1_OctetList, RRName.pu1_OctetList);
        i4PrevRRClass = i4RRClass;
        i4PrevRRType = i4RRType;
        i4PrevRRIndex = i4RRIndex;
    }
    while (nmhGetNextIndexDnsResCacheRRTable
           (&PrevRRName, &RRName, i4PrevRRClass, &i4RRClass, i4PrevRRType,
            &i4RRType, i4PrevRRIndex, &i4RRIndex) != SNMP_FAILURE);

    return CLI_SUCCESS;
}

