/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dnsquery.c,v 1.14 2015/01/12 10:24:37 siva Exp $
 *
 * Description: This file contains action routines for
 *              DNS Query proccessing
 *
 *******************************************************************/

#include "dnsinc.h"

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsSendQueryToResolver
 *
 *    DESCRIPTION      : This functions sends a DNS Query to a Name Server
 *
 *    INPUT            : pDnsQueryInfo - pointer to DNS Query structure
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : DNS_SUCCESS / DNS_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
DnsSendQueryToResolver (tDnsQueryInfo * pDnsQueryInfo)
{
    UINT1              *pu1QueryPkt = NULL;
    UINT4               u4PktSize = 0;

    pu1QueryPkt = MemAllocMemBlk (gDnsGlobalInfo.QueryRespMemPoolId);
    if (pu1QueryPkt == NULL)
    {
        DNS_TRACE ((DNS_QUERY_TRC | DNS_FAILURE_TRC,
                    " DNS Memory Allocation Fails \r\n"));
        return DNS_FAILURE;
    }

    MEMSET (pu1QueryPkt, 0, MAX_DNS_PKT_LEN);

    u4PktSize = (UINT4) DnsCreateQuery (pDnsQueryInfo, pu1QueryPkt);
    if (u4PktSize <= DNS_ZERO)
    {
        DNS_TRACE ((DNS_QUERY_TRC | DNS_FAILURE_TRC,
                    " Creation of DNS Query Fails \r\n"));
        gDnsStatistics.u4FailedQueries++;

        MemReleaseMemBlock (gDnsGlobalInfo.QueryRespMemPoolId, pu1QueryPkt);
        return DNS_FAILURE;
    }

    if (DnsSendQuery (pu1QueryPkt, u4PktSize, pDnsQueryInfo) != DNS_SUCCESS)
    {
        DNS_TRACE ((DNS_QUERY_TRC | DNS_FAILURE_TRC,
                    " Sending DNS Query Fails \r\n"));
        gDnsStatistics.u4FailedQueries++;

        MemReleaseMemBlock (gDnsGlobalInfo.QueryRespMemPoolId, pu1QueryPkt);
        return DNS_FAILURE;
    }
    gDnsStatistics.u4TotalQuerySent++;
    gDnsStatistics.u4UnansQueries++;

    MemReleaseMemBlock (gDnsGlobalInfo.QueryRespMemPoolId, pu1QueryPkt);
    return DNS_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsCreateQuery            
 *
 *    DESCRIPTION      : This functions creates a DNS Query 
 *
 *    INPUT            : pDnsQueryInfo - pointer to DNS Query structure
 *                       pu1QueryPkt - DNS Query Packet
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : Query Packet Size.         
 *
 ****************************************************************************/
PUBLIC INT4
DnsCreateQuery (tDnsQueryInfo * pDnsQueryInfo, UINT1 *pu1QueryPkt)
{
    UINT4               u4QueryQuesSize = 0;

    DnsFillQueryHeader (pu1QueryPkt, pDnsQueryInfo->u2QueryIndex);
    pu1QueryPkt = pu1QueryPkt + sizeof (tDnsHeader);

    u4QueryQuesSize = (UINT4) DnsFillQueryQues (pDnsQueryInfo, pu1QueryPkt);

    return ((INT4) (u4QueryQuesSize + sizeof (tDnsHeader)));
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsFillQueryHeader        
 *
 *    DESCRIPTION      : This functions fills the Query packet Header
 *
 *    INPUT            : u2QueryIndex  - DNS Pending Query Index.             
 *                       pu1QueryPkt - DNS Query Packet
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : DNS_SUCCESS                
 *
 ****************************************************************************/
PUBLIC INT4
DnsFillQueryHeader (UINT1 *pu1QueryPkt, UINT2 u2QueryIndex)
{
    tDnsHeader         *pDnsHeader = NULL;

    DNS_TRACE ((DNS_QUERY_TRC, "Filling DNS Query Header \r\n"));
    pDnsHeader = (tDnsHeader *) (VOID *) pu1QueryPkt;

    pDnsHeader->u2QId = OSIX_HTONS (u2QueryIndex);
    pDnsHeader->u2Flags = OSIX_HTONS (DNS_RECURSIVE_QUERY);
    pDnsHeader->u2ANCount = OSIX_HTONS (DNS_ANCOUNT);
    pDnsHeader->u2NSCount = OSIX_HTONS (DNS_NSCOUNT);
    pDnsHeader->u2ARCount = OSIX_HTONS (DNS_ARCOUNT);
    pDnsHeader->u2QDCount = OSIX_HTONS (DNS_QDCOUNT);

    return DNS_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsFillQueryQues          
 *
 *    DESCRIPTION      : This functions fills the Query section part
 *
 *    INPUT            : pDnsQueryInfo - pointer to DNS Query structure
 *                       pu1QueryPkt - DNS Query Packet
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : Current Query Packet Size.
 *
 ****************************************************************************/
PUBLIC INT4
DnsFillQueryQues (tDnsQueryInfo * pDnsQueryInfo, UINT1 *pu1QueryPkt)
{
    UINT1              *pu1QueryName = NULL;
    UINT1              *pu1SubStr = NULL;
    UINT1               u1LabelLen = 0;
    UINT4               u4CurSize = 0;
    UINT2               u2Type = 0;
    UINT2               u2Class = 0;

    DNS_TRACE ((DNS_QUERY_TRC, "Filling DNS Query Question Section \r\n"));
    pu1QueryName = pDnsQueryInfo->au1ResolveName;

    while (pu1QueryName != NULL)
    {
        pu1SubStr = (UINT1 *) STRSTR (pu1QueryName, ".");

        if (pu1SubStr != NULL)
        {
            u1LabelLen = (UINT1) (STRLEN (pu1QueryName) - STRLEN (pu1SubStr));
        }
        else
        {
            u1LabelLen = (UINT1) (STRLEN (pu1QueryName));
        }

        if (u1LabelLen != 0)
        {
            *pu1QueryPkt = (UINT1) u1LabelLen;
            pu1QueryPkt += 1;
            u4CurSize += 1;

            MEMCPY (pu1QueryPkt, pu1QueryName, u1LabelLen);
            pu1QueryPkt += u1LabelLen;
            u4CurSize += u1LabelLen;
        }

        if (pu1SubStr != NULL)
        {
            pu1QueryName = pu1QueryName + u1LabelLen + 1;
        }
        else
        {
            break;
        }
    }

    /* Specifies the end of the domain name  */
    *pu1QueryPkt = (UINT1) 0x0;
    pu1QueryPkt += 1;
    u4CurSize += 1;

    u2Type = OSIX_HTONS (pDnsQueryInfo->u4QueryType);
    MEMCPY (pu1QueryPkt, &u2Type, sizeof (u2Type));

    pu1QueryPkt += 2;
    u4CurSize += 2;

    u2Class = OSIX_HTONS (pDnsQueryInfo->u4QueryClass);
    MEMCPY (pu1QueryPkt, &u2Class, sizeof (UINT2));
    u4CurSize += 2;

    return ((INT4) u4CurSize);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DnsSendQuery              
 *
 *    DESCRIPTION      : This functions sends a Query to a Name Server
 *    
 *    INPUT            : pDnsQueryInfo - pointer to DNS Query structure
 *                       u4QueryPktSize - DNS Query Packet size
 *                       pu1QueryPkt - DNS Query Packet
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : DNS_SUCCESS / DNS_FAILURE.
 *
 ****************************************************************************/
PUBLIC INT4
DnsSendQuery (UINT1 *pu1QueryPkt, UINT4 u4QueryPktSize,
              tDnsQueryInfo * pDnsQueryInfo)
{
    struct sockaddr_in  ServAddr;
#ifdef IP6_WANTED
    struct sockaddr_in6 ServAddr6;
#endif
    UINT4               u4IpAddr = 0;
    CHR1               *pstr = NULL;

    if (pDnsQueryInfo->QueryNameServerIP.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        ServAddr.sin_family = AF_INET;
        ServAddr.sin_port = OSIX_HTONS (DNS_UDP_PORT);

        MEMCPY (&u4IpAddr, pDnsQueryInfo->QueryNameServerIP.au1Addr,
                sizeof (UINT4));
        ServAddr.sin_addr.s_addr = (u4IpAddr);
        u4IpAddr = OSIX_HTONL (u4IpAddr);
        CLI_CONVERT_IPADDR_TO_STR (pstr, u4IpAddr);
        DNS_TRACE ((DNS_QUERY_TRC, " .. Trying IPv4  Name Server ..  %s \r\n",
                    pstr));
        if (sendto
            (gDnsGlobalInfo.i4ResolverSockFd, pu1QueryPkt,
             (INT4) u4QueryPktSize, DNS_ZERO, (struct sockaddr *) &ServAddr,
             sizeof (ServAddr)) < DNS_ZERO)
        {
            DNS_TRACE ((DNS_QUERY_TRC | DNS_FAILURE_TRC,
                        " DNS Query SendTo Fails \r\n"));
            return DNS_FAILURE;
        }
    }
    else if (pDnsQueryInfo->QueryNameServerIP.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
#ifdef IP6_WANTED
        ServAddr6.sin6_family = AF_INET6;
        ServAddr6.sin6_port = OSIX_HTONS (DNS_UDP_PORT);
        MEMCPY (&ServAddr6.sin6_addr.s6_addr,
                pDnsQueryInfo->QueryNameServerIP.au1Addr,
                IPVX_MAX_INET_ADDR_LEN);
        DNS_TRACE ((DNS_QUERY_TRC, " .. Trying IPv6 Name Server ..   \r\n"));
        if (sendto
            (gDnsGlobalInfo.i4ResolverSock6Fd, pu1QueryPkt,
             (INT4) u4QueryPktSize, DNS_ZERO, (struct sockaddr *) &ServAddr6,
             sizeof (ServAddr6)) < DNS_ZERO)
        {
            DNS_TRACE ((DNS_QUERY_TRC | DNS_FAILURE_TRC,
                        " DNS Query SendTo Fails \r\n"));
            return DNS_FAILURE;
        }
#else
        return DNS_FAILURE;
#endif
    }

    return DNS_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DNSResolveIPvXFromName                             */
/*                                                                           */
/*     DESCRIPTION      : This function is used to resolve the IP Address    */
/*                        from the English name                              */
/*                                                                           */
/*     INPUT            : pu1QueryName - Name to be resolved                 */
/*                        u1QueryType  - Query Type                          */
/*                                                                           */
/*     OUTPUT           : pResolvedIP - Resolved IPvX                        */
/*                        pu4Error - Error Code                              */
/*                                                                           */
/*     RETURNS          : DNS_SUCCESS / DNS_FAILURE                          */
/*****************************************************************************/

PUBLIC INT4
DNSResolveIPvXFromName (UINT1 *pu1QueryName, UINT1 u1IsAsync,
                        tDNSResolvedIpInfo * pResolvedIpInfo)
{
    INT4                i4V4Status = DNSIP_RESOLVED;
    INT4                i4V6Status = DNSIP_RESOLVED;
    INT4                i4RetVal = DNSIP_NO_RESOLVED;
    INT4                i4IsPoll = OSIX_FALSE;

    /*Cache LookUp */
    if (gDnsResolverParams.i4QueryType & DNS_v6_RESOLVE)
    {
        i4V6Status = DNSIP_NO_RESOLVED;
        DnsChkCache (DNS_INTERNET_CLASS,
                     DNS_AAAA_RECORD, pu1QueryName, i4IsPoll, &i4V6Status,
                     pResolvedIpInfo);
    }

    if (gDnsResolverParams.i4QueryType & DNS_v4_RESOLVE)
    {
        i4V4Status = DNSIP_NO_RESOLVED;
        DnsChkCache (DNS_INTERNET_CLASS,
                     DNS_HOST_ADDRESS, pu1QueryName, i4IsPoll, &i4V4Status,
                     pResolvedIpInfo);
    }
    if ((i4V4Status == DNSIP_RESOLVED) && (i4V6Status == DNSIP_RESOLVED))
    {
        i4RetVal = DNSIP_RESOLVED;
    }
    else
    {
        i4RetVal =
            DnsReqSendQ (pu1QueryName, u1IsAsync, &i4V6Status, &i4V4Status,
                         pResolvedIpInfo);
    }

    return i4RetVal;
}

/****************************************************************************
 **
 **    FUNCTION NAME    : DnsFillQueryIndex
 **
 **    DESCRIPTION      : This function is used to Fill Index and add 
 **                       it in pending Query List
 **
 **    INPUT            : pDnsQueryInfo - Query Datastructure
 **                     : u4QueryClass - Query class
 **                     : u4QuerType   - Query Type
 **                     : pu1QueryName - Query Name 
 **
 **    OUTPUT           : None
 **
 **    RETURNS          : DNS_SUCCESS / DNS_FAILURE
 **
 *****************************************************************************/
PUBLIC INT4
DnsFillQueryIndex (tDnsQueryInfo * pDnsQueryInfo, UINT4 u4QueryClass,
                   UINT4 u4QueryType, UINT1 *pu1QueryName)
{

    UINT4               u4Len = 0;
    UINT4               u4QueryIndex = 0;

    u4Len = MEM_MAX_BYTES ((STRLEN (pu1QueryName)),
                           (sizeof (pDnsQueryInfo->au1QueryName) - 1));
    MEMCPY (pDnsQueryInfo->au1QueryName, pu1QueryName, u4Len);
    pDnsQueryInfo->au1QueryName[u4Len] = '\0';

    pDnsQueryInfo->u4QueryClass = u4QueryClass;
    pDnsQueryInfo->u4QueryType = u4QueryType;
    pDnsQueryInfo->u1ToBeCached = OSIX_TRUE;

    if (IndexManagerSetNextFreeIndex (gDnsIndexMgrListInfo.QueryIndexList,
                                      (UINT4 *) &u4QueryIndex) == INDEX_FAILURE)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, " Index Allocation Fails \r\n"));
        MemReleaseMemBlock (gDnsGlobalInfo.PendQryMemPoolId,
                            (UINT1 *) pDnsQueryInfo);

        return DNS_FAILURE;
    }
    pDnsQueryInfo->u2QueryIndex = (UINT2) u4QueryIndex;

    if (RBTreeAdd (gDnsGlobalInfo.PendingQueryList,
                   (tRBElem *) pDnsQueryInfo) == RB_FAILURE)
    {
        DNS_TRACE ((DNS_FAILURE_TRC,
                    " Adding to Pending Query RB Tree fails \r\n"));
        MemReleaseMemBlock (gDnsGlobalInfo.PendQryMemPoolId,
                            (UINT1 *) pDnsQueryInfo);

        return DNS_FAILURE;
    }
    if ((DnsTmrStart
         (&(pDnsQueryInfo->QryNodeTmrNode), DNS_QRY_TMR,
          gDnsResolverParams.u4QueryTimeout)) == TMR_FAILURE)
    {
        DnsDeleteQueryData (pDnsQueryInfo);
        return DNS_FAILURE;
    }
    return DNS_SUCCESS;
}

/****************************************************************************
 **
 **    FUNCTION NAME    : DnsChkCache
 **
 **    DESCRIPTION      : This function is used to Fill Index and add
 **                       it in pending Query List
 **
 **    INPUT            : pDnsQueryInfo - Query Datastructure
 **                     : u4QueryClass - Query class
 **                     : u4QuerType   - Query Type
 *                     : pu1QueryName - Query Name
 *                      : pi4ResolStat - Query Status
 *                      : 
 **
 **    OUTPUT           : pResolvedIpInfo - Resolved IP datastructure
 **
 **    RETURNS          : DNS_SUCCESS / DNS_FAILURE
 **
 ******************************************************************************/

PUBLIC INT4
DnsChkCache (UINT4 u4QueryClass,
             UINT4 u4QueryType, UINT1 *pu1QueryName, INT4 i4IsPoll,
             INT4 *pi4ResolStat, tDNSResolvedIpInfo * pResolvedIpInfo)
{
    tDnsCacheRRInfo    *pDnsCacheRRInfo = NULL;

    if ((pDnsCacheRRInfo =
         DnsCacheLookup (pu1QueryName, u4QueryClass, u4QueryType)) != NULL)
    {
        DNS_TRACE ((DNS_CACHE_TRC, " Retreiving from DNS Cache \r\n"));
        if (!i4IsPoll)
        {
            pDnsCacheRRInfo->u4HitCount = DNS_ONE;
        }
        *pi4ResolStat = DNSIP_RESOLVED;
        if (u4QueryType == DNS_AAAA_RECORD)
        {
            IPVX_ADDR_INIT_FROMV6 (pResolvedIpInfo->Resolv6Addr,
                                   pDnsCacheRRInfo->au1RRData);
        }
        else
        {
            IPVX_ADDR_INIT_IPV4 (pResolvedIpInfo->Resolv4Addr,
                                 IPVX_ADDR_FMLY_IPV4,
                                 pDnsCacheRRInfo->au1RRData);
        }
    }
    else
    {
        *pi4ResolStat = DNSIP_NO_RESOLVED;
    }
    return DNS_SUCCESS;
}

/****************************************************************************
 **
 **    FUNCTION NAME    : DnsQrySendNameserver
 **
 **    DESCRIPTION      : This function is used Check Retry Count 
 **                      and fills the Name server and send to NS
 **
 **    INPUT            : pDnsQueryInfo - Query Datastructure
 **                       
 **
 **
 **    OUTPUT           : None 
 **
 **    RETURNS          : OSIX_TRUE / OSIX_FALSE 
 **
 *****************************************************************************/

PUBLIC UINT1
DnsQrySendNameserver (tDnsQueryInfo * pDnsQueryInfo)
{

    UINT1               u1RetVal = OSIX_TRUE;

    do
    {
        if (DnsFillNameServerIP (pDnsQueryInfo) == DNS_SUCCESS)
        {
            if ((pDnsQueryInfo->u4CurRetryCount <=
                 gDnsResolverParams.u4QueryRetryCount) &&
                 (pDnsQueryInfo->u4NameServerCount > 0) 
                && (pDnsQueryInfo->u4NameServerCount <
                    MAX_DNS_NAME_SERVERS + 1))
            {
                if ((((pDnsQueryInfo->au1ErrorNum
                       [(pDnsQueryInfo->u4NameServerCount) -
                        1]) & DNS_RCODE_NO_NAME) != DNS_RCODE_NO_NAME) &&
                    (((pDnsQueryInfo->
                       au1ErrorNum[(pDnsQueryInfo->u4NameServerCount) -
                                   1]) & DNS_RCODE_SERV_FAIL) !=
                     DNS_RCODE_SERV_FAIL))

                {
            if (DnsSendQueryToResolver (pDnsQueryInfo) != DNS_FAILURE)
            {
                if (pDnsQueryInfo->u4CurRetryCount > 0)
                {
                    DNS_TRACE ((DNS_QUERY_TRC,
                                " Retransmitting DNS Query  \r\n"));
                    gDnsStatistics.u4RetransQueries++;
                }
            }
        }
    }
        }
    }
    while (pDnsQueryInfo->u4DomainCount != 0);

    return u1RetVal;
}

/****************************************************************************
 **
 **    FUNCTION NAME    : DnsUpdateResolIP
 **
 **    DESCRIPTION      : This function is used to Update the Resolved IP
 **                       in the Query datastructure
 **
 **    INPUT            : pDnsQueryInfo - Query Datastructure
 **                        pResolStat - Query Status
 **                        
 **
 **    OUTPUT           : pResolvedIpInfo - Resolved IP
 **
 **    RETURNS          : DNS_SUCCESS / DNS_FAILURE
 **
 *****************************************************************************/

PUBLIC INT4
DnsUpdateResolIP (tDnsQueryInfo * pDnsQueryInfo)
{
    INT4                i4RetVal = DNS_SUCCESS;

    if (pDnsQueryInfo->QueryResolvedIP.u1AddrLen != 0)
    {

        if (pDnsQueryInfo->u1ToBeCached == OSIX_TRUE)
        {
            DNS_TRACE ((DNS_CACHE_TRC,
                        " Adding the Resolved IP address "
                        "to the DNS Cache \r\n"));
            /* The Resolved IP address is now added to cache table */
            DnsAddCacheData (pDnsQueryInfo->au1QueryName,
                             pDnsQueryInfo->u4QueryClass,
                             pDnsQueryInfo->u4QueryType,
                             pDnsQueryInfo->u4QueryTTL,
                             pDnsQueryInfo->QueryNameServerIP,
                             pDnsQueryInfo->QueryResolvedIP);
        }
        DnsDeleteQueryData (pDnsQueryInfo);
    }
    else
    {
        gDnsStatistics.u4FailedQueries++;
        i4RetVal = DNS_FAILURE;
    }

    return i4RetVal;
}

/****************************************************************************
 **
 **    FUNCTION NAME    : DnsReqSendQuery
 **
 **    DESCRIPTION      : This function perform  Querying
 **                       to Name servers
 **
 **    INPUT            : pDnsQueryInfo -  Query Datastructure
 **
 **
 **
 **    RETURNS          : DNS_SUCCESS / DNS_FAILURE
 **
 ******************************************************************************/

PUBLIC INT4
DnsReqSendQuery (tDnsQueryInfo * pDnsQueryInfo)
{
    INT4                i4RetVal = DNS_FAILURE;
    switch (gDnsResolverParams.i4QueryMode)
    {
        case DNS_MODE_SEQUENCE:
            i4RetVal = DNSResolSeqMode (pDnsQueryInfo);
            break;

        case DNS_MODE_SIMUL:
            i4RetVal = DNSResolSimulMode (pDnsQueryInfo);
            break;
        default:
            return DNS_FAILURE;
    }
    return i4RetVal;
}

/****************************************************************************
 **
 **    FUNCTION NAME    : DNSResolSeqMode
 **
 **    DESCRIPTION      : This function perform Sequential Mode of Querying
 **                       to Name servers
 **
 **    INPUT            : pDnsQueryInfo -  Query Datastructure
 **
 **
 **
 **    RETURNS          : DNS_SUCCESS / DNS_FAILURE
 **
 ******************************************************************************/
PUBLIC INT4
DNSResolSeqMode (tDnsQueryInfo * pDnsQueryInfo)
{

    INT4                i4RetVal = DNS_FAILURE;

    i4RetVal = DnsQrySendNameserver (pDnsQueryInfo);
    return i4RetVal;
}

/****************************************************************************
 **
 **    FUNCTION NAME    : DNSResolSimulMode
 **
 **    DESCRIPTION      : This function is used to Send Queries Simultaneously
 **                       
 **
 **    INPUT            : pDnsQueryInfov4 - IPv4 Query Datastructure
 **                       pDnsQueryInfov6 - IPv6 Query Datastructure
 **
 **
 **    OUTPUT           : pi4V4Status - IPv4 Resolved Status
 **                       pi4V6Status - IPv6 Resolved Status
 **
 **    RETURNS          : DNS_SUCCESS / DNS_FAILURE
 **
 ******************************************************************************/
PUBLIC INT4
DNSResolSimulMode (tDnsQueryInfo * pDnsQueryInfo)
{
    UINT4               u4NSTotCnt = 0;
    UINT4               u4NSCnt = 0;
    UINT1               u1NoNameFlg = OSIX_FALSE;

    u4NSTotCnt = TMO_SLL_Count (&gDnsGlobalInfo.NameServerList);
    while (u4NSCnt < u4NSTotCnt)
    {
        DnsQrySendNameserver (pDnsQueryInfo);
        u4NSCnt++;
    }
    for (u4NSCnt = 0; u4NSCnt < u4NSTotCnt; u4NSCnt++)
    {
        if (((pDnsQueryInfo->au1ErrorNum[u4NSCnt] & DNS_RCODE_NO_NAME) !=
             DNS_RCODE_NO_NAME)
            || ((pDnsQueryInfo->au1ErrorNum[u4NSCnt] & DNS_RCODE_SERV_FAIL) !=
                DNS_RCODE_SERV_FAIL))
        {
            u1NoNameFlg = OSIX_TRUE;
        }
    }
    if (u1NoNameFlg == OSIX_FALSE)
    {
        DnsDeleteQueryData (pDnsQueryInfo);

    }

    return DNS_SUCCESS;
}
