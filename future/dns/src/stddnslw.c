/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stddnslw.c,v 1.7 2014/10/13 12:08:08 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/

#include "dnsinc.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDnsResConfigImplementIdent
 Input       :  The Indices

                The Object 
                retValDnsResConfigImplementIdent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResConfigImplementIdent (tSNMP_OCTET_STRING_TYPE *
                                  pRetValDnsResConfigImplementIdent)
{
    UNUSED_PARAM (pRetValDnsResConfigImplementIdent);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResConfigService
 Input       :  The Indices

                The Object 
                retValDnsResConfigService
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResConfigService (INT4 *pi4RetValDnsResConfigService)
{
    UNUSED_PARAM (pi4RetValDnsResConfigService);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResConfigMaxCnames
 Input       :  The Indices

                The Object 
                retValDnsResConfigMaxCnames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResConfigMaxCnames (INT4 *pi4RetValDnsResConfigMaxCnames)
{
    UNUSED_PARAM (pi4RetValDnsResConfigMaxCnames);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDnsResConfigMaxCnames
 Input       :  The Indices

                The Object 
                setValDnsResConfigMaxCnames
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDnsResConfigMaxCnames (INT4 i4SetValDnsResConfigMaxCnames)
{
    UNUSED_PARAM (i4SetValDnsResConfigMaxCnames);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DnsResConfigMaxCnames
 Input       :  The Indices

                The Object 
                testValDnsResConfigMaxCnames
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DnsResConfigMaxCnames (UINT4 *pu4ErrorCode,
                                INT4 i4TestValDnsResConfigMaxCnames)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValDnsResConfigMaxCnames);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DnsResConfigMaxCnames
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DnsResConfigMaxCnames (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DnsResConfigSbeltTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDnsResConfigSbeltTable
 Input       :  The Indices
                DnsResConfigSbeltAddr
                DnsResConfigSbeltSubTree
                DnsResConfigSbeltClass
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDnsResConfigSbeltTable (UINT4 u4DnsResConfigSbeltAddr,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pDnsResConfigSbeltSubTree,
                                                INT4 i4DnsResConfigSbeltClass)
{
    UNUSED_PARAM (u4DnsResConfigSbeltAddr);
    UNUSED_PARAM (pDnsResConfigSbeltSubTree);
    UNUSED_PARAM (i4DnsResConfigSbeltClass);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDnsResConfigSbeltTable
 Input       :  The Indices
                DnsResConfigSbeltAddr
                DnsResConfigSbeltSubTree
                DnsResConfigSbeltClass
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDnsResConfigSbeltTable (UINT4 *pu4DnsResConfigSbeltAddr,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pDnsResConfigSbeltSubTree,
                                        INT4 *pi4DnsResConfigSbeltClass)
{
    UNUSED_PARAM (pu4DnsResConfigSbeltAddr);
    UNUSED_PARAM (pDnsResConfigSbeltSubTree);
    UNUSED_PARAM (pi4DnsResConfigSbeltClass);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDnsResConfigSbeltTable
 Input       :  The Indices
                DnsResConfigSbeltAddr
                nextDnsResConfigSbeltAddr
                DnsResConfigSbeltSubTree
                nextDnsResConfigSbeltSubTree
                DnsResConfigSbeltClass
                nextDnsResConfigSbeltClass
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDnsResConfigSbeltTable (UINT4 u4DnsResConfigSbeltAddr,
                                       UINT4 *pu4NextDnsResConfigSbeltAddr,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pDnsResConfigSbeltSubTree,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextDnsResConfigSbeltSubTree,
                                       INT4 i4DnsResConfigSbeltClass,
                                       INT4 *pi4NextDnsResConfigSbeltClass)
{
    UNUSED_PARAM (u4DnsResConfigSbeltAddr);
    UNUSED_PARAM (pu4NextDnsResConfigSbeltAddr);
    UNUSED_PARAM (pDnsResConfigSbeltSubTree);
    UNUSED_PARAM (pNextDnsResConfigSbeltSubTree);
    UNUSED_PARAM (i4DnsResConfigSbeltClass);
    UNUSED_PARAM (pi4NextDnsResConfigSbeltClass);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDnsResConfigSbeltName
 Input       :  The Indices
                DnsResConfigSbeltAddr
                DnsResConfigSbeltSubTree
                DnsResConfigSbeltClass

                The Object 
                retValDnsResConfigSbeltName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResConfigSbeltName (UINT4 u4DnsResConfigSbeltAddr,
                             tSNMP_OCTET_STRING_TYPE *
                             pDnsResConfigSbeltSubTree,
                             INT4 i4DnsResConfigSbeltClass,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValDnsResConfigSbeltName)
{
    UNUSED_PARAM (u4DnsResConfigSbeltAddr);
    UNUSED_PARAM (pDnsResConfigSbeltSubTree);
    UNUSED_PARAM (i4DnsResConfigSbeltClass);
    UNUSED_PARAM (pRetValDnsResConfigSbeltName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResConfigSbeltRecursion
 Input       :  The Indices
                DnsResConfigSbeltAddr
                DnsResConfigSbeltSubTree
                DnsResConfigSbeltClass

                The Object 
                retValDnsResConfigSbeltRecursion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResConfigSbeltRecursion (UINT4 u4DnsResConfigSbeltAddr,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pDnsResConfigSbeltSubTree,
                                  INT4 i4DnsResConfigSbeltClass,
                                  INT4 *pi4RetValDnsResConfigSbeltRecursion)
{
    UNUSED_PARAM (u4DnsResConfigSbeltAddr);
    UNUSED_PARAM (pDnsResConfigSbeltSubTree);
    UNUSED_PARAM (i4DnsResConfigSbeltClass);
    UNUSED_PARAM (pi4RetValDnsResConfigSbeltRecursion);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResConfigSbeltPref
 Input       :  The Indices
                DnsResConfigSbeltAddr
                DnsResConfigSbeltSubTree
                DnsResConfigSbeltClass

                The Object 
                retValDnsResConfigSbeltPref
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResConfigSbeltPref (UINT4 u4DnsResConfigSbeltAddr,
                             tSNMP_OCTET_STRING_TYPE *
                             pDnsResConfigSbeltSubTree,
                             INT4 i4DnsResConfigSbeltClass,
                             INT4 *pi4RetValDnsResConfigSbeltPref)
{
    UNUSED_PARAM (u4DnsResConfigSbeltAddr);
    UNUSED_PARAM (pDnsResConfigSbeltSubTree);
    UNUSED_PARAM (i4DnsResConfigSbeltClass);
    UNUSED_PARAM (pi4RetValDnsResConfigSbeltPref);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResConfigSbeltStatus
 Input       :  The Indices
                DnsResConfigSbeltAddr
                DnsResConfigSbeltSubTree
                DnsResConfigSbeltClass

                The Object 
                retValDnsResConfigSbeltStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResConfigSbeltStatus (UINT4 u4DnsResConfigSbeltAddr,
                               tSNMP_OCTET_STRING_TYPE *
                               pDnsResConfigSbeltSubTree,
                               INT4 i4DnsResConfigSbeltClass,
                               INT4 *pi4RetValDnsResConfigSbeltStatus)
{
    UNUSED_PARAM (u4DnsResConfigSbeltAddr);
    UNUSED_PARAM (pDnsResConfigSbeltSubTree);
    UNUSED_PARAM (i4DnsResConfigSbeltClass);
    UNUSED_PARAM (pi4RetValDnsResConfigSbeltStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDnsResConfigSbeltName
 Input       :  The Indices
                DnsResConfigSbeltAddr
                DnsResConfigSbeltSubTree
                DnsResConfigSbeltClass

                The Object 
                setValDnsResConfigSbeltName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDnsResConfigSbeltName (UINT4 u4DnsResConfigSbeltAddr,
                             tSNMP_OCTET_STRING_TYPE *
                             pDnsResConfigSbeltSubTree,
                             INT4 i4DnsResConfigSbeltClass,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValDnsResConfigSbeltName)
{
    UNUSED_PARAM (u4DnsResConfigSbeltAddr);
    UNUSED_PARAM (pDnsResConfigSbeltSubTree);
    UNUSED_PARAM (i4DnsResConfigSbeltClass);
    UNUSED_PARAM (pSetValDnsResConfigSbeltName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDnsResConfigSbeltRecursion
 Input       :  The Indices
                DnsResConfigSbeltAddr
                DnsResConfigSbeltSubTree
                DnsResConfigSbeltClass

                The Object 
                setValDnsResConfigSbeltRecursion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDnsResConfigSbeltRecursion (UINT4 u4DnsResConfigSbeltAddr,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pDnsResConfigSbeltSubTree,
                                  INT4 i4DnsResConfigSbeltClass,
                                  INT4 i4SetValDnsResConfigSbeltRecursion)
{
    UNUSED_PARAM (u4DnsResConfigSbeltAddr);
    UNUSED_PARAM (pDnsResConfigSbeltSubTree);
    UNUSED_PARAM (i4DnsResConfigSbeltClass);
    UNUSED_PARAM (i4SetValDnsResConfigSbeltRecursion);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDnsResConfigSbeltPref
 Input       :  The Indices
                DnsResConfigSbeltAddr
                DnsResConfigSbeltSubTree
                DnsResConfigSbeltClass

                The Object 
                setValDnsResConfigSbeltPref
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDnsResConfigSbeltPref (UINT4 u4DnsResConfigSbeltAddr,
                             tSNMP_OCTET_STRING_TYPE *
                             pDnsResConfigSbeltSubTree,
                             INT4 i4DnsResConfigSbeltClass,
                             INT4 i4SetValDnsResConfigSbeltPref)
{
    UNUSED_PARAM (u4DnsResConfigSbeltAddr);
    UNUSED_PARAM (pDnsResConfigSbeltSubTree);
    UNUSED_PARAM (i4DnsResConfigSbeltClass);
    UNUSED_PARAM (i4SetValDnsResConfigSbeltPref);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDnsResConfigSbeltStatus
 Input       :  The Indices
                DnsResConfigSbeltAddr
                DnsResConfigSbeltSubTree
                DnsResConfigSbeltClass

                The Object 
                setValDnsResConfigSbeltStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDnsResConfigSbeltStatus (UINT4 u4DnsResConfigSbeltAddr,
                               tSNMP_OCTET_STRING_TYPE *
                               pDnsResConfigSbeltSubTree,
                               INT4 i4DnsResConfigSbeltClass,
                               INT4 i4SetValDnsResConfigSbeltStatus)
{
    UNUSED_PARAM (u4DnsResConfigSbeltAddr);
    UNUSED_PARAM (pDnsResConfigSbeltSubTree);
    UNUSED_PARAM (i4DnsResConfigSbeltClass);
    UNUSED_PARAM (i4SetValDnsResConfigSbeltStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DnsResConfigSbeltName
 Input       :  The Indices
                DnsResConfigSbeltAddr
                DnsResConfigSbeltSubTree
                DnsResConfigSbeltClass

                The Object 
                testValDnsResConfigSbeltName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DnsResConfigSbeltName (UINT4 *pu4ErrorCode,
                                UINT4 u4DnsResConfigSbeltAddr,
                                tSNMP_OCTET_STRING_TYPE *
                                pDnsResConfigSbeltSubTree,
                                INT4 i4DnsResConfigSbeltClass,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValDnsResConfigSbeltName)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4DnsResConfigSbeltAddr);
    UNUSED_PARAM (pDnsResConfigSbeltSubTree);
    UNUSED_PARAM (i4DnsResConfigSbeltClass);
    UNUSED_PARAM (pTestValDnsResConfigSbeltName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DnsResConfigSbeltRecursion
 Input       :  The Indices
                DnsResConfigSbeltAddr
                DnsResConfigSbeltSubTree
                DnsResConfigSbeltClass

                The Object 
                testValDnsResConfigSbeltRecursion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DnsResConfigSbeltRecursion (UINT4 *pu4ErrorCode,
                                     UINT4 u4DnsResConfigSbeltAddr,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pDnsResConfigSbeltSubTree,
                                     INT4 i4DnsResConfigSbeltClass,
                                     INT4 i4TestValDnsResConfigSbeltRecursion)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4DnsResConfigSbeltAddr);
    UNUSED_PARAM (pDnsResConfigSbeltSubTree);
    UNUSED_PARAM (i4DnsResConfigSbeltClass);
    UNUSED_PARAM (i4TestValDnsResConfigSbeltRecursion);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DnsResConfigSbeltPref
 Input       :  The Indices
                DnsResConfigSbeltAddr
                DnsResConfigSbeltSubTree
                DnsResConfigSbeltClass

                The Object 
                testValDnsResConfigSbeltPref
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DnsResConfigSbeltPref (UINT4 *pu4ErrorCode,
                                UINT4 u4DnsResConfigSbeltAddr,
                                tSNMP_OCTET_STRING_TYPE *
                                pDnsResConfigSbeltSubTree,
                                INT4 i4DnsResConfigSbeltClass,
                                INT4 i4TestValDnsResConfigSbeltPref)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4DnsResConfigSbeltAddr);
    UNUSED_PARAM (pDnsResConfigSbeltSubTree);
    UNUSED_PARAM (i4DnsResConfigSbeltClass);
    UNUSED_PARAM (i4TestValDnsResConfigSbeltPref);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DnsResConfigSbeltStatus
 Input       :  The Indices
                DnsResConfigSbeltAddr
                DnsResConfigSbeltSubTree
                DnsResConfigSbeltClass

                The Object 
                testValDnsResConfigSbeltStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DnsResConfigSbeltStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4DnsResConfigSbeltAddr,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pDnsResConfigSbeltSubTree,
                                  INT4 i4DnsResConfigSbeltClass,
                                  INT4 i4TestValDnsResConfigSbeltStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4DnsResConfigSbeltAddr);
    UNUSED_PARAM (pDnsResConfigSbeltSubTree);
    UNUSED_PARAM (i4DnsResConfigSbeltClass);
    UNUSED_PARAM (i4TestValDnsResConfigSbeltStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DnsResConfigSbeltTable
 Input       :  The Indices
                DnsResConfigSbeltAddr
                DnsResConfigSbeltSubTree
                DnsResConfigSbeltClass
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DnsResConfigSbeltTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDnsResConfigUpTime
 Input       :  The Indices

                The Object 
                retValDnsResConfigUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResConfigUpTime (UINT4 *pu4RetValDnsResConfigUpTime)
{
    UNUSED_PARAM (pu4RetValDnsResConfigUpTime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResConfigResetTime
 Input       :  The Indices

                The Object 
                retValDnsResConfigResetTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResConfigResetTime (UINT4 *pu4RetValDnsResConfigResetTime)
{
    UNUSED_PARAM (pu4RetValDnsResConfigResetTime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResConfigReset
 Input       :  The Indices

                The Object 
                retValDnsResConfigReset
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResConfigReset (INT4 *pi4RetValDnsResConfigReset)
{
    UNUSED_PARAM (pi4RetValDnsResConfigReset);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDnsResConfigReset
 Input       :  The Indices

                The Object 
                setValDnsResConfigReset
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDnsResConfigReset (INT4 i4SetValDnsResConfigReset)
{
    UNUSED_PARAM (i4SetValDnsResConfigReset);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DnsResConfigReset
 Input       :  The Indices

                The Object 
                testValDnsResConfigReset
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DnsResConfigReset (UINT4 *pu4ErrorCode,
                            INT4 i4TestValDnsResConfigReset)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValDnsResConfigReset);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DnsResConfigReset
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DnsResConfigReset (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DnsResCounterByOpcodeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDnsResCounterByOpcodeTable
 Input       :  The Indices
                DnsResCounterByOpcodeCode
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDnsResCounterByOpcodeTable (INT4
                                                    i4DnsResCounterByOpcodeCode)
{
    UNUSED_PARAM (i4DnsResCounterByOpcodeCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDnsResCounterByOpcodeTable
 Input       :  The Indices
                DnsResCounterByOpcodeCode
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDnsResCounterByOpcodeTable (INT4 *pi4DnsResCounterByOpcodeCode)
{
    UNUSED_PARAM (pi4DnsResCounterByOpcodeCode);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDnsResCounterByOpcodeTable
 Input       :  The Indices
                DnsResCounterByOpcodeCode
                nextDnsResCounterByOpcodeCode
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDnsResCounterByOpcodeTable (INT4 i4DnsResCounterByOpcodeCode,
                                           INT4
                                           *pi4NextDnsResCounterByOpcodeCode)
{
    UNUSED_PARAM (i4DnsResCounterByOpcodeCode);
    UNUSED_PARAM (pi4NextDnsResCounterByOpcodeCode);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDnsResCounterByOpcodeQueries
 Input       :  The Indices
                DnsResCounterByOpcodeCode

                The Object 
                retValDnsResCounterByOpcodeQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResCounterByOpcodeQueries (INT4 i4DnsResCounterByOpcodeCode,
                                    UINT4
                                    *pu4RetValDnsResCounterByOpcodeQueries)
{
    UNUSED_PARAM (i4DnsResCounterByOpcodeCode);
    UNUSED_PARAM (pu4RetValDnsResCounterByOpcodeQueries);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResCounterByOpcodeResponses
 Input       :  The Indices
                DnsResCounterByOpcodeCode

                The Object 
                retValDnsResCounterByOpcodeResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResCounterByOpcodeResponses (INT4 i4DnsResCounterByOpcodeCode,
                                      UINT4
                                      *pu4RetValDnsResCounterByOpcodeResponses)
{
    UNUSED_PARAM (i4DnsResCounterByOpcodeCode);
    UNUSED_PARAM (pu4RetValDnsResCounterByOpcodeResponses);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DnsResCounterByRcodeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDnsResCounterByRcodeTable
 Input       :  The Indices
                DnsResCounterByRcodeCode
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDnsResCounterByRcodeTable (INT4
                                                   i4DnsResCounterByRcodeCode)
{
    UNUSED_PARAM (i4DnsResCounterByRcodeCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDnsResCounterByRcodeTable
 Input       :  The Indices
                DnsResCounterByRcodeCode
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDnsResCounterByRcodeTable (INT4 *pi4DnsResCounterByRcodeCode)
{
    UNUSED_PARAM (pi4DnsResCounterByRcodeCode);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDnsResCounterByRcodeTable
 Input       :  The Indices
                DnsResCounterByRcodeCode
                nextDnsResCounterByRcodeCode
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDnsResCounterByRcodeTable (INT4 i4DnsResCounterByRcodeCode,
                                          INT4 *pi4NextDnsResCounterByRcodeCode)
{
    UNUSED_PARAM (i4DnsResCounterByRcodeCode);
    UNUSED_PARAM (pi4NextDnsResCounterByRcodeCode);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDnsResCounterByRcodeResponses
 Input       :  The Indices
                DnsResCounterByRcodeCode

                The Object 
                retValDnsResCounterByRcodeResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResCounterByRcodeResponses (INT4 i4DnsResCounterByRcodeCode,
                                     UINT4
                                     *pu4RetValDnsResCounterByRcodeResponses)
{
    UNUSED_PARAM (i4DnsResCounterByRcodeCode);
    UNUSED_PARAM (pu4RetValDnsResCounterByRcodeResponses);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDnsResCounterNonAuthDataResps
 Input       :  The Indices

                The Object 
                retValDnsResCounterNonAuthDataResps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResCounterNonAuthDataResps (UINT4
                                     *pu4RetValDnsResCounterNonAuthDataResps)
{
    UNUSED_PARAM (pu4RetValDnsResCounterNonAuthDataResps);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResCounterNonAuthNoDataResps
 Input       :  The Indices

                The Object 
                retValDnsResCounterNonAuthNoDataResps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResCounterNonAuthNoDataResps (UINT4
                                       *pu4RetValDnsResCounterNonAuthNoDataResps)
{
    UNUSED_PARAM (pu4RetValDnsResCounterNonAuthNoDataResps);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResCounterMartians
 Input       :  The Indices

                The Object 
                retValDnsResCounterMartians
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResCounterMartians (UINT4 *pu4RetValDnsResCounterMartians)
{
    UNUSED_PARAM (pu4RetValDnsResCounterMartians);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResCounterRecdResponses
 Input       :  The Indices

                The Object 
                retValDnsResCounterRecdResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResCounterRecdResponses (UINT4 *pu4RetValDnsResCounterRecdResponses)
{
    UNUSED_PARAM (pu4RetValDnsResCounterRecdResponses);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResCounterUnparseResps
 Input       :  The Indices

                The Object 
                retValDnsResCounterUnparseResps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResCounterUnparseResps (UINT4 *pu4RetValDnsResCounterUnparseResps)
{
    UNUSED_PARAM (pu4RetValDnsResCounterUnparseResps);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResCounterFallbacks
 Input       :  The Indices

                The Object 
                retValDnsResCounterFallbacks
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResCounterFallbacks (UINT4 *pu4RetValDnsResCounterFallbacks)
{
    UNUSED_PARAM (pu4RetValDnsResCounterFallbacks);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDnsResLameDelegationOverflows
 Input       :  The Indices

                The Object 
                retValDnsResLameDelegationOverflows
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResLameDelegationOverflows (UINT4
                                     *pu4RetValDnsResLameDelegationOverflows)
{
    UNUSED_PARAM (pu4RetValDnsResLameDelegationOverflows);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DnsResLameDelegationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDnsResLameDelegationTable
 Input       :  The Indices
                DnsResLameDelegationSource
                DnsResLameDelegationName
                DnsResLameDelegationClass
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDnsResLameDelegationTable (UINT4
                                                   u4DnsResLameDelegationSource,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pDnsResLameDelegationName,
                                                   INT4
                                                   i4DnsResLameDelegationClass)
{
    UNUSED_PARAM (u4DnsResLameDelegationSource);
    UNUSED_PARAM (pDnsResLameDelegationName);
    UNUSED_PARAM (i4DnsResLameDelegationClass);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDnsResLameDelegationTable
 Input       :  The Indices
                DnsResLameDelegationSource
                DnsResLameDelegationName
                DnsResLameDelegationClass
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDnsResLameDelegationTable (UINT4 *pu4DnsResLameDelegationSource,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pDnsResLameDelegationName,
                                           INT4 *pi4DnsResLameDelegationClass)
{
    UNUSED_PARAM (pu4DnsResLameDelegationSource);
    UNUSED_PARAM (pDnsResLameDelegationName);
    UNUSED_PARAM (pi4DnsResLameDelegationClass);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDnsResLameDelegationTable
 Input       :  The Indices
                DnsResLameDelegationSource
                nextDnsResLameDelegationSource
                DnsResLameDelegationName
                nextDnsResLameDelegationName
                DnsResLameDelegationClass
                nextDnsResLameDelegationClass
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDnsResLameDelegationTable (UINT4 u4DnsResLameDelegationSource,
                                          UINT4
                                          *pu4NextDnsResLameDelegationSource,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pDnsResLameDelegationName,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pNextDnsResLameDelegationName,
                                          INT4 i4DnsResLameDelegationClass,
                                          INT4
                                          *pi4NextDnsResLameDelegationClass)
{
    UNUSED_PARAM (u4DnsResLameDelegationSource);
    UNUSED_PARAM (pu4NextDnsResLameDelegationSource);
    UNUSED_PARAM (pDnsResLameDelegationName);
    UNUSED_PARAM (pNextDnsResLameDelegationName);
    UNUSED_PARAM (i4DnsResLameDelegationClass);
    UNUSED_PARAM (pi4NextDnsResLameDelegationClass);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDnsResLameDelegationCounts
 Input       :  The Indices
                DnsResLameDelegationSource
                DnsResLameDelegationName
                DnsResLameDelegationClass

                The Object 
                retValDnsResLameDelegationCounts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResLameDelegationCounts (UINT4 u4DnsResLameDelegationSource,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pDnsResLameDelegationName,
                                  INT4 i4DnsResLameDelegationClass,
                                  UINT4 *pu4RetValDnsResLameDelegationCounts)
{
    UNUSED_PARAM (u4DnsResLameDelegationSource);
    UNUSED_PARAM (pDnsResLameDelegationName);
    UNUSED_PARAM (i4DnsResLameDelegationClass);
    UNUSED_PARAM (pu4RetValDnsResLameDelegationCounts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResLameDelegationStatus
 Input       :  The Indices
                DnsResLameDelegationSource
                DnsResLameDelegationName
                DnsResLameDelegationClass

                The Object 
                retValDnsResLameDelegationStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResLameDelegationStatus (UINT4 u4DnsResLameDelegationSource,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pDnsResLameDelegationName,
                                  INT4 i4DnsResLameDelegationClass,
                                  INT4 *pi4RetValDnsResLameDelegationStatus)
{
    UNUSED_PARAM (u4DnsResLameDelegationSource);
    UNUSED_PARAM (pDnsResLameDelegationName);
    UNUSED_PARAM (i4DnsResLameDelegationClass);
    UNUSED_PARAM (pi4RetValDnsResLameDelegationStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDnsResLameDelegationStatus
 Input       :  The Indices
                DnsResLameDelegationSource
                DnsResLameDelegationName
                DnsResLameDelegationClass

                The Object 
                setValDnsResLameDelegationStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDnsResLameDelegationStatus (UINT4 u4DnsResLameDelegationSource,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pDnsResLameDelegationName,
                                  INT4 i4DnsResLameDelegationClass,
                                  INT4 i4SetValDnsResLameDelegationStatus)
{
    UNUSED_PARAM (u4DnsResLameDelegationSource);
    UNUSED_PARAM (pDnsResLameDelegationName);
    UNUSED_PARAM (i4DnsResLameDelegationClass);
    UNUSED_PARAM (i4SetValDnsResLameDelegationStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DnsResLameDelegationStatus
 Input       :  The Indices
                DnsResLameDelegationSource
                DnsResLameDelegationName
                DnsResLameDelegationClass

                The Object 
                testValDnsResLameDelegationStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DnsResLameDelegationStatus (UINT4 *pu4ErrorCode,
                                     UINT4 u4DnsResLameDelegationSource,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pDnsResLameDelegationName,
                                     INT4 i4DnsResLameDelegationClass,
                                     INT4 i4TestValDnsResLameDelegationStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4DnsResLameDelegationSource);
    UNUSED_PARAM (pDnsResLameDelegationName);
    UNUSED_PARAM (i4DnsResLameDelegationClass);
    UNUSED_PARAM (i4TestValDnsResLameDelegationStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DnsResLameDelegationTable
 Input       :  The Indices
                DnsResLameDelegationSource
                DnsResLameDelegationName
                DnsResLameDelegationClass
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DnsResLameDelegationTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDnsResCacheStatus
 Input       :  The Indices

                The Object 
                retValDnsResCacheStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResCacheStatus (INT4 *pi4RetValDnsResCacheStatus)
{
    *pi4RetValDnsResCacheStatus = (INT4) gDnsCacheInfo.u4Status;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResCacheMaxTTL
 Input       :  The Indices

                The Object 
                retValDnsResCacheMaxTTL
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResCacheMaxTTL (UINT4 *pu4RetValDnsResCacheMaxTTL)
{
    *pu4RetValDnsResCacheMaxTTL = gDnsCacheInfo.u4MaxTTL;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResCacheGoodCaches
 Input       :  The Indices

                The Object 
                retValDnsResCacheGoodCaches
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResCacheGoodCaches (UINT4 *pu4RetValDnsResCacheGoodCaches)
{
    *pu4RetValDnsResCacheGoodCaches = gDnsCacheInfo.u4TotalEntries;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResCacheBadCaches
 Input       :  The Indices

                The Object 
                retValDnsResCacheBadCaches
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResCacheBadCaches (UINT4 *pu4RetValDnsResCacheBadCaches)
{
    *pu4RetValDnsResCacheBadCaches = DNS_ZERO;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDnsResCacheStatus
 Input       :  The Indices

                The Object 
                setValDnsResCacheStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDnsResCacheStatus (INT4 i4SetValDnsResCacheStatus)
{
    if (i4SetValDnsResCacheStatus == (INT4) gDnsCacheInfo.u4Status)
    {
        return SNMP_SUCCESS;
    }

    if (i4SetValDnsResCacheStatus == DNS_CACHE_CLEAR)
    {
        DnsCacheClear ();
        return SNMP_SUCCESS;
    }

    gDnsCacheInfo.u4Status = (UINT4) i4SetValDnsResCacheStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDnsResCacheMaxTTL
 Input       :  The Indices

                The Object 
                setValDnsResCacheMaxTTL
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDnsResCacheMaxTTL (UINT4 u4SetValDnsResCacheMaxTTL)
{
    gDnsCacheInfo.u4MaxTTL = u4SetValDnsResCacheMaxTTL;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DnsResCacheStatus
 Input       :  The Indices

                The Object 
                testValDnsResCacheStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DnsResCacheStatus (UINT4 *pu4ErrorCode,
                            INT4 i4TestValDnsResCacheStatus)
{

    if (gDnsGlobalInfo.u1SystemCtrlStatus != DNS_START)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "DNS Module is Shutdown\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (DNS_CLI_SHUTDOWN);
        return SNMP_FAILURE;
    }

    if ((i4TestValDnsResCacheStatus != DNS_CACHE_ENABLE) &&
        (i4TestValDnsResCacheStatus != DNS_CACHE_DISABLE) &&
        (i4TestValDnsResCacheStatus != DNS_CACHE_CLEAR))
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Invalid DNS Cache value\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (DNS_CLI_INVALID_CACHE_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DnsResCacheMaxTTL
 Input       :  The Indices

                The Object 
                testValDnsResCacheMaxTTL
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DnsResCacheMaxTTL (UINT4 *pu4ErrorCode,
                            UINT4 u4TestValDnsResCacheMaxTTL)
{
    if (gDnsGlobalInfo.u1SystemCtrlStatus != DNS_START)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "DNS Module is Shutdown\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (DNS_CLI_SHUTDOWN);
        return SNMP_FAILURE;
    }

    if ((u4TestValDnsResCacheMaxTTL < DNS_MIN_CACHE_TTL) ||
        (u4TestValDnsResCacheMaxTTL > DNS_MAX_CACHE_TTL))
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Invalid DNS Cache TTL Value\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (DNS_CLI_INVALID_CACHE_TTL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DnsResCacheStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DnsResCacheStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2DnsResCacheMaxTTL
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DnsResCacheMaxTTL (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DnsResCacheRRTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDnsResCacheRRTable
 Input       :  The Indices
                DnsResCacheRRName
                DnsResCacheRRClass
                DnsResCacheRRType
                DnsResCacheRRIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDnsResCacheRRTable (tSNMP_OCTET_STRING_TYPE *
                                            pDnsResCacheRRName,
                                            INT4 i4DnsResCacheRRClass,
                                            INT4 i4DnsResCacheRRType,
                                            INT4 i4DnsResCacheRRIndex)
{

    if (pDnsResCacheRRName->i4_Length <= 0)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Invalid Cache Record name\r\n"));
        return SNMP_FAILURE;
    }

    if (STRCMP (pDnsResCacheRRName->pu1_OctetList, "") == 0)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Invalid Cache Record name\r\n"));
        return SNMP_FAILURE;
    }

    if (i4DnsResCacheRRClass != DNS_INTERNET_CLASS)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Invalid Cache Record Class \r\n"));
        return SNMP_FAILURE;
    }

    if ((i4DnsResCacheRRType != DNS_HOST_ADDRESS) &&
        (i4DnsResCacheRRType != DNS_AAAA_RECORD))
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Invalid Cache Record Type \r\n"));
        return SNMP_FAILURE;
    }

    if (i4DnsResCacheRRIndex == DNS_ZERO)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Invalid Cache Record Index \r\n"));
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDnsResCacheRRTable
 Input       :  The Indices
                DnsResCacheRRName
                DnsResCacheRRClass
                DnsResCacheRRType
                DnsResCacheRRIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDnsResCacheRRTable (tSNMP_OCTET_STRING_TYPE *
                                    pDnsResCacheRRName,
                                    INT4 *pi4DnsResCacheRRClass,
                                    INT4 *pi4DnsResCacheRRType,
                                    INT4 *pi4DnsResCacheRRIndex)
{
    tDnsCacheRRInfo    *pDnsCacheRRInfo = NULL;

    if (gDnsGlobalInfo.u1SystemCtrlStatus != DNS_START)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "DNS Module is Shutdown\r\n"));
        return SNMP_FAILURE;
    }

    if ((pDnsCacheRRInfo =
         (tDnsCacheRRInfo *) RBTreeGetFirst (gDnsCacheInfo.CacheList)) == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "DNS Cache list is empty\r\n"));
        return SNMP_FAILURE;
    }

    MEMSET (pDnsResCacheRRName->pu1_OctetList, DNS_ZERO, DNS_MAX_QUERY_LEN);
    MEMCPY (pDnsResCacheRRName->pu1_OctetList, pDnsCacheRRInfo->au1RRName,
            STRLEN (pDnsCacheRRInfo->au1RRName));
    pDnsResCacheRRName->i4_Length =
        (INT4) STRLEN (pDnsCacheRRInfo->au1RRName) + 1;
    *pi4DnsResCacheRRClass = (INT4) pDnsCacheRRInfo->u4RRClass;
    *pi4DnsResCacheRRType = (INT4) pDnsCacheRRInfo->u4RRType;
    *pi4DnsResCacheRRIndex = (INT4) pDnsCacheRRInfo->u4RRIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDnsResCacheRRTable
 Input       :  The Indices
                DnsResCacheRRName
                nextDnsResCacheRRName
                DnsResCacheRRClass
                nextDnsResCacheRRClass
                DnsResCacheRRType
                nextDnsResCacheRRType
                DnsResCacheRRIndex
                nextDnsResCacheRRIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDnsResCacheRRTable (tSNMP_OCTET_STRING_TYPE * pDnsResCacheRRName,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextDnsResCacheRRName,
                                   INT4 i4DnsResCacheRRClass,
                                   INT4 *pi4NextDnsResCacheRRClass,
                                   INT4 i4DnsResCacheRRType,
                                   INT4 *pi4NextDnsResCacheRRType,
                                   INT4 i4DnsResCacheRRIndex,
                                   INT4 *pi4NextDnsResCacheRRIndex)
{
    tDnsCacheRRInfo    *pDnsCacheRRInfo = NULL;
    tDnsCacheRRInfo    *pDnsNextCacheRRInfo = NULL;

    pDnsCacheRRInfo =
        DnsGetCacheRR (pDnsResCacheRRName->pu1_OctetList,
                       (UINT4) i4DnsResCacheRRClass,
                       (UINT4) i4DnsResCacheRRType,
                       (UINT4) i4DnsResCacheRRIndex);
    if (pDnsCacheRRInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Invalid Cache record indices\r\n"));
        return SNMP_FAILURE;
    }

    pDnsNextCacheRRInfo =
        (tDnsCacheRRInfo *) RBTreeGetNext (gDnsCacheInfo.CacheList,
                                           pDnsCacheRRInfo, NULL);

    if (pDnsNextCacheRRInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pNextDnsResCacheRRName->pu1_OctetList, DNS_ZERO, DNS_MAX_QUERY_LEN);
    MEMCPY (pNextDnsResCacheRRName->pu1_OctetList,
            pDnsNextCacheRRInfo->au1RRName,
            STRLEN (pDnsNextCacheRRInfo->au1RRName));
    pNextDnsResCacheRRName->i4_Length =
        (INT4) STRLEN (pDnsNextCacheRRInfo->au1RRName) + 1;
    *pi4NextDnsResCacheRRClass = (INT4) pDnsNextCacheRRInfo->u4RRClass;
    *pi4NextDnsResCacheRRType = (INT4) pDnsNextCacheRRInfo->u4RRType;
    *pi4NextDnsResCacheRRIndex = (INT4) pDnsNextCacheRRInfo->u4RRIndex;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDnsResCacheRRTTL
 Input       :  The Indices
                DnsResCacheRRName
                DnsResCacheRRClass
                DnsResCacheRRType
                DnsResCacheRRIndex

                The Object 
                retValDnsResCacheRRTTL
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResCacheRRTTL (tSNMP_OCTET_STRING_TYPE * pDnsResCacheRRName,
                        INT4 i4DnsResCacheRRClass, INT4 i4DnsResCacheRRType,
                        INT4 i4DnsResCacheRRIndex,
                        UINT4 *pu4RetValDnsResCacheRRTTL)
{
    tDnsCacheRRInfo    *pDnsCacheRRInfo = NULL;

    pDnsCacheRRInfo =
        DnsGetCacheRR (pDnsResCacheRRName->pu1_OctetList,
                       (UINT4) i4DnsResCacheRRClass,
                       (UINT4) i4DnsResCacheRRType,
                       (UINT4) i4DnsResCacheRRIndex);
    if (pDnsCacheRRInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Invalid Cache record indices\r\n"));
        return SNMP_FAILURE;
    }

    *pu4RetValDnsResCacheRRTTL = pDnsCacheRRInfo->u4RRTTL;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResCacheRRElapsedTTL
 Input       :  The Indices
                DnsResCacheRRName
                DnsResCacheRRClass
                DnsResCacheRRType
                DnsResCacheRRIndex

                The Object 
                retValDnsResCacheRRElapsedTTL
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResCacheRRElapsedTTL (tSNMP_OCTET_STRING_TYPE * pDnsResCacheRRName,
                               INT4 i4DnsResCacheRRClass,
                               INT4 i4DnsResCacheRRType,
                               INT4 i4DnsResCacheRRIndex,
                               UINT4 *pu4RetValDnsResCacheRRElapsedTTL)
{
    tDnsCacheRRInfo    *pDnsCacheRRInfo = NULL;

    pDnsCacheRRInfo =
        DnsGetCacheRR (pDnsResCacheRRName->pu1_OctetList,
                       (UINT4) i4DnsResCacheRRClass,
                       (UINT4) i4DnsResCacheRRType,
                       (UINT4) i4DnsResCacheRRIndex);
    if (pDnsCacheRRInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Invalid Cache record indices\r\n"));
        return SNMP_FAILURE;
    }

    *pu4RetValDnsResCacheRRElapsedTTL =
        (UINT4) DnsCacheGetElappsedTTL (pDnsCacheRRInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResCacheRRSource
 Input       :  The Indices
                DnsResCacheRRName
                DnsResCacheRRClass
                DnsResCacheRRType
                DnsResCacheRRIndex

                The Object 
                retValDnsResCacheRRSource
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResCacheRRSource (tSNMP_OCTET_STRING_TYPE * pDnsResCacheRRName,
                           INT4 i4DnsResCacheRRClass, INT4 i4DnsResCacheRRType,
                           INT4 i4DnsResCacheRRIndex,
                           UINT4 *pu4RetValDnsResCacheRRSource)
{
    tDnsCacheRRInfo    *pDnsCacheRRInfo = NULL;

    pDnsCacheRRInfo =
        DnsGetCacheRR (pDnsResCacheRRName->pu1_OctetList,
                       (UINT4) i4DnsResCacheRRClass,
                       (UINT4) i4DnsResCacheRRType,
                       (UINT4) i4DnsResCacheRRIndex);
    if (pDnsCacheRRInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Invalid Cache record indices\r\n"));
        return SNMP_FAILURE;
    }

    MEMCPY (pu4RetValDnsResCacheRRSource, pDnsCacheRRInfo->SourceIP.au1Addr,
            IPVX_IPV4_ADDR_LEN);
    *pu4RetValDnsResCacheRRSource = OSIX_HTONL (*pu4RetValDnsResCacheRRSource);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResCacheRRData
 Input       :  The Indices
                DnsResCacheRRName
                DnsResCacheRRClass
                DnsResCacheRRType
                DnsResCacheRRIndex

                The Object 
                retValDnsResCacheRRData
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResCacheRRData (tSNMP_OCTET_STRING_TYPE * pDnsResCacheRRName,
                         INT4 i4DnsResCacheRRClass, INT4 i4DnsResCacheRRType,
                         INT4 i4DnsResCacheRRIndex,
                         tSNMP_OCTET_STRING_TYPE * pRetValDnsResCacheRRData)
{
    tDnsCacheRRInfo    *pDnsCacheRRInfo = NULL;

    pDnsCacheRRInfo =
        DnsGetCacheRR (pDnsResCacheRRName->pu1_OctetList,
                       (UINT4) i4DnsResCacheRRClass,
                       (UINT4) i4DnsResCacheRRType,
                       (UINT4) i4DnsResCacheRRIndex);
    if (pDnsCacheRRInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Invalid Cache record indices\r\n"));
        return SNMP_FAILURE;
    }

    MEMCPY (pRetValDnsResCacheRRData->pu1_OctetList, pDnsCacheRRInfo->au1RRData,
            IPVX_IPV4_ADDR_LEN);
    pRetValDnsResCacheRRData->i4_Length = IPVX_IPV4_ADDR_LEN;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResCacheRRStatus
 Input       :  The Indices
                DnsResCacheRRName
                DnsResCacheRRClass
                DnsResCacheRRType
                DnsResCacheRRIndex

                The Object 
                retValDnsResCacheRRStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResCacheRRStatus (tSNMP_OCTET_STRING_TYPE * pDnsResCacheRRName,
                           INT4 i4DnsResCacheRRClass, INT4 i4DnsResCacheRRType,
                           INT4 i4DnsResCacheRRIndex,
                           INT4 *pi4RetValDnsResCacheRRStatus)
{
    tDnsCacheRRInfo    *pDnsCacheRRInfo = NULL;

    pDnsCacheRRInfo =
        DnsGetCacheRR (pDnsResCacheRRName->pu1_OctetList,
                       (UINT4) i4DnsResCacheRRClass,
                       (UINT4) i4DnsResCacheRRType,
                       (UINT4) i4DnsResCacheRRIndex);
    if (pDnsCacheRRInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Invalid Cache record indices\r\n"));
        return SNMP_FAILURE;
    }

    *pi4RetValDnsResCacheRRStatus = (INT4) pDnsCacheRRInfo->u4RRStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResCacheRRPrettyName
 Input       :  The Indices
                DnsResCacheRRName
                DnsResCacheRRClass
                DnsResCacheRRType
                DnsResCacheRRIndex

                The Object 
                retValDnsResCacheRRPrettyName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResCacheRRPrettyName (tSNMP_OCTET_STRING_TYPE * pDnsResCacheRRName,
                               INT4 i4DnsResCacheRRClass,
                               INT4 i4DnsResCacheRRType,
                               INT4 i4DnsResCacheRRIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValDnsResCacheRRPrettyName)
{
    tDnsCacheRRInfo    *pDnsCacheRRInfo = NULL;

    pDnsCacheRRInfo =
        DnsGetCacheRR (pDnsResCacheRRName->pu1_OctetList,
                       (UINT4) i4DnsResCacheRRClass,
                       (UINT4) i4DnsResCacheRRType,
                       (UINT4) i4DnsResCacheRRIndex);
    if (pDnsCacheRRInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Invalid Cache record indices\r\n"));
        return SNMP_FAILURE;
    }

    STRCPY (pRetValDnsResCacheRRPrettyName->pu1_OctetList,
            pDnsCacheRRInfo->au1RRPrettyName);
    pRetValDnsResCacheRRPrettyName->i4_Length =
        (INT4) STRLEN (pDnsCacheRRInfo->au1RRPrettyName);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDnsResCacheRRStatus
 Input       :  The Indices
                DnsResCacheRRName
                DnsResCacheRRClass
                DnsResCacheRRType
                DnsResCacheRRIndex

                The Object 
                setValDnsResCacheRRStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDnsResCacheRRStatus (tSNMP_OCTET_STRING_TYPE * pDnsResCacheRRName,
                           INT4 i4DnsResCacheRRClass, INT4 i4DnsResCacheRRType,
                           INT4 i4DnsResCacheRRIndex,
                           INT4 i4SetValDnsResCacheRRStatus)
{

    tDnsCacheRRInfo    *pDnsCacheRRInfo = NULL;

    pDnsCacheRRInfo =
        DnsGetCacheRR (pDnsResCacheRRName->pu1_OctetList,
                       (UINT4) i4DnsResCacheRRClass,
                       (UINT4) i4DnsResCacheRRType,
                       (UINT4) i4DnsResCacheRRIndex);
    if (pDnsCacheRRInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Invalid Cache record indices\r\n"));
        return SNMP_FAILURE;
    }

    if (pDnsCacheRRInfo->u4RRStatus == (UINT4) i4SetValDnsResCacheRRStatus)
    {
        return SNMP_SUCCESS;
    }

    switch (i4SetValDnsResCacheRRStatus)
    {
        case ACTIVE:
            pDnsCacheRRInfo->u4RRStatus = ACTIVE;
            break;

        case DESTROY:
            DnsDeleteCacheData (pDnsCacheRRInfo, DNS_CACHE_RB_REM);
            break;
        default:
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DnsResCacheRRStatus
 Input       :  The Indices
                DnsResCacheRRName
                DnsResCacheRRClass
                DnsResCacheRRType
                DnsResCacheRRIndex

                The Object 
                testValDnsResCacheRRStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DnsResCacheRRStatus (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pDnsResCacheRRName,
                              INT4 i4DnsResCacheRRClass,
                              INT4 i4DnsResCacheRRType,
                              INT4 i4DnsResCacheRRIndex,
                              INT4 i4TestValDnsResCacheRRStatus)
{
    tDnsCacheRRInfo    *pDnsCacheRRInfo = NULL;
    if (gDnsGlobalInfo.u1SystemCtrlStatus != DNS_START)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Dns Module is shutdown\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (DNS_CLI_SHUTDOWN);
        return SNMP_FAILURE;
    }

    pDnsCacheRRInfo =
        DnsGetCacheRR (pDnsResCacheRRName->pu1_OctetList,
                       (UINT4) i4DnsResCacheRRClass,
                       (UINT4) i4DnsResCacheRRType,
                       (UINT4) i4DnsResCacheRRIndex);
    if (pDnsCacheRRInfo == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC, "Invalid Cache record indices\r\n"));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (DNS_CLI_NO_CACHE_ENTRY);
        return SNMP_FAILURE;
    }

    if ((i4TestValDnsResCacheRRStatus != ACTIVE)
        && (i4TestValDnsResCacheRRStatus != DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (DNS_CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DnsResCacheRRTable
 Input       :  The Indices
                DnsResCacheRRName
                DnsResCacheRRClass
                DnsResCacheRRType
                DnsResCacheRRIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DnsResCacheRRTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDnsResNCacheStatus
 Input       :  The Indices

                The Object 
                retValDnsResNCacheStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResNCacheStatus (INT4 *pi4RetValDnsResNCacheStatus)
{
    UNUSED_PARAM (pi4RetValDnsResNCacheStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResNCacheMaxTTL
 Input       :  The Indices

                The Object 
                retValDnsResNCacheMaxTTL
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResNCacheMaxTTL (UINT4 *pu4RetValDnsResNCacheMaxTTL)
{
    UNUSED_PARAM (pu4RetValDnsResNCacheMaxTTL);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResNCacheGoodNCaches
 Input       :  The Indices

                The Object 
                retValDnsResNCacheGoodNCaches
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResNCacheGoodNCaches (UINT4 *pu4RetValDnsResNCacheGoodNCaches)
{
    UNUSED_PARAM (pu4RetValDnsResNCacheGoodNCaches);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResNCacheBadNCaches
 Input       :  The Indices

                The Object 
                retValDnsResNCacheBadNCaches
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResNCacheBadNCaches (UINT4 *pu4RetValDnsResNCacheBadNCaches)
{
    UNUSED_PARAM (pu4RetValDnsResNCacheBadNCaches);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDnsResNCacheStatus
 Input       :  The Indices

                The Object 
                setValDnsResNCacheStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDnsResNCacheStatus (INT4 i4SetValDnsResNCacheStatus)
{
    UNUSED_PARAM (i4SetValDnsResNCacheStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDnsResNCacheMaxTTL
 Input       :  The Indices

                The Object 
                setValDnsResNCacheMaxTTL
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDnsResNCacheMaxTTL (UINT4 u4SetValDnsResNCacheMaxTTL)
{
    UNUSED_PARAM (u4SetValDnsResNCacheMaxTTL);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DnsResNCacheStatus
 Input       :  The Indices

                The Object 
                testValDnsResNCacheStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DnsResNCacheStatus (UINT4 *pu4ErrorCode,
                             INT4 i4TestValDnsResNCacheStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValDnsResNCacheStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DnsResNCacheMaxTTL
 Input       :  The Indices

                The Object 
                testValDnsResNCacheMaxTTL
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DnsResNCacheMaxTTL (UINT4 *pu4ErrorCode,
                             UINT4 u4TestValDnsResNCacheMaxTTL)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4TestValDnsResNCacheMaxTTL);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DnsResNCacheStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DnsResNCacheStatus (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2DnsResNCacheMaxTTL
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DnsResNCacheMaxTTL (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DnsResNCacheErrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDnsResNCacheErrTable
 Input       :  The Indices
                DnsResNCacheErrQName
                DnsResNCacheErrQClass
                DnsResNCacheErrQType
                DnsResNCacheErrIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDnsResNCacheErrTable (tSNMP_OCTET_STRING_TYPE *
                                              pDnsResNCacheErrQName,
                                              INT4 i4DnsResNCacheErrQClass,
                                              INT4 i4DnsResNCacheErrQType,
                                              INT4 i4DnsResNCacheErrIndex)
{
    UNUSED_PARAM (pDnsResNCacheErrQName);
    UNUSED_PARAM (i4DnsResNCacheErrQClass);
    UNUSED_PARAM (i4DnsResNCacheErrQType);
    UNUSED_PARAM (i4DnsResNCacheErrIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDnsResNCacheErrTable
 Input       :  The Indices
                DnsResNCacheErrQName
                DnsResNCacheErrQClass
                DnsResNCacheErrQType
                DnsResNCacheErrIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDnsResNCacheErrTable (tSNMP_OCTET_STRING_TYPE *
                                      pDnsResNCacheErrQName,
                                      INT4 *pi4DnsResNCacheErrQClass,
                                      INT4 *pi4DnsResNCacheErrQType,
                                      INT4 *pi4DnsResNCacheErrIndex)
{
    UNUSED_PARAM (pDnsResNCacheErrQName);
    UNUSED_PARAM (pi4DnsResNCacheErrQClass);
    UNUSED_PARAM (pi4DnsResNCacheErrQType);
    UNUSED_PARAM (pi4DnsResNCacheErrIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDnsResNCacheErrTable
 Input       :  The Indices
                DnsResNCacheErrQName
                nextDnsResNCacheErrQName
                DnsResNCacheErrQClass
                nextDnsResNCacheErrQClass
                DnsResNCacheErrQType
                nextDnsResNCacheErrQType
                DnsResNCacheErrIndex
                nextDnsResNCacheErrIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDnsResNCacheErrTable (tSNMP_OCTET_STRING_TYPE *
                                     pDnsResNCacheErrQName,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextDnsResNCacheErrQName,
                                     INT4 i4DnsResNCacheErrQClass,
                                     INT4 *pi4NextDnsResNCacheErrQClass,
                                     INT4 i4DnsResNCacheErrQType,
                                     INT4 *pi4NextDnsResNCacheErrQType,
                                     INT4 i4DnsResNCacheErrIndex,
                                     INT4 *pi4NextDnsResNCacheErrIndex)
{
    UNUSED_PARAM (pDnsResNCacheErrQName);
    UNUSED_PARAM (pNextDnsResNCacheErrQName);
    UNUSED_PARAM (i4DnsResNCacheErrQClass);
    UNUSED_PARAM (pi4NextDnsResNCacheErrQClass);
    UNUSED_PARAM (i4DnsResNCacheErrQType);
    UNUSED_PARAM (pi4NextDnsResNCacheErrQType);
    UNUSED_PARAM (i4DnsResNCacheErrIndex);
    UNUSED_PARAM (pi4NextDnsResNCacheErrIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDnsResNCacheErrTTL
 Input       :  The Indices
                DnsResNCacheErrQName
                DnsResNCacheErrQClass
                DnsResNCacheErrQType
                DnsResNCacheErrIndex

                The Object 
                retValDnsResNCacheErrTTL
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResNCacheErrTTL (tSNMP_OCTET_STRING_TYPE * pDnsResNCacheErrQName,
                          INT4 i4DnsResNCacheErrQClass,
                          INT4 i4DnsResNCacheErrQType,
                          INT4 i4DnsResNCacheErrIndex,
                          UINT4 *pu4RetValDnsResNCacheErrTTL)
{
    UNUSED_PARAM (pDnsResNCacheErrQName);
    UNUSED_PARAM (i4DnsResNCacheErrQClass);
    UNUSED_PARAM (i4DnsResNCacheErrQType);
    UNUSED_PARAM (i4DnsResNCacheErrIndex);
    UNUSED_PARAM (pu4RetValDnsResNCacheErrTTL);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResNCacheErrElapsedTTL
 Input       :  The Indices
                DnsResNCacheErrQName
                DnsResNCacheErrQClass
                DnsResNCacheErrQType
                DnsResNCacheErrIndex

                The Object 
                retValDnsResNCacheErrElapsedTTL
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResNCacheErrElapsedTTL (tSNMP_OCTET_STRING_TYPE *
                                 pDnsResNCacheErrQName,
                                 INT4 i4DnsResNCacheErrQClass,
                                 INT4 i4DnsResNCacheErrQType,
                                 INT4 i4DnsResNCacheErrIndex,
                                 UINT4 *pu4RetValDnsResNCacheErrElapsedTTL)
{
    UNUSED_PARAM (pDnsResNCacheErrQName);
    UNUSED_PARAM (i4DnsResNCacheErrQClass);
    UNUSED_PARAM (i4DnsResNCacheErrQType);
    UNUSED_PARAM (i4DnsResNCacheErrIndex);
    UNUSED_PARAM (pu4RetValDnsResNCacheErrElapsedTTL);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResNCacheErrSource
 Input       :  The Indices
                DnsResNCacheErrQName
                DnsResNCacheErrQClass
                DnsResNCacheErrQType
                DnsResNCacheErrIndex

                The Object 
                retValDnsResNCacheErrSource
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResNCacheErrSource (tSNMP_OCTET_STRING_TYPE * pDnsResNCacheErrQName,
                             INT4 i4DnsResNCacheErrQClass,
                             INT4 i4DnsResNCacheErrQType,
                             INT4 i4DnsResNCacheErrIndex,
                             UINT4 *pu4RetValDnsResNCacheErrSource)
{
    UNUSED_PARAM (pDnsResNCacheErrQName);
    UNUSED_PARAM (i4DnsResNCacheErrQClass);
    UNUSED_PARAM (i4DnsResNCacheErrQType);
    UNUSED_PARAM (i4DnsResNCacheErrIndex);
    UNUSED_PARAM (pu4RetValDnsResNCacheErrSource);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResNCacheErrCode
 Input       :  The Indices
                DnsResNCacheErrQName
                DnsResNCacheErrQClass
                DnsResNCacheErrQType
                DnsResNCacheErrIndex

                The Object 
                retValDnsResNCacheErrCode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResNCacheErrCode (tSNMP_OCTET_STRING_TYPE * pDnsResNCacheErrQName,
                           INT4 i4DnsResNCacheErrQClass,
                           INT4 i4DnsResNCacheErrQType,
                           INT4 i4DnsResNCacheErrIndex,
                           INT4 *pi4RetValDnsResNCacheErrCode)
{
    UNUSED_PARAM (pDnsResNCacheErrQName);
    UNUSED_PARAM (i4DnsResNCacheErrQClass);
    UNUSED_PARAM (i4DnsResNCacheErrQType);
    UNUSED_PARAM (i4DnsResNCacheErrIndex);
    UNUSED_PARAM (pi4RetValDnsResNCacheErrCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResNCacheErrStatus
 Input       :  The Indices
                DnsResNCacheErrQName
                DnsResNCacheErrQClass
                DnsResNCacheErrQType
                DnsResNCacheErrIndex

                The Object 
                retValDnsResNCacheErrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResNCacheErrStatus (tSNMP_OCTET_STRING_TYPE * pDnsResNCacheErrQName,
                             INT4 i4DnsResNCacheErrQClass,
                             INT4 i4DnsResNCacheErrQType,
                             INT4 i4DnsResNCacheErrIndex,
                             INT4 *pi4RetValDnsResNCacheErrStatus)
{
    UNUSED_PARAM (pDnsResNCacheErrQName);
    UNUSED_PARAM (i4DnsResNCacheErrQClass);
    UNUSED_PARAM (i4DnsResNCacheErrQType);
    UNUSED_PARAM (i4DnsResNCacheErrIndex);
    UNUSED_PARAM (pi4RetValDnsResNCacheErrStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResNCacheErrPrettyName
 Input       :  The Indices
                DnsResNCacheErrQName
                DnsResNCacheErrQClass
                DnsResNCacheErrQType
                DnsResNCacheErrIndex

                The Object 
                retValDnsResNCacheErrPrettyName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResNCacheErrPrettyName (tSNMP_OCTET_STRING_TYPE *
                                 pDnsResNCacheErrQName,
                                 INT4 i4DnsResNCacheErrQClass,
                                 INT4 i4DnsResNCacheErrQType,
                                 INT4 i4DnsResNCacheErrIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValDnsResNCacheErrPrettyName)
{
    UNUSED_PARAM (pDnsResNCacheErrQName);
    UNUSED_PARAM (i4DnsResNCacheErrQClass);
    UNUSED_PARAM (i4DnsResNCacheErrQType);
    UNUSED_PARAM (i4DnsResNCacheErrIndex);
    UNUSED_PARAM (pRetValDnsResNCacheErrPrettyName);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDnsResNCacheErrStatus
 Input       :  The Indices
                DnsResNCacheErrQName
                DnsResNCacheErrQClass
                DnsResNCacheErrQType
                DnsResNCacheErrIndex

                The Object 
                setValDnsResNCacheErrStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDnsResNCacheErrStatus (tSNMP_OCTET_STRING_TYPE * pDnsResNCacheErrQName,
                             INT4 i4DnsResNCacheErrQClass,
                             INT4 i4DnsResNCacheErrQType,
                             INT4 i4DnsResNCacheErrIndex,
                             INT4 i4SetValDnsResNCacheErrStatus)
{
    UNUSED_PARAM (pDnsResNCacheErrQName);
    UNUSED_PARAM (i4DnsResNCacheErrQClass);
    UNUSED_PARAM (i4DnsResNCacheErrQType);
    UNUSED_PARAM (i4DnsResNCacheErrIndex);
    UNUSED_PARAM (i4SetValDnsResNCacheErrStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DnsResNCacheErrStatus
 Input       :  The Indices
                DnsResNCacheErrQName
                DnsResNCacheErrQClass
                DnsResNCacheErrQType
                DnsResNCacheErrIndex

                The Object 
                testValDnsResNCacheErrStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DnsResNCacheErrStatus (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pDnsResNCacheErrQName,
                                INT4 i4DnsResNCacheErrQClass,
                                INT4 i4DnsResNCacheErrQType,
                                INT4 i4DnsResNCacheErrIndex,
                                INT4 i4TestValDnsResNCacheErrStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pDnsResNCacheErrQName);
    UNUSED_PARAM (i4DnsResNCacheErrQClass);
    UNUSED_PARAM (i4DnsResNCacheErrQType);
    UNUSED_PARAM (i4DnsResNCacheErrIndex);
    UNUSED_PARAM (i4TestValDnsResNCacheErrStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DnsResNCacheErrTable
 Input       :  The Indices
                DnsResNCacheErrQName
                DnsResNCacheErrQClass
                DnsResNCacheErrQType
                DnsResNCacheErrIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DnsResNCacheErrTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDnsResOptCounterReferals
 Input       :  The Indices

                The Object 
                retValDnsResOptCounterReferals
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResOptCounterReferals (UINT4 *pu4RetValDnsResOptCounterReferals)
{
    UNUSED_PARAM (pu4RetValDnsResOptCounterReferals);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResOptCounterRetrans
 Input       :  The Indices

                The Object 
                retValDnsResOptCounterRetrans
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResOptCounterRetrans (UINT4 *pu4RetValDnsResOptCounterRetrans)
{
    UNUSED_PARAM (pu4RetValDnsResOptCounterRetrans);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResOptCounterNoResponses
 Input       :  The Indices

                The Object 
                retValDnsResOptCounterNoResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResOptCounterNoResponses (UINT4 *pu4RetValDnsResOptCounterNoResponses)
{
    UNUSED_PARAM (pu4RetValDnsResOptCounterNoResponses);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResOptCounterRootRetrans
 Input       :  The Indices

                The Object 
                retValDnsResOptCounterRootRetrans
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResOptCounterRootRetrans (UINT4 *pu4RetValDnsResOptCounterRootRetrans)
{
    UNUSED_PARAM (pu4RetValDnsResOptCounterRootRetrans);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResOptCounterInternals
 Input       :  The Indices

                The Object 
                retValDnsResOptCounterInternals
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResOptCounterInternals (UINT4 *pu4RetValDnsResOptCounterInternals)
{
    UNUSED_PARAM (pu4RetValDnsResOptCounterInternals);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDnsResOptCounterInternalTimeOuts
 Input       :  The Indices

                The Object 
                retValDnsResOptCounterInternalTimeOuts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDnsResOptCounterInternalTimeOuts (UINT4
                                        *pu4RetValDnsResOptCounterInternalTimeOuts)
{
    UNUSED_PARAM (pu4RetValDnsResOptCounterInternalTimeOuts);
    return SNMP_SUCCESS;
}
