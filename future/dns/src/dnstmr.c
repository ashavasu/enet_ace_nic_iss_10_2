/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: dnstmr.c,v 1.2 2015/01/12 10:24:37 siva Exp $
 *
 * Description: This file contains the routines for Timer functionality
 *              for DNS module.
 * ************************************************************************/
#include "dnsinc.h"

PRIVATE VOID DnsTmrInitTmrDesc PROTO ((VOID));
PRIVATE VOID DnsCacheTmrExpired PROTO ((VOID *));
PRIVATE VOID DnsQueryTmrExpired PROTO ((VOID *));

/************************************************************************/
/*  Function Name   : DnsTmrInit                                        */
/*                                                                      */
/*  Description     : This function creates the timer pool & List for   */
/*                    Dns Task.                                         */
/*                                                                      */
/*  Input(s)        : None                                              */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS or OSIX_FAILURE                      */
/************************************************************************/

PUBLIC INT4
DnsTmrInit ()
{
    /* Timer Initialization */
    if (TmrCreateTimerList
        ((UINT1 *) DNS_TASK_NAME, DNS_TMR_EXP_EVT, NULL,
         &(DNS_TMRLIST_ID)) == TMR_FAILURE)
    {
        DNS_TRACE ((DNS_FAILURE_TRC | DNS_INITSHUT_TRC,
                    " Creation of DNS Timer List Fails\r\n"));

        return OSIX_FAILURE;
    }

    DnsTmrInitTmrDesc ();

    return OSIX_SUCCESS;

}

/************************************************************************/
/*  Function Name   : DnsTmrInitTmrDesc                              */
/*                                                                      */
/*  Description     :  This function creates a timer list for all       */
/*                     the timers DNS Task.                             */
/*                                                                      */
/*  Input(s)        : None                                              */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : None                                              */
/************************************************************************/

PRIVATE VOID
DnsTmrInitTmrDesc (VOID)
{
    gDnsGlobalInfo.aTmrDesc[DNS_CACHE_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tDnsCacheRRInfo, CacheExpTmrNode);

    gDnsGlobalInfo.aTmrDesc[DNS_CACHE_TMR].TmrExpFn = DnsCacheTmrExpired;

    gDnsGlobalInfo.aTmrDesc[DNS_QRY_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tDnsQueryInfo, QryNodeTmrNode);
    gDnsGlobalInfo.aTmrDesc[DNS_QRY_TMR].TmrExpFn = DnsQueryTmrExpired;
    return;
}

/************************************************************************/
/*  Function Name   : DnsTmrStart                                  */
/*                                                                      */
/*  Description     : This function is called to start timer for DNS    */
/*                           Task.                                      */
/*                                                                      */
/*  Input(s)        : pDnsTmr pointer to tTmrBlk structure              */
/*                    u1TmrId - Type of timer ot be started.            */
/*                  : u4Duration  - Duration for which the timer needs  */
/*                                  to be started in units of sec.     */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS or OSIX_FAILURE                      */
/************************************************************************/
PUBLIC UINT4
DnsTmrStart (tTmrBlk * pDnsTmr, UINT1 u1TmrId, UINT4 u4Duration)
{
    UINT4               i4Ret = TMR_SUCCESS;
    if (TmrStart (DNS_TMRLIST_ID, pDnsTmr, u1TmrId, u4Duration,
                  DNS_ZERO) == TMR_FAILURE)
    {
        i4Ret = TMR_FAILURE;
        DNS_TRACE ((DNS_INITSHUT_TRC | DNS_FAILURE_TRC,
                    " Creation of DNS Timer List Fails \r\n"));

    }
    return i4Ret;
}

/****************************************************************************
*                                                                           *
* Function     : DnsTmrRestart                                          *
*                                                                           *
* Description  :  ReStarts Dns Timer                                       *
*                                                                           *
* Input        : pDnsTmr - pointer to tTmrBlk structure                    *
*                u1TmrId  - DNS timer ID                                   *
*                u4Duration   - seconds                                         *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
DnsTmrRestart (tTmrBlk * pDnsTmr, UINT1 u1TimerId, UINT4 u4Duration)
{
    TmrStop (DNS_TMRLIST_ID, pDnsTmr);
    DnsTmrStart (pDnsTmr, u1TimerId, u4Duration);

    return;
}

/************************************************************************/
/*  Function Name   : DnsTmrExpHandler                               */
/*                                                                      */
/*  Description     : This function is called from DNS Main when     */
/*                    Task receives the timer expiry event.             */
/*                                                                      */
/*  Input(s)        : None                                              */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS or OSIX_FAILURE                      */
/************************************************************************/
PUBLIC VOID
DnsTmrExpHandler ()
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;
    INT2                i2Offset = 0;

    while ((pExpiredTimers = TmrGetNextExpiredTimer (DNS_TMRLIST_ID)) != NULL)
    {
        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;
        if (u1TimerId < DNS_MAX_TMR_TYPES)
        {
            i2Offset = gDnsGlobalInfo.aTmrDesc[u1TimerId].i2Offset;

            /* Call the registered expired handler function */
            (*(gDnsGlobalInfo.aTmrDesc[u1TimerId].TmrExpFn))
                ((UINT1 *) pExpiredTimers - i2Offset);
        }
    }
    return;
}

/************************************************************************/
/*  Function Name   : DnsTmrStopTimer                                   */
/*                                                                      */
/*  Description     : This function is called to stop timer for Dns  */
/*                    Task.                                      */
/*                                                                      */
/*  Input(s)        : pDnsTmr pointer to tTmrBlk structure             */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS or OSIX_FAILURE                      */
/************************************************************************/
PUBLIC VOID
DnsTmrStopTimer (tTmrBlk * pDnsTmr)
{
    TmrStop (DNS_TMRLIST_ID, pDnsTmr);
    return;
}

/******************************************************************************
 * Function Name      : DnsQueryTmrExpired
 *
 * Description        : This routine is called when Query timer running
 *                      on the Pending Query got expired.
 *
 * Input(s)           : pDnsQueryInfo Pointer to Query Strcture
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/

PRIVATE VOID
DnsQueryTmrExpired (VOID *pArg)
{
    tDnsQueryInfo      *pDnsQueryInfo = NULL;
    pDnsQueryInfo = (tDnsQueryInfo *) pArg;

    if (((pDnsQueryInfo->u4CurRetryCount) <=
         gDnsResolverParams.u4QueryRetryCount)
        && (TMO_SLL_Count (&gDnsGlobalInfo.NameServerList)) > 0)
    {
        DnsTmrRestart (&pDnsQueryInfo->QryNodeTmrNode, DNS_QRY_TMR,
                       gDnsResolverParams.u4QueryTimeout);
        DnsReqSendQuery (pDnsQueryInfo);
    }
    else
    {
        DnsDeleteQueryData (pDnsQueryInfo);
        DNS_TRACE ((DNS_INITSHUT_TRC | DNS_FAILURE_TRC,
                    "DnsQueryTmrExpired: Query Timer Expired \r\n"));

    }
    return;
}

/******************************************************************************
 * Function Name      : DnsCacheTmrExpired
 *
 * Description        : This routine is called when Cache timer running
 *                      on the Cached RR got expired.
 *
 * Input(s)           : pDnsCacheRRInfo Pointer to Cache Strcture
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/

PRIVATE VOID
DnsCacheTmrExpired (VOID *pArg)
{
    tDnsCacheRRInfo    *pDnsCacheRRInfo = NULL;
    pDnsCacheRRInfo = (tDnsCacheRRInfo *) pArg;

    if (pDnsCacheRRInfo->u4HitCount == DNS_ZERO)
    {
        DnsDeleteCacheData (pDnsCacheRRInfo, DNS_CACHE_RB_REM);
        DNS_TRACE ((DNS_INITSHUT_TRC | DNS_FAILURE_TRC,
                    "DnsCacheTmrExpired: Cache Timer Expired \r\n"));

    }
    else
    {
        pDnsCacheRRInfo->u4HitCount = DNS_ZERO;
        /** Resend Query **/
        DnsQueMsg (pDnsCacheRRInfo->au1RRName, pDnsCacheRRInfo->u4RRType);
        DnsDeleteCacheData (pDnsCacheRRInfo, DNS_CACHE_RB_REM);
    }
    return;
}

/************************************************************************/
/*  Function Name   : DnsTmrDeInit                                   */
/*                                                                      */
/*  Description     : This function deletes the timer list created for  */
/*                    DNS Task.                               */
/*                                                                      */
/*  Input(s)        : None                                              */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : None                                              */
/************************************************************************/
PUBLIC INT4
DnsTmrDeInit ()
{
    INT4                i4RetVal = OSIX_SUCCESS;
    if (DNS_TMRLIST_ID != 0)
    {
        /* Deleting the Timer List */
        if (TmrDeleteTimerList (DNS_TMRLIST_ID) == TMR_FAILURE)
        {
            DNS_TRACE ((DNS_FAILURE_TRC | DNS_INITSHUT_TRC,
                        " Deletion of DNS Timer List Fails\r\n"));

            i4RetVal = OSIX_FAILURE;
        }
        MEMSET (gDnsGlobalInfo.aTmrDesc, 0,
                (sizeof (tTmrDesc) * DNS_MAX_TMR_TYPES));

        DNS_TMRLIST_ID = 0;
        DNS_TRACE ((DNS_FAILURE_TRC | DNS_INITSHUT_TRC,
                    " Creation of DNS Task Timers DeInitialized\r\n"));

    }
    return i4RetVal;
}
