/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dnscache.c,v 1.11 2015/07/17 09:57:40 siva Exp $
 *
 * Description: This file contains functions related with DNS Cache
 *              
 *
 *******************************************************************/
#include "dnsinc.h"

/*****************************************************************************/
/*     FUNCTION NAME    : DnsResolverCacheInit                               */
/*                                                                           */
/*     DESCRIPTION      : This function is used to Initialise the DNS Cache  */
/*                                                                           */
/*     INPUT            : None.                                              */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : DNS_SUCCESS / DNS_FAILURE                          */
/*****************************************************************************/
PUBLIC INT4
DnsResolverCacheInit (VOID)
{
    UINT1               u1Offset = 0;

    DNS_TRACE ((DNS_INITSHUT_TRC, " Intialising DNS Cache \r\n"));
    MEMSET (&gDnsCacheInfo, 0, sizeof (tDnsCacheInfo));

    gDnsCacheInfo.u4Status = DNS_CACHE_ENABLE;
    gDnsCacheInfo.u4MaxTTL = DNS_CACHE_DEFAULT_TTL;
    gDnsCacheInfo.u4TotalEntries = DNS_ZERO;

    u1Offset = FSAP_OFFSETOF (tDnsCacheRRInfo, CacheNode);
    if ((gDnsCacheInfo.CacheList =
         RBTreeCreateEmbedded (u1Offset, DnsCacheListCmp)) == NULL)
    {
        DNS_TRACE ((DNS_FAILURE_TRC | DNS_INITSHUT_TRC,
                    " Creation of DNS Cache RB Tree Fails\r\n"));
        return DNS_FAILURE;
    }
    DNS_TRACE ((DNS_INITSHUT_TRC, " DNS Cache successfully initialized \r\n"));

    return DNS_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsAddCacheData                                    */
/*                                                                           */
/*     DESCRIPTION      : This function is used add a Record to DNS Cache    */
/*                                                                           */
/*     INPUT            : pu1QueryName - DNS Query                           */
/*                        u4QueryClass - DNS Query Class                     */
/*                        u4QueryType  - DNS Query Type                      */
/*                        u4QueryTTL   - DNS Query TTL                       */
/*                        QueryNameServerIP - Name Server IP                 */
/*                        QueryResolvedIP - Resolved IP                      */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : DNS_SUCCESS / DNS_FAILURE                          */
/*****************************************************************************/
PUBLIC INT4
DnsAddCacheData (UINT1 *pu1QueryName, UINT4 u4QueryClass, UINT4 u4QueryType,
                 UINT4 u4QueryTTL, tIPvXAddr QueryNameServerIP,
                 tIPvXAddr QueryResolvedIP)
{
    tDnsCacheRRInfo    *pDnsCacheRRInfo = NULL;

    if (gDnsCacheInfo.u4Status != DNS_CACHE_ENABLE)
    {
        DNS_TRACE ((DNS_CACHE_TRC | DNS_FAILURE_TRC,
                    " DNS Cache Function is not enabled \r\n"));
        return DNS_FAILURE;
    }

    if ((u4QueryClass != DNS_INTERNET_CLASS)
        || (((u4QueryType != DNS_HOST_ADDRESS))
            && (u4QueryType != DNS_AAAA_RECORD)))
    {
        DNS_TRACE ((DNS_CACHE_TRC | DNS_FAILURE_TRC,
                    " Invalid Query Class and Type \r\n"));
        return DNS_FAILURE;
    }

    /* If the RR TTL is greater than Max cache TTL, we make RR TTL as Max cache TTL */
    if (u4QueryTTL > gDnsCacheInfo.u4MaxTTL)
    {
        u4QueryTTL = gDnsCacheInfo.u4MaxTTL;
    }

    pDnsCacheRRInfo = DnsCacheLookup (pu1QueryName, u4QueryClass, u4QueryType);
    if (pDnsCacheRRInfo == NULL)
    {
        if (gDnsCacheInfo.u4TotalEntries == MAX_DNS_CACHE_ENTRIES)
        {
            return DNS_CACHE_FULL;
        }

        pDnsCacheRRInfo = MemAllocMemBlk (gDnsGlobalInfo.CacheDataPoolId);
        if (pDnsCacheRRInfo == NULL)
        {
            DNS_TRACE ((DNS_CACHE_TRC | DNS_FAILURE_TRC,
                        " Memory allocation of cache record fails \r\n"));
            return DNS_FAILURE;
        }

        MEMSET (pDnsCacheRRInfo, 0, sizeof (tDnsCacheRRInfo));

        MEMCPY (pDnsCacheRRInfo->au1RRName, pu1QueryName,
                STRLEN (pu1QueryName));
        pDnsCacheRRInfo->u4RRClass = u4QueryClass;
        pDnsCacheRRInfo->u4RRType = u4QueryType;
        pDnsCacheRRInfo->u4RRTTL = u4QueryTTL;
        pDnsCacheRRInfo->u4RRIndex = 1;

        if (RBTreeAdd (gDnsCacheInfo.CacheList, (tRBElem *) pDnsCacheRRInfo) ==
            RB_FAILURE)
        {
            DNS_TRACE ((DNS_CACHE_TRC | DNS_FAILURE_TRC,
                        " Addition of Record to Cache fails \r\n"));
            return DNS_FAILURE;
        }

        gDnsCacheInfo.u4TotalEntries++;
    }
    else
    {
        DNS_TRACE ((DNS_CACHE_TRC,
                    " Already the Entry exists. Hence stopping the Cache timer \r\n"));
        DnsTmrStopTimer (&pDnsCacheRRInfo->CacheExpTmrNode);
    }

    pDnsCacheRRInfo->u4RRTTL = u4QueryTTL;
    pDnsCacheRRInfo->u4RRStatus = ACTIVE;

    DNS_TRACE ((DNS_CACHE_TRC, " Starting Cache Record timer for %d Seconds ",
                u4QueryTTL));
    if ((DnsTmrStart
         (&(pDnsCacheRRInfo->CacheExpTmrNode), DNS_CACHE_TMR,
          pDnsCacheRRInfo->u4RRTTL)) == TMR_FAILURE)
    {
        return DNS_FAILURE;
    }

    MEMCPY (&pDnsCacheRRInfo->SourceIP, &QueryNameServerIP, sizeof (tIPvXAddr));
    MEMCPY (pDnsCacheRRInfo->au1RRData, QueryResolvedIP.au1Addr,
            QueryResolvedIP.u1AddrLen);

    DNS_TRACE ((DNS_CACHE_TRC, " Successfully Added DNS Cache record \r\n"));
    return DNS_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsCacheLookup                                     */
/*                                                                           */
/*     DESCRIPTION      : This function is used Locate a Record in DNS Cache */
/*                                                                           */
/*     INPUT            : pu1QueryName - DNS Query                           */
/*                        u4QueryClass - DNS Query Class                     */
/*                        u4QueryType  - DNS Query Type                      */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : Cache Record / NULL                                */
/*****************************************************************************/
PUBLIC tDnsCacheRRInfo *
DnsCacheLookup (UINT1 *pu1QueryName, UINT4 u4QueryClass, UINT4 u4QueryType)
{
    tDnsCacheRRInfo    *pDnsCacheRRInfo = NULL;
    tDnsCacheRRInfo     DnsCacheRRInfo;

    if (gDnsCacheInfo.u4Status != DNS_CACHE_ENABLE)
    {
        DNS_TRACE ((DNS_CACHE_TRC | DNS_FAILURE_TRC,
                    " DNS Cache Function is not enabled \r\n"));
        return NULL;
    }

    MEMSET (&DnsCacheRRInfo, 0, sizeof (tDnsCacheRRInfo));
    STRNCPY (DnsCacheRRInfo.au1RRName, pu1QueryName,
             DNS_MAX_QUERY_LEN);

    DnsCacheRRInfo.au1RRName[STRLEN (pu1QueryName)] = '\0';
    DnsCacheRRInfo.u4RRClass = u4QueryClass;
    DnsCacheRRInfo.u4RRType = u4QueryType;

    pDnsCacheRRInfo =
        RBTreeGetNext (gDnsCacheInfo.CacheList, (tRBElem *) & DnsCacheRRInfo,
                       NULL);

    if (pDnsCacheRRInfo == NULL)
    {
	    if(u4QueryType ==DNS_AAAA_RECORD)
	    {
		    DNS_TRACE ((DNS_CACHE_TRC, "\r\n No Cache Record Found for IPV6 query request \r\n"));
	    }
	    else
	    {
		    DNS_TRACE ((DNS_CACHE_TRC, "\r\n No Cache Record Found for IPV4 query request \r\n"));
	    }

	    return NULL;
    }

    if ((MEMCMP
         (pDnsCacheRRInfo->au1RRName, pu1QueryName,
          STRLEN (pDnsCacheRRInfo->au1RRName)) == 0)
        && (pDnsCacheRRInfo->u4RRClass == u4QueryClass)
        && (pDnsCacheRRInfo->u4RRType == u4QueryType))
    {
        DNS_TRACE ((DNS_CACHE_TRC, "\r\n Cache Record Found \r\n"));
        return pDnsCacheRRInfo;
    }
    if(u4QueryType ==DNS_AAAA_RECORD)
    {
	    DNS_TRACE ((DNS_CACHE_TRC, "\r\n No Cache Record Found for IPV6 query request \r\n"));
    }
    else
    {
	    DNS_TRACE ((DNS_CACHE_TRC, "\r\n No Cache Record Found for IPV4 query request \r\n"));
    }

    return NULL;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsCacheClear                                      */
/*                                                                           */
/*     DESCRIPTION      : This function Clears all DNS Cache data            */
/*                                                                           */
/*     INPUT            : None.                                              */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : None.                                              */
/*****************************************************************************/
PUBLIC VOID
DnsCacheClear (VOID)
{
    RBTreeDrain (gDnsCacheInfo.CacheList, CacheFreeNode, DNS_ZERO);
}

/*****************************************************************************/
/*     FUNCTION NAME    : CacheFreeNode                                      */
/*                                                                           */
/*     DESCRIPTION      : This function frees a DNS Cache Record             */
/*                                                                           */
/*     INPUT            : None.                                              */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : DNS_SUCCESS                                        */
/*****************************************************************************/
PUBLIC INT4
CacheFreeNode (tRBElem * pRBElem, UINT4 u4Arg)
{
    tDnsCacheRRInfo    *pDnsCacheRRInfo = (tDnsCacheRRInfo *) pRBElem;

    UNUSED_PARAM (u4Arg);
    DnsDeleteCacheData (pDnsCacheRRInfo, DNS_CACHE_NO_RB_REM);
    return DNS_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsDeleteCacheData                                 */
/*                                                                           */
/*     DESCRIPTION      : This function Deletes a DNS Cache Record           */
/*                                                                           */
/*     INPUT            : pDnsCacheRRInfo - Cache Record                     */
/*                        u4Flag - DNS_CACHE_RB_REM / DNS_CACHE_NO_RB_REM    */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : None.                                              */
/*****************************************************************************/
PUBLIC VOID
DnsDeleteCacheData (tDnsCacheRRInfo * pDnsCacheRRInfo, UINT4 u4Flag)
{
    if (pDnsCacheRRInfo->u4RRIndex != 1)
    {
        IndexManagerFreeIndex (gDnsIndexMgrListInfo.CacheIndexList,
                               pDnsCacheRRInfo->u4RRIndex);
    }

    DnsTmrStopTimer (&pDnsCacheRRInfo->CacheExpTmrNode);
    if (u4Flag & DNS_CACHE_RB_REM)
    {
        RBTreeRem (gDnsCacheInfo.CacheList, (tRBElem *) pDnsCacheRRInfo);
    }
    MemReleaseMemBlock (gDnsGlobalInfo.CacheDataPoolId,
                        (UINT1 *) pDnsCacheRRInfo);
    gDnsCacheInfo.u4TotalEntries--;
    DNS_TRACE ((DNS_CACHE_TRC, "Successfully Deleted the cache Record \r\n"));
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsGetCacheRR                                      */
/*                                                                           */
/*     DESCRIPTION      : This function retreives a Cache Record             */
/*                                                                           */
/*     INPUT            : pu1RRName - RR Query Name.                         */
/*                        u4RRClass - RR Class.                              */
/*                        u4RRType - RR Type                                 */
/*                        u4RRIndex - RR Index                               */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : DNS Cache Resource Record                          */
/*****************************************************************************/
PUBLIC tDnsCacheRRInfo *
DnsGetCacheRR (UINT1 *pu1RRName, UINT4 u4RRClass, UINT4 u4RRType,
               UINT4 u4RRIndex)
{
    tDnsCacheRRInfo    *pDnsCacheRRInfo = NULL;
    tDnsCacheRRInfo     DnsCacheRRInfo;

    MEMSET (&DnsCacheRRInfo, 0, sizeof (DnsCacheRRInfo));

    MEMCPY (DnsCacheRRInfo.au1RRName, pu1RRName, STRLEN (pu1RRName));
    DnsCacheRRInfo.u4RRClass = u4RRClass;
    DnsCacheRRInfo.u4RRType = u4RRType;
    DnsCacheRRInfo.u4RRIndex = u4RRIndex;

    pDnsCacheRRInfo = RBTreeGet (gDnsCacheInfo.CacheList, &DnsCacheRRInfo);

    return pDnsCacheRRInfo;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DnsCacheGetElappsedTTL                                */
/*                                                                           */
/*     DESCRIPTION      : This function returns the remaining TTL            */
/*                        for a cache record.                                */
/*                                                                           */
/*     INPUT            : pDnsCacheRRInfo - Cache Record Info                */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : Remaining TTL for a cache record                   */
/*****************************************************************************/
PUBLIC INT4
DnsCacheGetElappsedTTL (tDnsCacheRRInfo * pDnsCacheRRInfo)
{
    UINT4               u4ElappedTTL = 0;

    if (TmrGetRemainingTime (gDnsGlobalInfo.TimerListId,
                             (tTmrAppTimer *) &
                             (pDnsCacheRRInfo->CacheExpTmrNode),
                             &u4ElappedTTL) == TMR_FAILURE)
    {
        return DNS_ZERO;
    }
    u4ElappedTTL = u4ElappedTTL / SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    return ((INT4) u4ElappedTTL);
}
