########################################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved                 #
# ------------------------------------------                           #
# $Id: make.h,v 1.1.1.1 2011/01/21 08:37:31 siva Exp $                      #  
#    DESCRIPTION            : Specifies the options and modules to be  #
#                             including for building the DNS module    #
#                                                                      #
########################################################################

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


TOTAL_OPNS =  $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

DNS_BASE_DIR = ${BASE_DIR}/dns
DNS_SRC_DIR  = ${DNS_BASE_DIR}/src
DNS_INC_DIR  = ${DNS_BASE_DIR}/inc
DNS_OBJ_DIR  = ${DNS_BASE_DIR}/obj


############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${DNS_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
