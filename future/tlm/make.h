#!/bin/csh
# (C) 2011 Aricent Technologies Holdings Ltd.
# $Id: make.h,v 1.2 2011/12/09 06:16:03 siva Exp $
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent                                       |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 19 august 2011                                |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

TOTAL_OPNS =  $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

TLM_BASE_DIR = ${BASE_DIR}/tlm
TLM_SRC_DIR  = ${TLM_BASE_DIR}/src
TLM_INC_DIR  = ${TLM_BASE_DIR}/inc
TLM_OBJ_DIR  = ${TLM_BASE_DIR}/obj
CLI_INC_DIR  = ${BASE_DIR}/inc/cli
############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${TLM_INC_DIR} \
                    -I$(MPLS_INC_DIR) \
                    -I$(TE_INC_DIR) \
                    -I$(CLI_INC_DIR)
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################

