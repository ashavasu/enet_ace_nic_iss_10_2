/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdtlmdb.h,v 1.2 2011/12/09 06:15:59 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDTLMDB_H
#define _STDTLMDB_H

UINT1 TeLinkTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 TeLinkDescriptorTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 TeLinkSrlgTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 TeLinkBandwidthTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 ComponentLinkTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 ComponentLinkDescriptorTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 ComponentLinkBandwidthTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 stdtlm [] ={1,3,6,1,2,1,10,200};
tSNMP_OID_TYPE stdtlmOID = {8, stdtlm};


UINT4 TeLinkAddressType [ ] ={1,3,6,1,2,1,10,200,1,1,1,1};
UINT4 TeLinkLocalIpAddr [ ] ={1,3,6,1,2,1,10,200,1,1,1,2};
UINT4 TeLinkRemoteIpAddr [ ] ={1,3,6,1,2,1,10,200,1,1,1,3};
UINT4 TeLinkMetric [ ] ={1,3,6,1,2,1,10,200,1,1,1,4};
UINT4 TeLinkMaximumReservableBandwidth [ ] ={1,3,6,1,2,1,10,200,1,1,1,5};
UINT4 TeLinkProtectionType [ ] ={1,3,6,1,2,1,10,200,1,1,1,6};
UINT4 TeLinkWorkingPriority [ ] ={1,3,6,1,2,1,10,200,1,1,1,7};
UINT4 TeLinkResourceClass [ ] ={1,3,6,1,2,1,10,200,1,1,1,8};
UINT4 TeLinkIncomingIfId [ ] ={1,3,6,1,2,1,10,200,1,1,1,9};
UINT4 TeLinkOutgoingIfId [ ] ={1,3,6,1,2,1,10,200,1,1,1,10};
UINT4 TeLinkRowStatus [ ] ={1,3,6,1,2,1,10,200,1,1,1,11};
UINT4 TeLinkStorageType [ ] ={1,3,6,1,2,1,10,200,1,1,1,12};
UINT4 TeLinkDescriptorId [ ] ={1,3,6,1,2,1,10,200,1,2,1,1};
UINT4 TeLinkDescrSwitchingCapability [ ] ={1,3,6,1,2,1,10,200,1,2,1,2};
UINT4 TeLinkDescrEncodingType [ ] ={1,3,6,1,2,1,10,200,1,2,1,3};
UINT4 TeLinkDescrMinLspBandwidth [ ] ={1,3,6,1,2,1,10,200,1,2,1,4};
UINT4 TeLinkDescrMaxLspBandwidthPrio0 [ ] ={1,3,6,1,2,1,10,200,1,2,1,5};
UINT4 TeLinkDescrMaxLspBandwidthPrio1 [ ] ={1,3,6,1,2,1,10,200,1,2,1,6};
UINT4 TeLinkDescrMaxLspBandwidthPrio2 [ ] ={1,3,6,1,2,1,10,200,1,2,1,7};
UINT4 TeLinkDescrMaxLspBandwidthPrio3 [ ] ={1,3,6,1,2,1,10,200,1,2,1,8};
UINT4 TeLinkDescrMaxLspBandwidthPrio4 [ ] ={1,3,6,1,2,1,10,200,1,2,1,9};
UINT4 TeLinkDescrMaxLspBandwidthPrio5 [ ] ={1,3,6,1,2,1,10,200,1,2,1,10};
UINT4 TeLinkDescrMaxLspBandwidthPrio6 [ ] ={1,3,6,1,2,1,10,200,1,2,1,11};
UINT4 TeLinkDescrMaxLspBandwidthPrio7 [ ] ={1,3,6,1,2,1,10,200,1,2,1,12};
UINT4 TeLinkDescrInterfaceMtu [ ] ={1,3,6,1,2,1,10,200,1,2,1,13};
UINT4 TeLinkDescrIndication [ ] ={1,3,6,1,2,1,10,200,1,2,1,14};
UINT4 TeLinkDescrRowStatus [ ] ={1,3,6,1,2,1,10,200,1,2,1,15};
UINT4 TeLinkDescrStorageType [ ] ={1,3,6,1,2,1,10,200,1,2,1,16};
UINT4 TeLinkSrlg [ ] ={1,3,6,1,2,1,10,200,1,3,1,1};
UINT4 TeLinkSrlgRowStatus [ ] ={1,3,6,1,2,1,10,200,1,3,1,2};
UINT4 TeLinkSrlgStorageType [ ] ={1,3,6,1,2,1,10,200,1,3,1,3};
UINT4 TeLinkBandwidthPriority [ ] ={1,3,6,1,2,1,10,200,1,4,1,1};
UINT4 TeLinkBandwidthUnreserved [ ] ={1,3,6,1,2,1,10,200,1,4,1,2};
UINT4 TeLinkBandwidthRowStatus [ ] ={1,3,6,1,2,1,10,200,1,4,1,3};
UINT4 TeLinkBandwidthStorageType [ ] ={1,3,6,1,2,1,10,200,1,4,1,4};
UINT4 ComponentLinkMaxResBandwidth [ ] ={1,3,6,1,2,1,10,200,1,5,1,1};
UINT4 ComponentLinkPreferredProtection [ ] ={1,3,6,1,2,1,10,200,1,5,1,2};
UINT4 ComponentLinkCurrentProtection [ ] ={1,3,6,1,2,1,10,200,1,5,1,3};
UINT4 ComponentLinkRowStatus [ ] ={1,3,6,1,2,1,10,200,1,5,1,4};
UINT4 ComponentLinkStorageType [ ] ={1,3,6,1,2,1,10,200,1,5,1,5};
UINT4 ComponentLinkDescrId [ ] ={1,3,6,1,2,1,10,200,1,6,1,1};
UINT4 ComponentLinkDescrSwitchingCapability [ ] ={1,3,6,1,2,1,10,200,1,6,1,2};
UINT4 ComponentLinkDescrEncodingType [ ] ={1,3,6,1,2,1,10,200,1,6,1,3};
UINT4 ComponentLinkDescrMinLspBandwidth [ ] ={1,3,6,1,2,1,10,200,1,6,1,4};
UINT4 ComponentLinkDescrMaxLspBandwidthPrio0 [ ] ={1,3,6,1,2,1,10,200,1,6,1,5};
UINT4 ComponentLinkDescrMaxLspBandwidthPrio1 [ ] ={1,3,6,1,2,1,10,200,1,6,1,6};
UINT4 ComponentLinkDescrMaxLspBandwidthPrio2 [ ] ={1,3,6,1,2,1,10,200,1,6,1,7};
UINT4 ComponentLinkDescrMaxLspBandwidthPrio3 [ ] ={1,3,6,1,2,1,10,200,1,6,1,8};
UINT4 ComponentLinkDescrMaxLspBandwidthPrio4 [ ] ={1,3,6,1,2,1,10,200,1,6,1,9};
UINT4 ComponentLinkDescrMaxLspBandwidthPrio5 [ ] ={1,3,6,1,2,1,10,200,1,6,1,10};
UINT4 ComponentLinkDescrMaxLspBandwidthPrio6 [ ] ={1,3,6,1,2,1,10,200,1,6,1,11};
UINT4 ComponentLinkDescrMaxLspBandwidthPrio7 [ ] ={1,3,6,1,2,1,10,200,1,6,1,12};
UINT4 ComponentLinkDescrInterfaceMtu [ ] ={1,3,6,1,2,1,10,200,1,6,1,13};
UINT4 ComponentLinkDescrIndication [ ] ={1,3,6,1,2,1,10,200,1,6,1,14};
UINT4 ComponentLinkDescrRowStatus [ ] ={1,3,6,1,2,1,10,200,1,6,1,15};
UINT4 ComponentLinkDescrStorageType [ ] ={1,3,6,1,2,1,10,200,1,6,1,16};
UINT4 ComponentLinkBandwidthPriority [ ] ={1,3,6,1,2,1,10,200,1,7,1,1};
UINT4 ComponentLinkBandwidthUnreserved [ ] ={1,3,6,1,2,1,10,200,1,7,1,2};
UINT4 ComponentLinkBandwidthRowStatus [ ] ={1,3,6,1,2,1,10,200,1,7,1,3};
UINT4 ComponentLinkBandwidthStorageType [ ] ={1,3,6,1,2,1,10,200,1,7,1,4};




tMbDbEntry stdtlmMibEntry[]= {

{{12,TeLinkAddressType}, GetNextIndexTeLinkTable, TeLinkAddressTypeGet, TeLinkAddressTypeSet, TeLinkAddressTypeTest, TeLinkTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, TeLinkTableINDEX, 1, 0, 0, NULL},

{{12,TeLinkLocalIpAddr}, GetNextIndexTeLinkTable, TeLinkLocalIpAddrGet, TeLinkLocalIpAddrSet, TeLinkLocalIpAddrTest, TeLinkTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, TeLinkTableINDEX, 1, 0, 0, NULL},

{{12,TeLinkRemoteIpAddr}, GetNextIndexTeLinkTable, TeLinkRemoteIpAddrGet, TeLinkRemoteIpAddrSet, TeLinkRemoteIpAddrTest, TeLinkTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, TeLinkTableINDEX, 1, 0, 0, NULL},

{{12,TeLinkMetric}, GetNextIndexTeLinkTable, TeLinkMetricGet, TeLinkMetricSet, TeLinkMetricTest, TeLinkTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, TeLinkTableINDEX, 1, 0, 0, NULL},

{{12,TeLinkMaximumReservableBandwidth}, GetNextIndexTeLinkTable, TeLinkMaximumReservableBandwidthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, TeLinkTableINDEX, 1, 0, 0, NULL},

{{12,TeLinkProtectionType}, GetNextIndexTeLinkTable, TeLinkProtectionTypeGet, TeLinkProtectionTypeSet, TeLinkProtectionTypeTest, TeLinkTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, TeLinkTableINDEX, 1, 0, 0, NULL},

{{12,TeLinkWorkingPriority}, GetNextIndexTeLinkTable, TeLinkWorkingPriorityGet, TeLinkWorkingPrioritySet, TeLinkWorkingPriorityTest, TeLinkTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, TeLinkTableINDEX, 1, 0, 0, NULL},

{{12,TeLinkResourceClass}, GetNextIndexTeLinkTable, TeLinkResourceClassGet, TeLinkResourceClassSet, TeLinkResourceClassTest, TeLinkTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, TeLinkTableINDEX, 1, 0, 0, NULL},

{{12,TeLinkIncomingIfId}, GetNextIndexTeLinkTable, TeLinkIncomingIfIdGet, TeLinkIncomingIfIdSet, TeLinkIncomingIfIdTest, TeLinkTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, TeLinkTableINDEX, 1, 0, 0, NULL},

{{12,TeLinkOutgoingIfId}, GetNextIndexTeLinkTable, TeLinkOutgoingIfIdGet, TeLinkOutgoingIfIdSet, TeLinkOutgoingIfIdTest, TeLinkTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, TeLinkTableINDEX, 1, 0, 0, NULL},

{{12,TeLinkRowStatus}, GetNextIndexTeLinkTable, TeLinkRowStatusGet, TeLinkRowStatusSet, TeLinkRowStatusTest, TeLinkTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, TeLinkTableINDEX, 1, 0, 1, NULL},

{{12,TeLinkStorageType}, GetNextIndexTeLinkTable, TeLinkStorageTypeGet, TeLinkStorageTypeSet, TeLinkStorageTypeTest, TeLinkTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, TeLinkTableINDEX, 1, 0, 0, NULL},

{{12,TeLinkDescriptorId}, GetNextIndexTeLinkDescriptorTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, TeLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,TeLinkDescrSwitchingCapability}, GetNextIndexTeLinkDescriptorTable, TeLinkDescrSwitchingCapabilityGet, TeLinkDescrSwitchingCapabilitySet, TeLinkDescrSwitchingCapabilityTest, TeLinkDescriptorTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, TeLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,TeLinkDescrEncodingType}, GetNextIndexTeLinkDescriptorTable, TeLinkDescrEncodingTypeGet, TeLinkDescrEncodingTypeSet, TeLinkDescrEncodingTypeTest, TeLinkDescriptorTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, TeLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,TeLinkDescrMinLspBandwidth}, GetNextIndexTeLinkDescriptorTable, TeLinkDescrMinLspBandwidthGet, TeLinkDescrMinLspBandwidthSet, TeLinkDescrMinLspBandwidthTest, TeLinkDescriptorTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, TeLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,TeLinkDescrMaxLspBandwidthPrio0}, GetNextIndexTeLinkDescriptorTable, TeLinkDescrMaxLspBandwidthPrio0Get, TeLinkDescrMaxLspBandwidthPrio0Set, TeLinkDescrMaxLspBandwidthPrio0Test, TeLinkDescriptorTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, TeLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,TeLinkDescrMaxLspBandwidthPrio1}, GetNextIndexTeLinkDescriptorTable, TeLinkDescrMaxLspBandwidthPrio1Get, TeLinkDescrMaxLspBandwidthPrio1Set, TeLinkDescrMaxLspBandwidthPrio1Test, TeLinkDescriptorTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, TeLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,TeLinkDescrMaxLspBandwidthPrio2}, GetNextIndexTeLinkDescriptorTable, TeLinkDescrMaxLspBandwidthPrio2Get, TeLinkDescrMaxLspBandwidthPrio2Set, TeLinkDescrMaxLspBandwidthPrio2Test, TeLinkDescriptorTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, TeLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,TeLinkDescrMaxLspBandwidthPrio3}, GetNextIndexTeLinkDescriptorTable, TeLinkDescrMaxLspBandwidthPrio3Get, TeLinkDescrMaxLspBandwidthPrio3Set, TeLinkDescrMaxLspBandwidthPrio3Test, TeLinkDescriptorTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, TeLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,TeLinkDescrMaxLspBandwidthPrio4}, GetNextIndexTeLinkDescriptorTable, TeLinkDescrMaxLspBandwidthPrio4Get, TeLinkDescrMaxLspBandwidthPrio4Set, TeLinkDescrMaxLspBandwidthPrio4Test, TeLinkDescriptorTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, TeLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,TeLinkDescrMaxLspBandwidthPrio5}, GetNextIndexTeLinkDescriptorTable, TeLinkDescrMaxLspBandwidthPrio5Get, TeLinkDescrMaxLspBandwidthPrio5Set, TeLinkDescrMaxLspBandwidthPrio5Test, TeLinkDescriptorTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, TeLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,TeLinkDescrMaxLspBandwidthPrio6}, GetNextIndexTeLinkDescriptorTable, TeLinkDescrMaxLspBandwidthPrio6Get, TeLinkDescrMaxLspBandwidthPrio6Set, TeLinkDescrMaxLspBandwidthPrio6Test, TeLinkDescriptorTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, TeLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,TeLinkDescrMaxLspBandwidthPrio7}, GetNextIndexTeLinkDescriptorTable, TeLinkDescrMaxLspBandwidthPrio7Get, TeLinkDescrMaxLspBandwidthPrio7Set, TeLinkDescrMaxLspBandwidthPrio7Test, TeLinkDescriptorTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, TeLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,TeLinkDescrInterfaceMtu}, GetNextIndexTeLinkDescriptorTable, TeLinkDescrInterfaceMtuGet, TeLinkDescrInterfaceMtuSet, TeLinkDescrInterfaceMtuTest, TeLinkDescriptorTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, TeLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,TeLinkDescrIndication}, GetNextIndexTeLinkDescriptorTable, TeLinkDescrIndicationGet, TeLinkDescrIndicationSet, TeLinkDescrIndicationTest, TeLinkDescriptorTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, TeLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,TeLinkDescrRowStatus}, GetNextIndexTeLinkDescriptorTable, TeLinkDescrRowStatusGet, TeLinkDescrRowStatusSet, TeLinkDescrRowStatusTest, TeLinkDescriptorTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, TeLinkDescriptorTableINDEX, 2, 0, 1, NULL},

{{12,TeLinkDescrStorageType}, GetNextIndexTeLinkDescriptorTable, TeLinkDescrStorageTypeGet, TeLinkDescrStorageTypeSet, TeLinkDescrStorageTypeTest, TeLinkDescriptorTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, TeLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,TeLinkSrlg}, GetNextIndexTeLinkSrlgTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, TeLinkSrlgTableINDEX, 2, 0, 0, NULL},

{{12,TeLinkSrlgRowStatus}, GetNextIndexTeLinkSrlgTable, TeLinkSrlgRowStatusGet, TeLinkSrlgRowStatusSet, TeLinkSrlgRowStatusTest, TeLinkSrlgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, TeLinkSrlgTableINDEX, 2, 0, 1, NULL},

{{12,TeLinkSrlgStorageType}, GetNextIndexTeLinkSrlgTable, TeLinkSrlgStorageTypeGet, TeLinkSrlgStorageTypeSet, TeLinkSrlgStorageTypeTest, TeLinkSrlgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, TeLinkSrlgTableINDEX, 2, 0, 0, NULL},

{{12,TeLinkBandwidthPriority}, GetNextIndexTeLinkBandwidthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, TeLinkBandwidthTableINDEX, 2, 0, 0, NULL},

{{12,TeLinkBandwidthUnreserved}, GetNextIndexTeLinkBandwidthTable, TeLinkBandwidthUnreservedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, TeLinkBandwidthTableINDEX, 2, 0, 0, NULL},

{{12,TeLinkBandwidthRowStatus}, GetNextIndexTeLinkBandwidthTable, TeLinkBandwidthRowStatusGet, TeLinkBandwidthRowStatusSet, TeLinkBandwidthRowStatusTest, TeLinkBandwidthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, TeLinkBandwidthTableINDEX, 2, 0, 1, NULL},

{{12,TeLinkBandwidthStorageType}, GetNextIndexTeLinkBandwidthTable, TeLinkBandwidthStorageTypeGet, TeLinkBandwidthStorageTypeSet, TeLinkBandwidthStorageTypeTest, TeLinkBandwidthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, TeLinkBandwidthTableINDEX, 2, 0, 0, NULL},

{{12,ComponentLinkMaxResBandwidth}, GetNextIndexComponentLinkTable, ComponentLinkMaxResBandwidthGet, ComponentLinkMaxResBandwidthSet, ComponentLinkMaxResBandwidthTest, ComponentLinkTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, ComponentLinkTableINDEX, 1, 0, 0, NULL},

{{12,ComponentLinkPreferredProtection}, GetNextIndexComponentLinkTable, ComponentLinkPreferredProtectionGet, ComponentLinkPreferredProtectionSet, ComponentLinkPreferredProtectionTest, ComponentLinkTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, ComponentLinkTableINDEX, 1, 0, 0, NULL},

{{12,ComponentLinkCurrentProtection}, GetNextIndexComponentLinkTable, ComponentLinkCurrentProtectionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, ComponentLinkTableINDEX, 1, 0, 0, NULL},

{{12,ComponentLinkRowStatus}, GetNextIndexComponentLinkTable, ComponentLinkRowStatusGet, ComponentLinkRowStatusSet, ComponentLinkRowStatusTest, ComponentLinkTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, ComponentLinkTableINDEX, 1, 0, 1, NULL},

{{12,ComponentLinkStorageType}, GetNextIndexComponentLinkTable, ComponentLinkStorageTypeGet, ComponentLinkStorageTypeSet, ComponentLinkStorageTypeTest, ComponentLinkTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, ComponentLinkTableINDEX, 1, 0, 0, NULL},

{{12,ComponentLinkDescrId}, GetNextIndexComponentLinkDescriptorTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, ComponentLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,ComponentLinkDescrSwitchingCapability}, GetNextIndexComponentLinkDescriptorTable, ComponentLinkDescrSwitchingCapabilityGet, ComponentLinkDescrSwitchingCapabilitySet, ComponentLinkDescrSwitchingCapabilityTest, ComponentLinkDescriptorTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, ComponentLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,ComponentLinkDescrEncodingType}, GetNextIndexComponentLinkDescriptorTable, ComponentLinkDescrEncodingTypeGet, ComponentLinkDescrEncodingTypeSet, ComponentLinkDescrEncodingTypeTest, ComponentLinkDescriptorTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, ComponentLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,ComponentLinkDescrMinLspBandwidth}, GetNextIndexComponentLinkDescriptorTable, ComponentLinkDescrMinLspBandwidthGet, ComponentLinkDescrMinLspBandwidthSet, ComponentLinkDescrMinLspBandwidthTest, ComponentLinkDescriptorTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, ComponentLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,ComponentLinkDescrMaxLspBandwidthPrio0}, GetNextIndexComponentLinkDescriptorTable, ComponentLinkDescrMaxLspBandwidthPrio0Get, ComponentLinkDescrMaxLspBandwidthPrio0Set, ComponentLinkDescrMaxLspBandwidthPrio0Test, ComponentLinkDescriptorTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, ComponentLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,ComponentLinkDescrMaxLspBandwidthPrio1}, GetNextIndexComponentLinkDescriptorTable, ComponentLinkDescrMaxLspBandwidthPrio1Get, ComponentLinkDescrMaxLspBandwidthPrio1Set, ComponentLinkDescrMaxLspBandwidthPrio1Test, ComponentLinkDescriptorTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, ComponentLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,ComponentLinkDescrMaxLspBandwidthPrio2}, GetNextIndexComponentLinkDescriptorTable, ComponentLinkDescrMaxLspBandwidthPrio2Get, ComponentLinkDescrMaxLspBandwidthPrio2Set, ComponentLinkDescrMaxLspBandwidthPrio2Test, ComponentLinkDescriptorTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, ComponentLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,ComponentLinkDescrMaxLspBandwidthPrio3}, GetNextIndexComponentLinkDescriptorTable, ComponentLinkDescrMaxLspBandwidthPrio3Get, ComponentLinkDescrMaxLspBandwidthPrio3Set, ComponentLinkDescrMaxLspBandwidthPrio3Test, ComponentLinkDescriptorTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, ComponentLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,ComponentLinkDescrMaxLspBandwidthPrio4}, GetNextIndexComponentLinkDescriptorTable, ComponentLinkDescrMaxLspBandwidthPrio4Get, ComponentLinkDescrMaxLspBandwidthPrio4Set, ComponentLinkDescrMaxLspBandwidthPrio4Test, ComponentLinkDescriptorTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, ComponentLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,ComponentLinkDescrMaxLspBandwidthPrio5}, GetNextIndexComponentLinkDescriptorTable, ComponentLinkDescrMaxLspBandwidthPrio5Get, ComponentLinkDescrMaxLspBandwidthPrio5Set, ComponentLinkDescrMaxLspBandwidthPrio5Test, ComponentLinkDescriptorTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, ComponentLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,ComponentLinkDescrMaxLspBandwidthPrio6}, GetNextIndexComponentLinkDescriptorTable, ComponentLinkDescrMaxLspBandwidthPrio6Get, ComponentLinkDescrMaxLspBandwidthPrio6Set, ComponentLinkDescrMaxLspBandwidthPrio6Test, ComponentLinkDescriptorTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, ComponentLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,ComponentLinkDescrMaxLspBandwidthPrio7}, GetNextIndexComponentLinkDescriptorTable, ComponentLinkDescrMaxLspBandwidthPrio7Get, ComponentLinkDescrMaxLspBandwidthPrio7Set, ComponentLinkDescrMaxLspBandwidthPrio7Test, ComponentLinkDescriptorTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, ComponentLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,ComponentLinkDescrInterfaceMtu}, GetNextIndexComponentLinkDescriptorTable, ComponentLinkDescrInterfaceMtuGet, ComponentLinkDescrInterfaceMtuSet, ComponentLinkDescrInterfaceMtuTest, ComponentLinkDescriptorTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, ComponentLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,ComponentLinkDescrIndication}, GetNextIndexComponentLinkDescriptorTable, ComponentLinkDescrIndicationGet, ComponentLinkDescrIndicationSet, ComponentLinkDescrIndicationTest, ComponentLinkDescriptorTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, ComponentLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,ComponentLinkDescrRowStatus}, GetNextIndexComponentLinkDescriptorTable, ComponentLinkDescrRowStatusGet, ComponentLinkDescrRowStatusSet, ComponentLinkDescrRowStatusTest, ComponentLinkDescriptorTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, ComponentLinkDescriptorTableINDEX, 2, 0, 1, NULL},

{{12,ComponentLinkDescrStorageType}, GetNextIndexComponentLinkDescriptorTable, ComponentLinkDescrStorageTypeGet, ComponentLinkDescrStorageTypeSet, ComponentLinkDescrStorageTypeTest, ComponentLinkDescriptorTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, ComponentLinkDescriptorTableINDEX, 2, 0, 0, NULL},

{{12,ComponentLinkBandwidthPriority}, GetNextIndexComponentLinkBandwidthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, ComponentLinkBandwidthTableINDEX, 2, 0, 0, NULL},

{{12,ComponentLinkBandwidthUnreserved}, GetNextIndexComponentLinkBandwidthTable, ComponentLinkBandwidthUnreservedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, ComponentLinkBandwidthTableINDEX, 2, 0, 0, NULL},

{{12,ComponentLinkBandwidthRowStatus}, GetNextIndexComponentLinkBandwidthTable, ComponentLinkBandwidthRowStatusGet, ComponentLinkBandwidthRowStatusSet, ComponentLinkBandwidthRowStatusTest, ComponentLinkBandwidthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, ComponentLinkBandwidthTableINDEX, 2, 0, 1, NULL},

{{12,ComponentLinkBandwidthStorageType}, GetNextIndexComponentLinkBandwidthTable, ComponentLinkBandwidthStorageTypeGet, ComponentLinkBandwidthStorageTypeSet, ComponentLinkBandwidthStorageTypeTest, ComponentLinkBandwidthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, ComponentLinkBandwidthTableINDEX, 2, 0, 0, NULL},
};
tMibData stdtlmEntry = { 60, stdtlmMibEntry };

#endif /* _STDTLMDB_H */

