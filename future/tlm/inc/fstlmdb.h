/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstlmdb.h,v 1.3 2012/04/02 12:31:45 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSTLMDB_H
#define _FSTLMDB_H

UINT1 FsTeLinkTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsTeLinkBwThresholdTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fstlm [] ={1,3,6,1,4,1,29601,2,67};
tSNMP_OID_TYPE fstlmOID = {9, fstlm};


UINT4 FsTeLinkTraceOption [ ] ={1,3,6,1,4,1,29601,2,67,1,1};
UINT4 FsTeLinkModuleStatus [ ] ={1,3,6,1,4,1,29601,2,67,1,2};
UINT4 FsTeLinkName [ ] ={1,3,6,1,4,1,29601,2,67,2,1,1,1};
UINT4 FsTeLinkRemoteRtrId [ ] ={1,3,6,1,4,1,29601,2,67,2,1,1,2};
UINT4 FsTeLinkMaximumBandwidth [ ] ={1,3,6,1,4,1,29601,2,67,2,1,1,3};
UINT4 FsTeLinkType [ ] ={1,3,6,1,4,1,29601,2,67,2,1,1,4};
UINT4 FsTeLinkInfoType [ ] ={1,3,6,1,4,1,29601,2,67,2,1,1,5};
UINT4 FsTeLinkIfType [ ] ={1,3,6,1,4,1,29601,2,67,2,1,1,6};
UINT4 FsTeLinkIsAdvertise [ ] ={1,3,6,1,4,1,29601,2,67,2,1,1,7};
UINT4 FsTeLinkBwThresholdIndex [ ] ={1,3,6,1,4,1,29601,2,67,2,2,1,1};
UINT4 FsTeLinkBwThreshold0 [ ] ={1,3,6,1,4,1,29601,2,67,2,2,1,2};
UINT4 FsTeLinkBwThreshold1 [ ] ={1,3,6,1,4,1,29601,2,67,2,2,1,3};
UINT4 FsTeLinkBwThreshold2 [ ] ={1,3,6,1,4,1,29601,2,67,2,2,1,4};
UINT4 FsTeLinkBwThreshold3 [ ] ={1,3,6,1,4,1,29601,2,67,2,2,1,5};
UINT4 FsTeLinkBwThreshold4 [ ] ={1,3,6,1,4,1,29601,2,67,2,2,1,6};
UINT4 FsTeLinkBwThreshold5 [ ] ={1,3,6,1,4,1,29601,2,67,2,2,1,7};
UINT4 FsTeLinkBwThreshold6 [ ] ={1,3,6,1,4,1,29601,2,67,2,2,1,8};
UINT4 FsTeLinkBwThreshold7 [ ] ={1,3,6,1,4,1,29601,2,67,2,2,1,9};
UINT4 FsTeLinkBwThreshold8 [ ] ={1,3,6,1,4,1,29601,2,67,2,2,1,10};
UINT4 FsTeLinkBwThreshold9 [ ] ={1,3,6,1,4,1,29601,2,67,2,2,1,11};
UINT4 FsTeLinkBwThreshold10 [ ] ={1,3,6,1,4,1,29601,2,67,2,2,1,12};
UINT4 FsTeLinkBwThreshold11 [ ] ={1,3,6,1,4,1,29601,2,67,2,2,1,13};
UINT4 FsTeLinkBwThreshold12 [ ] ={1,3,6,1,4,1,29601,2,67,2,2,1,14};
UINT4 FsTeLinkBwThreshold13 [ ] ={1,3,6,1,4,1,29601,2,67,2,2,1,15};
UINT4 FsTeLinkBwThreshold14 [ ] ={1,3,6,1,4,1,29601,2,67,2,2,1,16};
UINT4 FsTeLinkBwThreshold15 [ ] ={1,3,6,1,4,1,29601,2,67,2,2,1,17};
UINT4 FsTeLinkBwThresholdRowStatus [ ] ={1,3,6,1,4,1,29601,2,67,2,2,1,18};
UINT4 FsTeLinkBwThresholdForceOption [ ] ={1,3,6,1,4,1,29601,2,67,2,3};




tMbDbEntry fstlmMibEntry[]= {

{{11,FsTeLinkTraceOption}, NULL, FsTeLinkTraceOptionGet, FsTeLinkTraceOptionSet, FsTeLinkTraceOptionTest, FsTeLinkTraceOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsTeLinkModuleStatus}, NULL, FsTeLinkModuleStatusGet, FsTeLinkModuleStatusSet, FsTeLinkModuleStatusTest, FsTeLinkModuleStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{13,FsTeLinkName}, GetNextIndexFsTeLinkTable, FsTeLinkNameGet, FsTeLinkNameSet, FsTeLinkNameTest, FsTeLinkTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsTeLinkTableINDEX, 1, 0, 0, NULL},

{{13,FsTeLinkRemoteRtrId}, GetNextIndexFsTeLinkTable, FsTeLinkRemoteRtrIdGet, FsTeLinkRemoteRtrIdSet, FsTeLinkRemoteRtrIdTest, FsTeLinkTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsTeLinkTableINDEX, 1, 0, 0, NULL},

{{13,FsTeLinkMaximumBandwidth}, GetNextIndexFsTeLinkTable, FsTeLinkMaximumBandwidthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsTeLinkTableINDEX, 1, 0, 0, NULL},

{{13,FsTeLinkType}, GetNextIndexFsTeLinkTable, FsTeLinkTypeGet, FsTeLinkTypeSet, FsTeLinkTypeTest, FsTeLinkTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTeLinkTableINDEX, 1, 0, 0, "0"},

{{13,FsTeLinkInfoType}, GetNextIndexFsTeLinkTable, FsTeLinkInfoTypeGet, FsTeLinkInfoTypeSet, FsTeLinkInfoTypeTest, FsTeLinkTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTeLinkTableINDEX, 1, 0, 0, NULL},

{{13,FsTeLinkIfType}, GetNextIndexFsTeLinkTable, FsTeLinkIfTypeGet, FsTeLinkIfTypeSet, FsTeLinkIfTypeTest, FsTeLinkTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTeLinkTableINDEX, 1, 0, 0, "1"},

{{13,FsTeLinkIsAdvertise}, GetNextIndexFsTeLinkTable, FsTeLinkIsAdvertiseGet, FsTeLinkIsAdvertiseSet, FsTeLinkIsAdvertiseTest, FsTeLinkTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTeLinkTableINDEX, 1, 0, 0, "1"},

{{13,FsTeLinkBwThresholdIndex}, GetNextIndexFsTeLinkBwThresholdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsTeLinkBwThresholdTableINDEX, 2, 0, 0, NULL},

{{13,FsTeLinkBwThreshold0}, GetNextIndexFsTeLinkBwThresholdTable, FsTeLinkBwThreshold0Get, FsTeLinkBwThreshold0Set, FsTeLinkBwThreshold0Test, FsTeLinkBwThresholdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTeLinkBwThresholdTableINDEX, 2, 0, 0, NULL},

{{13,FsTeLinkBwThreshold1}, GetNextIndexFsTeLinkBwThresholdTable, FsTeLinkBwThreshold1Get, FsTeLinkBwThreshold1Set, FsTeLinkBwThreshold1Test, FsTeLinkBwThresholdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTeLinkBwThresholdTableINDEX, 2, 0, 0, NULL},

{{13,FsTeLinkBwThreshold2}, GetNextIndexFsTeLinkBwThresholdTable, FsTeLinkBwThreshold2Get, FsTeLinkBwThreshold2Set, FsTeLinkBwThreshold2Test, FsTeLinkBwThresholdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTeLinkBwThresholdTableINDEX, 2, 0, 0, NULL},

{{13,FsTeLinkBwThreshold3}, GetNextIndexFsTeLinkBwThresholdTable, FsTeLinkBwThreshold3Get, FsTeLinkBwThreshold3Set, FsTeLinkBwThreshold3Test, FsTeLinkBwThresholdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTeLinkBwThresholdTableINDEX, 2, 0, 0, NULL},

{{13,FsTeLinkBwThreshold4}, GetNextIndexFsTeLinkBwThresholdTable, FsTeLinkBwThreshold4Get, FsTeLinkBwThreshold4Set, FsTeLinkBwThreshold4Test, FsTeLinkBwThresholdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTeLinkBwThresholdTableINDEX, 2, 0, 0, NULL},

{{13,FsTeLinkBwThreshold5}, GetNextIndexFsTeLinkBwThresholdTable, FsTeLinkBwThreshold5Get, FsTeLinkBwThreshold5Set, FsTeLinkBwThreshold5Test, FsTeLinkBwThresholdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTeLinkBwThresholdTableINDEX, 2, 0, 0, NULL},

{{13,FsTeLinkBwThreshold6}, GetNextIndexFsTeLinkBwThresholdTable, FsTeLinkBwThreshold6Get, FsTeLinkBwThreshold6Set, FsTeLinkBwThreshold6Test, FsTeLinkBwThresholdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTeLinkBwThresholdTableINDEX, 2, 0, 0, NULL},

{{13,FsTeLinkBwThreshold7}, GetNextIndexFsTeLinkBwThresholdTable, FsTeLinkBwThreshold7Get, FsTeLinkBwThreshold7Set, FsTeLinkBwThreshold7Test, FsTeLinkBwThresholdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTeLinkBwThresholdTableINDEX, 2, 0, 0, NULL},

{{13,FsTeLinkBwThreshold8}, GetNextIndexFsTeLinkBwThresholdTable, FsTeLinkBwThreshold8Get, FsTeLinkBwThreshold8Set, FsTeLinkBwThreshold8Test, FsTeLinkBwThresholdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTeLinkBwThresholdTableINDEX, 2, 0, 0, NULL},

{{13,FsTeLinkBwThreshold9}, GetNextIndexFsTeLinkBwThresholdTable, FsTeLinkBwThreshold9Get, FsTeLinkBwThreshold9Set, FsTeLinkBwThreshold9Test, FsTeLinkBwThresholdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTeLinkBwThresholdTableINDEX, 2, 0, 0, NULL},

{{13,FsTeLinkBwThreshold10}, GetNextIndexFsTeLinkBwThresholdTable, FsTeLinkBwThreshold10Get, FsTeLinkBwThreshold10Set, FsTeLinkBwThreshold10Test, FsTeLinkBwThresholdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTeLinkBwThresholdTableINDEX, 2, 0, 0, NULL},

{{13,FsTeLinkBwThreshold11}, GetNextIndexFsTeLinkBwThresholdTable, FsTeLinkBwThreshold11Get, FsTeLinkBwThreshold11Set, FsTeLinkBwThreshold11Test, FsTeLinkBwThresholdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTeLinkBwThresholdTableINDEX, 2, 0, 0, NULL},

{{13,FsTeLinkBwThreshold12}, GetNextIndexFsTeLinkBwThresholdTable, FsTeLinkBwThreshold12Get, FsTeLinkBwThreshold12Set, FsTeLinkBwThreshold12Test, FsTeLinkBwThresholdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTeLinkBwThresholdTableINDEX, 2, 0, 0, NULL},

{{13,FsTeLinkBwThreshold13}, GetNextIndexFsTeLinkBwThresholdTable, FsTeLinkBwThreshold13Get, FsTeLinkBwThreshold13Set, FsTeLinkBwThreshold13Test, FsTeLinkBwThresholdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTeLinkBwThresholdTableINDEX, 2, 0, 0, NULL},

{{13,FsTeLinkBwThreshold14}, GetNextIndexFsTeLinkBwThresholdTable, FsTeLinkBwThreshold14Get, FsTeLinkBwThreshold14Set, FsTeLinkBwThreshold14Test, FsTeLinkBwThresholdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTeLinkBwThresholdTableINDEX, 2, 0, 0, NULL},

{{13,FsTeLinkBwThreshold15}, GetNextIndexFsTeLinkBwThresholdTable, FsTeLinkBwThreshold15Get, FsTeLinkBwThreshold15Set, FsTeLinkBwThreshold15Test, FsTeLinkBwThresholdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTeLinkBwThresholdTableINDEX, 2, 0, 0, NULL},

{{13,FsTeLinkBwThresholdRowStatus}, GetNextIndexFsTeLinkBwThresholdTable, FsTeLinkBwThresholdRowStatusGet, FsTeLinkBwThresholdRowStatusSet, FsTeLinkBwThresholdRowStatusTest, FsTeLinkBwThresholdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTeLinkBwThresholdTableINDEX, 2, 0, 1, NULL},

{{11,FsTeLinkBwThresholdForceOption}, NULL, FsTeLinkBwThresholdForceOptionGet, FsTeLinkBwThresholdForceOptionSet, FsTeLinkBwThresholdForceOptionTest, FsTeLinkBwThresholdForceOptionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData fstlmEntry = { 28, fstlmMibEntry };

#endif /* _FSTLMDB_H */

