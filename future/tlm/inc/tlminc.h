/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tlminc.h,v 1.3 2012/05/28 13:42:03 siva Exp $
 *
 * Description: This file contains all include files for TLM
 *******************************************************************/


#ifndef _TLMINC_H_
#define _TLMINC_H_

#include "lr.h"
#include "fsap.h"
#include "cfa.h"

#include "trace.h"
#include "tlm.h"

#include "tlmdef.h"



#include "tlmtdfs.h"
#include "tlmglob.h"
#include "tlmmacs.h"
#include "tlmprot.h"
#include "tlmdbg.h"
#include "stdtlmwr.h"
#include "stdtlmlw.h"
#include "fstlmwr.h"
#include "fstlmlw.h"

#include "tlmsz.h"
#include "sizereg.h"
#ifdef MPLS_WANTED
#include "mpls.h"
#endif
#include "redblack.h"
#endif
