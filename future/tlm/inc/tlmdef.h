/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: tlmdef.h,v 1.8 2014/11/17 12:00:11 siva Exp $
 *
 * Description: This file contains definitions macros for some 
 *              Hard coded values.
 *********************************************************************/

#ifndef _TLM_DEF_H_
#define _TLM_DEF_H_

#define TLM_ENABLED 1
#define TLM_DISABLED 2

#define TLM_ADMIN_UP 1
#define TLM_ADMIN_DOWN 2

#define TLM_OPER_UP   CFA_IF_UP
#define TLM_OPER_DOWN CFA_IF_DOWN

#define TLM_BW_THRESHOLD_FORCE_ENABLE 1

#define TLM_RESBW_INCREASE 1
#define TLM_RESBW_DECREASE 2

#define TLM_BUNDLE_UPDATE 1
#define TLM_BUNDLE_DELETE 2
#define TLM_UNBUNDLE_UPDATE 3
#define TLM_UNBUNDLE_DELETE 4

#define TLM_CLASS_TYPE_SET 1
#define TLM_CLASS_TYPE_UNSET 0

#define TLM_INVALID_CLASS_TYPE 255
#define TLM_INVALID_PRIORITY 255
#define TLM_INVALID_STATUS 255

#define TLM_ROWSTATUS_INVALID    0
#define TLM_INVALID    0
#define TLM_DEF_BANDWIDTH_VALUE 0

#define TLM_MPLS_SUCCESS TLM_SUCCESS
#define TLM_MPLS_FAILURE TLM_FAILURE

#define TLM_MAX_IFSPEED_VALUE    0xFFFFFFFF

#define TLM_HEX_FAILURE  -1

#define TLM_RESOURCE_CLASS_NAME  1
#define TLM_RESOURCE_CLASS_VALUE  2

#define TLM_MPLS_TE_CLASS_CREATED        1
#define TLM_MPLS_TE_CLASS_DELETED        2 
#define TLM_MPLS_TE_CLASS_NOT_IN_SERVICE 3 

#define TLM_DSTE_ENABLED             1
#define TLM_DSTE_DISABLED            0

#define TLM_MODULE_UP OSPF_TE_TLM_MODULE_UP 
#define TLM_MODULE_DOWN OSPF_TE_TLM_MODULE_DOWN
#define TLM_LINK_INFO OSPF_TE_TLM_LINK_INFO


#define TLM_STORAGE_OTHER 1
#define TLM_STORAGE_VOLATILE 2
#define TLM_STORAGE_NON_VOLATILE 3
#define TLM_STORAGE_PERMANENT 4
#define TLM_STORAGE_RDONLY 5

#define TLM_DEF_ADDRESS_TYPE    TLM_ADDRESSTYPE_UNKNOWN
#define TLM_DEF_SWITCHING_CAP   OSPF_TE_PSC_1
#define TLM_DEF_ENCODING_TYPE   TLM_TE_ENCODING_TYPE_PACKET
#define TLM_COMPONENT_ADD       1

#define TLM_TELINK_TABLE_INDICES_COUNT_ONE          1
#define TLM_TELINK_DESC_TABLE_INDICES_COUNT_TWO     2
#define TLM_TELINK_SRLG_TABLE_INDICES_COUNT_TWO     2
#define TLM_TELINK_BW_TABLE_INDICES_COUNT_TWO       2
#define TLM_COMPLINK_TABLE_INDICES_COUNT_ONE        1
#define TLM_COMPLINK_DESC_TABLE_INDICES_COUNT_TWO   2
#define TLM_COMPLINK_BW_TABLE_INDICES_COUNT_TWO     2
#define TLM_DEF_ROUTER_ID           0
#define TLM_DEF_METRIC_VALUE        1
#define TLM_DEF_PROTECTION_TYPE     TLM_TE_LINK_PROTECTION_TYPE_UNPROTECTED 
#define TLM_DEF_RESOURCECLASS_COLOR 0
#define TLM_DEF_LOCAL_IDENTIFIER    0
#define TLM_DEF_REMOTE_IDENTIFIER   0
#define TLM_DEF_MTU_SIZE            0
#define TLM_DEF_INVALID_INDICATION  0
#define TLM_ZERO                    0

#define TLM_COMPONENT_UPDATE        1
#define TLM_COMPONENT_DELETE        2
#define TLM_TELINK_UPDATE           3  
#define TLM_TELINK_DELETE           4  

#define TLM_TE_ENCODING_TYPE_PACKET           1

#define  TLM_TE_LINK_PROTECTION_PRIMARY      1
#define  TLM_TE_LINK_PROTECTION_SECONDARY    2

#define TLM_TE_LINK_INDICATION_STANDARD 0
#define TLM_TE_LINK_INDICATION_ARBITRARY 1

#define TLM_MAX_TE_LINK_PARAMETERS_VALUE        32767     
#define TLM_MAX_TE_DESCRIPTOR_PARAMETERS_VALUE    255 
#define TLM_MAX_TE_SRLG_PARAMETERS_VALUE            7 
#define TLM_MAX_TE_BANDWIDTH_PARAMETERS_VALUE       7
#define TLM_MAX_PERCENTAGE_VALUE                    100

#define TLM_RUSSIAN_DOLLS_MODEL           1
#define TLM_NOT_RUSSIAN_DOLLS_MODEL       2

#define TLM_COMPLINK_DESCRID         1
#define TLM_TELINK_DESCRID           1


#define  TLM_COMP_LINK_CURRENT_PROTECTION_PRIMARY        1
#define  TLM_COMP_LINK_CURRENT_PROTECTION_SECONDARY      2
#define  TLM_COMP_LINK_PREFERRED_PROTECTION_PRIMARY      1
#define  TLM_COMP_LINK_PREFERRED_PROTECTION_SECONDARY    2


#define TLM_THOUSAND_BYTES    1000 /* will use in TlmEventNotificationMsgInfo */


#define SNMP_SUCCESS 1

#define TLM_MAX_COMPONENT_LINKS_PER_TE_LINK  1
#define TLM_MAX_TE_LINKS_IN_BUNDLE           32
#define TLM_MAX_LEN_OCTETSTRING              255

#define TLM_MAX_IF_INDEX                     CFA_MAX_TE_LINK_IF_INDEX
#define TLM_MIN_IF_INDEX                     CFA_MIN_TE_LINK_IF_INDEX


#define  BUCKET_SIZE  16 

#define TLM_REGISTER     1        /* Tlm Module to register itself with OSPFTE */
#define TLM_DEREGISTER   2        /* Tlm Module to deregister itself with OSPFTE   Corrosponding to this things there must be nmh routines*/

/* Trace type  definitions */
#define TLM_MIN_TRC_VAL  0
#define TLM_MAX_TRC_VAL  TLM_ALL_TRC

#define MAX_TLM_QUEUE_SIZE          (MAX_TLM_LINKS * 2)

#define TLM_SNMP_TRUE  1
#define TLM_SNMP_FALSE  2

#define TLM_MUT_EXCL_SEM_NAME (UINT1 *) "TLMSem"

#define TLM_INIT_VAL    0
#define TM01_TASK_QUE_15_ENQ_EVENT  999

#define TLM_EV_CFG_MSG_IN_QUEUE     0x01
#define TLM_APP_REG_DREG_EVENT      0x02
#define TLM_CFA_IFSTATUS_CHANGE     0x03
#define TLM_CFA_INTF_DELETE         0x04
#define TLM_MPLS_DIFFSERV_TE_PARAMS 0x05

#define TLM_EV_ALL                  (TLM_EV_CFG_MSG_IN_QUEUE |\
                                     TLM_APP_REG_DREG_EVENT |\
                                     TLM_CFA_IFSTATUS_CHANGE |\
                                     TLM_CFA_INTF_DELETE |\
                                     TLM_MPLS_DIFFSERV_TE_PARAMS)


/*Various Possible Message Related to "TLM_EV_CFG_MSG_IN_QUEUE"*/

#define TLM_OPER_STATUS_CHG_MSG            0
#define TLM_TE_COMP_LINK_ASSOC_CHG_MSG     1
#define TLM_TE_COMP_LINK_ASSOC_CREATE_MSG  2
#define TLM_DSTE_STATUS_MSG                3
#define TLM_MPLS_BW_THRSHOLD_FORCE_MSG     4
#define TLM_MPLS_TECLASSS_CHG_STATUS_MSG   5
#define TLM_MPLS_CHG_CLASS_TYPE_MSG        6
#define TLM_RSPV_GR_COMPLETE_MSG           7
#define TLM_MPLS_BW_THRSHOLD_MSG           8
#define TLM_MIN_TELINK_NAME_LEN            1
#define TLM_MAX_TELINK_NAME_LEN     TLM_MAX_NAME_LEN

#define  TLM_ONE               1
#define  TLM_TWO               2
#define  TLM_THREE             3
#define  TLM_FOUR              4
#define  TLM_FIVE              5
#define  TLM_SIX               6
#define  TLM_SEVEN             7
#define  TLM_NULL              0
#define  TLM_ONE_BYTE          8
#define TLM_MAX_DEC_DIGIT     21
#define  TLM_BYTE_GROUP        8
#define PRIORITY0              0
#define PRIORITY1              1
#define PRIORITY2              2
#define PRIORITY3              3
#define PRIORITY4              4
#define PRIORITY5              5
#define PRIORITY6              6
#define PRIORITY7              7


#define  TLM_BW_THRESHOLD_ZERO        0
#define  TLM_BW_THRESHOLD_ONE         1
#define  TLM_BW_THRESHOLD_TWO         2
#define  TLM_BW_THRESHOLD_THREE       3
#define  TLM_BW_THRESHOLD_FOUR        4
#define  TLM_BW_THRESHOLD_FIVE        5
#define  TLM_BW_THRESHOLD_SIX         6
#define  TLM_BW_THRESHOLD_SEVEN       7
#define  TLM_BW_THRESHOLD_EIGHT       8
#define  TLM_BW_THRESHOLD_NINE        9
#define  TLM_BW_THRESHOLD_TEN         10
#define  TLM_BW_THRESHOLD_ELEVEN      11
#define  TLM_BW_THRESHOLD_TWELVE      12
#define  TLM_BW_THRESHOLD_THIRTEEN    13
#define  TLM_BW_THRESHOLD_FOURTEEN    14
#define  TLM_BW_THRESHOLD_FIFTEEN     15
#define  TLM_BW_THRESHOLD_SIXTEEN     16
#define  TLM_BW_THRESHOLD_UP_ROW_STATUS    17
#define  TLM_BW_THRESHOLD_DOWN_ROW_STATUS  18


#define TLM_TE_LINK_ADDR_TYPE             1
#define TLM_TE_LINK_LOCAL_IPADDR          2
#define TLM_TE_LINK_REMOTE_IPADDR         3
#define TLM_TE_LINK_METRIC_OBJ            4
#define TLM_TE_LINK_MAX_RESV_BW           5
#define TLM_TE_LINK_PROTECT_TYPE          6
#define TLM_TE_LINK_WORK_PRIORITY         7
#define TLM_TE_LINK_RESOURCE_CLASS        8
#define TLM_TE_LINK_INCOMING_IFID         9
#define TLM_TE_LINK_OUTGOING_IFID         10
#define TLM_TE_LINK_ROW_STATUS            11
#define TLM_TE_LINK_STORAGE_TYPE          12

#define TLM_TE_LINK_DESC_SWITCH_CAP       13
#define TLM_TE_LINK_DESC_ENCODE_TYPE      14
#define TLM_TE_LINK_DESC_MIN_LSP_BW       15
#define TLM_TE_LINK_DESC_MAX_LSP_BW_PRI0  16
#define TLM_TE_LINK_DESC_MAX_LSP_BW_PRI1  17
#define TLM_TE_LINK_DESC_MAX_LSP_BW_PRI2  18
#define TLM_TE_LINK_DESC_MAX_LSP_BW_PRI3  19
#define TLM_TE_LINK_DESC_MAX_LSP_BW_PRI4  21
#define TLM_TE_LINK_DESC_MAX_LSP_BW_PRI5  22
#define TLM_TE_LINK_DESC_MAX_LSP_BW_PRI6  23
#define TLM_TE_LINK_DESC_MAX_LSP_BW_PRI7  24

#define TLM_TE_LINK_DESC_INTERFACF_MTU    25
#define TLM_TE_LINK_DESC_INDICATION       26
#define TLM_TE_LINK_DESC_ROW_STATUS       27
#define TLM_TE_LINK_DESC_STORAGE_TYPE     28

#define TLM_TE_LINK_SRLG_ROW_STATUS       29
#define TLM_TE_LINK_SRLG_STORAGE_TYPE     30

#define TLM_TE_LINK_BW_ROW_STATUS         31
#define TLM_TE_LINK_BW_STORAGE_TYPE       32

#define TLM_COMP_LINK_MAX_RESV_BW         33
#define TLM_COMP_LINK_PREF_PROTECT        34
#define TLM_COMP_LINK_CURR_PROTECT        35
#define TLM_COMP_LINK_ROW_STATUS          36
#define TLM_COMP_LINK_STORAGE_TYPE        37

#define TLM_COMP_LINK_DESC_SWITCH_CAP       38
#define TLM_COMP_LINK_DESC_ENCODE_TYPE      39
#define TLM_COMP_LINK_DESC_MIN_LSP_BW       40
#define TLM_COMP_LINK_DESC_MAX_LSP_BW_PRI0  41
#define TLM_COMP_LINK_DESC_MAX_LSP_BW_PRI1  42
#define TLM_COMP_LINK_DESC_MAX_LSP_BW_PRI2  43
#define TLM_COMP_LINK_DESC_MAX_LSP_BW_PRI3  44
#define TLM_COMP_LINK_DESC_MAX_LSP_BW_PRI4  45
#define TLM_COMP_LINK_DESC_MAX_LSP_BW_PRI5  46
#define TLM_COMP_LINK_DESC_MAX_LSP_BW_PRI6  47
#define TLM_COMP_LINK_DESC_MAX_LSP_BW_PRI7  48
#define TLM_COMP_LINK_DESC_INTF_MTU         49
#define TLM_COMP_LINK_DESC_INDICATION       50
#define TLM_COMP_LINK_DESC_ROW_STATUS       51
#define TLM_COMP_LINK_DESC_STORAGE_TYPE     52

#define TLM_COMP_LINK_BW_UNRESV       53
#define TLM_COMP_LINK_BW_ROW_STATUS   54
#define TLM_COMP_LINK_BW_STORAGE_TYPE 55

#define TLM_TE_LINK_TRC_OPTION   56

#define TLM_TE_LINK_NAME_OBJ     57
#define TLM_TE_LINK_REMOTE_RTRID 58
#define TLM_TE_LINK_MAX_BW       59
#define TLM_TE_LINK_TYPE         60
#define TLM_TE_LINK_INFO_TYPE    61
#define TLM_TE_LINK_IF_TYPE    62
#define TLM_TE_LINK_ADVERTISE  63

/* For RB Tree */
#define TLM_RB_LESSER -1
#define TLM_RB_GREATER 1
#define TLM_RB_EQUAL 0

#endif  /* _TLM_DEFN_H_ */
