/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstlmlw.h,v 1.3 2012/04/02 12:31:45 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsTeLinkTraceOption ARG_LIST((INT4 *));

INT1
nmhGetFsTeLinkModuleStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsTeLinkTraceOption ARG_LIST((INT4 ));

INT1
nmhSetFsTeLinkModuleStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsTeLinkTraceOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsTeLinkModuleStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsTeLinkTraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsTeLinkModuleStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsTeLinkTable. */
INT1
nmhValidateIndexInstanceFsTeLinkTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsTeLinkTable  */

INT1
nmhGetFirstIndexFsTeLinkTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsTeLinkTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsTeLinkName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsTeLinkRemoteRtrId ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsTeLinkMaximumBandwidth ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsTeLinkType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsTeLinkInfoType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsTeLinkIfType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsTeLinkIsAdvertise ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsTeLinkName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsTeLinkRemoteRtrId ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsTeLinkType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsTeLinkInfoType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsTeLinkIfType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsTeLinkIsAdvertise ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsTeLinkName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsTeLinkRemoteRtrId ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsTeLinkType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsTeLinkInfoType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsTeLinkIfType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsTeLinkIsAdvertise ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsTeLinkTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsTeLinkBwThresholdTable. */
INT1
nmhValidateIndexInstanceFsTeLinkBwThresholdTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsTeLinkBwThresholdTable  */

INT1
nmhGetFirstIndexFsTeLinkBwThresholdTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsTeLinkBwThresholdTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsTeLinkBwThreshold0 ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTeLinkBwThreshold1 ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTeLinkBwThreshold2 ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTeLinkBwThreshold3 ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTeLinkBwThreshold4 ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTeLinkBwThreshold5 ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTeLinkBwThreshold6 ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTeLinkBwThreshold7 ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTeLinkBwThreshold8 ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTeLinkBwThreshold9 ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTeLinkBwThreshold10 ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTeLinkBwThreshold11 ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTeLinkBwThreshold12 ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTeLinkBwThreshold13 ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTeLinkBwThreshold14 ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTeLinkBwThreshold15 ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTeLinkBwThresholdRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsTeLinkBwThreshold0 ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTeLinkBwThreshold1 ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTeLinkBwThreshold2 ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTeLinkBwThreshold3 ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTeLinkBwThreshold4 ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTeLinkBwThreshold5 ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTeLinkBwThreshold6 ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTeLinkBwThreshold7 ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTeLinkBwThreshold8 ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTeLinkBwThreshold9 ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTeLinkBwThreshold10 ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTeLinkBwThreshold11 ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTeLinkBwThreshold12 ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTeLinkBwThreshold13 ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTeLinkBwThreshold14 ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTeLinkBwThreshold15 ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTeLinkBwThresholdRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsTeLinkBwThreshold0 ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTeLinkBwThreshold1 ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTeLinkBwThreshold2 ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTeLinkBwThreshold3 ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTeLinkBwThreshold4 ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTeLinkBwThreshold5 ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTeLinkBwThreshold6 ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTeLinkBwThreshold7 ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTeLinkBwThreshold8 ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTeLinkBwThreshold9 ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTeLinkBwThreshold10 ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTeLinkBwThreshold11 ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTeLinkBwThreshold12 ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTeLinkBwThreshold13 ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTeLinkBwThreshold14 ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTeLinkBwThreshold15 ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTeLinkBwThresholdRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsTeLinkBwThresholdTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsTeLinkBwThresholdForceOption ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsTeLinkBwThresholdForceOption ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsTeLinkBwThresholdForceOption ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsTeLinkBwThresholdForceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
