/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: tlmmacs.h,v 1.10 2014/07/09 13:23:45 siva Exp $
*
* Description: This file contains definitions of macros used.
*********************************************************************/

#ifndef _TLM_MACS_H_
#define _TLM_MACS_H_

/**********************************************
 * Getting the appropriate pointer from SLL.
 **********************************************/
#define TLM_OFFSET(x,y)  ((FS_ULONG)(&(((x *)0)->y)))

#define GET_TE_IF_PTR_FROM_SORT_LST(x)  ((tTlmTeLink *)(((FS_ULONG)x) \
                             - TLM_OFFSET(tTlmTeLink, NextSortLinkNode)))

#define GET_TE_TELINK_NAME_NODE(x) ((tTlmTeLink *)(((FS_ULONG)x) \
                               - TLM_OFFSET(tTlmTeLink, NextTeLinkNameNode)))                       

#define GET_UNBUNDLE_TE_LINK_LIST(x) ((tTlmTeLink *)(((FS_ULONG)x) \
                               - TLM_OFFSET(tTlmTeLink, NextSortUnbundledTeLinkNode)))

#define GET_TE_DESCR_PTR_FROM_SORT_LST(x) ((tTlmTeIfDescr *)(((FS_ULONG)x) \
                             - TLM_OFFSET(tTlmTeIfDescr, NextIfDescrNode)))
#define GET_TE_SRLG_PTR_FROM_SORT_LST(x) ((tTlmTeLinkSrlg *)(((FS_ULONG)x) \
                             - TLM_OFFSET(tTlmTeLinkSrlg, NextLinkSrlgNode)))


#define GET_COMPONENT_IF_PTR_FROM_SORT_LST(x)  ((tTlmComponentLink *)(((FS_ULONG)x) \
                             - TLM_OFFSET(tTlmComponentLink, NextSortComponentNode)))

#define GET_COMPONENT_IF_PTR_FROM_SORT_TE_LST(x)  ((tTlmComponentLink *)(((FS_ULONG)x) \
                             - TLM_OFFSET(tTlmComponentLink, NextSortComponentTeListNode)))
#define GET_TE_LINK_COMP_NODE_PTR_FROM_SORT_LST(x)  ((tTlmComponentLink *)(((FS_ULONG)x) \

#define GET_COMP_LINK_FROM_TE_LINK_COMP_LIST_NODE(x) \
                ((tTlmComponentLink *)(((tTlmTeLinkCompList *)((FS_ULONG)x))->pTlmComponentLink))

#define GET_COMPONENT_DESCR_PTR_FROM_SORT_LST(x) ((tTlmComponentIfDescr *)(((FS_ULONG)x) \
                                         - TLM_OFFSET(tTlmComponentIfDescr, NextIfComponentDescrNode)))



#define TLM_SEM_ID                          gTlmGlobalInfo.SemId
#define TLM_CFG_QID                         gTlmGlobalInfo.CfgQId
#define TLM_TASK_ID                         gTlmGlobalInfo.TaskId
#define TLM_CLASS_TYPE_SUPPORTED            gTlmGlobalInfo.u1ClassType
#define TLM_TE_CLASS_INFO(u1TeClass)        gTlmGlobalInfo.aTlmTeClassMap[u1TeClass]
#define TLM_BWCONSTRAINT_UP(x)              gTlmGlobalInfo.aTlmBWConstraintUp[x]
#define TLM_BWCONSTRAINT_DOWN(x)            gTlmGlobalInfo.aTlmBWConstraintDown[x]
#define TLM_BCM_SUPPORTED                   gTlmGlobalInfo.u1BCMSupported
#define TLM_DSTE_STATUS                     gTlmGlobalInfo.u1DSTEStatus


#define TLM_MEMCPY MEMCPY
#define TLM_STRLEN STRLEN
#define TLM_STRCMP STRCMP

#define TLM_STRNCMP             STRNCMP
#define TLM_STRCPY  STRCPY

#define TLM_LINK_TYPE(x) gTlmCliArgs[x].i4TeLinkType
#define TLM_TE_LINK_INDEX(x) gTlmCliArgs[x].i4TeIfIndex
#define TLM_BUNDLE_TE_LINK_INDEX(x) gTlmCliArgs[x].i4TeBundleIndex
#define TLM_COMP_LINK_INDEX(x) gTlmCliArgs[x].i4TeCompIndex

#define TLM_CONVERT_BPS_TO_KBPS(f4BpsVal) \
                   f4BpsVal = TLM_FLOAT_DIV(f4BpsVal);

#define TLM_KILOBYTE    1024.0

#define TLM_FLOAT_DIV(f4KbpsVal)  TlmFloatDivide (f4KbpsVal,(FLT4)TLM_KILOBYTE)

#define TLM_INTEGER_TO_OCTETSTRING(u4Index,pOctetString) {\
    UINT4 u4LocalIndex = OSIX_HTONL(u4Index);\
    UINT1 u1OctetStringLen = sizeof (UINT4);\
    MEMSET (pOctetString->pu1_OctetList, 0, u1OctetStringLen); \
    MEMCPY(pOctetString->pu1_OctetList,(UINT1*)&u4LocalIndex,\
           u1OctetStringLen);\
    pOctetString->i4_Length=u1OctetStringLen;}

#define TLM_OCTETSTRING_TO_FLOAT(pOctetString) TlmUtilOctetStringToFloat (pOctetString)

#define TLM_OCTETSTRING_TO_INTEGER(pOctetString,u4Index) { \
    u4Index = 0;\
    if(pOctetString->i4_Length > 0 && pOctetString->i4_Length <= 4){\
        MEMCPY((UINT1 *)&u4Index,pOctetString->pu1_OctetList,\
               pOctetString->i4_Length);\
        u4Index = OSIX_NTOHL(u4Index);}}

#define FLOAT_TO_OCTETSTRING(f4Value,pOctetString) {\
        UINT1 *pu1PtrTof4Value = NULL;\
        FLT4 f4Val = (OSIX_HTONF((FLT4)f4Value));\
        UINT4 u4Size = sizeof (FLT4);\
        pu1PtrTof4Value = (UINT1 *)((VOID *)(&(f4Val)));\
        MEMCPY(pOctetString->pu1_OctetList,pu1PtrTof4Value,\
               u4Size);\
        pOctetString->i4_Length = (INT4)u4Size;}

#define TLM_CHECK_ENDIANESS(u1ChkEnd) \
   UINT4 u4Num = TLM_ONE;             \
   CHR1 *c1Val = (CHR1 *)&u4Num;      \
   u1ChkEnd = c1Val[0];

#define TLM_CONVERT_NTOHL(x) (UINT4)(((x & 0xFF000000)>>24) | \
                              ((x & 0x00FF0000)>>8)  | \
                              ((x & 0x0000FF00)<<8 ) | \
                              ((x & 0x000000FF)<<24)   \
                             )

/*Converting bits per second to bytes per second*/
#define TLM_CONVERT_BPS_TO_BYTES_PER_SEC(x)\
{\
    x = (x/8);\
}

/*Converting Megabits per second to bytes per second*/
#define TLM_CONVERT_MBPS_TO_BYTES_PER_SEC(x)\
{\
    x = ((x*1000*1000)/8);\
}

/* Following macro converts kilo bits per second
 * to bytes per second */
#define TLM_CONVERT_KBPS_TO_BYTES_PER_SEC(x)\
{\
   x = (x*1000)/8;\
}

/* Following macro converts bytes per second to
 * kilo bits per second */
#define TLM_CONVERT_BYTES_PER_SEC_TO_KBPS(x)\
{\
   x = (x/1000)*8;\
}

/*
 * no. of hash buckets =
 * total no. of objects stored / no. of objects per hash bucket;
 * no. of objects per hash bucket is chosen to be 16
 * it is subject to change
 */

#define  IF_HASH_TABLE_SIZE       ((MAX_TLM_LINKS / BUCKET_SIZE) + 1)

#define  COMPONENT_IF_HASH_TABLE_SIZE       ((MAX_TLM_COMPONENT_LINKS / BUCKET_SIZE) + 1)

#define IF_HASH_FN(u4IfIndex) TlmUtilHashGetValue (IF_HASH_TABLE_SIZE, (UINT1 *)(&(u4IfIndex)), sizeof(UINT4))

#define COMPONENT_IF_HASH_FN(u4IfIndex) TlmUtilHashGetValue(COMPONENT_IF_HASH_TABLE_SIZE, \
                                    (UINT1 *)(&(u4IfIndex)), sizeof(UINT4))

#define IF_HASH_NAME_FN(teLinkName,teLinkNameLen) TlmUtilHashGetNameValue(teLinkName,teLinkNameLen)

#define TLM_CUR_TRACE_OPTION gTlmGlobalInfo.i4TraceOption
#define TLM_MODULE_STATUS gTlmGlobalInfo.i4ModuleStatus

#define TLM_STRLEN       STRLEN

#define LTD_B_CAST_ADDR 0xffffffff
#define LOOP_BACK_ADDR_MASK 0xff000000 /* Loop back Address range 127.x.x.x*/
#define LOOP_BACK_ADDRESSES 0x7f000000 /* Loop back Address range 127.x.x.x*/

#define IS_CLASS_A_ADDR(u4Addr) \
                              ((u4Addr & 0x80000000) == 0)

#define IS_CLASS_B_ADDR(u4Addr) \
                              ((u4Addr & 0xc0000000) == 0x80000000)

#define IS_CLASS_C_ADDR(u4Addr) \
                              ((u4Addr & 0xe0000000) == 0xc0000000)



#define IS_VALID_TE_PROTECTION_TYPE(x) ((x >= 0x01) && (x <= 0x06))

#define IS_VALID_TE_PROTECTION(x)          ((x == TLM_TE_LINK_PROTECTION_PRIMARY)    || \
                              (x == TLM_TE_LINK_PROTECTION_SECONDARY))
 
#define IS_VALID_TE_ADDRESS_TYPE(x)        ((x == TLM_ADDRESSTYPE_UNKNOWN)       || \
                              (x == TLM_ADDRESSTYPE_NUMBERED_IPV4)) 

#define IS_VALID_ADMIN_STATUS(x)           ((x == TLM_ENABLED)  ||\
                                            (x == TLM_DISABLED))

#define IS_VALID_DSTE_STATUS(x)           ((x == TLM_DSTE_ENABLED)  ||\
                                           (x == TLM_DSTE_DISABLED))

#define IS_VALID_CLASS_TYPE(x)  (x <= 0x07)

#define IS_VALID_TE_CLASS                   IS_VALID_CLASS_TYPE
#define IS_VALID_PRIORITY                   IS_VALID_CLASS_TYPE
#define IS_VALID_TE_INCOMING_ID(x)     (x <= 0x7fffffff && x > 0)
#define IS_VALID_TE_OUTGOING_ID(x)     (x <= 0x7fffffff && x > 0)
#define IS_VALID_TE_RESOURCE_CLASS(x)     (x <= 0xFFFFFFFF)
#define IS_VALID_TE_BW(x)                 ((x >= 0) && (x <= 0xffffffff))/*TO Check*/ 

/* Currently we are going to support only OSPF_TE_PSC_1 Switching Capability */            
#define IS_VALID_TE_SWITCH_CAP(x)     ((x == OSPF_TE_PSC_1) || \
                                       (x == OSPF_TE_PSC_2) || \
                                       (x == OSPF_TE_PSC_3) || \
                                       (x == OSPF_TE_PSC_4) || \
                                       (x == OSPF_TE_L2SC)  || \
                                       (x == OSPF_TE_TDM)   || \
                                       (x == OSPF_TE_LSC)   || \
                                       (x == OSPF_TE_FSC))  

/* Currently we are going to support only TLM_TE_ENCODING_TYPE_PACKET Encoding Type */            
#define IS_VALID_TE_ENCODING_TYPE(x)     (x == TLM_TE_ENCODING_TYPE_PACKET) 

#define IS_VALID_TE_MINLSPBW(x)     ((x >= 0) && (x <= 0xffffffff) && (x > 1.0))
#define IS_POSITIVE_VALUE(x)   (x >= 0) 
#define IS_POSITIVE_BW(x)      (x >= 0.0)
#define IS_VALID_TE_MTU(x)     (x <= 0xffff)

#define IS_VALID_TE_INDICATION(x)     ((x == TLM_TE_LINK_INDICATION_STANDARD) || \
                                       (x == TLM_TE_LINK_INDICATION_ARBITRARY))

#define IS_VALID_TE_BW_PRIORITY(x)     (x <= 7)

#define IS_VALID_FLOAT_OCTET_STRING(pOctetString) \
                (pOctetString->i4_Length == sizeof (FLT4))

#define IS_UNIQUE_VALUE(param, value,TeIfIndex, flag)                 \
{                                                                     \
    tTlmTeLink         *pTeLink = NULL;                               \
    tTMO_SLL_NODE      *pListNode = NULL;                              \
    flag = TLM_TRUE;                                                  \
    TMO_SLL_Scan (&(gTlmGlobalInfo.sortTeIfLst), pListNode, tTMO_SLL_NODE *)\
    {                                                                 \
        pTeLink = GET_TE_IF_PTR_FROM_SORT_LST (pListNode);            \
        if (pTeLink->param == value)                                  \
        {                                                             \
           if (pTeLink->u4IfIndex != (UINT4)TeIfIndex)                \
           {                                                          \
            flag = TLM_FALSE;                                         \
           }                                                          \
            break;                                                    \
        }                                                             \
   }                                                                  \
}


#define GET_TE_LINK_TE_LIST_NODE_PTR_FROM_SORT_LST(x) ( (tTlmTeLink *) (((FS_ULONG)x) -  \
                                               TLM_OFFSET(tTlmTeLink, NextSortUnbundledTeLinkNode)))


/***********************************************
* POOL IDs for TLM *
***********************************************/
#define TLM_TE_LINK_POOL_ID                 TLMMemPoolIds [MAX_TLM_LINKS_SIZING_ID]
#define TLM_TE_LINK_IF_DESCR_POOL_ID        TLMMemPoolIds[MAX_TLM_TE_IF_DESCR_SIZING_ID]
#define TLM_TE_LINK_SRLG_POOL_ID            TLMMemPoolIds [MAX_TLM_TE_SRLG_SIZING_ID]
#define TLM_COMP_LINK_POOL_ID               TLMMemPoolIds [MAX_TLM_COMPONENT_LINKS_SIZING_ID]
#define TLM_COMP_LINK_IF_DESCR_POOL_ID      TLMMemPoolIds [MAX_TLM_COMP_IF_DESCR_SIZING_ID]
#define TLM_TE_LINK_COMP_LINK_LIST_POOL_ID  TLMMemPoolIds [MAX_TLM_TE_COMP_LIST_SIZING_ID]
#define TLM_REG_MODULE_POOL_ID              TLMMemPoolIds [MAX_TLM_APPS_SIZING_ID]
#define TLM_MSG_QUEUE_POOL_ID               TLMMemPoolIds [MAX_TLM_QUEUE_SIZE_SIZING_ID]


/* Macros for memory allocation using cru buffers */
#define TELINK_ALLOC(pu1Block) \
        (pu1Block = (tTlmTeLink *) MemAllocMemBlk(TLM_TE_LINK_POOL_ID))
#define TEIFDESCR_ALLOC(pu1Block) \
        (pu1Block = (tTlmTeIfDescr *) MemAllocMemBlk(TLM_TE_LINK_IF_DESCR_POOL_ID))
#define TELINKSRLG_ALLOC(pu1Block) \
        (pu1Block = (tTlmTeLinkSrlg *) MemAllocMemBlk (TLM_TE_LINK_SRLG_POOL_ID))

#define COMPONENT_LINK_ALLOC(pu1Block) \
        (pu1Block = (tTlmComponentLink *) MemAllocMemBlk (TLM_COMP_LINK_POOL_ID))

#define COMPONENT_IFDESCR_ALLOC(pu1Block) \
        (pu1Block = (tTlmComponentIfDescr *) MemAllocMemBlk (TLM_COMP_LINK_IF_DESCR_POOL_ID))

#define TLM_CFG_MSGQ_ALLOC(pu1Block) \
        (pu1Block = (tTlmCfgMsg *) MemAllocMemBlk (TLM_MSG_QUEUE_POOL_ID))

#define TLM_REG_MOD_ALLOC(pu1Block) \
        (pu1Block = (tTlmCfgMsg *) MemAllocMemBlk (TLM_REG_MODULE_POOL_ID))

/* Macros for memory deallocation using cru buffers */
#define  TELINK_FREE(pu1Block) MemReleaseMemBlock(TLM_TE_LINK_POOL_ID,(UINT1 *)pu1Block)
#define  TEIFDESCR_FREE(pu1Block) MemReleaseMemBlock(TLM_TE_LINK_IF_DESCR_POOL_ID,(UINT1 *)pu1Block)
#define  TESRLG_FREE(pu1Block) MemReleaseMemBlock(TLM_TE_LINK_SRLG_POOL_ID,(UINT1 *)pu1Block)
#define  TELINK_COMP_LIST_NODE_FREE(pu1Block) MemReleaseMemBlock(TLM_TE_LINK_COMP_LINK_LIST_POOL_ID,(UINT1 *)pu1Block)
#define  COMPONENT_LINK_FREE(pu1Block) MemReleaseMemBlock(TLM_COMP_LINK_POOL_ID,(UINT1 *)pu1Block)
#define  COMPONENT_IFDESCR_FREE(pu1Block) MemReleaseMemBlock(TLM_COMP_LINK_IF_DESCR_POOL_ID, (UINT1 *)pu1Block)

#endif
