/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: tlmsz.h,v 1.6 2012/04/05 13:20:12 siva Exp $
*
* Description: This file contains sizing related MACROS
*
*******************************************************************/

enum {
    MAX_TLM_APPS_SIZING_ID,
    MAX_TLM_COMP_IF_DESCR_SIZING_ID,
    MAX_TLM_COMPONENT_LINKS_SIZING_ID,
    MAX_TLM_LINKS_SIZING_ID,
    MAX_TLM_QUEUE_SIZE_SIZING_ID,
    MAX_TLM_TE_IF_DESCR_SIZING_ID,
    MAX_TLM_TE_SRLG_SIZING_ID,
    TLM_MAX_SIZING_ID
};


#ifdef  _TLMSZ_C
tMemPoolId TLMMemPoolIds[ TLM_MAX_SIZING_ID];
INT4  TlmSizingMemCreateMemPools(VOID);
VOID  TlmSizingMemDeleteMemPools(VOID);
#else  /*  _TLMSZ_C  */
extern tMemPoolId TLMMemPoolIds[ ];
extern INT4  TlmSizingMemCreateMemPools(VOID);
extern VOID  TlmSizingMemDeleteMemPools(VOID);
#endif /*  _TLMSZ_C  */


#ifdef  _TLMSZ_C
tFsModSizingParams FsTLMSizingParams [] = {
{ "tTlmRegParams", "MAX_TLM_APPS", sizeof(tTlmRegParams),MAX_TLM_APPS, MAX_TLM_APPS,0 },
{ "tTlmComponentIfDescr", "MAX_TLM_COMP_IF_DESCR", sizeof(tTlmComponentIfDescr),MAX_TLM_COMP_IF_DESCR, MAX_TLM_COMP_IF_DESCR,0 },
{ "tTlmComponentLink", "MAX_TLM_COMPONENT_LINKS", sizeof(tTlmComponentLink),MAX_TLM_COMPONENT_LINKS, MAX_TLM_COMPONENT_LINKS,0 },
{ "tTlmTeLink", "MAX_TLM_LINKS", sizeof(tTlmTeLink),MAX_TLM_LINKS, MAX_TLM_LINKS,0 },
{ "tTlmCfgMsg", "MAX_TLM_QUEUE_SIZE", sizeof(tTlmCfgMsg),MAX_TLM_QUEUE_SIZE, MAX_TLM_QUEUE_SIZE,0 },
{ "tTlmTeIfDescr", "MAX_TLM_TE_IF_DESCR", sizeof(tTlmTeIfDescr),MAX_TLM_TE_IF_DESCR, MAX_TLM_TE_IF_DESCR,0 },
{ "tTlmTeLinkSrlg", "MAX_TLM_TE_SRLG", sizeof(tTlmTeLinkSrlg),MAX_TLM_TE_SRLG, MAX_TLM_TE_SRLG,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _TLMSZ_C  */
extern tFsModSizingParams FsTLMSizingParams [];
#endif /*  _TLMSZ_C  */


