/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: tlmtdfs.h,v 1.10 2012/12/11 15:11:07 siva Exp $
*
* Description: This file contains definition of the structures used.
*********************************************************************/

#ifndef _TLM_TDFS_H_
#define _TLM_TDFS_H_

typedef UINT1 tIPADDR[TLM_MAX_IP_LEN];
typedef tIPADDR tRouterId;

typedef struct TlmGlobalInfo{
    tMemPoolId       TlmRegMemPoolId;
    tMemPoolId       TlmCfgMsgQPoolId;
    tOsixTaskId      TaskId;
    tOsixSemId       SemId;
    tOsixQId         CfgQId;

    /* Hash Table for storing TE Link attributes (tTlmTeLink) */
    tTMO_HASH_TABLE  *pTeIfHashTable;  
    
    /* Sorted Interface List for storing TE Link attributes (tTlmTeLink) */
    tTMO_SLL         sortTeIfLst;

    /* Hash Table for storing Component TE Link attributes tTlmComponentLink */
    tTMO_HASH_TABLE  *pComponentIfHashTable;

    /* Sorted Interface List for storing Component TE Link attributes
     * tTlmComponentLink */
    tTMO_SLL         sortComponentIfLst;
    
    /* Hash Table for storing TE Link Name */
    tTMO_HASH_TABLE  *pTeLinkNameIfHashTable;
    
    /* RB Tree for storing TE Link Informations */
    tRBTree          TeLinkIDBasedTree;

    /* Holds information about the external Modules registed with TLM */
    tTlmRegParams    *apAppRegParams[TLM_MAX_APPS];

    /* Holds the information about the tTlmTeClassMap */
    tTlmTeClassMap      aTlmTeClassMap[TLM_MAX_TE_CLASS];

    tTlmClassType       aTlmClassType[TLM_MAX_TE_CLASS_TYPE];

    /* Trace level for TLM */
    INT4             i4TraceOption;

    /* Module Status of the TLM (Enable/Disable) */
    INT4             i4ModuleStatus;

    /* Resource class type */
    UINT1            u1ClassType;

    /* Bandwidth Constraint Model Supported */
    UINT1            u1BCMSupported;

    /* Diff Serv Status */
    UINT1            u1DSTEStatus;

    /* Enable Force BW Threshold */
    UINT1            u1BwForceThresholdEnable;

}tTlmGlobalInfo;

typedef struct _TlmCliArgs
{
    INT4   i4TeIfIndex;
    INT4   i4TeLinkType;
    INT4   i4TeBundleIndex;
    INT4   i4TeCompIndex;
}tTlmCliArgs;


typedef struct _TlmTeDescrUnResBw {
    /* Unreserved bandwidth */
    tBandWidth         unResBw;

    /* Reserved bandwidth*/ 
    tBandWidth         resBw;

    /* Row status */
    INT4               i4RowStatus;

    /* Storage Type value */
    INT4               i4StorageType;
} tTlmTeDescrUnResBw;

typedef struct _TlmComponentDescrUnResBw {
    /* Unreserved bandwidth */
    tBandWidth         unResBw;

    /* Reserved bandwidth*/ 
    tBandWidth         resBw;

    /* Row status */
    INT4               i4RowStatus;

    /* Storage Type value */
    INT4               i4StorageType;
} tTlmComponentDescrUnResBw;


typedef struct _TlmSrlgNode {
    /* Points to next Srlg Node*/
    tTMO_SLL_NODE     NextLinkSrlgNode;

    /* Srlg number */
    UINT4             u4SrlgNo;

    /* Row status */
    INT4              i4RowStatus;

    /* Storage Type value */
    INT4              i4StorageType;
} tTlmTeLinkSrlg;

typedef  struct _TlmTeIfDescr {
    /* Points to next Interface node */
    tTMO_SLL_NODE     NextIfDescrNode;

    /* Descriptor Min LSP bandwidth */
    tBandWidth        minLSPBw;

    /* Descriptor Max LSP bandwidth at Priority */
    tBandWidth        aMaxLSPBwPrio[TLM_MAX_PRIORITY_LVL];

    /* Aggregated bandwidth for each TE class */                                                      
    tBandWidth        aAggBw[TLM_MAX_TE_CLASS]; 

    /* Descriptor identifier */
    UINT4             u4DescrId;

    /* Descriptor entry Row Status*/
    INT4              i4RowStatus;  

    /* Descriptor Switching capability */
    INT4              i4SwitchingCap; 
    
    /* Descriptor Encoding Type */
    INT4              i4EncodingType; 

    /* Descriptor Indication - used only for TDM */
    INT4              i4Indication;

    /* Storage Type value */
    INT4              i4StorageType;

    /* Descriptor MTU value */
    UINT2             u2MTU;

    /* Padding */
    UINT1                    au1Rsvd[2];
} tTlmTeIfDescr;


typedef struct _TlmTeLink {
    /*Points to next TE-Link Interface node*/
    tTMO_HASH_NODE           NextLinkNode;

    /* Points to next sorted TE-Link Interface node*/
    tTMO_SLL_NODE            NextSortLinkNode;

    /* Points to next sorted unbundled TE-Links of the bundled TE-Link */
    tTMO_SLL_NODE            NextSortUnbundledTeLinkNode;

    /* Points to next name based hash node */
    tTMO_HASH_NODE           NextTeLinkNameNode;

    /* Back Pointer to the bundled TE-link. This is useful when we want to
     * retrieve Bundled TE Link from an Unbundled TE Link*/
    struct _TlmTeLink        *pBundledTeLink;

    /* Unreserved BW */
    tTlmTeDescrUnResBw       aUnResBw[TLM_MAX_TE_CLASS];

    /* Available BW of TE-link*/
    tBandWidth               availBw;

    /* Sorted List of unbundled TE-links of the undled TE-Link */
    tTMO_SLL                 sortUnbundleTeList;

    /* List of interface descriptors*/
    tTMO_SLL                 ifDescrList;

    /* List of SRLG */
    tTMO_SLL                 srlgList;

    /* List of Component Links*/
    tTMO_SLL                 ComponentList;
  

    /* RB Node to add in TeLinkInfoTree */
    tRBNodeEmbd              TeLinkIDBasedNode;

    /* Local IP address of the TE Link interface */
    tIpAddr                  LocalIpAddr;
  
    /* Remote IP address of the TE Link interface */
    tIpAddr                  RemoteIpAddr;
   
    /* Maximum Reservable Bandwidth*/
    tBandWidth               maxResBw;

    /* Maximum Bandwidth           */
    tBandWidth               maxBw; 

    /* Interface Index of the TE Link */
    INT4                     i4IfIndex; 

    /* Associated control channel If index */
    INT4                     i4CtrlMplsIfIndex;

    /* Remote Router address of the TE Link interface */
    UINT4                    u4RemoteRtrId;

    /* Working priority value of the TE Link */
    UINT4                    u4WorkingPriority;

    /* TE Metric value of the TE Link */
    UINT4                    u4TeMetric;

    /*  Administrative Group of the TE Link */
    UINT4                    u4RsrcClassColor;

    /* Local Identifier value in case of un-numbered interface */
    UINT4                    u4LocalIdentifier;

    /* Remote Identifier value in case of un-numbered interface */
    UINT4                    u4RemoteIdentifier;

   /* IfIndex of the MPLS interface stacked over this TE Link */
    UINT4                    u4MplsIfIndex;

    /* RowStatus of TE link entry */
    INT4                     i4RowStatus;

    /*Previous OperStatus of TE Link entry */
    INT4                     i4PrevOperStatus;
    /* Link Type of the TE link : Value 0 represents Unbundle 
     * and 1 represents bundle link */
    INT4                     i4TeLinkType;
                                             
    /* Represents the Address Type of this TE Link. Currently only IPV4 or
     * un-numbered are supported */
    INT4                     i4AddressType;
    
    /* AdminStatus of TE link */
    INT4                     i4TeAdminStatus;

    /* Protectype Provided by the TE link */
    INT4                     i4ProtectionType;

    /* Storage Type value */
    INT4                     i4StorageType;

    /*TE Link interface type 1.Point to point 2.Multiaccess*/
    INT4                     i4TeLinkIfType;
    
    /* If set to True, the TE Link will be advertised in IGP domain.
     * This variable corresponds to fsTeLinkIsAdvertise object of 
     * fstlm MIB */
    INT4                     i4IsTeLinkAdvertise;

    /*Last threshold Up Index*/
    INT4                     i4LastThresholdUpIndex;
    /*Last threshold Up Index*/
    INT4                     i4LastThresholdDownIndex;

    /*Row Status for BW Threshold Up*/
    INT1                     i1ThresholdUpRowStatus; 

    /*Row Status for BW Threshold Down*/
    INT1                     i1ThresholdDownRowStatus;

    /* Local IP address length */
    UINT1                   u1LocalIpLen;

    /* Remote IP address length */
    UINT1                   u1RemoteIpLen;

    /* Name of the TE-Link */
    UINT1                    au1TeLinkName[TLM_MAX_TELINK_NAME_LEN];

    /* Incremental Threshold values */
    UINT1                    aTlmBWThresholdUp[TLM_MAX_BW_THRESHOLD_SUPPORTED];

    /* Decremental Threshold values */
    UINT1                    aTlmBWThresholdDown[TLM_MAX_BW_THRESHOLD_SUPPORTED];

    /* Information type of the TE-link (Data channel or Control channel)*/
    UINT1                    u1InfoType;
   
    /* OperStatus of the TE-Link */
    UINT1                    u1OperStatus;

    /* TE Link Name Length */
    UINT1                    u1TeLinkNameLen;

    /* Added for padding */
    UINT1                    au1Rsvd[1];
} tTlmTeLink;


typedef struct _TlmComponentLink {
    /* Points to next Component Link Interface Node */

    tTMO_HASH_NODE               NextComponentNode;

    /* Points to next Sorted Component Link Interface Node */

    tTMO_SLL_NODE                NextSortComponentNode;
    /*Point to next in Componentlist in TeLink */
    tTMO_SLL_NODE                NextSortComponentTeListNode;
    /* Back Pointer to TeLink with which this component is associated */
    tTlmTeLink                  *pTeLink; 

    /* UnReserved Bandwidth at TE-CLASS */
    tTlmComponentDescrUnResBw    aUnResBw[TLM_MAX_TE_CLASS];

    /* Available BW of Component-link*/
    tBandWidth                   availBw;

    tBandWidth                   aBC[TLM_MAX_TE_CLASS_TYPE];

    /* List of Interace Descriptors */
    tTMO_SLL                     ifDescrList;

    /* Max Reservable Bandwidth */
    tBandWidth                   maxResBw;

    /* Interface Index of the Component Link */
    INT4                        i4IfIndex;

    /* Preferred Link Protection of the Component Link */
    INT4                         i4LinkPrefProtection;

    /* Current Protection of the Component Link */
    INT4                         i4LinkCurrentProtection;

     /* Row Status */
    INT4                         i4RowStatus;

    /* Storage Type value */
    INT4                         i4StorageType; 

    /* OperStatus value */
    UINT1                        u1OperStatus;

    /* Padding */
    UINT1                        au1Rsvd[3]; 
} tTlmComponentLink;
 
typedef  struct _TlmComponentIfDescr {
    /* Points to next Component Interface Descriptor Node */
    tTMO_SLL_NODE     NextIfComponentDescrNode;

    /* Minimum LSP bandwidth */
    tBandWidth        minLSPBw;

    /* Maximum LSP Bandwidth at Priority */
    tBandWidth        aMaxLSPBwPrio[TLM_MAX_PRIORITY_LVL];

    /* Descriptor Id of the Interface Descriptor */
    UINT4             u4DescrId;

    /* Descriptor entry Row Status*/
    INT4              i4RowStatus;

    /* Descriptor Switching capability */
    INT4              i4SwitchingCap;

    /* Descriptor Encoding Type */
    INT4              i4EncodingType;

     /* Storage Type value */
    INT4              i4StorageType;

    /* Descriptor Indication - used only for TDM */
    INT4              i4Indication;

    /* MTU value  */
    UINT2             u2MTU;

    /* Padding */
    UINT1            au1Rsvd[2]; 
} tTlmComponentIfDescr;


typedef struct TlmRtInfo
{

   /* Interface Index of the port*/
   UINT4            u4HighIfIndex;

   /* Interface Index of the port*/
   INT4            i4IfIndex; 

   /* Message type specifying Creation/deletion/updation of PDU received */
   UINT4            u4MsgType; 

   /*Module Id*/
   UINT4            u4ModId;
}tTlmRtInfo;

typedef struct TlmCfaInfo
{
   /* Interface Index of the port*/
   INT4            i4IfIndex; 
   /* Interface Type*/
   UINT1            u1IfType;
   /*Interface Status*/
   UINT1            u1IfStatus;

   /* Bridged Iface Status */
   UINT1            u1BridgedIface;

   /* Padding */
   UINT1            u1Rsvd;
}tTlmCfaInfo;

typedef struct TlmCfgMsg
{

   UINT4            u4MsgType;   
   union {
       tTlmRtInfo          TlmRtInfo;
       tTlmCfaInfo         TlmCfaInfo;
       tTlmDiffservInfo    TlmDiffservInfo;
   }u;
}tTlmCfgMsg;

#endif  /* _TLM_TDFS_H_ */
