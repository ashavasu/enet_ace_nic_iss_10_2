/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdtlmlw.h,v 1.2 2011/12/09 06:15:59 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for TeLinkTable. */
INT1
nmhValidateIndexInstanceTeLinkTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for TeLinkTable  */

INT1
nmhGetFirstIndexTeLinkTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexTeLinkTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetTeLinkAddressType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetTeLinkLocalIpAddr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetTeLinkRemoteIpAddr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetTeLinkMetric ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetTeLinkMaximumReservableBandwidth ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetTeLinkProtectionType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetTeLinkWorkingPriority ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetTeLinkResourceClass ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetTeLinkIncomingIfId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetTeLinkOutgoingIfId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetTeLinkRowStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetTeLinkStorageType ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetTeLinkAddressType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetTeLinkLocalIpAddr ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetTeLinkRemoteIpAddr ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetTeLinkMetric ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetTeLinkProtectionType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetTeLinkWorkingPriority ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetTeLinkResourceClass ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetTeLinkIncomingIfId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetTeLinkOutgoingIfId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetTeLinkRowStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetTeLinkStorageType ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2TeLinkAddressType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2TeLinkLocalIpAddr ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2TeLinkRemoteIpAddr ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2TeLinkMetric ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2TeLinkProtectionType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2TeLinkWorkingPriority ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2TeLinkResourceClass ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2TeLinkIncomingIfId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2TeLinkOutgoingIfId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2TeLinkRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2TeLinkStorageType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2TeLinkTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for TeLinkDescriptorTable. */
INT1
nmhValidateIndexInstanceTeLinkDescriptorTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for TeLinkDescriptorTable  */

INT1
nmhGetFirstIndexTeLinkDescriptorTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexTeLinkDescriptorTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetTeLinkDescrSwitchingCapability ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetTeLinkDescrEncodingType ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetTeLinkDescrMinLspBandwidth ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetTeLinkDescrMaxLspBandwidthPrio0 ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetTeLinkDescrMaxLspBandwidthPrio1 ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetTeLinkDescrMaxLspBandwidthPrio2 ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetTeLinkDescrMaxLspBandwidthPrio3 ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetTeLinkDescrMaxLspBandwidthPrio4 ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetTeLinkDescrMaxLspBandwidthPrio5 ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetTeLinkDescrMaxLspBandwidthPrio6 ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetTeLinkDescrMaxLspBandwidthPrio7 ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetTeLinkDescrInterfaceMtu ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetTeLinkDescrIndication ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetTeLinkDescrRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetTeLinkDescrStorageType ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetTeLinkDescrSwitchingCapability ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetTeLinkDescrEncodingType ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetTeLinkDescrMinLspBandwidth ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetTeLinkDescrMaxLspBandwidthPrio0 ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetTeLinkDescrMaxLspBandwidthPrio1 ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetTeLinkDescrMaxLspBandwidthPrio2 ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetTeLinkDescrMaxLspBandwidthPrio3 ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetTeLinkDescrMaxLspBandwidthPrio4 ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetTeLinkDescrMaxLspBandwidthPrio5 ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetTeLinkDescrMaxLspBandwidthPrio6 ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetTeLinkDescrMaxLspBandwidthPrio7 ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetTeLinkDescrInterfaceMtu ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetTeLinkDescrIndication ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetTeLinkDescrRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetTeLinkDescrStorageType ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2TeLinkDescrSwitchingCapability ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2TeLinkDescrEncodingType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2TeLinkDescrMinLspBandwidth ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2TeLinkDescrMaxLspBandwidthPrio0 ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2TeLinkDescrMaxLspBandwidthPrio1 ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2TeLinkDescrMaxLspBandwidthPrio2 ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2TeLinkDescrMaxLspBandwidthPrio3 ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2TeLinkDescrMaxLspBandwidthPrio4 ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2TeLinkDescrMaxLspBandwidthPrio5 ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2TeLinkDescrMaxLspBandwidthPrio6 ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2TeLinkDescrMaxLspBandwidthPrio7 ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2TeLinkDescrInterfaceMtu ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2TeLinkDescrIndication ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2TeLinkDescrRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2TeLinkDescrStorageType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2TeLinkDescriptorTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for TeLinkSrlgTable. */
INT1
nmhValidateIndexInstanceTeLinkSrlgTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for TeLinkSrlgTable  */

INT1
nmhGetFirstIndexTeLinkSrlgTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexTeLinkSrlgTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetTeLinkSrlgRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetTeLinkSrlgStorageType ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetTeLinkSrlgRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetTeLinkSrlgStorageType ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2TeLinkSrlgRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2TeLinkSrlgStorageType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2TeLinkSrlgTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for TeLinkBandwidthTable. */
INT1
nmhValidateIndexInstanceTeLinkBandwidthTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for TeLinkBandwidthTable  */

INT1
nmhGetFirstIndexTeLinkBandwidthTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexTeLinkBandwidthTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetTeLinkBandwidthUnreserved ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetTeLinkBandwidthRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetTeLinkBandwidthStorageType ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetTeLinkBandwidthRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetTeLinkBandwidthStorageType ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2TeLinkBandwidthRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2TeLinkBandwidthStorageType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2TeLinkBandwidthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for ComponentLinkTable. */
INT1
nmhValidateIndexInstanceComponentLinkTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for ComponentLinkTable  */

INT1
nmhGetFirstIndexComponentLinkTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexComponentLinkTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetComponentLinkMaxResBandwidth ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetComponentLinkPreferredProtection ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetComponentLinkCurrentProtection ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetComponentLinkRowStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetComponentLinkStorageType ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetComponentLinkMaxResBandwidth ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetComponentLinkPreferredProtection ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetComponentLinkRowStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetComponentLinkStorageType ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2ComponentLinkMaxResBandwidth ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2ComponentLinkPreferredProtection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2ComponentLinkRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2ComponentLinkStorageType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2ComponentLinkTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for ComponentLinkDescriptorTable. */
INT1
nmhValidateIndexInstanceComponentLinkDescriptorTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for ComponentLinkDescriptorTable  */

INT1
nmhGetFirstIndexComponentLinkDescriptorTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexComponentLinkDescriptorTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetComponentLinkDescrSwitchingCapability ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetComponentLinkDescrEncodingType ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetComponentLinkDescrMinLspBandwidth ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetComponentLinkDescrMaxLspBandwidthPrio0 ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetComponentLinkDescrMaxLspBandwidthPrio1 ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetComponentLinkDescrMaxLspBandwidthPrio2 ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetComponentLinkDescrMaxLspBandwidthPrio3 ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetComponentLinkDescrMaxLspBandwidthPrio4 ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetComponentLinkDescrMaxLspBandwidthPrio5 ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetComponentLinkDescrMaxLspBandwidthPrio6 ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetComponentLinkDescrMaxLspBandwidthPrio7 ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetComponentLinkDescrInterfaceMtu ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetComponentLinkDescrIndication ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetComponentLinkDescrRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetComponentLinkDescrStorageType ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetComponentLinkDescrSwitchingCapability ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetComponentLinkDescrEncodingType ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetComponentLinkDescrMinLspBandwidth ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetComponentLinkDescrMaxLspBandwidthPrio0 ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetComponentLinkDescrMaxLspBandwidthPrio1 ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetComponentLinkDescrMaxLspBandwidthPrio2 ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetComponentLinkDescrMaxLspBandwidthPrio3 ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetComponentLinkDescrMaxLspBandwidthPrio4 ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetComponentLinkDescrMaxLspBandwidthPrio5 ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetComponentLinkDescrMaxLspBandwidthPrio6 ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetComponentLinkDescrMaxLspBandwidthPrio7 ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetComponentLinkDescrInterfaceMtu ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetComponentLinkDescrIndication ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetComponentLinkDescrRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetComponentLinkDescrStorageType ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2ComponentLinkDescrSwitchingCapability ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2ComponentLinkDescrEncodingType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2ComponentLinkDescrMinLspBandwidth ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio0 ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio1 ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio2 ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio3 ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio4 ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio5 ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio6 ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio7 ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2ComponentLinkDescrInterfaceMtu ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2ComponentLinkDescrIndication ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2ComponentLinkDescrRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2ComponentLinkDescrStorageType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2ComponentLinkDescriptorTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for ComponentLinkBandwidthTable. */
INT1
nmhValidateIndexInstanceComponentLinkBandwidthTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for ComponentLinkBandwidthTable  */

INT1
nmhGetFirstIndexComponentLinkBandwidthTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexComponentLinkBandwidthTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetComponentLinkBandwidthUnreserved ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetComponentLinkBandwidthRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetComponentLinkBandwidthStorageType ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetComponentLinkBandwidthRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetComponentLinkBandwidthStorageType ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2ComponentLinkBandwidthRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2ComponentLinkBandwidthStorageType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2ComponentLinkBandwidthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
