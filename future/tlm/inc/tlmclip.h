/**********************************************************************
 * Copyright (C) 2011 Aricent Group . All Rights Reserved
 *
 * $Id: tlmclip.h,v 1.6 2014/07/09 13:23:45 siva Exp $
 *
 * Description: This has prototype definitions for TLM CLI submodule
 *
 ***********************************************************************/
#include "tlminc.h" 

#ifndef __TLMCLIP_H__
#define __TLMCLIP_H__


typedef struct _TeLinkCliArgs
{
    UINT4   u4Metric;
    INT4    i4AddrType;
    UINT4   u4RouterId;
    UINT4   u4LocalIpAddr;
    UINT4   u4RemoteIpAddr;
    UINT4   u4LocalIdentifier;
    UINT4   u4RemoteIdentifier;
    UINT4   u4ResourceClass;
    INT4    i4ProtectionType;
    UINT4   u4TeLinkSrlg;
    INT4    i4TeLinkInfoType; /* 1.Data channel 2.Data and Control Channel*/
    INT4    i4TeLinkIfType; /* P2P or MultiAccess */
    INT4    i4TeLinkAdvertise;
}tTeLinkCliArgs;

typedef struct _CompLinkCliArgs
{
    UINT4  u4MinLsp;
    UINT4  u4MaxResvBw;
    INT4   i4SwitchingCap;
    INT4   i4EncodingType;
    INT4   i4IfIndex;
}tCompLinkCliArgs;


PRIVATE INT4
TlmCliSetTeLinkProperties (tCliHandle CliHandle, UINT4 u4Command, tTeLinkCliArgs *pTeLinkCliArgs);

PRIVATE INT4
TlmCliSetCompLinkDescProperties (tCliHandle CliHandle, UINT4 u4Command,
                           tCompLinkCliArgs * pCompLinkCliArgs);
PRIVATE INT4 TlmCliTeMode PROTO ((tCliHandle));
PRIVATE VOID
TlmCliUpdateLinkIndices PROTO ((tCliHandle));
PRIVATE INT4
TlmCliChangePrompt PROTO ((BOOL1 , INT4, INT4));
PRIVATE INT4 TlmCliSetModuleStatus PROTO ((tCliHandle, INT4));
PRIVATE INT4 TlmCliCreateTeLink (tCliHandle CliHandle, UINT1 *pu1TeLinkName, INT4 i4BundleIfIndex,
                         INT4 i4TeLinkType);
PRIVATE INT4 
TlmCliCreateCompLink PROTO ((tCliHandle, INT4, INT4));
PRIVATE INT4
TlmCliDeleteCompLink PROTO ((tCliHandle, INT4, INT4));
PRIVATE INT4
TlmCliGetIndices PROTO ((tCliHandle CliHandle, INT4));

PRIVATE INT4
TlmCliSetTeLinkAdminStatusUp PROTO ((tCliHandle, INT4));
INT4
TlmCliSetTeLinkAdminStatusDn PROTO ((tCliHandle, INT4));

PRIVATE INT4
TlmCliSetCompLinkAdminStatusUp PROTO ((tCliHandle, INT4, INT4));
PRIVATE INT4
TlmCliSetCompLinkAdminStatusDown PROTO ((tCliHandle, INT4));

PRIVATE INT4
TlmCliSetTeLinkName PROTO ((tCliHandle, INT4, tSNMP_OCTET_STRING_TYPE));
PRIVATE INT4
TlmCliSetTeLinkType PROTO ((tCliHandle, INT4, INT4));
PRIVATE INT4
TlmCliSetTeLinkRowStatus PROTO ((tCliHandle, INT4, INT4));
PRIVATE INT4
TlmCliSetTeLinkBwRowStatus PROTO ((tCliHandle, INT4, INT4));
PRIVATE INT4 TlmCliDeleteTeLink PROTO ((tCliHandle, UINT1 *, INT4));
PRIVATE INT4
TlmCliSetTeLinkDescRowStatus (tCliHandle CliHandle, INT4 i4TeIfIndex, INT4 i4RowStatus);
PRIVATE INT4
TlmCliSetTeLinkMetric PROTO ((tCliHandle, UINT4, INT4));
PRIVATE INT4
TlmCliSetTeLinkAddrType PROTO ((tCliHandle, INT4, INT4));
PRIVATE INT4
TlmCliSetTeLinkIpAddr PROTO ((tCliHandle, UINT4, UINT4, INT4));
PRIVATE INT4
TlmCliSetTeLinkRemoteRouterId PROTO ((tCliHandle, UINT4, INT4));
PRIVATE INT4
TlmCliSetTeLinkUnNum PROTO ((tCliHandle, UINT4, UINT4, INT4));
PRIVATE INT4
TlmCliSetTeLinkRsrcCls PROTO ((tCliHandle, UINT4, INT4));
PRIVATE INT4
TlmCliSetTeLinkProtectionType PROTO ((tCliHandle, INT4, INT4));
PRIVATE INT4
TlmCliSetTeLinkSrlgUp PROTO ((tCliHandle , UINT4, INT4));
PRIVATE INT4
TlmCliSetTeLinkSrlgDown PROTO ((tCliHandle , UINT4, INT4));
PRIVATE INT4
TlmCliDeleteTeLinkRsrcCls PROTO ((tCliHandle, UINT4, INT4));

PRIVATE INT4
TlmCliSetCompLinkRowStatus PROTO ((tCliHandle, INT4, INT4));
PRIVATE INT4
TlmCliSetCompLinkDescRowStatus PROTO ((tCliHandle, INT4, INT4));
PRIVATE INT4
TlmCliSetCompLinkBwRowStatus PROTO ((tCliHandle, INT4, INT4));
PRIVATE INT4
TlmCliSetCompLinkSwCapAndEncType PROTO ((tCliHandle, INT4, INT4, INT4));
PRIVATE INT4
TlmCliSetCompLinkMaxRsrvableBw PROTO ((tCliHandle, UINT4, INT4));
PRIVATE INT4
TlmCliSetCompLinkMinLspBw PROTO ((tCliHandle, UINT4, INT4));
PRIVATE INT4
TlmCliSetTeLinkTrace PROTO ((UINT4, INT4));

PRIVATE INT4
TlmCliShowTeLink PROTO ((tCliHandle, UINT4, UINT1*));

PRIVATE INT4
TlmCliShowSpecificTeLink PROTO ((tCliHandle CliHandle, INT4 i4TeIfIndex));

PRIVATE VOID
TlmCliShowTeLinkDescInfo PROTO ((tCliHandle, INT4));

PRIVATE VOID
TlmCliShowCompLinkDescInfo PROTO ((tCliHandle, INT4));

PRIVATE INT4
TlmCliShowCompLinkInfo PROTO ((tCliHandle ,UINT4 , INT4));

PRIVATE INT1
TlmCliSetBwThreshold PROTO ((tCliHandle, INT4, INT4, INT4 *));
PRIVATE INT4 
TlmCliSetBwThresholdForce PROTO ((tCliHandle));
PRIVATE INT4
TlmCliSetFsTeLinkInfoType (tCliHandle CliHandle, INT4 i4TeLinkInfoType,
                           INT4 i4TeIfIndex);
PRIVATE INT4
TlmCliSetFsTeLinkIfType (tCliHandle CliHandle, UINT4 i4TeLinkIfType,
                         INT4 i4TeIfIndex);
PRIVATE INT1
TlmCliSetTeLinkAdvertise (tCliHandle CliHandle, INT4 i4TeLinAdvt, 
                          INT4 i4TeIfIndex);
#endif
