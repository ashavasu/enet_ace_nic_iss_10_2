/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstlmwr.h,v 1.3 2012/04/02 12:31:45 siva Exp $
*
* Description: Prototypes of Wrapper Routines for Proprietary TLM MIB
*********************************************************************/

#ifndef _FSTLMWR_H
#define _FSTLMWR_H

VOID RegisterFSTLM(VOID);

VOID UnRegisterFSTLM(VOID);
INT4 FsTeLinkTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkTraceOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsTeLinkModuleStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsTeLinkTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsTeLinkNameGet(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkRemoteRtrIdGet(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkMaximumBandwidthGet(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkInfoTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkIfTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkIsAdvertiseGet(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkNameSet(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkRemoteRtrIdSet(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkInfoTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkIfTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkIsAdvertiseSet(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkRemoteRtrIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkInfoTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkIfTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkIsAdvertiseTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsTeLinkBwThresholdTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsTeLinkBwThreshold0Get(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold1Get(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold2Get(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold3Get(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold4Get(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold5Get(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold6Get(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold7Get(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold8Get(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold9Get(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold10Get(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold11Get(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold12Get(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold13Get(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold14Get(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold15Get(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThresholdRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold0Set(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold1Set(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold2Set(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold3Set(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold4Set(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold5Set(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold6Set(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold7Set(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold8Set(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold9Set(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold10Set(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold11Set(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold12Set(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold13Set(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold14Set(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold15Set(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThresholdRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold0Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold1Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold2Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold3Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold4Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold5Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold6Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold7Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold8Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold9Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold10Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold11Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold12Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold13Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold14Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThreshold15Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThresholdRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThresholdTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsTeLinkBwThresholdForceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThresholdForceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThresholdForceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTeLinkBwThresholdForceOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _FSTLMWR_H */
