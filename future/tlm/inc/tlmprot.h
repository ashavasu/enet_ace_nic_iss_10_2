/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: tlmprot.h,v 1.14 2014/07/09 13:23:45 siva Exp $
*
* Description: This file contains function prototypes definitions.
*********************************************************************/

#ifndef _TLM_PROT_H_
#define _TLM_PROT_H_

INT1   
TlmTestAllTeLinkTable PROTO ((UINT4 *pu4ErrorCode, INT4 i4IfIndex , 
                             tTlmTeLink *pTlmTeLink,
                             UINT4 u4ObjectId));
INT1                                                                                                            
TlmTestAllTeLinkDescriptorTable PROTO ((UINT4 *pu4ErrorCode,
                                        INT4 i4IfIndex, 
                                        UINT4 ,
                             tTlmTeIfDescr *pTlmTeIfDescr,
                             UINT4 u4ObjectId));
INT1
TlmTestAllTeLinkSrlgTable PROTO ((UINT4 *pu4ErrorCode,   
                                  INT4 i4IfIndex,
                                  UINT4 u4TeLinkSrlg,
                                  tTlmTeLinkSrlg *pTlmTeLinkSrlg,
                             UINT4 u4ObjectId));

INT1
TlmTestAllTeLinkBandwidthTable PROTO ((UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                             UINT4 u4IfIndex,
                             tTlmTeLink *pTlmTeLink,
                             UINT4 u4ObjectId));
INT1
TlmTestAllComponentLinkTable PROTO ((UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                             tTlmComponentLink *pTlmComponentLink,
                             UINT4 u4ObjectId));

INT1
TlmTestAllComponentLinkDescriptorTable PROTO ((UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                             UINT4 u4ComponentLinkDescrId,
                             tTlmComponentIfDescr *pTlmComponentIfDescr,
                             UINT4 u4ObjectId));
INT1
TlmTestAllComponentLinkBandwidthTable PROTO ((UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                       UINT4 u4ComponentLinkBandwidthPriority,
                             tTlmComponentLink *pTlmComponentLink,
                             UINT4 u4ObjectId));

INT1
TlmTestAllFsTlmTeLinkBwThresholdTable PROTO (( UINT4 *pu4ErrorCode, INT4 i4IfIndex ,
                                       INT4 i4FsTeLinkBwThresholdIndex,
                                       tTlmTeLink *pTlmTeLink,
                                       UINT4 u4ObjectId));

INT1
TlmGetAllTeLinkTable PROTO ((INT4 i4IfIndex, tTlmTeLink *pTlmTeLink));

INT1
TlmGetAllTeLinkDescriptorTable PROTO ((INT4 i4IfIndex , UINT4 u4TeLinkDescriptorId,
                                tTlmTeIfDescr *pTlmTeIfDescr));

INT1
TlmGetAllTeLinkSrlgTable PROTO ((INT4 i4IfIndex , UINT4 u4TeLinkSrlg,
                          tTlmTeLinkSrlg *pTlmTeLinkSrlg));

INT1
TlmGetAllTeLinkBandwidthTable PROTO ((INT4 i4IfIndex , UINT4 u4TeLinkBandwidthPriority,
                               tTlmTeLink *pTlmTeLink));

INT1
TlmGetAllComponentLinkTable PROTO ((INT4 i4IfIndex, tTlmComponentLink *pTlmComponentLink));

INT1
TlmGetAllComponentLinkDescriptorTable PROTO ((INT4 i4IfIndex, UINT4 u4ComponentLinkDescrId,
                                       tTlmComponentIfDescr *pTlmComponentIfDescr));

INT1
TlmGetAllComponentLinkBandwidthTable PROTO ((INT4 i4IfIndex, UINT4 u4ComponentLinkBandwidthPriority,
                                      tTlmComponentLink *pTlmComponentLink));

INT1
TlmGetAllFsTlmTeLinkBwThresholdTable PROTO ((INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                                             tTlmTeLink *pTlmTeLink));

INT1
TlmSetAllTeLinkTable PROTO ((INT4 i4IfIndex, tTlmTeLink *pTlmTeLink, UINT4 u4ObjectId));

INT1
TlmSetAllTeLinkDescriptorTable PROTO ((INT4, UINT4, tTlmTeIfDescr *pTlmTeIfDescr, UINT4 u4ObjectId));

INT1
TlmSetAllTeLinkSrlgTable PROTO ((INT4 i4IfIndex , UINT4 u4TeLinkSrlg,
                                 tTlmTeLinkSrlg *ptTlmTeLinkSrlg, UINT4 u4ObjectId));

INT1
TlmSetAllTeLinkBandwidthTable PROTO ((INT4 i4IfIndex , UINT4 u4TeLinkBandwidthPriority,
                                     tTlmTeLink *pTlmTeLink,
                                     UINT4 u4ObjectId));

INT1
TlmSetAllComponentLinkTable PROTO ((INT4 i4IfIndex, tTlmComponentLink *TlmComponentLink,
                                   UINT4 u4ObjectId));

INT1
TlmSetAllComponentLinkDescriptorTable PROTO ((INT4 i4IfIndex , UINT4 u4ComponentLinkDescrId,
                                       tTlmComponentIfDescr *pTlmComponentIfDescr,
                                             UINT4 u4ObjectId));

INT1
TlmSetAllComponentLinkBandwidthTable PROTO ((INT4 i4IfIndex , UINT4 u4ComponentLinkBandwidthPriority,
                                      tTlmComponentLink *pTlmCompLink,
                                             UINT4 u4ObjectId));

INT1
TlmGetAllTeLinkBwThresholdTable PROTO ((INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                                        tTlmTeLink *pTlmTeLink));

INT1
TlmSetAllTeLinkBwThresholdTable PROTO ((INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                                        tTlmTeLink *pTlmTeLink,
                                        UINT4 u4ObjectId));

INT1
TlmTestAllTeLinkBwThresholdTable PROTO ((UINT4 *pu4ErrorCode, INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                                        tTlmTeLink *pTlmTeLink,
                                        UINT4 u4ObjectId));
VOID
TlmGetTeLinkBwThreshold (UINT4 u4ThresholdIndex, INT4 i4BwThresholdIndex,
                         tTlmTeLink *pTlmTeLink, INT4 *pi4RetVal);
VOID
TlmSetTeLinkBwThreshold (UINT4 u4ThresholdIndex, INT4 i4BwThresholdIndex,
                         tTlmTeLink *pTlmTeLink, INT4 i4RetVal);


tTlmTeLink * TlmCreateTeIf PROTO ((INT4));
tTlmTeIfDescr * TlmCreateTeDescr (UINT4 u4DescrId, tTlmTeLink * pTeIface);
tTlmTeLinkSrlg * TlmCreateTeSrlg (UINT4 u4LinkSrlg, tTlmTeLink * pTeIface);

tTlmComponentLink *TlmComponentIfCreate (INT4 i4IfIndex);

tTlmComponentIfDescr *TlmCreateComponentDescr (UINT4 u4DescrId, 
                                               tTlmComponentLink * pComponentIface);

tTlmTeLink* TlmFindTeIf (INT4 i4IfIndex);
tTlmTeIfDescr* TlmFindTeDescr (UINT4 u4DescrId, tTlmTeLink * pTeIface);

tTlmTeLinkSrlg* TlmFindTeSrlgIf (UINT4 u4LinkSrlg, tTlmTeLink * pTeIface);

tTlmComponentLink* TlmFindComponentIf (INT4 i4IfIndex);

tTlmComponentIfDescr* TlmFindComponentDescr (UINT4 u4DescrId, tTlmComponentLink * pcomponentiface);

UINT4
TlmUtilValidateTeCompLinkAssoc PROTO ((UINT4, INT4));
UINT1
TlmUtilValidateCompLinkIndex PROTO ((UINT4));
UINT1 TlmUtilValidateIpAddress (UINT4 u4IpAddr);

UINT1 TlmUtilCheckSupportedClassType  PROTO ((UINT1));

UINT1 TlmUtilCheckTeLinkNameUnique PROTO ((UINT1 *, INT4));

UINT4 TlmUtilHashGetValue (UINT4 u4HashSize, UINT1 *pKey, UINT1 u1KeyLen);

UINT4
TlmUtilHashGetNameValue (UINT1 *pu1LinkName, UINT1 u1LinkNameLen);

INT4
TlmUtilRdmCalcUnResBw (tTlmComponentLink * pComponentiface, UINT1 u1TeClass,
                                                        tBandWidth * unResBw);

PUBLIC INT4
TlmUtilGetTeClsfromPrioClsType (UINT1 u1Prio, UINT1 u1ClassType,
                                UINT1 *u1TeClass);

PUBLIC INT4
TlmUtilValidateTeClass (UINT1 u1TeClass);

VOID
TlmFillTeLinkOutInfo (tTlmTeLink * pTeLink, tOutTlmTeLink *pOutTlmTeLinkInfo);

VOID
TlmUpdateInfoToRegProtocol (UINT4 u4ModId);

INT4
TlmEnqueueMsgToTlmQ (tTlmCfgMsg * pMsg);

INT4
TlmDynamicStackEntryRowStatus (UINT4 u4HighIfIndex, UINT4 u4LowIfIndex,
                               UINT1 u1StackRowStatus);

INT4
TlmCreateMplsIfStackEntry (UINT4 u4IfIndex, UINT4 u4LowIfIndex);

INT4
TlmDeleteTeIf (tTlmTeLink * pTeIface);

UINT4
TlmDeleteTeDescr (tTlmTeIfDescr * pIfDesr, tTlmTeLink * pTeIface);

VOID
TlmDeleteTeLinkSrlg (tTlmTeLinkSrlg * pTeSrlg, tTlmTeLink * pTeIface);

INT4
TlmDeleteComponentIf (tTlmComponentLink * pComponentNode);

UINT4
TlmDeleteComponentDescr (tTlmComponentIfDescr * pIfDesr,
                         tTlmComponentLink * pCompIface);

INT1
TlmUpdateTeLinkOperStatus (tTlmTeLink * pTeIface, UINT1 u1OperStatus);
VOID
TlmUpdateCompLinkOperstatus (tTlmComponentLink *pCompIface, UINT1 u1OperStatus);

INT1
TlmUpdateBundleTeLinkInfo (tTlmTeLink* pMemberTeIface, tTlmTeLink * pBundleTeIface,
                           UINT1 u1RequestType);

UINT1
TlmUpdateTeLinkInfo (tTlmComponentLink * pTeComponent, tTlmTeLink * pTeIface,
                                                UINT1 u1RequestType);

VOID
TlmUpdateUnBundleTeLinks (tTlmTeLink * pTeIface, tTlmTeLink * pBundleIface,
                          UINT1 u1UpdateStatus);

VOID
TlmUpdateComponentLink (tTlmTeLink *pTeLink, UINT1 u1IfStatus);


INT4
TlmGetCompIndexFromTeIfIndex (UINT4 u4TeIfIndex, UINT4 *pu4CompIfIndex);

INT1
TlmUtilSendNotifMsgInfo (tTlmTeLink * pTeIface, UINT4 u4EventType);

INT4
TlmMplsSetTeClassForCTPrio (UINT1 u1ClassType, UINT1 u1PremptPrio, UINT1 u1TeClass,
                        UINT4 u4TeClsRowStatus);

FLT4 TlmFloatAdd       PROTO ((FLT4, FLT4));
FLT4 TlmFloatSubtract  PROTO ((FLT4, FLT4));

VOID TlmTeAddTosortIfLst             PROTO ((tTlmTeLink *));
VOID TlmTeAddTosortIfDescrLst PROTO ((tTlmTeIfDescr * , tTlmTeLink *));
VOID
TlmTeAddTosortIfSrlgLst (tTlmTeLinkSrlg * pTeSrlg, tTlmTeLink * pTeIface);
VOID
TlmCompDescrAddToSortIfDescrLst (tTlmComponentIfDescr * pComponentDescr,
                                      tTlmComponentLink * pComponentIface);
VOID
ComponentAddTosortIfLst (tTlmComponentLink * pComponentiface);

VOID
TlmTeAddTosortIfComponentLst (tTlmComponentLink *pCompIface,
                              tTlmTeLink * pTeIface);

VOID
TlmTeAddUnbundleTeIfToBundle (tTlmTeLink *pTeIface, tTlmTeLink *pBundleTeIface);

VOID
TlmTeDeleteUnbundleTeIfFromBundle (tTlmTeLink *pTeIface, tTlmTeLink *pBundleTeIface);

VOID
TlmTeDeleteTosortIfComponentLst (tTlmComponentLink *pCompIface,
                              tTlmTeLink * pTeIface);

VOID
TlmDeleteUnbundleTeList (tTlmTeLink * pBundleTeIface);

PUBLIC
INT1 TlmUtilFillEvenNotifMsg (tTlmTeLink *pTeLink, UINT4 u4EventType,
                              tTlmEventNotification *pRetNotifMsg);

VOID
TlmUtilFloodBwForThreshold (tTlmTeLink *pTeLink, tBandWidth changeBwPerc,
                                              INT4 i4BwChange);

INT1
TlmUtilNotifyProtocols (tTlmEventNotification * pTlmEventNotification);
INT4
TlmUtilGetIfIndexFromTeLinkName (UINT1 *pu1TeLinkName, UINT1 u1TeLinkNameLen,
                                 INT4 *pi4IfIndex);

VOID
TlmUtilUpdateUnReservBw (tTlmComponentLink *pCompIface);

VOID TlmUpdateComponentOperStatus  PROTO ((UINT4, UINT1));

INT4                                                                                                              TlmMplsCreateMplsIf (UINT4 u4TlmIntf, UINT4 u4IfIndex, INT4 i4IfType);
INT4
TlmExtDeleteTeLink (tTlmTeLink *pTeIface);

VOID
TlmCfaIfStatusUpdateHandler (UINT4 u4MsgType, INT4 i4IfIndex,
                             UINT1 u1IfType, UINT1 u1IfStatus, UINT1 u1BridgedIface);

INT4
TlmPortCreateTeIfOrMplsIf (UINT4 u4TeIfIndex, UINT1 u1IfType);
INT4
TlmPortStackTeLinkIfOrMplsIf (INT4 i4HighIfIndex, INT4 i4LowIfIndex, INT4 i4RowStatus);
INT4
TlmPortDeleteTeLinkIfOrMplsIf (INT4 i4TeIfIndex);
INT4
TlmPortCfaRegLL (VOID);
INT4
TlmPortCfaDeRegisterLL (UINT2 u2LenOrType);
FLT4 TlmUtilOctetStringToFloat (tSNMP_OCTET_STRING_TYPE *pOctetString);

VOID
TlmUtilUpdateCompLinkInfo (tTlmComponentLink *pCompIface,
                           tInTlmTeLink *pInTlmTeLinkInfo);
VOID
TlmMplsDiffservTeHandler (tTlmClassType aTlmClassType[],
                          tTlmTeClassMap  aTlmTeClassMap[]);
VOID
TlmUtilCalculateBCvalue (tTlmComponentLink  *pCompLink);

/* RB Tree */
PUBLIC INT4 TlmCreateTeLinkTree (VOID);
PUBLIC INT4 TlmDeleteTeLinkTree (VOID);
tTlmTeLink *
TlmGetUnNumberedTeLink (UINT4 u4RemoteRtrId, UINT4 u4RemoteIdentifier, UINT4 u4LocalIdentifier);
tTlmTeLink *
TlmGetNumberedTeLink (UINT4 u4RemoteIpAddr, UINT4 u4LocalIpAddr);
INT4 
TlmDeleteTeLinkIDBasedTree (tTlmTeLink *pTeLink);
INT4 
TlmAddTeLinkToIDBasedTree (tTlmTeLink *pTeLink);

INT4             
TlmCreateFATeLink (tInTlmTeLink *pInTlmTeLinkInfo, 
                   tOutTlmTeLink *pOutTlmTeLink);
INT4
TlmUpdateFATeLink (tInTlmTeLink * pInTlmTeLinkInfo, 
                   tTlmComponentLink *pCompIface);
INT4
TlmDeleteFATeLink ( tInTlmTeLink * pInTlmTeLinkInfo );

INT4
TlmGetTeLinkUnResvBw (tInTlmTeLink * pInTlmTeLinkInfo,
                      tOutTlmTeLink * pOutTlmTeLinkInfo);

INT4
TlmUpdateUnResvBwForUnBundleTeLink (UINT1 u1ResOrRel,
                                    tInTlmTeLink * pInTlmTeLinkInfo,
                                    tOutTlmTeLink * pOutTlmTeLinkInfo,
                                    tTlmTeLink *pTeLink);
INT4
TlmUpdateTeLinkUnResvBw (UINT1 u1ResOrRel, tInTlmTeLink * pInTlmTeLinkInfo,
                         tOutTlmTeLink * pOutTlmTeLinkInfo);

VOID TlmUtilRdmCalcUnResBwForAllTeClass (tTlmTeLink *pTeLink,
                                         tTlmComponentLink *pCompLink,
                                         UINT1 u1ResOrRel);

VOID TlmUtilCalculateBwForBundledTeLink (tTlmTeLink *pTeLink);

#endif
