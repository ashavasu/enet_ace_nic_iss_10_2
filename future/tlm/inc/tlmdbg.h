/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: tlmdbg.h,v 1.3 2012/02/09 11:27:53 siva Exp $
 *
 * Description: This file contains procedures and definitions
 *              used for debugging.
 *********************************************************************/

#ifndef _TLM_DBG_H_
#define _TLM_DBG_H

#define TLM_TRC_FLAG    (UINT4) (TLM_CUR_TRACE_OPTION)
#define TLM_NAME         "TLM"

#ifdef TRACE_WANTED
#define TLM_TRC(Value, Fmt)           UtlTrcLog(TLM_TRC_FLAG, \
                                                Value,        \
                                                TLM_NAME,        \
                                                Fmt)

#define TLM_TRC1(Value, Fmt, Arg)     UtlTrcLog(TLM_TRC_FLAG, \
                                                Value,        \
                                                TLM_NAME,        \
                                                Fmt,          \
                                                Arg)
#define TLM_TRC2(Value, Fmt, Arg1, Arg2)                      \
                                      UtlTrcLog(TLM_TRC_FLAG, \
                                               Value,        \
                                               TLM_NAME,        \
                                               Fmt,          \
                                               Arg1, Arg2)

#define TLM_TRC3(Value, Fmt, Arg1, Arg2, Arg3)                \
                                      UtlTrcLog(TLM_TRC_FLAG, \
                                               Value,        \
                                               TLM_NAME,        \
                                               Fmt,          \
                                               Arg1, Arg2, Arg3)

#define TLM_TRC4(Value, Fmt, Arg1, Arg2, Arg3, Arg4)          \
                                      UtlTrcLog(TLM_TRC_FLAG, \
                                               Value,        \
                                               TLM_NAME,        \
                                               Fmt,          \
                                               Arg1, Arg2,   \
                                               Arg3, Arg4)

#define TLM_TRC5(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)    \
                                      UtlTrcLog(TLM_TRC_FLAG, \
                                               Value,        \
                                               TLM_NAME,        \
                                               Fmt,          \
                                               Arg1, Arg2, Arg3, \
                                               Arg4, Arg5)

#define TLM_TRC6(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
                                      UtlTrcLog(TLM_TRC_FLAG, \
                                               Value,        \
                                               TLM_NAME,        \
                                               Fmt,          \
                                               Arg1, Arg2, Arg3, \
                                               Arg4, Arg5, Arg6)

#define TLM_TRC_FN_ENTRY() \
    if (TLM_TRC_FLAG & TLM_FN_ENTRY_TRC ) \
{\
    MOD_FN_ENTRY (TLM_TRC_FLAG, TLM_FN_ENTRY_TRC, \
                  (const CHR1 *) "TLM")\
}

#define TLM_TRC_FN_EXIT() \
    if (TLM_TRC_FLAG & TLM_FN_EXIT_TRC ) \
{\
    MOD_FN_EXIT (TLM_TRC_FLAG, TLM_FN_EXIT_TRC, \
                  (const CHR1 *) "TLM")\
}

#else
#define  TLM_TRC(Value, Fmt)
#define  TLM_TRC1(Value, Fmt, Arg)
#define  TLM_TRC2(Value, Fmt, Arg1, Arg2)
#define  TLM_TRC3(Value, Fmt, Arg1, Arg2, Arg3)
#define  TLM_TRC4(Value, Fmt, Arg1, Arg2, Arg3, Arg4)
#define  TLM_TRC5(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)
#define  TLM_TRC6(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)
#define  TLM_TRC_FN_ENTRY()
#define  TLM_TRC_FN_EXIT()   
          
#endif   /* _TLM_DBG_H_ */
#endif
