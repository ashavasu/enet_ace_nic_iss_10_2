/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: tlmglob.h,v 1.3 2011/12/14 15:45:44 siva Exp $
*
* Description: This file defines global parameters and data structures.
*********************************************************************/

#ifndef _TLM_GLOB_H_
#define _TLM_GLOB_H_

#ifdef _TLMMAIN_C_
/* Declaration of Global variables */
tTlmGlobalInfo gTlmGlobalInfo;
tTlmCliArgs gTlmCliArgs[CLI_MAX_SESSIONS];
INT4 gai4BwThresholds[TLM_MAX_BW_THRESHOLD_SUPPORTED] = {15,30,45,60,70,75,80,85,90,93,95,96,97,98,99,100};
#else
extern tTlmGlobalInfo gTlmGlobalInfo;
extern tTlmCliArgs gTlmCliArgs[CLI_MAX_SESSIONS];
extern INT4 gai4BwThresholds[TLM_MAX_BW_THRESHOLD_SUPPORTED];
#endif



#endif
