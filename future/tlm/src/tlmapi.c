/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: tlmapi.c,v 1.16 2015/09/22 05:58:08 siva Exp $
 *
 * Description: TLM APIs to interface with applications.
 ***********************************************************************/
#include "tlminc.h"
#include "tlmcli.h"

/******************************************************************************
 * Function Name      : TlmLock
 *
 * Description        : Function to take the mutual exclusion protocol
 *                      semaphore.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : TLM_SUCCESS/TLM_FAILURE
 *****************************************************************************/
INT4
TlmLock ()
{
    if (OsixSemTake (gTlmGlobalInfo.SemId) != OSIX_SUCCESS)
    {
        TLM_TRC (TLM_CRITICAL_TRC, "Failed to Take TLM Protocol "
                 "Mutual Exclusion Semaphore \n");
        return TLM_FAILURE;
    }
    return TLM_SUCCESS;
}

/******************************************************************************
 * Function Name      : TlmUnLock
 *
 * Description        : This function is used to give the mutual
 *                      exclusion semaphore to avoid simultaneous access to
 *                      protocol data structures by the protocol task and
 *                      configuration task/thread.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : TLM_SUCCESS
 *****************************************************************************/
INT4
TlmUnLock ()
{
    OsixSemGive (gTlmGlobalInfo.SemId);
    return TLM_SUCCESS;
}

/******************************************************************************
 * Function Name      : TlmApiGetTeLinkParams
 *
 * Description        : This function is used by RSVP-TE  to Get the
 *                      the Te-Link related parameters .
 *
 * Input(s)           : u4Flags - Flags to indicate the input params given
 *                      pInTlmTeLinkInfo - Inputs to the TE link params 
 *
 * Output(s)          : pOutTlmTeLinkInfo - TE link params
 *
 * Return Value(s)    : TLM_SUCCESS / TLM_FAILURE
 *
 ******************************************************************************/
INT4
TlmApiGetTeLinkParams (UINT4 u4Flags, tInTlmTeLink * pInTlmTeLinkInfo,
                       tOutTlmTeLink * pOutTlmTeLinkInfo)
{
    tTlmTeLink         *pTeLink = NULL;
    tTMO_HASH_TABLE    *pTeIfHashTable = gTlmGlobalInfo.pTeIfHashTable;
    UINT4               u4HashIndex = TLM_ZERO;
    INT4                i4RetVal = TLM_FAILURE;

    TLM_TRC_FN_ENTRY ();

    if (gTlmGlobalInfo.i4ModuleStatus == TLM_DISABLED)
    {
        return TLM_FAILURE;
    }

    TLM_LOCK ();

    if (pTeIfHashTable == NULL)
    {
        TLM_UNLOCK ();
        return TLM_FAILURE;
    }

    TMO_HASH_Scan_Table (pTeIfHashTable, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (pTeIfHashTable, IF_HASH_FN (u4HashIndex),
                              pTeLink, tTlmTeLink *)
        {
            if (u4Flags & TLM_TE_LINK_IF_INDEX)
            {
                if (pTeLink->i4IfIndex == (INT4) pInTlmTeLinkInfo->u4IfIndex)
                {
                    TlmFillTeLinkOutInfo (pTeLink, pOutTlmTeLinkInfo);
                    TLM_UNLOCK ();
                    return i4RetVal;
                }
            }
            if (u4Flags & TLM_TE_LINK_LOCAL_IDENTIFIER)
            {
                if (pTeLink->u4LocalIdentifier ==
                    pInTlmTeLinkInfo->u4LocalIdentifier)
                {
                    TlmFillTeLinkOutInfo (pTeLink, pOutTlmTeLinkInfo);
                    TLM_UNLOCK ();
                    return i4RetVal;
                }
            }
            if (u4Flags & TLM_TE_LINK_REMOTE_IDENTIFIER)
            {
                if ((pTeLink->u4RemoteIdentifier ==
                     pInTlmTeLinkInfo->u4RemoteIdentifier) &&
                    (pTeLink->u4RemoteRtrId == pInTlmTeLinkInfo->u4RemoteRtrId))
                {
                    TlmFillTeLinkOutInfo (pTeLink, pOutTlmTeLinkInfo);
                    TLM_UNLOCK ();
                    return i4RetVal;
                }
            }
            if (u4Flags & TLM_TE_LINK_REMOTE_IP_ADD)
            {
                if (MEMCMP
                    (&pTeLink->RemoteIpAddr, &pInTlmTeLinkInfo->RemoteIpAddr,
                     sizeof (pTeLink->RemoteIpAddr)) == TLM_ZERO)
                {
                    TlmFillTeLinkOutInfo (pTeLink, pOutTlmTeLinkInfo);
                    TLM_UNLOCK ();
                    return i4RetVal;
                }
            }
            if (u4Flags & TLM_TE_LINK_LOCAL_IP_ADD)
            {
                if (MEMCMP
                    (&pTeLink->LocalIpAddr, &pInTlmTeLinkInfo->LocalIpAddr,
                     sizeof (pTeLink->LocalIpAddr)) == TLM_ZERO)
                {
                    TlmFillTeLinkOutInfo (pTeLink, pOutTlmTeLinkInfo);
                    TLM_UNLOCK ();
                    return i4RetVal;
                }
            }
        }
    }

    TLM_UNLOCK ();

    TLM_TRC_FN_EXIT ();
    return i4RetVal;
}

/******************************************************************************
 * Function Name      : TlmApiGetTeLinkUnResvBw
 *
 * Description        : This function is used by RSVP-TE  to Get the
 *                      the Unreserved bandwidth of a TE link .
 *
 * Input(s)           : pInTlmTeLinkInfo - Inputs to the TE link params 
 *
 * Output(s)          : pOutTlmTeLinkInfo - UnReserved BW and Available BW
 *                      of a TE-link interface
 *
 * Return Value(s)    : TLM_SUCCESS / TLM_FAILURE
 *
 ******************************************************************************/
PUBLIC INT4
TlmApiGetTeLinkUnResvBw (tInTlmTeLink * pInTlmTeLinkInfo,
                         tOutTlmTeLink * pOutTlmTeLinkInfo)
{
    INT4                i4RetVal = TLM_FAILURE;

    TLM_LOCK ();

    i4RetVal = TlmGetTeLinkUnResvBw (pInTlmTeLinkInfo, pOutTlmTeLinkInfo);

    TLM_UNLOCK ();

    return i4RetVal;
}

/******************************************************************************
 * Function Name      : TlmApiUpdateTeLinkUnResvBw                                                     
 *
 * Description        : This function is used to update the unreserved bandwidth
 *                      information required to RSVP-TE.
 *
 * Input(s)           : u1ResOrRel       - Reserve/Release
 *                      pInTlmTeLinkInfo - Inputs to the TE link params 
 * Output(s)          : pOutTlmTeLinkInfo - The return data structure of the
 *                                      Te-Link parameters.
 * Return Value(s)    : TLM_SUCCESS/TLM_FAILURE
 *
 *                                                                                                                 ******************************************************************************/

INT4
TlmApiUpdateTeLinkUnResvBw (UINT1 u1ResOrRel, tInTlmTeLink * pInTlmTeLinkInfo,
                            tOutTlmTeLink * pOutTlmTeLinkInfo)
{
    INT4                i4RetVal = TLM_FAILURE;

    TLM_LOCK ();

    i4RetVal = TlmUpdateTeLinkUnResvBw (u1ResOrRel, pInTlmTeLinkInfo,
                                        pOutTlmTeLinkInfo);

    TLM_UNLOCK ();

    return i4RetVal;
}

/******************************************************************************
 * Function Name      : TlmFillTeLinkOutInfo
 *
 * Description        : This function is used to send the information
 *                      required to RSVP-TE.
 *
 * Input(s)           : pTeLink - Pointer to te-link which contains the
 *                                 actual values.
 *                      pOutTlmTeLinkInfo - The return data structure of the
 *                                      Te-Link parameters.
 * Output(s)          : None
 * Return Value(s)    : TLM_SUCCESS/TLM_FAILURE
 *******************************************************************************/
VOID
TlmFillTeLinkOutInfo (tTlmTeLink * pTeLink, tOutTlmTeLink * pOutTlmTeLinkInfo)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTlmComponentLink  *pCompLink = NULL;
    tTlmTeIfDescr      *pTeIfDescr = NULL;
    INT4                i4TeLinkNameLength = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();

    if ((pLstNode = TMO_SLL_First (&(pTeLink->ComponentList))) != NULL)
    {
        pCompLink = GET_COMPONENT_IF_PTR_FROM_SORT_TE_LST (pLstNode);

    }
    if (pCompLink == NULL)
    {
        pOutTlmTeLinkInfo->u4CompIfIndex = TLM_ZERO;
    }
    else
    {
        pOutTlmTeLinkInfo->u4CompIfIndex = (UINT4) pCompLink->i4IfIndex;
    }

    i4TeLinkNameLength = (INT4) TLM_STRLEN (pTeLink->au1TeLinkName);

    TLM_MEMCPY (&pOutTlmTeLinkInfo->au1TeLinkName[TLM_ZERO],
                pTeLink->au1TeLinkName, i4TeLinkNameLength);

    pOutTlmTeLinkInfo->LocalIpAddr = pTeLink->LocalIpAddr;

    pOutTlmTeLinkInfo->RemoteIpAddr = pTeLink->RemoteIpAddr;

    pOutTlmTeLinkInfo->u4RemoteRtrId = pTeLink->u4RemoteRtrId;

    pOutTlmTeLinkInfo->u4LocalIdentifier = pTeLink->u4LocalIdentifier;

    pOutTlmTeLinkInfo->u4RemoteIdentifier = pTeLink->u4RemoteIdentifier;

    pOutTlmTeLinkInfo->i4TeLinkType = pTeLink->i4TeLinkType;

    pOutTlmTeLinkInfo->u4RsrcClassColor = pTeLink->u4RsrcClassColor;

    pOutTlmTeLinkInfo->i4AddressType = pTeLink->i4AddressType;

    pOutTlmTeLinkInfo->u1OperStatus = pTeLink->u1OperStatus;
    pOutTlmTeLinkInfo->u4IfIndex = (UINT4) pTeLink->i4IfIndex;

    pTeIfDescr = TlmFindTeDescr (TLM_TELINK_DESCRID, pTeLink);

    if (pTeIfDescr != NULL)
    {
        pOutTlmTeLinkInfo->i4SwitchingCap = pTeIfDescr->i4SwitchingCap;

        pOutTlmTeLinkInfo->i4EncodingType = pTeIfDescr->i4EncodingType;
    }
    TLM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : TlmApiRegisterProtocols
 *
 * Description        : This function is used to register the applications
 *                      with TLM module
 *
 * Input(s)           : tTlmRegParams - Information of the application
 *                                      
 * Output(s)          : None
 * Return Value(s)    : TLM_SUCCESS/TLM_FAILURE
 *******************************************************************************/

UINT4
TlmApiRegisterProtocols (tTlmRegParams * pTlmRegParams)
{
    tTlmCfgMsg          CfgMsg;
    UINT4               u4ModId = TLM_ZERO;
    UINT4               u4EventsId = TLM_ZERO;

    MEMSET (&CfgMsg, TLM_ZERO, sizeof (tTlmCfgMsg));

    TLM_TRC_FN_ENTRY ();

    if (pTlmRegParams == NULL)
    {
        return TLM_FAILURE;
    }

    u4ModId = pTlmRegParams->u4ModId;

    if (u4ModId >= MAX_TLM_APPS)
    {
        return TLM_FAILURE;
    }

    TLM_LOCK ();

    if (gTlmGlobalInfo.apAppRegParams[u4ModId] == NULL)
    {
        gTlmGlobalInfo.apAppRegParams[u4ModId]
            = (tTlmRegParams *) MemAllocMemBlk (TLM_REG_MODULE_POOL_ID);

        if (gTlmGlobalInfo.apAppRegParams[u4ModId] == NULL)
        {
            TLM_UNLOCK ();
            return TLM_FAILURE;
        }

        MEMSET (gTlmGlobalInfo.apAppRegParams[u4ModId], TLM_ZERO,
                sizeof (tTlmRegParams));
    }

    u4EventsId = pTlmRegParams->u4EventsId;

    gTlmGlobalInfo.apAppRegParams[u4ModId]->u4ModId = u4ModId;
    gTlmGlobalInfo.apAppRegParams[u4ModId]->u4EventsId = u4EventsId;
    gTlmGlobalInfo.apAppRegParams[u4ModId]->pFnRcvPkt
        = pTlmRegParams->pFnRcvPkt;

    CfgMsg.u.TlmRtInfo.u4ModId = u4ModId;
    CfgMsg.u4MsgType = TLM_APP_REG_DREG_EVENT;

    /*Posting Application registration message to TLM message Queue
     * internally*/
    TlmEnqueueMsgToTlmQ (&CfgMsg);

    TLM_UNLOCK ();

    TLM_TRC_FN_EXIT ();
    return TLM_SUCCESS;
}

/******************************************************************************
 * Function           : TlmApiDeRegisterProtcols
 * Input(s)           : u4ModId - Application to be de-registered 
 * Output(s)          : None.
 * Returns            : TLM_SUCCESS/TLM_FAILURE.
 * Action             : Routine used by protocols to de-register with TLM.
 *******************************************************************************/
UINT4
TlmApiDeRegisterProtocols (UINT4 u4ModId)
{

    TLM_TRC_FN_ENTRY ();
    if ((u4ModId == TLM_ZERO) || (u4ModId >= MAX_TLM_APPS))
    {
        return TLM_FAILURE;
    }

    if ((gTlmGlobalInfo.apAppRegParams[u4ModId]) == NULL)
    {
        return TLM_FAILURE;
    }

    TLM_LOCK ();

    /* Releasing memory for the Application Id */
    if (MemReleaseMemBlock (TLM_REG_MODULE_POOL_ID,
                            (UINT1 *) gTlmGlobalInfo.apAppRegParams[u4ModId])
        != MEM_SUCCESS)
    {
        TLM_UNLOCK ();
        return TLM_FAILURE;
    }

    gTlmGlobalInfo.apAppRegParams[u4ModId] = NULL;

    TLM_UNLOCK ();
    TLM_TRC_FN_EXIT ();
    return TLM_SUCCESS;
}

/*****************************************************************************/
/* Function                  : TlmApiIfDeleteCallBack                        */
/*                                                                           */
/* Description               : This is the callback API provided by the TLM  */
/*                             module to the Interface manager in the system */
/*                             to notify about interface deletion that has   */
/*                             occured in the system. In ISS, this will be   */
/*                             invoked by the CFA module.                    */
/*                                                                           */
/* Input                     : pCfaRegInfo - Pointer to the CFA interface    */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Returns                   : TLM_SUCCESS/TLM_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
TlmApiIfDeleteCallBack (tCfaRegInfo * pCfaRegInfo)
{
    tTlmCfgMsg          CfgMsg;
    UINT1               u1IfType = TLM_ZERO;
    UINT1               u1BridgedIface = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();

    MEMSET (&CfgMsg, 0, sizeof (tTlmCfgMsg));

    if (gTlmGlobalInfo.i4ModuleStatus == TLM_DISABLED)
    {
        /* TLM module itself is not initialized. */
        return TLM_SUCCESS;
    }

    u1IfType = pCfaRegInfo->uCfaRegParams.CfaInterfaceInfo.u1IfType;
    u1BridgedIface = pCfaRegInfo->uCfaRegParams.CfaInterfaceInfo.u1BridgedIface;

    if ((u1IfType != CFA_TELINK) && (u1IfType != CFA_L3IPVLAN) &&
        ((u1IfType == CFA_ENET) && (u1BridgedIface != CFA_DISABLED)))
    {
        return TLM_SUCCESS;
    }

    CfgMsg.u.TlmCfaInfo.i4IfIndex = (INT4) pCfaRegInfo->u4IfIndex;
    CfgMsg.u.TlmCfaInfo.u1BridgedIface = u1BridgedIface;
    CfgMsg.u.TlmCfaInfo.u1IfType = u1IfType;
    CfgMsg.u.TlmCfaInfo.u1IfStatus = CFA_IF_DEL;
    CfgMsg.u4MsgType = TLM_CFA_INTF_DELETE;

    TlmEnqueueMsgToTlmQ (&CfgMsg);

    TLM_TRC_FN_EXIT ();
    return TLM_SUCCESS;
}

/*****************************************************************************/
/* Function                  : TlmApiIfOperChgCallBack                       */
/*                                                                           */
/* Description               : This is the callback API provided by the TLM  */
/*                             module to the Interface manager in the system */
/*                             to notify about interface status change that  */
/*                             occured for the interface.                    */
/*                                                                           */
/* Input                     : pCfaRegInfo - Pointer to the CFA interface    */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Returns                   : TLM_SUCCESS/TLM_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
TlmApiIfOperChgCallBack (tCfaRegInfo * pCfaRegInfo)
{
    tTlmCfgMsg          CfgMsg;
    UINT1               u1IfType = TLM_ZERO;
    UINT1               u1BridgedIface = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();

    MEMSET (&CfgMsg, 0, sizeof (tTlmCfgMsg));

    if (gTlmGlobalInfo.i4ModuleStatus == TLM_DISABLED)
    {
        /* TLM module itself is not initialized. */
        return TLM_SUCCESS;
    }

    u1IfType = pCfaRegInfo->uCfaRegParams.CfaInterfaceInfo.u1IfType;
    u1BridgedIface = pCfaRegInfo->uCfaRegParams.CfaInterfaceInfo.u1BridgedIface;

    if ((u1IfType != CFA_TELINK) && (u1IfType != CFA_L3IPVLAN) &&
        ((u1IfType == CFA_ENET) && (u1BridgedIface != CFA_DISABLED)))
    {
        return TLM_SUCCESS;
    }
    CfgMsg.u.TlmCfaInfo.i4IfIndex = (INT4) pCfaRegInfo->u4IfIndex;
    CfgMsg.u.TlmCfaInfo.u1IfType = u1IfType;
    CfgMsg.u.TlmCfaInfo.u1BridgedIface = u1BridgedIface;

    /* Oper up indication will be carried in CFA_IF_UP &
     * Oper down indication will be carried in CFA_IF_DOWN
     * */

    CfgMsg.u.TlmCfaInfo.u1IfStatus =
        pCfaRegInfo->uCfaRegParams.CfaInterfaceInfo.u1IfOperStatus;
    CfgMsg.u4MsgType = TLM_CFA_IFSTATUS_CHANGE;

    TlmEnqueueMsgToTlmQ (&CfgMsg);

    TLM_TRC_FN_EXIT ();
    return TLM_SUCCESS;
}

/*****************************************************************************/
/* Function                  : TlmCfaIfStatusUpdateHandler                   */
/*                                                                           */
/* Description               : This is the callback API provided by the TLM  */
/*                             module to the Interface manager in the system */
/*                             to notify about interface status change that  */
/*                             occured for the interface.                    */
/*                                                                           */
/* Input                     : u4MsgTYpe - Interface status change/Delete    */
/*                             i4IfIndex - Interface Index                   */
/*                             u1IfType  - Interface Type                    */
/*                             u1IfStatus - Interface Status(CFA_IF_UP /     */
/*                                                           CFA_IF_DOWN)    */
/*                             u1BridgedIface - Bridged Iface Status         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Returns                   : None                                          */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
TlmCfaIfStatusUpdateHandler (UINT4 u4MsgType, INT4 i4IfIndex,
                             UINT1 u1IfType, UINT1 u1IfStatus,
                             UINT1 u1BridgedIface)
{
    tTlmTeLink         *pTeIface = NULL;
    tTlmTeLink         *pBundleTeIface = NULL;
    tTlmComponentLink  *pCompIface = NULL;
    UINT1               u1BundleUpdate = TLM_ZERO;
    UINT1               u1UnBundleUpdate = TLM_ZERO;
    UINT4               u4HighIfIndex = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();
    switch (u4MsgType)
    {
        case TLM_CFA_IFSTATUS_CHANGE:
            if ((u1IfType == CFA_ENET) && (u1BridgedIface == CFA_DISABLED))
            {
                pCompIface = TlmFindComponentIf (i4IfIndex);

                if (pCompIface == NULL)
                {
                    break;
                }

                if (pCompIface->u1OperStatus == u1IfStatus)
                {
                    break;
                }

                pCompIface->u1OperStatus = u1IfStatus;
                TlmUpdateTeLinkOperStatus (pCompIface->pTeLink, u1IfStatus);
                if (u1IfStatus == TLM_OPER_DOWN)
                {
                    TlmUtilUpdateUnReservBw (pCompIface);
                    TlmTeDeleteTosortIfComponentLst (pCompIface,
                                                     pCompIface->pTeLink);
                    pCompIface->pTeLink = NULL;
                }
            }
            else if (u1IfType == CFA_TELINK)
            {
                pTeIface = TlmFindTeIf (i4IfIndex);

                if (pTeIface == NULL)
                {
                    break;
                }

                if (pTeIface->i4TeLinkType == TLM_BUNDLE)
                {
                    pTeIface->u1OperStatus = u1IfStatus;

                    if (u1IfStatus == TLM_OPER_DOWN)
                    {
                        u1BundleUpdate = TLM_BUNDLE_DELETE;
                    }
                    else
                    {
                        u1BundleUpdate = TLM_BUNDLE_UPDATE;
                    }

                    TlmUpdateUnBundleTeLinks (pTeIface, NULL, u1BundleUpdate);
                    TlmUpdateTeLinkOperStatus (pTeIface, u1IfStatus);
                }
                else if (pTeIface->i4TeLinkType == TLM_UNBUNDLE)
                {
                    if (pTeIface->u1OperStatus == u1IfStatus)
                    {
                       break;
		    }
                    TlmUpdateComponentLink (pTeIface, u1IfStatus);
                    if (u1IfStatus == TLM_OPER_DOWN)
                    {
                        u1BundleUpdate = TLM_TELINK_DELETE;
                        u1UnBundleUpdate = TLM_UNBUNDLE_DELETE;
                    }
                    else
                    {
                        u1BundleUpdate = TLM_TELINK_UPDATE;
                        u1UnBundleUpdate = TLM_UNBUNDLE_UPDATE;
                    }
                    /*Getting the bundle TE link of member TE link */
                    if (CfaApiGetHLTeLinkFromLLTeLink
                        ((UINT4) pTeIface->i4IfIndex, &u4HighIfIndex,
                         TRUE) == CFA_FAILURE)
                    {
                        break;
                    }

                    pBundleTeIface = TlmFindTeIf ((INT4) u4HighIfIndex);
                    /*Update the Bandwidth to Bundle TE link from member TE links */
                    TlmUpdateUnBundleTeLinks (pTeIface, pBundleTeIface,
                                              u1UnBundleUpdate);
                    TlmUpdateBundleTeLinkInfo (pTeIface, pBundleTeIface,
                                               u1BundleUpdate);
                    TlmUpdateTeLinkOperStatus (pTeIface, u1IfStatus);

                }
            }
            break;
        case TLM_CFA_INTF_DELETE:
            if (u1IfType == CFA_TELINK)
            {
                pTeIface = TlmFindTeIf (i4IfIndex);

                if (pTeIface != NULL)
                {
                    TlmExtDeleteTeLink (pTeIface);
                }
            }
            break;
        default:
            break;
    }
    TLM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : TlmApiGetIfIndexFromTeLinkName
 *
 * Description        : This function gets the TE Link IfIndex from TE Link Name
 *
 * Input(s)           : pu1TeLinkName - TE Link Name
 *
 * Output(s)          : pu4TeLinkIf - TE Link IfIndex
 *
 * Return Value(s)    : Tlm_SUCCESS / Tlm_FAILURE
 *****************************************************************************/

INT4
TlmApiGetIfIndexFromTeLinkName (UINT1 *pu1TeLinkName, UINT4 *pu4TeLinkIf)
{
    tTlmTeLink         *pTeLink = NULL;
    tTMO_HASH_TABLE    *pTeIfHashTable = gTlmGlobalInfo.pTeIfHashTable;
    UINT4               u4HashIndex = TLM_ZERO;

    if (gTlmGlobalInfo.i4ModuleStatus == TLM_DISABLED)
    {
        return TLM_FAILURE;
    }

    TLM_LOCK ();

    if (pTeIfHashTable == NULL)
    {
        TLM_UNLOCK ();
        return TLM_FAILURE;
    }

    TMO_HASH_Scan_Table (pTeIfHashTable, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (pTeIfHashTable, IF_HASH_FN (u4HashIndex),
                              pTeLink, tTlmTeLink *)
        {
            if (STRCMP (pTeLink->au1TeLinkName, pu1TeLinkName) == TLM_ZERO)
            {
                *pu4TeLinkIf = (UINT4) pTeLink->i4IfIndex;

                TLM_UNLOCK ();
                return TLM_SUCCESS;
            }
        }
    }

    TLM_UNLOCK ();

    return TLM_FAILURE;
}

/******************************************************************************
 * Function Name      : TlmApiSetDiffservParams
 *
 * Description        : This function sets diffserv-te related params
 *
 * Input(s)           : TlmDiffservInfo - Diffserv Info
 *
 * Output(s)          : None
 *
 * Return Value(s)    : NONE
 *****************************************************************************/

VOID
TlmApiSetDiffservParams (tTlmDiffservInfo * pTlmDiffservInfo)
{
    tTlmCfgMsg          CfgMsg;
    UINT1               u1ClassType = TLM_ZERO;
    UINT4               u4TeClass = TLM_ZERO;

    MEMSET (&CfgMsg, TLM_ZERO, sizeof (tTlmCfgMsg));

    for (u1ClassType = TLM_ZERO; u1ClassType < TLM_MAX_TE_CLASS_TYPE;
         u1ClassType++)
    {
        CfgMsg.u.TlmDiffservInfo.aTlmClassType[u1ClassType].u4BwPercentage =
            pTlmDiffservInfo->aTlmClassType[u1ClassType].u4BwPercentage;
    }

    for (u4TeClass = TLM_ZERO; u4TeClass < TLM_MAX_TE_CLASS; u4TeClass++)
    {
        if (pTlmDiffservInfo->aTlmTeClassMap[u4TeClass].u1IsSet == TLM_TRUE)
        {
            CfgMsg.u.TlmDiffservInfo.aTlmTeClassMap[u4TeClass].u1ClassType
                = pTlmDiffservInfo->aTlmTeClassMap[u4TeClass].u1ClassType;
            CfgMsg.u.TlmDiffservInfo.aTlmTeClassMap[u4TeClass].u1PremptPrio
                = pTlmDiffservInfo->aTlmTeClassMap[u4TeClass].u1PremptPrio;
            CfgMsg.u.TlmDiffservInfo.aTlmTeClassMap[u4TeClass].u1IsSet
                = pTlmDiffservInfo->aTlmTeClassMap[u4TeClass].u1IsSet;
        }
    }
    CfgMsg.u4MsgType = TLM_MPLS_DIFFSERV_TE_PARAMS;
    TlmEnqueueMsgToTlmQ (&CfgMsg);
    return;
}

/******************************************************************************
 * Function Name      : TlmMplsDiffservTeHandler
 * 
 * Description        : This function handles the diffser-te related message 
 *                      recieved from MPLS module
 *
 * Input(s)           : aTlmClassType - Class type array
 *
 *                      aTlmTeClassMap - TE class array
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Tlm_SUCCESS / Tlm_FAILURE
 *****************************************************************************/

VOID
TlmMplsDiffservTeHandler (tTlmClassType aTlmClassType[],
                          tTlmTeClassMap aTlmTeClassMap[])
{
    UINT1               u1ClassType = TLM_ZERO;
    UINT4               u4TeClass = TLM_ZERO;

    for (u1ClassType = TLM_ZERO; u1ClassType < TLM_MAX_TE_CLASS_TYPE;
         u1ClassType++)
    {
        gTlmGlobalInfo.aTlmClassType[u1ClassType].u4BwPercentage =
            aTlmClassType[u1ClassType].u4BwPercentage;
    }

    for (u4TeClass = TLM_ZERO; u4TeClass < TLM_MAX_TE_CLASS; u4TeClass++)
    {
        if (aTlmTeClassMap[u4TeClass].u1IsSet == TLM_TRUE)
        {
            gTlmGlobalInfo.aTlmTeClassMap[u4TeClass].u1ClassType
                = aTlmTeClassMap[u4TeClass].u1ClassType;
            gTlmGlobalInfo.aTlmTeClassMap[u4TeClass].u1PremptPrio
                = aTlmTeClassMap[u4TeClass].u1PremptPrio;
            gTlmGlobalInfo.aTlmTeClassMap[u4TeClass].u1IsSet
                = aTlmTeClassMap[u4TeClass].u1IsSet;
        }
    }
    return;
}

/******************************************************************************
 * Function Name      : TlmApiCreateFATeLink
 *
 * Description        : This function updates the FA TE-Link Information
 *
 * Input(s)           : pInTlmTeLinkInfo - Inputs to the TE link params
 *
 * Output(s)          : None
 *
 * Return Value(s)    : TLM_SUCCESS / TLM_FAILURE
 *****************************************************************************/
INT4
TlmApiCreateFATeLink (tInTlmTeLink * pInTlmTeLinkInfo,
                      tOutTlmTeLink * pOutTlmTeLink)
{
    INT4                i4RetVal = TLM_SUCCESS;

    TLM_TRC_FN_ENTRY ();

    TLM_LOCK ();
    i4RetVal = TlmCreateFATeLink (pInTlmTeLinkInfo, pOutTlmTeLink);
    TLM_UNLOCK ();

    TLM_TRC_FN_EXIT ();
    return i4RetVal;
}

/******************************************************************************
 * Function Name      : TlmCreateFATeLink
 *
 * Description        : This function updates the FA TE-Link Information
 *
 * Input(s)           : pInTlmTeLinkInfo - Inputs to the TE link params
 *
 * Output(s)          : None
 *
 * Return Value(s)    : TLM_SUCCESS / TLM_FAILURE
 *****************************************************************************/
INT4
TlmCreateFATeLink (tInTlmTeLink * pInTlmTeLinkInfo,
                   tOutTlmTeLink * pOutTlmTeLink)
{
    tTlmTeLink         *pTeLink = NULL;
    tTlmComponentLink  *pCompIface = NULL;
    tTlmTeIfDescr      *pTeLinkDesc = NULL;
    tTlmComponentIfDescr *pCompIfaceDesc = NULL;
    INT4                i4TeIfIndex = TLM_ZERO;
    INT4                i4Count = TLM_ZERO;
    UINT4               u4TeIfIndex = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();

    if (pInTlmTeLinkInfo == NULL)
    {
        TLM_TRC (TLM_FAILURE_TRC, "Input paramter is empty\n");
        return TLM_FAILURE;
    }

    pCompIface = TlmFindComponentIf ((INT4) pInTlmTeLinkInfo->u4CompIfIndex);

    if (pCompIface != NULL)
    {
        return (TlmUpdateFATeLink (pInTlmTeLinkInfo, pCompIface));
    }

    /* Create Component Link in TLM database */
    pCompIface = TlmComponentIfCreate ((INT4) pInTlmTeLinkInfo->u4CompIfIndex);

    if (pCompIface == NULL)
    {
        TLM_TRC (TLM_RESOURCE_TRC, "Memory Allocation Failed for "
                 "Component Link Creation \n");
        return TLM_FAILURE;
    }
    pCompIface->maxResBw = pInTlmTeLinkInfo->reqResBw;
    /* Create Component Link Descriptor in TLM database */
    pCompIfaceDesc = TlmCreateComponentDescr (TLM_ONE, pCompIface);

    if (pCompIfaceDesc == NULL)
    {
        TLM_TRC (TLM_RESOURCE_TRC, "Memory Allocation Failed for Component-Link"
                 " Descriptor Creation Failed\n");

        /* Creation Failed. So delete and Free the previously created
         * Entries */
        TlmDeleteComponentIf (pCompIface);
        return TLM_FAILURE;
    }

    pCompIfaceDesc->i4SwitchingCap = pInTlmTeLinkInfo->i4SwitchingCap;
    pCompIfaceDesc->i4EncodingType = pInTlmTeLinkInfo->i4EncodingType;
    pCompIfaceDesc->i4StorageType = TLM_STORAGE_VOLATILE;

    /* Get Free IfIndex for the TE_LINK from CFA */
    if (CfaGetFreeInterfaceIndex (&u4TeIfIndex, CFA_TELINK) == OSIX_FAILURE)
    {
        TLM_TRC (TLM_FAILURE_TRC, "Unable to get Free If Index from CFA\n");
        /* Creation Failed. So delete and Free the previously created
         * Entries */
        TlmDeleteComponentDescr (pCompIfaceDesc, pCompIface);
        TlmDeleteComponentIf (pCompIface);
        return TLM_FAILURE;
    }

    i4TeIfIndex = (INT4) u4TeIfIndex;

    /*Create an entry in IfTable for TeLink interface */
    if ((CfaApiCreateDynamicTeLinkInterface (u4TeIfIndex,
                                             pInTlmTeLinkInfo->u4CompIfIndex))
        == CFA_FAILURE)
    {
        TLM_TRC (TLM_FAILURE_TRC, "TE Link Creation Failed in CFA\n");
        /* Creation Failed. So delete and Free the previously created
         * Entries */
        TlmDeleteComponentDescr (pCompIfaceDesc, pCompIface);
        TlmDeleteComponentIf (pCompIface);
        return TLM_FAILURE;
    }

    /* Create Te Link in the TLM database */
    pTeLink = TlmCreateTeIf (i4TeIfIndex);
    if (pTeLink == NULL)
    {
        TLM_TRC (TLM_RESOURCE_TRC, "Memory Allocation Failed for TE-Link"
                 " Creation \n");
        /* Creation Failed. So delete and Free the previously created
         * Entries */
        TlmDeleteComponentDescr (pCompIfaceDesc, pCompIface);
        TlmDeleteComponentIf (pCompIface);
        CfaApiDeleteDynamicTeLink (u4TeIfIndex, CFA_TRUE);
        return TLM_FAILURE;
    }

    /* Create Te Link Descriptor in TLM database */
    pTeLinkDesc = TlmCreateTeDescr (TLM_ONE, pTeLink);
    if (pTeLinkDesc == NULL)
    {
        TLM_TRC (TLM_RESOURCE_TRC, "Memory Allocation Failed for TE-Link"
                 " Descriptor Creation Failed \n");
        /* Creation Failed. So delete and Free the previously created
         * Entries */
        TlmDeleteComponentDescr (pCompIfaceDesc, pCompIface);
        TlmDeleteComponentIf (pCompIface);
        TlmDeleteTeIf (pTeLink);
        CfaApiDeleteDynamicTeLink (u4TeIfIndex, CFA_TRUE);
        return TLM_FAILURE;
    }

    /* Since the pCompIface validity and pInTlmTeLinkInfo validity is done
     * before calling the function, the function is always expected to
     * be success one. */

    TlmUtilCalculateBCvalue (pCompIface);
    TlmUtilUpdateCompLinkInfo (pCompIface, pInTlmTeLinkInfo);
    TlmUpdateCompLinkOperstatus (pCompIface, TLM_OPER_UP);
    pCompIface->i4RowStatus = ACTIVE;
    pCompIface->i4StorageType = TLM_STORAGE_VOLATILE;

    /* Since the input arguments for this function is validated before,
     * no need to verify whether this function returns failure. */

    /* Fill Te Link Information based on component Link Information */
    TlmUpdateTeLinkInfo (pCompIface, pTeLink, TLM_COMPONENT_UPDATE);
    pTeLinkDesc->i4StorageType = TLM_STORAGE_VOLATILE;
    pTeLinkDesc->i4RowStatus = ACTIVE;

    for (i4Count = TLM_MIN_PRIORITY_LVL; i4Count < TLM_MAX_PRIORITY_LVL;
         i4Count++)
    {
        pTeLink->aUnResBw[i4Count].i4RowStatus = ACTIVE;
        pTeLink->aUnResBw[i4Count].i4StorageType = TLM_STORAGE_VOLATILE;
    }

    /* Fill the Te Link Information from pInTlmTeLinkInfo */
    SPRINTF ((CHR1 *) pTeLink->au1TeLinkName, "%s%d", "teLink",
             pInTlmTeLinkInfo->u4CompIfIndex);

    pTeLink->i4IsTeLinkAdvertise = pInTlmTeLinkInfo->i4IsTeLinkAdvtReqd;
    pTeLink->u4LocalIdentifier = pInTlmTeLinkInfo->u4LocalIdentifier;
    pTeLink->u4RemoteIdentifier = pInTlmTeLinkInfo->u4RemoteIdentifier;
    pTeLink->u4TeMetric = pInTlmTeLinkInfo->u4TeMetric;
    pTeLink->u4RsrcClassColor = pInTlmTeLinkInfo->u4RsrcClassColor;
    pTeLink->u4RemoteRtrId = pInTlmTeLinkInfo->u4RemoteRtrId;
    /* For FA TE-Link below values are constant and should not be modified */
    pTeLink->i4AddressType = TLM_ADDRESSTYPE_UNKNOWN;
    pTeLink->u1InfoType = TLM_FWD_ADJACENCY_CHANNEL;
    pTeLink->i4TeLinkIfType = TLM_POINT_TO_POINT;
    pTeLink->i4StorageType = TLM_STORAGE_VOLATILE;
    pTeLink->i4RowStatus = ACTIVE;
    TlmUpdateTeLinkOperStatus (pTeLink, TLM_OPER_UP);

    if (TlmAddTeLinkToIDBasedTree (pTeLink) == TLM_FAILURE)
    {
        TLM_TRC (TLM_FAILURE_TRC, "Failed to Add node in the "
                 "Global RBTree\n");
        TlmDeleteTeDescr (pTeLinkDesc, pTeLink);
        TlmDeleteTeIf (pTeLink);
        CfaApiDeleteDynamicTeLink (u4TeIfIndex, CFA_TRUE);
        TlmDeleteComponentDescr (pCompIfaceDesc, pCompIface);
        TlmDeleteComponentIf (pCompIface);
        return TLM_FAILURE;
    }

    TlmFillTeLinkOutInfo (pTeLink, pOutTlmTeLink);

    TLM_TRC_FN_EXIT ();
    return TLM_SUCCESS;
}

/******************************************************************************
 * Function Name      : TlmApiDeleteFATeLink
 *
 * Description        : This function Deletes the FA TE-Link Information
 *
 * Input(s)           : pInTlmTeLinkInfo - Inputs to the TE link params
 *
 * Output(s)          : None
 *
 * Return Value(s)    : TLM_SUCCESS / TLM_FAILURE
 *****************************************************************************/
INT4
TlmApiDeleteFATeLink (tInTlmTeLink * pInTlmTeLinkInfo)
{
    INT4                i4RetVal = TLM_SUCCESS;

    TLM_TRC_FN_ENTRY ();
    TLM_LOCK ();
    i4RetVal = TlmDeleteFATeLink (pInTlmTeLinkInfo);
    TLM_UNLOCK ();

    TLM_TRC_FN_EXIT ();
    return i4RetVal;
}

/******************************************************************************
 * Function Name      : TlmDeleteFATeLink
 *
 * Description        : This function Deletes the FA TE-Link Information
 *
 * Input(s)           : pInTlmTeLinkInfo - Inputs to the TE link params
 *
 * Output(s)          : None
 *
 * Return Value(s)    : TLM_SUCCESS / TLM_FAILURE
 *****************************************************************************/
INT4
TlmDeleteFATeLink (tInTlmTeLink * pInTlmTeLinkInfo)
{
    tTlmTeLink         *pTeLink = NULL;
    tTlmComponentLink  *pCompIface = NULL;
    INT4                i4TeIfIndex = TLM_ZERO;
    UINT4               u4TeIfIndex = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();

    if (pInTlmTeLinkInfo == NULL)
    {
        TLM_TRC (TLM_FAILURE_TRC, "Input paramter is empty\n");
        return TLM_FAILURE;
    }

    /* Get the TE Link If Index from the Given component If Index
     * (from Mpls Tunnel IfIndex) */
    CfaApiGetTeLinkIfFromL3If (pInTlmTeLinkInfo->u4CompIfIndex,
                               &u4TeIfIndex, TLM_ONE, TRUE);

    i4TeIfIndex = (INT4) u4TeIfIndex;
    pTeLink = TlmFindTeIf (i4TeIfIndex);
    if (pTeLink == NULL)
    {
        TLM_TRC (TLM_FAILURE_TRC, "No corresponding TE Link Entry Found\n");
        return TLM_FAILURE;
    }

    pCompIface = TlmFindComponentIf ((INT4) pInTlmTeLinkInfo->u4CompIfIndex);

    if (pCompIface == NULL)
    {
        TLM_TRC (TLM_FAILURE_TRC, "No Component Link Information Found "
                 "in TLM database\n");
        return TLM_FAILURE;
    }

    TlmUtilSendNotifMsgInfo (pTeLink, TE_LINK_DELETED);
    TlmDeleteComponentIf (pCompIface);
    TlmDeleteTeIf (pTeLink);
    CfaApiDeleteDynamicTeLink (u4TeIfIndex, CFA_TRUE);

    if (TlmDeleteTeLinkIDBasedTree (pTeLink) == TLM_FAILURE)
    {
        TLM_TRC (TLM_FAILURE_TRC, "Failed to Delete node in the "
                 "Global RBTree\n");
        return TLM_FAILURE;
    }

    TLM_TRC_FN_EXIT ();

    return TLM_SUCCESS;

}

/******************************************************************************
 * Function Name      : TlmUpdateFATeLink
 *
 * Description        : This function updates the FA TE-Link Information
 *
 * Input(s)           : pInTlmTeLinkInfo - Inputs to the TE link params
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Tlm_SUCCESS / Tlm_FAILURE
 *****************************************************************************/
INT4
TlmUpdateFATeLink (tInTlmTeLink * pInTlmTeLinkInfo,
                   tTlmComponentLink * pCompIface)
{
    tTlmTeLink         *pTeLink = NULL;
    INT4                i4TeIfIndex = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();
    if (pInTlmTeLinkInfo == NULL)
    {
        TLM_TRC (TLM_FAILURE_TRC, "Input paramter is empty\n");
        return TLM_FAILURE;
    }

    /* Get the TE Link If Index from the Given component If Index
     * (from Mpls Tunnel IfIndex) */

    CfaApiGetTeLinkIfFromL3If (pInTlmTeLinkInfo->u4CompIfIndex,
                               (UINT4 *) &i4TeIfIndex, TLM_ONE, TRUE);

    if (i4TeIfIndex == TLM_ZERO)
    {
        TLM_TRC (TLM_FAILURE_TRC, "No corresponding FA TE-Link entry "
                 "found for the given mplsTunnel Interface \n");
        return TLM_FAILURE;
    }
    /* Get the TE Link Entry from the TE Link If Index and 
     * update the parameters passed through pInTlmTeLinkInfo */
    pTeLink = TlmFindTeIf (i4TeIfIndex);

    if (pTeLink == NULL)
    {
        TLM_TRC (TLM_FAILURE_TRC, "No TE Link Information Found "
                 "in TLM database\n");
        return TLM_FAILURE;
    }

    /* Since the pCompIface validity and pInTlmTeLinkInfo validity is done
     * before calling the function, the function is always expected to
     * be success one. */

    TlmUtilUpdateCompLinkInfo (pCompIface, pInTlmTeLinkInfo);

    /* Since the input arguments for this function is validated before,
     * no need to verify whether this function returns failure. */

    /* Fill Te Link Information based on component Link Information */
    TlmUpdateTeLinkInfo (pCompIface, pTeLink, TLM_COMPONENT_UPDATE);

    /* Fill the Te Link Information from pInTlmTeLinkInfo */
    pTeLink->i4IsTeLinkAdvertise = pInTlmTeLinkInfo->i4IsTeLinkAdvtReqd;
    pTeLink->u4LocalIdentifier = pInTlmTeLinkInfo->u4LocalIdentifier;
    pTeLink->u4RemoteIdentifier = pInTlmTeLinkInfo->u4RemoteIdentifier;
    pTeLink->u4TeMetric = pInTlmTeLinkInfo->u4TeMetric;
    pTeLink->u4RemoteRtrId = pInTlmTeLinkInfo->u4RemoteRtrId;
    /* For FA TE-Link below values are constant and should not be modified */
    pTeLink->i4AddressType = TLM_ADDRESSTYPE_UNKNOWN;
    pTeLink->u1InfoType = TLM_FWD_ADJACENCY_CHANNEL;
    pTeLink->i4TeLinkIfType = TLM_POINT_TO_POINT;
    pTeLink->i4StorageType = TLM_STORAGE_VOLATILE;
    pTeLink->u1OperStatus = TLM_OPER_UP;
    pTeLink->i4RowStatus = ACTIVE;

    TlmUtilSendNotifMsgInfo (pTeLink, TE_LINK_UPDATED);

    TLM_TRC_FN_EXIT ();
    return TLM_SUCCESS;

}

/******************************************************************************
 * Function Name      : TlmApiAssociateCtrlIfAndDataIf
 *
 * Description        : This function associates control mpls interface with
 *                      data te link interface
 *
 * Input(s)           : u4CtrlMplsIfIndex - control mpls interface
 *
 *                      u4DataTeIfIndex - data telink Index
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Tlm_SUCCESS / Tlm_FAILURE
 *****************************************************************************/
INT4
TlmApiAssociateCtrlIfAndDataIf (UINT4 u4CtrlMplsIfIndex, UINT4 u4DataTeIfIndex)
{
    tTlmTeLink         *pTeIface = NULL;

    TLM_LOCK ();
    pTeIface = TlmFindTeIf ((INT4) u4DataTeIfIndex);

    if (pTeIface == NULL)
    {
        TLM_TRC (TLM_FAILURE_TRC, "No TE Link Information Found "
                 "in TLM database\n");
        TLM_UNLOCK ();
        return TLM_FAILURE;
    }

    pTeIface->i4CtrlMplsIfIndex = (INT4) u4CtrlMplsIfIndex;
    TLM_UNLOCK ();
    return TLM_SUCCESS;
}
