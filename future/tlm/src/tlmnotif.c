/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: tlmnotif.c,v 1.7 2014/07/09 13:23:18 siva Exp $
*
* Description: This file contains the routines for TLM notifications
*              to the external modules
*********************************************************************/

#include "tlminc.h"
#include "tlmcli.h"

/*****************************************************************************/
/*  Function    :  TlmUtilSendNotifMsgInfo                                   */
/*                                                                           */
/*  Description :  This function creates the message and fill the message    */
/*                 with link information                                     */
/*                                                                           */
/*  Input       :  pTeIface - TE Link interface                              */
/*                 u4EventType - Event Type                                  */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to message with TE Link information               */
/*                 NULL otherwise.                                           */
/*****************************************************************************/
PUBLIC INT1
TlmUtilSendNotifMsgInfo (tTlmTeLink * pTeIface, UINT4 u4EventType)
{
    tTlmEventNotification NotifMsg;
    tTlmTeLink         *pMemberTeLink = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    INT1                i1RetVal = TLM_FAILURE;
    UINT4               u4BundleEvtType = TLM_ZERO;
    UINT4               u4UnBundleEvtType = TLM_ZERO;

    MEMSET (&NotifMsg, TLM_ZERO, sizeof (tTlmEventNotification));

    if (pTeIface == NULL)
    {
        return i1RetVal;
    }

    if (pTeIface->i4IsTeLinkAdvertise == TLM_FALSE)
    {
        return TLM_SUCCESS;
    }

    if ((u4EventType == TE_LINK_CREATED) || (u4EventType == TE_LINK_UPDATED))
    {
        /* Avoiding sending of notifications when row status is
         * NOT_IN_SERVICE */
        if ((pTeIface->u1OperStatus != TLM_OPER_UP) &&
            (pTeIface->i4RowStatus != ACTIVE))
        {
            return i1RetVal;
        }
    }

    if (u4EventType == TE_LINK_CREATED)
    {
        if (pTeIface->i4PrevOperStatus == TE_LINK_CREATED)
        {
            u4EventType = TE_LINK_UPDATED;
        }
    }

    if (u4EventType == TE_LINK_DELETED)
    {
        NotifMsg.u4IfIndex = (UINT4) pTeIface->i4IfIndex;
        NotifMsg.u4EventType = u4EventType;
        NotifMsg.u1LinkStatus = (UINT1) u4EventType;
        NotifMsg.u1MsgSubType = TLM_LINK_INFO;

        i1RetVal = TlmUtilNotifyProtocols (&NotifMsg);
    }
    else if ((pTeIface->i4TeLinkType == TLM_UNBUNDLE)
             && (pTeIface->pBundledTeLink == NULL))
    {
        TlmUtilFillEvenNotifMsg (pTeIface, u4EventType, &NotifMsg);

        i1RetVal = TlmUtilNotifyProtocols (&NotifMsg);
    }
    else if (pTeIface->i4TeLinkType == TLM_BUNDLE)
    {
        if (u4EventType == TE_LINK_DELETED)
        {
            u4BundleEvtType = TE_LINK_DELETED;
            u4UnBundleEvtType = TE_LINK_CREATED;
        }
        else if (u4EventType == TE_LINK_CREATED)
        {
            u4BundleEvtType = TE_LINK_CREATED;
            u4UnBundleEvtType = TE_LINK_DELETED;
        }
        else
        {
            u4BundleEvtType = TE_LINK_UPDATED;
        }

        /*Send Delete notification for unbundled TE links, once they are configured
         * under a single TE link as bundled link.
         * Later send create notification for the new bundled TE link*/

        if (u4EventType != TE_LINK_UPDATED)
        {
            TMO_SLL_Scan (&(pTeIface->sortUnbundleTeList), pLstNode,
                          tTMO_SLL_NODE *)
            {
                pMemberTeLink = GET_UNBUNDLE_TE_LINK_LIST (pLstNode);
                TlmUtilFillEvenNotifMsg (pMemberTeLink, u4UnBundleEvtType,
                                         &NotifMsg);
                i1RetVal = TlmUtilNotifyProtocols (&NotifMsg);

                MEMSET (&NotifMsg, TLM_ZERO, sizeof (tTlmEventNotification));
            }
        }

        MEMSET (&NotifMsg, TLM_ZERO, sizeof (tTlmEventNotification));

        TlmUtilFillEvenNotifMsg (pTeIface, u4BundleEvtType, &NotifMsg);

        i1RetVal = TlmUtilNotifyProtocols (&NotifMsg);
    }
    if (i1RetVal == TLM_SUCCESS)
    {
        pTeIface->i4PrevOperStatus = (INT4) u4EventType;
    }

    return i1RetVal;
}

/*****************************************************************************/
/*  Function    :  TlmUtilFillEvenNotifMsg                                   */
/*                                                                           */
/*  Description :  This function creates the message and fill the message    */
/*                 with link information                                     */
/*                                                                           */
/*  Input       :  pTeIface - Holds pointer to TE interface                  */
/*                 u4EventType - Event Type                                  */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to message with TE Link information               */
/*                 NULL otherwise.                                           */
/*****************************************************************************/

PUBLIC INT1
TlmUtilFillEvenNotifMsg (tTlmTeLink * pTeIface, UINT4 u4EventType,
                         tTlmEventNotification * pRetNotifMsg)
{
    tTlmEventNotification NotifMsg;
    tTlmTeIfDescr      *pTeIfDescr = NULL;
    tTlmTeLinkSrlg     *pTeSrlg = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT4               u4IfDescCnt = TLM_ZERO;
    UINT4               u4SrlgCnt = TLM_ZERO;
    tTlmComponentLink  *pCompLink = NULL;
    UINT4               u4CompIndex = TLM_ZERO;
    UINT4               u4MplsIfIndex = TLM_ZERO;
    UINT4               u4LowerCompIfCfaPortIndex = TLM_ZERO;
    UINT1               u1Count = TLM_ZERO;
    UINT1               u1DescCount = TLM_ZERO;
    UINT1               u1SrlgCount = TLM_ZERO;

    MEMSET (&NotifMsg, TLM_ZERO, sizeof (tTlmEventNotification));

    NotifMsg.u4EventType = u4EventType;
    NotifMsg.u1LinkStatus = (UINT1) u4EventType;
    NotifMsg.u1MsgSubType = TLM_LINK_INFO;
    NotifMsg.maxBw = pTeIface->maxBw;
    NotifMsg.maxResBw = pTeIface->maxResBw;

    for (u1Count = TLM_ZERO; u1Count < TLM_MAX_TE_CLASS; u1Count++)
    {
        NotifMsg.aUnResBw[u1Count] = pTeIface->aUnResBw[u1Count].unResBw;
    }

    NotifMsg.LocalIpAddr = pTeIface->LocalIpAddr;
    NotifMsg.RemoteIpAddr = pTeIface->RemoteIpAddr;
    NotifMsg.u4RemoteRtrId = pTeIface->u4RemoteRtrId;

    NotifMsg.u4IfIndex = (UINT4) pTeIface->i4IfIndex;
    NotifMsg.u4MplsIfIndex = pTeIface->u4MplsIfIndex;

    NotifMsg.u4TeMetric = pTeIface->u4TeMetric;
    NotifMsg.u4RsrcClassColor = pTeIface->u4RsrcClassColor;

    NotifMsg.u4LocalIdentifier = pTeIface->u4LocalIdentifier;
    NotifMsg.u4RemoteIdentifier = pTeIface->u4RemoteIdentifier;

    NotifMsg.u4WorkingPriority = pTeIface->u4WorkingPriority;
    NotifMsg.i4AddressType = pTeIface->i4AddressType;
    NotifMsg.i4ProtectionType = pTeIface->i4ProtectionType;
    NotifMsg.u1InfoType = pTeIface->u1InfoType;
    NotifMsg.u4TeLinkIfType = (UINT4) pTeIface->i4TeLinkIfType;

    u4IfDescCnt = TMO_SLL_Count (&pTeIface->ifDescrList);

    if (u4IfDescCnt != TLM_ZERO)
    {
        TMO_SLL_Scan (&pTeIface->ifDescrList, pLstNode, tTMO_SLL_NODE *)
        {
            pTeIfDescr = GET_TE_DESCR_PTR_FROM_SORT_LST (pLstNode);
            if (pTeIfDescr->i4RowStatus != ACTIVE)
            {
                continue;
            }

            for (u1Count = TLM_ZERO; u1Count < TLM_MAX_TE_CLASS; u1Count++)
            {
                NotifMsg.aIfDescriptors[u1DescCount].aDescMaxLSPBw[u1Count] =
                    pTeIfDescr->aMaxLSPBwPrio[u1Count];
            }
            NotifMsg.aIfDescriptors[u1DescCount].minLSPBw =
                pTeIfDescr->minLSPBw;
            NotifMsg.aIfDescriptors[u1DescCount].u4DescrId =
                pTeIfDescr->u4DescrId;
            NotifMsg.aIfDescriptors[u1DescCount].u2MTU = pTeIfDescr->u2MTU;
            NotifMsg.aIfDescriptors[u1DescCount].u1SwitchingCap =
                (UINT1) pTeIfDescr->i4SwitchingCap;
            NotifMsg.aIfDescriptors[u1DescCount].u1EncodingType =
                (UINT1) pTeIfDescr->i4EncodingType;
            NotifMsg.aIfDescriptors[u1DescCount].u1Indication =
                (UINT1) pTeIfDescr->i4Indication;

            u1DescCount++;

            if (u1DescCount > TLM_MAX_DESCRIPTOR_NO)
            {
                break;
            }

        }
        NotifMsg.u4IfDescCnt = u1DescCount;
    }

    u4SrlgCnt = TMO_SLL_Count (&pTeIface->srlgList);

    if (u4SrlgCnt != TLM_ZERO)
    {
        TMO_SLL_Scan (&pTeIface->srlgList, pLstNode, tTMO_SLL_NODE *)
        {
            pTeSrlg = GET_TE_SRLG_PTR_FROM_SORT_LST (pLstNode);
            if (pTeSrlg->i4RowStatus != ACTIVE)
            {
                continue;
            }
            if (u1SrlgCount >= TLM_MAX_SRLG_NO)
            {
                break;
            }

            NotifMsg.Srlg.aSrlgNumber[u1SrlgCount] = pTeSrlg->u4SrlgNo;
            NotifMsg.Srlg.u4NoOfSrlg++;
            u1SrlgCount++;
        }
    }
    if (pTeIface->u1InfoType == TLM_FWD_ADJACENCY_CHANNEL)
    {
        if ((pLstNode = TMO_SLL_First (&(pTeIface->ComponentList))) != NULL)
        {
            pCompLink = GET_COMPONENT_IF_PTR_FROM_SORT_TE_LST (pLstNode);
        }
        if (pCompLink != NULL)
        {
            u4CompIndex = (UINT4) pCompLink->i4IfIndex;
        }
        /* For FA TeLink, u4CompIndex is always a MPLS TUNNEL Interface Index */
        CfaUtilGetMplsIfFromMplsTnlIf (u4CompIndex, &u4MplsIfIndex, CFA_TRUE);
        if (CfaUtilGetL3IfFromMplsIf
            (u4MplsIfIndex, &u4LowerCompIfCfaPortIndex,
             CFA_TRUE) == CFA_FAILURE)
        {
            TLM_TRC (TLM_FAILURE_TRC,
                     "TlmUtilFillEvenNotifMsg: Failed to get L3 Index from Mpls Index\n");
        }
        NotifMsg.u4LowerCompIfCfaPortIndex = u4LowerCompIfCfaPortIndex;
    }

    MEMCPY (pRetNotifMsg, &NotifMsg, sizeof (tTlmEventNotification));

    return TLM_SUCCESS;
}

/*****************************************************************************
 * Function Name      : TlmUtilNotifyProtocols                               *
 *                                                                           *
 * Description        : Routine used to notify various applications of the   *
 *                      events for which they have registered                *
 *                                                                           *
 * Input(s)           : pTlmEventNotification - Pointer to the information   *
 *                      which will be passed to the application              *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : TLM_FAILURE/TLM_Success                              *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT1
TlmUtilNotifyProtocols (tTlmEventNotification * pTlmEventNotification)
{
    UINT4               u4ModId = TLM_INIT_VAL;
    UINT4               u4EventType = TLM_INIT_VAL;
    INT1                i1RetVal = OSIX_FAILURE;

    /* Get the event ID for which the function is called.
     * Based on the Event the notification will be sent to the 
     * registered applications.
     */
    u4EventType = pTlmEventNotification->u4EventType;
    for (u4ModId = TLM_APP_ID; u4ModId < TLM_MAX_APPS; u4ModId++)
    {
        /* Check for the Module id registered with TLM and 
         * indicate the module.
         */
        if ((gTlmGlobalInfo.apAppRegParams[u4ModId] != NULL) &&
            (gTlmGlobalInfo.apAppRegParams[u4ModId]->pFnRcvPkt != NULL) &&
            ((gTlmGlobalInfo.apAppRegParams[u4ModId]->u4EventsId & u4EventType)
             != TLM_INIT_VAL))
        {
            i1RetVal
                = (*(gTlmGlobalInfo.apAppRegParams[u4ModId]->pFnRcvPkt))
                (pTlmEventNotification);
        }
    }

    i1RetVal = (i1RetVal == OSIX_SUCCESS) ? TLM_SUCCESS : TLM_FAILURE;

    return i1RetVal;
}

/*****************************************************************************
 * Function Name      : TlmUtilFloodBwForThreshold                           *
 *                                                                           *
 * Description        : This function is used to flood the bandwidth change  *
 *                      of a Te-Link                                         * 
 *                                                                           *
 * Input(s)           : pTeLink  - Te-Link entry                             *
 *                      changeBwPerc - Change of Bw in percentage            *                      
 *                      u1IsBwInc - TRUE if the Bandwidth increased          *
 *                                                                           *
 * Output(s)          : None.                                                *
 *                                                                           *
 * Return Value(s)    : TLM_FAILURE/TLM_SUCCESS                              *
 *                                                                           *
 *****************************************************************************/
VOID
TlmUtilFloodBwForThreshold (tTlmTeLink * pTeLink, tBandWidth changeBwPerc,
                            INT4 i4BwChange)
{
    UNUSED_PARAM (i4BwChange);
    if (changeBwPerc == TLM_ZERO)
    {
        return;
    }

    TlmUtilSendNotifMsgInfo (pTeLink, TE_LINK_UPDATED);
    return;
}
