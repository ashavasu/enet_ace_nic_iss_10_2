/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: tlmmain.c,v 1.5 2012/05/28 13:42:05 siva Exp $
*
* Description: This file contains entry point function to TLM and its 
*              supporting functions
* *********************************************************************/
#ifndef _TLMMAIN_C_
#define _TLMMAIN_C_

#include "tlminc.h"

/* Prototypes for the routines used in this file */
PRIVATE INT4 TlmTaskInit PROTO ((VOID));
PRIVATE INT4 TlmProcessQMsg PROTO ((UINT4));
PRIVATE VOID TlmTaskInitFailure PROTO ((VOID));
PRIVATE VOID TlmCfgQueueHandler PROTO ((tTlmCfgMsg *));

/******************************************************************************
 * Function Name      : TlmMain
 *
 * Description        : This is the entry routine for the Te-Link Manager. 
 *
 * Input(s)           : pArg - pointer to the Arguments
 *
 * Output(s)          : None
 *
 * Return Value(s)    : VOID
 *****************************************************************************/
VOID
TlmTaskMain (INT1 *pArg)
{
    UINT4               u4RcvEvents = TLM_ZERO;

    UNUSED_PARAM (pArg);

    /* Initialize the Tlm Task */
    if (TlmTaskInit () != TLM_SUCCESS)
    {
        TlmTaskInitFailure ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    RegisterSTDTLM ();
    RegisterFSTLM ();

    lrInitComplete (OSIX_SUCCESS);

    OsixTskIdSelf ((tOsixTaskId *) & TLM_TASK_ID);

    while (TLM_ONE)
    {
        if (OsixEvtRecv (TLM_TASK_ID, TLM_EV_ALL,
                         OSIX_WAIT, &u4RcvEvents) == OSIX_SUCCESS)
        {
            TlmProcessQMsg (u4RcvEvents);
        }
    }
}

 /******************************************************************************
 * Function Name      : TlmTaskInit
 *
 * Description        : This routine is used to initialize the data structure,
 *                      Initialize memory pools,Create Hash Table structure,  
 *                      Create Semaphore and Configuration message Queue.       
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : TLM_SUCCESS / Tlm_FAILURE
 *****************************************************************************/
PRIVATE INT4
TlmTaskInit ()
{

    UINT1               u1Counter = TLM_ZERO;
    /* Reset Global Info to all zeros */
    MEMSET (&gTlmGlobalInfo, TLM_ZERO, sizeof (tTlmGlobalInfo));

    gTlmGlobalInfo.i4TraceOption = TLM_CRITICAL_TRC;

    /* Creating a Configuration Queue */
    if (OsixCreateQ ((const UINT1 *) "TLMQ", MAX_TLM_QUEUE_SIZE,
                     (UINT4) 0, (tOsixQId *) & gTlmGlobalInfo.CfgQId)
        != OSIX_SUCCESS)
    {
        TLM_TRC (TLM_CRITICAL_TRC,
                 "TlmTaskInit: Configuration Queue Creation Failed\n");
        return TLM_FAILURE;
    }

    /* Creating a TLM Semaphore */
    if (OsixSemCrt
        (TLM_MUT_EXCL_SEM_NAME,
         (tOsixSemId *) & gTlmGlobalInfo.SemId) != OSIX_SUCCESS)
    {
        TLM_TRC (TLM_CRITICAL_TRC, "TlmTaskInit: Semaphore Creation Failed\n");
        return TLM_FAILURE;
    }

    OsixSemGive (gTlmGlobalInfo.SemId);

    if (TlmSizingMemCreateMemPools () != OSIX_SUCCESS)
    {
        TLM_TRC (TLM_CRITICAL_TRC,
                 "TlmTaskInit: Memory Initialization Failed\n");
        return TLM_FAILURE;
    }

    /* By Default Bandwidth constraint model will be Russian Dolls Model */
    gTlmGlobalInfo.u1BCMSupported = TLM_RUSSIAN_DOLLS_MODEL;

    /* Create TE-Link If Index based Hash Table  */
    gTlmGlobalInfo.pTeIfHashTable =
        TMO_HASH_Create_Table (IF_HASH_TABLE_SIZE, NULL, FALSE);

    /* Initialize Sorted TE-Link If Index based List */
    TMO_SLL_Init (&(gTlmGlobalInfo.sortTeIfLst));

    /* Create Component TE-Link If Index based Hash Table */
    gTlmGlobalInfo.pComponentIfHashTable =
        TMO_HASH_Create_Table (COMPONENT_IF_HASH_TABLE_SIZE, NULL, FALSE);

    /* Initialize Sorted Component TE-Link If Index based List */
    TMO_SLL_Init (&(gTlmGlobalInfo.sortComponentIfLst));

    /* Create TE-Link If Name based Hash Table */
    gTlmGlobalInfo.pTeLinkNameIfHashTable =
        TMO_HASH_Create_Table (IF_HASH_TABLE_SIZE, NULL, FALSE);

    TlmCreateTeLinkTree ();

    gTlmGlobalInfo.u1BwForceThresholdEnable = TLM_DISABLED;

    gTlmGlobalInfo.i4ModuleStatus = TLM_DISABLED;

    gTlmGlobalInfo.i4TraceOption = TLM_ZERO;
    /*For diffserv disabled case-currently only class0 supported */
    /*In the case of Diffserv enabled,take classtype and priority configured by MPLS */
    TLM_CLASS_TYPE_SUPPORTED = 7;
    for (u1Counter = TLM_ZERO; u1Counter < TLM_MAX_TE_CLASS; u1Counter++)
    {
        /*For testing  with classtype 0 for RDM */
        TLM_TE_CLASS_INFO (u1Counter).u1ClassType = TLM_ZERO;
        TLM_TE_CLASS_INFO (u1Counter).u1PremptPrio = u1Counter;
    }
    gTlmGlobalInfo.aTlmClassType[TLM_ZERO].u4BwPercentage = 100;

    return TLM_SUCCESS;
}

/******************************************************************************
 * Function Name      : TlmTaskInitFailure
 *
 * Description        : This function is called when the Initialization of
 *                      the TLM Task fails at any point during initialization
 * Input(s)           : None
 * Output(s)          : None
 * Return Value(s)    : None
 * ****************************************************************************/

PRIVATE VOID
TlmTaskInitFailure (VOID)
{
    /* Delete the TLM Semaphore */
    if (gTlmGlobalInfo.SemId != TLM_ZERO)
    {
        OsixSemDel (gTlmGlobalInfo.SemId);
        gTlmGlobalInfo.SemId = TLM_ZERO;
    }

    /* Delete the Configuration Queue */
    if (gTlmGlobalInfo.CfgQId != TLM_ZERO)
    {
        OsixQueDel (gTlmGlobalInfo.CfgQId);
        gTlmGlobalInfo.CfgQId = TLM_ZERO;
    }

    TlmSizingMemDeleteMemPools ();;

    /* Delete Te IfHash Table */
    if (gTlmGlobalInfo.pTeIfHashTable != NULL)
    {
        TMO_HASH_Delete_Table (gTlmGlobalInfo.pTeIfHashTable, NULL);
        gTlmGlobalInfo.pTeIfHashTable = NULL;
    }

    TlmDeleteTeLinkTree();

    /* Delete Component IfHash Table */
    if (gTlmGlobalInfo.pComponentIfHashTable != NULL)
    {
        TMO_HASH_Delete_Table (gTlmGlobalInfo.pComponentIfHashTable, NULL);
        gTlmGlobalInfo.pComponentIfHashTable = NULL;
    }

    /* Delete Component IfHash Table */
    if (gTlmGlobalInfo.pTeLinkNameIfHashTable != NULL)
    {
        TMO_HASH_Delete_Table (gTlmGlobalInfo.pTeLinkNameIfHashTable, NULL);
        gTlmGlobalInfo.pTeLinkNameIfHashTable = NULL;
    }
    return;
}

/******************************************************************************
 * Function Name      : TlmProcessQMsg
 *
 * Description        : This routine deque the Configuration Queue Messages and
 *                      QueueHandler process the messages.
 *
 * Input(s)           : u4Event - Event to indicate messages present in the
 *                      message queues.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Tlm_SUCCESS
 *****************************************************************************/
PRIVATE INT4
TlmProcessQMsg (UINT4 u4Event)
{
    tTlmCfgMsg         *pQMsg = NULL;

    if (u4Event & TLM_EV_ALL)
    {
        while (OsixQueRecv (gTlmGlobalInfo.CfgQId, (UINT1 *) &pQMsg,
                            OSIX_DEF_MSG_LEN, TLM_ZERO) == OSIX_SUCCESS)
        {
            TLM_LOCK ();
            TlmCfgQueueHandler (pQMsg);
            MemReleaseMemBlock (TLMMemPoolIds[MAX_TLM_QUEUE_SIZE_SIZING_ID],
                                (UINT1 *) pQMsg);
            TLM_UNLOCK ();
        }
    }
    return TLM_SUCCESS;
}

/******************************************************************************
 * Function Name      : TlmCfgQueueHandler
 *
 * Description        : This process messages on the Queue
 *
 * Input(s)           : pQMsg - pointer to the configuration message received
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Tlm_SUCCESS / Tlm_FAILURE
 *****************************************************************************/
PRIVATE VOID
TlmCfgQueueHandler (tTlmCfgMsg * pQMsg)
{
    switch (pQMsg->u4MsgType)
    {
        case TLM_APP_REG_DREG_EVENT:
            TlmUpdateInfoToRegProtocol (pQMsg->u.TlmRtInfo.u4ModId);
            break;

        case TLM_CFA_IFSTATUS_CHANGE:
        case TLM_CFA_INTF_DELETE:
            TlmCfaIfStatusUpdateHandler (pQMsg->u4MsgType,
                                         pQMsg->u.TlmCfaInfo.i4IfIndex,
                                         pQMsg->u.TlmCfaInfo.u1IfType,
                                         pQMsg->u.TlmCfaInfo.u1IfStatus,
                                         pQMsg->u.TlmCfaInfo.u1BridgedIface);
            break;

        case TLM_MPLS_DIFFSERV_TE_PARAMS:
            TlmMplsDiffservTeHandler (pQMsg->u.TlmDiffservInfo.aTlmClassType,
                                      pQMsg->u.TlmDiffservInfo.aTlmTeClassMap);
            break;
        default:
            TLM_TRC (TLM_CRITICAL_TRC, "TlmCfgQueueHandler: Invalid Event \n");
            break;
    }
    return;
}

#endif
