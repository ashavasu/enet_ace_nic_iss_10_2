/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: tlmif.c,v 1.16 2017/06/16 13:35:14 siva Exp $
*
* Description: This file contains the routines for the protocol                                                                  Database Access for the module TLM
*********************************************************************/
#include "tlminc.h"
#include "tlmcli.h"
#include "fssnmp.h"

extern UINT4        TeLinkDescrRowStatus[12];
extern UINT4        TeLinkSrlgRowStatus[12];
extern UINT4        TeLinkRowStatus[12];
extern UINT4        ComponentLinkRowStatus[12];
extern UINT4        ComponentLinkDescrRowStatus[12];

PRIVATE INT4
       TlmTeLinkIDBasedCompareFn (tRBElem * pRBElem1, tRBElem * pRBElem2);

/******************************************************************************
 * Function Name      : TlmFindTeIf
 *
 * Description        : This routine is used to find the Te-Link
 *
 * Input(s)           : i4IfIndex - Te-Link IfIndex
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Te-Link structure(pTeIface)/NULL
 *****************************************************************************/
tTlmTeLink         *
TlmFindTeIf (INT4 i4IfIndex)
{
    tTlmTeLink         *pTeIface = NULL;
    tTMO_HASH_TABLE    *pTeIfHashTable = gTlmGlobalInfo.pTeIfHashTable;

    TLM_TRC_FN_ENTRY ();

    if (pTeIfHashTable == NULL)
    {
        TLM_TRC1 (TLM_ALL_TRC | TLM_FAILURE_TRC,
                  "TlmFindTeIf: No Node for given IfIndex %d\r\n", i4IfIndex);
        return NULL;
    }

    TMO_HASH_Scan_Bucket (pTeIfHashTable, IF_HASH_FN (i4IfIndex), pTeIface,
                          tTlmTeLink *)
    {
        if (pTeIface->i4IfIndex == i4IfIndex)
        {
            return (pTeIface);
        }
    }

    TLM_TRC1 (TLM_ALL_TRC | TLM_FAILURE_TRC | TLM_FN_EXIT_TRC,
              "TlmFindTeIf: No Node for given IfIndex %d\r\n", i4IfIndex);
    return (NULL);
}

/******************************************************************************
 * Function Name      : TlmFindTeDescr
 *
 * Description        : This routine is used to find the descriptor
 *
 * Input(s)           : u4DescrId - Descriptor ID.
 *                      pTeIface - Te-Link information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Pointer to the Descriptor(pIfDescr)/NULL
 *****************************************************************************/
tTlmTeIfDescr      *
TlmFindTeDescr (UINT4 u4DescrId, tTlmTeLink * pTeIface)
{
    tTlmTeIfDescr      *pIfDescr = NULL;

    TLM_TRC_FN_ENTRY ();
    TMO_SLL_Scan (&(pTeIface->ifDescrList), pIfDescr, tTlmTeIfDescr *)
    {
        if (pIfDescr->u4DescrId == u4DescrId)
        {
            break;
        }
    }

    TLM_TRC_FN_EXIT ();
    return (pIfDescr);
}

/******************************************************************************
 * Function Name      : TlmFindTeSrlgIf
 *
 * Description        : This routine is used to find the Srlg.
 *
 * Input(s)           : u4LinkSrlg - Srlg number.
 *                      pTeIface - Te-Link information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Pointer to the Srlg(pLinkSrlg)/NULL
 *****************************************************************************/
tTlmTeLinkSrlg     *
TlmFindTeSrlgIf (UINT4 u4LinkSrlg, tTlmTeLink * pTeIface)
{
    tTlmTeLinkSrlg     *pLinkSrlg = NULL;

    TMO_SLL_Scan (&(pTeIface->srlgList), pLinkSrlg, tTlmTeLinkSrlg *)
    {
        if (pLinkSrlg->u4SrlgNo == u4LinkSrlg)
        {
            break;
        }
    }
    return (pLinkSrlg);
}

/******************************************************************************
 * Function Name      : TlmFindComponentIf
 *
 * Description        : This routine is used to find the Component link.
 *
 * Input(s)           : i4IfIndex - Component IfIndex
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Pointer to Component link(pComponentIface)/NULL
 *****************************************************************************/
tTlmComponentLink  *
TlmFindComponentIf (INT4 i4IfIndex)
{
    tTlmComponentLink  *pComponentIface = NULL;
    tTMO_HASH_TABLE    *pComponentIfHashTable =
        gTlmGlobalInfo.pComponentIfHashTable;

    TLM_TRC_FN_ENTRY ();
    if (pComponentIfHashTable == NULL)
    {
        TLM_TRC1 (TLM_ALL_TRC | TLM_FAILURE_TRC,
                  "TlmFindComponentIf: No Component-Link with given Index %d\r\n",
                  i4IfIndex);
        return NULL;
    }

    TMO_HASH_Scan_Bucket (pComponentIfHashTable,
                          COMPONENT_IF_HASH_FN (i4IfIndex), pComponentIface,
                          tTlmComponentLink *)
    {
        if (pComponentIface->i4IfIndex == i4IfIndex)
        {
            return (pComponentIface);
        }
    }
    TLM_TRC1 (TLM_ALL_TRC | TLM_FAILURE_TRC | TLM_FN_EXIT_TRC,
              "TlmFindComponentIf: No Component-Link with given Index %d\r\n",
              i4IfIndex);
    return (NULL);
}

/******************************************************************************
 * Function Name      : TlmFindComponentDescr
 *
 * Description        : This routine is used to find the component descriptor
 *
 * Input(s)           : u4DescrId - Descriptor Id.
 *                      pComponentIface - Component information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Pointer to the Descriptor(pIfDescr)/NULL
 *****************************************************************************/
tTlmComponentIfDescr *
TlmFindComponentDescr (UINT4 u4DescrId, tTlmComponentLink * pComponentIface)
{
    tTlmComponentIfDescr *pIfDescr = NULL;

    TLM_TRC_FN_ENTRY ();
    TMO_SLL_Scan (&(pComponentIface->ifDescrList), pIfDescr,
                  tTlmComponentIfDescr *)
    {
        if (pIfDescr->u4DescrId == u4DescrId)
        {
            break;
        }
    }
    TLM_TRC_FN_EXIT ();
    return (pIfDescr);
}

/******************************************************************************
 * Function Name      : TlmCreateTeIf
 *
 * Description        : This routine is used to create the Te-Link and 
 *                      intialize the Te-Link parameters to default values.
 *
 * Input(s)           : i4IfIndex - Te-Link interface index. 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Te-Link inforamtion(pteif)/NULL
 *****************************************************************************/
tTlmTeLink         *
TlmCreateTeIf (INT4 i4IfIndex)
{
    tTlmTeLink         *pTeLink = NULL;
    UINT1               u1Index1 = TLM_ZERO;
    UINT1               u1Index2 = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();
    /*Allocate Buffer for tTlmTeLink data structure */
    if (TELINK_ALLOC (pTeLink) == NULL)
    {
        TLM_TRC (TLM_ALL_TRC | TLM_FAILURE_TRC | TLM_RESOURCE_TRC,
                 "TlmCreateTeIf: Buffer allocation failure for TE-Link\r\n");
        return NULL;
    }

    MEMSET (pTeLink, TLM_ZERO, sizeof (tTlmTeLink));
    MEMSET (pTeLink->LocalIpAddr.u4_addr, TLM_ZERO,
            sizeof (pTeLink->LocalIpAddr.u4_addr));

    TMO_SLL_Init_Node (&pTeLink->NextSortLinkNode);
    TMO_SLL_Init_Node (&pTeLink->NextSortUnbundledTeLinkNode);

    pTeLink->i4AddressType = TLM_ADDRESSTYPE_UNKNOWN;
    pTeLink->u4RemoteRtrId = TLM_DEF_ROUTER_ID;
    pTeLink->i4RowStatus = TLM_INVALID;
    pTeLink->i4IfIndex = i4IfIndex;
    pTeLink->u4WorkingPriority = TLM_MAX_PRIORITY_LVL - TLM_ONE;
    pTeLink->u4TeMetric = TLM_DEF_METRIC_VALUE;
    pTeLink->i4ProtectionType = TLM_DEF_PROTECTION_TYPE;
    pTeLink->u4RsrcClassColor = TLM_DEF_RESOURCECLASS_COLOR;
    pTeLink->u4LocalIdentifier = TLM_DEF_LOCAL_IDENTIFIER;
    pTeLink->u4RemoteIdentifier = TLM_DEF_REMOTE_IDENTIFIER;
    pTeLink->maxResBw = TLM_DEF_BANDWIDTH_VALUE;
    pTeLink->maxBw = TLM_DEF_BANDWIDTH_VALUE;
    pTeLink->u1OperStatus = CFA_IF_DOWN;
    pTeLink->i4StorageType = TLM_STORAGE_NON_VOLATILE;
    pTeLink->i4TeAdminStatus = TLM_DISABLED;
    pTeLink->u1InfoType = TLM_DATA_CONTROL_CHANNEL;
    pTeLink->i4TeLinkIfType = TLM_POINT_TO_POINT;
    pTeLink->i4IsTeLinkAdvertise = TLM_TRUE;

    TMO_SLL_Init (&(pTeLink->sortUnbundleTeList));
    TMO_SLL_Init (&(pTeLink->ifDescrList));
    TMO_SLL_Init (&(pTeLink->srlgList));
    TMO_SLL_Init (&(pTeLink->ComponentList));

    for (u1Index1 = TLM_MIN_PRIORITY_LVL; u1Index1 < TLM_MAX_PRIORITY_LVL;
         u1Index1++)
    {
        pTeLink->aUnResBw[u1Index1].i4RowStatus = TLM_ROWSTATUS_INVALID;
        pTeLink->aUnResBw[u1Index1].unResBw = TLM_DEF_BANDWIDTH_VALUE;
        pTeLink->aUnResBw[u1Index1].resBw = TLM_DEF_BANDWIDTH_VALUE;
        pTeLink->aUnResBw[u1Index1].i4StorageType = TLM_STORAGE_NON_VOLATILE;
    }

    for (u1Index1 = TLM_ZERO; u1Index1 < TLM_MAX_BW_THRESHOLD_SUPPORTED;
         u1Index1++)
    {
        pTeLink->aTlmBWThresholdUp[u1Index1] =
            (UINT1) gai4BwThresholds[u1Index1];
    }

    for (u1Index1 = (TLM_MAX_BW_THRESHOLD_SUPPORTED - TLM_ONE), u1Index2 =
         TLM_ZERO;
         ((INT1) u1Index1 >= TLM_ZERO)
         && (u1Index2 < TLM_MAX_BW_THRESHOLD_SUPPORTED); u1Index1--, u1Index2++)
    {
        pTeLink->aTlmBWThresholdDown[u1Index1] =
            (UINT1) gai4BwThresholds[u1Index2];
    }

    /* Add this TeInterface to HASH TABLE */
    TMO_HASH_Add_Node (gTlmGlobalInfo.pTeIfHashTable,
                       (tTMO_HASH_NODE *) pTeLink,
                       IF_HASH_FN (pTeLink->i4IfIndex), NULL);

    /* Add this TeInterface to sortIfLst */
    TlmTeAddTosortIfLst (pTeLink);

    TLM_TRC_FN_EXIT ();
    return (pTeLink);
}

/******************************************************************************
 * Function Name      : TlmTeAddTosortIfLst
 *
 * Description        : This routine is used to add the Te-Link to 
 *                      the global Te-Link sort list. 
 *
 * Input(s)           : pTeIface - Pointer to the Te-Link. 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : VOID
 *****************************************************************************/
VOID
TlmTeAddTosortIfLst (tTlmTeLink * pTeIface)
{
    tTlmTeLink         *pTeLink = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevLstNode = NULL;

    TLM_TRC_FN_ENTRY ();
    TMO_SLL_Scan (&(gTlmGlobalInfo.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pTeLink = GET_TE_IF_PTR_FROM_SORT_LST (pLstNode);
        if (pTeIface->i4IfIndex < pTeLink->i4IfIndex)
        {
            break;
        }
        pPrevLstNode = pLstNode;
    }
    TMO_SLL_Insert (&(gTlmGlobalInfo.sortTeIfLst), pPrevLstNode,
                    &(pTeIface->NextSortLinkNode));
    TLM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : TlmCreateTeDescr
 *
 * Description        : This routine is used to create a descriptor entry and
 *                      intializes its parameters to deafult values and then
 *                      add this descriptor to the Te-Link descriptor list.
 *
 * Input(s)           : u4DescrId - Descriptor ID.
 *                      pTeIface - Te-Link information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Pointer to the Descriptor(pTeIfDescr)/NULL
 *****************************************************************************/
tTlmTeIfDescr      *
TlmCreateTeDescr (UINT4 u4DescrId, tTlmTeLink * pTeIface)
{
    tTlmTeIfDescr      *pTeIfDescr = NULL;
    UINT4               u4Index = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();
    /*Allocate Buffer for tTlmTeIfDescr data structure */
    if (TEIFDESCR_ALLOC (pTeIfDescr) == NULL)
    {
        TLM_TRC (TLM_ALL_TRC | TLM_RESOURCE_TRC | TLM_FAILURE_TRC,
                 "TlmCreateTeDescr: Memory allocation failed for TeLink Descriptor\r\n");
        return (NULL);
    }

    MEMSET (pTeIfDescr, TLM_ZERO, sizeof (tTlmTeIfDescr));

    pTeIfDescr->u4DescrId = u4DescrId;
    pTeIfDescr->i4SwitchingCap = OSPF_TE_PSC_1;
    pTeIfDescr->i4EncodingType = TLM_TE_ENCODING_TYPE_PACKET;
    pTeIfDescr->minLSPBw = TLM_DEF_BANDWIDTH_VALUE;
    pTeIfDescr->u2MTU = TLM_DEF_MTU_SIZE;
    pTeIfDescr->i4StorageType = TLM_STORAGE_NON_VOLATILE;

    pTeIfDescr->i4Indication = TLM_DEF_INVALID_INDICATION;

    for (u4Index = TLM_MIN_PRIORITY_LVL; u4Index < TLM_MAX_PRIORITY_LVL;
         ++u4Index)
    {
        pTeIfDescr->aMaxLSPBwPrio[u4Index] = TLM_DEF_BANDWIDTH_VALUE;
    }

    /* Add this Interface Descriptor to TMO_SLL in TELINK */
    TlmTeAddTosortIfDescrLst (pTeIfDescr, pTeIface);

    KW_FALSEPOSITIVE_FIX (pTeIfDescr);

    TLM_TRC_FN_EXIT ();
    return (pTeIfDescr);
}

/******************************************************************************
 * Function Name      : TlmTeAddTosortIfDescrLst
 *
 * Description        : This routine is used to add the Descriptor to the
 *                      Te-Link descriptor list.
 *                                                                                                                 * Input(s)           : pTeDescr - Descriptor Information.
 *                      pTeIface - Te-Link Information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : VOID
 *****************************************************************************/
VOID
TlmTeAddTosortIfDescrLst (tTlmTeIfDescr * pTeDescr, tTlmTeLink * pTeIface)
{
    tTlmTeIfDescr      *pTeDescrNode = NULL;

    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevLstNode = NULL;

    TLM_TRC_FN_ENTRY ();
    TMO_SLL_Scan (&(pTeIface->ifDescrList), pLstNode, tTMO_SLL_NODE *)
    {
        pTeDescrNode = GET_TE_DESCR_PTR_FROM_SORT_LST (pLstNode);
        if (pTeDescr->u4DescrId < pTeDescrNode->u4DescrId)
        {
            break;
        }
        pPrevLstNode = pLstNode;
    }
    TMO_SLL_Insert (&(pTeIface->ifDescrList), pPrevLstNode,
                    &(pTeDescr->NextIfDescrNode));
    TLM_TRC_FN_EXIT ();

}

/******************************************************************************
 * Function Name      : TlmCreateTeSrlg
 *
 * Description        : This routine is used to create a SRLG entry for a 
 *                      TE Link interface .
 *
 * Input(s)           : u4LinkSrlg - Srlg no.
 *                      pTeIface - Te-Link information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Pointer to the Srlg(pLinkSrlg)/NULL
 *****************************************************************************/
tTlmTeLinkSrlg     *
TlmCreateTeSrlg (UINT4 u4LinkSrlg, tTlmTeLink * pTeIface)
{
    tTlmTeLinkSrlg     *pLinkSrlg = NULL;

    TLM_TRC_FN_ENTRY ();
    /*Allocate Buffer for tTlmTeLinkSrlg data structure */
    if (TELINKSRLG_ALLOC (pLinkSrlg) == NULL)
    {
        TLM_TRC (TLM_ALL_TRC | TLM_RESOURCE_TRC | TLM_FAILURE_TRC,
                 "TlmCreateTeSrlg: Memory allocation failed for Te-Link SRLG\r\n");
        return (NULL);
    }
    MEMSET (pLinkSrlg, TLM_ZERO, sizeof (tTlmTeLinkSrlg));

    pLinkSrlg->u4SrlgNo = u4LinkSrlg;
    pLinkSrlg->i4StorageType = TLM_STORAGE_NON_VOLATILE;

    /* Add this Interface srlg to TMO_SLL in TeLINK */
    TlmTeAddTosortIfSrlgLst (pLinkSrlg, pTeIface);

    KW_FALSEPOSITIVE_FIX (pLinkSrlg);

    TLM_TRC_FN_EXIT ();
    return (pLinkSrlg);
}

/******************************************************************************
 * Function Name      : TlmTeAddTosortIfSrlgLst
 *
 * Description        : This routine is used to add the srlg to the
 *                      Te-Link srlg list.
 *
 * Input(s)           : pTeSrlg - Srlg information.
 *                      pTeIface - Te-Link information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : VOID
 *****************************************************************************/
VOID
TlmTeAddTosortIfSrlgLst (tTlmTeLinkSrlg * pTeSrlg, tTlmTeLink * pTeIface)
{
    tTlmTeLinkSrlg     *ptesrlg = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevLstNode = NULL;

    TLM_TRC_FN_ENTRY ();
    TMO_SLL_Scan (&(pTeIface->srlgList), pLstNode, tTMO_SLL_NODE *)
    {
        ptesrlg = GET_TE_SRLG_PTR_FROM_SORT_LST (pLstNode);
        if (pTeSrlg->u4SrlgNo < ptesrlg->u4SrlgNo)
        {
            break;
        }
        pPrevLstNode = pLstNode;
    }
    TMO_SLL_Insert (&(pTeIface->srlgList), pPrevLstNode,
                    &(pTeSrlg->NextLinkSrlgNode));
    TLM_TRC_FN_EXIT ();
}

/******************************************************************************
 * Function Name      : TlmComponentIfCreate
 *
 * Description        : This routine is used to create a component interface and
 *                      intialize the parameters to default values.
 *
 * Input(s)           : i4IfIndex - Component IfIndex
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Component information(pcomponentif)/NULL
 *****************************************************************************/
tTlmComponentLink  *
TlmComponentIfCreate (INT4 i4IfIndex)
{
    tTlmComponentLink  *pcomponentif = NULL;
    UINT4               u4Index = TLM_ZERO;
    tCfaIfInfo          CfaIfInfo;

    TLM_TRC_FN_ENTRY ();

    MEMSET (&CfaIfInfo, TLM_ZERO, sizeof (tCfaIfInfo));

    /*Allocate Buffer for tTlmComponentLink data structure */
    if (COMPONENT_LINK_ALLOC (pcomponentif) == NULL)
    {
        TLM_TRC (TLM_ALL_TRC | TLM_FAILURE_TRC | TLM_RESOURCE_TRC,
                 "TlmComponentIfCreate: Buffer allocation failure for TE-Link\r\n");
        return NULL;
    }
    MEMSET (pcomponentif, TLM_ZERO, sizeof (tTlmComponentLink));

    TMO_HASH_Init_Node (&pcomponentif->NextComponentNode);
    TMO_SLL_Init_Node (&pcomponentif->NextSortComponentNode);
    TMO_SLL_Init_Node (&pcomponentif->NextSortComponentTeListNode);

    TMO_SLL_Init (&(pcomponentif->ifDescrList));

    pcomponentif->i4IfIndex = i4IfIndex;

    pcomponentif->i4LinkPrefProtection = TLM_TE_LINK_PROTECTION_PRIMARY;
    pcomponentif->i4LinkCurrentProtection = TLM_TE_LINK_PROTECTION_PRIMARY;

    CfaGetIfInfo ((UINT4) i4IfIndex, &CfaIfInfo);
    pcomponentif->i4RowStatus = TLM_INVALID;
    pcomponentif->u1OperStatus = CfaIfInfo.u1IfOperStatus;
    pcomponentif->i4StorageType = TLM_STORAGE_NON_VOLATILE;

    for (u4Index = TLM_MIN_PRIORITY_LVL; u4Index < TLM_MAX_PRIORITY_LVL;
         ++u4Index)
    {
        pcomponentif->aUnResBw[u4Index].i4RowStatus = TLM_ROWSTATUS_INVALID;
        pcomponentif->aUnResBw[u4Index].unResBw = TLM_DEF_BANDWIDTH_VALUE;
        pcomponentif->aUnResBw[u4Index].resBw = TLM_DEF_BANDWIDTH_VALUE;
        pcomponentif->aUnResBw[u4Index].i4StorageType =
            TLM_STORAGE_NON_VOLATILE;
    }
    /* Add this ComponentInterface to HASH TABLE */
    TMO_HASH_Add_Node (gTlmGlobalInfo.pComponentIfHashTable,
                       (tTMO_HASH_NODE *) pcomponentif,
                       COMPONENT_IF_HASH_FN (pcomponentif->i4IfIndex), NULL);
    /* Add this ComponentInterface to sortIfLst */
    ComponentAddTosortIfLst (pcomponentif);

    TLM_TRC_FN_EXIT ();
    return (pcomponentif);
}

/******************************************************************************
 * Function Name      : ComponentAddTosortIfLst
 *
 * Description        : This routine is used to add the component to
 *                      the global component sort list.
 *
 * Input(s)           : pComponentiface - Pointer to the Component interface.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : VOID
 *****************************************************************************/
VOID
ComponentAddTosortIfLst (tTlmComponentLink * pComponentiface)
{
    tTlmComponentLink  *pcomponentif = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevLstNode = NULL;

    TLM_TRC_FN_ENTRY ();
    TMO_SLL_Scan (&(gTlmGlobalInfo.sortComponentIfLst), pLstNode,
                  tTMO_SLL_NODE *)
    {
        pcomponentif = GET_COMPONENT_IF_PTR_FROM_SORT_LST (pLstNode);
        if (pComponentiface->i4IfIndex < pcomponentif->i4IfIndex)
        {
            break;
        }
        pPrevLstNode = pLstNode;
    }
    TMO_SLL_Insert (&(gTlmGlobalInfo.sortComponentIfLst), pPrevLstNode,
                    &(pComponentiface->NextSortComponentNode));
    TLM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : TlmTeAddTosortIfComponentLst
 *
 * Description        : This routine is used to add the component to Te-Link
 *                      Component list.
 *
 * Input(s)           : pTeComponent - Component Information.
 *                      pTeIface - Te-Link information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : VOID
 *****************************************************************************/
VOID
TlmTeAddTosortIfComponentLst (tTlmComponentLink * pCompIface,
                              tTlmTeLink * pTeIface)
{
    TLM_TRC_FN_ENTRY ();

    TMO_SLL_Insert (&(pTeIface->ComponentList), NULL,
                    &(pCompIface->NextSortComponentTeListNode));
    TLM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : TlmTeDeletesortIfComponentLst
 *
 * Description        : This routine is used to add the component from Te-Link
 *                      Component list.
 *
 * Input(s)           : pTeComponent - Component Information.
 *                      pTeIface - Te-Link information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : VOID
 *****************************************************************************/
VOID
TlmTeDeleteTosortIfComponentLst (tTlmComponentLink * pCompIface,
                                 tTlmTeLink * pTeIface)
{
    TLM_TRC_FN_ENTRY ();

    TMO_SLL_Delete (&(pTeIface->ComponentList),
                    &(pCompIface->NextSortComponentTeListNode));
    TLM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : TlmTeAddUnbundleTeIfToBundle
 *
 * Description        : This routine is used to add the unbundled TE-link to
 *                      its own bundle link                        
 *  
 * Input(s)           : pTeIface - Unbundle TE link Information.
 *                      pBundleTeIface - Bundle Te-Link information.
 * Output(s)          : None
 *
 * Return Value(s)    : VOID
 *****************************************************************************/
VOID
TlmTeAddUnbundleTeIfToBundle (tTlmTeLink * pTeIface,
                              tTlmTeLink * pBundleTeIface)
{

    TLM_TRC_FN_ENTRY ();

    if (TMO_SLL_Is_Node_In_List ((&pTeIface->NextSortUnbundledTeLinkNode)))
    {
        return;
    }

    TMO_SLL_Add (&(pBundleTeIface->sortUnbundleTeList),
                 &(pTeIface->NextSortUnbundledTeLinkNode));

    pTeIface->pBundledTeLink = pBundleTeIface;

    TLM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : TlmTeDeleteUnbundleTeIfFromBundle
 *
 * Description        : This routine is used to delete the unbundled TE-link
 *                      from its bundle link                        
 *  
 * Input(s)           : pTeIface - Unbundle TE link Information.
 *                      pBundleTeIface - Bundle Te-Link information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : VOID
 *****************************************************************************/
VOID
TlmTeDeleteUnbundleTeIfFromBundle (tTlmTeLink * pTeIface,
                                   tTlmTeLink * pBundleTeIface)
{
    TLM_TRC_FN_ENTRY ();

    TMO_SLL_Delete (&(pBundleTeIface->sortUnbundleTeList),
                    &(pTeIface->NextSortUnbundledTeLinkNode));

    TLM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : TlmDeleteUnbundleTeList
 *
 * Description        : This routine is used to delete all the unbundled TE-links
 *                      from a bundle link                        
 *  
 * Input(s)           : pBundleTeIface - Bundle Te-Link information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : VOID
 *****************************************************************************/
VOID
TlmDeleteUnbundleTeList (tTlmTeLink * pBundleTeIface)
{
    tTlmTeLink         *pTeLink = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTempLstNode = NULL;

    TLM_TRC_FN_ENTRY ();

    TMO_DYN_SLL_Scan (&(pBundleTeIface->sortUnbundleTeList), pLstNode,
                      pTempLstNode, tTMO_SLL_NODE *)
    {
        pTeLink = GET_UNBUNDLE_TE_LINK_LIST (pLstNode);

        pTeLink->pBundledTeLink = NULL;

        TMO_SLL_Delete (&(pBundleTeIface->sortUnbundleTeList),
                        &(pTeLink->NextSortUnbundledTeLinkNode));
    }
    TLM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : TlmCreateComponentDescr
 *
 * Description        : This routine is used to create the descriptor and
 *                      intializes its parameters to deafult values and then
 *                      add this descriptor to the Component-Link
 *                       descriptor list.
 *
 * Input(s)           : i4IfIndex - Component IfIndex
 *                      u1Status - OperStatus of the Interface
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Pointer to the Descriptor(pComponentIfDescr)/NULL
 *****************************************************************************/
tTlmComponentIfDescr *
TlmCreateComponentDescr (UINT4 u4DescrId, tTlmComponentLink * pComponentIface)
{
    tTlmComponentIfDescr *pComponentIfDescr = NULL;
    UINT1               u1Counter = TLM_ZERO;
    tCfaIfInfo          CfaIfInfo;

    TLM_TRC_FN_ENTRY ();
    /*Allocate Buffer for tTlmComponentIfDescr data structure */
    if (COMPONENT_IFDESCR_ALLOC (pComponentIfDescr) == NULL)
    {
        TLM_TRC (TLM_ALL_TRC | TLM_RESOURCE_TRC | TLM_FAILURE_TRC,
                 "TlmCreateComponentDescr: Memory allocation failed"
                 " for Component Descriptor\r\n");
        return (NULL);
    }
    MEMSET (pComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    TMO_SLL_Init_Node (&pComponentIfDescr->NextIfComponentDescrNode);

    pComponentIfDescr->i4RowStatus = TLM_INVALID;
    pComponentIfDescr->u4DescrId = u4DescrId;
    pComponentIfDescr->i4SwitchingCap = OSPF_TE_PSC_1;
    pComponentIfDescr->i4EncodingType = TLM_TE_ENCODING_TYPE_PACKET;
    pComponentIfDescr->minLSPBw = TLM_DEF_BANDWIDTH_VALUE;
    pComponentIfDescr->i4StorageType = TLM_STORAGE_NON_VOLATILE;
    if (CfaGetIfInfo ((UINT4) pComponentIface->i4IfIndex, &CfaIfInfo) !=
        CFA_SUCCESS)
    {
        pComponentIfDescr->u2MTU = TLM_DEF_MTU_SIZE;
    }
    else
    {
        pComponentIfDescr->u2MTU = (UINT2) CfaIfInfo.u4IfMtu;
    }
    pComponentIfDescr->i4Indication = TLM_DEF_INVALID_INDICATION;

    if (TLM_DSTE_STATUS == TLM_DSTE_DISABLED)
    {
        for (u1Counter = TLM_ZERO; u1Counter < TLM_MAX_TE_CLASS; u1Counter++)
        {
            pComponentIfDescr->aMaxLSPBwPrio[u1Counter] =
                pComponentIface->maxResBw;
        }
    }
    else
    {
        for (u1Counter = TLM_MIN_PRIORITY_LVL; u1Counter < TLM_MAX_PRIORITY_LVL;
             u1Counter++)
        {
            pComponentIfDescr->aMaxLSPBwPrio[u1Counter] =
                TLM_DEF_BANDWIDTH_VALUE;
        }
    }

    /* Add this Interface Descriptor to TMO_SLL in ComponentLINK */
    TlmCompDescrAddToSortIfDescrLst (pComponentIfDescr, pComponentIface);

    KW_FALSEPOSITIVE_FIX (pComponentIfDescr);

    TLM_TRC_FN_EXIT ();
    return (pComponentIfDescr);
}

/******************************************************************************
 * Function Name      : TlmCompDescrAddToSortIfDescrLst
 *
 * Description        : This routine is used to add the descriptor to the
 *                      Component descriptor list.
 *
 * Input(s)           : pComponentDescr - Descriptor inforamtion.
 *                      pComponentIface - Component information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : VOID
 *****************************************************************************/
VOID
TlmCompDescrAddToSortIfDescrLst (tTlmComponentIfDescr * pComponentDescr,
                                 tTlmComponentLink * pComponentIface)
{
    tTlmComponentIfDescr *pComponentDescrNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevLstNode = NULL;

    TLM_TRC_FN_ENTRY ();
    TMO_SLL_Scan (&(pComponentIface->ifDescrList), pLstNode, tTMO_SLL_NODE *)
    {
        pComponentDescrNode = GET_COMPONENT_DESCR_PTR_FROM_SORT_LST (pLstNode);
        if (pComponentDescr->u4DescrId < pComponentDescrNode->u4DescrId)
        {
            break;
        }
        pPrevLstNode = pLstNode;
    }
    TMO_SLL_Insert (&(pComponentIface->ifDescrList), pPrevLstNode,
                    &(pComponentDescr->NextIfComponentDescrNode));
    TLM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : TlmUpdateUnBundleTeLinks
 *
 * Description        : This routine is used to update the member TE links
 *                      of a bundle TE link based on the RowStatus value
 *
 * Input(s)           : pTeIface        - pointer to unbundle TE link
 *                      pBundleIface    - pointer to bundle TeLink
 *                      u1UpdateStatus  - TLM_UNBUNDLE_UPDATE / TLM_UNBUNDLE_DELETE
 *
 * Output(s)          : None
 *
 * Return Value(s)    : TLM_SUCCESS/TLM_FAILURE
 *****************************************************************************/
VOID
TlmUpdateUnBundleTeLinks (tTlmTeLink * pTeIface, tTlmTeLink * pBundleIface,
                          UINT1 u1UpdateStatus)
{
    tTlmTeLink         *pMemberTeLink = NULL;
    INT4                i4CurLLTeLinkIf = TLM_ZERO;
    INT4                i4LLTeLinkIf = TLM_ZERO;
    UINT4               u4IfIndex = TLM_ZERO;

    switch (u1UpdateStatus)
    {
        case TLM_BUNDLE_UPDATE:
        {
            /*Getting first underlying member TE link of a bundle TE link */
            if (CfaApiGetFirstLLTeLinkFromHLTeLink ((UINT4) pTeIface->i4IfIndex,
                                                    &u4IfIndex, TRUE)
                == CFA_FAILURE)
            {
                return;
            }

            /*Getting next member TE links of a bundle TE link */
            do
            {
                i4CurLLTeLinkIf = i4LLTeLinkIf = (INT4) u4IfIndex;
                pMemberTeLink = TlmFindTeIf (i4LLTeLinkIf);

                if (pMemberTeLink == NULL)
                {
                    continue;
                }

                TlmTeAddUnbundleTeIfToBundle (pMemberTeLink, pTeIface);

            }
            while (CfaApiGetNextLLTeLinkFromHLTeLink
                   ((UINT4) pTeIface->i4IfIndex, (UINT4) i4CurLLTeLinkIf,
                    &u4IfIndex, TRUE) == CFA_SUCCESS);
            break;
        }

        case TLM_BUNDLE_DELETE:
        {
            TlmDeleteUnbundleTeList (pTeIface);
            break;
        }

        case TLM_UNBUNDLE_UPDATE:
        {
            if (pBundleIface == NULL)
            {
                return;
            }
            TlmTeAddUnbundleTeIfToBundle (pTeIface, pBundleIface);
            break;
        }

        case TLM_UNBUNDLE_DELETE:
        {
            if (pBundleIface == NULL)
            {
                return;
            }

            TlmTeDeleteUnbundleTeIfFromBundle (pTeIface, pBundleIface);
            break;
        }

        default:
        {
            break;
        }
    }

    return;
}

/******************************************************************************
 * Function Name      : TlmUpdateComponentLink
 *
 * Description        : This routine is used to update the Component link
 *                      of a TE link
 *
 * Input(s)           : pTeLink    - TE link interface
 *                      u1IfStatus     - Interface Status
 * Output(s)          : None
 *
 * Return Value(s)    : TLM_SUCCESS/TLM_FAILURE
 *****************************************************************************/
VOID
TlmUpdateComponentLink (tTlmTeLink * pTeIface, UINT1 u1IfStatus)
{
    tTlmComponentLink  *pCompIface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    INT4                i4CompIfIndex = TLM_ZERO;

    if ((pTeIface == NULL))
    {
        return;
    }
    switch (u1IfStatus)
    {
        case TLM_OPER_DOWN:
            pLstNode = TMO_SLL_First (&pTeIface->ComponentList);

            if (pLstNode != NULL)
            {
                pCompIface = GET_COMPONENT_IF_PTR_FROM_SORT_TE_LST (pLstNode);

                TlmTeDeleteTosortIfComponentLst (pCompIface, pTeIface);
                pCompIface->pTeLink = NULL;
            }
            TlmUpdateTeLinkOperStatus (pTeIface, u1IfStatus);
            break;
        case TLM_OPER_UP:

            if (CfaApiGetL3IfFromTeLinkIf ((UINT4) pTeIface->i4IfIndex,
                                           (UINT4 *) &i4CompIfIndex,
                                           TRUE) == CFA_FAILURE)
            {
                break;
            }
            pCompIface = TlmFindComponentIf (i4CompIfIndex);
            if (pCompIface == NULL)
            {
                break;
            }
            TlmTeAddTosortIfComponentLst (pCompIface, pTeIface);
            pCompIface->pTeLink = pTeIface;

            TlmUpdateTeLinkInfo (pCompIface, pTeIface, TLM_COMPONENT_UPDATE);
            TlmUpdateTeLinkOperStatus (pTeIface, u1IfStatus);
            break;
        default:
            break;
    }
    return;
}

/******************************************************************************
 * Function Name      : TlmUpdateTeLinkOperStatus
 *
 * Description        : This routine is used to set the OperStatus value for
 *                      TeLink, on the basis of the values of it's admin status
 *                      and its  asscoiated component links. It calls for notify
 *                      msg sending incase of any change in operstatus of TeLink
 *
 * Input(s)           : pTeIface     - pointer to TeLink
 *                      u1OperStatus - Oper Status
 *
 * Output(s)          : None
 *
 * Return Value(s)    : TLM_SUCCESS/TLM_FAILURE
 *****************************************************************************/
INT1
TlmUpdateTeLinkOperStatus (tTlmTeLink * pTeIface, UINT1 u1OperStatus)
{

    tTlmTeLink         *pTeListNode = NULL;
    tTlmComponentLink  *pCompNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4EventType = TLM_ZERO;
    INT1                i1RetVal = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();

    /*
     * In case of Bundled TE-links, if any of the underlying TeLink
     * OperStatus is CFA_UP, then bundled TeLink OperStatus is UP.
     *
     * In case of unbundled TE links:
     * - If any component link associated with TeLink Interface
     *   OperStatus is CFA_IF_UP then TeLink operStatus is CFA_IF_UP
     * - If No Component link is associated with Te Link then TeLink
     *   operStatus is CFA_IF_DOWN
     * - If No component link associated with TeLink has operstatus
     *   as CFA_IF_UP, then TeLink operStatus is  CFA_IF_DOWN
     */

    if (pTeIface == NULL)
    {
        return TLM_FAILURE;
    }
#ifdef MPLS_WANTED
    if ((pTeIface->i4CtrlMplsIfIndex != TLM_ZERO) &&
        (pTeIface->u1InfoType == TLM_DATA_CHANNEL))
    {
        RpteApiIfStatusChgNotify (pTeIface->i4CtrlMplsIfIndex,
                                  pTeIface->i4IfIndex, u1OperStatus);
    }
    else if ((pTeIface->pBundledTeLink != NULL) &&
             (pTeIface->pBundledTeLink->i4CtrlMplsIfIndex != TLM_ZERO) &&
             (pTeIface->pBundledTeLink->u1InfoType == TLM_DATA_CHANNEL))
    {
        RpteApiIfStatusChgNotify (pTeIface->pBundledTeLink->i4CtrlMplsIfIndex,
                                  pTeIface->pBundledTeLink->i4IfIndex,
                                  u1OperStatus);
    }
#endif
    if (u1OperStatus == TLM_OPER_DOWN)
    {
        pTeIface->u1OperStatus = u1OperStatus;
        u4EventType = TE_LINK_DELETED;

        i1RetVal = TlmUtilSendNotifMsgInfo (pTeIface, u4EventType);

        return i1RetVal;
    }

    if (pTeIface->i4TeLinkType == TLM_BUNDLE)
    {
        TMO_SLL_Scan (&(pTeIface->sortUnbundleTeList), pLstNode,
                      tTMO_SLL_NODE *)
        {
            pTeListNode = GET_TE_LINK_TE_LIST_NODE_PTR_FROM_SORT_LST (pLstNode);

            if (pTeListNode->u1OperStatus == TLM_OPER_UP)
            {
                u1OperStatus = TLM_OPER_UP;
                break;
            }
        }
    }
    else
    {
        pLstNode = TMO_SLL_First (&(pTeIface->ComponentList));

        if (pLstNode == NULL)
        {
            return TLM_FAILURE;
        }
        pCompNode = GET_COMPONENT_IF_PTR_FROM_SORT_TE_LST (pLstNode);

        CfaGetIfInfo ((UINT4) pTeIface->i4IfIndex, &CfaIfInfo);

        if ((CfaIfInfo.u1IfOperStatus == TLM_OPER_UP) &&
            (pCompNode->u1OperStatus == TLM_OPER_UP))
        {
            u1OperStatus = TLM_OPER_UP;
        }
    }

    u4EventType = TE_LINK_CREATED;
    pTeIface->u1OperStatus = u1OperStatus;

    i1RetVal = TlmUtilSendNotifMsgInfo (pTeIface, u4EventType);

    TLM_TRC_FN_EXIT ();

    return i1RetVal;
}

/******************************************************************************
 * Function Name      : TlmUpdateCompLinkOperstatus
 *
 * Description        : This routine is used to set the OperStatus value for
 *                      Component Link, based on the operstatus of the
 *                      physical interface
 *
 * Input(s)           : pCompIface   - pointer to Component Link
 *                      u1OperStatus - Oper Status
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/

VOID
TlmUpdateCompLinkOperstatus (tTlmComponentLink * pCompIface, UINT1 u1OperStatus)
{
    tCfaIfInfo          CfaIfInfo;

    TLM_TRC_FN_ENTRY ();

    MEMSET (&CfaIfInfo, TLM_ZERO, sizeof (tCfaIfInfo));

    CfaGetIfInfo ((UINT4) pCompIface->i4IfIndex, &CfaIfInfo);

    if (u1OperStatus == CFA_IF_UP)
    {
        pCompIface->u1OperStatus = CfaIfInfo.u1IfOperStatus;
    }
    else
    {
        pCompIface->u1OperStatus = u1OperStatus;
    }

    TlmUpdateTeLinkOperStatus (pCompIface->pTeLink, pCompIface->u1OperStatus);

    TLM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : TlmDeleteTeIf
 *
 * Description        : This routine is used to delete the Te-Link.
 *
 * Input(s)           : pTeIface - Te-Link interface to delete.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : SNMP_SUCCESS
 *****************************************************************************/
INT4
TlmDeleteTeIf (tTlmTeLink * pTeIface)
{
    tTMO_HASH_TABLE    *pTeIfHashTable = gTlmGlobalInfo.pTeIfHashTable;
    tTMO_HASH_TABLE    *pTeIfNameHashTable =
        gTlmGlobalInfo.pTeLinkNameIfHashTable;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTempLstNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tTlmTeIfDescr      *pIfDescr = NULL;
    tTlmTeLinkSrlg     *pTeLinkSrlg = NULL;
    UINT4               u4SeqNum = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();

    /*Send Delete notifications to the applications, only for bundle TE link
     * and unbundle TE link which is not a member of a TE link*/

    /*Delete the member unbundle TE link from Unbundle Te List before deleting */
    if (pTeIface->pBundledTeLink != NULL)
    {
        TlmTeDeleteUnbundleTeIfFromBundle (pTeIface, pTeIface->pBundledTeLink);
    }
    else
    {
        TlmUtilSendNotifMsgInfo (pTeIface, TE_LINK_DELETED);
    }

    TMO_DYN_SLL_Scan (&pTeIface->ifDescrList, pLstNode, pTempLstNode,
                      tTMO_SLL_NODE *)
    {
        pIfDescr = GET_TE_DESCR_PTR_FROM_SORT_LST (pLstNode);
        TlmDeleteTeDescr (pIfDescr, pTeIface);

        /* No need to send MSR Notifications for Dynamically created
         * Entries (Storage type = volatile) */
        if (pIfDescr->i4StorageType == TLM_STORAGE_NON_VOLATILE)
        {
            /*Notifying MSR for TE-Link Descriptor RowStatus-Destroy event */
            RM_GET_SEQ_NUM (&u4SeqNum);
            MEMSET (&SnmpNotifyInfo, TLM_ZERO, sizeof (tSnmpNotifyInfo));

            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, TeLinkDescrRowStatus,
                                  u4SeqNum, TRUE, TlmLock,
                                  TlmUnLock, 1, SNMP_SUCCESS);

            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                              pTeIface->i4IfIndex, pIfDescr->u4DescrId,
                              DESTROY));
        }

    }

    /* If SRLG support is provided for FA TE-Link, then
     * sending MSR Notifications for dynamically generated SRLG Table
     * needs to be handled */
    TMO_DYN_SLL_Scan (&pTeIface->srlgList, pLstNode, pTempLstNode,
                      tTMO_SLL_NODE *)
    {
        pTeLinkSrlg = GET_TE_SRLG_PTR_FROM_SORT_LST (pLstNode);
        TlmDeleteTeLinkSrlg (pTeLinkSrlg, pTeIface);

        /*Notifying MSR for TE-Link SRLG RowStatus-Destroy event */
        RM_GET_SEQ_NUM (&u4SeqNum);
        MEMSET (&SnmpNotifyInfo, TLM_ZERO, sizeof (tSnmpNotifyInfo));

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, TeLinkSrlgRowStatus,
                              u4SeqNum, TRUE, TlmLock,
                              TlmUnLock, 1, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pTeIface->i4IfIndex, pTeLinkSrlg->u4SrlgNo, DESTROY));
    }

    /* No need to send MSR Notifications for Dynamically created
     * Entries (Storage type = volatile) */
    if (pTeIface->i4StorageType == TLM_STORAGE_NON_VOLATILE)
    {
        /*Notifying MSR for TE-Link RowStatus-Destroy event */
        RM_GET_SEQ_NUM (&u4SeqNum);
        MEMSET (&SnmpNotifyInfo, TLM_ZERO, sizeof (tSnmpNotifyInfo));

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, TeLinkRowStatus,
                              u4SeqNum, TRUE, TlmLock, TlmUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pTeIface->i4IfIndex, DESTROY));
    }

    TMO_HASH_Delete_Node (pTeIfHashTable, &pTeIface->NextLinkNode,
                          IF_HASH_FN (pTeIface->i4IfIndex));

    TMO_HASH_Delete_Node (pTeIfNameHashTable, &pTeIface->NextTeLinkNameNode,
                          IF_HASH_NAME_FN (pTeIface->au1TeLinkName,
                                           pTeIface->u1TeLinkNameLen));

    TMO_SLL_Delete (&(gTlmGlobalInfo.sortTeIfLst),
                    &(pTeIface->NextSortLinkNode));

    TlmDeleteTeLinkIDBasedTree (pTeIface);

    TELINK_FREE (pTeIface);
    TLM_TRC_FN_EXIT ();
    return SNMP_SUCCESS;
}

/******************************************************************************
 * Function Name      : TlmDeleteTeDescr
 *
 * Description        : This routine is used to delete the descriptor
 *                      from Te-Link.
 *
 * Input(s)           : pIfDesr - Descriptor to delete.
 *                      pTeIface - Te-Link information.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : TLM_SUCCESS
 *****************************************************************************/
UINT4
TlmDeleteTeDescr (tTlmTeIfDescr * pIfDesr, tTlmTeLink * pTeIface)
{
    TMO_SLL_Delete (&(pTeIface->ifDescrList), &(pIfDesr->NextIfDescrNode));
    TEIFDESCR_FREE (pIfDesr);
    return TLM_SUCCESS;
}

/******************************************************************************
 * Function Name      : TlmDeleteTeLinkSrlg
 *
 * Description        : This routine is used to delete the Srlg from Te-Link.
 *
 * Input(s)           : pTeSrlg - Srlg information.
 *                      pTeIface - Te-Link information
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None.
 *****************************************************************************/
VOID
TlmDeleteTeLinkSrlg (tTlmTeLinkSrlg * pTeSrlg, tTlmTeLink * pTeIface)
{
    TMO_SLL_Delete (&(pTeIface->srlgList), &(pTeSrlg->NextLinkSrlgNode));
    TESRLG_FREE (pTeSrlg);
    return;
}

/******************************************************************************
 * Function Name      : TlmDeleteComponentIf
 *
 * Description        : This routine is used to delete the component link.
 *
 * Input(s)           : pComponentNode - Component node to delete.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : SNMP_SUCCESS
 *****************************************************************************/
INT4
TlmDeleteComponentIf (tTlmComponentLink * pComponentNode)
{
    tTMO_HASH_TABLE    *pTeIfHashTable = gTlmGlobalInfo.pComponentIfHashTable;
    tTlmComponentIfDescr *pCompIfDescr = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
    tTlmTeLink         *pTeLink = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();
    /* Getting back pointer of telink to
       which this component link is assiciated */
    pTeLink = pComponentNode->pTeLink;

    TlmUpdateComponentLink (pTeLink, TLM_OPER_DOWN);

    /* Updating TeLink values by default values */
    TlmUpdateTeLinkInfo (pComponentNode, pTeLink, TLM_COMPONENT_DELETE);
    /* Delete all the interface descriptors */
    TMO_DYN_SLL_Scan (&(pComponentNode->ifDescrList), pLstNode,
                      pTempNode, tTMO_SLL_NODE *)
    {
        pCompIfDescr = (tTlmComponentIfDescr *) pLstNode;
        TlmDeleteComponentDescr (pCompIfDescr, pComponentNode);

        /* No need to send MSR Notifications for Dynamically created
         * Entries (Storage type = volatile) */
        if (pCompIfDescr->i4StorageType == TLM_STORAGE_NON_VOLATILE)
        {
            /*Notifying MSR for Component-Link Descriptor RowStatus-Destroy event */
            RM_GET_SEQ_NUM (&u4SeqNum);
            MEMSET (&SnmpNotifyInfo, TLM_ZERO, sizeof (tSnmpNotifyInfo));

            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, ComponentLinkDescrRowStatus,
                                  u4SeqNum, TRUE, TlmLock,
                                  TlmUnLock, 1, SNMP_SUCCESS);

            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                              pComponentNode->i4IfIndex,
                              pCompIfDescr->u4DescrId, DESTROY));
        }

    }
    /*Deleting the link to TE link */
    pComponentNode->pTeLink = NULL;

    /* No need to send MSR Notifications for Dynamically created
     * Entries (Storage type = volatile) */
    if (pComponentNode->i4StorageType == TLM_STORAGE_NON_VOLATILE)
    {
        /*Notifying MSR for Component-Link RowStatus-Destroy event */
        RM_GET_SEQ_NUM (&u4SeqNum);
        MEMSET (&SnmpNotifyInfo, TLM_ZERO, sizeof (tSnmpNotifyInfo));

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, ComponentLinkRowStatus,
                              u4SeqNum, TRUE, TlmLock, TlmUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i",
                          pComponentNode->i4IfIndex, DESTROY));
    }

    TMO_HASH_Delete_Node (pTeIfHashTable,
                          (tTMO_HASH_NODE *) pComponentNode,
                          COMPONENT_IF_HASH_FN (pComponentNode->i4IfIndex));
    TMO_SLL_Delete (&(gTlmGlobalInfo.sortComponentIfLst),
                    &(pComponentNode->NextSortComponentNode));
    COMPONENT_LINK_FREE (pComponentNode);
    TLM_TRC_FN_EXIT ();
    return SNMP_SUCCESS;
}

/******************************************************************************
 * Function Name      : TlmDeleteComponentDescr
 *
 * Description        : This routine is used to delete the component descriptor
 *
 * Input(s)           : pIfDesr - Descriptor to delete.
 *                      pcomponentiface - Component to delete.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : TLM_SUCCESS
 *****************************************************************************/
UINT4
TlmDeleteComponentDescr (tTlmComponentIfDescr * pCompDescr,
                         tTlmComponentLink * pCompIface)
{
    tTlmTeLink         *pTeLink = NULL;
    tTlmTeIfDescr      *pTeIfDescNode = NULL;
    UINT4               u4Index = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();

    /* TeLink associated with this component link */
    pTeLink = pCompIface->pTeLink;

    if (pTeLink != NULL)
    {
        if ((pTeIfDescNode =
             (tTlmTeIfDescr *) TMO_SLL_First (&(pTeLink->ifDescrList))) != NULL)
        {
            pTeIfDescNode->i4SwitchingCap = TLM_DEF_SWITCHING_CAP;
            pTeIfDescNode->minLSPBw = TLM_DEF_BANDWIDTH_VALUE;
            for (u4Index = TLM_MIN_PRIORITY_LVL; u4Index < TLM_MAX_PRIORITY_LVL;
                 ++u4Index)
            {
                pTeIfDescNode->aMaxLSPBwPrio[u4Index] = TLM_DEF_BANDWIDTH_VALUE;
            }
        }
    }
    TMO_SLL_Delete (&(pCompIface->ifDescrList),
                    &(pCompDescr->NextIfComponentDescrNode));
    COMPONENT_IFDESCR_FREE (pCompDescr);

    TLM_TRC_FN_EXIT ();
    return TLM_SUCCESS;
}

/******************************************************************************
 * Function Name      : TlmUpdateTeLinkInfo
 *
 * Description        : This routine is used to update the Te-Link information
 *                      when we associate Te-Link with Component Link.
 *
 * Input(s)           : pTeComponent - Component Information.
 *                      pTeIface - Te-Link information.
 *                      u1RequestType - TLM_COMPONENT_ADD/TLM_COMPONENT_DELETE
 *
 * Output(s)          : None
 *
 * Return Value(s)    : TLM_SUCCESS?TLM_FAILURE
 *****************************************************************************/
UINT1
TlmUpdateTeLinkInfo (tTlmComponentLink * pTeComponent, tTlmTeLink * pTeIface,
                     UINT1 u1RequestType)
{
    tTlmComponentIfDescr *pComponentIfDescNode = NULL;
    tTlmTeIfDescr      *pTeIfDescNode = NULL;
    tCfaIfInfo          CfaIfInfo;
    tSNMP_OCTET_STRING_TYPE LspBw;
    UINT1               u1Array[TLM_MAX_BW_LEN];
    UINT4               u4Index = TLM_ZERO;
    INT4                i1RetVal = TLM_FAILURE;

    TLM_TRC_FN_ENTRY ();
    if (pTeIface == NULL)
    {
        TLM_TRC (TLM_ALL_TRC | TLM_FAILURE_TRC,
                 "TlmUpdateTeLinkInfo: No Te-Link information with given"
                 " information\r\n");
        return TLM_FAILURE;
    }

    MEMSET (u1Array, TLM_ZERO, sizeof (u1Array));
    MEMSET (&LspBw, TLM_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    LspBw.pu1_OctetList = &u1Array[TLM_ZERO];

    switch (u1RequestType)
    {
        case TLM_COMPONENT_UPDATE:
        {
            TLM_TRC (TLM_ALL_TRC | TLM_FAILURE_TRC,
                     "TlmUpdateTeLinkInfo: TLM_COMPONENT_UPDATE\r\n");
            /* Fill the band width information */
            for (u4Index = TLM_MIN_PRIORITY_LVL; u4Index < TLM_MAX_PRIORITY_LVL;
                 ++u4Index)
            {
                pTeIface->aUnResBw[u4Index].unResBw =
                    pTeComponent->aUnResBw[u4Index].unResBw;
                pTeIface->aUnResBw[u4Index].resBw =
                    pTeComponent->aUnResBw[u4Index].resBw;

            }

            MEMSET (&CfaIfInfo, TLM_ZERO, sizeof (tCfaIfInfo));

            if (CfaGetIfInfo ((UINT4) pTeComponent->i4IfIndex, &CfaIfInfo) !=
                CFA_SUCCESS)
            {
                pTeIface->maxBw = TLM_DEF_BANDWIDTH_VALUE;
            }
            else
            {
                if (CfaIfInfo.u4IfSpeed < TLM_MAX_IFSPEED_VALUE)
                {
                    pTeIface->maxBw = (FLT4) CfaIfInfo.u4IfSpeed;
                    TLM_CONVERT_BPS_TO_BYTES_PER_SEC (pTeIface->maxBw);
                }
                else
                {
                    pTeIface->maxBw = (FLT4) CfaIfInfo.u4IfHighSpeed;
                    TLM_CONVERT_MBPS_TO_BYTES_PER_SEC (pTeIface->maxBw);
                }
            }

            pTeIface->maxResBw = pTeComponent->maxResBw;

            pTeIface->availBw = pTeComponent->availBw;

            /* Filling Descriptor information to the  TeLink Descriptor table */

            pTeIfDescNode =
                (tTlmTeIfDescr *) TMO_SLL_First (&(pTeIface->ifDescrList));
            pComponentIfDescNode =
                (tTlmComponentIfDescr *)
                TMO_SLL_First (&(pTeComponent->ifDescrList));

            if ((pTeIfDescNode == NULL) || (pComponentIfDescNode == NULL))
            {
                return TLM_FAILURE;
            }
            pTeIfDescNode->i4SwitchingCap =
                pComponentIfDescNode->i4SwitchingCap;
            pTeIfDescNode->minLSPBw = pComponentIfDescNode->minLSPBw;
            pTeIfDescNode->i4EncodingType =
                pComponentIfDescNode->i4EncodingType;
            pTeIfDescNode->u2MTU = pComponentIfDescNode->u2MTU;
            pTeIfDescNode->i4StorageType = pComponentIfDescNode->i4StorageType;
            pTeIfDescNode->i4Indication = pComponentIfDescNode->i4Indication;

            for (u4Index = TLM_MIN_PRIORITY_LVL; u4Index < TLM_MAX_PRIORITY_LVL;
                 ++u4Index)
            {
                pTeIfDescNode->aMaxLSPBwPrio[u4Index] =
                    pComponentIfDescNode->aMaxLSPBwPrio[u4Index];
            }

            break;
        }

        case TLM_COMPONENT_DELETE:
        {

            TLM_TRC (TLM_ALL_TRC | TLM_FAILURE_TRC,
                     "TlmUpdateTeLinkInfo: TLM_COMPONENT_DELETE\r\n");
            for (u4Index = TLM_MIN_PRIORITY_LVL; u4Index < TLM_MAX_PRIORITY_LVL;
                 ++u4Index)
            {
                pTeIface->aUnResBw[u4Index].unResBw = TLM_DEF_BANDWIDTH_VALUE;
                pTeIface->aUnResBw[u4Index].resBw = TLM_DEF_BANDWIDTH_VALUE;
            }
            pTeIface->maxResBw = TLM_DEF_BANDWIDTH_VALUE;
            pTeIface->maxBw = TLM_DEF_BANDWIDTH_VALUE;
            pTeIface->availBw = TLM_DEF_BANDWIDTH_VALUE;

            if ((pTeIfDescNode =
                 (tTlmTeIfDescr *) TMO_SLL_First (&(pTeIface->ifDescrList))) !=
                NULL)
            {
                pTeIfDescNode->i4SwitchingCap = OSPF_TE_PSC_1;
                pTeIfDescNode->minLSPBw = TLM_DEF_BANDWIDTH_VALUE;
                pTeIfDescNode->i4EncodingType = TLM_TE_ENCODING_TYPE_PACKET;
                pTeIfDescNode->u2MTU = TLM_DEF_MTU_SIZE;
                pTeIfDescNode->i4StorageType = TLM_STORAGE_NON_VOLATILE;
                pTeIfDescNode->i4Indication = TLM_DEF_INVALID_INDICATION;

                for (u4Index = TLM_MIN_PRIORITY_LVL;
                     u4Index < TLM_MAX_PRIORITY_LVL; ++u4Index)
                {
                    pTeIfDescNode->aMaxLSPBwPrio[u4Index] =
                        TLM_DEF_BANDWIDTH_VALUE;
                }
            }

            break;
        }

        default:
            TLM_TRC (TLM_ALL_TRC | TLM_FAILURE_TRC,
                     "TlmUpdateTeLinkInfo: Default Case\r\n");
            return TLM_FAILURE;
    }
    TLM_TRC_FN_ENTRY ();
    return (UINT1) i1RetVal;
}

/******************************************************************************
 * Function Name      : TlmUpdateBundleTeLinkInfo
 *
 * Description        : This routine is used to update the bundle Te-Link information
 *                      when we associate member Te-Links with it.
 *
 * Input(s)           : pMemberTeIface - Member Te Link Information.
 *                      pBundleTeIface - Bundle Te-Link information.
 *                      u1RequestType - TLM_TE_UPDATE/TLM_TE_DELETE
 *
 * Output(s)          : None
 *
 * Return Value(s)    : TLM_SUCCESS/TLM_FAILURE
 *****************************************************************************/
INT1
TlmUpdateBundleTeLinkInfo (tTlmTeLink * pMemberTeIface,
                           tTlmTeLink * pBundleTeIface, UINT1 u1RequestType)
{
    tTlmTeIfDescr      *pTeIfDescNode = NULL;
    tTlmTeIfDescr      *pTeMemberIfDescNode = NULL;
    tTlmTeLink         *pUnBundleTeLink = NULL;
    tTlmTeIfDescr      *pUnBundleDescNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT4               u4Index = TLM_ZERO;
    INT1                i1RetVal = TLM_ZERO;

    if (pMemberTeIface == NULL || pBundleTeIface == NULL)
    {
        return TLM_SUCCESS;
    }

    switch (u1RequestType)
    {
        case TLM_TELINK_UPDATE:
        {
            if (pBundleTeIface->i4TeLinkType != TLM_BUNDLE)
            {
                return TLM_SUCCESS;
            }
            if (pBundleTeIface->u4TeMetric == TLM_DEF_METRIC_VALUE)
            {
                pBundleTeIface->u4TeMetric = pMemberTeIface->u4TeMetric;
            }

            if (pBundleTeIface->u4RsrcClassColor == TLM_ZERO)
            {
                pBundleTeIface->u4RsrcClassColor =
                    pMemberTeIface->u4RsrcClassColor;
            }
            if (pBundleTeIface->u4RemoteRtrId == TLM_ZERO)
            {
                pBundleTeIface->u4RemoteRtrId = pMemberTeIface->u4RemoteRtrId;
            }

            if ((pBundleTeIface->u1RemoteIpLen == TLM_ZERO) &&
                (pBundleTeIface->u1LocalIpLen == TLM_ZERO) &&
                (pBundleTeIface->u4RemoteIdentifier == TLM_ZERO) &&
                (pBundleTeIface->u4LocalIdentifier == TLM_ZERO))
            {
                pBundleTeIface->LocalIpAddr.u4_addr[0] =
                    pMemberTeIface->LocalIpAddr.u4_addr[0];
                pBundleTeIface->RemoteIpAddr.u4_addr[0] =
                    pMemberTeIface->RemoteIpAddr.u4_addr[0];
                pBundleTeIface->u1RemoteIpLen = pMemberTeIface->u1RemoteIpLen;
                pBundleTeIface->u1LocalIpLen = pMemberTeIface->u1LocalIpLen;
                pBundleTeIface->i4AddressType = pMemberTeIface->i4AddressType;
            }

            if ((pBundleTeIface->i4AddressType == TLM_ADDRESSTYPE_UNKNOWN)
                && (pBundleTeIface->u4RemoteIdentifier == TLM_ZERO) &&
                (pBundleTeIface->u4LocalIdentifier == TLM_ZERO))
            {
                pBundleTeIface->u4RemoteIdentifier =
                    pMemberTeIface->u4RemoteIdentifier;
                pBundleTeIface->u4LocalIdentifier =
                    pMemberTeIface->u4LocalIdentifier;
                pBundleTeIface->i4AddressType =
                    (INT4) pMemberTeIface->u4LocalIdentifier;
            }

            pTeIfDescNode =
                (tTlmTeIfDescr *)
                TMO_SLL_First (&(pBundleTeIface->ifDescrList));

            /*Get the bandwidth info from the member TE links of a 
             * bundle TE link*/

            for (u4Index = TLM_MIN_PRIORITY_LVL;
                 u4Index < TLM_MAX_PRIORITY_LVL; ++u4Index)
            {
                pBundleTeIface->aUnResBw[u4Index].unResBw +=
                    pMemberTeIface->aUnResBw[u4Index].unResBw;
                pBundleTeIface->aUnResBw[u4Index].resBw +=
                    pMemberTeIface->aUnResBw[u4Index].resBw;

            }

            if (pBundleTeIface->availBw < pMemberTeIface->availBw)
            {
                pBundleTeIface->availBw = pMemberTeIface->availBw;
            }

            pBundleTeIface->maxResBw += pMemberTeIface->maxResBw;

            pTeMemberIfDescNode =
                (tTlmTeIfDescr *)
                TMO_SLL_First (&(pMemberTeIface->ifDescrList));

            /*Either Descriptor entry exists (Or) Descriptor Table 
             * already updated*/
            if (pTeMemberIfDescNode == NULL || pTeIfDescNode == NULL)
            {
                return TLM_SUCCESS;
            }

            pTeIfDescNode->i4SwitchingCap = pTeMemberIfDescNode->i4SwitchingCap;
            pTeIfDescNode->minLSPBw = pTeMemberIfDescNode->minLSPBw;
            pTeIfDescNode->i4EncodingType = pTeMemberIfDescNode->i4EncodingType;
            pTeIfDescNode->u2MTU = pTeMemberIfDescNode->u2MTU;
            pTeIfDescNode->i4StorageType = pTeMemberIfDescNode->i4StorageType;
            pTeIfDescNode->i4Indication = pTeMemberIfDescNode->i4Indication;

            for (u4Index = TLM_MIN_PRIORITY_LVL; u4Index < TLM_MAX_PRIORITY_LVL;
                 ++u4Index)
            {
                if (pTeMemberIfDescNode->aMaxLSPBwPrio[u4Index] >
                    pTeIfDescNode->aMaxLSPBwPrio[u4Index])
                {
                    pTeIfDescNode->aMaxLSPBwPrio[u4Index] =
                        pTeMemberIfDescNode->aMaxLSPBwPrio[u4Index];
                }
            }

            i1RetVal = TLM_SUCCESS;
            break;
        }

        case TLM_TELINK_DELETE:
        {
            if (pBundleTeIface->i4TeLinkType != TLM_BUNDLE)
            {
                return TLM_SUCCESS;
            }
            pTeIfDescNode =
                (tTlmTeIfDescr *)
                TMO_SLL_First (&(pBundleTeIface->ifDescrList));

            /*Get the bandwidth info from the member TE links of a 
             * bundle TE link*/

            for (u4Index = TLM_MIN_PRIORITY_LVL;
                 u4Index < TLM_MAX_PRIORITY_LVL; ++u4Index)
            {
                pBundleTeIface->aUnResBw[u4Index].unResBw -=
                    pMemberTeIface->aUnResBw[u4Index].unResBw;
                pBundleTeIface->aUnResBw[u4Index].resBw -=
                    pMemberTeIface->aUnResBw[u4Index].resBw;

            }

            if (pBundleTeIface->availBw < pMemberTeIface->availBw)
            {
                pBundleTeIface->availBw = pMemberTeIface->availBw;
            }

            pBundleTeIface->maxResBw -= pMemberTeIface->maxResBw;

            pTeMemberIfDescNode =
                (tTlmTeIfDescr *)
                TMO_SLL_First (&(pMemberTeIface->ifDescrList));

            /*Either Descriptor entry exists (Or) Descriptor Table 
             * already updated*/
            if (pTeMemberIfDescNode == NULL || pTeIfDescNode == NULL)
            {
                return TLM_SUCCESS;
            }

            for (u4Index = TLM_MIN_PRIORITY_LVL; u4Index < TLM_MAX_PRIORITY_LVL;
                 ++u4Index)
            {
                if (pTeIfDescNode->aMaxLSPBwPrio[u4Index] ==
                    pTeMemberIfDescNode->aMaxLSPBwPrio[u4Index])
                {
                    TMO_SLL_Scan (&(pBundleTeIface->sortUnbundleTeList),
                                  pLstNode, tTMO_SLL_NODE *)
                    {
                        if (pUnBundleTeLink == NULL)
                        {
                            break;
                        }

                        pUnBundleTeLink =
                            GET_TE_LINK_TE_LIST_NODE_PTR_FROM_SORT_LST
                            (pLstNode);
                        pUnBundleDescNode =
                            (tTlmTeIfDescr *)
                            TMO_SLL_First (&(pUnBundleTeLink->ifDescrList));
                        if (pUnBundleDescNode == NULL)
                        {
                            break;
                        }
                        pTeIfDescNode->aMaxLSPBwPrio[u4Index] =
                            pUnBundleDescNode->aMaxLSPBwPrio[u4Index];

                        if ((pUnBundleDescNode->aMaxLSPBwPrio[u4Index] >
                             pTeIfDescNode->aMaxLSPBwPrio[u4Index])
                            && (pUnBundleDescNode->aMaxLSPBwPrio[u4Index] !=
                                pTeIfDescNode->aMaxLSPBwPrio[u4Index]))
                            pTeIfDescNode->aMaxLSPBwPrio[u4Index] =
                                pUnBundleDescNode->aMaxLSPBwPrio[u4Index];

                    }
                }
            }

            i1RetVal = TLM_SUCCESS;
            break;
        }

        default:
            TLM_TRC (TLM_ALL_TRC | TLM_FAILURE_TRC,
                     "TlmUpdateBundleTeLinkInfo: Default Case\r\n");
            return TLM_FAILURE;
    }
    return i1RetVal;
}

/* *********************************************************************** */
/* Function    : TlmCreateTeLinkTree                                       */
/*                                                                         */
/* Input       : None                                                      */
/*                                                                         */
/* Description : Creates a RB Tree for TE Link Entries                     */
/*                                                                         */
/* Output      : None                                                      */
/*                                                                         */
/* Returns     : TLM_SUCCESS / TLM_FAILURE                                 */
/* *********************************************************************** */
PUBLIC INT4
TlmCreateTeLinkTree ()
{
    gTlmGlobalInfo.TeLinkIDBasedTree =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tTlmTeLink,
                                             TeLinkIDBasedNode),
                              TlmTeLinkIDBasedCompareFn);

    if (gTlmGlobalInfo.TeLinkIDBasedTree == NULL)
    {
        return TLM_FAILURE;
    }

    return TLM_SUCCESS;
}

/* *********************************************************************** */
/* Function    : TlmDeleteTeLinkTree                                       */
/*                                                                         */
/* Input       : None                                                      */
/*                                                                         */
/* Description : Deletes a RB Tree for TE Link Entries                     */
/*                                                                         */
/* Output      : None                                                      */
/*                                                                         */
/* Returns     : TLM_SUCCESS / TLM_FAILURE                                 */
/* *********************************************************************** */
PUBLIC INT4
TlmDeleteTeLinkTree ()
{
    if (gTlmGlobalInfo.TeLinkIDBasedTree != NULL)
    {
        RBTreeDestroy (gTlmGlobalInfo.TeLinkIDBasedTree, NULL, 0);
    }

    gTlmGlobalInfo.TeLinkIDBasedTree = NULL;
    return TLM_SUCCESS;
}

/************************************************************************ */
/* Function    : TlmTeLinkIDBasedCompareFn                                 */
/*                                                                         */
/* Input       : pRBElem1 - TBTree Node Element 1                          */
/*               pRBElem2 - TBTree Node Element 2                          */
/*                                                                         */
/* Description : TlmTeLinkIDBasedCompareFn is the compare function that is */
/*               called whenever RB Tree is traversed.                     */
/*                                                                         */
/* Output      : None                                                      */
/*                                                                         */
/* Returns     : Positive or Negative  Integer value                       */
/* *********************************************************************** */

/* In compare Function - First argument is the item we insert, second
 * argument is the item in the tree */
PRIVATE INT4
TlmTeLinkIDBasedCompareFn (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tTlmTeLink         *pTeLink1 = (tTlmTeLink *) pRBElem1;
    tTlmTeLink         *pTeLink2 = (tTlmTeLink *) pRBElem2;

    if (pTeLink1->i4AddressType < pTeLink2->i4AddressType)
    {
        return TLM_RB_LESSER;
    }
    else if (pTeLink1->i4AddressType > pTeLink2->i4AddressType)
    {
        return TLM_RB_GREATER;
    }

    if (pTeLink1->i4AddressType == ADDR_TYPE_UNKNOWN)
    {
        if (pTeLink1->u4RemoteRtrId < pTeLink2->u4RemoteRtrId)
        {
            return TLM_RB_LESSER;
        }
        else if (pTeLink1->u4RemoteRtrId > pTeLink2->u4RemoteRtrId)
        {
            return TLM_RB_GREATER;
        }
        if (pTeLink1->u4RemoteIdentifier < pTeLink2->u4RemoteIdentifier)
        {
            return TLM_RB_LESSER;
        }
        else if (pTeLink1->u4RemoteIdentifier > pTeLink2->u4RemoteIdentifier)
        {
            return TLM_RB_GREATER;
        }
        if (pTeLink1->u4LocalIdentifier < pTeLink2->u4LocalIdentifier)
        {
            return TLM_RB_LESSER;
        }
        else if (pTeLink1->u4LocalIdentifier > pTeLink2->u4LocalIdentifier)
        {
            return TLM_RB_GREATER;
        }
    }

    if (pTeLink1->i4AddressType == ADDR_TYPE_IPV4)
    {
        if (pTeLink1->RemoteIpAddr.u4_addr[0] <
            pTeLink2->RemoteIpAddr.u4_addr[0])
        {
            return TLM_RB_LESSER;
        }
        else if (pTeLink1->RemoteIpAddr.u4_addr[0] >
                 pTeLink2->RemoteIpAddr.u4_addr[0])
        {
            return TLM_RB_GREATER;
        }
        if (pTeLink1->LocalIpAddr.u4_addr[0] < pTeLink2->LocalIpAddr.u4_addr[0])
        {
            return TLM_RB_LESSER;
        }
        else if (pTeLink1->LocalIpAddr.u4_addr[0] >
                 pTeLink2->LocalIpAddr.u4_addr[0])
        {
            return TLM_RB_GREATER;
        }
    }
    return TLM_RB_EQUAL;
}

/************************************************************************* */
/* Function    : TlmAddTeLinkToIDBasedTree                                 */
/*                                                                         */
/* Input       : pTeLink - Pointer to the TE Link needs to be added in     */
/*               TeLinkIDBasedTree                                         */
/*                                                                         */
/* Description : TlmAddTeLinkToIDBasedTree adds the given pTeLink to the   */
/*               Global RB Tree                                            */
/*                                                                         */
/* Output      : None                                                      */
/*                                                                         */
/* Returns     : TLM_SUCCESS / TLM_FAILURE                                 */
/* *********************************************************************** */
INT4
TlmAddTeLinkToIDBasedTree (tTlmTeLink * pTeLink)
{
    tTlmTeLink         *pTeLinkIf = NULL;

    if (pTeLink->i4AddressType == ADDR_TYPE_UNKNOWN)
    {
        pTeLinkIf = TlmGetUnNumberedTeLink (pTeLink->u4RemoteRtrId,
                                            pTeLink->u4RemoteIdentifier,
                                            pTeLink->u4LocalIdentifier);
    }
    else
    {
        pTeLinkIf = TlmGetNumberedTeLink (pTeLink->RemoteIpAddr.u4_addr[0],
                                          pTeLink->LocalIpAddr.u4_addr[0]);
    }

    if ((pTeLinkIf != NULL) && (pTeLinkIf->i4IfIndex != pTeLink->i4IfIndex))
    {
        return TLM_FAILURE;
    }
    else if (pTeLinkIf != NULL)
    {
        return TLM_SUCCESS;
    }

    if (RBTreeAdd (gTlmGlobalInfo.TeLinkIDBasedTree,
                   (tRBElem *) pTeLink) != RB_SUCCESS)
    {
        return TLM_FAILURE;
    }

    return TLM_SUCCESS;
}

/************************************************************************* */
/* Function    : TlmDeleteTeLinkIDBasedTree                                */
/*                                                                         */
/* Input       : pTeLink - Pointer to the TE Link needs to be deleted from */
/*               TeLinkIDBasedTree                                         */
/*                                                                         */
/* Description : This function deletes the given pTeLink from the          */
/*               Global RB Tree                                            */
/*                                                                         */
/* Output      : None                                                      */
/*                                                                         */
/* Returns     : TLM_SUCCESS / TLM_FAILURE                                 */
/* *********************************************************************** */
INT4
TlmDeleteTeLinkIDBasedTree (tTlmTeLink * pTeLink)
{
    RBTreeRem (gTlmGlobalInfo.TeLinkIDBasedTree, (tRBElem *) pTeLink);

    return TLM_SUCCESS;
}

/************************************************************************* */
/* Function    : TlmGetUnNumberedTeLink                                    */
/*                                                                         */
/* Input       : u4RemoteRtrId - Remote Router Id of the TE Link           */
/*               u4RemoteIdentifier - Remote Identifier of the TE Link     */
/*               u4LocalIdentifier - Local Identifier of the TE Link       */
/*                                                                         */
/* Description : This is to perform get operation for the given Remote     */
/*               Identifier and Remoter Id.                                */
/*               The caller of this function, should perform NULL check    */
/*               before using the Return parameter.                        */
/*                                                                         */
/* Output      : None                                                      */
/*                                                                         */
/* Returns     : Pointer to tTlmTeLink                                     */
/* *********************************************************************** */
tTlmTeLink         *
TlmGetUnNumberedTeLink (UINT4 u4RemoteRtrId, UINT4 u4RemoteIdentifier,
                        UINT4 u4LocalIdentifier)
{
    tTlmTeLink          TeLink;
    tTlmTeLink         *pRetTeLink = NULL;

    TeLink.i4AddressType = ADDR_TYPE_UNKNOWN;
    TeLink.u4RemoteRtrId = u4RemoteRtrId;
    TeLink.u4RemoteIdentifier = u4RemoteIdentifier;
    TeLink.u4LocalIdentifier = u4LocalIdentifier;

    pRetTeLink = (tTlmTeLink *) (VOID *) RBTreeGet
        (gTlmGlobalInfo.TeLinkIDBasedTree, (tRBElem *) & TeLink);

    return pRetTeLink;
}

/************************************************************************* */
/* Function    : TlmGetNumberedTeLink                                      */
/*                                                                         */
/* Input       : u4RemoteIpAddr - Remote IP Address of the TE Link         */
/*               u4LocalIpAddr - Local Ip Address of the TE Link           */
/*                                                                         */
/* Description : This is to perform get operation for the given Remote     */
/*               IP Address.                                               */
/*               The caller of this function, should perform NULL check    */
/*               before using the Return parameter.                        */
/*                                                                         */
/* Output      : None                                                      */
/*                                                                         */
/* Returns     : Pointer to tTlmTeLink                                     */
/* *********************************************************************** */
tTlmTeLink         *
TlmGetNumberedTeLink (UINT4 u4RemoteIpAddr, UINT4 u4LocalIpAddr)
{
    tTlmTeLink          TeLink;
    tTlmTeLink         *pRetTeLink = NULL;

    TeLink.i4AddressType = ADDR_TYPE_IPV4;
    TeLink.RemoteIpAddr.u4_addr[0] = u4RemoteIpAddr;
    TeLink.LocalIpAddr.u4_addr[0] = u4LocalIpAddr;

    pRetTeLink = (tTlmTeLink *) (VOID *) RBTreeGet
        (gTlmGlobalInfo.TeLinkIDBasedTree, (tRBElem *) & TeLink);

    return pRetTeLink;
}
