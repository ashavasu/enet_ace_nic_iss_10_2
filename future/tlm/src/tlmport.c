/********************************************************************
 * Copyright (C) 2011 Aricent Group . All Rights Reserved
 *
 * $Id: tlmport.c,v 1.7 2016/07/22 09:45:48 siva Exp $
 *
 * Description: This file contains porting related routines for TLM.
 ***********************************************************************/

#include "tlminc.h"
#include "tlmcli.h"
#include "fscfacli.h"
#include "ifmibcli.h"
#include "snmputil.h"
#include "rmgr.h"

extern UINT4        TeLinkRowStatus[12];
extern INT4         IfMainAdminStatusSet (tSnmpIndex *, tRetVal *);
extern INT4         IfMainRowStatusSet (tSnmpIndex *, tRetVal *);
extern INT4         IfMainTypeSet (tSnmpIndex *, tRetVal *);
extern INT4         IfStackStatusSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData);

extern INT1 nmhTestv2IfMainType ARG_LIST ((UINT4 *pu4ErrorCode,
                                           INT4 i4IfMainIndex,
                                           INT4 i4TestValIfMainType));
extern INT1 nmhSetIfMainType ARG_LIST ((INT4 i4IfMainIndex,
                                        INT4 i4SetValIfMainType));

extern INT1 nmhTestv2IfStackStatus ARG_LIST ((UINT4 *pu4ErrorCode,
                                              INT4 i4IfStackHigherLayer,
                                              INT4 i4IfStackLowerLayer,
                                              INT4 i4TestValIfStackStatus));
extern INT1 nmhSetIfStackStatus ARG_LIST ((INT4 i4IfStackHigherLayer,
                                           INT4 i4IfStackLowerLayer,
                                           INT4 i4SetValIfStackStatus));

extern INT1 nmhTestv2IfMainRowStatus ARG_LIST ((UINT4 *pu4ErrorCode,
                                                INT4 i4IfMainIndex,
                                                INT4 i4TestValIfMainRowStatus));
extern INT1 nmhSetIfMainRowStatus ARG_LIST ((INT4 i4IfMainIndex,
                                             INT4 i4SetValIfMainRowStatus));

extern INT1 nmhTestv2IfMainAdminStatus ARG_LIST ((UINT4 *pu4ErrorCode,
                                                  INT4 i4IfMainIndex,
                                                  INT4
                                                  i4TestValIfMainAdminStatus));
extern INT1         nmhSetIfMainAdminStatus
ARG_LIST ((INT4 i4IfMainIndex, INT4 i4SetValIfMainAdminStatus));

/******************************************************************************
 * Function Name      : TlmFloatAdd
 *
 * Description        : Function to add two float values.
 *                      The function can be modified to fulfill the requirements
 *                      of porting specifications.
 *
 *
 * Input(s)           : f4Value1, f4Value2 - float values to be added 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : f4ReturnValue
 *****************************************************************************/

FLT4
TlmFloatAdd (FLT4 f4Value1, FLT4 f4Value2)
{
    FLT4                f4ReturnValue = (FLT4) TLM_ZERO;
    f4ReturnValue = f4Value1 + f4Value2;
    return f4ReturnValue;
}

/******************************************************************************
 * Function Name      : TlmFloatSubtract
 *
 * Description        : Function to subtract two float values.
 *                      The function can be modified to fulfill the requirements
 *                      of porting specifications.
 *
 *
 * Input(s)           : f4Minuend - from which f4Subtrahend will be subtracted
 *
 * Output(s)          : None
 *
 * Return Value(s)    : f4ReturnValue
 *****************************************************************************/

FLT4
TlmFloatSubtract (FLT4 f4Minuend, FLT4 f4Subtrahend)
{
    FLT4                f4ReturnValue = (FLT4) TLM_ZERO;
    f4ReturnValue = f4Minuend - f4Subtrahend;
    if (f4ReturnValue < TLM_ZERO)
    {
        f4ReturnValue = TLM_ZERO;
    }	
    return f4ReturnValue;
}

/*****************************************************************************
 *
 *    Function Name      : TlmPortCreateTeIfOrMplsIf
 *
 *    Description        : This function is for creating TeLink/MPLS Interface.
 *
 *    Input(s)           : u4TeIfIndex - TE Link IfIndex
 *                         u1IfType    - Interface Type  
 *    Output(s)          : None.
 *
 *    Returns            : TLM_SUCCESS/TLM_FAILURE
 *****************************************************************************/
INT4
TlmPortCreateTeIfOrMplsIf (UINT4 u4IfIndex, UINT1 u1IfType)
{
    UINT4               u4ErrCode = TLM_ZERO;
    INT4                i4IfIndex = (INT4) u4IfIndex;

    CFA_LOCK ();

    if (nmhSetIfMainRowStatus (i4IfIndex, CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return TLM_FAILURE;
    }

    if (nmhTestv2IfMainType (&u4ErrCode, i4IfIndex, u1IfType) == SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (i4IfIndex, DESTROY);

        CFA_UNLOCK ();
        return TLM_FAILURE;
    }

    if (nmhSetIfMainType (i4IfIndex, u1IfType) == SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (i4IfIndex, DESTROY);

        CFA_UNLOCK ();
        return TLM_FAILURE;
    }

    if (nmhTestv2IfMainRowStatus (&u4ErrCode, i4IfIndex, ACTIVE)
        == SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (i4IfIndex, DESTROY);

        CFA_UNLOCK ();
        return TLM_FAILURE;
    }

    if (nmhSetIfMainRowStatus (i4IfIndex, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (i4IfIndex, DESTROY);

        CFA_UNLOCK ();
        return TLM_FAILURE;
    }

    if (nmhTestv2IfMainAdminStatus (&u4ErrCode, i4IfIndex, CFA_IF_UP)
        == SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (i4IfIndex, DESTROY);

        CFA_UNLOCK ();
        return TLM_FAILURE;
    }

    nmhSetIfMainAdminStatus (i4IfIndex, CFA_IF_UP);

    CFA_UNLOCK ();
    return TLM_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : TlmPortStackTeLinkIfOrMplsIf
 *
 *    Description        : This function is for stacking TE link over comp link
 *
 *    Input(s)           : u4HighIfIndex - TE Link IfIndex/MPLS IfIndex
 *                         u4LowIfIndex  - Comp Link IfIndex/TeLink IfIndex 
 *                         u4RowStatus  - RowStatus Value
 *    Output(s)          : None.
 *
 *    Returns            : TLM_SUCCESS/TLM_FAILURE
 *****************************************************************************/
INT4
TlmPortStackTeLinkIfOrMplsIf (INT4 i4HighIfIndex, INT4 i4LowIfIndex,
                              INT4 i4RowStatus)
{
    UINT4               u4ErrCode = TLM_ZERO;

    CFA_LOCK ();

    if (nmhTestv2IfStackStatus (&u4ErrCode, i4HighIfIndex, i4LowIfIndex,
                                i4RowStatus) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return TLM_FAILURE;
    }

    nmhSetIfStackStatus (i4HighIfIndex, i4LowIfIndex, i4RowStatus);

    CFA_UNLOCK ();
    return TLM_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : TlmPortDeleteTeLinkIfOrMplsIf 
 *
 *    Description        : This function creates the TE Link/MPLS Interface 
 *                         in ifMainTable.
 *
 *    Input(s)           : u4IfIndex - Interface Index.
 *
 *    Output(s)          : None.
 *
 *    Returns            : TLM_SUCCESS or TLM_FAILURE.
 *
 *****************************************************************************/

INT4
TlmPortDeleteTeLinkIfOrMplsIf (INT4 i4IfIndex)
{
    UINT4               u4ErrCode = 0;

    TLM_TRC_FN_ENTRY ();
    CFA_LOCK ();

    if (nmhTestv2IfMainRowStatus (&u4ErrCode, i4IfIndex,
                                  DESTROY) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return TLM_FAILURE;
    }

    if (nmhTestv2IfMainAdminStatus (&u4ErrCode, i4IfIndex, CFA_IF_DOWN)
        == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return TLM_FAILURE;
    }

    nmhSetIfMainAdminStatus (i4IfIndex, CFA_IF_DOWN);

    nmhSetIfMainRowStatus (i4IfIndex, DESTROY);

    CFA_UNLOCK ();

    TLM_TRC_FN_EXIT ();
    return TLM_SUCCESS;
}

/******************************************************************************
 * Function Name      : TlmExtDeleteTeLink
 *
 * Description        : This routine is used to delete TE Link interface
 *                      from the external modules
 *
 * Input(s)           : pTeIface - TeLink information
 *
 * Output(s)          : None
 *
 * Return Value(s)    : TLM_SUCCESS
 *****************************************************************************/

INT4
TlmExtDeleteTeLink (tTlmTeLink * pTeIface)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();
    MEMSET (&SnmpNotifyInfo, TLM_ZERO, sizeof (tSnmpNotifyInfo));

    TlmDeleteTeIf (pTeIface);

    /*Notifying MSR for TE-Link Descriptor RowStatus-Destroy event */
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, TeLinkRowStatus,
                          u4SeqNum, TRUE, TlmLock, TlmUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", pTeIface->i4IfIndex, DESTROY));

    TLM_TRC_FN_EXIT ();
    return TLM_SUCCESS;
}

/****************************************************************************/
/*
 * Function Name : TlmEnqueueMsgToTlmQ
 * Description   : Enqueues the message to TLM Message Queue and generate 
 *                 appropriate events
 * Input(s)      : aTaskEnqMsgInfo - Enqueued message.
 * Output(s)     : None.
 * Return(s)     : TLM_SUCCESS or TLM_FAILURE
 */
/****************************************************************************/
PUBLIC INT4
TlmEnqueueMsgToTlmQ (tTlmCfgMsg * pMsg)
{
    UINT1              *pu1TmpMsg = NULL;

    TLM_TRC_FN_ENTRY ();
    if (pMsg == NULL)
    {
        TLM_TRC (TLM_CRITICAL_TRC,
                 "TlmEnqueueMsgToTlmQ: CFG msg is invalid \n");
        return TLM_FAILURE;

    }

    pu1TmpMsg
        =
        (UINT1 *) MemAllocMemBlk (TLMMemPoolIds[MAX_TLM_QUEUE_SIZE_SIZING_ID]);

    if (pu1TmpMsg == NULL)
    {
        return TLM_FAILURE;
    }

    MEMCPY (pu1TmpMsg, pMsg, sizeof (tTlmCfgMsg));

    if (OsixQueSend (TLM_CFG_QID, (UINT1 *) (&pu1TmpMsg),
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (TLMMemPoolIds[MAX_TLM_QUEUE_SIZE_SIZING_ID],
                            (UINT1 *) pu1TmpMsg);
        TLM_TRC (TLM_CRITICAL_TRC,
                 "TlmEnqueueMsgToTlmQ: Enqueue Configuration Message FAILED \n");

        return TLM_FAILURE;
    }

    /* Sending Event */
    if (OsixEvtSend (TLM_TASK_ID, pMsg->u4MsgType) != OSIX_SUCCESS)
    {
        TLM_TRC (TLM_CRITICAL_TRC,
                 "TlmEnqueueMsgToTlmQ: Sending Event for condifurgation"
                 " message to TLM Task FAILED \n");
        return TLM_FAILURE;
    }
    TLM_TRC_FN_EXIT ();
    return TLM_SUCCESS;
}

/*****************************************************************************/
/* Function     : TlmPortCfaRegLL                                            */
/*                                                                           */
/* Description  : TLM module during initialization will invoke this API after*/
/*                loading the structure CfaRegParams with necessary callback */
/*                functions. On receiving a interface status changes, CFA    */
/*                module should invoke the mentioned call back functions     */
/*                appropriately.                                             */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CFA_SUCCESS/CFA_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
TlmPortCfaRegLL (VOID)
{
    tCfaRegParams       CfaRegParams;
    INT4                i4RetVal = OSIX_SUCCESS;
    MEMSET (&CfaRegParams, 0, sizeof (tCfaRegParams));

    TLM_TRC_FN_ENTRY ();
    CfaRegParams.u2LenOrType = CFA_TELINK;
    CfaRegParams.u2RegMask = CFA_IF_DELETE | CFA_IF_OPER_ST_CHG;
    CfaRegParams.pIfCreate = NULL;
    CfaRegParams.pIfDelete = TlmApiIfDeleteCallBack;
    CfaRegParams.pIfUpdate = NULL;
    CfaRegParams.pIfOperStChg = TlmApiIfOperChgCallBack;
    CfaRegParams.pIfRcvPkt = NULL;

    if (CfaRegisterHL (&CfaRegParams) != CFA_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }
    TLM_TRC_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function     : TlmPortCfaDeRegisterLL                                     */
/*                                                                           */
/* Description  : This API will be invoked by TLM whenever the module is     */
/*                de-initialized. This API should relinquish all the         */
/*                registrations earlier done by TLM module during            */
/*                initialization.                                            */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CFA_SUCCESS/CFA_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
TlmPortCfaDeRegisterLL (UINT2 u2LenOrType)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    TLM_TRC_FN_ENTRY ();
    if (CfaDeregisterHL (u2LenOrType) != CFA_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }
    TLM_TRC_FN_EXIT ();
    return i4RetVal;
}
