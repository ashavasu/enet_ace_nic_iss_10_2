/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: tlmmpls.c,v 1.2 2011/12/09 06:16:02 siva Exp $
*
* Description: Protocol Low Level Routines
* *********************************************************************/

#include "tlminc.h"

PRIVATE INT4 TlmMplsIsMsgForDiffServValid PROTO ((UINT4, UINT1, UINT1, UINT1,
                                                  UINT1));

/******************************************************************************
 * Function Name      : TlmMplsIsMsgForDiffServValid
 *
 * Description        : This function validates the message from MPLS for
 *                      Diffserv.
 *
 * Input(s)           : u4MsgType - Message from MPLS
 *                      u1DsTeStatus - DSTE status.
 *                      u1ClassType - Class Type
 *                      u1PremptPrio - Priority
 *                      u1TeClass - TE-Class                
 *
 * Output(s)          : None
 *
 * Return Value(s)    : TLM_SUCCESS / TLM_FAILURE
 *
 ******************************************************************************/
PRIVATE INT4
TlmMplsIsMsgForDiffServValid (UINT4 u4MsgType, UINT1 u1DsTeStatus,
                              UINT1 u1ClassType, UINT1 u1PremptPrio,
                              UINT1 u1TeClass)
{
    TLM_TRC_FN_ENTRY ();
    if (TMO_SLL_Count (&(gTlmGlobalInfo.sortTeIfLst)) > TLM_ZERO)
    {
        TLM_TRC (TLM_FAILURE_TRC,
                 "TlmMplsIsMsgForDiffServValid: Cannot Handle Msg from MPLS "
                 "- TE-Links Present\n");
        return TLM_FAILURE;
    }

    if (TMO_SLL_Count (&(gTlmGlobalInfo.sortComponentIfLst)) > TLM_ZERO)
    {
        TLM_TRC (TLM_FAILURE_TRC,
                 "TlmMplsIsMsgForDiffServValid: Cannot Handle Msg from MPLS "
                 "- Component Links Present\n");
        return TLM_FAILURE;
    }

    /* Validations for TLM_DSTE_STATUS_MSG passed. So, return */
    if (u4MsgType == TLM_DSTE_STATUS_MSG)
    {
        return TLM_SUCCESS;
    }

    if (gTlmGlobalInfo.u1DSTEStatus == TLM_DISABLED)
    {
        TLM_TRC (TLM_FAILURE_TRC,
                 "TlmMplsIsMsgForDiffServValid: Cannot Handle Msg from MPLS"
                 "- DSTE Status Disabled\n");
        return TLM_FAILURE;
    }

    if (!IS_VALID_CLASS_TYPE (u1ClassType))
    {
        TLM_TRC (TLM_FAILURE_TRC,
                 "TlmMplsIsMsgForDiffServValid: Cannot Handle Msg from MPLS"
                 "- Invalid Class Type\n");
        return TLM_FAILURE;
    }

    /* Validations for TLM_MPLS_CHG_CLASS_TYPE_MSG passed. So, return */
    if (u4MsgType == TLM_MPLS_CHG_CLASS_TYPE_MSG)
    {
        return TLM_SUCCESS;
    }

    if ((!IS_VALID_PRIORITY (u1PremptPrio)) || (!IS_VALID_TE_CLASS (u1TeClass)))
    {
        TLM_TRC (TLM_FAILURE_TRC,
                 "TlmMplsIsMsgForDiffServValid: Cannot Handle Msg from MPLS"
                 "- Invalid Priority or TE-Class\n");
        return TLM_FAILURE;
    }

    TLM_TRC_FN_EXIT ();
    /* All Validations passed. So, return. */
    return TLM_SUCCESS;
}

/******************************************************************************
 * Function Name      : TlmMplsHandleDsTeStatus
 *
 * Description        : This function handles DiffServ-TE status received from
 *                      MPLS module.
 *
 * Input(s)           : u1DsTeStatus - DSTE status Enabled / Disabled.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : TLM_SUCCESS / TLM_FAILURE
 *
 ******************************************************************************/

PUBLIC INT4
TlmMplsHandleDsTeStatus (UINT1 u1DsTeStatus)
{
    UINT1               u1Counter = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();
    if (TlmMplsIsMsgForDiffServValid (TLM_DSTE_STATUS_MSG,
                                      u1DsTeStatus, TLM_ZERO, TLM_ZERO,
                                      TLM_ZERO) != TLM_SUCCESS)
    {
        TLM_TRC (TLM_FAILURE_TRC,
                 "TlmMplsHandleDsTeStatus: Message from MPLS Invalid\n");
        return TLM_FAILURE;
    }

    switch (u1DsTeStatus)
    {
        case TLM_ENABLED:
        {
            gTlmGlobalInfo.u1ClassType = TLM_INVALID_CLASS_TYPE;

            for (u1Counter = TLM_ZERO; u1Counter < TLM_MAX_TE_CLASS;
                 u1Counter++)
            {
                gTlmGlobalInfo.aTlmTeClassMap[u1Counter].u1ClassType
                    = TLM_INVALID_CLASS_TYPE;
                gTlmGlobalInfo.aTlmTeClassMap[u1Counter].u1PremptPrio
                    = TLM_INVALID_PRIORITY;
                gTlmGlobalInfo.aTlmTeClassMap[u1Counter].u1IsSet
                    = TLM_INVALID_STATUS;
            }

            break;
        }

        case TLM_DISABLED:
        {
            gTlmGlobalInfo.u1ClassType = TLM_ZERO;

            for (u1Counter = TLM_ZERO; u1Counter < TLM_MAX_TE_CLASS;
                 u1Counter++)
            {
                gTlmGlobalInfo.aTlmTeClassMap[u1Counter].u1ClassType = TLM_ZERO;
                gTlmGlobalInfo.aTlmTeClassMap[u1Counter].u1PremptPrio
                    = u1Counter;
                gTlmGlobalInfo.aTlmTeClassMap[u1Counter].u1IsSet
                    = TLM_CLASS_TYPE_SET;
            }

            break;
        }

        default:
            TLM_TRC (TLM_FAILURE_TRC,
                     "TlmMplsHandleDsTeStatus: Invalid Diffserv TE Status "
                     "received from MPLS\n");
            return TLM_FAILURE;
    }

    gTlmGlobalInfo.u1DsTeStatus = u1DsTeStatus;

    TLM_TRC_FN_EXIT ();
    return TLM_SUCCESS;
}

/******************************************************************************
 * Function Name      : TlmMplsSetClassType
 *
 * Description        : This function sets the class type given by MPLS
 *                      MPLS in TLM..
 *
 * Input(s)           : u1ClassType - Class type value.
 *                      u1IsAdded - Flag to Add/Delete class type
 *
 * Output(s)          : None
 * Return Value(s)    : TLM_SUCCESS / TLM_FAILURE
 *
 ******************************************************************************/
PUBLIC INT4
TlmMplsSetClassType (UINT1 u1ClassType, UINT1 u1IsAdded)
{
    TLM_TRC_FN_ENTRY ();
    if (TlmMplsIsMsgForDiffServValid (TLM_MPLS_CHG_CLASS_TYPE_MSG,
                                      TLM_ZERO, u1ClassType, TLM_ZERO,
                                      TLM_ZERO) != TLM_SUCCESS)
    {
        TLM_TRC (TLM_FAILURE_TRC,
                 "TlmMplsSetClassType: Message from MPLS Invalid\n");
        return TLM_FAILURE;
    }

    switch (u1IsAdded)
    {
        case TLM_CLASS_TYPE_ADD:
        {
            gTlmGlobalInfo.u1ClassType |= (TLM_ONE << u1ClassType);
            break;
        }

        case TLM_CLASS_TYPE_DELETE:
        {
            gTlmGlobalInfo.u1ClassType &= ~(TLM_ONE << u1ClassType);
            break;
        }

        default:
        {
            TLM_TRC (TLM_FAILURE_TRC,
                     "TlmSetClassType: Invalid Class Type Request\n");
            return TLM_FAILURE;
        }
    }

    TLM_TRC_FN_EXIT ();
    return TLM_SUCCESS;
}

/******************************************************************************
 * Function Name      : TlmMplsSetTeClassForCTPrio
 *
 * Description        : This function handles association of Class Types and
 *                      Priorities to Te-Class. It is received as a message
 *                      from MPLS if Diffserv is enabled.
 *
 * Input(s)           : u1ClassType - Class type value.
 *                      u1PremptPrio - Premption Priority.
 *                      u1TeClass - Value of Te-Class.
 *                      u4TeClsRowStatus - Value of Te-Class Row Status
 *
 * Output(s)          : None
 * Return Value(s)    : TLM_SUCCESS / TLM_FAILURE
 *
 ******************************************************************************/
PUBLIC INT4
TlmMplsSetTeClassForCTPrio (UINT1 u1ClassType, UINT1 u1PremptPrio,
                            UINT1 u1TeClass, UINT4 u4TeClsRowStatus)
{
    INT4                i4RetVal = TLM_FAILURE;

    TLM_TRC_FN_ENTRY ();
    if (TlmMplsIsMsgForDiffServValid (TLM_MPLS_TECLASSS_CHG_STATUS_MSG,
                                      TLM_ZERO, u1ClassType, u1PremptPrio,
                                      u1TeClass) != TLM_SUCCESS)
    {
        TLM_TRC (TLM_FAILURE_TRC,
                 "TlmMplsSetTeClassForCTPrio: Message from MPLS Invalid\n");
        return i4RetVal;
    }

    switch (u4TeClsRowStatus)
    {
        case TLM_MPLS_TE_CLASS_CREATED:
        {
            if (gTlmGlobalInfo.u1ClassType & (TLM_ONE << u1ClassType))
            {
                gTlmGlobalInfo.aTlmTeClassMap[u1TeClass].u1ClassType
                    = u1ClassType;
                gTlmGlobalInfo.aTlmTeClassMap[u1TeClass].u1PremptPrio
                    = u1PremptPrio;
                gTlmGlobalInfo.aTlmTeClassMap[u1TeClass].u1IsSet
                    = TLM_CLASS_TYPE_SET;
                i4RetVal = TLM_SUCCESS;
            }
            else
            {
                i4RetVal = TLM_FAILURE;
                TLM_TRC (TLM_FAILURE_TRC,
                         "TlmMplsSetTeClassForCTPrio: Addition Fails "
                         "- Wrong Class Type\n");
            }

            break;
        }

        case TLM_MPLS_TE_CLASS_DELETED:
        {
            if ((gTlmGlobalInfo.aTlmTeClassMap[u1TeClass].u1ClassType
                 == u1ClassType) &&
                (gTlmGlobalInfo.aTlmTeClassMap[u1TeClass].u1PremptPrio
                 == u1PremptPrio))
            {
                gTlmGlobalInfo.aTlmTeClassMap[u1TeClass].u1ClassType
                    = TLM_INVALID_CLASS_TYPE;
                gTlmGlobalInfo.aTlmTeClassMap[u1TeClass].u1PremptPrio
                    = TLM_INVALID_PRIORITY;
                gTlmGlobalInfo.aTlmTeClassMap[u1TeClass].u1IsSet
                    = TLM_INVALID_STATUS;
                i4RetVal = TLM_SUCCESS;
            }
            else
            {
                i4RetVal = TLM_FAILURE;
                TLM_TRC (TLM_FAILURE_TRC,
                         "TlmMplsSetTeClassForCTPrio: Deletion fails "
                         "- Invalid input\n");
            }

            break;
        }

        case TLM_MPLS_TE_CLASS_NOT_IN_SERVICE:
        {
            i4RetVal = TLM_SUCCESS;
            break;
        }

        default:
        {
            i4RetVal = TLM_FAILURE;
            break;
        }
    }

    TLM_TRC_FN_EXIT ();
    return i4RetVal;
}
