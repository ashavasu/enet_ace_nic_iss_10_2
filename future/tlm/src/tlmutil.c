/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: tlmutil.c,v 1.13 2015/02/28 12:19:21 siva Exp $
 *
 * Description: This files contains the utility functions for TLM
 ***********************************************************************/

#include "tlminc.h"
#include "tlmcli.h"

/*****************************************************************************
 * Function Name      : TlmGetTeLinkBwThreshold                              *
 *                                                                           *
 * Description        : This Routine is used to Get the threshold value      *
 *                                                                           *
 * Input(s)           : u4ThresholdIndex - Threshold index (Table index)     *
 *                      i4BwThresholdIndex - Bandwidth threshold index       *
 *                                                                           *
 * Output(s)          : pi4RetVal - Bandwidth threshold value                *
 *                                                                           *
 * Return Value(s)    : None.                                                *
 *                                                                           *
 *****************************************************************************/

PUBLIC VOID
TlmGetTeLinkBwThreshold (UINT4 u4ThresholdIndex, INT4 i4BwThresholdIndex,
                         tTlmTeLink * pTlmTeLink, INT4 *pi4RetVal)
{
    if (i4BwThresholdIndex == TLM_ONE)
    {
        *pi4RetVal = pTlmTeLink->aTlmBWThresholdUp[u4ThresholdIndex];
    }
    else
    {
        *pi4RetVal = pTlmTeLink->aTlmBWThresholdDown[u4ThresholdIndex];
    }
}

/*****************************************************************************
 * Function Name      : TlmSetTeLinkBwThreshold                              *
 *                                                                           *
 * Description        : This Routine is used to Set the threshold value      *
 *                                                                           *
 * Input(s)           : u4ThresholdIndex - Threshold index (Table index)     *
 *                      i4BwThresholdIndex - Bandwidth threshold index       *
 *                                                                           *
 * Output(s)          : i4Value - Bandwidth threshold value                  *
 *                                                                           *
 * Return Value(s)    : None.                                                *
 *                                                                           *
 *****************************************************************************/

PUBLIC VOID
TlmSetTeLinkBwThreshold (UINT4 u4ThresholdIndex, INT4 i4BwThresholdIndex,
                         tTlmTeLink * pTlmTeLink, INT4 i4Value)
{
    if (i4BwThresholdIndex == TLM_ONE)
    {
        pTlmTeLink->aTlmBWThresholdUp[u4ThresholdIndex] = (UINT1) i4Value;
    }
    else
    {
        pTlmTeLink->aTlmBWThresholdDown[u4ThresholdIndex] = (UINT1) i4Value;
    }
}

/*****************************************************************************
 * Function Name      : TlmUtilUpdateUnReservBw                              *
 *                                                                           *
 * Description        : This function is used to reset the Unreserved        * 
 *                      bandwidth and available bandwidth to maxReservBw     *
 *                                                                           *
 * Input(s)           : pCompIface - Component Link Interface                *
 *                                                                           *
 * Output(s)          : None.                                                *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 *                                                                           *
 *****************************************************************************/

VOID
TlmUtilUpdateUnReservBw (tTlmComponentLink * pCompIface)
{
    UINT1               u1Priority = TLM_ZERO;

    pCompIface->availBw = pCompIface->maxResBw;

    for (u1Priority = TLM_ZERO; u1Priority < TLM_MAX_PRIORITY_LVL; u1Priority++)
    {
        pCompIface->aUnResBw[u1Priority].unResBw = pCompIface->maxResBw;
        pCompIface->aUnResBw[u1Priority].resBw = TLM_ZERO;
    }
	 /*Update Te-Link Info*/
       	   TlmUpdateTeLinkInfo(pCompIface, pCompIface->pTeLink,
                            TLM_COMPONENT_UPDATE);
    return;
}

/*****************************************************************************
 * Function Name      : TlmUtilValidateIpAddress                             *
 *                                                                           *
 * Description        : This Routine is used to validate IP Address          *
 *                                                                           *
 * Input(s)           : u4IpAddr - Ip Address                                *
 *                                                                           *
 * Output(s)          : None.                                                *
 *                                                                           *
 * Return Value(s)    : TRUE/FALSE                                           *
 *                                                                           *
 *****************************************************************************/
UINT1
TlmUtilValidateIpAddress (UINT4 u4IpAddr)
{
    if (u4IpAddr == TLM_ZERO)
    {
        TLM_TRC (TLM_FAILURE_TRC,
                 "TlmUtilValidateIpAddress :Invalid ip addres" "ZERO\r\n");
        return (TRUE);
    }
    if (u4IpAddr == LTD_B_CAST_ADDR)
    {
        TLM_TRC (TLM_FAILURE_TRC,
                 "TlmUtilValidateIpAddress :Invalid ip addres\r\n");
        return (FALSE);
    }
    if ((u4IpAddr & LOOP_BACK_ADDR_MASK) == LOOP_BACK_ADDRESSES)
    {
        TLM_TRC (TLM_FAILURE_TRC,
                 "TlmUtilValidateIpAddress :Invalid ip addres\r\n");
        return (FALSE);
    }
    if (!((IS_CLASS_A_ADDR (u4IpAddr)) ||
          (IS_CLASS_B_ADDR (u4IpAddr)) || (IS_CLASS_C_ADDR (u4IpAddr))))
    {
        TLM_TRC (TLM_FAILURE_TRC,
                 "TlmUtilValidateIpAddress :Invalid ip addres\r\n");
        return (FALSE);
    }
    return (TRUE);                /*Valid Ip Address */
}

/*****************************************************************************
 * Function Name      : TlmUtilCheckSupportedClassType                       *
 *                                                                           *
 * Description        : This Routine is used check whether the given Class   *
 *                      type is supported or not                             *
 *                                                                           *
 * Input(s)           : u1ClassType - Class Type.                            *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : TLM_FALSE/TLM_TRUE                                   *
 *                                                                           *
 *****************************************************************************/
UINT1
TlmUtilCheckSupportedClassType (UINT1 u1ClassType)
{
    UINT1               u1TeClass = TLM_ZERO;
    UINT1               u1RetVal = TLM_FAILURE;

    for (u1TeClass = TLM_ZERO; u1TeClass < TLM_MAX_TE_CLASS; u1TeClass++)
    {
        if (gTlmGlobalInfo.aTlmTeClassMap[u1TeClass].u1ClassType == u1ClassType)
        {
            u1RetVal = TLM_SUCCESS;
            break;
        }
    }
    return u1RetVal;
}

/*****************************************************************************
 * Function Name      : TlmUtilGetTeClsfromPrioClsType                   *
 *                                                                           *
 * Description        : This Routine is used to get te class info from       *
 *                      priority and te class type value                     *
 *                                                                           *
 * Input(s)           : u1Prio - Priority value(0-7).                        *
 *                      u1ClassType - Class Type(0-7).                       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ClassType Total Supported(u1TeClass)                 *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT4
TlmUtilGetTeClsfromPrioClsType (UINT1 u1Prio, UINT1 u1ClassType,
                                UINT1 *u1TeClass)
{

    UINT1               u1Count = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();
    for (u1Count = TLM_ZERO; u1Count < TLM_MAX_TE_CLASS; u1Count++)
    {
        if (TlmUtilValidateTeClass (u1Count) == TLM_FALSE)
        {
            continue;
        }
        if ((TLM_TE_CLASS_INFO (u1Count).u1PremptPrio ==
             u1Prio) &&
            (TLM_TE_CLASS_INFO (u1Count).u1ClassType == u1ClassType))
        {

            *u1TeClass = u1Count;
            return TLM_SUCCESS;
        }
    }
    TLM_TRC_FN_EXIT ();
    return TLM_FAILURE;

}

/******************************************************************************
 * Function Name      : TlmUtilValidateTeClass
 *
 * Description        : This function is used to Validate te class
 *
 * Input(s)           : u1TeClass -Te class given
 *
 * Return Value(s)    : TLM_TRUE / TLM_FALSE
 *
 ******************************************************************************/
PUBLIC INT4
TlmUtilValidateTeClass (UINT1 u1TeClass)
{
    UINT1               u1ClassType = TLM_ZERO;
    UINT1               u1PremPrio = TLM_ZERO;
    TLM_TRC_FN_ENTRY ();
    u1ClassType = TLM_TE_CLASS_INFO (u1TeClass).u1ClassType;
    u1PremPrio = TLM_TE_CLASS_INFO (u1TeClass).u1PremptPrio;
    if ((IS_VALID_CLASS_TYPE (u1ClassType)) && (IS_VALID_PRIORITY (u1PremPrio)))
    {
        return TLM_TRUE;
    }
    TLM_TRC_FN_EXIT ();
    return TLM_FALSE;
}

/*****************************************************************************
 * Function     : TlmUtilHashGetValue                                        *
 *                                                                           *
 * Description  : This procedure gets the hash value for the key             *
 *                                                                           *
 * Input        : u4HashSize    :  Max. no. of buckets                       *
 *                pKey           :  Pointer to information to be hashed      *
 *                u1KeyLen      :  Length of the information that is to      *
 *                                   be hashed.                              *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : Hash Bucket value                                          *
 *                                                                           *
 *****************************************************************************/
PUBLIC UINT4
TlmUtilHashGetValue (UINT4 u4HashSize, UINT1 *pKey, UINT1 u1KeyLen)
{

    UINT4               u4Num1 = TLM_ZERO;
    UINT4               u4Num2 = TLM_ZERO;
    UINT1              *pu1Ptr = NULL;

    for (pu1Ptr = pKey; u1KeyLen; pu1Ptr++, u1KeyLen--)
    {

        u4Num1 = (u4Num1 << TLM_FOUR) + (*pu1Ptr);
        u4Num2 = u4Num1 & 0xf0000000;
        if (u4Num2)
        {

            u4Num1 = u4Num1 ^ (u4Num2 >> 24);
            u4Num1 = u4Num1 ^ u4Num2;
            u4Num2 = u4Num1 & 0xf0000000;
        }
    }
    return (u4Num1 % u4HashSize);
}

/******************************************************************************
 * Function Name      : TlmUtilHashGetNameValue                               *
 *                                                                            *
 * Description        : This procedure gets the hash value for the key.       *
 *                                                                            *
 * Input(s)           : tlmName - Pointer to information to be hashed.        *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : Hash Bucket value                                     *
 ******************************************************************************/
PUBLIC UINT4
TlmUtilHashGetNameValue (UINT1 *pu1TlmName, UINT1 u1TlmNameLen)
{
    UINT4               u4Key = TLM_ZERO;
    UINT1               u1Index = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();
    for (u1Index = TLM_ZERO; u1Index < u1TlmNameLen; u1Index++)
    {
        u4Key += (UINT4) (pu1TlmName[u1Index] * (u1Index + TLM_ONE));
    }
    u4Key =
        TlmUtilHashGetValue (IF_HASH_TABLE_SIZE, (UINT1 *) (&(u4Key)),
                             sizeof (UINT4));
    TLM_TRC_FN_EXIT ();
    return u4Key;
}

/*****************************************************************************
 * Function Name      : TlmUtilGetIfIndexFromTeLinkName                      *
 *                                                                           *
 * Description        : Routine used to get the TeLink interface index       *
 *                      from the given TE link name                          *
 *                                                                           *
 * Input(s)           : TeLinkName - TeLink interface index                  *
 *                                                                           *
 * Output(s)          : pi4IfIndex  - TeLink interface IfIndex                *
 *                                                                           *
 * Return Value(s)    : TLM_FAILURE/TLM_SUCCESS                              *
 *                                                                           *
 *****************************************************************************/
INT4
TlmUtilGetIfIndexFromTeLinkName (UINT1 *pu1TeLinkName, UINT1 u1TeLinkNameLen,
                                 INT4 *pi4IfIndex)
{
    tTlmTeLink         *pTeLink = NULL;
    tTMO_HASH_TABLE    *pTeLinkNameHashTable = NULL;
    tTMO_HASH_NODE     *pTlmTeNameNode = NULL;

    pTeLinkNameHashTable = gTlmGlobalInfo.pTeLinkNameIfHashTable;

    if (pTeLinkNameHashTable == NULL)
    {
        TLM_TRC (TLM_FAILURE_TRC,
                 "TlmUtilGetIfIndexFromTeLinkName: HashTable is empty\n");
        return TLM_FAILURE;
    }

    TMO_HASH_Scan_Bucket (pTeLinkNameHashTable,
                          IF_HASH_NAME_FN (pu1TeLinkName, u1TeLinkNameLen),
                          pTlmTeNameNode, tTMO_HASH_NODE *)
    {
        pTeLink = GET_TE_TELINK_NAME_NODE (pTlmTeNameNode);

        if (TLM_STRLEN (pTeLink->au1TeLinkName) != TLM_STRLEN (pu1TeLinkName))
        {
            continue;
        }

        if (TLM_STRCMP (pTeLink->au1TeLinkName, pu1TeLinkName) == TLM_ZERO)
        {
            *pi4IfIndex = pTeLink->i4IfIndex;
            return TLM_SUCCESS;
        }
    }

    TLM_TRC (TLM_FAILURE_TRC,
             "TlmUtilGetIfIndexFromTeLinkName: Could not find TE Link\n");
    return TLM_FAILURE;
}

/*****************************************************************************
 * Function Name      : TlmUtilCheckTeLinkNameUnique                         *
 *                                                                           *
 * Description        : This Routine is used check whether Link name         * 
 *                      is unique (or) already present.                      *
 *                                                                           *
 * Input(s)           : pu1TeLinkName - Name of the Te-Link.                 *
 *                      i4TeIfIndex   - TE If Index                          *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : TLM_FALSE/TLM_TRUE                                   *
 *                                                                           *
 *****************************************************************************/
PUBLIC UINT1
TlmUtilCheckTeLinkNameUnique (UINT1 *pu1TeLinkName, INT4 i4TeIfIndex)
{
    tTMO_HASH_TABLE    *pTeLinkNameHashTable =
        gTlmGlobalInfo.pTeLinkNameIfHashTable;
    tTMO_HASH_NODE     *pTlmTeNameNode = NULL;
    tTlmTeLink         *pTeLink = NULL;
    UINT1               u1TeLinkNameLen = TLM_ZERO;
    UINT4               u4HashIndex = TLM_ZERO;

    if (pTeLinkNameHashTable == NULL)
    {
        return TLM_FALSE;
    }

    u1TeLinkNameLen = (UINT1) TLM_STRLEN (pu1TeLinkName);

    u4HashIndex = IF_HASH_NAME_FN (pu1TeLinkName, u1TeLinkNameLen);

    TMO_HASH_Scan_Bucket (pTeLinkNameHashTable, u4HashIndex,
                          pTlmTeNameNode, tTMO_HASH_NODE *)
    {
        pTeLink = GET_TE_TELINK_NAME_NODE (pTlmTeNameNode);

        if (TLM_STRLEN (pTeLink->au1TeLinkName) != TLM_STRLEN (pu1TeLinkName))
        {
            continue;
        }

        if ((TLM_STRCMP (pTeLink->au1TeLinkName, pu1TeLinkName) == TLM_ZERO) &&
            (pTeLink->i4IfIndex != i4TeIfIndex))
        {
            return TLM_FALSE;
        }
    }

    return TLM_TRUE;
}

/******************************************************************************
 * Function Name      : TlmUpdateInfoToRegProtocol 
 * 
 * Description        : This function is used to update the Tlm updatation
 *                      information to all the registered protocols
 *
 * Input(s)           : u4ModId - Module id of Application to be registered
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 ******************************************************************************/
VOID
TlmUpdateInfoToRegProtocol (UINT4 u4ModId)
{

    tTlmTeLink         *pTeIface = NULL;
    tTMO_HASH_TABLE    *pTeIfHashTable = gTlmGlobalInfo.pTeIfHashTable;
    tTMO_SLL_NODE      *pLstNode = NULL;

    TLM_TRC_FN_ENTRY ();

    if (pTeIfHashTable == NULL)
    {
        return;
    }

    if (u4ModId != TLM_OSPF_TE_APP_ID)
    {
        return;
    }

    TMO_SLL_Scan (&(gTlmGlobalInfo.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pTeIface = GET_TE_IF_PTR_FROM_SORT_LST (pLstNode);

        TlmUtilSendNotifMsgInfo (pTeIface, TE_LINK_CREATED);
    }

    TLM_TRC_FN_EXIT ();

    return;
}

/******************************************************************************
 * Function Name      : TlmUtilOctetStringToFloat
 *
 * Description        : This function converts OctetString to Float
 *
 * Input(s)           : pOctetString - Octet String
 *
 * Output(s)          : None
 *
 * Return Value(s)    : f4Float - Float Value
 ******************************************************************************/
FLT4
TlmUtilOctetStringToFloat (tSNMP_OCTET_STRING_TYPE * pOctetString)
{
    FLT4                f4Float = TLM_ZERO;

    MEMCPY ((UINT1 *) &f4Float, pOctetString->pu1_OctetList,
            pOctetString->i4_Length);

    f4Float = OSIX_HTONF (f4Float);

    return f4Float;
}

/******************************************************************************
 * Function Name      : TlmUtilCalculateBCvalue
 *
 * Description        : This function calculates Bandwidth constraint value and
 *                      available, unreserved bandwidth of each TE class
 *
 * Input(s)           : pCompLink      - Pointer to component link structure
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Tlm_SUCCESS / Tlm_FAILURE
 *****************************************************************************/
VOID
TlmUtilCalculateBCvalue (tTlmComponentLink * pCompLink)
{
    tBandWidth          ClassTypeBw = TLM_ZERO;
    INT1                i1ClassType = TLM_ZERO;
    UINT1               u1TeClass = TLM_ZERO;
    UINT1               u1BCValue = TLM_ZERO;

    for (u1BCValue = TLM_ZERO; u1BCValue < TLM_MAX_TE_CLASS_TYPE; u1BCValue++)
    {
        pCompLink->aBC[u1BCValue] = TLM_ZERO;
    }

    /* calculate Bandwith consraint values
     * BC0 -> CT0 bw+ CT1 bw+....+ CT7 bw
     * BC1 -> CT1 bw+....+ CT7 bw
     * ....
     * BC7 -> CT7 bw
     * */
    for (u1BCValue = TLM_ZERO; u1BCValue < TLM_MAX_TE_CLASS_TYPE; u1BCValue++)
    {
        for (i1ClassType = TLM_MAX_TE_CLASS_TYPE - 1; i1ClassType >= u1BCValue;
             i1ClassType--)
        {
            ClassTypeBw =
                ((((tBandWidth) gTlmGlobalInfo.aTlmClassType[i1ClassType].
                   u4BwPercentage) * pCompLink->maxResBw) / 100);
            pCompLink->aBC[u1BCValue] += ClassTypeBw;
        }
    }
    for (u1TeClass = TLM_ZERO; u1TeClass < TLM_MAX_TE_CLASS; u1TeClass++)
    {
        i1ClassType =
            (INT1) gTlmGlobalInfo.aTlmTeClassMap[u1TeClass].u1ClassType;
        pCompLink->aUnResBw[u1TeClass].unResBw = pCompLink->aBC[i1ClassType];
    }
    return;
}

/******************************************************************************
 * Function Name      : TlmUtilUpdateCompLinkInfo
 *
 * Description        : This function updates the Component Link Information 
 *
 * Input(s)           : pCompLink - CompLink to be updated
 *                      pInTlmTeLinkInfo - updating informations 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : NONE
 ******************************************************************************/
VOID
TlmUtilUpdateCompLinkInfo (tTlmComponentLink * pCompIface,
                           tInTlmTeLink * pInTlmTeLinkInfo)
{

    tTlmComponentIfDescr *pCompIfaceDesc = NULL;
    INT4                i4Count = TLM_ZERO;

    pCompIface->availBw = pInTlmTeLinkInfo->reqResBw;
    pCompIface->maxResBw = pInTlmTeLinkInfo->reqResBw;

    pCompIfaceDesc = (tTlmComponentIfDescr *)
        TMO_SLL_First (&(pCompIface->ifDescrList));

    if (pCompIfaceDesc == NULL)
    {
        return;
    }
    pCompIfaceDesc->i4StorageType = TLM_STORAGE_VOLATILE;
    pCompIfaceDesc->i4SwitchingCap = pInTlmTeLinkInfo->i4SwitchingCap;
    pCompIfaceDesc->i4EncodingType = pInTlmTeLinkInfo->i4EncodingType;
    pCompIfaceDesc->minLSPBw = TLM_ZERO;
    pCompIfaceDesc->i4StorageType = TLM_STORAGE_VOLATILE;

    /* Set the unreserved bandwidth and max LSP bandwidth 
     * at all priorities to the unresv bandwidth
     * recevied from pInTlmTeLinkInfo */

    for (i4Count = TLM_MIN_PRIORITY_LVL; i4Count < TLM_MAX_PRIORITY_LVL;
         i4Count++)
    {
        pCompIface->aUnResBw[i4Count].unResBw = pInTlmTeLinkInfo->reqResBw;
        pCompIface->aUnResBw[i4Count].i4RowStatus = ACTIVE;
        pCompIface->aUnResBw[i4Count].i4StorageType = TLM_STORAGE_VOLATILE;
        pCompIfaceDesc->aMaxLSPBwPrio[i4Count] = pInTlmTeLinkInfo->reqResBw;
    }

    return;
}
