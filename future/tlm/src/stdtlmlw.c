/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdtlmlw.c,v 1.10 2016/01/28 09:40:35 siva Exp $
*
* Description: Protocol Low Level Routines for Standard TLM mib
*********************************************************************/

#include "tlminc.h"
#include "tlmcli.h"
#include "msr.h"

/* LOW LEVEL Routines for Table : TeLinkTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceTeLinkTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceTeLinkTable (INT4 i4IfIndex)
{
    if ((i4IfIndex < CFA_MIN_TELINK_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_TELINK_IF_INDEX))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexTeLinkTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexTeLinkTable (INT4 *pi4IfIndex)
{
    tTlmTeLink         *pTeIface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    if ((pLstNode = TMO_SLL_First (&(gTlmGlobalInfo.sortTeIfLst))) != NULL)
    {
        pTeIface = GET_TE_IF_PTR_FROM_SORT_LST (pLstNode);

        *pi4IfIndex = pTeIface->i4IfIndex;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexTeLinkTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexTeLinkTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tTlmTeLink         *pTeIface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    TMO_SLL_Scan (&(gTlmGlobalInfo.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pTeIface = GET_TE_IF_PTR_FROM_SORT_LST (pLstNode);

        if (pTeIface->i4IfIndex > i4IfIndex)
        {
            *pi4NextIfIndex = pTeIface->i4IfIndex;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetTeLinkAddressType
 Input       :  The Indices
                IfIndex

                The Object 
                retValTeLinkAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkAddressType (INT4 i4IfIndex, INT4 *pi4RetValTeLinkAddressType)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkTable (i4IfIndex, &TlmTeLink);

    *pi4RetValTeLinkAddressType = TlmTeLink.i4AddressType;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkLocalIpAddr
 Input       :  The Indices
                IfIndex

                The Object 
                retValTeLinkLocalIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkLocalIpAddr (INT4 i4IfIndex,
                         tSNMP_OCTET_STRING_TYPE * pRetValTeLinkLocalIpAddr)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkTable (i4IfIndex, &TlmTeLink);

    TLM_INTEGER_TO_OCTETSTRING (TlmTeLink.LocalIpAddr.u4_addr[0],
                                pRetValTeLinkLocalIpAddr);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkRemoteIpAddr
 Input       :  The Indices
                IfIndex

                The Object 
                retValTeLinkRemoteIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkRemoteIpAddr (INT4 i4IfIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValTeLinkRemoteIpAddr)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkTable (i4IfIndex, &TlmTeLink);

    TLM_INTEGER_TO_OCTETSTRING (TlmTeLink.RemoteIpAddr.u4_addr[0],
                                pRetValTeLinkRemoteIpAddr);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkMetric
 Input       :  The Indices
                IfIndex

                The Object 
                retValTeLinkMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkMetric (INT4 i4IfIndex, UINT4 *pu4RetValTeLinkMetric)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkTable (i4IfIndex, &TlmTeLink);

    *pu4RetValTeLinkMetric = TlmTeLink.u4TeMetric;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkMaximumReservableBandwidth
 Input       :  The Indices
                IfIndex

                The Object 
                retValTeLinkMaximumReservableBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkMaximumReservableBandwidth (INT4 i4IfIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValTeLinkMaximumReservableBandwidth)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkTable (i4IfIndex, &TlmTeLink);

    FLOAT_TO_OCTETSTRING (TlmTeLink.maxResBw,
                          pRetValTeLinkMaximumReservableBandwidth);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkProtectionType
 Input       :  The Indices
                IfIndex

                The Object 
                retValTeLinkProtectionType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkProtectionType (INT4 i4IfIndex, INT4 *pi4RetValTeLinkProtectionType)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkTable (i4IfIndex, &TlmTeLink);

    *pi4RetValTeLinkProtectionType = TlmTeLink.i4ProtectionType;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkWorkingPriority
 Input       :  The Indices
                IfIndex

                The Object 
                retValTeLinkWorkingPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkWorkingPriority (INT4 i4IfIndex,
                             UINT4 *pu4RetValTeLinkWorkingPriority)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkTable (i4IfIndex, &TlmTeLink);

    *pu4RetValTeLinkWorkingPriority = TlmTeLink.u4WorkingPriority;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkResourceClass
 Input       :  The Indices
                IfIndex

                The Object 
                retValTeLinkResourceClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkResourceClass (INT4 i4IfIndex, UINT4 *pu4RetValTeLinkResourceClass)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkTable (i4IfIndex, &TlmTeLink);

    *pu4RetValTeLinkResourceClass = TlmTeLink.u4RsrcClassColor;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkIncomingIfId
 Input       :  The Indices
                IfIndex

                The Object 
                retValTeLinkIncomingIfId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkIncomingIfId (INT4 i4IfIndex, INT4 *pi4RetValTeLinkIncomingIfId)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkTable (i4IfIndex, &TlmTeLink);

    *pi4RetValTeLinkIncomingIfId = (INT4) TlmTeLink.u4RemoteIdentifier;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkOutgoingIfId
 Input       :  The Indices
                IfIndex

                The Object 
                retValTeLinkOutgoingIfId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkOutgoingIfId (INT4 i4IfIndex, INT4 *pi4RetValTeLinkOutgoingIfId)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkTable (i4IfIndex, &TlmTeLink);

    *pi4RetValTeLinkOutgoingIfId = (INT4) TlmTeLink.u4LocalIdentifier;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValTeLinkRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkRowStatus (INT4 i4IfIndex, INT4 *pi4RetValTeLinkRowStatus)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkTable (i4IfIndex, &TlmTeLink);

    *pi4RetValTeLinkRowStatus = TlmTeLink.i4RowStatus;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkStorageType
 Input       :  The Indices
                IfIndex

                The Object 
                retValTeLinkStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkStorageType (INT4 i4IfIndex, INT4 *pi4RetValTeLinkStorageType)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkTable (i4IfIndex, &TlmTeLink);

    *pi4RetValTeLinkStorageType = TlmTeLink.i4StorageType;

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetTeLinkAddressType
 Input       :  The Indices
                IfIndex

                The Object 
                setValTeLinkAddressType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkAddressType (INT4 i4IfIndex, INT4 i4SetValTeLinkAddressType)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.i4AddressType = i4SetValTeLinkAddressType;

    i1RetVal = TlmSetAllTeLinkTable (i4IfIndex, &TlmTeLink,
                                     TLM_TE_LINK_ADDR_TYPE);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetTeLinkLocalIpAddr
 Input       :  The Indices
                IfIndex

                The Object 
                setValTeLinkLocalIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkLocalIpAddr (INT4 i4IfIndex,
                         tSNMP_OCTET_STRING_TYPE * pSetValTeLinkLocalIpAddr)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TLM_OCTETSTRING_TO_INTEGER (pSetValTeLinkLocalIpAddr,
                                TlmTeLink.LocalIpAddr.u4_addr[0]);

    i1RetVal = TlmSetAllTeLinkTable (i4IfIndex, &TlmTeLink,
                                     TLM_TE_LINK_LOCAL_IPADDR);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetTeLinkRemoteIpAddr
 Input       :  The Indices
                IfIndex

                The Object 
                setValTeLinkRemoteIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkRemoteIpAddr (INT4 i4IfIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValTeLinkRemoteIpAddr)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TLM_OCTETSTRING_TO_INTEGER (pSetValTeLinkRemoteIpAddr,
                                TlmTeLink.RemoteIpAddr.u4_addr[0]);

    i1RetVal = TlmSetAllTeLinkTable (i4IfIndex, &TlmTeLink,
                                     TLM_TE_LINK_REMOTE_IPADDR);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetTeLinkMetric
 Input       :  The Indices
                IfIndex

                The Object 
                setValTeLinkMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkMetric (INT4 i4IfIndex, UINT4 u4SetValTeLinkMetric)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.u4TeMetric = u4SetValTeLinkMetric;

    i1RetVal = TlmSetAllTeLinkTable (i4IfIndex, &TlmTeLink,
                                     TLM_TE_LINK_METRIC_OBJ);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetTeLinkProtectionType
 Input       :  The Indices
                IfIndex

                The Object 
                setValTeLinkProtectionType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkProtectionType (INT4 i4IfIndex, INT4 i4SetValTeLinkProtectionType)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.i4ProtectionType = i4SetValTeLinkProtectionType;

    i1RetVal = TlmSetAllTeLinkTable (i4IfIndex, &TlmTeLink,
                                     TLM_TE_LINK_PROTECT_TYPE);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetTeLinkWorkingPriority
 Input       :  The Indices
                IfIndex

                The Object 
                setValTeLinkWorkingPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkWorkingPriority (INT4 i4IfIndex,
                             UINT4 u4SetValTeLinkWorkingPriority)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.u4WorkingPriority = u4SetValTeLinkWorkingPriority;

    i1RetVal = TlmSetAllTeLinkTable (i4IfIndex, &TlmTeLink,
                                     TLM_TE_LINK_WORK_PRIORITY);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetTeLinkResourceClass
 Input       :  The Indices
                IfIndex

                The Object 
                setValTeLinkResourceClass
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkResourceClass (INT4 i4IfIndex, UINT4 u4SetValTeLinkResourceClass)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.u4RsrcClassColor = u4SetValTeLinkResourceClass;

    i1RetVal = TlmSetAllTeLinkTable (i4IfIndex, &TlmTeLink,
                                     TLM_TE_LINK_RESOURCE_CLASS);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetTeLinkIncomingIfId
 Input       :  The Indices
                IfIndex

                The Object 
                setValTeLinkIncomingIfId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkIncomingIfId (INT4 i4IfIndex, INT4 i4SetValTeLinkIncomingIfId)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.u4RemoteIdentifier = (UINT4) i4SetValTeLinkIncomingIfId;

    i1RetVal = TlmSetAllTeLinkTable (i4IfIndex, &TlmTeLink,
                                     TLM_TE_LINK_INCOMING_IFID);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetTeLinkOutgoingIfId
 Input       :  The Indices
                IfIndex

                The Object 
                setValTeLinkOutgoingIfId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkOutgoingIfId (INT4 i4IfIndex, INT4 i4SetValTeLinkOutgoingIfId)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.u4LocalIdentifier = (UINT4) i4SetValTeLinkOutgoingIfId;

    i1RetVal = TlmSetAllTeLinkTable (i4IfIndex, &TlmTeLink,
                                     TLM_TE_LINK_OUTGOING_IFID);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetTeLinkRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                setValTeLinkRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkRowStatus (INT4 i4IfIndex, INT4 i4SetValTeLinkRowStatus)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.i4RowStatus = i4SetValTeLinkRowStatus;

    i1RetVal = TlmSetAllTeLinkTable (i4IfIndex, &TlmTeLink,
                                     TLM_TE_LINK_ROW_STATUS);
	
	if (CfaSetIfMainAdminStatus (i4IfIndex, i4SetValTeLinkRowStatus)
        == CFA_FAILURE)
     {
         return i1RetVal;
     }


    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetTeLinkStorageType
 Input       :  The Indices
                IfIndex

                The Object 
                setValTeLinkStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkStorageType (INT4 i4IfIndex, INT4 i4SetValTeLinkStorageType)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.i4StorageType = i4SetValTeLinkStorageType;

    i1RetVal = TlmSetAllTeLinkTable (i4IfIndex, &TlmTeLink,
                                     TLM_TE_LINK_STORAGE_TYPE);

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2TeLinkAddressType
 Input       :  The Indices
                pu4ErrorCode
                IfIndex
                testValTeLinkAddressType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkAddressType (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                            INT4 i4TestValTeLinkAddressType)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.i4AddressType = i4TestValTeLinkAddressType;

    i1RetVal = TlmTestAllTeLinkTable (pu4ErrorCode, i4IfIndex, &TlmTeLink,
                                      TLM_TE_LINK_ADDR_TYPE);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkLocalIpAddr
 Input       :  The Indices
                IfIndex

                The Object 
                testValTeLinkLocalIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkLocalIpAddr (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                            tSNMP_OCTET_STRING_TYPE * pTestValTeLinkLocalIpAddr)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.u1LocalIpLen = (UINT1) pTestValTeLinkLocalIpAddr->i4_Length;

    TLM_OCTETSTRING_TO_INTEGER (pTestValTeLinkLocalIpAddr,
                                TlmTeLink.LocalIpAddr.u4_addr[0]);
    i1RetVal = TlmTestAllTeLinkTable (pu4ErrorCode, i4IfIndex, &TlmTeLink,
                                      TLM_TE_LINK_LOCAL_IPADDR);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkRemoteIpAddr
 Input       :  The Indices
                IfIndex

                The Object 
                testValTeLinkRemoteIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkRemoteIpAddr (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValTeLinkRemoteIpAddr)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.u1RemoteIpLen = (UINT1) pTestValTeLinkRemoteIpAddr->i4_Length;

    TLM_OCTETSTRING_TO_INTEGER (pTestValTeLinkRemoteIpAddr,
                                TlmTeLink.RemoteIpAddr.u4_addr[0]);
    i1RetVal = TlmTestAllTeLinkTable (pu4ErrorCode, i4IfIndex, &TlmTeLink,
                                      TLM_TE_LINK_REMOTE_IPADDR);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkMetric
 Input       :  The Indices
                IfIndex

                The Object 
                testValTeLinkMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkMetric (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                       UINT4 u4TestValTeLinkMetric)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.u4TeMetric = u4TestValTeLinkMetric;

    i1RetVal = TlmTestAllTeLinkTable (pu4ErrorCode, i4IfIndex, &TlmTeLink,
                                      TLM_TE_LINK_METRIC_OBJ);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkProtectionType
 Input       :  The Indices
                IfIndex

                The Object 
                testValTeLinkProtectionType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkProtectionType (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               INT4 i4TestValTeLinkProtectionType)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.i4ProtectionType = i4TestValTeLinkProtectionType;

    i1RetVal = TlmTestAllTeLinkTable (pu4ErrorCode, i4IfIndex, &TlmTeLink,
                                      TLM_TE_LINK_PROTECT_TYPE);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkWorkingPriority
 Input       :  The Indices
                IfIndex

                The Object 
                testValTeLinkWorkingPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkWorkingPriority (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                UINT4 u4TestValTeLinkWorkingPriority)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.u4WorkingPriority = u4TestValTeLinkWorkingPriority;

    i1RetVal = TlmTestAllTeLinkTable (pu4ErrorCode, i4IfIndex, &TlmTeLink,
                                      TLM_TE_LINK_WORK_PRIORITY);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkResourceClass
 Input       :  The Indices
                IfIndex

                The Object 
                testValTeLinkResourceClass
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkResourceClass (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                              UINT4 u4TestValTeLinkResourceClass)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.u4RsrcClassColor = u4TestValTeLinkResourceClass;

    i1RetVal = TlmTestAllTeLinkTable (pu4ErrorCode, i4IfIndex, &TlmTeLink,
                                      TLM_TE_LINK_WORK_PRIORITY);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkIncomingIfId
 Input       :  The Indices
                IfIndex

                The Object 
                testValTeLinkIncomingIfId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkIncomingIfId (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                             INT4 i4TestValTeLinkIncomingIfId)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.u4RemoteIdentifier = (UINT4) i4TestValTeLinkIncomingIfId;

    i1RetVal = TlmTestAllTeLinkTable (pu4ErrorCode, i4IfIndex, &TlmTeLink,
                                      TLM_TE_LINK_INCOMING_IFID);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkOutgoingIfId
 Input       :  The Indices
                IfIndex

                The Object 
                testValTeLinkOutgoingIfId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkOutgoingIfId (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                             INT4 i4TestValTeLinkOutgoingIfId)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.u4LocalIdentifier = (UINT4) i4TestValTeLinkOutgoingIfId;

    i1RetVal = TlmTestAllTeLinkTable (pu4ErrorCode, i4IfIndex, &TlmTeLink,
                                      TLM_TE_LINK_OUTGOING_IFID);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                testValTeLinkRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                          INT4 i4TestValTeLinkRowStatus)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));
    if (TLM_MODULE_STATUS == TLM_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }

    TlmTeLink.i4RowStatus = i4TestValTeLinkRowStatus;

    i1RetVal = TlmTestAllTeLinkTable (pu4ErrorCode, i4IfIndex, &TlmTeLink,
                                      TLM_TE_LINK_ROW_STATUS);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkStorageType
 Input       :  The Indices
                IfIndex

                The Object 
                testValTeLinkStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkStorageType (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                            INT4 i4TestValTeLinkStorageType)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.i4StorageType = i4TestValTeLinkStorageType;

    i1RetVal = TlmTestAllTeLinkTable (pu4ErrorCode, i4IfIndex, &TlmTeLink,
                                      TLM_TE_LINK_STORAGE_TYPE);

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2TeLinkTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2TeLinkTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : TeLinkDescriptorTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceTeLinkDescriptorTable
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceTeLinkDescriptorTable (INT4 i4IfIndex,
                                               UINT4 u4TeLinkDescriptorId)
{
    UNUSED_PARAM (u4TeLinkDescriptorId);

    return (nmhValidateIndexInstanceTeLinkTable (i4IfIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexTeLinkDescriptorTable
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexTeLinkDescriptorTable (INT4 *pi4IfIndex,
                                       UINT4 *pu4TeLinkDescriptorId)
{
    tTlmTeLink         *pTeIface = NULL;
    tTlmTeIfDescr      *pTeDescr = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    if ((pLstNode = TMO_SLL_First (&(gTlmGlobalInfo.sortTeIfLst))) != NULL)
    {
        pTeIface = GET_TE_IF_PTR_FROM_SORT_LST (pLstNode);

        if ((pLstNode = TMO_SLL_First (&(pTeIface->ifDescrList))) != NULL)
        {
            pTeDescr = GET_TE_DESCR_PTR_FROM_SORT_LST (pLstNode);

            *pi4IfIndex = pTeIface->i4IfIndex;
            *pu4TeLinkDescriptorId = pTeDescr->u4DescrId;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexTeLinkDescriptorTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                TeLinkDescriptorId
                nextTeLinkDescriptorId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexTeLinkDescriptorTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                      UINT4 u4TeLinkDescriptorId,
                                      UINT4 *pu4NextTeLinkDescriptorId)
{
    tTlmTeLink         *pTeIface = NULL;
    tTlmTeIfDescr      *pTeDescr = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pIfNode = NULL;

    TMO_SLL_Scan (&(gTlmGlobalInfo.sortTeIfLst), pIfNode, tTMO_SLL_NODE *)
    {
        pTeIface = GET_TE_IF_PTR_FROM_SORT_LST (pIfNode);
        if (pTeIface->i4IfIndex == i4IfIndex)
        {
            TMO_SLL_Scan (&(pTeIface->ifDescrList), pLstNode, tTMO_SLL_NODE *)
            {
                pTeDescr = GET_TE_DESCR_PTR_FROM_SORT_LST (pLstNode);

                if (pTeDescr->u4DescrId > u4TeLinkDescriptorId)
                {
                    *pi4NextIfIndex = pTeIface->i4IfIndex;
                    *pu4NextTeLinkDescriptorId = pTeDescr->u4DescrId;
                    return SNMP_SUCCESS;
                }
            }
        }
        else if (pTeIface->i4IfIndex > i4IfIndex)
        {
            if ((pLstNode = TMO_SLL_First (&(pTeIface->ifDescrList))) != NULL)
            {
                pTeDescr = GET_TE_DESCR_PTR_FROM_SORT_LST (pLstNode);
                *pi4NextIfIndex = pTeIface->i4IfIndex;
                *pu4NextTeLinkDescriptorId = pTeDescr->u4DescrId;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetTeLinkDescrSwitchingCapability
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                retValTeLinkDescrSwitchingCapability
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkDescrSwitchingCapability (INT4 i4IfIndex,
                                      UINT4 u4TeLinkDescriptorId,
                                      INT4
                                      *pi4RetValTeLinkDescrSwitchingCapability)
{
    tTlmTeIfDescr       TlmTeIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeIfDescr, 0, sizeof (TlmTeIfDescr));

    i1RetVal = TlmGetAllTeLinkDescriptorTable (i4IfIndex, u4TeLinkDescriptorId,
                                               &TlmTeIfDescr);

    *pi4RetValTeLinkDescrSwitchingCapability = TlmTeIfDescr.i4SwitchingCap;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkDescrEncodingType
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                retValTeLinkDescrEncodingType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkDescrEncodingType (INT4 i4IfIndex, UINT4 u4TeLinkDescriptorId,
                               INT4 *pi4RetValTeLinkDescrEncodingType)
{
    tTlmTeIfDescr       TlmTeIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeIfDescr, 0, sizeof (TlmTeIfDescr));

    i1RetVal = TlmGetAllTeLinkDescriptorTable (i4IfIndex,
                                               u4TeLinkDescriptorId,
                                               &TlmTeIfDescr);

    *pi4RetValTeLinkDescrEncodingType = TlmTeIfDescr.i4EncodingType;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkDescrMinLspBandwidth
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                retValTeLinkDescrMinLspBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkDescrMinLspBandwidth (INT4 i4IfIndex, UINT4 u4TeLinkDescriptorId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValTeLinkDescrMinLspBandwidth)
{
    tTlmTeIfDescr       TlmTeIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeIfDescr, 0, sizeof (TlmTeIfDescr));

    i1RetVal = TlmGetAllTeLinkDescriptorTable (i4IfIndex, u4TeLinkDescriptorId,
                                               &TlmTeIfDescr);

    FLOAT_TO_OCTETSTRING (TlmTeIfDescr.minLSPBw,
                          pRetValTeLinkDescrMinLspBandwidth);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkDescrMaxLspBandwidthPrio0
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                retValTeLinkDescrMaxLspBandwidthPrio0
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkDescrMaxLspBandwidthPrio0 (INT4 i4IfIndex,
                                       UINT4 u4TeLinkDescriptorId,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pRetValTeLinkDescrMaxLspBandwidthPrio0)
{
    tTlmTeIfDescr       TlmTeIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeIfDescr, 0, sizeof (TlmTeIfDescr));

    i1RetVal = TlmGetAllTeLinkDescriptorTable (i4IfIndex, u4TeLinkDescriptorId,
                                               &TlmTeIfDescr);

    FLOAT_TO_OCTETSTRING (TlmTeIfDescr.aMaxLSPBwPrio[PRIORITY0],
                          pRetValTeLinkDescrMaxLspBandwidthPrio0);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkDescrMaxLspBandwidthPrio1
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                retValTeLinkDescrMaxLspBandwidthPrio1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkDescrMaxLspBandwidthPrio1 (INT4 i4IfIndex,
                                       UINT4 u4TeLinkDescriptorId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValTeLinkDescrMaxLspBandwidthPrio1)
{
    tTlmTeIfDescr       TlmTeIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeIfDescr, 0, sizeof (TlmTeIfDescr));

    i1RetVal = TlmGetAllTeLinkDescriptorTable (i4IfIndex, u4TeLinkDescriptorId,
                                               &TlmTeIfDescr);

    FLOAT_TO_OCTETSTRING (TlmTeIfDescr.aMaxLSPBwPrio[PRIORITY1],
                          pRetValTeLinkDescrMaxLspBandwidthPrio1);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetTeLinkDescrMaxLspBandwidthPrio2
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                retValTeLinkDescrMaxLspBandwidthPrio2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkDescrMaxLspBandwidthPrio2 (INT4 i4IfIndex,
                                       UINT4 u4TeLinkDescriptorId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValTeLinkDescrMaxLspBandwidthPrio2)
{
    tTlmTeIfDescr       TlmTeIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeIfDescr, 0, sizeof (TlmTeIfDescr));

    i1RetVal = TlmGetAllTeLinkDescriptorTable (i4IfIndex, u4TeLinkDescriptorId,
                                               &TlmTeIfDescr);
    FLOAT_TO_OCTETSTRING (TlmTeIfDescr.aMaxLSPBwPrio[PRIORITY2],
                          pRetValTeLinkDescrMaxLspBandwidthPrio2);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkDescrMaxLspBandwidthPrio3
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                retValTeLinkDescrMaxLspBandwidthPrio3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkDescrMaxLspBandwidthPrio3 (INT4 i4IfIndex,
                                       UINT4 u4TeLinkDescriptorId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValTeLinkDescrMaxLspBandwidthPrio3)
{
    tTlmTeIfDescr       TlmTeIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeIfDescr, 0, sizeof (TlmTeIfDescr));

    i1RetVal = TlmGetAllTeLinkDescriptorTable (i4IfIndex, u4TeLinkDescriptorId,
                                               &TlmTeIfDescr);

    FLOAT_TO_OCTETSTRING (TlmTeIfDescr.aMaxLSPBwPrio[PRIORITY3],
                          pRetValTeLinkDescrMaxLspBandwidthPrio3);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkDescrMaxLspBandwidthPrio4
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                retValTeLinkDescrMaxLspBandwidthPrio4
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkDescrMaxLspBandwidthPrio4 (INT4 i4IfIndex,
                                       UINT4 u4TeLinkDescriptorId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValTeLinkDescrMaxLspBandwidthPrio4)
{
    tTlmTeIfDescr       TlmTeIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeIfDescr, 0, sizeof (TlmTeIfDescr));

    i1RetVal = TlmGetAllTeLinkDescriptorTable (i4IfIndex, u4TeLinkDescriptorId,
                                               &TlmTeIfDescr);

    FLOAT_TO_OCTETSTRING (TlmTeIfDescr.aMaxLSPBwPrio[PRIORITY4],
                          pRetValTeLinkDescrMaxLspBandwidthPrio4);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkDescrMaxLspBandwidthPrio5
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                retValTeLinkDescrMaxLspBandwidthPrio5
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkDescrMaxLspBandwidthPrio5 (INT4 i4IfIndex,
                                       UINT4 u4TeLinkDescriptorId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValTeLinkDescrMaxLspBandwidthPrio5)
{
    tTlmTeIfDescr       TlmTeIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeIfDescr, 0, sizeof (TlmTeIfDescr));

    i1RetVal = TlmGetAllTeLinkDescriptorTable (i4IfIndex, u4TeLinkDescriptorId,
                                               &TlmTeIfDescr);

    FLOAT_TO_OCTETSTRING (TlmTeIfDescr.aMaxLSPBwPrio[PRIORITY5],
                          pRetValTeLinkDescrMaxLspBandwidthPrio5);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkDescrMaxLspBandwidthPrio6
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                retValTeLinkDescrMaxLspBandwidthPrio6
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkDescrMaxLspBandwidthPrio6 (INT4 i4IfIndex,
                                       UINT4 u4TeLinkDescriptorId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValTeLinkDescrMaxLspBandwidthPrio6)
{
    tTlmTeIfDescr       TlmTeIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeIfDescr, 0, sizeof (TlmTeIfDescr));

    i1RetVal = TlmGetAllTeLinkDescriptorTable (i4IfIndex, u4TeLinkDescriptorId,
                                               &TlmTeIfDescr);
    FLOAT_TO_OCTETSTRING (TlmTeIfDescr.aMaxLSPBwPrio[PRIORITY6],
                          pRetValTeLinkDescrMaxLspBandwidthPrio6);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkDescrMaxLspBandwidthPrio7
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                retValTeLinkDescrMaxLspBandwidthPrio7
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkDescrMaxLspBandwidthPrio7 (INT4 i4IfIndex,
                                       UINT4 u4TeLinkDescriptorId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValTeLinkDescrMaxLspBandwidthPrio7)
{
    tTlmTeIfDescr       TlmTeIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeIfDescr, 0, sizeof (TlmTeIfDescr));

    i1RetVal = TlmGetAllTeLinkDescriptorTable (i4IfIndex, u4TeLinkDescriptorId,
                                               &TlmTeIfDescr);
    FLOAT_TO_OCTETSTRING (TlmTeIfDescr.aMaxLSPBwPrio[PRIORITY7],
                          pRetValTeLinkDescrMaxLspBandwidthPrio7);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkDescrInterfaceMtu
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                retValTeLinkDescrInterfaceMtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkDescrInterfaceMtu (INT4 i4IfIndex, UINT4 u4TeLinkDescriptorId,
                               UINT4 *pu4RetValTeLinkDescrInterfaceMtu)
{
    tTlmTeIfDescr       TlmTeIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeIfDescr, 0, sizeof (TlmTeIfDescr));

    i1RetVal = TlmGetAllTeLinkDescriptorTable (i4IfIndex, u4TeLinkDescriptorId,
                                               &TlmTeIfDescr);

    *pu4RetValTeLinkDescrInterfaceMtu = TlmTeIfDescr.u2MTU;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkDescrIndication
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                retValTeLinkDescrIndication
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkDescrIndication (INT4 i4IfIndex, UINT4 u4TeLinkDescriptorId,
                             INT4 *pi4RetValTeLinkDescrIndication)
{
    tTlmTeIfDescr       TlmTeIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeIfDescr, 0, sizeof (TlmTeIfDescr));

    i1RetVal = TlmGetAllTeLinkDescriptorTable (i4IfIndex, u4TeLinkDescriptorId,
                                               &TlmTeIfDescr);

    *pi4RetValTeLinkDescrIndication = TlmTeIfDescr.i4Indication;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkDescrRowStatus
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                retValTeLinkDescrRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkDescrRowStatus (INT4 i4IfIndex, UINT4 u4TeLinkDescriptorId,
                            INT4 *pi4RetValTeLinkDescrRowStatus)
{
    tTlmTeIfDescr       TlmTeIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeIfDescr, 0, sizeof (TlmTeIfDescr));

    i1RetVal = TlmGetAllTeLinkDescriptorTable (i4IfIndex, u4TeLinkDescriptorId,
                                               &TlmTeIfDescr);

    *pi4RetValTeLinkDescrRowStatus = TlmTeIfDescr.i4RowStatus;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkDescrStorageType
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                retValTeLinkDescrStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkDescrStorageType (INT4 i4IfIndex, UINT4 u4TeLinkDescriptorId,
                              INT4 *pi4RetValTeLinkDescrStorageType)
{
    tTlmTeIfDescr       TlmTeIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeIfDescr, 0, sizeof (TlmTeIfDescr));

    i1RetVal = TlmGetAllTeLinkDescriptorTable (i4IfIndex, u4TeLinkDescriptorId,
                                               &TlmTeIfDescr);

    *pi4RetValTeLinkDescrStorageType = TlmTeIfDescr.i4StorageType;

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetTeLinkDescrSwitchingCapability
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                setValTeLinkDescrSwitchingCapability
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkDescrSwitchingCapability (INT4 i4IfIndex,
                                      UINT4 u4TeLinkDescriptorId,
                                      INT4
                                      i4SetValTeLinkDescrSwitchingCapability)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (i4SetValTeLinkDescrSwitchingCapability);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetTeLinkDescrEncodingType
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                setValTeLinkDescrEncodingType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkDescrEncodingType (INT4 i4IfIndex, UINT4 u4TeLinkDescriptorId,
                               INT4 i4SetValTeLinkDescrEncodingType)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (i4SetValTeLinkDescrEncodingType);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetTeLinkDescrMinLspBandwidth
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                setValTeLinkDescrMinLspBandwidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkDescrMinLspBandwidth (INT4 i4IfIndex, UINT4 u4TeLinkDescriptorId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValTeLinkDescrMinLspBandwidth)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (pSetValTeLinkDescrMinLspBandwidth);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetTeLinkDescrMaxLspBandwidthPrio0
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                setValTeLinkDescrMaxLspBandwidthPrio0
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkDescrMaxLspBandwidthPrio0 (INT4 i4IfIndex,
                                       UINT4 u4TeLinkDescriptorId,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pSetValTeLinkDescrMaxLspBandwidthPrio0)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (pSetValTeLinkDescrMaxLspBandwidthPrio0);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetTeLinkDescrMaxLspBandwidthPrio1
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                setValTeLinkDescrMaxLspBandwidthPrio1
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkDescrMaxLspBandwidthPrio1 (INT4 i4IfIndex,
                                       UINT4 u4TeLinkDescriptorId,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pSetValTeLinkDescrMaxLspBandwidthPrio1)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (pSetValTeLinkDescrMaxLspBandwidthPrio1);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetTeLinkDescrMaxLspBandwidthPrio2
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                setValTeLinkDescrMaxLspBandwidthPrio2
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkDescrMaxLspBandwidthPrio2 (INT4 i4IfIndex,
                                       UINT4 u4TeLinkDescriptorId,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pSetValTeLinkDescrMaxLspBandwidthPrio2)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (pSetValTeLinkDescrMaxLspBandwidthPrio2);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetTeLinkDescrMaxLspBandwidthPrio3
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                setValTeLinkDescrMaxLspBandwidthPrio3
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkDescrMaxLspBandwidthPrio3 (INT4 i4IfIndex,
                                       UINT4 u4TeLinkDescriptorId,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pSetValTeLinkDescrMaxLspBandwidthPrio3)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (pSetValTeLinkDescrMaxLspBandwidthPrio3);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetTeLinkDescrMaxLspBandwidthPrio4
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                setValTeLinkDescrMaxLspBandwidthPrio4
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkDescrMaxLspBandwidthPrio4 (INT4 i4IfIndex,
                                       UINT4 u4TeLinkDescriptorId,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pSetValTeLinkDescrMaxLspBandwidthPrio4)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (pSetValTeLinkDescrMaxLspBandwidthPrio4);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetTeLinkDescrMaxLspBandwidthPrio5
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                setValTeLinkDescrMaxLspBandwidthPrio5
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkDescrMaxLspBandwidthPrio5 (INT4 i4IfIndex,
                                       UINT4 u4TeLinkDescriptorId,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pSetValTeLinkDescrMaxLspBandwidthPrio5)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (pSetValTeLinkDescrMaxLspBandwidthPrio5);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetTeLinkDescrMaxLspBandwidthPrio6
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                setValTeLinkDescrMaxLspBandwidthPrio6
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkDescrMaxLspBandwidthPrio6 (INT4 i4IfIndex,
                                       UINT4 u4TeLinkDescriptorId,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pSetValTeLinkDescrMaxLspBandwidthPrio6)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (pSetValTeLinkDescrMaxLspBandwidthPrio6);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetTeLinkDescrMaxLspBandwidthPrio7
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                setValTeLinkDescrMaxLspBandwidthPrio7
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkDescrMaxLspBandwidthPrio7 (INT4 i4IfIndex,
                                       UINT4 u4TeLinkDescriptorId,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pSetValTeLinkDescrMaxLspBandwidthPrio7)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (pSetValTeLinkDescrMaxLspBandwidthPrio7);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetTeLinkDescrInterfaceMtu
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                setValTeLinkDescrInterfaceMtu
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkDescrInterfaceMtu (INT4 i4IfIndex,
                               UINT4 u4TeLinkDescriptorId,
                               UINT4 u4SetValTeLinkDescrInterfaceMtu)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (u4SetValTeLinkDescrInterfaceMtu);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetTeLinkDescrIndication
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                setValTeLinkDescrIndication
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkDescrIndication (INT4 i4IfIndex,
                             UINT4 u4TeLinkDescriptorId,
                             INT4 i4SetValTeLinkDescrIndication)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (i4SetValTeLinkDescrIndication);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetTeLinkDescrRowStatus
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                setValTeLinkDescrRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkDescrRowStatus (INT4 i4IfIndex, UINT4 u4TeLinkDescriptorId,
                            INT4 i4SetValTeLinkDescrRowStatus)
{
    tTlmTeIfDescr       TlmTeIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeIfDescr, 0, sizeof (TlmTeIfDescr));

    TlmTeIfDescr.i4RowStatus = i4SetValTeLinkDescrRowStatus;

    i1RetVal = TlmSetAllTeLinkDescriptorTable (i4IfIndex, u4TeLinkDescriptorId,
                                               &TlmTeIfDescr,
                                               TLM_TE_LINK_DESC_ROW_STATUS);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetTeLinkDescrStorageType
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                setValTeLinkDescrStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkDescrStorageType (INT4 i4IfIndex, UINT4 u4TeLinkDescriptorId,
                              INT4 i4SetValTeLinkDescrStorageType)
{
    tTlmTeIfDescr       TlmTeIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeIfDescr, 0, sizeof (TlmTeIfDescr));

    TlmTeIfDescr.i4StorageType = i4SetValTeLinkDescrStorageType;

    i1RetVal = TlmSetAllTeLinkDescriptorTable (i4IfIndex, u4TeLinkDescriptorId,
                                               &TlmTeIfDescr,
                                               TLM_TE_LINK_DESC_STORAGE_TYPE);

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2TeLinkDescrSwitchingCapability
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                testValTeLinkDescrSwitchingCapability
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkDescrSwitchingCapability (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                         UINT4 u4TeLinkDescriptorId,
                                         INT4
                                         i4TestValTeLinkDescrSwitchingCapability)
{
    *pu4ErrorCode = SNMP_ERR_READ_ONLY;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (i4TestValTeLinkDescrSwitchingCapability);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkDescrEncodingType
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                testValTeLinkDescrEncodingType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkDescrEncodingType (UINT4 *pu4ErrorCode,
                                  INT4 i4IfIndex,
                                  UINT4 u4TeLinkDescriptorId,
                                  INT4 i4TestValTeLinkDescrEncodingType)
{
    *pu4ErrorCode = SNMP_ERR_READ_ONLY;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (i4TestValTeLinkDescrEncodingType);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkDescrMinLspBandwidth
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                testValTeLinkDescrMinLspBandwidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkDescrMinLspBandwidth (UINT4 *pu4ErrorCode,
                                     INT4 i4IfIndex,
                                     UINT4 u4TeLinkDescriptorId,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pTestValTeLinkDescrMinLspBandwidth)
{
    *pu4ErrorCode = SNMP_ERR_READ_ONLY;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (pTestValTeLinkDescrMinLspBandwidth);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkDescrMaxLspBandwidthPrio0
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                testValTeLinkDescrMaxLspBandwidthPrio0
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkDescrMaxLspBandwidthPrio0 (UINT4 *pu4ErrorCode,
                                          INT4 i4IfIndex,
                                          UINT4 u4TeLinkDescriptorId,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pTestValTeLinkDescrMaxLspBandwidthPrio0)
{
    *pu4ErrorCode = SNMP_ERR_READ_ONLY;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (pTestValTeLinkDescrMaxLspBandwidthPrio0);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkDescrMaxLspBandwidthPrio1
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                testValTeLinkDescrMaxLspBandwidthPrio1
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkDescrMaxLspBandwidthPrio1 (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                          UINT4 u4TeLinkDescriptorId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pTestValTeLinkDescrMaxLspBandwidthPrio1)
{
    *pu4ErrorCode = SNMP_ERR_READ_ONLY;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (pTestValTeLinkDescrMaxLspBandwidthPrio1);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkDescrMaxLspBandwidthPrio2
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                testValTeLinkDescrMaxLspBandwidthPrio2
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkDescrMaxLspBandwidthPrio2 (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                          UINT4 u4TeLinkDescriptorId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pTestValTeLinkDescrMaxLspBandwidthPrio2)
{
    *pu4ErrorCode = SNMP_ERR_READ_ONLY;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (pTestValTeLinkDescrMaxLspBandwidthPrio2);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkDescrMaxLspBandwidthPrio3
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                testValTeLinkDescrMaxLspBandwidthPrio3
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkDescrMaxLspBandwidthPrio3 (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                          UINT4 u4TeLinkDescriptorId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pTestValTeLinkDescrMaxLspBandwidthPrio3)
{
    *pu4ErrorCode = SNMP_ERR_READ_ONLY;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (pTestValTeLinkDescrMaxLspBandwidthPrio3);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkDescrMaxLspBandwidthPrio4
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                testValTeLinkDescrMaxLspBandwidthPrio4
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkDescrMaxLspBandwidthPrio4 (UINT4 *pu4ErrorCode,
                                          INT4 i4IfIndex,
                                          UINT4 u4TeLinkDescriptorId,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pTestValTeLinkDescrMaxLspBandwidthPrio4)
{
    *pu4ErrorCode = SNMP_ERR_READ_ONLY;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (pTestValTeLinkDescrMaxLspBandwidthPrio4);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkDescrMaxLspBandwidthPrio5
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                testValTeLinkDescrMaxLspBandwidthPrio5
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkDescrMaxLspBandwidthPrio5 (UINT4 *pu4ErrorCode,
                                          INT4 i4IfIndex,
                                          UINT4 u4TeLinkDescriptorId,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pTestValTeLinkDescrMaxLspBandwidthPrio5)
{
    *pu4ErrorCode = SNMP_ERR_READ_ONLY;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (pTestValTeLinkDescrMaxLspBandwidthPrio5);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkDescrMaxLspBandwidthPrio6
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                testValTeLinkDescrMaxLspBandwidthPrio6
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkDescrMaxLspBandwidthPrio6 (UINT4 *pu4ErrorCode,
                                          INT4 i4IfIndex,
                                          UINT4 u4TeLinkDescriptorId,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pTestValTeLinkDescrMaxLspBandwidthPrio6)
{
    *pu4ErrorCode = SNMP_ERR_READ_ONLY;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (pTestValTeLinkDescrMaxLspBandwidthPrio6);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkDescrMaxLspBandwidthPrio7
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                testValTeLinkDescrMaxLspBandwidthPrio7
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkDescrMaxLspBandwidthPrio7 (UINT4 *pu4ErrorCode,
                                          INT4 i4IfIndex,
                                          UINT4 u4TeLinkDescriptorId,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pTestValTeLinkDescrMaxLspBandwidthPrio7)
{
    *pu4ErrorCode = SNMP_ERR_READ_ONLY;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (pTestValTeLinkDescrMaxLspBandwidthPrio7);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkDescrInterfaceMtu
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                testValTeLinkDescrInterfaceMtu
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkDescrInterfaceMtu (UINT4 *pu4ErrorCode,
                                  INT4 i4IfIndex,
                                  UINT4 u4TeLinkDescriptorId,
                                  UINT4 u4TestValTeLinkDescrInterfaceMtu)
{
    *pu4ErrorCode = SNMP_ERR_READ_ONLY;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (u4TestValTeLinkDescrInterfaceMtu);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkDescrIndication
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                testValTeLinkDescrIndication
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkDescrIndication (UINT4 *pu4ErrorCode,
                                INT4 i4IfIndex,
                                UINT4 u4TeLinkDescriptorId,
                                INT4 i4TestValTeLinkDescrIndication)
{
    *pu4ErrorCode = SNMP_ERR_READ_ONLY;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TeLinkDescriptorId);
    UNUSED_PARAM (i4TestValTeLinkDescrIndication);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkDescrRowStatus
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                testValTeLinkDescrRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkDescrRowStatus (UINT4 *pu4ErrorCode,
                               INT4 i4IfIndex,
                               UINT4 u4TeLinkDescriptorId,
                               INT4 i4TestValTeLinkDescrRowStatus)
{
    tTlmTeIfDescr       TlmTeIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeIfDescr, 0, sizeof (TlmTeIfDescr));

    TlmTeIfDescr.i4RowStatus = i4TestValTeLinkDescrRowStatus;

    i1RetVal = TlmTestAllTeLinkDescriptorTable (pu4ErrorCode, i4IfIndex,
                                                u4TeLinkDescriptorId,
                                                &TlmTeIfDescr,
                                                TLM_TE_LINK_DESC_ROW_STATUS);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkDescrStorageType
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId

                The Object 
                testValTeLinkDescrStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkDescrStorageType (UINT4 *pu4ErrorCode,
                                 INT4 i4IfIndex,
                                 UINT4 u4TeLinkDescriptorId,
                                 INT4 i4TestValTeLinkDescrStorageType)
{
    tTlmTeIfDescr       TlmTeIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeIfDescr, 0, sizeof (TlmTeIfDescr));

    TlmTeIfDescr.i4StorageType = i4TestValTeLinkDescrStorageType;

    i1RetVal = TlmTestAllTeLinkDescriptorTable (pu4ErrorCode, i4IfIndex,
                                                u4TeLinkDescriptorId,
                                                &TlmTeIfDescr,
                                                TLM_TE_LINK_DESC_STORAGE_TYPE);

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2TeLinkDescriptorTable
 Input       :  The Indices
                IfIndex
                TeLinkDescriptorId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2TeLinkDescriptorTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : TeLinkSrlgTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceTeLinkSrlgTable
 Input       :  The Indices
                IfIndex
                TeLinkSrlg
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceTeLinkSrlgTable (INT4 i4IfIndex, UINT4 u4TeLinkSrlg)
{
    UNUSED_PARAM (u4TeLinkSrlg);

    return (nmhValidateIndexInstanceTeLinkTable (i4IfIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexTeLinkSrlgTable
 Input       :  The Indices
                IfIndex
                TeLinkSrlg
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexTeLinkSrlgTable (INT4 *pi4IfIndex, UINT4 *pu4TeLinkSrlg)
{
    tTlmTeLink         *pTeIface = NULL;
    tTlmTeLinkSrlg     *pTeSrlg = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    if ((pLstNode = TMO_SLL_First (&(gTlmGlobalInfo.sortTeIfLst))) != NULL)
    {
        pTeIface = GET_TE_IF_PTR_FROM_SORT_LST (pLstNode);

        if ((pLstNode = TMO_SLL_First (&(pTeIface->srlgList))) != NULL)
        {
            pTeSrlg = GET_TE_SRLG_PTR_FROM_SORT_LST (pLstNode);

            *pi4IfIndex = pTeIface->i4IfIndex;
            *pu4TeLinkSrlg = pTeSrlg->u4SrlgNo;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexTeLinkSrlgTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                TeLinkSrlg
                nextTeLinkSrlg
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexTeLinkSrlgTable (INT4 i4IfIndex,
                                INT4 *pi4NextIfIndex,
                                UINT4 u4TeLinkSrlg, UINT4 *pu4NextTeLinkSrlg)
{
    tTlmTeLink         *pTeIface = NULL;
    tTlmTeLinkSrlg     *pTeSrlg = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pIfNode = NULL;

    TMO_SLL_Scan (&(gTlmGlobalInfo.sortTeIfLst), pIfNode, tTMO_SLL_NODE *)
    {
        pTeIface = GET_TE_IF_PTR_FROM_SORT_LST (pIfNode);
        if (pTeIface->i4IfIndex == i4IfIndex)
        {
            TMO_SLL_Scan (&(pTeIface->srlgList), pLstNode, tTMO_SLL_NODE *)
            {
                pTeSrlg = GET_TE_SRLG_PTR_FROM_SORT_LST (pLstNode);

                if (pTeSrlg->u4SrlgNo > u4TeLinkSrlg)
                {
                    *pi4NextIfIndex = pTeIface->i4IfIndex;
                    *pu4NextTeLinkSrlg = pTeSrlg->u4SrlgNo;
                    return SNMP_SUCCESS;
                }
            }
        }
        else if (pTeIface->i4IfIndex > i4IfIndex)
        {
            if ((pLstNode = TMO_SLL_First (&(pTeIface->srlgList))) != NULL)
            {
                pTeSrlg = GET_TE_SRLG_PTR_FROM_SORT_LST (pLstNode);
                *pi4NextIfIndex = pTeIface->i4IfIndex;
                *pu4NextTeLinkSrlg = pTeSrlg->u4SrlgNo;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetTeLinkSrlgRowStatus
 Input       :  The Indices
                IfIndex
                TeLinkSrlg

                The Object 
                retValTeLinkSrlgRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkSrlgRowStatus (INT4 i4IfIndex,
                           UINT4 u4TeLinkSrlg,
                           INT4 *pi4RetValTeLinkSrlgRowStatus)
{
    tTlmTeLinkSrlg      TlmTeLinkSrlg;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLinkSrlg, 0, sizeof (TlmTeLinkSrlg));

    i1RetVal = TlmGetAllTeLinkSrlgTable (i4IfIndex, u4TeLinkSrlg,
                                         &TlmTeLinkSrlg);

    *pi4RetValTeLinkSrlgRowStatus = TlmTeLinkSrlg.i4RowStatus;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkSrlgStorageType
 Input       :  The Indices
                IfIndex
                TeLinkSrlg

                The Object 
                retValTeLinkSrlgStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkSrlgStorageType (INT4 i4IfIndex,
                             UINT4 u4TeLinkSrlg,
                             INT4 *pi4RetValTeLinkSrlgStorageType)
{
    tTlmTeLinkSrlg      TlmTeLinkSrlg;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLinkSrlg, 0, sizeof (TlmTeLinkSrlg));

    i1RetVal = TlmGetAllTeLinkSrlgTable (i4IfIndex, u4TeLinkSrlg,
                                         &TlmTeLinkSrlg);

    *pi4RetValTeLinkSrlgStorageType = TlmTeLinkSrlg.i4StorageType;

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetTeLinkSrlgRowStatus
 Input       :  The Indices
                IfIndex
                TeLinkSrlg

                The Object 
                setValTeLinkSrlgRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkSrlgRowStatus (INT4 i4IfIndex,
                           UINT4 u4TeLinkSrlg, INT4 i4SetValTeLinkSrlgRowStatus)
{
    tTlmTeLinkSrlg      TlmTeLinkSrlg;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLinkSrlg, 0, sizeof (TlmTeLinkSrlg));
    TlmTeLinkSrlg.i4RowStatus = i4SetValTeLinkSrlgRowStatus;

    i1RetVal =
        TlmSetAllTeLinkSrlgTable (i4IfIndex, u4TeLinkSrlg, &TlmTeLinkSrlg,
                                  TLM_TE_LINK_SRLG_ROW_STATUS);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetTeLinkSrlgStorageType
 Input       :  The Indices
                IfIndex
                TeLinkSrlg

                The Object 
                setValTeLinkSrlgStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkSrlgStorageType (INT4 i4IfIndex,
                             UINT4 u4TeLinkSrlg,
                             INT4 i4SetValTeLinkSrlgStorageType)
{
    tTlmTeLinkSrlg      TlmTeLinkSrlg;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLinkSrlg, 0, sizeof (TlmTeLinkSrlg));

    TlmTeLinkSrlg.i4StorageType = i4SetValTeLinkSrlgStorageType;

    i1RetVal =
        TlmSetAllTeLinkSrlgTable (i4IfIndex, u4TeLinkSrlg, &TlmTeLinkSrlg,
                                  TLM_TE_LINK_SRLG_STORAGE_TYPE);

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2TeLinkSrlgRowStatus
 Input       :  The Indices
                IfIndex
                TeLinkSrlg

                The Object 
                testValTeLinkSrlgRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkSrlgRowStatus (UINT4 *pu4ErrorCode,
                              INT4 i4IfIndex,
                              UINT4 u4TeLinkSrlg,
                              INT4 i4TestValTeLinkSrlgRowStatus)
{
    tTlmTeLinkSrlg      TlmTeLinkSrlg;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLinkSrlg, 0, sizeof (TlmTeLinkSrlg));

    TlmTeLinkSrlg.i4RowStatus = i4TestValTeLinkSrlgRowStatus;

    i1RetVal = TlmTestAllTeLinkSrlgTable (pu4ErrorCode, i4IfIndex,
                                          u4TeLinkSrlg, &TlmTeLinkSrlg,
                                          TLM_TE_LINK_SRLG_ROW_STATUS);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkSrlgStorageType
 Input       :  The Indices
                IfIndex
                TeLinkSrlg

                The Object 
                testValTeLinkSrlgStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkSrlgStorageType (UINT4 *pu4ErrorCode,
                                INT4 i4IfIndex,
                                UINT4 u4TeLinkSrlg,
                                INT4 i4TestValTeLinkSrlgStorageType)
{
    tTlmTeLinkSrlg      TlmTeLinkSrlg;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLinkSrlg, 0, sizeof (TlmTeLinkSrlg));

    TlmTeLinkSrlg.i4StorageType = i4TestValTeLinkSrlgStorageType;

    i1RetVal = TlmTestAllTeLinkSrlgTable (pu4ErrorCode, i4IfIndex,
                                          u4TeLinkSrlg, &TlmTeLinkSrlg,
                                          TLM_TE_LINK_SRLG_STORAGE_TYPE);

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2TeLinkSrlgTable
 Input       :  The Indices
                IfIndex
                TeLinkSrlg
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2TeLinkSrlgTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : TeLinkBandwidthTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceTeLinkBandwidthTable
 Input       :  The Indices
                IfIndex
                TeLinkBandwidthPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceTeLinkBandwidthTable (INT4 i4IfIndex,
                                              UINT4 u4TeLinkBandwidthPriority)
{
    if ((i4IfIndex < CFA_MIN_TELINK_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_TELINK_IF_INDEX))
    {
        return SNMP_FAILURE;
    }

    if (u4TeLinkBandwidthPriority > TLM_MAX_PRIORITY_LVL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexTeLinkBandwidthTable
 Input       :  The Indices
                IfIndex
                TeLinkBandwidthPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexTeLinkBandwidthTable (INT4 *pi4IfIndex,
                                      UINT4 *pu4TeLinkBandwidthPriority)
{
    tTlmTeLink         *pTeIface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT1               u1Priority = TLM_ZERO;

    TMO_SLL_Scan (&(gTlmGlobalInfo.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pTeIface = GET_TE_IF_PTR_FROM_SORT_LST (pLstNode);
        for (u1Priority = TLM_ZERO; u1Priority < TLM_MAX_PRIORITY_LVL;
             u1Priority++)
        {
            if (pTeIface->aUnResBw[u1Priority].i4RowStatus !=
                TLM_ROWSTATUS_INVALID)
            {
                *pi4IfIndex = pTeIface->i4IfIndex;
                *pu4TeLinkBandwidthPriority = u1Priority;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexTeLinkBandwidthTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                TeLinkBandwidthPriority
                nextTeLinkBandwidthPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexTeLinkBandwidthTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                     UINT4 u4TeLinkBandwidthPriority,
                                     UINT4 *pu4NextTeLinkBandwidthPriority)
{
    tTlmTeLink         *pTeIface = NULL;
    tTMO_SLL_NODE      *pIfNode = NULL;
    UINT1               u1Priority = TLM_ZERO;

    TMO_SLL_Scan (&(gTlmGlobalInfo.sortTeIfLst), pIfNode, tTMO_SLL_NODE *)
    {
        pTeIface = GET_TE_IF_PTR_FROM_SORT_LST (pIfNode);
        if (pTeIface->i4IfIndex == i4IfIndex)
        {
            for (u1Priority = (UINT1) (u4TeLinkBandwidthPriority + TLM_ONE);
                 u1Priority < TLM_MAX_PRIORITY_LVL; u1Priority++)
            {
                if (pTeIface->aUnResBw[u1Priority].i4RowStatus !=
                    TLM_ROWSTATUS_INVALID)
                {
                    *pi4NextIfIndex = pTeIface->i4IfIndex;
                    *pu4NextTeLinkBandwidthPriority = u1Priority;
                    return SNMP_SUCCESS;
                }
            }
        }
        else if (pTeIface->i4IfIndex > i4IfIndex)
        {
            for (u1Priority = TLM_ZERO; u1Priority < TLM_MAX_PRIORITY_LVL;
                 u1Priority++)
            {
                if (pTeIface->aUnResBw[u1Priority].i4RowStatus !=
                    TLM_ROWSTATUS_INVALID)
                {
                    *pi4NextIfIndex = pTeIface->i4IfIndex;
                    *pu4NextTeLinkBandwidthPriority = u1Priority;
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetTeLinkBandwidthUnreserved
 Input       :  The Indices
                IfIndex
                TeLinkBandwidthPriority

                The Object 
                retValTeLinkBandwidthUnreserved
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkBandwidthUnreserved (INT4 i4IfIndex,
                                 UINT4 u4TeLinkBandwidthPriority,
                                 tSNMP_OCTET_STRING_TYPE
                                 * pRetValTeLinkBandwidthUnreserved)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4UnResBwPrio = u4TeLinkBandwidthPriority;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (tTlmTeLink));

    i1RetVal = TlmGetAllTeLinkBandwidthTable (i4IfIndex, u4UnResBwPrio,
                                              &TlmTeLink);
    FLOAT_TO_OCTETSTRING (TlmTeLink.aUnResBw[u4UnResBwPrio].unResBw,
                          pRetValTeLinkBandwidthUnreserved);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkBandwidthRowStatus
 Input       :  The Indices
                IfIndex
                TeLinkBandwidthPriority

                The Object 
                retValTeLinkBandwidthRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkBandwidthRowStatus (INT4 i4IfIndex,
                                UINT4 u4TeLinkBandwidthPriority,
                                INT4 *pi4RetValTeLinkBandwidthRowStatus)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4UnResBwPrio = u4TeLinkBandwidthPriority;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkBandwidthTable (i4IfIndex, u4UnResBwPrio,
                                              &TlmTeLink);

    *pi4RetValTeLinkBandwidthRowStatus =
        TlmTeLink.aUnResBw[u4UnResBwPrio].i4RowStatus;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetTeLinkBandwidthStorageType
 Input       :  The Indices
                IfIndex
                TeLinkBandwidthPriority

                The Object 
                retValTeLinkBandwidthStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTeLinkBandwidthStorageType (INT4 i4IfIndex,
                                  UINT4 u4TeLinkBandwidthPriority,
                                  INT4 *pi4RetValTeLinkBandwidthStorageType)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4UnResBwPrio = u4TeLinkBandwidthPriority;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    i1RetVal =
        TlmGetAllTeLinkBandwidthTable (i4IfIndex, u4TeLinkBandwidthPriority,
                                       &TlmTeLink);

    *pi4RetValTeLinkBandwidthStorageType =
        TlmTeLink.aUnResBw[u4UnResBwPrio].i4StorageType;

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetTeLinkBandwidthRowStatus
 Input       :  The Indices
                IfIndex
                TeLinkBandwidthPriority

                The Object 
                setValTeLinkBandwidthRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkBandwidthRowStatus (INT4 i4IfIndex,
                                UINT4 u4TeLinkBandwidthPriority,
                                INT4 i4SetValTeLinkBandwidthRowStatus)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4UnResBwPrio = u4TeLinkBandwidthPriority;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    TlmTeLink.aUnResBw[u4UnResBwPrio].i4RowStatus =
        i4SetValTeLinkBandwidthRowStatus;

    i1RetVal =
        TlmSetAllTeLinkBandwidthTable (i4IfIndex, u4TeLinkBandwidthPriority,
                                       &TlmTeLink, TLM_TE_LINK_BW_ROW_STATUS);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetTeLinkBandwidthStorageType
 Input       :  The Indices
                IfIndex
                TeLinkBandwidthPriority

                The Object 
                setValTeLinkBandwidthStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTeLinkBandwidthStorageType (INT4 i4IfIndex,
                                  UINT4 u4TeLinkBandwidthPriority,
                                  INT4 i4SetValTeLinkBandwidthStorageType)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4UnResBwPrio = u4TeLinkBandwidthPriority;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    TlmTeLink.aUnResBw[u4UnResBwPrio].i4StorageType =
        i4SetValTeLinkBandwidthStorageType;

    i1RetVal =
        TlmSetAllTeLinkBandwidthTable (i4IfIndex, u4TeLinkBandwidthPriority,
                                       &TlmTeLink, TLM_TE_LINK_BW_STORAGE_TYPE);

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2TeLinkBandwidthRowStatus
 Input       :  The Indices
                IfIndex
                TeLinkBandwidthPriority

                The Object 
                testValTeLinkBandwidthRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkBandwidthRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                   UINT4 u4TeLinkBandwidthPriority,
                                   INT4 i4TestValTeLinkBandwidthRowStatus)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4UnResBwPrio = u4TeLinkBandwidthPriority;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (tTlmTeLink));

    TlmTeLink.aUnResBw[u4UnResBwPrio].i4RowStatus =
        i4TestValTeLinkBandwidthRowStatus;

    i1RetVal = TlmTestAllTeLinkBandwidthTable (pu4ErrorCode, i4IfIndex,
                                               u4TeLinkBandwidthPriority,
                                               &TlmTeLink,
                                               TLM_TE_LINK_BW_ROW_STATUS);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2TeLinkBandwidthStorageType
 Input       :  The Indices
                IfIndex
                TeLinkBandwidthPriority

                The Object 
                testValTeLinkBandwidthStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TeLinkBandwidthStorageType (UINT4 *pu4ErrorCode,
                                     INT4 i4IfIndex,
                                     UINT4 u4TeLinkBandwidthPriority,
                                     INT4 i4TestValTeLinkBandwidthStorageType)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (tTlmTeLink));

    TlmTeLink.i4StorageType = i4TestValTeLinkBandwidthStorageType;

    i1RetVal = TlmTestAllTeLinkBandwidthTable (pu4ErrorCode, i4IfIndex,
                                               u4TeLinkBandwidthPriority,
                                               &TlmTeLink,
                                               TLM_TE_LINK_BW_STORAGE_TYPE);

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2TeLinkBandwidthTable
 Input       :  The Indices
                IfIndex
                TeLinkBandwidthPriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2TeLinkBandwidthTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : ComponentLinkTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceComponentLinkTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceComponentLinkTable (INT4 i4IfIndex)
{
    if ((i4IfIndex < TLM_ONE) || (i4IfIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexComponentLinkTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexComponentLinkTable (INT4 *pi4IfIndex)
{
    tTlmComponentLink  *pComponentIface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    if ((pLstNode =
         TMO_SLL_First (&(gTlmGlobalInfo.sortComponentIfLst))) != NULL)
    {
        pComponentIface = GET_COMPONENT_IF_PTR_FROM_SORT_LST (pLstNode);

        *pi4IfIndex = pComponentIface->i4IfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexComponentLinkTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexComponentLinkTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tTlmComponentLink  *pComponentIface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    TMO_SLL_Scan (&(gTlmGlobalInfo.sortComponentIfLst), pLstNode,
                  tTMO_SLL_NODE *)
    {
        pComponentIface = GET_COMPONENT_IF_PTR_FROM_SORT_LST (pLstNode);
        if (pComponentIface->i4IfIndex > i4IfIndex)
        {
            *pi4NextIfIndex = pComponentIface->i4IfIndex;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetComponentLinkMaxResBandwidth
 Input       :  The Indices
                IfIndex

                The Object 
                retValComponentLinkMaxResBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkMaxResBandwidth (INT4 i4IfIndex,
                                    tSNMP_OCTET_STRING_TYPE
                                    * pRetValComponentLinkMaxResBandwidth)
{
    tTlmComponentLink   TlmComponentLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentLink, 0, sizeof (tTlmComponentLink));

    i1RetVal = TlmGetAllComponentLinkTable (i4IfIndex, &TlmComponentLink);

    FLOAT_TO_OCTETSTRING (TlmComponentLink.maxResBw,
                          pRetValComponentLinkMaxResBandwidth);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetComponentLinkPreferredProtection
 Input       :  The Indices
                IfIndex

                The Object 
                retValComponentLinkPreferredProtection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkPreferredProtection (INT4 i4IfIndex,
                                        INT4
                                        *pi4RetValComponentLinkPreferredProtection)
{
    tTlmComponentLink   TlmComponentLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentLink, 0, sizeof (tTlmComponentLink));

    i1RetVal = TlmGetAllComponentLinkTable (i4IfIndex, &TlmComponentLink);

    *pi4RetValComponentLinkPreferredProtection =
        TlmComponentLink.i4LinkPrefProtection;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetComponentLinkCurrentProtection
 Input       :  The Indices
                IfIndex

                The Object 
                retValComponentLinkCurrentProtection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkCurrentProtection (INT4 i4IfIndex,
                                      INT4
                                      *pi4RetValComponentLinkCurrentProtection)
{
    tTlmComponentLink   TlmComponentLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentLink, 0, sizeof (tTlmComponentLink));

    i1RetVal = TlmGetAllComponentLinkTable (i4IfIndex, &TlmComponentLink);

    *pi4RetValComponentLinkCurrentProtection =
        TlmComponentLink.i4LinkCurrentProtection;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetComponentLinkRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValComponentLinkRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkRowStatus (INT4 i4IfIndex,
                              INT4 *pi4RetValComponentLinkRowStatus)
{
    tTlmComponentLink   TlmComponentLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentLink, 0, sizeof (tTlmComponentLink));

    i1RetVal = TlmGetAllComponentLinkTable (i4IfIndex, &TlmComponentLink);

    *pi4RetValComponentLinkRowStatus = TlmComponentLink.i4RowStatus;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetComponentLinkStorageType
 Input       :  The Indices
                IfIndex

                The Object 
                retValComponentLinkStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkStorageType (INT4 i4IfIndex,
                                INT4 *pi4RetValComponentLinkStorageType)
{
    tTlmComponentLink   TlmComponentLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentLink, 0, sizeof (tTlmComponentLink));

    i1RetVal = TlmGetAllComponentLinkTable (i4IfIndex, &TlmComponentLink);

    *pi4RetValComponentLinkStorageType = TlmComponentLink.i4StorageType;

    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetComponentLinkMaxResBandwidth
 Input       :  The Indices
                IfIndex

                The Object 
                setValComponentLinkMaxResBandwidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetComponentLinkMaxResBandwidth (INT4 i4IfIndex,
                                    tSNMP_OCTET_STRING_TYPE
                                    * pSetValComponentLinkMaxResBandwidth)
{
    tTlmComponentLink   TlmComponentLink;
    tBandWidth          maxResBw = (tBandWidth) TLM_ZERO;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentLink, 0, sizeof (tTlmComponentLink));

    maxResBw = TLM_OCTETSTRING_TO_FLOAT (pSetValComponentLinkMaxResBandwidth);
    TlmComponentLink.maxResBw = maxResBw;

    i1RetVal = TlmSetAllComponentLinkTable (i4IfIndex, &TlmComponentLink,
                                            TLM_COMP_LINK_MAX_RESV_BW);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetComponentLinkPreferredProtection
 Input       :  The Indices
                IfIndex

                The Object 
                setValComponentLinkPreferredProtection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetComponentLinkPreferredProtection (INT4 i4IfIndex,
                                        INT4
                                        i4SetValComponentLinkPreferredProtection)
{
    tTlmComponentLink   TlmComponentLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentLink, 0, sizeof (tTlmComponentLink));

    TlmComponentLink.i4LinkPrefProtection =
        i4SetValComponentLinkPreferredProtection;

    i1RetVal = TlmSetAllComponentLinkTable (i4IfIndex, &TlmComponentLink,
                                            TLM_COMP_LINK_PREF_PROTECT);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetComponentLinkRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                setValComponentLinkRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetComponentLinkRowStatus (INT4 i4IfIndex,
                              INT4 i4SetValComponentLinkRowStatus)
{
    tTlmComponentLink   TlmComponentLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentLink, 0, sizeof (tTlmComponentLink));

    TlmComponentLink.i4RowStatus = i4SetValComponentLinkRowStatus;

    i1RetVal = TlmSetAllComponentLinkTable (i4IfIndex, &TlmComponentLink,
                                            TLM_COMP_LINK_ROW_STATUS);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetComponentLinkStorageType
 Input       :  The Indices
                IfIndex

                The Object 
                setValComponentLinkStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetComponentLinkStorageType (INT4 i4IfIndex,
                                INT4 i4SetValComponentLinkStorageType)
{
    tTlmComponentLink   TlmComponentLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentLink, 0, sizeof (tTlmComponentLink));

    TlmComponentLink.i4StorageType = i4SetValComponentLinkStorageType;

    i1RetVal = TlmSetAllComponentLinkTable (i4IfIndex, &TlmComponentLink,
                                            TLM_COMP_LINK_STORAGE_TYPE);

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2ComponentLinkMaxResBandwidth
 Input       :  The Indices
                IfIndex

                The Object 
                testValComponentLinkMaxResBandwidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ComponentLinkMaxResBandwidth (UINT4 *pu4ErrorCode,
                                       INT4 i4IfIndex,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pTestValComponentLinkMaxResBandwidth)
{
    tTlmComponentLink   TlmComponentLink;
    tBandWidth          maxResBw = (tBandWidth) TLM_ZERO;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentLink, 0, sizeof (tTlmComponentLink));

    maxResBw = TLM_OCTETSTRING_TO_FLOAT (pTestValComponentLinkMaxResBandwidth);
    TlmComponentLink.maxResBw = maxResBw;

    i1RetVal = TlmTestAllComponentLinkTable (pu4ErrorCode, i4IfIndex,
                                             &TlmComponentLink,
                                             TLM_COMP_LINK_MAX_RESV_BW);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2ComponentLinkPreferredProtection
 Input       :  The Indices
                IfIndex

                The Object 
                testValComponentLinkPreferredProtection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ComponentLinkPreferredProtection (UINT4 *pu4ErrorCode,
                                           INT4 i4IfIndex,
                                           INT4
                                           i4TestValComponentLinkPreferredProtection)
{
    tTlmComponentLink   TlmComponentLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentLink, 0, sizeof (tTlmComponentLink));

    TlmComponentLink.i4LinkPrefProtection =
        i4TestValComponentLinkPreferredProtection;

    i1RetVal = TlmTestAllComponentLinkTable (pu4ErrorCode, i4IfIndex,
                                             &TlmComponentLink,
                                             TLM_COMP_LINK_PREF_PROTECT);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2ComponentLinkRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                testValComponentLinkRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ComponentLinkRowStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4IfIndex,
                                 INT4 i4TestValComponentLinkRowStatus)
{
    tTlmComponentLink   TlmComponentLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentLink, 0, sizeof (tTlmComponentLink));

    if (TLM_MODULE_STATUS == TLM_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }
    TlmComponentLink.i4RowStatus = i4TestValComponentLinkRowStatus;

    i1RetVal = TlmTestAllComponentLinkTable (pu4ErrorCode, i4IfIndex,
                                             &TlmComponentLink,
                                             TLM_COMP_LINK_ROW_STATUS);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2ComponentLinkStorageType
 Input       :  The Indices
                IfIndex

                The Object 
                testValComponentLinkStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ComponentLinkStorageType (UINT4 *pu4ErrorCode,
                                   INT4 i4IfIndex,
                                   INT4 i4TestValComponentLinkStorageType)
{
    tTlmComponentLink   TlmComponentLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentLink, 0, sizeof (tTlmComponentLink));

    TlmComponentLink.i4StorageType = i4TestValComponentLinkStorageType;

    i1RetVal = TlmTestAllComponentLinkTable (pu4ErrorCode, i4IfIndex,
                                             &TlmComponentLink,
                                             TLM_COMP_LINK_STORAGE_TYPE);

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2ComponentLinkTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2ComponentLinkTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : ComponentLinkDescriptorTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceComponentLinkDescriptorTable
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceComponentLinkDescriptorTable (INT4 i4IfIndex,
                                                      UINT4
                                                      u4ComponentLinkDescrId)
{
    UNUSED_PARAM (u4ComponentLinkDescrId);

    return (nmhValidateIndexInstanceComponentLinkTable (i4IfIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexComponentLinkDescriptorTable
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexComponentLinkDescriptorTable (INT4 *pi4IfIndex,
                                              UINT4 *pu4ComponentLinkDescrId)
{
    tTlmComponentLink  *pComponentIface = NULL;
    tTlmComponentIfDescr *pComponentDescr = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    if ((pLstNode =
         TMO_SLL_First (&(gTlmGlobalInfo.sortComponentIfLst))) != NULL)
    {
        pComponentIface = GET_COMPONENT_IF_PTR_FROM_SORT_LST (pLstNode);

        if ((pLstNode =
             TMO_SLL_First (&(pComponentIface->ifDescrList))) != NULL)
        {
            pComponentDescr = GET_COMPONENT_DESCR_PTR_FROM_SORT_LST (pLstNode);

            *pi4IfIndex = pComponentIface->i4IfIndex;
            *pu4ComponentLinkDescrId = pComponentDescr->u4DescrId;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexComponentLinkDescriptorTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                ComponentLinkDescrId
                nextComponentLinkDescrId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexComponentLinkDescriptorTable (INT4 i4IfIndex,
                                             INT4 *pi4NextIfIndex,
                                             UINT4 u4ComponentLinkDescrId,
                                             UINT4 *pu4NextComponentLinkDescrId)
{
    tTlmComponentLink  *pComponentIface = NULL;
    tTlmComponentIfDescr *pComponentDescr = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pIfNode = NULL;

    TMO_SLL_Scan (&(gTlmGlobalInfo.sortComponentIfLst), pIfNode,
                  tTMO_SLL_NODE *)
    {
        pComponentIface = GET_COMPONENT_IF_PTR_FROM_SORT_LST (pIfNode);
        if (pComponentIface->i4IfIndex == i4IfIndex)
        {
            TMO_SLL_Scan (&(pComponentIface->ifDescrList), pLstNode,
                          tTMO_SLL_NODE *)
            {
                pComponentDescr =
                    GET_COMPONENT_DESCR_PTR_FROM_SORT_LST (pLstNode);

                if (pComponentDescr->u4DescrId > u4ComponentLinkDescrId)
                {
                    *pi4NextIfIndex = pComponentIface->i4IfIndex;
                    *pu4NextComponentLinkDescrId = pComponentDescr->u4DescrId;
                    return SNMP_SUCCESS;
                }
            }
        }
        else if (pComponentIface->i4IfIndex > i4IfIndex)
        {
            if ((pLstNode =
                 TMO_SLL_First (&(pComponentIface->ifDescrList))) != NULL)
            {
                pComponentDescr =
                    GET_COMPONENT_DESCR_PTR_FROM_SORT_LST (pLstNode);
                *pi4NextIfIndex = pComponentIface->i4IfIndex;
                *pu4NextComponentLinkDescrId = pComponentDescr->u4DescrId;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetComponentLinkDescrSwitchingCapability
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                retValComponentLinkDescrSwitchingCapability
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkDescrSwitchingCapability (INT4 i4IfIndex,
                                             UINT4 u4ComponentLinkDescrId,
                                             INT4
                                             *pi4RetValComponentLinkDescrSwitchingCapability)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    i1RetVal = TlmGetAllComponentLinkDescriptorTable (i4IfIndex,
                                                      u4ComponentLinkDescrId,
                                                      &TlmComponentIfDescr);

    *pi4RetValComponentLinkDescrSwitchingCapability =
        TlmComponentIfDescr.i4SwitchingCap;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetComponentLinkDescrEncodingType
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                retValComponentLinkDescrEncodingType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkDescrEncodingType (INT4 i4IfIndex,
                                      UINT4 u4ComponentLinkDescrId,
                                      INT4
                                      *pi4RetValComponentLinkDescrEncodingType)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    i1RetVal = TlmGetAllComponentLinkDescriptorTable (i4IfIndex,
                                                      u4ComponentLinkDescrId,
                                                      &TlmComponentIfDescr);

    *pi4RetValComponentLinkDescrEncodingType =
        TlmComponentIfDescr.i4EncodingType;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetComponentLinkDescrMinLspBandwidth
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                retValComponentLinkDescrMinLspBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkDescrMinLspBandwidth (INT4 i4IfIndex,
                                         UINT4 u4ComponentLinkDescrId,
                                         tSNMP_OCTET_STRING_TYPE
                                         *
                                         pRetValComponentLinkDescrMinLspBandwidth)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    i1RetVal = TlmGetAllComponentLinkDescriptorTable (i4IfIndex,
                                                      u4ComponentLinkDescrId,
                                                      &TlmComponentIfDescr);

    FLOAT_TO_OCTETSTRING (TlmComponentIfDescr.minLSPBw,
                          pRetValComponentLinkDescrMinLspBandwidth);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetComponentLinkDescrMaxLspBandwidthPrio0
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                retValComponentLinkDescrMaxLspBandwidthPrio0
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkDescrMaxLspBandwidthPrio0 (INT4 i4IfIndex,
                                              UINT4 u4ComponentLinkDescrId,
                                              tSNMP_OCTET_STRING_TYPE
                                              *
                                              pRetValComponentLinkDescrMaxLspBandwidthPrio0)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    i1RetVal = TlmGetAllComponentLinkDescriptorTable (i4IfIndex,
                                                      u4ComponentLinkDescrId,
                                                      &TlmComponentIfDescr);

    FLOAT_TO_OCTETSTRING (TlmComponentIfDescr.aMaxLSPBwPrio[PRIORITY0],
                          pRetValComponentLinkDescrMaxLspBandwidthPrio0);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetComponentLinkDescrMaxLspBandwidthPrio1
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                retValComponentLinkDescrMaxLspBandwidthPrio1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkDescrMaxLspBandwidthPrio1 (INT4 i4IfIndex,
                                              UINT4 u4ComponentLinkDescrId,
                                              tSNMP_OCTET_STRING_TYPE
                                              *
                                              pRetValComponentLinkDescrMaxLspBandwidthPrio1)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    i1RetVal = TlmGetAllComponentLinkDescriptorTable (i4IfIndex,
                                                      u4ComponentLinkDescrId,
                                                      &TlmComponentIfDescr);
    FLOAT_TO_OCTETSTRING (TlmComponentIfDescr.aMaxLSPBwPrio[PRIORITY1],
                          pRetValComponentLinkDescrMaxLspBandwidthPrio1);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetComponentLinkDescrMaxLspBandwidthPrio2
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                retValComponentLinkDescrMaxLspBandwidthPrio2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkDescrMaxLspBandwidthPrio2 (INT4 i4IfIndex,
                                              UINT4 u4ComponentLinkDescrId,
                                              tSNMP_OCTET_STRING_TYPE
                                              *
                                              pRetValComponentLinkDescrMaxLspBandwidthPrio2)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    i1RetVal = TlmGetAllComponentLinkDescriptorTable (i4IfIndex,
                                                      u4ComponentLinkDescrId,
                                                      &TlmComponentIfDescr);

    FLOAT_TO_OCTETSTRING (TlmComponentIfDescr.aMaxLSPBwPrio[PRIORITY2],
                          pRetValComponentLinkDescrMaxLspBandwidthPrio2);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetComponentLinkDescrMaxLspBandwidthPrio3
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                retValComponentLinkDescrMaxLspBandwidthPrio3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkDescrMaxLspBandwidthPrio3 (INT4 i4IfIndex,
                                              UINT4 u4ComponentLinkDescrId,
                                              tSNMP_OCTET_STRING_TYPE
                                              *
                                              pRetValComponentLinkDescrMaxLspBandwidthPrio3)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    i1RetVal = TlmGetAllComponentLinkDescriptorTable (i4IfIndex,
                                                      u4ComponentLinkDescrId,
                                                      &TlmComponentIfDescr);
    FLOAT_TO_OCTETSTRING (TlmComponentIfDescr.aMaxLSPBwPrio[PRIORITY3],
                          pRetValComponentLinkDescrMaxLspBandwidthPrio3);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetComponentLinkDescrMaxLspBandwidthPrio4
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                retValComponentLinkDescrMaxLspBandwidthPrio4
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkDescrMaxLspBandwidthPrio4 (INT4 i4IfIndex,
                                              UINT4 u4ComponentLinkDescrId,
                                              tSNMP_OCTET_STRING_TYPE
                                              *
                                              pRetValComponentLinkDescrMaxLspBandwidthPrio4)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    i1RetVal = TlmGetAllComponentLinkDescriptorTable (i4IfIndex,
                                                      u4ComponentLinkDescrId,
                                                      &TlmComponentIfDescr);
    FLOAT_TO_OCTETSTRING (TlmComponentIfDescr.aMaxLSPBwPrio[PRIORITY4],
                          pRetValComponentLinkDescrMaxLspBandwidthPrio4);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetComponentLinkDescrMaxLspBandwidthPrio5
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                retValComponentLinkDescrMaxLspBandwidthPrio5
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkDescrMaxLspBandwidthPrio5 (INT4 i4IfIndex,
                                              UINT4 u4ComponentLinkDescrId,
                                              tSNMP_OCTET_STRING_TYPE
                                              *
                                              pRetValComponentLinkDescrMaxLspBandwidthPrio5)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    i1RetVal = TlmGetAllComponentLinkDescriptorTable (i4IfIndex,
                                                      u4ComponentLinkDescrId,
                                                      &TlmComponentIfDescr);
    FLOAT_TO_OCTETSTRING (TlmComponentIfDescr.aMaxLSPBwPrio[PRIORITY5],
                          pRetValComponentLinkDescrMaxLspBandwidthPrio5);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetComponentLinkDescrMaxLspBandwidthPrio6
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                retValComponentLinkDescrMaxLspBandwidthPrio6
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkDescrMaxLspBandwidthPrio6 (INT4 i4IfIndex,
                                              UINT4 u4ComponentLinkDescrId,
                                              tSNMP_OCTET_STRING_TYPE
                                              *
                                              pRetValComponentLinkDescrMaxLspBandwidthPrio6)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    i1RetVal = TlmGetAllComponentLinkDescriptorTable (i4IfIndex,
                                                      u4ComponentLinkDescrId,
                                                      &TlmComponentIfDescr);
    FLOAT_TO_OCTETSTRING (TlmComponentIfDescr.aMaxLSPBwPrio[PRIORITY6],
                          pRetValComponentLinkDescrMaxLspBandwidthPrio6);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetComponentLinkDescrMaxLspBandwidthPrio7
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                retValComponentLinkDescrMaxLspBandwidthPrio7
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkDescrMaxLspBandwidthPrio7 (INT4 i4IfIndex,
                                              UINT4 u4ComponentLinkDescrId,
                                              tSNMP_OCTET_STRING_TYPE
                                              *
                                              pRetValComponentLinkDescrMaxLspBandwidthPrio7)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    i1RetVal = TlmGetAllComponentLinkDescriptorTable (i4IfIndex,
                                                      u4ComponentLinkDescrId,
                                                      &TlmComponentIfDescr);
    FLOAT_TO_OCTETSTRING (TlmComponentIfDescr.aMaxLSPBwPrio[PRIORITY7],
                          pRetValComponentLinkDescrMaxLspBandwidthPrio7);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetComponentLinkDescrInterfaceMtu
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                retValComponentLinkDescrInterfaceMtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkDescrInterfaceMtu (INT4 i4IfIndex,
                                      UINT4 u4ComponentLinkDescrId,
                                      UINT4
                                      *pu4RetValComponentLinkDescrInterfaceMtu)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    i1RetVal = TlmGetAllComponentLinkDescriptorTable (i4IfIndex,
                                                      u4ComponentLinkDescrId,
                                                      &TlmComponentIfDescr);
    *pu4RetValComponentLinkDescrInterfaceMtu = TlmComponentIfDescr.u2MTU;
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetComponentLinkDescrIndication
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                retValComponentLinkDescrIndication
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkDescrIndication (INT4 i4IfIndex,
                                    UINT4 u4ComponentLinkDescrId,
                                    INT4 *pi4RetValComponentLinkDescrIndication)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    i1RetVal = TlmGetAllComponentLinkDescriptorTable (i4IfIndex,
                                                      u4ComponentLinkDescrId,
                                                      &TlmComponentIfDescr);
    *pi4RetValComponentLinkDescrIndication = TlmComponentIfDescr.i4Indication;
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetComponentLinkDescrRowStatus
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                retValComponentLinkDescrRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkDescrRowStatus (INT4 i4IfIndex,
                                   UINT4 u4ComponentLinkDescrId,
                                   INT4 *pi4RetValComponentLinkDescrRowStatus)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    i1RetVal = TlmGetAllComponentLinkDescriptorTable (i4IfIndex,
                                                      u4ComponentLinkDescrId,
                                                      &TlmComponentIfDescr);
    *pi4RetValComponentLinkDescrRowStatus = TlmComponentIfDescr.i4RowStatus;
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetComponentLinkDescrStorageType
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                retValComponentLinkDescrStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkDescrStorageType (INT4 i4IfIndex,
                                     UINT4 u4ComponentLinkDescrId,
                                     INT4
                                     *pi4RetValComponentLinkDescrStorageType)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    i1RetVal = TlmGetAllComponentLinkDescriptorTable (i4IfIndex,
                                                      u4ComponentLinkDescrId,
                                                      &TlmComponentIfDescr);

    *pi4RetValComponentLinkDescrStorageType = TlmComponentIfDescr.i4StorageType;
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetComponentLinkDescrSwitchingCapability
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                setValComponentLinkDescrSwitchingCapability
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetComponentLinkDescrSwitchingCapability (INT4 i4IfIndex,
                                             UINT4 u4ComponentLinkDescrId,
                                             INT4
                                             i4SetValComponentLinkDescrSwitchingCapability)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    TlmComponentIfDescr.i4SwitchingCap =
        i4SetValComponentLinkDescrSwitchingCapability;

    i1RetVal = TlmSetAllComponentLinkDescriptorTable (i4IfIndex,
                                                      u4ComponentLinkDescrId,
                                                      &TlmComponentIfDescr,
                                                      TLM_COMP_LINK_DESC_SWITCH_CAP);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetComponentLinkDescrEncodingType
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                setValComponentLinkDescrEncodingType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetComponentLinkDescrEncodingType (INT4 i4IfIndex,
                                      UINT4 u4ComponentLinkDescrId,
                                      INT4
                                      i4SetValComponentLinkDescrEncodingType)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    TlmComponentIfDescr.i4EncodingType = i4SetValComponentLinkDescrEncodingType;

    i1RetVal = TlmSetAllComponentLinkDescriptorTable (i4IfIndex,
                                                      u4ComponentLinkDescrId,
                                                      &TlmComponentIfDescr,
                                                      TLM_COMP_LINK_DESC_ENCODE_TYPE);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetComponentLinkDescrMinLspBandwidth
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                setValComponentLinkDescrMinLspBandwidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetComponentLinkDescrMinLspBandwidth (INT4 i4IfIndex,
                                         UINT4 u4ComponentLinkDescrId,
                                         tSNMP_OCTET_STRING_TYPE
                                         *
                                         pSetValComponentLinkDescrMinLspBandwidth)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    tBandWidth          minLSPBw = (tBandWidth) TLM_ZERO;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    minLSPBw =
        TLM_OCTETSTRING_TO_FLOAT (pSetValComponentLinkDescrMinLspBandwidth);

    TlmComponentIfDescr.minLSPBw = minLSPBw;

    i1RetVal = TlmSetAllComponentLinkDescriptorTable (i4IfIndex,
                                                      u4ComponentLinkDescrId,
                                                      &TlmComponentIfDescr,
                                                      TLM_COMP_LINK_DESC_MIN_LSP_BW);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetComponentLinkDescrMaxLspBandwidthPrio0
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                setValComponentLinkDescrMaxLspBandwidthPrio0
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetComponentLinkDescrMaxLspBandwidthPrio0 (INT4 i4IfIndex,
                                              UINT4 u4ComponentLinkDescrId,
                                              tSNMP_OCTET_STRING_TYPE
                                              *
                                              pSetValComponentLinkDescrMaxLspBandwidthPrio0)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4ComponentLinkDescrId);
    UNUSED_PARAM (pSetValComponentLinkDescrMaxLspBandwidthPrio0);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetComponentLinkDescrMaxLspBandwidthPrio1
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                setValComponentLinkDescrMaxLspBandwidthPrio1
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetComponentLinkDescrMaxLspBandwidthPrio1 (INT4 i4IfIndex,
                                              UINT4 u4ComponentLinkDescrId,
                                              tSNMP_OCTET_STRING_TYPE
                                              *
                                              pSetValComponentLinkDescrMaxLspBandwidthPrio1)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4ComponentLinkDescrId);
    UNUSED_PARAM (pSetValComponentLinkDescrMaxLspBandwidthPrio1);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetComponentLinkDescrMaxLspBandwidthPrio2
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                setValComponentLinkDescrMaxLspBandwidthPrio2
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetComponentLinkDescrMaxLspBandwidthPrio2 (INT4 i4IfIndex,
                                              UINT4 u4ComponentLinkDescrId,
                                              tSNMP_OCTET_STRING_TYPE
                                              *
                                              pSetValComponentLinkDescrMaxLspBandwidthPrio2)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4ComponentLinkDescrId);
    UNUSED_PARAM (pSetValComponentLinkDescrMaxLspBandwidthPrio2);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetComponentLinkDescrMaxLspBandwidthPrio3
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                setValComponentLinkDescrMaxLspBandwidthPrio3
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetComponentLinkDescrMaxLspBandwidthPrio3 (INT4 i4IfIndex,
                                              UINT4 u4ComponentLinkDescrId,
                                              tSNMP_OCTET_STRING_TYPE
                                              *
                                              pSetValComponentLinkDescrMaxLspBandwidthPrio3)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4ComponentLinkDescrId);
    UNUSED_PARAM (pSetValComponentLinkDescrMaxLspBandwidthPrio3);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetComponentLinkDescrMaxLspBandwidthPrio4
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                setValComponentLinkDescrMaxLspBandwidthPrio4
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetComponentLinkDescrMaxLspBandwidthPrio4 (INT4 i4IfIndex,
                                              UINT4 u4ComponentLinkDescrId,
                                              tSNMP_OCTET_STRING_TYPE
                                              *
                                              pSetValComponentLinkDescrMaxLspBandwidthPrio4)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4ComponentLinkDescrId);
    UNUSED_PARAM (pSetValComponentLinkDescrMaxLspBandwidthPrio4);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetComponentLinkDescrMaxLspBandwidthPrio5
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                setValComponentLinkDescrMaxLspBandwidthPrio5
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetComponentLinkDescrMaxLspBandwidthPrio5 (INT4 i4IfIndex,
                                              UINT4 u4ComponentLinkDescrId,
                                              tSNMP_OCTET_STRING_TYPE
                                              *
                                              pSetValComponentLinkDescrMaxLspBandwidthPrio5)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4ComponentLinkDescrId);
    UNUSED_PARAM (pSetValComponentLinkDescrMaxLspBandwidthPrio5);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetComponentLinkDescrMaxLspBandwidthPrio6
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                setValComponentLinkDescrMaxLspBandwidthPrio6
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetComponentLinkDescrMaxLspBandwidthPrio6 (INT4 i4IfIndex,
                                              UINT4 u4ComponentLinkDescrId,
                                              tSNMP_OCTET_STRING_TYPE
                                              *
                                              pSetValComponentLinkDescrMaxLspBandwidthPrio6)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4ComponentLinkDescrId);
    UNUSED_PARAM (pSetValComponentLinkDescrMaxLspBandwidthPrio6);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetComponentLinkDescrMaxLspBandwidthPrio7
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                setValComponentLinkDescrMaxLspBandwidthPrio7
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetComponentLinkDescrMaxLspBandwidthPrio7 (INT4 i4IfIndex,
                                              UINT4 u4ComponentLinkDescrId,
                                              tSNMP_OCTET_STRING_TYPE
                                              *
                                              pSetValComponentLinkDescrMaxLspBandwidthPrio7)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4ComponentLinkDescrId);
    UNUSED_PARAM (pSetValComponentLinkDescrMaxLspBandwidthPrio7);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetComponentLinkDescrInterfaceMtu
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                setValComponentLinkDescrInterfaceMtu
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetComponentLinkDescrInterfaceMtu (INT4 i4IfIndex,
                                      UINT4 u4ComponentLinkDescrId,
                                      UINT4
                                      u4SetValComponentLinkDescrInterfaceMtu)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    TlmComponentIfDescr.u2MTU = (UINT2) u4SetValComponentLinkDescrInterfaceMtu;

    i1RetVal = TlmSetAllComponentLinkDescriptorTable (i4IfIndex,
                                                      u4ComponentLinkDescrId,
                                                      &TlmComponentIfDescr,
                                                      TLM_COMP_LINK_DESC_INTF_MTU);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetComponentLinkDescrIndication
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                setValComponentLinkDescrIndication
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetComponentLinkDescrIndication (INT4 i4IfIndex,
                                    UINT4 u4ComponentLinkDescrId,
                                    INT4 i4SetValComponentLinkDescrIndication)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    TlmComponentIfDescr.i4Indication = i4SetValComponentLinkDescrIndication;

    i1RetVal = TlmSetAllComponentLinkDescriptorTable (i4IfIndex,
                                                      u4ComponentLinkDescrId,
                                                      &TlmComponentIfDescr,
                                                      TLM_COMP_LINK_DESC_INDICATION);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetComponentLinkDescrRowStatus
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                setValComponentLinkDescrRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetComponentLinkDescrRowStatus (INT4 i4IfIndex,
                                   UINT4 u4ComponentLinkDescrId,
                                   INT4 i4SetValComponentLinkDescrRowStatus)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    TlmComponentIfDescr.i4RowStatus = i4SetValComponentLinkDescrRowStatus;

    i1RetVal = TlmSetAllComponentLinkDescriptorTable (i4IfIndex,
                                                      u4ComponentLinkDescrId,
                                                      &TlmComponentIfDescr,
                                                      TLM_COMP_LINK_DESC_ROW_STATUS);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetComponentLinkDescrStorageType
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                setValComponentLinkDescrStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetComponentLinkDescrStorageType (INT4 i4IfIndex,
                                     UINT4 u4ComponentLinkDescrId,
                                     INT4 i4SetValComponentLinkDescrStorageType)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    TlmComponentIfDescr.i4StorageType = i4SetValComponentLinkDescrStorageType;

    i1RetVal = TlmSetAllComponentLinkDescriptorTable (i4IfIndex,
                                                      u4ComponentLinkDescrId,
                                                      &TlmComponentIfDescr,
                                                      TLM_COMP_LINK_DESC_STORAGE_TYPE);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2ComponentLinkDescrSwitchingCapability
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                testValComponentLinkDescrSwitchingCapability
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ComponentLinkDescrSwitchingCapability (UINT4 *pu4ErrorCode,
                                                INT4 i4IfIndex,
                                                UINT4 u4ComponentLinkDescrId,
                                                INT4
                                                i4TestValComponentLinkDescrSwitchingCapability)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    TlmComponentIfDescr.i4SwitchingCap =
        i4TestValComponentLinkDescrSwitchingCapability;

    i1RetVal = TlmTestAllComponentLinkDescriptorTable (pu4ErrorCode, i4IfIndex,
                                                       u4ComponentLinkDescrId,
                                                       &TlmComponentIfDescr,
                                                       TLM_COMP_LINK_DESC_SWITCH_CAP);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2ComponentLinkDescrEncodingType
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                testValComponentLinkDescrEncodingType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ComponentLinkDescrEncodingType (UINT4 *pu4ErrorCode,
                                         INT4 i4IfIndex,
                                         UINT4 u4ComponentLinkDescrId,
                                         INT4
                                         i4TestValComponentLinkDescrEncodingType)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    TlmComponentIfDescr.i4EncodingType =
        i4TestValComponentLinkDescrEncodingType;

    i1RetVal = TlmTestAllComponentLinkDescriptorTable (pu4ErrorCode, i4IfIndex,
                                                       u4ComponentLinkDescrId,
                                                       &TlmComponentIfDescr,
                                                       TLM_COMP_LINK_DESC_ENCODE_TYPE);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2ComponentLinkDescrMinLspBandwidth
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                testValComponentLinkDescrMinLspBandwidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ComponentLinkDescrMinLspBandwidth (UINT4 *pu4ErrorCode,
                                            INT4 i4IfIndex,
                                            UINT4 u4ComponentLinkDescrId,
                                            tSNMP_OCTET_STRING_TYPE
                                            *
                                            pTestValComponentLinkDescrMinLspBandwidth)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    TlmComponentIfDescr.minLSPBw =
        TLM_OCTETSTRING_TO_FLOAT (pTestValComponentLinkDescrMinLspBandwidth);

    i1RetVal = TlmTestAllComponentLinkDescriptorTable (pu4ErrorCode, i4IfIndex,
                                                       u4ComponentLinkDescrId,
                                                       &TlmComponentIfDescr,
                                                       TLM_COMP_LINK_DESC_MIN_LSP_BW);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio0
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                testValComponentLinkDescrMaxLspBandwidthPrio0
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio0 (UINT4 *pu4ErrorCode,
                                                 INT4 i4IfIndex,
                                                 UINT4 u4ComponentLinkDescrId,
                                                 tSNMP_OCTET_STRING_TYPE
                                                 *
                                                 pTestValComponentLinkDescrMaxLspBandwidthPrio0)
{
    *pu4ErrorCode = SNMP_ERR_READ_ONLY;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4ComponentLinkDescrId);
    UNUSED_PARAM (pTestValComponentLinkDescrMaxLspBandwidthPrio0);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio1
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                testValComponentLinkDescrMaxLspBandwidthPrio1
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio1 (UINT4 *pu4ErrorCode,
                                                 INT4 i4IfIndex,
                                                 UINT4 u4ComponentLinkDescrId,
                                                 tSNMP_OCTET_STRING_TYPE
                                                 *
                                                 pTestValComponentLinkDescrMaxLspBandwidthPrio1)
{
    *pu4ErrorCode = SNMP_ERR_READ_ONLY;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4ComponentLinkDescrId);
    UNUSED_PARAM (pTestValComponentLinkDescrMaxLspBandwidthPrio1);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio2
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                testValComponentLinkDescrMaxLspBandwidthPrio2
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio2 (UINT4 *pu4ErrorCode,
                                                 INT4 i4IfIndex,
                                                 UINT4 u4ComponentLinkDescrId,
                                                 tSNMP_OCTET_STRING_TYPE
                                                 *
                                                 pTestValComponentLinkDescrMaxLspBandwidthPrio2)
{
    *pu4ErrorCode = SNMP_ERR_READ_ONLY;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4ComponentLinkDescrId);
    UNUSED_PARAM (pTestValComponentLinkDescrMaxLspBandwidthPrio2);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio3
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                testValComponentLinkDescrMaxLspBandwidthPrio3
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio3 (UINT4 *pu4ErrorCode,
                                                 INT4 i4IfIndex,
                                                 UINT4 u4ComponentLinkDescrId,
                                                 tSNMP_OCTET_STRING_TYPE
                                                 *
                                                 pTestValComponentLinkDescrMaxLspBandwidthPrio3)
{
    *pu4ErrorCode = SNMP_ERR_READ_ONLY;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4ComponentLinkDescrId);
    UNUSED_PARAM (pTestValComponentLinkDescrMaxLspBandwidthPrio3);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio4
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                testValComponentLinkDescrMaxLspBandwidthPrio4
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio4 (UINT4 *pu4ErrorCode,
                                                 INT4 i4IfIndex,
                                                 UINT4 u4ComponentLinkDescrId,
                                                 tSNMP_OCTET_STRING_TYPE
                                                 *
                                                 pTestValComponentLinkDescrMaxLspBandwidthPrio4)
{
    *pu4ErrorCode = SNMP_ERR_READ_ONLY;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4ComponentLinkDescrId);
    UNUSED_PARAM (pTestValComponentLinkDescrMaxLspBandwidthPrio4);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio5
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                testValComponentLinkDescrMaxLspBandwidthPrio5
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio5 (UINT4 *pu4ErrorCode,
                                                 INT4 i4IfIndex,
                                                 UINT4 u4ComponentLinkDescrId,
                                                 tSNMP_OCTET_STRING_TYPE
                                                 *
                                                 pTestValComponentLinkDescrMaxLspBandwidthPrio5)
{
    *pu4ErrorCode = SNMP_ERR_READ_ONLY;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4ComponentLinkDescrId);
    UNUSED_PARAM (pTestValComponentLinkDescrMaxLspBandwidthPrio5);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio6
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                testValComponentLinkDescrMaxLspBandwidthPrio6
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio6 (UINT4 *pu4ErrorCode,
                                                 INT4 i4IfIndex,
                                                 UINT4 u4ComponentLinkDescrId,
                                                 tSNMP_OCTET_STRING_TYPE
                                                 *
                                                 pTestValComponentLinkDescrMaxLspBandwidthPrio6)
{
    *pu4ErrorCode = SNMP_ERR_READ_ONLY;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4ComponentLinkDescrId);
    UNUSED_PARAM (pTestValComponentLinkDescrMaxLspBandwidthPrio6);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio7
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                testValComponentLinkDescrMaxLspBandwidthPrio7
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ComponentLinkDescrMaxLspBandwidthPrio7 (UINT4 *pu4ErrorCode,
                                                 INT4 i4IfIndex,
                                                 UINT4 u4ComponentLinkDescrId,
                                                 tSNMP_OCTET_STRING_TYPE
                                                 *
                                                 pTestValComponentLinkDescrMaxLspBandwidthPrio7)
{
    *pu4ErrorCode = SNMP_ERR_READ_ONLY;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4ComponentLinkDescrId);
    UNUSED_PARAM (pTestValComponentLinkDescrMaxLspBandwidthPrio7);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2ComponentLinkDescrInterfaceMtu
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                testValComponentLinkDescrInterfaceMtu
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ComponentLinkDescrInterfaceMtu (UINT4 *pu4ErrorCode,
                                         INT4 i4IfIndex,
                                         UINT4 u4ComponentLinkDescrId,
                                         UINT4
                                         u4TestValComponentLinkDescrInterfaceMtu)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    TlmComponentIfDescr.u2MTU = (UINT2) u4TestValComponentLinkDescrInterfaceMtu;

    i1RetVal = TlmTestAllComponentLinkDescriptorTable (pu4ErrorCode,
                                                       i4IfIndex,
                                                       u4ComponentLinkDescrId,
                                                       &TlmComponentIfDescr,
                                                       TLM_COMP_LINK_DESC_INTF_MTU);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2ComponentLinkDescrIndication
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                testValComponentLinkDescrIndication
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ComponentLinkDescrIndication (UINT4 *pu4ErrorCode,
                                       INT4 i4IfIndex,
                                       UINT4 u4ComponentLinkDescrId,
                                       INT4
                                       i4TestValComponentLinkDescrIndication)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    TlmComponentIfDescr.i4Indication = i4TestValComponentLinkDescrIndication;

    i1RetVal = TlmTestAllComponentLinkDescriptorTable (pu4ErrorCode, i4IfIndex,
                                                       u4ComponentLinkDescrId,
                                                       &TlmComponentIfDescr,
                                                       TLM_COMP_LINK_DESC_INDICATION);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2ComponentLinkDescrRowStatus
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                testValComponentLinkDescrRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ComponentLinkDescrRowStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4IfIndex,
                                      UINT4 u4ComponentLinkDescrId,
                                      INT4 i4TestValComponentLinkDescrRowStatus)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    if (TLM_MODULE_STATUS == TLM_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }
    TlmComponentIfDescr.i4RowStatus = i4TestValComponentLinkDescrRowStatus;

    i1RetVal = TlmTestAllComponentLinkDescriptorTable (pu4ErrorCode, i4IfIndex,
                                                       u4ComponentLinkDescrId,
                                                       &TlmComponentIfDescr,
                                                       TLM_COMP_LINK_DESC_ROW_STATUS);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2ComponentLinkDescrStorageType
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId

                The Object 
                testValComponentLinkDescrStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ComponentLinkDescrStorageType (UINT4 *pu4ErrorCode,
                                        INT4 i4IfIndex,
                                        UINT4 u4ComponentLinkDescrId,
                                        INT4
                                        i4TestValComponentLinkDescrStorageType)
{
    tTlmComponentIfDescr TlmComponentIfDescr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentIfDescr, TLM_ZERO, sizeof (tTlmComponentIfDescr));

    TlmComponentIfDescr.i4StorageType = i4TestValComponentLinkDescrStorageType;

    i1RetVal = TlmTestAllComponentLinkDescriptorTable (pu4ErrorCode, i4IfIndex,
                                                       u4ComponentLinkDescrId,
                                                       &TlmComponentIfDescr,
                                                       TLM_COMP_LINK_DESC_STORAGE_TYPE);
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2ComponentLinkDescriptorTable
 Input       :  The Indices
                IfIndex
                ComponentLinkDescrId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2ComponentLinkDescriptorTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : ComponentLinkBandwidthTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceComponentLinkBandwidthTable
 Input       :  The Indices
                IfIndex
                ComponentLinkBandwidthPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceComponentLinkBandwidthTable (INT4 i4IfIndex,
                                                     UINT4
                                                     u4ComponentLinkBandwidthPriority)
{
    if ((i4IfIndex < TLM_ONE) || (i4IfIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES))
    {
        return SNMP_FAILURE;
    }

    if (u4ComponentLinkBandwidthPriority > TLM_MAX_PRIORITY_LVL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexComponentLinkBandwidthTable
 Input       :  The Indices
                IfIndex
                ComponentLinkBandwidthPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexComponentLinkBandwidthTable (INT4 *pi4IfIndex,
                                             UINT4
                                             *pu4ComponentLinkBandwidthPriority)
{
    tTlmComponentLink  *pComponentIface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT1               u1Priority = TLM_ZERO;

    TMO_SLL_Scan (&(gTlmGlobalInfo.sortComponentIfLst), pLstNode,
                  tTMO_SLL_NODE *)
    {
        pComponentIface = GET_COMPONENT_IF_PTR_FROM_SORT_LST (pLstNode);
        for (u1Priority = TLM_ZERO; u1Priority < TLM_MAX_PRIORITY_LVL;
             u1Priority++)
        {
            if (pComponentIface->aUnResBw[u1Priority].i4RowStatus !=
                TLM_ROWSTATUS_INVALID)
            {
                *pi4IfIndex = pComponentIface->i4IfIndex;
                *pu4ComponentLinkBandwidthPriority = u1Priority;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexComponentLinkBandwidthTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                ComponentLinkBandwidthPriority
                nextComponentLinkBandwidthPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexComponentLinkBandwidthTable (INT4 i4IfIndex,
                                            INT4 *pi4NextIfIndex,
                                            UINT4
                                            u4ComponentLinkBandwidthPriority,
                                            UINT4
                                            *pu4NextComponentLinkBandwidthPriority)
{
    tTlmComponentLink  *pComponentIface = NULL;
    tTMO_SLL_NODE      *pIfNode = NULL;
    UINT1               u1Priority = TLM_ZERO;

    TMO_SLL_Scan (&(gTlmGlobalInfo.sortComponentIfLst), pIfNode,
                  tTMO_SLL_NODE *)
    {
        pComponentIface = GET_COMPONENT_IF_PTR_FROM_SORT_LST (pIfNode);
        if (pComponentIface->i4IfIndex == i4IfIndex)
        {
            for (u1Priority =
                 (UINT1) (u4ComponentLinkBandwidthPriority + TLM_ONE);
                 u1Priority < TLM_MAX_PRIORITY_LVL; u1Priority++)
            {
                if (pComponentIface->aUnResBw[u1Priority].i4RowStatus !=
                    TLM_ROWSTATUS_INVALID)
                {
                    *pi4NextIfIndex = pComponentIface->i4IfIndex;
                    *pu4NextComponentLinkBandwidthPriority = u1Priority;
                    return SNMP_SUCCESS;
                }
            }
        }
        else if (pComponentIface->i4IfIndex > i4IfIndex)
        {
            for (u1Priority = TLM_ZERO; u1Priority < TLM_MAX_PRIORITY_LVL;
                 u1Priority++)
            {
                if (pComponentIface->aUnResBw[u1Priority].i4RowStatus !=
                    TLM_ROWSTATUS_INVALID)
                {
                    *pi4NextIfIndex = pComponentIface->i4IfIndex;
                    *pu4NextComponentLinkBandwidthPriority = u1Priority;
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetComponentLinkBandwidthUnreserved
 Input       :  The Indices
                IfIndex
                ComponentLinkBandwidthPriority

                The Object 
                retValComponentLinkBandwidthUnreserved
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkBandwidthUnreserved (INT4 i4IfIndex,
                                        UINT4 u4ComponentLinkBandwidthPriority,
                                        tSNMP_OCTET_STRING_TYPE
                                        *
                                        pRetValComponentLinkBandwidthUnreserved)
{
    tTlmComponentLink   TlmComponentLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentLink, TLM_ZERO, sizeof (tTlmComponentLink));

    i1RetVal = TlmGetAllComponentLinkBandwidthTable (i4IfIndex,
                                                     u4ComponentLinkBandwidthPriority,
                                                     &TlmComponentLink);
    FLOAT_TO_OCTETSTRING (TlmComponentLink.
                          aUnResBw[u4ComponentLinkBandwidthPriority].unResBw,
                          pRetValComponentLinkBandwidthUnreserved);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetComponentLinkBandwidthRowStatus
 Input       :  The Indices
                IfIndex
                ComponentLinkBandwidthPriority

                The Object 
                retValComponentLinkBandwidthRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkBandwidthRowStatus (INT4 i4IfIndex,
                                       UINT4 u4ComponentLinkBandwidthPriority,
                                       INT4
                                       *pi4RetValComponentLinkBandwidthRowStatus)
{
    tTlmComponentLink   TlmComponentLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentLink, TLM_ZERO, sizeof (tTlmComponentLink));

    i1RetVal = TlmGetAllComponentLinkBandwidthTable (i4IfIndex,
                                                     u4ComponentLinkBandwidthPriority,
                                                     &TlmComponentLink);
    *pi4RetValComponentLinkBandwidthRowStatus = TlmComponentLink.i4RowStatus;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetComponentLinkBandwidthStorageType
 Input       :  The Indices
                IfIndex
                ComponentLinkBandwidthPriority

                The Object 
                retValComponentLinkBandwidthStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetComponentLinkBandwidthStorageType (INT4 i4IfIndex,
                                         UINT4 u4ComponentLinkBandwidthPriority,
                                         INT4
                                         *pi4RetValComponentLinkBandwidthStorageType)
{
    tTlmComponentLink   TlmComponentLink;

    INT1                i1RetVal = SNMP_FAILURE;
    MEMSET (&TlmComponentLink, TLM_ZERO, sizeof (tTlmComponentLink));

    i1RetVal = TlmGetAllComponentLinkBandwidthTable (i4IfIndex,
                                                     u4ComponentLinkBandwidthPriority,
                                                     &TlmComponentLink);
    *pi4RetValComponentLinkBandwidthStorageType =
        TlmComponentLink.i4StorageType;

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetComponentLinkBandwidthRowStatus
 Input       :  The Indices
                IfIndex
                ComponentLinkBandwidthPriority

                The Object 
                setValComponentLinkBandwidthRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetComponentLinkBandwidthRowStatus (INT4 i4IfIndex,
                                       UINT4 u4ComponentLinkBandwidthPriority,
                                       INT4
                                       i4SetValComponentLinkBandwidthRowStatus)
{
    tTlmComponentLink   TlmCompLink;
    UINT4               u4BwPriority = u4ComponentLinkBandwidthPriority;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmCompLink, TLM_ZERO, sizeof (tTlmComponentLink));

    TlmCompLink.aUnResBw[u4BwPriority].i4RowStatus =
        i4SetValComponentLinkBandwidthRowStatus;

    i1RetVal = TlmSetAllComponentLinkBandwidthTable (i4IfIndex,
                                                     u4ComponentLinkBandwidthPriority,
                                                     &TlmCompLink,
                                                     TLM_COMP_LINK_BW_ROW_STATUS);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetComponentLinkBandwidthStorageType
 Input       :  The Indices
                IfIndex
                ComponentLinkBandwidthPriority

                The Object 
                setValComponentLinkBandwidthStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetComponentLinkBandwidthStorageType (INT4 i4IfIndex,
                                         UINT4 u4ComponentLinkBandwidthPriority,
                                         INT4
                                         i4SetValComponentLinkBandwidthStorageType)
{
    tTlmComponentLink   TlmCompLink;
    UINT4               u4BwPriority = u4ComponentLinkBandwidthPriority;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmCompLink, TLM_ZERO, sizeof (tTlmComponentLink));

    TlmCompLink.aUnResBw[u4BwPriority].i4StorageType =
        i4SetValComponentLinkBandwidthStorageType;

    i1RetVal = TlmSetAllComponentLinkBandwidthTable (i4IfIndex,
                                                     u4ComponentLinkBandwidthPriority,
                                                     &TlmCompLink,
                                                     TLM_COMP_LINK_BW_STORAGE_TYPE);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2ComponentLinkBandwidthRowStatus
 Input       :  The Indices
                IfIndex
                ComponentLinkBandwidthPriority

                The Object 
                testValComponentLinkBandwidthRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ComponentLinkBandwidthRowStatus (UINT4 *pu4ErrorCode,
                                          INT4 i4IfIndex,
                                          UINT4
                                          u4ComponentLinkBandwidthPriority,
                                          INT4
                                          i4TestValComponentLinkBandwidthRowStatus)
{
    tTlmComponentLink   TlmComponentLink;
    UINT4               u4BwPriority = u4ComponentLinkBandwidthPriority;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentLink, TLM_ZERO, sizeof (tTlmComponentLink));

    if (TLM_MODULE_STATUS == TLM_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1RetVal;
    }
    TlmComponentLink.aUnResBw[u4BwPriority].i4RowStatus =
        i4TestValComponentLinkBandwidthRowStatus;

    i1RetVal = TlmTestAllComponentLinkBandwidthTable (pu4ErrorCode, i4IfIndex,
                                                      u4ComponentLinkBandwidthPriority,
                                                      &TlmComponentLink,
                                                      TLM_COMP_LINK_BW_ROW_STATUS);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2ComponentLinkBandwidthStorageType
 Input       :  The Indices
                IfIndex
                ComponentLinkBandwidthPriority

                The Object 
                testValComponentLinkBandwidthStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2ComponentLinkBandwidthStorageType (UINT4 *pu4ErrorCode,
                                            INT4 i4IfIndex,
                                            UINT4
                                            u4ComponentLinkBandwidthPriority,
                                            INT4
                                            i4TestValComponentLinkBandwidthStorageType)
{
    tTlmComponentLink   TlmComponentLink;
    UINT4               u4BwPriority = u4ComponentLinkBandwidthPriority;

    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmComponentLink, TLM_ZERO, sizeof (tTlmComponentLink));

    TlmComponentLink.aUnResBw[u4BwPriority].i4StorageType =
        i4TestValComponentLinkBandwidthStorageType;

    i1RetVal = TlmTestAllComponentLinkBandwidthTable (pu4ErrorCode, i4IfIndex,
                                                      u4ComponentLinkBandwidthPriority,
                                                      &TlmComponentLink,
                                                      TLM_COMP_LINK_BW_STORAGE_TYPE);
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2ComponentLinkBandwidthTable
 Input       :  The Indices
                IfIndex
                ComponentLinkBandwidthPriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2ComponentLinkBandwidthTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
