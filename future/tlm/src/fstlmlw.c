/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstlmlw.c,v 1.10 2014/07/09 13:23:18 siva Exp $
*
* Description: Protocol Low Level Routines for Proprietary TLM MIB
*********************************************************************/
#include "tlminc.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTeLinkTraceOption
 Input       :  The Indices

                The Object 
                retValFsTeLinkTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkTraceOption (INT4 *pi4RetValFsTeLinkTraceOption)
{
    *pi4RetValFsTeLinkTraceOption = TLM_CUR_TRACE_OPTION;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkModuleStatus
 Input       :  The Indices

                The Object 
                retValFsTeLinkModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkModuleStatus (INT4 *pi4RetValFsTeLinkModuleStatus)
{
    *pi4RetValFsTeLinkModuleStatus = TLM_MODULE_STATUS;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsTeLinkTraceOption
 Input       :  The Indices

                The Object 
                setValFsTeLinkTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkTraceOption (INT4 i4SetValFsTeLinkTraceOption)
{
    if (TLM_CUR_TRACE_OPTION == i4SetValFsTeLinkTraceOption)
    {
        return SNMP_SUCCESS;
    }

    TLM_CUR_TRACE_OPTION = i4SetValFsTeLinkTraceOption;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsTeLinkModuleStatus
 Input       :  The Indices

                The Object 
                setValFsTeLinkModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkModuleStatus (INT4 i4SetValFsTeLinkModuleStatus)
{
    tTlmEventNotification NotifMsg;
    INT4                i4RetVal = OSIX_FAILURE;

    MEMSET (&NotifMsg, TLM_ZERO, sizeof (tTlmEventNotification));

    if (TLM_MODULE_STATUS == i4SetValFsTeLinkModuleStatus)
    {
        return SNMP_SUCCESS;
    }

    TLM_MODULE_STATUS = i4SetValFsTeLinkModuleStatus;

    if (i4SetValFsTeLinkModuleStatus == TLM_ENABLED)
    {
        i4RetVal = TlmPortCfaRegLL ();
    }
    else
    {
        i4RetVal = TlmPortCfaDeRegisterLL (CFA_TELINK);
    }

    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    NotifMsg.u1MsgSubType = (UINT1) TLM_MODULE_STATUS;
    NotifMsg.u4EventType = (UINT4) TLM_MODULE_STATUS;

    TlmUtilNotifyProtocols (&NotifMsg);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkTraceOption
 Input       :  The Indices

                The Object 
                testValFsTeLinkTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkTraceOption (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsTeLinkTraceOption)
{
    if (i4TestValFsTeLinkTraceOption > TLM_MAX_TRC_VAL)
    {
        TLM_TRC (TLM_ALL_TRC | TLM_FAILURE_TRC | TLM_MGMT_TRC,
                 "nmhTestv2FsTeLinkTraceOption: Invalid Trace Option\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkModuleStatus
 Input       :  The Indices

                The Object 
                testValFsTeLinkModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkModuleStatus (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsTeLinkModuleStatus)
{
    if ((i4TestValFsTeLinkModuleStatus != TLM_ENABLED) &&
        (i4TestValFsTeLinkModuleStatus != TLM_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsTeLinkTraceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTeLinkTraceOption (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsTeLinkModuleStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTeLinkModuleStatus (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsTeLinkTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsTeLinkTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsTeLinkTable (INT4 i4IfIndex)
{
    return (nmhValidateIndexInstanceTeLinkTable (i4IfIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsTeLinkTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsTeLinkTable (INT4 *pi4IfIndex)
{
    return (nmhGetFirstIndexTeLinkTable (pi4IfIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsTeLinkTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsTeLinkTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    return (nmhGetNextIndexTeLinkTable (i4IfIndex, pi4NextIfIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTeLinkName
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsTeLinkName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkName (INT4 i4IfIndex,
                    tSNMP_OCTET_STRING_TYPE * pRetValFsTeLinkName)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkTable (i4IfIndex, &TlmTeLink);

    MEMCPY (pRetValFsTeLinkName->pu1_OctetList, TlmTeLink.au1TeLinkName,
            TLM_STRLEN (TlmTeLink.au1TeLinkName));
    pRetValFsTeLinkName->i4_Length =
        (INT4) TLM_STRLEN (TlmTeLink.au1TeLinkName);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkRemoteRtrId
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsTeLinkRemoteRtrId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkRemoteRtrId (INT4 i4IfIndex, UINT4 *pu4RetValFsTeLinkRemoteRtrId)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkTable (i4IfIndex, &TlmTeLink);

    *pu4RetValFsTeLinkRemoteRtrId = TlmTeLink.u4RemoteRtrId;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkMaximumBandwidth
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsTeLinkMaximumBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkMaximumBandwidth (INT4 i4IfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsTeLinkMaximumBandwidth)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkTable (i4IfIndex, &TlmTeLink);

    FLOAT_TO_OCTETSTRING (TlmTeLink.maxBw, pRetValFsTeLinkMaximumBandwidth);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkType
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsTeLinkType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkType (INT4 i4IfIndex, INT4 *pi4RetValFsTeLinkType)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkTable (i4IfIndex, &TlmTeLink);

    *pi4RetValFsTeLinkType = TlmTeLink.i4TeLinkType;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkInfoType
 Input       :  The Indices
                IfIndex

                The Object
                retValFsTeLinkInfoType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkInfoType (INT4 i4IfIndex, INT4 *pi4RetValFsTeLinkInfoType)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkTable (i4IfIndex, &TlmTeLink);

    *pi4RetValFsTeLinkInfoType = TlmTeLink.u1InfoType;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkIfType
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsTeLinkIfType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkIfType (INT4 i4IfIndex, INT4 *pi4RetValFsTeLinkIfType)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkTable (i4IfIndex, &TlmTeLink);

    *pi4RetValFsTeLinkIfType = TlmTeLink.i4TeLinkIfType;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkIsAdvertise
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsTeLinkIsAdvertise
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkIsAdvertise (INT4 i4IfIndex, INT4 *pi4RetValFsTeLinkIsAdvertise)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkTable (i4IfIndex, &TlmTeLink);

    *pi4RetValFsTeLinkIsAdvertise = TlmTeLink.i4IsTeLinkAdvertise;
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsTeLinkName
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsTeLinkName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkName (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE
                    * pSetValFsTeLinkName)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    MEMCPY (TlmTeLink.au1TeLinkName, pSetValFsTeLinkName->pu1_OctetList,
            pSetValFsTeLinkName->i4_Length);

    TlmTeLink.u1TeLinkNameLen = (UINT1) pSetValFsTeLinkName->i4_Length;

    i1RetVal = TlmSetAllTeLinkTable (i4IfIndex, &TlmTeLink,
                                     TLM_TE_LINK_NAME_OBJ);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTeLinkRemoteRtrId
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsTeLinkRemoteRtrId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkRemoteRtrId (INT4 i4IfIndex, UINT4 u4SetValFsTeLinkRemoteRtrId)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));
    TlmTeLink.u4RemoteRtrId = u4SetValFsTeLinkRemoteRtrId;

    i1RetVal = TlmSetAllTeLinkTable (i4IfIndex,
                                     &TlmTeLink, TLM_TE_LINK_REMOTE_RTRID);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTeLinkType
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsTeLinkType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkType (INT4 i4IfIndex, INT4 i4SetValFsTeLinkType)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.i4TeLinkType = i4SetValFsTeLinkType;

    i1RetVal = TlmSetAllTeLinkTable (i4IfIndex, &TlmTeLink, TLM_TE_LINK_TYPE);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTeLinkInfoType
 Input       :  The Indices
                IfIndex

                The Object
                setValFsTeLinkInfoType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkInfoType (INT4 i4IfIndex, INT4 i4SetValFsTeLinkInfoType)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.u1InfoType = (UINT1) i4SetValFsTeLinkInfoType;

    i1RetVal = TlmSetAllTeLinkTable (i4IfIndex, &TlmTeLink,
                                     TLM_TE_LINK_INFO_TYPE);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTeLinkIfType
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsTeLinkIfType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkIfType (INT4 i4IfIndex, INT4 i4SetValFsTeLinkIfType)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.i4TeLinkIfType = i4SetValFsTeLinkIfType;

    i1RetVal = TlmSetAllTeLinkTable (i4IfIndex, &TlmTeLink,
                                     TLM_TE_LINK_IF_TYPE);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTeLinkIsAdvertise
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsTeLinkIsAdvertise
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkIsAdvertise (INT4 i4IfIndex, INT4 i4SetValFsTeLinkIsAdvertise)
{

    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.i4IsTeLinkAdvertise = i4SetValFsTeLinkIsAdvertise;

    i1RetVal = TlmSetAllTeLinkTable (i4IfIndex, &TlmTeLink,
                                     TLM_TE_LINK_ADVERTISE);

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkName
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsTeLinkName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkName (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                       tSNMP_OCTET_STRING_TYPE * pTestValFsTeLinkName)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    MEMCPY (TlmTeLink.au1TeLinkName, pTestValFsTeLinkName->pu1_OctetList,
            pTestValFsTeLinkName->i4_Length);

    i1RetVal = TlmTestAllTeLinkTable (pu4ErrorCode, i4IfIndex, &TlmTeLink,
                                      TLM_TE_LINK_NAME_OBJ);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkRemoteRtrId
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsTeLinkRemoteRtrId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkRemoteRtrId (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                              UINT4 u4TestValFsTeLinkRemoteRtrId)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.u4RemoteRtrId = u4TestValFsTeLinkRemoteRtrId;

    i1RetVal = TlmTestAllTeLinkTable (pu4ErrorCode, i4IfIndex,
                                      &TlmTeLink, TLM_TE_LINK_REMOTE_RTRID);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkType
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsTeLinkType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkType (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                       INT4 i4TestValFsTeLinkType)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.i4TeLinkType = (UINT1) i4TestValFsTeLinkType;

    i1RetVal = TlmTestAllTeLinkTable (pu4ErrorCode, i4IfIndex,
                                      &TlmTeLink, TLM_TE_LINK_TYPE);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkInfoType
 Input       :  The Indices
                IfIndex

                The Object
                testValFsTeLinkInfoType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkInfoType (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                           INT4 i4TestValFsTeLinkInfoType)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.u1InfoType = (UINT1) i4TestValFsTeLinkInfoType;

    i1RetVal = TlmTestAllTeLinkTable (pu4ErrorCode, i4IfIndex,
                                      &TlmTeLink, TLM_TE_LINK_INFO_TYPE);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkIfType
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsTeLinkIfType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkIfType (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                         INT4 i4TestValFsTeLinkIfType)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.i4TeLinkIfType = i4TestValFsTeLinkIfType;

    i1RetVal = TlmTestAllTeLinkTable (pu4ErrorCode, i4IfIndex,
                                      &TlmTeLink, TLM_TE_LINK_IF_TYPE);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkIsAdvertise
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsTeLinkIsAdvertise
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkIsAdvertise (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                              INT4 i4TestValFsTeLinkIsAdvertise)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, 0, sizeof (TlmTeLink));

    TlmTeLink.i4IsTeLinkAdvertise = i4TestValFsTeLinkIsAdvertise;

    i1RetVal = TlmTestAllTeLinkTable (pu4ErrorCode, i4IfIndex,
                                      &TlmTeLink, TLM_TE_LINK_ADVERTISE);
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsTeLinkTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTeLinkTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsTeLinkBwThresholdTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsTeLinkBwThresholdTable
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsTeLinkBwThresholdTable (INT4 i4IfIndex,
                                                  INT4
                                                  i4FsTeLinkBwThresholdIndex)
{
    if ((i4IfIndex < CFA_MIN_TELINK_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_TELINK_IF_INDEX))
    {
        return SNMP_FAILURE;
    }

    if ((i4FsTeLinkBwThresholdIndex < TLM_ONE) ||
        (i4FsTeLinkBwThresholdIndex > TLM_TWO))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsTeLinkBwThresholdTable
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsTeLinkBwThresholdTable (INT4 *pi4IfIndex,
                                          INT4 *pi4FsTeLinkBwThresholdIndex)
{
    tTlmTeLink         *pTeIface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    if ((pLstNode = TMO_SLL_First (&(gTlmGlobalInfo.sortTeIfLst))) != NULL)
    {
        pTeIface = GET_TE_IF_PTR_FROM_SORT_LST (pLstNode);

        *pi4IfIndex = pTeIface->i4IfIndex;

        *pi4FsTeLinkBwThresholdIndex = TLM_ONE;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsTeLinkBwThresholdTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                FsTeLinkBwThresholdIndex
                nextFsTeLinkBwThresholdIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsTeLinkBwThresholdTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                         INT4 i4FsTeLinkBwThresholdIndex,
                                         INT4 *pi4NextFsTeLinkBwThresholdIndex)
{
    tTlmTeLink         *pTeIface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    TMO_SLL_Scan (&(gTlmGlobalInfo.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pTeIface = GET_TE_IF_PTR_FROM_SORT_LST (pLstNode);

        if (i4FsTeLinkBwThresholdIndex == TLM_ONE)
        {
            *pi4NextFsTeLinkBwThresholdIndex = TLM_TWO;
            return SNMP_SUCCESS;
        }
        else if (i4FsTeLinkBwThresholdIndex == TLM_TWO)
        {
            *pi4NextFsTeLinkBwThresholdIndex = TLM_ONE;

            if (pTeIface->i4IfIndex > i4IfIndex)
            {
                *pi4NextIfIndex = pTeIface->i4IfIndex;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTeLinkBwThreshold0
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                retValFsTeLinkBwThreshold0
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkBwThreshold0 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                            INT4 *pi4RetValFsTeLinkBwThreshold0)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_ZERO;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink);
    TlmGetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, pi4RetValFsTeLinkBwThreshold0);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkBwThreshold1
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                retValFsTeLinkBwThreshold1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkBwThreshold1 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                            INT4 *pi4RetValFsTeLinkBwThreshold1)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_ONE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    i1RetVal =
        TlmGetAllTeLinkBwThresholdTable (i4IfIndex, i4FsTeLinkBwThresholdIndex,
                                         &TlmTeLink);
    TlmGetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, pi4RetValFsTeLinkBwThreshold1);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkBwThreshold2
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                retValFsTeLinkBwThreshold2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkBwThreshold2 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                            INT4 *pi4RetValFsTeLinkBwThreshold2)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_TWO;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink);
    TlmGetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, pi4RetValFsTeLinkBwThreshold2);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkBwThreshold3
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                retValFsTeLinkBwThreshold3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkBwThreshold3 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                            INT4 *pi4RetValFsTeLinkBwThreshold3)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_THREE;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink);
    TlmGetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, pi4RetValFsTeLinkBwThreshold3);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkBwThreshold4
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                retValFsTeLinkBwThreshold4
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkBwThreshold4 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                            INT4 *pi4RetValFsTeLinkBwThreshold4)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_FOUR;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink);
    TlmGetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, pi4RetValFsTeLinkBwThreshold4);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkBwThreshold5
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                retValFsTeLinkBwThreshold5
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkBwThreshold5 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                            INT4 *pi4RetValFsTeLinkBwThreshold5)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_FIVE;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink);

    TlmGetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, pi4RetValFsTeLinkBwThreshold5);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkBwThreshold6
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                retValFsTeLinkBwThreshold6
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkBwThreshold6 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                            INT4 *pi4RetValFsTeLinkBwThreshold6)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_SIX;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink);
    TlmGetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, pi4RetValFsTeLinkBwThreshold6);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkBwThreshold7
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                retValFsTeLinkBwThreshold7
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkBwThreshold7 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                            INT4 *pi4RetValFsTeLinkBwThreshold7)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_SEVEN;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink);
    TlmGetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, pi4RetValFsTeLinkBwThreshold7);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkBwThreshold8
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                retValFsTeLinkBwThreshold8
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkBwThreshold8 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                            INT4 *pi4RetValFsTeLinkBwThreshold8)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_EIGHT;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink);
    TlmGetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, pi4RetValFsTeLinkBwThreshold8);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkBwThreshold9
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                retValFsTeLinkBwThreshold9
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkBwThreshold9 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                            INT4 *pi4RetValFsTeLinkBwThreshold9)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_NINE;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink);
    TlmGetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, pi4RetValFsTeLinkBwThreshold9);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkBwThreshold10
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                retValFsTeLinkBwThreshold10
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkBwThreshold10 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                             INT4 *pi4RetValFsTeLinkBwThreshold10)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_TEN;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink);
    TlmGetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, pi4RetValFsTeLinkBwThreshold10);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkBwThreshold11
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                retValFsTeLinkBwThreshold11
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkBwThreshold11 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                             INT4 *pi4RetValFsTeLinkBwThreshold11)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_ELEVEN;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink);
    TlmGetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, pi4RetValFsTeLinkBwThreshold11);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkBwThreshold12
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                retValFsTeLinkBwThreshold12
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkBwThreshold12 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                             INT4 *pi4RetValFsTeLinkBwThreshold12)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_TWELVE;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink);
    TlmGetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, pi4RetValFsTeLinkBwThreshold12);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkBwThreshold13
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                retValFsTeLinkBwThreshold13
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkBwThreshold13 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                             INT4 *pi4RetValFsTeLinkBwThreshold13)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_THIRTEEN;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink);
    TlmGetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, pi4RetValFsTeLinkBwThreshold13);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkBwThreshold14
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                retValFsTeLinkBwThreshold14
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkBwThreshold14 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                             INT4 *pi4RetValFsTeLinkBwThreshold14)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_FOURTEEN;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink);
    TlmGetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, pi4RetValFsTeLinkBwThreshold14);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkBwThreshold15
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                retValFsTeLinkBwThreshold15
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkBwThreshold15 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                             INT4 *pi4RetValFsTeLinkBwThreshold15)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_FIFTEEN;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink);
    TlmGetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, pi4RetValFsTeLinkBwThreshold15);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTeLinkBwThresholdRowStatus
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                retValFsTeLinkBwThresholdRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkBwThresholdRowStatus (INT4 i4IfIndex,
                                    INT4 i4FsTeLinkBwThresholdIndex,
                                    INT4 *pi4RetValFsTeLinkBwThresholdRowStatus)
{
    tTlmTeLink          TlmTeLink;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    i1RetVal = TlmGetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink);
    if (i4FsTeLinkBwThresholdIndex == TLM_ONE)
    {
        *pi4RetValFsTeLinkBwThresholdRowStatus =
            TlmTeLink.i1ThresholdUpRowStatus;
    }
    else
    {
        *pi4RetValFsTeLinkBwThresholdRowStatus =
            TlmTeLink.i1ThresholdDownRowStatus;
    }
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsTeLinkBwThreshold0
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex
                The Object 
                setValFsTeLinkBwThreshold0
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkBwThreshold0 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                            INT4 i4SetValFsTeLinkBwThreshold0)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_ZERO;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4SetValFsTeLinkBwThreshold0);

    i1RetVal = TlmSetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink,
                                                TLM_BW_THRESHOLD_ZERO);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTeLinkBwThreshold1
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                setValFsTeLinkBwThreshold1
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkBwThreshold1 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                            INT4 i4SetValFsTeLinkBwThreshold1)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_ONE;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4SetValFsTeLinkBwThreshold1);

    i1RetVal = TlmSetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink,
                                                TLM_BW_THRESHOLD_ONE);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTeLinkBwThreshold2
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                setValFsTeLinkBwThreshold2
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkBwThreshold2 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                            INT4 i4SetValFsTeLinkBwThreshold2)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_TWO;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4SetValFsTeLinkBwThreshold2);

    i1RetVal = TlmSetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink,
                                                TLM_BW_THRESHOLD_TWO);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTeLinkBwThreshold3
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                setValFsTeLinkBwThreshold3
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkBwThreshold3 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                            INT4 i4SetValFsTeLinkBwThreshold3)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_THREE;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4SetValFsTeLinkBwThreshold3);

    i1RetVal = TlmSetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink,
                                                TLM_BW_THRESHOLD_THREE);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTeLinkBwThreshold4
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                setValFsTeLinkBwThreshold4
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkBwThreshold4 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                            INT4 i4SetValFsTeLinkBwThreshold4)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_FOUR;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4SetValFsTeLinkBwThreshold4);

    i1RetVal = TlmSetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink,
                                                TLM_BW_THRESHOLD_FOUR);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTeLinkBwThreshold5
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                setValFsTeLinkBwThreshold5
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkBwThreshold5 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                            INT4 i4SetValFsTeLinkBwThreshold5)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_FIVE;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4SetValFsTeLinkBwThreshold5);

    i1RetVal = TlmSetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink,
                                                TLM_BW_THRESHOLD_FIVE);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTeLinkBwThreshold6
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                setValFsTeLinkBwThreshold6
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkBwThreshold6 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                            INT4 i4SetValFsTeLinkBwThreshold6)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_SIX;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4SetValFsTeLinkBwThreshold6);

    i1RetVal = TlmSetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink,
                                                TLM_BW_THRESHOLD_SIX);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTeLinkBwThreshold7
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                setValFsTeLinkBwThreshold7
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkBwThreshold7 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                            INT4 i4SetValFsTeLinkBwThreshold7)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_SEVEN;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4SetValFsTeLinkBwThreshold7);

    i1RetVal = TlmSetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink,
                                                TLM_BW_THRESHOLD_SEVEN);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTeLinkBwThreshold8
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                setValFsTeLinkBwThreshold8
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkBwThreshold8 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                            INT4 i4SetValFsTeLinkBwThreshold8)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_EIGHT;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4SetValFsTeLinkBwThreshold8);

    i1RetVal = TlmSetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink,
                                                TLM_BW_THRESHOLD_EIGHT);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTeLinkBwThreshold9
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                setValFsTeLinkBwThreshold9
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkBwThreshold9 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                            INT4 i4SetValFsTeLinkBwThreshold9)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_NINE;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4SetValFsTeLinkBwThreshold9);

    i1RetVal = TlmSetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink,
                                                TLM_BW_THRESHOLD_NINE);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTeLinkBwThreshold10
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                setValFsTeLinkBwThreshold10
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkBwThreshold10 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                             INT4 i4SetValFsTeLinkBwThreshold10)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_TEN;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4SetValFsTeLinkBwThreshold10);

    i1RetVal = TlmSetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink,
                                                TLM_BW_THRESHOLD_TEN);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTeLinkBwThreshold11
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                setValFsTeLinkBwThreshold11
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkBwThreshold11 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                             INT4 i4SetValFsTeLinkBwThreshold11)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_ELEVEN;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4SetValFsTeLinkBwThreshold11);

    i1RetVal = TlmSetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink,
                                                TLM_BW_THRESHOLD_ELEVEN);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTeLinkBwThreshold12
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                setValFsTeLinkBwThreshold12
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkBwThreshold12 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                             INT4 i4SetValFsTeLinkBwThreshold12)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_TWELVE;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4SetValFsTeLinkBwThreshold12);

    i1RetVal = TlmSetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink,
                                                TLM_BW_THRESHOLD_TWELVE);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTeLinkBwThreshold13
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                setValFsTeLinkBwThreshold13
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkBwThreshold13 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                             INT4 i4SetValFsTeLinkBwThreshold13)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_THIRTEEN;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4SetValFsTeLinkBwThreshold13);

    i1RetVal = TlmSetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink,
                                                TLM_BW_THRESHOLD_THIRTEEN);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTeLinkBwThreshold14
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                setValFsTeLinkBwThreshold14
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkBwThreshold14 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                             INT4 i4SetValFsTeLinkBwThreshold14)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_FOURTEEN;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4SetValFsTeLinkBwThreshold14);

    i1RetVal = TlmSetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink,
                                                TLM_BW_THRESHOLD_FOURTEEN);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTeLinkBwThreshold15
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                setValFsTeLinkBwThreshold15
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkBwThreshold15 (INT4 i4IfIndex, INT4 i4FsTeLinkBwThresholdIndex,
                             INT4 i4SetValFsTeLinkBwThreshold15)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_FIFTEEN;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4SetValFsTeLinkBwThreshold15);

    i1RetVal = TlmSetAllTeLinkBwThresholdTable (i4IfIndex,
                                                i4FsTeLinkBwThresholdIndex,
                                                &TlmTeLink,
                                                TLM_BW_THRESHOLD_FIFTEEN);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTeLinkBwThresholdRowStatus
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                setValFsTeLinkBwThresholdRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkBwThresholdRowStatus (INT4 i4IfIndex,
                                    INT4 i4FsTeLinkBwThresholdIndex,
                                    INT4 i4SetValFsTeLinkBwThresholdRowStatus)
{

    tTlmTeLink          TlmTeLink;
    UINT4               u4ObjectId = TLM_ZERO;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    if (i4FsTeLinkBwThresholdIndex == TLM_ONE)
    {
        TlmTeLink.i1ThresholdUpRowStatus =
            (INT1) i4SetValFsTeLinkBwThresholdRowStatus;

        u4ObjectId = TLM_BW_THRESHOLD_UP_ROW_STATUS;
    }
    else
    {
        TlmTeLink.i1ThresholdDownRowStatus =
            (INT1) i4SetValFsTeLinkBwThresholdRowStatus;

        u4ObjectId = TLM_BW_THRESHOLD_DOWN_ROW_STATUS;
    }

    i1RetVal =
        TlmSetAllTeLinkBwThresholdTable (i4IfIndex, i4FsTeLinkBwThresholdIndex,
                                         &TlmTeLink, u4ObjectId);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkBwThreshold0
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                testValFsTeLinkBwThreshold0
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkBwThreshold0 (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               INT4 i4FsTeLinkBwThresholdIndex,
                               INT4 i4TestValFsTeLinkBwThreshold0)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_ZERO;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));
    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4TestValFsTeLinkBwThreshold0);

    i1RetVal = TlmTestAllTeLinkBwThresholdTable (pu4ErrorCode,
                                                 i4IfIndex,
                                                 i4FsTeLinkBwThresholdIndex,
                                                 &TlmTeLink,
                                                 TLM_BW_THRESHOLD_ZERO);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkBwThreshold1
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                testValFsTeLinkBwThreshold1
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkBwThreshold1 (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               INT4 i4FsTeLinkBwThresholdIndex,
                               INT4 i4TestValFsTeLinkBwThreshold1)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_ONE;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));
    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4TestValFsTeLinkBwThreshold1);

    i1RetVal = TlmTestAllTeLinkBwThresholdTable (pu4ErrorCode,
                                                 i4IfIndex,
                                                 i4FsTeLinkBwThresholdIndex,
                                                 &TlmTeLink,
                                                 TLM_BW_THRESHOLD_ONE);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkBwThreshold2
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                testValFsTeLinkBwThreshold2
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkBwThreshold2 (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               INT4 i4FsTeLinkBwThresholdIndex,
                               INT4 i4TestValFsTeLinkBwThreshold2)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_ONE;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));
    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4TestValFsTeLinkBwThreshold2);

    i1RetVal = TlmTestAllTeLinkBwThresholdTable (pu4ErrorCode,
                                                 i4IfIndex,
                                                 i4FsTeLinkBwThresholdIndex,
                                                 &TlmTeLink,
                                                 TLM_BW_THRESHOLD_TWO);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkBwThreshold3
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                testValFsTeLinkBwThreshold3
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkBwThreshold3 (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               INT4 i4FsTeLinkBwThresholdIndex,
                               INT4 i4TestValFsTeLinkBwThreshold3)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_THREE;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));
    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4TestValFsTeLinkBwThreshold3);

    i1RetVal = TlmTestAllTeLinkBwThresholdTable (pu4ErrorCode,
                                                 i4IfIndex,
                                                 i4FsTeLinkBwThresholdIndex,
                                                 &TlmTeLink,
                                                 TLM_BW_THRESHOLD_THREE);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkBwThreshold4
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                testValFsTeLinkBwThreshold4
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkBwThreshold4 (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               INT4 i4FsTeLinkBwThresholdIndex,
                               INT4 i4TestValFsTeLinkBwThreshold4)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_FOUR;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));
    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4TestValFsTeLinkBwThreshold4);

    i1RetVal = TlmTestAllTeLinkBwThresholdTable (pu4ErrorCode,
                                                 i4IfIndex,
                                                 i4FsTeLinkBwThresholdIndex,
                                                 &TlmTeLink,
                                                 TLM_BW_THRESHOLD_FOUR);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkBwThreshold5
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                testValFsTeLinkBwThreshold5
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkBwThreshold5 (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               INT4 i4FsTeLinkBwThresholdIndex,
                               INT4 i4TestValFsTeLinkBwThreshold5)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_FIVE;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));
    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4TestValFsTeLinkBwThreshold5);

    i1RetVal = TlmTestAllTeLinkBwThresholdTable (pu4ErrorCode,
                                                 i4IfIndex,
                                                 i4FsTeLinkBwThresholdIndex,
                                                 &TlmTeLink,
                                                 TLM_BW_THRESHOLD_FIVE);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkBwThreshold6
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                testValFsTeLinkBwThreshold6
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkBwThreshold6 (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               INT4 i4FsTeLinkBwThresholdIndex,
                               INT4 i4TestValFsTeLinkBwThreshold6)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_SIX;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));
    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4TestValFsTeLinkBwThreshold6);

    i1RetVal = TlmTestAllTeLinkBwThresholdTable (pu4ErrorCode,
                                                 i4IfIndex,
                                                 i4FsTeLinkBwThresholdIndex,
                                                 &TlmTeLink,
                                                 TLM_BW_THRESHOLD_SIX);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkBwThreshold7
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                testValFsTeLinkBwThreshold7
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkBwThreshold7 (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               INT4 i4FsTeLinkBwThresholdIndex,
                               INT4 i4TestValFsTeLinkBwThreshold7)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_SEVEN;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));
    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4TestValFsTeLinkBwThreshold7);

    i1RetVal = TlmTestAllTeLinkBwThresholdTable (pu4ErrorCode,
                                                 i4IfIndex,
                                                 i4FsTeLinkBwThresholdIndex,
                                                 &TlmTeLink,
                                                 TLM_BW_THRESHOLD_SEVEN);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkBwThreshold8
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                testValFsTeLinkBwThreshold8
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkBwThreshold8 (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               INT4 i4FsTeLinkBwThresholdIndex,
                               INT4 i4TestValFsTeLinkBwThreshold8)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_EIGHT;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));
    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4TestValFsTeLinkBwThreshold8);

    i1RetVal = TlmTestAllTeLinkBwThresholdTable (pu4ErrorCode,
                                                 i4IfIndex,
                                                 i4FsTeLinkBwThresholdIndex,
                                                 &TlmTeLink,
                                                 TLM_BW_THRESHOLD_EIGHT);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkBwThreshold9
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                testValFsTeLinkBwThreshold9
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkBwThreshold9 (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               INT4 i4FsTeLinkBwThresholdIndex,
                               INT4 i4TestValFsTeLinkBwThreshold9)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_NINE;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));
    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4TestValFsTeLinkBwThreshold9);

    i1RetVal = TlmTestAllTeLinkBwThresholdTable (pu4ErrorCode,
                                                 i4IfIndex,
                                                 i4FsTeLinkBwThresholdIndex,
                                                 &TlmTeLink,
                                                 TLM_BW_THRESHOLD_NINE);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkBwThreshold10
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                testValFsTeLinkBwThreshold10
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkBwThreshold10 (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                INT4 i4FsTeLinkBwThresholdIndex,
                                INT4 i4TestValFsTeLinkBwThreshold10)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_TEN;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));
    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4TestValFsTeLinkBwThreshold10);

    i1RetVal = TlmTestAllTeLinkBwThresholdTable (pu4ErrorCode,
                                                 i4IfIndex,
                                                 i4FsTeLinkBwThresholdIndex,
                                                 &TlmTeLink,
                                                 TLM_BW_THRESHOLD_TEN);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkBwThreshold11
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                testValFsTeLinkBwThreshold11
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkBwThreshold11 (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                INT4 i4FsTeLinkBwThresholdIndex,
                                INT4 i4TestValFsTeLinkBwThreshold11)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_ELEVEN;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));
    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4TestValFsTeLinkBwThreshold11);

    i1RetVal = TlmTestAllTeLinkBwThresholdTable (pu4ErrorCode,
                                                 i4IfIndex,
                                                 i4FsTeLinkBwThresholdIndex,
                                                 &TlmTeLink,
                                                 TLM_BW_THRESHOLD_ELEVEN);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkBwThreshold12
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                testValFsTeLinkBwThreshold12
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkBwThreshold12 (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                INT4 i4FsTeLinkBwThresholdIndex,
                                INT4 i4TestValFsTeLinkBwThreshold12)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_TWELVE;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));
    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4TestValFsTeLinkBwThreshold12);

    i1RetVal = TlmTestAllTeLinkBwThresholdTable (pu4ErrorCode,
                                                 i4IfIndex,
                                                 i4FsTeLinkBwThresholdIndex,
                                                 &TlmTeLink,
                                                 TLM_BW_THRESHOLD_TWELVE);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkBwThreshold13
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                testValFsTeLinkBwThreshold13
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkBwThreshold13 (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                INT4 i4FsTeLinkBwThresholdIndex,
                                INT4 i4TestValFsTeLinkBwThreshold13)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_THIRTEEN;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));
    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4TestValFsTeLinkBwThreshold13);

    i1RetVal = TlmTestAllTeLinkBwThresholdTable (pu4ErrorCode,
                                                 i4IfIndex,
                                                 i4FsTeLinkBwThresholdIndex,
                                                 &TlmTeLink,
                                                 TLM_BW_THRESHOLD_THIRTEEN);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkBwThreshold14
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                testValFsTeLinkBwThreshold14
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkBwThreshold14 (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                INT4 i4FsTeLinkBwThresholdIndex,
                                INT4 i4TestValFsTeLinkBwThreshold14)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_FOURTEEN;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));
    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4TestValFsTeLinkBwThreshold14);

    i1RetVal = TlmTestAllTeLinkBwThresholdTable (pu4ErrorCode,
                                                 i4IfIndex,
                                                 i4FsTeLinkBwThresholdIndex,
                                                 &TlmTeLink,
                                                 TLM_BW_THRESHOLD_FOURTEEN);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkBwThreshold15
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                testValFsTeLinkBwThreshold15
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkBwThreshold15 (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                INT4 i4FsTeLinkBwThresholdIndex,
                                INT4 i4TestValFsTeLinkBwThreshold15)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ThresholdIndex = TLM_BW_THRESHOLD_FIFTEEN;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));
    TlmSetTeLinkBwThreshold (u4ThresholdIndex, i4FsTeLinkBwThresholdIndex,
                             &TlmTeLink, i4TestValFsTeLinkBwThreshold15);

    i1RetVal = TlmTestAllTeLinkBwThresholdTable (pu4ErrorCode,
                                                 i4IfIndex,
                                                 i4FsTeLinkBwThresholdIndex,
                                                 &TlmTeLink,
                                                 TLM_BW_THRESHOLD_FIFTEEN);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkBwThresholdRowStatus
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex

                The Object 
                testValFsTeLinkBwThresholdRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkBwThresholdRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                       INT4 i4FsTeLinkBwThresholdIndex,
                                       INT4
                                       i4TestValFsTeLinkBwThresholdRowStatus)
{
    tTlmTeLink          TlmTeLink;
    UINT4               u4ObjectId = TLM_ZERO;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TlmTeLink, TLM_ZERO, sizeof (TlmTeLink));

    if (i4FsTeLinkBwThresholdIndex == TLM_ONE)
    {
        TlmTeLink.i1ThresholdUpRowStatus
            = (INT1) i4TestValFsTeLinkBwThresholdRowStatus;

        u4ObjectId = TLM_BW_THRESHOLD_UP_ROW_STATUS;
    }
    else
    {
        TlmTeLink.i1ThresholdDownRowStatus
            = (INT1) i4TestValFsTeLinkBwThresholdRowStatus;

        u4ObjectId = TLM_BW_THRESHOLD_DOWN_ROW_STATUS;
    }

    i1RetVal = TlmTestAllTeLinkBwThresholdTable (pu4ErrorCode, i4IfIndex,
                                                 i4FsTeLinkBwThresholdIndex,
                                                 &TlmTeLink, u4ObjectId);
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsTeLinkBwThresholdTable
 Input       :  The Indices
                IfIndex
                FsTeLinkBwThresholdIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTeLinkBwThresholdTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTeLinkBwThresholdForceOption
 Input       :  The Indices

                The Object 
                retValFsTeLinkBwThresholdForceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTeLinkBwThresholdForceOption (INT4
                                      *pi4RetValFsTeLinkBwThresholdForceOption)
{
    *pi4RetValFsTeLinkBwThresholdForceOption =
        gTlmGlobalInfo.u1BwForceThresholdEnable;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsTeLinkBwThresholdForceOption
 Input       :  The Indices

                The Object 
                setValFsTeLinkBwThresholdForceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTeLinkBwThresholdForceOption (INT4
                                      i4SetValFsTeLinkBwThresholdForceOption)
{
    gTlmGlobalInfo.u1BwForceThresholdEnable
        = (UINT1) i4SetValFsTeLinkBwThresholdForceOption;

    TlmUpdateInfoToRegProtocol (TLM_OSPF_TE_APP_ID);

    gTlmGlobalInfo.u1BwForceThresholdEnable = TLM_ZERO;

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsTeLinkBwThresholdForceOption
 Input       :  The Indices

                The Object 
                testValFsTeLinkBwThresholdForceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTeLinkBwThresholdForceOption (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4TestValFsTeLinkBwThresholdForceOption)
{
    if (i4TestValFsTeLinkBwThresholdForceOption !=
        TLM_BW_THRESHOLD_FORCE_ENABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsTeLinkBwThresholdForceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTeLinkBwThresholdForceOption (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
