/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: tlmcli.c,v 1.23 2016/08/08 10:04:40 siva Exp $
 *
 * Description: This file contains the code for supporting CLI 
 *              commands to TLM module
 ***********************************************************************/
#include "tlminc.h"
#include "tlmcli.h"
#include "tlmclip.h"
#include "stdtlmcli.h"
#include "fstlmcli.h"
#include "cfa.h"
#include "mplscli.h"
extern INT4         CliGetCurModePromptStr (INT1 *);
PRIVATE VOID        TlmCliShowBundleType (tCliHandle CliHandle,
                                          INT4 i4TeIfIndex);
PRIVATE VOID        TlmCliShowTeLinkConfig (tCliHandle CliHandle,
                                            INT4 i4TeIfIndex);
PRIVATE VOID        TlmCliShowTeCompLinkInfo (tCliHandle CliHandle,
                                              INT4 i4TeIfIndex);
PRIVATE VOID        TlmCliShowCompLinkConfig (tCliHandle CliHandle,
                                              INT4 i4CompIfIndex);
PRIVATE VOID        TlmCliShowSwiCapType (tCliHandle CliHandle,
                                          INT4 i4SwitchCap, INT4 i4EncodType);
PRIVATE VOID        TlmCliShowThresholdValues (tCliHandle CliHandle,
                                               INT4 i4TeIfIndex,
                                               INT4 i4BwThreshIndex);

/**************************************************************************/
/*  Function Name   : cli_process_tlm_cmd                                 */
/*                                                                        */
/*  Description     : Protocol CLI message handler function               */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    u4Command - Command identifier                      */
/*                    ... -Variable command argument list                 */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/
INT4
cli_process_tlm_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    tTeLinkCliArgs      TeLinkCliArgs;
    tCompLinkCliArgs    CompLinkCliArgs;
    UINT4              *apu4args[TLM_CLI_MAX_ARGS];
    UINT4               u4InterfaceIndex = TLM_ZERO;
    INT4                i4TeIfIndex = TLM_ZERO;
    INT4                i4CompIfIndex = TLM_ZERO;
    INT4                i4BundleIfIndex = TLM_ZERO;
    INT4                ai4BwThreshold[TLM_MAX_BW_THRESHOLD_SUPPORTED];
    UINT4               u4Count = TLM_ZERO;
    INT4                i4RetVal = TLM_FAILURE;
    INT4                i4TeLinkType = TLM_ZERO;
    INT1                argno = TLM_ZERO;

    MEMSET (ai4BwThreshold, TLM_ZERO, sizeof (ai4BwThreshold));
    CliRegisterLock (CliHandle, TlmLock, TlmUnLock);
    TlmLock ();

    va_start (ap, u4Command);

    /* second argument is always (interface name/index) */
    u4InterfaceIndex = va_arg (ap, UINT4);

    while (1)
    {
        apu4args[argno] = va_arg (ap, UINT4 *);
        argno++;
        if (argno == TLM_CLI_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);

    switch (u4Command)
    {
        case TLM_CLI_TE_MODE:
            i4RetVal = TlmCliTeMode (CliHandle);
            break;

        case TLM_CLI_MODULE_ENABLE:
            i4RetVal = TlmCliSetModuleStatus (CliHandle, TLM_ENABLED);
            break;

        case TLM_CLI_MODULE_DISABLE:
            i4RetVal = TlmCliSetModuleStatus (CliHandle, TLM_DISABLED);
            break;

        case TLM_CLI_TELINK_CREATE:

            i4RetVal = TlmCliCreateTeLink (CliHandle, (UINT1 *) apu4args[0],
                                           i4TeIfIndex,
                                           CLI_PTR_TO_I4 (apu4args[1]));
            break;

        case TLM_CLI_TELINK_DELETE:

            i4RetVal = TlmCliDeleteTeLink (CliHandle, (UINT1 *) apu4args[0],
                                           TLM_ZERO);

            break;

        case TLM_CLI_UNBUNDLE_TELINK_CREATE:

            i4BundleIfIndex = TLM_BUNDLE_TE_LINK_INDEX (CliHandle);

            i4RetVal = TlmCliCreateTeLink (CliHandle, (UINT1 *) apu4args[0],
                                           i4BundleIfIndex, TLM_UNBUNDLE);
            break;

        case TLM_CLI_UNBUNDLE_TELINK_DELETE:

            i4BundleIfIndex = TLM_BUNDLE_TE_LINK_INDEX (CliHandle);

            i4RetVal =
                TlmCliDeleteTeLink (CliHandle, (UINT1 *) apu4args[0],
                                    i4BundleIfIndex);

            break;

        case TLM_CLI_TELINK_METRIC:

            i4TeIfIndex =
                TlmCliGetIndices (CliHandle, TLM_LINK_TYPE (CliHandle));

            TeLinkCliArgs.u4Metric = *apu4args[TLM_INDEX_ZERO];

            i4RetVal = TlmCliSetTeLinkProperties (CliHandle, u4Command,
                                                  &TeLinkCliArgs);
            break;

        case TLM_CLI_TELINK_ADDRTYPE:

            i4TeIfIndex =
                TlmCliGetIndices (CliHandle, TLM_LINK_TYPE (CliHandle));

            TeLinkCliArgs.i4AddrType = CLI_PTR_TO_I4 (apu4args[TLM_INDEX_ZERO]);
            i4RetVal = TlmCliSetTeLinkProperties (CliHandle, u4Command,
                                                  &TeLinkCliArgs);
            break;

        case TLM_CLI_TELINK_ROUTER_ID:

            i4TeIfIndex =
                TlmCliGetIndices (CliHandle, TLM_LINK_TYPE (CliHandle));

            TeLinkCliArgs.u4RouterId = *apu4args[TLM_INDEX_ZERO];
            i4RetVal = TlmCliSetTeLinkProperties (CliHandle, u4Command,
                                                  &TeLinkCliArgs);
            break;

        case TLM_CLI_TELINK_NUM:

            i4TeIfIndex =
                TlmCliGetIndices (CliHandle, TLM_LINK_TYPE (CliHandle));

            TeLinkCliArgs.u4LocalIpAddr = *apu4args[TLM_INDEX_ZERO];
            TeLinkCliArgs.u4RemoteIpAddr = *apu4args[TLM_INDEX_ONE];

            i4RetVal = TlmCliSetTeLinkProperties (CliHandle, u4Command,
                                                  &TeLinkCliArgs);
            break;

        case TLM_CLI_TELINK_UNNUM:

            i4TeIfIndex =
                TlmCliGetIndices (CliHandle, TLM_LINK_TYPE (CliHandle));

            TeLinkCliArgs.u4LocalIdentifier = *apu4args[TLM_INDEX_ZERO];
            TeLinkCliArgs.u4RemoteIdentifier = *apu4args[TLM_INDEX_ONE];

            i4RetVal = TlmCliSetTeLinkProperties (CliHandle, u4Command,
                                                  &TeLinkCliArgs);
            break;

        case TLM_CLI_TELINK_RESOURCE_CLASS:

            i4TeIfIndex =
                TlmCliGetIndices (CliHandle, TLM_LINK_TYPE (CliHandle));

            TeLinkCliArgs.u4ResourceClass = *apu4args[TLM_INDEX_ZERO];

            i4RetVal = TlmCliSetTeLinkProperties (CliHandle, u4Command,
                                                  &TeLinkCliArgs);
            break;

        case TLM_CLI_TELINK_CHANNEL_TYPE:
            i4TeIfIndex =
                TlmCliGetIndices (CliHandle, TLM_LINK_TYPE (CliHandle));
            TeLinkCliArgs.i4TeLinkInfoType =
                CLI_PTR_TO_I4 (apu4args[TLM_INDEX_ZERO]);
            i4RetVal =
                TlmCliSetTeLinkProperties (CliHandle, u4Command,
                                           &TeLinkCliArgs);
            break;

        case TLM_CLI_TELINK_IFTYPE:
            i4TeIfIndex =
                TlmCliGetIndices (CliHandle, TLM_LINK_TYPE (CliHandle));
            TeLinkCliArgs.i4TeLinkIfType =
                CLI_PTR_TO_I4 (apu4args[TLM_INDEX_ZERO]);
            i4RetVal =
                TlmCliSetTeLinkProperties (CliHandle, u4Command,
                                           &TeLinkCliArgs);
            break;

        case TLM_CLI_TELINK_NO_RESOURCE_CLASS:

            i4TeIfIndex =
                TlmCliGetIndices (CliHandle, TLM_LINK_TYPE (CliHandle));

            TeLinkCliArgs.u4ResourceClass = *apu4args[TLM_INDEX_ZERO];

            i4RetVal = TlmCliSetTeLinkProperties (CliHandle, u4Command,
                                                  &TeLinkCliArgs);
            break;

        case TLM_CLI_TELINK_PROTECTION_TYPE:

            i4TeIfIndex =
                TlmCliGetIndices (CliHandle, TLM_LINK_TYPE (CliHandle));
            TeLinkCliArgs.i4ProtectionType =
                CLI_PTR_TO_I4 (apu4args[TLM_INDEX_ZERO]);

            i4RetVal = TlmCliSetTeLinkProperties (CliHandle, u4Command,
                                                  &TeLinkCliArgs);
            break;

        case TLM_CLI_TELINK_SRLG:

            i4TeIfIndex =
                TlmCliGetIndices (CliHandle, TLM_LINK_TYPE (CliHandle));
            TeLinkCliArgs.u4TeLinkSrlg = *apu4args[TLM_INDEX_ZERO];

            i4RetVal = TlmCliSetTeLinkProperties (CliHandle, u4Command,
                                                  &TeLinkCliArgs);
            break;

        case TLM_CLI_TELINK_NO_SRLG:

            i4TeIfIndex =
                TlmCliGetIndices (CliHandle, TLM_LINK_TYPE (CliHandle));
            TeLinkCliArgs.u4TeLinkSrlg = *apu4args[TLM_INDEX_ZERO];

            i4RetVal = TlmCliSetTeLinkProperties (CliHandle, u4Command,
                                                  &TeLinkCliArgs);

            break;
        case TLM_CLI_TELINK_UP:

            i4TeIfIndex =
                TlmCliGetIndices (CliHandle, TLM_LINK_TYPE (CliHandle));

            nmhGetFsTeLinkType (i4TeIfIndex, &i4TeLinkType);

            i4RetVal = TlmCliSetTeLinkAdminStatusUp (CliHandle, i4TeIfIndex);
            break;

        case TLM_CLI_TELINK_DOWN:

            i4TeIfIndex =
                TlmCliGetIndices (CliHandle, TLM_LINK_TYPE (CliHandle));

            nmhGetFsTeLinkType (i4TeIfIndex, &i4TeLinkType);

            i4RetVal = TlmCliSetTeLinkAdminStatusDn (CliHandle, i4TeIfIndex);
            break;
        case TLM_CLI_COMPLINK_CREATE:

            i4CompIfIndex =
                TlmCliGetIndices (CliHandle, TLM_LINK_TYPE (CliHandle));
            i4TeIfIndex = TLM_TE_LINK_INDEX (CliHandle);
             /** Interface index is component link index here **/
            /* Create component link */
            i4RetVal =
                TlmCliCreateCompLink (CliHandle, (INT4) u4InterfaceIndex,
                                      i4TeIfIndex);
            break;
        case TLM_CLI_COMPLINK_DELETE:

            i4TeIfIndex = TLM_TE_LINK_INDEX (CliHandle);
            i4CompIfIndex =
                TlmCliGetIndices (CliHandle, TLM_LINK_TYPE (CliHandle));

            i4RetVal =
                TlmCliDeleteCompLink (CliHandle, (INT4) u4InterfaceIndex,
                                      i4TeIfIndex);
            break;
        case TLM_CLI_COMPLINK_SWCAP_ENCOD:

            i4CompIfIndex =
                TlmCliGetIndices (CliHandle, TLM_LINK_TYPE (CliHandle));

            CompLinkCliArgs.i4IfIndex = i4CompIfIndex;
            CompLinkCliArgs.i4SwitchingCap =
                CLI_PTR_TO_I4 (apu4args[TLM_INDEX_ZERO]);
            CompLinkCliArgs.i4EncodingType =
                CLI_PTR_TO_I4 (apu4args[TLM_INDEX_ONE]);

            i4RetVal = TlmCliSetCompLinkDescProperties (CliHandle, u4Command,
                                                        &CompLinkCliArgs);
            break;
        case TLM_CLI_COMPLINK_MAX_RESBW:

            i4CompIfIndex = TLM_COMP_LINK_INDEX (CliHandle);

            i4RetVal = TlmCliSetCompLinkMaxRsrvableBw (CliHandle,
                                                       *apu4args
                                                       [TLM_INDEX_ZERO],
                                                       i4CompIfIndex);
            break;

        case TLM_CLI_COMPLINK_NOMAX_RESBW:

            /*This set the default value(Cfa IfSpeed) for
             * Max reservable bandwidth */

            i4CompIfIndex = TLM_COMP_LINK_INDEX (CliHandle);

            i4RetVal = TlmCliSetCompLinkMaxRsrvableBw (CliHandle,
                                                       TLM_INDEX_ZERO,
                                                       i4CompIfIndex);

            break;

        case TLM_CLI_COMPLINK_MINLSPBW:

            /*apu4args[TLM_INDEX_ZERO] =  Min Lsp bandwidth */

            i4CompIfIndex = TLM_COMP_LINK_INDEX (CliHandle);

            CompLinkCliArgs.i4IfIndex = i4CompIfIndex;
            CompLinkCliArgs.u4MinLsp = *apu4args[TLM_INDEX_ZERO];

            i4RetVal = TlmCliSetCompLinkDescProperties (CliHandle, u4Command,
                                                        &CompLinkCliArgs);

            break;

        case TLM_CLI_COMPLINK_NOMINLSPBW:

            /*apu4args[TLM_INDEX_ZERO] =  Min Lsp bandwidth */

            i4CompIfIndex = TLM_COMP_LINK_INDEX (CliHandle);

            CompLinkCliArgs.i4IfIndex = i4CompIfIndex;
            CompLinkCliArgs.u4MinLsp = TLM_INDEX_ZERO;

            i4RetVal = TlmCliSetCompLinkDescProperties (CliHandle, u4Command,
                                                        &CompLinkCliArgs);
            break;

        case TLM_CLI_COMPLINK_UP:

            i4CompIfIndex = TLM_COMP_LINK_INDEX (CliHandle);
            i4TeIfIndex = TLM_TE_LINK_INDEX (CliHandle);

            i4RetVal = TlmCliSetCompLinkAdminStatusUp (CliHandle, i4TeIfIndex,
                                                       i4CompIfIndex);
            break;

        case TLM_CLI_COMPLINK_DOWN:

            i4TeIfIndex = TLM_TE_LINK_INDEX (CliHandle);
            i4CompIfIndex = TLM_COMP_LINK_INDEX (CliHandle);

            i4RetVal = TlmCliSetCompLinkAdminStatusDown (CliHandle,
                                                         i4CompIfIndex);

            break;

        case TLM_CLI_TELINK_BW_THRESHOLD:

            i4TeIfIndex = TLM_TE_LINK_INDEX (CliHandle);
            for (u4Count = TLM_ZERO; u4Count < TLM_MAX_BW_THRESHOLD_SUPPORTED;
                 u4Count++)
            {
                ai4BwThreshold[u4Count] =
                    CLI_PTR_TO_I4 (apu4args[u4Count + TLM_ONE]);
            }
            i4RetVal = TlmCliSetBwThreshold (CliHandle, i4TeIfIndex,
                                             CLI_PTR_TO_I4 (apu4args
                                                            [TLM_INDEX_ZERO]),
                                             ai4BwThreshold);
            break;

        case TLM_CLI_FORCE_BW_THRESHOLD:
            i4RetVal = TlmCliSetBwThresholdForce (CliHandle);
            break;
        case TLM_CLI_DBG:
        case TLM_CLI_NO_DBG:
            i4RetVal = TlmCliSetTeLinkTrace (u4Command,
                                             CLI_PTR_TO_I4 (apu4args
                                                            [TLM_INDEX_ZERO]));
            break;
        case TLM_CLI_SHOW_TELINK_NAME:
        case TLM_CLI_SHOW_TELINK:

            i4RetVal = TlmCliShowTeLink (CliHandle, u4Command,
                                         (UINT1 *) apu4args[TLM_INDEX_ZERO]);
            break;
        case TLM_CLI_SHOW_COMPLINK_INDEX:
        case TLM_CLI_SHOW_COMPLINK:

            i4CompIfIndex = (INT4) u4InterfaceIndex;

            TlmCliShowCompLinkInfo (CliHandle, u4Command, i4CompIfIndex);

            i4RetVal = CLI_SUCCESS;
            break;
        case TLM_CLI_UPDATE_LINK_INDICES:
            TlmCliUpdateLinkIndices (CliHandle);
            i4RetVal = CLI_SUCCESS;
            break;

        case TLM_CLI_TELINK_ADVERTISE:

            TeLinkCliArgs.i4TeLinkAdvertise = TLM_SNMP_TRUE;
            i4RetVal =
                TlmCliSetTeLinkProperties (CliHandle, u4Command,
                                           &TeLinkCliArgs);
            break;

        case TLM_CLI_TELINK_NO_ADVERTISE:

            TeLinkCliArgs.i4TeLinkAdvertise = TLM_SNMP_FALSE;
            i4RetVal =
                TlmCliSetTeLinkProperties (CliHandle, u4Command,
                                           &TeLinkCliArgs);
            break;

        default:
            break;
    }

    TlmUnLock ();
    CliUnRegisterLock (CliHandle);

    return i4RetVal;
}

/******************************************************************************/
/* Function Name  :  TlmCliSetModuleStatus                                    */
/*                                                                            */
/* Description    : This function sets the TE link module status              */
/*                                                                            */
/*  Input         : CliHandle : Cli Context Id                                */
/*                  i4Status  : TLM_ENABLED/TLM_DISABLED                      */
/*  Output        : None                                                      */
/*                                                                            */
/*  Returns       : CLI_SUCCESS/CLI_FAILURE                                   */
/******************************************************************************/
PRIVATE INT4
TlmCliSetModuleStatus (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsTeLinkModuleStatus (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {

        CliPrintf (CliHandle, "\r\n%%Unable to set TLM module status\r\n");
        return CLI_FAILURE;
    }

    nmhSetFsTeLinkModuleStatus (i4Status);

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name  :  TlmCliUpdateLinkIndices                                  */
/*                                                                            */
/* Description    : This function updates the link indices based on the       */
/*                  exit command                                              */
/*                                                                            */
/*  Input         : CliHandle : Cli Context Id                                */
/*  Output        : None                                                      */
/*                                                                            */
/*  Returns       : CLI_SUCCESS/CLI_FAILURE                                   */
/******************************************************************************/
VOID
TlmCliUpdateLinkIndices (tCliHandle CliHandle)
{
    if (gTlmCliArgs[CliHandle].i4TeLinkType == TLM_COMPLINK_INDEX)
    {
        gTlmCliArgs[CliHandle].i4TeLinkType = TLM_TELINK_INDEX;
    }
    else if (gTlmCliArgs[CliHandle].i4TeLinkType == TLM_TELINK_INDEX)
    {
        gTlmCliArgs[CliHandle].i4TeLinkType = TLM_BUNDLE_TELINK_INDEX;
    }
    return;
}

/******************************************************************************/
/* Function Name  :  TlmCliGetIndices                                         */
/*                                                                            */
/* Description    : This function fills the TE link indices                   */
/*                                                                            */
/*  Input         : CliHandle : Cli Context Id                                */
/*                  u4LinkType : TeLink/CompLink                              */
/*  Output        : None                                                      */
/*                                                                            */
/*  Returns       : i4TeIfIndex - TE link IfIndex                             */
/******************************************************************************/
INT4
TlmCliGetIndices (tCliHandle CliHandle, INT4 i4LinkType)
{
    INT4                i4IfIndex = TLM_ZERO;

    if (i4LinkType == TLM_TELINK_INDEX)
    {
        i4IfIndex = gTlmCliArgs[CliHandle].i4TeIfIndex;
    }
    else if (i4LinkType == TLM_BUNDLE_TELINK_INDEX)
    {
        i4IfIndex = gTlmCliArgs[CliHandle].i4TeBundleIndex;
    }
    else
    {
        i4IfIndex = gTlmCliArgs[CliHandle].i4TeCompIndex;
    }
    return i4IfIndex;
}

/******************************************************************************/
/* Function Name  :  TlmCliTeMode                                             */
/*                                                                            */
/* Description    : This function change the mode to config-mpls-te           */
/*                                                                            */
/*  Input         : CliHandle : Cli Context Id                                */
/*                                                                            */
/*  Output        :  None                                                     */
/*                                                                            */
/*  Returns       :  CLI_SUCCESS/CLI_FAILURE                                  */
/******************************************************************************/
INT4
TlmCliTeMode (tCliHandle CliHandle)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];

    MEMSET (au1Cmd, TLM_ZERO, sizeof (au1Cmd));

    /* Currently No interface index for this mode is there
     *  as this command just  changes only the mode  */
    if (TLM_MODULE_STATUS == TLM_DISABLED)
    {
        CliPrintf (CliHandle,
                   "\r\n%%TE-LINK cannot be configured as TLM is disabled\r\n");
        return CLI_FAILURE;
    }

    SPRINTF ((CHR1 *) au1Cmd, "%s", CLI_MODE_TELINK_CONFIG);

    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to change to TELINK mode\r\n");

        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : TlmCliSetTeLinkAdminStatusUp                        */
/*                                                                         */
/*     Description   : The function is used to set the admin status of     */
/*                     Telink as enable                                    */
/*                                                                         */
/*     Input(s)      : tCliHandle     : Cli context handler                */
/*                     i4TeIfIndex    : TeLink IfIndex                     */
/*                     i4BundleIfIndex: Bundle link IfIndex                */
/*     Output(s)     : NULL                                                */
/*                                                                         */
/*     Returns       : CLI_SUCCESS/CLI_FAILURE.                            */
/*                                                                         */
/***************************************************************************/
INT4
TlmCliSetTeLinkAdminStatusUp (tCliHandle CliHandle, INT4 i4TeIfIndex)
{
	UINT4   u4ErrCode = TLM_ZERO;
    if (TlmCliSetTeLinkRowStatus (CliHandle, i4TeIfIndex, ACTIVE)
        == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to activate TE link row status\r\n");
        return CLI_FAILURE;
    }
 	CFA_LOCK ();
    if (nmhTestv2IfMainAdminStatus (&u4ErrCode, i4TeIfIndex, CFA_IF_UP)
            == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return CLI_FAILURE;
    }

    if (nmhSetIfMainAdminStatus (i4TeIfIndex, CFA_IF_UP)
            == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return CLI_FAILURE;
    }

    CFA_UNLOCK ();


    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : TlmCliSetTeLinkAdminStatusDn                        */
/*                                                                         */
/*     Description   : The function is to set the admin status of te       */
/*                     link as disable                                     */
/*                                                                         */
/*     Input(s)      : tCliHandle     : Cli context handler                */
/*                     i4TeIfIndex    : TeLink IfIndex                     */
/*                     i4BundleIfIndex: BundleIfIndex                      */
/*     Output(s)     : NULL                                                */
/*                                                                         */
/*     Returns       : CLI_SUCCESS/CLI_FAILURE.                            */
/*                                                                         */
/***************************************************************************/
INT4
TlmCliSetTeLinkAdminStatusDn (tCliHandle CliHandle, INT4 i4TeIfIndex)
{
	UINT4   u4ErrCode = TLM_ZERO;	
    if (TlmCliSetTeLinkRowStatus (CliHandle, i4TeIfIndex, NOT_IN_SERVICE)
        == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to activate TE link row status\r\n");
        return CLI_FAILURE;
    }

    CFA_LOCK ();
    if (nmhTestv2IfMainAdminStatus (&u4ErrCode, i4TeIfIndex, CFA_IF_DOWN)
            == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return CLI_FAILURE;
    }

    if (nmhSetIfMainAdminStatus (i4TeIfIndex, CFA_IF_DOWN)
            == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return CLI_FAILURE;
    }
    CFA_UNLOCK ();

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :   TlmCliSetCompLinkAdminStatusUp                           */
/*                                                                           */
/*  Description :  This function create a row in Complink table              */
/*                  comp desc,bandwidth table  as ACTIVE                     */
/*                                                                           */
/*  Input       :   i4TeIfIndex - TeLink IfIndex                             */
/*                  i4CompIfIndex - Component Link IfIndex                   */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/
INT4
TlmCliSetCompLinkAdminStatusUp (tCliHandle CliHandle, INT4 i4TeIfIndex,
                                INT4 i4CompIfIndex)
{
    UNUSED_PARAM (i4TeIfIndex);

    if (TlmCliSetCompLinkRowStatus (CliHandle, i4CompIfIndex, ACTIVE)
        == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to activate Component Link entry\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :   TlmCliSetCompLinkAdminStatusDown                         */
/*                                                                           */
/*  Description :  This function set  a row in complink table                */
/*                  comp desc,bandwidth table  as NOTINSERVICE               */
/*                                                                           */
/*  Input       : CliHandle : Cli Context Id                                 */
/*                i4CompIfIndex - Component Link IfIndex                     */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/
INT4
TlmCliSetCompLinkAdminStatusDown (tCliHandle CliHandle, INT4 i4CompIfIndex)
{
    if (TlmCliSetCompLinkRowStatus (CliHandle, i4CompIfIndex, NOT_IN_SERVICE)
        == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to activate Component Link entry\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :   TlmCliCreateCompLink                                     */
/*                                                                           */
/*  Description :  This function create the component link and associate     */
/*                 descriptor with that component link                       */
/*                                                                           */
/*  Input(s)        : CliHandle - CliContext ID                              */
/*                    i4CompLinkIndex - Component Index                      */
/*                    i4TeIfIndex - TE link interface Index                  */
/*                                                                           */
/*  Output(s)       : None                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*****************************************************************************/
INT4
TlmCliCreateCompLink (tCliHandle CliHandle, INT4 i4CompLinkIndex,
                      INT4 i4TeIfIndex)
{
    tTlmComponentLink  *pCompIface = NULL;
    INT4                i4RetVal = CLI_FAILURE;
    UINT4               u4HighStackIndex = TLM_ZERO;
    UINT4               u4LowerStackIndex = TLM_ZERO;
    INT4                i4TeLinkType = TLM_ZERO;

    nmhGetFsTeLinkType (i4TeIfIndex, &i4TeLinkType);

    if (i4TeLinkType == TLM_BUNDLE)
    {
        CliPrintf (CliHandle, "\r%% Invalid association of component"
                   " link to a bundle TE link\r\n");
        return CLI_FAILURE;
    }

    CfaApiGetTeLinkIfFromL3If ((UINT4) i4CompLinkIndex,
                               (UINT4 *) &u4HighStackIndex, TLM_ONE, TRUE);

    if ((u4HighStackIndex != TLM_ZERO) &&
        (u4HighStackIndex != (UINT4) i4TeIfIndex))
    {
        CliPrintf (CliHandle, "\r%% Invalid association of component"
                   " link to a wrong TE link\r\n");
        return CLI_FAILURE;
    }
    else if (u4HighStackIndex == (UINT4) i4TeIfIndex)
    {
        i4RetVal =
            TlmCliChangePrompt (TLM_FALSE, TLM_COMPLINK_INDEX, i4CompLinkIndex);

        gTlmCliArgs[CliHandle].i4TeIfIndex = i4TeIfIndex;
        gTlmCliArgs[CliHandle].i4TeLinkType = TLM_COMPLINK_INDEX;
        gTlmCliArgs[CliHandle].i4TeCompIndex = i4CompLinkIndex;

        return i4RetVal;
    }
    else
    {
        pCompIface = TlmFindComponentIf (i4CompLinkIndex);

        CfaApiGetL3IfFromTeLinkIf ((UINT4) i4TeIfIndex,
                                   (UINT4 *) &u4LowerStackIndex, TRUE);

        if ((pCompIface != NULL)
            && (u4LowerStackIndex == (UINT4) i4CompLinkIndex))
        {
            i4RetVal =
                TlmCliChangePrompt (TLM_FALSE, TLM_COMPLINK_INDEX,
                                    i4CompLinkIndex);
            return i4RetVal;
        }
    }

    if (TlmCliSetCompLinkRowStatus (CliHandle, i4CompLinkIndex, CREATE_AND_WAIT)
        == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to create component link entry\r\n");
        return CLI_FAILURE;
    }

    if (TlmCliSetCompLinkDescRowStatus (CliHandle, i4CompLinkIndex,
                                        CREATE_AND_WAIT) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to create component link descriptor"
                   " entry\r\n");
        TlmCliSetCompLinkRowStatus (CliHandle, i4CompLinkIndex, DESTROY);
        return CLI_FAILURE;
    }
    if (TlmCliSetCompLinkDescRowStatus (CliHandle, i4CompLinkIndex,
                                        ACTIVE) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to activate component link descriptor"
                   " entry\r\n");
        TlmCliSetCompLinkRowStatus (CliHandle, i4CompLinkIndex, DESTROY);
        return CLI_FAILURE;
    }

    if (TlmCliSetCompLinkBwRowStatus
        (CliHandle, i4CompLinkIndex, CREATE_AND_WAIT) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to create component link bandwidth"
                   " entry\r\n");
        TlmCliSetCompLinkRowStatus (CliHandle, i4CompLinkIndex, DESTROY);
        TlmCliSetCompLinkDescRowStatus (CliHandle, i4CompLinkIndex, DESTROY);
        return CLI_FAILURE;
    }
    if (TlmCliSetCompLinkBwRowStatus
        (CliHandle, i4CompLinkIndex, ACTIVE) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to activate component link bandwidth"
                   " entry\r\n");
        TlmCliSetCompLinkRowStatus (CliHandle, i4CompLinkIndex, DESTROY);
        TlmCliSetCompLinkDescRowStatus (CliHandle, i4CompLinkIndex, DESTROY);
        return CLI_FAILURE;
    }

    /*Stacking of TE link interface over Component Link interface */
    if (TlmPortStackTeLinkIfOrMplsIf (i4TeIfIndex, i4CompLinkIndex,
                                      CREATE_AND_WAIT) == TLM_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to stack TE link interface over "
                   "Component\r\n");
   TlmCliSetCompLinkBwRowStatus (CliHandle, i4CompLinkIndex, DESTROY);
   TlmCliSetCompLinkDescRowStatus (CliHandle, i4CompLinkIndex, DESTROY);
   TlmCliSetCompLinkRowStatus (CliHandle, i4CompLinkIndex, DESTROY);
   return CLI_FAILURE;
    }
    if (TlmPortStackTeLinkIfOrMplsIf (i4TeIfIndex, i4CompLinkIndex,
                                      ACTIVE) == TLM_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to stack TE link interface over "
                   "Component\r\n");
   TlmCliSetCompLinkBwRowStatus (CliHandle, i4CompLinkIndex, DESTROY);
   TlmCliSetCompLinkDescRowStatus (CliHandle, i4CompLinkIndex, DESTROY);
   TlmCliSetCompLinkRowStatus (CliHandle, i4CompLinkIndex, DESTROY);
   return CLI_FAILURE;
    }

    if (TlmCliSetCompLinkRowStatus (CliHandle, i4CompLinkIndex, ACTIVE)
        == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to activate component link entry\r\n");
   TlmCliSetCompLinkBwRowStatus (CliHandle, i4CompLinkIndex, DESTROY);
   TlmCliSetCompLinkDescRowStatus (CliHandle, i4CompLinkIndex, DESTROY);
   return CLI_FAILURE;
    }

    gTlmCliArgs[CliHandle].i4TeIfIndex = i4TeIfIndex;
    gTlmCliArgs[CliHandle].i4TeLinkType = TLM_COMPLINK_INDEX;
    gTlmCliArgs[CliHandle].i4TeCompIndex = i4CompLinkIndex;

    i4RetVal =
        TlmCliChangePrompt (TLM_FALSE, TLM_COMPLINK_INDEX, i4CompLinkIndex);

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : TlmCliDeleteCompLink                               */
/*                                                                           */
/*     DESCRIPTION      : This function delete comp link                     */
/*                                                                           */
/*     INPUT            : tCliHandle   CliHandle                             */
/*                        i4CompLinkIndex : Comp link index                  */
/*                        i4TeIfIndex : TeLink IfIndex                       */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_FAILURE/CLI_SUCCESS                            */
/*                                                                           */
/*****************************************************************************/
INT4
TlmCliDeleteCompLink (tCliHandle CliHandle, INT4 i4CompLinkIndex,
                      INT4 i4TeIfIndex)
{
    if (TlmPortStackTeLinkIfOrMplsIf (i4TeIfIndex, i4CompLinkIndex,
                                      DESTROY) == TLM_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to delete stack entry of TE link "
                   "over Component Link\r\n");
        return CLI_FAILURE;
    }
    if (TlmCliSetCompLinkDescRowStatus (CliHandle, i4CompLinkIndex, DESTROY) ==
        CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to delete Component link descriptor entry\r\n");
        return CLI_FAILURE;
    }
    if (TlmCliSetCompLinkBwRowStatus (CliHandle, i4CompLinkIndex, DESTROY) ==
        CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to delete Component link bandwidth entry\r\n");
        return CLI_FAILURE;
    }
    if (TlmCliSetCompLinkRowStatus (CliHandle, i4CompLinkIndex, DESTROY)
        == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to delete Component link entry\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name  :  TlmCliCreateTeLink                                       */
/*                                                                            */
/* Description    : This function create an entry in TELinkTable,             */
/*                  TeLinkDescriptor Table and TeLinkBwTable                  */
/*                                                                            */
/*  Input         : CliHandle - Cli Context Id                                */
/*                  pu1TeLinkName - TeLinkName                                */
/*                  i4BundleIfIndex - IfIndex of bundle link                  */
/*                  i4TeLinkType    - Type of a TE link                       */
/*  Output        :  None                                                     */
/*                                                                            */
/*  Returns       :  CLI_SUCCESS/CLI_FAILURE                                  */
/******************************************************************************/
INT4
TlmCliCreateTeLink (tCliHandle CliHandle, UINT1 *pu1TeLinkName,
                    INT4 i4BundleIfIndex, INT4 i4TeLinkType)
{
    tSNMP_OCTET_STRING_TYPE TeLinkName;
    tTlmTeLink         *pTeIface = NULL;
    tTlmTeLink         *pBundleTeIf = NULL;
    INT4                i4TeIfIndex = TLM_ZERO;
    UINT4               u4TeLinkCount = TLM_ZERO;
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4LinkType = TLM_ZERO;
    UINT4               u4MplsIfIndex = TLM_ZERO;

    TeLinkName.pu1_OctetList = pu1TeLinkName;
    TeLinkName.i4_Length = (INT4) TLM_STRLEN (pu1TeLinkName);

    /* Get the IfIndex from the given name and verify whether this TE link is 
     * associated with another bundle TE link*/

    if (TlmUtilGetIfIndexFromTeLinkName
        (pu1TeLinkName, (UINT1) TeLinkName.i4_Length,
         &i4TeIfIndex) == TLM_SUCCESS)
    {
        pTeIface = TlmFindTeIf (i4TeIfIndex);

        nmhGetFsTeLinkType (i4TeIfIndex, &i4LinkType);

        if (i4LinkType == TLM_UNBUNDLE)
        {
            if ((pTeIface != NULL) && (pTeIface->pBundledTeLink != NULL) &&
                (pTeIface->pBundledTeLink->i4IfIndex != i4BundleIfIndex))
            {
                CliPrintf (CliHandle, "\r%% Invalid bundle TE link and member "
                           "TE link association\r\n");
                return CLI_FAILURE;
            }
        }

        if (i4LinkType == TLM_UNBUNDLE)
        {
            gTlmCliArgs[CliHandle].i4TeIfIndex = i4TeIfIndex;
            gTlmCliArgs[CliHandle].i4TeLinkType = TLM_TELINK_INDEX;
        }
        else
        {
            gTlmCliArgs[CliHandle].i4TeBundleIndex = i4TeIfIndex;
            gTlmCliArgs[CliHandle].i4TeLinkType = TLM_BUNDLE_TELINK_INDEX;
        }

        i4RetVal = TlmCliChangePrompt (TLM_TRUE, TLM_TELINK_INDEX, i4TeIfIndex);
        return i4RetVal;
    }
    else
    {
        pBundleTeIf = TlmFindTeIf (i4BundleIfIndex);

        if ((pBundleTeIf != NULL) && (pBundleTeIf->i4TeLinkType == TLM_BUNDLE))
        {
            u4TeLinkCount = TMO_SLL_Count (&(pBundleTeIf->sortUnbundleTeList));
            /*Maximum of TE links can be added to a bundle is 32 */
            if (u4TeLinkCount > TLM_MAX_TE_LINKS_IN_BUNDLE)
            {
                CliPrintf (CliHandle, "\r%% Unable to associate more than"
                           "TLM_MAX_TE_LINKS_IN_BUNDLE in a bundle link \r\n");
                return CLI_FAILURE;
            }
        }

        /* Get a Free IfIndex from CFA for TE Link */

        if (CfaGetFreeInterfaceIndex ((UINT4 *) &i4TeIfIndex, CFA_TELINK) ==
            OSIX_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Next free avialable TE link IfIndex "
                       "does not exist\r\n");
            return CLI_FAILURE;
        }
        /*Create an entry in IfTable for TeLink interface */
        if (TlmPortCreateTeIfOrMplsIf ((UINT4) i4TeIfIndex, CFA_TELINK) ==
            TLM_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to create TE link interface\r\n");
            return CLI_FAILURE;
        }

        if (i4TeLinkType == TLM_UNBUNDLE)
        {
            gTlmCliArgs[CliHandle].i4TeIfIndex = i4TeIfIndex;
            gTlmCliArgs[CliHandle].i4TeLinkType = TLM_TELINK_INDEX;
        }
        else
        {
            gTlmCliArgs[CliHandle].i4TeBundleIndex = i4TeIfIndex;
            gTlmCliArgs[CliHandle].i4TeLinkType = TLM_BUNDLE_TELINK_INDEX;
        }

        /* Create an ifTable entry for the te link and get Te link index */
        if (TlmCliSetTeLinkRowStatus (CliHandle, i4TeIfIndex,
                                      CREATE_AND_WAIT) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to create TE link entry\r\n");
            return CLI_FAILURE;
        }
        if (TlmCliSetTeLinkName (CliHandle, i4TeIfIndex, TeLinkName) ==
            CLI_FAILURE)
        {
            /* set te-link and complink rowstatus as
             * destroy if set name return failure */
            TlmCliSetTeLinkRowStatus (CliHandle, i4TeIfIndex, DESTROY);
            CliPrintf (CliHandle, "\r%% Te link Name entry creation fails\r\n");
            return CLI_FAILURE;
        }
        if (i4TeLinkType == TLM_BUNDLE)
        {
            if (TlmCliSetTeLinkType (CliHandle, i4TeIfIndex, i4TeLinkType) ==
                CLI_FAILURE)
            {
                TlmCliSetTeLinkRowStatus (CliHandle, i4TeIfIndex, DESTROY);
                CliPrintf (CliHandle, "\r%% Unable to set TE link type\r\n");
                return CLI_FAILURE;
            }
        }

        if (TlmCliSetTeLinkDescRowStatus (CliHandle,
                                          i4TeIfIndex, CREATE_AND_WAIT)
            == CLI_FAILURE)
        {
            /*Te-Link RS set as destroy if descriptor entry creation is failure */
            CliPrintf (CliHandle, "\r%% Unable to create TE link descriptor "
                       "entry\r\n");
            TlmCliSetTeLinkRowStatus (CliHandle, i4TeIfIndex, DESTROY);
            return CLI_FAILURE;
        }
        if (TlmCliSetTeLinkDescRowStatus (CliHandle, i4TeIfIndex, ACTIVE)
            == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to activate TE link descriptor "
                       "entry\r\n");
            TlmCliSetTeLinkRowStatus (CliHandle, i4TeIfIndex, DESTROY);
            return CLI_FAILURE;
        }

        if (TlmCliSetTeLinkBwRowStatus
            (CliHandle, i4TeIfIndex, CREATE_AND_WAIT) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to create TE link bandwidth "
                       "entry\r\n");
            TlmCliSetTeLinkDescRowStatus (CliHandle, i4TeIfIndex, DESTROY);
            TlmCliSetTeLinkRowStatus (CliHandle, i4TeIfIndex, DESTROY);
            return CLI_FAILURE;
        }
        if (TlmCliSetTeLinkBwRowStatus
            (CliHandle, i4TeIfIndex, ACTIVE) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to activate TE link bandwidth "
                       "entry\r\n");
            TlmCliSetTeLinkDescRowStatus (CliHandle, i4TeIfIndex, DESTROY);
            TlmCliSetTeLinkRowStatus (CliHandle, i4TeIfIndex, DESTROY);
            return CLI_FAILURE;
        }

        /*For bundle TE link and unbundle TE link which is not member 
         * of bundle TE link, Create an MPLS interface and stack it over
         * TE link*/
        if (i4BundleIfIndex == TLM_ZERO)
        {
            /*Getting Free MPLS interface available */
            if (CfaGetFreeInterfaceIndex (&u4MplsIfIndex, CFA_MPLS) ==
                OSIX_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to get Free Mpls IfIndex\r\n");
                return CLI_FAILURE;
            }
            /*Creating MPLS interface */
            if (TlmPortCreateTeIfOrMplsIf (u4MplsIfIndex, CFA_MPLS) ==
                TLM_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to create MPLS interface\r\n");
                return CLI_FAILURE;
            }
            /*Stacking MPLS interface over Bundle TE link interface */
            if (TlmPortStackTeLinkIfOrMplsIf ((INT4) u4MplsIfIndex, i4TeIfIndex,
                                              CREATE_AND_WAIT) == TLM_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to Stack MPLS interface over TE link "
                           "interface\r\n");
                return CLI_FAILURE;
            }
            if (TlmPortStackTeLinkIfOrMplsIf ((INT4) u4MplsIfIndex, i4TeIfIndex,
                                              ACTIVE) == TLM_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to Stack MPLS interface over TE link "
                           "interface\r\n");
                return CLI_FAILURE;
            }
        }
        else
        {
            if (TlmPortStackTeLinkIfOrMplsIf (i4BundleIfIndex,
                                              i4TeIfIndex,
                                              CREATE_AND_WAIT) == TLM_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to Create bundle TE link "
                           "stack entry\r\n");
                return CLI_FAILURE;
            }
            if (TlmPortStackTeLinkIfOrMplsIf (i4BundleIfIndex,
                                              i4TeIfIndex,
                                              ACTIVE) == TLM_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to Activate bundle TE link"
                           " stack entry\r\n");
                return CLI_FAILURE;
            }

        }

    }

    i4RetVal = TlmCliChangePrompt (TLM_TRUE, TLM_TELINK_INDEX, i4TeIfIndex);
    return i4RetVal;
}

/*****************************************************************************/
/*  Function    :   TlmCliDeleteTeLink                                       */
/*                                                                           */
/*  Description :  This function deletes the entries in telinkname table     */
/*                 TeLinkDescTable and TeLinkBwTable of a TE link            */
/*                                                                           */
/*  Input       : CliHandle - Cli Context Id                                 */
/*                pu1TeLinkName - Te Link Name                               */
/*                i4TeIfIndex - TE link ifIndex                              */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/
INT4
TlmCliDeleteTeLink (tCliHandle CliHandle, UINT1 *pu1TeLinkName,
                    INT4 i4BundleIfIndex)
{
    UINT4               u4LowerStackIndex = TLM_ZERO;
    UINT4               u4ErrCode = 0;
    UINT1               u1TeLinkNameLen = TLM_ZERO;
    INT4                i4TeIfIndex = TLM_ZERO;
    INT4                i4HighStackIndex = TLM_ZERO;
    INT4                i4TeLinkType = TLM_ZERO;

    u1TeLinkNameLen = (UINT1) TLM_STRLEN (pu1TeLinkName);

    /* Get Te link index from the TE link name */
    if (TlmUtilGetIfIndexFromTeLinkName (pu1TeLinkName, u1TeLinkNameLen,
                                         &i4TeIfIndex) != TLM_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Unable to get TE link Ifindex "
                   "from TE link name\r\n");
        return CLI_FAILURE;
    }

    nmhGetFsTeLinkType (i4TeIfIndex, &i4TeLinkType);
    if (i4BundleIfIndex == TLM_ZERO)
    {
       CfaApiGetMplsIfFromTeLinkIf ((UINT4) i4TeIfIndex,
                                     (UINT4 *) &i4HighStackIndex, TRUE);
        if (i4HighStackIndex != TLM_ZERO)
        {
            CFA_LOCK ();
            if (nmhTestv2IfMainRowStatus (&u4ErrCode, i4HighStackIndex,
                                          DESTROY) == SNMP_FAILURE)
            {
                CFA_UNLOCK ();
                CliPrintf (CliHandle,
                           "\r%% Unable to delete MPLS interface \r\n");
                return CLI_FAILURE;
            }
            CFA_UNLOCK ();
        }
    }


    if (i4BundleIfIndex != TLM_ZERO)
    {
        if (TlmPortStackTeLinkIfOrMplsIf (i4BundleIfIndex, i4TeIfIndex,
                                          DESTROY) == TLM_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to Delete the Bundle TE link stacking\r\n");
            return CLI_FAILURE;
        }
    }
    if (i4TeLinkType == TLM_UNBUNDLE)
    {
        CfaApiGetL3IfFromTeLinkIf ((UINT4) i4TeIfIndex, &u4LowerStackIndex,
                                   TRUE);
        if (u4LowerStackIndex != TLM_ZERO)
        {
            TlmCliDeleteCompLink (CliHandle, (INT4) u4LowerStackIndex,
                                  i4TeIfIndex);
        }
    }

    if (i4BundleIfIndex == TLM_ZERO)
    {
        /*Getting the MPLS interface which is stacked over the TeLink interface */

        CfaApiGetMplsIfFromTeLinkIf ((UINT4) i4TeIfIndex,
                                     (UINT4 *) &i4HighStackIndex, TRUE);
        if (i4HighStackIndex != TLM_ZERO)
        {
            /*Deleting the stacking of MPLS interface */
            if (TlmPortStackTeLinkIfOrMplsIf (i4HighStackIndex,
                                              i4TeIfIndex,
                                              DESTROY) == TLM_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to delete MPLS interface "
                           "over TE link interface stacking\r\n");
                return CLI_FAILURE;
            }
            /*Deleting MPLS interface */
            if (TlmPortDeleteTeLinkIfOrMplsIf (i4HighStackIndex) == TLM_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to delete MPLS interface \r\n");
                return CLI_FAILURE;
            }
        }
    }

    /*Deleting TE link interface */
    if (TlmPortDeleteTeLinkIfOrMplsIf (i4TeIfIndex) == TLM_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to delete MPLS interface \r\n");
        return CLI_FAILURE;
    }
    /*Deleting the TE link entry */
    if (TlmCliSetTeLinkRowStatus (CliHandle, i4TeIfIndex, DESTROY) ==
        CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to delete TE link interface \r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  TlmCliSetTeLinkProperties                                 */
/*                                                                           */
/*  Description :  This function sets the objects of TE Link Table           */
/*                                                                           */
/*  Input       :  CliHandle       - Cli Context Id                          */
/*                 u4Command       - Command                                 */
/*                 pTeLinkCliArgs  - TE Link Cli Args                        */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/

INT4
TlmCliSetTeLinkProperties (tCliHandle CliHandle, UINT4 u4Command,
                           tTeLinkCliArgs * pTeLinkCliArgs)
{
    INT4                i4IfIndex = TLM_ZERO;
    INT4                i4RowStatus = TLM_ZERO;
    INT4                i4RetVal = TLM_ZERO;

    i4IfIndex = TlmCliGetIndices (CliHandle, TLM_LINK_TYPE (CliHandle));

    nmhGetTeLinkRowStatus (i4IfIndex, &i4RowStatus);

    if (i4RowStatus == ACTIVE)
    {
                CliPrintf (CliHandle, "\r%% Unable To Set Te-Link Parameters "
                           "as admin status is UP for Te-Link\r\n");
                return CLI_FAILURE;
    }
	else
	{
        TlmCliSetTeLinkRowStatus (CliHandle, i4IfIndex, NOT_IN_SERVICE);
	}

    switch (u4Command)
    {
        case TLM_CLI_TELINK_METRIC:
            i4RetVal = TlmCliSetTeLinkMetric (CliHandle,
                                              pTeLinkCliArgs->u4Metric,
                                              i4IfIndex);
            break;

        case TLM_CLI_TELINK_ADDRTYPE:
            i4RetVal = TlmCliSetTeLinkAddrType (CliHandle,
                                                pTeLinkCliArgs->i4AddrType,
                                                i4IfIndex);
            break;

        case TLM_CLI_TELINK_ROUTER_ID:

            i4RetVal = TlmCliSetTeLinkRemoteRouterId (CliHandle,
                                                      pTeLinkCliArgs->
                                                      u4RouterId, i4IfIndex);
            break;
        case TLM_CLI_TELINK_CHANNEL_TYPE:

            i4RetVal = TlmCliSetFsTeLinkInfoType (CliHandle,
                                                  pTeLinkCliArgs->
                                                  i4TeLinkInfoType, i4IfIndex);
            break;

        case TLM_CLI_TELINK_IFTYPE:

            i4RetVal = TlmCliSetFsTeLinkIfType (CliHandle,
                                                (UINT4) pTeLinkCliArgs->
                                                i4TeLinkIfType, i4IfIndex);
            break;

        case TLM_CLI_TELINK_NUM:

            i4RetVal = TlmCliSetTeLinkIpAddr (CliHandle,
                                              pTeLinkCliArgs->u4LocalIpAddr,
                                              pTeLinkCliArgs->u4RemoteIpAddr,
                                              i4IfIndex);
            break;

        case TLM_CLI_TELINK_UNNUM:

            i4RetVal = TlmCliSetTeLinkUnNum (CliHandle,
                                             pTeLinkCliArgs->u4LocalIdentifier,
                                             pTeLinkCliArgs->u4RemoteIdentifier,
                                             i4IfIndex);
            break;

        case TLM_CLI_TELINK_RESOURCE_CLASS:

            i4RetVal = TlmCliSetTeLinkRsrcCls (CliHandle,
                                               pTeLinkCliArgs->u4ResourceClass,
                                               i4IfIndex);
            break;

        case TLM_CLI_TELINK_NO_RESOURCE_CLASS:

            i4RetVal = TlmCliDeleteTeLinkRsrcCls (CliHandle,
                                                  pTeLinkCliArgs->
                                                  u4ResourceClass, i4IfIndex);
            break;

        case TLM_CLI_TELINK_PROTECTION_TYPE:

            i4RetVal = TlmCliSetTeLinkProtectionType (CliHandle,
                                                      pTeLinkCliArgs->
                                                      i4ProtectionType,
                                                      i4IfIndex);
            break;

        case TLM_CLI_TELINK_SRLG:
            i4RetVal =
                TlmCliSetTeLinkSrlgUp (CliHandle, pTeLinkCliArgs->u4TeLinkSrlg,
                                       i4IfIndex);
            break;
        case TLM_CLI_TELINK_NO_SRLG:
            i4RetVal =
                TlmCliSetTeLinkSrlgDown (CliHandle,
                                         pTeLinkCliArgs->u4TeLinkSrlg,
                                         i4IfIndex);
            break;

        case TLM_CLI_TELINK_ADVERTISE:
        case TLM_CLI_TELINK_NO_ADVERTISE:
            i4RetVal
                = TlmCliSetTeLinkAdvertise (CliHandle,
                                            pTeLinkCliArgs->i4TeLinkAdvertise,
                                            i4IfIndex);
            break;
        default:
            break;
    }

    return i4RetVal;
}

/*****************************************************************************/
/*  Function    :  TlmCliSetCompLinkDescProperties                           */
/*                                                                           */
/*  Description :  This function sets the objects of Comp Link Table         */
/*                                                                           */
/*  Input       :  CliHandle       - Cli Context Id                          */
/*                 u4Command       - Command                                 */
/*                 pCompLinkCliArgs - Comp Link Cli Args                     */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/

INT4
TlmCliSetCompLinkDescProperties (tCliHandle CliHandle, UINT4 u4Command,
                                 tCompLinkCliArgs * pCompLinkCliArgs)
{
    INT4                i4IfIndex = TLM_ZERO;
    INT4                i4RowStatus = TLM_ZERO;
	INT4				i4HighStackIndexRowStatus = TLM_ZERO;
    INT4                i4RetVal = TLM_ZERO;
	UINT4               u4HighStackIndex = TLM_ZERO;

    i4IfIndex = pCompLinkCliArgs->i4IfIndex;

    nmhGetComponentLinkDescrRowStatus (i4IfIndex, TLM_COMPLINK_DESCRID,
                                       &i4RowStatus);

	 /*Fetch Te-Link Interface from Physical Port Interface*/
	 CfaApiGetTeLinkIfFromL3If ((UINT4) i4IfIndex,
                               (UINT4 *) &u4HighStackIndex, TLM_ONE, TRUE);
	 nmhGetTeLinkRowStatus (u4HighStackIndex, &i4HighStackIndexRowStatus);
	
    if (i4HighStackIndexRowStatus == ACTIVE)
    {
		CliPrintf (CliHandle, "\r%% Unable To Set Te-Link Parameters "
                   "as admin status is UP for Te-Link\r\n");
        return CLI_FAILURE;

	}
	else
	{
        TlmCliSetCompLinkDescRowStatus (CliHandle, i4IfIndex, NOT_IN_SERVICE);
	}
    switch (u4Command)
    {
        case TLM_CLI_COMPLINK_SWCAP_ENCOD:
            i4RetVal = TlmCliSetCompLinkSwCapAndEncType (CliHandle,
                                                         pCompLinkCliArgs->
                                                         i4SwitchingCap,
                                                         pCompLinkCliArgs->
                                                         i4EncodingType,
                                                         i4IfIndex);
            break;

        case TLM_CLI_COMPLINK_MINLSPBW:
        case TLM_CLI_COMPLINK_NOMINLSPBW:

            i4RetVal = TlmCliSetCompLinkMinLspBw (CliHandle,
                                                  pCompLinkCliArgs->u4MinLsp,
                                                  i4IfIndex);
            break;
        default:
            break;
    }

    if (i4RowStatus == ACTIVE)
    {
        TlmCliSetCompLinkDescRowStatus (CliHandle, i4IfIndex, ACTIVE);
    }
    return i4RetVal;
}

/*****************************************************************************/
/*  Function    :   TlmCliSetTeLinkName                                      */
/*                                                                           */
/*  Description :  This function create a row in telinkname table            */
/*                                                                           */
/*  Input       : CliHandle : Cli Context Id,i4TeIfIndex                     */
/*                pu1TeLinkName : Te Link Name                               */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/

INT4
TlmCliSetTeLinkName (tCliHandle CliHandle, INT4 i4TeIfIndex,
                     tSNMP_OCTET_STRING_TYPE TeLinkName)
{
    UINT4               u4ErrorCode = TLM_ZERO;
    /* test and set  the value of Te link name corresponds to index  */
    if (nmhTestv2FsTeLinkName (&u4ErrorCode, i4TeIfIndex, &TeLinkName) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set TE link Name\n");
        return CLI_FAILURE;
    }

    nmhSetFsTeLinkName (i4TeIfIndex, &TeLinkName);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :   TlmCliSetTeLinkType                                     */
/*                                                                           */
/*  Description :  This function create a row in telinkname table            */
/*                                                                           */
/*  Input       : CliHandle : Cli Context Id,i4TeIfIndex                     */
/*                pu1TeLinkName : Te Link Name                               */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/
INT4
TlmCliSetTeLinkType (tCliHandle CliHandle, INT4 i4TeIfIndex, INT4 TeLinkType)
{
    UINT4               u4ErrorCode = TLM_ZERO;
    /* Test the value of Te link name for the index  */
    if (nmhTestv2FsTeLinkType (&u4ErrorCode, i4TeIfIndex, TeLinkType) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set TE link Type\n");
        return CLI_FAILURE;
    }

    /* Set the value of Te link name for the index  */
    nmhSetFsTeLinkType (i4TeIfIndex, TeLinkType);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :   TlmCliSetTeLinkRowStatus                                 */
/*                                                                           */
/*  Description :  This function create a row in telink table                */
/*                                                                           */
/*  Input       : CliHandle : Cli Context Id                                 */
/*                i4TeIfIndex: te interface index                           */
/*                i4RowStatus : Row status                                   */
/*  Output      : None                                                      */
/*                                                                           */
/*  Returns     :  CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/
INT4
TlmCliSetTeLinkRowStatus (tCliHandle CliHandle, INT4 i4TeIfIndex,
                          INT4 i4RowStatus)
{
    UINT4               u4ErrorCode = TLM_ZERO;
    /* Test the value of row status in TE Link Table */
    if (nmhTestv2TeLinkRowStatus (&u4ErrorCode, i4TeIfIndex,
                                  i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to Set TE link row status\n");
        return CLI_FAILURE;
    }

    /* Set the value of row status in TE Link Table */
    if (nmhSetTeLinkRowStatus (i4TeIfIndex, i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Unable to Set TE link row status\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :   TlmCliSetTeLinkBwRowStatus                        */
/*                                                                           */
/*  Description :  This function updates Row Status in telink bandwidth table*/
/*                                                                           */
/*  Input       : CliHandle : Cli Context Id                                 */
/*                i4TeIfIndex: TE link Ifindex                               */
/*                i4RowStatus : Row status                                   */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/
INT4
TlmCliSetTeLinkBwRowStatus (tCliHandle CliHandle, INT4 i4TeIfIndex,
                            INT4 i4RowStatus)
{
    UINT4               u4ErrorCode = TLM_ZERO;
    UINT1               u1Counter = TLM_ZERO;

    for (u1Counter = TLM_ZERO; u1Counter < TLM_MAX_TE_CLASS; u1Counter++)
    {
        /* Test the value of row status in TE Link BwTable */
        if (nmhTestv2TeLinkBandwidthRowStatus
            (&u4ErrorCode, i4TeIfIndex, u1Counter, i4RowStatus) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to Set TE link BW entry "
                       "row status\n");
            return CLI_FAILURE;
        }
        /* Set the value of row status in TE Link BwTable */
        if (nmhSetTeLinkBandwidthRowStatus
            (i4TeIfIndex, u1Counter, i4RowStatus) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Unable to Set TE link BW entry "
                       "row status\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  TlmCliSetTeLinkDescRowStatus                              */
/*                                                                           */
/*  Description :  This function create a row in telink Descriptor table     */
/*                                                                           */
/*  Input       : CliHandle - Cli Context Id                                 */
/*                i4TeIfIndex - te interface index                           */
/*                i4RowStatus - Row status                                   */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/
INT4
TlmCliSetTeLinkDescRowStatus (tCliHandle CliHandle, INT4 i4TeIfIndex,
                              INT4 i4RowStatus)
{
    UINT4               u4ErrorCode = TLM_ZERO;

    /* Test the value of row status in TE Link Descriptor Table */
    if (nmhTestv2TeLinkDescrRowStatus
        (&u4ErrorCode, i4TeIfIndex, TLM_TELINK_DESCRID,
         i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to Set TE link Descriptor entry "
                   "row status\n");
        return CLI_FAILURE;
    }

    /* Set the value of row status in TE Link Descriptor Table */
    if (nmhSetTeLinkDescrRowStatus
        (i4TeIfIndex, TLM_TELINK_DESCRID, i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Unable to Set TE link Descriptor entry "
                   "row status\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :   TlmCliSetTeLinkMetric                                    */
/*                                                                           */
/*  Description :  This function set the metric value  of telink table       */
/*                                                                           */
/*  Input       : CliHandle : Cli Context Id                                 */
/*                u4MetricValue : metric of TeLink                           */
/*                i4TeIfIndex : TE link IfIndex                              */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/
INT4
TlmCliSetTeLinkMetric (tCliHandle CliHandle, UINT4 u4MetricValue,
                       INT4 i4TeIfIndex)
{
    UINT4               u4ErrorCode = TLM_ZERO;

    if (nmhTestv2TeLinkMetric (&u4ErrorCode, i4TeIfIndex, u4MetricValue)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set TE link metric value\n");
        return CLI_FAILURE;
    }

    nmhSetTeLinkMetric (i4TeIfIndex, u4MetricValue);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :   TlmCliSetTeLinkAddrType                                  */
/*                                                                           */
/*  Description :  This function set the address type of telink table        */
/*                  as ipv4,ipv6,un-known                                    */
/*                                                                           */
/*  Input       : CliHandle : Cli Context Id                                 */
/*                i4AddrType : AddressType of TeLink                         */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/
INT4
TlmCliSetTeLinkAddrType (tCliHandle CliHandle, INT4 i4AddrType,
                         INT4 i4TeIfIndex)
{
    UINT4               u4ErrorCode = TLM_ZERO;

    /* Test and set Address type of Te link */
    if (nmhTestv2TeLinkAddressType
        (&u4ErrorCode, i4TeIfIndex, i4AddrType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set TE link Address type\n");
        return CLI_FAILURE;
    }

    nmhSetTeLinkAddressType (i4TeIfIndex, i4AddrType);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :   TlmCliSetTeLinkRemoteRouterId                            */
/*                                                                           */
/*  Description :  This function set the Remote router id                    */
/*                  for te link                                              */
/*                                                                           */
/*  Input       :  CliHandle : Cli context id                                */
/*                 u4RemoteRtrId : Remote router id                          */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     : CLI_SUCCESS/CLI_FAILURE                                    */
/*****************************************************************************/
INT4
TlmCliSetTeLinkRemoteRouterId (tCliHandle CliHandle, UINT4 u4RemoteRtrId,
                               INT4 i4TeIfIndex)
{
    UINT4               u4ErrorCode = TLM_ZERO;

    if (nmhTestv2FsTeLinkRemoteRtrId
        (&u4ErrorCode, i4TeIfIndex, u4RemoteRtrId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set remote router id\r\n");
        return CLI_FAILURE;
    }

    nmhSetFsTeLinkRemoteRtrId (i4TeIfIndex, u4RemoteRtrId);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :   TlmCliSetFsTeLinkInfoType                                */
/*                                                                           */
/*  Description :  This function sets TE Link Info Type (control channel     */
/*                 separation) for the TE Link                               */
/*                                                                           */
/*  Input       :  CliHandle        : Cli context id                         */
/*                 i4TeLinkInfoType : TE Link Info Type                      */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     : CLI_SUCCESS/CLI_FAILURE                                    */
/*****************************************************************************/
INT4
TlmCliSetFsTeLinkInfoType (tCliHandle CliHandle, INT4 i4TeLinkInfoType,
                           INT4 i4TeIfIndex)
{
    UINT4               u4ErrorCode = TLM_ZERO;

    if (nmhTestv2FsTeLinkInfoType
        (&u4ErrorCode, i4TeIfIndex, (INT4) i4TeLinkInfoType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to Set Te Link Infotype value\r\n");
        return CLI_FAILURE;
    }

    nmhSetFsTeLinkInfoType (i4TeIfIndex, i4TeLinkInfoType);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :   TlmCliSetFsTeLinkIfType                                  */
/*                                                                           */
/*  Description :  This function sets TE Link If Type (Point-to-Point or     */
/*                 MultiAccess)  for the TE Link                             */
/*                                                                           */
/*  Input       :  CliHandle      : Cli context id                           */
/*                 i4TeLinkIfType : TE Link Info Type                        */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     : CLI_SUCCESS/CLI_FAILURE                                    */
/*****************************************************************************/
INT4
TlmCliSetFsTeLinkIfType (tCliHandle CliHandle, UINT4 i4TeLinkIfType,
                         INT4 i4TeIfIndex)
{
    UINT4               u4ErrorCode = TLM_ZERO;

    if (nmhTestv2FsTeLinkIfType
        (&u4ErrorCode, i4TeIfIndex, (INT4) i4TeLinkIfType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set TE link" " Iftype value\r\n");
        return CLI_FAILURE;
    }

    nmhSetFsTeLinkIfType (i4TeIfIndex, i4TeLinkIfType);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :   TlmCliSetTeLinkIpAddr                                    */
/*                                                                           */
/*  Description :  This function set local and remote ip address of Te Link  */
/*                                                                           */
/*  Input       : CliHandle : Cli Context Id                                 */
/*                u4LocalIpAddr : Local Ip Address TeLink                    */
/*                u4RemoteIpAddr : Remote Ip Address TeLink                  */
/*                i4TeIfIndex : Te Link IfIndex                              */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/
INT4
TlmCliSetTeLinkIpAddr (tCliHandle CliHandle, UINT4 u4LocalIpAddr,
                       UINT4 u4RemoteIpAddr, INT4 i4TeIfIndex)
{
    UINT4               u4ErrorCode = TLM_ZERO;
    UINT1               au1Array1[TLM_MAX_IP_LEN];
    UINT1               au1Array2[TLM_MAX_IP_LEN];
    tSNMP_OCTET_STRING_TYPE LocalIp;
    tSNMP_OCTET_STRING_TYPE RemoteIp;

    MEMSET (au1Array1, TLM_ZERO, sizeof (au1Array1));
    MEMSET (au1Array2, TLM_ZERO, sizeof (au1Array2));

    LocalIp.pu1_OctetList = au1Array1;
    TLM_INTEGER_TO_OCTETSTRING (u4LocalIpAddr, (&LocalIp));

    RemoteIp.pu1_OctetList = au1Array2;
    TLM_INTEGER_TO_OCTETSTRING (u4RemoteIpAddr, (&RemoteIp));

    /* Test Local ip address */
    if (nmhTestv2TeLinkLocalIpAddr (&u4ErrorCode, i4TeIfIndex, &LocalIp)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set te link"
                   " Local ip Address value\r\n");
        return CLI_FAILURE;
    }

    /* Test Remote ip address */
    if (nmhTestv2TeLinkRemoteIpAddr (&u4ErrorCode, i4TeIfIndex,
                                     &RemoteIp) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set te link"
                   " remote ip Address value\r\n");
        return CLI_FAILURE;
    }

    nmhSetTeLinkLocalIpAddr (i4TeIfIndex, &LocalIp);

    nmhSetTeLinkRemoteIpAddr (i4TeIfIndex, &RemoteIp);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :   TlmCliSetTeLinkUnNum                                     */
/*                                                                           */
/*  Description :  This function set the local and remote identifier         */
/*                  for te link                                              */
/*                                                                           */
/*  Input       :  CliHandle : Cli context id                                */
/*                 u4OutgoingId : Outgoing identifier                        */
/*                 u4IncomingId : Incoming identifier                        */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     : CLI_SUCCESS/CLI_FAILURE                                    */
/*****************************************************************************/
INT4
TlmCliSetTeLinkUnNum (tCliHandle CliHandle, UINT4 u4OutgoingId,
                      UINT4 u4IncomingId, INT4 i4TeIfIndex)
{
    UINT4               u4ErrorCode = TLM_ZERO;

    /* Test incoming identifier  value */
    if (nmhTestv2TeLinkIncomingIfId
        (&u4ErrorCode, i4TeIfIndex, (INT4) u4IncomingId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set te link"
                   " remote identifier value\r\n");
        return CLI_FAILURE;
    }

    /* Test Outgoing identifier value */
    if (nmhTestv2TeLinkOutgoingIfId (&u4ErrorCode, i4TeIfIndex,
                                     (INT4) u4OutgoingId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set te link"
                   " local identifier value\r\n");
        return CLI_FAILURE;
    }

    nmhSetTeLinkIncomingIfId (i4TeIfIndex, u4IncomingId);

    nmhSetTeLinkOutgoingIfId (i4TeIfIndex, u4OutgoingId);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :   TlmCliSetTeLinkRsrcCls                                   */
/*                                                                           */
/*  Description :  This function set the resource class value                */
/*                  for te link                                              */
/*                                                                           */
/*  Input       :  CliHandle : Cli context id                                */
/*                 pu1Class  : Resource Class                                */
/*                 i4TeIfIndex : TE link IfIndex                             */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     : CLI_SUCCESS/CLI_FAILURE                                    */
/*****************************************************************************/

INT4
TlmCliSetTeLinkRsrcCls (tCliHandle CliHandle, UINT4 u4GetRsrcClsVal,
                        INT4 i4TeIfIndex)
{
    UINT4               u4ErrorCode = TLM_ZERO;
    UINT4               u4TeLinkRsrcCls = TLM_ZERO;

    /* Converting Hex string to decimal */
    nmhGetTeLinkResourceClass (i4TeIfIndex, &u4TeLinkRsrcCls);

    u4TeLinkRsrcCls = u4TeLinkRsrcCls | u4GetRsrcClsVal;

    /* Test the Resource Class of Te link */
    if (nmhTestv2TeLinkResourceClass
        (&u4ErrorCode, i4TeIfIndex, u4TeLinkRsrcCls) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set te link"
                   " resource class value\r\n");
        return CLI_FAILURE;
    }

    /* Set the Resource Class of Te link */
    nmhSetTeLinkResourceClass (i4TeIfIndex, u4TeLinkRsrcCls);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :   TlmCliSetTeLinkProtectionType                            */
/*                                                                           */
/*  Description :  This function set the Protection type of telink table     */
/*                                                                           */
/*  Input       : CliHandle : Cli Context Id                                 */
/*                i4AddrType : AddressType of TeLink                         */
/*                i4TeIfIndex : TE link IfIndex                              */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/
INT4
TlmCliSetTeLinkProtectionType (tCliHandle CliHandle, INT4 i4ProtectionType,
                               INT4 i4TeIfIndex)
{
    UINT4               u4ErrorCode = TLM_ZERO;

    /* Test the protection type of Te link */
    if (nmhTestv2TeLinkProtectionType
        (&u4ErrorCode, i4TeIfIndex, i4ProtectionType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set te link"
                   " Protetction type value\r\n");
        return CLI_FAILURE;
    }

    /* Set the protection type of Te link */
    nmhSetTeLinkProtectionType (i4TeIfIndex, i4ProtectionType);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  TlmCliSetTeLinkSrlgUp                                     */
/*                                                                           */
/*  Description :  This function set the  Rowstatus of srlg values of the    */
/*                  TE link as ACTIVE                                        */
/*                                                                           */
/*  Input       : CliHandle : Cli Context Id                                 */
/*                u4TeLinkSrlg : Srlg no of TeLink                           */
/*                i4TeIfIndex : Telink IfIndex                               */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/
INT4
TlmCliSetTeLinkSrlgUp (tCliHandle CliHandle, UINT4 u4TeLinkSrlg,
                       INT4 i4TeIfIndex)
{
    UINT4               u4ErrorCode = TLM_ZERO;

    /* Test the Rowstatus of Srlg Table */
    if (nmhTestv2TeLinkSrlgRowStatus (&u4ErrorCode, i4TeIfIndex,
                                      u4TeLinkSrlg,
                                      CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set TE Link Srlg RowStatus\r\n");
        return CLI_FAILURE;
    }

    /* Set the Rowstatus of Srlg Table */
    if (nmhSetTeLinkSrlgRowStatus (i4TeIfIndex, u4TeLinkSrlg, CREATE_AND_WAIT)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Unable to set TE link srlg RowStatus\r\n");
        return CLI_FAILURE;
    }

    /* Set the Rowstatus of Srlg Table */
    nmhSetTeLinkSrlgRowStatus (i4TeIfIndex, u4TeLinkSrlg, ACTIVE);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  TlmCliSetTeLinkSrlgDown                                   */
/*                                                                           */
/*  Description :  This function is used to Delete the SRLG entries of a     */
/*                 TE link                                                   */
/*                                                                           */
/*  Input       : CliHandle : Cli Context Id                                 */
/*                u4TeLinkSrlg : Srlg no of TeLink                           */
/*                i4TeIfIndex : Telink IfIndex                               */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/
INT4
TlmCliSetTeLinkSrlgDown (tCliHandle CliHandle, UINT4 u4TeLinkSrlg,
                         INT4 i4TeIfIndex)
{
    UINT4               u4ErrorCode = TLM_ZERO;

    /* Test the Rowstatus of Srlg Table */
    if (nmhTestv2TeLinkSrlgRowStatus (&u4ErrorCode, i4TeIfIndex,
                                      u4TeLinkSrlg, DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set TE link srlg RowStatus\r\n");
        return CLI_FAILURE;
    }

    /* Set the Rowstatus of Srlg Table */
    nmhSetTeLinkSrlgRowStatus (i4TeIfIndex, u4TeLinkSrlg, DESTROY);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :   TlmCliDeleteTeLinkRsrcCls                                */
/*                                                                           */
/*  Description :  This function Delete the value of the Resourse class      */
/*                 of te link values : 0-31                                  */
/*                                                                           */
/*  Input       : CliHandle : Cli Context Id                                 */
/*                u4RsrcClsType : Resource class of TeLink                   */
/*                i4TeIfIndex : Telink IfIndex                               */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/
INT4
TlmCliDeleteTeLinkRsrcCls (tCliHandle CliHandle, UINT4 u4GetRsrcClsVal,
                           INT4 i4TeIfIndex)
{
    UINT4               u4ErrorCode = TLM_ZERO;
    UINT4               u4TeLinkRsrcCls = TLM_ZERO;
    UINT4               u4PrevTeLinkRsrcCls = TLM_ZERO;
	

    nmhGetTeLinkResourceClass (i4TeIfIndex, &u4TeLinkRsrcCls);

    /* Deleting the set bit for the given class type */
	if (u4GetRsrcClsVal > u4TeLinkRsrcCls)
	{
        CliPrintf (CliHandle, "\r%% Unable to reset te link"
                   " resource class value,configured values is less than reset value\r\n");
		return CLI_FAILURE;
	}
	u4PrevTeLinkRsrcCls = u4TeLinkRsrcCls;
    u4TeLinkRsrcCls = u4TeLinkRsrcCls & (~u4GetRsrcClsVal);
	if (u4TeLinkRsrcCls == u4PrevTeLinkRsrcCls)
	{
        CliPrintf (CliHandle, "\r%% Warning:Resource Class value is"
                   " invalid,bits are not set\r\n");
	}

    /* Test the Resource Class of Te link */
    if (nmhTestv2TeLinkResourceClass
        (&u4ErrorCode, i4TeIfIndex, u4TeLinkRsrcCls) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set te link"
                   " resource class value\r\n");
        return CLI_FAILURE;
    }

    /* Set the Resource Class of Te link */
    nmhSetTeLinkResourceClass (i4TeIfIndex, u4TeLinkRsrcCls);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :   TlmCliSetCompLinkRowStatus                               */
/*                                                                           */
/*  Description :  This function set Complink RS                             */
/*                                                                           */
/*  Input       : CliHandle   - Cli context id                               */
/*                i4RowStatus -  Row status value                            */
/*                i4CompLinkIndex - Component interface  Index               */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/
INT4
TlmCliSetCompLinkRowStatus (tCliHandle CliHandle, INT4 i4CompLinkIndex,
                            INT4 i4RowStatus)
{
    UINT4               u4ErrorCode = TLM_ZERO;
    /* Test and set the value of rowstatus in component link as */
    if (nmhTestv2ComponentLinkRowStatus
        (&u4ErrorCode, i4CompLinkIndex, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set component link"
                   " Row Status\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetComponentLinkRowStatus (i4CompLinkIndex, i4RowStatus) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set component link"
                   " Row Status\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  TlmCliSetCompLinkDescRowStatus                            */
/*                                                                           */
/*  Description :  This function create a row in Complink Descriptor table   */
/*                                                                           */
/*  Input       :  CliHandle - Cli context ID                                */
/*                 i4RowStatus -  Row status value                           */
/*                 i4CompLinkIndex - Component interface Index               */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/
INT4
TlmCliSetCompLinkDescRowStatus (tCliHandle CliHandle, INT4 i4CompLinkIndex,
                                INT4 i4RowStatus)
{
    UINT4               u4ErrorCode = TLM_ZERO;

    /* Test and Set the value of row status in discreptor Table */
    if (nmhTestv2ComponentLinkDescrRowStatus
        (&u4ErrorCode, i4CompLinkIndex, TLM_COMPLINK_DESCRID,
         i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set component link"
                   " Row Status\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetComponentLinkDescrRowStatus
        (i4CompLinkIndex, TLM_COMPLINK_DESCRID, i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set component link"
                   " Row Status\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  TlmCliSetCompLinkBwRowStatus                       */
/*                                                                           */
/*  Description :  This function create a row in Complink Bandwidth table    */
/*                                                                           */
/*  Input       :  CliHandle - Cli context id                                */
/*                 i4RowStatus -  Row status value                            */
/*                i4CompLinkIndex - Component interface  Index               */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/
INT4
TlmCliSetCompLinkBwRowStatus (tCliHandle CliHandle, INT4 i4CompLinkIndex,
                              INT4 i4RowStatus)
{
    UINT4               u4ErrorCode = TLM_ZERO;
    UINT1               u1Counter = TLM_ZERO;

    for (u1Counter = TLM_ZERO; u1Counter < TLM_MAX_TE_CLASS; u1Counter++)
    {
        if (nmhTestv2ComponentLinkBandwidthRowStatus
            (&u4ErrorCode, i4CompLinkIndex, u1Counter,
             i4RowStatus) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set component link"
                       " BW Row Status\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetComponentLinkBandwidthRowStatus
            (i4CompLinkIndex, u1Counter, i4RowStatus) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set component link"
                       " BW Row Status\r\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  TlmCliSetCompLinkSwCapAndEncType                          */
/*                                                                           */
/*  Description :  This function set the switch capably and encoding         */
/*                  type of Componenlink (sw-psc1,Enctype (Packet/Ethernet)) */
/*                                                                           */
/*  Input       : CliHandle : Cli Context Id                                 */
/*                i4SwitchCapabilty : Switch Capability  of TeLink           */
/*                i4EncType : Encoding Type of TeLink                        */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/
INT4
TlmCliSetCompLinkSwCapAndEncType (tCliHandle CliHandle, INT4 i4SwitchCapabilty,
                                  INT4 i4EncType, INT4 i4CompIfIndex)
{
    UINT4               u4ErrorCode = TLM_ZERO;

    /* Descriptor id is taken by default as 1
     * as only one disrc is there currently */
    /* Test switch capability  of Te link */
    if (nmhTestv2ComponentLinkDescrSwitchingCapability
        (&u4ErrorCode, i4CompIfIndex, TLM_COMPLINK_DESCRID,
         i4SwitchCapabilty) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set Comp link"
                   " switch capability value\r\n");
        return CLI_FAILURE;
    }

    /* Test Encoding Type of Te link */
    if (nmhTestv2ComponentLinkDescrEncodingType
        (&u4ErrorCode, i4CompIfIndex, TLM_COMPLINK_DESCRID,
         i4EncType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set Comp link"
                   " encoding value\r\n");
        return CLI_FAILURE;
    }

    nmhSetComponentLinkDescrSwitchingCapability (i4CompIfIndex,
                                                 TLM_COMPLINK_DESCRID,
                                                 i4SwitchCapabilty);

    nmhSetComponentLinkDescrEncodingType (i4CompIfIndex, TLM_COMPLINK_DESCRID,
                                          i4EncType);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  TlmCliSetCompLinkMaxRsrvableBw                            */
/*                                                                           */
/*  Description :  This function set max reservable bandwidth of comp link   */
/*                                                                           */
/*  Input       :  CliHandle : Cli Context id                                */
/*                 pu1MaxResrvableBw : Max reservable Bandwidth              */
/*                 i4CompIfIndex : Component link IfIndex                    */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/
INT4
TlmCliSetCompLinkMaxRsrvableBw (tCliHandle CliHandle, UINT4 u4MaxResrvableBw,
                                INT4 i4CompIfIndex)
{
    UINT4               u4ErrorCode = TLM_ZERO;
	UINT4				u4HighStackIndex = TLM_ZERO;
    UINT1               au1Array[TLM_FOUR];
    FLT4                f4MaxResBw = TLM_ZERO;
    tSNMP_OCTET_STRING_TYPE MaxResrvableBw;
    INT4                i4CmpLinkRowStatus = TLM_ZERO;
    INT4                i4DescRowStatus = TLM_ZERO;
	INT4 				i4HighStackIndexRowStatus = TLM_ZERO;

    MEMSET ((&MaxResrvableBw), TLM_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1Array, TLM_ZERO, sizeof (au1Array));
    MaxResrvableBw.pu1_OctetList = &au1Array[TLM_INDEX_ZERO];
    MaxResrvableBw.i4_Length = TLM_FOUR;

    nmhGetComponentLinkRowStatus (i4CompIfIndex, &i4CmpLinkRowStatus);
	CfaApiGetTeLinkIfFromL3If ((UINT4) i4CompIfIndex,
                               (UINT4 *) &u4HighStackIndex, TLM_ONE, TRUE);
    nmhGetTeLinkRowStatus (u4HighStackIndex, &i4HighStackIndexRowStatus);

    if (i4HighStackIndexRowStatus == ACTIVE)
    {
		 CliPrintf (CliHandle, "\r%% Unable To Set Te-Link Parameters "
                   "as admin status is UP for Te-Link\r\n");
        return CLI_FAILURE;

    }
	else
	{
        nmhSetComponentLinkRowStatus (i4CompIfIndex, NOT_IN_SERVICE);
	}

    nmhGetComponentLinkDescrRowStatus (i4CompIfIndex, TLM_COMPLINK_DESCRID,
                                       &i4DescRowStatus);
    if (i4DescRowStatus == ACTIVE)
    {
        nmhSetComponentLinkDescrRowStatus (i4CompIfIndex, TLM_COMPLINK_DESCRID,
                                           NOT_IN_SERVICE);
    }
    
    /*Type casting Integer to Float */
    f4MaxResBw = (FLT4) u4MaxResrvableBw;

    /*Converting Kbps to Bytes per second */
    TLM_CONVERT_KBPS_TO_BYTES_PER_SEC (f4MaxResBw);

    /* Typecast from Float to OctetString */
    FLOAT_TO_OCTETSTRING (f4MaxResBw, (&MaxResrvableBw));

    /* Test and Set the Max reservable  bandwidth value in Component link Table */
    if (nmhTestv2ComponentLinkMaxResBandwidth
        (&u4ErrorCode, i4CompIfIndex, &MaxResrvableBw) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set Comp link"
                   " max resevable bandwidth value\r\n");
        return CLI_FAILURE;
    }

    nmhSetComponentLinkMaxResBandwidth (i4CompIfIndex, &MaxResrvableBw);

    if (i4CmpLinkRowStatus == ACTIVE)
    {
        nmhSetComponentLinkRowStatus (i4CompIfIndex, ACTIVE);
        
    }
   
    if(i4DescRowStatus == ACTIVE)
    {
        nmhSetComponentLinkDescrRowStatus (i4CompIfIndex, TLM_COMPLINK_DESCRID,
                                           ACTIVE);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  TlmCliSetCompLinkMinLspBw                                 */
/*                                                                           */
/*  Description :  This function set the minimum lsp bandwidth of component  */
/*                  link                                                     */
/*                                                                           */
/*  Input       :  CliHandle -Cli context handler                            */
/*                 pu1MinLspBw - Min lsp bw                                  */
/*                 i4CompIfIndex - Component link IfIndex                    */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
INT4
TlmCliSetCompLinkMinLspBw (tCliHandle CliHandle, UINT4 u4MinLspBw,
                           INT4 i4CompIfIndex)
{
    tSNMP_OCTET_STRING_TYPE MinLspBw;
    UINT4               u4ErrorCode = TLM_ZERO;
    UINT1               au1Array[TLM_FOUR];
    FLT4                f4MinLspBw = TLM_ZERO;

    static UINT1        au1MinLspBw[TLM_FOUR];

    MEMSET (au1Array, TLM_ZERO, sizeof (au1Array));
    MEMSET ((&MinLspBw), TLM_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

    MinLspBw.pu1_OctetList = au1MinLspBw;
    MinLspBw.i4_Length = TLM_FOUR;
    /*Typecasting Integer to Float */
    f4MinLspBw = (FLT4) u4MinLspBw;

    /*Converting Kbps to Bytes per second */
    TLM_CONVERT_KBPS_TO_BYTES_PER_SEC (f4MinLspBw);

    /* Typecast from Float to OctetString */
    FLOAT_TO_OCTETSTRING (f4MinLspBw, (&MinLspBw));

    /* Descriptor id is taken by default as 1 as only
     * one Descriptor is considered currently */
    /* Test and Set the min lsp bandwidth value in discreptor Table */
    if (nmhTestv2ComponentLinkDescrMinLspBandwidth
        (&u4ErrorCode, i4CompIfIndex, TLM_COMPLINK_DESCRID,
         &MinLspBw) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set Comp link"
                   " min lsp bandwidth value\r\n");
        return CLI_FAILURE;
    }

    nmhSetComponentLinkDescrMinLspBandwidth (i4CompIfIndex,
                                             TLM_COMPLINK_DESCRID, &MinLspBw);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :   TlmCliShowTeLink                                         */
/*                                                                           */
/*  Description :  This function Show the various interface and values       */
/*                 in te link                                                */
/*                                                                           */
/*  Input       :  tCliHandle : CliHandle                                    */
/*                 u4Type : Command type                                     */
/*                 pu1TeLinkName: Telink name                                */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
INT4
TlmCliShowTeLink (tCliHandle CliHandle, UINT4 u4Type, UINT1 *pu1TeLinkName)
{
    INT4                i4TeIfIndex = TLM_ZERO;
    UINT1               u1TeLinkNameLen = TLM_ZERO;

    UNUSED_PARAM (u4Type);

    if (pu1TeLinkName != NULL)
    {
        u1TeLinkNameLen = (UINT1) TLM_STRLEN (pu1TeLinkName);

        if (TlmUtilGetIfIndexFromTeLinkName (pu1TeLinkName, u1TeLinkNameLen,
                                             &i4TeIfIndex) != TLM_SUCCESS)
        {
	     CliPrintf (CliHandle, "\r\n%%Invalid te-link\n");
            return CLI_FAILURE;
        }

        return (TlmCliShowSpecificTeLink (CliHandle, i4TeIfIndex));
    }

    /* getting 1st index of Te link table */
    if (nmhGetFirstIndexTeLinkTable (&i4TeIfIndex) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Loop to get information of all Te links */
    do
    {
        TlmCliShowSpecificTeLink (CliHandle, i4TeIfIndex);
    }
    while (nmhGetNextIndexTeLinkTable (i4TeIfIndex, &i4TeIfIndex)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :   TlmCliShowSpecificTeLink                                 */
/*                                                                           */
/*  Description :  This function Show the various interface and values       */
/*                 in te link                                                */
/*                                                                           */
/*  Input       :  tCliHandle : CliHandle                                    */
/*                 i4TeIfIndex - TE Link If Index                            */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
INT4
TlmCliShowSpecificTeLink (tCliHandle CliHandle, INT4 i4TeIfIndex)
{
    CHR1               *pu1String = NULL;
    FLT4                f4TeLinkMaxResBw = TLM_ZERO;
    FLT4                f4TeLinkMaxBw = TLM_ZERO;
    FLT4                f4TeLinkUnresBwPri = TLM_ZERO;
    UINT4               u4BwPriLvl = TLM_ZERO;
    UINT4               u4TeLinkMetric = TLM_ZERO;
    UINT4               u4RemoteIpAddr = TLM_ZERO;
    UINT4               u4LocalIpAddr = TLM_ZERO;
    UINT4               u4TeLinkSrlg = TLM_ZERO;
    UINT4               u4TeLinkRemoteRtrId = TLM_ZERO;
    UINT4               u4ResourceClass = TLM_ZERO;
    UINT4               u4TeLinkWorkingPriority = TLM_ZERO;
    UINT4               u4HighIfIndex = TLM_ZERO;
    INT4                i4TeIncomingIfId = TLM_ZERO;
    INT4                i4TeOutgoingIfId = TLM_ZERO;
    INT4                i4TeLinkProtectionType = TLM_ZERO;
    INT4                i4TeLinkInfoType = TLM_ZERO;
    INT4                i4TeLinkIfType = TLM_ZERO;
    INT4                i4TeLinkAddressType = TLM_ZERO;
    INT4                i4TempTeIfIndex = TLM_ZERO;
    INT4                i4RowStatus = TLM_ZERO;
    INT4                i4TeLinkType = TLM_ZERO;
    UINT1               au1NameArray[TLM_MAX_NAME_LEN];
    UINT1               au1IpArray[TLM_MAX_IP_LEN];
    UINT1               au1BwArray[TLM_FOUR];
    UINT1               au1RsrcClsName[TLM_MAX_RESOURCE_CLASS_NAME_LEN];
    UINT1               u1RsrcClsVlaue = TLM_ZERO;
    UINT1               u1Count = TLM_ZERO;
    UINT1               u1OperStatus = TLM_ZERO;
    BOOL1               bHdrReqd = TRUE;

    tSNMP_OCTET_STRING_TYPE TeLinkName;
    tSNMP_OCTET_STRING_TYPE TeLinkLocalIp;
    tSNMP_OCTET_STRING_TYPE TeLinkRemoteIp;
    tSNMP_OCTET_STRING_TYPE TeLinkMaxResBw;
    tSNMP_OCTET_STRING_TYPE TeLinkMaxBw;
    tSNMP_OCTET_STRING_TYPE TeLinkUnresBwPri;

    /* MEMSET all the local structures */

    MEMSET (&TeLinkName, TLM_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TeLinkLocalIp, TLM_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TeLinkRemoteIp, TLM_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TeLinkMaxResBw, TLM_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TeLinkMaxBw, TLM_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TeLinkUnresBwPri, TLM_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1RsrcClsName, '\0',
            sizeof (UINT1) * TLM_MAX_RESOURCE_CLASS_NAME_LEN);

    TeLinkName.pu1_OctetList = au1NameArray;

    MEMSET (TeLinkName.pu1_OctetList, TLM_ZERO, TLM_MAX_NAME_LEN);

    /* Getting the TE-Link name */
    nmhGetFsTeLinkName (i4TeIfIndex, &TeLinkName);

    nmhGetTeLinkRowStatus (i4TeIfIndex, &i4RowStatus);
    nmhGetFsTeLinkType (i4TeIfIndex, &i4TeLinkType);

    CfaGetIfOperStatus ((UINT4) i4TeIfIndex, &u1OperStatus);

    CliPrintf (CliHandle, "\r\n Te-Link Name: %s, Index: %d, Admin: ",
               TeLinkName.pu1_OctetList, i4TeIfIndex);

    if (i4RowStatus == ACTIVE)
    {
        CliPrintf (CliHandle, "Up");
    }
    else
    {
        CliPrintf (CliHandle, "Down");
    }

    CliPrintf (CliHandle, ", Oper: ");

    if (u1OperStatus == CFA_IF_UP)
    {
        CliPrintf (CliHandle, "Up");
    }
    else
    {
        CliPrintf (CliHandle, "Down");
    }

    /*Getting the address type of TE link */
    nmhGetTeLinkAddressType (i4TeIfIndex, &i4TeLinkAddressType);

    if (i4TeLinkAddressType == TLM_CLI_ADDRTYPE_UNKNOWN)
    {
        /* Getting Incoming Interface Id of Te link */
        nmhGetTeLinkOutgoingIfId (i4TeIfIndex, &i4TeIncomingIfId);

        CliPrintf (CliHandle, "\r\n Local Identifier: %d,", i4TeIncomingIfId);

        /* Getting Outgoing  Interface Id of Te link */
        nmhGetTeLinkIncomingIfId (i4TeIfIndex, &i4TeOutgoingIfId);

        CliPrintf (CliHandle, " Remote Identifier: %d,", i4TeOutgoingIfId);
    }
    else
    {
        /* Getting Local ip address of Te link */
        TeLinkLocalIp.pu1_OctetList = &au1IpArray[TLM_INDEX_ZERO];
        MEMSET (TeLinkLocalIp.pu1_OctetList, TLM_ZERO, TLM_MAX_IP_LEN);

        nmhGetTeLinkLocalIpAddr (i4TeIfIndex, &TeLinkLocalIp);
        TLM_OCTETSTRING_TO_INTEGER ((&TeLinkLocalIp), u4LocalIpAddr);
        /*Type cast Ip address to string */
        TLM_CLI_IPADDR_TO_STR (pu1String, u4LocalIpAddr);

        CliPrintf (CliHandle, "\r\n Local IP Address: %s,", pu1String);

        /* Getting Remote ip address of Te link */
        TeLinkRemoteIp.pu1_OctetList = &au1IpArray[TLM_INDEX_ZERO];

        MEMSET (TeLinkRemoteIp.pu1_OctetList, TLM_ZERO, TLM_MAX_IP_LEN);

        nmhGetTeLinkRemoteIpAddr (i4TeIfIndex, &TeLinkRemoteIp);
        TLM_OCTETSTRING_TO_INTEGER ((&TeLinkRemoteIp), u4RemoteIpAddr);

        /*Type cast Ip address to string */
        TLM_CLI_IPADDR_TO_STR (pu1String, u4RemoteIpAddr);

        CliPrintf (CliHandle, " Remote IP Address: %s,", pu1String);
    }
    /* Getting Remote router Id of Te link */
    nmhGetFsTeLinkRemoteRtrId (i4TeIfIndex, &u4TeLinkRemoteRtrId);

    /*Type cast Ip address to string */
    TLM_CLI_IPADDR_TO_STR (pu1String, u4TeLinkRemoteRtrId);
    CliPrintf (CliHandle, "\r\n Remote Router-id: %s,", pu1String);

    /* Getting  Maximum  Bandwidth of Te link  */
    TeLinkMaxBw.pu1_OctetList = &au1BwArray[TLM_INDEX_ZERO];
    TeLinkMaxBw.i4_Length = TLM_FOUR;

    MEMSET (TeLinkMaxBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);
    nmhGetFsTeLinkMaximumBandwidth (i4TeIfIndex, &TeLinkMaxBw);
    f4TeLinkMaxBw = TLM_OCTETSTRING_TO_FLOAT ((&TeLinkMaxBw));

    TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TeLinkMaxBw);
    CliPrintf (CliHandle, " Maximum Bandwidth: %u kbps,",
               (UINT4) f4TeLinkMaxBw);

    /* Getting  Maximum reservable Bandwidth of Te link */
    TeLinkMaxResBw.pu1_OctetList = &au1BwArray[TLM_INDEX_ZERO];
    TeLinkMaxResBw.i4_Length = TLM_FOUR;

    MEMSET (TeLinkMaxResBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);
    nmhGetTeLinkMaximumReservableBandwidth (i4TeIfIndex, (&TeLinkMaxResBw));
    f4TeLinkMaxResBw = TLM_OCTETSTRING_TO_FLOAT ((&TeLinkMaxResBw));
    TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TeLinkMaxResBw);
    CliPrintf (CliHandle, "\r\n Maximum Reservable Bandwidth: %u kbps,",
               (UINT4) f4TeLinkMaxResBw);

    /*Getting Link Information Type */
    nmhGetFsTeLinkInfoType (i4TeIfIndex, &i4TeLinkInfoType);

    if (i4TeLinkInfoType == TLM_DATA_CHANNEL)
    {
        CliPrintf (CliHandle, "\r\n Link Information Type: Data channel,");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n Link Information Type: Data and Control channel,");
    }
    /*Getting Link Interface Type */
    nmhGetFsTeLinkIfType (i4TeIfIndex, &i4TeLinkIfType);
    if (i4TeLinkIfType == TLM_POINT_TO_POINT)
    {
        CliPrintf (CliHandle, "\r\n Link Interface Type: Point to point,");
    }
    else
    {
        CliPrintf (CliHandle, "\r\n Link Interface Type: MultiAccess,");
    }

    /* Getting  Protection Type of Te link */
    nmhGetTeLinkProtectionType (i4TeIfIndex, &i4TeLinkProtectionType);
    if (i4TeLinkProtectionType == TLM_TE_LINK_PROTECTION_TYPE_EXTRATRAFFIC)
    {
        CliPrintf (CliHandle, "\r\n Protection Type: Extra-Traffic,");
    }
    else if (i4TeLinkProtectionType == TLM_TE_LINK_PROTECTION_TYPE_UNPROTECTED)
    {
        CliPrintf (CliHandle, "\r\n Protection Type: Unprotected,");
    }
    else if (i4TeLinkProtectionType == TLM_TE_LINK_PROTECTION_TYPE_SHARED)
    {
        CliPrintf (CliHandle, "\r\n Protection Type: Shared,");
    }
    else if (i4TeLinkProtectionType ==
             TLM_TE_LINK_PROTECTION_TYPE_DEDICATED1FOR1)
    {
        CliPrintf (CliHandle, "\r\n Protection Type: Dedicated1For1,");
    }
    else if (i4TeLinkProtectionType ==
             TLM_TE_LINK_PROTECTION_TYPE_DEDICATED1PLUS1)
    {
        CliPrintf (CliHandle, "\r\n Protection Type: Dedicated1Plus1,");
    }
    else if (i4TeLinkProtectionType == TLM_TE_LINK_PROTECTION_TYPE_ENHANCED)
    {
        CliPrintf (CliHandle, "\r\n Protection Type: Enhanced,");
    }

    nmhGetTeLinkMetric (i4TeIfIndex, &u4TeLinkMetric);

    CliPrintf (CliHandle, " Metric: %u,", u4TeLinkMetric);
    /* Getting  Working Priority of Te link */
    nmhGetTeLinkWorkingPriority (i4TeIfIndex, &u4TeLinkWorkingPriority);
    CliPrintf (CliHandle, " Working Priority: %d,", u4TeLinkWorkingPriority);
    /* showing  All Resource class values of Te link */
    CliPrintf (CliHandle, "\r\n Resource Class Value:");

    /* Getting Resource Class of Te-Link */
    nmhGetTeLinkResourceClass (i4TeIfIndex, &u4ResourceClass);

    CliPrintf (CliHandle, " %u,", u4ResourceClass);
    u1RsrcClsVlaue = TLM_ZERO;
    u1Count = TLM_ZERO;
    while (u4ResourceClass != TLM_ZERO)
    {
        if ((u4ResourceClass % TLM_TWO) != TLM_ZERO)
        {
            MEMSET (&au1RsrcClsName, '\0',
                    sizeof (UINT1) * TLM_MAX_RESOURCE_CLASS_NAME_LEN);

            if (TLM_STRLEN (au1RsrcClsName) != TLM_ZERO)
            {
                if ((u1Count % TLM_THREE) == 0)
                    CliPrintf (CliHandle, "\r\n%*c", TLM_TWO, TLM_ZERO);

                CliPrintf (CliHandle, " %20s,", au1RsrcClsName);
                u1Count++;
            }
        }
        u1RsrcClsVlaue = (UINT1) (u1RsrcClsVlaue + TLM_ONE);
        u4ResourceClass = (u4ResourceClass / TLM_TWO);
    }
    /* Added for identifying Bundled and Unbundled Te  SPR 4512 */
    if (i4TeLinkType == TLM_BUNDLE)
    {
        CliPrintf (CliHandle, "\r\n TE-Link is Bundled");
    }
    else
    {
        if (CfaApiGetHLTeLinkFromLLTeLink ((UINT4) i4TeIfIndex,
                                           &u4HighIfIndex, TRUE) == CFA_SUCCESS)
        {
            TeLinkName.pu1_OctetList = au1NameArray;

            MEMSET (TeLinkName.pu1_OctetList, TLM_ZERO, TLM_MAX_NAME_LEN);

            /* Getting the TE-Link name */
            nmhGetFsTeLinkName ((INT4) u4HighIfIndex, &TeLinkName);

            CliPrintf (CliHandle,
                       "\r\n TE-Link is a member of %s Bundle",
                       TeLinkName.pu1_OctetList);
        }
        else
        {
            CliPrintf (CliHandle, "\r\n TE-Link is Un-Bundled");
        }
    }
    /* Added for identifying Bundled and Unbundled Te */

    /* Scan Srlg list in Te link */
    i4TempTeIfIndex = i4TeIfIndex;

    while (nmhGetNextIndexTeLinkSrlgTable (i4TempTeIfIndex, &i4TempTeIfIndex,
                                           u4TeLinkSrlg, &u4TeLinkSrlg)
           == SNMP_SUCCESS)
    {
        if (i4TempTeIfIndex != i4TeIfIndex)
        {
            break;
        }

        if (bHdrReqd == TRUE)
        {
            CliPrintf (CliHandle, "\r\n Srlg Value:");
            bHdrReqd = FALSE;
        }
        CliPrintf (CliHandle, " %u,", u4TeLinkSrlg);
    }

    /*< End scan Srlg list of Te link> */

    /*  show te link descriptor information */
    TlmCliShowTeLinkDescInfo (CliHandle, i4TeIfIndex);

    /* Unreserved Bandwidth for pri 0-7 */
    CliPrintf (CliHandle, "\r\n Unreserved Bandwidth per Priority: ");
    TeLinkUnresBwPri.pu1_OctetList = &au1BwArray[TLM_INDEX_ZERO];
    TeLinkUnresBwPri.i4_Length = TLM_FOUR;

    /* Re-Initialize the bandwidth count variable */
    u4BwPriLvl = TLM_ZERO;

    /*To print Unreserved Bw for all priority levels */
    while (u4BwPriLvl < TLM_MAX_PRIORITY_LVL)
    {
        if (u4BwPriLvl % TLM_TWO == TLM_NULL)
        {
            CliPrintf (CliHandle, "\r\n");
        }

        MEMSET (TeLinkUnresBwPri.pu1_OctetList, TLM_ZERO, TLM_FOUR);

        nmhGetTeLinkBandwidthUnreserved
            (i4TeIfIndex, u4BwPriLvl, &TeLinkUnresBwPri);

        f4TeLinkUnresBwPri = TLM_OCTETSTRING_TO_FLOAT ((&TeLinkUnresBwPri));
        TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TeLinkUnresBwPri);
        CliPrintf (CliHandle, "\tPriority %d: %10u kbps,", u4BwPriLvl,
                   (UINT4) f4TeLinkUnresBwPri);

        u4BwPriLvl = u4BwPriLvl + TLM_ONE;

    }                            /*<while loop ends for u4BwPriLvl > */
    CliPrintf (CliHandle, "\r\n");

    TlmCliShowTeCompLinkInfo (CliHandle, i4TeIfIndex);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  TlmCliShowTeLinkDescInfo                                  */
/*                                                                           */
/*  Description :  This function Show the various interface and values       */
/*                   in Telink desc table                                    */
/*                                                                           */
/*  Input       :  tCliHandle   : CliHandle                                  */
/*                 i4TeIfIndex : te link index                               */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
VOID
TlmCliShowTeLinkDescInfo (tCliHandle CliHandle, INT4 i4TeIfIndex)
{
    FLT4                f4TeLinkMaxLspBw = TLM_ZERO;
    FLT4                f4TeLinkMinLspBw = TLM_ZERO;
    UINT4               u4TeDescrId = TLM_ZERO;
    UINT4               u4TeLinkDescrInterfaceMtu = TLM_ZERO;
    UINT4               u4BwPriLvl = TLM_ZERO;
    INT4                i4TeDescrEncodingType = TLM_ZERO;
    INT4                i4TeDescrSwCap = TLM_ZERO;
    UINT1               au1BwArray[TLM_FOUR];
    tSNMP_OCTET_STRING_TYPE TeLinkMinLspBw;
    tSNMP_OCTET_STRING_TYPE TeLinkMaxLspBw;
    INT4                i4InTeIfIndex = i4TeIfIndex;

    while (nmhGetNextIndexTeLinkDescriptorTable (i4InTeIfIndex, &i4InTeIfIndex,
                                                 u4TeDescrId, &u4TeDescrId)
           == SNMP_SUCCESS)
    {
        if (i4InTeIfIndex != i4TeIfIndex)
        {
            break;
        }

        /* getting Descr  Switching Capability of te link */
        nmhGetTeLinkDescrSwitchingCapability
            (i4TeIfIndex, u4TeDescrId, &i4TeDescrSwCap);

        CliPrintf (CliHandle, "\r\n Switching Capability: ");
        if (i4TeDescrSwCap == TLM_CLI_SWCAP_PSC1)
        {
            CliPrintf (CliHandle, "psc1,");
        }
        if (i4TeDescrSwCap == TLM_CLI_SWCAP_PSC2)
        {
            CliPrintf (CliHandle, "psc2,");
        }

        /* getting Descr Encoding Type of te link */
        nmhGetTeLinkDescrEncodingType
            (i4TeIfIndex, u4TeDescrId, &i4TeDescrEncodingType);

        CliPrintf (CliHandle, " encoding: ");

        if (i4TeDescrEncodingType == TLM_ONE)
        {
            CliPrintf (CliHandle, "packet,");
        }
        /* Getting minimum LSP bandwidth of Te link */
        TeLinkMinLspBw.pu1_OctetList = &au1BwArray[TLM_INDEX_ZERO];
        TeLinkMinLspBw.i4_Length = TLM_FOUR;

        MEMSET (TeLinkMinLspBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);

        nmhGetTeLinkDescrMinLspBandwidth
            (i4TeIfIndex, u4TeDescrId, &TeLinkMinLspBw);
        f4TeLinkMinLspBw = TLM_OCTETSTRING_TO_FLOAT ((&TeLinkMinLspBw));

        TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TeLinkMinLspBw);

        CliPrintf (CliHandle, "\r\n Minimum Lsp Bandwidth: %u kbps,",
                   (UINT4) f4TeLinkMinLspBw);
        /* Getting Interface type  of Te link */
        nmhGetTeLinkDescrInterfaceMtu
            (i4TeIfIndex, u4TeDescrId, &u4TeLinkDescrInterfaceMtu);

        CliPrintf (CliHandle, " Interface MTU: %u,", u4TeLinkDescrInterfaceMtu);

        /* Maximum Lsp Bandwidth for prio 0-7 */
        CliPrintf (CliHandle, "\r\n Maximum Lsp Bandwidth per Priority: ");

        u4BwPriLvl = TLM_ZERO;
        TeLinkMaxLspBw.pu1_OctetList = &au1BwArray[TLM_INDEX_ZERO];
        TeLinkMaxLspBw.i4_Length = TLM_FOUR;

        MEMSET (TeLinkMaxLspBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);

        nmhGetTeLinkDescrMaxLspBandwidthPrio0
            (i4TeIfIndex, u4TeDescrId, &TeLinkMaxLspBw);

        f4TeLinkMaxLspBw = TLM_OCTETSTRING_TO_FLOAT ((&TeLinkMaxLspBw));

        TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TeLinkMaxLspBw);
        CliPrintf (CliHandle, "\r\n\tPriority %d: %10u kbps,",
                   u4BwPriLvl++, (UINT4) f4TeLinkMaxLspBw);

        MEMSET (TeLinkMaxLspBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);

        nmhGetTeLinkDescrMaxLspBandwidthPrio1
            (i4TeIfIndex, u4TeDescrId, &TeLinkMaxLspBw);
        f4TeLinkMaxLspBw = TLM_OCTETSTRING_TO_FLOAT ((&TeLinkMaxLspBw));

        TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TeLinkMaxLspBw);
        CliPrintf (CliHandle, "\tPriority %d: %10u kbps,", u4BwPriLvl++,
                   (UINT4) f4TeLinkMaxLspBw);

        MEMSET (TeLinkMaxLspBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);
        nmhGetTeLinkDescrMaxLspBandwidthPrio2
            (i4TeIfIndex, u4TeDescrId, &TeLinkMaxLspBw);

        f4TeLinkMaxLspBw = TLM_OCTETSTRING_TO_FLOAT ((&TeLinkMaxLspBw));
        TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TeLinkMaxLspBw);
        CliPrintf (CliHandle, "\r\n\tPriority %d: %10u kbps,",
                   u4BwPriLvl++, (UINT4) f4TeLinkMaxLspBw);

        MEMSET (TeLinkMaxLspBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);
        nmhGetTeLinkDescrMaxLspBandwidthPrio3
            (i4TeIfIndex, u4TeDescrId, &TeLinkMaxLspBw);
        f4TeLinkMaxLspBw = TLM_OCTETSTRING_TO_FLOAT ((&TeLinkMaxLspBw));
        TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TeLinkMaxLspBw);
        CliPrintf (CliHandle, "\tPriority %d: %10u kbps,", u4BwPriLvl++,
                   (UINT4) f4TeLinkMaxLspBw);

        MEMSET (TeLinkMaxLspBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);

        nmhGetTeLinkDescrMaxLspBandwidthPrio4
            (i4TeIfIndex, u4TeDescrId, &TeLinkMaxLspBw);

        f4TeLinkMaxLspBw = TLM_OCTETSTRING_TO_FLOAT ((&TeLinkMaxLspBw));
        TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TeLinkMaxLspBw);
        CliPrintf (CliHandle, "\r\n\tPriority %d: %10u kbps,",
                   u4BwPriLvl++, (UINT4) f4TeLinkMaxLspBw);

        MEMSET (TeLinkMaxLspBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);

        nmhGetTeLinkDescrMaxLspBandwidthPrio5
            (i4TeIfIndex, u4TeDescrId, &TeLinkMaxLspBw);

        f4TeLinkMaxLspBw = TLM_OCTETSTRING_TO_FLOAT ((&TeLinkMaxLspBw));
        TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TeLinkMaxLspBw);
        CliPrintf (CliHandle, "\tPriority %d: %10u kbps,", u4BwPriLvl++,
                   (UINT4) f4TeLinkMaxLspBw);

        MEMSET (TeLinkMaxLspBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);

        nmhGetTeLinkDescrMaxLspBandwidthPrio6
            (i4TeIfIndex, u4TeDescrId, &TeLinkMaxLspBw);

        f4TeLinkMaxLspBw = TLM_OCTETSTRING_TO_FLOAT ((&TeLinkMaxLspBw));
        TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TeLinkMaxLspBw);
        CliPrintf (CliHandle, "\r\n\tPriority %d: %10u kbps,",
                   u4BwPriLvl++, (UINT4) f4TeLinkMaxLspBw);

        MEMSET (TeLinkMaxLspBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);

        nmhGetTeLinkDescrMaxLspBandwidthPrio7
            (i4TeIfIndex, u4TeDescrId, &TeLinkMaxLspBw);

        f4TeLinkMaxLspBw = TLM_OCTETSTRING_TO_FLOAT ((&TeLinkMaxLspBw));
        TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TeLinkMaxLspBw);
        CliPrintf (CliHandle, "\tPriority %d: %10u kbps,",
                   u4BwPriLvl++, (UINT4) f4TeLinkMaxLspBw);
    }
}

/*****************************************************************************/
/*  Function    :   TlmCliShowTeCompLinkInfo                                 */
/*                                                                           */
/*  Description :  This function Show the various values   in Comp link      */
/*                   associated with te link                                 */
/*                                                                           */
/*  Input       :  tCliHandle  : CliHandle                                   */
/*                 i4TeIfIndex : te link index                               */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
VOID
TlmCliShowTeCompLinkInfo (tCliHandle CliHandle, INT4 i4TeIfIndex)
{
    tTlmComponentLink  *pComponentIface = NULL;
    tSNMP_OCTET_STRING_TYPE CompLinkUnResBwPri;
    tSNMP_OCTET_STRING_TYPE CompMaxResBw;
    FLT4                f4CompLinkUnResBwPri = TLM_ZERO;
    FLT4                f4CompMaxResBw = TLM_ZERO;
    UINT4               u4BwPriLvl = TLM_ZERO;
    INT4                i4CompLinkIndex = TLM_ZERO;
    INT4                i4CompLinkCurrProtType = TLM_ZERO;
    INT4                i4CompLinkPreferredProtType = TLM_ZERO;
    UINT1              *pu1CompIfName = NULL;
    UINT1               au1NameArray[TLM_MAX_NAME_LEN];
    UINT1               au1BwArray[TLM_FOUR];
    UINT4               u4LowerStackIndex = TLM_ZERO;

    /*Find Component in TE Link From Stack Table Rather than
     * from association from TeLink*/

    CfaApiGetL3IfFromTeLinkIf ((UINT4) i4TeIfIndex,
                               (UINT4 *) &u4LowerStackIndex, TRUE);

    if (u4LowerStackIndex != TLM_ZERO)
    {
        i4CompLinkIndex = (INT4) u4LowerStackIndex;

        CompMaxResBw.pu1_OctetList = &au1BwArray[TLM_INDEX_ZERO];
        CompMaxResBw.i4_Length = TLM_FOUR;

        MEMSET (CompMaxResBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);

        nmhGetComponentLinkMaxResBandwidth (i4CompLinkIndex, &CompMaxResBw);

        f4CompMaxResBw = TLM_OCTETSTRING_TO_FLOAT ((&CompMaxResBw));

        TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4CompMaxResBw);
        /* Getting Operational status of comp link from CFA */
        if ((pComponentIface = TlmFindComponentIf (i4CompLinkIndex)) == NULL)
        {
            return;
        }

        CliPrintf (CliHandle,
                   "\r\n %-15s%-15s%-10s%-12s", "Name", "Oper",
                   "IfIndex", "Bandwidth");

        pu1CompIfName = &au1NameArray[TLM_INDEX_ZERO];
        MEMSET (pu1CompIfName, TLM_ZERO, sizeof (au1NameArray));

        /* Getting name of comp link from CFA */
        CfaCliGetIfName ((UINT4) i4CompLinkIndex, (INT1 *) pu1CompIfName);
        CliPrintf (CliHandle, "\r\n %-15s", pu1CompIfName);

        /* display Component Link Operational Status */
        if ((pComponentIface->u1OperStatus == CFA_IF_UP) &&
            (pComponentIface->i4RowStatus == ACTIVE))
        {
            CliPrintf (CliHandle, "%-15s", "Up");
        }
        else
        {
            CliPrintf (CliHandle, "%-10s", "Down");
        }

        CliPrintf (CliHandle, "%-9d%u kbps ", i4CompLinkIndex,
                   (UINT4) f4CompMaxResBw);

        /* Getting Component Descr Info */
        nmhGetComponentLinkPreferredProtection (i4CompLinkIndex,
                                                &i4CompLinkPreferredProtType);

        if (i4CompLinkPreferredProtType ==
            TLM_COMP_LINK_PREFERRED_PROTECTION_PRIMARY)
        {
            CliPrintf (CliHandle, "\r\n Preferred Protection Type: Primary,");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n Preferred Protection Type: Secondary,");
        }

        nmhGetComponentLinkCurrentProtection (i4CompLinkIndex,
                                              &i4CompLinkCurrProtType);

        if (i4CompLinkCurrProtType == TLM_COMP_LINK_CURRENT_PROTECTION_PRIMARY)
        {
            CliPrintf (CliHandle, " Current Protection Type: Primary,");
        }
        else
        {
            CliPrintf (CliHandle, " Current Protection Type: Secondary,");
        }

        TlmCliShowCompLinkDescInfo (CliHandle, i4CompLinkIndex);
        /* Getting unreserved Bandwidth for pri 0-7 of component link */
        CompLinkUnResBwPri.pu1_OctetList = &au1BwArray[TLM_INDEX_ZERO];
        CompLinkUnResBwPri.i4_Length = TLM_FOUR;

        CliPrintf (CliHandle, "\r\n Unreserved Bandwidth per Priority: ");

        /* initialize u4BwPriLvl with zero */
        u4BwPriLvl = TLM_ZERO;

        /*To print Unreserved Bw for all priority levels */
        while (u4BwPriLvl < TLM_MAX_PRIORITY_LVL)
        {
            if (u4BwPriLvl % TLM_TWO == TLM_ZERO)
            {
                CliPrintf (CliHandle, "\r\n");
            }
            MEMSET (CompLinkUnResBwPri.pu1_OctetList, TLM_ZERO, TLM_FOUR);
            nmhGetComponentLinkBandwidthUnreserved
                (i4CompLinkIndex, u4BwPriLvl, &CompLinkUnResBwPri);

            f4CompLinkUnResBwPri =
                TLM_OCTETSTRING_TO_FLOAT ((&CompLinkUnResBwPri));
            TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4CompLinkUnResBwPri);
            CliPrintf (CliHandle, "\tPriority %d: %10u kbps,", u4BwPriLvl,
                       (UINT4) f4CompLinkUnResBwPri);

            u4BwPriLvl = u4BwPriLvl + TLM_ONE;
        }                        /*<while loop ends for u4BwPriLvl > */
        CliPrintf (CliHandle, "\r\n");
    }
}

/*****************************************************************************/
/*  Function    :  TlmCliShowCompLinkInfo                                    */
/*                                                                           */
/*  Description :  This function Show the various  values                    */
/*                   in comp link  table                                     */
/*                                                                           */
/*  Input       :  tCliHandle : CliHandle                                    */
/*                 u4Type : command type                                     */
/*                 i4CompIfIndex : Comp link index                           */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
INT4
TlmCliShowCompLinkInfo (tCliHandle CliHandle, UINT4 u4Type, INT4 i4CompIfIndex)
{
    tTlmComponentLink  *pComponentIface = NULL;
    tSNMP_OCTET_STRING_TYPE CompLinkUnResBwPri;
    tSNMP_OCTET_STRING_TYPE CompMaxResBw;
    FLT4                f4CompLinkUnResBwPri = TLM_ZERO;
    FLT4                f4CompMaxResBw = TLM_ZERO;
    UINT4               u4BwPriLvl = TLM_ZERO;
    INT4                i4PrevCompLinkIndex = TLM_ZERO;
    INT4                i4CompLinkIndex = TLM_ZERO;
    INT4                i4CompLinkCurrProtType = TLM_ZERO;
    INT4                i4CompLinkPreferredProtType = TLM_ZERO;
    UINT1              *pu1CompIfName = NULL;
    UINT1               au1NameArray[TLM_MAX_NAME_LEN];
    UINT1               au1BwArray[TLM_FOUR];

    if (nmhGetFirstIndexComponentLinkTable (&i4CompLinkIndex) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    do
    {
        i4PrevCompLinkIndex = i4CompLinkIndex;
        if (u4Type == TLM_CLI_SHOW_COMPLINK_INDEX)
        {
            if (i4CompIfIndex != i4CompLinkIndex)
            {
                continue;
            }
        }
        CompMaxResBw.pu1_OctetList = &au1BwArray[TLM_INDEX_ZERO];
        CompMaxResBw.i4_Length = TLM_FOUR;

        MEMSET (CompMaxResBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);
        nmhGetComponentLinkMaxResBandwidth (i4CompLinkIndex, &CompMaxResBw);

        f4CompMaxResBw = TLM_OCTETSTRING_TO_FLOAT ((&CompMaxResBw));
        TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4CompMaxResBw);

        CliPrintf (CliHandle,
                   "\r\n %-10s%-9s%-10s%-12s", "Name", "Oper",
                   "IfIndex", "Bandwidth");

        pu1CompIfName = &au1NameArray[TLM_INDEX_ZERO];
        MEMSET (pu1CompIfName, TLM_ZERO, sizeof (au1NameArray));
        /* Getting name of comp link from CFA */
        CfaCliGetIfName ((UINT4) i4CompLinkIndex, (INT1 *) pu1CompIfName);
        CliPrintf (CliHandle, "\r\n %-10s", pu1CompIfName);

        if ((pComponentIface = TlmFindComponentIf (i4CompLinkIndex)) == NULL)
        {
            break;
        }

        /* display Component Link Operational Status */
        if ((pComponentIface->u1OperStatus == CFA_IF_UP) &&
            (pComponentIface->i4RowStatus == ACTIVE))
        {
            CliPrintf (CliHandle, "%-10s", "Up");
        }
        else
        {
            CliPrintf (CliHandle, "%-10s", "Down");
        }
        CliPrintf (CliHandle, "%-9d%u kbps ", i4CompLinkIndex,
                   (UINT4) f4CompMaxResBw);

        nmhGetComponentLinkPreferredProtection (i4CompLinkIndex,
                                                &i4CompLinkPreferredProtType);

        if (i4CompLinkPreferredProtType ==
            TLM_COMP_LINK_PREFERRED_PROTECTION_PRIMARY)
        {
            CliPrintf (CliHandle, "\r\n Preferred Protection Type: Primary,");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n Preferred Protection Type: Secondary,");
        }

        nmhGetComponentLinkCurrentProtection (i4CompLinkIndex,
                                              &i4CompLinkCurrProtType);
        if (i4CompLinkCurrProtType == TLM_COMP_LINK_CURRENT_PROTECTION_PRIMARY)
        {
            CliPrintf (CliHandle, " Current Protection Type: Primary,");
        }
        else
        {
            CliPrintf (CliHandle, " Current Protection Type: Secondary,");
        }

        TlmCliShowCompLinkDescInfo (CliHandle, i4CompLinkIndex);
        CompLinkUnResBwPri.pu1_OctetList = &au1BwArray[TLM_INDEX_ZERO];
        CompLinkUnResBwPri.i4_Length = TLM_FOUR;

        CliPrintf (CliHandle,
                   "\r\n Unreserved Component Bandwidth per Priority: ");
        u4BwPriLvl = TLM_ZERO;
        while (u4BwPriLvl < TLM_MAX_PRIORITY_LVL)
        {
            if (u4BwPriLvl % TLM_TWO == TLM_NULL)
            {
                CliPrintf (CliHandle, "\r\n");
            }
            MEMSET (CompLinkUnResBwPri.pu1_OctetList, TLM_ZERO, TLM_FOUR);
            nmhGetComponentLinkBandwidthUnreserved
                (i4CompLinkIndex, u4BwPriLvl, &CompLinkUnResBwPri);
            f4CompLinkUnResBwPri =
                TLM_OCTETSTRING_TO_FLOAT ((&CompLinkUnResBwPri));
            TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4CompLinkUnResBwPri);
            CliPrintf (CliHandle, "\tPriority %d: %10u kbps,", u4BwPriLvl,
                       (UINT4) f4CompLinkUnResBwPri);
            u4BwPriLvl = u4BwPriLvl + TLM_ONE;
        }                        /*<while loop ends for u4BwPriLvl > */
        CliPrintf (CliHandle, "\r\n");
    }
    while (nmhGetNextIndexComponentLinkTable (i4PrevCompLinkIndex,
                                              &i4CompLinkIndex));
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  TlmCliShowCompLinkDescInfo                                */
/*                                                                           */
/*  Description :  This function Show the various interface and values       */
/*                   in Componentlink desc table                             */
/*                                                                           */
/*  Input       :  tCliHandle   : CliHandle                                  */
/*                 i4CompLinkIndex : Comp link index                         */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
VOID
TlmCliShowCompLinkDescInfo (tCliHandle CliHandle, INT4 i4CompLinkIndex)
{
    tSNMP_OCTET_STRING_TYPE CompLinkMinLspBw;
    tSNMP_OCTET_STRING_TYPE CompLinkMaxLspBw;
    UINT1               au1BwArray[TLM_FOUR];
    FLT4                f4CompLinkMinLspBw = TLM_ZERO;
    FLT4                f4CompLinkMaxLspBw = TLM_ZERO;
    UINT4               u4CompDescrId = TLM_ZERO;
    UINT4               u4BwPriLvl = TLM_ZERO;
    UINT4               u4CompDescrInterfaceMtu = TLM_ZERO;
    INT4                i4CompLinkDescrEncoType = TLM_ZERO;
    INT4                i4CompLinkDescrSwCap = TLM_ZERO;
    INT4                i4InCompLinkIndex = i4CompLinkIndex;

    while (nmhGetNextIndexComponentLinkDescriptorTable (i4InCompLinkIndex,
                                                        &i4InCompLinkIndex,
                                                        u4CompDescrId,
                                                        &u4CompDescrId)
           == SNMP_SUCCESS)
    {
        if (i4InCompLinkIndex != i4CompLinkIndex)
        {
            break;
        }

        nmhGetComponentLinkDescrSwitchingCapability
            (i4CompLinkIndex, u4CompDescrId, &i4CompLinkDescrSwCap);

        CliPrintf (CliHandle, "\r\n Switching Capability: ");
        if (i4CompLinkDescrSwCap == TLM_CLI_SWCAP_PSC1)
        {
            CliPrintf (CliHandle, "psc1,");
        }

        nmhGetComponentLinkDescrEncodingType
            (i4CompLinkIndex, u4CompDescrId, &i4CompLinkDescrEncoType);

        if (i4CompLinkDescrEncoType == TLM_ONE)
        {
            CliPrintf (CliHandle, " encoding: packet,");
        }

        /* Getting mininun LSP bandwidth of Te link */
        CompLinkMinLspBw.pu1_OctetList = &au1BwArray[TLM_INDEX_ZERO];
        CompLinkMinLspBw.i4_Length = TLM_FOUR;

        MEMSET (CompLinkMinLspBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);

        nmhGetComponentLinkDescrMinLspBandwidth
            (i4CompLinkIndex, u4CompDescrId, &CompLinkMinLspBw);

        f4CompLinkMinLspBw = TLM_OCTETSTRING_TO_FLOAT ((&CompLinkMinLspBw));
        TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4CompLinkMinLspBw);
        CliPrintf (CliHandle, "\r\n Minimum Lsp Bandwidth: %u kbps,",
                   (UINT4) f4CompLinkMinLspBw);

        nmhGetComponentLinkDescrInterfaceMtu
            (i4CompLinkIndex, u4CompDescrId, &u4CompDescrInterfaceMtu);

        CliPrintf (CliHandle, " Interface MTU: %u,", u4CompDescrInterfaceMtu);

        /* Maximum Lsp Bandwidth for prio 0-7 */
        CliPrintf (CliHandle, "\r\n Maximum Lsp Bandwidth per Priority: ");

        u4BwPriLvl = TLM_ZERO;
        CompLinkMaxLspBw.pu1_OctetList = &au1BwArray[TLM_INDEX_ZERO];
        CompLinkMaxLspBw.i4_Length = TLM_FOUR;

        MEMSET (CompLinkMaxLspBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);

        nmhGetComponentLinkDescrMaxLspBandwidthPrio0
            (i4CompLinkIndex, u4CompDescrId, &CompLinkMaxLspBw);

        f4CompLinkMaxLspBw = TLM_OCTETSTRING_TO_FLOAT ((&CompLinkMaxLspBw));

        TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4CompLinkMaxLspBw);
        CliPrintf (CliHandle, "\r\n\tPriority %d: %10u kbps,",
                   u4BwPriLvl++, (UINT4) f4CompLinkMaxLspBw);

        MEMSET (CompLinkMaxLspBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);

        nmhGetComponentLinkDescrMaxLspBandwidthPrio1
            (i4CompLinkIndex, u4CompDescrId, &CompLinkMaxLspBw);

        f4CompLinkMaxLspBw = TLM_OCTETSTRING_TO_FLOAT ((&CompLinkMaxLspBw));

        TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4CompLinkMaxLspBw);
        CliPrintf (CliHandle, "\tPriority %d: %10u kbps,",
                   u4BwPriLvl++, (UINT4) f4CompLinkMaxLspBw);

        MEMSET (CompLinkMaxLspBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);

        nmhGetComponentLinkDescrMaxLspBandwidthPrio2
            (i4CompLinkIndex, u4CompDescrId, &CompLinkMaxLspBw);

        f4CompLinkMaxLspBw = TLM_OCTETSTRING_TO_FLOAT ((&CompLinkMaxLspBw));

        TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4CompLinkMaxLspBw);
        CliPrintf (CliHandle, "\r\n\tPriority %d: %10u kbps,",
                   u4BwPriLvl++, (UINT4) f4CompLinkMaxLspBw);

        MEMSET (CompLinkMaxLspBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);

        nmhGetComponentLinkDescrMaxLspBandwidthPrio3
            (i4CompLinkIndex, u4CompDescrId, &CompLinkMaxLspBw);

        f4CompLinkMaxLspBw = TLM_OCTETSTRING_TO_FLOAT ((&CompLinkMaxLspBw));

        TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4CompLinkMaxLspBw);
        CliPrintf (CliHandle, "\tPriority %d: %10u kbps,",
                   u4BwPriLvl++, (UINT4) f4CompLinkMaxLspBw);

        MEMSET (CompLinkMaxLspBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);

        nmhGetComponentLinkDescrMaxLspBandwidthPrio4
            (i4CompLinkIndex, u4CompDescrId, &CompLinkMaxLspBw);

        f4CompLinkMaxLspBw = TLM_OCTETSTRING_TO_FLOAT ((&CompLinkMaxLspBw));

        TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4CompLinkMaxLspBw);
        CliPrintf (CliHandle, "\r\n\tPriority %d: %10u kbps,",
                   u4BwPriLvl++, (UINT4) f4CompLinkMaxLspBw);

        MEMSET (CompLinkMaxLspBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);

        nmhGetComponentLinkDescrMaxLspBandwidthPrio5
            (i4CompLinkIndex, u4CompDescrId, &CompLinkMaxLspBw);

        f4CompLinkMaxLspBw = TLM_OCTETSTRING_TO_FLOAT ((&CompLinkMaxLspBw));

        TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4CompLinkMaxLspBw);
        CliPrintf (CliHandle, "\tPriority %d: %10u kbps,",
                   u4BwPriLvl++, (UINT4) f4CompLinkMaxLspBw);

        MEMSET (CompLinkMaxLspBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);

        nmhGetComponentLinkDescrMaxLspBandwidthPrio6
            (i4CompLinkIndex, u4CompDescrId, &CompLinkMaxLspBw);

        f4CompLinkMaxLspBw = TLM_OCTETSTRING_TO_FLOAT ((&CompLinkMaxLspBw));

        TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4CompLinkMaxLspBw);
        CliPrintf (CliHandle, "\r\n\tPriority %d: %10u kbps,",
                   u4BwPriLvl++, (UINT4) f4CompLinkMaxLspBw);

        MEMSET (CompLinkMaxLspBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);

        nmhGetComponentLinkDescrMaxLspBandwidthPrio7
            (i4CompLinkIndex, u4CompDescrId, &CompLinkMaxLspBw);

        f4CompLinkMaxLspBw = TLM_OCTETSTRING_TO_FLOAT ((&CompLinkMaxLspBw));

        TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (f4CompLinkMaxLspBw);
        CliPrintf (CliHandle, "\tPriority %d: %10u kbps,",
                   u4BwPriLvl++, (UINT4) f4CompLinkMaxLspBw);
    }

}

/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : TlmCliSetBwThreshold                              */
/*                                                                          */
/*     DESCRIPTION      : This function configures bandwidth thresholds     */
/*                                                                          */
/*     INPUT            : CliHandle - CLI context id                        */
/*                        i4TeIfIndex - TeLink IfIndex                      */
/*                        u4ThresholdIndex - Threshold index (Up/Down)      */
/*                        pi4BwThreshold - Bw Threshold values              */
/*                                                                          */
/*     OUTPUT           : None                                              */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT1
TlmCliSetBwThreshold (tCliHandle CliHandle, INT4 i4TeIfIndex,
                      INT4 i4ThresholdIndex, INT4 *pi4BwThreshold)
{
    INT1                i1RetVal = TLM_FAILURE;
    UINT4               u4ErrorCode = TLM_ZERO;

    i1RetVal = nmhTestv2FsTeLinkBwThresholdRowStatus (&u4ErrorCode,
                                                      i4TeIfIndex,
                                                      i4ThresholdIndex,
                                                      CREATE_AND_WAIT);
    if (i1RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Failed to Create BW Threshold Table\r\n");
        return CLI_FAILURE;
    }

    i1RetVal = nmhSetFsTeLinkBwThresholdRowStatus (i4TeIfIndex,
                                                   i4ThresholdIndex,
                                                   CREATE_AND_WAIT);

    if (i1RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Failed to Create BW Threshold Table\r\n");
        return CLI_FAILURE;
    }

    do
    {
        if (pi4BwThreshold[TLM_BW_THRESHOLD_ZERO] != TLM_ZERO)
        {
            /* set BW Threshold */
            i1RetVal = nmhTestv2FsTeLinkBwThreshold0 (&u4ErrorCode, i4TeIfIndex,
                                                      i4ThresholdIndex,
                                                      pi4BwThreshold
                                                      [TLM_BW_THRESHOLD_ZERO]);
            if (i1RetVal == SNMP_FAILURE)
            {
                break;
            }

            nmhSetFsTeLinkBwThreshold0 (i4TeIfIndex, i4ThresholdIndex,
                                        pi4BwThreshold[TLM_BW_THRESHOLD_ZERO]);
        }

        if (pi4BwThreshold[TLM_BW_THRESHOLD_ONE] != TLM_ZERO)
        {
            /* set BW Threshold */
            i1RetVal = nmhTestv2FsTeLinkBwThreshold1 (&u4ErrorCode, i4TeIfIndex,
                                                      i4ThresholdIndex,
                                                      pi4BwThreshold
                                                      [TLM_BW_THRESHOLD_ONE]);
            if (i1RetVal == SNMP_FAILURE)
            {
                break;
            }

            nmhSetFsTeLinkBwThreshold1 (i4TeIfIndex, i4ThresholdIndex,
                                        pi4BwThreshold[TLM_BW_THRESHOLD_ONE]);
        }

        if (pi4BwThreshold[TLM_BW_THRESHOLD_TWO] != TLM_ZERO)
        {
            /* set BW Threshold */
            i1RetVal = nmhTestv2FsTeLinkBwThreshold2 (&u4ErrorCode, i4TeIfIndex,
                                                      i4ThresholdIndex,
                                                      pi4BwThreshold
                                                      [TLM_BW_THRESHOLD_TWO]);
            if (i1RetVal == SNMP_FAILURE)
            {
                break;
            }

            nmhSetFsTeLinkBwThreshold2 (i4TeIfIndex, i4ThresholdIndex,
                                        pi4BwThreshold[TLM_BW_THRESHOLD_TWO]);
        }

        if (pi4BwThreshold[TLM_BW_THRESHOLD_THREE] != TLM_ZERO)
        {
            /* set BW Threshold */
            i1RetVal = nmhTestv2FsTeLinkBwThreshold3 (&u4ErrorCode, i4TeIfIndex,
                                                      i4ThresholdIndex,
                                                      pi4BwThreshold
                                                      [TLM_BW_THRESHOLD_THREE]);
            if (i1RetVal == SNMP_FAILURE)
            {
                break;
            }

            nmhSetFsTeLinkBwThreshold3 (i4TeIfIndex, i4ThresholdIndex,
                                        pi4BwThreshold[TLM_BW_THRESHOLD_THREE]);
        }

        if (pi4BwThreshold[TLM_BW_THRESHOLD_FOUR] != TLM_ZERO)
        {
            /* set BW Threshold */
            i1RetVal = nmhTestv2FsTeLinkBwThreshold4 (&u4ErrorCode, i4TeIfIndex,
                                                      i4ThresholdIndex,
                                                      pi4BwThreshold
                                                      [TLM_BW_THRESHOLD_FOUR]);
            if (i1RetVal == SNMP_FAILURE)
            {
                break;
            }

            nmhSetFsTeLinkBwThreshold4 (i4TeIfIndex, i4ThresholdIndex,
                                        pi4BwThreshold[TLM_BW_THRESHOLD_FOUR]);
        }

        if (pi4BwThreshold[TLM_BW_THRESHOLD_FIVE] != TLM_ZERO)
        {
            /* set BW Threshold */
            i1RetVal = nmhTestv2FsTeLinkBwThreshold5 (&u4ErrorCode, i4TeIfIndex,
                                                      i4ThresholdIndex,
                                                      pi4BwThreshold
                                                      [TLM_BW_THRESHOLD_FIVE]);
            if (i1RetVal == SNMP_FAILURE)
            {
                break;
            }

            nmhSetFsTeLinkBwThreshold5 (i4TeIfIndex, i4ThresholdIndex,
                                        pi4BwThreshold[TLM_BW_THRESHOLD_FIVE]);
        }

        if (pi4BwThreshold[TLM_BW_THRESHOLD_SIX] != TLM_ZERO)
        {
            /* set BW Threshold */
            i1RetVal = nmhTestv2FsTeLinkBwThreshold6 (&u4ErrorCode, i4TeIfIndex,
                                                      i4ThresholdIndex,
                                                      pi4BwThreshold
                                                      [TLM_BW_THRESHOLD_SIX]);
            if (i1RetVal == SNMP_FAILURE)
            {
                break;
            }

            nmhSetFsTeLinkBwThreshold6 (i4TeIfIndex, i4ThresholdIndex,
                                        pi4BwThreshold[TLM_BW_THRESHOLD_SIX]);
        }

        if (pi4BwThreshold[TLM_BW_THRESHOLD_SEVEN] != TLM_ZERO)
        {
            /* set BW Threshold */
            i1RetVal = nmhTestv2FsTeLinkBwThreshold7 (&u4ErrorCode, i4TeIfIndex,
                                                      i4ThresholdIndex,
                                                      pi4BwThreshold
                                                      [TLM_BW_THRESHOLD_SEVEN]);
            if (i1RetVal == SNMP_FAILURE)
            {
                break;
            }

            nmhSetFsTeLinkBwThreshold7 (i4TeIfIndex, i4ThresholdIndex,
                                        pi4BwThreshold[TLM_BW_THRESHOLD_SEVEN]);
        }

        if (pi4BwThreshold[TLM_BW_THRESHOLD_EIGHT] != TLM_ZERO)
        {
            /* set BW Threshold */
            i1RetVal = nmhTestv2FsTeLinkBwThreshold8 (&u4ErrorCode, i4TeIfIndex,
                                                      i4ThresholdIndex,
                                                      pi4BwThreshold
                                                      [TLM_BW_THRESHOLD_EIGHT]);
            if (i1RetVal == SNMP_FAILURE)
            {
                break;
            }

            nmhSetFsTeLinkBwThreshold8 (i4TeIfIndex, i4ThresholdIndex,
                                        pi4BwThreshold[TLM_BW_THRESHOLD_EIGHT]);
        }

        if (pi4BwThreshold[TLM_BW_THRESHOLD_NINE] != TLM_ZERO)
        {
            /* set BW Threshold */
            i1RetVal = nmhTestv2FsTeLinkBwThreshold9 (&u4ErrorCode, i4TeIfIndex,
                                                      i4ThresholdIndex,
                                                      pi4BwThreshold
                                                      [TLM_BW_THRESHOLD_NINE]);
            if (i1RetVal == SNMP_FAILURE)
            {
                break;
            }

            nmhSetFsTeLinkBwThreshold9 (i4TeIfIndex, i4ThresholdIndex,
                                        pi4BwThreshold[TLM_BW_THRESHOLD_NINE]);
        }

        if (pi4BwThreshold[TLM_BW_THRESHOLD_TEN] != TLM_ZERO)
        {
            /* set BW Threshold */
            i1RetVal =
                nmhTestv2FsTeLinkBwThreshold10 (&u4ErrorCode, i4TeIfIndex,
                                                i4ThresholdIndex,
                                                pi4BwThreshold
                                                [TLM_BW_THRESHOLD_TEN]);
            if (i1RetVal == SNMP_FAILURE)
            {
                break;
            }

            nmhSetFsTeLinkBwThreshold10 (i4TeIfIndex, i4ThresholdIndex,
                                         pi4BwThreshold[TLM_BW_THRESHOLD_TEN]);
        }

        if (pi4BwThreshold[TLM_BW_THRESHOLD_ELEVEN] != TLM_ZERO)
        {
            /* set BW Threshold */
            i1RetVal =
                nmhTestv2FsTeLinkBwThreshold11 (&u4ErrorCode, i4TeIfIndex,
                                                i4ThresholdIndex,
                                                pi4BwThreshold
                                                [TLM_BW_THRESHOLD_ELEVEN]);
            if (i1RetVal == SNMP_FAILURE)
            {
                break;
            }

            nmhSetFsTeLinkBwThreshold11 (i4TeIfIndex, i4ThresholdIndex,
                                         pi4BwThreshold
                                         [TLM_BW_THRESHOLD_ELEVEN]);
        }

        if (pi4BwThreshold[TLM_BW_THRESHOLD_TWELVE] != TLM_ZERO)
        {
            /* set BW Threshold */
            i1RetVal =
                nmhTestv2FsTeLinkBwThreshold12 (&u4ErrorCode, i4TeIfIndex,
                                                i4ThresholdIndex,
                                                pi4BwThreshold
                                                [TLM_BW_THRESHOLD_TWELVE]);
            if (i1RetVal == SNMP_FAILURE)
            {
                break;
            }

            nmhSetFsTeLinkBwThreshold12 (i4TeIfIndex, i4ThresholdIndex,
                                         pi4BwThreshold
                                         [TLM_BW_THRESHOLD_TWELVE]);
        }

        if (pi4BwThreshold[TLM_BW_THRESHOLD_THIRTEEN] != TLM_ZERO)
        {
            /* set BW Threshold */
            i1RetVal =
                nmhTestv2FsTeLinkBwThreshold13 (&u4ErrorCode, i4TeIfIndex,
                                                i4ThresholdIndex,
                                                pi4BwThreshold
                                                [TLM_BW_THRESHOLD_THIRTEEN]);
            if (i1RetVal == SNMP_FAILURE)
            {
                break;
            }

            nmhSetFsTeLinkBwThreshold13 (i4TeIfIndex, i4ThresholdIndex,
                                         pi4BwThreshold
                                         [TLM_BW_THRESHOLD_THIRTEEN]);
        }

        if (pi4BwThreshold[TLM_BW_THRESHOLD_FOURTEEN] != TLM_ZERO)
        {
            /* set BW Threshold */
            i1RetVal =
                nmhTestv2FsTeLinkBwThreshold14 (&u4ErrorCode, i4TeIfIndex,
                                                i4ThresholdIndex,
                                                pi4BwThreshold
                                                [TLM_BW_THRESHOLD_FOURTEEN]);
            if (i1RetVal == SNMP_FAILURE)
            {
                break;
            }

            nmhSetFsTeLinkBwThreshold14 (i4TeIfIndex, i4ThresholdIndex,
                                         pi4BwThreshold
                                         [TLM_BW_THRESHOLD_FOURTEEN]);
        }

        if (pi4BwThreshold[TLM_BW_THRESHOLD_FIFTEEN] != TLM_ZERO)
        {
            /* set BW Threshold */
            i1RetVal =
                nmhTestv2FsTeLinkBwThreshold15 (&u4ErrorCode, i4TeIfIndex,
                                                i4ThresholdIndex,
                                                pi4BwThreshold
                                                [TLM_BW_THRESHOLD_FIFTEEN]);
            if (i1RetVal == SNMP_FAILURE)
            {
                break;
            }

            nmhSetFsTeLinkBwThreshold15 (i4TeIfIndex, i4ThresholdIndex,
                                         pi4BwThreshold
                                         [TLM_BW_THRESHOLD_FIFTEEN]);
        }
    }
    while (0);

    if (i1RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Invalid Threshold Value\r\n");

        nmhSetFsTeLinkBwThresholdRowStatus (i4TeIfIndex, i4ThresholdIndex,
                                            DESTROY);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : TlmCliSetBwThresholdForce                         */
/*                                                                          */
/*     DESCRIPTION      : This function sends notification messages       */
/*                                                                          */
/*     INPUT            : u4Type - Type of the command                      */
/*                        i4TraceVal - TLM TE Debug Level                   */
/*                                                                          */
/*     OUTPUT           : None                                              */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
TlmCliSetBwThresholdForce (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);

    nmhSetFsTeLinkBwThresholdForceOption (TLM_BW_THRESHOLD_FORCE_ENABLE);

    return CLI_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : TlmCliSetTeLinkTrace                              */
/*                                                                          */
/*     DESCRIPTION      : This function configures TLM TE Debug level       */
/*                                                                          */
/*     INPUT            : u4Type - Type of the command                      */
/*                        i4TraceVal - TLM TE Debug Level                   */
/*                                                                          */
/*     OUTPUT           : None                                              */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
TlmCliSetTeLinkTrace (UINT4 u4Type, INT4 i4TraceVal)
{
    UINT4               u4ErrorCode = TLM_ZERO;
    INT4                i4Level = TLM_ZERO;

    /* Getting existing trace level */
    nmhGetFsTeLinkTraceOption (&i4Level);

    if (u4Type == TLM_CLI_DBG)
    {
        i4TraceVal = i4TraceVal | i4Level;
    }
    else
    {
        i4TraceVal = i4TraceVal & i4Level;
    }

    /*Testing the trace value before setting it */
    if (nmhTestv2FsTeLinkTraceOption (&u4ErrorCode, i4TraceVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* this routine never return failure */
    nmhSetFsTeLinkTraceOption (i4TraceVal);

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : TlmChangePrompt                                   */
/*                                                                          */
/*     DESCRIPTION      : This function changes the current prompt for both */
/*                        TeLinks and component links                       */
/*                                                                          */
/*     INPUT            : isMemberBundle - If it is member of bundle link   */
/*                        i4i4LinkType   - TeLink/Component Link            */
/*                                                                          */
/*     OUTPUT           : None                                              */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
TlmCliChangePrompt (BOOL1 isMemberBundle, INT4 i4LinkType, INT4 i4IfIndex)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT1               au1CurMode[50];
    INT4                i4RetVal = CLI_FAILURE;

    switch (i4LinkType)
    {
        case TLM_TELINK_INDEX:

            CliGetCurModePromptStr ((INT1 *) au1CurMode);

            if (isMemberBundle == TLM_TRUE)
            {
                if (!STRNCMP ((CHR1 *) au1CurMode, CLI_MODE_TE_LINK_INTF_PROMPT,
                              STRLEN (CLI_MODE_TE_LINK_INTF_PROMPT)))
                {
                    SPRINTF ((CHR1 *) au1Cmd, "%s %d",
                             CLI_MODE_TE_COMP_LINK_INTF, i4IfIndex);
                }
                else
                {
                    SPRINTF ((CHR1 *) au1Cmd, "%s %d", CLI_MODE_TE_LINK_INTF,
                             i4IfIndex);
                }

                i4RetVal = CliChangePath ((CHR1 *) au1Cmd);
            }
            else
            {
                if (!STRNCMP ((CHR1 *) au1CurMode, CLI_MODE_TE_LINK_PROMPT,
                              STRLEN (CLI_MODE_TE_LINK_PROMPT)))
                {
                    SPRINTF ((CHR1 *) au1Cmd, "%s %d", CLI_MODE_TE_COMP_LINK,
                             i4IfIndex);
                }
                else
                {
                    SPRINTF ((CHR1 *) au1Cmd, "%s %d", CLI_MODE_TE_LINK,
                             i4IfIndex);
                }

                i4RetVal = CliChangePath ((CHR1 *) au1Cmd);
            }
            break;

        case TLM_COMPLINK_INDEX:

            CliGetCurModePromptStr ((INT1 *) au1CurMode);

            if (!STRNCMP ((CHR1 *) au1CurMode, CLI_MODE_TE_LINK_PROMPT,
                          STRLEN (CLI_MODE_TE_LINK_PROMPT)))
            {
                SPRINTF ((CHR1 *) au1Cmd, "%s %d", CLI_MODE_TE_COMP_LINK,
                         i4IfIndex);
            }
            else if (!STRNCMP
                     ((CHR1 *) au1CurMode, CLI_MODE_TE_LINK_INTF_PROMPT,
                      STRLEN (CLI_MODE_TE_LINK_INTF_PROMPT)))
            {
                SPRINTF ((CHR1 *) au1Cmd, "%s %d", CLI_MODE_TE_COMP_LINK_INTF,
                         i4IfIndex);
            }
            else
            {
                SPRINTF ((CHR1 *) au1Cmd, "%s %d", CLI_MODE_TE_LINK, i4IfIndex);
            }

            i4RetVal = CliChangePath ((CHR1 *) au1Cmd);

            break;
        default:
            i4RetVal = CLI_FAILURE;
    }

    return i4RetVal;
}

/******************************************************************************/
/* Function Name     : TlmGetCfgTePrompt                                      */
/*                                                                            */
/* Description       : This function validates the given pi1ModeName          */
/*                     and returns the prompt in pi1DispStr if valid,         */
/*                                                                            */
/* Input Parameters  : pi1ModeName - Mode Name to validate                    */
/*                     pi1DispStr - Display string to be returned.            */
/*                                                                            */
/* Output Parameters : pi1DispStr - Display string.                           */
/*                                                                            */
/* Return Value      : TRUE/FALSE                                             */
/******************************************************************************/

INT1
TlmGetCfgTePrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = TLM_ZERO;

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }
    u4Len = TLM_STRLEN (CLI_MODE_TELINK_CONFIG);
    if (TLM_STRNCMP (pi1ModeName, CLI_MODE_TELINK_CONFIG, u4Len) != TLM_ZERO)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    /* No need to take lock here, since it is taken by
     *  Cli in cli_process_tlm_cmd. */

    TLM_STRCPY (pi1DispStr, "(config-mpls-te)#");
    return TRUE;
}

/******************************************************************************/
/* Function Name     : TlmGetTeLinkPrompt                                     */
/*                                                                            */
/* Description       : This function validates the given pi1ModeName          */
/*                     and returns the prompt in pi1DispStr if valid,         */
/*                                                                            */
/* Input Parameters  : pi1ModeName - Mode Name to validate                    */
/*                     pi1DispStr - Display string to be returned.            */
/*                                                                            */
/* Output Parameters : pi1DispStr - Display string.                           */
/*                                                                            */
/* Return Value      : TRUE/FALSE                                             */
/******************************************************************************/

INT1
TlmGetTeLinkPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = TLM_ZERO;

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    u4Len = TLM_STRLEN (CLI_MODE_TE_LINK);

    if (TLM_STRNCMP (pi1ModeName, CLI_MODE_TE_LINK, u4Len) != TLM_ZERO)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;

    /* No need to take lock here, since it is taken by
     * Cli in cli_process_tlm_cmd. */

    TLM_STRCPY (pi1DispStr, "(config-mpls-te-link)#");
    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : TlmGetTeIfConfigPrompt                             */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the prompt in pi1DispStr if valid.     */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- Display string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/
INT1
TlmGetTeIfConfigPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = TLM_ZERO;
    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    u4Len = TLM_STRLEN (CLI_MODE_TE_LINK_INTF);

    if (TLM_STRNCMP (pi1ModeName, CLI_MODE_TE_LINK_INTF, u4Len) != TLM_ZERO)
    {
        return FALSE;
    }

    TLM_STRCPY (pi1DispStr, "(config-mpls-te-link-if)#");

    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : TlmGetCompTeConfigPrompt                           */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the prompt in pi1DispStr if valid.     */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- Display string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/
INT1
TlmGetCompTeConfigPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = TLM_ZERO;
    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    u4Len = TLM_STRLEN (CLI_MODE_TE_COMP_LINK);

    if (TLM_STRNCMP (pi1ModeName, CLI_MODE_TE_COMP_LINK, u4Len) != TLM_ZERO)
    {
        return FALSE;
    }

    TLM_STRCPY (pi1DispStr, "(config-mpls-comp-link)#");

    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : TlmGetCompIfConfigPrompt                           */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the prompt in pi1DispStr if valid.     */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- Display string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/
INT1
TlmGetCompIfConfigPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = TLM_ZERO;
    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    u4Len = TLM_STRLEN (CLI_MODE_TE_COMP_LINK_INTF);

    if (TLM_STRNCMP (pi1ModeName, CLI_MODE_TE_COMP_LINK_INTF, u4Len) !=
        TLM_ZERO)
    {
        return FALSE;
    }

    TLM_STRCPY (pi1DispStr, "(config-mpls-comp-link-if)#");
    return TRUE;
}

/**************************************************************************/
/*  Function Name   : TlmCliShowRunningConfig                             */
/*                                                                        */
/*  Description     : This function  displays the TLM   Configuration     */
/*                                                                        */
/*  Input(s)        : CliHandle                                           */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/
INT4
TlmCliShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4TeIfIndex = TLM_ZERO;
    INT4                i4ModuleStatus = TLM_DISABLED;

    nmhGetFsTeLinkModuleStatus (&i4ModuleStatus);

    if (i4ModuleStatus == TLM_DISABLED)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\n!");

    CliPrintf (CliHandle, "\r\nmpls traffic-eng tunnels");

    if (nmhGetFirstIndexTeLinkTable (&i4TeIfIndex) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\nmpls traffic-eng");

    do
    {
        TlmCliShowBundleType (CliHandle, i4TeIfIndex);
    }
    while (nmhGetNextIndexTeLinkTable (i4TeIfIndex, &i4TeIfIndex) !=
           SNMP_FAILURE);

    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : TlmCliShowBundleType                                */
/*                                                                        */
/*  Description     : This function  displays the TE-LINK configuration   */
/*                                                                        */
/*  Input(s)        : CliHandle - CliHandle                               */
/*                    i4TeIfIndex - TeLink If Index                       */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : None                                                */
/**************************************************************************/
PRIVATE VOID
TlmCliShowBundleType (tCliHandle CliHandle, INT4 i4TeIfIndex)
{
    tSNMP_OCTET_STRING_TYPE TeLinkName;
    INT4                i4LinkType = TLM_ZERO;
    INT4                i4TeRowStatus = TLM_ZERO;
    UINT4               u4LLTeIfIndex = TLM_ZERO;
    UINT4               u4HighIfIndex = TLM_ZERO;
    UINT1               au1Name[TLM_MAX_NAME_LEN];
    INT4                i4StorageType = TLM_ZERO;

    TeLinkName.pu1_OctetList = &au1Name[TLM_INDEX_ZERO];
    MEMSET (TeLinkName.pu1_OctetList, TLM_ZERO, TLM_MAX_NAME_LEN);

    nmhGetFsTeLinkType (i4TeIfIndex, &i4LinkType);

    nmhGetFsTeLinkName (i4TeIfIndex, &TeLinkName);

    nmhGetTeLinkStorageType (i4TeIfIndex, &i4StorageType);

    if (i4StorageType == TLM_STORAGE_VOLATILE)
    {
        return;
    }

    /* If any higher TE link is present, don't display
     * SRC details of this TE link. It is already displayed
     * as part of higher TE link's unbundled TE links. */
    if ((i4LinkType == TLM_UNBUNDLE) &&
        (CfaApiGetHLTeLinkFromLLTeLink ((UINT4) i4TeIfIndex,
                                        &u4HighIfIndex, TRUE) == CFA_SUCCESS))
    {
        return;
    }

    CliPrintf (CliHandle, "\r\n te-link %s", TeLinkName.pu1_OctetList);

    if (i4LinkType == TLM_BUNDLE)
    {
        CliPrintf (CliHandle, " bundle");

        TlmCliShowTeLinkConfig (CliHandle, i4TeIfIndex);

        while (CfaApiGetNextLLTeLinkFromHLTeLink ((UINT4) i4TeIfIndex,
                                                  u4LLTeIfIndex,
                                                  &u4LLTeIfIndex, TRUE)
               == CFA_SUCCESS)
        {
            TeLinkName.pu1_OctetList = &au1Name[TLM_INDEX_ZERO];
            MEMSET (TeLinkName.pu1_OctetList, TLM_ZERO, TLM_MAX_NAME_LEN);

            nmhGetFsTeLinkName ((INT4) u4LLTeIfIndex, &TeLinkName);

            CliPrintf (CliHandle, "\r\n  interface te-link %s",
                       TeLinkName.pu1_OctetList);

            TlmCliShowTeLinkConfig (CliHandle, (INT4) u4LLTeIfIndex);
        }

        nmhGetTeLinkRowStatus (i4TeIfIndex, &i4TeRowStatus);

        if (i4TeRowStatus == ACTIVE)
        {
            CliPrintf (CliHandle, "\r\n  no shutdown");
            CliPrintf (CliHandle, "\r\n   exit");
        }
    }
    else
    {
        TlmCliShowTeLinkConfig (CliHandle, i4TeIfIndex);
    }
}

/**************************************************************************/
/*  Function Name   : TlmCliShowCompLinkConfig                            */
/*                                                                        */
/*  Description     : This function displays the Component Link           */
/*                    Configuration                                       */
/*                                                                        */
/*  Input(s)        : CliHandle - CliHandle                               */
/*                    i4CompIfIndex - Component Link If Index             */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : NONE                                                */
/**************************************************************************/
PRIVATE VOID
TlmCliShowCompLinkConfig (tCliHandle CliHandle, INT4 i4CompIfIndex)
{
    tSNMP_OCTET_STRING_TYPE CompBw;
    tBandWidth          maxResvBw = TLM_ZERO;
    tBandWidth          minLspBw = TLM_ZERO;
    INT1               *pi1IfName = NULL;
    INT4                i4DescIndex1 = TLM_ZERO;
    INT4                i4SwitchCap = TLM_ZERO;
    INT4                i4EncodType = TLM_ZERO;
    INT4                i4RowStatus = TLM_ZERO;
    UINT4               u4DescId = TLM_ZERO;
    UINT1               au1Bw[TLM_FOUR];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { 0 };

    pi1IfName = (INT1 *) &au1IfName[0];

    CompBw.pu1_OctetList = &au1Bw[TLM_INDEX_ZERO];
    CompBw.i4_Length = TLM_FOUR;

    MEMSET (CompBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);

    if (i4CompIfIndex != TLM_ZERO)
    {
        CfaCliConfGetIfName ((UINT4) i4CompIfIndex, pi1IfName);
        CliPrintf (CliHandle, "\r\n  interface %s", pi1IfName);
    }
    /* Get Max Res Bandwidth */

    nmhGetComponentLinkMaxResBandwidth (i4CompIfIndex, &CompBw);
    maxResvBw = TLM_OCTETSTRING_TO_FLOAT ((&CompBw));

    TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (maxResvBw);

    if (maxResvBw != TLM_ZERO)
    {
        CliPrintf (CliHandle, "\r\n  max reservable bandwidth %d",
                   (UINT4) maxResvBw);
    }

    i4DescIndex1 = i4CompIfIndex;

    while (nmhGetNextIndexComponentLinkDescriptorTable (i4DescIndex1,
                                                        &i4DescIndex1,
                                                        u4DescId, &u4DescId)
           == SNMP_SUCCESS)
    {
        if (i4DescIndex1 != i4CompIfIndex)
        {
            continue;
        }

        /* Get Descriptor Switching Type */
        nmhGetComponentLinkDescrSwitchingCapability (i4CompIfIndex,
                                                     u4DescId, &i4SwitchCap);
        /* Get Descriptor Encoding Type */
        nmhGetComponentLinkDescrEncodingType (i4CompIfIndex,
                                              u4DescId, &i4EncodType);

        TlmCliShowSwiCapType (CliHandle, i4SwitchCap, i4EncodType);

        /* Get Descriptor Min LSP bandwidth */
        MEMSET (CompBw.pu1_OctetList, TLM_ZERO, TLM_FOUR);
        nmhGetComponentLinkDescrMinLspBandwidth (i4CompIfIndex, u4DescId,
                                                 &CompBw);
        minLspBw = TLM_OCTETSTRING_TO_FLOAT ((&CompBw));

        TLM_CONVERT_BYTES_PER_SEC_TO_KBPS (minLspBw);

        if (minLspBw != TLM_ZERO)
        {
            CliPrintf (CliHandle, "\r\n  min lsp bandwidth %d",
                       (UINT4) minLspBw);
        }
    }

    nmhGetComponentLinkRowStatus (i4CompIfIndex, &i4RowStatus);

    if (i4RowStatus == ACTIVE)
    {
        CliPrintf (CliHandle, "\r\n   no shutdown");
        CliPrintf (CliHandle, "\r\n   exit");
    }
}

/**************************************************************************/
/*  Function Name   : TlmCliShowSwiCapType                                */
/*                                                                        */
/*  Description     : Maps and Prints the Switching capability with the   */
/*                    given input                                         */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4SwitchCap - value corresponds to switching -      */
/*                    capability                                          */
/*                    i4EncodType - value corresponds to Encoding Type    */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : NONE                                                */
/**************************************************************************/
PRIVATE VOID
TlmCliShowSwiCapType (tCliHandle CliHandle, INT4 i4SwitchCap, INT4 i4EncodType)
{
    UINT1               au1EncType[11][15] = { {"packet"}, {"ethernet"},
    {"ANSI/ETSI PDH"}, {"Reserved"},
    {"SONET"}, {"Reserved"},
    {"Digital Wrapper"},
    {"Lambda"}, {"Fiber"},
    {"Reserved"}, {"Fiber Channel"}
    };

    switch (i4SwitchCap)
    {
        case TLM_TE_PSC_1:
            CliPrintf (CliHandle,
                       "\r\n  switching-capability psc1 "
                       "encoding %s", au1EncType[i4EncodType - 1]);
            break;
        case TLM_TE_PSC_2:
            CliPrintf (CliHandle,
                       "\r\n  switching-capability psc2 "
                       "encoding %s", au1EncType[i4EncodType - 1]);
            break;
        case TLM_TE_PSC_3:
            CliPrintf (CliHandle,
                       "\r\n  switching-capability psc3 "
                       "encoding %s", au1EncType[i4EncodType - 1]);
            break;
        case TLM_TE_PSC_4:
            CliPrintf (CliHandle,
                       "\r\n  switching-capability psc4 "
                       "encoding %s", au1EncType[i4EncodType - 1]);
            break;
        case TLM_TE_L2SC:
            CliPrintf (CliHandle,
                       "\r\n  switching-capability l2sc "
                       "encoding %s", au1EncType[i4EncodType - 1]);
            break;
        case TLM_TE_TDM:
            CliPrintf (CliHandle,
                       "\r\n  switching-capability tdm "
                       "encoding %s", au1EncType[i4EncodType - 1]);
            break;
        case TLM_TE_LSC:
            CliPrintf (CliHandle,
                       "\r\n  switching-capability lsc "
                       "encoding %s", au1EncType[i4EncodType - 1]);
            break;
        case TLM_TE_FSC:
            CliPrintf (CliHandle,
                       "     Switching Cap      : fsc "
                       "encoding %s", au1EncType[i4EncodType - 1]);
            break;
        default:
            break;
    }
}

/**************************************************************************/
/*  Function Name   : TlmCliShowThresholdValues                           */
/*                                                                        */
/*  Description     : This function displays the Component Link           */
/*                    Configuration                                       */
/*                                                                        */
/*  Input(s)        : CliHandle   - CLI Handle                            */
/*                    i4TeIfIndex - TE-Link IfIndex                       */
/*                    i4BwThreshIndex - Threshhold Index                  */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : NONE                                                */
/**************************************************************************/
PRIVATE VOID
TlmCliShowThresholdValues (tCliHandle CliHandle,
                           INT4 i4TeIfIndex, INT4 i4BwThreshIndex)
{
    INT4                ai4ThresholdVal[TLM_MAX_BW_THRESHOLD_SUPPORTED];
    INT4                i4Count = TLM_ZERO;
    UINT1               u1Index1 = TLM_ZERO;
    UINT1               u1Index2 = TLM_ZERO;
    BOOL1               bHdrReqd = TRUE;
    BOOL1               bNonDefault = FALSE;

    MEMSET (ai4ThresholdVal, TLM_ZERO, sizeof (ai4ThresholdVal));

    nmhGetFsTeLinkBwThreshold0 (i4TeIfIndex, i4BwThreshIndex,
                                &ai4ThresholdVal[0]);
    nmhGetFsTeLinkBwThreshold1 (i4TeIfIndex, i4BwThreshIndex,
                                &ai4ThresholdVal[1]);
    nmhGetFsTeLinkBwThreshold2 (i4TeIfIndex, i4BwThreshIndex,
                                &ai4ThresholdVal[2]);
    nmhGetFsTeLinkBwThreshold3 (i4TeIfIndex, i4BwThreshIndex,
                                &ai4ThresholdVal[3]);
    nmhGetFsTeLinkBwThreshold4 (i4TeIfIndex, i4BwThreshIndex,
                                &ai4ThresholdVal[4]);
    nmhGetFsTeLinkBwThreshold5 (i4TeIfIndex, i4BwThreshIndex,
                                &ai4ThresholdVal[5]);
    nmhGetFsTeLinkBwThreshold6 (i4TeIfIndex, i4BwThreshIndex,
                                &ai4ThresholdVal[6]);
    nmhGetFsTeLinkBwThreshold7 (i4TeIfIndex, i4BwThreshIndex,
                                &ai4ThresholdVal[7]);
    nmhGetFsTeLinkBwThreshold8 (i4TeIfIndex, i4BwThreshIndex,
                                &ai4ThresholdVal[8]);
    nmhGetFsTeLinkBwThreshold9 (i4TeIfIndex, i4BwThreshIndex,
                                &ai4ThresholdVal[9]);
    nmhGetFsTeLinkBwThreshold10 (i4TeIfIndex, i4BwThreshIndex,
                                 &ai4ThresholdVal[10]);
    nmhGetFsTeLinkBwThreshold11 (i4TeIfIndex, i4BwThreshIndex,
                                 &ai4ThresholdVal[11]);
    nmhGetFsTeLinkBwThreshold12 (i4TeIfIndex, i4BwThreshIndex,
                                 &ai4ThresholdVal[12]);
    nmhGetFsTeLinkBwThreshold13 (i4TeIfIndex, i4BwThreshIndex,
                                 &ai4ThresholdVal[13]);
    nmhGetFsTeLinkBwThreshold14 (i4TeIfIndex, i4BwThreshIndex,
                                 &ai4ThresholdVal[14]);
    nmhGetFsTeLinkBwThreshold15 (i4TeIfIndex, i4BwThreshIndex,
                                 &ai4ThresholdVal[15]);

    if (i4BwThreshIndex == TLM_ONE)
    {
        for (u1Index1 = TLM_ZERO; u1Index1 < TLM_MAX_BW_THRESHOLD_SUPPORTED;
             u1Index1++)
        {
            if (ai4ThresholdVal[u1Index1] != gai4BwThresholds[u1Index1])
            {
                bNonDefault = TRUE;
                break;
            }
        }
    }
    else
    {
        for (u1Index1 = (TLM_MAX_BW_THRESHOLD_SUPPORTED - TLM_ONE), u1Index2 =
             TLM_ZERO;
             ((INT1) u1Index1 >= TLM_ZERO)
             && (u1Index2 < TLM_MAX_BW_THRESHOLD_SUPPORTED);
             u1Index1--, u1Index2++)
        {
            if (ai4ThresholdVal[u1Index1] != gai4BwThresholds[u1Index2])
            {
                bNonDefault = TRUE;
                break;
            }
        }
    }

    if (bNonDefault == FALSE)
    {
        return;
    }

    for (i4Count = TLM_ZERO; i4Count < TLM_MAX_BW_THRESHOLD_SUPPORTED;
         i4Count++)
    {
        if (ai4ThresholdVal[i4Count] != TLM_ZERO)
        {
            if (bHdrReqd == TRUE)
            {
                bHdrReqd = FALSE;

                CliPrintf (CliHandle, "\r\n  flooding thresholds ");

                if (i4BwThreshIndex == TLM_ONE)
                {
                    CliPrintf (CliHandle, "up");
                }
                else
                {
                    CliPrintf (CliHandle, "down");
                }
            }

            CliPrintf (CliHandle, " %d", ai4ThresholdVal[i4Count]);
        }
    }

    return;
}

/**************************************************************************/
/*  Function Name   : TlmCliShowTeLinkConfig                              */
/*                                                                        */
/*  Description     : This function  displays the TE-LINK configuration   */
/*                                                                        */
/*  Input(s)        : CliHandle - CliHandle                               */
/*                    i4TeIfIndex - TeLink If Index                       */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : None                                                */
/**************************************************************************/
PRIVATE VOID
TlmCliShowTeLinkConfig (tCliHandle CliHandle, INT4 i4TeIfIndex)
{
    tSNMP_OCTET_STRING_TYPE TeLinkLocalIp;
    tSNMP_OCTET_STRING_TYPE TeLinkRemoteIp;
    INT4                i4SrlgIfIndex = TLM_ZERO;
    INT4                i4AddrType = TLM_ZERO;
    INT4                i4IncomingIf = TLM_ZERO;
    INT4                i4OutgoingIf = TLM_ZERO;
    INT4                i4ProtectType = TLM_ZERO;
    INT4                i4CompIfIndex = TLM_ZERO;
    INT4                i4TeRowStatus = TLM_ZERO;
    INT4                i4TeLinkType = TLM_ZERO;
    UINT4               u4LocalIpAddr = TLM_ZERO;
    UINT4               u4RemoteIpAddr = TLM_ZERO;
    UINT4               u4RmtRtrIp = TLM_ZERO;
    UINT4               u4Metric = TLM_ZERO;
    UINT4               u4RsrcClass = TLM_ZERO;
    UINT4               u4SrlgIndex = TLM_ZERO;
    CHR1               *pu1String = NULL;
    UINT1               au1LocalIp[TLM_MAX_IP_LEN];
    UINT1               au1RemoteIp[TLM_MAX_IP_LEN];
    UINT1               au1ProcType[6][15] = { {"extra-traffic"},
    {"unprotected"},
    {"shared"}, {"dedicated1For1"},
    {"dedicated1Plus1"},
    {"enhanced"}
    };

	CliPrintf (CliHandle, "\r\n  shutdown");
    /* Get TE-LINK Remote Router IP */
    nmhGetFsTeLinkRemoteRtrId (i4TeIfIndex, &u4RmtRtrIp);

    if (u4RmtRtrIp != TLM_ZERO)
    {
        TLM_CLI_IPADDR_TO_STR (pu1String, u4RmtRtrIp);
        CliPrintf (CliHandle, "\r\n  remote router-id %s", pu1String);
    }
    /* Get TE-Link Address Type */
    nmhGetTeLinkAddressType (i4TeIfIndex, &i4AddrType);

    nmhGetTeLinkIncomingIfId (i4TeIfIndex, &i4IncomingIf);
    nmhGetTeLinkOutgoingIfId (i4TeIfIndex, &i4OutgoingIf);

    /* Get Local Ip and Remote Ip */
    TeLinkLocalIp.pu1_OctetList = &au1LocalIp[TLM_INDEX_ZERO];
    MEMSET (TeLinkLocalIp.pu1_OctetList, TLM_ZERO, TLM_MAX_IP_LEN);
    nmhGetTeLinkLocalIpAddr (i4TeIfIndex, &TeLinkLocalIp);
    TLM_OCTETSTRING_TO_INTEGER ((&TeLinkLocalIp), u4LocalIpAddr);

    TeLinkRemoteIp.pu1_OctetList = &au1RemoteIp[TLM_INDEX_ZERO];
    MEMSET (TeLinkRemoteIp.pu1_OctetList, TLM_ZERO, TLM_MAX_IP_LEN);
    nmhGetTeLinkRemoteIpAddr (i4TeIfIndex, &TeLinkRemoteIp);
    TLM_OCTETSTRING_TO_INTEGER ((&TeLinkRemoteIp), u4RemoteIpAddr);

    if ((i4AddrType == TLM_ADDRESSTYPE_NUMBERED_IPV4) &&
        (u4LocalIpAddr != TLM_ZERO) && (u4RemoteIpAddr != TLM_ZERO))
    {
        CliPrintf (CliHandle, "\r\n  address-type ipv4");

        TLM_CLI_IPADDR_TO_STR (pu1String, u4LocalIpAddr);
        CliPrintf (CliHandle, "\r\n  local te-link ipv4 %s", pu1String);

        TLM_CLI_IPADDR_TO_STR (pu1String, u4RemoteIpAddr);
        CliPrintf (CliHandle, " remote te-link ipv4 %s", pu1String);
    }
    else if ((i4AddrType == TLM_ADDRESSTYPE_UNKNOWN) &&
             (i4IncomingIf != TLM_DEF_LOCAL_IDENTIFIER) &&
             (i4OutgoingIf != TLM_DEF_REMOTE_IDENTIFIER))
    {
        /* Get Local ID and Remote ID */
        CliPrintf (CliHandle, "\r\n  address-type un-known");

        CliPrintf (CliHandle, "\r\n  local te-link unnum %d"
                   " remote te-link unnum %d", i4OutgoingIf, i4IncomingIf);
    }
    /* Get TE-LINK Protection Type */
    nmhGetTeLinkProtectionType (i4TeIfIndex, &i4ProtectType);
    if (i4ProtectType != TLM_DEF_PROTECTION_TYPE)
    {
        CliPrintf (CliHandle, "\r\n  protection-type %s",
                   au1ProcType[i4ProtectType - TLM_ONE]);
    }
    /* Get TE-LINK Metric */
    nmhGetTeLinkMetric (i4TeIfIndex, &u4Metric);
    if (u4Metric != TLM_DEF_METRIC_VALUE)
    {
        CliPrintf (CliHandle, "\r\n  metric %u", u4Metric);
    }
    /* Get TE-LINK Resource Class */
    nmhGetTeLinkResourceClass (i4TeIfIndex, &u4RsrcClass);
    if (u4RsrcClass != TLM_ZERO)
    {
        CliPrintf (CliHandle, "\r\n  resource-class class-value %u",
                   u4RsrcClass);
    }

    /* Get TE-LINK SRLG Values */
    i4SrlgIfIndex = i4TeIfIndex;

    while (nmhGetNextIndexTeLinkSrlgTable (i4SrlgIfIndex, &i4SrlgIfIndex,
                                           u4SrlgIndex, &u4SrlgIndex)
           == SNMP_SUCCESS)
    {
        if (i4SrlgIfIndex != i4TeIfIndex)
        {
            break;
        }

        CliPrintf (CliHandle, "\r\n  associate srlg %u", u4SrlgIndex);
    }

    TlmCliShowThresholdValues (CliHandle, i4TeIfIndex, TLM_ONE);
    TlmCliShowThresholdValues (CliHandle, i4TeIfIndex, TLM_TWO);

    CfaApiGetL3IfFromTeLinkIf ((UINT4) i4TeIfIndex, (UINT4 *) &i4CompIfIndex,
                               TRUE);
    TlmCliShowCompLinkConfig (CliHandle, i4CompIfIndex);

    nmhGetFsTeLinkType (i4TeIfIndex, &i4TeLinkType);

    if (i4TeLinkType == TLM_UNBUNDLE)
    {
        nmhGetTeLinkRowStatus (i4TeIfIndex, &i4TeRowStatus);

        if (i4TeRowStatus == ACTIVE)
        {
            CliPrintf (CliHandle, "\r\n  no shutdown");
            CliPrintf (CliHandle, "\r\n   exit");
        }
    }

    return;
}

/*****************************************************************************/
/*  Function    :  IssTlmCliShowDebugging                                    */
/*                                                                           */
/*  Description :  This function Shows the current debug                     */
/*                 configuration of TLM                                      */
/*                                                                           */
/*  Input       :  tCliHandle CliHandle                                      */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
VOID
IssTlmCliShowDebugging (tCliHandle CliHandle)
{

    INT4                i4Debug = TLM_ZERO;

    nmhGetFsTeLinkTraceOption (&i4Debug);

    if (i4Debug == TLM_ZERO)
    {
        return;
    }

    CliPrintf (CliHandle, "\rTLM: ");

    if (i4Debug & TLM_CRITICAL_TRC)
    {
        CliPrintf (CliHandle, "\r\n TLM Critical debugging is on");
    }
    if (i4Debug & TLM_FN_ENTRY_TRC)
    {
        CliPrintf (CliHandle, "\r\n TLM Function entry debugging is on");
    }
    if (i4Debug & TLM_FN_EXIT_TRC)
    {
        CliPrintf (CliHandle, "\r\n TLM Function exit debugging is on");
    }
    if (i4Debug & TLM_FAILURE_TRC)
    {
        CliPrintf (CliHandle, "\r\n TLM Failure debugging is on");
    }
    if (i4Debug & TLM_RESOURCE_TRC)
    {
        CliPrintf (CliHandle, "\r\n TLM Resource failure debugging is on");
    }
    if (i4Debug & TLM_MGMT_TRC)
    {
        CliPrintf (CliHandle, "\r\n TLM Management debugging is on");
    }
    CliPrintf (CliHandle, "\r\n");
}

/**************************************************************************/
/*  Function Name   : TlmCliSetTeLinkAdvertise                            */
/*                                                                        */
/*  Description     : This function enables/disables TE link advertisment */
/*                                                                        */
/*  Input(s)        : CliHandle - CliHandle                               */
/*                    i4TeLinkAdvt - Enable/Disable Advertisement         */
/*                    i4TeIfIndex - TE link IfIndex                       */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : None                                                */
/**************************************************************************/
INT1
TlmCliSetTeLinkAdvertise (tCliHandle CliHandle, INT4 i4TeLinkAdvt,
                          INT4 i4TeIfIndex)
{
    UINT4               u4ErrorCode = TLM_ZERO;

    if (nmhTestv2FsTeLinkIsAdvertise (&u4ErrorCode, i4TeIfIndex,
                                      i4TeLinkAdvt) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n Unable to Set TE Link Advertisement\n");
        return CLI_FAILURE;
    }

    nmhSetFsTeLinkIsAdvertise (i4TeIfIndex, i4TeLinkAdvt);

    return CLI_SUCCESS;
}
