/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: tlmrdm.c,v 1.10 2015/05/04 09:50:08 siva Exp $
 *
 * Description: Russian dolls model Bandwidth calculation
 ***********************************************************************/
#include "tlminc.h"
#include "tlmcli.h"

/******************************************************************************
 * Function Name      : TlmUtilRdmCalcUnResBw
 * 
 * Description        : This function is used to calculate Unreserved Bw &
 *                      Premptable Bw for TE-Class in case of DSTE enabled.
 *
 * Input(s)           : i4IfIndex - Interface Index of the te-link.
 *                      u1TeClass - Value of Te-Class.
 *                      
 * Output(s)          : unResBw - returns the value of the Unres bw.
 *                      freeBw -  returns the value of the free bw. 
 *
 * Return Value(s)    : TLM_SUCCESS / TLM_FAILURE
 *
 ******************************************************************************/
INT4
TlmUtilRdmCalcUnResBw (tTlmComponentLink * pComponentIface, UINT1 u1TeClass,
                       tBandWidth * unResBw)
{
    tBandWidth          minUnResBw = (tBandWidth) TLM_ZERO;
    tBandWidth          tempUnResBw = (tBandWidth) TLM_ZERO;
    /*Reserved Bandwidth */
    tBandWidth          resBw = (tBandWidth) TLM_ZERO;
    UINT1               u1MinBwCheck = TLM_ZERO;
    UINT1               u1ClassType = TLM_ZERO;
    UINT1               u1PremPrio = TLM_ZERO;
    UINT1               u1CTLoop = TLM_ZERO;
    UINT1               u1IntCTLoop = TLM_ZERO;
    INT1                i1PrioLoop = TLM_ZERO;

    if (!(IS_VALID_TE_CLASS (u1TeClass)))
    {
        TLM_TRC1 (TLM_FAILURE_TRC,
                  "TlmUtilRdmCalcUnResBw: Invalid te class %u.\n", u1TeClass);
        return TLM_FAILURE;
    }

    u1ClassType = TLM_TE_CLASS_INFO (u1TeClass).u1ClassType;
    u1PremPrio = TLM_TE_CLASS_INFO (u1TeClass).u1PremptPrio;

    /******************************************************************* 
       Loop to calculate the UnresBw Bandwidth for the given Te-Class by using the 
       Russian Dolls Model
       
       --------------------------------------------------------
    If CTc = CT0, then "Unreserved TE-Class [i]" =
       [ BC0 - SUM ( Reserved(CTb,q) ) ] for q <= p and 0 <= b <= 2
       
    If CTc = CT1, then "Unreserved TE-Class [i]" =
         MIN  [
         [ BC1 - SUM ( Reserved(CTb,q) ) ] for q <= p and 1 <= b <= 2,
         [ BC0 - SUM ( Reserved(CTb,q) ) ] for q <= p and 0 <= b <= 2
              ]
    --------------------------------------------------------
    
    For Loop1:
     a) for BC0, loop will run only once 
        b) for BC1, loop will run for BC0 and BC1 and the min of the two bandwidths shall 
            be taken into consideration
        ********************************************************************/

    for (u1CTLoop = TLM_ZERO; u1CTLoop <= u1ClassType; u1CTLoop++)
    {
        /* To Check whether class type is supported or not in the system */
        if (TlmUtilCheckSupportedClassType (u1CTLoop) == TLM_FALSE)
        {
            continue;
        }
        tempUnResBw = (tBandWidth) TLM_ZERO;
        resBw = (tBandWidth) TLM_ZERO;

        /************************************************
              For Loop2:
              a) Shall be run from 0 to b for BC0
              b) Shall run from 1 to b for BC1
              For Loop3:
              a) Shall run for all TE-Classes i.e. on the basis of priority,
              Loop1 & Loop2 will find the SUM ( Reserved(CTb,q)     
        *************************************************/

        for (u1IntCTLoop = u1CTLoop; u1IntCTLoop < TLM_MAX_TE_CLASS_TYPE;
             u1IntCTLoop++)
        {
            /* To Check whether class type is supported or not in the system */
            if (TlmUtilCheckSupportedClassType (u1IntCTLoop) == TLM_FALSE)
            {
                continue;
            }

            /* Loop to calculate the reserved Bw for current ClassType */
            for (i1PrioLoop = TLM_MAX_TE_CLASS - TLM_ONE;
                 i1PrioLoop >= TLM_ZERO; i1PrioLoop--)
            {
                /* Function to find the Reserved bw for the Te-Class so this 
                 * finds the Te-Class for the current 
                 * Priority and current Class Type*/
                if (TlmUtilGetTeClsfromPrioClsType
                    (*(UINT1 *) &i1PrioLoop, u1IntCTLoop,
                     &u1TeClass) == TLM_SUCCESS)
                {
                    if (i1PrioLoop <= u1PremPrio)
                    {
                        resBw = TlmFloatAdd (resBw,
                                             pComponentIface->
                                             aUnResBw[u1TeClass].resBw);
                    }
                }
            }
        }
        /* calculating the Unreserved Bw for the current Class Type */
        tempUnResBw = TlmFloatSubtract (pComponentIface->aBC[u1CTLoop], resBw);

        /* If this first time of the loop then u1MinBwCheck value will be zero
         * so the minUnResBw will be minimum Unresrved bandwidth*/
        if (u1MinBwCheck == TLM_ZERO)
        {
            u1MinBwCheck = TLM_ONE;
            minUnResBw = tempUnResBw;
        }
        else
        {
            if (tempUnResBw < minUnResBw)
            {
                minUnResBw = tempUnResBw;
            }
        }
    }

    *unResBw = minUnResBw;

    return TLM_SUCCESS;
}

/******************************************************************************
 * Function Name      : TlmUpdateTeLinkUnResvBw                                                     
 *
 * Description        : This function is used to update the unreserved bandwidth
 *                      information required to RSVP-TE.
 *
 * Input(s)           : u1ResOrRel         - Reserve/Release
 *                      pInTlmTeLinkInfo   - Inputs to the TE link params 
 * Output(s)          : pOutTlmTeLinkInfo  - The return data structure of the
 *                                           Te-Link parameters.
 * Return Value(s)    : TLM_SUCCESS/TLM_FAILURE
 *
 *******************************************************************************/
INT4
TlmUpdateTeLinkUnResvBw (UINT1 u1ResOrRel, tInTlmTeLink * pInTlmTeLinkInfo,
                         tOutTlmTeLink * pOutTlmTeLinkInfo)
{
    tTMO_SLL_NODE      *pTeLinkNode = NULL;
    tTlmTeLink         *pTeLink = NULL;
    INT4                i4RetVal = TLM_FAILURE;
    tTlmTeLink         *pUnBundledTeLink = NULL;

    if (gTlmGlobalInfo.i4ModuleStatus == TLM_DISABLED)
    {
        return TLM_FAILURE;
    }

    pTeLink = TlmFindTeIf ((INT4) pInTlmTeLinkInfo->u4IfIndex);

    if (pTeLink == NULL)
    {
        return TLM_FAILURE;
    }

    if (pTeLink->u1OperStatus == TLM_OPER_DOWN)
    {
        if (u1ResOrRel == TLM_TRUE)
        {
            return TLM_FAILURE;
        }
    }
 

    if (pTeLink->i4TeLinkType == TLM_BUNDLE &&
                ((pTeLink->i4RowStatus == NOT_IN_SERVICE)||
                (pTeLink->i4RowStatus == ACTIVE)))
    {
        TMO_SLL_Scan (&(pTeLink->sortUnbundleTeList), pTeLinkNode,
                      tTMO_SLL_NODE *)
        {
            pUnBundledTeLink =
                GET_TE_LINK_TE_LIST_NODE_PTR_FROM_SORT_LST (pTeLinkNode);

            i4RetVal
                = TlmUpdateUnResvBwForUnBundleTeLink (u1ResOrRel,
                                                      pInTlmTeLinkInfo,
                                                      pOutTlmTeLinkInfo,
                                                      pUnBundledTeLink);

            if (i4RetVal == TLM_FAILURE)
            {
                continue;
            }
            else
            {
                break;
            }
        }
    }
    else
    {
        i4RetVal
            = TlmUpdateUnResvBwForUnBundleTeLink (u1ResOrRel,
                                                  pInTlmTeLinkInfo,
                                                  pOutTlmTeLinkInfo, pTeLink);
    }

    return i4RetVal;
}

/******************************************************************************
 * Function Name      : TlmGetTeLinkUnResvBw
 *
 * Description        : This function is used by RSVP-TE  to Get the
 *                      the Unreserved bandwidth of a TE link .
 *
 * Input(s)           : pInTlmTeLinkInfo - Inputs to the TE link params 
 *
 * Output(s)          : pOutTlmTeLinkInfo - UnReserved BW and Available BW
 *                      of a TE-link interface
 *
 * Return Value(s)    : TLM_SUCCESS / TLM_FAILURE
 *
 ******************************************************************************/
INT4
TlmGetTeLinkUnResvBw (tInTlmTeLink * pInTlmTeLinkInfo,
                      tOutTlmTeLink * pOutTlmTeLinkInfo)
{
    tTlmTeLink         *pTeLink = NULL;
    tBandWidth          unResvBw = TLM_ZERO;
    tBandWidth          availBw = TLM_ZERO;
    tBandWidth          maxResBw = TLM_ZERO;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTlmTeLink         *pTempTeListNode = NULL;
    INT4                i4IfIndex = TLM_ZERO;

    if (gTlmGlobalInfo.i4ModuleStatus == TLM_DISABLED)
    {
        return TLM_FAILURE;
    }

    i4IfIndex = (INT4) pInTlmTeLinkInfo->u4IfIndex;

    pTeLink = TlmFindTeIf (i4IfIndex);

    if (pTeLink == NULL)
    {
        return TLM_FAILURE;
    }

    if (pTeLink->i4TeLinkType == TLM_BUNDLE)
    {
        TMO_SLL_Scan (&(pTeLink->sortUnbundleTeList), pLstNode, tTMO_SLL_NODE *)
        {
            pTempTeListNode =
                GET_TE_LINK_TE_LIST_NODE_PTR_FROM_SORT_LST (pLstNode);

            if (unResvBw <
                pTempTeListNode->aUnResBw[pInTlmTeLinkInfo->u4TeClass].unResBw)
            {
                unResvBw +=
                    pTempTeListNode->aUnResBw[pInTlmTeLinkInfo->u4TeClass].
                    unResBw;
            }
            if (availBw < pTempTeListNode->availBw)
            {
                availBw = pTempTeListNode->availBw;
            }
            if (maxResBw < pTempTeListNode->maxResBw)
            {
                maxResBw = pTempTeListNode->maxResBw;
            }
        }
        pOutTlmTeLinkInfo->unResBw = unResvBw;
        pOutTlmTeLinkInfo->availBw = availBw;
        pOutTlmTeLinkInfo->maxResBw = maxResBw;
        pOutTlmTeLinkInfo->u1OperStatus = pTeLink->u1OperStatus;
    }
    else
    {
        pOutTlmTeLinkInfo->unResBw =
            pTeLink->aUnResBw[pInTlmTeLinkInfo->u4TeClass].unResBw;
        pOutTlmTeLinkInfo->availBw = pTeLink->availBw;
        pOutTlmTeLinkInfo->maxResBw = pTeLink->maxResBw;
        pOutTlmTeLinkInfo->u1OperStatus = pTeLink->u1OperStatus;
    }

    return TLM_SUCCESS;
}

/******************************************************************************
 * Function Name      : TlmUpdateUnResvBwForUnBundleTeLink
 *
 * Description        : This function is used to update the unreserved bandwidth
 *                      information required to RSVP-TE.
 *
 * Input(s)           : u1ResOrRel         - Reserve/Release
 *                      pInTlmTeLinkInfo   - Inputs to the TE link params 
 * Output(s)          : pOutTlmTeLinkInfo  - The return data structure of the
 *                                           Te-Link parameters.
 * Return Value(s)    : TLM_SUCCESS/TLM_FAILURE
 *
 *******************************************************************************/
INT4
TlmUpdateUnResvBwForUnBundleTeLink (UINT1 u1ResOrRel,
                                    tInTlmTeLink * pInTlmTeLinkInfo,
                                    tOutTlmTeLink * pOutTlmTeLinkInfo,
                                    tTlmTeLink * pTeLink)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTlmComponentLink  *pCompLink = NULL;
    tTlmComponentIfDescr *pCompDescr = NULL;
    tBandWidth          reqBw = TLM_ZERO;
    tBandWidth          changeBwPerc = TLM_ZERO;
    tBandWidth          prevBw = TLM_ZERO;
    UINT4               u4TeClass = TLM_ZERO;
    INT4                i4RetVal = TLM_FAILURE;
    INT4                i4BwChange = TLM_ZERO;

    reqBw = pInTlmTeLinkInfo->reqResBw;
    u4TeClass = pInTlmTeLinkInfo->u4TeClass;


    /* Currently TE Link is associated with only one component link */
    pLstNode = TMO_SLL_First (&(pTeLink->ComponentList));


    if (pLstNode == NULL)
    {
        if((pTeLink->u1OperStatus == TLM_OPER_DOWN) && 
            (u1ResOrRel == TLM_TRUE))
        {
            return TLM_FAILURE;
        } 
        return TLM_SUCCESS;
    }

    pCompLink = GET_COMPONENT_IF_PTR_FROM_SORT_TE_LST (pLstNode);

    if ((u1ResOrRel == TLM_FALSE) &&
        (pCompLink->i4IfIndex != (INT4) pInTlmTeLinkInfo->u4CompIfIndex))
    {
        return TLM_FAILURE;
    }

    pCompDescr = TlmFindComponentDescr (TLM_COMPLINK_DESCRID, pCompLink);

    if (pCompDescr == NULL)
    {
        return TLM_FAILURE;
    }

    switch (u1ResOrRel)
    {
        case TLM_TRUE:
            if (reqBw < pCompDescr->minLSPBw)
            {
                i4RetVal = TLM_FAILURE;
                break;
            }

            if ((reqBw > pCompLink->availBw) ||
                (reqBw > pCompLink->aUnResBw[u4TeClass].unResBw))
            {
                i4RetVal = TLM_FAILURE;
                break;
            }

            pCompLink->aUnResBw[u4TeClass].resBw += reqBw;

            TlmUtilRdmCalcUnResBwForAllTeClass (pTeLink, pCompLink, u1ResOrRel);

            prevBw = pCompLink->availBw;

            pCompLink->availBw -= reqBw;
	    


            if (pCompLink->availBw <= TLM_ZERO)
            {
                pCompLink->availBw = TLM_ZERO;
            }

            pTeLink->availBw = pCompLink->availBw;

	    TlmUpdateTeLinkInfo(pCompLink,pTeLink,TLM_ONE);

            changeBwPerc = (((prevBw - pCompLink->availBw) * 100) / prevBw);

            i4BwChange = TLM_RESBW_INCREASE;

            pOutTlmTeLinkInfo->u4CompIfIndex = (UINT4) pCompLink->i4IfIndex;

            TlmUtilCalculateBwForBundledTeLink (pTeLink);

            i4RetVal = TLM_SUCCESS;
            break;

        case TLM_FALSE:

            if (reqBw > pCompLink->maxResBw)
            {
                i4RetVal = TLM_FAILURE;
                break;
            }

            pCompLink->aUnResBw[u4TeClass].resBw -= reqBw;

            TlmUtilRdmCalcUnResBwForAllTeClass (pTeLink, pCompLink, u1ResOrRel);

            prevBw = pCompLink->availBw;

            pCompLink->availBw += reqBw;
		

            pTeLink->availBw = pCompLink->availBw;

	    TlmUpdateTeLinkInfo(pCompLink,pTeLink,TLM_ONE);

            changeBwPerc = (((prevBw + pCompLink->availBw) * 100) / prevBw);

            i4BwChange = TLM_RESBW_DECREASE;

            pOutTlmTeLinkInfo->u4CompIfIndex = (UINT4) pCompLink->i4IfIndex;

            TlmUtilCalculateBwForBundledTeLink (pTeLink);

            i4RetVal = TLM_SUCCESS;
            break;
        default:
            break;
    }

    TlmUtilFloodBwForThreshold (pTeLink, changeBwPerc, i4BwChange);

    if (pTeLink->pBundledTeLink != NULL)
    {
        TlmUtilFloodBwForThreshold (pTeLink->pBundledTeLink, changeBwPerc,
                                    i4BwChange);
    }

    return i4RetVal;
}

/******************************************************************************
 * Function Name      : TlmUtilRdmCalcUnResBwForAllTeClass
 *
 * Description        : This function is used to calculate unreserved bandwidth
 *                      for all TE class
 *
 * Input(s)           : pCompLink          - Pointer to Component Link
 *                      pInTlmTeLinkInfo   - Inputs to the TE link params 
 *                      u1ResOrRel         - Indicates whether resource is reserved
 *                                           or released
 * Output(s)          : None
 * Return Value(s)    : None
 *
 *******************************************************************************/
VOID
TlmUtilRdmCalcUnResBwForAllTeClass (tTlmTeLink * pTeLink,
                                    tTlmComponentLink * pCompLink,
                                    UINT1 u1ResOrRel)
{
    UINT1               u1TeClass = TLM_ZERO;
    tBandWidth          unResBw = TLM_ZERO;

    for (u1TeClass = TLM_ZERO; u1TeClass < TLM_MAX_TE_CLASS; u1TeClass++)
    {
        if (TlmUtilRdmCalcUnResBw (pCompLink, u1TeClass, &unResBw)
            == TLM_FAILURE)
        {
            continue;
        }

        pCompLink->aUnResBw[u1TeClass].unResBw = unResBw;

        pTeLink->aUnResBw[u1TeClass].unResBw = unResBw;

        if (pTeLink->pBundledTeLink != NULL)
        {
            if (u1ResOrRel == TLM_TRUE)
            {
                pTeLink->pBundledTeLink->aUnResBw[u1TeClass].unResBw -= unResBw;
            }
            else
            {
                pTeLink->pBundledTeLink->aUnResBw[u1TeClass].unResBw += unResBw;
            }
        }
    }

    return;
}

/******************************************************************************
 * Function Name      : TlmUtilCalculateBwForBundledTeLink
 *
 * Description        : This function is used to calculate available bandwidth
 *                      and unreserved bandwidth per TE class for the bundled
 *                      TE link.
 *
 * Input(s)           : pTeLink            - Pointer to TE link
 * Output(s)          : None
 * Return Value(s)    : None
 *
 *******************************************************************************/
VOID
TlmUtilCalculateBwForBundledTeLink (tTlmTeLink * pTeLink)
{
    UINT4               u4TeClass = TLM_ZERO;
    tTlmTeLink         *pBundledTeLink = NULL;
    tTlmTeLink         *pUnBundledTeLink = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    if (pTeLink->pBundledTeLink == NULL)
    {
        return;
    }

    pBundledTeLink = pTeLink->pBundledTeLink;

    for (u4TeClass = TLM_ZERO; u4TeClass < TLM_MAX_TE_CLASS; u4TeClass++)
    {
        pBundledTeLink->aUnResBw[u4TeClass].unResBw = TLM_ZERO;
    }

    pBundledTeLink->availBw = TLM_ZERO;

    TMO_SLL_Scan (&(pBundledTeLink->sortUnbundleTeList), pLstNode,
                  tTMO_SLL_NODE *)
    {
        pUnBundledTeLink =
            GET_TE_LINK_TE_LIST_NODE_PTR_FROM_SORT_LST (pLstNode);

	if ( pUnBundledTeLink->u1OperStatus == TLM_OPER_DOWN)
	{
		break;
	} 
        for (u4TeClass = TLM_ZERO; u4TeClass < TLM_MAX_TE_CLASS; u4TeClass++)
        {
            pBundledTeLink->aUnResBw[u4TeClass].unResBw
                += pUnBundledTeLink->aUnResBw[u4TeClass].unResBw;
        }

        pBundledTeLink->availBw += pUnBundledTeLink->availBw;
    }

    return;
}
