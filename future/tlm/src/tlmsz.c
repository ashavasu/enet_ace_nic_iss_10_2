/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: tlmsz.c,v 1.2 2014/07/09 13:23:17 siva Exp $
 *
 * Description: This file contains the code for MemPools
 ***********************************************************************/
#define _TLMSZ_C
#include "tlminc.h"
INT4
TlmSizingMemCreateMemPools ()
{
    UINT4               u4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < TLM_MAX_SIZING_ID; i4SizingId++)
    {
        u4RetVal = MemCreateMemPool (FsTLMSizingParams[i4SizingId].u4StructSize,
                                     FsTLMSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(TLMMemPoolIds[i4SizingId]));
        if (u4RetVal == MEM_FAILURE)
        {
            TlmSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
TlmSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsTLMSizingParams);
    return OSIX_SUCCESS;
}

VOID
TlmSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < TLM_MAX_SIZING_ID; i4SizingId++)
    {
        if (TLMMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (TLMMemPoolIds[i4SizingId]);
            TLMMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
