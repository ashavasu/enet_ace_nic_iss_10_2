/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: tlmget.c,v 1.6 2014/07/09 13:23:18 siva Exp $
*
* Description: This file contains the routines for the protocol
               Database Access for the module TLM
*********************************************************************/
#include "tlminc.h"
#include "tlmcli.h"

/****************************************************************************
 Function    :  TlmGetAllTeLinkTable
 Input       :  i4IfIndex
                pTlmTeLink
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmGetAllTeLinkTable (INT4 i4IfIndex, tTlmTeLink * pTlmTeLink)
{
    tTlmTeLink         *pTeIface = NULL;
    INT4                i4TeLinkNameLen = TLM_ZERO;

    pTeIface = TlmFindTeIf (i4IfIndex);
    if (pTeIface == NULL)
    {
        TLM_TRC1 (TLM_ALL_TRC | TLM_FAILURE_TRC | TLM_MGMT_TRC,
                  "TlmGetAllTeLinkTable: No Te-Link Information "
                  "for %d Index\r\n", i4IfIndex);
        return SNMP_FAILURE;
    }

    pTlmTeLink->i4AddressType = pTeIface->i4AddressType;

    pTlmTeLink->u4TeMetric = pTeIface->u4TeMetric;

    pTlmTeLink->maxResBw = pTeIface->maxResBw;

    pTlmTeLink->i4ProtectionType = pTeIface->i4ProtectionType;

    pTlmTeLink->u4WorkingPriority = pTeIface->u4WorkingPriority;

    pTlmTeLink->u4RsrcClassColor = pTeIface->u4RsrcClassColor;

    pTlmTeLink->LocalIpAddr.u4_addr[0] = pTeIface->LocalIpAddr.u4_addr[0];

    pTlmTeLink->RemoteIpAddr.u4_addr[0] = pTeIface->RemoteIpAddr.u4_addr[0];

    pTlmTeLink->i4RowStatus = pTeIface->i4RowStatus;

    pTlmTeLink->i4StorageType = pTeIface->i4StorageType;

    i4TeLinkNameLen = (INT4) TLM_STRLEN (pTeIface->au1TeLinkName);

    MEMCPY (pTlmTeLink->au1TeLinkName, pTeIface->au1TeLinkName,
            i4TeLinkNameLen);

    pTlmTeLink->u4RemoteRtrId = pTeIface->u4RemoteRtrId;

    pTlmTeLink->u4LocalIdentifier = pTeIface->u4LocalIdentifier;
    pTlmTeLink->u4RemoteIdentifier = pTeIface->u4RemoteIdentifier;

    pTlmTeLink->maxBw = pTeIface->maxBw;

    pTlmTeLink->i4TeLinkType = pTeIface->i4TeLinkType;

    pTlmTeLink->i4TeLinkIfType = pTeIface->i4TeLinkIfType;

    pTlmTeLink->u1InfoType = pTeIface->u1InfoType;
    pTlmTeLink->i4IsTeLinkAdvertise = pTeIface->i4IsTeLinkAdvertise;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TlmGetAllTeLinkDescriptorTable
 Input       :  i4IfIndex
                u4TeLinkDescriptorId
                pTlmTeIfDescr
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmGetAllTeLinkDescriptorTable (INT4 i4IfIndex, UINT4 u4TeLinkDescriptorId,
                                tTlmTeIfDescr * pTlmTeIfDescr)
{
    tTlmTeLink         *pTeIface = NULL;
    tTlmTeIfDescr      *pTeDescr = NULL;
    UINT4               u4DescrId = u4TeLinkDescriptorId;

    pTeIface = TlmFindTeIf (i4IfIndex);
    if (pTeIface == NULL)
    {
        TLM_TRC1 (TLM_ALL_TRC | TLM_FAILURE_TRC | TLM_MGMT_TRC,
                  "TlmGetAllTeLinkDescriptorTable: No Te-Link Information"
                  "for %d Index\r\n", i4IfIndex);

        return SNMP_FAILURE;
    }

    pTeDescr = TlmFindTeDescr (u4DescrId, pTeIface);
    if (pTeDescr == NULL)
    {
        TLM_TRC1 (TLM_ALL_TRC | TLM_FAILURE_TRC | TLM_MGMT_TRC,
                  "TlmGetAllTeLinkDescriptorTable: No Te-Link Desc Information"
                  "for %d Index\r\n", i4IfIndex);
        return SNMP_FAILURE;
    }

    pTlmTeIfDescr->i4SwitchingCap = pTeDescr->i4SwitchingCap;

    pTlmTeIfDescr->i4EncodingType = pTeDescr->i4EncodingType;

    pTlmTeIfDescr->minLSPBw = pTeDescr->minLSPBw;

    pTlmTeIfDescr->aMaxLSPBwPrio[PRIORITY0] =
        pTeDescr->aMaxLSPBwPrio[PRIORITY0];
    pTlmTeIfDescr->aMaxLSPBwPrio[PRIORITY1] =
        pTeDescr->aMaxLSPBwPrio[PRIORITY1];
    pTlmTeIfDescr->aMaxLSPBwPrio[PRIORITY2] =
        pTeDescr->aMaxLSPBwPrio[PRIORITY2];
    pTlmTeIfDescr->aMaxLSPBwPrio[PRIORITY3] =
        pTeDescr->aMaxLSPBwPrio[PRIORITY3];
    pTlmTeIfDescr->aMaxLSPBwPrio[PRIORITY4] =
        pTeDescr->aMaxLSPBwPrio[PRIORITY4];
    pTlmTeIfDescr->aMaxLSPBwPrio[PRIORITY5] =
        pTeDescr->aMaxLSPBwPrio[PRIORITY5];
    pTlmTeIfDescr->aMaxLSPBwPrio[PRIORITY6] =
        pTeDescr->aMaxLSPBwPrio[PRIORITY6];
    pTlmTeIfDescr->aMaxLSPBwPrio[PRIORITY7] =
        pTeDescr->aMaxLSPBwPrio[PRIORITY7];

    pTlmTeIfDescr->u2MTU = pTeDescr->u2MTU;
    pTlmTeIfDescr->i4Indication = pTeDescr->i4Indication;
    pTlmTeIfDescr->i4RowStatus = pTeDescr->i4RowStatus;
    pTlmTeIfDescr->i4StorageType = pTeDescr->i4StorageType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TlmGetAllTeLinkSrlgTable
 Input       :  i4IfIndex
                u4TeLinkSrlg
                pTlmGetStdTlmTeLinkSrlgTable
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmGetAllTeLinkSrlgTable (INT4 i4IfIndex, UINT4 u4TeLinkSrlg,
                          tTlmTeLinkSrlg * pTlmTeLinkSrlg)
{
    tTlmTeLink         *pTeIface = NULL;
    tTlmTeLinkSrlg     *pTeSrlg = NULL;
    UINT4               u4Srlg = u4TeLinkSrlg;

    if ((pTeIface = TlmFindTeIf (i4IfIndex)) == NULL)
    {
        TLM_TRC1 (TLM_ALL_TRC | TLM_FAILURE_TRC | TLM_MGMT_TRC,
                  "TlmGetAllTeLinkSrlgTable: No Te-Link Information"
                  "for %d Index\r\n", i4IfIndex);

        return SNMP_FAILURE;
    }
    else if ((pTeSrlg = TlmFindTeSrlgIf (u4Srlg, pTeIface)) == NULL)
    {
        TLM_TRC1 (TLM_ALL_TRC | TLM_FAILURE_TRC | TLM_MGMT_TRC,
                  "TlmGetAllTeLinkSrlgTable: No Srlg Information"
                  "for %d Index\r\n", i4IfIndex);
        return SNMP_FAILURE;
    }

    pTlmTeLinkSrlg->i4RowStatus = pTeSrlg->i4RowStatus;
    pTlmTeLinkSrlg->i4StorageType = pTeSrlg->i4StorageType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TlmGetAllTeLinkBandwidthTable
 Input       :  i4IfIndex
                u4TeLinkBandwidthPriority
                pTlmTeLink
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmGetAllTeLinkBandwidthTable (INT4 i4IfIndex, UINT4 u4TeLinkBandwidthPriority,
                               tTlmTeLink * pTlmTeLink)
{
    tTlmTeLink         *pTeIface = NULL;
    UINT4               u4UnResBwPrio = u4TeLinkBandwidthPriority;

    if ((pTeIface = TlmFindTeIf (i4IfIndex)) == NULL)
    {
        TLM_TRC1 (TLM_ALL_TRC | TLM_FAILURE_TRC | TLM_MGMT_TRC,
                  "TlmSetAllTeLinkBandwidthTable: No Te-Link BW Information"
                  "for %d Index\r\n", i4IfIndex);
        return SNMP_FAILURE;
    }

    pTlmTeLink->aUnResBw[u4UnResBwPrio].i4RowStatus =
        pTeIface->aUnResBw[u4UnResBwPrio].i4RowStatus;
    pTlmTeLink->aUnResBw[u4UnResBwPrio].i4StorageType =
        pTeIface->aUnResBw[u4UnResBwPrio].i4StorageType;
    pTlmTeLink->aUnResBw[u4UnResBwPrio].unResBw =
        pTeIface->aUnResBw[u4UnResBwPrio].unResBw;
    pTlmTeLink->aUnResBw[u4UnResBwPrio].resBw =
        pTeIface->aUnResBw[u4UnResBwPrio].resBw;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TlmGetAllComponentLinkTable
 Input       :  i4IfIndex
                pTlmComponentLink
                u4ObjectId
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmGetAllComponentLinkTable (INT4 i4IfIndex,
                             tTlmComponentLink * pTlmComponentLink)
{
    tTlmComponentLink  *pComponentIface = NULL;

    if ((pComponentIface = TlmFindComponentIf (i4IfIndex)) == NULL)
    {
        TLM_TRC1 (TLM_ALL_TRC | TLM_FAILURE_TRC | TLM_MGMT_TRC,
                  "TlmGetAllComponentLinkTable: No Comp-link Information"
                  "for %d Index\r\n", i4IfIndex);
        return SNMP_FAILURE;
    }

    pTlmComponentLink->maxResBw = pComponentIface->maxResBw;

    pTlmComponentLink->i4LinkPrefProtection =
        pComponentIface->i4LinkPrefProtection;

    pTlmComponentLink->i4LinkCurrentProtection =
        pComponentIface->i4LinkCurrentProtection;

    pTlmComponentLink->i4RowStatus = pComponentIface->i4RowStatus;
    pTlmComponentLink->i4StorageType = pComponentIface->i4StorageType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TlmGetAllComponentLinkDescriptorTable
 Input       :  i4IfIndex
                u4ComponentLinkDescrId
                pTlmComponentIfDescr  
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmGetAllComponentLinkDescriptorTable (INT4 i4IfIndex,
                                       UINT4 u4ComponentLinkDescrId,
                                       tTlmComponentIfDescr *
                                       pTlmComponentIfDescr)
{
    tTlmComponentLink  *pComponentIface = NULL;
    tTlmComponentIfDescr *pComponentDescr = NULL;
    UINT4               u4DescrId = u4ComponentLinkDescrId;

    if ((pComponentIface = TlmFindComponentIf (i4IfIndex)) == NULL)
    {
        TLM_TRC1 (TLM_ALL_TRC | TLM_FAILURE_TRC | TLM_MGMT_TRC,
                  "TlmGetAllComponentLinkDescriptorTable: "
                  "No Comp-link Information for %d Index\r\n", i4IfIndex);

        return SNMP_FAILURE;
    }
    else if ((pComponentDescr =
              TlmFindComponentDescr (u4DescrId, pComponentIface)) == NULL)
    {
        TLM_TRC1 (TLM_ALL_TRC | TLM_FAILURE_TRC | TLM_MGMT_TRC,
                  "TlmGetAllComponentLinkDescriptorTable: "
                  "No Comp-link Desc Information for %d Index\r\n", i4IfIndex);
        return SNMP_FAILURE;
    }

    pTlmComponentIfDescr->i4SwitchingCap = pComponentDescr->i4SwitchingCap;
    pTlmComponentIfDescr->i4EncodingType = pComponentDescr->i4EncodingType;
    pTlmComponentIfDescr->minLSPBw = pComponentDescr->minLSPBw;
    pTlmComponentIfDescr->aMaxLSPBwPrio[PRIORITY0] =
        pComponentDescr->aMaxLSPBwPrio[PRIORITY0];

    pTlmComponentIfDescr->aMaxLSPBwPrio[PRIORITY1] =
        pComponentDescr->aMaxLSPBwPrio[PRIORITY1];
    pTlmComponentIfDescr->aMaxLSPBwPrio[PRIORITY2] =
        pComponentDescr->aMaxLSPBwPrio[PRIORITY2];
    pTlmComponentIfDescr->aMaxLSPBwPrio[PRIORITY3] =
        pComponentDescr->aMaxLSPBwPrio[PRIORITY3];
    pTlmComponentIfDescr->aMaxLSPBwPrio[PRIORITY4] =
        pComponentDescr->aMaxLSPBwPrio[PRIORITY4];
    pTlmComponentIfDescr->aMaxLSPBwPrio[PRIORITY5] =
        pComponentDescr->aMaxLSPBwPrio[PRIORITY5];
    pTlmComponentIfDescr->aMaxLSPBwPrio[PRIORITY6] =
        pComponentDescr->aMaxLSPBwPrio[PRIORITY6];
    pTlmComponentIfDescr->aMaxLSPBwPrio[PRIORITY7] =
        pComponentDescr->aMaxLSPBwPrio[PRIORITY7];

    pTlmComponentIfDescr->u2MTU = pComponentDescr->u2MTU;
    pTlmComponentIfDescr->i4Indication = pComponentDescr->i4Indication;
    pTlmComponentIfDescr->i4RowStatus = pComponentDescr->i4RowStatus;
    pTlmComponentIfDescr->i4StorageType = pComponentDescr->i4StorageType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TlmGetAllComponentLinkDescriptorTable
 Input       :  i4IfIndex
                u4ComponentLinkBandwidthPriority
                pTlmComponentLink
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmGetAllComponentLinkBandwidthTable (INT4 i4IfIndex,
                                      UINT4 u4ComponentLinkBandwidthPriority,
                                      tTlmComponentLink * pTlmComponentLink)
{
    tTlmComponentLink  *pComponentIface = NULL;
    UINT4               u4UnResBwPrio = u4ComponentLinkBandwidthPriority;

    if ((pComponentIface = TlmFindComponentIf (i4IfIndex)) == NULL)
    {
        TLM_TRC1 (TLM_ALL_TRC | TLM_FAILURE_TRC | TLM_MGMT_TRC,
                  "TlmGetAllComponentLinkDescriptorTable: "
                  "No Comp-link BW Information for %d Index\r\n", i4IfIndex);
        return SNMP_FAILURE;
    }

    pTlmComponentLink->aUnResBw[u4UnResBwPrio].unResBw =
        pComponentIface->aUnResBw[u4UnResBwPrio].unResBw;
    pTlmComponentLink->i4RowStatus = pComponentIface->i4RowStatus;
    pTlmComponentLink->i4StorageType = pComponentIface->i4StorageType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TlmGetAllFsTlmTeLinkBwThresholdTable
 Input       :  i4IfIndex
                i4FsTeLinkBwThresholdIndex
                pTlmTeLink
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmGetAllTeLinkBwThresholdTable (INT4 i4IfIndex,
                                 INT4 i4FsTeLinkBwThresholdIndex,
                                 tTlmTeLink * pTlmTeLink)
{
    tTlmTeLink         *pTeIface = NULL;
    UINT1               u1Temp = TLM_ZERO;

    pTeIface = TlmFindTeIf (i4IfIndex);

    if (pTeIface == NULL)
    {
        TLM_TRC1 (TLM_ALL_TRC | TLM_FAILURE_TRC | TLM_MGMT_TRC,
                  "TlmGetAllTlmTeLinkBwThresholdTable: "
                  "No Te Link Information for %d Index\r\n", i4IfIndex);
        return SNMP_FAILURE;
    }

    if (i4FsTeLinkBwThresholdIndex == TLM_ONE)
    {
        for (u1Temp = 0; u1Temp < TLM_MAX_BW_THRESHOLD_SUPPORTED; u1Temp++)
        {
            pTlmTeLink->aTlmBWThresholdUp[u1Temp] =
                pTeIface->aTlmBWThresholdUp[u1Temp];
        }

        pTlmTeLink->i1ThresholdUpRowStatus = pTeIface->i1ThresholdUpRowStatus;
    }
    else
    {
        for (u1Temp = 0; u1Temp < TLM_MAX_BW_THRESHOLD_SUPPORTED; u1Temp++)
        {
            pTlmTeLink->aTlmBWThresholdDown[u1Temp] =
                pTeIface->aTlmBWThresholdDown[u1Temp];
        }

        pTlmTeLink->i1ThresholdDownRowStatus =
            pTeIface->i1ThresholdDownRowStatus;
    }

    return SNMP_SUCCESS;
}
