/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: tlmtest.c,v 1.12 2016/01/28 09:40:35 siva Exp $
*
* Description: This file contains the common test routines for TLM
*********************************************************************/
#include "tlminc.h"
#include "tlmcli.h"
/****************************************************************************
 Function    :  TlmTestAllTeLinkTable
 Input       :  pu4ErrorCode
                i4IfIndex
                pTlmSetStdTlmTeLinkTableEntry
                pTlmIsSetStdTlmTeLinkTableEntry
                u4ObjectId
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmTestAllTeLinkTable (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                       tTlmTeLink * pSetTlmTeLink, UINT4 u4ObjectId)
{
    tTlmTeLink         *pTeIface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTlmTeLink         *pTempTeListNode = NULL;
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4TeLinkNameLen = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();
    pTeIface = TlmFindTeIf (i4IfIndex);

    if (u4ObjectId != TLM_TE_LINK_ROW_STATUS)
    {
        if (pTeIface == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        else if (pTeIface->i4RowStatus == ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (pSetTlmTeLink->i4RowStatus != CREATE_AND_WAIT)
    {
        if (pTeIface == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    else if (pTeIface != NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case TLM_TE_LINK_INFO_TYPE:
        {
            if ((pSetTlmTeLink->u1InfoType != TLM_DATA_CHANNEL) &&
                (pSetTlmTeLink->u1InfoType != TLM_DATA_CONTROL_CHANNEL))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }

        case TLM_TE_LINK_ADDR_TYPE:
        {
            if (!((pSetTlmTeLink->i4AddressType == TLM_ADDRESSTYPE_UNKNOWN) ||
                  (pSetTlmTeLink->i4AddressType ==
                   TLM_ADDRESSTYPE_NUMBERED_IPV4)))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case TLM_TE_LINK_LOCAL_IPADDR:
        {
            if ((TlmUtilValidateIpAddress
                 (pSetTlmTeLink->LocalIpAddr.u4_addr[0]) == FALSE)
                || (pSetTlmTeLink->u1LocalIpLen != TLM_MAX_IP_LEN))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            if (pTeIface->i4AddressType != TLM_ADDRESSTYPE_NUMBERED_IPV4)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }
        case TLM_TE_LINK_REMOTE_IPADDR:
        {
            if ((TlmUtilValidateIpAddress
                 (pSetTlmTeLink->RemoteIpAddr.u4_addr[0]) == FALSE)
                || (pSetTlmTeLink->u1RemoteIpLen != TLM_MAX_IP_LEN))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            if (pTeIface->i4AddressType != TLM_ADDRESSTYPE_NUMBERED_IPV4)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;;
                return SNMP_FAILURE;
            }
            break;
        }
        case TLM_TE_LINK_METRIC_OBJ:
        {
            return SNMP_SUCCESS;
        }
        case TLM_TE_LINK_PROTECT_TYPE:
        {
            if (!
                (IS_VALID_TE_PROTECTION_TYPE (pSetTlmTeLink->i4ProtectionType)))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }
        case TLM_TE_LINK_WORK_PRIORITY:
        {
            if (!(IS_VALID_PRIORITY ((INT4) pSetTlmTeLink->u4WorkingPriority)))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case TLM_TE_LINK_RESOURCE_CLASS:
        {
            if (!(IS_VALID_TE_RESOURCE_CLASS (pSetTlmTeLink->u4RsrcClassColor)))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case TLM_TE_LINK_INCOMING_IFID:
        {
            if (!(IS_VALID_TE_INCOMING_ID (pSetTlmTeLink->u4RemoteIdentifier)))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            if (pTeIface->i4AddressType != TLM_ADDRESSTYPE_UNKNOWN)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case TLM_TE_LINK_OUTGOING_IFID:
        {
            if (!(IS_VALID_TE_OUTGOING_ID (pSetTlmTeLink->u4LocalIdentifier)))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            if (pTeIface->i4AddressType != TLM_ADDRESSTYPE_UNKNOWN)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }
        case TLM_TE_LINK_ROW_STATUS:
        {
            if (pSetTlmTeLink->i4RowStatus == CREATE_AND_WAIT)
            {
                if ((CfaGetIfInfo ((UINT4) i4IfIndex, &CfaIfInfo)
                     == CFA_FAILURE) || (CfaIfInfo.u1IfType != CFA_TELINK))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
                if (TMO_SLL_Count (&(gTlmGlobalInfo.sortTeIfLst)) >
                    MAX_TLM_LINKS)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            else if (pSetTlmTeLink->i4RowStatus == ACTIVE)
            {
                if (pTeIface->i4TeLinkType == TLM_BUNDLE)
                {
                    TMO_SLL_Scan (&(pTeIface->sortUnbundleTeList), pLstNode,
                                  tTMO_SLL_NODE *)
                    {
                        pTempTeListNode =
                            GET_TE_LINK_TE_LIST_NODE_PTR_FROM_SORT_LST
                            (pLstNode);

                        if (pTeIface->u4RemoteRtrId !=
                            pTempTeListNode->u4RemoteRtrId)
                        {
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            return SNMP_FAILURE;
                        }
                        if (pTeIface->u4TeMetric != pTempTeListNode->u4TeMetric)
                        {
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            return SNMP_FAILURE;
                        }
                        if (pTeIface->u4RsrcClassColor !=
                            pTempTeListNode->u4RsrcClassColor)
                        {
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            return SNMP_FAILURE;
                        }

                    }
                }

                if (pTeIface->i4AddressType == TLM_ADDRESSTYPE_UNKNOWN)
                {
                    if ((pTeIface->u4LocalIdentifier == TLM_ZERO) ||
                        (pTeIface->u4RemoteIdentifier == TLM_ZERO))
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return SNMP_FAILURE;
                    }
                }
                else if (pTeIface->i4AddressType ==
                         TLM_ADDRESSTYPE_NUMBERED_IPV4)
                {
                    if ((pTeIface->LocalIpAddr.u4_addr[0] == TLM_ZERO) ||
                        (pTeIface->RemoteIpAddr.u4_addr[0] == TLM_ZERO))
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return SNMP_FAILURE;
                    }
                    if (pTeIface->LocalIpAddr.u4_addr[0] ==
                        pTeIface->RemoteIpAddr.u4_addr[0])
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return SNMP_FAILURE;
                    }
                }
                
                if (TLM_STRLEN (pTeIface->au1TeLinkName) == TLM_ZERO)
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
            }
            else if ((pSetTlmTeLink->i4RowStatus == NOT_READY) ||
                     (pSetTlmTeLink->i4RowStatus == CREATE_AND_GO) ||
                     (pSetTlmTeLink->i4RowStatus < ACTIVE) ||
                     (pSetTlmTeLink->i4RowStatus > DESTROY))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }
        case TLM_TE_LINK_STORAGE_TYPE:
        {
            if ((pSetTlmTeLink->i4StorageType < TLM_STORAGE_OTHER) ||
                (pSetTlmTeLink->i4StorageType > TLM_STORAGE_RDONLY))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case TLM_TE_LINK_NAME_OBJ:
        {
            u4TeLinkNameLen = TLM_STRLEN (pSetTlmTeLink->au1TeLinkName);

            if ((u4TeLinkNameLen < TLM_MIN_TELINK_NAME_LEN) ||
                (u4TeLinkNameLen > TLM_MAX_TELINK_NAME_LEN - 1))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if (TlmUtilCheckTeLinkNameUnique (pSetTlmTeLink->au1TeLinkName,
                                              i4IfIndex) == TLM_FALSE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case TLM_TE_LINK_REMOTE_RTRID:
        {
            if (pSetTlmTeLink->u4RemoteRtrId == TLM_ZERO)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case TLM_TE_LINK_IF_TYPE:
        {
            if ((pSetTlmTeLink->i4TeLinkIfType != TLM_POINT_TO_POINT) &&
                (pSetTlmTeLink->i4TeLinkIfType != TLM_MULTI_ACCESS))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case TLM_TE_LINK_TYPE:
        {
            if ((pSetTlmTeLink->i4TeLinkType != TLM_BUNDLE) &&
                (pSetTlmTeLink->i4TeLinkType != TLM_UNBUNDLE))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case TLM_TE_LINK_ADVERTISE:
        {
            if ((pSetTlmTeLink->i4IsTeLinkAdvertise != TLM_SNMP_TRUE) &&
                (pSetTlmTeLink->i4IsTeLinkAdvertise != TLM_SNMP_FALSE))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }

        default:
        {
            return SNMP_FAILURE;
        }
    }
    TLM_TRC_FN_EXIT ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TlmTestAllTeLinkDescriptorTable
 Input       :  pu4ErrorCode
                pSetTeIfDescr
                u4ObjectId
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmTestAllTeLinkDescriptorTable (UINT4 *pu4ErrorCode,
                                 INT4 i4IfIndex,
                                 UINT4 u4DescrId,
                                 tTlmTeIfDescr * pSetTlmTeLink,
                                 UINT4 u4ObjectId)
{
    tTlmTeLink         *pTeIface = NULL;
    tTlmTeIfDescr      *pTeDescr = NULL;

    TLM_TRC_FN_ENTRY ();
    pTeIface = TlmFindTeIf (i4IfIndex);

    if (pTeIface == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pTeDescr = TlmFindTeDescr (u4DescrId, pTeIface);

    if (u4ObjectId != TLM_TE_LINK_DESC_ROW_STATUS)
    {
        if (pTeDescr == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        else if (pTeDescr->i4RowStatus == ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (pSetTlmTeLink->i4RowStatus != CREATE_AND_WAIT)
    {
        if (pTeDescr == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    else if (pTeDescr != NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case TLM_TE_LINK_DESC_ROW_STATUS:
        {
            if ((pSetTlmTeLink->i4RowStatus == CREATE_AND_GO) ||
                (pSetTlmTeLink->i4RowStatus == NOT_READY) ||
                (pSetTlmTeLink->i4RowStatus < ACTIVE) ||
                (pSetTlmTeLink->i4RowStatus > DESTROY))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }
        case TLM_TE_LINK_DESC_STORAGE_TYPE:
        {
            if ((pSetTlmTeLink->i4StorageType < TLM_STORAGE_OTHER) ||
                (pSetTlmTeLink->i4StorageType > TLM_STORAGE_RDONLY))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        default:
        {
            return SNMP_FAILURE;
        }
    }
    TLM_TRC_FN_EXIT ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TlmTestAllTeLinkSrlgTable
 Input       :  pu4ErrorCode
                pSetTeLinkSrlg
                u4ObjectId
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmTestAllTeLinkSrlgTable (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                           UINT4 u4TeLinkSrlg,
                           tTlmTeLinkSrlg * pSetTeLinkSrlg, UINT4 u4ObjectId)
{
    tTlmTeLink         *pTeIface = NULL;
    tTlmTeLinkSrlg     *pTeLinkSrlg = NULL;

    pTeIface = TlmFindTeIf (i4IfIndex);

    TLM_TRC_FN_ENTRY ();
    if (pTeIface == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pTeLinkSrlg = TlmFindTeSrlgIf (u4TeLinkSrlg, pTeIface);

    if (u4ObjectId != TLM_TE_LINK_SRLG_ROW_STATUS)
    {
        if (pTeLinkSrlg == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        else if (pTeLinkSrlg->i4RowStatus == ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (pSetTeLinkSrlg->i4RowStatus != CREATE_AND_WAIT)
    {
        if (pTeLinkSrlg == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    else if (pTeLinkSrlg != NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case TLM_TE_LINK_SRLG_ROW_STATUS:
        {
            if ((pSetTeLinkSrlg->i4RowStatus < ACTIVE) ||
                (pSetTeLinkSrlg->i4RowStatus > DESTROY))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            if ((pSetTeLinkSrlg->i4RowStatus == NOT_READY) ||
                (pSetTeLinkSrlg->i4RowStatus == NOT_IN_SERVICE))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }
        case TLM_TE_LINK_SRLG_STORAGE_TYPE:
        {
            if ((pSetTeLinkSrlg->i4StorageType < TLM_STORAGE_OTHER) ||
                (pSetTeLinkSrlg->i4StorageType > TLM_STORAGE_RDONLY))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }

        default:
        {
            return SNMP_FAILURE;
        }
    }
    TLM_TRC_FN_EXIT ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TlmTestAllTeLinkBandwidthTable
 Input       :  pu4ErrorCode
                i4IfIndex
                pTlmTeDescrUnResBw
                u4ObjectId
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmTestAllTeLinkBandwidthTable (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                UINT4 u4TeLinkBandwidthPriority,
                                tTlmTeLink * pSetTlmTeLink, UINT4 u4ObjectId)
{
    tTlmTeLink         *pTeIface = NULL;
    UINT4               u4BwPriority = u4TeLinkBandwidthPriority;
    INT4                i4StorageType =
        pSetTlmTeLink->aUnResBw[u4BwPriority].i4StorageType;
    INT4                i4RowStatus = TLM_ZERO;
    INT4                i4SetRowStatus = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();
    pTeIface = TlmFindTeIf (i4IfIndex);

    if (pTeIface == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    i4RowStatus = pSetTlmTeLink->aUnResBw[u4BwPriority].i4RowStatus;

    i4SetRowStatus = pTeIface->aUnResBw[u4BwPriority].i4RowStatus;

    if (u4ObjectId != TLM_TE_LINK_BW_ROW_STATUS)
    {
        if (i4SetRowStatus == TLM_ZERO)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        else if (i4SetRowStatus == ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (i4RowStatus != CREATE_AND_WAIT)
    {
        if (i4SetRowStatus == TLM_ZERO)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    else if (i4SetRowStatus != TLM_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case TLM_TE_LINK_BW_ROW_STATUS:
        {
            i4RowStatus = pSetTlmTeLink->aUnResBw[u4BwPriority].i4RowStatus;

            if ((i4RowStatus < ACTIVE) || (i4RowStatus > DESTROY))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }
        case TLM_TE_LINK_BW_STORAGE_TYPE:
        {
            if ((i4StorageType < TLM_STORAGE_OTHER) ||
                (i4StorageType > TLM_STORAGE_RDONLY))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }

        default:
        {
            return SNMP_FAILURE;
        }
    }
    TLM_TRC_FN_EXIT ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TlmTestAllComponentLinkTable
 Input       :  pu4ErrorCode
                i4IfIndex
                pTlmComponentLink
                u4ObjectId
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmTestAllComponentLinkTable (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                              tTlmComponentLink * pSetTlmCompLink,
                              UINT4 u4ObjectId)
{
    tTlmComponentLink  *pCompLink = NULL;
    tTlmComponentIfDescr *pCompDesc = NULL;
    tCfaIfInfo          CfaIfInfo;
    tTMO_SLL_NODE      *pCompNode = NULL;
    tSNMP_OCTET_STRING_TYPE MaxResvBw;
    UINT1               au1MaxResBw[TLM_MAX_BW_LEN];
    BOOL1               bIsActiveCompDescrFound = TLM_FALSE;

    TLM_TRC_FN_ENTRY ();

    MEMSET ((&MaxResvBw), TLM_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

    MaxResvBw.pu1_OctetList = &au1MaxResBw[TLM_ZERO];
    pCompLink = TlmFindComponentIf (i4IfIndex);

    if (u4ObjectId != TLM_COMP_LINK_ROW_STATUS)
    {
        if (pCompLink == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        else if (pCompLink->i4RowStatus == ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (pSetTlmCompLink->i4RowStatus != CREATE_AND_WAIT)
    {
        if (pCompLink == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    else if (pCompLink != NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case TLM_COMP_LINK_MAX_RESV_BW:
        {
            if (pSetTlmCompLink->maxResBw != TLM_ZERO)
            {
	        if (CfaGetIfInfo ((UINT4) i4IfIndex, &CfaIfInfo) != CFA_SUCCESS)
                {
                   *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
                               TLM_CONVERT_BPS_TO_BYTES_PER_SEC(CfaIfInfo.u4IfSpeed);
                               if (pSetTlmCompLink->maxResBw > CfaIfInfo.u4IfSpeed)
                               {
                                       *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;

                               }

                if (!(IS_VALID_TE_BW (pSetTlmCompLink->maxResBw)))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
            }
            FLOAT_TO_OCTETSTRING (pSetTlmCompLink->maxResBw, (&MaxResvBw));

            break;
        }
        case TLM_COMP_LINK_PREF_PROTECT:
        {
            if (pSetTlmCompLink->i4LinkPrefProtection !=
                TLM_TE_LINK_PROTECTION_PRIMARY)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case TLM_COMP_LINK_ROW_STATUS:
        {
            if (pSetTlmCompLink->i4RowStatus == CREATE_AND_WAIT)
            {
                if (TMO_SLL_Count
                    (&(gTlmGlobalInfo.sortComponentIfLst)) >=
                    MAX_TLM_COMPONENT_LINKS)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
                if (CfaGetIfInfo ((UINT4) i4IfIndex, &CfaIfInfo) != CFA_SUCCESS)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

                if ((CfaIfInfo.u1IfType == CFA_ENET) &&
                    (CfaIfInfo.u1BridgedIface == CFA_ENABLED))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            else if (pSetTlmCompLink->i4RowStatus == ACTIVE)
            {
                TMO_SLL_Scan (&(pCompLink->ifDescrList), pCompNode,
                              tTMO_SLL_NODE *)
                {
                    pCompDesc = GET_COMPONENT_DESCR_PTR_FROM_SORT_LST
                        (pCompNode);
                    if (pCompDesc->i4RowStatus == ACTIVE)
                    {
                        bIsActiveCompDescrFound = TRUE;
                        break;
                    }
                }

                if (bIsActiveCompDescrFound == FALSE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            else if ((pSetTlmCompLink->i4RowStatus == CREATE_AND_GO) ||
                     (pSetTlmCompLink->i4RowStatus == NOT_READY) ||
                     (pSetTlmCompLink->i4RowStatus < ACTIVE) ||
                     (pSetTlmCompLink->i4RowStatus > DESTROY))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }
        case TLM_COMP_LINK_STORAGE_TYPE:
        {
            if ((pSetTlmCompLink->i4StorageType < TLM_STORAGE_OTHER) ||
                (pSetTlmCompLink->i4StorageType > TLM_STORAGE_RDONLY))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }

        default:
        {
            return SNMP_FAILURE;
        }
    }
    TLM_TRC_FN_EXIT ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TlmTestAllComponentLinkDescriptorTable
 Input       :  pu4ErrorCode
                i4IfIndex
                u4ComponentLinkDescrId
                pTlmComponentIfDescr
                u4ObjectId
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmTestAllComponentLinkDescriptorTable (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                        UINT4 u4ComponentLinkDescrId,
                                        tTlmComponentIfDescr *
                                        pSetTlmCompIfDescr, UINT4 u4ObjectId)
{
    tTlmComponentLink  *pCompLink = NULL;
    tTlmComponentIfDescr *pCompDescr = NULL;
    tSNMP_OCTET_STRING_TYPE MinLspBw;
    UINT4               u4DescrId = u4ComponentLinkDescrId;
    UINT1               au1MinLspBw[TLM_MAX_BW_LEN];

    TLM_TRC_FN_ENTRY ();

    MEMSET ((&MinLspBw), TLM_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

    MinLspBw.pu1_OctetList = &au1MinLspBw[TLM_ZERO];

    pCompLink = TlmFindComponentIf (i4IfIndex);

    if (pCompLink == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pCompDescr = TlmFindComponentDescr (u4DescrId, pCompLink);

    if (u4ObjectId != TLM_COMP_LINK_DESC_ROW_STATUS)
    {
        if (pCompDescr == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        else if (pCompDescr->i4RowStatus == ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (pSetTlmCompIfDescr->i4RowStatus != CREATE_AND_WAIT)
    {
        if (pCompDescr == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    else if (pCompDescr != NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case TLM_COMP_LINK_DESC_SWITCH_CAP:
        {
            if (!((pSetTlmCompIfDescr->i4SwitchingCap == OSPF_TE_PSC_1) ||
                  (pSetTlmCompIfDescr->i4SwitchingCap == OSPF_TE_PSC_2)))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case TLM_COMP_LINK_DESC_ENCODE_TYPE:
        {
            if (!(IS_VALID_TE_ENCODING_TYPE
                  (pSetTlmCompIfDescr->i4EncodingType)))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case TLM_COMP_LINK_DESC_MIN_LSP_BW:
        {
            if (pSetTlmCompIfDescr->minLSPBw != TLM_ZERO)
            {
                if (!(IS_VALID_TE_BW (pSetTlmCompIfDescr->minLSPBw)))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
            }
            FLOAT_TO_OCTETSTRING (pSetTlmCompIfDescr->minLSPBw, (&MinLspBw));

            break;
        }

        case TLM_COMP_LINK_DESC_INTF_MTU:
        case TLM_COMP_LINK_DESC_INDICATION:
            break;
        case TLM_COMP_LINK_DESC_ROW_STATUS:
        {
            if ((pSetTlmCompIfDescr->i4RowStatus == CREATE_AND_GO) ||
                (pSetTlmCompIfDescr->i4RowStatus == NOT_READY) ||
                (pSetTlmCompIfDescr->i4RowStatus < ACTIVE) ||
                (pSetTlmCompIfDescr->i4RowStatus > DESTROY))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }
        case TLM_COMP_LINK_DESC_STORAGE_TYPE:
        {
            if ((pSetTlmCompIfDescr->i4StorageType < TLM_STORAGE_OTHER) ||
                (pSetTlmCompIfDescr->i4StorageType > TLM_STORAGE_RDONLY))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }

        default:
        {
            return SNMP_FAILURE;
        }
    }

    TLM_TRC_FN_EXIT ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TlmTestAllComponentLinkBandwidthTable
 Input       :  pu4ErrorCode
                i4IfIndex
                u4ComponentLinkBandwidthPriority
                pTlmComponentDescrUnResBw
                u4ObjectId
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmTestAllComponentLinkBandwidthTable (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                       UINT4 u4ComponentLinkBandwidthPriority,
                                       tTlmComponentLink * pSetComponentLink,
                                       UINT4 u4ObjectId)
{
    tTlmComponentLink  *pCompLink = NULL;
    UINT4               u4BwPriority = u4ComponentLinkBandwidthPriority;
    INT4                i4StorageType =
        pSetComponentLink->aUnResBw[u4BwPriority].i4StorageType;
    INT4                i4RowStatus = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();
    pCompLink = TlmFindComponentIf (i4IfIndex);

    if (pCompLink == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case TLM_COMP_LINK_BW_ROW_STATUS:
        {
            i4RowStatus = pSetComponentLink->aUnResBw[u4BwPriority].i4RowStatus;

            if ((i4RowStatus == CREATE_AND_WAIT) ||
                (i4RowStatus == CREATE_AND_GO))
            {
                if (pCompLink->aUnResBw[u4BwPriority].i4RowStatus != TLM_ZERO)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            else if ((i4RowStatus == DESTROY) || (i4RowStatus == ACTIVE) ||
                     (i4RowStatus == NOT_IN_SERVICE))
            {
                if (pCompLink->aUnResBw[u4BwPriority].i4RowStatus == TLM_ZERO)
                {
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    return SNMP_FAILURE;
                }
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }
        case TLM_COMP_LINK_BW_STORAGE_TYPE:
        {
            if ((i4StorageType < TLM_STORAGE_OTHER) ||
                (i4StorageType > TLM_STORAGE_RDONLY))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }

        default:
        {
            return SNMP_FAILURE;
        }
    }
    TLM_TRC_FN_EXIT ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TlmTestAllFsTlmTeLinkBwThresholdTable
 Input       :  pu4ErrorCode
                pTlmTeLink
                u4ObjectId
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmTestAllTeLinkBwThresholdTable (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                  INT4 i4FsTeLinkBwThresholdIndex,
                                  tTlmTeLink * pSetTlmTeLink, UINT4 u4ObjectId)
{
    tTlmTeLink         *pTeLink = NULL;
    UINT1               u1BwThreshold = TLM_ZERO;
    INT1                i1RowStatus = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();
    pTeLink = TlmFindTeIf (i4IfIndex);

    if (pTeLink == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4FsTeLinkBwThresholdIndex != TLM_ONE) &&
        (i4FsTeLinkBwThresholdIndex != TLM_TWO))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case TLM_BW_THRESHOLD_ZERO:
        case TLM_BW_THRESHOLD_ONE:
        case TLM_BW_THRESHOLD_TWO:
        case TLM_BW_THRESHOLD_THREE:
        case TLM_BW_THRESHOLD_FOUR:
        case TLM_BW_THRESHOLD_FIVE:
        case TLM_BW_THRESHOLD_SIX:
        case TLM_BW_THRESHOLD_SEVEN:
        case TLM_BW_THRESHOLD_EIGHT:
        case TLM_BW_THRESHOLD_NINE:
        case TLM_BW_THRESHOLD_TEN:
        case TLM_BW_THRESHOLD_ELEVEN:
        case TLM_BW_THRESHOLD_TWELVE:
        case TLM_BW_THRESHOLD_THIRTEEN:
        case TLM_BW_THRESHOLD_FOURTEEN:
        case TLM_BW_THRESHOLD_FIFTEEN:
        {
            if (i4FsTeLinkBwThresholdIndex == TLM_ONE)
            {
                i1RowStatus = pTeLink->i1ThresholdUpRowStatus;
                u1BwThreshold = pSetTlmTeLink->aTlmBWThresholdUp[u4ObjectId];
            }
            else
            {
                i1RowStatus = pTeLink->i1ThresholdDownRowStatus;
                u1BwThreshold = pSetTlmTeLink->aTlmBWThresholdDown[u4ObjectId];
            }
            if (i1RowStatus == TLM_ZERO)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            else if (i1RowStatus == ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if (u1BwThreshold > 100)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case TLM_BW_THRESHOLD_UP_ROW_STATUS:
        {
            if (pSetTlmTeLink->i1ThresholdUpRowStatus == NOT_READY)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case TLM_BW_THRESHOLD_DOWN_ROW_STATUS:
        {
            if (pSetTlmTeLink->i1ThresholdDownRowStatus == NOT_READY)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }

        default:
        {
            return SNMP_FAILURE;
        }

    }
    TLM_TRC_FN_EXIT ();
    return SNMP_SUCCESS;
}
