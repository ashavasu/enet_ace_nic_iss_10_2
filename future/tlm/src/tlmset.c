/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: tlmset.c,v 1.14 2014/07/09 13:23:17 siva Exp $
*
* Description: This file contains the common set routines for TLM 
*********************************************************************/
#include "tlminc.h"
#include "tlmcli.h"

/****************************************************************************
 Function    :  TlmSetAllTeLinkTable
 Input       :  i4IfIndex
                pTlmTeLink
                u4ObjectId
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmSetAllTeLinkTable (INT4 i4IfIndex, tTlmTeLink * pSetTlmTeLink,
                      UINT4 u4ObjectId)
{
    tTlmTeLink         *pTeIface = NULL;
    tTlmTeLink         *pBundleTeIface = NULL;
    UINT4               u4HighIfIndex = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();

    pTeIface = TlmFindTeIf (i4IfIndex);

    if ((u4ObjectId != TLM_TE_LINK_ROW_STATUS) ||
        (pSetTlmTeLink->i4RowStatus != CREATE_AND_WAIT))
    {
        if (pTeIface == NULL)
        {
            TLM_TRC1 (TLM_ALL_TRC | TLM_FAILURE_TRC | TLM_MGMT_TRC,
                      "TlmSetAllTeLinkTable: No Te-Link Information"
                      "for %d Index\r\n", i4IfIndex);

            return SNMP_FAILURE;
        }
    }
    else if (pTeIface != NULL)
    {
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case TLM_TE_LINK_IF_TYPE:
            pTeIface->i4TeLinkIfType = pSetTlmTeLink->i4TeLinkIfType;
            break;
        case TLM_TE_LINK_INFO_TYPE:
            pTeIface->u1InfoType = pSetTlmTeLink->u1InfoType;
            break;
        case TLM_TE_LINK_ADDR_TYPE:
            pTeIface->i4AddressType = pSetTlmTeLink->i4AddressType;
            break;

        case TLM_TE_LINK_METRIC_OBJ:
            pTeIface->u4TeMetric = pSetTlmTeLink->u4TeMetric;
            break;

        case TLM_TE_LINK_MAX_RESV_BW:
            pTeIface->maxResBw = pSetTlmTeLink->maxResBw;
            break;

        case TLM_TE_LINK_PROTECT_TYPE:
            pTeIface->i4ProtectionType = pSetTlmTeLink->i4ProtectionType;
            break;

        case TLM_TE_LINK_WORK_PRIORITY:
            pTeIface->u4WorkingPriority = pSetTlmTeLink->u4WorkingPriority;
            break;

        case TLM_TE_LINK_RESOURCE_CLASS:
            pTeIface->u4RsrcClassColor = pSetTlmTeLink->u4RsrcClassColor;
            break;

        case TLM_TE_LINK_LOCAL_IPADDR:
            pTeIface->LocalIpAddr.u4_addr[0] =
                pSetTlmTeLink->LocalIpAddr.u4_addr[0];
            pTeIface->u1LocalIpLen = sizeof
                (pSetTlmTeLink->LocalIpAddr.u4_addr[0]);
            break;

        case TLM_TE_LINK_REMOTE_IPADDR:
            pTeIface->RemoteIpAddr.u4_addr[0] =
                pSetTlmTeLink->RemoteIpAddr.u4_addr[0];
            pTeIface->u1RemoteIpLen = sizeof
                (pSetTlmTeLink->RemoteIpAddr.u4_addr[0]);
            break;

        case TLM_TE_LINK_INCOMING_IFID:
            pTeIface->u4RemoteIdentifier = pSetTlmTeLink->u4RemoteIdentifier;
            break;
        case TLM_TE_LINK_OUTGOING_IFID:
            pTeIface->u4LocalIdentifier = pSetTlmTeLink->u4LocalIdentifier;
            break;

        case TLM_TE_LINK_ROW_STATUS:
            if (pSetTlmTeLink->i4RowStatus != CREATE_AND_WAIT)
            {
                if (pTeIface->i4RowStatus == pSetTlmTeLink->i4RowStatus)
                {
                    return SNMP_SUCCESS;
                }
            }
            if (pSetTlmTeLink->i4RowStatus == CREATE_AND_WAIT)
            {
                pTeIface = TlmCreateTeIf (i4IfIndex);

                if (pTeIface == NULL)
                {
                    return SNMP_FAILURE;
                }
                pTeIface->i4RowStatus = NOT_READY;
            }
            else if (pSetTlmTeLink->i4RowStatus == ACTIVE)
            {
                pTeIface->i4RowStatus = ACTIVE;

                if (pTeIface->i4TeLinkType == TLM_UNBUNDLE)
                {
                    /*Getting the bundle TE link of member TE link */
                    CfaApiGetHLTeLinkFromLLTeLink ((UINT4) pTeIface->i4IfIndex,
                                                   &u4HighIfIndex, TRUE);

                    pBundleTeIface = TlmFindTeIf ((INT4) u4HighIfIndex);

                    /*Update Unbundle TE link list of a Bundle TE link */
                    TlmUpdateUnBundleTeLinks (pTeIface, pBundleTeIface,
                                              TLM_UNBUNDLE_UPDATE);
                    /*Update the Bandwidth to Bundle TE link from member TE links */
                    TlmUpdateBundleTeLinkInfo (pTeIface, pBundleTeIface,
                                               TLM_TELINK_UPDATE);
                }
                else
                {
                    TlmUpdateUnBundleTeLinks (pTeIface, NULL,
                                              TLM_BUNDLE_UPDATE);
                }

                if (TlmAddTeLinkToIDBasedTree (pTeIface) == TLM_FAILURE)
                {
                    TLM_TRC (TLM_FAILURE_TRC, "Failed to Add node in the "
                             "Global RBTree\n");
                    return SNMP_FAILURE;
                }

                TlmUpdateTeLinkOperStatus (pTeIface, TLM_OPER_UP);
            }
            else if (pSetTlmTeLink->i4RowStatus == DESTROY)
            {
                if (pTeIface->i4TeLinkType == TLM_UNBUNDLE)
                {
                    /*Update the Bandwidth to Bundle TE link from member TE links */
                    TlmUpdateBundleTeLinkInfo (pTeIface,
                                               pTeIface->pBundledTeLink,
                                               TLM_TELINK_DELETE);

                    TlmUpdateUnBundleTeLinks (pTeIface,
                                              pTeIface->pBundledTeLink,
                                              TLM_UNBUNDLE_DELETE);
                }
                else
                {
                    TlmUpdateUnBundleTeLinks (pTeIface, NULL,
                                              TLM_BUNDLE_DELETE);
                }
                TlmDeleteTeIf (pTeIface);
            }
            else if (pSetTlmTeLink->i4RowStatus == NOT_IN_SERVICE)
            {
                pTeIface->i4RowStatus = NOT_IN_SERVICE;

                TlmUpdateTeLinkOperStatus (pTeIface, TLM_OPER_DOWN);
            }
            break;
        case TLM_TE_LINK_STORAGE_TYPE:
            pTeIface->i4StorageType = pSetTlmTeLink->i4StorageType;
            break;

        case TLM_TE_LINK_NAME_OBJ:

            if ((pTeIface->u1TeLinkNameLen != pSetTlmTeLink->u1TeLinkNameLen) ||
                (MEMCMP (pTeIface->au1TeLinkName, pSetTlmTeLink->au1TeLinkName,
                         pTeIface->u1TeLinkNameLen) != TLM_ZERO))
            {
                TMO_HASH_Delete_Node (gTlmGlobalInfo.pTeLinkNameIfHashTable,
                                      &pTeIface->NextTeLinkNameNode,
                                      IF_HASH_NAME_FN (pTeIface->au1TeLinkName,
                                                       pTeIface->
                                                       u1TeLinkNameLen));
            }
            else
            {
                break;
            }

            pTeIface->u1TeLinkNameLen = pSetTlmTeLink->u1TeLinkNameLen;
            MEMCPY (pTeIface->au1TeLinkName, pSetTlmTeLink->au1TeLinkName,
                    pTeIface->u1TeLinkNameLen);

            TMO_HASH_Init_Node (&pTeIface->NextTeLinkNameNode);

            /* Add this Interface to HASH TABLE */
            TMO_HASH_Add_Node (gTlmGlobalInfo.pTeLinkNameIfHashTable,
                               &pTeIface->NextTeLinkNameNode,
                               IF_HASH_NAME_FN (pTeIface->au1TeLinkName,
                                                pTeIface->u1TeLinkNameLen),
                               NULL);
            break;

        case TLM_TE_LINK_REMOTE_RTRID:
            pTeIface->u4RemoteRtrId = pSetTlmTeLink->u4RemoteRtrId;
            break;

        case TLM_TE_LINK_TYPE:
            pTeIface->i4TeLinkType = pSetTlmTeLink->i4TeLinkType;
            break;
        case TLM_TE_LINK_ADVERTISE:
            pTeIface->i4IsTeLinkAdvertise = pSetTlmTeLink->i4IsTeLinkAdvertise;
            break;
        default:
            break;
    }

    TLM_TRC_FN_EXIT ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TlmSetAllTeLinkDescriptorTable
 Input       :  i4IfIndex
                u4DescrId
                pSetTlmTeIfDescr
                u4ObjectId
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmSetAllTeLinkDescriptorTable (INT4 i4IfIndex, UINT4 u4DescrId,
                                tTlmTeIfDescr * pSetTlmTeIfDescr,
                                UINT4 u4ObjectId)
{
    tTlmTeLink         *pTeIface = NULL;
    tTlmTeIfDescr      *pTeDescr = NULL;

    TLM_TRC_FN_ENTRY ();
    if (u4DescrId != TLM_TELINK_DESCRID)
    {
        return SNMP_FAILURE;
    }
    pTeIface = TlmFindTeIf (i4IfIndex);

    if (pTeIface == NULL)
    {
        return SNMP_FAILURE;

    }

    if ((u4ObjectId != TLM_TE_LINK_DESC_ROW_STATUS) ||
        (pSetTlmTeIfDescr->i4RowStatus != CREATE_AND_WAIT))
    {
        pTeDescr = TlmFindTeDescr (u4DescrId, pTeIface);
        if (pTeDescr == NULL)
        {
            return SNMP_FAILURE;
        }
    }

    switch (u4ObjectId)
    {
        case TLM_TE_LINK_DESC_ROW_STATUS:

            if (pSetTlmTeIfDescr->i4RowStatus != CREATE_AND_WAIT)
            {
                if (pTeDescr->i4RowStatus == pSetTlmTeIfDescr->i4RowStatus)
                {
                    return SNMP_SUCCESS;
                }
            }
            if (pSetTlmTeIfDescr->i4RowStatus == CREATE_AND_WAIT)
            {
                pTeDescr = TlmCreateTeDescr (u4DescrId, pTeIface);

                if (pTeDescr != NULL)
                {
                    pTeDescr->i4RowStatus = NOT_READY;
                }
                else
                {
                    return SNMP_FAILURE;
                }
            }
            else if (pSetTlmTeIfDescr->i4RowStatus == ACTIVE)
            {
                pTeDescr->i4RowStatus = ACTIVE;
            }
            else if (pSetTlmTeIfDescr->i4RowStatus == DESTROY)
            {
                TlmDeleteTeDescr (pTeDescr, pTeIface);
            }
            else if (pSetTlmTeIfDescr->i4RowStatus == NOT_IN_SERVICE)
            {
                pTeDescr->i4RowStatus = NOT_IN_SERVICE;
            }

            TlmUtilSendNotifMsgInfo (pTeIface, TE_LINK_UPDATED);

            break;
        case TLM_TE_LINK_DESC_STORAGE_TYPE:
            pTeDescr->i4StorageType = pSetTlmTeIfDescr->i4StorageType;
            break;
        default:
            break;
    }
    TLM_TRC_FN_EXIT ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TlmSetAllTeLinkSrlgTable
 Input       :  i4IfIndex
                u4TeLinkSrlg
                pTlmTeLinkSrlg
                u4ObjectId
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmSetAllTeLinkSrlgTable (INT4 i4IfIndex, UINT4 u4TeLinkSrlg,
                          tTlmTeLinkSrlg * pSetTeLinkSrlg, UINT4 u4ObjectId)
{
    tTlmTeLink         *pTeIface = NULL;
    tTlmTeLinkSrlg     *pTeLinkSrlg = NULL;

    TLM_TRC_FN_ENTRY ();
    pTeIface = TlmFindTeIf (i4IfIndex);

    if (pTeIface == NULL)
    {
        TLM_TRC1 (TLM_ALL_TRC | TLM_FAILURE_TRC | TLM_MGMT_TRC,
                  "TlmSetAllTeLinkSrlgTable: No Te-Link Information"
                  "for %d Index\r\n", i4IfIndex);

        return SNMP_FAILURE;
    }

    pTeLinkSrlg = TlmFindTeSrlgIf (u4TeLinkSrlg, pTeIface);

    if ((pSetTeLinkSrlg->i4RowStatus == CREATE_AND_GO) ||
        (pSetTeLinkSrlg->i4RowStatus == NOT_READY))
    {
        return SNMP_FAILURE;
    }

    if ((u4ObjectId != TLM_TE_LINK_SRLG_ROW_STATUS) ||
        (pSetTeLinkSrlg->i4RowStatus != CREATE_AND_WAIT))
    {
        if (pTeLinkSrlg == NULL)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (pTeLinkSrlg != NULL)
        {
            return SNMP_FAILURE;
        }
    }

    switch (u4ObjectId)
    {
        case TLM_TE_LINK_SRLG_ROW_STATUS:

            if (pSetTeLinkSrlg->i4RowStatus != CREATE_AND_WAIT)
            {
                if (pTeLinkSrlg->i4RowStatus == pSetTeLinkSrlg->i4RowStatus)
                {
                    return SNMP_SUCCESS;
                }
            }
            if (pSetTeLinkSrlg->i4RowStatus == CREATE_AND_WAIT)
            {
                pTeLinkSrlg = TlmCreateTeSrlg (u4TeLinkSrlg, pTeIface);

                if (pTeLinkSrlg == NULL)
                {
                    return SNMP_FAILURE;
                }
                pTeLinkSrlg->i4RowStatus = NOT_READY;
            }
            else if (pSetTeLinkSrlg->i4RowStatus == ACTIVE)
            {
                pTeLinkSrlg->i4RowStatus = ACTIVE;
            }
            else if (pSetTeLinkSrlg->i4RowStatus == DESTROY)
            {
                TlmDeleteTeLinkSrlg (pTeLinkSrlg, pTeIface);
            }

            TlmUtilSendNotifMsgInfo (pTeIface, TE_LINK_UPDATED);

            break;
        case TLM_TE_LINK_SRLG_STORAGE_TYPE:
            pTeLinkSrlg->i4StorageType = pSetTeLinkSrlg->i4StorageType;
            break;
        default:
            break;
    }
    TLM_TRC_FN_EXIT ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TlmSetAllTeLinkBandwidthTable
 Input       :  i4IfIndex
                u4TeLinkBandwidthPriority
                pTlmTeDescrUnResBw
                u4ObjectId
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmSetAllTeLinkBandwidthTable (INT4 i4IfIndex, UINT4 u4TeLinkBandwidthPriority,
                               tTlmTeLink * pSetTlmTeLink, UINT4 u4ObjectId)
{
    tTlmTeLink         *pTeIface = NULL;
    UINT4               u4UnResBwPrio = u4TeLinkBandwidthPriority;
    INT4                i4RowStatus = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();
    i4RowStatus = pSetTlmTeLink->aUnResBw[u4UnResBwPrio].i4RowStatus;

    pTeIface = TlmFindTeIf (i4IfIndex);

    if (pTeIface == NULL)
    {
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case TLM_TE_LINK_BW_ROW_STATUS:

            if (i4RowStatus == CREATE_AND_WAIT)
            {
                pTeIface->aUnResBw[u4UnResBwPrio].unResBw = pTeIface->maxResBw;
                pTeIface->aUnResBw[u4UnResBwPrio].resBw =
                    TLM_DEF_BANDWIDTH_VALUE;
                pTeIface->aUnResBw[u4UnResBwPrio].i4RowStatus = NOT_READY;
            }
            else if (i4RowStatus == DESTROY)
            {
                pTeIface->aUnResBw[u4UnResBwPrio].unResBw =
                    TLM_DEF_BANDWIDTH_VALUE;
                pTeIface->aUnResBw[u4UnResBwPrio].resBw =
                    TLM_DEF_BANDWIDTH_VALUE;
                pTeIface->aUnResBw[u4UnResBwPrio].i4RowStatus = TLM_ZERO;
            }
            else if (i4RowStatus == ACTIVE)
            {
                if ((pTeIface->aUnResBw[u4UnResBwPrio].i4RowStatus ==
                     NOT_IN_SERVICE)
                    || (pTeIface->aUnResBw[u4UnResBwPrio].i4RowStatus ==
                        NOT_READY))
                {
                    pTeIface->aUnResBw[u4UnResBwPrio].i4RowStatus = ACTIVE;
                }

            }

            TlmUtilSendNotifMsgInfo (pTeIface, TE_LINK_UPDATED);

            break;
        case TLM_TE_LINK_BW_STORAGE_TYPE:
            pTeIface->aUnResBw[u4UnResBwPrio].i4StorageType =
                pSetTlmTeLink->aUnResBw[u4UnResBwPrio].i4StorageType;
            break;
        default:
            break;
    }
    TLM_TRC_FN_EXIT ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TlmSetAllComponentLinkTable
 Input       :  i4IfIndex
                pSetTlmCompLink
                u4ObjectId
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmSetAllComponentLinkTable (INT4 i4IfIndex,
                             tTlmComponentLink * pSetTlmCompLink,
                             UINT4 u4ObjectId)
{
    tTlmComponentLink  *pCompIface = NULL;
    UINT1               u1Count = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();
    pCompIface = TlmFindComponentIf (i4IfIndex);

    if ((u4ObjectId != TLM_COMP_LINK_ROW_STATUS) ||
        pSetTlmCompLink->i4RowStatus != CREATE_AND_WAIT)
    {
        if (pCompIface == NULL)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (pCompIface != NULL)
        {
            return SNMP_FAILURE;
        }
    }

    switch (u4ObjectId)
    {
        case TLM_COMP_LINK_MAX_RESV_BW:
            pCompIface->availBw = pSetTlmCompLink->maxResBw;
            pCompIface->maxResBw = pSetTlmCompLink->maxResBw;
            for (u1Count = TLM_ZERO; u1Count < TLM_MAX_PRIORITY_LVL; u1Count++)
            {
                pCompIface->aUnResBw[u1Count].unResBw =
                    pSetTlmCompLink->maxResBw;
            }

            break;
        case TLM_COMP_LINK_PREF_PROTECT:
            pCompIface->i4LinkPrefProtection =
                pSetTlmCompLink->i4LinkPrefProtection;
            break;
        case TLM_COMP_LINK_CURR_PROTECT:
            pCompIface->i4LinkCurrentProtection =
                pSetTlmCompLink->i4LinkCurrentProtection;
            break;
        case TLM_COMP_LINK_ROW_STATUS:
            if (pSetTlmCompLink->i4RowStatus != CREATE_AND_WAIT)
            {
                if (pCompIface->i4RowStatus == pSetTlmCompLink->i4RowStatus)
                {
                    return SNMP_SUCCESS;
                }
            }
            if (pSetTlmCompLink->i4RowStatus == CREATE_AND_WAIT)
            {
                pCompIface = TlmComponentIfCreate (i4IfIndex);

                if (pCompIface != NULL)
                {
                    pCompIface->i4RowStatus = NOT_READY;
                }
                else
                {
                    return SNMP_FAILURE;
                }

            }
            else if (pSetTlmCompLink->i4RowStatus == ACTIVE)
            {
                pCompIface->i4RowStatus = ACTIVE;
                TlmUpdateCompLinkOperstatus (pCompIface, CFA_IF_UP);
            }
            else if (pSetTlmCompLink->i4RowStatus == DESTROY)
            {
                TlmDeleteComponentIf (pCompIface);
            }
            else if (pSetTlmCompLink->i4RowStatus == NOT_IN_SERVICE)
            {
                pCompIface->i4RowStatus = NOT_IN_SERVICE;
                TlmUpdateCompLinkOperstatus (pCompIface, CFA_IF_DOWN);
            }

            break;
        case TLM_COMP_LINK_STORAGE_TYPE:
            pCompIface->i4StorageType = pSetTlmCompLink->i4StorageType;
            break;
        default:
            break;
    }
    TLM_TRC_FN_EXIT ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TlmSetAllComponentLinkDescriptorTable
 Input       :  i4IfIndex
                u4ComponentLinkDescrId
                pTlmComponentIfDescr
                u4ObjectId
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmSetAllComponentLinkDescriptorTable (INT4 i4IfIndex,
                                       UINT4 u4ComponentLinkDescrId,
                                       tTlmComponentIfDescr *
                                       pSetTlmCompIfDescr, UINT4 u4ObjectId)
{
    tTlmTeLink         *pTeIface = NULL;
    tTlmComponentLink  *pCompIface = NULL;
    tTlmComponentIfDescr *pCompDescr = NULL;
    UINT4               u4DescrId = u4ComponentLinkDescrId;
    UINT4               u4TeIfIndex = TLM_ZERO;
    UINT1               u1Count = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();
    pCompIface = TlmFindComponentIf (i4IfIndex);

    if (pCompIface == NULL)
    {
        return SNMP_FAILURE;
    }
    pCompDescr = TlmFindComponentDescr (u4DescrId, pCompIface);

    if ((u4ObjectId != TLM_COMP_LINK_DESC_ROW_STATUS) ||
        (pSetTlmCompIfDescr->i4RowStatus != CREATE_AND_WAIT))
    {
        if (pCompDescr == NULL)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (pCompDescr != NULL)
        {
            return SNMP_FAILURE;
        }
    }

    switch (u4ObjectId)
    {
        case TLM_COMP_LINK_DESC_SWITCH_CAP:
            pCompDescr->i4SwitchingCap = pSetTlmCompIfDescr->i4SwitchingCap;
            break;
        case TLM_COMP_LINK_DESC_ENCODE_TYPE:
            pCompDescr->i4EncodingType = pSetTlmCompIfDescr->i4EncodingType;
            break;
        case TLM_COMP_LINK_DESC_MIN_LSP_BW:
            pCompDescr->minLSPBw = pSetTlmCompIfDescr->minLSPBw;
            break;
        case TLM_COMP_LINK_DESC_INTF_MTU:
            pCompDescr->u2MTU = pSetTlmCompIfDescr->u2MTU;
            break;
        case TLM_COMP_LINK_DESC_INDICATION:
            pCompDescr->i4Indication = pSetTlmCompIfDescr->i4Indication;
            break;
        case TLM_COMP_LINK_DESC_ROW_STATUS:

            if (pSetTlmCompIfDescr->i4RowStatus != CREATE_AND_WAIT)
            {
                if (pCompDescr->i4RowStatus == pSetTlmCompIfDescr->i4RowStatus)
                {
                    return SNMP_SUCCESS;
                }
            }
            if (pSetTlmCompIfDescr->i4RowStatus == CREATE_AND_WAIT)
            {
                pCompDescr = TlmCreateComponentDescr (u4DescrId, pCompIface);
                if (pCompDescr != NULL)
                {
                    pCompDescr->i4RowStatus = NOT_READY;
                }
                else
                {
                    return SNMP_FAILURE;
                }
            }
            else if (pSetTlmCompIfDescr->i4RowStatus == ACTIVE)
            {
                pCompDescr->i4RowStatus = ACTIVE;

                CfaApiGetTeLinkIfFromL3If ((UINT4) pCompIface->i4IfIndex,
                                           &u4TeIfIndex, TLM_ONE, TRUE);
                pTeIface = TlmFindTeIf ((INT4) u4TeIfIndex);

                for (u1Count = TLM_ZERO; u1Count < TLM_MAX_PRIORITY_LVL;
                     u1Count++)
                {
                    pCompDescr->aMaxLSPBwPrio[u1Count] = pCompIface->maxResBw;
                }
                TlmUtilCalculateBCvalue (pCompIface);
                TlmUpdateTeLinkInfo (pCompIface, pTeIface,
                                     TLM_COMPONENT_UPDATE);
                TlmUtilSendNotifMsgInfo (pTeIface, TE_LINK_UPDATED);
            }
            else if (pSetTlmCompIfDescr->i4RowStatus == NOT_IN_SERVICE)
            {
                pCompDescr->i4RowStatus = NOT_IN_SERVICE;
            }
            else if (pSetTlmCompIfDescr->i4RowStatus == DESTROY)
            {
                TlmDeleteComponentDescr (pCompDescr, pCompIface);
                CfaApiGetTeLinkIfFromL3If ((UINT4) pCompIface->i4IfIndex,
                                           &u4TeIfIndex, TLM_ONE, TRUE);

                pTeIface = TlmFindTeIf ((INT4) u4TeIfIndex);

                TlmUpdateTeLinkInfo (pCompIface, pTeIface,
                                     TLM_COMPONENT_DELETE);
                TlmUtilSendNotifMsgInfo (pTeIface, TE_LINK_UPDATED);
            }
            break;
        case TLM_COMP_LINK_DESC_STORAGE_TYPE:
            pCompDescr->i4StorageType = pSetTlmCompIfDescr->i4StorageType;
            break;
        default:
            break;
    }
    TLM_TRC_FN_EXIT ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  TlmSetAllComponentLinkBandwidthTable
 Input       :  i4IfIndex
                u4ComponentLinkBandwidthPriority
                pSetTlmCompLink
                u4ObjectId
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmSetAllComponentLinkBandwidthTable (INT4 i4IfIndex,
                                      UINT4 u4ComponentLinkBandwidthPriority,
                                      tTlmComponentLink * pSetTlmCompLink,
                                      UINT4 u4ObjectId)
{
    tTlmComponentLink  *pCompIface = NULL;
    UINT4               u4UnResBwPrio = u4ComponentLinkBandwidthPriority;
    INT4                i4RowStatus = TLM_ZERO;

    TLM_TRC_FN_ENTRY ();
    i4RowStatus = pSetTlmCompLink->aUnResBw[u4UnResBwPrio].i4RowStatus;

    pCompIface = TlmFindComponentIf (i4IfIndex);

    if ((u4ObjectId != TLM_COMP_LINK_DESC_ROW_STATUS) ||
        (i4RowStatus != CREATE_AND_WAIT))
    {
        if (pCompIface == NULL)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (pCompIface != NULL)
        {
            return SNMP_FAILURE;
        }
    }

    switch (u4ObjectId)
    {
        case TLM_COMP_LINK_BW_UNRESV:
            pCompIface->aUnResBw[u4UnResBwPrio].unResBw =
                pSetTlmCompLink->aUnResBw[u4UnResBwPrio].unResBw;
            break;
        case TLM_COMP_LINK_BW_ROW_STATUS:
            if (i4RowStatus == CREATE_AND_WAIT)
            {
                pCompIface->aUnResBw[u4UnResBwPrio].unResBw =
                    pCompIface->maxResBw;
                pCompIface->aUnResBw[u4UnResBwPrio].resBw =
                    TLM_DEF_BANDWIDTH_VALUE;
                pCompIface->aUnResBw[u4UnResBwPrio].i4RowStatus = i4RowStatus;
            }
            else if (i4RowStatus == ACTIVE)
            {
                if ((pSetTlmCompLink->aUnResBw[u4UnResBwPrio].i4RowStatus ==
                     NOT_IN_SERVICE) ||
                    (pSetTlmCompLink->aUnResBw[u4UnResBwPrio].i4RowStatus ==
                     NOT_READY))
                {
                    pCompIface->aUnResBw[u4UnResBwPrio].i4RowStatus =
                        pSetTlmCompLink->aUnResBw[u4UnResBwPrio].i4RowStatus;
                }
            }
            else if (i4RowStatus == DESTROY)
            {
                pCompIface->aUnResBw[u4UnResBwPrio].unResBw =
                    TLM_DEF_BANDWIDTH_VALUE;
                pCompIface->aUnResBw[u4UnResBwPrio].resBw =
                    TLM_DEF_BANDWIDTH_VALUE;
            }
            else if (i4RowStatus == NOT_IN_SERVICE)
            {
                pCompIface->aUnResBw[u4UnResBwPrio].i4RowStatus =
                    NOT_IN_SERVICE;
            }
            break;
        case TLM_COMP_LINK_BW_STORAGE_TYPE:
            pCompIface->aUnResBw[u4UnResBwPrio].i4StorageType =
                pSetTlmCompLink->aUnResBw[u4UnResBwPrio].i4StorageType;
            break;
        default:
            break;
    }
    TLM_TRC_FN_EXIT ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TlmSetAllTeLinkBwThresholdTable
 Input       :  i4IfIndex
                i4FsTeLinkBwThresholdIndex
                pSetTlmTeLink
                u4ObjectId
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TlmSetAllTeLinkBwThresholdTable (INT4 i4IfIndex,
                                 INT4 i4FsTeLinkBwThresholdIndex,
                                 tTlmTeLink * pSetTlmTeLink, UINT4 u4ObjectId)
{
    tTlmTeLink         *pTeIface = NULL;

    TLM_TRC_FN_ENTRY ();
    pTeIface = TlmFindTeIf (i4IfIndex);

    if (pTeIface == NULL)
    {
        TLM_TRC1 (TLM_ALL_TRC | TLM_FAILURE_TRC | TLM_MGMT_TRC,
                  "TlmSetAllTlmTeLinkBwThresholdTable: "
                  "No Te Link Information for %d Index\r\n", i4IfIndex);
        return SNMP_FAILURE;
    }
    switch (u4ObjectId)
    {
        case TLM_BW_THRESHOLD_UP_ROW_STATUS:
        {
            if (pSetTlmTeLink->i1ThresholdUpRowStatus == CREATE_AND_WAIT)
            {
                pTeIface->i1ThresholdUpRowStatus = NOT_READY;
            }
            else if (pSetTlmTeLink->i1ThresholdUpRowStatus == NOT_IN_SERVICE)
            {
                pTeIface->i1ThresholdUpRowStatus = NOT_IN_SERVICE;
            }
            else if (pSetTlmTeLink->i1ThresholdUpRowStatus == ACTIVE)
            {
                pTeIface->i1ThresholdUpRowStatus = ACTIVE;
            }

            break;
        }
        case TLM_BW_THRESHOLD_DOWN_ROW_STATUS:
        {
            if (pSetTlmTeLink->i1ThresholdDownRowStatus == CREATE_AND_WAIT)
            {
                pTeIface->i1ThresholdDownRowStatus = NOT_READY;
            }
            else if (pSetTlmTeLink->i1ThresholdDownRowStatus == NOT_IN_SERVICE)
            {
                pTeIface->i1ThresholdDownRowStatus = NOT_IN_SERVICE;
            }
            else if (pSetTlmTeLink->i1ThresholdDownRowStatus == ACTIVE)
            {
                pTeIface->i1ThresholdDownRowStatus = ACTIVE;
            }

            break;
        }
        case TLM_BW_THRESHOLD_ZERO:
        case TLM_BW_THRESHOLD_ONE:
        case TLM_BW_THRESHOLD_TWO:
        case TLM_BW_THRESHOLD_THREE:
        case TLM_BW_THRESHOLD_FOUR:
        case TLM_BW_THRESHOLD_FIVE:
        case TLM_BW_THRESHOLD_SIX:
        case TLM_BW_THRESHOLD_SEVEN:
        case TLM_BW_THRESHOLD_EIGHT:
        case TLM_BW_THRESHOLD_NINE:
        case TLM_BW_THRESHOLD_TEN:
        case TLM_BW_THRESHOLD_ELEVEN:
        case TLM_BW_THRESHOLD_TWELVE:
        case TLM_BW_THRESHOLD_THIRTEEN:
        case TLM_BW_THRESHOLD_FOURTEEN:
        case TLM_BW_THRESHOLD_FIFTEEN:
        {
            if (i4FsTeLinkBwThresholdIndex == TLM_ONE)
            {
                pTeIface->aTlmBWThresholdUp[u4ObjectId] =
                    pSetTlmTeLink->aTlmBWThresholdUp[u4ObjectId];

            }
            else
            {
                pTeIface->aTlmBWThresholdDown[u4ObjectId] =
                    pSetTlmTeLink->aTlmBWThresholdDown[u4ObjectId];

            }
        }
        default:
            break;
    }

    TLM_TRC_FN_EXIT ();
    return SNMP_SUCCESS;
}
