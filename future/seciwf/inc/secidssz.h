/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: secidssz.h,v 1.2 2012/04/23 13:04:18 siva Exp $
 *
 * Description:This file contains the sizing macros for IDS
 *******************************************************************/

enum {
    MAX_IDS_PKT_BUF_SIZING_ID,
 MAX_IDS_ATTACK_PKT_BUF_SIZING_ID,
    SECIDS_MAX_SIZING_ID
};


#ifdef  _SECIDSSZ_C
tMemPoolId SECIDSMemPoolIds[ SECIDS_MAX_SIZING_ID];
INT4  SecidsSizingMemCreateMemPools(VOID);
VOID  SecidsSizingMemDeleteMemPools(VOID);
#else  /*  _SECIDSSZ_C  */
extern tMemPoolId SECIDSMemPoolIds[ ];
extern INT4  SecidsSizingMemCreateMemPools(VOID);
extern VOID  SecidsSizingMemDeleteMemPools(VOID);
#endif /*  _SECIDSSZ_C  */


#ifdef  _SECIDSSZ_C
tFsModSizingParams FsSECIDSSizingParams [] = {
{ "tIdsPktBlock", "MAX_IDS_BUFF_MSG_SIZE", sizeof(tIdsPktBlock),MAX_IDS_BUFF_MSG_SIZE, MAX_IDS_BUFF_MSG_SIZE,0 },
{ "tIdsAttackPktBlock", "MAX_IDS_ATTACK_BUFF_MSG_SIZE", sizeof(tIdsAttackPktBlock),MAX_IDS_ATTACK_BUFF_MSG_SIZE, MAX_IDS_ATTACK_BUFF_MSG_SIZE,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _SECIDSSZ_C  */
extern tFsModSizingParams FsSECIDSSizingParams [];
#endif /*  _SECIDSSZ_C  */


