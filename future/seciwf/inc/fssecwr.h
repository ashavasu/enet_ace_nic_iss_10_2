/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssecwr.h,v 1.2 2011/09/24 10:03:21 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/
#ifndef _FSSECWR_H
#define _FSSECWR_H

VOID RegisterFSSEC(VOID);

VOID UnRegisterFSSEC(VOID);
INT4 FsSecDebugOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsSecDebugOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsSecDebugOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSecDebugOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _FSSECWR_H */
