/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secmem.h,v 1.3 2015/01/19 11:34:41 siva Exp $
 *
 * Description:This file contains macros related to MemPool.        
 *
 *******************************************************************/
#ifndef _SECMEM_H
#define _SECMEM_H
#include "lr.h"
/*****************************************************************************/
/*            Macros for defining respective PIDs (Pool Id)                  */
/*****************************************************************************/

#define PPPOE_DEF_SESSION_POOL_ID SECMemPoolIds[MAX_PPPOE_DEF_SESSION_SIZING_ID]
#define SEC_IF_IPADDR_POOL_ID  SECMemPoolIds[MAX_SEC_INTERFACE_IPADDR_SIZING_ID]

#define SEC_CRU_BUF_ID SECMemPoolIds[MAX_SEC_CRU_BUF_SIZING_ID]

#endif
