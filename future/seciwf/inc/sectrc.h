/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: sectrc.h,v 1.1.1.1 2011/09/12 09:03:16 siva Exp $
 *
 * Description:This file contains the global structure for IDS
 *******************************************************************/

#ifndef _SECTRC_H_
#define _SECTRC_H_

#define IDS_MOD_NAME          ((const char *) "IDS-IPC")    /* Module Name */
#define SEC_TRC_NONE             0x00000000
#define SEC_INIT_SHUT_TRC        0x00000001
#define SEC_MGMT_TRC             0x00000002
#define SEC_RESOURCE_TRC         0x00000020
#define SEC_FAILURE_TRC          0x00000040
#define SEC_CONTROL_PLANE_TRC    0x00000008
#define SEC_TRC_ALL              0x0000ffff


/* Traces in IDS-IPC */
#define SEC_TRC(TraceType, fmt)                             \
        MOD_TRC(IDS_TRC_FLAG, TraceType, IDS_MOD_NAME,      \
                (const char *)fmt)

#define SEC_TRC_ARG1(TraceType, fmt, arg1)                  \
        MOD_TRC_ARG1(IDS_TRC_FLAG, TraceType, IDS_MOD_NAME, \
                     (const char *)fmt, arg1)

#define SEC_TRC_ARG2(TraceType, fmt, arg1, arg2)            \
        MOD_TRC_ARG2(IDS_TRC_FLAG, TraceType, IDS_MOD_NAME, \
                     (const char *)fmt, arg1, arg2)




#endif /* _SECIDS_H_ */

