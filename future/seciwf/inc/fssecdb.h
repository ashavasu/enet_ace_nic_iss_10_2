/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssecdb.h,v 1.2 2011/09/24 10:03:21 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSSECDB_H
#define _FSSECDB_H


UINT4 fssec [] ={1,3,6,1,4,1,29601,2,64};
tSNMP_OID_TYPE fssecOID = {9, fssec};


UINT4 FsSecDebugOption [ ] ={1,3,6,1,4,1,29601,2,64,1,1};




tMbDbEntry fssecMibEntry[]= {

{{11,FsSecDebugOption}, NULL, FsSecDebugOptionGet, FsSecDebugOptionSet, FsSecDebugOptionTest, FsSecDebugOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"}
};

tMibData fssecEntry = { 1, fssecMibEntry };

#endif /* _FSSECDB_H */

