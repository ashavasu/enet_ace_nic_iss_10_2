/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: seckextn.h,v 1.1.1.1 2011/09/12 09:03:16 siva Exp $
 *
 * Description:This file contains the exported declarations for 
 *             the Security module
 *******************************************************************/
extern tOsixTaskId  gu4KernCfaTaskId;
extern tOsixSemId   gKernCfaSemId;
extern tOsixQId     gKernCfaQueId;
extern INT4 CfaGddInit (VOID);
extern UINT4     gu4SecTrcFlag;
extern UINT1 *CRU_BUF_PUT (struct sk_buff *pSkb, UINT4 u4Len);
extern struct sk_buff     * ArBufKernDequeueSkbWithSpinLock (struct sk_buff_head *);
extern VOID CRU_BUF_KernMemFreeSkb (struct sk_buff *pSkb);
extern VOID ArBufKernInitSkbQueueHead (struct sk_buff_head *pQlist);
extern VOID OsixInitWaitqHead (wait_queue_head_t *pWaitQ);
extern VOID CfaGddDeInit (VOID);
extern tNetDevice         *pDefXmitDev;
extern UINT4 OsixUtilEthTypeTrans (struct sk_buff *pSkb, struct net_device *pNetDev);
extern VOID OsixUtilUnRegDevAddPack (struct packet_type *pPktType);
extern UINT4 OsixQueSend (tOsixQId QueId, UINT1 *pu1Msg, UINT4 u4MsgLen);
extern UINT4 OsixEvtSend (tOsixTaskId TskId, UINT4 u4Events);
