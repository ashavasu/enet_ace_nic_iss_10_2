/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: seckgen.h,v 1.1.1.1 2011/09/12 09:03:16 siva Exp $
 *
 * Description:This file contains the  definitions and 
 *             macros used accross  Security kernel  modules 
 *******************************************************************/

#ifndef _SECKGEN_H_
#define _SECKGEN_H_

#define MAX_SEC_QUEUE_LEN  8


#define KERN_VPNC_INT                  "vpnc"
#define KERN_VPNC_INT_SIZE              4

#define CFA_TASK_NAME         "CFA"
#define CFA_SEM_NAME          "CFAS"
#define CFA_QUEUE_NAME        "CFAQ"
#define CFA_EVENT_WAIT_FLAGS  OSIX_WAIT 

#ifdef _SECDEV_C_
UINT4 gu4NpSimReadModIdx=0;
UINT4 gu4SecReadModIdx=0;
UINT4 gu4SnortReadModIdx=0;
UINT4 gu4NpSimWriteModIdx=0;
UINT4 gu4SecWriteModIdx=0;
UINT4 gu4SnortWriteModIdx=0;
UINT4 gu4ChrDevModIdx=0;
UINT4 gu4DevPutModIdx=0;
UINT4 gu4CopyModIdx=0;
UINT4 gu4CopySkbModIdx=0;
UINT4 gu4SecUsrModule = 0; 
UINT4 gu4NpSimModule = 0; 
UINT4 gu4SnortModule = 0; 
INT4  gi4CliWebSetError;
#else
extern UINT4 gu4NpSimReadModIdx;
extern UINT4 gu4SecReadModIdx;
extern UINT4 gu4SnortReadModIdx;
extern UINT4 gu4NpSimWriteModIdx;
extern UINT4 gu4SecWriteModIdx;
extern UINT4 gu4SnortWriteModIdx;
extern UINT4 gu4ChrDevModIdx;
extern UINT4 gu4DevPutModIdx;
extern UINT4 gu4CopyModIdx;
extern UINT4 gu4CopySkbModIdx;
extern INT4  gi4CliWebSetError;
extern UINT4 gu4SecUsrModule; 
extern UINT4 gu4NpSimModule; 
extern UINT4 gu4SnortModule; 
#endif

/*
Below Macros Will Be removed Later */
#define   NAT_INBOUND           1
#define   NAT_OUTBOUND          2

#endif /* _SECKGEN_H_ */
