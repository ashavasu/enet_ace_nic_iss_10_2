/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: secglob.h,v 1.4 2012/03/26 11:42:28 siva Exp $
 *
 * Description:This file contains the global variables
 *             used accross  Security module 
 *******************************************************************/

#ifndef _SECGLOB_H_
#define _SECGLOB_H_

extern tOsixSemId          gSecLock;
/* Global buffers for sending/Receiving data from user<->kernel space. 
 */
extern UINT1  gau1RxMsgBuf[SEC_MAX_MSG_BUF_SIZE];
extern UINT1  gau1TxMsgBuf[SEC_MAX_MSG_BUF_SIZE];

/* Related to IDS Logging */
UINT1                gau1IdsPrevLogFileName[IDS_FILENAME_LEN];
UINT4                gu4IdsLogContentSize = 0;
UINT4                gu4IdsMaxLogSize = IDS_MAX_LOG_FILESIZE;
UINT4                gu4IdsLogThreshold = IDS_DEFAULT_LOG_THRESHOLD;
CHR1                 gau1IdsLogFile [IDS_FILENAME_LEN];


tIDSGlobalInfo       gIDSGlobalInfo;
tIdsSockInfo         gIdsIssSockInfo;
tMemPoolId           gIdsPktBufPoolId = 0;
tOsixSemId           gIdsSemId = 0;
/* Global variable that controls the state of applying security to bridged packets.*/
UINT4                gu4SecBridgingStatus;

/* List of L2 VLANs to which security has to be applied. */
tSecVlanList        gSecvlanList;

/* Default L3 vlan index used for applying security for bridged packets. */
INT4                gi4SecIvrIfIndex;


tTMO_SLL gSecPppSessionInfoList; 

UINT1      gau1TempIfStatus[SYS_MAX_INTERFACES];

#endif /* _SECGLOB_H_ */
