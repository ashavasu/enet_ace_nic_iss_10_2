/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: seckprot.h,v 1.7 2017/12/06 09:35:10 siva Exp $
 *
 * Description:This file contains the Required Prototypes
 *             used accross  Security kernel  modules 
 *******************************************************************/

#ifndef _SECKPROT_H_
#define _SECKPROT_H_

UINT4 SNMPCheckForNVTChars (UINT1 *, INT4);
INT4 SecChkIsPktDestinedForDut(tCRU_BUF_CHAIN_HEADER *);
VOID SecGetIntAndDirInfoFromMac(tMacAddr *, tMacAddr *, INT1 *, INT4 *, INT4 *);
VOID SecUnLock (VOID);
VOID SecLock (VOID);
INT4 SecIsMacAllZeros (UINT1 *);
VOID SecProcessRxFrame(tSkb *, INT4, INT4, INT1);
INT4 SecPostPktToUser(tCRU_BUF_CHAIN_HEADER *, INT4);
INT4 SecDevAddPackCbkHandler(tSkb *, tNetDevice *, tPktType *, tNetDevice *);
INT4 SecProcessTxPkt (tCRU_BUF_CHAIN_HEADER *);
INT4 SecCheckForDosAttacks PROTO ((UINT1 *pu1Data, UINT4 u4PktSize));
INT4 SecPostMsgToUser PROTO ((VOID *pBuf, UINT4 u4MsgLen, INT4 i4ModuleIdx));
INT4 SecModInit PROTO ((VOID));
VOID SecModDeInit PROTO ((VOID));
INT4 KernUtilGetIpHdrOffset PROTO ((UINT1 *pu1Data, UINT4 u4PktSize));
INT4 SecDevUpdateWanPhyIndex(UINT4 u4Command, FS_ULONG ulPtr );
INT4
GddKSecUpdateOutTag(UINT1 u1Direction, UINT2 u2DsaVlanId);
ssize_t SecDevRead PROTO((tFile * pFile, CHR1 *pc1Buf, size_t buf_length,
               loff_t * pos));

ssize_t SecDevWrite (tFile * pFile, CONST CHR1 *pBuf, size_t length,
                loff_t * ppos);

#ifndef LINUX_KERNEL_3_3_4VER
INT4
SecDevIoctl (tInode *pInode, tFile *pFile, UINT4 u4IoctlNum,
                FS_ULONG ulIoctlParam);
#else

INT4 SecDevIoctl (tFile *pFile, UINT4 u4IoctlNum,
                FS_ULONG ulIoctlParam);
#endif

INT4
SecDevOpen (tInode *pInode, tFile *pFile);

INT4
SecDevRelease (tInode *pInode, tFile *pFile);

INT4   ipif_is_our_address PROTO((UINT4 u4Addr));

/* Prototypes of stub functions */

INT4 IssSzRegisterModuleSizingParams PROTO ((CHR1 * pu1ModName,
                                 tFsModSizingParams * pModSizingParams));

/*VOID                                                                          
SysLogMsg PROTO ((UINT4 u4Level, UINT4 u4ModuleId, const CHR1 * pFormat, ...)); */

INT1 nmhGetFsSyslogLogging PROTO ((INT4 *pi4RetValFsSyslogLogging));

VOID SecDevTaskCallBackFn (FS_ULONG ulModListPtr);
INT4 SecDevUpdateIfInfo (UINT4 u4Command, FS_ULONG ulPtr);

INT4 SecDevRegisterModule (INT1 i1Module, tFile * pFile);

INT4 SecDevPPPoEAddNewSession (FS_ULONG ulPtr);

INT4 SecDevPPPoEDelSession (FS_ULONG ulPtr);

INT4
SecDevUpdateSecIpInfo (UINT4 u4Command, FS_ULONG ulPtr);

INT4 CfaGetIfMtu (UINT4 u4IfIdx, UINT4 *pu4Mtu);

VOID SeckDeInit  PROTO((VOID));
INT4 SeckInit (VOID);

VOID SecMainInitGaModInfo (VOID);

INT4
MemBuddyInit (UINT4 u4Instances);

INT4 SecInitRegWithCfa (VOID);
INT4 SecInitRegWithIpv6 (VOID);
INT4 SecDevRegisterChrDevice (VOID);
INT4 SecInitPostPacketToKernel (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIdx,
                           UINT1 u1Direction);
VOID FsUtilBitListInit (UINT4 u4Count);
VOID SecInit (VOID);
INT4 SecInitDevices (VOID);
VOID SecDeInitDevices (VOID);
INT4 SecInitDeRegWithCfa (VOID);
INT4 SecInitDeRegWithIpv6 (VOID);
INT4 SeckCfaInit(VOID);
VOID SeckCfaMain(VOID *arg);

INT4 SecDevUpdateSecIvrIndex PROTO ((FS_ULONG ulIoctlPtr));
INT4 SecDevUpdateSecBridgingStatus PROTO ((FS_ULONG ulIoctlParam));
INT4 SecDevSetVlanList PROTO ((FS_ULONG ulIoctlParam));
INT4  SecDevReSetVlanList PROTO ((FS_ULONG ulIoctlParam));
VOID SecDevProcessPktFromUser (tCRU_BUF_CHAIN_HEADER *pBuf,
                          UINT4 u4IfIndex,
                          UINT1 u1MsgType,
                          UINT1 u1Direction);
INT4 SecDevDebugTrace(UINT4 u4Command, FS_ULONG ulPtr );

#endif /* _SECKPROT_H_*/
