/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: secgen.h,v 1.5 2015/02/24 11:51:49 siva Exp $
 *
 * Description:This file contains the Required basic includes
 *             used accross  Security modules 
 *******************************************************************/

#ifndef _SECGEN_H_
#define _SECGEN_H_
#include "lr.h"
#include "iss.h"

extern    tOsixTaskId               gCfaTaskId;
#define   CFA_TASK_ID               gCfaTaskId
#define   CFA_GDD_INTERRUPT_EVENT   0x00010000

#define   SEC_MODULE_DATA_SIZE      28

/* Tag Control Identifier TCI contains 3bits priority, 
 * 1 canonical form bit and 12 bits Vlan ID 
 * Mask used for seggregation of VLAN ID from TCI field i
 */

#define SEC_VLAN_VID_MASK                0x0fff

#define SYS_COMMAND_MAX_LEN 50


/*For User to Kernel communication use this CRU buffer*/
#define SEC_CRU_MTU_SZ 1500
#define SEC_CRU_MIN_HDR 128

/* Invalid Object. */
#define   SEC_UNALLOCATED           -1

/* Value that defines the offset of the Ethernet header
 * within the Security Module data. 
 */
#define   SEC_ETH_HDR_OFFSET        8
#define MAX_SEC_INTERFACES 3
#define SYS_MAX_LAN_INTERFACES IP_DEV_MAX_IP_INTF

#define SEC_SKIP_NAT VPN_SKIP_NAT
#define SEC_SKIP_FWL VPN_SKIP_FWL
#define SEC_SKIP_VPN VPN_SKIP_FWL * 2

#define CFA_31BIT_MASK_VALUE        0xFFFFFFFE

#define SEC_MOD_CFA_IF_CREATE        CFA_IF_CREATE
#define SEC_MOD_CFA_IF_DELETE        CFA_IF_DELETE
#define SEC_MOD_CFA_IF_UPDATE        CFA_IF_UPDATE
#define SEC_MOD_CFA_IF_UPDATE_VLANID  0x08 
#define SEC_MOD_IP6_ADDR_ADD          0x10
#define SEC_MOD_IP6_ADDR_DELETE       0x20

#define SEC_ADD_VLAN_LIST(au1List1, au1List2) \
              {\
                 UINT2 u2ByteIndex;\
                 \
                 for (u2ByteIndex = 0;\
                      u2ByteIndex < CFA_SEC_VLAN_LIST_SIZE;\
                      u2ByteIndex++) {\
                    au1List1[u2ByteIndex] |= au1List2[u2ByteIndex];\
                 }\
              }

#define SEC_REMOVE_VLAN_LIST(au1List1, au1List2) \
              {\
                 UINT2 u2ByteIndex;\
                 \
                 for (u2ByteIndex = 0;\
                      u2ByteIndex < CFA_SEC_VLAN_LIST_SIZE;\
                      u2ByteIndex++) {\
                    au1List1[u2ByteIndex] &= ~au1List2[u2ByteIndex];\
                 }\
              }

#define SEC_UCAST_IP 0
#define SEC_BCAST_IP 1

typedef struct
{
    tIp6Addr    Ip6Addr;
    UINT4       u4Ip6PrefixLen;
    tMacAddr    MacAddress;
    UINT2    u2VlanId;
    INT4  i4IfIndex;
    UINT4  u4IpAddr;
    UINT4  u4Mtu;
    UINT4  u4NetMask;
    UINT4  u4BcastAddr;
    INT4  i4PhyIndex;
    UINT1  au1InfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1  u1WanType;
    UINT1  u1NwType;
    UINT1  u1IfType;
    UINT1  u1OperStatus;
}tSecIfInfo;

typedef struct
{
    tTMO_SLL_NODE  SecPppSessionNode;
    INT4           i4PPPIfIndex;  /* PPP Index*/
    INT4           i4IfIndex;  /* Physical Index*/
    UINT2          u2SessionId;
    UINT1          au1Rsvd[2];
}tSecPppSessionInfo;

/* RB Tree with interface IP address as index. This is required
   in the case of checking for local IP address. SecUtilChkIsOurIpAddress */
typedef struct
{
    tRBNodeEmbd         SecIfIpAddrNode;
    UINT4               u4IpAddr;
    UINT4               u4Mask;
    INT4                i4IfIndex;
}tSecIfIpAddrInfo;
/*Used to update "gaSecWanPhyIf" in kernel in cases of IVR.
*"gaSecWanPhyIf" will be used to decide direction in cases of IVR interfaces 
*and in platforms where the inbound and outbound traffic are received 
*on the same interface for security processing at Kernel */
typedef struct
{
    INT4  i4IfIndex;
    UINT1 u1NwType;
    UINT1 au1Rsvd[3];
}tSecWanInfo;

#endif
