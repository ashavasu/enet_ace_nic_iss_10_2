/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: secextn.h,v 1.6 2015/02/24 11:51:48 siva Exp $
 *
 * Description:This file contains the exported variables
 *             used across  Security module 
 *******************************************************************/

#ifndef _SECEXTN_H_
#define _SECEXTN_H_

extern tRBTree             gSecIfIpAddrList;
extern tRBTree             gSecIfBcastIpAddrList;
extern UINT4     gu4SecTrcFlag;
extern tSecIfInfo gaSecWanIfInfo[SYS_MAX_WAN_INTERFACES];
extern tSecIfInfo gaSecLanIfInfo[SYS_MAX_LAN_INTERFACES];
extern INT1 gaSecWanPhyIf[SYS_MAX_WAN_PHY_INTERFACES];

extern struct tasklet_struct gLrMainTasklet;
/* Global buffers for sending/Receiving data from user<->kernel space. 
 */
extern tIDSGlobalInfo       gIDSGlobalInfo;
extern tIdsSockInfo   gIdsIssSockInfo;
extern tMemPoolId     gIdsPktBufPoolId;
extern tOsixSemId     gIdsSemId;
extern tTMO_SLL gSecPppSessionInfoList;
extern INT4 CfaGddInit (VOID);
extern VOID SecMainInitGaModInfo (VOID);

extern VOID FwlGenerateMemFailureTrap  PROTO((UINT1 u1NodeName)); 
extern VOID FwlGenerateAttackSumTrap PROTO((VOID)); 
extern VOID IdsSendAttackPktTrap (UINT1 *pu1BlackListIpAddr);

extern UINT4 FsNpGetDummyVlanId (UINT4 u4IfIndex);
extern INT4 CfaIwfEnetProcessRxFrame (tCRU_BUF_CHAIN_HEADER * pCruBuf, 
                                      UINT4 u4IfIndex,
                                      UINT4 u4PktSize, UINT2 u2Protocol,
                                      UINT1 u1Direction);
/* Related to IDS Logging */
extern UINT1  gau1IdsPrevLogFileName[IDS_FILENAME_LEN];
extern UINT4  gu4IdsLogContentSize;
extern UINT4  gu4IdsMaxLogSize;
extern UINT4  gu4IdsLogThreshold;
extern CHR1   gau1IdsLogFile [IDS_FILENAME_LEN];
/* Global variable that controls the state of applying security to bridged packets.*/
extern UINT4                gu4SecBridgingStatus;

/* List of L2 VLANs to which security has to be applied. */
extern tSecVlanList        gSecvlanList;

/* Default L3 vlan index used for applying security for bridged packets. */
extern INT4               gi4SecIvrIfIndex;

extern UINT1      gau1TempIfStatus[SYS_MAX_INTERFACES];

#endif /* _SECGLOB_H_ */
