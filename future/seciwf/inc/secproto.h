/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: secproto.h,v 1.9 2014/01/29 13:19:46 siva Exp $
 *
 * Description:This file contains the Required basic includes
 *             used accross  Security modules 
 *******************************************************************/

#ifndef _SECPROTO_H_
#define _SECPROTO_H_
VOID SecDeInit PROTO ((VOID));
INT4 SecInitRegWithCfa (VOID);
INT4 SecInitDeRegWithCfa (VOID);
INT4 SecInitRegWithIpv6 (VOID);
INT4 SecInitDeRegWithIpv6 (VOID);
VOID SecApiNotifyIpv6IfStatusChange PROTO ((tNetIpv6HliParams * pNetIpv6HlParams));
VOID SecInitHandleMsgFromKernel PROTO ((UINT1 *pu1Buf, UINT4 u4BufLen));
VOID SecInitPostPktToCfa PROTO ((UINT1 *pu1PktBuf, 
                                 UINT4 u4PktLen));

INT4 SecProcessOutboundPkt PROTO((tCRU_BUF_CHAIN_HEADER * pBuf,
                           UINT4                   u4IfIndex,
                           BOOL1                    *pi1ByPass,
                           UINT2 u2Protocol, tCfaIfInfo * IfInfo));

INT4 SecProcessInboundPkt PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT4                   u4IfIndex,
                                 UINT2 u2Protocol, tCfaIfInfo * IfInfo));

INT4 SecUtilChkIsOurIpAddress PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, t_IP_HEADER *pIpHeader,
                          UINT2 u2Proto));

INT4
SecMainHandlePacketFromCfa PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                        UINT4 u4IfIdx));
INT4
SecUtilUpdateIfInfo PROTO ((UINT4 u4Command, tSecIfInfo *pSecIfInfo));

INT4 SecUtilExtractIpHdr PROTO ((t_IP_HEADER * pIp, 
                                 tCRU_BUF_CHAIN_HEADER * pBuf));
INT4
SecUtilExtractIp6Hdr PROTO ((tIp6Hdr * pIp6, tCRU_BUF_CHAIN_HEADER * pBuf));

INT4
SecUtilUpdateEthHdrToModuleData PROTO((tCRU_BUF_CHAIN_HEADER * , 
                                 UINT2 *));
INT4
SecUtilPrependEthHdrToPkt PROTO((tCRU_BUF_CHAIN_HEADER *));


VOID SecLock PROTO((VOID));
VOID SecUnLock PROTO((VOID));
INT4 SecInitPostPacketToKernel (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIdx, UINT1 u1Direction);
INT4 SecInitDevModInit (VOID);
VOID SecInitDevModDeInit (VOID);

PUBLIC INT4 SecApiIntfCreateCallBack (tCfaRegInfo * pCfaRegInfo);
PUBLIC INT4 SecApiIntfDelCallBack (tCfaRegInfo * pCfaRegInfo);
PUBLIC INT4 SecApiIntfUpdtCallBack (tCfaRegInfo * pCfaRegInfo);

INT4 SeckInit (VOID);

INT4
SecInitInvokeIoctl PROTO ((UINT4 u4Command , VOID *pSecInfo));

/************************************
 * Porting API's in SECURITY MODULE *
 ************************************/

UINT4 
SecPortFsNpGetDummyVlanId PROTO((UINT4 u4IfIndex ));

INT1 
SecPortSecUtilGetGlobalStatus  PROTO ((VOID));

UINT4 
SecPortFwlAclProcessPktForInFiltering PROTO((tCRU_BUF_CHAIN_HEADER * pBuf , UINT4 u4InIfaceNum ));

UINT4 
SecPortFwlAclProcessPktForOutFiltering PROTO((tCRU_BUF_CHAIN_HEADER * pBuf , UINT4 u4OutIfaceNum ));

INT4 
SecPortNatTranslateInBoundPkt PROTO((tCRU_BUF_CHAIN_HEADER ** ppBuf , UINT4 u4InIf , INT1 *pi1FrgReass ));

INT4 
SecPortNatTranslateOutBoundPkt PROTO((tCRU_BUF_CHAIN_HEADER ** ppBuf , UINT4 u4OutIf , INT1 *pi1FrgReass ));

INT4 
SecPortNatLock  PROTO ((VOID));

INT4 
SecPortNatUnLock  PROTO ((VOID));

INT4 
SecPortNatUtilGetGlobalNatStatus  PROTO ((VOID));

UINT4 
SecPortNatCheckIfNatEnable PROTO((UINT4 u4IfNum ));

UINT4 
SecPortVpnGetTunnelEndpoint PROTO((UINT2 u2PolicyIndex , UINT4 u4SrcAddr ,
                                   UINT4 u4DestAddr , UINT4 u4Protocol ,
                                   UINT4 *pu4TunFlag , UINT4 *pu4RuleFlag,
                                   UINT4 *pu4Src, UINT4 *pu4Dest));

UINT1 
SecPortSecv4InProcess PROTO((tIP_BUF_CHAIN_HEADER ** ppBuf , UINT4 u4Port ));

UINT1 
SecPortSecv4OutProcess PROTO((tIP_BUF_CHAIN_HEADER * pCBuf , UINT2 u2Port ));

VOID 
SecPortIpUpdateInIfaceErrInCxt PROTO((UINT4 u4CxtId ));

VOID
SecPortIpUpdateOutIfaceErrInCxt PROTO((UINT4 u4ContextId ));

INT4
SecPortVcmGetContextIdFromCfaIfIndex PROTO((UINT4 u4CfaIfIndex , UINT4 *pu4CxtId ));

VOID
SecPortSecv4SendIcmpErrMsg (tCRU_BUF_CHAIN_HEADER * pBuf, t_ICMP * pIcmp);

VOID SecPortFwlGenerateAttackSumTrap (VOID);
VOID
SecPortFwlGenerateMemFailureTrap (UINT1 u1NodeName);
INT4
SecPortFwlSndThresholdExceedTrp (UINT4 u4IfIndex, UINT4 u4PktCount);
VOID SecPortIdsSendAttackPktTrap (UINT1 *pu1BlackListIpAddr);
VOID
SecPortFwlLogMessage (UINT4 u4AttackType,
               UINT4 u4IfaceNum,
               tCRU_BUF_CHAIN_HEADER * pBuf,
               UINT4 u4LogLevel, UINT4 u4Severity, UINT1 *pMsg);
INT1 SecPortFwlErrorMessageGenerate (tCRU_BUF_CHAIN_HEADER * pBuf, 
                                     UINT4 u4IfIndex);

UINT1 
SecPortSecv6InProcess PROTO((tIP_BUF_CHAIN_HEADER * pBuf , UINT4 u4Port ));

UINT1 
SecPortSecv6OutProcess PROTO((tIP_BUF_CHAIN_HEADER * pCBuf , UINT4 u4Port ));

INT1 SecPortSecv6UtilGetGlobalStatus PROTO ((VOID));

UINT4
SecPortVpnGetTunnelEndpointForIp6 PROTO ((tIp6Addr *pIp6SrcAddr,
                             tIp6Addr *pIp6DestAddr, UINT4 u4Protocol,
                             UINT4 *pu4TunFlag, UINT4 *pu4RuleFlag));

VOID SecPortIp6UpdateIfaceInDiscards PROTO ((UINT4 u4IfIndex));

VOID SecPortIp6UpdateIfaceOutDiscards PROTO ((UINT4 u4IfIndex));

INT1
SecPortArpResolve PROTO ((UINT4 u4IpAddr, INT1 *pi1Hw_addr, UINT1 *pu1EncapType));

/* prototypes for Security module initialization functions  */
VOID SecInitHandleFwlLogMessage (UINT1 *pu1Buf, UINT4 u4BufLen);
VOID  SecInitHandleFwlTrapMsssage (UINT1 *pu1Buf, UINT4 u4BufLen);
VOID  SecInitHandleFwlErrorMsssage (UINT1 *pu1Buf, UINT4 u4BufLen);
VOID  SecInitHandleSecv4ErrorMsssage (UINT1 *pu1Buf, UINT4 u4BufLen);
VOID  SecInitHandleSecv4DummyMessage (UINT1 *pu1Buf, UINT4 u4BufLen);

VOID SecInitHandleIkeMsssage (UINT1 *pu1Buf, UINT4 u4BufLen);
VOID SecPortIndDummyVlanCreation (UINT4 u4IfIndex);
INT4 SecMainDeInit (VOID);
VOID SecInit (VOID);
INT4 SecInitDevices (VOID);
VOID SecDeInitDevices (VOID);
VOID SeckDeInit  (VOID);
INT4 SecInitKernModule (CONST CHR1 * pFileName);
VOID SecDeInitKernModule  (CONST CHR1 * pFileName);
INT4 SecApiUpdateIfInfo (UINT4 u4Command, tSecIfInfo * pSecIfInfo);
VOID SecInitHandleUtlTrcMsg (UINT1 *pu1Buf, UINT4 u4BufLen);

INT4
SecUtilSendDataPktToCfa PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, 
                              UINT4 u4IfIndex, UINT2 u2Protocol, t_IP_HEADER *pIpHdr));

VOID
SecNotifyHigherLayerIntfStatus PROTO ((tSecIfInfo     *pIfInfo,
                                tIpConfigInfo   *pIpInfo,
                                UINT4           u4IfIndex,
                                UINT1           u1Action));

VOID SecUpdateVlanWanInfo PROTO ((tVlanId VlanId, tSecIfInfo *pSecIfInfo));

INT4 
SecUtilPPPoEAddNewSession PROTO ((tSecPppSessionInfo *pSecPppSessionInfo));

INT4 
SecUtilPPPoEDelSession PROTO ((tSecPppSessionInfo *pSecPppSessionInfo));

INT4
SecUtilGetPhyIndexFromL3VlanIndex PROTO((UINT4 u4IfIndex, UINT4 *pu4PhyIfIndex));
INT4 
SecUtilPPPoEGetSessionInfo PROTO ((tSecPppSessionInfo *pSecPppSessionInfo));
UINT4 SecUtilGetVlanId PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                               UINT4 u4IfIndex, UINT1 u1Direction));

INT4 SecUtilIfIpAddrEntryCmp (tRBElem * prbElem1, tRBElem * prbElem2);
INT4 SecUtilAddIfIpAddrEntry (tSecIfInfo *pSecIfInfo, UINT1 u1IsBcastIp);
tSecIfIpAddrInfo * SecUtilGetIfIpAddrEntry (UINT4 u4IpAddr, UINT1 u1IsBcastIp);
INT4 SecUtilDeleteIfIpAddrNode(UINT4 u4IpAddr, UINT1 u1IsBcastIp);

INT4 SecUtilUpdateIdsStatus (UINT4 u4Status);
UINT4 SecUtilGetIdsStatus (VOID);
INT4 SecUtilFreeIfIpAddrEntry(tRBElem * pElem, UINT4 u4Arg);
INT4
SecUtilWalkFnGetIfIpAddrEntry (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                         void *pArg, void *pOut);
INT4
SecUtilWalkFnDeleteSecIpInfo (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                         void *pArg, void *pOut);
VOID
SecUtilDeleteAllSecIpInfo (INT4 i4IfIndex);
INT4 SecDecodeEthPkt PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4DataLength));
PUBLIC INT4 PmeScanInit PROTO ((VOID));
PUBLIC INT4 PmeScanMain PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4OffSet));
INT4 
SecProcessCheckInvalidIntf(tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Offset,UINT1 u1NwType);

INT4 SecProcessCheckIcmpv6Pkt(tCRU_BUF_CHAIN_HEADER * pBuf);
#endif /* _SECPROTO_H_ */
