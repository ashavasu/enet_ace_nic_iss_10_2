/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: secids.h,v 1.6 2014/08/21 12:19:13 siva Exp $
 *
 * Description:This file contains the global structure for IDS
 *******************************************************************/

#ifndef _SECIDS_H_
#define _SECIDS_H_

/* IDS-IPC Global Structure */
typedef struct
{
    tOsixTaskId IdsTaskId;        /* IDSIPC Task ID */
    tOsixQId    IdsQueId;         /* IDSIPC Queue ID */
    tMemPoolId  IdsMemPoolId;     /* Pool Id for IDSIPC */
    tMemPoolId  IdsAttackPktMemPoolId;     /* Pool Id for IDS attack packets */
    UINT4       u4IdsTrcFlag;     /* IDSIPC Trace Flag */
}tIDSGlobalInfo;

typedef struct
{
    tCRU_BUF_CHAIN_HEADER      *pBuffer;
    UINT4                      u4MsgType; 
}tIdsQMsg;

typedef struct
{
    UINT1   au1IdsPktBuf[ISS_IDS_MAX_BUF_SIZE + 1];
}tIdsPktBlock;

typedef struct
{
    UINT1   au1IdsAttackPktBuf[SEC_MAX_PKT_SIZE + 1];
}tIdsAttackPktBlock;


#define IDS_MAX_Q_DEPTH        1500
#define IDS_TASK_NAME         ((UINT1 *)"IDSIPC")           /* Task Name */
#define IDS_TASK_QUE_NAME     ((UINT1 *)"IDSIPCQ")          /* Queue Name */
#define IDS_MOD_NAME          ((const char *) "IDS-IPC")    /* Module Name */

/* IDS_IPC Semaphore related constants */
#define IDS_SEMAPHORE         ((const UINT1*)"IDSS")
#define IDS_BINARY_SEM        1
#define IDS_SEM_FLAGS         0 /* unused */

#define IDS_TASK_ID         gIDSGlobalInfo.IdsTaskId
#define IDS_TRC_FLAG        gIDSGlobalInfo.u4IdsTrcFlag
#define IDS_QUE_ID          gIDSGlobalInfo.IdsQueId
#define IDS_QUE_POOL_ID     gIDSGlobalInfo.IdsMemPoolId
#define IDS_PKT_BUF_POOL_ID gIdsPktBufPoolId
#define IDS_ATTACk_PKT_POOL_ID gIDSGlobalInfo.IdsAttackPktMemPoolId

#define IDS_MSG_NOSIGNAL   0x4000 /* Synonymous with MSG_NOSIGNAL defined in 
                                   * linux/socket.h.
                                   */
/* Event Types */
#define IDS_PKT_ENQ_EVENT  0x01
#define IDS_PKT_CALLBK_EVENT 0x02

/* Message Types */
#define DATA_FROM_SEC_TO_IDS   1
#define DATA_FROM_IDS_TO_CFA   2
#define CTRLMSG_FRM_ISS_TO_IDS 3
#define CTRLMSG_FRM_IDS_TO_ISS 4
#define IDS_DATA_PKT_TO_SEC 5
#define IDS_CTRL_MSG_TO_SEC 6

#define IDS_ALL_EVENTS     (IDS_PKT_ENQ_EVENT | IDS_PKT_CALLBK_EVENT)

#define IDS_TRC_NONE             0x00000000
#define IDS_INIT_SHUT_TRC        0x00000001
#define IDS_MGMT_TRC             0x00000002
#define IDS_RESOURCE_TRC         0x00000020
#define IDS_FAILURE_TRC          0x00000040
#define IDS_CONTROL_PLANE_TRC    0x00000008
#define IDS_TRC_ALL              0x0000ffff

/* IDS Lock */
#define IDS_LOCK         SecIdsLock
#define IDS_UNLOCK       SecIdsUnLock

/* Configurable Files in IDS */
#define IDS_CONF_FILE         "snort.conf"
#define IDS_RULES_DIR         "rules/"
#define IDS_PID_FILE          FLASH "snort_iss.pid"
#define IDS_PID_LEN           16 
#define IDS_CMD_LEN           64

/* Traces in IDS-IPC */
#define IDS_TRC(TraceType, fmt)                             \
        MOD_TRC(IDS_TRC_FLAG, TraceType, IDS_MOD_NAME,      \
                (const char *)fmt)

#define IDS_TRC_ARG1(TraceType, fmt, arg1)                  \
        MOD_TRC_ARG1(IDS_TRC_FLAG, TraceType, IDS_MOD_NAME, \
                     (const char *)fmt, arg1)

#define IDS_TRC_ARG2(TraceType, fmt, arg1, arg2)            \
        MOD_TRC_ARG2(IDS_TRC_FLAG, TraceType, IDS_MOD_NAME, \
                     (const char *)fmt, arg1, arg2)

/*Macros for IDS-IPC communication */

#define ISS_IDS_SWITCHID gu4SecSwitchId
#define ISS_IDS_CTRLFD (gIdsIssSockInfo.i4CtrlSockFd)
#define ISS_IDS_DATAFD (gIdsIssSockInfo.i4DataSockFd)
#define ISS_IDS_MGMTFD (gIdsIssSockInfo.i4MgmtSockFd)

#define ISS_IDS_CONN_STATUS (gIdsIssSockInfo.u1IsSockConnected)
#define IDS_IS_SOCKFD_CREATED(fd) (ISS_IDS_SOCK_INIT != (fd))

#define IS_ISS_IDS_SOCKFD_CONNECTED(connStatus) (ISS_IDS_CONN_STATUS  & (connStatus))

#define IS_IDS_VERDICT_VALID(verdict) \
    (((verdict) & (ISS_IDS_PKT_ALLOW|ISS_IDS_PKT_DROP|ISS_IDS_PKT_BLACKLIST|ISS_IDS_TRAP_PKT_DROP_THRESH))\
               && ((verdict) != ISS_IDS_PKT_NONE) && ((verdict) != ISS_IDS_PKT_MAX))

#define IS_IDS_CTRL_MSG_VALID(message) \
    (((message) & (ISS_IDS_DATA_PKT|ISS_IDS_TRAP_PKT_DROP_THRESH|\
                   ISS_IDS_LOG_DATA|ISS_IDS_INIT_COMP)))

#define IS_IDS_MGMT_MSG_VALID(message) \
    (((message) & (ISS_IDS_SET_PKT_DROP_THRESH|ISS_IDS_VERSION_REQ|\
                   ISS_IDS_GLB_STATS_REQ| ISS_IDS_MGMT_RESPONSE|\
                   ISS_IDS_INTF_STATS_REQ|ISS_IDS_SET_LOG_STATUS|ISS_IDS_SET_DEL_INTF|\
                   ISS_IDS_CLEAR_GLB_STATS|ISS_IDS_CLEAR_INTF_STATS|ISS_IDS_RESET_IDS|ISS_IDS_LOAD_RULES|ISS_IDS_UNLOAD_RULES)))

#define IS_IDS_MGMT_GET_MSG_VALID(message) \
    (((message) & (ISS_IDS_VERSION_REQ|ISS_IDS_GLB_STATS_REQ|\
                   ISS_IDS_INTF_STATS_REQ)))

#define IS_IDS_MGMT_SET_MSG_VALID(message) \
    (((message) & (ISS_IDS_SET_PKT_DROP_THRESH| ISS_IDS_SET_LOG_STATUS|\
                   ISS_IDS_SET_DEL_INTF|ISS_IDS_CLEAR_GLB_STATS|\
                   ISS_IDS_CLEAR_INTF_STATS|ISS_IDS_MGMT_RESPONSE|ISS_IDS_RESET_IDS|ISS_IDS_LOAD_RULES|ISS_IDS_UNLOAD_RULES)))

#define ISS_IDS_SET_SOCKFD_CONN_STATUS(connStatus) \
do{\
    ISS_IDS_CONN_STATUS |= (connStatus);\
}while(0)

#define ISS_IDS_CLEAR_SOCKFD_CONN_STATUS(connStatus) \
do{\
    ISS_IDS_CONN_STATUS = (UINT1)(ISS_IDS_CONN_STATUS & ~(connStatus));\
}while(0)

/* Constant related to timer */
#define  IDS_MAX_TIME_LEN                             21

/* prototype definition */

INT4 SecIdsMainTaskInit PROTO ((VOID));
INT4 SecIdsMainTaskDeInit PROTO ((VOID));

INT4 SecIdsEnqueueDataFrame PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1MesgType, UINT4 u4IfIndex));
INT4 SecIdsEnqueueCtrlMsg PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1MesgType));

VOID SecIdsHandleEnqPktEvt PROTO ((VOID));
VOID SecIdsHandleCallbkEvt PROTO ((VOID));

INT4 SecIdsLock PROTO ((VOID));
INT4 SecIdsUnLock PROTO ((VOID));

/* prototype for IDS-IPC communication */
INT4 SecIdsIPCInit PROTO ((VOID));
INT4 SecIdsIPCDeInit PROTO ((VOID));

INT4 SecIdsIpcOpenAndConnectCtrlFd PROTO ((VOID));
INT4 SecIdsIpcOpenAndConnectDataFd PROTO ((VOID));
INT4 SecIdsIpcOpenAndConnectMgmtFd PROTO ((VOID));

INT4 SecIdsIpcCloseCtrlFd PROTO ((VOID));
INT4 SecIdsIpcCloseDataFd PROTO ((VOID));
INT4 SecIdsIpcCloseMgmtFd PROTO ((VOID));

INT4 SecIdsIpcSendDataPkt PROTO ((tCRU_BUF_CHAIN_HEADER *pCRUBuf, UINT4 u4IfIndex));
INT4 SecIdsIpcSendCtrlMsg PROTO ((tCRU_BUF_CHAIN_HEADER *pCRUBuf));
INT4 SecIdsIpcSendMgmtMsg PROTO ((UINT4 u4OpCode, UINT4 u4Value));

INT4 SecIdsIpcRcvDataPkt PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf));
INT4 SecIdsIpcRcvCtrlMsg PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf));
INT4 SecIdsIpcRcvMgmtMsg PROTO ((tIdsGetInfo *pIdsGetInfo));

VOID SecIdsIpcWriteCallBackFn PROTO ((INT4 i4SockFd));
UINT4 SecIdsIpcGetIpAddr PROTO ((VOID));

VOID SecIdsLogData PROTO ((UINT1 *, UINT4));

#endif /* _SECIDS_H_ */

