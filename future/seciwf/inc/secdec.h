/********************************************************************                                * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secdec.h,v 1.2 2012/02/15 12:23:38 siva Exp $
 *
 * Description:This file contains the exported definitions and
 *             macros of security ids decoder module
 *
 *******************************************************************/

#ifndef _SECDEC_H
#define _SECDEC_H

#include "seckinc.h"
#include "secinc.h"

#define DECODE_START_INDEX 0
enum {
    DECODE_TCP_XMAS = DECODE_START_INDEX,                                                               DECODE_TCP_NMAP_XMAS,
    DECODE_DOS_NAPTHA,
    DECODE_SYN_TO_MULTICAST,
    DECODE_ZERO_TTL,
    DECODE_BAD_FRAGBITS,
    DECODE_UDP_IPV6_ZERO_CHECKSUM,                                                                      DECODE_IP4_LEN_OFFSET,
    DECODE_ICMP4_TYPE_OTHER = 418,
    DECODE_TCP_BAD_URP,
    DECODE_TCP_SYN_FIN,
    DECODE_TCP_SYN_RST,                                                                                 DECODE_TCP_MUST_ACK,
    DECODE_TCP_NO_SYN_ACK_RST,
    DECODE_ETH_HDR_TRUNC,
    DECODE_IP4_HDR_TRUNC,
    DECODE_ICMP4_HDR_TRUNC,
    DECODE_ICMP6_HDR_TRUNC,
    DECODE_IP4_MIN_TTL,
    DECODE_IP6_ZERO_HOP_LIMIT,
    DECODE_IP4_DF_OFFSET,
    DECODE_ICMP6_TYPE_OTHER,
    DECODE_ICMP6_DST_MULTICAST,
    DECODE_TCP_SHAFT_SYNFLOOD,
    DECODE_ICMP_PING_NMAP,
    DECODE_ICMP_ICMPENUM,
    DECODE_ICMP_REDIRECT_HOST,
    DECODE_ICMP_REDIRECT_NET,
    DECODE_ICMP_TRACEROUTE_IPOPTS,
    DECODE_ICMP_SOURCE_QUENCH,
    DECODE_ICMP_BROADSCAN_SMURF_SCANNER,
    DECODE_ICMP_DST_UNREACH_ADMIN_PROHIBITED,
    DECODE_ICMP_DST_UNREACH_DST_HOST_PROHIBITED,
    DECODE_ICMP_DST_UNREACH_DST_NET_PROHIBITED,
    DECODE_IP_OPTION_SET,
    DECODE_UDP_LARGE_PACKET,
    DECODE_TCP_PORT_ZERO,
    DECODE_UDP_PORT_ZERO,
    DECODE_IP_RESERVED_FRAG_BIT,
    DECODE_IP_UNASSIGNED_PROTO,
    DECODE_IP_BAD_PROTO,
    DECODE_ICMP_PATH_MTU_DOS,
    DECODE_ICMP_DOS_ATTEMPT,

    DECODE_INDEX_MAX
};

#define SEC_DEC_ETH_HDR_LEN    14
#define SEC_DEC_VLAN_TAG_HDR_LEN 18
#define SEC_DEC_DF_BIT_MASK    0x4000
#define SEC_DEC_MF_BIT_MASK    0x2000

#define SEC_DEC_IP6_EXTMAX 40

#define SEC_DEC_FRAG_OFFSET_MASK  0x1fff
#define SEC_DEC_MF_DF_BIT_MASK   0x6000

#define SEC_DEC_MIN_UNASSIGNED_IP_PROTO 143

#define SEC_DEC_IP_HEAD_LEN_MASK 0x0f
#define SEC_DEC_HEAD_LEN_FACTOR 4 /* Factor to multiply to get exact len */
#define SEC_DEC_TCP_OPTLENMAX                 40

#define SEC_DEC_TCPOPT_EOL              0   /* End of Option List [RFC793] */
#define SEC_DEC_TCPOLEN_EOL             1   /* Always one byte */

#define SEC_DEC_TCPOPT_NOP              1   /* No-Option [RFC793] */
#define SEC_DEC_TCPOLEN_NOP             1   /* Always one byte */

#define SEC_DEC_TCPOPT_MAXSEG           2   /* Maximum Segment Size [RFC793] */
#define SEC_DEC_TCPOLEN_MAXSEG          4   /* Always 4 bytes */

#define SEC_DEC_TCPOPT_WSCALE           3
#define SEC_DEC_TCPOLEN_WSCALE          3

#define SEC_DEC_TCPOPT_SACKOK           4    /* Experimental [RFC2018]*/
#define SEC_DEC_TCPOLEN_SACKOK          2

#define SEC_DEC_TCPOPT_SACK             5 

#define SEC_DEC_TCPOPT_ECHO             6    /* Echo (obsoleted by option 8)      [RFC1072] */
#define SEC_DEC_TCPOLEN_ECHO            6    /* 6 bytes  */

#define SEC_DEC_TCPOPT_ECHOREPLY        7    /* Echo Reply (obsoleted by option 8)[RFC1072] */
#define SEC_DEC_TCPOLEN_ECHOREPLY       6    /* 6 bytes  */

#define SEC_DEC_TCPOPT_TIMESTAMP        8   /* Timestamp [RFC1323], 10 bytes */
#define SEC_DEC_TCPOLEN_TIMESTAMP       10

#define SEC_DEC_TCPOPT_PARTIAL_PERM     9   /* Partial Order Permitted/ Experimental [RFC1693] */
#define SEC_DEC_TCPOLEN_PARTIAL_PERM    2   /* Partial Order Permitted/ Experimental [RFC1693] */

#define SEC_DEC_TCPOPT_PARTIAL_SVC      10  /*  Partial Order Profile [RFC1693] */
#define SEC_DEC_TCPOLEN_PARTIAL_SVC     3   /*  3 bytes long -- Experimental */

#define SEC_DEC_TCP_OPT_TRUNC -1
#define SEC_DEC_TCP_OPT_BADLEN -2

/* atleast decode T/TCP options... */
#define SEC_DEC_TCPOPT_CC               11  /*  T/TCP Connection count  [RFC1644] */
#define SEC_DEC_TCPOPT_CC_NEW           12  /*  CC.NEW [RFC1644] */
#define SEC_DEC_TCPOPT_CC_ECHO          13  /*  CC.ECHO [RFC1644] */
#define SEC_DEC_TCPOLEN_CC             6  /* page 17 of rfc1644 */
#define SEC_DEC_TCPOLEN_CC_NEW         6  /* page 17 of rfc1644 */
#define SEC_DEC_TCPOLEN_CC_ECHO        6  /* page 17 of rfc1644 */
#define SEC_DEC_TCPOPT_ALTCSUM          15  /* TCP Alternate Checksum Data [RFC1146], variable length */
#define SEC_DEC_TCPOPT_SKEETER          16  /* Skeeter [Knowles] */
#define SEC_DEC_TCPOPT_BUBBA            17  /* Bubba   [Knowles] */

#define SEC_DEC_TCPOPT_TRAILER_CSUM     18  /* Trailer Checksum Option [Subbu & Monroe] */
#define SEC_DEC_TCPOLEN_TRAILER_CSUM  3

#define SEC_DEC_TCPOPT_MD5SIG           19  /* MD5 Signature Option [RFC2385] */
#define SEC_DEC_TCPOLEN_MD5SIG        18

#define SEC_DEC_TCPOPT_SCPS             20  /* Capabilities [Scott] */
#define SEC_DEC_TCPOPT_SELNEGACK        21  /* Selective Negative Acknowledgements [Scott] */
#define SEC_DEC_TCPOPT_RECORDBOUND         22  /* Record Boundaries [Scott] */
#define SEC_DEC_TCPOPT_CORRUPTION          23  /* Corruption experienced [Scott] */

#define SEC_DEC_TCPOPT_SNAP                24  /* SNAP [Sukonnik] -- anyone have info?*/
#define SEC_DEC_TCPOPT_UNASSIGNED          25

#define SEC_DEC_IP_HEADER_LEN           20
#define SEC_DEC_ICMP_HEADER_LEN         8
#define SEC_DEC_IP6_HDR_LEN             40
#define SEC_DEC_UDP_HEADER_LENGTH       8
#define SEC_DEC_TCP_HEADER_LEN          20
#define SEC_DEC_IPV6_FRAG_HDR_LEN       8

#define SEC_DEC_ICMP_ECHOREPLY          0    /* Echo Reply                   */
#define SEC_DEC_ICMP_DEST_UNREACH       3    /* Destination Unreachable      */
#define SEC_DEC_ICMP_SOURCE_QUENCH      4    /* Source Quench                */
#define SEC_DEC_ICMP_REDIRECT           5    /* Redirect (change route)      */
#define SEC_DEC_ICMP_ECHO               8    /* Echo Request                 */
#define SEC_DEC_ICMP_ROUTER_ADVERTISE   9    /* Router Advertisement         */
#define SEC_DEC_ICMP_ROUTER_SOLICIT     10    /* Router Solicitation          */
#define SEC_DEC_ICMP_TIME_EXCEEDED      11    /* Time Exceeded                */
#define SEC_DEC_ICMP_PARAMETERPROB      12    /* Parameter Problem            */
#define SEC_DEC_ICMP_TIMESTAMP          13    /* Timestamp Request            */
#define SEC_DEC_ICMP_TIMESTAMPREPLY     14    /* Timestamp Reply              */
#define SEC_DEC_ICMP_INFO_REQUEST       15    /* Information Request          */
#define SEC_DEC_ICMP_INFO_REPLY         16    /* Information Reply            */
#define SEC_DEC_ICMP_ADDRESS            17    /* Address Mask Request         */
#define SEC_DEC_ICMP_ADDRESSREPLY       18    /* Address Mask Reply           */

#define SEC_DEC_ICMP_REDIR_NET          0
#define SEC_DEC_ICMP_REDIR_HOST         1
#define SEC_DEC_ICMP_REDIR_TOS_NET      2
#define SEC_DEC_ICMP_REDIR_TOS_HOST     3

/* IPv6 macros definitions */
#define SEC_DEC_ICMP6_MIN_HEADER_LEN 4 /* type - 1, code - 1, cksum -2 */

/* Macros are taken with values from SNORT to identify the corresponding attack */
#define     DECODE_IPV6_IS_NOT                    271
#define     DECODE_IPV6_TRUNCATED_EXT             272
#define     DECODE_IPV6_TRUNCATED                 273
#define     DECODE_IPV6_BAD_OPT_TYPE              279
#define     DECODE_IPV6_ROUTE_AND_HOPBYHOP        282
#define     DECODE_IPV6_TWO_ROUTE_HEADERS         283
#define     DECODE_IPV6_DSTOPTS_WITH_ROUTING      292
#define     SEC_DEC_IPV6_BAD_OPT_LEN              295
#define     SEC_DEC_IPV6_BAD_OPT_TYPE             276


#define     DECODE_TCP_INVALID_OFFSET             46
#define     DECODE_TCPOPT_BADLEN                  54
#define     DECODE_TCPOPT_TRUNCATED               55
#define     DECODE_TCPOPT_TTCP                    56
#define     DECODE_TCPOPT_OBSOLETE                57
#define     DECODE_TCPOPT_EXPERIMENT              58
#define     DECODE_TCPOPT_WSCALE_INVALID          59


#define     DECODE_ICMP_DGRAM_LT_ICMPHDR          105
#define     DECODE_ICMP_DGRAM_LT_TIMESTAMPHDR     106
#define     DECODE_ICMP_DGRAM_LT_ADDRHDR          107

#define   SEC_DEC_IP_PROTO_NOT_SUPPORTED 1111

#define     DECODE_ICMP_ORIG_IP_TRUNCATED         250
#define     DECODE_ICMP_ORIG_IP_VER_MISMATCH      251
#define     DECODE_ICMP_ORIG_DGRAM_LT_ORIG_IP     252
#define     DECODE_ICMP_ORIG_PAYLOAD_LT_64        253
#define     DECODE_ICMP_ORIG_PAYLOAD_GT_576       254
#define     DECODE_ICMP_ORIG_IP_WITH_FRAGOFFSET   255

#define     DECODE_IPV6_MIN_TTL                   270
#define     DECODE_IPV6_IS_NOT                    271
#define     DECODE_IPV6_TRUNCATED_EXT             272
#define     DECODE_IPV6_TRUNCATED                 273
#define     DECODE_IPV6_DGRAM_LT_IPHDR            274
#define     DECODE_IPV6_DGRAM_GT_CAPLEN           275                                               #define     DECODE_IPV6_DST_ZERO                  276                                               #define     DECODE_IPV6_SRC_MULTICAST             277
#define     DECODE_IPV6_DST_RESERVED_MULTICAST    278                                               #define     DECODE_IPV6_BAD_OPT_TYPE              279
#define     DECODE_IPV6_BAD_MULTICAST_SCOPE       280
#define     DECODE_IPV6_BAD_NEXT_HEADER           281
#define     DECODE_IPV6_ROUTE_AND_HOPBYHOP        282
#define     DECODE_IPV6_TWO_ROUTE_HEADERS         283
#define     DECODE_IPV6_UNORDERED_EXTENSIONS      296

#define SEC_DEC_IP6_OPT_PAD1        0x00    /* 00 0 00000 */
#define SEC_DEC_IP6_OPT_PADN        0x01    /* 00 0 00001 */
#define SEC_DEC_IP6_OPT_JUMBO       0xC2    /* 11 0 00010 = 194 */
#define SEC_DEC_IP6_OPT_JUMBO_LEN   6
#define SEC_DEC_IP6_OPT_RTALERT     0x05    /* 00 0 00101 */
 /*
 * IPv6 Options (not Extension Headers)
 */
#define SEC_DEC_IP6_OPT_TUNNEL_ENCAP    0x04
#define SEC_DEC_IP6_OPT_QUICK_START     0x06
#define SEC_DEC_IP6_OPT_CALIPSO         0x07
#define SEC_DEC_IP6_OPT_HOME_ADDRESS    0xC9
#define SEC_DEC_IP6_OPT_ENDPOINT_IDENT  0x8A


#define     DECODE_ICMPV6_TOO_BIG_BAD_MTU         285
#define     DECODE_ICMPV6_UNREACHABLE_BAD_CODE    286
#define     DECODE_ICMPV6_SOLICITATION_BAD_CODE   287
#define     DECODE_ICMPV6_ADVERT_BAD_CODE         288
#define     DECODE_ICMPV6_SOLICITATION_BAD_RESERVED     289
#define     DECODE_ICMPV6_ADVERT_BAD_REACHABLE    290

#define     SEC_DEC_ICMP6_UNREACH 1
#define     SEC_DEC_ICMP6_BIG    2
#define     SEC_DEC_ICMP6_TIME   3
#define     SEC_DEC_ICMP6_PARAMS 4
#define     SEC_DEC_ICMP6_ECHO   128
#define     SEC_DEC_ICMP6_REPLY  129
#define     SEC_DEC_ICMP6_SOLICITATION 133
#define     SEC_DEC_ICMP6_ADVERTISEMENT 134  
#define     UDP_DGRAM_LT_UDPHDR                    95
#define     DECODE_UDP_DGRAM_SHORT_PACKET          97
#define     DECODE_UDP_DGRAM_LONG_PACKET           98

#define SEC_DEC_TH_FIN  0x01
#define SEC_DEC_TH_SYN  0x02
#define SEC_DEC_TH_RST  0x04
#define SEC_DEC_TH_PUSH 0x08
#define SEC_DEC_TH_ACK  0x10
#define SEC_DEC_TH_URG  0x20
#define SEC_DEC_TH_ECE  0x40
#define SEC_DEC_TH_CWR  0x80
#define SEC_DEC_TH_NORESERVED (SEC_DEC_TH_FIN | SEC_DEC_TH_SYN | SEC_DEC_TH_RST | SEC_DEC_TH_PUSH | SEC_DEC_TH_ACK | SEC_DEC_TH_URG)

#define TCP_ISFLAGSET(TcpHdr, flags) (((TcpHdr)->u1Flags & (flags)) == (flags))

#ifndef NTOHL
#define NTOHL ntohl
#endif

typedef struct IPv4Hdr
{
    UINT1 u1IpVerHl;      /* version & header length */
    UINT1 u1Tos;        /* type of service */
    UINT2 u2Len;       /* datagram length */
    UINT2 u2Id;        /* identification  */
    UINT2 u2FragOff;       /* fragment offset */
    UINT1 u1Ttl;        /* time to live field */
    UINT1 u1Proto;      /* datagram protocol */
    UINT2 u2Cksum;      /* checksum */
    UINT4 u4SrcIp;          /* source IP */
    UINT4 u4DestIp;          /* dest IP */
    UINT4 u4Options;
} tSecDecIP4Hdr;

typedef struct _ICMPHdr
{
    UINT1 u1Type;
    UINT1 u1Code;
    UINT2 u2Cksum;
    union
    {
        struct
        {
            UINT1 u1pptr;
            UINT1 u1pres1;
            UINT2 u2pres2;
        } param;

        struct in_addr gwaddr;

        struct idseq
        {
            UINT2 u2Id;
            UINT2 u2Seq;
        } idseq;

        UINT4 u4Sih_void;

        struct pmtu
        {
            UINT2 u2Ipm_void;
            UINT2 u2Nextmtu;
        } pmtu;

        struct rtradv
        {
            UINT1 u1Num_addrs;
            UINT1 u1Wpa;
            UINT2 u2Lifetime;
        } rtradv;
    } icmp_hun;

    union
    {
        /* timestamp */
        struct ts
        {
            UINT4 u4Otime;
            UINT4 u4Rtime;
            UINT4 u4Ttime;
        } ts;

        /* IP header for unreach */
        struct ih_ip
        {
            tSecDecIP4Hdr *Ip;
            /* options and then 64 bits of data */
        } ip;

        struct ra_addr
        {
            UINT4 u4Addr;
            UINT4 u4Preference;
        } radv;

        UINT4 u4Mask;

        CHR1   u1Data[1];

    } icmp_dun;

}tSecDecICMPHdr;

typedef struct SecDecUDPHdr
{
    UINT2 u2SrcPort;
    UINT2 u2DestPort;
    UINT2 u2UdpLen;
    UINT2 u2UdpCkSum;
}tSecDecUdpHdr;

typedef struct TcpHdr
{
    UINT2    u2Sport;     /* source port */
    UINT2    u2Dport;     /* destination port */
    UINT4    u4Seq;       /* sequence number */
    UINT4    u4Ack;       /* acknowledgement number */
    UINT1    u1Offset;      /* offset and reserved */
    UINT1    u1Flags;
    UINT2    u2Win;       /* window */
    UINT2    u2Sum;       /* checksum */
    UINT2    u2Urp;       /* urgent pointer */

}tSecDecTcpHdr;

typedef struct TcpOptions
{
    UINT1  u1Code;
    UINT1  u1Len; /* length of the data section */
    CONST UINT1 *pu1Data;
} tSecDecTcpOptions;

/* IPv6 structures */
/* IPv6 Address definition */
typedef struct IP6_ADDRESS
{
    union
    {
        UINT1  u1ByteAddr[16];
        UINT2  u2ShortAddr[8];
        UINT4  u4WordAddr[4];
    }
    ip6_addr_u;

#define  u4_addr  ip6_addr_u.u4WordAddr
#define  u2_addr  ip6_addr_u.u2ShortAddr
#define  u1_addr  ip6_addr_u.u1ByteAddr

} tSecDecIp6Addr;

/* Standard IPv6 header */
typedef struct IPv6Hdr
{
    UINT4    u4VerTcFl;      /* version, class, and label */
    UINT2    u2PayloadLen;      /* length of the payload */
    UINT1    u1Next;     /* next header
                         * Uses the same flags as
                         * the IPv4 protocol field */
    UINT1    u1HopLimit;  /* hop limit */
    tSecDecIp6Addr SrcIp;
    tSecDecIp6Addr DestIp;
}tSecDecIP6Hdr;

/* Generic Extension Header */
typedef struct IPv6ExtHdr
{
    UINT1  u1NextHdr;
    UINT1  u1Length;
    UINT1  au1Pad[6];
}tSecDecIpv6ExtHdr;

typedef struct ICMP6Hdr
{
    UINT1 u1Type;
    UINT1 u1Code;
    UINT2 u2CkSum;
}tSecDecIcmp6Hdr;

typedef struct ICMP6TooBig
{
    UINT1 u1Type;
    UINT1 u1Code;
    UINT2 u2CkSum;
    UINT4 u4Mtu;
} tSecDecIcmp6TooBig;
               
typedef struct ICMP6RouterAdvertisement
{          
    UINT1 u1Type;
    UINT1 u1Code;
    UINT2 u2CkSum;
    UINT1 u1NumAddrs;
    UINT1 u1AddrEntrySize;
    UINT2 u2Lifetime;
    UINT4 u4ReachTime;
    UINT4 u4RetransTime;
}tSecDecIcmp6RtrAdvt;

typedef struct ICMP6RouterSolicitation
{
    UINT1 u1Type;
    UINT1 u1Code;
    UINT2 u2CkSum;
    UINT4 u4Reserved;
}tSecDecIcmp6RtrSolicit;


/* Prototype Declaration */

INT4 SecDecodeEthPkt PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4DataLen));
INT4 SecDecodeIpv4Pkt PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4Offset, 
                              UINT4 *pu4DataLen));
INT4 SecDecodeIcmpPkt PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4Offset, 
                              UINT4 u4TotLen, UINT4 *pu4DataLen));
INT4 SecDecodeIpv6Pkt PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4Offset, UINT4 *pu4DataLen));
INT4 SecDecLogMessage PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4AttackId));
INT4 SecDecodeVlanPkt PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, 
                              UINT4 u4Offset, UINT4 *pu4DataLen));
INT4 SecDecodePppoePkt PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, 
                               UINT4 u4Offset, UINT4 *pu4DataLen));
INT4 SecDecodeTcpPkt PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4Offset, 
                             UINT4 u4TotLen, UINT4 *pu4DataLen));
INT4 SecDecodeUdpPkt PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4Offset, 
                             UINT4 u4TotLen, UINT4 *pu4DataLen));
INT4 SecDecodeTcpOption PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, const UINT1 *pStart,
                                UINT2 u2OptionLen));
INT4 SecDecOptLenValidate PROTO ((const UINT1 *pOption, const UINT1 *pEnd, const UINT1 *pLen,
                   UINT4 u4ExpectedLen, tSecDecTcpOptions *pTcpOpt, UINT4 *pByteSkip));

INT4 SecDecodeIpv6Options PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT2 u2PayloadLen,
                                  UINT4 u4Offset, UINT4 *pu4DataLen, UINT1 u1NextHdr));
INT4 SecDecodeIcmpv6Pkt PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                         UINT4 u4Offset,
                         UINT2 u2PayLoadLen,
                         UINT4 *pu4DataLen));
INT4 SecDecCheckIpv6HopOptions PROTO ((UINT1 *pPktBuf, UINT4 *pu4ErrCode));

INT4 SecDecIPv6ExtensionOrder PROTO ((UINT1 u1Type));
#endif
