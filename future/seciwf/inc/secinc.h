/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: secinc.h,v 1.4 2014/08/14 10:55:23 siva Exp $
 *
 * Description:This file contains the Required basic includes
 *             used accross  Security modules 
 *******************************************************************/

#ifndef _SECINC_H_
#define _SECINC_H_
#include "lr.h"
#include "fsbuddy.h"
#include "fssyslog.h"
#include "srmbuf.h"
#include "nat.h"
#include "cfa.h"
#include "arp.h"
#include "vcm.h"
#include "secgen.h"
#include "ip.h"
#include "osix.h"
#include "vpn.h"
#include "pppoe.h"
#include "sec.h" 
#include "secmod.h"
#include "arsec.h"
#include "secv4.h"
#include "secv6.h"
#include "ipvx.h"
#include "ppp.h"
#include "pppoe.h"
#include "ids.h"
#include "secids.h"
#include "secproto.h"
#include "firewall.h"
#include "secextn.h"
#ifndef SECURITY_KERNEL_WANTED
#include <sys/ioctl.h>
#else
#include <linux/ioctl.h>
#endif
#include "secproto.h"
#include "fssecwr.h"
#include "fsseclw.h"
#include "secsz.h"
#include "secmem.h"
#include "fsutil.h"
#endif /* _SECINC_H_ */
