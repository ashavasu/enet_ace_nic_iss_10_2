/*****************************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: secstubs.c,v 1.2 2013/02/14 14:00:05 siva Exp $ 
 *
 * Description: This file contains the stub routines for kernel space compilation
 *****************************************************************************/
#ifndef _SECSTUBS_C_
#define _SECSTUBS_C_

#include "secinc.h"
#include "sectrc.h"

/*****************************************************************************
 *
 *    Function Name       : SeckInit
 *
 *    Description         : This function is Used to Initialise Security
 *                               Module in the kernel
 *
 *                               Registers with the character device              
 *                               Adds the callback function with device add pack 
 *
 *
 *    Input(s)            : None
 *
 *    Output(s)           : NONE.
 *
 *    Global Variables Referred : None.                                         
 *                                                                           
 *    Global Variables Modified : None.                                         
 *                                                                           
 *    Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
INT4
SeckInit (VOID)
{
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SeckDeInit                                       */
/*                                                                           */
/*    Description         : Security Module DeInit Function in the kernel.   */
/*                          This function release all the resources of the   */
/*                          security module.                                 */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
SeckDeInit (VOID)
{
    return;
}

#ifdef SECURITY_KERNEL_MAKE_WANTED
/************************************************************************/
/*  Function Name   : Secv4Initialize                                   */
/*  Description     : This function Initializes Lists and Arrays used   */
/*                  : used for Configuration and Statistics purpose     */
/*                  :                                                   */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : SEC_SUCCESS or SEC_FAILURE                        */
/************************************************************************/
INT1
Secv4Initialize (VOID)
{
    lrInitComplete (OSIX_SUCCESS);
    return SEC_SUCCESS;
}

/***********************************************************************/
/*  Function Name : Secv4InProcess                                     */
/*  Description   : This function gets the incomming packet            */
/*                : from Driver and converts it into linear buffer and */
/*                : invokes the respective function of IPSEC           */
/*                :                                                    */
/*  Input(s)      : pCBuf - Buffer contains IP Secured packet.          */
/*                : u4Port - Interface on which the Packet arrives.    */
/*                :                                                    */
/*  Output(s)     : pCBuf - Buffer contains Decoded IP packet.          */
/*                :                                                    */
/*  Return Values : None                                               */
/***********************************************************************/

UINT1
Secv4InProcess (tIP_BUF_CHAIN_HEADER ** ppCBuf, UINT4 u4Port)
{
    UNUSED_PARAM (ppCBuf);
    UNUSED_PARAM (u4Port);
    return SEC_SUCCESS;
}

/************************************************************************/
/*  Function Name : Secv4OutProcess                                     */
/*  Description   : This function gets the outgoing packet              */
/*                : from Driver and converts it into linear buffer and  */
/*                : passes to corresponding function in IPSEC           */
/*                :                                                     */
/*  Input(s)      : pBuf - Buffer contains IP packet.                   */
/*                : u4Port - The interface on which the packet arrived. */
/*                :                                                     */
/*  Output(s)     : pBuf - Buffer contains IP Secured packet.           */
/*                :                                                     */
/*  Return Values : SEC_SUCCESS or SEC_FAILURE                          */
/************************************************************************/

UINT1
Secv4OutProcess (tIP_BUF_CHAIN_HEADER * pCBuf, UINT4 u4Port)
{
    UNUSED_PARAM (pCBuf);
    UNUSED_PARAM (u4Port);
    return SEC_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : FwlAclProcessPktForInFiltering             */
/*                                                                           */
/*    Description               : This routine is called by the external     */
/*                                module (Forwarding or Fast forwarding)     */
/*                                whenever the packet is coming in through   */
/*                                an interface, that to be inspected         */
/*                                against the configured InFilters by the   */
/*                                AccessList.                                */
/*                                                                           */
/*    Input(s)                  : pBuf         -- Pointer to the incoming pkt*/
/*                                u4InIfaceNum -- Incoming interface number  */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : SUCCESS if the packet is to permitted,     */
/*                                otherwise FAILURE.                         */
/*****************************************************************************/
UINT4
FwlAclProcessPktForInFiltering (tCRU_BUF_CHAIN_HEADER * pBuf,
                                UINT4 u4InIfaceNum)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4InIfaceNum);
    return FWL_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : FwlAclProcessPktForOutFiltering            */
/*                                                                           */
/*    Description               : This routine is called by the external     */
/*                                module (Forwarding or Fast forwarding)     */
/*                                whenever the packet is going out through   */
/*                                an interface, that to be inspected         */
/*                                against the configured OutFilters by the   */
/*                                AccessList.                                */
/*                                                                           */
/*    Input(s)                  : pBuf         -- Pointer to the outgoing pkt*/
/*                                u4OutIfaceNum -- Outgoing interface number */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : SUCCESS if the packet is to permitted,     */
/*                                otherwise FAILURE.                         */
/*****************************************************************************/
UINT4
FwlAclProcessPktForOutFiltering (tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT4 u4OutIfaceNum)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4OutIfaceNum);
    return FWL_SUCCESS;
}

 /***************************************************************************
  * Function Name    :  SecUtilGetGlobalStatus
  * Description      :  This function returns the global IPSEC status
  *
  * Input (s)        :  None.
  *
  * Output (s)       :  None.
  * Returns          :  SEC_ENABLE/SEC_DISABLE.
  ***************************************************************************/
INT1
SecUtilGetGlobalStatus (VOID)
{
    return 0;
}

#endif
#ifndef IDS_WANTED
INT4
SecIdsGetInfo (UINT4 u4MsgType, tIdsGetInfo * pIdsGetInfo)
{
    UNUSED_PARAM (u4MsgType);
    UNUSED_PARAM (pIdsGetInfo);
    return IDS_SUCCESS;
}

INT4
SecIdsSetInfo (UINT4 u4MsgType, UINT4 u4Value)
{
    UNUSED_PARAM (u4MsgType);
    UNUSED_PARAM (u4Value);
    return IDS_FAILURE;
}

VOID
SecIdsCheckForIdsFiles (UINT1 *pu1FileName)
{
    UNUSED_PARAM (pu1FileName);
}
#endif
#endif
