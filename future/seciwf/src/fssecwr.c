/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssecwr.c,v 1.2 2011/09/24 10:03:22 siva Exp $
*
* Description: File containing mid level routines  
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsseclw.h"
# include  "fssecwr.h"
# include  "fssecdb.h"

VOID
RegisterFSSEC ()
{
    SNMPRegisterMib (&fssecOID, &fssecEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fssecOID, (const UINT1 *) "fssec");
}

VOID
UnRegisterFSSEC ()
{
    SNMPUnRegisterMib (&fssecOID, &fssecEntry);
    SNMPDelSysorEntry (&fssecOID, (const UINT1 *) "fssec");
}

INT4
FsSecDebugOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSecDebugOption (&(pMultiData->i4_SLongValue)));
}

INT4
FsSecDebugOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSecDebugOption (pMultiData->i4_SLongValue));
}

INT4
FsSecDebugOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSecDebugOption (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSecDebugOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSecDebugOption (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
