/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved                      */
/* License Aricent Inc., 2001-2010                                           */
/*                                                                           */
/* $Id: secdec.c,v 1.2 2012/02/15 12:23:37 siva Exp $                        */
/*                                                                           */
/*  FILE NAME             : secdec.c                                         */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : Signature Based IDS                              */
/*  MODULE NAME           : IDS Decoder module                               */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 31 Mar 2011                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains IDS decoding related routines */
/*****************************************************************************/

#include "secdec.h"

/*************************************************************************/
/* Function Name      : SecDecodeEthPkt                                  */
/* Description        : Decodes the ethernet packet                      */
/* Input(s)           : pBuf         -- Pointer to the packet            */
/* Output(s)          : None.                                            */
/* Returns            : OSIX_SUCCESS if packet is to permited, otherwise */
/*                      OSIX_FAILURE.                                    */
/*************************************************************************/

INT4
SecDecodeEthPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4DataLen)
{
    tSecModuleData     *pSecModuleData = NULL;
    UINT2               u2PID = 0;

    /* Get the Module Data from the CRU_BUFFER */

    if (NULL ==
        (pSecModuleData = (tSecModuleData *) CRU_BUF_Get_ModuleData (pBuf)))
    {
        return OSIX_FAILURE;
    }

    u2PID = pSecModuleData->u2LenOrType;

    switch (u2PID)
    {
        case CFA_PPPOE_SESSION:
            return (SecDecodePppoePkt (pBuf, SEC_DEC_ETH_HDR_LEN, pu4DataLen));
            break;

        case CFA_ENET_IPV4:
            return (SecDecodeIpv4Pkt (pBuf, SEC_DEC_ETH_HDR_LEN, pu4DataLen));
            break;

        case CFA_ENET_IPV6:
            return (SecDecodeIpv6Pkt (pBuf, SEC_DEC_ETH_HDR_LEN, pu4DataLen));
            break;

        case CFA_VLAN_PROTOCOL_ID:
            return (SecDecodeVlanPkt (pBuf, SEC_DEC_ETH_HDR_LEN, pu4DataLen));
            break;

        default:
            return OSIX_FAILURE;
    }
}

/*************************************************************************/
/* Function Name      : SecDecodeIpv4Pkt                                 */
/* Description        : Decodes the IP version 4 packet                  */
/* Input(s)           : pBuf     -- Pointer to the packet                */
/*                      u4Offset -- offset value to IP header in packet  */
/* Output(s)          : None.                                            */
/* Returns            : OSIX_SUCCESS if packet is to permited, otherwise */
/*                      OSIX_FAILURE.                                    */
/*************************************************************************/
INT4
SecDecodeIpv4Pkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Offset,
                  UINT4 *pu4DataLen)
{
    tSecDecIP4Hdr      *pIpHdr;
    UINT2               u2FragOff = 0;
    UINT1               u1IpHdrLen = 0;
    INT4                i4Retval = 0;
    UINT2               u2TotLen = 0;

    pIpHdr = (tSecDecIP4Hdr *) (pBuf->pSkb->data + u4Offset);

    /* MIN TTL - Snort has configurable value */
    if (pIpHdr->u1Ttl == 0)
    {
        SecDecLogMessage (pBuf, DECODE_ZERO_TTL);
        return OSIX_FAILURE;
    }

    /* Check both DF & Frag offset is set */
    u2FragOff = ntohs (pIpHdr->u2FragOff);

    if (((u2FragOff & SEC_DEC_DF_BIT_MASK) != 0) &&
        ((u2FragOff & SEC_DEC_FRAG_OFFSET_MASK) != 0))
    {
        SecDecLogMessage (pBuf, DECODE_IP4_DF_OFFSET);
        return OSIX_FAILURE;
    }

    /* Reserved IP FRAG bit set */
    if ((u2FragOff & SEC_DEC_MF_DF_BIT_MASK) == SEC_DEC_MF_DF_BIT_MASK)
    {
        SecDecLogMessage (pBuf, DECODE_BAD_FRAGBITS);
        return OSIX_FAILURE;
    }
    /*Unassigned Protocol */

    if ((u2FragOff & SEC_DEC_MF_BIT_MASK) == 0)
    {
        u1IpHdrLen = OSIX_NTOHL ((pIpHdr->
                                  u1IpVerHl & SEC_DEC_IP_HEAD_LEN_MASK) *
                                 SEC_DEC_HEAD_LEN_FACTOR);

        *pu4DataLen = *pu4DataLen + u1IpHdrLen;

        u2TotLen = OSIX_NTOHS (pIpHdr->u2Len);
        switch (pIpHdr->u1Proto)
        {
            case IPPROTO_TCP:
                i4Retval = SecDecodeTcpPkt (pBuf, (u4Offset + u1IpHdrLen),
                                            (u2TotLen - u1IpHdrLen),
                                            pu4DataLen);
                break;

            case IPPROTO_UDP:
                i4Retval = SecDecodeUdpPkt (pBuf,
                                            (u4Offset + u1IpHdrLen),
                                            (u2TotLen - u1IpHdrLen),
                                            pu4DataLen);
                break;

            case IPPROTO_ICMP:
                i4Retval = SecDecodeIcmpPkt (pBuf, (u4Offset + u1IpHdrLen),
                                             u2TotLen, pu4DataLen);
                break;

            default:
                if (pIpHdr->u1Proto >= SEC_DEC_MIN_UNASSIGNED_IP_PROTO)
                {
                    SecDecLogMessage (pBuf, SEC_DEC_MIN_UNASSIGNED_IP_PROTO);
                    return OSIX_FAILURE;
                }
                SecDecLogMessage (pBuf, SEC_DEC_IP_PROTO_NOT_SUPPORTED);
                return OSIX_FAILURE;
        }
        return i4Retval;
    }

    return OSIX_SUCCESS;
}

/*************************************************************************/
/* Function Name      : SecDecodeIcmpPkt                                 */
/* Description        : Decodes the ICMP version 4 packet                */
/* Input(s)           : pBuf     -- Pointer to the packet                */
/*                      u4Offset -- offset value to IP header in packet  */
/*                      u4TotLen -- total length of IP datagram          */
/* Output(s)          : None.                                            */
/* Returns            : OSIX_SUCCESS if packet is to permited, otherwise */
/*                      OSIX_FAILURE.                                    */
/*************************************************************************/
INT4
SecDecodeIcmpPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Offset,
                  UINT4 u4TotLen, UINT4 *pu4DataLen)
{
    tSecDecICMPHdr     *pIcmpHdr = NULL;
    tSecDecIP4Hdr      *pOrigIpHdr = NULL;
    UINT4               u4DataSize = 0;

    pIcmpHdr = (tSecDecICMPHdr *) (pBuf->pSkb->data + u4Offset);

    *pu4DataLen = *pu4DataLen + SEC_DEC_ICMP_HEADER_LEN;

    switch (pIcmpHdr->u1Type)
    {
        case SEC_DEC_ICMP_SOURCE_QUENCH:
        case SEC_DEC_ICMP_DEST_UNREACH:
        case SEC_DEC_ICMP_REDIRECT:
        case SEC_DEC_ICMP_TIME_EXCEEDED:
        case SEC_DEC_ICMP_PARAMETERPROB:
        case SEC_DEC_ICMP_ECHOREPLY:
        case SEC_DEC_ICMP_ECHO:
        case SEC_DEC_ICMP_ROUTER_ADVERTISE:
        case SEC_DEC_ICMP_ROUTER_SOLICIT:
        case SEC_DEC_ICMP_INFO_REQUEST:
        case SEC_DEC_ICMP_INFO_REPLY:
            if (u4TotLen < 8)
            {
                SecDecLogMessage (pBuf, DECODE_ICMP_DGRAM_LT_ICMPHDR);
                return OSIX_FAILURE;
            }
            break;
        case SEC_DEC_ICMP_TIMESTAMP:
        case SEC_DEC_ICMP_TIMESTAMPREPLY:
            if (u4TotLen < 20)
            {
                SecDecLogMessage (pBuf, DECODE_ICMP_DGRAM_LT_TIMESTAMPHDR);
                return OSIX_FAILURE;
            }
            break;
        case SEC_DEC_ICMP_ADDRESS:
        case SEC_DEC_ICMP_ADDRESSREPLY:
            if (u4TotLen < 12)
            {
                SecDecLogMessage (pBuf, DECODE_ICMP_DGRAM_LT_ADDRHDR);
                return OSIX_FAILURE;
            }
            break;
        default:
            SecDecLogMessage (pBuf, DECODE_ICMP_DGRAM_LT_ADDRHDR);
            return OSIX_FAILURE;
    }

    u4DataSize = u4TotLen - SEC_DEC_ICMP_HEADER_LEN;

    switch (pIcmpHdr->u1Type)
    {
        case SEC_DEC_ICMP_DEST_UNREACH:
            if ((pIcmpHdr->u1Code == ICMP_FRAG_NEEDED)
                && (ntohs (pIcmpHdr->icmp_hun.pmtu.u2Nextmtu) < 576))
            {
                SecDecLogMessage (pBuf, DECODE_ICMP_PATH_MTU_DOS);
                return OSIX_FAILURE;
            }
            break;
        case SEC_DEC_ICMP_SOURCE_QUENCH:
        case SEC_DEC_ICMP_REDIRECT:
        case SEC_DEC_ICMP_TIME_EXCEEDED:
        case SEC_DEC_ICMP_PARAMETERPROB:
            /* TODO Need to check on the embedded IP header */
            /* account for extra 4 bytes in header */
            u4DataSize = u4DataSize - 4;
            pOrigIpHdr = (tSecDecIP4Hdr *) (pIcmpHdr + u4DataSize);

            if (u4DataSize < SEC_DEC_IP_HEADER_LEN)
            {
                SecDecLogMessage (pBuf, DECODE_ICMP_ORIG_IP_TRUNCATED);
                return OSIX_FAILURE;
            }
            if (((pOrigIpHdr->u1IpVerHl) >> 4) != 4)
            {
                SecDecLogMessage (pBuf, DECODE_ICMP_ORIG_IP_VER_MISMATCH);
                return OSIX_FAILURE;
            }

            if (u4DataSize < ((pOrigIpHdr->u1IpVerHl) & 0x0f) << 2)
            {
                SecDecLogMessage (pBuf, DECODE_ICMP_ORIG_DGRAM_LT_ORIG_IP);
                return OSIX_FAILURE;
            }

            break;
        default:
            break;
    }

    if ((u4DataSize == 0) && (pIcmpHdr->u1Type == SEC_DEC_ICMP_ECHO))
    {
        SecDecLogMessage (pBuf, DECODE_ICMP_PING_NMAP);
        return OSIX_FAILURE;
    }

    if ((u4DataSize == 0) && (pIcmpHdr->icmp_hun.idseq.u2Seq == 666))
    {
        SecDecLogMessage (pBuf, DECODE_ICMP_ICMPENUM);
        return OSIX_FAILURE;
    }

    if ((pIcmpHdr->u1Code == 1) && (pIcmpHdr->u1Type == SEC_DEC_ICMP_REDIRECT))
    {
        SecDecLogMessage (pBuf, DECODE_ICMP_REDIRECT_HOST);
        return OSIX_FAILURE;
    }
    if ((pIcmpHdr->u1Type == SEC_DEC_ICMP_REDIRECT) && (pIcmpHdr->u1Code == 0))
    {
        SecDecLogMessage (pBuf, DECODE_ICMP_REDIRECT_NET);
        return OSIX_FAILURE;
    }
    if ((pIcmpHdr->u1Type == SEC_DEC_ICMP_SOURCE_QUENCH) &&
        (pIcmpHdr->u1Code == 0))
    {
        SecDecLogMessage (pBuf, DECODE_ICMP_SOURCE_QUENCH);
        return OSIX_FAILURE;
    }

    if ((u4DataSize == 4) &&
        (pIcmpHdr->u1Type == SEC_DEC_ICMP_ECHO) &&
        (pIcmpHdr->icmp_hun.idseq.u2Seq == 0) && (pIcmpHdr->u1Code == 0))
    {
        SecDecLogMessage (pBuf, DECODE_ICMP_BROADSCAN_SMURF_SCANNER);
        return OSIX_FAILURE;
    }
    if ((pIcmpHdr->u1Type == SEC_DEC_ICMP_DEST_UNREACH) &&
        (pIcmpHdr->u1Code == 13))
    {
        SecDecLogMessage (pBuf, DECODE_ICMP_DST_UNREACH_ADMIN_PROHIBITED);
        return OSIX_FAILURE;
    }
    if ((pIcmpHdr->u1Type == SEC_DEC_ICMP_DEST_UNREACH) &&
        (pIcmpHdr->u1Code == 10))
    {
        SecDecLogMessage (pBuf, DECODE_ICMP_DST_UNREACH_DST_HOST_PROHIBITED);
        return OSIX_FAILURE;
    }
    if ((pIcmpHdr->u1Type == SEC_DEC_ICMP_DEST_UNREACH) &&
        (pIcmpHdr->u1Code == 9))
    {
        SecDecLogMessage (pBuf, DECODE_ICMP_DST_UNREACH_DST_NET_PROHIBITED);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*************************************************************************/
/* Function Name      : SecDecodeIpv6Pkt                                 */
/* Description        : Decodes the IP version 6 packet                  */
/* Input(s)           : pBuf     -- Pointer to the packet                */
/*                      u4Offset -- offset value to IP header in packet  */
/*                      u4TotLen -- total length of IP datagram          */
/* Output(s)          : None.                                            */
/* Returns            : OSIX_SUCCESS if packet is to permited, otherwise */
/*                      OSIX_FAILURE.                                    */
/*************************************************************************/
INT4
SecDecodeIpv6Pkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Offset,
                  UINT4 *pu4DataLen)
{
    tSecDecIP6Hdr      *pIpv6Hdr = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT2               u2PayloadLen = 0;

    pIpv6Hdr = (tSecDecIP6Hdr *) (pBuf->pSkb->data + u4Offset);

    if ((pBuf->pSkb->len - u4Offset) < SEC_DEC_IP6_HDR_LEN)
    {
        SecDecLogMessage (pBuf, DECODE_IPV6_TRUNCATED);
        return OSIX_FAILURE;
    }

    if (((pIpv6Hdr->u4VerTcFl) >> 28) != 6)
    {
        SecDecLogMessage (pBuf, DECODE_IPV6_IS_NOT);
        return OSIX_FAILURE;
    }

    u2PayloadLen = OSIX_NTOHS (pIpv6Hdr->u2PayloadLen);
    u4Offset = u4Offset + SEC_DEC_IP6_HDR_LEN;

    *pu4DataLen = *pu4DataLen + SEC_DEC_IP6_HDR_LEN;
    switch (pIpv6Hdr->u1Next)
    {
        case IPPROTO_TCP:
            i4RetVal = SecDecodeTcpPkt (pBuf, u4Offset, u2PayloadLen,
                                        pu4DataLen);
            break;
        case IPPROTO_UDP:
            i4RetVal = SecDecodeUdpPkt (pBuf, u4Offset, u2PayloadLen,
                                        pu4DataLen);
            break;
        case IPPROTO_ICMPV6:
            i4RetVal = SecDecodeIcmpv6Pkt (pBuf, u4Offset, u2PayloadLen,
                                           pu4DataLen);
            break;
        case IPPROTO_NONE:
            break;
        case IPPROTO_HOPOPTS:
        case IPPROTO_DSTOPTS:
        case IPPROTO_ROUTING:
        case IPPROTO_FRAGMENT:
        case IPPROTO_AH:
            i4RetVal = SecDecodeIpv6Options (pBuf, u2PayloadLen,
                                             u4Offset, pu4DataLen,
                                             pIpv6Hdr->u1Next);
            break;
        default:
            SecDecLogMessage (pBuf, DECODE_IPV6_BAD_NEXT_HEADER);
            return OSIX_FAILURE;
    }

    return i4RetVal;
}

/*************************************************************************/
/* Function Name      : SecDecodeIpv6Options                             */
/* Description        : Decodes the option values present in IP          */
/*                      version 6 packet header                          */
/* Input(s)           : pBuf     -- Pointer to the packet                */
/*                      u2PayloadLen - Payload Length                    */
/*                      u4Offset -- offset value to IP header in packet  */
/*                      *pu4DataLen -- pointer to data length of IP      */
/*                                     datagram                          */
/*                      u1NextHdr -- Next header in IPv6 packet          */
/* Output(s)          : None.                                            */
/* Returns            : OSIX_SUCCESS if packet is to permited, otherwise */
/*                      OSIX_FAILURE.                                    */
/*************************************************************************/
INT4
SecDecodeIpv6Options (tCRU_BUF_CHAIN_HEADER * pBuf,
                      UINT2 u2PayloadLen,
                      UINT4 u4Offset, UINT4 *pu4DataLen, UINT1 u1NextHdr)
{
    tSecDecIpv6ExtHdr  *pIpv6ExtHdr = NULL;
    UINT1               au1HdrType[SEC_DEC_IP6_EXTMAX];
    INT4                i4RetVal = OSIX_FAILURE;
    UINT4               u4ExtHdrLen = 0;
    UINT4               u4ErrCode = 0;
    UINT1               u1NoMoreExtHdr = OSIX_TRUE;
    UINT1               u1ExtHdrCount = 0;
    UINT1               u1NextType = 0;
    UINT1               u1CurType = 0;
    UINT1               u1RtrHdr = 0;
    UINT1               u1Count = 0;

    if (u2PayloadLen < sizeof (tSecDecIpv6ExtHdr))
    {
        SecDecLogMessage (pBuf, DECODE_IPV6_TRUNCATED_EXT);
        return OSIX_FAILURE;
    }

    while (u4ExtHdrLen < u2PayloadLen)
    {
        switch (u1NextHdr)
        {
            case IPPROTO_HOPOPTS:
                pIpv6ExtHdr =
                    (tSecDecIpv6ExtHdr *) (pBuf->pSkb->data +
                                           u4Offset + u4ExtHdrLen);
                u4ExtHdrLen = (u4ExtHdrLen + sizeof (tSecDecIpv6ExtHdr) +
                               (pIpv6ExtHdr->u1Length >> 3));
                if (OSIX_FAILURE ==
                    SecDecCheckIpv6HopOptions ((UINT1 *)
                                               (pBuf->pSkb->data + u4Offset),
                                               &u4ErrCode))
                {
                    SecDecLogMessage (pBuf, u4ErrCode);
                    return OSIX_FAILURE;
                }
                u1ExtHdrCount++;
                break;
            case IPPROTO_DSTOPTS:
                if (pIpv6ExtHdr->u1NextHdr == IPPROTO_ROUTING)
                {
                    SecDecLogMessage (pBuf, DECODE_IPV6_DSTOPTS_WITH_ROUTING);
                    return OSIX_FAILURE;
                }
                u4ExtHdrLen = (u4ExtHdrLen + sizeof (tSecDecIpv6ExtHdr) +
                               (pIpv6ExtHdr->u1Length >> 3));
                if (OSIX_FAILURE ==
                    SecDecCheckIpv6HopOptions ((UINT1 *)
                                               (pBuf->pSkb->data + u4Offset),
                                               &u4ErrCode))
                {
                    SecDecLogMessage (pBuf, u4ErrCode);
                    return OSIX_FAILURE;
                }
                u1ExtHdrCount++;

                break;

            case IPPROTO_ROUTING:
                if (pIpv6ExtHdr->u1NextHdr == IPPROTO_ROUTING)
                {
                    SecDecLogMessage (pBuf, DECODE_IPV6_TWO_ROUTE_HEADERS);
                    return OSIX_FAILURE;
                }
                else if (pIpv6ExtHdr->u1NextHdr == IPPROTO_HOPOPTS)
                {
                    SecDecLogMessage (pBuf, DECODE_IPV6_ROUTE_AND_HOPBYHOP);
                    return OSIX_FAILURE;
                }
                u4ExtHdrLen = (u4ExtHdrLen + sizeof (tSecDecIpv6ExtHdr) +
                               (pIpv6ExtHdr->u1Length >> 3));
                u1ExtHdrCount++;
                break;

            case IPPROTO_FRAGMENT:
                u4ExtHdrLen = u4ExtHdrLen + SEC_DEC_IPV6_FRAG_HDR_LEN;
                u1ExtHdrCount++;
                u1NoMoreExtHdr = OSIX_TRUE;
                break;
            case IPPROTO_AH:

                u4ExtHdrLen = (u4ExtHdrLen + sizeof (tSecDecIpv6ExtHdr) +
                               (pIpv6ExtHdr->u1Length << 2));
                u1ExtHdrCount++;
                break;

            case IPPROTO_TCP:
                i4RetVal = SecDecodeTcpPkt (pBuf, (u4Offset + u4ExtHdrLen),
                                            (u2PayloadLen - u4ExtHdrLen),
                                            pu4DataLen);
                u1ExtHdrCount++;
                u1NoMoreExtHdr = OSIX_TRUE;
                break;

            case IPPROTO_UDP:
                i4RetVal = SecDecodeUdpPkt (pBuf, (u4Offset + u4ExtHdrLen),
                                            (u2PayloadLen - u4ExtHdrLen),
                                            pu4DataLen);
                u1ExtHdrCount++;
                u1NoMoreExtHdr = OSIX_TRUE;
                break;

            case IPPROTO_ICMPV6:
                i4RetVal = SecDecodeIcmpv6Pkt (pBuf, (u4Offset + u4ExtHdrLen),
                                               (u2PayloadLen - u4ExtHdrLen),
                                               pu4DataLen);
                u1ExtHdrCount++;
                u1NoMoreExtHdr = OSIX_TRUE;
                break;
            case IPPROTO_NONE:
                u1NoMoreExtHdr = OSIX_TRUE;
                break;
        }
        if (u1NoMoreExtHdr == OSIX_TRUE)
        {
            if (u1ExtHdrCount > 0)
            {
                u1CurType = SecDecIPv6ExtensionOrder (au1HdrType[0]);
            }

            for (u1Count = 1; u1Count < (u1ExtHdrCount); u1Count++)
            {
                u1NextType = SecDecIPv6ExtensionOrder (au1HdrType[u1Count]);

                if (au1HdrType[u1Count] == IPPROTO_ROUTING)
                {
                    u1RtrHdr = 1;
                }

                if (u1NextType <= u1CurType)
                {
                    /* A second "Destination Options" header is allowed iff:
                       1) A routing header was already seen, and
                       2) The second destination header is the last one before the upper layer.
                     */
                    if (!u1RtrHdr ||
                        !(au1HdrType[u1Count] == IPPROTO_DSTOPTS) ||
                        !(u1Count + 1 == u1ExtHdrCount))
                    {
                        SecDecLogMessage (pBuf,
                                          DECODE_IPV6_UNORDERED_EXTENSIONS);
                        return OSIX_FAILURE;
                    }
                }
                u1CurType = u1NextType;
            }
            break;
        }

        pIpv6ExtHdr = (tSecDecIpv6ExtHdr *)
            ((pBuf->pSkb->data) + u4Offset + u4ExtHdrLen);
        u1NextHdr = pIpv6ExtHdr->u1NextHdr;
    }
    return i4RetVal;
}

/*************************************************************************/
/* Function Name      : SecDecIPv6ExtensionOrder                         */
/* Description        : Function to check the order of the extension     */
/*                      header present in the IPv6 packet                */
/* Input(s)           : u1Type -- extension header type                  */
/* Output(s)          : None.                                            */
/* Returns            : Order of the extension header                    */
/*************************************************************************/
INT4
SecDecIPv6ExtensionOrder (UINT1 u1Type)
{
    switch (u1Type)
    {
        case IPPROTO_HOPOPTS:
            return 1;
            break;
        case IPPROTO_DSTOPTS:
            return 2;
            break;
        case IPPROTO_ROUTING:
            return 3;
            break;
        case IPPROTO_FRAGMENT:
            return 4;
            break;
        case IPPROTO_AH:
            return 5;
            break;
        case IPPROTO_ESP:
            return 6;
            break;
        default:
            return 7;
            break;
    }
}

/*************************************************************************/
/* Function Name      : SecDecCheckIpv6HopOptions                        */
/* Description        : Function to check the Hop by Hop extension header*/
/*                      present in the IPv6 packet                       */
/* Input(s)           : u1Type -- extension header type                  */
/* Output(s)          : None.                                            */
/* Returns            : Order of the extension header                    */
/*************************************************************************/
INT4
SecDecCheckIpv6HopOptions (UINT1 *pPktBuf, UINT4 *pu4ErrCode)
{
    tSecDecIpv6ExtHdr  *pExtHdr = (tSecDecIpv6ExtHdr *) pPktBuf;
    UINT4               u4TotOctets = (pExtHdr->u1Length * 8) + 8;
    UINT1              *pu1HdrEnd = pPktBuf + u4TotOctets;
    UINT1               u1Type = 0;
    UINT1               u1OpLen = 0;

    /* Skip to the options */
    pPktBuf = pPktBuf + 2;

    /* Iterate through the options, check for bad ones */
    while (pPktBuf < pu1HdrEnd)
    {
        u1Type = *pPktBuf;
        switch (u1Type)
        {
            case SEC_DEC_IP6_OPT_PAD1:
                pPktBuf++;
                break;
            case SEC_DEC_IP6_OPT_PADN:
            case SEC_DEC_IP6_OPT_JUMBO:
            case SEC_DEC_IP6_OPT_RTALERT:
            case SEC_DEC_IP6_OPT_TUNNEL_ENCAP:
            case SEC_DEC_IP6_OPT_QUICK_START:
            case SEC_DEC_IP6_OPT_CALIPSO:
            case SEC_DEC_IP6_OPT_HOME_ADDRESS:
            case SEC_DEC_IP6_OPT_ENDPOINT_IDENT:
                u1OpLen = *(++pPktBuf);
                if ((pPktBuf + u1OpLen + 1) > pu1HdrEnd)
                {
                    *pu4ErrCode = SEC_DEC_IPV6_BAD_OPT_LEN;
                    return OSIX_FAILURE;
                }
                pPktBuf += u1OpLen + 1;
                break;
            default:
                *pu4ErrCode = SEC_DEC_IPV6_BAD_OPT_TYPE;
                return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/*************************************************************************/
/* Function Name      : SecDecLogMessage                                 */
/* Description        : Logs the attack information                      */
/* Input(s)           : pBuf     -- Pointer to the packet                */
/*                      u4AttackId- Index to identify the type of attack */
/* Output(s)          : None.                                            */
/* Returns            : OSIX_SUCCESS if packet is to permited, otherwise */
/*                      OSIX_FAILURE.                                    */
/*************************************************************************/
INT4
SecDecLogMessage (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4AttackId)
{
    /* Check the database to log */
    /* If so log it else return */
    return OSIX_SUCCESS;
}

/*************************************************************************/
/* Function Name      : SecDecodeVlanPkt                                 */
/* Description        : Decodes the VLAN packet                          */
/* Input(s)           : pBuf     -- Pointer to the packet                */
/*                      u4Offset -- offset value to IP header in packet  */
/*                      u4TotLen -- total length of IP datagram          */
/* Output(s)          : None.                                            */
/* Returns            : OSIX_SUCCESS if packet is to permited, otherwise */
/*                      OSIX_FAILURE.                                    */
/*************************************************************************/
INT4
SecDecodeVlanPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Offset,
                  UINT4 *pu4DataLen)
{
    UINT2               u2Protocol = 0;

    *pu4DataLen = *pu4DataLen + SEC_DEC_VLAN_TAG_HDR_LEN;

    u2Protocol = (SEC_GET_MODULE_DATA_PTR (pBuf)->u2EthType);

    switch (u2Protocol)
    {
        case CFA_PPPOE_SESSION:
            return (SecDecodePppoePkt
                    (pBuf, SEC_DEC_VLAN_TAG_HDR_LEN, pu4DataLen));
            break;

        case CFA_ENET_IPV4:
            return (SecDecodeIpv4Pkt
                    (pBuf, SEC_DEC_VLAN_TAG_HDR_LEN, pu4DataLen));
            break;

        case CFA_ENET_IPV6:
            return (SecDecodeIpv6Pkt
                    (pBuf, SEC_DEC_VLAN_TAG_HDR_LEN, pu4DataLen));
            break;

        default:
            break;
    }
    return OSIX_SUCCESS;
}

/*************************************************************************/
/* Function Name      : SecDecodePppoePkt                                */
/* Description        : Decodes the PPPoE packet                         */
/* Input(s)           : pBuf     -- Pointer to the packet                */
/*                      u4Offset -- offset value to IP header in packet  */
/*                      u4TotLen -- total length of IP datagram          */
/* Output(s)          : None.                                            */
/* Returns            : OSIX_SUCCESS if packet is to permited, otherwise */
/*                      OSIX_FAILURE.                                    */
/*************************************************************************/
INT4
SecDecodePppoePkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                   UINT4 u4Offset, UINT4 *pu4DataLen)
{
    return OSIX_SUCCESS;
}

/*************************************************************************/
/* Function Name      : SecDecodeTcpPkt                                  */
/* Description        : Decodes the TCP packet                           */
/* Input(s)           : pBuf     -- Pointer to the packet                */
/*                      u4Offset -- offset value to IP header in packet  */
/* Output(s)          : None.                                            */
/* Returns            : OSIX_SUCCESS if packet is to permited, otherwise */
/*                      OSIX_FAILURE.                                    */
/*************************************************************************/
INT4
SecDecodeTcpPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                 UINT4 u4Offset, UINT4 u4TotLength, UINT4 *pu4DataLen)
{
    tSecDecTcpHdr      *pTcpHdr = NULL;
    UINT4               u4DataSize = 0;
    UINT1               u1HdrLen = 0;
    UINT1               u1OptionsLen = 0;

    pTcpHdr = (tSecDecTcpHdr *) (pBuf->pSkb->data + u4Offset);

    u1HdrLen = ((pTcpHdr->u1Offset >> 4) & (0x0F)) * 4;

    *pu4DataLen = *pu4DataLen + u1HdrLen;

    if (u1HdrLen < SEC_DEC_TCP_HEADER_LEN)
    {
        SecDecLogMessage (pBuf, DECODE_TCP_INVALID_OFFSET);
        return OSIX_FAILURE;
    }

    if (!TCP_ISFLAGSET (pTcpHdr, SEC_DEC_TH_SYN))
    {
        if (!(pTcpHdr->u1Flags & (SEC_DEC_TH_ACK | SEC_DEC_TH_RST)))
        {
            SecDecLogMessage (pBuf, DECODE_TCP_NO_SYN_ACK_RST);
            return OSIX_FAILURE;
        }
    }
    if ((pTcpHdr->u1Flags & (SEC_DEC_TH_FIN | SEC_DEC_TH_PUSH | SEC_DEC_TH_URG))
        && !(pTcpHdr->u1Flags & SEC_DEC_TH_ACK))
    {
        SecDecLogMessage (pBuf, DECODE_TCP_MUST_ACK);
        return OSIX_FAILURE;

    }

    u1OptionsLen = u1HdrLen - SEC_DEC_TCP_HEADER_LEN;

    if (u1OptionsLen > 0)
    {
        SecDecodeTcpOption (pBuf,
                            (UINT1 *) (pBuf->pSkb->data + u4Offset +
                                       SEC_DEC_TCP_HEADER_LEN),
                            (UINT2) u1OptionsLen);
    }

    u4DataSize = u4TotLength - u1HdrLen;

    if ((pTcpHdr->u1Flags & SEC_DEC_TH_URG) &&
        (!u4DataSize || ntohs (pTcpHdr->u2Urp) > u4DataSize))
    {
        SecDecLogMessage (pBuf, DECODE_TCP_BAD_URP);
        return OSIX_FAILURE;

    }

    if (((pTcpHdr->u1Flags & SEC_DEC_TH_NORESERVED) == SEC_DEC_TH_SYN) &&
        (pTcpHdr->u4Seq == htonl (674711609)))
    {
        SecDecLogMessage (pBuf, DECODE_TCP_SHAFT_SYNFLOOD);
        return OSIX_FAILURE;

    }
    if ((ntohs (pTcpHdr->u2Sport) == 0) || (ntohs (pTcpHdr->u2Dport) == 0))
    {
        SecDecLogMessage (pBuf, DECODE_TCP_PORT_ZERO);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*************************************************************************/
/* Function Name      : SecDecodeUdpPkt                                  */
/* Description        : Decodes the UDP packet                           */
/* Input(s)           : pBuf     -- Pointer to the packet                */
/*                      u4Offset -- offset value to IP header in packet  */
/*                      u4TotLen -- total length of IP datagram          */
/* Output(s)          : None.                                            */
/* Returns            : OSIX_SUCCESS if packet is to permited, otherwise */
/*                      OSIX_FAILURE.                                    */
/*************************************************************************/
INT4
SecDecodeUdpPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                 UINT4 u4Offset, UINT4 u4TotLen, UINT4 *pu4DataLen)
{
    tSecDecUdpHdr      *pUdpHdr = NULL;

    pUdpHdr = (tSecDecUdpHdr *) (pBuf->pSkb->data + u4Offset);

    if (u4TotLen < SEC_DEC_UDP_HEADER_LENGTH)
    {
        SecDecLogMessage (pBuf, UDP_DGRAM_LT_UDPHDR);
        return OSIX_FAILURE;
    }

    if ((ntohs (pUdpHdr->u2SrcPort) == 0) || (ntohs (pUdpHdr->u2DestPort) == 0))
    {
        SecDecLogMessage (pBuf, DECODE_UDP_PORT_ZERO);
        return OSIX_FAILURE;
    }

    if (u4TotLen < ntohs (pUdpHdr->u2UdpLen))
    {
        SecDecLogMessage (pBuf, DECODE_UDP_DGRAM_SHORT_PACKET);
        return OSIX_FAILURE;
    }

    if (u4TotLen > ntohs (pUdpHdr->u2UdpLen))
    {
        SecDecLogMessage (pBuf, DECODE_UDP_DGRAM_LONG_PACKET);
        return OSIX_FAILURE;
    }

    *pu4DataLen = *pu4DataLen + SEC_DEC_UDP_HEADER_LENGTH;
    return OSIX_SUCCESS;
}

INT4
SecDecodeTcpOption (tCRU_BUF_CHAIN_HEADER * pBuf,
                    const UINT1 *pStart, UINT2 u2OptionLen)
{
    tSecDecTcpOptions   TcpOptions[SEC_DEC_TCP_OPTLENMAX];
    const UINT1        *pOption = pStart;
    const UINT1        *pEnd = pStart + u2OptionLen;

    const UINT1        *pLen;
    UINT2               u2OptionCount = 0;
    UINT1               u1Done = 0;    /* have we reached TCPOPT_EOL yet? */
    UINT1               u1ExperimentalOptionFound = 0;    /* are all options RFC compliant? */
    UINT1               u1ObsoleteOptionFound = 0;
    UINT1               u1TtcpFound = 0;

    UINT4               u4Code = 2;
    UINT4               u4ByteSkip = 0;

    /* Here's what we're doing so that when we find out what these
     ** other buggers of TCP option codes are, we can do something
     * * useful
     **
     ** 1) get option code
     ** 2) check for enough space for current option code
     ** 3) set option data ptr
     ** 4) increment option code ptr
     **
     ** TCP_OPTLENMAX = 40 because of
     **        (((2^4) - 1) * 4  - TCP_HEADER_LEN)
     **
     **/

    if (u2OptionLen > SEC_DEC_TCP_OPTLENMAX)
    {
        return OSIX_FAILURE;
    }

    while ((pOption < pEnd) &&
           (u2OptionCount < SEC_DEC_TCP_OPTLENMAX) && (u4Code >= 0) && !u1Done)
    {
        TcpOptions[u2OptionCount].u1Code = *pOption;

        if ((pOption + 1) < pEnd)
        {
            pLen = pOption + 1;
        }
        else
        {
            pLen = NULL;
        }

        switch (*pOption)
        {
            case SEC_DEC_TCPOPT_EOL:
                u1Done = 1;
            case SEC_DEC_TCPOPT_NOP:
                u4ByteSkip = 1;
                u4Code = 0;
                break;
            case SEC_DEC_TCPOPT_MAXSEG:
                u4Code =
                    SecDecOptLenValidate (pOption, pEnd, pLen,
                                          SEC_DEC_TCPOLEN_MAXSEG,
                                          &TcpOptions[u2OptionCount],
                                          &u4ByteSkip);
                break;
            case SEC_DEC_TCPOPT_SACKOK:
                u4Code =
                    SecDecOptLenValidate (pOption, pEnd, pLen,
                                          SEC_DEC_TCPOLEN_SACKOK,
                                          &TcpOptions[u2OptionCount],
                                          &u4ByteSkip);
                break;

            case SEC_DEC_TCPOPT_WSCALE:

                u4Code =
                    SecDecOptLenValidate (pOption, pEnd, pLen,
                                          SEC_DEC_TCPOLEN_WSCALE,
                                          &TcpOptions[u2OptionCount],
                                          &u4ByteSkip);
                if (u4Code == 0)
                {
                    if (((UINT2) TcpOptions[u2OptionCount].pu1Data[0] > 14))
                    {
                        SecDecLogMessage (pBuf, DECODE_TCPOPT_WSCALE_INVALID);
                        return OSIX_FAILURE;
                    }
                }
                break;

            case SEC_DEC_TCPOPT_ECHO:
            case SEC_DEC_TCPOPT_ECHOREPLY:

                u1ObsoleteOptionFound = 1;
                u4Code =
                    SecDecOptLenValidate (pOption, pEnd, pLen,
                                          SEC_DEC_TCPOLEN_ECHO,
                                          &TcpOptions[u2OptionCount],
                                          &u4ByteSkip);
                break;

            case SEC_DEC_TCPOPT_MD5SIG:

                u1ObsoleteOptionFound = 1;
                u4Code =
                    SecDecOptLenValidate (pOption, pEnd, pLen,
                                          SEC_DEC_TCPOLEN_MD5SIG,
                                          &TcpOptions[u2OptionCount],
                                          &u4ByteSkip);
                break;
            case SEC_DEC_TCPOPT_SACK:
                u4Code = SecDecOptLenValidate (pOption, pEnd, pLen, -1,
                                               &TcpOptions[u2OptionCount],
                                               &u4ByteSkip);
                if (TcpOptions[u2OptionCount].pu1Data == NULL)
                {
                    u4Code = SEC_DEC_TCP_OPT_BADLEN;
                }
                break;
            case SEC_DEC_TCPOPT_CC_ECHO:
                u1TtcpFound = 1;

            case SEC_DEC_TCPOPT_CC:    /* all 3 use the same lengths / T/TCP */
            case SEC_DEC_TCPOPT_CC_NEW:
                u4Code =
                    SecDecOptLenValidate (pOption, pEnd, pLen,
                                          SEC_DEC_TCPOLEN_CC,
                                          &TcpOptions[u2OptionCount],
                                          &u4ByteSkip);
                break;
            case SEC_DEC_TCPOPT_TRAILER_CSUM:
                u1ExperimentalOptionFound = 1;
                u4Code =
                    SecDecOptLenValidate (pOption, pEnd, pLen,
                                          SEC_DEC_TCPOLEN_TRAILER_CSUM,
                                          &TcpOptions[u2OptionCount],
                                          &u4ByteSkip);
                break;

            case SEC_DEC_TCPOPT_TIMESTAMP:
                u4Code =
                    SecDecOptLenValidate (pOption, pEnd, pLen,
                                          SEC_DEC_TCPOLEN_TIMESTAMP,
                                          &TcpOptions[u2OptionCount],
                                          &u4ByteSkip);
                break;

            case SEC_DEC_TCPOPT_SKEETER:
            case SEC_DEC_TCPOPT_BUBBA:
            case SEC_DEC_TCPOPT_UNASSIGNED:
                u1ObsoleteOptionFound = 1;
                u4Code = SecDecOptLenValidate (pOption, pEnd, pLen, -1,
                                               &TcpOptions[u2OptionCount],
                                               &u4ByteSkip);
                break;

            default:
            case SEC_DEC_TCPOPT_SCPS:
            case SEC_DEC_TCPOPT_SELNEGACK:
            case SEC_DEC_TCPOPT_RECORDBOUND:
            case SEC_DEC_TCPOPT_CORRUPTION:
            case SEC_DEC_TCPOPT_PARTIAL_PERM:
            case SEC_DEC_TCPOPT_PARTIAL_SVC:
            case SEC_DEC_TCPOPT_ALTCSUM:
            case SEC_DEC_TCPOPT_SNAP:
                u1ExperimentalOptionFound = 1;
                u4Code = SecDecOptLenValidate (pOption, pEnd, pLen, -1,
                                               &TcpOptions[u2OptionCount],
                                               &u4ByteSkip);
                break;

        }

        if (u4Code < 0)
        {
            if (u4Code == SEC_DEC_TCP_OPT_BADLEN)
            {
                SecDecLogMessage (pBuf, SEC_DEC_TCP_OPT_BADLEN);
                return OSIX_FAILURE;
            }
            else if (u4Code == SEC_DEC_TCP_OPT_TRUNC)
            {
                SecDecLogMessage (pBuf, SEC_DEC_TCP_OPT_TRUNC);
                return OSIX_FAILURE;

            }
            return OSIX_SUCCESS;
        }
        u2OptionCount++;
        pOption = pOption + u4ByteSkip;
    }
    if (u1ExperimentalOptionFound)
    {
        SecDecLogMessage (pBuf, DECODE_TCPOPT_EXPERIMENT);
        return OSIX_FAILURE;
    }
    else if (u1ObsoleteOptionFound)
    {
        SecDecLogMessage (pBuf, DECODE_TCPOPT_OBSOLETE);
        return OSIX_FAILURE;

    }
    else if (u1TtcpFound)
    {
        SecDecLogMessage (pBuf, DECODE_TCPOPT_TTCP);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
SecDecOptLenValidate (const UINT1 *pOption,
                      const UINT1 *pEnd,
                      const UINT1 *pLen,
                      UINT4 u4ExpectedLen,
                      tSecDecTcpOptions * pTcpOpt, UINT4 *pByteSkip)
{
    *pByteSkip = 0;

    if (pLen == NULL)
    {
        return SEC_DEC_TCP_OPT_TRUNC;
    }

    if (*pLen == 0 || u4ExpectedLen == 0 || u4ExpectedLen == 1)
    {
        return SEC_DEC_TCP_OPT_BADLEN;
    }
    else if (u4ExpectedLen > 1)
    {
        if ((pOption + u4ExpectedLen) > pEnd)
        {
            return SEC_DEC_TCP_OPT_TRUNC;
        }

        if (*pLen != u4ExpectedLen)
        {
            return (SEC_DEC_TCP_OPT_BADLEN);
        }
    }
    else                        /* u4ExpectedLen < 0 (i.e. variable length) */
    {
        if (*pLen < 2)
        {
            return (SEC_DEC_TCP_OPT_BADLEN);
        }

        if ((pOption + *pLen) > pEnd)
        {
            return (SEC_DEC_TCP_OPT_TRUNC);
        }
    }

    pTcpOpt->u1Len = *pLen - 2;

    if (*pLen == 2)
    {
        pTcpOpt->pu1Data = NULL;
    }
    else
    {
        pTcpOpt->pu1Data = (UINT1 *) (pOption + 2);
    }

    *pByteSkip = *pLen;

    return 0;
}

/*************************************************************************/
/* Function Name      : SecDecodeIcmpv6Pkt                               */
/* Description        : Decodes the ICMPv6 packet                        */
/* Input(s)           : pBuf     -- Pointer to the packet                */
/*                      u4Offset -- offset value to IP header in packet  */
/*                      u2PayLoadLen - Payload length                    */
/*                      pu4DataLen - pointer in which data length to be  */
/*                          updated                                      */
/* Output(s)          : None.                                            */
/* Returns            : OSIX_SUCCESS if packet is to permited, otherwise */
/*                      OSIX_FAILURE.                                    */
/*************************************************************************/
INT4
SecDecodeIcmpv6Pkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                    UINT4 u4Offset, UINT2 u2PayLoadLen, UINT4 *pu4DataLen)
{
    tSecDecIcmp6Hdr    *pIcmp6Hdr = NULL;
    tSecDecIcmp6TooBig *pIcmp6TooBig = NULL;
    tSecDecIcmp6RtrAdvt *pIcmp6RtrAdvt = NULL;
    tSecDecIcmp6RtrSolicit *pIcmp6RtrSol = NULL;
    UINT4               u4DataSize = 0;

    if (u2PayLoadLen < SEC_DEC_ICMP6_MIN_HEADER_LEN)
    {
        SecDecLogMessage (pBuf, DECODE_ICMP6_HDR_TRUNC);
        return OSIX_FAILURE;
    }

    /* fetch the ICMPv6 header */
    pIcmp6Hdr = (tSecDecIcmp6Hdr *) (pBuf->pSkb->data + u4Offset);

    /* Determine the ICMP6 data size */
    u4DataSize = u2PayLoadLen - SEC_DEC_ICMP6_MIN_HEADER_LEN;

    /* Update the data offset */
    *pu4DataLen = *pu4DataLen + SEC_DEC_ICMP6_MIN_HEADER_LEN;

    switch (pIcmp6Hdr->u1Type)
    {
        case SEC_DEC_ICMP6_ECHO:
        case SEC_DEC_ICMP6_REPLY:
            /* Update the data offset */
            *pu4DataLen = *pu4DataLen + sizeof (struct idseq);
            break;
        case SEC_DEC_ICMP6_BIG:
            if (u4DataSize >= sizeof (tSecDecIcmp6TooBig))
            {
                pIcmp6TooBig =
                    (tSecDecIcmp6TooBig *) (pBuf->pSkb->data + u4Offset);
                if (NTOHL (pIcmp6TooBig->u4Mtu) < 1280)
                {
                    SecDecLogMessage (pBuf, DECODE_ICMPV6_TOO_BIG_BAD_MTU);
                    return OSIX_FAILURE;
                }
                /* Update the data offset */
                *pu4DataLen = *pu4DataLen + 4;
            }
            else
            {
                SecDecLogMessage (pBuf, DECODE_ICMP_DGRAM_LT_ICMPHDR);
                return OSIX_FAILURE;
            }
            break;
        case SEC_DEC_ICMP6_TIME:
        case SEC_DEC_ICMP6_PARAMS:
        case SEC_DEC_ICMP6_UNREACH:
            if (u4DataSize >= 4)
            {
                if ((pIcmp6Hdr->u1Type == SEC_DEC_ICMP6_UNREACH) &&
                    (pIcmp6Hdr->u1Code == 2))
                {
                    SecDecLogMessage (pBuf, DECODE_ICMPV6_UNREACHABLE_BAD_CODE);
                    return OSIX_FAILURE;
                }
                /* Update the data offset */
                *pu4DataLen = *pu4DataLen + 4;
            }
            else
            {
                SecDecLogMessage (pBuf, DECODE_ICMP_DGRAM_LT_ICMPHDR);
                return OSIX_FAILURE;
            }
            break;
        case SEC_DEC_ICMP6_ADVERTISEMENT:
            if (u4DataSize >= (sizeof (tSecDecIcmp6RtrAdvt) -
                               SEC_DEC_ICMP6_MIN_HEADER_LEN))
            {
                pIcmp6RtrAdvt = (tSecDecIcmp6RtrAdvt *)
                    (pBuf->pSkb->data + u4Offset);
                if (pIcmp6Hdr->u1Code != 0)
                {
                    SecDecLogMessage (pBuf, DECODE_ICMPV6_ADVERT_BAD_CODE);
                    return OSIX_FAILURE;
                }

                if (NTOHL (pIcmp6RtrAdvt->u4ReachTime) > 3600000)
                {
                    SecDecLogMessage (pBuf, DECODE_ICMPV6_ADVERT_BAD_REACHABLE);
                    return OSIX_FAILURE;
                }
            }
            else
            {
                SecDecLogMessage (pBuf, DECODE_ICMP_DGRAM_LT_ICMPHDR);
                return OSIX_FAILURE;
            }
            break;
        case SEC_DEC_ICMP6_SOLICITATION:
            if (u4DataSize >= (sizeof (tSecDecIcmp6RtrSolicit) -
                               SEC_DEC_ICMP6_MIN_HEADER_LEN))
            {
                if (pIcmp6RtrSol->u1Code != 0)
                {
                    SecDecLogMessage (pBuf,
                                      DECODE_ICMPV6_SOLICITATION_BAD_CODE);
                    return OSIX_FAILURE;
                }
                if (NTOHL (pIcmp6RtrSol->u4Reserved) != 0)
                {
                    SecDecLogMessage (pBuf,
                                      DECODE_ICMPV6_SOLICITATION_BAD_RESERVED);
                    return OSIX_FAILURE;
                }
            }
            else
            {
                SecDecLogMessage (pBuf, DECODE_ICMP_DGRAM_LT_ICMPHDR);
                return OSIX_FAILURE;
            }
            break;
        default:
            SecDecLogMessage (pBuf, DECODE_ICMP6_TYPE_OTHER);
            return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
