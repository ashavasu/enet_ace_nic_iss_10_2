/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved                      */
/* License Aricent Inc., 2001-2010                                           */
/*                                                                           */
/* $Id: secidsipc.c,v 1.12 2015/03/31 06:06:29 siva Exp $                     */
/*                                                                           */
/*  FILE NAME             : secidsipc.c                                      */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : Signature Based IDS                              */
/*  MODULE NAME           : Security Module                                  */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 31 Mar 2011                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains IDS-IPC Task related routines */
/*****************************************************************************/
#include "secinc.h"
#include "secextn.h"

#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <sys/time.h>
#include "selutil.h"
#include "errno.h"
#include "signal.h"
#include "msr.h"

/*****************************************************************************/
/* Function Name      : SecIdsIPCInit                                        */
/*                                                                           */
/* Description        : This function initialize sockets for IPC             */
/*                      communication                                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On Failure.                            */
/*****************************************************************************/
INT4
SecIdsIPCInit (VOID)
{
    INT4                i4RetVal = IDS_FAILURE;

    ISS_IDS_CTRLFD = ISS_IDS_SOCK_INIT;
    ISS_IDS_DATAFD = ISS_IDS_SOCK_INIT;
    ISS_IDS_MGMTFD = ISS_IDS_SOCK_INIT;

    ISS_IDS_CONN_STATUS = ISS_IDS_SOCK_CONN_NONE;

    /* Create Control FD and connect */
    i4RetVal = SecIdsIpcOpenAndConnectCtrlFd ();

    if (IDS_FAILURE == i4RetVal)
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsIPCInit:"
                 "Failed to Initialize Ctrl IPC !!!\n");
    }
    /* Create Management FD and connect */
    i4RetVal = SecIdsIpcOpenAndConnectMgmtFd ();

    if (IDS_FAILURE == i4RetVal)
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsIPCInit:"
                 "Failed to Initialize Mgmt IPC !!!\n");
    }

    /* Create Data FD and connect */
    i4RetVal = SecIdsIpcOpenAndConnectDataFd ();

    if (IDS_FAILURE == i4RetVal)
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsIPCInit:"
                 "Failed to Initalize Data IPC !!!\n");
    }
    return IDS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecIdsIpcOpenAndConnectCtrlFd                        */
/*                                                                           */
/* Description        : This function opens and connect to the socket, which */
/*                      is created for sending attack packets from cfa to    */
/*                      Ids                                                  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On Failure.                            */
/*****************************************************************************/
INT4
SecIdsIpcOpenAndConnectCtrlFd (VOID)
{
    struct sockaddr_in  serverAddr;
    INT4                i4RetVal = IDS_FAILURE;
    INT4                i4SockFd = ISS_IDS_SOCK_INIT;

    /* Setting server address structure to zero */
    MEMSET (&serverAddr, 0, sizeof (struct sockaddr_in));

    /* Creating client socket */
    i4SockFd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (ISS_IDS_IPC_ERROR == i4SockFd)
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcOpenAndConnectCtrlFd:"
                 "Failed to open Control Socket !!!\r\n");
        return IDS_FAILURE;
    }

    /* Store Control Socket Fd */
    ISS_IDS_CTRLFD = i4SockFd;

    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = OSIX_HTONS (ISS_IDS_CTRLFD_PORT);
    serverAddr.sin_addr.s_addr = OSIX_HTONL (ISS_IDS_BASE_IP);

    /* Connecting to the Ids, control channel server */
    i4RetVal = connect (ISS_IDS_CTRLFD,
                        (struct sockaddr *) &serverAddr, sizeof (serverAddr));

    if (ISS_IDS_IPC_ERROR == i4RetVal)
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcOpenAndConnectCtrlFd:"
                 "Failed to connect Ids using Control Socket !!\n");
        i4RetVal = SecIdsIpcCloseCtrlFd ();
        return IDS_FAILURE;
    }

    SelAddFd (ISS_IDS_CTRLFD, SecIdsIpcWriteCallBackFn);

    /* Store connection status */
    ISS_IDS_SET_SOCKFD_CONN_STATUS (ISS_IDS_SOCK_CONN_CTRLFD);

    IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcOpenAndConnectCtrlFd:"
             "Control channel is successfully created and connected to Ids\n");
    return IDS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecIdsIpcOpenAndConnectMgmtFd                        */
/*                                                                           */
/* Description        : This function opens and connect to the socket, which */
/*                      is created for sending mgmt configuratuions from sec */
/*                      to Ids                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On Failure.                            */
/*****************************************************************************/
INT4
SecIdsIpcOpenAndConnectMgmtFd (VOID)
{
    struct sockaddr_in  serverAddr;
    INT4                i4RetVal = IDS_FAILURE;
    INT4                i4SockFd = ISS_IDS_SOCK_INIT;

    /* Setting server address structure to zero */
    MEMSET (&serverAddr, 0, sizeof (struct sockaddr_in));

    /* Creating client socket */
    i4SockFd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (ISS_IDS_IPC_ERROR == i4SockFd)
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcOpenAndConnectMgmtFd:"
                 "Failed to open Mgmt Socket !!!\r\n");
        return IDS_FAILURE;
    }

    /* Store Management Socket Fd */
    ISS_IDS_MGMTFD = i4SockFd;

    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = OSIX_HTONS (ISS_IDS_MGMTFD_PORT);
    serverAddr.sin_addr.s_addr = OSIX_HTONL (ISS_IDS_BASE_IP);

    /* Connecting to the Ids, control channel server */
    i4RetVal = connect (ISS_IDS_MGMTFD,
                        (struct sockaddr *) &serverAddr, sizeof (serverAddr));

    if (ISS_IDS_IPC_ERROR == i4RetVal)
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcOpenAndConnectMgmtFd:"
                 "Failed to connect Ids using Mgmt Socket !!\n");
        i4RetVal = SecIdsIpcCloseMgmtFd ();
        return IDS_FAILURE;
    }

    /* Store connection status */
    ISS_IDS_SET_SOCKFD_CONN_STATUS (ISS_IDS_SOCK_CONN_MGMTFD);

    IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcOpenAndConnectMgmtFd:"
             "Management channel is successfully created and connected to Ids\n");
    return IDS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecIdsIpcOpenAndConnectDataFd                        */
/*                                                                           */
/* Description        : This function opens and connect to the socket, which */
/*                      is created for sending/receiving data packets between*/
/*                      ISS and Ids                                          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On Failure.                            */
/*****************************************************************************/
INT4
SecIdsIpcOpenAndConnectDataFd (VOID)
{
    struct sockaddr_in  serverAddr;
    INT4                i4SockFd = ISS_IDS_SOCK_INIT;
    INT4                i4RetVal = IDS_FAILURE;

    if (SEC_KERN_USER != gi4SysOperMode)
    {
        /* If it is only in USER SPACE */
        /* Setting server address structure to zero */
        MEMSET (&serverAddr, 0, sizeof (struct sockaddr_in));

        /* Creating client socket */
        i4SockFd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);

        if (ISS_IDS_IPC_ERROR == i4SockFd)
        {
            IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcOpenAndConnectDataFd:"
                     "Failed to open Data Socket !!!\n");
            return IDS_FAILURE;
        }

        /* Store Data Socket Fd */
        ISS_IDS_DATAFD = i4SockFd;

        serverAddr.sin_family = AF_INET;
        serverAddr.sin_port = OSIX_HTONS (ISS_IDS_DATAFD_PORT);
        serverAddr.sin_addr.s_addr = OSIX_HTONL (ISS_IDS_BASE_IP);

        /* Connecting to the Ids, data channel server */
        i4RetVal = connect (ISS_IDS_DATAFD,
                            (struct sockaddr *) &serverAddr,
                            sizeof (serverAddr));

        if (ISS_IDS_IPC_ERROR == i4RetVal)
        {
            IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcOpenAndConnectDataFd:"
                     "Failed to connect Ids using Data Socket !!\n");
            i4RetVal = SecIdsIpcCloseDataFd ();
            return IDS_FAILURE;
        }
        SelAddFd (ISS_IDS_DATAFD, SecIdsIpcWriteCallBackFn);

        /* Store connection status */
        ISS_IDS_SET_SOCKFD_CONN_STATUS (ISS_IDS_SOCK_CONN_DATAFD);

        IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcOpenAndConnectDataFd:"
                 "Data channel is successfully created and connected to Ids\n");
    }
    else
    {
        UNUSED_PARAM (serverAddr);
        UNUSED_PARAM (i4SockFd);
        UNUSED_PARAM (i4RetVal);
    }
    return IDS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecIdsIPCDeInit                                      */
/*                                                                           */
/* Description        : This function Deinitialize the sockets opened for IPC*/
/*                      communication                                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On Failure.                            */
/*****************************************************************************/

INT4
SecIdsIPCDeInit (VOID)
{
    INT4                i4RetVal = IDS_FAILURE;

    /* Close Management Socket */
    i4RetVal = SecIdsIpcCloseMgmtFd ();

    if (IDS_FAILURE == i4RetVal)
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsIPCDeInit:"
                 "Failed to close the management socket FD !!!\n");
        return IDS_FAILURE;
    }

    /* Close Control Socket */
    i4RetVal = SecIdsIpcCloseCtrlFd ();

    if (IDS_FAILURE == i4RetVal)
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsIPCDeInit:"
                 "Failed to close the Control socket FD !!!\n");
        return IDS_FAILURE;
    }

    /* Close Data Socket */
    i4RetVal = SecIdsIpcCloseDataFd ();

    if (IDS_FAILURE == i4RetVal)
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsIPCDeInit:"
                 "Failed to close the Data socket FD !!!\n");
        return IDS_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : SecIdsIpcCloseCtrlFd                                 */
/*                                                                           */
/* Description        : This function close the socket, which is created for */
/*                      sending attack packets from cfa to Ids               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On Failure.                            */
/*****************************************************************************/
INT4
SecIdsIpcCloseCtrlFd (VOID)
{
    INT4                i4RetVal = IDS_FAILURE;

    if (ISS_IDS_CTRLFD <= 0)
    {
        return IDS_SUCCESS;
    }

    i4RetVal = close (ISS_IDS_CTRLFD);

    if (ISS_IDS_IPC_ERROR == i4RetVal)
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcCloseCtrlFd:"
                 "Failed to close Control Socket !!!\n");
        return IDS_FAILURE;
    }

    /* Set (-1) to Control FD variable */
    ISS_IDS_CTRLFD = ISS_IDS_SOCK_INIT;

    /* Clear the connection status */
    ISS_IDS_CLEAR_SOCKFD_CONN_STATUS (ISS_IDS_SOCK_CONN_CTRLFD);
    IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcCloseCtrlFd:"
             "Control Socket is successfully closed\n");

    return IDS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecIdsIpcCloseMgmtFd                                 */
/*                                                                           */
/* Description        : This function close the socket, which is created for */
/*                      sending management messages from sec to Ids          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On Failure.                            */
/*****************************************************************************/
INT4
SecIdsIpcCloseMgmtFd (VOID)
{
    INT4                i4RetVal = IDS_FAILURE;

    if (ISS_IDS_MGMTFD <= 0)
    {
        return IDS_SUCCESS;
    }

    i4RetVal = close (ISS_IDS_MGMTFD);

    if (ISS_IDS_IPC_ERROR == i4RetVal)
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcCloseMgmtFd:"
                 "Failed to close Mgmt Socket !!!\n");
        return IDS_FAILURE;
    }

    /* Set (-1) to Management FD variable */
    ISS_IDS_MGMTFD = ISS_IDS_SOCK_INIT;

    /* Clear the connection status */
    ISS_IDS_CLEAR_SOCKFD_CONN_STATUS (ISS_IDS_SOCK_CONN_MGMTFD);
    IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcCloseMgmtFd:"
             "Mgmt Socket is successfully closed\n");

    return IDS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecIdsIpcCloseDataFd                                 */
/*                                                                           */
/* Description        : This function closes the socket, which is created    */
/*                      for sending/receiving data packets between ISS and   */
/*                      Ids                                                  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On Failure.                            */
/*****************************************************************************/
INT4
SecIdsIpcCloseDataFd (VOID)
{
    INT4                i4RetVal = IDS_FAILURE;

    if (SEC_KERN_USER != gi4SysOperMode)
    {
        if (ISS_IDS_DATAFD <= 0)
        {
            return IDS_SUCCESS;
        }

        i4RetVal = close (ISS_IDS_DATAFD);

        if (ISS_IDS_IPC_ERROR == i4RetVal)
        {
            perror ("SecIdsIpcCloseDataFd: Close");
            IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcCloseDataFd:"
                     "Failed to close Data Socket !!!\n");
            return IDS_FAILURE;
        }

        /* Set (-1) to Data FD variable */
        ISS_IDS_DATAFD = ISS_IDS_SOCK_INIT;

        /* Clear the connection status */
        ISS_IDS_CLEAR_SOCKFD_CONN_STATUS (ISS_IDS_SOCK_CONN_DATAFD);
        IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcCloseDataFd:"
                 "Data Socket is successfully closed\n");
    }
    else
    {
        UNUSED_PARAM (i4RetVal);
    }

    return IDS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecIdsIpcSendDataPkt                                 */
/*                                                                           */
/* Description        : This function send packet to the data socket, which  */
/*                      is created for sending/receiving data packets between*/
/*                      ISS and Ids                                          */
/*                                                                           */
/* Input(s)           : pBuf - Data Buffer                                   */
/*                      u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On Failure.                            */
/*****************************************************************************/
INT4
SecIdsIpcSendDataPkt (tCRU_BUF_CHAIN_HEADER * pCRUBuf, UINT4 u4IfIndex)
{
    tIdsMsg            *pIdsMsg = NULL;
    struct sockaddr_in  serverAddr;
    UINT1              *pu1DataBuf = NULL;
    UINT4               u4MsgLen = 0;
    INT4                i4RetVal = IDS_FAILURE;

    if (NULL == pCRUBuf)
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcSendDataPkt:"
                 "Data buffer pointer is NULL !!!\n");
        return IDS_FAILURE;
    }

    /* Allocate packet buffer from the pool */
    if ((pu1DataBuf = MemAllocMemBlk (IDS_PKT_BUF_POOL_ID)) == NULL)
    {
        IDS_TRC (IDS_RESOURCE_TRC | IDS_FAILURE_TRC, "SecIdsIpcSendDataPkt:"
                 "IDS Packet Buffer memory Allocation Failed !!!\r\n");
        CRU_BUF_Release_MsgBufChain (pCRUBuf, FALSE);
        return IDS_FAILURE;
    }

    MEMSET (pu1DataBuf, 0, ISS_IDS_MAX_BUF_SIZE);
    pIdsMsg = (tIdsMsg *) ((VOID *) pu1DataBuf);

    IDS_MSG_OPCODE (pIdsMsg) = ISS_IDS_DATA_PKT;
    IDS_MSG_INTF (pIdsMsg) = u4IfIndex;
    IDS_MSG_DATALEN (pIdsMsg) = CRU_BUF_Get_ChainValidByteCount (pCRUBuf);

    IDS_MSG_DATA (pIdsMsg) =
        CRU_BUF_GetDataPtr (CRU_BUF_GetFirstDataDesc (pCRUBuf));

    if (NULL == IDS_MSG_DATA (pIdsMsg))
    {
        MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
        IDS_TRC (IDS_RESOURCE_TRC | IDS_FAILURE_TRC, "SecIdsIpcSendDataPkt:"
                 "IDS control Message is empty !!!\r\n");
        CRU_BUF_Release_MsgBufChain (pCRUBuf, FALSE);
        return IDS_FAILURE;
    }

    CRU_BUF_Copy_FromBufChain (pCRUBuf,
                               (UINT1 *) (pu1DataBuf + IDS_DATA_START_OFFSET),
                               0, IDS_MSG_DATALEN (pIdsMsg));

    u4MsgLen = (IDS_MSG_DATALEN (pIdsMsg) + IDS_DATA_START_OFFSET);

    /* Send to Ids using Data FD */
    i4RetVal =
        send (ISS_IDS_DATAFD, (VOID *) pu1DataBuf, u4MsgLen, IDS_MSG_NOSIGNAL);

    if (ISS_IDS_IPC_ERROR == i4RetVal)
    {
        if (EPIPE == errno)
        {
            IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcSendDataPkt:"
                     "Send Failed : Broken Pipe !!!\n");

            if (IDS_FAILURE == SecIdsIpcCloseCtrlFd ())
            {
                IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcSendDataPkt:"
                         "Send Failed : Ids is not UP yet. Attempt to close !!\n");
                MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
                CRU_BUF_Release_MsgBufChain (pCRUBuf, FALSE);
                return IDS_FAILURE;
            }
            if (IDS_FAILURE == SecIdsIpcOpenAndConnectCtrlFd ())
            {
                IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcSendDataPkt:"
                         "Send Failed : Ids is not UP yet. Attempt to create and "
                         "connect failed !!\n");
                MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
                CRU_BUF_Release_MsgBufChain (pCRUBuf, FALSE);
                return IDS_FAILURE;
            }
            if (IDS_FAILURE == SecIdsIpcCloseDataFd ())
            {
                IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcSendDataPkt:"
                         "Send Failed : Ids is not UP yet. Attempt to close failed !!\n");
                MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
                CRU_BUF_Release_MsgBufChain (pCRUBuf, FALSE);
                return IDS_FAILURE;
            }
            if (IDS_FAILURE == SecIdsIpcOpenAndConnectDataFd ())
            {
                IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcSendDataPkt:"
                         "Send Failed : Ids is not UP yet. Attempt to create and "
                         "connect failed !!\n");
                MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
                CRU_BUF_Release_MsgBufChain (pCRUBuf, FALSE);
                return IDS_FAILURE;
            }
        }
        else if (ENOTCONN == errno)
        {

            /* Setting server address structure to zero */
            MEMSET (&serverAddr, 0, sizeof (struct sockaddr_in));

            serverAddr.sin_family = AF_INET;
            serverAddr.sin_port = OSIX_HTONS (ISS_IDS_CTRLFD_PORT);
            serverAddr.sin_addr.s_addr = OSIX_HTONL (ISS_IDS_SERVER_IP);

            /* Connecting to the Ids, control channel server */
            i4RetVal = connect (ISS_IDS_CTRLFD, (struct sockaddr *) &serverAddr,
                                sizeof (struct sockaddr));

            if (ISS_IDS_IPC_ERROR == i4RetVal)
            {
                IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcSendDataPkt:"
                         "Control Socket was not connected and failed to reconnect again !!!\n");
            }
            else
            {
                /* Store connection status */
                ISS_IDS_SET_SOCKFD_CONN_STATUS (ISS_IDS_SOCK_CONN_CTRLFD);
                SelAddFd (ISS_IDS_CTRLFD, SecIdsIpcWriteCallBackFn);
                IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcSendDataPkt:"
                         "Control Socket is reconnected again \n");
            }

            /* Setting server address structure to zero */
            MEMSET (&serverAddr, 0, sizeof (struct sockaddr_in));

            serverAddr.sin_family = AF_INET;
            serverAddr.sin_port = OSIX_HTONS (ISS_IDS_DATAFD_PORT);
            serverAddr.sin_addr.s_addr = OSIX_HTONL (ISS_IDS_SERVER_IP);

            /* Connecting to the Ids, data channel server */
            i4RetVal = connect (ISS_IDS_DATAFD, (struct sockaddr *) &serverAddr,
                                sizeof (struct sockaddr));

            if (ISS_IDS_IPC_ERROR == i4RetVal)
            {
                IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcSendDataPkt:"
                         "Data Socket was not connected and failed to reconnect again !!!\n");
                MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
                CRU_BUF_Release_MsgBufChain (pCRUBuf, FALSE);
                return IDS_FAILURE;
            }

            /* Store connection status */
            ISS_IDS_SET_SOCKFD_CONN_STATUS (ISS_IDS_SOCK_CONN_DATAFD);
            SelAddFd (ISS_IDS_DATAFD, SecIdsIpcWriteCallBackFn);
            IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcSendDataPkt:"
                     "Data Socket is reconnected again \n");

            /* Re-Send to Ids using Data FD */
            i4RetVal =
                send (ISS_IDS_DATAFD, (VOID *) pu1DataBuf, u4MsgLen,
                      IDS_MSG_NOSIGNAL);

            if (ISS_IDS_IPC_ERROR == i4RetVal)
            {
                i4RetVal = IDS_FAILURE;
                IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcSendDataPkt:"
                         "Data packet send failed !!\n");
            }
            else
            {
                i4RetVal = IDS_SUCCESS;
            }

            MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
            CRU_BUF_Release_MsgBufChain (pCRUBuf, FALSE);
            return i4RetVal;
        }
        else
        {
            IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcSendDataPkt:"
                     "Failed to send Data packet to Ids !!!\n");
            MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
            CRU_BUF_Release_MsgBufChain (pCRUBuf, FALSE);
            return IDS_FAILURE;
        }
    }

    MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
    CRU_BUF_Release_MsgBufChain (pCRUBuf, FALSE);

    IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcSendDataPkt:"
             "Data packet successfully sent to Ids\n");
    return IDS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecIdsIpcSendCtrlMsg                                 */
/*                                                                           */
/* Description        : This function send the attacked packets to Ids for   */
/*                      logging via socket calls                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On Failure.                            */
/*****************************************************************************/

INT4
SecIdsIpcSendCtrlMsg (tCRU_BUF_CHAIN_HEADER * pCRUBuf)
{
    struct sockaddr_in  serverAddr;
    tIdsMsg            *pIdsMsg = NULL;
    UINT1              *pu1DataBuf = NULL;
    INT4                i4RetVal = IDS_FAILURE;
    UINT4               u4MsgLen = 0;

    if (NULL == pCRUBuf)
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcSendCtrlMsg:"
                 "control buffer pointer is NULL !!!\n");
        return IDS_FAILURE;
    }

    /* Check whether Control FD is created or not then check the connection status */
    if (!IDS_IS_SOCKFD_CREATED (ISS_IDS_CTRLFD))
    {
        /* Create Control FD and connect */
        i4RetVal = SecIdsIpcOpenAndConnectCtrlFd ();
        if (IDS_FAILURE == i4RetVal || ISS_IDS_SOCK_INIT >= ISS_IDS_CTRLFD)
        {
            IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcSendCtrlMsg:"
                     "Control Socket was not created and "
                     "failed to recreate again !!!\n");
            CRU_BUF_Release_MsgBufChain (pCRUBuf, FALSE);
            return IDS_FAILURE;
        }
    }

    /* Check the connection status, if not connected then go for connect again */
    if (!IS_ISS_IDS_SOCKFD_CONNECTED (ISS_IDS_SOCK_CONN_CTRLFD))
    {
        /* Setting server address structure to zero */
        MEMSET (&serverAddr, 0, sizeof (struct sockaddr_in));

        serverAddr.sin_family = AF_INET;
        serverAddr.sin_port = OSIX_HTONS (ISS_IDS_CTRLFD_PORT);
        serverAddr.sin_addr.s_addr = OSIX_HTONL (ISS_IDS_SERVER_IP);

        /* Connecting to the Ids, control channel server */
        i4RetVal = connect (ISS_IDS_CTRLFD,
                            (struct sockaddr *) &serverAddr,
                            sizeof (struct sockaddr));

        if (ISS_IDS_IPC_ERROR == i4RetVal)
        {
            IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcSendCtrlMsg:"
                     "Control Socket was not connected and failed to reconnect again !!!\n");
            CRU_BUF_Release_MsgBufChain (pCRUBuf, FALSE);
            return IDS_FAILURE;
        }

        /* Store connection status */
        ISS_IDS_SET_SOCKFD_CONN_STATUS (ISS_IDS_SOCK_CONN_CTRLFD);
        SelAddFd (ISS_IDS_CTRLFD, SecIdsIpcWriteCallBackFn);
    }

    /* Allocate packet buffer from the pool */
    if ((pu1DataBuf = MemAllocMemBlk (IDS_PKT_BUF_POOL_ID)) == NULL)
    {
        IDS_TRC (IDS_RESOURCE_TRC | IDS_FAILURE_TRC, "SecIdsIpcSendCtrlMsg:"
                 "IDS Packet Buffer memory Allocation Failed !!!\r\n");
        CRU_BUF_Release_MsgBufChain (pCRUBuf, FALSE);
        return IDS_FAILURE;
    }

    MEMSET (pu1DataBuf, 0, ISS_IDS_MAX_BUF_SIZE);

    pIdsMsg = (tIdsMsg *) ((VOID *) pu1DataBuf);

    IDS_MSG_OPCODE (pIdsMsg) = ISS_IDS_DATA_PKT;
    IDS_MSG_DATALEN (pIdsMsg) = CRU_BUF_Get_ChainValidByteCount (pCRUBuf);
    IDS_MSG_DATA (pIdsMsg) =
        CRU_BUF_GetDataPtr (CRU_BUF_GetFirstDataDesc (pCRUBuf));

    if (IDS_MSG_DATA (pIdsMsg) == NULL)
    {
        MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
        IDS_TRC (IDS_RESOURCE_TRC | IDS_FAILURE_TRC, "SecIdsIpcSendCtrlMsg:"
                 "IDS control Message is empty !!!\r\n");
        CRU_BUF_Release_MsgBufChain (pCRUBuf, FALSE);
        return IDS_FAILURE;
    }

    CRU_BUF_Copy_FromBufChain (pCRUBuf,
                               (UINT1 *) (pu1DataBuf + IDS_DATA_START_OFFSET),
                               0, IDS_MSG_DATALEN (pIdsMsg));

    u4MsgLen = (IDS_MSG_DATALEN (pIdsMsg) + IDS_DATA_START_OFFSET);

    /* Send to Ids using Control FD */
    i4RetVal =
        send (ISS_IDS_CTRLFD, (VOID *) pu1DataBuf, u4MsgLen, IDS_MSG_NOSIGNAL);

    if (ISS_IDS_IPC_ERROR == i4RetVal)
    {
        if (EPIPE == errno)
        {
            IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcSendCtrlMsg:"
                     "Send Failed : Broken Pipe !!!\n");
            SecIdsIpcCloseCtrlFd ();
        }

        MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcSendCtrlMsg:"
                 "Failed to send DOS packet to Ids !!!\n");

        /* It is not connected to Ids, So reset the connection bit */
        /* Clear the connection status */
        ISS_IDS_CLEAR_SOCKFD_CONN_STATUS (ISS_IDS_SOCK_CONN_CTRLFD);
        CRU_BUF_Release_MsgBufChain (pCRUBuf, FALSE);
        return IDS_FAILURE;
    }

    MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
    CRU_BUF_Release_MsgBufChain (pCRUBuf, FALSE);

    IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcSendCtrlMsg:"
             "control messages successfully sent to Ids\n");

    return IDS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecIdsIpcSendMgmtMsg                                 */
/*                                                                           */
/* Description        : This function sends mgmt messages to Ids via,        */
/*                      mgmt socket which is created for sending/receiving   */
/*                      management messages between ISS and Ids              */
/*                                                                           */
/* Input(s)           : u4OpCode - OpCode Value                              */
/*                      u4Value  - value for set request                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On Failure.                            */
/*****************************************************************************/
INT4
SecIdsIpcSendMgmtMsg (UINT4 u4OpCode, UINT4 u4Value)
{
    tIdsMsg            *pIdsMsg = NULL;
    struct sockaddr_in  serverAddr;
    UINT1              *pu1DataBuf = NULL;
    UINT4               u4MsgLen = 0;
    INT4                i4RetVal = IDS_FAILURE;
    UINT4               u4InterfaceIndex = 0;
    tCfaIfInfo          IfInfo;

    /* Check whether ctrl FD is created or not then check the connection status */
    if (!IDS_IS_SOCKFD_CREATED (ISS_IDS_MGMTFD))
    {
        /* Create Mgmt FD and connect */
        i4RetVal = SecIdsIpcOpenAndConnectMgmtFd ();
        if (IDS_FAILURE == i4RetVal || ISS_IDS_SOCK_INIT >= ISS_IDS_MGMTFD)
        {
            IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcSendMgmtMsg:"
                     "Mgmt Socket was not created and "
                     "failed to recreate again !!!\n");
            return IDS_FAILURE;
        }
    }

    /* Check the connection status, if not connected then go for connect again */
    if (!IS_ISS_IDS_SOCKFD_CONNECTED (ISS_IDS_SOCK_CONN_MGMTFD))
    {
        /* Setting server address structure to zero */
        MEMSET (&serverAddr, 0, sizeof (struct sockaddr_in));

        serverAddr.sin_family = AF_INET;
        serverAddr.sin_port = OSIX_HTONS (ISS_IDS_MGMTFD_PORT);
        serverAddr.sin_addr.s_addr = OSIX_HTONL (ISS_IDS_SERVER_IP);

        /* Connecting to the Ids, mgmt channel server */
        i4RetVal = connect (ISS_IDS_MGMTFD,
                            (struct sockaddr *) &serverAddr,
                            sizeof (struct sockaddr));

        if (ISS_IDS_IPC_ERROR == i4RetVal)
        {
            IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcSendMgmtMsg:"
                     "mgmt Socket was not connected and failed to reconnect again !!!\n");
            return IDS_FAILURE;
        }

        /* Store connection status */
        ISS_IDS_SET_SOCKFD_CONN_STATUS (ISS_IDS_SOCK_CONN_MGMTFD);
    }

    /* Allocate packet buffer from the pool */
    if ((pu1DataBuf = MemAllocMemBlk (IDS_PKT_BUF_POOL_ID)) == NULL)
    {
        IDS_TRC (IDS_RESOURCE_TRC | IDS_FAILURE_TRC, "SecIdsIpcSendMgmtMsg:"
                 "IDS Packet Buffer memory Allocation Failed !!!\r\n");
        return IDS_FAILURE;
    }

    MEMSET (pu1DataBuf, 0, ISS_IDS_MAX_BUF_SIZE);

    pIdsMsg = (tIdsMsg *) ((VOID *) pu1DataBuf);

    IDS_MSG_OPCODE (pIdsMsg) = u4OpCode;

    if ((u4OpCode & (ISS_IDS_SET_PKT_DROP_THRESH | ISS_IDS_SET_LOG_STATUS)))
    {
        IDS_MSG_DATALEN (pIdsMsg) = IDS_MGMT_MSG_LEN;
        MEMCPY ((pu1DataBuf + IDS_DATA_START_OFFSET), &u4Value,
                IDS_MGMT_MSG_LEN);
    }
    if ((u4OpCode & (ISS_IDS_INTF_STATS_REQ | ISS_IDS_SET_DEL_INTF |
                     ISS_IDS_CLEAR_INTF_STATS)))
    {
        IDS_MSG_INTF (pIdsMsg) = u4Value;
    }
    u4MsgLen = (IDS_MSG_DATALEN (pIdsMsg) + IDS_DATA_START_OFFSET);

    /* Shutting down the WAN ports only when IDS is Enabled irrespective of LOADED or NOT LOADED or RESET */

    if ((gu4PrevIdsStatus == IDS_ENABLE)
        && ((ISS_IDS_RESET_IDS & u4OpCode) || (ISS_IDS_LOAD_RULES & u4OpCode)
            || (ISS_IDS_UNLOAD_RULES & u4OpCode)))
    {

        MEMSET (gau1TempIfStatus, 0, sizeof (gau1TempIfStatus));

        for (u4InterfaceIndex = 0; u4InterfaceIndex < SYS_MAX_INTERFACES;
             u4InterfaceIndex += 1)
        {
            MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

            CfaGetIfInfo (u4InterfaceIndex, &IfInfo);
            if ((CFA_NETWORK_TYPE_WAN == IfInfo.u1NwType) &&
                (CFA_IF_UP == IfInfo.u1IfAdminStatus))
            {
                CfaSetIfMainAdminStatus ((INT4) u4InterfaceIndex, CFA_IF_DOWN);
                gau1TempIfStatus[u4InterfaceIndex] = OSIX_TRUE;
            }
        }
    }

    /* Send to Ids using Mgmt FD */
    i4RetVal =
        send (ISS_IDS_MGMTFD, (VOID *) pu1DataBuf, u4MsgLen, IDS_MSG_NOSIGNAL);

    if (ISS_IDS_IPC_ERROR == i4RetVal)
    {
        if (EPIPE == errno)
        {
            IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcSendMgmtMsg:"
                     "Send Failed : Broken Pipe !!!\n");
            SecIdsIpcCloseMgmtFd ();
        }

        IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcSendMgmtMsg:"
                 "Failed to send mgmt message to Ids !!!\n");

        /* Clear the connection status */
        ISS_IDS_CLEAR_SOCKFD_CONN_STATUS (ISS_IDS_SOCK_CONN_MGMTFD);
        MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
        return IDS_FAILURE;
    }

    MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);

    IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcSendMgmtMsg:"
             "Control message successfully sent to Ids\n");
    return IDS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecIdsIpcWriteCallBackFn                             */
/*                                                                           */
/* Description        : This function is invoked from secutity/ids module to */
/*                      send the data frame to the SEC-IDS task.             */
/*                                                                           */
/* Input(s)           : i4SockFd - socket Identifier                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On failure.                            */
/*****************************************************************************/
VOID
SecIdsIpcWriteCallBackFn (INT4 i4SockFd)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT1               u1MsgType = 0;

    IDS_TRC (IDS_CONTROL_PLANE_TRC, "Entered SecIdsIpcWriteCallBackFn\r\n");

    pBuf = CRU_BUF_Allocate_MsgBufChain (ISS_IDS_MAX_BUF_SIZE, 0);

    if (NULL == pBuf)
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcWriteCallBackFn:"
                 "Failed to allocate message buffer !!!\n");
        return;
    }

    if (ISS_IDS_DATAFD == i4SockFd)
    {
        u1MsgType = IDS_DATA_PKT_TO_SEC;
    }
    else if (ISS_IDS_CTRLFD == i4SockFd)
    {
        u1MsgType = IDS_CTRL_MSG_TO_SEC;
    }
    /* Set the MessageType in pBuf */
    SEC_SET_COMMAND (pBuf, u1MsgType);

    if (OsixQueSend (IDS_QUE_ID, (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN)
        != OSIX_SUCCESS)
    {
        /* Release the allocated memory for the buffer if the posting
         * message to the queue fails */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcWriteCallBackFn:"
                 "Osix send to Queue Failed !!! \r\n");
        return;
    }

    OsixEvtSend (IDS_TASK_ID, IDS_PKT_ENQ_EVENT);

    return;
}

/*****************************************************************************/
/* Function Name      : SecIdsIpcRcvDataPkt                                  */
/*                                                                           */
/* Description        : This function listens on the data socket for pcaket  */
/*                      reception from Ids process                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On Failure.                            */
/*****************************************************************************/
INT4
SecIdsIpcRcvDataPkt (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIdsMsg            *pIdsMsg;
    UINT1              *pu1DataBuf = NULL;
    INT4                i4RetVal = IDS_FAILURE;

    if (NULL == pBuf)
    {
        pBuf = CRU_BUF_Allocate_MsgBufChain (ISS_IDS_MAX_BUF_SIZE, 0);

        if (NULL == pBuf)
        {
            IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcRcvDataPkt:"
                     "Failed to allocate message buffer !!!\n");
            return IDS_FAILURE;
        }
    }
    /* Allocate packet buffer from the pool */
    if ((pu1DataBuf = MemAllocMemBlk (IDS_PKT_BUF_POOL_ID)) == NULL)
    {
        IDS_TRC (IDS_RESOURCE_TRC | IDS_FAILURE_TRC, "SecIdsIpcRcvDataPkt:"
                 "IDS Packet Buffer memory Allocation Failed !!!\r\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return IDS_FAILURE;
    }

    MEMSET (pu1DataBuf, 0, ISS_IDS_MAX_BUF_SIZE);

    i4RetVal = recv (ISS_IDS_DATAFD, pu1DataBuf, ISS_IDS_MAX_BUF_SIZE, 0);

    if (i4RetVal <= 0)
    {
        IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcRcvDataPkt:"
                 "Failed to RECV data buffer using socket !!!\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
        return IDS_NO_DATA_READ;
    }

    pIdsMsg = (tIdsMsg *) ((VOID *) pu1DataBuf);

    if (!IS_IDS_VERDICT_VALID (IDS_MSG_OPCODE (pIdsMsg)))
    {
        IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcRcvDataPkt:"
                 "Invalid verdict received from Ids !!!\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
        return IDS_INVALID_VERDICT;
    }

    if (i4RetVal != (INT4) ISS_IDS_MAX_BUF_SIZE)
    {
        IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcRcvDataPkt:"
                 "Corrupted data received from Ids !!\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
        return IDS_FAILURE;
    }

    if (CRU_FAILURE == CRU_BUF_Copy_OverBufChain (pBuf,
                                                  (UINT1 *) (pu1DataBuf +
                                                             IDS_DATA_START_OFFSET),
                                                  0, pIdsMsg->u4PktLen))
    {
        IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcRcvDataPkt:"
                 "Copying received data from Ids failed !!\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
        return IDS_FAILURE;
    }

    if (ISS_IDS_PKT_ALLOW & IDS_MSG_OPCODE (pIdsMsg))
    {
        IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcRcvDataPkt:"
                 "Ids Allows Data packet !!!\n");
        if (IDS_FAILURE ==
            SecIdsEnqueueDataFrame (pBuf, DATA_FROM_IDS_TO_CFA,
                                    pIdsMsg->u4IfIndex))
        {
            IDS_TRC (IDS_FAILURE, "SecIdsRcvPkt:"
                     "Failed to Enque Event to IDS thread !!!\n");
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
            return IDS_FAILURE;
        }
    }
    else
    {
        IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcRcvDataPkt:"
                 "Data Socket receives invalid message type !!!\n");
    }

    IDS_TRC_ARG2 (IDS_CONTROL_PLANE_TRC, "SecIdsIpcRcvDataPkt:"
                  "%d Bytes of data received from Ids, Verdict = 0x%X !!\n",
                  IDS_MSG_DATALEN (pIdsMsg), IDS_MSG_OPCODE (pIdsMsg));

    MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
    return IDS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecIdsIpcRcvCtrlMsg                                  */
/*                                                                           */
/* Description        : This function listens on the control messages between*/
/*                      Ids and ISS                                          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On Failure.                            */
/*****************************************************************************/
INT4
SecIdsIpcRcvCtrlMsg (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIdsMsg            *pIdsMsg;
    INT4                i4RetVal = IDS_FAILURE;
    UINT4               u4PktCnt = 0;
    UINT4               u4MsgLen = 0;
    UINT4               u4InterfaceIndex = 0;
    UINT1              *pu1DataBuf = NULL;
    tCfaIfInfo          IfInfo;

    UNUSED_PARAM (pBuf);

    /* Allocate packet buffer from the pool */
    if ((pu1DataBuf = MemAllocMemBlk (IDS_PKT_BUF_POOL_ID)) == NULL)
    {
        IDS_TRC (IDS_RESOURCE_TRC | IDS_FAILURE_TRC, "SecIdsIpcRcvCtrlMsg:"
                 "IDS Packet Buffer memory Allocation Failed !!!\r\n");
        return IDS_FAILURE;
    }

    MEMSET (pu1DataBuf, 0, ISS_IDS_MAX_BUF_SIZE);

    i4RetVal = recv (ISS_IDS_CTRLFD, pu1DataBuf, ISS_IDS_MAX_BUF_SIZE, 0);

    if (i4RetVal <= 0)
    {
        MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
        IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcRcvCtrlMsg:"
                 "Failed to RECV data buffer using socket !!!\n");
        return IDS_NO_DATA_READ;
    }

    pIdsMsg = (tIdsMsg *) ((VOID *) pu1DataBuf);

    if (!IS_IDS_CTRL_MSG_VALID (IDS_MSG_OPCODE (pIdsMsg)))
    {
        MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
        IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcRcvCtrlMsg:"
                 "Invalid control message received from Ids !!!\n");
        return IDS_INVALID_VERDICT;
    }

    if (i4RetVal != (INT4) ISS_IDS_MAX_BUF_SIZE)
    {
        MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
        IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcRcvCtrlMsg:"
                 "Corrupted data received from Ids !!\n");
        return IDS_FAILURE;
    }

    if (IDS_MSG_OPCODE (pIdsMsg) & ISS_IDS_DATA_PKT)
    {
        IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcRcvCtrlMsg:"
                 "Ids Blacklisted this packet !!!\n");
#ifdef FIREWALL_WANTED
        FwlHandleBlackListInfoFromIDS ((pu1DataBuf + IDS_DATA_START_OFFSET));
#endif
    }

    if (IDS_MSG_OPCODE (pIdsMsg) & ISS_IDS_TRAP_PKT_DROP_THRESH)
    {
        IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcRcvCtrlMsg:"
                 "Ids Indicated Exceeded Pkt Drop Threshold  !!!\n");

        u4MsgLen =
            (IDS_DATA_START_OFFSET +
             (IDS_MSG_DATALEN (pIdsMsg) - IDS_MGMT_MSG_LEN));

        MEMCPY (&u4PktCnt, (pu1DataBuf + u4MsgLen), IDS_MGMT_MSG_LEN);
    }
    else if (ISS_IDS_LOG_DATA & IDS_MSG_OPCODE (pIdsMsg))
    {
        if (pIdsMsg->u4PktLen < (ISS_IDS_MAX_BUF_SIZE - IDS_DATA_START_OFFSET))
        {
            SecIdsLogData (pu1DataBuf + IDS_DATA_START_OFFSET,
                           pIdsMsg->u4PktLen);
        }
    }
    if (ISS_IDS_INIT_COMP & IDS_MSG_OPCODE (pIdsMsg))
    {
        FwlUtilUpdateIdsRulesStatus (IDS_RULES_LOADED);
        FwlUtilRestoreIdsStatus ();

        for (u4InterfaceIndex = 0; u4InterfaceIndex < SYS_MAX_INTERFACES;
             u4InterfaceIndex += 1)
        {
            CfaGetIfInfo (u4InterfaceIndex, &IfInfo);
            if ((CFA_NETWORK_TYPE_WAN == IfInfo.u1NwType) &&
                (OSIX_TRUE == gau1TempIfStatus[u4InterfaceIndex]))
            {
                CfaSetIfMainAdminStatus ((INT4) u4InterfaceIndex, CFA_IF_UP);
            }
        }
    }

    IDS_TRC_ARG2 (IDS_CONTROL_PLANE_TRC, "SecIdsIpcRcvCtrlMsg:"
                  "%d Bytes of data received from Ids, OpCode = 0x%X !!\n",
                  IDS_MSG_DATALEN (pIdsMsg), IDS_MSG_OPCODE (pIdsMsg));

    MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
    return IDS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecIdsIpcRcvMgmtMsg                                  */
/*                                                                           */
/* Description        : This function listens on the control messages between*/
/*                      Ids and ISS                                          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On Failure.                            */
/*****************************************************************************/
INT4
SecIdsIpcRcvMgmtMsg (tIdsGetInfo * pIdsGetInfo)
{
    tIdsMsg            *pIdsMsg;
    struct timeval      tv;
    UINT1              *pu1DataBuf = NULL;
    fd_set              readFds;
    INT4                i4RetVal = IDS_FAILURE;
    INT4                i4Ret = IDS_FAILURE;

    FD_ZERO (&readFds);
    FD_SET ((UINT4) ISS_IDS_MGMTFD, &readFds);

    if (NULL == pIdsGetInfo)
    {
        IDS_TRC (IDS_FAILURE_TRC, "SecIdsIpcRcvMgmtMsg:"
                 "pIgsGetInfo is NULL !!!\r\n");
        return IDS_FAILURE;
    }

    while (TRUE)
    {
        /* Allocate packet buffer from the pool */
        if ((pu1DataBuf = MemAllocMemBlk (IDS_PKT_BUF_POOL_ID)) == NULL)
        {
            IDS_TRC (IDS_RESOURCE_TRC | IDS_FAILURE_TRC, "SecIdsIpcRcvMgmtMsg:"
                     "IDS Packet Buffer memory Allocation Failed !!!\r\n");
            return IDS_FAILURE;
        }

        MEMSET (pu1DataBuf, 0, ISS_IDS_MAX_BUF_SIZE);

        tv.tv_sec = 0;
        tv.tv_usec = ISS_IDS_SOCK_TIMEOUT;

        i4RetVal = select ((ISS_IDS_MGMTFD + 1), &readFds, NULL, NULL, &tv);

        if (i4RetVal < 0)
        {
            perror ("SecIdsIpcRcvMgmtMsg: Select:");
            MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
            IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcRcvMgmtMsg:"
                     "No Data to read from Ids or some error\n");
            return (IDS_NO_DATA_READ);
        }

        i4RetVal = recv (ISS_IDS_MGMTFD, pu1DataBuf, ISS_IDS_MAX_BUF_SIZE, 0);

        if (i4RetVal <= 0)
        {
            MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
            IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcRcvMgmtMsg:"
                     "Failed to RECV control messages via mgmt socket !!!\n");
            i4Ret = IDS_NO_DATA_READ;
            break;
        }

        pIdsMsg = (tIdsMsg *) ((VOID *) pu1DataBuf);

        if (!IS_IDS_MGMT_MSG_VALID (IDS_MSG_OPCODE (pIdsMsg)))
        {
            MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
            IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcRcvMgmtMsg:"
                     "Invalid message received from Ids !!!\n");
            i4Ret = IDS_INVALID_VERDICT;
            break;
        }

        if (i4RetVal !=
            (INT4) ISS_IDS_MAX_BUF_SIZE)
        {
            MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
            IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcRcvMgmtMsg:"
                     "Corrupted message received from Ids !!\n");

        }

        if (IDS_MSG_OPCODE (pIdsMsg) & ISS_IDS_VERSION_REQ)
        {
            IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcRcvMgmtMsg:"
                     "Received current Version Information !!!\n");
            if (pIdsMsg->u4PktLen <
                (ISS_IDS_MAX_BUF_SIZE - IDS_DATA_START_OFFSET))
            {

                MEMCPY (pIdsGetInfo->au1Version,
                        (pu1DataBuf + IDS_DATA_START_OFFSET),
                        pIdsMsg->u4PktLen);
            }
            MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
            return IDS_SUCCESS;
        }
        else if (IDS_MSG_OPCODE (pIdsMsg) &
                 (ISS_IDS_GLB_STATS_REQ | ISS_IDS_INTF_STATS_REQ))
        {
            IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcRcvMgmtMsg:"
                     "Received Stats information from Ids  !!!\n");
            MEMCPY (&pIdsGetInfo->u4AllowStats,
                    (pu1DataBuf + IDS_DATA_START_OFFSET), IDS_MGMT_MSG_LEN);
            MEMCPY (&pIdsGetInfo->u4DropStats,
                    (pu1DataBuf + (IDS_DATA_START_OFFSET + IDS_MGMT_MSG_LEN)),
                    IDS_MGMT_MSG_LEN);
            MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
            return IDS_SUCCESS;
        }
        else if (IS_IDS_MGMT_SET_MSG_VALID (IDS_MSG_OPCODE (pIdsMsg)))
        {
            IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcRcvMgmtMsg:"
                     "Received configuration reply messages From Ids  !!!\n");
            if ((pIdsMsg->u4OpCode & ISS_IDS_MGMT_RESPONSE))
            {
                MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
                return IDS_SUCCESS;
            }
        }

        MemReleaseMemBlock (IDS_PKT_BUF_POOL_ID, (UINT1 *) pu1DataBuf);
        IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsIpcRcvMgmtMsg:"
                 "Opcode is not Matched !!!\n");
        break;
    }
    return i4Ret;
}
