/*****************************************************************************/
/* Copyright (C) 2011 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2011                                          */
/*                                                                           */
/* $Id: secsz.c,v 1.2 2015/04/04 10:33:53 siva Exp $                       */
/*                                                                           */
/*  FILE NAME             : secsz.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : Packet Handler                                   */
/*  MODULE NAME           : Security Module                                  */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 16 Dec 2010                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains sizing functions      */
/*  in the user space                                                        */
/*****************************************************************************/
#define _SECSZ_C
#include "secinc.h"
extern INT4
 
 
 
 
IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                 tFsModSizingParams * pModSizingParams);
INT4
SecSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < SEC_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = MemCreateMemPool (FsSECSizingParams[i4SizingId].u4StructSize,
                                     FsSECSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(SECMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            SecSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
#ifdef IPSECv4_WANTED 
    if (MemCreateMemPool (sizeof (tIkeQMsg), 1000,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &SECv4_IPSEC_MSG_TO_IKE_MEMPOOl) != MEM_SUCCESS)
    {
        return (OSIX_FAILURE);
    }
#endif    
    return OSIX_SUCCESS;
}

INT4
SecSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsSECSizingParams);
    return OSIX_SUCCESS;
}

VOID
SecSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < SEC_MAX_SIZING_ID; i4SizingId++)
    {
        if (SECMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (SECMemPoolIds[i4SizingId]);
            SECMemPoolIds[i4SizingId] = 0;
        }
    }
#ifdef IPSECv4_WANTED 
    /* Release the Memory allocated to send msg from IPsec to IKE */
    MemDeleteMemPool (SECv4_IPSEC_MSG_TO_IKE_MEMPOOl);
#endif
    return;
}
