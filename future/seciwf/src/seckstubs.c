
/*****************************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: seckstubs.c,v 1.6 2015/01/19 11:34:41 siva Exp $ 
 *
 * Description: This file contains the stub routines for user space compilation
 *****************************************************************************/
#ifndef _SECKSTUBS_C_
#define _SECKSTUBS_C_

#include "secinc.h"
#include "seckinc.h"
#include "sectrc.h"

tMemPoolId          gBitListPoolId;
tMemPoolId          gLocalPortListPoolId;
tMemPoolId          gVlanListSizePoolId;
tMemPoolId          gVlanIdSizePoolId;
tMemPoolId          UTILMemPoolIds[UTIL_MAX_SIZING_ID];
/*****************************************************************************
 *
 *    Function Name       : SecInit
 *
 *    Description         : This function is Used to Initialise Security
 *                          Module.  This function blocks on reading data from 
 *                          the security character device.
 *
 *
 *    Input(s)            : None
 *
 *    Output(s)           : NONE.
 *
 *    Global Variables Referred : gi4SecDevFd, gau1RxMsgBuf.                                         
 *                                                                           
 *    Global Variables Modified : None.                                         
 *                                                                           
 *    Use of Recursion          : None.                                         
 *
 *    Output(s)           : None.
 *
 *    Returns             : None
 *****************************************************************************/
VOID
SecInit (VOID)
{
    return;
}

/*****************************************************************************
 *
 *    Function Name       : SecInitDevices 
 *
 *    Description         : This function is Used to Initialise Security
 *                          Module in user space. 
 *
 *                          It creates the device arsec  with major number 
 *                          200, loads the ISSSec.ko in the kernel, and 
 *                          calls the ioctl  to create the sec user module
 *                          queue in the kernel.
 *                         
 *                          It invokes the ioctl to initialize the queue in
 *                          the character device to interact with the user 
 *                          space.
 *
 *
 *    Input(s)            : None
 *
 *    Output(s)           : NONE.
 *
 *    Global Variables Referred : None.                                         
 *                                                                           
 *    Global Variables Modified : None.                                         
 *                                                                           
 *    Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
INT4
SecInitDevices (VOID)
{
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name    : SecDeInitDevices 
 *
 *    Description      : This function is Used to unload the ISSSec.ko 
 *                       from the running kernel 
 *
 *
 *    Input(s)         : None
 *
 *    Output(s)        : NONE.
 *
 *    Global Variables Referred : gi4SysOperMode.                                         
 *                                                                           
 *    Global Variables Modified : None.                                         
 *                                                                           
 *    Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : NONE 
 *****************************************************************************/
VOID
SecDeInitDevices (VOID)
{
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function           : IpUpdateInIfaceErrInCxt                              *
 *                                                                           *
 * Description        : Increment the error count for input interface        *
 *                      error in the specified context.                      *
 *                                                                           *
 * Input(s)           : u4ContextId                                          *
 *                                                                           *
 * Output(s)          : NONE.                                                *
 *                                                                           *
 * Returns            : NONE                                                 *
 *                                                                           *
 *****************************************************************************/
VOID
IpUpdateInIfaceErrInCxt (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return;
}

/******************************************************************************
 * Function           : CfaRegisterHL
 * Input(s)           : pCfaRegParams - Reg params to be filled-in by HL. 
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : Routine used by HL to register with CFA. 
 ******************************************************************************/
INT4
CfaRegisterHL (tCfaRegParams * pCfaRegParams)
{
    UNUSED_PARAM (pCfaRegParams);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetContextIdFromCfaIfIndex                      */
/*                                                                           */
/*     DESCRIPTION      : This functions gets the context id associated      */
/*                        with the given cfa ifIndex                         */
/*                                                                           */
/*     INPUT            : u4CfaIfIndex  - Cfa interface index                */
/*                                                                           */
/*     OUTPUT           : *pu4CxtId - Context id of the interface            */
/*                                    having cfa IfIndex as u4CfaIfIndex     */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS    - If interface is found and the     */
/*                                        context is associated              */
/*                                        with that interface is returned    */
/*                        VCM_FAILURE    - Otherwise                         */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetContextIdFromCfaIfIndex (UINT4 u4CfaIfIndex, UINT4 *pu4CxtId)
{
    UNUSED_PARAM (u4CfaIfIndex);
    UNUSED_PARAM (pu4CxtId);
    return VCM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIdentifyVlanId                               */
/*                                                                           */
/*    Description         : This function is called by IVR to get the VlanId */
/*                          Assumption is that buffer will be released by    */
/*                          the calling function                             */
/*                                                                           */
/*    Input(s)            : pFrame   - Pointer to the incoming packet        */
/*                          u2InPort - Incoming interface Port Number        */
/*                                                                           */
/*    Output(s)           : pVlanId  - Gip Associated with the packet.       */
/*                                                                           */
/*    Returns            : VLAN_FORWARD / VLAN_NO_FORWARD                    */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIdentifyVlanId (tCRU_BUF_CHAIN_DESC * pFrame,
                    UINT4 u4InPort, tVlanId * pVlanId)
{
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (u4InPort);
    UNUSED_PARAM (pVlanId);
    return VLAN_NO_FORWARD;
}

int
ioctl (int __fd, unsigned long int __request, ...)
{
    UNUSED_PARAM (__fd);
    UNUSED_PARAM (__request);
    return 0;
}

/*****************************************************************************
 *                                                                           *
 * Function           : IpUpdateOutIfaceErrInCxt                             *
 *                                                                           *
 * Description        : Increment the error count for outgoing interface     *
 *                      error in the specified context.                      *
 *                                                                           *
 * Input(s)           : u4ContextId                                          *
 *                                                                           *
 * Output(s)          : NONE.                                                *
 *                                                                           *
 * Returns            : NONE                                                 *
 *                                                                           *
 *****************************************************************************/
VOID
IpUpdateOutIfaceErrInCxt (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return;
}

/****************************************************************************
* Function     : ArpResolve     
*                                            
* Description  : To check whether the entry is available in the Arp cache.  
*
* Input        : u4IpAddr - IpAddress to be resolved.     
*
* Output       : pi1Hw_addr -Hardware address.
*                pu1EncapType - EncapType for that Entry.   
*
* Returns      : ARP_SUCCESS/ARP_FAILURE   
*
***************************************************************************/
INT1
ArpResolve (UINT4 u4IpAddr, INT1 *pi1Hw_addr, UINT1 *pu1EncapType)
{
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (pi1Hw_addr);
    UNUSED_PARAM (pu1EncapType);
    return ARP_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieInit ()                                              */
/*                                                                         */
/*  Description   This function initialises an array. Each member of this  */
/*                array is a tRadixNodeHead structure and represents       */
/*                different instance of Trie. It also initialises integer  */
/*                fields of all members. It creates global semaphore.      */
/*                This ensures that even in presence of more applications  */
/*                TrieCreate () is always called in  mutual exclusion.     */
/*                                                                         */
/*  Call          This function should be called once                      */
/*  condition     when the Trie library needs to be initialised.           */
/*                                                                         */
/*  Input(s)      None.                                                    */
/*                                                                         */
/*  Output(s)     None.                                                    */
/*                                                                         */
/*                                                                         */
/*  Access        Global task - as applicable                              */
/*  privileges                                                             */
/*                                                                         */
/*  Return        TRIE_SUCCESS - If successful in initialising Trie family. */
/*                TRIE_FAILURE - Otherwise.                                */
/*                                                                         */
/***************************************************************************/
INT4
TrieInit (void)
{
    return (OSIX_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieLibInit ()                                           */
/*                                                                         */
/*  Description   This function initialises an array. Each member of this  */
/*                array is a tRadixNodeHead structure and represents       */
/*                different instance of Trie. It also initialises integer  */
/*                fields of all members. It creates global semaphore.      */
/*                This ensures that even in presence of more applications  */
/*                TrieCrt () is always called in  mutual exclusion.        */
/*                                                                         */
/*  Call          This function should be called once                      */
/*  condition     when the Trie library needs to be initialised.           */
/*                                                                         */
/*  Input(s)      None.                                                    */
/*                                                                         */
/*  Output(s)     None.                                                    */
/*                                                                         */
/*                                                                         */
/*  Access        Global task - as applicable                              */
/*  privileges                                                             */
/*                                                                         */
/*  Return        TRIE_SUCCESS - If successful in initialising Trie family.*/
/*                TRIE_FAILURE - Otherwise.                                */
/*                                                                         */
/***************************************************************************/
INT4
TrieLibInit (void)
{

    return (OSIX_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieLibMemPoolInit ()                                    */
/*                                                                         */
/*  Description   This function creates common memory pools for            */
/*                Radix nodes, Leaf nodes and KeyIds. It creates           */
/*                semphore for each pool                                   */
/*                                                                         */
/*  Call          This function should be called once                      */
/*  condition     when the Trie library needs to be initialised.           */
/*                                                                         */
/*  Input(s)      u4NumNodes - Total number of trie2 nodes needed          */
/*                             by all protocols.                           */
/*                                                                         */
/*  Output(s)     None.                                                    */
/*                                                                         */
/*                                                                         */
/*  Access        Global task - as applicable                              */
/*  privileges                                                             */
/*                                                                         */
/*  Return        TRIE_SUCCESS - If successful in initialising Trie family.*/
/*                TRIE_FAILURE - Otherwise.                                */
/*                                                                         */
/***************************************************************************/
INT4
TrieLibMemPoolInit (VOID)
{
    return (OSIX_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogMsg                                              */
/*                                                                           */
/* Description      : This function is invoked to log a system event.        */
/*                                                                           */
/* Input Parameters : UINT4 u4Level  - Log level                             */
/*                    UINT4 u4ModuleId - Module Identifier                   */
/*                    INT1 *pFormat - Message Given by user                  */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  None                                                  */
/*****************************************************************************/

VOID
SysLogMsg (UINT4 u4Level, UINT4 u4ModuleId, CONST CHR1 * pFormat, ...)
{
    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (u4ModuleId);
    UNUSED_PARAM (pFormat);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetIpAddrFromNvRam                            */
/*                                                                          */
/*    Description        : This function is invoked to get the IP address   */
/*                         of the default interface from the NVRAM.         */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : pIpAddr - IP Address of the default interface    */
/****************************************************************************/
UINT4
IssGetIpAddrFromNvRam (VOID)
{
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name     : IndexManagerInit                                      */
/*                                                                           */
/* Description       : This function intialiases the Index Manager           */
/*                                                                           */
/* Input Parameters  : None                                                  */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : INDEX_SUCCESS / INDEX_FAILURE                         */
/*****************************************************************************/
UINT1
IndexManagerUtilInit ()
{
    return INDEX_SUCCESS;
}

VOID
CustomStartup (int argc, char *argv[])
{
    UNUSED_PARAM (argc);
    UNUSED_PARAM (argv);
    return;
}

/*******************************************************************************
 * Function    : MemBuddyInit ()
 * Description : This function initializes the global Buddy table.
 * Input (s)   : None
 * Outputs(s)  : None
 * Globals     : gBuddyTable Referred and Modified.
 * Returns     : VOID
 ******************************************************************************/
INT4
MemBuddyInit (UINT4 u4Instances)
{
    UNUSED_PARAM (u4Instances);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : SecInitRegWithCfa
 *
 *    Description         : This function is Used to Register with Cfa for
 *                          Interface Callback Notifications.
 *
 *    Input(s)            : None
 *
 *    Output(s)           : NONE.
 *
 * Global Variables Referred : None.                                         
 *                                                                           
 * Global Variables Modified : None.                                         
 *                                                                           
 * Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
INT4
SecInitRegWithCfa (VOID)
{
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : SecInitDeRegWithCfa
 *
 *    Description         : This function is Used to DeRegister with Cfa for
 *                          Interface Callback Notifications.
 *
 *    Input(s)            : None
 *
 *    Output(s)           : NONE.
 *
 * Global Variables Referred : None.                                         
 *                                                                           
 * Global Variables Modified : None.                                         
 *                                                                           
 * Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
INT4
SecInitDeRegWithCfa (VOID)
{
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function     : FsUtilBitListInit   
*                                            
* Description  : To intialize the mempool required for bitlist variable.
*
* Input        : None
*
* Output       : None
*
* Returns      : None   
*
***************************************************************************/
VOID
FsUtilBitListInit (UINT4 u4Count)
{
    UNUSED_PARAM (u4Count);
    return;
}

/*****************************************************************************
 *
 *    Function Name       :  SecInitPostPacketToKernel
 *
 *    Description         : This function does the following. 
 *                          1. If the interface index is invalid, return 
 *                             SECMOD_FAILURE.
 *                          2. If the packet has to be written on a non-WAN 
 *                             interface,return SECMOD_CONTINUE.  
 *                          3. If the packet has to be written on a valid WAN
 *                             interface, 
 *                             (i) Security Message header is 
 *                                 appropriately filled and prepended to 
 *                                 the packet.
 *                             (ii) The packet is written on Security 
 *                                  character device. 
 *                          4. If write fails, return SECMOD_FAILURE.
 *
 *    Input(s)            : pu1PktBuf - Pointer to packet buffer.
 *                          u4IfIdx - Interface Index On which Packet has to 
 *                          be transmitted.
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : SECMOD_CONTINUE - if u4IfIdx is a non-WAN interface
 *                          SECMOD_FAILURE  - if interface is invalid or write 
 *                                            fails. 
 *                          SECMOD_SUCCESS - If write on WAN interface succeeds
 *****************************************************************************/
INT4
SecInitPostPacketToKernel (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIdx,
                           UINT1 u1Direction)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4IfIdx);
    UNUSED_PARAM (u1Direction);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecInitInvokeIoctl                               */
/*                                                                           */
/*    Description         : Utility function to to invoke the ioctl          */
/*                          to send the information to the kernel            */
/*                                                                           */
/*    Input(s)            : u4Command-Command to identify the request        */
/*                          pSecInfo - Information to be passed to kernel    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_FAILURE or OSIX_SUCCESS                      */
/*                                                                           */
/*****************************************************************************/
INT4
SecInitInvokeIoctl (UINT4 u4Command, VOID *pSecInfo)
{
    UNUSED_PARAM (u4Command);
    UNUSED_PARAM (pSecInfo);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : SecInitRegWithIpv6
 *
 *    Description         : This function is Used to Register with ip for
 *                          getting ipv6 Interface Callback Notifications.
 *
 *    Input(s)            : None
 *
 *    Output(s)           : NONE.
 *
 * Global Variables Referred : None.                                         
 *                                                                           
 * Global Variables Modified : None.                                         
 *                                                                           
 * Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
INT4
SecInitRegWithIpv6 (VOID)
{
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : SecInitDeRegWithIp
 *
 *    Description         : This function is Used to DeRegister with IPv6 for
 *                          Interface Callback Notifications.
 *
 *    Input(s)            : None
 *
 *    Output(s)           : NONE.
 *
 * Global Variables Referred : None.                                         
 *                                                                           
 * Global Variables Modified : None.                                         
 *                                                                           
 * Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
INT4
SecInitDeRegWithIpv6 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                 tFsModSizingParams * pModSizingParams)
{
    UNUSED_PARAM (pu1ModName);
    UNUSED_PARAM (pModSizingParams);
    return OSIX_SUCCESS;
}

#ifdef SECURITY_KERNEL_WANTED
INT4
IssSzRegisterModulePoolId (CHR1 * pu1ModName, tMemPoolId * pModPoolId)
{
    UNUSED_PARAM (pu1ModName);
    UNUSED_PARAM (pModPoolId);
    return OSIX_SUCCESS;

}
#endif
PUBLIC INT1
FwlErrorV6MessageGenerate (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4IfIndex);
    return FWL_SUCCESS;
}

INT4
SecIdsGetInfo (UINT4 u4MsgType, tIdsGetInfo * pIdsGetInfo)
{
    UNUSED_PARAM (u4MsgType);
    UNUSED_PARAM (pIdsGetInfo);
    return IDS_SUCCESS;
}

INT4
SecIdsSetInfo (UINT4 u4MsgType, UINT4 u4Value)
{
    UNUSED_PARAM (u4MsgType);
    UNUSED_PARAM (u4Value);
    return IDS_FAILURE;
}

/*****************************************************************************
*    Function Name            : CfaIpIfGetIfIndexFromIpAddress
*
*    Description              :  Provides the interface having the given IP
*                                Address
*
*    Input(s)                  : u4IpAddress - IP address for which the
*                                interface index to be derived
*
*    Output(s)                 :  Interface index.
*
*    Global Variables Referred : gIpIfInfo.pIpIfTable
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE
*****************************************************************************/
INT4
CfaIpIfGetIfIndexFromIpAddress (UINT4 u4IpAddress, UINT4 *pu4CfaIfIndex)
{
    UNUSED_PARAM (u4IpAddress);
    UNUSED_PARAM (pu4CfaIfIndex);
    return (CFA_SUCCESS);
}

/************************************************************************
 *  Function Name   : UtlSnmpMemInit
 *  Description     : Function to initialise SNMP related utility mem pools
 *                    This will be called if SNMP is not enabled by default.
 *  Input           : None
 *  Output          : None
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
INT4
UtlSnmpMemInit (VOID)
{
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetVlanInterfaceIndex
 *
 *    Description         : This function returns the VLAN interface index 
 *
 *    Input(s)            : VLAN ID. 
 *
 *    Output(s)           : VLAN Interface Index.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :  VLAN interface index corresponding to the 
 *                          VLAN ID,if success. Else, returns the invalid 
 *                          interface index.
 *****************************************************************************/

UINT4
CfaGetVlanInterfaceIndex (tVlanIfaceVlanId VlanId)
{
    UNUSED_PARAM (VlanId);
    return CFA_INVALID_INDEX;
}

/*****************************************************************************
 *   Function Name       : UtilSizingMemCreateMemPools
 *   Description         : This function is an stub for Kernel space & 
 *                         interact with userspace,the main defination is in 
 *                         util/cmn/utilsz.c. 
 *   Input(s)            : NONE. 
 *   Output(s)           : NONE.
 *   Returns            : OSIX_SUCCESS/OSIX_FAILURE.
 *****************************************************************************/

INT4
UtilSizingMemCreateMemPools (VOID)
{
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function     : UtilPlstInitLocalPortList
*
* Description  : To intialize the mempool required for Local Portlist variable.
*
* Input        : None
*
* Output       : None
*
* Returns      : None
*
***************************************************************************/

VOID
UtilPlstInitLocalPortList (UINT4 u4Count)
{
    UNUSED_PARAM (u4Count);
    return;
}

/****************************************************************************
* Function     : UtilVlanInitVlanListSize
*
* Description  : To intialize the mempool required for VlanListSize (512).
*
* Input        : None
*
* Output       : None
*
* Returns      : None
*
***************************************************************************/

VOID
UtilVlanInitVlanListSize (UINT4 u4Count)
{
    UNUSED_PARAM (u4Count);
    return;
}

/****************************************************************************
* Function     : UtilVlanInitVlanIdSize
*
* Description  : To intialize the mempool required for VlanIdSize (4094).
*
* Input        : None
*
* Output       : None
*
* Returns      : None
*
***************************************************************************/

VOID
UtilVlanInitVlanIdSize (UINT4 u4Count)
{
    UNUSED_PARAM (u4Count);
    return;
}

/******************************************************************************/
/*                                                                            */
/*    Function Name      : UtlShMemInitPoolIds                                */
/*                                                                            */
/*    Description        : This function initializes the common memory pools  */
/*                         utilised by Radius,Tacacs and the other modules    */
/*                         which makes use of OIfList interdependently.       */
/*                                                                            */
/*                                                                            */
/*    Input(s)           : None                                               */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : None                                               */
/******************************************************************************/

VOID
UtlShMemInitPoolIds (VOID)
{
    return;
}

INT4
MsrGetSaveStatus (VOID)
{
    return ISS_TRUE;
}

INT4
VpnIkeImportKey (UINT1 *pu1KeyName, UINT1 *pu1Filename, UINT4 u4Type)
{
    UNUSED_PARAM (pu1KeyName);
    UNUSED_PARAM (pu1Filename);
    UNUSED_PARAM (u4Type);
    return OSIX_SUCCESS;
}

INT4
VpnIkeImportCert (UINT1 *pu1Filename, UINT1 u1EncodeType, UINT1 *pu1Key)
{
    UNUSED_PARAM (pu1Filename);
    UNUSED_PARAM (pu1Key);
    UNUSED_PARAM (u1EncodeType);
    return OSIX_SUCCESS;
}

INT4
VpnIkeDelKeys (UINT1 *pu1KeyID, UINT4 u4Type)
{
    UNUSED_PARAM (pu1KeyID);
    UNUSED_PARAM (u4Type);
    return OSIX_SUCCESS;
}

INT4
VpnIkeImportCACert (UINT1 *pu1CaCertName, UINT1 *pu1FileName,
                    UINT1 u1EncodeType)
{
    UNUSED_PARAM (pu1CaCertName);
    UNUSED_PARAM (pu1FileName);
    UNUSED_PARAM (u1EncodeType);
    return OSIX_SUCCESS;
}

INT4
VpnIkeDelCerts (UINT4 u4Option, UINT1 *pu1Key)
{
    UNUSED_PARAM (u4Option);
    UNUSED_PARAM (pu1Key);
    return OSIX_SUCCESS;
}

INT4
NetIpv4GetCfaIfIndexFromPort (UINT4 u4Port, UINT4 *pu4IfIndex)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (pu4IfIndex);
    return NETIPV4_SUCCESS;
}

INT4
NetIpv4GetRoute (tRtInfoQueryMsg * pRtQuery, tNetIpv4RtInfo * pNetIpRtInfo)
{
    UNUSED_PARAM (pRtQuery);
    UNUSED_PARAM (pNetIpRtInfo);
    return NETIPV4_SUCCESS;
}

#ifndef SNMP_3_WANTED
/************************************************************************/
/*  Function Name   : IssNotifyConfiguration                            */
/*  Description     : This function is called from the low level        */
/*                     set routines, to send configuration update event */
/*                     triggers to MSR.                                 */
/*                                                                      */
/*  Input           : SnmpNotifyInfo - Snmp Notification Strucutre      */
/*                    pu1Fmt      - The data type of indices and data.  */
/*                    variable set of inputs depending the on the       */
/*                    indices and the data.                             */
/*  Output          : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IssNotifyConfiguration (tSnmpNotifyInfo SnmpNotifyInfo, CHR1 * pu1Fmt, ...)
{

    UNUSED_PARAM (SnmpNotifyInfo);
    UNUSED_PARAM (pu1Fmt);
    return;
}
#endif
#endif
