/***********************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved    *
 *                                                         *
 * $Id: secport.c,v 1.11 2015/01/19 11:34:41 siva Exp $     *
 *                                                         *
 * Description: This file contains the external APIs.      *
 *                                                         *
 ***********************************************************/

#ifndef _SECPORT_C_
#define _SECPORT_C_

#include "secinc.h"
#include "secextn.h"
#include "sectrc.h"
#include "seckgen.h"

/*****************************************************************************/
/* Function Name      : SecPortHandlePktFromSecurity                         */
/*                                                                           */
/* Description        : This function is invoked from secutity module to     */
/*                      send the data frame to the SEC-IDS task if IDS is    */
/*                      defined otherwise the packet will be given to CFA    */
/*                                                                           */
/* Input(s)           : pBuf - data Frame                                    */
/*                      u4IfIndex - Interface Index                          */
/*                      u1IsIpSec - Flag indicating, whether IPSEC processing*/
/*                                  is applied or not.                       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On failure.                            */
/*****************************************************************************/
INT4
SecPortHandlePktFromSecurity (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                              UINT1 u1IsIpSec, tCfaIfInfo * IfInfo,
                              UINT2 u2Protocol, t_IP_HEADER * pIpHdr)
{
#ifdef SECURITY_KERNEL_WANTED
    UINT4               u4DataOffset = 0;
    INT4                i4Retval = -1;
#endif
    if ((CFA_NETWORK_TYPE_WAN == IfInfo->u1NwType) && (OSIX_FALSE == u1IsIpSec))
    {

        /* For complete ISS userspace IDS is present or not is identified by
           using the IDS_WANTED switch.

           If ISS is in kernel we will identify the IDS is present or not by using
           the SECURITY_KERNEL_WANTED switch and  gu4SnortModule variable
           (This will be initialized only if IDS process is registered with the
           security character device) */

        if ((SecUtilGetIdsStatus ()) == IDS_ENABLE)
        {
#ifdef IDS_WANTED
            if (IDS_FAILURE == SecIdsEnqueueDataFrame (pBuf,
                                                       DATA_FROM_SEC_TO_IDS,
                                                       u4IfIndex))
            {
                return OSIX_FAILURE;
            }
            return OSIX_SUCCESS;
#endif

#ifdef SECURITY_KERNEL_WANTED
#ifndef IDS_WANTED
            /* When PME is not available in the hardware, packet will be sent to SNORT in
             * * user space for processing against the rules and logging
             * */
            if (gu4SnortModule == SNORT_MODULE)
            {

                if (IDS_FAILURE ==
                        SecIdsEnqueueDataFrame (pBuf,
                                                DATA_FROM_SEC_TO_IDS,
                                                u4IfIndex))
                {
                    return OSIX_FAILURE;
                }
                return OSIX_SUCCESS;
            }
#else
            /* Log immediately without checking snort rules if the received packet is malformed packet
             * Pass to PME module for processing the packet based on the rules
             * Enqueue the Packets to SEC module when logging is required for the packet.
             */
            if (gu4SnortModule == SNORT_MODULE)
            {
                if (SecDecodeEthPkt (pBuf, &u4DataOffset) == OSIX_FAILURE)
                {
                    if (IDS_FAILURE ==
                        SecIdsEnqueueDataFrame (pBuf,
                                                DATA_FROM_SEC_TO_IDS,
                                                u4IfIndex))
                    {
                        return OSIX_FAILURE;
                    }
                    return OSIX_SUCCESS;
                }

                i4Retval = PmeScanMain (pBuf, u4DataOffset);

                if (i4Retval == -1)
                {
                    SEC_TRC (SEC_CONTROL_PLANE_TRC,
                             "PmeScanMain returned Failure\r\n");
                    return OSIX_FAILURE;
                }
                else if (i4Retval > 0)
                {
                    if (IDS_FAILURE ==
                        SecIdsEnqueueDataFrame (pBuf,
                                                DATA_FROM_SEC_TO_IDS,
                                                u4IfIndex))
                    {
                        return OSIX_FAILURE;
                    }
                    return OSIX_SUCCESS;
                }
            }
#endif
#endif
        }
    }

    if (OSIX_FAILURE ==
        SecUtilSendDataPktToCfa (pBuf, u4IfIndex, u2Protocol, pIpHdr))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name      : SecPortFsNpGetDummyVlanId                            */
/*                                                                           */
/* Description        : This API is porting API to get the dummy vlan ID     */
/*                      for given interface index                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : Dummy Vlan Id                                        */
/*                                                                           */
/*****************************************************************************/

UINT4
SecPortFsNpGetDummyVlanId (UINT4 u4IfIndex)
{
    UINT4               u4VlanId = 0;
#ifndef SECURITY_KERNEL_WANTED
#if defined(BCMX_WANTED) || defined(XCAT)
    u4VlanId = FsNpGetDummyVlanId (u4IfIndex);
#else
    UNUSED_PARAM (u4VlanId);
    UNUSED_PARAM (u4IfIndex);
#endif
#else
    UNUSED_PARAM (u4VlanId);
    UNUSED_PARAM (u4IfIndex);
#endif
    return u4VlanId;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecPortSecUtilGetGlobalStatus                    */
/*                                                                           */
/*    Description         : This API is porting API to                       */
/*                          fetch the global IPSEC status                    */
/*                                                                           */
/*    Input(s)            : NONE                                             */
/*                                                                           */
/*    Output(s)           : NONE                                             */
/*                                                                           */
/*    Returns             : SEC_ENABLE/SEC_DISABLE                           */
/*                                                                           */
/*****************************************************************************/

INT1
SecPortSecUtilGetGlobalStatus (VOID)
{
#ifdef IPSECv4_WANTED
    return (SecUtilGetGlobalStatus ());
#else
    return SEC_DISABLE;
#endif
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecPortFwlAclProcessPktForInFiltering            */
/*                                                                           */
/*    Description         : This API is porting API to inspect the packet    */
/*                          coming in through an interface against the       */
/*                          configured InFilters by the AccessList.          */
/*                                                                           */
/*    Input(s)            : pBuf         -- Pointer to the incoming pkt      */
/*                          u4OutIfaceNum -- Incoming interface number       */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : SUCCESS if the packet is to permitted,           */
/*                          otherwise FAILURE.                               */
/*                                                                           */
/*****************************************************************************/

UINT4
SecPortFwlAclProcessPktForInFiltering (tCRU_BUF_CHAIN_HEADER * pBuf,
                                       UINT4 u4InIfaceNum)
{
#ifdef FIREWALL_WANTED
    return (FwlAclProcessPktForInFiltering (pBuf, u4InIfaceNum));
#else
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4InIfaceNum);
    return FWL_SUCCESS;
#endif /* FIREWALL_WANTED */
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecPortFwlAclProcessPktForOutFiltering           */
/*                                                                           */
/*    Description         : This API is porting API to inspect the packet    */
/*                          going out through an interface against the       */
/*                          configured OutFilters by the AccessList.         */
/*                                                                           */
/*    Input(s)            : pBuf         -- Pointer to the outgoing pkt      */
/*                          u4OutIfaceNum -- Outgoing interface number       */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : SUCCESS if the packet is to permitted,           */
/*                          otherwise FAILURE.                               */
/*                                                                           */
/*****************************************************************************/

UINT4
SecPortFwlAclProcessPktForOutFiltering (tCRU_BUF_CHAIN_HEADER * pBuf,
                                        UINT4 u4OutIfaceNum)
{
#ifdef FIREWALL_WANTED
    return (FwlAclProcessPktForOutFiltering (pBuf, u4OutIfaceNum));
#else
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4OutIfaceNum);
    return FWL_SUCCESS;
#endif /* FIREWALL_WANTED */
}

/*****************************************************************************/
/*                                                                           */
/*  Function Name  : SecPortNatTranslateInBoundPkt                           */
/*                                                                           */
/*  Description    : This API is porting API to                              */
/*                   translate the Inbound packet if Global NAT is           */
/*                   enabled and if the packet needs translation.            */
/*  Input (s)      : 1. pBuf - This stores the full IP packet along          */
/*                   with TCP/UDP header and payload.                        */
/*                   2. u4InIf - This gives the incoming interface number    */
/*                                                                           */
/*  Output (s)     : Either                                                  */
/*                    modified IP Packet with all references to Global IP    */
/*                    and port translated to local IP and port               */
/*                   Or                                                      */
/*                    unmodified IP Packet which is to be just forwarded.    */
/*                   and  Variable pi1FrgReass  showing if the fragments     */
/*                   are reassembled or not, when the fragments are received */
/*                                                                           */
/*  Returns        : SUCCESS if the packet is to be forwarded to the Inside  */
/*                   Network irrespctive of whether the packet "needs        */
/*                   translation" or not.                                    */
/*                   FAILURE if the packet is to be dropped by the calling   */
/*                   module.This happens when the packet needs to be         */
/*                   translated but NAT is unable to do it.                  */
/*                                                                           */
/*****************************************************************************/

INT4
SecPortNatTranslateInBoundPkt (tCRU_BUF_CHAIN_HEADER ** ppBuf, UINT4 u4InIf,
                               INT1 *pi1FrgReass)
{
#ifdef NAT_WANTED
    return (NatTranslateInBoundPkt (ppBuf, u4InIf, pi1FrgReass));
#else
    UNUSED_PARAM (pi1FrgReass);
    UNUSED_PARAM (ppBuf);
    UNUSED_PARAM (u4InIf);
    return SUCCESS;
#endif /* NAT_WANTED */

}

/*****************************************************************************/
/*                                                                           */
/*  Function Name  : SecPortNatTranslateOutBoundPkt                          */
/*                                                                           */
/*  Description    : This API is porting API to                              */
/*                   translate the Outbound packet if Global NAT is          */
/*                   enabled and if the packet needs translation.            */
/*  Input (s)      : 1. pBuf - This stores the full IP packet along          */
/*                   with TCP/UDP header and payload.                        */
/*                   2. u4InIf - This gives the outgoing interface number    */
/*                                                                           */
/*  Output (s)     : Either                                                  */
/*                    modified IP Packet with all references to local IP     */
/*                    and port translated to Global IP and port              */
/*                   Or                                                      */
/*                    unmodified IP Packet which is to be just forwarded.    */
/*                   and  Variable pi1FrgReass  showing if the fragments     */
/*                   are reassembled or not, when the fragments are received */
/*                                                                           */
/*  Returns        : SUCCESS if the packet is to be forwarded to the Outside */
/*                   Network irrespctive of whether the packet "needs        */
/*                   translation" or not.                                    */
/*                   FAILURE if the packet is to be dropped by the calling   */
/*                   module.This happens when the packet needs to be         */
/*                   translated but NAT is unable to do it.                  */
/*                   FAILURE - when all the fragments are not received       */
/*                                                                           */
/*****************************************************************************/

INT4
SecPortNatTranslateOutBoundPkt (tCRU_BUF_CHAIN_HEADER ** ppBuf, UINT4 u4OutIf,
                                INT1 *pi1FrgReass)
{
#ifdef NAT_WANTED
    return (NatTranslateOutBoundPkt (ppBuf, u4OutIf, pi1FrgReass));
#else
    UNUSED_PARAM (pi1FrgReass);
    UNUSED_PARAM (ppBuf);
    UNUSED_PARAM (u4OutIf);
    return SUCCESS;
#endif /* NAT_WANTED */
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecPortNatLock                                   */
/*                                                                           */
/*    Description         : This API is porting API to take the              */
/*                          NAT protocol Sema4 to avoid simultaneous access  */
/*                          access to the NAT database                       */
/*                                                                           */
/*    Input(s)            : NONE                                             */
/*                                                                           */
/*    Output(s)           : NONE                                             */
/*                                                                           */
/*    Returns             : NAT_SUCCESS/NAT_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
SecPortNatLock (VOID)
{
#ifdef NAT_WANTED
    return (NatLock ());
#else
    return NAT_SUCCESS;
#endif /* NAT_WANTED */
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecPortNatUnLock                                 */
/*                                                                           */
/*    Description         : This API is porting API to give the              */
/*                          NAT protocol Sema4 to avoid simultaneous access  */
/*                          access to the NAT database                       */
/*                                                                           */
/*    Input(s)            : NONE                                             */
/*                                                                           */
/*    Output(s)           : NONE                                             */
/*                                                                           */
/*    Returns             : NAT_SUCCESS/NAT_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
SecPortNatUnLock (VOID)
{
#ifdef NAT_WANTED
    return (NatUnLock ());
#else
    return NAT_SUCCESS;
#endif /* NAT_WANTED */
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecPortNatUtilGetGlobalNatStatus                 */
/*                                                                           */
/*    Description         : This API is porting API to                       */
/*                          fetch the global NAT status                      */
/*                                                                           */
/*    Input(s)            : NONE                                             */
/*                                                                           */
/*    Output(s)           : NONE                                             */
/*                                                                           */
/*    Returns             : NAT_ENABLE/NAT_DISABLE                           */
/*                                                                           */
/*****************************************************************************/

INT4
SecPortNatUtilGetGlobalNatStatus (VOID)
{
#ifdef NAT_WANTED
    return (NatUtilGetGlobalNatStatus ());
#else
    return NAT_DISABLE;
#endif /* NAT_WANTED */
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecPortNatCheckIfNatEnable                       */
/*                                                                           */
/*    Description         : This API is porting API to                       */
/*                          fetch the NAT status for the particular interface*/
/*                                                                           */
/*    Input(s)            : u4IfNum : Interface Index                        */
/*                                                                           */
/*    Output(s)           : NONE                                             */
/*                                                                           */
/*    Returns             : NAT_ENABLE/NAT_DISABLE                           */
/*                                                                           */
/*****************************************************************************/

UINT4
SecPortNatCheckIfNatEnable (UINT4 u4IfNum)
{
#ifdef NAT_WANTED
    return (NatCheckIfNatEnable (u4IfNum));
#else
    UNUSED_PARAM (u4IfNum);
    return NAT_DISABLE;
#endif /* NAT_WANTED */
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecPortVpnGetTunnelEndpoint                      */
/*                                                                           */
/*    Description         : This API is porting API to                       */
/*                          fetch the Tunnel endpoint using the configured SA*/
/*                                                                           */
/*    Input(s)            : u2PolicyIndex, u4SrcAddr,u4DestAddr, u4Protocol  */
/*                          pu4TunFlag, pu4RuleFlag                          */
/*                                                                           */
/*    Output(s)           : NONE                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS/SEC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

UINT4
SecPortVpnGetTunnelEndpoint (UINT2 u2PolicyIndex, UINT4 u4SrcAddr,
                             UINT4 u4DestAddr, UINT4 u4Protocol,
                             UINT4 *pu4TunFlag, UINT4 *pu4RuleFlag,
                             UINT4 *pu4Src, UINT4 *pu4Dest)
{
#ifdef VPN_WANTED
    return (VpnGetTunnelEndpoint (u2PolicyIndex, u4SrcAddr,
                                  u4DestAddr, u4Protocol,
                                  pu4TunFlag, pu4RuleFlag, pu4Src, pu4Dest));
#else
    UNUSED_PARAM (u2PolicyIndex);
    UNUSED_PARAM (u4SrcAddr);
    UNUSED_PARAM (u4DestAddr);
    UNUSED_PARAM (u4Protocol);
    UNUSED_PARAM (pu4TunFlag);
    UNUSED_PARAM (pu4RuleFlag);
    UNUSED_PARAM (pu4Src);
    UNUSED_PARAM (pu4Dest);
    return SEC_SUCCESS;
#endif /* VPN_WANTED */
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecPortSecv4InProcess                            */
/*                                                                           */
/*    Description         : This API is porting API to                       */
/*                          invoke the respective function of IPSEC          */
/*                                                                           */
/*    Input(s)            : pCBuf - Buffer contains IP Secured packet.       */
/*                          u2Port - Interface on which the Packet arrives.  */
/*                                                                           */
/*    Output(s)           : pCBuf - Buffer contains Decoded IP packet.       */
/*                                                                           */
/*    Return Values       : OSIX_SUCCESS or SEC_FAILURE or SEC_BYPASS        */
/*                                                                           */
/*****************************************************************************/

UINT1
SecPortSecv4InProcess (tIP_BUF_CHAIN_HEADER ** ppBuf, UINT4 u4Port)
{
#ifdef IPSECv4_WANTED
    return (Secv4InProcess (ppBuf, u4Port));
#else
    UNUSED_PARAM (ppBuf);
    UNUSED_PARAM (u4Port);
    return SEC_BYPASS;
#endif /* VPN_WANTED */
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecPortSecv4OutProcess                           */
/*                                                                           */
/*    Description         : This API is porting API to                       */
/*                          invoke the respective function of IPSEC          */
/*                                                                           */
/*    Input(s)            : pCBuf - Buffer contains IP packet.               */
/*                          u2Port - Interface on which the Packet to be sent*/
/*                                                                           */
/*    Output(s)           : pCBuf - Buffer contains secured IP packet.       */
/*                                                                           */
/*    Return Values       : SEC_SUCCESS or SEC_FAILURE or SEC_BYPASS         */
/*                                                                           */
/*****************************************************************************/

UINT1
SecPortSecv4OutProcess (tIP_BUF_CHAIN_HEADER * pCBuf, UINT2 u2Port)
{
#ifdef IPSECv4_WANTED
    return (Secv4OutProcess (pCBuf, u2Port));
#else
    UNUSED_PARAM (pCBuf);
    UNUSED_PARAM (u2Port);
    return SEC_SUCCESS;
#endif /* VPN_WANTED */
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecPortIpUpdateInIfaceErrInCxt                   */
/*                                                                           */
/*    Description         : This API is porting API to                       */
/*                          Increment the error count for in interface       */
/*                          error in the specified context.                  */
/*                                                                           */
/*    Input(s)            : u4ContextId                                      */
/*                                                                           */
/*    Output(s)           : NONE                                             */
/*                                                                           */
/*    Returns             : NONE                                             */
/*                                                                           */
/*****************************************************************************/

VOID
SecPortIpUpdateInIfaceErrInCxt (UINT4 u4CxtId)
{
#ifdef IP_WANTED
    IpUpdateInIfaceErrInCxt (u4CxtId);
#else
    UNUSED_PARAM (u4CxtId);
#endif
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecPortIpUpdateOutIfaceErrInCxt                  */
/*                                                                           */
/*    Description         : This API is porting API to                       */
/*                          Increment the error count for out interface      */
/*                          error in the specified context.                  */
/*                                                                           */
/*    Input(s)            : u4ContextId                                      */
/*                                                                           */
/*    Output(s)           : NONE                                             */
/*                                                                           */
/*    Returns             : NONE                                             */
/*                                                                           */
/*****************************************************************************/

VOID
SecPortIpUpdateOutIfaceErrInCxt (UINT4 u4ContextId)
{
#ifdef IP_WANTED
    IpUpdateOutIfaceErrInCxt (u4ContextId);
    return;
#else
    UNUSED_PARAM (u4ContextId);
    return;
#endif
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecPortVcmGetContextIdFromCfaIfIndex             */
/*                                                                           */
/*    Description         : This API is porting API to get context id        */
/*                          associated with the given cfa ifIndex            */
/*                                                                           */
/*    INPUT               : u4CfaIfIndex  - Cfa interface index              */
/*                                                                           */
/*    OUTPUT              : *pu4CxtId - Context id of the interface          */
/*                                      having cfa IfIndex as u4CfaIfIndex   */
/*                                                                           */
/*    RETURNS             : VCM_SUCCESS    - If interface is found and the   */
/*                                          context is associated            */
/*                                          with that interface is returned  */
/*                          VCM_FAILURE    - Otherwise                       */
/*                                                                           */
/*****************************************************************************/

INT4
SecPortVcmGetContextIdFromCfaIfIndex (UINT4 u4CfaIfIndex, UINT4 *pu4CxtId)
{
#ifdef VCM_WANTED
    return (VcmGetContextIdFromCfaIfIndex (u4CfaIfIndex, pu4CxtId));
#else
    UNUSED_PARAM (u4CfaIfIndex);
    UNUSED_PARAM (pu4CxtId);
    return VCM_SUCCESS;
#endif /* VCM_WANTED */
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlLogMessage                                    */
/*                                                                          */
/*    Description        : This function logs the information about packets */
/*                         denied.                                          */
/*                                                                          */
/*    Input(s)           : u4AttackType -- Type of Attack                   */
/*                         u4SrcAddr -- Source Address                      */
/*                         u4DestAddr -- Destn Address                      */
/*                         u4IfaceNum -- Interface Number                   */
/*                         u1Protocol -- Protocol number                    */
/*                         u2SrcPort -- Destination Port number             */
/*                         u2DestPort -- Destination Port number            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/
VOID
SecPortFwlLogMessage (UINT4 u4AttackType,
                      UINT4 u4IfaceNum,
                      tCRU_BUF_CHAIN_HEADER * pBuf,
                      UINT4 u4LogLevel, UINT4 u4Severity, UINT1 *pMsg)
{
#ifdef FIREWALL_WANTED
    FwlLogMessage (u4AttackType, u4IfaceNum, pBuf, u4LogLevel,
                   u4Severity, pMsg);
    return;
#else
    UNUSED_PARAM (u4AttackType);
    UNUSED_PARAM (u4IfaceNum);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4LogLevel);
    UNUSED_PARAM (u4Severity);
    UNUSED_PARAM (pMsg);
    return;
#endif
}

/**************************************************************************/
/*                                                                        */
/*  Function Name      :SecPortFwlSndThresholdExceedTrp                   */
/*                                                                        */
/*  Description        : This function generates a TRAP when the Interface*/
/*                       or Global Packet Discard Count exceeds the       */
/*                       Threshold set                                    */
/*                                                                        */
/*  Input(s)           : u4IfIndex - Interface Index                      */
/*                       u4PktCount - Total Denied Packets Count          */
/*                                                                        */
/*  Output(s)          : None                                             */
/*                                                                        */
/*  Returns            : FWL_FAILURE/FWL_SUCCESS                          */
/**************************************************************************/
INT4
SecPortFwlSndThresholdExceedTrp (UINT4 u4IfIndex, UINT4 u4PktCount)
{
#ifdef FIREWALL_WANTED
    return (FwlSendThresholdExceededTrap (u4IfIndex, u4PktCount));
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PktCount);
    return FWL_SUCCESS;
#endif
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SecPortFwlGenerateMemFailureTrap                 */
/*                                                                          */
/*    Description        : This function generates a TRAP when memory       */
/*                         Failure occurs. This Trap contains a information */
/*                         about the allocation Failure.                    */
/*                                                                          */
/*    Input(s)           : u1NodeName - This vaulue specified during which  */
/*                                     allocation the failure has occurred  */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
SecPortFwlGenerateMemFailureTrap (UINT1 u1NodeName)
{
#ifdef FIREWALL_WANTED
    FwlGenerateMemFailureTrap (u1NodeName);
    return;
#else
    UNUSED_PARAM (u1NodeName);
    return;
#endif
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SecPortFwlGenerateAttackSumTrap                  */
/*                                                                          */
/*    Description        : This function generates a TRAP when number of    */
/*                         firewall attacks exceeded the limit              */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
SecPortFwlGenerateAttackSumTrap (VOID)
{
#ifdef FIREWALL_WANTED
    FwlGenerateAttackSumTrap ();
#endif
    return;
}

/******************************************************************************
 * Function Name      : SecPortIdsSendAttackPktTrap
 *
 * Description        : This function is used to send IDS notifications
 *                      when attack-packet is identified in IDS
 *
 * Input(s)           : pu1BlackListIpAddr - BlackListed Ip address of the
 *                                           attack-packet identified in IDS.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
SecPortIdsSendAttackPktTrap (UINT1 *pu1BlackListIpAddr)
{
#ifdef FIREWALL_WANTED
    IdsSendAttackPktTrap (pu1BlackListIpAddr);
#else
    UNUSED_PARAM (pu1BlackListIpAddr);
#endif
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SecPortFwlErrorMessageGenerate                   */
/*                                                                          */
/*    Description        : Calls the ICMP module to generate the Error      */
/*                         message of Communication Administratively        */
/*                         Prohibited.                                      */
/*                                                                          */
/*    Input(s)           : pBuf         -- Pointer to the packet            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/*                                                                          */
/****************************************************************************/
INT1
SecPortFwlErrorMessageGenerate (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex)
{
#ifdef FIREWALL_WANTED
    return (FwlErrorMessageGenerate (pBuf, u4IfIndex));
#else
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4IfIndex);
    return FWL_SUCCESS;
#endif
}

/******************************************************************************/
/*  Function Name : SecPortSecv4SendIcmpErrMsg                                */
/*  Description   : This fuction Sends out the ICMP error message             */
/*  Input(s)      : pBuf  -  Actual Pkt                                       */
/*                : pIcmp - Pointer to the ICMP Sturcture                     */
/*                : i1flag - Flag to indicate the presence of IP options      */
/*  Output(s)     : None                                                      */
/*  Return Values : None                                                      */
/******************************************************************************/
VOID
SecPortSecv4SendIcmpErrMsg (tCRU_BUF_CHAIN_HEADER * pBuf, t_ICMP * pIcmp)
{
#ifdef IPSECv4_WANTED
    Secv4SendIcmpErrMsg (pBuf, pIcmp);
    return;
#else
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pIcmp);
    return;
#endif
}

/*****************************************************************************/
/* Function Name      : SecPortHandleLogMessages                             */
/*                                                                           */
/* Description        : This function is invoked from any module for sending */
/*                      frames for logging into Ids log file, this function  */
/*                      post this event to the SEC-IDS task if IDS is        */
/*                      definded otherwise logging will not happen           */
/*                                                                           */
/* Input(s)           : pBuf - data Frame                                    */
/*                      u4MesgType -Message Type, can be                     */
/*                      SEC_TO_IDS -posting data from secutity to IDS thread */
/*                      IDS_TO_CFA -posting data from IDS to cfa task        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : IDS_SUCCESS - On success.                            */
/*                      IDS_FAILURE - On failure.                            */
/*****************************************************************************/
INT4
SecPortHandleLogMessages (tCRU_BUF_CHAIN_HEADER * pBuf)
{
#ifdef IDS_WANTED
#ifdef SECURITY_KERNEL_MAKE_WANTED
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
    UINT1              *pu1Frame = NULL;
    INT4                i4IcmpGenerate = FWL_DISABLE;
    UINT4               u4IfIndex = 0;
    UINT4               u4BufSize = 0;

    i4IcmpGenerate = FwlUtilGetIcmpControlStatus ();

    if (FWL_ENABLE == i4IcmpGenerate)
    {
        u4BufSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
        if (u4BufSize == 0)
        {
            return OSIX_FAILURE;
        }

        /* Allocate packet buffer from the pool */
        if ((pu1Frame = MemAllocMemBlk (IDS_ATTACk_PKT_POOL_ID)) == NULL)
        {
            IDS_TRC (IDS_RESOURCE_TRC | IDS_FAILURE_TRC,
                     "SecPortHandleLogMessages:"
                     "IDS Attack Packet Buffer memory Allocation Failed !!!\r\n");
            return OSIX_FAILURE;
        }

        u4IfIndex = SEC_GET_IFINDEX (pBuf);

        /* allocate CRU buffer and duplicate the contents in the pDupBuf.
         * This pDupBuf is used for sending the icmp error message to the 
         * host, when icmp generate option is enabled in firewall. 
         */
        pDupBuf = (tCRU_BUF_CHAIN_HEADER *)
            CRU_BUF_Allocate_MsgBufChain (u4BufSize, 0);
        if (NULL == pDupBuf)
        {
            MemReleaseMemBlock (IDS_ATTACk_PKT_POOL_ID, (UINT1 *) pu1Frame);
            return OSIX_FAILURE;
        }

        CRU_BUF_Copy_FromBufChain (pBuf, pu1Frame, 0, u4BufSize);
        CRU_BUF_Copy_OverBufChain (pDupBuf, pu1Frame, 0, u4BufSize);
        CRU_BUF_Move_ValidOffset (pDupBuf,
                                  VLAN_TAGGED_HEADER_SIZE + sizeof (UINT2));

        /* Send ICMP error message, if Firewall icmp generate option is enabled. */
        SecPortFwlErrorMessageGenerate (pDupBuf, u4IfIndex);
        MemReleaseMemBlock (IDS_ATTACk_PKT_POOL_ID, (UINT1 *) pu1Frame);
        /* Release the buffer once the error msg generation is done. */
        CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);

    }
#endif /* SECURITY_KERNEL_MAKE_WANTED */
    if (IDS_FAILURE == SecIdsEnqueueCtrlMsg (pBuf, CTRLMSG_FRM_ISS_TO_IDS))
    {
        return OSIX_FAILURE;
    }

#else
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
#endif /* IDS_WANTED */
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecPortSecv6InProcess                            */
/*                                                                           */
/*    Description         : This API is porting API to                       */
/*                          invoke the respective function of IPSECv6        */
/*                                                                           */
/*    Input(s)            : pCBuf - Buffer contains IPv6 Secured packet.     */
/*                          u4Port - Interface on which the Packet arrives.  */
/*                                                                           */
/*    Output(s)           : pCBuf - Buffer contains Decoded IP packet.       */
/*                                                                           */
/*    Return Values       : OSIX_SUCCESS or SEC_FAILURE                      */
/*                                                                           */
/*****************************************************************************/

UINT1
SecPortSecv6InProcess (tIP_BUF_CHAIN_HEADER * pBuf, UINT4 u4Port)
{
#ifdef IPSECv6_WANTED
    tIp6If              If6;
    tIp6Hdr             Ip6Hdr;
    tIp6Hdr            *pIp6Hdr = NULL;
    UINT4               u4Len;
    UINT1              *pNxtHdr = NULL;

    MEMSET (&If6, 0, sizeof (tIp6If));
    pIp6Hdr =
        (tIp6Hdr *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0,
                                                           IPV6_HEADER_LEN);
    if (pIp6Hdr == NULL)
    {
        pIp6Hdr = &Ip6Hdr;
        MEMSET (&Ip6Hdr, 0, sizeof (tIp6Hdr));
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &Ip6Hdr, 0, IPV6_HEADER_LEN);
    }

    If6.u4Index = u4Port;
    pNxtHdr = &pIp6Hdr->u1Nh;
    u4Len = sizeof (tIp6Hdr);

    return (Secv6InProcess (pBuf, &If6, pNxtHdr, &u4Len));
#else
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4Port);
    return SEC_SUCCESS;
#endif /* VPN_WANTED */
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecPortSecv6OutProcess                           */
/*                                                                           */
/*    Description         : This API is porting API to                       */
/*                          invoke the respective function of IPSECv6        */
/*                                                                           */
/*    Input(s)            : pCBuf - Buffer contains IPv6 packet.             */
/*                          u2Port - Interface on which the Packet to be sent*/
/*                                                                           */
/*    Output(s)           : pCBuf - Buffer contains secured IPv6 packet.     */
/*                                                                           */
/*    Return Values       : SEC_SUCCESS or SEC_FAILURE                       */
/*                                                                           */
/*****************************************************************************/

UINT1
SecPortSecv6OutProcess (tIP_BUF_CHAIN_HEADER * pCBuf, UINT4 u4Port)
{
#ifdef IPSECv6_WANTED
    tIp6If              If6;
    UINT4               u4Len;

    MEMSET (&If6, 0, sizeof (tIp6If));

    If6.u4Index = u4Port;
    return (Secv6OutProcess (pCBuf, &If6, &u4Len));
#else
    UNUSED_PARAM (pCBuf);
    UNUSED_PARAM (u4Port);
    return SEC_SUCCESS;
#endif /* VPN_WANTED */
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecPortSecv6UtilGetGlobalStatus                  */
/*                                                                           */
/*    Description         : This API is porting API to                       */
/*                          fetch the global IPSECv6 status                  */
/*                                                                           */
/*    Input(s)            : NONE                                             */
/*                                                                           */
/*    Output(s)           : NONE                                             */
/*                                                                           */
/*    Returns             : SEC_ENABLE/SEC_DISABLE                           */
/*                                                                           */
/*****************************************************************************/

INT1
SecPortSecv6UtilGetGlobalStatus (VOID)
{
#ifdef IPSECv6_WANTED
    return (Secv6UtilGetGlobalStatus ());
#else
    return SEC_DISABLE;
#endif
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecPortVpnGetTunnelEndpointForIp6                */
/*                                                                           */
/*    Description         : This API is porting API to                       */
/*                          fetch the Tunnel endpoint using the configured SA*/
/*                                                                           */
/*    Input(s)            : pIp6SrcAddr, pIp6DestAddr, u4Protocol            */
/*                          pu4TunFlag, pu4RuleFlag                          */
/*                                                                           */
/*    Output(s)           : NONE                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS/SEC_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

UINT4
SecPortVpnGetTunnelEndpointForIp6 (tIp6Addr * pIp6SrcAddr,
                                   tIp6Addr * pIp6DestAddr, UINT4 u4Protocol,
                                   UINT4 *pu4TunFlag, UINT4 *pu4RuleFlag)
{
#ifdef VPN_WANTED
    return (VpnGetTunnelEndpointForIp6 (pIp6SrcAddr, pIp6DestAddr, u4Protocol,
                                        pu4TunFlag, pu4RuleFlag));
#else
    UNUSED_PARAM (pIp6SrcAddr);
    UNUSED_PARAM (pIp6DestAddr);
    UNUSED_PARAM (u4Protocol);
    UNUSED_PARAM (pu4TunFlag);
    UNUSED_PARAM (pu4RuleFlag);
    return SEC_SUCCESS;
#endif /* VPN_WANTED */
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecPortIp6UpdateIfaceInDiscards                  */
/*                                                                           */
/*    Description         : This API is porting API to                       */
/*                          Increment the Indiscard count for an specified   */
/*                          interface.                                       */
/*                                                                           */
/*    Input(s)            : u4IfIndex                                        */
/*                                                                           */
/*    Output(s)           : NONE                                             */
/*                                                                           */
/*    Returns             : NONE                                             */
/*                                                                           */
/*****************************************************************************/

VOID
SecPortIp6UpdateIfaceInDiscards (UINT4 u4IfIndex)
{
#if !defined LNXIP6_WANTED && defined IP6_WANTED
    /* Only when FSIP + IPv6 enabled */
    Ip6UpdateIfaceInDiscards (u4IfIndex);
#else
    UNUSED_PARAM (u4IfIndex);
#endif
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecPortIp6UpdateIfaceOutDiscards                 */
/*                                                                           */
/*    Description         : This API is porting API to                       */
/*                          Increment the Outdiscard count for an specified  */
/*                          interface.                                       */
/*                                                                           */
/*    Input(s)            : u4IfIndex                                        */
/*                                                                           */
/*    Output(s)           : NONE                                             */
/*                                                                           */
/*    Returns             : NONE                                             */
/*                                                                           */
/*****************************************************************************/

VOID
SecPortIp6UpdateIfaceOutDiscards (UINT4 u4IfIndex)
{
#if !defined LNXIP6_WANTED && defined IP6_WANTED
    /* Only when FSIP + IPv6 enabled */
    Ip6UpdateIfaceOutDiscards (u4IfIndex);
#else
    UNUSED_PARAM (u4IfIndex);
#endif
}

/****************************************************************************
* Function     : SecPortArpResolve
*
* Description  : To check whether the entry is available in the Arp cache.
*
* Input        : u4IpAddr - IpAddress to be resolved.
*
* Output       : pi1Hw_addr -Hardware address.
*                pu1EncapType - EncapType for that Entry.
*
* Returns      : ARP_SUCCESS/ARP_FAILURE
*
***************************************************************************/
INT1
SecPortArpResolve (UINT4 u4IpAddr, INT1 *pi1Hw_addr, UINT1 *pu1EncapType)
{
#ifndef SECURITY_KERNEL_WANTED
    if (ArpResolve (u4IpAddr, pi1Hw_addr, pu1EncapType) == ARP_FAILURE)
    {
        return SEC_FAILURE;
    }
#else
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (pi1Hw_addr);
    UNUSED_PARAM (pu1EncapType);
#endif
    return SEC_SUCCESS;
}

#endif /* _SECPORT_C_ */
