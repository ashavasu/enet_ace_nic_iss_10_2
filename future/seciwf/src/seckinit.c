/*****************************************************************************/
/* Copyright (C) 2011 Aricent Inc . All Rights Reserved                      */
/*                                                                           */
/* $Id: seckinit.c,v 1.12 2015/11/20 10:45:01 siva Exp $                   */
/*                                                                           */
/*  FILE NAME             : seckinit.c                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : Security Module                                  */
/*  MODULE NAME           : Security                                         */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains Security Task related routines*/
/*                          for kernel space                                 */
/*****************************************************************************/
#ifndef _SECKINIT_C_
#define _SECKINIT_C_

#include "secinc.h"
#include "seckinc.h"
#include "sectrc.h"
#include "natfragext.h"

tRBTree             gSecIfIpAddrList;
tRBTree             gSecIfBcastIpAddrList;
tSecIfInfo          gaSecWanIfInfo[SYS_MAX_WAN_INTERFACES];
tSecIfInfo          gaSecLanIfInfo[SYS_MAX_LAN_INTERFACES];
UINT1               gau1RxMsgBuf[SEC_MAX_MSG_BUF_SIZE];
UINT1               gau1TxMsgBuf[SEC_MAX_MSG_BUF_SIZE];
UINT4               gu4SecTrcFlag;
tOsixTaskId         gu4KernCfaTaskId;
tOsixSemId          gKernCfaSemId;
tOsixQId            gKernCfaQueId;

#ifndef IDS_WANTED
UINT4               gu4PrevIdsStatus = IDS_ENABLE;
INT4                gi4IdsRulesStatus = IDS_RULES_LOADED;
#endif

UINT4               gu4IdsStatus;
INT4                SecKCfaUnLock (VOID);
INT4                SecKCfaLock (VOID);

INT4                gi4SecDevFd;    /* Interface between ISS and Security Kernel
                                     * Module 
                                     */
INT4                gi4SysOperMode;

/*"gaSecWanPhyIf" will be used to decide direction in cases of IVR interfaces 
 *and in platforms where the inbound and outbound traffic are 
 *received on the same interface for security processing at Kernel */
INT1                gaSecWanPhyIf[SYS_MAX_WAN_PHY_INTERFACES]; 
/*****************************************************************************
 *
 *    Function Name       : SeckInit
 *
 *    Description         : This function is Used to Initialise Security
 *                               Module in the kernel
 *
 *                               Registers with the character device              
 *                               Adds the callback function with device add pack 
 *
 *
 *    Input(s)            : None
 *
 *    Output(s)           : NONE.
 *
 *    Global Variables Referred : None.                                         
 *                                                                           
 *    Global Variables Modified : None.                                         
 *                                                                           
 *    Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/

INT4
SeckInit (VOID)
{
    INT4 i4Index = 0;
    if (SecDevRegisterChrDevice () == OSIX_FAILURE)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SeckInit:"
                 "Failed to Register character device in kernel !!!\r\n");
        return OSIX_FAILURE;
    }

    if (SeckCfaInit () == OSIX_FAILURE)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SeckInit:"
                 "Failed to initialize the CFA thread in kernel  !!!\r\n");
        return OSIX_FAILURE;
    }

    /* Adds the callback function to the device add pack */
    if (CFA_FAILURE == CfaGddInit ())
    {
        SEC_TRC (SEC_FAILURE_TRC, "SeckInit:"
                 "Failed to register call back function in dev_add_pack !!!\r\n");
        return OSIX_FAILURE;
    }
#ifdef IDS_WANTED
    /* Initialize the PME related stuff */
    if (PmeScanInit () == OSIX_FAILURE)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SeckInit: PME Initialisation failed \r\n");
        return OSIX_FAILURE;
    }
#endif
    /* Initialize the data structures related to Security over bridged packets. */
    MEMSET (&gSecvlanList, 0, sizeof (tSecVlanList));

    /* By default, security over bridged packets is disabled. */
    gu4SecBridgingStatus = CFA_SEC_BRIDGE_DISABLED;

    gi4SecIvrIfIndex = CFA_INVALID_INDEX;

/*Initialize gaSecWanPhyIf while kernel initialization*/
    for( i4Index = 0; i4Index< SYS_MAX_WAN_PHY_INTERFACES; i4Index++ )
    {
         gaSecWanPhyIf[i4Index] = SEC_UNALLOCATED;   
    }        

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : SeckCfaInit
 *
 *    Description         : This function is Used to Initialise CFA
 *                               Module in the kernel
 *
 *                               Spawns a Worker thread for CFA to receives events              
 *
 *
 *    Input(s)            : None
 *
 *    Output(s)           : NONE.
 *
 *    Global Variables Referred : None.                                         
 *                                                                           
 *    Global Variables Modified : None.                                         
 *                                                                           
 *    Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
INT4
SeckCfaInit (VOID)
{

    if (OsixCreateSem (CFA_SEM_NAME, 1, OSIX_LOCAL, &gKernCfaSemId)
        == OSIX_FAILURE)
    {

        return (OSIX_FAILURE);
    }

    gKernCfaQueId = KERN_TO_KERN_Q;
    if (OsixQueCrt (CFA_QUEUE_NAME, 4, 400, &gKernCfaQueId) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (OsixCreateTask (CFA_TASK_NAME,
                        20,
                        OSIX_DEFAULT_STACK_SIZE,
                        (VOID *) SeckCfaMain, 0,
                        OSIX_LOCAL, &gu4KernCfaTaskId) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    return (OSIX_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name       : SeckCfaMain
 *
 *    Description         : This function is Used to Initialise Security
 *                               Module in the kernel
 *
 *                          Receives events and makes appropriate function calls to
 *                          Firewall/Nat/IPSec for processing the events.
 *
 *    Input(s)            : None
 *
 *    Output(s)           : NONE.
 *
 *    Global Variables Referred : None.                                         
 *                                                                           
 *    Global Variables Modified : None.                                         
 *                                                                           
 *    Use of Recursion          : None.                                         
 *
 *    Output(s)           : NONE.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
VOID
SeckCfaMain (VOID *arg)
{
    UINT4               u4EventMask = 0;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tEnetV2Header      *pEthHdr = NULL;
    tEnetV2Header       EthHdr;
    UINT4               u4PktSize;
    UINT4               u4IfIndex;
    UINT1               u1MsgType;
    UINT4               u4IpAddr;
    UINT4               u4VlanOffset = 0;
    UINT2               u2Protocol = 0;
    UINT1               u1Direction;

    UNUSED_PARAM (arg);

    while (1)
    {
        OsixEvtRecv (gu4KernCfaTaskId, (CFA_PACKET_ARRIVAL_EVENT
#ifdef NAT_WANTED
                                        | NAT_TIMER_EXPIRY_EVENT
/* NAPT TIMER */
                                        | NAPT_TIMER_EXPIRY_EVENT
/* END */
/* SIPALG TIMER */
                                        | NAT_SIPALG_TIMER_EXPIRY_EVENT
                                        | NAT_FRAG_TIMER_EXPIRY_EVENT
/* END */
#endif /* NAT_WANTED */
#ifdef FIREWALL_WANTED
                                        | FWL_TIMER_EXPIRY_EVENT
#endif
#ifdef  IPSECv4_WANTED
                                        | IPSEC_TIMER_EXPIRY_EVENT
#endif
                     ), CFA_EVENT_WAIT_FLAGS, &u4EventMask);

#ifdef NAT_WANTED
        if (u4EventMask & NAT_TIMER_EXPIRY_EVENT)
        {
            SecKCfaLock ();
            NatProcessTimeOut ();
            SecKCfaUnLock ();
        }
        /* NAPT TIMER */
        if (u4EventMask & NAPT_TIMER_EXPIRY_EVENT)
        {
            SecKCfaLock ();
            NaptProcessTimeOut ((tTimerListId) NAPT_TIMER_LIST_ID);
            SecKCfaUnLock ();
        }
        /* END */
        /* SIPALG TIMER */
        if (u4EventMask & NAT_SIPALG_TIMER_EXPIRY_EVENT)
        {
            SecKCfaLock ();
            NatSipAlgProcessTimeOut ((tTimerListId) NAT_SIPALG_TIMER_LIST_ID);
            SecKCfaUnLock ();
        }
        /* END */
#endif
#ifdef FIREWALL_WANTED
/* Process the Expiry of the Firewall timer */
        if (u4EventMask & FWL_TIMER_EXPIRY_EVENT)
        {
            SecKCfaLock ();
            FwlTmrHandleTmrExpiry ();
            SecKCfaUnLock ();
        }
#endif

#ifdef IPSECv4_WANTED
        if (u4EventMask & IPSEC_TIMER_EXPIRY_EVENT)
        {
            SecKCfaLock ();
            Secv4ProcessTimeOut ();
            SecKCfaUnLock ();
        }
#endif
        if (u4EventMask & CFA_PACKET_ARRIVAL_EVENT)
        {
            /* we process all the packets from other modules */
            while (OsixQueRecv (gKernCfaQueId, (UINT1 *) &pBuf,
                                OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
            {
                SecKCfaLock ();

                u1MsgType = SEC_GET_MSGTYPE (pBuf);
                u4IfIndex = SEC_GET_IFINDEX (pBuf);

                if (u1MsgType == SEC_MSG_TYPE_ETH_FRAME)
                {
                    u1Direction = SEC_GET_DIRECTION (pBuf);
                    CfaIwfEnetProcessRxFrame ((tCRU_BUF_CHAIN_HEADER *) pBuf,
                                              u4IfIndex, u4PktSize,
                                              u2Protocol, u1Direction);
                }

                if (u1MsgType == SEC_IDS_MSG_TYPE_ETH_FRAME)
                {
                    if (OSIX_SUCCESS ==
                        SecUtilChkIsOurIpAddress (pBuf, NULL, 0))
                    {
                        if (CFA_FAILURE == CfaHandlePktFromSec (pBuf))
                        {
                            SEC_TRC (SEC_CONTROL_PLANE_TRC, "SeckCfaMain:"
                                     "Failed to send packet to CFA !!!\r\n");
                            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        }
                        SecKCfaUnLock ();
                        return;
                    }
                    else
                    {
                        if (CRU_FAILURE == CRU_BUF_Copy_FromBufChain (pBuf,
                                                                      ((UINT1
                                                                        *)
                                                                       (&u2Protocol)),
                                                                      CFA_VLAN_TAG_OFFSET,
                                                                      sizeof
                                                                      (UINT2)))
                        {

                            SEC_TRC (SEC_CONTROL_PLANE_TRC, "SeckCfaMain:"
                                     "Failed to copy the Protocol !!!\r\n");
                            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                            SecKCfaUnLock ();
                            return;
                        }

                        if (CFA_VLAN_PROTOCOL_ID == u2Protocol)
                        {
                            SEC_GET_MODULE_DATA_PTR (pBuf)->u2LenOrType =
                                CFA_VLAN_PROTOCOL_ID;
                        }

                        if (CFA_FAILURE ==
                            CfaIwfEnetProcessTxFrame (pBuf, u4IfIndex, NULL,
                                                      0, u2Protocol,
                                                      CFA_ENCAP_ENETV2))
                        {
                            SEC_TRC (SEC_CONTROL_PLANE_TRC,
                                     "SecDevTaskCallBackFn:"
                                     "Failed to transmit the packet out !!!\r\n");
                            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                            SecKCfaUnLock ();
                            return;
                        }
                    }
                }
                SecKCfaUnLock ();
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecInitDevModInit                                */
/*                                                                           */
/*    Description         : Module Init Function for the kernel space.        */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_SUCCESS for success (0)                      */
/*                         OSIX_FAILURE for failure (1)                      */
/*                                                                           */
/*****************************************************************************/

INT4                __init
SecInitDevModInit (VOID)
{
    CHR1               *pac1CmdArg[1];

    printk (KERN_ALERT "\n ISS Security insmod started \n\r\r");

    pac1CmdArg[0] = "-sk";

    if (OSIX_SUCCESS == LrMain (1, pac1CmdArg))
    {
        return OSIX_SUCCESS;
    }

    SEC_TRC (SEC_FAILURE_TRC, "SeckInit:"
             "Failed to initialize security modules !!!\r\n");
    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecInitDevModDeInit                              */
/*                                                                           */
/*    Description         : Module DeInit Function for the kernel space      */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID                __exit
SecInitDevModDeInit (VOID)
{
    SeckDeInit ();
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SeckDeInit                                       */
/*                                                                           */
/*    Description         : Security Module DeInit Function in the kernel.   */
/*                          This function release all the resources of the   */
/*                          security module.                                 */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
SeckDeInit (VOID)
{
    UINT1               au1FileName[SYS_COMMAND_MAX_LEN];
    MEMSET (au1FileName, 0, SYS_COMMAND_MAX_LEN);
    SNPRINTF (au1FileName, STRLEN ("/dev/ksec"), "/dev/ksec");

#ifdef LINUX_KERNEL_3_3_4VER
     unregister_chrdev (SEC_MAJOR_NUMBER,au1FileName);
#endif


    CfaGddDeInit ();

    /* Shutdown CRU Buffer Manager */
    CRU_BUF_Shutdown_Manager ();

    /* De-Initialize the Timer Manager. */
    TmrTimerShutdown ();

    /* De-Initialize the memory Pool Manager */
    MemShutDownMemPool ();

    Fsap2Shutdown ();

    OsixQueDel (gu4ChrDevModIdx);
    OsixQueDel (gu4DevPutModIdx);

    return;
}

/*****************************************************************************/
/* Function Name      : SecKCfaLock                                          */
/*                                                                           */
/* Description        : This function is used to take the CFA  protocol      */
/*                      SEMa4 to avoid simultaneous access to                */
/*                      CFA database                                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
SecKCfaLock (VOID)
{
    if (OsixSemTake (gKernCfaSemId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecKCfaUnLock                                        */
/*                                                                           */
/* Description        : This function is used to give the CFA                */
/*                      SEMa4 to avoid simultaneous access to                */
/*                      CFA database                                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS or CFA_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
SecKCfaUnLock (VOID)
{
    OsixSemGive (gKernCfaSemId);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : atol                                             */
/*                                                                           */
/*    Description         : Utility function char string to long             */
/*                                                                           */
/*    Input(s)            : CHR1 array                                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            :   respective LONG value                           */
/*                                                                           */
/*****************************************************************************/

FS_LONG
atol (CONST CHR1 * nptr)
{
    FS_LONG             total = 0;
    INT4                minus = 0;

    while (isspace (*nptr))
        nptr++;
    if (*nptr == '+')
        nptr++;
    else if (*nptr == '-')
    {
        minus = 1;
        nptr++;
    }
    while (isdigit (*nptr))
    {
        total *= 10;
        total += (*nptr++ - '0');
    }
    return minus ? -total : total;

}

module_init (SecInitDevModInit);
module_exit (SecInitDevModDeInit);

/*-------------------------------------------------------------------+
 * Function           : CfaGetHLIfIndex
 *                      Returns the Higher layer Interface Index if
 *                      Present
 *
 * Input(s)           : u2LLIfIndex - Lower layer If Index
 *                    : *pu2HLIfIndex - Pointer to the HL If Index
 *
 * Output(s)          : None.
 *
 * Returns            : OSIX_SUCCESS or CfA_FAILURE
------------------------------------------------------------------- */
INT4
CfaGetHLIfIndex (UINT4 u4IfIndex, UINT4 *pu4HLIfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4HLIfIndex);
    return OSIX_FAILURE;
}

/****************************************************************************
* Function    :  CfaGetIfIpAddr
* Description :  Provides the interface index having the given IP
*                Address
*
* Input       :  IfMainIndex - The interface index of the interface 
*                
*
* Output      :  pu4RetValIfIpAddr - Stores Ip information of an interface
*
* Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
CfaGetIfIpAddr (INT4 i4IfMainIndex, UINT4 *pu4RetValIfIpAddr)
{
    INT4                i4Index = 0;

    for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
    {
        if (gaSecWanIfInfo[i4Index].i4IfIndex == i4IfMainIndex)
        {
            *pu4RetValIfIpAddr = gaSecWanIfInfo[i4Index].u4IpAddr;
            return OSIX_SUCCESS;
        }
    }

    for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
    {
        if (gaSecLanIfInfo[i4Index].i4IfIndex == i4IfMainIndex)
        {
            *pu4RetValIfIpAddr = gaSecLanIfInfo[i4Index].u4IpAddr;
            return OSIX_SUCCESS;
        }
    }

    SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecUtilGetIfIpAddr:"
             "Interface not available !!!\r\n");
    return OSIX_FAILURE;
}

/*********************************************************************
*  Function Name :SNMPCheckForNVTChars
*  Description   :This procedure finds if any non-printable characters
*                 present in the given string ( includes special chars,
*                 non-US ascii chars). Useful to check for valid inputs 
*                 for DisplayString Type objects.(as per RFC: 1903).
*
*  Parameter(s)  : pu1TestStr - pointer to string to be validated,
*                  u4Length   - Length upto which the string will be
*                               validated. 
*
*  Return Values : OSIX_SUCCESS - if there are no such characters present.
*                  OSIX_FAILURE - if at all any non-printable characters 
*                                 present in the given string.
*********************************************************************/
UINT4
SNMPCheckForNVTChars (UINT1 *pu1TestStr, INT4 i4Length)
{
    INT4                i4Len = 0;

    for (i4Len = 0; i4Len < i4Length; i4Len++)
    {
        /* Check if the char falls between (0-127) decimal and (32-126) US ASCII.
         */
        if (pu1TestStr[i4Len] > 0x7F)
        {
            /* *pu4ErrorCode = SNMP_ERR_WRONG_VALUE; */
            return OSIX_FAILURE;
        }
        /* Check if always CR (0x0D) followed by LF (0x0A) or
         * CR followed by NUL (0x00).So if only CR is received,
         * return ERROR.
         */
        if (pu1TestStr[i4Len] == 0x0D)
        {
            if ((i4Len + 1) == i4Length)
            {
                return OSIX_FAILURE;
            }
            if ((pu1TestStr[i4Len + 1] != 0x0A) && (pu1TestStr[i4Len + 1] != 0))
            {
                /* *pu4ErrorCode = SNMP_ERR_WRONG_VALUE; */
                return OSIX_FAILURE;
            }
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGetIfInfo
 *
 *    Description          : This function returns the interface related params
 *                           assigned to this interface to the security module.
 *
 *    Input(s)             : u4IfIndex, *pIfInfo
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred :None
 *
 *    Global Variables Modified :None
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns            :CFA_SUCCESS if u4IfIndex is valid
 *                        CFA_FAILURE if u4IfIndex is Invalid
 *****************************************************************************/
INT4
CfaGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    INT4                i4Index = 0;

    if (u4IfIndex > SYS_MAX_INTERFACES || u4IfIndex == 0)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecUtilGetIfInfo:"
                 "Interface Index is not valid !!!\r\n");
        return CFA_FAILURE;
    }
    for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
    {
        if (gaSecWanIfInfo[i4Index].i4IfIndex == (INT4) u4IfIndex)
        {
            /* gaSecWanIfInfo[i4Index].u1IfNwType; */
            pIfInfo->u1WanType = gaSecWanIfInfo[i4Index].u1WanType;
            pIfInfo->u1NwType = gaSecWanIfInfo[i4Index].u1NwType;
            pIfInfo->u4IfMtu = gaSecWanIfInfo[i4Index].u4Mtu;
            pIfInfo->u1IfOperStatus = gaSecWanIfInfo[i4Index].u1OperStatus;
            pIfInfo->u1IfType = gaSecWanIfInfo[i4Index].u1IfType;
            STRCPY (pIfInfo->au1IfName, gaSecWanIfInfo[i4Index].au1InfName);
            return CFA_SUCCESS;
        }
    }
    for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
    {
        if (gaSecLanIfInfo[i4Index].i4IfIndex == (INT4) u4IfIndex)
        {
            /* gaSecWanIfInfo[i4Index].u1IfNwType; */
            pIfInfo->u1WanType = gaSecLanIfInfo[i4Index].u1WanType;
            pIfInfo->u1NwType = gaSecLanIfInfo[i4Index].u1NwType;
            pIfInfo->u4IfMtu = gaSecLanIfInfo[i4Index].u4Mtu;
            pIfInfo->u1IfOperStatus = gaSecLanIfInfo[i4Index].u1OperStatus;
            pIfInfo->u1IfType = gaSecLanIfInfo[i4Index].u1IfType;
            STRCPY (pIfInfo->au1IfName, gaSecLanIfInfo[i4Index].au1InfName);
            return CFA_SUCCESS;
        }
    }

    SEC_TRC (SEC_FAILURE_TRC, "SecUtilGetIfInfo:"
             "Interface is not available !!!\r\n");
    return CFA_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name     : SecIdsEnqueueDataFrame
 *
 *    Description       : This function is used to send the packet to
 *                        IDS in user space.
 *    Input(s)          : pBuf - The packet buffer
 *                        u1MesgType - type of the message
 *                        u4IfIndex - interface index.
 *
 *    Output(s)         : None
 *
 *    Returns           : IDS_SUCCESS / IDS_FAILURE.
 *
 *****************************************************************************/
INT4
SecIdsEnqueueDataFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1MesgType,
                        UINT4 u4IfIndex)
{
    INT1                i1RetVal = OSIX_SUCCESS;
    tSecQueMsg          SecQueMsg;
    UINT1               au1Buf[SEC_MODULE_DATA_SIZE];
    UINT2               u2TPID = ISS_ZERO_ENTRY;
    tCRU_BUF_CHAIN_HEADER *pNewBuf = NULL;
    MEMSET(au1Buf, 0, SEC_MODULE_DATA_SIZE);

        /* Remove the Vlan Tag here for routerport packets */

        /* Extract the Tag Protocol Identifier from the frame */
         u2TPID =
             OSIX_NTOHS (*
                     ((UINT2 *) ((VOID *) (pBuf->pSkb->data) +
                         CFA_VLAN_TAG_OFFSET)));


        if (u2TPID == CFA_VLAN_PROTOCOL_ID)
        {
            /* If packet is vlan tagged and is routerport */
            if (u4IfIndex < SYS_DEF_MAX_PHYSICAL_INTERFACES)
            {
                /* use CFA_VLAN_TAG_OFFSET since it is size of srcmac,dstmac */
                if (CRU_FAILURE == CRU_BUF_Copy_FromBufChain (pBuf, au1Buf, 0,
                            CFA_VLAN_TAG_OFFSET))
                {
                    IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsEnqueueDataFrame:"
                            "Failed to Copy from Buffer !!!\r\n");
                    return OSIX_FAILURE;
                }

                if (CRU_FAILURE == CRU_BUF_Move_ValidOffset (pBuf,
                            (CFA_VLAN_TAGGED_HEADER_SIZE)))
                {
                    IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsEnqueueDataFrame:"
                            "Failed to Move Valid Offset !!!\r\n");
                    return OSIX_FAILURE;
                }
                if (CRU_FAILURE == CRU_BUF_Prepend_BufChain (pBuf, au1Buf, CFA_VLAN_TAG_OFFSET))
                {
                    IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecIdsEnqueueDataFrame:"
                            "Failed to Prepend buffer !!!\r\n");
                    return OSIX_FAILURE;
                }
            }
        }


    UNUSED_PARAM (u1MesgType);
    SEC_SET_IFINDEX(pBuf,u4IfIndex);
    UNUSED_PARAM (u4IfIndex);
    /* Since buffer is being passed from caller we need to again create SEC buffer
     * copy to SEC buffer and free  fsap CRU buffer*/

    pNewBuf = SEC_CRU_BUF_Duplicate_BufChain(pBuf);
    if (pNewBuf == NULL)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecIdsEnqueueDataFrame:"
                        " Failed to Allocate New SEC CRU buff !!!\r\n");
        return IDS_FAILURE;
    }
    if (CRU_BUF_Prepend_BufChain (pNewBuf, (UINT1 *)
                                  SEC_GET_MODULE_DATA_PTR (pBuf),
                                  sizeof (tSecModuleData)) == CRU_FAILURE)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecIdsEnqueueDataFrame:"
                            " CRU_BUF_Copy_OverBufChain failed !!!\r\n");
        SEC_CRU_BUF_Release_MsgBufChain (pNewBuf, FALSE);
        return IDS_FAILURE;
    }

    MEMSET (&SecQueMsg, 0, sizeof (tSecQueMsg));
    SecQueMsg.u1MsgType = SEC_PKT_TO_IDS_FROM_KERNEL;

    if (CRU_BUF_Prepend_BufChain (pNewBuf, (UINT1 *) &SecQueMsg,
                                  sizeof (tSecQueMsg)) == CRU_FAILURE)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecIdsEnqueueDataFrame:"
                 " Failed to prepend the SecQue Message !!!\r\n");
        SEC_CRU_BUF_Release_MsgBufChain (pNewBuf, FALSE);
        return IDS_FAILURE;
    }

    i1RetVal = OsixQueSend (gu4SnortReadModIdx,
                            (UINT1 *) &pNewBuf, OSIX_DEF_MSG_LEN);

    if (i1RetVal != OSIX_SUCCESS)
    {
        SEC_CRU_BUF_Release_MsgBufChain( pNewBuf, FALSE);
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecIdsEnqueueDataFrame:"
                 " Failed to send the packet to IDS in user space!!!\r\n");
        return IDS_FAILURE;
    }
    CRU_BUF_Release_MsgBufChain(pBuf, FALSE);
    return IDS_SUCCESS;
}
#endif
