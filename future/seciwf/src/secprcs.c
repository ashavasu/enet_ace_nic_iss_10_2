/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2011                                          */
/*                                                                           */
/* $Id: secprcs.c,v 1.13 2014/11/23 09:19:10 siva Exp $                       */
/*                                                                           */
/*  FILE NAME             : secprcs.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : Packet Handler                                   */
/*  MODULE NAME           : Security Module                                  */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 16 Dec 2010                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains handlers for security         */
/*                          processing on the frames sent/received over      */
/*                          WAN interface.                                   */
/*****************************************************************************/
#ifndef _SECPRCS_C_
#define _SECPRCS_C_

#include "secinc.h"
#include "secextn.h"
#include "vpn.h"
#include "sectrc.h"
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecProcessFrame                                  */
/*                                                                           */
/*    Description         : This function serves the packets received/sent   */
/*                          on WAN interface. (both INBOUND and OUTBOUND).   */
/*                          (i) Determine the direction of the frame and the */
/*                              incoming or outgoing interface index.        */
/*                          (ii) Check the command type of the CRU buffer.   */
/*                               If it is set to SEC_TO_CFA. unset the type  */
/*                               and return OSIX_SUCCESS.                    */
/*                          (iii) If the direction is SEC_INBOUND,           */
/*                                Invoke SecProcessInBoundPkt.               */
/*                                If the direction is SEC_OUTBOUND,          */
/*                                Invoke SeCProcesssOutBoundPkt.             */
/*                          (iv) Return the return values of the Inbound/    */
/*                               OutBound processing functions.              */
/*                                                                           */
/*    Input(s)            : pBuf -> Pointer to CRU Buffer containing Ethernet*/
/*                                  frame.                                   */
/*                                                                           */
/*    Output(s)           : pBuf - Frame after security processing is applied*/
/*                          pbSecVerdict -                                   */
/*                          When set to SEC_STOLEN,                          */
/*                          Indicates if the frame is stolen by              */
/*                                      Security module, so that CFA does not*/
/*                                      operate further on the frame.        */
/*                                                                           */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_FAILURE -> When security Processing failed.  */
/*                         OSIX_SUCCESS -> If security is applied on the     */
/*                                         frame.                            */
/*                                                                           */
/*****************************************************************************/
UINT4
SecProcessFrame (tCRU_BUF_CHAIN_HEADER * pBuf,
                 tSecIntfInfo * pSecIntfInfo,
                 UINT1 u1Direction, BOOL1 * pbSecVerdict)
{
    tSecPppSessionInfo  SecPppSessionInfo;
    tCfaIfInfo          IfInfo;
    UINT4               u4PppIndex = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4Offset = 0;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT2               u2L2HdrLen = 0;
    UINT2               u2Protocol = 0;
    UINT2               u2SessionId = 0;

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    /* If the Flag SEC_TO-CFA is set it implies that security processing 
     * on the packet is over and we are supposed to continue with the 
     * remaining processing.  Hence returning OSIX_SUCCESS to allow further 
     * non security related processing.
     */
    u4IfIndex = pSecIntfInfo->u4PhyIfIndex;

    if (SEC_GET_COMMAND (pBuf) == SEC_TO_CFA)
    {
        SEC_SET_COMMAND (pBuf, 0);
        return OSIX_SUCCESS;
    }

    CfaGetIfInfo (u4IfIndex, &IfInfo);

    /* Fetch the Direction of the frame and the incoming/outgoing interface
     * index based on the direction. 
     */

    /* Preserve the Ethernet header in the CRU buffer's MODULE_DATA,
     * since Ethernet headers are stripped before security processing.
     * Once the security processing is done for the inbound packets,
     * the preserved Ethernet header will be prepended before the IP packets
     * and the frame is enqueued to CFA for further processing.
     */

    /* Since the tSecModuleData structure has EnetHeader information
     * present at 5th byte offset, Module data pointer is moved 5 bytes forward
     * and the Ethernet header is saved from that offset.
     */
    if (OSIX_FAILURE == SecUtilUpdateEthHdrToModuleData (pBuf, &u2L2HdrLen))
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessFrame:"
                 "Failed to extract ethernet header and update the"
                 "Module Date !!!\r\n");
        return OSIX_FAILURE;
    }

    u2Protocol = (SEC_GET_MODULE_DATA_PTR (pBuf)->u2EthType);

    /* Need not to encrypt/decrypt ICMPv6 neighbor/router solicitation
     * advertisement packets */
    if (u2Protocol == CFA_ENET_IPV6)
    {

/* Added the function to avoid the Stack size */
        i4RetVal = SecProcessCheckIcmpv6Pkt (pBuf);
        if (i4RetVal == OSIX_SUCCESS)
        {
            return OSIX_SUCCESS;
        }
    }

    if (CFA_PPPOE_SESSION == u2Protocol)
    {
        u2SessionId = (SEC_GET_MODULE_DATA_PTR (pBuf)->u2SessionId);
        if (0 == u2SessionId)
        {
            return OSIX_SUCCESS;
        }

        if (CFA_PPPOE_IP_PROTO != SEC_GET_MODULE_DATA_PTR (pBuf)->u2PppProtocol)
        {
            if (SEC_KERN == gi4SysOperMode)
            {
                *pbSecVerdict = SEC_STOLEN;
                SEC_SET_COMMAND (pBuf, SEC_TO_CFA);
                if (CFA_FAILURE == CfaHandlePktFromSec (pBuf))
                {
                    return OSIX_FAILURE;
                }
            }
            return OSIX_SUCCESS;
        }

        MEMSET (&SecPppSessionInfo, 0, sizeof (tSecPppSessionInfo));
        SecPppSessionInfo.u2SessionId = u2SessionId;

        if (OSIX_FAILURE == SecUtilPPPoEGetSessionInfo (&SecPppSessionInfo))
        {
            return OSIX_SUCCESS;
        }
        u4PppIndex = SecPppSessionInfo.i4PPPIfIndex;
    }

    /* Strip away the ethernet header and submit the packet for Security 
     * processing. 
     */
    if (gi4SysOperMode != SEC_KERN_USER)
    {
        if (CRU_BUF_Move_ValidOffset (pBuf, u2L2HdrLen) != CRU_SUCCESS)
        {
            SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessFrame:"
                     "Failed to Move Valid Offset !!!\r\n");
            return OSIX_FAILURE;
        }
    }
    else
    {
        u4Offset = u2L2HdrLen;
    }

    /* Avoid LAN packets destined to ISS WAN interfaces and 
     * WAN packets destined to ISS LAN interfaces */

    if (u2Protocol == CFA_ENET_IPV4)
    {
/* Added the function to avoid the Stack size */
        i4RetVal = SecProcessCheckInvalidIntf (pBuf, u4Offset, IfInfo.u1NwType);
        if (i4RetVal != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }
    if (SEC_INBOUND == u1Direction)
    {
        /* Indicates that the frame is stolen by security module for
         * security processing. 
         */
        if (SEC_KERN_USER == gi4SysOperMode)
        {
            /* Frames received from kernel space after the completion of
             * security processing will be hitting this block.
             * Hence return OSIX_SUCCESS. 
             */
            i4RetVal = OSIX_SUCCESS;
        }
        else
        {
            *pbSecVerdict = SEC_STOLEN;
            SEC_SET_IFINDEX (pBuf, u4IfIndex);
            if (CFA_PPPOE_SESSION == u2Protocol)
            {
                u4IfIndex = u4PppIndex;
                /* Updating interface information for the 
                 * updated interface index */
                CfaGetIfInfo (u4IfIndex, &IfInfo);
            }
            if (IfInfo.u1BridgedIface == CFA_ENABLED)
            {
                u4IfIndex = pSecIntfInfo->u4VlanIfIndex;
                /* Updating interface information for the 
                 * updated interface index */
                CfaGetIfInfo (u4IfIndex, &IfInfo);
            }

            i4RetVal =
                SecProcessInboundPkt (pBuf, u4IfIndex, u2Protocol, &IfInfo);
        }
    }

    if (SEC_OUTBOUND == u1Direction)
    {

        if (SEC_KERN_USER == gi4SysOperMode)
        {
            /* For user<->Kernel architecture, and the packet is sent out on
             * WAN interface from user space, Write the frame to Kernel with the
             * help of Security File descriptor.
             * Mark the Security Verdict to SEC_STOLEN since the frame is sent
             * to Kernel.
             */
            if (CFA_NETWORK_TYPE_WAN == IfInfo.u1NwType)
            {
                i4RetVal =
                    SecInitPostPacketToKernel (pBuf, u4IfIndex, u1Direction);
                if (i4RetVal == OSIX_SUCCESS)
                {
                    *pbSecVerdict = SEC_STOLEN;
                }
            }
            else
            {
                /* If the packet is sent out of non=WAN interface return
                 * success. 
                 */
                i4RetVal = OSIX_SUCCESS;
            }
        }
        else
        {
            SEC_SET_IFINDEX (pBuf, u4IfIndex);
            if (CFA_PPPOE_SESSION == u2Protocol)
            {
                u4IfIndex = u4PppIndex;
                CfaGetIfInfo (u4IfIndex, &IfInfo);
            }
            if (pSecIntfInfo->u4VpncIfIndex != 0)
            {
                u4IfIndex = pSecIntfInfo->u4VpncIfIndex;
                CfaGetIfInfo (u4IfIndex, &IfInfo);
            }
            if (IfInfo.u1BridgedIface == CFA_ENABLED)
            {
                u4IfIndex = pSecIntfInfo->u4VlanIfIndex;
                CfaGetIfInfo (u4IfIndex, &IfInfo);
            }

            i4RetVal = SecProcessOutboundPkt (pBuf, u4IfIndex, pbSecVerdict,
                                              u2Protocol, &IfInfo);
        }
    }

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecProcessCheckInvalidIntf                       */
/*                                                                           */
/*    Description         : Avoid LAN packets destined to ISS WAN interfaces and
*                           WAN packets destined to ISS LAN interfaces       */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : pBuf -> Pointer to CRU Buffer containing Ethernet*/
/*                                  frame.                                   */
/*                                                                           */
/*    Output(s)           : pBuf - Frame after security processing is applied*/
/*                          u4IfIndex - Interface index on which the packet  */
/*                                      is received.                         */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_FAILURE -> When security Processing failed.  */
/*                         OSIX_SUCCESS -> If security is applied on the     */
/*                                         frame.                            */
/*                                                                           */
/*****************************************************************************/
INT4
SecProcessCheckInvalidIntf (tCRU_BUF_CHAIN_HEADER * pBuf,
                            UINT4 u4Offset, UINT1 u1NwType)
{
    t_IP_HEADER         IPHdr;
    tCfaIfInfo          DestIfInfo;
    tSecIfIpAddrInfo   *pSecIfIpAddrNode = NULL;

    MEMSET (&DestIfInfo, 0, sizeof (tCfaIfInfo));
    MEMSET (&IPHdr, 0, sizeof (t_IP_HEADER));

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &IPHdr, u4Offset,
                               CFA_IP_HDR_LEN);
    pSecIfIpAddrNode =
        SecUtilGetIfIpAddrEntry (OSIX_NTOHL (IPHdr.u4Dest), SEC_UCAST_IP);
    if (pSecIfIpAddrNode != NULL)
    {
        CfaGetIfInfo (pSecIfIpAddrNode->i4IfIndex, &DestIfInfo);
        if ((DestIfInfo.u1IfType != CFA_LOOPBACK)
            && (DestIfInfo.u1NwType != u1NwType))
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecProcessCheckIcmpv6Pkt                             */
/*                                                                           */
/*    Description         : Need not to encrypt/decrypt 
 *                          ICMPv6 neighbor/router solicitation
 *                         advertisement packets */
/*                                                                           */
/*    Input(s)            : pBuf -> Pointer to CRU Buffer containing Ethernet*/
/*                                  frame.                                   */
/*                                                                           */
/*    Output(s)           : pBuf - Frame after security processing is applied*/
/*                          u4IfIndex - Interface index on which the packet  */
/*                                      is received.                         */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_FAILURE -> When security Processing failed.  */
/*                         OSIX_SUCCESS -> If security is applied on the     */
/*                                         frame.                            */
/*                                                                           */
/*****************************************************************************/
INT4
SecProcessCheckIcmpv6Pkt (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIp6Hdr            *pIp6Hdr = NULL;
    tIp6Hdr             Ip6Hdr;
    UINT1               u1Type = 0;

    pIp6Hdr =
        (tIp6Hdr *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear (pBuf,
                                                           CFA_ENET_V2_HEADER_SIZE,
                                                           sizeof (tIp6Hdr));
    if (pIp6Hdr == NULL)
    {
        pIp6Hdr = &Ip6Hdr;
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &Ip6Hdr,
                                   CFA_ENET_V2_HEADER_SIZE, sizeof (tIp6Hdr));
    }

    if (pIp6Hdr->u1Nh == ICMPV6_PROTOCOL_ID)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1Type,
                                   (CFA_ENET_V2_HEADER_SIZE +
                                    sizeof (tIp6Hdr)), sizeof (UINT1));
        switch (u1Type)
        {
            case ICMP6_ROUTER_SOLICITATION:
            case ICMP6_ROUTER_ADVERTISEMENT:
            case ICMP6_NEIGHBOUR_SOLICITATION:
            case ICMP6_NEIGHBOUR_ADVERTISEMENT:
            case ICMP6_ROUTE_REDIRECT:

                return OSIX_SUCCESS;
                break;
        }
    }
    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecProcessInboundPkt                             */
/*                                                                           */
/*    Description         : This function serves the packets received on WAN */
/*                          interface.                                       */
/*                                                                           */
/*    Input(s)            : pBuf -> Pointer to CRU Buffer containing Ethernet*/
/*                                  frame.                                   */
/*                                                                           */
/*    Output(s)           : pBuf - Frame after security processing is applied*/
/*                          u4IfIndex - Interface index on which the packet  */
/*                                      is received.                         */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_FAILURE -> When security Processing failed.  */
/*                         OSIX_SUCCESS -> If security is applied on the     */
/*                                         frame.                            */
/*                                                                           */
/*****************************************************************************/
INT4
SecProcessInboundPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                      UINT2 u2Protocol, tCfaIfInfo * IfInfo)
{
    tIp6Hdr             Ip6Hdr;
    tIp6Hdr            *pIp6Hdr = NULL;
    UINT4               u4RuleFlag = 0;
    UINT4               u4SkipFlg = 0;
    UINT1               u1RetVal = OSIX_FAILURE;
    UINT1               u1IsIpSec = OSIX_FALSE;

    if (u2Protocol == CFA_ENET_IPV4)
    {
        /* IPv4 Processing */
        /* Passing the address of pBuf in case if the pointer is updated with
         * the re-fragmented packet */
        u1RetVal = SecPortSecv4InProcess (&pBuf, u4IfIndex);

        switch (u1RetVal)
        {
            case SEC_SUCCESS:
            {
                /* IPSec Will take care of fwding the packet */
                return OSIX_SUCCESS;
            }
            case SEC_FAILURE:
            {
                SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessInboundPkt:"
                         "IPSECv4 processing is failed !!!\r\n");
                return OSIX_FAILURE;
            }

            case SEC_IKE:
            {
                /* Security Processing is completed for the Inbound packets. 
                 * Prepend the ethernet header in the CRU Buffer's Module Data,
                 * which is preserved during the start of security processing. 
                 */
                if (OSIX_FAILURE == SecUtilPrependEthHdrToPkt (pBuf))
                {
                    SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessInboundPkt:"
                             "Failed to prepend Ethernet header to Packet !!!\r\n");
                    return OSIX_FAILURE;
                }

                /* Set the command to SEC_TO_CFA since security is applied. 
                 * on the packet. 
                 */
                SEC_SET_COMMAND (pBuf, SEC_TO_CFA);

                if (CFA_FAILURE == CfaHandlePktFromSec (pBuf))
                {
                    return OSIX_FAILURE;
                }
                return OSIX_SUCCESS;

            }

                /* Intentional fallthrough */
            case SEC_BYPASS:
            default:
            {
                /* All traffic other than IPSec, IKE */
                break;
            }
        }
    }
    else if (u2Protocol == CFA_ENET_IPV6)
    {
        /* IPv6 Processing */
        u1RetVal = SecPortSecv6InProcess (pBuf, u4IfIndex);
        if (SEC_FAILURE == u1RetVal)
        {
            SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessInboundPkt:"
                     "IPSECv6 processing is failed !!!\r\n");
            return OSIX_FAILURE;
        }
        else if (SEC_IKE == u1RetVal)
        {
            /* Security Processing is completed for the Inbound packets. 
             * Prepend the ethernet header in the CRU Buffer's Module Data,
             * which is preserved during the start of security processing. 
             */
            if (OSIX_FAILURE == SecUtilPrependEthHdrToPkt (pBuf))
            {
                SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessInboundPkt:"
                         "Failed to prepend Ethernet header to Packet !!!\r\n");
                return OSIX_FAILURE;
            }

            /* Set the command to SEC_TO_CFA since security is applied. 
             * on the packet. 
             */
            SEC_SET_COMMAND (pBuf, SEC_TO_CFA);

            if (CFA_FAILURE == CfaHandlePktFromSec (pBuf))
            {
                return OSIX_FAILURE;
            }
            return OSIX_SUCCESS;
        }
        else
        {
            /* IPSecv6 will return the decrypted buffer unlike IPSecv4.
             * if the VPN acccess list is matching ignore the firewall
             * processing */
            pIp6Hdr =
                (tIp6Hdr *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0,
                                                                   IPV6_HEADER_LEN);
            if (pIp6Hdr == NULL)
            {
                pIp6Hdr = &Ip6Hdr;
                MEMSET (&Ip6Hdr, 0, sizeof (tIp6Hdr));
                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &Ip6Hdr, 0,
                                           IPV6_HEADER_LEN);
            }

            if (SecPortVpnGetTunnelEndpointForIp6 (&(pIp6Hdr->dstAddr),
                                                   &(pIp6Hdr->srcAddr),
                                                   pIp6Hdr->u1Nh, &u4SkipFlg,
                                                   &u4RuleFlag) == SEC_SUCCESS)
            {
                if ((u4SkipFlg & SEC_SKIP_FWL) != 0)
                {
                    u1IsIpSec = OSIX_TRUE;
                }
            }
        }
    }

    /* All traffic other than IPSec, IKE and decrypted tunnel
     * tunnel traffic will continue.
     */
    if (OSIX_FAILURE ==
        SecProcessContinueInBound (pBuf, u4IfIndex, 0, 0, u1IsIpSec,
                                   IfInfo, u2Protocol))
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessInboundPkt:"
                 "Security processing is Failed !!!\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecProcessContinueInBound                        */
/*                                                                           */
/*    Description         : This function serves the packets received on WAN */
/*                          interface.                                       */
/*                                                                           */
/*    Input(s)            : pBuf -> Pointer to CRU Buffer containing IP      */
/*                                  packet.                                  */
/*                                                                           */
/*    Output(s)           : pBuf - Frame after security processing is applied*/
/*                          u4IfIndex - Interface index on which the packet  */
/*                                      is received.                         */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_FAILURE -> When security Processing failed.  */
/*                         OSIX_SUCCESS -> If security is applied on the     */
/*                                         frame.                            */
/*                                                                           */
/*****************************************************************************/
INT4
SecProcessContinueInBound (tCRU_BUF_CHAIN_HEADER * pBuf,
                           UINT4 u4IfIndex,
                           UINT4 u4VpncIndex,
                           UINT1 u1AssocMode,
                           UINT1 u1IsIpSec, tCfaIfInfo * IfInfo,
                           UINT2 u2Protocol)
{
    t_IP_HEADER         IPHdr;
    UINT4               u4CxtId = 0;
    INT1                i1FragReass = 0;
    INT4                i4GlobalNat = NAT_DISABLE;
    UINT4               u4IntfNat = NAT_DISABLE;
    UINT1               u1SkipNat = OSIX_FALSE;

    i4GlobalNat = SecPortNatUtilGetGlobalNatStatus ();
    u4IntfNat = SecPortNatCheckIfNatEnable (u4IfIndex);

    if (u4IfIndex > SYS_MAX_INTERFACES || u4IfIndex == 0)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecProcessContinueInBound:"
                 "Interface index is not valid !!!\r\n");
        return OSIX_FAILURE;
    }

    if ((i4GlobalNat == NAT_DISABLE)
        || (CFA_NETWORK_TYPE_WAN != IfInfo->u1NwType)
        || (CFA_ENET_IPV6 == u2Protocol))
    {
        u1SkipNat = OSIX_TRUE;
    }

    /* For Inbound traffic firewall will be processed first to avoid dynamic
     * entry creation for unwanted traffic in case of nat disabled */
    if ((NAT_DISABLE == i4GlobalNat) || (NAT_DISABLE == u4IntfNat))
    {
        /* Skip the firewall processing for IPSec decoded packets.
         * else perform the firewall processing */
        if (OSIX_FALSE == u1IsIpSec)
        {
            if (SecPortFwlAclProcessPktForInFiltering (pBuf, u4IfIndex) !=
                FWL_SUCCESS)
            {
                /* Error, update IP In discards */
                /* Get the context id for this interface */
                SecPortVcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4CxtId);
                if (CFA_ENET_IPV4 == u2Protocol)
                {
                    SecPortIpUpdateInIfaceErrInCxt (u4CxtId);
                }
                else if (CFA_ENET_IPV6 == u2Protocol)
                {
                    SecPortIp6UpdateIfaceOutDiscards (u4IfIndex);
                }
                SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessContinueInBound:"
                         "Firewall Processing is Failed !!!\r\n");
                return OSIX_FAILURE;    /* buf released by IWF/GDD */
            }
        }
    }

    /* For IpSec decoded packets, If the Sec Assoc is Transport, 
     * then apply NAT, else there
     * will be neither NAT nor FIREWALL.
     * For Non-Ipsec packets, apply NAT and firewall.  */
    if (((((OSIX_TRUE == u1IsIpSec)) &&
          ((u1AssocMode == SEC_TRANSPORT) || (u4VpncIndex != 0))) ||
         (OSIX_FALSE == u1IsIpSec)) && (u1SkipNat == OSIX_FALSE))
    {
        /* If the VpncIndex is not Zero (i.e packet is received
         * on vpnc interface, give it to NAT) after changing the
         * IfIndex - from wan ifindex to VPNC index*/
        if ((u4VpncIndex != 0) && (u4IfIndex != u4VpncIndex))
        {
            u4IfIndex = u4VpncIndex;
        }
#ifndef SECURITY_KERNEL_WANTED
        SecPortNatLock ();
#endif
        if (SecPortNatTranslateInBoundPkt (&pBuf, u4IfIndex, &i1FragReass) !=
            SUCCESS)
        {
            /* Get the context id for this interface */
            SecPortVcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4CxtId);
            /* Error, update IP In discards */
            SecPortIpUpdateInIfaceErrInCxt (u4CxtId);
#ifndef SECURITY_KERNEL_WANTED
            SecPortNatUnLock ();
#endif
            SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessContinueInBound:"
                     "Nat Processing is Failed !!!\r\n");
            return OSIX_FAILURE;    /* buf released by IWF/GDD */
        }
#ifndef SECURITY_KERNEL_WANTED
        SecPortNatUnLock ();
#endif
    }

    if ((NAT_ENABLE == i4GlobalNat) && (NAT_ENABLE == u4IntfNat))
    {
        /* Skip the firewall processing for IPSec decoded packets.
         * else perform the firewall processing */
        if (OSIX_FALSE == u1IsIpSec)
        {
            if (SecPortFwlAclProcessPktForInFiltering (pBuf, u4IfIndex) !=
                FWL_SUCCESS)
            {
                /* Get the context id for this interface */
                SecPortVcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4CxtId);
                /* Error, update IP In discards */
                if (CFA_ENET_IPV4 == u2Protocol)
                {
                    SecPortIpUpdateInIfaceErrInCxt (u4CxtId);
                }
                else if (CFA_ENET_IPV6 == u2Protocol)
                {
                    SecPortIp6UpdateIfaceInDiscards (u4IfIndex);
                }
                SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessContinueInBound:"
                         "Firewall Processing is Failed !!!\r\n");
                return OSIX_FAILURE;    /* buf released by IWF/GDD */
            }
        }
    }

    if (u2Protocol == CFA_ENET_IPV4)
    {
        MEMSET (&IPHdr, 0, sizeof (t_IP_HEADER));
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &IPHdr, 0, CFA_IP_HDR_LEN);
    }

    /* Security Processing is completed for the Inbound packets. 
     * Prepend the ethernet header in the CRU Buffer's Module Data,
     * which is preserved during the start of security processing. 
     */
    if (OSIX_FAILURE == SecUtilPrependEthHdrToPkt (pBuf))
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessContinueInBound:"
                 "Failed to prepend ethernet header !!!\r\n");
        return OSIX_FAILURE;
    }

    /* Set the command to SEC_TO_CFA since security is applied. 
     * on the packet. 
     */
    SEC_SET_COMMAND (pBuf, SEC_TO_CFA);

    if (IDS_FAILURE ==
        SecPortHandlePktFromSecurity (pBuf, u4IfIndex,
                                      u1IsIpSec, IfInfo, u2Protocol, &IPHdr))
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessContinueInBound:"
                 "Failed to send the packet to IDS/CFA !!!\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecProcessOutboundPkt                            */
/*                                                                           */
/*    Description         : This function serves the packets sent out on WAN */
/*                          interface.                                       */
/*                                                                           */
/*    Input(s)            : pBuf -> Pointer to CRU Buffer containing Ethernet*/
/*                                  frame.                                   */
/*                                                                           */
/*    Output(s)           : pBuf - Frame for security processing             */
/*                          u4IfIndex - Interface index on which the packet  */
/*                                      has to be sent.                      */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_FAILURE -> When security Processing failed.  */
/*                         OSIX_SUCCESS -> If security is applied on the     */
/*                                         frame.                            */
/*                                                                           */
/*****************************************************************************/
INT4
SecProcessOutboundPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                       UINT4 u4IfIndex, BOOL1 * pbSecVerdict,
                       UINT2 u2Protocol, tCfaIfInfo * IfInfo)
{
    t_IP_HEADER         IPHdr;
    tIp6Hdr             IP6Hdr;
    UINT4               u4Src = 0;
    UINT4               u4Dest = 0;
    UINT4               u4RuleFlag = 0;
    UINT4               u4SkipFlg = 0;
    UINT4               u4PktSize = 0;
    UINT4               u4CxtId = 0;
    UINT1               u1Retval = 0;
    INT1                i1FragReass = 0;

    MEMSET (&IPHdr, 0, sizeof (t_IP_HEADER));

    if (!
        ((CFA_NETWORK_TYPE_WAN == IfInfo->u1NwType)
         || (IfInfo->u1IfType == CFA_VPNC)))
    {
        u4SkipFlg = ((SEC_SKIP_VPN) | (SEC_SKIP_NAT));
    }

    if (CFA_ENET_IPV4 == u2Protocol)
    {
        /* Extract the IPv4 header */
        /* Copy the header */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &IPHdr, 0, CFA_IP_HDR_LEN);
    }
    else if (CFA_ENET_IPV6 == u2Protocol)
    {
        u4SkipFlg = SEC_SKIP_NAT;
        MEMSET (&IP6Hdr, 0, sizeof (tIp6Hdr));
        /* Extract the IPv6 header */
        if (OSIX_FAILURE == SecUtilExtractIp6Hdr (&IP6Hdr, pBuf))
        {
            SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessOutboundPkt:"
                     "Failed to extract IPv6 header !!!\r\n");
            return OSIX_FAILURE;    /* buf released by IWF/GDD */
        }
    }

    /* Check if there exists a policy in the IPSec matching this traffic. If
     * yes, then skip NAT and FWL  processing.
     */
    if ((u4SkipFlg & (SEC_SKIP_VPN)) == 0)
    {
        if (CFA_ENET_IPV4 == u2Protocol)
        {
            if (SecPortVpnGetTunnelEndpoint (0, OSIX_NTOHL (IPHdr.u4Src),
                                             OSIX_NTOHL (IPHdr.u4Dest),
                                             IPHdr.u1Proto, &u4SkipFlg,
                                             &u4RuleFlag, &u4Src,
                                             &u4Dest) == SEC_SUCCESS)
            {
                if (u4RuleFlag == SEC_FILTER)
                {
                    SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessOutboundPkt:"
                             "VPN filter is matched !!!\r\n");
                    return (OSIX_FAILURE);
                }
            }
        }
        else if (CFA_ENET_IPV6 == u2Protocol)
        {
            if (SecPortVpnGetTunnelEndpointForIp6
                (&IP6Hdr.srcAddr, &IP6Hdr.dstAddr, IP6Hdr.u1Nh, &u4SkipFlg,
                 &u4RuleFlag) == SEC_SUCCESS)
            {
                if (u4RuleFlag == SEC_FILTER)
                {
                    SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessOutboundPkt:"
                             "VPN filter is matched !!!\r\n");
                    return (OSIX_FAILURE);
                }
            }
        }
    }

    if ((u4SkipFlg & SEC_SKIP_FWL) == 0)
    {
        if (SecPortFwlAclProcessPktForOutFiltering (pBuf, u4IfIndex) !=
            FWL_SUCCESS)
        {
            /* Get the context id for this interface */
            SecPortVcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4CxtId);
            /* update IP OutDiscards */
            if (CFA_ENET_IPV4 == u2Protocol)
            {
                SecPortIpUpdateOutIfaceErrInCxt (u4CxtId);
            }
            else if (CFA_ENET_IPV6 == u2Protocol)
            {
                SecPortIp6UpdateIfaceOutDiscards (u4IfIndex);
            }
            SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessOutboundPkt:"
                     "Firewall Processing is failed !!!\r\n");
            return OSIX_FAILURE;
        }
    }

    if ((u4SkipFlg & SEC_SKIP_NAT) == 0)
    {
#ifndef SECURITY_KERNEL_WANTED
        SecPortNatLock ();
#endif
        if (SecPortNatTranslateOutBoundPkt (&pBuf, u4IfIndex, &i1FragReass) !=
            SUCCESS)
        {
            /* Get the context id for this interface */
            SecPortVcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4CxtId);
            /* update IP OutDiscards */
            SecPortIpUpdateOutIfaceErrInCxt (u4CxtId);
#ifndef SECURITY_KERNEL_WANTED
            SecPortNatUnLock ();
#endif
            SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessOutboundPkt:"
                     "Nat translation is failed !!!\r\n");
            return OSIX_FAILURE;
        }
#ifndef SECURITY_KERNEL_WANTED
        SecPortNatUnLock ();
#endif
    }

    /* If IPSECv4 processing is successfull, then return as the IPSECv4
       module would've posted the packet.
       For the other cases, 
       1. IPSECv4 is disabled, just go ahead.
       2. IPSECv4 is Enabled and packet is by-passed, just go ahead.
     */
    if (((u4SkipFlg) & (SEC_SKIP_VPN)) == 0)
    {
        if (CFA_ENET_IPV4 == u2Protocol)
        {
            u1Retval = SecPortSecv4OutProcess (pBuf, (UINT2) u4IfIndex);
            if (u1Retval == SEC_FAILURE)
            {
                SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessOutboundPkt:"
                         "IPSECV4 processing is failed !!!\r\n");
                return OSIX_FAILURE;
            }
            else if ((SEC_SUCCESS == u1Retval) &&
                     (SEC_ENABLE == SecPortSecUtilGetGlobalStatus ()))
            {
                *pbSecVerdict = SEC_STOLEN;
                return OSIX_SUCCESS;
            }
        }
        else if (CFA_ENET_IPV6 == u2Protocol)
        {
            u1Retval = SecPortSecv6OutProcess (pBuf, u4IfIndex);
            if (u1Retval == SEC_FAILURE)
            {
                SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessOutboundPkt:"
                         "IPSECV6 processing is failed !!!\r\n");
                return OSIX_FAILURE;
            }
        }
    }
    /* Security Processing is completed for the outbound packets. 
     * Prepend the ethernet header in the CRU Buffer's Module Data,
     * which is preserved during the start of security processing. 
     */
    if (OSIX_FAILURE == SecUtilPrependEthHdrToPkt (pBuf))
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessOutboundPkt:"
                 "Prepend Ethernet header to the packet is Failed !!!\r\n");
        return OSIX_FAILURE;
    }

    if (SEC_KERN == gi4SysOperMode)
    {
        /* Submit to gddkern.c for writing the packet on internal 
         * port. 
         */
        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

        if (CFA_FAILURE == CfaIwfEnetProcessTxFrame (pBuf, u4IfIndex,
                                                     NULL, u4PktSize,
                                                     u2Protocol,
                                                     CFA_ENCAP_ENETV2))
        {
            SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessOutboundPkt:"
                     "Packet transmission after security processing is failed  !!!\r\n");
            return OSIX_FAILURE;
        }
        else
        {
            *pbSecVerdict = SEC_STOLEN;
        }

    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                         : 
 *    Function Name        : SecProcessContinueOutBound
 *                         : 
 *    Description          : Receives data from the IPSec outbound module
 *                         : and tramits over physical layer
 *                         : 
 *     Input(s)            : pBuf - the pointer to the data buffer
 *                         :  u4IfIndex - the MIB-2 ifIndex
 *                         : 
 *    Output(s)            : None.
 *                         : 
 *  Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Returns            : OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
SecProcessContinueOutBound (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex)
{
    UINT4               u4PktSize = 0;
    tCfaIfInfo          IfInfo;

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    /* Security Processing is completed for the outbound packets. 
     * Prepend the ethernet header in the CRU Buffer's Module Data,
     * which is preserved during the start of security processing. 
     */
    if (OSIX_FAILURE == SecUtilPrependEthHdrToPkt (pBuf))
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessContinueOutBound:"
                 "Prepend ethernet header to the packet is failed  !!!\r\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSIX_FAILURE;
    }

    if (SecUtilGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        /* Memory should be freed wherever it is called */
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessContinueOutBound:"
                 "Get interface info failed !!!\r\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSIX_FAILURE;
    }

    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

    if (CFA_FAILURE == CfaGddWrite (pBuf, u4IfIndex,
                                    u4PktSize, CFA_FALSE, &IfInfo))
    {
        /* Memory is released by CfaGddWrite. */
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecProcessContinueOutBound:"
                 "Packet transmission after security processing is failed !!!\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

#endif /* _SECPRCS_C_ */
