#####################################################################
# Copyright (C) 2010 Aricent Inc . All Rights Reserved              #
#
# $Id: make.h,v 1.1.1.1 2011/09/12 09:03:16 siva Exp $
#
# Description: header file for SECURITY make file. 
#
#####################################################################


###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

GLOBAL_OPNS = ${SYSTEM_COMPILATION_SWITCHES} 


############################################################################
#                         Directories                                      #
############################################################################

PROJECT_NAME  = FutureSECIWF
PROJECT_BASE_DIR = ${BASE_DIR}/seciwf
PROJECT_SOURCE_DIR = ${PROJECT_BASE_DIR}/src
PROJECT_INCLUDE_DIR = ${PROJECT_BASE_DIR}/inc
SEC_INCLUDE_DIR = ${BASE_DIR}/seciwf/inc
PROJECT_OBJECT_DIR = ${PROJECT_BASE_DIR}/obj


PROJECT_FINAL_INCLUDES_DIRS = -I$(PROJECT_INCLUDE_DIR) \
     $(COMMON_INCLUDE_DIRS)

#############################################################################

