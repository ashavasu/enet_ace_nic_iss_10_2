/*************************************************************************
* Copyright (C) 2007-2015 Aricent Group . All Rights Reserved
*
* $Id: fsfsbwr.h,v 1.4 2017/10/09 13:13:57 siva Exp $
*
* Description: Proto types for Low Level  Routines
*
*************************************************************************/

#ifndef _FSFSBWR_H
#define _FSFSBWR_H
INT4 GetNextIndexFsMIFsbContextTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSFSB(VOID);

VOID UnRegisterFSFSB(VOID);
INT4 FsMIFsbSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFcMapModeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFcmapGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbHouseKeepingTimePeriodGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbTrapStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbClearStatsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbDefaultVlanIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbTraceSeverityLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFcMapModeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFcmapSet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbHouseKeepingTimePeriodSet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbTrapStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbClearStatsSet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbDefaultVlanIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbTraceSeverityLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIFsbModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFcMapModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFcmapTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIFsbHouseKeepingTimePeriodTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIFsbTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIFsbTrapStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIFsbClearStatsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIFsbDefaultVlanIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIFsbRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIFsbTraceSeverityLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIFsbContextTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIFsbFIPSnoopingTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIFsbFIPSnoopingFcmapGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFIPSnoopingEnabledStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFIPSnoopingRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFIPSnoopingPinnedPortsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFIPSnoopingFcmapSet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFIPSnoopingEnabledStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFIPSnoopingRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFIPSnoopingPinnedPortsSet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFIPSnoopingFcmapTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFIPSnoopingEnabledStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFIPSnoopingRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFIPSnoopingPinnedPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFIPSnoopingTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIFsbIntfTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIFsbIntfPortRoleGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbIntfRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbIntfPortRoleSet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbIntfRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbIntfPortRoleTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIFsbIntfRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIFsbIntfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIFsbFIPSessionTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIFsbFIPSessionFcMapGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFIPSessionFcfIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFIPSessionFcfNameIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFIPSessionFcIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFIPSessionEnodeConnectTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFIPSessionHouseKeepingTimerStatusGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIFsbFcfTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIFsbFcfFcMapGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFcfAddressingModeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFcfEnodeLoginCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFcfNameIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFcfFabricNameGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbFcfPinnedPortsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIFsbGlobalStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIFsbGlobalStatsVlanRequestsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbGlobalStatsVlanNotificationGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIFsbVlanStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIFsbVlanStatsUnicastDisAdvGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbVlanStatsMulticastDisAdvGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbVlanStatsUnicastDisSolGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbVlanStatsMulticastDisSolGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbVlanStatsFLOGICountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbVlanStatsFDISCCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbVlanStatsLOGOCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbVlanStatsFLOGIAcceptCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbVlanStatsFLOGIRejectCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbVlanStatsFDISCAcceptCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbVlanStatsFDISCRejectCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbVlanStatsLOGOAcceptCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbVlanStatsLOGORejectCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbVlanStatsClearLinkCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbVlanFcMapMisMatchCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbVlanMTUMisMatchCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbVlanACLFailureCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbVlanInvalidFIPFramesCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbVlanFCFDiscoveryTimeoutsCountGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIFsbSessStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIFsbSessStatsEnodeKeepAliveCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIFsbSessStatsVNPortKeepAliveCountGet(tSnmpIndex *, tRetVal *);
#endif /* _FSFSBWR_H */
