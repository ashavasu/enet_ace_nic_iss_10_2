/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsfsbdb.h,v 1.5 2017/10/09 13:13:56 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSFSBDB_H
#define _FSFSBDB_H

UINT1 FsMIFsbContextTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIFsbFIPSnoopingTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIFsbIntfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIFsbFIPSessionTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsMIFsbFcfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsMIFsbGlobalStatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIFsbVlanStatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIFsbSessStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};

UINT4 fsfsb [] ={1,3,6,1,4,1,29601,2,102};
tSNMP_OID_TYPE fsfsbOID = {9, fsfsb};


UINT4 FsMIFsbContextId [ ] ={1,3,6,1,4,1,29601,2,102,1,1,1,1};
UINT4 FsMIFsbSystemControl [ ] ={1,3,6,1,4,1,29601,2,102,1,1,1,2};
UINT4 FsMIFsbModuleStatus [ ] ={1,3,6,1,4,1,29601,2,102,1,1,1,3};
UINT4 FsMIFsbFcMapMode [ ] ={1,3,6,1,4,1,29601,2,102,1,1,1,4};
UINT4 FsMIFsbFcmap [ ] ={1,3,6,1,4,1,29601,2,102,1,1,1,5};
UINT4 FsMIFsbHouseKeepingTimePeriod [ ] ={1,3,6,1,4,1,29601,2,102,1,1,1,6};
UINT4 FsMIFsbTraceOption [ ] ={1,3,6,1,4,1,29601,2,102,1,1,1,7};
UINT4 FsMIFsbTrapStatus [ ] ={1,3,6,1,4,1,29601,2,102,1,1,1,8};
UINT4 FsMIFsbClearStats [ ] ={1,3,6,1,4,1,29601,2,102,1,1,1,9};
UINT4 FsMIFsbDefaultVlanId [ ] ={1,3,6,1,4,1,29601,2,102,1,1,1,10};
UINT4 FsMIFsbRowStatus [ ] ={1,3,6,1,4,1,29601,2,102,1,1,1,11};
UINT4 FsMIFsbTraceSeverityLevel [ ] ={1,3,6,1,4,1,29601,2,102,1,1,1,12};
UINT4 FsMIFsbFIPSnoopingVlanIndex [ ] ={1,3,6,1,4,1,29601,2,102,1,2,1,1};
UINT4 FsMIFsbFIPSnoopingFcmap [ ] ={1,3,6,1,4,1,29601,2,102,1,2,1,2};
UINT4 FsMIFsbFIPSnoopingEnabledStatus [ ] ={1,3,6,1,4,1,29601,2,102,1,2,1,3};
UINT4 FsMIFsbFIPSnoopingRowStatus [ ] ={1,3,6,1,4,1,29601,2,102,1,2,1,4};
UINT4 FsMIFsbFIPSnoopingPinnedPorts [ ] ={1,3,6,1,4,1,29601,2,102,1,2,1,5};
UINT4 FsMIFsbIntfVlanIndex [ ] ={1,3,6,1,4,1,29601,2,102,2,1,1,1};
UINT4 FsMIFsbIntfIfIndex [ ] ={1,3,6,1,4,1,29601,2,102,2,1,1,2};
UINT4 FsMIFsbIntfPortRole [ ] ={1,3,6,1,4,1,29601,2,102,2,1,1,3};
UINT4 FsMIFsbIntfRowStatus [ ] ={1,3,6,1,4,1,29601,2,102,2,1,1,4};
UINT4 FsMIFsbFIPSessionVlanId [ ] ={1,3,6,1,4,1,29601,2,102,2,2,1,1};
UINT4 FsMIFsbFIPSessionEnodeIfIndex [ ] ={1,3,6,1,4,1,29601,2,102,2,2,1,2};
UINT4 FsMIFsbFIPSessionEnodeMacAddress [ ] ={1,3,6,1,4,1,29601,2,102,2,2,1,3};
UINT4 FsMIFsbFIPSessionFcfMacAddress [ ] ={1,3,6,1,4,1,29601,2,102,2,2,1,4};
UINT4 FsMIFsbFIPSessionFCoEMacAddress [ ] ={1,3,6,1,4,1,29601,2,102,2,2,1,5};
UINT4 FsMIFsbFIPSessionFcMap [ ] ={1,3,6,1,4,1,29601,2,102,2,2,1,6};
UINT4 FsMIFsbFIPSessionFcfIfIndex [ ] ={1,3,6,1,4,1,29601,2,102,2,2,1,7};
UINT4 FsMIFsbFIPSessionFcfNameId [ ] ={1,3,6,1,4,1,29601,2,102,2,2,1,8};
UINT4 FsMIFsbFIPSessionFcId [ ] ={1,3,6,1,4,1,29601,2,102,2,2,1,9};
UINT4 FsMIFsbFIPSessionEnodeConnectType [ ] ={1,3,6,1,4,1,29601,2,102,2,2,1,10};
UINT4 FsMIFsbFIPSessionHouseKeepingTimerStatus [ ] ={1,3,6,1,4,1,29601,2,102,2,2,1,11};
UINT4 FsMIFsbFcfVlanId [ ] ={1,3,6,1,4,1,29601,2,102,2,3,1,1};
UINT4 FsMIFsbFcfIfIndex [ ] ={1,3,6,1,4,1,29601,2,102,2,3,1,2};
UINT4 FsMIFsbFcfMacAddress [ ] ={1,3,6,1,4,1,29601,2,102,2,3,1,3};
UINT4 FsMIFsbFcfFcMap [ ] ={1,3,6,1,4,1,29601,2,102,2,3,1,4};
UINT4 FsMIFsbFcfAddressingMode [ ] ={1,3,6,1,4,1,29601,2,102,2,3,1,5};
UINT4 FsMIFsbFcfEnodeLoginCount [ ] ={1,3,6,1,4,1,29601,2,102,2,3,1,6};
UINT4 FsMIFsbFcfNameId [ ] ={1,3,6,1,4,1,29601,2,102,2,3,1,7};
UINT4 FsMIFsbFcfFabricName [ ] ={1,3,6,1,4,1,29601,2,102,2,3,1,8};
UINT4 FsMIFsbFcfPinnedPorts [ ] ={1,3,6,1,4,1,29601,2,102,2,3,1,9};
UINT4 FsMIFsbGlobalStatsVlanRequests [ ] ={1,3,6,1,4,1,29601,2,102,3,1,1,1};
UINT4 FsMIFsbGlobalStatsVlanNotification [ ] ={1,3,6,1,4,1,29601,2,102,3,1,1,2};
UINT4 FsMIFsbVlanStatsUnicastDisAdv [ ] ={1,3,6,1,4,1,29601,2,102,3,2,1,1};
UINT4 FsMIFsbVlanStatsMulticastDisAdv [ ] ={1,3,6,1,4,1,29601,2,102,3,2,1,2};
UINT4 FsMIFsbVlanStatsUnicastDisSol [ ] ={1,3,6,1,4,1,29601,2,102,3,2,1,3};
UINT4 FsMIFsbVlanStatsMulticastDisSol [ ] ={1,3,6,1,4,1,29601,2,102,3,2,1,4};
UINT4 FsMIFsbVlanStatsFLOGICount [ ] ={1,3,6,1,4,1,29601,2,102,3,2,1,5};
UINT4 FsMIFsbVlanStatsFDISCCount [ ] ={1,3,6,1,4,1,29601,2,102,3,2,1,6};
UINT4 FsMIFsbVlanStatsLOGOCount [ ] ={1,3,6,1,4,1,29601,2,102,3,2,1,7};
UINT4 FsMIFsbVlanStatsFLOGIAcceptCount [ ] ={1,3,6,1,4,1,29601,2,102,3,2,1,8};
UINT4 FsMIFsbVlanStatsFLOGIRejectCount [ ] ={1,3,6,1,4,1,29601,2,102,3,2,1,9};
UINT4 FsMIFsbVlanStatsFDISCAcceptCount [ ] ={1,3,6,1,4,1,29601,2,102,3,2,1,10};
UINT4 FsMIFsbVlanStatsFDISCRejectCount [ ] ={1,3,6,1,4,1,29601,2,102,3,2,1,11};
UINT4 FsMIFsbVlanStatsLOGOAcceptCount [ ] ={1,3,6,1,4,1,29601,2,102,3,2,1,12};
UINT4 FsMIFsbVlanStatsLOGORejectCount [ ] ={1,3,6,1,4,1,29601,2,102,3,2,1,13};
UINT4 FsMIFsbVlanStatsClearLinkCount [ ] ={1,3,6,1,4,1,29601,2,102,3,2,1,14};
UINT4 FsMIFsbVlanFcMapMisMatchCount [ ] ={1,3,6,1,4,1,29601,2,102,3,2,1,15};
UINT4 FsMIFsbVlanMTUMisMatchCount [ ] ={1,3,6,1,4,1,29601,2,102,3,2,1,16};
UINT4 FsMIFsbVlanACLFailureCount [ ] ={1,3,6,1,4,1,29601,2,102,3,2,1,17};
UINT4 FsMIFsbVlanInvalidFIPFramesCount [ ] ={1,3,6,1,4,1,29601,2,102,3,2,1,18};
UINT4 FsMIFsbVlanFCFDiscoveryTimeoutsCount [ ] ={1,3,6,1,4,1,29601,2,102,3,2,1,19};
UINT4 FsMIFsbSessStatsVlanId [ ] ={1,3,6,1,4,1,29601,2,102,3,3,1,1};
UINT4 FsMIFsbSessStatsEnodeIfIndex [ ] ={1,3,6,1,4,1,29601,2,102,3,3,1,2};
UINT4 FsMIFsbSessStatsEnodeMacAddress [ ] ={1,3,6,1,4,1,29601,2,102,3,3,1,3};
UINT4 FsMIFsbSessStatsFcfMacAddress [ ] ={1,3,6,1,4,1,29601,2,102,3,3,1,4};
UINT4 FsMIFsbSessStatsFCoEMacAddress [ ] ={1,3,6,1,4,1,29601,2,102,3,3,1,5};
UINT4 FsMIFsbSessStatsEnodeKeepAliveCount [ ] ={1,3,6,1,4,1,29601,2,102,3,3,1,6};
UINT4 FsMIFsbSessStatsVNPortKeepAliveCount [ ] ={1,3,6,1,4,1,29601,2,102,3,3,1,7};
UINT4 FsMIFsbSessionVlanId [ ] ={1,3,6,1,4,1,29601,2,102,4,1,1};
UINT4 FsMIFsbSessionEnodeIfIndex [ ] ={1,3,6,1,4,1,29601,2,102,4,1,2};
UINT4 FsMIFsbSessionEnodeMacAddress [ ] ={1,3,6,1,4,1,29601,2,102,4,1,3};




tMbDbEntry fsfsbMibEntry[]= {

{{13,FsMIFsbContextId}, GetNextIndexFsMIFsbContextTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIFsbContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIFsbSystemControl}, GetNextIndexFsMIFsbContextTable, FsMIFsbSystemControlGet, FsMIFsbSystemControlSet, FsMIFsbSystemControlTest, FsMIFsbContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsbContextTableINDEX, 1, 0, 0, "2"},

{{13,FsMIFsbModuleStatus}, GetNextIndexFsMIFsbContextTable, FsMIFsbModuleStatusGet, FsMIFsbModuleStatusSet, FsMIFsbModuleStatusTest, FsMIFsbContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsbContextTableINDEX, 1, 0, 0, "2"},

{{13,FsMIFsbFcMapMode}, GetNextIndexFsMIFsbContextTable, FsMIFsbFcMapModeGet, FsMIFsbFcMapModeSet, FsMIFsbFcMapModeTest, FsMIFsbContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsbContextTableINDEX, 1, 0, 0, "1"},

{{13,FsMIFsbFcmap}, GetNextIndexFsMIFsbContextTable, FsMIFsbFcmapGet, FsMIFsbFcmapSet, FsMIFsbFcmapTest, FsMIFsbContextTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIFsbContextTableINDEX, 1, 0, 0, "982016"},

{{13,FsMIFsbHouseKeepingTimePeriod}, GetNextIndexFsMIFsbContextTable, FsMIFsbHouseKeepingTimePeriodGet, FsMIFsbHouseKeepingTimePeriodSet, FsMIFsbHouseKeepingTimePeriodTest, FsMIFsbContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIFsbContextTableINDEX, 1, 0, 0, "300"},

{{13,FsMIFsbTraceOption}, GetNextIndexFsMIFsbContextTable, FsMIFsbTraceOptionGet, FsMIFsbTraceOptionSet, FsMIFsbTraceOptionTest, FsMIFsbContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFsbContextTableINDEX, 1, 0, 0, "0"},

{{13,FsMIFsbTrapStatus}, GetNextIndexFsMIFsbContextTable, FsMIFsbTrapStatusGet, FsMIFsbTrapStatusSet, FsMIFsbTrapStatusTest, FsMIFsbContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsbContextTableINDEX, 1, 0, 0, "2"},

{{13,FsMIFsbClearStats}, GetNextIndexFsMIFsbContextTable, FsMIFsbClearStatsGet, FsMIFsbClearStatsSet, FsMIFsbClearStatsTest, FsMIFsbContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsbContextTableINDEX, 1, 0, 0, NULL},
#ifndef FSB_CUSTOM_FCOE_TAGGED_WANTED
/* IMPORTANT :- This changes are exception taken for not to modifiy 
 * the generated db.h file. With package compiled for
 * FSB_CUSTOM_FCOE_TAGGED_WANTED option - 
 * the MIB object fsMIfsbDefaultVlanId is not applicable,
 * Hence is entry is excluded in db.h, to avoid its presence in SNMP registeration.
 * The changes/customization needs to be taken while regenerating the MIB 
 * and integrating the generated db.h file. */
{{13,FsMIFsbDefaultVlanId}, GetNextIndexFsMIFsbContextTable, FsMIFsbDefaultVlanIdGet, FsMIFsbDefaultVlanIdSet, FsMIFsbDefaultVlanIdTest, FsMIFsbContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsbContextTableINDEX, 1, 0, 0, NULL},
#endif
{{13,FsMIFsbRowStatus}, GetNextIndexFsMIFsbContextTable, FsMIFsbRowStatusGet, FsMIFsbRowStatusSet, FsMIFsbRowStatusTest, FsMIFsbContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsbContextTableINDEX, 1, 0, 1, NULL},

{{13,FsMIFsbTraceSeverityLevel}, GetNextIndexFsMIFsbContextTable, FsMIFsbTraceSeverityLevelGet, FsMIFsbTraceSeverityLevelSet, FsMIFsbTraceSeverityLevelTest, FsMIFsbContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsbContextTableINDEX, 1, 0, 0, "2"},

{{13,FsMIFsbFIPSnoopingVlanIndex}, GetNextIndexFsMIFsbFIPSnoopingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIFsbFIPSnoopingTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbFIPSnoopingFcmap}, GetNextIndexFsMIFsbFIPSnoopingTable, FsMIFsbFIPSnoopingFcmapGet, FsMIFsbFIPSnoopingFcmapSet, FsMIFsbFIPSnoopingFcmapTest, FsMIFsbFIPSnoopingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIFsbFIPSnoopingTableINDEX, 2, 0, 0, "982016"},

{{13,FsMIFsbFIPSnoopingEnabledStatus}, GetNextIndexFsMIFsbFIPSnoopingTable, FsMIFsbFIPSnoopingEnabledStatusGet, FsMIFsbFIPSnoopingEnabledStatusSet, FsMIFsbFIPSnoopingEnabledStatusTest, FsMIFsbFIPSnoopingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsbFIPSnoopingTableINDEX, 2, 0, 0, "2"},

{{13,FsMIFsbFIPSnoopingRowStatus}, GetNextIndexFsMIFsbFIPSnoopingTable, FsMIFsbFIPSnoopingRowStatusGet, FsMIFsbFIPSnoopingRowStatusSet, FsMIFsbFIPSnoopingRowStatusTest, FsMIFsbFIPSnoopingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsbFIPSnoopingTableINDEX, 2, 0, 1, NULL},

{{13,FsMIFsbFIPSnoopingPinnedPorts}, GetNextIndexFsMIFsbFIPSnoopingTable, FsMIFsbFIPSnoopingPinnedPortsGet, FsMIFsbFIPSnoopingPinnedPortsSet, FsMIFsbFIPSnoopingPinnedPortsTest, FsMIFsbFIPSnoopingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIFsbFIPSnoopingTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbIntfVlanIndex}, GetNextIndexFsMIFsbIntfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIFsbIntfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbIntfIfIndex}, GetNextIndexFsMIFsbIntfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIFsbIntfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbIntfPortRole}, GetNextIndexFsMIFsbIntfTable, FsMIFsbIntfPortRoleGet, FsMIFsbIntfPortRoleSet, FsMIFsbIntfPortRoleTest, FsMIFsbIntfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsbIntfTableINDEX, 2, 0, 0, "1"},

{{13,FsMIFsbIntfRowStatus}, GetNextIndexFsMIFsbIntfTable, FsMIFsbIntfRowStatusGet, FsMIFsbIntfRowStatusSet, FsMIFsbIntfRowStatusTest, FsMIFsbIntfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsbIntfTableINDEX, 2, 0, 1, NULL},

{{13,FsMIFsbFIPSessionVlanId}, GetNextIndexFsMIFsbFIPSessionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIFsbFIPSessionTableINDEX, 5, 0, 0, NULL},

{{13,FsMIFsbFIPSessionEnodeIfIndex}, GetNextIndexFsMIFsbFIPSessionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIFsbFIPSessionTableINDEX, 5, 0, 0, NULL},

{{13,FsMIFsbFIPSessionEnodeMacAddress}, GetNextIndexFsMIFsbFIPSessionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsMIFsbFIPSessionTableINDEX, 5, 0, 0, NULL},

{{13,FsMIFsbFIPSessionFcfMacAddress}, GetNextIndexFsMIFsbFIPSessionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsMIFsbFIPSessionTableINDEX, 5, 0, 0, NULL},

{{13,FsMIFsbFIPSessionFCoEMacAddress}, GetNextIndexFsMIFsbFIPSessionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsMIFsbFIPSessionTableINDEX, 5, 0, 0, NULL},

{{13,FsMIFsbFIPSessionFcMap}, GetNextIndexFsMIFsbFIPSessionTable, FsMIFsbFIPSessionFcMapGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIFsbFIPSessionTableINDEX, 5, 0, 0, NULL},

{{13,FsMIFsbFIPSessionFcfIfIndex}, GetNextIndexFsMIFsbFIPSessionTable, FsMIFsbFIPSessionFcfIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIFsbFIPSessionTableINDEX, 5, 0, 0, NULL},

{{13,FsMIFsbFIPSessionFcfNameId}, GetNextIndexFsMIFsbFIPSessionTable, FsMIFsbFIPSessionFcfNameIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIFsbFIPSessionTableINDEX, 5, 0, 0, NULL},

{{13,FsMIFsbFIPSessionFcId}, GetNextIndexFsMIFsbFIPSessionTable, FsMIFsbFIPSessionFcIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIFsbFIPSessionTableINDEX, 5, 0, 0, NULL},

{{13,FsMIFsbFIPSessionEnodeConnectType}, GetNextIndexFsMIFsbFIPSessionTable, FsMIFsbFIPSessionEnodeConnectTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIFsbFIPSessionTableINDEX, 5, 0, 0, NULL},

{{13,FsMIFsbFIPSessionHouseKeepingTimerStatus}, GetNextIndexFsMIFsbFIPSessionTable, FsMIFsbFIPSessionHouseKeepingTimerStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIFsbFIPSessionTableINDEX, 5, 0, 0, NULL},

{{13,FsMIFsbFcfVlanId}, GetNextIndexFsMIFsbFcfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIFsbFcfTableINDEX, 3, 0, 0, NULL},

{{13,FsMIFsbFcfIfIndex}, GetNextIndexFsMIFsbFcfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIFsbFcfTableINDEX, 3, 0, 0, NULL},

{{13,FsMIFsbFcfMacAddress}, GetNextIndexFsMIFsbFcfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsMIFsbFcfTableINDEX, 3, 0, 0, NULL},

{{13,FsMIFsbFcfFcMap}, GetNextIndexFsMIFsbFcfTable, FsMIFsbFcfFcMapGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIFsbFcfTableINDEX, 3, 0, 0, NULL},

{{13,FsMIFsbFcfAddressingMode}, GetNextIndexFsMIFsbFcfTable, FsMIFsbFcfAddressingModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIFsbFcfTableINDEX, 3, 0, 0, "1"},

{{13,FsMIFsbFcfEnodeLoginCount}, GetNextIndexFsMIFsbFcfTable, FsMIFsbFcfEnodeLoginCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIFsbFcfTableINDEX, 3, 0, 0, NULL},

{{13,FsMIFsbFcfNameId}, GetNextIndexFsMIFsbFcfTable, FsMIFsbFcfNameIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIFsbFcfTableINDEX, 3, 0, 0, NULL},

{{13,FsMIFsbFcfFabricName}, GetNextIndexFsMIFsbFcfTable, FsMIFsbFcfFabricNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIFsbFcfTableINDEX, 3, 0, 0, NULL},

{{13,FsMIFsbFcfPinnedPorts}, GetNextIndexFsMIFsbFcfTable, FsMIFsbFcfPinnedPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIFsbFcfTableINDEX, 3, 0, 0, NULL},

{{13,FsMIFsbGlobalStatsVlanRequests}, GetNextIndexFsMIFsbGlobalStatsTable, FsMIFsbGlobalStatsVlanRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIFsbGlobalStatsVlanNotification}, GetNextIndexFsMIFsbGlobalStatsTable, FsMIFsbGlobalStatsVlanNotificationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIFsbVlanStatsUnicastDisAdv}, GetNextIndexFsMIFsbVlanStatsTable, FsMIFsbVlanStatsUnicastDisAdvGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbVlanStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbVlanStatsMulticastDisAdv}, GetNextIndexFsMIFsbVlanStatsTable, FsMIFsbVlanStatsMulticastDisAdvGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbVlanStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbVlanStatsUnicastDisSol}, GetNextIndexFsMIFsbVlanStatsTable, FsMIFsbVlanStatsUnicastDisSolGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbVlanStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbVlanStatsMulticastDisSol}, GetNextIndexFsMIFsbVlanStatsTable, FsMIFsbVlanStatsMulticastDisSolGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbVlanStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbVlanStatsFLOGICount}, GetNextIndexFsMIFsbVlanStatsTable, FsMIFsbVlanStatsFLOGICountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbVlanStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbVlanStatsFDISCCount}, GetNextIndexFsMIFsbVlanStatsTable, FsMIFsbVlanStatsFDISCCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbVlanStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbVlanStatsLOGOCount}, GetNextIndexFsMIFsbVlanStatsTable, FsMIFsbVlanStatsLOGOCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbVlanStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbVlanStatsFLOGIAcceptCount}, GetNextIndexFsMIFsbVlanStatsTable, FsMIFsbVlanStatsFLOGIAcceptCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbVlanStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbVlanStatsFLOGIRejectCount}, GetNextIndexFsMIFsbVlanStatsTable, FsMIFsbVlanStatsFLOGIRejectCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbVlanStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbVlanStatsFDISCAcceptCount}, GetNextIndexFsMIFsbVlanStatsTable, FsMIFsbVlanStatsFDISCAcceptCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbVlanStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbVlanStatsFDISCRejectCount}, GetNextIndexFsMIFsbVlanStatsTable, FsMIFsbVlanStatsFDISCRejectCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbVlanStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbVlanStatsLOGOAcceptCount}, GetNextIndexFsMIFsbVlanStatsTable, FsMIFsbVlanStatsLOGOAcceptCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbVlanStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbVlanStatsLOGORejectCount}, GetNextIndexFsMIFsbVlanStatsTable, FsMIFsbVlanStatsLOGORejectCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbVlanStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbVlanStatsClearLinkCount}, GetNextIndexFsMIFsbVlanStatsTable, FsMIFsbVlanStatsClearLinkCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbVlanStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbVlanFcMapMisMatchCount}, GetNextIndexFsMIFsbVlanStatsTable, FsMIFsbVlanFcMapMisMatchCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbVlanStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbVlanMTUMisMatchCount}, GetNextIndexFsMIFsbVlanStatsTable, FsMIFsbVlanMTUMisMatchCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbVlanStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbVlanACLFailureCount}, GetNextIndexFsMIFsbVlanStatsTable, FsMIFsbVlanACLFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbVlanStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbVlanInvalidFIPFramesCount}, GetNextIndexFsMIFsbVlanStatsTable, FsMIFsbVlanInvalidFIPFramesCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbVlanStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbVlanFCFDiscoveryTimeoutsCount}, GetNextIndexFsMIFsbVlanStatsTable, FsMIFsbVlanFCFDiscoveryTimeoutsCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbVlanStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsMIFsbSessStatsVlanId}, GetNextIndexFsMIFsbSessStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIFsbSessStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMIFsbSessStatsEnodeIfIndex}, GetNextIndexFsMIFsbSessStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIFsbSessStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMIFsbSessStatsEnodeMacAddress}, GetNextIndexFsMIFsbSessStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsMIFsbSessStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMIFsbSessStatsFcfMacAddress}, GetNextIndexFsMIFsbSessStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsMIFsbSessStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMIFsbSessStatsFCoEMacAddress}, GetNextIndexFsMIFsbSessStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsMIFsbSessStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMIFsbSessStatsEnodeKeepAliveCount}, GetNextIndexFsMIFsbSessStatsTable, FsMIFsbSessStatsEnodeKeepAliveCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbSessStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMIFsbSessStatsVNPortKeepAliveCount}, GetNextIndexFsMIFsbSessStatsTable, FsMIFsbSessStatsVNPortKeepAliveCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsbSessStatsTableINDEX, 5, 0, 0, NULL},

{{12,FsMIFsbSessionVlanId}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{12,FsMIFsbSessionEnodeIfIndex}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{12,FsMIFsbSessionEnodeMacAddress}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
#ifndef FSB_CUSTOM_FCOE_TAGGED_WANTED
tMibData fsfsbEntry = { 72, fsfsbMibEntry };
#else
tMibData fsfsbEntry = { 71, fsfsbMibEntry };
#endif
#endif /* _FSFSBDB_H */

