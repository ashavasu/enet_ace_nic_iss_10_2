/*************************************************************************
* Copyright (C) 2007-2012 Aricent Group . All Rights Reserved
*
* $Id: fsbmacro.h,v 1.15 2017/11/07 12:23:59 siva Exp $
*
* Description: This file contains the macros used in FIP-snooping Module
*
*************************************************************************/
#ifndef _FSBMACRO_H
#define _FSBMACRO_H

/* FIP-snooping Packet Related Macros */
#define FSB_MAX_FIP_PKT_LEN                 2500
#define FSB_FCMAP_LEN                       3
#define FSB_FCID_LEN                        3
#define FSB_NAME_ID_LEN                     8
#define FSB_FABRIC_NAME_LEN                 8
#define FSB_ETHER_TYPE_LEN                  2 
#define FSB_OPCODE_LEN                      2
#define FSB_SUB_OPCODE_LEN                  1
#define FSB_FLAG_LEN                        2

#define FSB_DESCRIPTOR_LEN                  1
#define FSB_PRIORITY_LEN                    1   
#define FSB_SWITCH_NAME_LEN                 8
#define FSB_FCMAP_LEN                       3
#define FSB_FKA_ADV_PERIOD_LEN              4
#define FSB_MAX_FRAME_LEN                   2
#define FSB_VN_MAC_TYPE                     11 
#define FSB_ENODE_MAC_TYPE                  2 

/*Addressing Mode Macros*/
#define FSB_FPMA_ADDRESSING_MODE            1
#define FSB_SPMA_ADDRESSING_MODE            2
/*Packet Related Macros*/
#define FSB_ONE_BYTE                        1
#define FSB_TWO_BYTE                        2
#define FSB_THREE_BYTE                      3
#define FSB_FOUR_BYTE                       4
#define FSB_FIVE_BYTE                       5
#define FSB_SIX_BYTE                        6
#define FSB_SEVEN_BYTE                      7
#define FSB_FOURTEEN_BYTE                   14
#define FSB_NAME_ID_OFFSET_FROM_PRIO        12
#define FSB_NEXT_VN_MAC_ADDR_OFFSET         18
#define FSB_VN_MAC_OFFSET_FROM_FLAG         20
#define FSB_DIS_SOL_MTU_OFFSET_FROM_FLAG    22 
#define FCOE_MAC_ADDR_OFFSET_FROM_DESC      117
#define FSB_CVL_MIN_DESC_LEN                10
#define FLOGI_CMD_CODE_OFFSET_FROM_FLAG     27
#define FSB_FLOGO_MAC_DESC_OFFSET_FROM_FLAG 31
 
#define FSB_FIP_LOGI_DESCRIPTOR             0x07
#define FSB_NPIV_FDISC_DESCRIPTOR           0x08
#define FSB_FIP_LOGO_DESCRIPTOR             0x09 
#define FSB_FIP_MAC_DESCRIPTOR              0x02

#define FSB_FLOGI_RJT_CMD_CODE              0x01
#define FSB_FLOGI_ACC_CMD_CODE              0x02

/*Opcode for packets*/
#define FSB_DISCOVERY_ADV_OPCODE            0x0001
#define FSB_LOGI_OPCODE                     0x0002
#define FSB_KEEP_ALIVE_OPCODE               0x0003
#define FSB_FIP_VLAN_OPCODE                 0x0004

#define FSB_SOLICITATION_SUB_OPCODE         0x0001
#define FSB_DISCOVERY_ADV_SUB_OPCODE        0x0002
/*FLOGI sub opcode*/ 
#define FSB_FLOGI_REQ_SUB_OPCODE            0x0001
#define FSB_FLOGI_ACCEPT_SUB_OPCODE         0x0002
 
#define FSB_FIP_KEEP_ALIVE_SUB_OPCODE       0x0001
#define FSB_CLEAR_VIRTUAL_LINK_SUB_OPCODE   0x0002

#define FSB_FIP_VLAN_REQ_SUB_OPCODE          0x0001
#define FSB_FIP_VLAN_NOTIFICATION_SUB_OPCODE 0x0002

/*sub opcode Macros*/
/* BitMask Macros */
#define FSB_FPMA_BIT_MASK                   0x8000
#define FSB_SPMA_BIT_MASK                   0x4000
#define FSB_DIS_ADVERTISEMENT_MASK          0x2000 
#define FSB_DBIT_MASK                       0x0001 
/*Type of Macro*/
#define ALL_ENODE_MAC                       RsvdAllENodeMAC
#define ALL_FCF_MAC                         RsvdAllFCFMAC
#define ALL_ZERO_MAC                        RsvdAllZeroMAC
/* Trace related Macros */
#define FSB_TRC_MAX_SIZE                    255

#define MAC_TO_STRING_LEN                   21

/* Trap related Macros */
#define   FSB_TRAP_OID_LEN                  11
#define   FSB_SESSION_CLEAR                 1
#define   FSB_FCMAP_MISMATCH                2
#define   FSB_MTU_MISMATCH                  3
#define   FSB_ACL_FAILURE                   4

/* Timer  Related Macros */
#define FSB_NUM_OF_MSEC_IN_A_SEC            1000
#define FSB_FCF_TIMEOUT_MULTIPLIER          3

/* Task Related Macros */
#define FSB_TASK_NAME                       ((UINT1 *)"FsbT")
#define FSB_TASK_ID                         gFsbGlobals.gFsbTaskId
#define FSB_TASK_QUEUE_DEPTH                1024
#define FSB_TASK_QUEUE_LIMIT                (FSB_TASK_QUEUE_DEPTH * 0.8)

/* Semaphore Related Macros */
#define FSB_SEM_NAME                        ((UINT1 *)"FsbS")
#define FSB_SEM_ID                          gFsbGlobals.gFsbSemId
#define FSB_SESSION_RB_PREFIX_NAME          ((UINT1 *)"S")
#define FSB_FCOE_RB_PREFIX_NAME             ((UINT1 *)"F")

/* Queue Related Macros */
#define FSB_TASK_QUEUE_NAME                 ((UINT1 *)"FsbQ")
#define FSB_QUEUE_ID                        gFsbGlobals.gFsbTaskQId
#define FSB_QUEUE_EVENT                     0x01
#define FSB_TIMER_EXP_EVENT                 0x02
#define FSB_RED_BULK_UPD_EVENT              0x04
#define FSB_ALL_EVENTS                      (FSB_QUEUE_EVENT | FSB_TIMER_EXP_EVENT | FSB_RED_BULK_UPD_EVENT)

/* Memory Pool Related Macros */
#define FSB_MEMORY_TYPE                     MEM_DEFAULT_MEMORY_TYPE
#define FSB_CONTEXT_MEMPOOL_ID              gFsbGlobals.FsbContextPoolId
#define FSB_FCOE_VLAN_ENTRIES_MEMPOOL_ID    gFsbGlobals.FsbFipSnoopingNodePoolId
#define FSB_INTF_ENTRIES_MEMPOOL_ID         gFsbGlobals.FsbIntfEntryNodePoolId
#define FSB_SESSION_ENTRIES_MEMPOOL_ID      gFsbGlobals.FsbFipSessEntryNodePoolId
#define FSB_FCOE_ENTRIES_MEMPOOL_ID         gFsbGlobals.FsbFipSessFCoEEntryNodePoolId
#define FSB_FCF_ENTRIES_MEMPOOL_ID          gFsbGlobals.FsbFcfEntryNodePoolId
#define FSB_NOTIFY_PARAMS_INFO_MEMPOOL_ID   gFsbGlobals.FsbNotifyParamsPoolId
#define FSB_MBSM_MSG_MEMPOOL_ID             gFsbGlobals.FsbMbsmMsgPoolId
#define FSB_FILTER_ENTRIES_MEMPOOL_ID       gFsbGlobals.FsbFilterEntryNodePoolId
#define FSB_FCOE_MAC_ADDR_MEMPOOL_ID        gFsbGlobals.FsbFCoEMacAddrPoolId
#define FSB_SCHAN_FILT_ENTRIES_MEMPOOL_ID   gFsbGlobals.FsbSChannelMemPoolId

/* Call Back Macro */
#define FSB_CALLBACK       gFsbCallBack

/* LR Related Macros */
#define FSB_INIT_COMPLETE(u4Status)         lrInitComplete(u4Status)

/* Context Related Macros */
#define FSB_DEFAULT_CONTEXT_ID              L2IWF_DEFAULT_CONTEXT
#define FSB_MAX_CONTEXT_COUNT               SYS_DEF_MAX_NUM_CONTEXTS
#define FSB_ALIAS_MAX_LEN                   VCM_ALIAS_MAX_LEN

/* FSAP Related Macros */
#define FSB_FALSE                           OSIX_FALSE
#define FSB_TRUE                            OSIX_TRUE

#define FSB_GREATER                         1
#define FSB_EQUAL                           0
#define FSB_LESSER                         -1

#define FSB_MEMSET                          MEMSET
#define FSB_MEMCPY                          MEMCPY
#define FSB_MEMCMP                          MEMCMP


#define FSB_GET_1BYTE(u1Val, pu1PktBuf) \
        do {                             \
           u1Val = *pu1PktBuf;           \
           pu1PktBuf += 1;        \
        } while(0)

#define FSB_GET_2BYTE(u2Val, pu1PktBuf)            \
        do {                                        \
           FSB_MEMCPY (&u2Val, pu1PktBuf, 2);\
           u2Val = (UINT2)FSB_NTOHS(u2Val);               \
           pu1PktBuf += 2;                   \
        } while(0)

#define FSB_GET_4BYTE(u4Val, pu1PktBuf)              \
        do {                                          \
           FSB_MEMCPY (&u4Val, pu1PktBuf, 4); \
           u4Val = FSB_NTOHL(u4Val);                 \
           pu1PktBuf += 4;                    \
        } while(0)

#define   FSB_HTONS             OSIX_HTONS
#define   FSB_HTONL             OSIX_HTONL
#define   FSB_NTOHS             OSIX_NTOHS
#define   FSB_NTOHL             OSIX_NTOHL

#define FSB_GET_FILTER_ID()     ++gu4FilterId

/* Priority */
#define FSB_FIELD_ENTRY_PRIO_LOWEST         0
#define FSB_FIELD_ENTRY_PRIO_HIGHEST        1
#define FSB_FIELD_ENTRY_VLAN_REQ_PRIO       2
#define FSB_FIELD_ENTRY_KEEP_ALIVE_PRIO     2
#define FSB_FIELD_ENTRY_DISC_ADV_PRIO       2
#define FSB_FIELD_ENTRY_SOLI_MULTI_PRIO     3

/* Ether Type */
#define FIP_ETHER_TYPE                       0x8914
#define FCOE_ETHER_TYPE                      0x8906

/* FIP - Discovery */
#define FSB_DISCOVERY                       1
#define FSB_DISCOVERY_SOLICITATION          1
#define FSB_DISCOVERY_ADVERTISEMENT         2

/* FIP - Instantiation */
#define FSB_VL_INSTANTIATION                2
#define FSB_VL_INSTANTIATION_REQUEST        1
#define FSB_VL_INSTANTIATION_REPLY          2

/* FIP - Keep Alive */
#define FSB_FIP_KEEP_ALIVE                  1
#define FSB_FIP_CLEAR_VIRTUAL_LINKS         2

/* FIP - VLAN Discovery */
#define FSB_VLAN_DISCOVERY                  4
#define FSB_VLAN_REQUEST                    1
#define FSB_VLAN_REPLY                      2

#define FSB_FIP_FLOGI                       1
#define FSB_FIP_FDISC                       2

#define FSB_SESS_ESTABLISHMENT_TMR_VALUE    20
#define FSB_MBSM_STAGERRING_TIME            1

#define FSB_FCMAP_MISMATCH_STATS            19
#define FSB_MTU_MISMATCH_STATS              20
#define FSB_ACL_FAILURE_STATS               21
#define FSB_INVAILD_FIP_FRAMES              22
#define FSB_FCF_DISCOVERY_TIMEOUT           23
#define FSB_MTU_FRAME_SIZE                  2500
enum
{
 FSB_ALLOW_DIS_SOLICITATION = 1,
 FSB_ALLOW_DIS_ADVERTISEMENT,
 FSB_GLOBAL_DENY,
};

enum
{
    FSB_FCOE_TRAFFIC_FROM_FCF = 1,
    FSB_FCOE_TRAFFIC_FROM_ENODE,
    FSB_VN_PORT_KEEP_ALIVE,
    FSB_ENODE_KEEP_ALIVE,
    FSB_FIP_CLEAR_VIRTUAL_LINK
};
/*port related Macros*/
#define  FSB_IF_UP                           1
#define  FSB_IF_DOWN                         2
#define  FSB_IF_NP                           CFA_IF_NP


#define FSB_MBSM_MAX_STAGGERING_VLAN_COUNT  11


#ifdef MBSM_WANTED
#define FSB_MBSM_NEXT_INTF_ENTRY() \
        gFsbGlobals.FsbMbsmGlobalInfo.pNextFsbIntfEntry
#endif

#define FSB_OFFSET_NONE 0
#endif  /* _FSBMACRO_H */

