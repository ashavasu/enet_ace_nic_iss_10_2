/*************************************************************************
* Copyright (C) 2007-2015 Aricent Group . All Rights Reserved
*
* $Id: fsbtrc.h,v 1.6 2017/10/09 13:13:56 siva Exp $
*
* Description: This file contains procedures and definitions used for
*              debugging FIP-snooping Module
*
*************************************************************************/

#ifndef _FSBTRC_H_
#define _FSBTRC_H_

/* Trace and debug flags */
#define FSB_MODULE_TRACE(u4ContextId)     \
    gFsbGlobals.apFsbContextInfo[u4ContextId]->u4TraceOption
#define FSB_TRC_LEVEL_FLAG(u4ContextId) \
    gFsbGlobals.apFsbContextInfo[u4ContextId]->u4TraceSeverityLevel
#define FSB_CONTEXT_TRC             FsbUtilContextTrc

#define FSB_GLOBAL_TRC(args) \
     UtlTrcLog (FSB_NO_TRC, FSB_NO_TRC, FSB_MODULE_NAME, args)

/* Module name */
#define FSB_MODULE_NAME             ((const char *)"FSB")
 
#define FSB_TRC_MIN_VAL                  0
#define FSB_TRC_MAX_VAL                  0x3FF 


/* This Macro is used for the trace purpose only.  This needs to be revisited */
#define FSB_DEFAULT_CXT_TRACE            0 

#define FSB_PKT_DUMP                  FsbDumpPacket
#endif/* _FSBTRC_H_ */

