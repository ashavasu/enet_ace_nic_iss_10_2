/*************************************************************************
* Copyright (C) 2007-2015 Aricent Group . All Rights Reserved
*
* $Id: fsbsz.h,v 1.5 2016/03/05 12:04:01 siva Exp $
*
* Description: This file contains FIP snooping Module mempool init routines
*
*************************************************************************/

enum {
    MAX_FSB_CONTEXT_SIZING_ID,
    MAX_FSB_FCF_ENTRIES_SIZING_ID,
    MAX_FSB_FCOE_ENTRIES_SIZING_ID,
    MAX_FSB_FCOE_MAC_COUNT_SIZING_ID,
    MAX_FSB_FCOE_VLAN_ENTRIES_SIZING_ID,
    MAX_FSB_FILTER_ENTRIES_SIZING_ID,
    MAX_FSB_FIP_SESS_ENTRIES_SIZING_ID,
    MAX_FSB_INTF_ENTRIES_SIZING_ID,
    MAX_FSB_MBSM_MSG_SIZING_ID,
    MAX_FSB_NOTIFY_PARAMS_INFO_SIZING_ID,
    MAX_FSB_SCHANNEL_FILTER_ENTRIES_SIZING_ID,
    FSB_MAX_SIZING_ID
};


#ifdef  _FSBSZ_C
tMemPoolId FSBMemPoolIds[ FSB_MAX_SIZING_ID];
INT4  FsbSizingMemCreateMemPools(VOID);
VOID  FsbSizingMemDeleteMemPools(VOID);
INT4  FsbSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _FSBSZ_C  */
extern tMemPoolId FSBMemPoolIds[ ];
extern INT4  FsbSizingMemCreateMemPools(VOID);
extern VOID  FsbSizingMemDeleteMemPools(VOID);
#endif /*  _FSBSZ_C  */


#ifdef  _FSBSZ_C
tFsModSizingParams FsFSBSizingParams [] = {
{ "tFsbContextInfo", "MAX_FSB_CONTEXT", sizeof(tFsbContextInfo),MAX_FSB_CONTEXT, MAX_FSB_CONTEXT,0 },
{ "tFsbFcfEntry", "MAX_FSB_FCF_ENTRIES", sizeof(tFsbFcfEntry),MAX_FSB_FCF_ENTRIES, MAX_FSB_FCF_ENTRIES,0 },
{ "tFsbFipSessFCoEEntry", "MAX_FSB_FCOE_ENTRIES", sizeof(tFsbFipSessFCoEEntry),MAX_FSB_FCOE_ENTRIES, MAX_FSB_FCOE_ENTRIES,0 },
{ "UINT1[MAX_FSB_FCOE_MAC_SIZE]", "MAX_FSB_FCOE_MAC_COUNT", sizeof(UINT1[MAX_FSB_FCOE_MAC_SIZE]),MAX_FSB_FCOE_MAC_COUNT, MAX_FSB_FCOE_MAC_COUNT,0 },
{ "tFsbFipSnoopingEntry", "MAX_FSB_FCOE_VLAN_ENTRIES", sizeof(tFsbFipSnoopingEntry),MAX_FSB_FCOE_VLAN_ENTRIES, MAX_FSB_FCOE_VLAN_ENTRIES,0 },
{ "tFsbFilterEntry", "MAX_FSB_FILTER_ENTRIES", sizeof(tFsbFilterEntry),MAX_FSB_FILTER_ENTRIES, MAX_FSB_FILTER_ENTRIES,0 },
{ "tFsbFipSessEntry", "MAX_FSB_FIP_SESS_ENTRIES", sizeof(tFsbFipSessEntry),MAX_FSB_FIP_SESS_ENTRIES, MAX_FSB_FIP_SESS_ENTRIES,0 },
{ "tFsbIntfEntry", "MAX_FSB_INTF_ENTRIES", sizeof(tFsbIntfEntry),MAX_FSB_INTF_ENTRIES, MAX_FSB_INTF_ENTRIES,0 },
{ "tMbsmProtoMsg", "MAX_FSB_MBSM_MSG", sizeof(tMbsmProtoMsg),MAX_FSB_MBSM_MSG, MAX_FSB_MBSM_MSG,0 },
{ "tFsbNotifyParams", "MAX_FSB_NOTIFY_PARAMS_INFO", sizeof(tFsbNotifyParams),MAX_FSB_NOTIFY_PARAMS_INFO, MAX_FSB_NOTIFY_PARAMS_INFO,0 },
{ "tFsbSChannelFilterEntry", "MAX_FSB_SCHANNEL_FILTER_ENTRIES", sizeof(tFsbSChannelFilterEntry),MAX_FSB_SCHANNEL_FILTER_ENTRIES, MAX_FSB_SCHANNEL_FILTER_ENTRIES,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _FSBSZ_C  */
extern tFsModSizingParams FsFSBSizingParams [];
#endif /*  _FSBSZ_C  */


