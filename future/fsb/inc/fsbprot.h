/*************************************************************************
* Copyright (C) 2007-2015 Aricent Group . All Rights Reserved
*
* $Id: fsbprot.h,v 1.18 2017/11/07 12:23:59 siva Exp $
*
* Description: This file contains the function prototypes for FIP-snooping Module
*
*************************************************************************/
#ifndef _FSBPROTO_H
#define _FSBPROTO_H

/*fsbmain.c*/
INT4 FsbDepacketizer PROTO ((tFsbNotifyParams *pFsbNotifyParams));
VOID FsbEnQFrameToFsbTask PROTO ((tFsbNotifyParams * pMsg));
VOID FsbInterfaceHandleQueueEvent PROTO ((VOID));

INT4 FsbDispatcher PROTO ((tFSBPktInfo  *pFSBPktInfo));

/* fsbinit.c */
INT4 FsbInit PROTO ((VOID));
VOID FsbDeInit PROTO ((VOID));
VOID FsbAssignMempoolIds PROTO ((VOID));

/* fsbcli.c */
INT4 FsbCliSetSystemControl PROTO ((tCliHandle, UINT4,INT4));
INT4 FsbCliSetModuleStatus PROTO ((tCliHandle, UINT4, INT4));
INT4 FsbCliSetFcMapMode PROTO ((tCliHandle, UINT4 , INT4));
INT4 FsbCliSetFcMap PROTO ((tCliHandle, UINT4, tSNMP_OCTET_STRING_TYPE *));
INT4 FsbCliSetHouseKeepingTime PROTO ((tCliHandle, UINT4, UINT4));
INT4 FsbCliSetDebugs PROTO ((tCliHandle, UINT4, INT4, INT4));
INT4 FsbCliSetTrapStatus PROTO ((tCliHandle, UINT4, INT4));
INT4 FsbCliClearStats PROTO((tCliHandle, UINT4));
INT4 FsbCliSetUntaggedVlan PROTO ((tCliHandle, UINT4, INT4));
INT4 FsbCliSetFCoEVlan PROTO ((tCliHandle , UINT4, INT4, INT4));
INT4 FsbCliSetVlanEnabledStatus PROTO ((tCliHandle, UINT4, INT4, INT4));
INT4 FsbCliSetVlanFcMap PROTO ((tCliHandle, UINT4, INT4,
                                tSNMP_OCTET_STRING_TYPE *));
INT4 FsbCliSetFCoEVlanPinnedPorts PROTO ((tCliHandle, UINT4, INT4,
                                                 UINT1 * ));

INT4 FsbCliAddIntfEntryForFCoEVlan PROTO ((tCliHandle, UINT4, INT4, INT4));
INT4 FsbCliSetIntfPortRole PROTO ((tCliHandle, INT4, INT4, INT4));
INT4 FsbCliShowStatusInfo PROTO ((tCliHandle, UINT1 *));
INT4 FsbCliShowFIPSnoopingVlan PROTO ((tCliHandle, UINT1 *, INT4));
INT4 FsbCliShowInterface PROTO ((tCliHandle, INT4,INT4));
INT4 FsbCliShowFcf PROTO ((tCliHandle, INT4, INT4));
INT4 FsbCliShowGlobalStatistics PROTO ((tCliHandle, UINT1 *));
INT4 FsbCliShowVlanStatistics PROTO ((tCliHandle, UINT1 *, INT4));
INT4 FsbCliShowSessionStatistics PROTO ((tCliHandle, INT4, INT4));
INT4 FsbCliShowFIPSession PROTO ((tCliHandle, INT4, INT4));
INT4 FsbCliShowSessionEntries PROTO ((tCliHandle));

INT4 FsbCliShowRunningConfig PROTO((tCliHandle CliHandle));
VOID FsbCliShowRunningStatusInfoConfig PROTO((tCliHandle));
VOID FsbCliShowRunningFIPSnoopingVlanConfig PROTO((tCliHandle));
VOID FsbCliShowRunningInterfaceConfig PROTO((tCliHandle));
VOID FsbCliShowRunningFIPSnoopingModuleStatusConfig PROTO ((tCliHandle));
VOID FsbShowRunningConfigInterfaceDetails PROTO ((tCliHandle, INT4));
INT4 FsbCliSetSeverityLevel PROTO ((tCliHandle, UINT4, UINT4));

#ifdef FSB_RED_DEBUG
VOID FsbCliShowContextFilterEntry PROTO ((tCliHandle));
VOID FsbCliShowVlanFilterEntry PROTO ((tCliHandle));
VOID FsbCliShowSessionFilterEntry PROTO ((tCliHandle));
VOID FsbPrintSessionFilterEntry PROTO ((tCliHandle));
VOID FsbPrintFCoEFilterEntry PROTO ((tCliHandle));
VOID FsbCliShowSchannelFilterEntry PROTO ((tCliHandle));
VOID FsbCliShowFcfFilterEntry PROTO ((tCliHandle));
#endif
/* fsbcxt.c */
tFsbContextInfo *FsbCxtMemInit PROTO ((UINT4));
INT4 FsbCxtHandleDeleteContext PROTO ((UINT4));
INT4 FsbHandleDeletePort PROTO ((UINT4));
INT4 FsbDeleteAllTablesInContext PROTO ((UINT4));
INT4 FsbDeleteAllFipSnoopingEntry PROTO ((UINT4));
INT4 FsbDeleteAllIntfEntry PROTO ((UINT4));
INT4 FsbDeleteAllFcfEntry PROTO ((UINT4));
tFsbContextInfo *FsbCxtGetContextEntry PROTO ((UINT4));

/* fsbfcf.c */
INT4 FsbHandleFcfDiscovery PROTO ((tFSBPktInfo *));
tFsbFcfEntry *FsbGetFsbFcfEntry PROTO ((UINT2, UINT4, tMacAddr));
INT4 FsbDeleteFcfEntry PROTO ((tFsbFcfEntry *));
INT4 FsbDeleteFCFEntryForFcoeVlan PROTO ((UINT4, UINT2));
INT4 FsbDeleteFcfEntryForIfIndex PROTO ((UINT4));
INT4 FsbDeleteFcfEntryForVLANIfIndex PROTO ((tVlanId, UINT4));
BOOL1 FsbIsFcfEntryModified PROTO ((tFsbFcfEntry *pFsbFcfEntry, tFSBPktInfo *pFSBPktInfo));
tFsbFcfEntry* FsbGetFcfInfo PROTO ((UINT2, tMacAddr));

/* fsbtrap.c */
VOID FsbSnmpIfSendTrap PROTO ((UINT1, VOID *));

/* fsbutil.c */
INT4 FsbCompareFIPSnoopingEntryIndex PROTO ((tRBElem *, tRBElem *));
INT4 FsbCompareIntfEntryIndex PROTO ((tRBElem *, tRBElem *));
INT4 FsbCompareFipSessEntryIndex PROTO ((tRBElem *, tRBElem *));
INT4 FsbCompareFipSessFCoEEntryIndex PROTO ((tRBElem *, tRBElem *));
INT4 FsbCompareFcfEntryIndex PROTO ((tRBElem *, tRBElem *));
INT4 FsbCompareFipFilterEntryIndex PROTO ((tRBElem *, tRBElem *));
INT4 FsbCompareSessFilterEntryIndex PROTO ((tRBElem *, tRBElem *));
INT4 FsbMbsmCompareIntfEntryIndex PROTO ((tRBElem *, tRBElem *));
INT4 FsbCompareSChannelEntryIndex PROTO ((tRBElem * e1, tRBElem * e2));
tFsbFipSnoopingEntry *FsbGetFIPSnoopingEntry PROTO ((UINT4, UINT2));
tFsbIntfEntry *FsbGetFsbIntfEntry PROTO ((UINT2, UINT4));
VOID FsbUtilContextTrc PROTO ((UINT4, UINT4, UINT4, const char *, ...));
UINT1 FsbUtilIsFsbStarted PROTO ((UINT4));
INT4 FsbUtilPopulatePortList PROTO ((UINT2, UINT4,UINT1, tPortList ));
INT4 FsbGetVlanPortsAndPopulateIntfEntry PROTO((UINT4, UINT2));
INT4 FsbValidatePortEntryInFcoeVlan PROTO ((UINT4 *, UINT4, UINT2));
INT4 FsbDeleteFipSnoopingEntry PROTO ((UINT4, UINT2));
INT4 FsbPopulateIntfEntry PROTO ((UINT4, UINT2, UINT4));
INT4 FsbPopulateDefFilterAndIntfEntry PROTO ((UINT4, UINT2, UINT4));
INT4 FsbRemoveIntfEntryForFcoeVlan PROTO ((UINT4, UINT2));
INT4 FsbDeleteIntfEntry PROTO ((UINT2, UINT4));
INT4 FsbHandleDeleteVlanIndication PROTO ((UINT4 u4ContextId, tVlanId VlanId));
INT4 FsbHandleFIPVlanPackets PROTO ((tFSBPktInfo *));
INT4 FsbUpdateVLANStatistics PROTO ((UINT4, UINT2, UINT1));
INT4 FsbHandleAddPortToAgg PROTO ((UINT4 , UINT2 ));
INT4 FsbHandleRemovePortFromAgg PROTO ((UINT4 , UINT2));

INT4 FsbClearGlobalStats PROTO ((UINT4));
INT4 FsbClearVlanStats PROTO ((UINT4));
INT4 FsbClearSessionStats PROTO ((UINT4));
INT4 FsbHandlePortDown PROTO ((UINT4));
INT4 FsbHandleVLANPortDelete PROTO ((tVlanId, UINT4));
INT4 FsbCustCallBack PROTO ((UINT4, tFsbCallBackArgs *));
VOID FsbDisable PROTO((VOID));
VOID FsbEnable PROTO((VOID));
INT4 FsbConfigUcastEntry PROTO ((UINT4, tMacAddr, tVlanId, UINT4, UINT1));
INT4 FsbGetUntaggedVlanId PROTO ((UINT4, UINT2*));
INT4 FsbUpdateVlanPortList PROTO ((UINT4, tVlanId, tLocalPortList, tLocalPortList));
INT4 FsbUpdateVLANFilter PROTO ((UINT4, UINT2));
INT4 FsbGetFcoeVlanForIfIndex PROTO ((UINT4 u4IfIndex, UINT2* pu2FcoeVlanId));
INT4 FsbValidateFIPFrame PROTO ((tFSBPktInfo  *));
INT4 FsbHandleFIPFrame PROTO ((tFsbNotifyParams *));
INT4 FsbHandlePortRoleChange PROTO ((tFsbIntfEntry *, UINT1));
INT4 FsbUpdateVlanPorts PROTO ((UINT4 u4ContextId, tVlanId VlanId,
        UINT4 u4LocalPortId));
INT4 FsbValidatePortsInFCoEVlan PROTO  ((UINT4 u4ContextId, UINT2 u2VlanIndex));
VOID FsbDumpPacket PROTO ((UINT4 u4ContextId, UINT4 u4Mask,
                UINT1 *pu1DataBuf, UINT4 u4PktSize));
INT4 FsbGetIntfCountForFcoeVlan PROTO ((UINT2, UINT4 *));
INT4 FsbHandlePortRemoveFromVlan PROTO ((UINT4, tVlanId, UINT4));
INT4
FsbCfaCliGetIfList PROTO ((INT1 *, INT1 *, UINT1 *,
                             UINT4 ));
INT4 FsbIsMLAGPortChannel PROTO ((UINT2 ));
INT4 FsbIsMLAGPortInFCoEVlan PROTO ((UINT4 u4ContextId, UINT2 u2VlanIndex));
INT4 FsbUpdateDefaultFilter PROTO ((tFsbFipSnoopingEntry    *pFsbFipSnoopingEntry,
                                tPortList FsbPinnedPorts));
INT4 FsbGetFreeIndex PROTO ((tFsbRBFreeList FsbFreeList, UINT4 *pu4Index));
INT4 FsbResetFreeIndex PROTO ((tFsbRBFreeList FsbFreeList, UINT4 u4Index));

/* fsbport.c */
INT4 FsbPortIsSwitchExist PROTO ((UINT1 *, UINT4 *));
INT4 FsbPortGetSwitchAliasName PROTO((UINT4, UINT1 *));
INT4 FsbPortIsVcExist PROTO((UINT4));
VOID FsbPortGetDefaultVlanId PROTO ((UINT2 *));
INT4 FsbPortIsVlanActive PROTO ((UINT4, tVlanId));
INT4 FsbSetFCoEVlanType PROTO ((UINT4, tVlanId, UINT1));
INT4 FsbPortGetVlanLocalEgressPorts PROTO ((UINT4, tVlanId, tPortList));
INT4 FsbGetContextIdFromCfaIfIndex PROTO((UINT4, UINT4 *));
INT4 FsbGetIfOperStatus PROTO ((UINT4, UINT1 *));
INT4 FsbGetIfIndexFromLocalPort PROTO ((UINT4, UINT2, UINT4*));
INT4 FsbCfaGetInterfaceBrgPortType PROTO ((UINT4, INT4 *));
VOID FsbVlanApiEvbGetSbpPortsOnUap PROTO ((UINT4, tVlanEvbSbpArray *));
INT4 FsbPortIsDcbxEnabled PROTO ((VOID));
INT4 FsbPortIsDcbxOperStateUp PROTO ((UINT4 u4IfIndex));
INT4 FsbPortIsDcbxAdminStateUp PROTO ((UINT4 u4IfIndex));
INT4 FsbCfaGetIfType PROTO ((UINT4, UINT1 *));
INT4 FsbL2IwfGetConfiguredPortsForPortChannel PROTO ((UINT4 u4IfIndex, UINT2 au2ConfPorts[], UINT2 *pu2NumPorts));
UINT4 FsbRmEnqMsgToRmFromAppl PROTO ((tRmMsg *, UINT2, UINT4, UINT4));
UINT4 FsbRmGetNodeState PROTO((VOID));
UINT1 FsbRmGetStandbyNodeCount PROTO((VOID));
PUBLIC UINT1 FsbRmHandleProtocolEvent PROTO((tRmProtoEvt *));
UINT4 FsbRmRegisterProtocols PROTO((tRmRegParams *));
UINT4 FsbRmDeRegisterProtocols PROTO((VOID));
UINT4 FsbRmReleaseMemoryForMsg  PROTO((UINT1 *));
INT4  FsbRmSetBulkUpdatesStatus PROTO ((UINT4));
INT4 FsbPortSetVlanMacLearningStatus PROTO ((UINT4, tVlanId, UINT1));
INT4 FsbPortVlanConfigStaticUcastEntry PROTO ((tConfigStUcastInfo *,UINT1, UINT1 *));
INT4 FsbPortCfaGddEthWrite PROTO ((UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)); 
INT4 FsbPacketTxFrame (tFsbNotifyParams *pFsbNotifyParams, tFSBPktInfo  *pFSBPktInfo);
INT4 FsbPortVlanGetFdbEntryDetails (UINT4 u4FdbId, tMacAddr MacAddr, UINT2 *pu2Port, UINT1 *pu1Status);
INT4 FsbPortL2IwfGetPortChannelForPort PROTO ((UINT4, UINT2 *));
BOOL1 FsbPortIsVlanUntagMemberPort PROTO ((UINT4 u4ContextId, tVlanId VlanId, UINT4 u4IfIndex));
VOID FsbStripVlanTag PROTO ((tCRU_BUF_CHAIN_DESC * pFrame));
INT4 FsbVlanApiGetSChInfoFromSChIfIndex PROTO ((UINT4 u4SChIfIndex, UINT4 *pu4UapIfIndex,UINT2 *pu2SVID));
INT4 FsbVlanApiGetSChIfIndex PROTO ((UINT4 u4ContextId, UINT4 u4UapIfIndex, UINT2 u2SVID, UINT4 *pu4SChIfIndex));
INT4 FsbVlanGetVlanInfoAndUntagFrame PROTO ((tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4InPort, tVlanTag * pVlanTag));
INT4 FsbVlanAddOuterTagInFrame PROTO ((tCRU_BUF_CHAIN_DESC *pBuf, tVlanId VlanId,  UINT1 u1Priority, UINT4 u4IfIndex));
INT4 FsbL2IwfGetPotentialTxPortForAgg PROTO ((UINT4 u4ContextId, UINT2 u2VlanId,UINT2 u2AggId, UINT2 *pu2PhyPort));
INT4 FsbPortIsPortInPortChannel PROTO ((UINT4));
VOID FsbVlanTagFrame PROTO  ((tCRU_BUF_CHAIN_DESC *pBuf, tVlanId VlanId, UINT1 u1Priority));


/* fsbacl.c*/
INT4 FsFsbFormDefaultVLANFilter PROTO ((UINT4, UINT2, tFsbFilterEntry *, UINT1 ));
INT4 FsFsbFormDefaultFilter PROTO ((UINT4, UINT2, UINT2, tFsbFilterEntry *));
INT4 FsFsbFormFIPSessionFilter PROTO ((tFsbFipSessEntry *, UINT2 , tMacAddr,
                                       tFsbFilterEntry *));
INT4 FsFsbFormFLOGIAcceptFilter PROTO((tFsbFipSessFCoEEntry *,UINT2,
                                     tFsbFilterEntry *));

/* fsbwrap.c */
INT4 FsFsbHwWrCreateFilter PROTO ((tFsbFilterEntry *, UINT4 *));
INT4 FsFsbHwWrDeleteFilter PROTO ((tFsbFilterEntry *, UINT4));
INT4 FsMiNpFsbHwWrInit PROTO ((tFsbFilterEntry *, tFsbLocRemFilterId *));
INT4 FsMiNpFsbHwWrDeInit PROTO ((tFsbFilterEntry *, tFsbLocRemFilterId));
INT4 FsFsbHwWrCreateSChannelFilter PROTO ((tFsbSChannelFilterEntry *pFsbSChannelFilterEntry, UINT4 *pu4HwFilterId));
INT4 FsFsbHwWrDeleteSChannelFilter PROTO  ((tFsbSChannelFilterEntry *pFsbSChannelFilterEntry, UINT4 u4HwFilterId));
INT4 FsFsbHwWrSetFCoEParams PROTO ((UINT4 u4FsbContextId, UINT2 u2VlanIndex, UINT1 u1FIPSnoopingEnabledStatus));
INT4 FsMiNpFsbMbsmHwWrInit PROTO ((tFsbFilterEntry *, tFsbLocRemFilterId *));
INT4 FsFsbMbsmHwWrCreateFilter PROTO ((tFsbFilterEntry *, UINT4 *));
INT4 FsbRemoveSChannelFilters  PROTO ((UINT4 u4ContextId));

/* fsbsess.c */
INT4 FsbHandleFIPSession PROTO ((tFSBPktInfo *));
INT4 FsbHandleFIPSolicit PROTO ((tFSBPktInfo *, tFsbFipSessEntry *));
INT4 FsbHandleFIPAdvUcast PROTO ((tFSBPktInfo *, tFsbFipSessEntry *));
INT4 FsbHandleFLOGIRequest PROTO ((tFSBPktInfo *, tFsbFipSessEntry *));
INT4 FsbHandleFLOGIReqFromInitState PROTO ((tFSBPktInfo *, tFsbFipSessEntry *));
INT4 FsbHandleFLOGIAccept PROTO ((tFSBPktInfo *, tFsbFipSessFCoEEntry *));
INT4 FsbHandleKeepAlive PROTO ((tFsbFipSessEntry *, tFSBPktInfo *));
INT4 FsbHandleClearLink PROTO ((tFsbFipSessEntry *, tFSBPktInfo *));
INT4 FsbClearFIPSession PROTO ((tFsbFipSessEntry *));
INT4 FsbDeleteFIPSessionEntry PROTO((tFsbFipSessEntry *));
INT4 FsbDeleteFCoEEntry PROTO((tFsbFipSessFCoEEntry *));
INT4 FsbInstallFIPSessionFilter PROTO ((tFSBPktInfo *, tFsbFipSessEntry *));
INT4 FsbInstallFLOGIAcceptFilter PROTO ((tFsbFipSessFCoEEntry *));

INT4 FsbRemoveFIPSessionFilter PROTO ((tFsbFipSessEntry *));
INT4 FsbRemoveFLOGIAcceptFilter PROTO ((tFsbFipSessFCoEEntry *));
tFsbFipSessEntry *FsbGetFIPSessionInfo PROTO ((tFSBPktInfo *));
tFsbFipSessEntry *FsbGetFIPSessionEntry PROTO ((UINT2, UINT4, tMacAddr));
tFsbFipSessFCoEEntry *FsbGetFIPSessionFCoEEntry PROTO ((UINT2, UINT4, tMacAddr, tMacAddr, tMacAddr));
tFsbFipSessFCoEEntry *FsbGetFIPSessionFCoEInfo PROTO ((tFSBPktInfo *, tFsbFipSessEntry *));

INT4 FsbValidateFcMap PROTO ((tFSBPktInfo *));
INT4 FsbValidateMtuSize PROTO ((tFSBPktInfo *));
INT4 FsbDeleteFIPSessEntryForFcf PROTO ((tFsbFcfEntry *pFsbFcfEntry));
INT4 FsbDeleteAllFipSessEntry PROTO ((UINT4));
INT4 FsbDeleteFipSessionEntryForFcoeVlan PROTO (( UINT4, UINT2));
INT4 FsbDeleteSessEntryForIfIndex PROTO((UINT4 u4IfIndex));
INT4 FsbDeleteSessEntryForVLANIfIndex PROTO((tVlanId, UINT4 u4IfIndex));
INT4 FsbHandleFLOGOAccept PROTO ((tFsbFipSessEntry *, tFSBPktInfo *));


/* fsbdefacl.c */
INT4 FsbInstallDefaultVLANFilter PROTO ((UINT4, tFsbContextInfo *));
INT4 FsbRemoveDefaultVLANFilter PROTO ((tFsbContextInfo *));
INT4 FsbInstallDefaultFilter PROTO ((UINT2 , tFsbFipSnoopingEntry *));
INT4 FsbRemoveDefaultFilter PROTO ((tFsbFipSnoopingEntry *));
INT4 FsbAddDefaultFilter PROTO ((UINT4));
INT4 FsbDeleteDefaultFilter PROTO ((UINT4));
INT4 FsbInstallDefaultFCFFilter PROTO ((UINT2, tFsbFcfEntry *));
INT4 FsbDeleteDefaultFCFFilter PROTO ((tFsbFcfEntry *));

/* fsbmbsm.c */
INT4 FsbMbsmUpdateOnCardInsertion PROTO ((tMbsmPortInfo *, tMbsmSlotInfo *));
INT4 FsbMbsmUpdateOnCardRemoval PROTO ((tMbsmPortInfo *, tMbsmSlotInfo *));
INT4 FsbMbsmConfigDefaultFilter PROTO ((UINT4, UINT2, UINT4, tMbsmPortInfo *));
INT4 FsbSendAckToMbsm PROTO ((INT4));
#endif
