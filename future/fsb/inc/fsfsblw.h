/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsfsblw.h,v 1.4 2017/10/09 13:13:57 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsMIFsbContextTable. */
INT1
nmhValidateIndexInstanceFsMIFsbContextTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIFsbContextTable  */

INT1
nmhGetFirstIndexFsMIFsbContextTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsbContextTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsbSystemControl ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIFsbModuleStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIFsbFcMapMode ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIFsbFcmap ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIFsbHouseKeepingTimePeriod ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIFsbTraceOption ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIFsbTrapStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIFsbClearStats ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIFsbDefaultVlanId ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIFsbRowStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIFsbTraceSeverityLevel ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIFsbSystemControl ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIFsbModuleStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIFsbFcMapMode ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIFsbFcmap ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIFsbHouseKeepingTimePeriod ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIFsbTraceOption ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIFsbTrapStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIFsbClearStats ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIFsbDefaultVlanId ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIFsbRowStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIFsbTraceSeverityLevel ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIFsbSystemControl ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIFsbModuleStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIFsbFcMapMode ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIFsbFcmap ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIFsbHouseKeepingTimePeriod ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIFsbTraceOption ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIFsbTrapStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIFsbClearStats ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIFsbDefaultVlanId ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIFsbRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIFsbTraceSeverityLevel ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFsbContextTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIFsbFIPSnoopingTable. */
INT1
nmhValidateIndexInstanceFsMIFsbFIPSnoopingTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIFsbFIPSnoopingTable  */

INT1
nmhGetFirstIndexFsMIFsbFIPSnoopingTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsbFIPSnoopingTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsbFIPSnoopingFcmap ARG_LIST((UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIFsbFIPSnoopingEnabledStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIFsbFIPSnoopingRowStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIFsbFIPSnoopingPinnedPorts ARG_LIST((UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIFsbFIPSnoopingFcmap ARG_LIST((UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIFsbFIPSnoopingEnabledStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIFsbFIPSnoopingRowStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIFsbFIPSnoopingPinnedPorts ARG_LIST((UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIFsbFIPSnoopingFcmap ARG_LIST((UINT4 *  ,UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIFsbFIPSnoopingEnabledStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsbFIPSnoopingRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsbFIPSnoopingPinnedPorts ARG_LIST((UINT4 *  ,UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFsbFIPSnoopingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIFsbIntfTable. */
INT1
nmhValidateIndexInstanceFsMIFsbIntfTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIFsbIntfTable  */

INT1
nmhGetFirstIndexFsMIFsbIntfTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsbIntfTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsbIntfPortRole ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIFsbIntfRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIFsbIntfPortRole ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIFsbIntfRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIFsbIntfPortRole ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsbIntfRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFsbIntfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIFsbFIPSessionTable. */
INT1
nmhValidateIndexInstanceFsMIFsbFIPSessionTable ARG_LIST((INT4  , INT4  , tMacAddr  , tMacAddr  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsMIFsbFIPSessionTable  */

INT1
nmhGetFirstIndexFsMIFsbFIPSessionTable ARG_LIST((INT4 * , INT4 * , tMacAddr *  , tMacAddr *  , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsbFIPSessionTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tMacAddr , tMacAddr *  , tMacAddr , tMacAddr *  , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsbFIPSessionFcMap ARG_LIST((INT4  , INT4  , tMacAddr  , tMacAddr  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIFsbFIPSessionFcfIfIndex ARG_LIST((INT4  , INT4  , tMacAddr  , tMacAddr  , tMacAddr ,INT4 *));

INT1
nmhGetFsMIFsbFIPSessionFcfNameId ARG_LIST((INT4  , INT4  , tMacAddr  , tMacAddr  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIFsbFIPSessionFcId ARG_LIST((INT4  , INT4  , tMacAddr  , tMacAddr  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIFsbFIPSessionEnodeConnectType ARG_LIST((INT4  , INT4  , tMacAddr  , tMacAddr  , tMacAddr ,INT4 *));

INT1
nmhGetFsMIFsbFIPSessionHouseKeepingTimerStatus ARG_LIST((INT4  , INT4  , tMacAddr  , tMacAddr  , tMacAddr ,INT4 *));

/* Proto Validate Index Instance for FsMIFsbFcfTable. */
INT1
nmhValidateIndexInstanceFsMIFsbFcfTable ARG_LIST((INT4  , INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsMIFsbFcfTable  */

INT1
nmhGetFirstIndexFsMIFsbFcfTable ARG_LIST((INT4 * , INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsbFcfTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsbFcfFcMap ARG_LIST((INT4  , INT4  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIFsbFcfAddressingMode ARG_LIST((INT4  , INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsMIFsbFcfEnodeLoginCount ARG_LIST((INT4  , INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsMIFsbFcfNameId ARG_LIST((INT4  , INT4  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIFsbFcfFabricName ARG_LIST((INT4  , INT4  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIFsbFcfPinnedPorts ARG_LIST((INT4  , INT4  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsMIFsbGlobalStatsTable. */
INT1
nmhValidateIndexInstanceFsMIFsbGlobalStatsTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIFsbGlobalStatsTable  */

INT1
nmhGetFirstIndexFsMIFsbGlobalStatsTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsbGlobalStatsTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsbGlobalStatsVlanRequests ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIFsbGlobalStatsVlanNotification ARG_LIST((UINT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIFsbVlanStatsTable. */
INT1
nmhValidateIndexInstanceFsMIFsbVlanStatsTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIFsbVlanStatsTable  */

INT1
nmhGetFirstIndexFsMIFsbVlanStatsTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsbVlanStatsTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsbVlanStatsUnicastDisAdv ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIFsbVlanStatsMulticastDisAdv ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIFsbVlanStatsUnicastDisSol ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIFsbVlanStatsMulticastDisSol ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIFsbVlanStatsFLOGICount ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIFsbVlanStatsFDISCCount ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIFsbVlanStatsLOGOCount ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIFsbVlanStatsFLOGIAcceptCount ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIFsbVlanStatsFLOGIRejectCount ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIFsbVlanStatsFDISCAcceptCount ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIFsbVlanStatsFDISCRejectCount ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIFsbVlanStatsLOGOAcceptCount ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIFsbVlanStatsLOGORejectCount ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIFsbVlanStatsClearLinkCount ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIFsbVlanFcMapMisMatchCount ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIFsbVlanMTUMisMatchCount ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIFsbVlanACLFailureCount ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIFsbVlanInvalidFIPFramesCount ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIFsbVlanFCFDiscoveryTimeoutsCount ARG_LIST((UINT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIFsbSessStatsTable. */
INT1
nmhValidateIndexInstanceFsMIFsbSessStatsTable ARG_LIST((INT4  , INT4  , tMacAddr  , tMacAddr  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsMIFsbSessStatsTable  */

INT1
nmhGetFirstIndexFsMIFsbSessStatsTable ARG_LIST((INT4 * , INT4 * , tMacAddr *  , tMacAddr *  , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsbSessStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tMacAddr , tMacAddr *  , tMacAddr , tMacAddr *  , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsbSessStatsEnodeKeepAliveCount ARG_LIST((INT4  , INT4  , tMacAddr  , tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetFsMIFsbSessStatsVNPortKeepAliveCount ARG_LIST((INT4  , INT4  , tMacAddr  , tMacAddr  , tMacAddr ,UINT4 *));
