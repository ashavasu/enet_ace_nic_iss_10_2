/*************************************************************************
* Copyright (C) 2007-2015 Aricent Group . All Rights Reserved
*
* $Id: fsbglob.h,v 1.6 2017/03/03 12:52:33 siva Exp $
*
* Description: This file contains the global variables for FIP-snooping Module
*
*************************************************************************/
#ifndef _FSBGLOB_H
#define _FSBGLOB_H

#ifdef _FSBINIT_C_
tFsbGlobals         gFsbGlobals;
tFsbCallBackEntry   gFsbCallBack [FSB_MAX_CALLBACK_EVENTS];
UINT4               gu4FilterId = 0;
CONST UINT1         gau1DefaultFcMapValue[] = { 0x0E, 0xFC, 0x00 };
CONST UINT1         RsvdAllFCFMAC[]   = { 0x01, 0x10, 0x18, 0x01, 0x00, 0x02 };
CONST UINT1         RsvdAllENodeMAC[] = {0x01, 0x10, 0x18, 0x01, 0x00, 0x01};
CONST UINT1         RsvdAllZeroMAC[]  = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
#ifdef MBSM_WANTED
INT4                gi4FsbProtoId = 0;
INT4                gi4FsbMbsmSlotId = 0;
tMbsmPortInfo       gFsbMbsmPortInfo;
#endif  /* MBSM_WANTED */
#else
PUBLIC tFsbGlobals  gFsbGlobals;
PUBLIC tFsbCallBackEntry   gFsbCallBack [FSB_MAX_CALLBACK_EVENTS];
PUBLIC UINT4        gu4FilterId;
PUBLIC CONST UINT1  gau1DefaultFcMapValue[];
PUBLIC CONST UINT1  RsvdAllENodeMAC[]; 
PUBLIC CONST UINT1  RsvdAllFCFMAC[]; 
PUBLIC CONST UINT1  RsvdAllZeroMAC[]; 
#ifdef MBSM_WANTED
PUBLIC INT4             gi4FsbProtoId;
PUBLIC INT4             gi4FsbMbsmSlotId;
PUBLIC tMbsmPortInfo    gFsbMbsmPortInfo;
#endif  /* MBSM_WANTED */
#endif  /* _FSBINIT_C_ */
#endif

