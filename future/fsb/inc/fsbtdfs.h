/*************************************************************************
* Copyright (C) 2007-2015 Aricent Group . All Rights Reserved
*
* $Id: fsbtdfs.h,v 1.19 2017/11/07 12:23:59 siva Exp $
*
* Description: This file contains the structures for FIP-snooping Module.
*
*************************************************************************/
#ifndef _FSBTDFS_H
#define _FSBTDFS_H

#include "fsbtmr.h"

#define FSB_RB_FREE_LIST_SIZE ((MAX_FSB_FIP_SESS_ENTRIES + 31)/32 * 4)
typedef UINT1 tFsbRBFreeList [FSB_RB_FREE_LIST_SIZE];

typedef struct FsbFilterEntry
{
    tRBNodeEmbd     FsbFilterNode;              /* FipFilter Node */
    tPortList       PortList;                   /* Port List for Port Specific Filters */
    UINT4           u4ContextId;                /* Context Id */
    UINT4           u4FilterId;                 /* Filter Identifier */
    UINT4           u4HwFilterId;               /* Hardware filter identifer */
    UINT4           u4AggIndex;                 /* Aggregation Interface index */
    UINT4           u4IfIndex;                  /* Interface index */
    UINT4           u4PinnnedPortIfIndex;       /* Pinned Port Interface Index */
    tMacAddr        DstMac;                     /* Destination MAC address */
    tMacAddr        SrcMac;                     /* Source MAC address*/
    UINT2           u2VlanId;                   /* FCoE VLAN identifier*/
    UINT2           u2ClassId;                  /* Class identifier*/
    UINT2           u2Ethertype;                /* Ether type */
    UINT2           u2OpcodeFilterOffsetValue;  /* Filter offset value*/
    UINT2           u2OpcodeFilterOffsetMask;   /* Filter offset mask*/
    UINT2           u2SubOpcodeFilterOffsetValue;   /* Filter offset value*/
    UINT2           u2SubOpcodeFilterOffsetMask;   /* Filter offset mask/ */
    UINT1           u1FilterAction;             /* Filter Action */
    UINT1           u1FilterType;               /* Specifies the filter type (global/port-based) */
    UINT1           au1FcMap[FSB_FCMAP_LEN];    /* FC-MAP Value */
    UINT1           u1Priority;                 /* Specifies the filter priority */
    UINT1           au1NameId[FSB_NAME_ID_LEN]; /* Name ID */
    UINT1           u1DefFilterType;            /* Specifies the Default filter type */
    UINT1           au1Pad1[3];                 /* Padding byte */
}tFsbFilterEntry;

typedef struct FsbDefaultVLANFilterEntry
{
    tFsbFilterEntry   FsbVlanDiscovery;
    tFsbFilterEntry   FsbVlanResponseTagged;
}tFsbDefaultVLANFilterEntry;

/* Context Info Table - Contains context specific attributes 
 * This data structure corresponds to fsMIFsbContextTable and 
 * fsMIFsbGlobalStatsTable MIB Table */
typedef struct FsbContextInfo
{
    tFsbDefaultVLANFilterEntry    FsbLocFilterEntry;          /* Local Filter Entry */
    tFsbDefaultVLANFilterEntry    FsbRemFilterEntry;          /* Remote Filter Entry */
    UINT1                         au1FcMap[FSB_FCMAP_LEN];    /* FC-MAP Value */
    UINT1                         u1Pad;                      /* Padding byte */
    UINT4                         u4VlanRequestCount;         /* Vlan Request Count */
    UINT4                         u4VlanNotificationCount;    /* Vlan Notification Count*/
    UINT4                         u4TraceOption;              /* Trace Variable */
    UINT4                         u4TraceSeverityLevel;        /*Severity level for traces*/
    UINT2                         u2HouseKeepingTimePeriod;   /* HouseKeeping Period */
    UINT2                         u2DefaultVlanId;            /* Default VLAN Index */
    UINT1                         u1SystemControl;            /* Shutdown Status */
    UINT1                         u1ModuleStatus;             /* Module Status */
    UINT1                         u1FcMapMode;                /* FC-MAP Mode */
    UINT1                         u1TrapStatus;               /* Trap Status */
    UINT1                         u1RowStatus;                /* SNMP Row Status */
    BOOL1                         bClearStats;                /* Clear Stats*/
    UINT1                         au1pad[2];
} tFsbContextInfo;


/* FIP Snooping Table - Contains details of VLANs for which FIP snooping
 * is individualy enabled
 * This data sructure corresponds to fsMIFsbFIPSnoopingTable and 
 * fsMIFsbVlanStatsTable MIB Table */
typedef struct  FsbFipSnoopingEntry
{
    tRBNodeEmbd           FsbFipSnoopingNode;         /* FipSnoopingNode */
    tRBTree               FsbFilterEntry;             /* FsbFipFilterEntry */ 
    tPortList             FsbPinnedPorts;             /* Pinned Port List Configuration*/ 
    UINT1                 au1FcMap[FSB_FCMAP_LEN];    /* FC-MAP Value */
    UINT1                 u1Pad;                      /* Padding Byte */
    UINT4                 u4ContextId;                /* RBTree INDEX - Context ID */
    UINT4                 u4PinnnedPortIfIndex;       /* Pinned Port Interface Index */
    UINT4                 u4UnicastDisAdvCount;       /* Unicast Discovery Advt Count */
    UINT4                 u4MulticastDisAdvCount;     /* Multicast Discovery Advt Count */
    UINT4                 u4UnicastDisSolCount;       /* Unicast Discovery Sol Count */
    UINT4                 u4MulticastDisSolCount;     /* Multicast Discovery Sol Count */
    UINT4                 u4FLOGICount;               /* FLOGI Count */
    UINT4                 u4FDISCCount;               /* FDISC Count */
    UINT4                 u4LOGOCount;                /* LOGO Count */
    UINT4                 u4FLOGIAcceptCount;         /* FLOGI Accept Count */
    UINT4                 u4FLOGIRejectCount;         /* FLOGI Reject Count */
    UINT4                 u4FDISCAcceptCount;         /* FDISC Accept Count */
    UINT4                 u4FDISCRejectCount;         /* FDISC Reject Count */
    UINT4                 u4LOGOAcceptCount;          /* LOGO Accept Count */
    UINT4                 u4LOGORejectCount;          /* LOGO Reject Count */
    UINT4                 u4ClearLinkCount;           /* CLEAR Link Count */
    UINT4                 u4PinnedPortCount;          /* Pinned Port Count*/
    UINT4                 u4FcMapMisMatchCount;       /* FC-MAP Mismatch Count */
    UINT4                 u4MTUMisMatchCount;         /* MTU Mismatch Count */
    UINT4                 u4ACLFailureCount;          /* ACL Failure Count */
    UINT4                 u4InvalidFIPFramesCount;    /* Invalid FIP Frames Count */
    UINT4                 u4FCFDiscoveryTimeoutsCount;/*FCF Discovery time out count*/
    UINT2                 u2VlanId;                   /* RBTree INDEX - VLAN Index */
    UINT1                 u1EnabledStatus;            /* Value to indicate Enabled/Disabled Status */
    UINT1                 u1RowStatus;                /* SNMP Row Status */
} tFsbFipSnoopingEntry;

/* FSB Interface Table - Contains details of Port Roles of the interface
 * in FIP Snooping Enabled FCoE VLAN.
 * This data structure corresponds to fsMIFsbIntfTable MIB Table */
typedef struct  FsbIntfEntry
{
    tRBNodeEmbd     FsbIntfEntryNode;           /* FSBIntfEntryNode */
    UINT4           u4ContextId;                /* Context ID */
    UINT4           u4IfIndex;                  /* RBTree INDEX - Interface Index */
    UINT2           u2VlanId;                   /* RBTree INDEX - VLAN Index */
    UINT1           u1PortRole;                 /* Interface Port Role */
    UINT1           u1RowStatus;                /* SNMP Row Status */
} tFsbIntfEntry;

/* FSB Session Table - Contains details of active session between
 * ENode and FCF
 * This data structure corresponds to fsMIFsbFIPSessionTable and 
 * fsMIFsbSessStatsTable MIB Table */
typedef struct  FsbFipSessEntry
{
    tRBNodeEmbd     FipSessEntryNode;           /* FipSessEntryNode */
    tRBTree         FsbSessFilterEntry;         /* FsbSessFilterEntry */
    tFsbTimer       FsbHouseKeepingTmr;         /* HouseKeeping Timer */
    tFsbTimer       FsbSessEstablishmentTmr;    /* Session Timer */
    UINT1           au1FcMap[FSB_FCMAP_LEN];    /* FC-MAP Value */
    UINT1           u1Pad1;                     /* Padding Byte */
    UINT1           au1NameId[FSB_NAME_ID_LEN]; /* Name ID */
    UINT4           u4ContextId;                /* Context ID */
    UINT4           u4ENodeIfIndex;             /* RBTree INDEX - Enode IfIndex */
    UINT4           u4FcfIfIndex;               /* FCF IfIndex */
    UINT4           u4KeepAliveCount;           /* Number of ENode Keep Alive Frames
                                                   sent from ENode to an FCF for this session*/
    UINT4           u4FCoECount;                /* No of FCoE Entries per session */
    UINT4           u4Index;                    /* Index of this FIP Session Entry  */
    tMacAddr        ENodeMacAddr;               /* RBTree INDEX - Enode MAC Address */
    UINT2           u2VlanId;                   /* RBTree INDEX - VLAN Index */
    UINT2           u2HouseKeepingTimePeriod;   /* HouseKeeping Period Based on FKAV present in FcFEntry*/
    UINT1           u1State;                    /* State of the FIP Session */
    UINT1           u1HouseKeepingTimerStatus;  /* House keeping timer status */
    UINT1           u1HouseKeepingTimerFlag;    /* House keeping time Flag */
    UINT1           u1DeleteReasonCode;
    UINT1           u1IssuMaintenanceMode;      /* ISSU-maintenance mode value*/
    UINT1           au1Pad[1];                  /* Padding byte */
} tFsbFipSessEntry;

/* FIP Session FCoE Table - Contains details of active session between
 * VM(ENode) and FCF */
typedef struct  FsbFipSessFCoEEntry
{
    tRBNodeEmbd         FipSessEntryFCoENode;
    tRBTree             FsbSessFCoEFilterEntry;     /* FsbSessFCoEFilterEntry for Filters. */
    tFsbTimer           FsbHouseKeepingTmr;         /* HouseKeeping Timer – Based on VN_PORT Keep Alive */
    tFsbFipSessEntry    *pFsbFipSessEntry;          /* Back Pointer to corresponding
                                                       tFsbFipSessEntry element – Required SNMP/ CLI */
    UINT1               au1FcfId[FSB_FCID_LEN];     /* FC-ID */
    UINT1               u1Pad;
    UINT4               u4ENodeIfIndex;             /* RBTree INDEX - Enode IfIndex */
    UINT4               u4VNKeepAliveCount;         /* Number of VN Port Keep Alive Frames sent from
                                                       ENode to an FCF for this session*/
    UINT4               u4Index;                    /* Index of this FCoE Session Entry  */
    tMacAddr            ENodeMacAddr;               /* RBTree INDEX - Enode MAC Address */
    tMacAddr            FcfMacAddr;                 /* RBTree INDEX -FCF MAC Address */
    tMacAddr            FCoEMacAddr;                /* RBTree INDEX - FPMA/SPMA Address */
    UINT2               u2VlanId;                   /* RBTree INDEX - VLAN Index */
    UINT1               u1EnodeConnType;            /* Enode Connection type */
    UINT1               u1HouseKeepingTimerStatus;  /* House keeping timer status */
    UINT1               u1HouseKeepingTimerFlag;    /* House keeping time Flag */
    UINT1               u1SyncTimerStatus;          /* Sync Timer Status */
    UINT2               u2HouseKeepingTimePeriod;   /* HouseKeeping Period Based on ContextInfo*/
    UINT1               u1DeleteReasonCode;
    UINT1               u1IssuMaintenanceMode;      /* ISSU-maintenance mode value*/
} tFsbFipSessFCoEEntry;

/* FCF Table - Contains details of FCFs which are currently active
 * This data structure corresponds to fsMIFsbFcfTable MIB Table */
typedef struct  FsbFcfEntry
{
    tRBNodeEmbd     FsbFcfEntryNode;            /* FcfEntryNode */
    tFsbTimer       FCFAliveTimer;              /* FCF Alive Timer */
    UINT1           au1FcMap[FSB_FCMAP_LEN];    /* FC-MAP Value */
    UINT1           u1Pad1;                     /* Padding Byte */
    UINT1           au1NameId[FSB_NAME_ID_LEN]; /* Name ID */
    UINT1           au1FabricName[FSB_FABRIC_NAME_LEN];/* Fabric Name */
    UINT4           u4ContextId;                /* Context ID */
    UINT4           u4FcfKeepAliveTimerValue;   /* FCF keep alive timer value */
    UINT4           u4FcfKeepAliveFieldValue;   /* FCF keep alive field value */
    UINT4           u4FcfIfIndex;               /* RBTree INDEX - FCF IfIndex */
    UINT4           u4EnodeLoginCount;          /* Addressing Mode */
    tMacAddr        FcfMacAddr;                 /* RBTree INDEX - FCF MAC Address */
    UINT2           u2VlanId;                   /* RBTree INDEX - VLAN Index */
    UINT2           u2DbitFlag;                  /* Dbit Flag */
    UINT1           u1AddressingMode;           /* Addressing Mode */
    UINT1           u1IssuMaintenanceMode;      /* ISSU-maintenance mode value*/
} tFsbFcfEntry;

typedef struct FsbSChannelFilterEntry
{
    tRBNodeEmbd     FsbSChannelFilterNode;      /* FsbSChannelFilterNode */
    UINT4           u4IfIndex;                  /* Interface index - RBTree index*/
    UINT4           u4FilterId;                 /* Filter Identifier */
    UINT4           u4HwFilterId;               /* Hardware filter identifer */
    UINT4           u4ContextId;                /* Context Identifier */
    UINT2           u2VlanId;                   /* S-Channel VLAN identifier - RBTree index*/
    UINT1           u1Priority;                 /* Specifies the filter priority */
    UINT1           au1Pad[1];                  /* Padding byte */
}tFsbSChannelFilterEntry;

typedef struct FSBREDGLOBALS
{
    tFsbFipSnoopingEntry       *pNextFipSnoopingEntry;
    tFsbFipSessFCoEEntry       *pNextFipSessFCoEEntry;
    tFsbFipSessEntry           *pNextFipSessEntry;
    tFsbFcfEntry               *pNextFcfEntry;
    tFsbSChannelFilterEntry    *pNextSChannelFilterEntry;
    UINT4                      u4NextContextId;
    UINT1                      u1NodeStatus;
                                       /* Node Status - Idle/Active/Standby.
                                        * Idle    - Node Status on boot-up.
                                        * Active  - Node where hardware programming
                                        *           is done.
                                        * Standby - Node which is in sync with active
                                        *           of all the information. it can
                                        *           take Active role, in case of
                                        *           Active node fails.*/
    UINT1                      u1NumPeersUp;
                                       /* Indicates number of standby nodes that are up. */
    BOOL1                      bBulkReqRcvd; 
                                       /* Bulk Request Received Flag
                                        * - OSIX_TRUE/OSIX_FALSE.
                                        * This flag is set when the dynamic bulk
                                        * request message reaches the active node
                                        * before the STANDBY_UP event. Bulk updates
                                        * are sent only after STANDBY_UP event is
                                        * reached.*/
    BOOL1                       bFcfBulkUpdStatus;
                                       /* This flag will be set to OSIX_TRUE
                                        * after all FCF node has been
                                        * synced with the Standby.*/
    BOOL1                       bDefVlanFilIdBulkUpdStatus;
                                       /* This flag will be set to OSIX_TRUE
                                        * after all Default Vlan Filter Id has been
                                        * synced with the Standby.*/
    BOOL1                       bDefFilIdBulkUpdStatus;
                                       /* This flag will be set to OSIX_TRUE
                                        * after all Default Filter Id has been
                                        * synced with the Standby.*/
    BOOL1                       bSessBulkUpdStatus;
                                       /* This flag will be set to OSIX_TRUE
                                        * after all FIP Session Entries has been
                                        * synced with the Standby.*/
    BOOL1                       bFCoEBulkUpdStatus;
                                       /* This flag will be set to OSIX_TRUE
                                        * after all FCoE Entries has been
                                        * synced with the Standby.*/
    UINT1                      bSChanFilIdBulkUpdStatus;
                                       /* This flag will be set to OSIX_TRUE
                                        * after all SChannel Filter Id has been
                                        * synced with the Standby.*/
    UINT1                      u1BulkUpdStatus;
                                       /* This will be set to OSIX_TRUE
                                        * when bulk update request handling 
                                        * has been completed.*/
    UINT1                      au1Pad[2];
}tFsbRedGlobalInfo;

/* Global Declaration tFSBGlobals - Contains globals used for various handlers
 * and variables*/

typedef struct FSBGLOBALS {
    tFsbContextInfo     *apFsbContextInfo[SYS_DEF_MAX_NUM_CONTEXTS]; /* Global Context array */
    tMemPoolId          FsbContextPoolId;               /* Pool Id for Context Table */
    tMemPoolId          FsbFipSnoopingNodePoolId;       /* Pool Id for FIP Snooping Table */
    tMemPoolId          FsbIntfEntryNodePoolId;         /* Pool Id for FSB Interface Table */
    tMemPoolId          FsbFipSessEntryNodePoolId;      /* Pool Id for FIP Session Table */
    tMemPoolId          FsbFipSessFCoEEntryNodePoolId;  /* Pool Id for FIP Session FCoE Table */
    tMemPoolId          FsbFcfEntryNodePoolId;          /* Pool Id for FCF Table */
    tMemPoolId          FsbNotifyParamsPoolId;          /* Pool Id for FsbNotifyParams */
    tMemPoolId          FsbMbsmMsgPoolId;               /* Pool Id for MBSM Msg */
    tMemPoolId          FsbFilterEntryNodePoolId;       /* Pool Id for Filter entry table */
    tMemPoolId          FsbFCoEMacAddrPoolId;           /* Pool Id for FCoE Mac Address */
    tMemPoolId          FsbSChannelMemPoolId;           /* Pool Id for FSB s-channel id */
    tRBTree             FsbFipSnoopingTable;            /* FIP-snooping List */
    tRBTree             FsbIntfTable;                   /* FSB Interface List */
    tRBTree             FsbFipSessTable;                /* FIP Session List */
    tRBTree             FsbFipSessFCoETable;            /* FIP Session FCoE List */ 
    tRBTree             FsbFcfTable;                    /* FCF List */
    tRBTree             FsbSChannelFilterTable;         /* FIP S-Channle Filter list */
    tOsixTaskId         gFsbTaskId;                     /* Task ID for FSB Module*/
    tOsixSemId          gFsbSemId;                      /* Semaphore for FSB Module */
    tOsixQId            gFsbTaskQId;                    /* Queue ID for FSB Module */
    UINT4               u4SyslogId;                     /* Syslog ID */
    tTimerListId        FsbTimerId;                     /* FSB timer id*/
    tFsbTimer           FsbMbsmTmr;                     /* Mbsm Timer */
    UINT4               u4FilterCount;                  /* Filter Count */
    tFsbRedGlobalInfo   FsbRedGlobalInfo;
    tFsbRBFreeList      SessFilterEntryList;
    tFsbRBFreeList      FcoeFilterEntryList;
    UINT4               u4FsbFilterEntryRBCount;
    UINT4               u4FsbSessFilterEntryRBCount;
    UINT4               u4FsbFCoEFilterEntryRBCount;
    UINT4               gu4FsbFIPSessionCount;          /* FIP Session Count */
}tFsbGlobals;

/* Data structures used for Messaging/Interface with RM  */
#ifdef L2RED_WANTED
typedef struct FsbRmMsg {
    tRmMsg           *pData;     /* RM message pointer */
    UINT2             u2DataLen; /* Length of RM message */
    UINT1             u1Event;   /* RM event */
    UINT1             au1Pad[1];
} tFsbRmMsg;
#endif /* L2RED_WANTED */

typedef struct FsbNotifyParams
{
#ifdef MBSM_WANTED
    tMbsmProtoMsg   *pMbsmProtoMsg;             /* MBSM Structure */
#endif
#ifdef L2RED_WANTED
    tFsbRmMsg       RmMsg;
#endif
    tLocalPortList  AddedPorts;                 /* Added PortList */
    tLocalPortList  DeletedPorts;               /* Deleted PortList */
    UINT1           au1DataBuf[FSB_MAX_FIP_PKT_LEN];/* DataBuf */
    UINT4           u4ContextId;                /* Context ID */
    UINT4           u4IfIndex;                  /* Interface index */
    UINT4           u4MsgType;                  /* Message Type */
    UINT4           u4PacketLen;                /* Packet Length */
    UINT4           u4AggIndex;                 /* Aggregate index */
    UINT2           u2VlanId;                   /* VLAN Index */
    UINT1           u1OperStatus;               /* Operational status of port*/
    UINT1           u1Pad;                      /* Padding Byte */
} tFsbNotifyParams;

/* Message types received by FSB module */
typedef enum
{
    FSB_PACKET_RECV_MSG = 1,
    FSB_OPER_STATUS_CHG_MSG,
    FSB_PORT_UNMAP_MSG,
    FSB_PORT_DISCARDING_MSG,
    FSB_CONTEXT_DELETE_MSG,
    FSB_VLAN_DELETE_MSG,
    FSB_ADD_PORT_TO_AGG,
    FSB_REMOVE_PORT_FROM_AGG,
    FSB_MBSM_CARD_INSERT_MSG,
    FSB_MBSM_CARD_REMOVE_MSG,
    FSB_MBSM_STAGERRING_MSG,
 FSB_RM_MSG,
    FSB_PORTLIST_UPDATE_MSG,
    FSB_PORT_UPDATE_MSG,
    FSB_PORT_DELETE_NOTIFY_MSG
}eFsbMsgType;
/* tFsbTrapInfo - Contain information which is used to
 * notify SNMP agent about trap */
typedef struct FsbTrapInfo
{
    UINT4           u4ContextId;                /* Context Identifier */
    UINT4           u4FsbSessionEnodeIfIndex;   /* Enode IfIndex */
    tMacAddr        FsbSessionEnodeMacAddress;  /* Enode MAC Address */
    UINT2           u2FsbSessionVlanId;         /* VLAN Index */
}tFsbTrapInfo;

/* Enum for all type for FIP Packet */
typedef enum {
    FSB_FIP_INITIAL_STATE = 0,
    FSB_FIP_VLAN_DISCOVERY,
    FSB_FIP_VLAN_NOTIFICATION,
    FSB_FIP_ADV_MCAST,
    FSB_FIP_ADV_UCAST,
    FSB_FIP_SOLICIT_MCAST,
    FSB_FIP_SOLICIT_UCAST,
    FSB_FIP_FLOGI_REQUEST,
    FSB_FIP_FLOGI_ACCEPT,
    FSB_FIP_FLOGI_REJECT,
    FSB_FIP_NPIV_FDISC_REQUEST,
    FSB_FIP_NPIV_FDISC_ACCEPT,
    FSB_FIP_NPIV_FDISC_REJECT,
    FSB_FIP_LOGO_REQUEST,
    FSB_FIP_LOGO_ACCEPT,
    FSB_FIP_LOGO_REJECT,
    FSB_FIP_ENODE_KEEP_ALIVE,
    FSB_FIP_VNPORT_KEEP_ALIVE,
    FSB_FIP_CLEAR_LINK
/* fsbmacro.h contain macro for handling failures scenarios,
 *  value of those macros should be FSB_FIP_CLEAR_LINK +1,+2 and so on.
 *  If any enum variable is appended here, those macro values also 
 *  should be updated properly */
} ePacketType;

#ifdef _FSBMAIN_C_
CONST CHR1  *FsbErrString [] ={
    "FSB_FIP_INITIAL_STATE",
    "FSB_FIP_VLAN_DISCOVERY",
    "FSB_FIP_VLAN_NOTIFICATION",
    "FSB_FIP_ADV_MCAST",
    "FSB_FIP_ADV_UCAST",
    "FSB_FIP_SOLICIT_MCAST",
    "FSB_FIP_SOLICIT_UCAST",
    "FSB_FIP_FLOGI_REQUEST",
    "FSB_FIP_FLOGI_ACCEPT",
    "FSB_FIP_FLOGI_REJECT",
    "FSB_FIP_NPIV_FDISC_REQUEST",
    "FSB_FIP_NPIV_FDISC_ACCEPT",
    "FSB_FIP_NPIV_FDISC_REJECT",
    "FSB_FIP_LOGO_REQUEST",
    "FSB_FIP_LOGO_ACCEPT",
    "FSB_FIP_LOGO_REJECT",
    "FSB_FIP_ENODE_KEEP_ALIVE",
    "FSB_FIP_VNPORT_KEEP_ALIVE",
    "FSB_FIP_CLEAR_LINK"
};
#else
PUBLIC CHR1 *FsbErrString[];
#endif
typedef struct FcfDiscoveryAdvertisementInfo
{
    UINT4           u4FkaAdvPeriod;             /* Advertisment Period */
    UINT1           au1NameId[FSB_NAME_ID_LEN]; /* Name ID */
    UINT1           au1FabricName[FSB_FABRIC_NAME_LEN];/*Fabric descriptor */
    UINT1           au1FcMap[FSB_FCMAP_LEN];    /* FC-Map */
    UINT1           u1Priority;                 /* Priority */
    UINT2           u2DbitFlag;                 /* D-bit flag */
    UINT1           au1Pad[2];                  /* Padding byte */
}tFcfDiscoveryAdvertisementInfo;

typedef struct FCFSolicitationUnicastInfo
{
    UINT2           u2MaxFrameSize;             /*Maximum Frame size*/
    UINT1           au1pad[2];                  /* Padding Byte */
} tFCFSolicitationUnicastInfo;

typedef struct FIPLogiInfo
{
    tMacAddr        FCoEMacAddr;                /* FCoE MAC Address*/
    UINT1           au1pad[2];                  /* Padding Byte */
}tFIPLogiInfo;

typedef struct FIPLogoInfo
{
    tMacAddr        FCoEMacAddr;                /* FCoE MAC Address*/
    UINT1           au1pad[2];                  /* Padding Byte */
}tFIPLogoInfo;

typedef struct FIPClearLink 
{
    tMacAddr        *pVNMacAddr;                /* VN_Port MAC Address list */
}tFIPClearLink;

typedef struct FIPVNKeepAlive 
{
    tMacAddr        VNMacAddr;                /* VN_Port MAC Address */
    tMacAddr        ENodeMacAddr;             /* ENode MAC Address */
}tFIPVNKeepAlive;
typedef union
{
    tFcfDiscoveryAdvertisementInfo FcfDiscoveryAdvertisementInfo;
    tFCFSolicitationUnicastInfo    FCFSolicitationUnicastInfo;
    tFIPLogiInfo                   FIPLogiInfo;
    tFIPLogoInfo                   FIPLogoInfo;
    tFIPClearLink                  FIPClearLink;
    tFIPVNKeepAlive                FIPVNKeepAlive;
} unFsbFipsPktFields;


typedef struct FSBPktInfo
{
    unFsbFipsPktFields  FsbFipsPktFields;       /* Packet Field */
    tMacAddr            DestAddr;               /* Destination MAC address */
    tMacAddr            SrcAddr;                /* Source MAC address */
    UINT4               u4ContextId;            /* Context Identifier */
    UINT4               u4IfIndex;              /* Interface Index */
    ePacketType         PacketType;             /* Packet Type */
    UINT2               u2EtherType;            /* Packet Ether Type */
    UINT2               u2Opcode;               /* Opcode */
    UINT2               u2SubOpcode;            /* SubOpcode */
    UINT2               u2Flag;                 /* Flag */
    UINT2               u2VlanId;               /* VLAN Index */
    UINT1               u1AddressMode;          /* Addressing Mode */
    UINT1               u1Pad;                  /* Padding Byte */
}tFSBPktInfo;

/*  CallBack Structure to set MTU for the Fcoe Vlan interfaces */

typedef union FsbCallBackEntry {
    INT4 (*pFsbSetIfMtuCallBack) (UINT4);
} tFsbCallBackEntry;

typedef struct FsbCallBackArgs {
    UINT4 u4IfIndex;
}tFsbCallBackArgs;

/* Enum for FIP session deletion reason code */
typedef enum {
    FSB_REASON_CODE_CONTEXT_DELETE = 1,
    FSB_REASON_CODE_VLAN_DELETE,
    FSB_REASON_CODE_PORT_DOWN,
    FSB_REASON_CODE_FCF_DELETE,
    FSB_REASON_CODE_FLOGI_REJECT,
    FSB_REASON_CODE_FLOGO_ACCEPT,
    FSB_REASON_CODE_ENODE_CVL,
    FSB_REASON_CODE_VNPORT_CVL,
    FSB_REASON_CODE_ESTABLISHMENT_TMR_EXPIRY,
    FSB_REASON_CODE_ENODE_TMR_EXPIRY,
    FSB_REASON_CODE_VNMAC_TMR_EXPIRY,
    FSB_REASON_CODE_SESS_REESTABLISHMENT_FROM_SERVER,
    FSB_REASON_CODE_FILTER_INSTALLATION_FAILURE,
    FSB_REASON_CODE_MAX
} eReasonCode;

#ifdef _FSBMAIN_C_
CONST CHR1  *FsbDeleteReasonString [] ={
    NULL,
    "Shutdown/Module Status disable",
    "FCoE VLAN delete",
    "Port down/Port Unmap",
    "FCF delete",
    "FLOGI Reject received",
    "FLOGO Accept received",
    "ENode CVL received",
    "VNPort CVL received",
    "Establishment timer expiry",
    "ENode house-keeping timer expiry",
    "VNMac house-keeping timer expiry",
    "Server re-initiating session",
    "Filter Installation failure",
    "\r\n"
};
#else
PUBLIC CHR1 *FsbDeleteReasonString[];
#endif





#endif

