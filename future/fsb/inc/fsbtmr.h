/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsbtmr.h,v 1.4 2016/01/14 11:52:37 siva Exp $
*
* Description: FSB File.
*********************************************************************/

#ifndef _FSBTMR_H
#define _FSBTMR_H

typedef enum { 
    FSB_MIN_TIMER_ID                 =   0,
    FSB_SESS_ESTABLISHMENT_TIMER_ID  =   1,
    FSB_ENODE_HOUSE_KEEPING_TIMER_ID =   2,
    FSB_VNMAC_HOUSE_KEEPING_TIMER_ID =   3,
    FSB_FCF_KEEP_ALIVE_TIMER_ID      =   4,
    FSB_MBSM_TIMER_ID                =   5,
    FSB_MAX_TIMER_ID                 =   6
} eFsbTimerId;

typedef struct  FSBTIMER {
     tTmrAppTimer Node;
     VOID *pContext;              /* Refers to the context to which 
                                       timer is attached */
     UINT1  u1TimerId;              /* Identification of Timer */
     UINT1 u1Status;                /* Validity of the structure 
                                       stored */
     UINT2 u2Pad;                   /* Padding for 4 Byte allignment */
}tFsbTimer;


#define FSB_TIMER_ACTIVE     1
#define FSB_TIMER_INACTIVE   0
#define FSB_INTERIM_UPDATE_TIMOUT_VAL 120

#define FSB_INIT_TIMER(pUserTimer)      \
    ((pUserTimer)->u1Status = FSB_TIMER_INACTIVE)
#define FSB_ACTIVATE_TIMER(pUserTimer)      \
    ((pUserTimer)->u1Status = FSB_TIMER_ACTIVE)
#define FSB_IS_TIMER_ACTIVE(pUserTimer) \
    (((pUserTimer)->u1Status == FSB_TIMER_INACTIVE)?FALSE:TRUE)




UINT4 FsbTmrInit PROTO ((VOID));
UINT4 FsbTmrStartTimer  PROTO ((tFsbTimer *pFsbTmr,
                                eFsbTimerId UserTmrId,
                                UINT4 u4TimeOut,
                                VOID *pTmrContext));

VOID FsbTmrProcessTimer PROTO ((VOID));

UINT4 FsbTmrStopTimer                 PROTO ((tFsbTimer *pFsbTmr));
VOID  FsbProcSessEstablishmentTmr     PROTO ((VOID *pTmrContext));
VOID  FsbProcENodeHouseKeepingTmr     PROTO ((VOID *pTmrContext));
VOID  FsbProcVNMacHouseKeepingTmr     PROTO ((VOID *pTmrContext));
VOID  FsbProcFcfKeepAliveTmr          PROTO ((VOID *pTmrContext));
VOID  FsbProcMbsmTmr                  PROTO ((VOID *pTmrContext));
#endif /* _FSBTMR_H */
