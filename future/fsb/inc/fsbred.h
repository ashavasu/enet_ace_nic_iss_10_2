/*************************************************************************
* Copyright (C) 2007-2012 Aricent Group . All Rights Reserved
*
* $Id: fsbred.h,v 1.4 2017/01/17 14:10:27 siva Exp $
*
* Description: This file contains the macros used in FIP-snooping Module
*
*************************************************************************/
#ifndef _FSBRED_H
#define _FSBRED_H

#define FSB_NODE_IDLE           RM_INIT
#define FSB_NODE_ACTIVE         RM_ACTIVE
#define FSB_NODE_STANDBY        RM_STANDBY

#define FSB_RM_OFFSET                        0
#define FSB_SYNC_MSG_TYPE_SIZE               4
#define FSB_RED_LENGTH_SIZE                  2
#define FSB_TWO_BYTE                         2                      
#define FSB_FOUR_BYTE                        4                       
#define FSB_SIX_BYTE                         6

/* Bulk Update Tail message length */
#define FSB_BULK_MSG_TYPE_SIZE               4
#define FSB_BULK_MSG_LEN_SIZE                2
#define FSB_BULK_UPD_TAIL_SIZE               (FSB_BULK_MSG_TYPE_SIZE+\
                                             FSB_BULK_MSG_LEN_SIZE)

/* Bulk Update messages are split in to 1500 bytes messages and sent. The
 * following macro is for this size. */
#define FSB_BULK_SPIT_MSG_SIZE        1500

/* Default VLAN - Hw Filter Id */
#define FSB_SYN_DEF_VLAN_MSG_SIZE            22
/* Default Filter - Hw filter Id */
/* Sync Message for Default filter - Context Id   = 4
 *                                   Vlan Id      = 4
 *                                   MAC Address  = 6
 *                                   Hw Filter Id = 4 */
/* Filter installed per VLAN     - 3 * (MAC Address + Hw Filter Id)  = 30 */
/* Maximum Number of FCF         - 8 * (MAC Address + Hw Filter Id)  = 80 
 * Index for FIP Snooping Entry  - (Context Id + Vlan Id)            =  8
 * Maximum Sync message Length                                       = 118 */
#define FSB_SYN_MAX_DEF_FILTER_MSG_SIZE      118
#define FSB_SYN_SCHAN_FILTER_MSG_SIZE        100 /* Need to change this macro based on No of filters */
/* Fcf Entry */
#define FSB_SYN_FCF_ADD_MSG_SIZE             41
#define FSB_SYN_FCF_FILTER_UPT_MSG_SIZE      16
#define FSB_SYN_FCF_DEL_MSG_SIZE             12       
/* FIP Session Entry */
#define FSB_SYNC_FIP_SOLICIT_MSG_SIZE        41
#define FSB_SYNC_FIP_UCAST_ADVT_MSG_SIZE     58
#define FSB_SYNC_FIP_INST_MSG_SIZE           31 
#define FSB_SYNC_FIP_FLOGI_ACCEPT_MSG_SIZE   153
#define FSB_SYNC_FIP_FDISC_REJECT_MSG_SIZE    9 
#define FSB_SYNC_FIP_LOGO_REQUEST_MSG_SIZE    13 
#define FSB_SYNC_FIP_LOGO_REJECT_MSG_SIZE     9
#define FSB_SYNC_FIP_CLEAR_SESSION_MSG_SIZE   9 
#define FSB_SYNC_MAX_VNMAC_MSG_SIZE          1530
#define FSB_SYNC_FIP_SESS_DEL_MSG_SIZE        12 
#define FSB_SYN_FCOE_DEL_MSG_SIZE             24
/* Oper Status */
#define FSB_SYN_OPER_STATUS_MSG_SIZE            5
/* Bulk Update Count */
#define FSB_RED_FCF_CNT_PER_BULK_UPDT         40 /* TLV size for 1 FCF = 37
                                                  * Approx. 40FCF can be
                                                  * synced-up per bulk update. */
#define FSB_RED_CXT_FIL_ID_PER_BULK_UPDT      40
#define FSB_RED_DEF_FIL_ID_PER_BULK_UPDT      20
#define FSB_RED_SESS_CNT_PER_BULK_UPDT        15
#define FSB_RED_FCOE_CNT_PER_BULK_UPDT        7
#define FSB_RED_SCHAN_FIL_ID_PER_BULK_UPDT    125  

#define FSB_RED_PORT_DOWN 1

#define FSB_NODE_STATUS()\
     (gFsbGlobals.FsbRedGlobalInfo.u1NodeStatus)

#define FSB_BULK_REQ_RCVD()\
     (gFsbGlobals.FsbRedGlobalInfo.bBulkReqRcvd)

#ifdef RM_WANTED
#define  FSB_IS_NP_PROGRAMMING_ALLOWED() \
        ((L2RED_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE) ? FSB_TRUE: FSB_FALSE)
#else
#define  FSB_IS_NP_PROGRAMMING_ALLOWED() FSB_TRUE
#endif

#define FSB_GET_NUM_STANDBY_NODES() gFsbGlobals.FsbRedGlobalInfo.u1NumPeersUp

#define FSB_INIT_NUM_STANDBY_NODES() \
                      gFsbGlobals.FsbRedGlobalInfo.u1NumPeersUp =0;

#define FSB_IS_STANDBY_UP() \
          ((gFsbGlobals.FsbRedGlobalInfo.u1NumPeersUp > 0) ? FSB_TRUE : FSB_FALSE)

#define FSB_RM_GET_NUM_STANDBY_NODES_UP() \
      gFsbGlobals.FsbRedGlobalInfo.u1NumPeersUp = FsbRmGetStandbyNodeCount ()

#define FSB_NUM_STANDBY_NODES() gFsbGlobals.FsbRedGlobalInfo.u1NumPeersUp

enum
{
    FSB_BULK_UPD_NOT_STARTED = 0,
    FSB_BULK_UPD_COMPLETED,
    FSB_BULK_UPD_IN_PROGRESS
};

#define FSB_RED_FCF_MSG_LEN           41
#define FSB_RED_VLAN_FIL_ID_MSG_LEN   20 
#define FSB_RED_SCHAN_FIL_ID_MSG_LEN  12
#define FSB_FIP_SNOOPING_INDEX_SIZE   6  /* Context Id + Vlan Id*/
#define FSB_FILTER_COUNT              2 
#define FSB_DEF_FIL_ID_MSG_SIZE       10 /* DstMac + Hw Filter Id*/
#define FSB_FIP_SESS_INDEX_SIZE       12 /* VlanId  + ENodeIfIndex + ENodeMac */
#define FSB_FIP_SESS_ENTRY_SIZE       24 /* FcMap + NameId + Context Id 
                                          * FcfIfIndex + FCoECount + State */
#define FSB_FIP_SESS_FIL_ID_MSG_SIZE  32 /* DstMac + SrcMac + OpCode + SubOpCode +
                                          * Portlist + AggIndex + FilterId + Hw FilterId */
#define FSB_FCOE_INDEX_SIZE           24 /* ENodeIfIndex + ENodeMac + FCFMac
                                          * FCoEMac + VlanId */          
#define FSB_FCOE_ENTRY_SIZE           5  /* FcfId + EnodeConnType + 
                                          * HouseKeepingTimerFlag */ 
#define FSB_FCOE_FIL_ID_MSG_SIZE      35 /* DstMac + SrcMac + OpCode + SubOpCode +
                                          * Portlist + Ethrt Type +  Filter Action +
                                          * AggIndex + FilterId + Hw FilterId */
#define FSB_RED_DEF_FIL_ID_MSG_LEN(u2NoOfFilter)\
    FSB_FIP_SNOOPING_INDEX_SIZE + FSB_FILTER_COUNT +\
   (FSB_DEF_FIL_ID_MSG_SIZE * u2NoOfFilter)
#define FSB_RED_FIP_SESS_MSG_LEN(u2NoOfFilter)\
    FSB_FIP_SESS_INDEX_SIZE + FSB_FIP_SESS_ENTRY_SIZE+ \
    FSB_FILTER_COUNT + (FSB_FIP_SESS_FIL_ID_MSG_SIZE * u2NoOfFilter)
#define FSB_RED_FCOE_MSG_LEN(u2NoOfFilter)\
    FSB_FCOE_INDEX_SIZE + FSB_FCOE_ENTRY_SIZE +\
    FSB_FILTER_COUNT +(FSB_FCOE_FIL_ID_MSG_SIZE * u2NoOfFilter)

#define FSB_RED_BULK_UPD_STATUS()\
    gFsbGlobals.FsbRedGlobalInfo.u1BulkUpdStatus

#define FSB_RED_FCF_BULK_UPD_STATUS()\
    gFsbGlobals.FsbRedGlobalInfo.bFcfBulkUpdStatus

#define FSB_RED_CXT_FIL_ID_BULK_UPD_STATUS()\
    gFsbGlobals.FsbRedGlobalInfo.bDefVlanFilIdBulkUpdStatus

#define FSB_RED_DEF_FIL_ID_BULK_UPD_STATUS()\
    gFsbGlobals.FsbRedGlobalInfo.bDefFilIdBulkUpdStatus

#define FSB_RED_FIP_SESS_BULK_UPD_STATUS()\
    gFsbGlobals.FsbRedGlobalInfo.bSessBulkUpdStatus

#define FSB_RED_FCOE_BULK_UPD_STATUS()\
    gFsbGlobals.FsbRedGlobalInfo.bFCoEBulkUpdStatus

#define FSB_RED_SCHAN_FIL_ID_BULK_UPD_STATUS()\
    gFsbGlobals.FsbRedGlobalInfo.bSChanFilIdBulkUpdStatus

#define FSB_RED_NEXT_FCF_ENTRY() \
    gFsbGlobals.FsbRedGlobalInfo.pNextFcfEntry

#define FSB_RED_NEXT_CXT_ID() \
    gFsbGlobals.FsbRedGlobalInfo.u4NextContextId

#define FSB_RED_NEXT_SNOOPING_ENTRY() \
    gFsbGlobals.FsbRedGlobalInfo.pNextFipSnoopingEntry

#define FSB_RED_NEXT_SESS_ENTRY() \
    gFsbGlobals.FsbRedGlobalInfo.pNextFipSessEntry

#define FSB_RED_NEXT_FCOE_ENTRY() \
    gFsbGlobals.FsbRedGlobalInfo.pNextFipSessFCoEEntry

#define FSB_RED_NEXT_SCHAN_ENTRY()\
    gFsbGlobals.FsbRedGlobalInfo.pNextSChannelFilterEntry
enum
{
 FSB_BULK_REQ_MSG = RM_BULK_UPDT_REQ_MSG,
 FSB_BULK_UPD_TAIL_MSG = RM_BULK_UPDT_TAIL_MSG,
 FSB_SYN_DEF_VLAN_UPT_MSG,
 FSB_SYN_DEF_FILTER_UPT_MSG,
 FSB_SYN_SCHAN_FILTER_UPT_MSG,
 FSB_SYN_FCF_ADD_MSG,
 FSB_SYN_FCF_FILTER_UPT_MSG,
 FSB_SYN_FCF_DEL_MSG,
 FSB_SYNC_FIP_SOLICIT_MSG,
 FSB_SYNC_FIP_ADVT_MSG,
 FSB_SYNC_FIP_INST_MSG,
 FSB_SYNC_FIP_FLOGI_ACCEPT_MSG,
 FSB_SYNC_FIP_FDISC_REJECT_MSG,
 FSB_SYNC_FIP_LOGO_REQUEST_MSG,
 FSB_SYNC_FIP_LOGO_REJECT_MSG,
 FSB_SYNC_FIP_CLEAR_SESSION_MSG,
 FSB_SYNC_FIP_CLEAR_LINK_MSG,
 FSB_SYNC_FIP_SESS_DEL_MSG,
 FSB_SYN_FCOE_DEL_MSG,
 FSB_FIP_SESS_BULK_UPDT_MSG,
 FSB_FCOE_BULK_UPDT_MSG,
 FSB_SYN_OPER_STATUS_MSG 
};
#define FSB_FCF_ADD    1
#define FSB_FCF_DEL    2

#define FSB_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define FSB_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define FSB_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define FSB_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
    RM_COPY_TO_OFFSET(pdest, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

#define FSB_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define FSB_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define FSB_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define FSB_RM_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
do { \
    RM_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
        *(pu4Offset) += u4Size; \
}while (0)

INT4 FsbRedSendMsgToRm PROTO ((tRmMsg *, UINT2));
VOID FsbRedHandleRmEvents PROTO ((tFsbNotifyParams *pMsg));
INT4 FsbRedMakeNodeActive PROTO ((VOID));
INT4 FsbRedRcvPktFromRm PROTO ((UINT1, tRmMsg *, UINT2));
INT4 FsbRedRegisterWithRM PROTO ((VOID));
INT4 FsbRedDeRegisterWithRM PROTO ((VOID));
tRmMsg* FsbRedGetMsgBuffer PROTO((UINT2 u2BufSize));


VOID FsbRedHandleSyncUpMessage PROTO ((tRmMsg *, UINT2));
VOID FsbRedHandleGoStandbyEvent PROTO((VOID));
VOID FsbRedHandleStandByUpEvent PROTO((VOID));
INT4 FsbRedSendBulkReq PROTO((VOID));

VOID FsbRedHandleBulkUpdateEvent PROTO((VOID));
VOID FsbRedHandleBulkReqEvent PROTO((VOID));
INT4 FsbRedSendBulkUpdateTailMsg PROTO((VOID));
VOID FsbRedInitBulkUpdateFlags PROTO((VOID));

VOID FsbRedStartFIPFCoESessionTimers PROTO((VOID));
VOID FsbRedStartFcfTimer PROTO((VOID));

INT4 FsbRedSyncUpOperStatus PROTO((UINT4, UINT1));
INT4 FsbRedProcessOperStatus PROTO((tRmMsg *, UINT4 *, UINT2*));

/* Default VLAN Filter - Hardware Filter Id */
INT4 FsbRedSyncUpDefaultVlanFilter PROTO ((tFsbContextInfo *, UINT4));
INT4 FsbRedProcessDefaultVlanFilter PROTO ((tRmMsg *, UINT4 *, UINT2*));
VOID FsbDefVLANHwFilterIdBulkUpdate PROTO((BOOL1 *));

/* Default Filter - Hardware Filter Id */
INT4 FsbRedSyncUpDefaultFilter PROTO ((tFsbFipSnoopingEntry *));
INT4 FsbRedProcessDefaultFilter PROTO ((tRmMsg *, UINT4 *, UINT2*));
VOID FsbDefHwFilterIdBulkUpdate PROTO((BOOL1 *));
VOID FsbRedUpdateDefFilterId PROTO((tFsbFipSnoopingEntry *, tRmMsg *, UINT4 *, UINT2));

/* SChannel - Hardware Filter Id */
INT4 FsbRedSyncUpSChannelFilterId PROTO((UINT4, UINT2, UINT4));
INT4 FsbRedProcessSChannelFilterId PROTO((tRmMsg *, UINT4 *, UINT2*));
VOID FsbSChanHwFilterIdBulkUpdate PROTO((BOOL1 *));

/* FCF Entry */
VOID FsbRedConstructFCFEntry PROTO ((tFsbFcfEntry *, tRmMsg *, UINT4 *));
INT4 FsbRedSyncUpFCFEntry PROTO ((tFsbFcfEntry *, UINT1 ));
INT4 FsbRedSyncUpAddFCFEntry PROTO ((tFsbFcfEntry *, tRmMsg *));
INT4 FsbRedSyncUpFCFFilterId PROTO ((tFsbFcfEntry *));
INT4 FsbRedSyncUpDeleteFCFEntry PROTO ((tFsbFcfEntry *, tRmMsg *));
INT4 FsbRedProcessAddFCFEntry PROTO ((tRmMsg *, UINT4 *, UINT2*));
INT4 FsbRedProcessFCFFilterId PROTO ((tRmMsg *, UINT4 *, UINT2*));
INT4 FsbRedProcessDeleteFCFEntry PROTO ((tRmMsg *, UINT4 *, UINT2*));
VOID FsbFCFBulkUpdate PROTO((BOOL1 *));

/* FIP Session Entry */
tFsbFilterEntry* FsbRedGetFilterEntry PROTO ((tFsbFipSessFCoEEntry *, UINT1));
INT4 FsbRedSyncUpFIPSessEntry PROTO ((tFsbFipSessEntry *, tFSBPktInfo *));
INT4 FsbRedSyncUpFIPSolicit PROTO  ((tFsbFipSessEntry *, tRmMsg *, tFSBPktInfo *));
INT4 FsbRedSyncUpFIPAdvtUcast PROTO ((tFsbFipSessEntry *, tRmMsg *, tFSBPktInfo *));
INT4 FsbRedSyncUpFIPInstRequest PROTO ((tFsbFipSessEntry *, tRmMsg *, tFSBPktInfo *));
INT4 FsbRedSyncUpFIPFLOGIAccept PROTO ((tFsbFipSessEntry *, tRmMsg *, tFSBPktInfo *));
INT4 FsbRedSyncUpFIPFDISCReject PROTO ((tFsbFipSessEntry *, tRmMsg *, tFSBPktInfo *));
INT4 FsbRedSyncUpFIPLOGORequest PROTO ((tFsbFipSessEntry *, tRmMsg *, tFSBPktInfo *));
INT4 FsbRedSyncUpFIPLOGOReject PROTO ((tFsbFipSessEntry *, tRmMsg *, tFSBPktInfo *));
INT4 FsbRedSyncUpClearFIPSession PROTO ((tFsbFipSessEntry *, tRmMsg *,UINT1));
INT4 FsbRedSyncUpClearVirtualLink PROTO ((tFsbFipSessEntry *, tRmMsg *, tFSBPktInfo *, UINT2 *));
INT4 FsbRedSyncUpFIPSessionDelete PROTO ((tFsbFipSessEntry *));
INT4 FsbRedSyncUpDeleteFCoEEntry PROTO ((tFsbFipSessFCoEEntry *));
INT4 FsbRedProcessFIPSolicit PROTO ((tRmMsg *, UINT4 *, UINT2 *));
INT4 FsbRedProcessFIPAdvtUcast PROTO ((tRmMsg * pMsg, UINT4 *, UINT2 *));
INT4 FsbRedProcessFIPInstRequest PROTO ((tRmMsg * pMsg, UINT4 *, UINT2 *));
INT4 FsbRedProcessFIPFLOGIAccept PROTO ((tRmMsg * pMsg, UINT4 *, UINT2 *));
INT4 FsbRedProcessFIPFDISCReject PROTO ((tRmMsg * pMsg, UINT4 *, UINT2 *));
INT4 FsbRedProcessFIPLOGORequest PROTO ((tRmMsg * pMsg, UINT4 *, UINT2 *));
INT4 FsbRedProcessFIPLOGOReject PROTO ((tRmMsg * pMsg, UINT4 *, UINT2 *));
INT4 FsbRedProcessFIPClearSession PROTO ((tRmMsg * pMsg, UINT4 *, UINT2 *));
INT4 FsbRedProcessClearVirtualLink PROTO ((tRmMsg * pMsg, UINT4 *, UINT2 *));
INT4 FsbRedProcessFipSessDelete PROTO ((tRmMsg * pMsg, UINT4 *, UINT2 *));
INT4 FsbRedProcessDeleteFCoEEntry PROTO ((tRmMsg * pMsg, UINT4 *, UINT2 *));
INT4 FsbRedProcessFipSessBulkUpdate PROTO ((tRmMsg * pMsg, UINT4 *, UINT2 *));
INT4 FsbRedProcessFCoEBulkUpdate PROTO ((tRmMsg * pMsg, UINT4 *, UINT2 *));
VOID FsbFIPSessionBulkUpdate PROTO ((BOOL1 *));
VOID FsbRedFormFipSessBulkUpdMsg PROTO ((tFsbFipSessEntry *,tRmMsg *, UINT4 *, 
                                         UINT2));
VOID FsbRedFormFCoEBulkUpdMsg PROTO ((tFsbFipSessFCoEEntry *, tRmMsg *, UINT4 *,
                                         UINT2));
VOID FsbFCoEBulkUpdate PROTO ((BOOL1 *));

#endif /* _FSBRED_H */
