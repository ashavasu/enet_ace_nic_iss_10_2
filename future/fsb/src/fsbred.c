/*************************************************************************
* Copyright (C) 2007-2015 Aricent Group . All Rights Reserved
*
* $Id: fsbred.c,v 1.7 2017/11/07 12:23:59 siva Exp $
*
* Description: This file contains FSB redundancy related routines
*********************************************************************/
#include "fsbinc.h"

/*****************************************************************************/
/* Function Name      : FsbRegisterWithRM                                    */
/*                                                                           */
/* Description        : Registers FSB with RM by providing an application    */
/*                      ID for FSB and a call back function to be called     */
/*                      whenever RM needs to send an event to FSB.           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then FSB_SUCCESS          */
/*                      Otherwise FSB_FAILURE                                */
/*****************************************************************************/
INT4
FsbRedRegisterWithRM (VOID)
{
    tRmRegParams        RmRegParams;

    RmRegParams.u4EntId = RM_FSB_APP_ID;
    RmRegParams.pFnRcvPkt = FsbRedRcvPktFromRm;
    if (FsbRmRegisterProtocols (&RmRegParams) != RM_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_DEBUGGING_LEVEL, FSB_INIT_SHUT_TRC,
                         "\r\nFsbRedRegisterWithRM Failed ... \r\n");
        return FSB_FAILURE;
    }

    MEMSET (&gFsbGlobals.FsbRedGlobalInfo, 0, sizeof (tFsbRedGlobalInfo));

    FSB_NODE_STATUS () = FSB_NODE_IDLE;
    FSB_BULK_REQ_RCVD () = FSB_FALSE;
    FSB_NUM_STANDBY_NODES () = 0;
    FSB_RED_BULK_UPD_STATUS () = FSB_BULK_UPD_NOT_STARTED;
    FSB_RM_GET_NUM_STANDBY_NODES_UP ();

    FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                     FSB_DEBUGGING_LEVEL, FSB_INIT_SHUT_TRC,
                     "\r\nFsbRedRegisterWithRM SUCCESS ... \r\n");

    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbDeRegisterWithRM                                  */
/*                                                                           */
/* Description        : Deregisters FSB with RM                              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if deregistration is success then FSB_SUCCESS        */
/*                      Otherwise FSB_FAILURE                                */
/*****************************************************************************/
INT4
FsbRedDeRegisterWithRM (VOID)
{
    if (FsbRmDeRegisterProtocols () != RM_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_DEBUGGING_LEVEL, FSB_INIT_SHUT_TRC,
                         "\r\nFsbRedDeRegisterWithRM Failed ... \r\n");

        return FSB_FAILURE;
    }

    FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                     FSB_DEBUGGING_LEVEL, FSB_INIT_SHUT_TRC,
                     "\r\nFsbRedDeRegisterWithRM SUCCESS ... \r\n");
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedRcvPktFromRm                                   */
/*                                                                           */
/* Description        : This function constructs a message containing the    */
/*                      given RM event and RM message and post it to the FSB */
/*                      queue.                                               */
/*                                                                           */
/* Input(s)           : u1Event - Event given by RM module                   */
/*                      pData   - Msg to be enqueue                          */
/*                      u2DataLen - Msg size                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if msg is enqueued and event sent then RM_SUCCESS    */
/*                      Otherwise RM_FAILURE                                 */
/*****************************************************************************/
INT4
FsbRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tFsbNotifyParams   *pBuf = NULL;

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Message absent and hence no need to process and no need
         * to send anything to FSB task. */

        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbRedRcvPktFromRm() invalid data returning failure ... \r\n");
        return RM_FAILURE;
    }

    if ((pBuf =
         (tFsbNotifyParams *)
         MemAllocMemBlk (FSB_NOTIFY_PARAMS_INFO_MEMPOOL_ID)) == NULL)
    {
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            FsbRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_BUFFER_TRC,
                         "\r\nFsbRedRcvPktFromRm() MemAllocMemBlk failed ... \r\n");

        return RM_FAILURE;

    }

    FSB_MEMSET (pBuf, 0, sizeof (tFsbNotifyParams));

    pBuf->u4MsgType = FSB_RM_MSG;

    pBuf->RmMsg.u1Event = u1Event;
    pBuf->RmMsg.pData = pData;
    pBuf->RmMsg.u2DataLen = u2DataLen;

    if (OsixQueSend (FSB_QUEUE_ID, (UINT1 *) &pBuf,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (FSB_NOTIFY_PARAMS_INFO_MEMPOOL_ID, (UINT1 *) pBuf);

        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            FsbRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_BUFFER_TRC,
                         "\r\nFsbRedRcvPktFromRm() OsixQueSend() failed  ... \r\n");
        return RM_FAILURE;

    }
    /* Sent the event to FSB */
    OsixEvtSend (FSB_TASK_ID, FSB_QUEUE_EVENT);

    FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                     FSB_CONTROL_PATH_TRC,
                     "FsbRedRcvPktFromRm() Received success ... \r\n");

    return RM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedSendMsgToRm                                    */
/*                                                                           */
/* Description        : This function constructs the RM message from the     */
/*                      given linear buf and sends it to Redundancy Manager. */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to the RM input buffer.               */
/*                      u2BufSize  - Size of the given input buffer.         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if sync msg is sent then FSB_SUCCESS                 */
/*                      Otherwise FSB_FAILURE                                */
/*****************************************************************************/
INT4
FsbRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2BufSize)
{
    /* Call the API provided by RM to send the data to RM */
    if (pMsg == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\n FsbRedSendMsgToRm() - pMsg is NULL \r\n");

        return FSB_FAILURE;
    }
    if (FsbRmEnqMsgToRmFromAppl (pMsg, u2BufSize, RM_FSB_APP_ID, RM_FSB_APP_ID)
        == RM_FAILURE)
    {
        /* pMsg is freed only in failure case. RM will free the buf
         * in success case. */
        RM_FREE (pMsg);

        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbRedSendMsgToRm () FsbRmEnqMsgToRmFromAppl() returns failure ... \r\n");
        return FSB_FAILURE;
    }

    FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                     FSB_CONTROL_PATH_TRC,
                     "FsbRedSendMsgToRm () send  success ... \r\n");
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedHandleRmEvents                                 */
/*                                                                           */
/* Description        : This function is invoked to process the following    */
/*                      from RM module:-                                     */
/*                           - RM events and                                 */
/*                           - update messages.                              */
/*                      This function interprets the RM events and calls the */
/*                      corresponding module functions to process those      */
/*                      events.                                              */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to the  input buffer.                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
FsbRedHandleRmEvents (tFsbNotifyParams * pMsg)
{
    tRmNodeInfo        *pData = NULL;
    UINT1               u1PrevNodeState = FSB_NODE_IDLE;
    tRmProtoEvt         ProtoEvt;
    tRmProtoAck         ProtoAck;
    UINT4               u4SeqNum = 0;

    ProtoEvt.u4AppId = RM_FSB_APP_ID;
    ProtoEvt.u4Error = RM_NONE;
    switch (pMsg->RmMsg.u1Event)
    {
        case GO_ACTIVE:

            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                             FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                             "\r\nFsbRedHandleRmEvents() received GO_ACTIVE ... \r\n");

            if (FSB_NODE_STATUS () == FSB_NODE_ACTIVE)
            {
                break;
            }

            u1PrevNodeState = FSB_NODE_STATUS ();
            FsbRedMakeNodeActive ();
            if (FSB_BULK_REQ_RCVD () == FSB_TRUE)
            {
                FSB_BULK_REQ_RCVD () = FSB_FALSE;
                FsbRedInitBulkUpdateFlags ();
                FSB_RED_BULK_UPD_STATUS () = FSB_BULK_UPD_IN_PROGRESS;
                FsbRedHandleBulkUpdateEvent ();
            }
            if (u1PrevNodeState == FSB_NODE_IDLE)
            {
                ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
            }
            else
            {
                ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
            }
            FsbRmHandleProtocolEvent (&ProtoEvt);
            break;

        case GO_STANDBY:

            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                             FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                             "\r\nFsbRedHandleRmEvents() received GO_STANDBY ... \r\n");

            if (FSB_NODE_STATUS () == FSB_NODE_STANDBY)
            {
                break;
            }
            else if (FSB_NODE_STATUS () == FSB_NODE_IDLE)
            {
                return;
            }
            else if (FSB_NODE_STATUS () == FSB_NODE_ACTIVE)
            {
                FSB_NODE_STATUS () = FSB_NODE_STANDBY;
                FSB_INIT_NUM_STANDBY_NODES ();
                FSB_RED_BULK_UPD_STATUS () = FSB_BULK_UPD_NOT_STARTED;
                FsbDisable ();
                FsbEnable ();
                ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                FsbRmHandleProtocolEvent (&ProtoEvt);
            }
            FSB_BULK_REQ_RCVD () = FSB_FALSE;
            break;

        case RM_STANDBY_UP:

            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                             FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                             "\r\nFsbRedHandleRmEvents() received RM_STANDBY_UP ... \r\n");

            pData = (tRmNodeInfo *) pMsg->RmMsg.pData;
            FSB_NUM_STANDBY_NODES () = pData->u1NumStandby;
            FsbRmReleaseMemoryForMsg ((UINT1 *) pData);
            FsbRedHandleStandByUpEvent ();
            break;

        case RM_STANDBY_DOWN:

            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                             FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                             "\r\nFsbRedHandleRmEvents() received RM_STANDBY_DOWN ... \r\n");

            pData = (tRmNodeInfo *) pMsg->RmMsg.pData;
            FSB_NUM_STANDBY_NODES () = pData->u1NumStandby;
            FsbRmReleaseMemoryForMsg ((UINT1 *) pData);
            break;

        case RM_MESSAGE:

            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                             FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                             "\r\nFsbRedHandleRmEvents() received RM_MESSAGE ... \r\n");

            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM ((tRmMsg *) (pMsg->RmMsg.pData), &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR ((tRmMsg *) (pMsg->RmMsg.pData),
                                 pMsg->RmMsg.u2DataLen);

            ProtoAck.u4AppId = RM_FSB_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;
            FsbRedHandleSyncUpMessage
                ((tRmMsg *) pMsg->RmMsg.pData, pMsg->RmMsg.u2DataLen);

            /* Processing of message is over, hence free the RM message. */
            RM_FREE ((tRmMsg *) (pMsg->RmMsg.pData));

            /* Sending ACK to RM */
            RmApiSendProtoAckToRM (&ProtoAck);
            break;

        case RM_DYNAMIC_SYNCH_AUDIT:
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                             FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                             "\r\nFsbRedHandleRmEvents() received RM_DYNAMIC_SYNCH_AUDIT ... \r\n");
            break;

        case RM_COMPLETE_SYNC_UP:
            break;

        case L2_COMPLETE_SYNC_UP:
            break;

        case L2_INITIATE_BULK_UPDATES:
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                             FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                             "[STANDBY]: Received L2Initiate Bulk Update Message \r\n");
            FsbRedSendBulkReq ();
            break;

        case RM_INIT_HW_AUDIT:
            break;

        case RM_CONFIG_RESTORE_COMPLETE:
            if (FSB_NODE_STATUS () == FSB_NODE_IDLE)
            {
                if (RmGetNodeState () == RM_STANDBY)
                {
                    FSB_NODE_STATUS () = FSB_NODE_STANDBY;
                    FSB_INIT_NUM_STANDBY_NODES ();
                }

                ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                FsbRmHandleProtocolEvent (&ProtoEvt);
            }
            break;

        default:
            break;
    }
}

/*****************************************************************************/
/* Function Name      : FsbRedMakeNodeActive.                                */
/*                                                                           */
/* Description        : This function is used to handle the GO_ACTIVE event  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS - On success                             */
/*                      FSB_FAILURE - On failure.                            */
/*****************************************************************************/
INT4
FsbRedMakeNodeActive (VOID)
{
    if (FSB_NODE_STATUS () == FSB_NODE_ACTIVE)
    {
        return FSB_SUCCESS;
    }

    if (FSB_NODE_STATUS () == FSB_NODE_IDLE)
    {
        FSB_NODE_STATUS () = FSB_NODE_ACTIVE;
        FSB_RM_GET_NUM_STANDBY_NODES_UP ();
        return FSB_SUCCESS;
    }
    if (FSB_NODE_STATUS () == FSB_NODE_STANDBY)
    {
        FSB_NODE_STATUS () = FSB_NODE_ACTIVE;
        FSB_RM_GET_NUM_STANDBY_NODES_UP ();
        FsbRedStartFIPFCoESessionTimers ();
        FsbRedStartFcfTimer ();
        return FSB_SUCCESS;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedHandleStandByUpEvent.                          */
/*                                                                           */
/* Description        : This function handles the STANDBY UP event from RM.  */
/*                      If node is active, then it increments the            */
/*                      u1NumPeersUp and calls FsbRedHandleBulkReqEvent      */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
FsbRedHandleStandByUpEvent ()
{

    if (FSB_BULK_REQ_RCVD () == FSB_TRUE)
    {
        FSB_BULK_REQ_RCVD () = FSB_FALSE;
        /* Bulk request msg is recieved before RM_STANDBY_UP event.
         * So we are sending bulk updates now.
         */
        FsbRedInitBulkUpdateFlags ();
        FSB_RED_BULK_UPD_STATUS () = FSB_BULK_UPD_IN_PROGRESS;
        FsbRedHandleBulkUpdateEvent ();
    }
}

/*****************************************************************************/
/* Function Name      : FsbRedHandleSyncUpMessage.                           */
/*                                                                           */
/* Description        : This function is used to process the peer active     */
/*                      node messages at standby side.                       */
/*                                                                           */
/* Input(s)           : pMesg    - Pointer to the sync up message.           */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
FsbRedHandleSyncUpMessage (tRmMsg * pMesg, UINT2 u2Length)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4RedMsgType = 0;
    UINT4               u4Offset = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_FSB_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* No need to do null pointer check for pMesg as it is done
     * in FsbRedRcvPktFromRm. */
    if (u2Length < (FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE))
    {
        /* Message size is very less to process. */
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_CONTROL_PATH_TRC,
                         "FsbRedHandleSyncUpMessage: Message size is very less to process\r\n");
        return;
    }
    /* Get the Message type. */
    FSB_RM_GET_4_BYTE (pMesg, &u4Offset, u4RedMsgType);
    u2Length = (UINT2) (u2Length - FSB_SYNC_MSG_TYPE_SIZE);

    switch (u4RedMsgType)
    {
        case FSB_SYN_DEF_VLAN_UPT_MSG:
            /* ------------------------------------------------------
             * |MsgType |MsgLen |No of Def   |CntxtId |Hw Filter Id | 
             * |        |       |VLAN filter |        |             |
             * -----------------------------------------------------*/
            FsbRedProcessDefaultVlanFilter (pMesg, &u4Offset, &u2Length);
            break;

        case FSB_SYN_DEF_FILTER_UPT_MSG:

            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                             FSB_CONTROL_PATH_TRC,
                             "\r\nFsbRedHandleSyncUpMessage() - FSB_SYN_DEF_FILTER_UPT_MSG  ... \r\n");

            /* ----------------------------------------------------------------------------------------------------
             * |MsgType |MsgLen |No of Def |CntxtId |Vlan Id |Dst MAC1 |Hw Filter Id1 |...|Dst MACn |Hw Filter Idn| 
             * |        |       |filter    |        |        |Address  |              |   |Address  |             |
             * ---------------------------------------------------------------------------------------------------*/
            FsbRedProcessDefaultFilter (pMesg, &u4Offset, &u2Length);
            break;

        case FSB_SYN_SCHAN_FILTER_UPT_MSG:

            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                             FSB_CONTROL_PATH_TRC,
                             "\r\nFsbRedHandleSyncUpMessage() - FSB_SYN_SCHAN_FILTER_UPT_MSG   ... \r\n");

            /* ----------------------------------------------------------------------------------
             * |MsgType |MsgLen |No of SChan |IfIndex |Vlan Id |Hw Filter Id1 |...|Hw Filter Idn| 
             * |        |       |filter      |        |        |              |   |             |
             * ---------------------------------------------------------------------------------*/
            FsbRedProcessSChannelFilterId (pMesg, &u4Offset, &u2Length);
            break;

        case FSB_SYN_FCF_ADD_MSG:
            /* ---------------------------------------------------------------------------------------
             * |MsgType |MsgLen |No Of FCF |CntxtId |VlanId |FCFIf |Addr |FCMAP |FCF |NameId |Fabric | 
             * |        |       |          |        |       |Index |Mode |      |MAC |       |Name   |
             * --------------------------------------------------------------------------------------*/
            FsbRedProcessAddFCFEntry (pMesg, &u4Offset, &u2Length);
            break;

        case FSB_SYN_FCF_FILTER_UPT_MSG:
            /* -----------------------------------------------------------
             * |MsgType |MsgLen |CntxtId |Vlan Id |FCF MAC |Hw Filter Id | 
             * |        |       |        |        |        |             |
             * ----------------------------------------------------------*/
            FsbRedProcessFCFFilterId (pMesg, &u4Offset, &u2Length);
            break;

        case FSB_SYN_FCF_DEL_MSG:
            /* -----------------------------------------
             * |MsgType |MsgLen |VlanId |FCFIf |FCFMAC | 
             * |        |       |       |Index |       |                
             * ----------------------------------------*/
            FsbRedProcessDeleteFCFEntry (pMesg, &u4Offset, &u2Length);
            break;

        case FSB_SYNC_FIP_SOLICIT_MSG:
            /* ---------------------------------------------------
             * |MsgType |MsgLen |VlanId |ENodeIf |ENode |CntxtId |
             * |        |       |       |Index   |MAC   |        |
             * --------------------------------------------------*/
            FsbRedProcessFIPSolicit (pMesg, &u4Offset, &u2Length);
            break;

        case FSB_SYNC_FIP_ADVT_MSG:
            /* --------------------------------------------------------------
             * |MsgType |MsgLen |VlanId |ENodeIf |ENode |FCFIf |FCF |NameId |
             * |        |       |       |Index   |MAC   |Index |MAP |       |
             * -------------------------------------------------------------*/
            FsbRedProcessFIPAdvtUcast (pMesg, &u4Offset, &u2Length);
            break;

        case FSB_SYNC_FIP_INST_MSG:
            /* --------------------------------------------------------------
             * |MsgType |MsgLen |VlanId |ENodeIf |ENode |FCF |ENodeConnType |
             * |        |       |       |Index   |MAC   |MAC |              |
             * -------------------------------------------------------------*/
            FsbRedProcessFIPInstRequest (pMesg, &u4Offset, &u2Length);
            break;

        case FSB_SYNC_FIP_FLOGI_ACCEPT_MSG:
            /* -------------------------------------------------------
             * |MsgType |MsgLen |VlanId |ENodeIf |ENode |FCoE |FCFId |
             * |        |       |       |Index   |MAC   |MAC  |      |
             * ------------------------------------------------------*/
            FsbRedProcessFIPFLOGIAccept (pMesg, &u4Offset, &u2Length);
            break;

        case FSB_SYNC_FIP_FDISC_REJECT_MSG:
            /* -----------------------------------------
             * |MsgType |MsgLen |VlanId |ENode |Packet |
             * |        |       |       |MAC   |Type   |
             * ----------------------------------------*/
            FsbRedProcessFIPFDISCReject (pMesg, &u4Offset, &u2Length);
            break;

        case FSB_SYNC_FIP_LOGO_REQUEST_MSG:
            /* --------------------------------------------------
             * |MsgType |MsgLen |VlanId |ENodeIf |ENode |Packet |
             * |        |       |       |Index   |MAC   |Type   |
             * -------------------------------------------------*/
            FsbRedProcessFIPLOGORequest (pMesg, &u4Offset, &u2Length);
            break;

        case FSB_SYNC_FIP_LOGO_REJECT_MSG:
            /* -----------------------------------------
             * |MsgType |MsgLen |VlanId |ENode |Packet |
             * |        |       |       |MAC   |Type   |
             * ----------------------------------------*/
            FsbRedProcessFIPLOGOReject (pMesg, &u4Offset, &u2Length);
            break;

        case FSB_SYNC_FIP_CLEAR_SESSION_MSG:
            /* -----------------------------------------
             * |MsgType |MsgLen |VlanId |ENode |Packet |
             * |        |       |       |MAC   |Type   |
             * ----------------------------------------*/
            FsbRedProcessFIPClearSession (pMesg, &u4Offset, &u2Length);
            break;

        case FSB_SYNC_FIP_CLEAR_LINK_MSG:
            /* ------------------------------------------------------------------
             * |MsgType |MsgLen |VlanId |ENode |FCF |VNMac |VNMac1 |... |VNMacN |
             * |        |       |       |MAC   |MAC |Count |       |    |       |
             * -----------------------------------------------------------------*/
            FsbRedProcessClearVirtualLink (pMesg, &u4Offset, &u2Length);
            break;

        case FSB_SYNC_FIP_SESS_DEL_MSG:
            /* ------------------------------------------
             * |MsgType |MsgLen |VlanId |ENodeIf |ENode |
             * |        |       |       |Index   |MAC   |
             * -----------------------------------------*/
            FsbRedProcessFipSessDelete (pMesg, &u4Offset, &u2Length);
            break;

        case FSB_SYN_FCOE_DEL_MSG:
            /* -----------------------------------------------------
             * |MsgType |MsgLen |VlanId |ENodeIf |ENode |FCoE |FCF | 
             * |        |       |       |Index   |MAC   |MAC  |MAC |                
             * ----------------------------------------------------*/
            FsbRedProcessDeleteFCoEEntry (pMesg, &u4Offset, &u2Length);
            break;

        case FSB_SYN_OPER_STATUS_MSG:
            /* ----------------------------------------
             * |MsgType |MsgLen |IfIndex |Oper Status |
             * |        |       |        |            |    
             * ---------------------------------------*/
            FsbRedProcessOperStatus (pMesg, &u4Offset, &u2Length);
            break;

        case FSB_FIP_SESS_BULK_UPDT_MSG:
            /* --------------------------------------------------------------------------------------------------------------------------------
             * |MsgType |MsgLen |VlanId |ENodeIf |ENode |FC  |Name |Cxt |FcfIf |State |Dst |Src |Op   |SubOp |If    |Agg   |Filter |Hw Filter |
             * |        |       |       |Index   |MAC   |MAP |Id   |Id  |Index |      |Mac |Mac |Code |Code  |Index |Index |Id     |Id        |
             * -------------------------------------------------------------------------------------------------------------------------------*/
            FsbRedProcessFipSessBulkUpdate (pMesg, &u4Offset, &u2Length);
            break;
        case FSB_FCOE_BULK_UPDT_MSG:

            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                             FSB_CONTROL_PATH_TRC,
                             "\r\nFsbRedHandleSyncUpMessage() - FSB_FCOE_BULK_UPDT_MSG  ... \r\n");

            /* -----------------------------------------------------------------------------------------------------------------------------------------
             * |MsgType |MsgLen |VlanId |ENodeIf |ENode |Fcf |FCoE |Fcf |ENode    |Dst |Src |Op   |SubOp |If    |Ether |Filter |Agg |Filter |Hw Filter |
             * |        |       |       |Index   |MAC   |MAC |MAC  |Id  |ConnType |Mac |Mac |Code |Code  |Index |Type  |Action |Id  |Id     |Id        |
             * ----------------------------------------------------------------------------------------------------------------------------------------*/
            FsbRedProcessFCoEBulkUpdate (pMesg, &u4Offset, &u2Length);
            break;

        case FSB_BULK_REQ_MSG:

            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                             FSB_CONTROL_PATH_TRC,
                             "\r\nFsbRedHandleSyncUpMessage() - FSB_BULK_REQ_MSG  ... \r\n");

            if (FSB_IS_STANDBY_UP () == FSB_FALSE)
            {
                /* This is a special case, where bulk request msg from
                 * standby is coming before RM_STANDBY_UP. So no need to
                 * process the bulk request now. Bulk updates will be send
                 * on RM_STANDBY_UP event.
                 */
                FSB_BULK_REQ_RCVD () = FSB_TRUE;
                break;
            }
            FSB_BULK_REQ_RCVD () = FSB_FALSE;
            /* On recieving FSB_BULK_REQ_MSG, Bulk updation process should be
             * restarted.
             */
            FsbRedInitBulkUpdateFlags ();
            FSB_RED_BULK_UPD_STATUS () = FSB_BULK_UPD_IN_PROGRESS;
            FsbRedHandleBulkUpdateEvent ();
            break;

        case FSB_BULK_UPD_TAIL_MSG:

            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                             FSB_CONTROL_PATH_TRC,
                             "[STANDBY]: Received Bulk Update Tail Message \r\n");
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                             FSB_CONTROL_PATH_TRC,
                             "\r\nFsbRedHandleSyncUpMessage() - FSB_BULK_UPD_TAIL_MSG  ... \r\n");

            ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
            ProtoEvt.u4Error = RM_NONE;
            FsbRmHandleProtocolEvent (&ProtoEvt);
            break;

        default:
            break;
    }
}

/*****************************************************************************/
/* Function Name      : FsbRedSyncUpDefaultVlanFilter                        */
/*                                                                           */
/* Description        : This function sends the Default VLAN filter's        */
/*                      Hardware Filter Id to the Standby.                   */
/*                                                                           */
/* Input(s)           : pFsbContextInfo - Pointer to Context Info            */
/*                      u4ContextId     - Context Id                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpDefaultVlanFilter (tFsbContextInfo * pFsbContextInfo,
                               UINT4 u4ContextId)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT2               u2NoOfSyncMsg = 1;
    UINT2               u2BufSize = 0;

    if (pFsbContextInfo == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbRedSyncUpDefaultVlanFilter Failed ... \r\n");
        return FSB_FAILURE;
    }
    if ((FSB_IS_STANDBY_UP () == FSB_FALSE) ||
        (FSB_NODE_STATUS () != FSB_NODE_ACTIVE))
    {
        return FSB_SUCCESS;
    }

    u2BufSize =
        FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE +
        FSB_SYN_DEF_VLAN_MSG_SIZE;
    pMsg = FsbRedGetMsgBuffer (u2BufSize);
    if (pMsg == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbRedSyncUpDefaultVlanFilter - FsbRedGetMsgBuffer() Failed ... \r\n");
        return FSB_FAILURE;
    }
    /* Fill the message type and message length */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYN_DEF_VLAN_UPT_MSG);
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, FSB_SYN_DEF_VLAN_MSG_SIZE);

    /* Copy Number of Sync Message from the received TLV */
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2NoOfSyncMsg);

    /* Fill the Default VLAN Hw filter Id to be synced up */
    /* Fill the Context Id  - Index for ContextInfo Table */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, u4ContextId);

    /* Fill Hw filter Id for Local Filter of Vlan Discovery */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, pFsbContextInfo->FsbLocFilterEntry.
                       FsbVlanDiscovery.u4HwFilterId);

    /* Fill Hw filter Id for Remote Filter of Vlan Discovery */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, pFsbContextInfo->FsbRemFilterEntry.
                       FsbVlanDiscovery.u4HwFilterId);

    /* Fill Hw filter Id for Local Filter of Vlan Response Tagged */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, pFsbContextInfo->FsbLocFilterEntry.
                       FsbVlanResponseTagged.u4HwFilterId);

    /* Fill Hw filter Id for Remote Filter of Vlan Response Tagged */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, pFsbContextInfo->FsbRemFilterEntry.
                       FsbVlanResponseTagged.u4HwFilterId);
    if (FsbRedSendMsgToRm (pMsg, u2BufSize) == FSB_FAILURE)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbRedSyncUpDefaultVlanFilter - FsbRedSendMsgToRm () Failed ... \r\n");
        return FSB_FAILURE;
    }

    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedProcessDefaultVlanFilter                       */
/*                                                                           */
/* Description        : This function process the Default VLAN Hardware      */
/*                      Filter Id updation                                   */
/*                                                                           */
/* Input(s)           : pMesg     - Pointer to the sync up message.          */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS/FSB_FAILURE                              */
/*****************************************************************************/
INT4
FsbRedProcessDefaultVlanFilter (tRmMsg * pMesg, UINT4 *pu4Offset,
                                UINT2 *pu2Length)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4ContextId = 0;
    UINT2               u2NoOfSyncMsg = 0;
    UINT2               u2MesgSize = 0;

    ProtoEvt.u4AppId = RM_FSB_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (FSB_NODE_STATUS () != FSB_NODE_STANDBY)
    {
        /* Not a Standby node, hence don't process the received
         * sync message. */
        return FSB_SUCCESS;
    }

    /* Get the Message size encoded by the FSB module in Active node. */
    FSB_RM_GET_2_BYTE (pMesg, pu4Offset, u2MesgSize);
    *pu2Length = (UINT2) (*pu2Length - FSB_RED_LENGTH_SIZE);

    if (*pu2Length != u2MesgSize)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbRedProcessDefaultVlanFilter  - Invalid Message length ... \r\n");
        /* Data corruption, hence ignore the whole sync up message. */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        FsbRmHandleProtocolEvent (&ProtoEvt);
        return FSB_FAILURE;
    }
    /* Copy Number of Sync Message from the received TLV */
    FSB_RM_GET_2_BYTE (pMesg, pu4Offset, u2NoOfSyncMsg);
    while (u2NoOfSyncMsg != 0)
    {
        /* Copy the Context Id received in TLV */
        FSB_RM_GET_4_BYTE (pMesg, pu4Offset, u4ContextId);

        pFsbContextInfo = FsbCxtGetContextEntry (u4ContextId);
        if (pFsbContextInfo == NULL)
        {
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                             "\r\nFsbRedProcessDefaultVlanFilter  - FsbCxtGetContextEntry returns NULL ... \r\n");
            ProtoEvt.u4Error = RM_PROCESS_FAIL;
            FsbRmHandleProtocolEvent (&ProtoEvt);
            return FSB_FAILURE;
        }
        /* In Standby node, Local Filter Id is seen as Remote and Remote Filter 
         * Id is seen as Local. Hence, the entries received in pMesg from Active
         * node is swapped and stored in Local and Remote strucutres */
        /* Copy Local and Remote Filter Id for Vlan Discovery */
        FSB_RM_GET_4_BYTE (pMesg, pu4Offset,
                           (pFsbContextInfo->FsbRemFilterEntry.FsbVlanDiscovery.
                            u4HwFilterId));
        FSB_RM_GET_4_BYTE (pMesg, pu4Offset,
                           (pFsbContextInfo->FsbLocFilterEntry.FsbVlanDiscovery.
                            u4HwFilterId));
        /* Copy Local and Remote Filter Id for Vlan Response Tagged */
        FSB_RM_GET_4_BYTE (pMesg, pu4Offset,
                           (pFsbContextInfo->FsbRemFilterEntry.
                            FsbVlanResponseTagged.u4HwFilterId));
        FSB_RM_GET_4_BYTE (pMesg, pu4Offset,
                           (pFsbContextInfo->FsbLocFilterEntry.
                            FsbVlanResponseTagged.u4HwFilterId));
        u2NoOfSyncMsg--;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedSyncUpDefaultFilter                            */
/*                                                                           */
/* Description        : This function sends the Default filter's             */
/*                      Hardware Filter Id to the Standby.                   */
/*                                                                           */
/* Input(s)           : pFsbFipSnoopingEntry - Pointer to Fip Snooping       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpDefaultFilter (tFsbFipSnoopingEntry * pFsbFipSnoopingEntry)
{
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT4               u4SyncLengthOffset = 0;
    UINT4               u4NoOfFilterOffset = 0;
    UINT2               u2NoOfEntry = 1;
    UINT2               u2NoOfFilter = 0;
    UINT2               u2SynMsgLen = 0;
    UINT2               u2MaxBufSize = 0;

    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    if ((FSB_IS_STANDBY_UP () == FSB_FALSE) ||
        (FSB_NODE_STATUS () != FSB_NODE_ACTIVE))
    {
        return FSB_SUCCESS;
    }

    if (pFsbFipSnoopingEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbRedSyncUpDefaultFilter - pFsbFipSnoopingEntry is NULL ... \r\n");
        return FSB_FAILURE;
    }

    u2MaxBufSize =
        FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE +
        FSB_SYN_MAX_DEF_FILTER_MSG_SIZE;
    pMsg = FsbRedGetMsgBuffer (u2MaxBufSize);
    if (pMsg == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbRedSyncUpDefaultFilter : FsbRedGetMsgBuffer() returns failure  ... \r\n");
        return FSB_FAILURE;
    }
    /* Fill the message type */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYN_DEF_FILTER_UPT_MSG);
    /* Save the offset of Message Length in u4SyncLengthOffset and 
     * put zero as Message Length. At the end update the Message
     * Length value */
    u4SyncLengthOffset = u4Offset;
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SynMsgLen);

    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2NoOfEntry);

    /* Fill the Default Hw filter Id to be synced up */
    /* Fill the Context Id and Vlan Id - Index for FIPSnoopingEntry Table
     * and also increment the Message Length */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, pFsbFipSnoopingEntry->u4ContextId);
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, pFsbFipSnoopingEntry->u2VlanId);
    u4NoOfFilterOffset = u4Offset;
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2NoOfFilter);

    pFsbFilterEntry = (tFsbFilterEntry *)
        RBTreeGetFirst (pFsbFipSnoopingEntry->FsbFilterEntry);
    if (pFsbFilterEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbRedSyncUpDefaultFilter : pFsbFilterEntry returns failure  ... \r\n");
        RM_FREE (pMsg);
        return FSB_FAILURE;
    }
    do
    {
        FSB_MEMCPY (FsbFilterEntry.DstMac, pFsbFilterEntry->DstMac,
                    MAC_ADDR_LEN);
        /* Fill the Destination MAC Address and increment the Message Length */
        FSB_RM_PUT_N_BYTE (pMsg, pFsbFilterEntry->DstMac, &u4Offset,
                           MAC_ADDR_LEN);
        /* Fill the Hw Filter Id and increment the Message Length */
        FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, pFsbFilterEntry->u4HwFilterId);
        /* Fetch next Filter entry from RBTree */
        pFsbFilterEntry = (tFsbFilterEntry *)
            RBTreeGetNext (pFsbFipSnoopingEntry->FsbFilterEntry,
                           (tRBElem *) & FsbFilterEntry, NULL);
        u2NoOfFilter++;
    }
    while (pFsbFilterEntry != NULL);

    u2SynMsgLen =
        (UINT2) (u4Offset - (FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE));

    /* Fill the length field */
    FSB_RM_PUT_2_BYTE (pMsg, &u4SyncLengthOffset, u2SynMsgLen);
    FSB_RM_PUT_2_BYTE (pMsg, &u4NoOfFilterOffset, u2NoOfFilter);

    if (FsbRedSendMsgToRm (pMsg, (UINT2) u4Offset) == FSB_FAILURE)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbRedSyncUpDefaultFilter : FsbRedSendMsgToRm returns failure ... \r\n");
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedProcessDefaultFilter                           */
/*                                                                           */
/* Description        : This function process the Default Hardware           */
/*                      Filter Id updation                                   */
/*                                                                           */
/* Input(s)           : pMesg     - Pointer to the sync up message.          */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS/FSB_FAILURE                              */
/*****************************************************************************/
INT4
FsbRedProcessDefaultFilter (tRmMsg * pMesg, UINT4 *pu4Offset, UINT2 *pu2Length)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4ContextId = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2NoOfEntry = 0;
    UINT2               u2NoOfFilter = 0;
    UINT2               u2MesgSize = 0;

    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    ProtoEvt.u4AppId = RM_FSB_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (FSB_NODE_STATUS () != FSB_NODE_STANDBY)
    {
        /* Not a Standby node, hence don't process the received
         * sync message. */
        return FSB_SUCCESS;
    }

    /* Get the Message size encoded by the FSB module in Active node. */
    FSB_RM_GET_2_BYTE (pMesg, pu4Offset, u2MesgSize);
    *pu2Length = (UINT2) (*pu2Length - FSB_RED_LENGTH_SIZE);

    if (*pu2Length != u2MesgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        FsbRmHandleProtocolEvent (&ProtoEvt);
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbRedProcessDefaultFilter : Invalid Message Size  ... \r\n");
        return FSB_FAILURE;
    }
    FSB_RM_GET_2_BYTE (pMesg, pu4Offset, u2NoOfEntry);
    while (u2NoOfEntry != 0)
    {

        /* Copy the Context Id and Vlan Id (index for FIPSnoopingEntry Table) received in TLV */
        FSB_RM_GET_4_BYTE (pMesg, pu4Offset, u4ContextId);
        FSB_RM_GET_2_BYTE (pMesg, pu4Offset, u2VlanId);
        /* Copy the number of filters in the given Context and Vlan */
        FSB_RM_GET_2_BYTE (pMesg, pu4Offset, u2NoOfFilter);

        pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4ContextId, u2VlanId);
        if (pFsbFipSnoopingEntry == NULL)
        {
            ProtoEvt.u4Error = RM_PROCESS_FAIL;
            FsbRmHandleProtocolEvent (&ProtoEvt);
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                             "\r\nFsbRedProcessDefaultFilter : Invalid pFsbFipSnoopingEntry ... \r\n");

            return FSB_FAILURE;
        }

        while (u2NoOfFilter != 0)
        {
            FSB_RM_GET_N_BYTE (pMesg, (FsbFilterEntry.DstMac), pu4Offset,
                               MAC_ADDR_LEN);
            pFsbFilterEntry =
                RBTreeGet (pFsbFipSnoopingEntry->FsbFilterEntry,
                           (tRBElem *) & FsbFilterEntry);
            if (pFsbFilterEntry != NULL)
            {
                FSB_RM_GET_4_BYTE (pMesg, pu4Offset,
                                   (pFsbFilterEntry->u4HwFilterId));
            }
            u2NoOfFilter--;
        }
        u2NoOfEntry--;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedSyncUpSChannelFilterId                         */
/*                                                                           */
/* Description        : This function sends the S Channel Hardware Filter Id */
/*                      to the Standby.                                      */
/*                                                                           */
/* Input(s)           : pFsbFipSnoopingEntry - Pointer to Fip Snooping       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpSChannelFilterId (UINT4 u4IfIndex, UINT2 u2VlanId,
                              UINT4 u4HwFilterId)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT4               u4SyncLengthOffset = 0;
    UINT2               u2SynMsgLen = 0;
    UINT2               u2NoOfFilter = 1;
    UINT2               u2MaxBufSize = 0;

    if ((FSB_IS_STANDBY_UP () == FSB_FALSE) ||
        (FSB_NODE_STATUS () != FSB_NODE_ACTIVE))
    {
        return FSB_SUCCESS;
    }

    u2MaxBufSize =
        FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE +
        FSB_SYN_SCHAN_FILTER_MSG_SIZE;
    pMsg = FsbRedGetMsgBuffer (u2MaxBufSize);
    if (pMsg == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbRedSyncUpSChannelFilterId : FsbRedGetMsgBuffer() returns NULL ... \r\n");

        return FSB_FAILURE;
    }
    /* Fill the message type */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYN_SCHAN_FILTER_UPT_MSG);
    /* Save the offset of Message Length in u4SyncLengthOffset and
     * put zero as Message Length. At the end update the Message
     * Length value */
    u4SyncLengthOffset = u4Offset;
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SynMsgLen);

    /* Fill the S Channel Hw filter Id to be synced up */
    /* Fill the If Index and Vlan Id - Index for FsbSChannelFilterTable */
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2NoOfFilter);
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, u4IfIndex);
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2VlanId);

    /* Fill the Hw Filter Id */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, u4HwFilterId);

    u2SynMsgLen =
        (UINT2) (u4Offset - (FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE));
    /* Fill the length field */
    FSB_RM_PUT_2_BYTE (pMsg, &u4SyncLengthOffset, u2SynMsgLen);

    if (FsbRedSendMsgToRm (pMsg, (UINT2) u4Offset) == FSB_FAILURE)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbRedSyncUpSChannelFilterId : FsbRedSendMsgToRm() returns failure ... \r\n");
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedProcessSChannelFilterId                        */
/*                                                                           */
/* Description        : This function process the S Channel Hardware         */
/*                      Filter Id updation                                   */
/*                                                                           */
/* Input(s)           : pMesg     - Pointer to the sync up message.          */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS/FSB_FAILURE                              */
/*****************************************************************************/
INT4
FsbRedProcessSChannelFilterId (tRmMsg * pMesg, UINT4 *pu4Offset,
                               UINT2 *pu2Length)
{
    tFsbSChannelFilterEntry *pFsbSChannelFilterEntry = NULL;
    tFsbSChannelFilterEntry FsbSChannelFilterEntry;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2NoOfFilter = 0;
    UINT2               u2MesgSize = 0;

    FSB_MEMSET (&FsbSChannelFilterEntry, 0, sizeof (tFsbSChannelFilterEntry));

    ProtoEvt.u4AppId = RM_FSB_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (FSB_NODE_STATUS () != FSB_NODE_STANDBY)
    {
        /* Not a Standby node, hence don't process the received
         * sync message. */
        return FSB_SUCCESS;
    }

    /* Get the Message size encoded by the FSB module in Active node. */
    FSB_RM_GET_2_BYTE (pMesg, pu4Offset, u2MesgSize);
    *pu2Length = (UINT2) (*pu2Length - FSB_RED_LENGTH_SIZE);

    if (*pu2Length != u2MesgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        FsbRmHandleProtocolEvent (&ProtoEvt);
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbRedProcessSChannelFilterId  : Invalid length ... \r\n");

        return FSB_FAILURE;
    }
    FSB_RM_GET_2_BYTE (pMesg, pu4Offset, u2NoOfFilter);
    while (u2NoOfFilter != 0)
    {
        /* Copy the IfIndex and Vlan Id (index for FsbSChannelFilterTable) received in TLV */
        FSB_RM_GET_4_BYTE (pMesg, pu4Offset, FsbSChannelFilterEntry.u4IfIndex);
        FSB_RM_GET_2_BYTE (pMesg, pu4Offset, FsbSChannelFilterEntry.u2VlanId);

        pFsbSChannelFilterEntry = RBTreeGet (gFsbGlobals.FsbSChannelFilterTable,
                                             (tRBElem *) &
                                             FsbSChannelFilterEntry);
        if (pFsbSChannelFilterEntry != NULL)
        {
            FSB_RM_GET_4_BYTE (pMesg, pu4Offset,
                               (pFsbSChannelFilterEntry->u4HwFilterId));
        }
        u2NoOfFilter--;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedConstructFCFEntry                              */
/*                                                                           */
/* Description        : This function populate FCF Entry which is            */
/*                      to be sent to the standby                            */
/*                                                                           */
/* Input(s)           : pFsbFcfEntry - Pointer to FsbFcfEntry                */
/*                      pMsg         - Pointer to RM Message                 */
/*                      pu4Offset    - Pointer to Offset                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
VOID
FsbRedConstructFCFEntry (tFsbFcfEntry * pFsbFcfEntry, tRmMsg * pMsg,
                         UINT4 *pu4Offset)
{
    /* Fill the FSB FCF information to be synced up */
    /* The following are the index for the table */
    FSB_RM_PUT_2_BYTE (pMsg, pu4Offset, pFsbFcfEntry->u2VlanId);
    FSB_RM_PUT_4_BYTE (pMsg, pu4Offset, pFsbFcfEntry->u4FcfIfIndex);
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFcfEntry->FcfMacAddr, pu4Offset, MAC_ADDR_LEN);

    /* Fill the Context Id */
    FSB_RM_PUT_4_BYTE (pMsg, pu4Offset, pFsbFcfEntry->u4ContextId);

    /* Fill the addressing mode(FPMA/SPMA) */
    FSB_RM_PUT_1_BYTE (pMsg, pu4Offset, (pFsbFcfEntry->u1AddressingMode));

    /* Fill the Enode Login count */
    FSB_RM_PUT_4_BYTE (pMsg, pu4Offset, (pFsbFcfEntry->u4EnodeLoginCount));

    /* Fill the FCMAP value */
    FSB_RM_PUT_N_BYTE (pMsg, (pFsbFcfEntry->au1FcMap), pu4Offset,
                       FSB_FCMAP_LEN);

    /* Fill the Name ID recieved in TLV */
    FSB_RM_PUT_N_BYTE (pMsg, (pFsbFcfEntry->au1NameId), pu4Offset,
                       FSB_NAME_ID_LEN);

    /* Fill the fabric name recieved in TLV */
    FSB_RM_PUT_N_BYTE (pMsg, (pFsbFcfEntry->au1FabricName), pu4Offset,
                       FSB_FABRIC_NAME_LEN);

    /* Fill the Keep Alive Timer Value */
    FSB_RM_PUT_4_BYTE (pMsg, pu4Offset, pFsbFcfEntry->u4FcfKeepAliveFieldValue);
    return;
}

/*****************************************************************************/
/* Function Name      : FsbRedSyncUpFCFEntry                                 */
/*                                                                           */
/* Description        : This function sends the dynamically populated FCF    */
/*                      entry to the Standby.                                */
/*                                                                           */
/* Input(s)           : pFsbFcfEntry - Pointer to FsbFcfEntry                */
/*                      u1MsgType    - Message Type                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpFCFEntry (tFsbFcfEntry * pFsbFcfEntry, UINT1 u1MsgType)
{
    tRmMsg             *pMsg = NULL;
    UINT2               u2BufSize = 0;

    if ((FSB_IS_STANDBY_UP () == FSB_FALSE) ||
        (FSB_NODE_STATUS () != FSB_NODE_ACTIVE))
    {
        /* Standby node is not present, so no need to send the
         * sync up message. */
        return FSB_SUCCESS;
    }

    switch (u1MsgType)
    {
        case FSB_FCF_ADD:
            u2BufSize =
                FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE +
                FSB_SYN_FCF_ADD_MSG_SIZE;
            pMsg = FsbRedGetMsgBuffer (u2BufSize);
            if (pMsg != NULL)
            {
                FsbRedSyncUpAddFCFEntry (pFsbFcfEntry, pMsg);
            }
            break;
        case FSB_FCF_DEL:
            u2BufSize =
                FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE +
                FSB_SYN_FCF_DEL_MSG_SIZE;
            pMsg = FsbRedGetMsgBuffer (u2BufSize);
            if (pMsg != NULL)
            {
                FsbRedSyncUpDeleteFCFEntry (pFsbFcfEntry, pMsg);
            }
            break;
        default:
            break;
    }
    if (pMsg == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\n FsbRedSyncUpFCFEntry() - FsbRedGetMsgBuffer returns NULL \r\n");

        return FSB_FAILURE;
    }
    if (FsbRedSendMsgToRm (pMsg, u2BufSize) == FSB_FAILURE)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\n FsbRedSyncUpFCFEntry  : FsbRedSendMsgToRm() returns failure ... \r\n");
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedSyncUpAddFCFEntry                              */
/*                                                                           */
/* Description        : This function populate FCF Entry which is            */
/*                      to be sent to the standby                            */
/*                                                                           */
/* Input(s)           : pFsbFcfEntry - Pointer to FsbFcfEntry                */
/*                      pMsg         - Pointer to RM Message                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpAddFCFEntry (tFsbFcfEntry * pFsbFcfEntry, tRmMsg * pMsg)
{
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT2               u2NoOfSyncMsg = 1;
    UINT2               u2SyncMsgLen = 0;

    if (pFsbFcfEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\n FsbRedSyncUpAddFCFEntry : pFsbFcfEntry is NULL ... \r\n");
        return FSB_FAILURE;
    }

    u2SyncMsgLen = FSB_SYN_FCF_ADD_MSG_SIZE;

    /* Fill the message type and message length */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYN_FCF_ADD_MSG);
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);

    /* Fill the number of Syn-Up message */
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2NoOfSyncMsg);

    FsbRedConstructFCFEntry (pFsbFcfEntry, pMsg, &u4Offset);
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedProcessAddFCFEntry                             */
/*                                                                           */
/* Description        : This function process the received Add FCFEntry      */
/*                                                                           */
/* Input(s)           : pMesg     - Pointer to the sync up message.          */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS/FSB_FAILURE                              */
/*****************************************************************************/
INT4
FsbRedProcessAddFCFEntry (tRmMsg * pMesg, UINT4 *pu4Offset, UINT2 *pu2Length)
{
    tFSBPktInfo         FSBPktInfo;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4FcfKeepAliveTimerValue = 0;
    UINT4               u4ENodeLoginCount = 0;
    UINT2               u2NoOfSyncMsg = 0;
    UINT2               u2MesgSize = 0;

    FSB_MEMSET (&FSBPktInfo, 0, sizeof (tFSBPktInfo));

    ProtoEvt.u4AppId = RM_FSB_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (FSB_NODE_STATUS () != FSB_NODE_STANDBY)
    {
        /* Not a Standby node, hence don't process the received
         * sync message. */
        return FSB_SUCCESS;
    }

    /* Get the Message size encoded by the FSB module in Active node. */
    FSB_RM_GET_2_BYTE (pMesg, pu4Offset, u2MesgSize);
    *pu2Length = (UINT2) (*pu2Length - FSB_RED_LENGTH_SIZE);

    if (*pu2Length != u2MesgSize)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\n FsbRedProcessAddFCFEntry : Invalid Message length ... \r\n");

        /* Data corruption, hence ignore the whole sync up message. */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        FsbRmHandleProtocolEvent (&ProtoEvt);
        return FSB_FAILURE;
    }
    /* Copy Number of Sync Message from the received TLV */
    FSB_RM_GET_2_BYTE (pMesg, pu4Offset, u2NoOfSyncMsg);
    while (u2NoOfSyncMsg != 0)
    {
        /* Copy the VLAN id and FCF Interface index and FCF Mac Address from the 
         * recieved TLV */
        FSB_RM_GET_2_BYTE (pMesg, pu4Offset, (FSBPktInfo.u2VlanId));
        FSB_RM_GET_4_BYTE (pMesg, pu4Offset, (FSBPktInfo.u4IfIndex));
        FSB_RM_GET_N_BYTE (pMesg, (FSBPktInfo.SrcAddr), pu4Offset,
                           MAC_ADDR_LEN);

        /* Copy the Context Id */
        FSB_RM_GET_4_BYTE (pMesg, pu4Offset, (FSBPktInfo.u4ContextId));

        /* Copy the addressing mode(FPMA/SPMA) */
        FSB_RM_GET_1_BYTE (pMesg, pu4Offset, (FSBPktInfo.u1AddressMode));

        FSB_RM_GET_4_BYTE (pMesg, pu4Offset, u4ENodeLoginCount);

        /* Copy the FCMAP value */
        FSB_RM_GET_N_BYTE (pMesg,
                           (FSBPktInfo.FsbFipsPktFields.
                            FcfDiscoveryAdvertisementInfo.au1FcMap), pu4Offset,
                           FSB_FCMAP_LEN);

        /* Copy the Name ID recieved in TLV */
        FSB_RM_GET_N_BYTE (pMesg,
                           (FSBPktInfo.FsbFipsPktFields.
                            FcfDiscoveryAdvertisementInfo.au1NameId), pu4Offset,
                           FSB_NAME_ID_LEN);

        /* Copy the fabric name recieved in TLV */
        FSB_RM_GET_N_BYTE (pMesg,
                           (FSBPktInfo.FsbFipsPktFields.
                            FcfDiscoveryAdvertisementInfo.au1FabricName),
                           pu4Offset, FSB_FABRIC_NAME_LEN);

        FSB_RM_GET_4_BYTE (pMesg, pu4Offset, u4FcfKeepAliveTimerValue);
        FSBPktInfo.FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.
            u4FkaAdvPeriod = u4FcfKeepAliveTimerValue;

        if (FsbHandleFcfDiscovery (&FSBPktInfo) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                             FSB_SESSION_TRC,
                             "\r\n FsbRedProcessAddFCFEntry : FsbHandleFcfDiscovery() failed ... \r\n");
            return FSB_FAILURE;
        }
        u2NoOfSyncMsg--;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedSyncUpFCFFilterId                              */
/*                                                                           */
/* Description        : This function populate FCF Filter Id which is        */
/*                      to be sent to the standby                            */
/*                                                                           */
/* Input(s)           : pFsbFcfEntry - Pointer to FsbFcfEntry                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpFCFFilterId (tFsbFcfEntry * pFsbFcfEntry)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT2               u2SyncMsgLen = 0;
    UINT2               u2BufSize = 0;

    if ((FSB_IS_STANDBY_UP () == FSB_FALSE) ||
        (FSB_NODE_STATUS () != FSB_NODE_ACTIVE))
    {
        /* Standby node is not present, so no need to send the
         * sync up message. */
        return FSB_SUCCESS;
    }

    if (pFsbFcfEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\n FsbRedSyncUpFCFFilterId () - pFsbFcfEntry is NULL \r\n");
        return FSB_FAILURE;
    }

    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    u2BufSize =
        FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE +
        FSB_SYN_FCF_FILTER_UPT_MSG_SIZE;
    pMsg = FsbRedGetMsgBuffer (u2BufSize);
    if (pMsg == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\n FsbRedSyncUpFCFFilterId () - FsbRedGetMsgBuffer returns NULL \r\n");

        return FSB_FAILURE;
    }

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (pFsbFcfEntry->u4ContextId,
                                                   pFsbFcfEntry->u2VlanId);
    if (pFsbFipSnoopingEntry == NULL)
    {
        FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedSyncUpFCFFilterId: pFsbFipSnoopingEntry is NULL\r\n");
        RM_FREE (pMsg);
        return FSB_FAILURE;
    }
    FSB_MEMCPY ((FsbFilterEntry.DstMac), (pFsbFcfEntry->FcfMacAddr),
                MAC_ADDR_LEN);
    pFsbFilterEntry =
        RBTreeGet (pFsbFipSnoopingEntry->FsbFilterEntry, &FsbFilterEntry);

    if (pFsbFilterEntry == NULL)
    {
        FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedSyncUpFCFFilterId: "
                         "pFsbFilterEntry is NULL\r\n");
        RM_FREE (pMsg);
        return FSB_FAILURE;
    }
    u2SyncMsgLen = FSB_SYN_FCF_FILTER_UPT_MSG_SIZE;

    /* Fill the message type and message length */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYN_FCF_FILTER_UPT_MSG);
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);

    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, pFsbFcfEntry->u4ContextId);
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, pFsbFcfEntry->u2VlanId);
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFcfEntry->FcfMacAddr, &u4Offset, MAC_ADDR_LEN);
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, pFsbFilterEntry->u4HwFilterId);

    if (FsbRedSendMsgToRm (pMsg, (UINT2) u4Offset) == FSB_FAILURE)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\n FsbRedSyncUpFCFFilterId () - FsbRedSendMsgToRm failed  \r\n");
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedProcessFCFFilterId                             */
/*                                                                           */
/* Description        : This function process the received FCF Hw Filter Id  */
/*                                                                           */
/* Input(s)           : pMesg     - Pointer to the sync up message.          */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS/FSB_FAILURE                              */
/*****************************************************************************/
INT4
FsbRedProcessFCFFilterId (tRmMsg * pMesg, UINT4 *pu4Offset, UINT2 *pu2Length)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4ContextId = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2MesgSize = 0;

    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    ProtoEvt.u4AppId = RM_FSB_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (FSB_NODE_STATUS () != FSB_NODE_STANDBY)
    {
        /* Not a Standby node, hence don't process the received
         * sync message. */
        return FSB_SUCCESS;
    }

    /* Get the Message size encoded by the FSB module in Active node. */
    FSB_RM_GET_2_BYTE (pMesg, pu4Offset, u2MesgSize);
    *pu2Length = (UINT2) (*pu2Length - FSB_RED_LENGTH_SIZE);

    if (*pu2Length != u2MesgSize)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\n FsbRedProcessFCFFilterId () - Invalid Length \r\n");

        /* Data corruption, hence ignore the whole sync up message. */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        FsbRmHandleProtocolEvent (&ProtoEvt);
        return FSB_FAILURE;
    }
    /* Copy the VLAN id and FCF Interface index and FCF Mac Address from the 
     * recieved TLV */
    FSB_RM_GET_4_BYTE (pMesg, pu4Offset, u4ContextId);
    FSB_RM_GET_2_BYTE (pMesg, pu4Offset, u2VlanId);
    FSB_RM_GET_N_BYTE (pMesg, (FsbFilterEntry.DstMac), pu4Offset, MAC_ADDR_LEN);
    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4ContextId, u2VlanId);
    if (pFsbFipSnoopingEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\n FsbRedProcessFCFFilterId () - pFsbFipSnoopingEntry is NULL \r\n");

        return FSB_FAILURE;
    }

    pFsbFilterEntry = RBTreeGet (pFsbFipSnoopingEntry->FsbFilterEntry,
                                 (tRBElem *) & FsbFilterEntry);
    if (pFsbFilterEntry == NULL)
    {
        FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId, FSB_ERROR_LEVEL,
                         FSB_NONE,
                         "FsbRedProcessFCFFilterId: "
                         "pFsbFilterEntry is NULL\r\n");
        return FSB_FAILURE;
    }

    /* Copy the Context Id */
    FSB_RM_GET_4_BYTE (pMesg, pu4Offset, pFsbFilterEntry->u4HwFilterId);

    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedSyncUpDeleteFCFEntry                           */
/*                                                                           */
/* Description        : This function populate Delete FCF Entry  indication  */
/*                      to be sent to the standby                            */
/*                                                                           */
/* Input(s)           : pFsbFcfEntry - Pointer to FsbFcfEntry                */
/*                      pMsg         - Pointer to RM Message                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpDeleteFCFEntry (tFsbFcfEntry * pFsbFcfEntry, tRmMsg * pMsg)
{
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT2               u2SyncMsgLen = 0;

    if (pFsbFcfEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedSyncUpDeleteFCFEntry: pFsbFcfEntry is NULL \r\n ");
        return FSB_FAILURE;
    }

    u2SyncMsgLen = FSB_SYN_FCF_DEL_MSG_SIZE;

    /* Fill the message type and message length */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYN_FCF_DEL_MSG);
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);

    /* Fill the FSB FCF information to be synced up */
    /* The following are the index for the table */
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, pFsbFcfEntry->u2VlanId);
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, pFsbFcfEntry->u4FcfIfIndex);
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFcfEntry->FcfMacAddr, &u4Offset, MAC_ADDR_LEN);

    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedProcessDeleteFCFEntry                          */
/*                                                                           */
/* Description        : This function process the received Delete FCFEntry   */
/*                                                                           */
/* Input(s)           : pMesg     - Pointer to the sync up message.          */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS/ FSB_FAILURE                             */
/*****************************************************************************/
INT4
FsbRedProcessDeleteFCFEntry (tRmMsg * pMesg, UINT4 *pu4Offset, UINT2 *pu2Length)
{
    tFsbFcfEntry       *pFsbFcfEntry = NULL;
    tFsbFcfEntry        FsbFcfEntry;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2MesgSize = 0;

    FSB_MEMSET (&FsbFcfEntry, 0, sizeof (tFsbFcfEntry));

    ProtoEvt.u4AppId = RM_FSB_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (FSB_NODE_STATUS () != FSB_NODE_STANDBY)
    {
        /* Not a Standby node, hence don't process the received
         * sync message. */
        return FSB_SUCCESS;
    }

    /* Get the Message size encoded by the FSB module in Active node. */
    FSB_RM_GET_2_BYTE (pMesg, pu4Offset, u2MesgSize);
    *pu2Length = (UINT2) (*pu2Length - FSB_RED_LENGTH_SIZE);

    if (*pu2Length != u2MesgSize)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbRedProcessDeleteFCFEntry(): Invalid Length \r\n");

        /* Data corruption, hence ignore the whole sync up message. */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        FsbRmHandleProtocolEvent (&ProtoEvt);
        return FSB_FAILURE;
    }

    FSB_RM_GET_2_BYTE (pMesg, pu4Offset, (FsbFcfEntry.u2VlanId));
    FSB_RM_GET_4_BYTE (pMesg, pu4Offset, (FsbFcfEntry.u4FcfIfIndex));
    FSB_RM_GET_N_BYTE (pMesg, (FsbFcfEntry.FcfMacAddr), pu4Offset,
                       MAC_ADDR_LEN);

    pFsbFcfEntry = (tFsbFcfEntry *)
        RBTreeGet (gFsbGlobals.FsbFcfTable, (tRBElem *) & FsbFcfEntry);
    if (pFsbFcfEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedProcessDeleteFCFEntry: pFsbFcfEntry is NULL\r\n");
        return FSB_FAILURE;
    }

    if (FsbDeleteFIPSessEntryForFcf (pFsbFcfEntry) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedProcessDeleteFCFEntry: FsbDeleteFIPSessEntryForFcf failed "
                         "for FcFEntry with VlanId: %d, FcfIfIndex: %d and FcFMAC: "
                         "%02x:%02x:%02x:%02x:%02x:%02x \r\n",
                         pFsbFcfEntry->u2VlanId, pFsbFcfEntry->u4FcfIfIndex,
                         pFsbFcfEntry->FcfMacAddr[0],
                         pFsbFcfEntry->FcfMacAddr[1],
                         pFsbFcfEntry->FcfMacAddr[2],
                         pFsbFcfEntry->FcfMacAddr[3],
                         pFsbFcfEntry->FcfMacAddr[4],
                         pFsbFcfEntry->FcfMacAddr[5]);
        return FSB_FAILURE;
    }
    if (FsbDeleteFcfEntry (pFsbFcfEntry) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedProcessDeleteFCFEntry: FsbDeleteFcfEntry failed\r\n");
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : FsbRedSyncUpFIPSessEntry                             */
/*                                                                           */
/* Description        : This function sends the dynamically populated FIP    */
/*                      session entry to the Standby.                        */
/*                                                                           */
/* Input(s)           : pFsbFipSessEntry - Pointer to FsbFipSessEntry        */
/*                      pFSBPktInfo      - Pointer to FSBPktInfo             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpFIPSessEntry (tFsbFipSessEntry * pFsbFipSessEntry,
                          tFSBPktInfo * pFSBPktInfo)
{
    tRmMsg             *pMsg = NULL;
    UINT2               u2BufSize;
    if ((FSB_IS_STANDBY_UP () == FSB_FALSE) ||
        (FSB_NODE_STATUS () != FSB_NODE_ACTIVE))
    {
        /* Standby node is not present, so no need to send the
         * sync up message. */
        return FSB_SUCCESS;
    }

    switch (pFSBPktInfo->PacketType)
    {

        case FSB_FIP_SOLICIT_MCAST:
        case FSB_FIP_SOLICIT_UCAST:
            u2BufSize =
                FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE +
                FSB_SYNC_FIP_SOLICIT_MSG_SIZE;
            pMsg = FsbRedGetMsgBuffer (u2BufSize);
            if (pMsg != NULL)
            {
                FsbRedSyncUpFIPSolicit (pFsbFipSessEntry, pMsg, pFSBPktInfo);
            }
            break;

        case FSB_FIP_ADV_UCAST:
            u2BufSize =
                FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE +
                FSB_SYNC_FIP_UCAST_ADVT_MSG_SIZE;
            pMsg = FsbRedGetMsgBuffer (u2BufSize);
            if (pMsg != NULL)
            {
                FsbRedSyncUpFIPAdvtUcast (pFsbFipSessEntry, pMsg, pFSBPktInfo);
            }
            break;

        case FSB_FIP_FLOGI_REQUEST:
        case FSB_FIP_NPIV_FDISC_REQUEST:
            u2BufSize =
                FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE +
                FSB_SYNC_FIP_INST_MSG_SIZE;
            pMsg = FsbRedGetMsgBuffer (u2BufSize);
            if (pMsg != NULL)
            {
                FsbRedSyncUpFIPInstRequest (pFsbFipSessEntry, pMsg,
                                            pFSBPktInfo);
            }
            break;

        case FSB_FIP_FLOGI_ACCEPT:
        case FSB_FIP_NPIV_FDISC_ACCEPT:
            u2BufSize =
                FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE +
                FSB_SYNC_FIP_FLOGI_ACCEPT_MSG_SIZE;
            pMsg = FsbRedGetMsgBuffer (u2BufSize);
            if (pMsg != NULL)
            {
                FsbRedSyncUpFIPFLOGIAccept (pFsbFipSessEntry, pMsg,
                                            pFSBPktInfo);
            }
            break;

        case FSB_FIP_FLOGI_REJECT:
        case FSB_FIP_LOGO_ACCEPT:
            u2BufSize =
                FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE +
                FSB_SYNC_FIP_CLEAR_SESSION_MSG_SIZE;
            pMsg = FsbRedGetMsgBuffer (u2BufSize);
            if (pMsg != NULL)
            {
                FsbRedSyncUpClearFIPSession (pFsbFipSessEntry, pMsg,
                                             pFSBPktInfo->PacketType);
            }
            break;

        case FSB_FIP_CLEAR_LINK:
            u2BufSize =
                FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE +
                FSB_SYNC_MAX_VNMAC_MSG_SIZE;
            pMsg = FsbRedGetMsgBuffer (u2BufSize);
            if (pMsg != NULL)
            {
                FsbRedSyncUpClearVirtualLink (pFsbFipSessEntry, pMsg,
                                              pFSBPktInfo, &u2BufSize);
            }
            break;
        case FSB_FIP_NPIV_FDISC_REJECT:
            u2BufSize =
                FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE +
                FSB_SYNC_FIP_FDISC_REJECT_MSG_SIZE;
            pMsg = FsbRedGetMsgBuffer (u2BufSize);
            if (pMsg != NULL)
            {
                FsbRedSyncUpFIPFDISCReject (pFsbFipSessEntry, pMsg,
                                            pFSBPktInfo);
            }
            break;
        case FSB_FIP_LOGO_REQUEST:
            u2BufSize =
                FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE +
                FSB_SYNC_FIP_LOGO_REQUEST_MSG_SIZE;
            pMsg = FsbRedGetMsgBuffer (u2BufSize);
            if (pMsg != NULL)
            {
                FsbRedSyncUpFIPLOGORequest (pFsbFipSessEntry, pMsg,
                                            pFSBPktInfo);
            }
            break;

        case FSB_FIP_LOGO_REJECT:
            u2BufSize =
                FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE +
                FSB_SYNC_FIP_LOGO_REJECT_MSG_SIZE;
            pMsg = FsbRedGetMsgBuffer (u2BufSize);
            if (pMsg != NULL)
            {
                FsbRedSyncUpFIPLOGOReject (pFsbFipSessEntry, pMsg, pFSBPktInfo);
            }
            break;

        case FSB_FIP_ENODE_KEEP_ALIVE:
        case FSB_FIP_VNPORT_KEEP_ALIVE:
        case FSB_FIP_INITIAL_STATE:
        case FSB_FIP_VLAN_DISCOVERY:
        case FSB_FIP_VLAN_NOTIFICATION:
        case FSB_FIP_ADV_MCAST:
            return FSB_SUCCESS;

        default:
            return FSB_SUCCESS;
    }

    if (pMsg == NULL)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_DEBUGGING_LEVEL,
                         FSB_BUFFER_TRC,
                         "FsbRedSyncUpFIPSessEntry: RM memory is not available.\r\n");
        return FSB_FAILURE;
    }

    if (FsbRedSendMsgToRm (pMsg, u2BufSize) == FSB_FAILURE)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedSendMsgToRm: Sending message to RM failed.\r\n");
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedSyncUpFIPSolicit                               */
/*                                                                           */
/* Description        : This function populates the FIP solicit message      */
/*                      to be sent to the standby                            */
/*                                                                           */
/* Input(s)           : pFsbFipSessEntry - Pointer to FsbFipSessEntry        */
/*                      pMsg             - Pointer to RM Message             */
/*                      pFSBPktInfo      - Pointer to FSBPktInfo             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpFIPSolicit (tFsbFipSessEntry * pFsbFipSessEntry, tRmMsg * pMsg,
                        tFSBPktInfo * pFSBPktInfo)
{
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;
    tMacAddr            ZeroMacAddr;
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT2               u2SyncMsgLen = 0;

    FSB_MEMSET (ZeroMacAddr, 0, MAC_ADDR_LEN);
    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    if (pFsbFipSessEntry == NULL)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedSyncUpFIPSolicit: pFsbFipSessEntry is NULL\r\n");
        RM_FREE (pMsg);
        return FSB_FAILURE;
    }

    if (pFSBPktInfo->PacketType == FSB_FIP_SOLICIT_MCAST)
    {
        FSB_MEMCPY ((FsbFilterEntry.SrcMac), ZeroMacAddr, MAC_ADDR_LEN);
    }
    else
    {
        FSB_MEMCPY ((FsbFilterEntry.SrcMac), pFSBPktInfo->DestAddr,
                    MAC_ADDR_LEN);
    }
    FSB_MEMCPY ((FsbFilterEntry.DstMac), pFSBPktInfo->SrcAddr, MAC_ADDR_LEN);
    FsbFilterEntry.u2OpcodeFilterOffsetValue = FSB_DISCOVERY;
    FsbFilterEntry.u2SubOpcodeFilterOffsetValue = FSB_DISCOVERY_ADVERTISEMENT;
    pFsbFilterEntry = (tFsbFilterEntry *)
        RBTreeGet (pFsbFipSessEntry->FsbSessFilterEntry,
                   (tRBElem *) & FsbFilterEntry);
    if (pFsbFilterEntry == NULL)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_DEBUGGING_LEVEL,
                         FSB_OS_RESOURCE_TRC,
                         "FsbRedSyncUpFIPSolicit:pFsbFilterEntry is NULL\r\n");
        RM_FREE (pMsg);
        return FSB_FAILURE;
    }

    u2SyncMsgLen = FSB_SYNC_FIP_SOLICIT_MSG_SIZE;

    /* Fill the message type. */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYNC_FIP_SOLICIT_MSG);
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);

    /* Fill the FIP Sess information to be synced up. */
    /* The following are the index for the table */
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, pFsbFipSessEntry->u2VlanId);
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, pFsbFipSessEntry->u4ENodeIfIndex);
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFipSessEntry->ENodeMacAddr, &u4Offset,
                       MAC_ADDR_LEN);
    /* Fill and send the FCF Mac Address, context id and Packet Type */
    FSB_RM_PUT_N_BYTE (pMsg, pFSBPktInfo->DestAddr, &u4Offset, MAC_ADDR_LEN);
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, pFsbFipSessEntry->u4ContextId);
    FSB_RM_PUT_1_BYTE (pMsg, &u4Offset, pFSBPktInfo->PacketType);

    /* Fill the FIP Sess Filter Entry to be synced up. */
    /* The following are the index for the table */
    FSB_RM_PUT_N_BYTE (pMsg, (pFsbFilterEntry->SrcMac), &u4Offset,
                       MAC_ADDR_LEN);
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset,
                       (pFsbFilterEntry->u2OpcodeFilterOffsetValue));
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset,
                       (pFsbFilterEntry->u2SubOpcodeFilterOffsetValue));
    /* Fill and send the Sw Filter Id, Hw Filter Id */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, (pFsbFilterEntry->u4FilterId));
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, (pFsbFilterEntry->u4HwFilterId));

    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedProcessFIPSolicit                              */
/*                                                                           */
/* Description        : This function handles the FIP solicit message        */
/*                      recieved in the standby                              */
/*                                                                           */
/* Input(s)           : pMsg      - Pointer to the sync up message.          */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedProcessFIPSolicit (tRmMsg * pMsg, UINT4 *pu4Offset, UINT2 *pu2Length)
{
    tFsbFipSessEntry   *pFsbFipSessEntry = NULL;
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFipSessEntry    FsbFipSessEntry;
    tFsbFilterEntry     FsbFilterEntry;
    tFSBPktInfo         FSBPktInfo;
    UINT2               u2MesgSize = 0;
    UINT1               u1PacketType = 0;

    FSB_MEMSET (&FsbFipSessEntry, 0, sizeof (tFsbFipSessEntry));
    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));
    FSB_MEMSET (&FSBPktInfo, 0, sizeof (tFSBPktInfo));

    /* Get the size of the syncup message */
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, u2MesgSize);
    *pu2Length = (UINT2) (*pu2Length - FSB_RED_LENGTH_SIZE);

    if (*pu2Length != u2MesgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedProcessFIPSolicit: Data corruption \r\n");
        return FSB_FAILURE;
    }
    /* Get the index of FIP Session Entry Table filled in message */
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, (FSBPktInfo.u2VlanId));
    FSB_RM_GET_4_BYTE (pMsg, pu4Offset, (FSBPktInfo.u4IfIndex));
    FSB_RM_GET_N_BYTE (pMsg, (FSBPktInfo.SrcAddr), pu4Offset, MAC_ADDR_LEN);
    FSB_RM_GET_N_BYTE (pMsg, (FSBPktInfo.DestAddr), pu4Offset, MAC_ADDR_LEN);
    FSB_RM_GET_4_BYTE (pMsg, pu4Offset, (FSBPktInfo.u4ContextId));
    FSB_RM_GET_1_BYTE (pMsg, pu4Offset, u1PacketType);

    FSBPktInfo.PacketType = u1PacketType;
    if (FsbHandleFIPSession (&FSBPktInfo) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                         FSB_DEBUGGING_LEVEL, FSB_SESSION_TRC,
                         "FsbRedProcessFIPSolicit: "
                         "FsbHandleFIPSession failed\r\n");
    }

    /* Get the index of FIP Session Entry */
    FsbFipSessEntry.u2VlanId = FSBPktInfo.u2VlanId;
    FsbFipSessEntry.u4ENodeIfIndex = FSBPktInfo.u4IfIndex;
    FSB_MEMCPY ((FsbFipSessEntry.ENodeMacAddr), (FSBPktInfo.SrcAddr),
                MAC_ADDR_LEN);

    /* Get pointer to FIP Session Entry */
    pFsbFipSessEntry = (tFsbFipSessEntry *)
        RBTreeGet (gFsbGlobals.FsbFipSessTable, (tRBElem *) & FsbFipSessEntry);
    if (pFsbFipSessEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                         FSB_DEBUGGING_LEVEL, FSB_OS_RESOURCE_TRC,
                         "FsbRedProcessFIPSolicit: "
                         "pFsbFipSessEntry is NULL\r\n");
        return FSB_FAILURE;
    }
    /* Get the index of FIP Session Filter Entry */
    FSB_MEMCPY ((FsbFilterEntry.DstMac), pFsbFipSessEntry->ENodeMacAddr,
                MAC_ADDR_LEN);
    FSB_RM_GET_N_BYTE (pMsg, (FsbFilterEntry.SrcMac), pu4Offset, MAC_ADDR_LEN);
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset,
                       (FsbFilterEntry.u2OpcodeFilterOffsetValue));
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset,
                       (FsbFilterEntry.u2SubOpcodeFilterOffsetValue));
    /* Get pointer to FIP Session Filter Entry */
    pFsbFilterEntry = (tFsbFilterEntry *)
        RBTreeGet (pFsbFipSessEntry->FsbSessFilterEntry,
                   (tRBElem *) & FsbFilterEntry);
    if (pFsbFilterEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                         FSB_DEBUGGING_LEVEL, FSB_OS_RESOURCE_TRC,
                         "FsbRedProcessFIPSolicit: "
                         "pFsbFilterEntry is NULL\r\n");
        return FSB_FAILURE;
    }
    /* Update Sw and Hw Filter Id in FIP Session Filter Entry */
    FSB_RM_GET_4_BYTE (pMsg, pu4Offset, (pFsbFilterEntry->u4FilterId));
    FSB_RM_GET_4_BYTE (pMsg, pu4Offset, (pFsbFilterEntry->u4HwFilterId));

    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedSyncUpFIPAdvtUcast                             */
/*                                                                           */
/* Description        : This function populates the FIP Advt message         */
/*                      to be sent to the standby                            */
/*                                                                           */
/* Input(s)           : pFsbFipSessEntry - Pointer to FsbFipSessEntry        */
/*                      pMsg             - Pointer to RM message             */
/*                      pFSBPktInfo      - Pointer to FSBPktInfo             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpFIPAdvtUcast (tFsbFipSessEntry * pFsbFipSessEntry, tRmMsg * pMsg,
                          tFSBPktInfo * pFSBPktInfo)
{
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT2               u2SyncMsgLen = 0;

    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    if (pFsbFipSessEntry == NULL)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedSyncUpFIPAdvtUcast: pFsbFipSessEntry is NULL\r\n");
        return FSB_FAILURE;
    }

    FSB_MEMCPY ((FsbFilterEntry.SrcMac), pFSBPktInfo->DestAddr, MAC_ADDR_LEN);
    FSB_MEMCPY ((FsbFilterEntry.DstMac), pFSBPktInfo->SrcAddr, MAC_ADDR_LEN);
    FsbFilterEntry.u2OpcodeFilterOffsetValue = FSB_VL_INSTANTIATION;
    FsbFilterEntry.u2SubOpcodeFilterOffsetValue = FSB_VL_INSTANTIATION_REQUEST;
    pFsbFilterEntry = (tFsbFilterEntry *)
        RBTreeGet (pFsbFipSessEntry->FsbSessFilterEntry,
                   (tRBElem *) & FsbFilterEntry);
    if (pFsbFilterEntry == NULL)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                         FSB_DEBUGGING_LEVEL, FSB_OS_RESOURCE_TRC,
                         "FsbRedSyncUpFIPAdvtUcast: pFsbFilterEntry is NULL\r\n");
        RM_FREE (pMsg);
        return FSB_FAILURE;
    }

    u2SyncMsgLen = FSB_SYNC_FIP_UCAST_ADVT_MSG_SIZE;

    /* Fill the message type. */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYNC_FIP_ADVT_MSG);
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);

    /* Fill the FIP Sess information to be synced up. */
    /* The following are the index for the table */
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, pFsbFipSessEntry->u2VlanId);
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, pFsbFipSessEntry->u4ENodeIfIndex);
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFipSessEntry->ENodeMacAddr, &u4Offset,
                       MAC_ADDR_LEN);
    /* Fill the FCF If index */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, pFsbFipSessEntry->u4FcfIfIndex);
    /* Fill the FCMAP */
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFipSessEntry->au1FcMap, &u4Offset,
                       FSB_FCMAP_LEN);
    /* Fill the Name Identifier */
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFipSessEntry->au1NameId, &u4Offset,
                       FSB_NAME_ID_LEN);
    /* Fill the Fabric Name */
    FSB_RM_PUT_N_BYTE (pMsg,
                       pFSBPktInfo->FsbFipsPktFields.
                       FcfDiscoveryAdvertisementInfo.au1FabricName, &u4Offset,
                       FSB_FABRIC_NAME_LEN);
    /* Fill the keep alive field value */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset,
                       pFSBPktInfo->FsbFipsPktFields.
                       FcfDiscoveryAdvertisementInfo.u4FkaAdvPeriod);
    /* Fill the Packter Type */
    FSB_RM_PUT_1_BYTE (pMsg, &u4Offset, pFSBPktInfo->PacketType);

    /* Fill the FIP Sess Filter Entry to be synced up. */
    /* The following are the index for the table */
    FSB_RM_PUT_N_BYTE (pMsg, (pFsbFilterEntry->DstMac), &u4Offset,
                       MAC_ADDR_LEN);
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset,
                       (pFsbFilterEntry->u2OpcodeFilterOffsetValue));
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset,
                       (pFsbFilterEntry->u2SubOpcodeFilterOffsetValue));
    /* Fill and send the Sw Filter Id, Hw Filter Id */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, (pFsbFilterEntry->u4FilterId));
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, (pFsbFilterEntry->u4HwFilterId));

    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedProcessFIPAdvtUcast                            */
/*                                                                           */
/* Description        : This function handles the FIP Advertisment unicast   */
/*                      message recieved in the standby                      */
/*                                                                           */
/* Input(s)           : pMsg      - Pointer to the sync up message.          */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedProcessFIPAdvtUcast (tRmMsg * pMsg, UINT4 *pu4Offset, UINT2 *pu2Length)
{
    tFsbFipSessEntry   *pFsbFipSessEntry = NULL;
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFipSessEntry    FsbFipSessEntry;
    tFsbFilterEntry     FsbFilterEntry;
    tFSBPktInfo         FSBPktInfo;
    UINT4               u4ENodeIfIndex = 0;
    UINT4               u4FcfKeepAliveFieldValue = 0;
    UINT2               u2MesgSize = 0;
    UINT1               u1PacketType = 0;

    FSB_MEMSET (&FsbFipSessEntry, 0, sizeof (tFsbFipSessEntry));
    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));
    FSB_MEMSET (&FSBPktInfo, 0, sizeof (tFSBPktInfo));

    /* Get the size of the syncup message */
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, u2MesgSize);
    *pu2Length = (UINT2) (*pu2Length - FSB_RED_LENGTH_SIZE);

    if (*pu2Length != u2MesgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedProcessFIPAdvtUcast: Data corruption \r\n");
        return FSB_FAILURE;
    }
    /* Get the Index of the table (VlanId, ENode IfIndex and ENode Mac Address) */
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, (FSBPktInfo.u2VlanId));
    FSB_RM_GET_4_BYTE (pMsg, pu4Offset, u4ENodeIfIndex);
    FSB_RM_GET_N_BYTE (pMsg, (FSBPktInfo.DestAddr), pu4Offset, MAC_ADDR_LEN);

    /* Get the FCF if index */
    FSB_RM_GET_4_BYTE (pMsg, pu4Offset, (FSBPktInfo.u4IfIndex));
    /* Get the FCMAP */
    FSB_RM_GET_N_BYTE (pMsg,
                       (FSBPktInfo.FsbFipsPktFields.
                        FcfDiscoveryAdvertisementInfo.au1FcMap), pu4Offset,
                       FSB_FCMAP_LEN);
    /* Get the Name Identifier */
    FSB_RM_GET_N_BYTE (pMsg,
                       (FSBPktInfo.FsbFipsPktFields.
                        FcfDiscoveryAdvertisementInfo.au1NameId), pu4Offset,
                       FSB_NAME_ID_LEN);
    /* Get the Fabric Name */
    FSB_RM_GET_N_BYTE (pMsg,
                       (FSBPktInfo.FsbFipsPktFields.
                        FcfDiscoveryAdvertisementInfo.au1FabricName), pu4Offset,
                       FSB_FABRIC_NAME_LEN);

    /* Get the FCF keep alive timer field value */
    FSB_RM_GET_4_BYTE (pMsg, pu4Offset, u4FcfKeepAliveFieldValue);
    FSBPktInfo.FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.u4FkaAdvPeriod =
        u4FcfKeepAliveFieldValue;

    /* Get the Packet Type */
    FSB_RM_GET_1_BYTE (pMsg, pu4Offset, u1PacketType);
    FSBPktInfo.PacketType = u1PacketType;
    /* Get FCF Mac Address */
    FSB_RM_GET_N_BYTE (pMsg, (FSBPktInfo.SrcAddr), pu4Offset, MAC_ADDR_LEN);
    FSBPktInfo.FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.u2DbitFlag = 1;

    if (FsbHandleFIPSession (&FSBPktInfo) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                         FSB_DEBUGGING_LEVEL, FSB_SESSION_TRC,
                         "FsbRedProcessFIPAdvtUcast: "
                         "FsbHandleFIPSession failed\r\n");
        return FSB_FAILURE;
    }
    /* Get the index of FIP Session Entry */
    FsbFipSessEntry.u2VlanId = FSBPktInfo.u2VlanId;
    FsbFipSessEntry.u4ENodeIfIndex = u4ENodeIfIndex;
    FSB_MEMCPY ((FsbFipSessEntry.ENodeMacAddr), (FSBPktInfo.DestAddr),
                MAC_ADDR_LEN);

    /* Get pointer to FIP Session Entry */
    pFsbFipSessEntry = (tFsbFipSessEntry *)
        RBTreeGet (gFsbGlobals.FsbFipSessTable, (tRBElem *) & FsbFipSessEntry);
    if (pFsbFipSessEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                         FSB_DEBUGGING_LEVEL, FSB_OS_RESOURCE_TRC,
                         "FsbRedProcessFIPAdvtUcast: "
                         "pFsbFipSessEntry is NULL\r\n");
        return FSB_FAILURE;
    }
    /* Get the index of FIP Session Filter Entry */
    FSB_MEMCPY ((FsbFilterEntry.SrcMac), pFsbFipSessEntry->ENodeMacAddr,
                MAC_ADDR_LEN);
    FSB_MEMCPY ((FsbFilterEntry.DstMac), (FSBPktInfo.SrcAddr), MAC_ADDR_LEN);
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset,
                       (FsbFilterEntry.u2OpcodeFilterOffsetValue));
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset,
                       (FsbFilterEntry.u2SubOpcodeFilterOffsetValue));
    /* Get pointer to FIP Session Filter Entry */
    pFsbFilterEntry = (tFsbFilterEntry *)
        RBTreeGet (pFsbFipSessEntry->FsbSessFilterEntry,
                   (tRBElem *) & FsbFilterEntry);
    if (pFsbFilterEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                         FSB_DEBUGGING_LEVEL, FSB_OS_RESOURCE_TRC,
                         "FsbRedProcessFIPAdvtUcast: "
                         "pFsbFilterEntry is NULL\r\n");
        return FSB_FAILURE;
    }
    /* Update Sw and Hw Filter Id in FIP Session Filter Entry */
    FSB_RM_GET_4_BYTE (pMsg, pu4Offset, (pFsbFilterEntry->u4FilterId));
    FSB_RM_GET_4_BYTE (pMsg, pu4Offset, (pFsbFilterEntry->u4HwFilterId));

    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedSyncUpFIPInstRequest                           */
/*                                                                           */
/* Description        : This function populates the FIP Instantiation        */
/*                      message to be sent to the standby.                   */
/*                                                                           */
/* Input(s)           : pFsbFipSessEntry - Pointer to FsbFipSessEntry        */
/*                      pMsg             - Pointer to RM message             */
/*                      pFSBPktInfo      - Pointer to FSBPktInfo             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpFIPInstRequest (tFsbFipSessEntry * pFsbFipSessEntry, tRmMsg * pMsg,
                            tFSBPktInfo * pFSBPktInfo)
{
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT2               u2SyncMsgLen = 0;

    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    if (pFsbFipSessEntry == NULL)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedSyncUpFIPInstRequest: pFsbFipSessEntry is NULL\r\n");
        RM_FREE (pMsg);
        return FSB_FAILURE;
    }

    FSB_MEMCPY ((FsbFilterEntry.SrcMac), pFSBPktInfo->DestAddr, MAC_ADDR_LEN);
    FSB_MEMCPY ((FsbFilterEntry.DstMac), pFSBPktInfo->SrcAddr, MAC_ADDR_LEN);
    FsbFilterEntry.u2OpcodeFilterOffsetValue = FSB_VL_INSTANTIATION;
    FsbFilterEntry.u2SubOpcodeFilterOffsetValue = FSB_VL_INSTANTIATION_REPLY;
    pFsbFilterEntry = (tFsbFilterEntry *)
        RBTreeGet (pFsbFipSessEntry->FsbSessFilterEntry,
                   (tRBElem *) & FsbFilterEntry);
    if (pFsbFilterEntry == NULL)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                         FSB_DEBUGGING_LEVEL, FSB_OS_RESOURCE_TRC,
                         "FsbRedSyncUpFIPInstRequest: pFsbFilterEntry is NULL\r\n");
        RM_FREE (pMsg);
        return FSB_FAILURE;
    }

    u2SyncMsgLen = FSB_SYNC_FIP_INST_MSG_SIZE;

    /* Fill the message type. */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYNC_FIP_INST_MSG);
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);

    /* Fill the FIP Sess information to be synced up. */
    /* The following are the index for the table */
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, pFsbFipSessEntry->u2VlanId);
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, pFsbFipSessEntry->u4ENodeIfIndex);
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFipSessEntry->ENodeMacAddr, &u4Offset,
                       MAC_ADDR_LEN);

    /* Fill the Packet Type and FCF MAC address */
    FSB_RM_PUT_1_BYTE (pMsg, &u4Offset, pFSBPktInfo->PacketType);
    FSB_RM_PUT_N_BYTE (pMsg, pFSBPktInfo->DestAddr, &u4Offset, MAC_ADDR_LEN);

    /* Fill the FIP Sess Filter Entry to be synced up. */
    /* The following are the index for the table */
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset,
                       (pFsbFilterEntry->u2OpcodeFilterOffsetValue));
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset,
                       (pFsbFilterEntry->u2SubOpcodeFilterOffsetValue));
    /* Fill and send the Sw Filter Id, Hw Filter Id */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, (pFsbFilterEntry->u4FilterId));
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, (pFsbFilterEntry->u4HwFilterId));

    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedProcessFIPInstRequest                          */
/*                                                                           */
/* Description        : This function handles the FIP Instantiation(FLOGI/   */
/*                      FDISC message recieved in the standby                */
/*                                                                           */
/* Input(s)           : pMsg      - Pointer to the sync up message.          */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedProcessFIPInstRequest (tRmMsg * pMsg, UINT4 *pu4Offset, UINT2 *pu2Length)
{
    tFsbFipSessEntry   *pFsbFipSessEntry = NULL;
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFipSessEntry    FsbFipSessEntry;
    tFsbFilterEntry     FsbFilterEntry;
    tFSBPktInfo         FSBPktInfo;
    UINT2               u2MesgSize = 0;
    UINT1               u1PacketType = 0;

    FSB_MEMSET (&FSBPktInfo, 0, sizeof (tFSBPktInfo));

    /* Get the size of the syncup message */
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, u2MesgSize);
    *pu2Length = (UINT2) (*pu2Length - FSB_RED_LENGTH_SIZE);

    if (*pu2Length != u2MesgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedProcessFIPInstRequest: Data corruption \r\n");
        return FSB_FAILURE;
    }
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, (FSBPktInfo.u2VlanId));
    FSB_RM_GET_4_BYTE (pMsg, pu4Offset, (FSBPktInfo.u4IfIndex));
    FSB_RM_GET_N_BYTE (pMsg, (FSBPktInfo.SrcAddr), pu4Offset, MAC_ADDR_LEN);
    /* Get the Packet Type */
    FSB_RM_GET_1_BYTE (pMsg, pu4Offset, (u1PacketType));
    FSBPktInfo.PacketType = u1PacketType;
    /* Get the FCF MAC address */
    FSB_RM_GET_N_BYTE (pMsg, (FSBPktInfo.DestAddr), pu4Offset, MAC_ADDR_LEN);

    if (FsbHandleFIPSession (&FSBPktInfo) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                         FSB_DEBUGGING_LEVEL, FSB_SESSION_TRC,
                         "FsbRedProcessFIPInstRequest"
                         "FsbHandleFIPSession failed\r\n");
        return FSB_FAILURE;
    }
    /* Get the index of FIP Session Entry */
    FsbFipSessEntry.u2VlanId = FSBPktInfo.u2VlanId;
    FsbFipSessEntry.u4ENodeIfIndex = FSBPktInfo.u4IfIndex;
    FSB_MEMCPY ((FsbFipSessEntry.ENodeMacAddr), (FSBPktInfo.SrcAddr),
                MAC_ADDR_LEN);

    /* Get pointer to FIP Session Entry */
    pFsbFipSessEntry = (tFsbFipSessEntry *)
        RBTreeGet (gFsbGlobals.FsbFipSessTable, (tRBElem *) & FsbFipSessEntry);
    if (pFsbFipSessEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                         FSB_DEBUGGING_LEVEL, FSB_OS_RESOURCE_TRC,
                         "FsbRedProcessFIPInstRequest: pFsbFipSessEntry is NULL\r\n");
        return FSB_FAILURE;
    }
    /* Get the index of FIP Session Filter Entry */
    FSB_MEMCPY ((FsbFilterEntry.DstMac), pFsbFipSessEntry->ENodeMacAddr,
                MAC_ADDR_LEN);
    FSB_MEMCPY ((FsbFilterEntry.SrcMac), (FSBPktInfo.DestAddr), MAC_ADDR_LEN);
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset,
                       (FsbFilterEntry.u2OpcodeFilterOffsetValue));
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset,
                       (FsbFilterEntry.u2SubOpcodeFilterOffsetValue));
    /* Get pointer to FIP Session Filter Entry */
    pFsbFilterEntry = (tFsbFilterEntry *)
        RBTreeGet (pFsbFipSessEntry->FsbSessFilterEntry,
                   (tRBElem *) & FsbFilterEntry);
    if (pFsbFilterEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                         FSB_DEBUGGING_LEVEL, FSB_OS_RESOURCE_TRC,
                         "FsbRedProcessFIPInstRequest: pFsbFilterEntry is NULL\r\n");
        return FSB_FAILURE;
    }
    /* Update Sw and Hw Filter Id in FIP Session Filter Entry */
    FSB_RM_GET_4_BYTE (pMsg, pu4Offset, (pFsbFilterEntry->u4FilterId));
    FSB_RM_GET_4_BYTE (pMsg, pu4Offset, (pFsbFilterEntry->u4HwFilterId));

    return FSB_SUCCESS;
}

/*******************************************************************************/
/* Function Name      : FsbRedGetFilterEntry                                   */
/*                                                                             */
/* Description        : This function construct FIP Session Filter Entry       */
/*                      to be sent to the standby.                             */
/*                                                                             */
/* Input(s)           : pFsbFipSessFCoEEntry - Pointer to FsbFipSessFCoEEntry  */
/*                      u1MsgType            - Message Type                    */
/*                                                                             */
/* Output(s)          : None                                                   */
/*                                                                             */
/* Global Variables                                                            */
/* Referred           : None.                                                  */
/*                                                                             */
/* Global Variables                                                            */
/* Modified           : None.                                                  */
/*                                                                             */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                             */
/*******************************************************************************/
tFsbFilterEntry    *
FsbRedGetFilterEntry (tFsbFipSessFCoEEntry * pFsbFipSessFCoEEntry,
                      UINT1 u1MsgType)
{
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;

    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    if (pFsbFipSessFCoEEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedGetFilterEntry: pFsbFipSessFCoEEntry is NULL\r\n");
        return NULL;
    }

    switch (u1MsgType)
    {
        case FSB_FCOE_TRAFFIC_FROM_FCF:
            FSB_MEMCPY ((FsbFilterEntry.SrcMac),
                        pFsbFipSessFCoEEntry->FCoEMacAddr, MAC_ADDR_LEN);
            FSB_MEMCPY ((FsbFilterEntry.DstMac),
                        pFsbFipSessFCoEEntry->FcfMacAddr, MAC_ADDR_LEN);
            FsbFilterEntry.u2OpcodeFilterOffsetValue = 0;
            FsbFilterEntry.u2SubOpcodeFilterOffsetValue = 0;
            break;
        case FSB_FCOE_TRAFFIC_FROM_ENODE:
            FSB_MEMCPY ((FsbFilterEntry.SrcMac),
                        pFsbFipSessFCoEEntry->FcfMacAddr, MAC_ADDR_LEN);
            FSB_MEMCPY ((FsbFilterEntry.DstMac),
                        pFsbFipSessFCoEEntry->FCoEMacAddr, MAC_ADDR_LEN);
            FsbFilterEntry.u2OpcodeFilterOffsetValue = 0;
            FsbFilterEntry.u2SubOpcodeFilterOffsetValue = 0;
            break;
        case FSB_VN_PORT_KEEP_ALIVE:
            FSB_MEMCPY ((FsbFilterEntry.SrcMac),
                        pFsbFipSessFCoEEntry->FCoEMacAddr, MAC_ADDR_LEN);
            FSB_MEMCPY ((FsbFilterEntry.DstMac),
                        pFsbFipSessFCoEEntry->FcfMacAddr, MAC_ADDR_LEN);
            FsbFilterEntry.u2OpcodeFilterOffsetValue = FSB_KEEP_ALIVE;
            FsbFilterEntry.u2SubOpcodeFilterOffsetValue = FSB_FIP_KEEP_ALIVE;
            break;
        case FSB_ENODE_KEEP_ALIVE:
            FSB_MEMCPY ((FsbFilterEntry.SrcMac),
                        pFsbFipSessFCoEEntry->ENodeMacAddr, MAC_ADDR_LEN);
            FSB_MEMCPY ((FsbFilterEntry.DstMac),
                        pFsbFipSessFCoEEntry->FcfMacAddr, MAC_ADDR_LEN);
            FsbFilterEntry.u2OpcodeFilterOffsetValue = FSB_KEEP_ALIVE;
            FsbFilterEntry.u2SubOpcodeFilterOffsetValue = FSB_FIP_KEEP_ALIVE;
            break;
        case FSB_FIP_CLEAR_VIRTUAL_LINK:
            FSB_MEMCPY ((FsbFilterEntry.SrcMac),
                        pFsbFipSessFCoEEntry->FcfMacAddr, MAC_ADDR_LEN);
            FSB_MEMCPY ((FsbFilterEntry.DstMac),
                        pFsbFipSessFCoEEntry->ENodeMacAddr, MAC_ADDR_LEN);
            FsbFilterEntry.u2OpcodeFilterOffsetValue = FSB_KEEP_ALIVE;
            FsbFilterEntry.u2SubOpcodeFilterOffsetValue =
                FSB_FIP_CLEAR_VIRTUAL_LINKS;
            break;
        default:
            break;
    }
    pFsbFilterEntry = (tFsbFilterEntry *)
        RBTreeGet (pFsbFipSessFCoEEntry->FsbSessFCoEFilterEntry,
                   (tRBElem *) & FsbFilterEntry);
    return pFsbFilterEntry;
}

/*****************************************************************************/
/* Function Name      : FsbRedSyncUpFIPFLOGIAccept                           */
/*                                                                           */
/* Description        : This function populates the FIP FLOGI Accept         */
/*                      message to be sent to the standby.                   */
/*                                                                           */
/* Input(s)           : pFsbFipSessEntry - Pointer to FsbFipSessEntry        */
/*                      pMsg             - Pointer to RM message             */
/*                      pFSBPktInfo      - Pointer to FSBPktInfo             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpFIPFLOGIAccept (tFsbFipSessEntry * pFsbFipSessEntry, tRmMsg * pMsg,
                            tFSBPktInfo * pFSBPktInfo)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT4               u4FilterOffset = 0;
    UINT2               u2SyncMsgLen = 0;
    UINT1               u1NoOFilter = 0;
    UINT1               u1MsgType = 0;

    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    if (pFsbFipSessEntry == NULL)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedSyncUpFIPFLOGIAccept: pFsbFipSessEntry is NULL\r\n");
        RM_FREE (pMsg);
        return FSB_FAILURE;
    }

    u2SyncMsgLen = FSB_SYNC_FIP_FLOGI_ACCEPT_MSG_SIZE;

    /* Fill the message type. */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYNC_FIP_FLOGI_ACCEPT_MSG);
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);

    /* Fill the fsb sess information to be synced up. */
    /* The following are the index for the table */
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, pFsbFipSessEntry->u2VlanId);
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, pFsbFipSessEntry->u4ENodeIfIndex);
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFipSessEntry->ENodeMacAddr, &u4Offset,
                       MAC_ADDR_LEN);
    FSB_RM_PUT_N_BYTE (pMsg, pFSBPktInfo->SrcAddr, &u4Offset, MAC_ADDR_LEN);
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, pFsbFipSessEntry->u4FcfIfIndex);

    /* Fill the FCoE MAC address */
    FSB_RM_PUT_N_BYTE (pMsg,
                       (pFSBPktInfo->FsbFipsPktFields.FIPLogiInfo.FCoEMacAddr),
                       &u4Offset, MAC_ADDR_LEN);
    FSB_RM_PUT_1_BYTE (pMsg, &u4Offset, pFSBPktInfo->PacketType);
    FSB_RM_PUT_1_BYTE (pMsg, &u4Offset,
                       pFsbFipSessEntry->u1HouseKeepingTimerFlag);
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset,
                       pFsbFipSessEntry->u2HouseKeepingTimePeriod);
    u4FilterOffset = u4Offset;
    FSB_RM_PUT_1_BYTE (pMsg, &u4Offset, u1NoOFilter);

    pFsbFipSessFCoEEntry =
        FsbGetFIPSessionFCoEEntry (pFsbFipSessEntry->u2VlanId,
                                   pFsbFipSessEntry->u4ENodeIfIndex,
                                   pFsbFipSessEntry->ENodeMacAddr,
                                   pFSBPktInfo->SrcAddr,
                                   pFSBPktInfo->FsbFipsPktFields.FIPLogiInfo.
                                   FCoEMacAddr);
    if (pFsbFipSessFCoEEntry == NULL)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedSyncUpFIPFLOGIAccept: pFsbFipSessFCoEEntry is NULL\r\n");
        RM_FREE (pMsg);
        return FSB_FAILURE;
    }

    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset,
                       pFsbFipSessFCoEEntry->u2HouseKeepingTimePeriod);

    for (u1MsgType = FSB_FCOE_TRAFFIC_FROM_FCF;
         u1MsgType <= FSB_FIP_CLEAR_VIRTUAL_LINK; u1MsgType++)
    {
        pFsbFilterEntry =
            FsbRedGetFilterEntry (pFsbFipSessFCoEEntry, u1MsgType);
        if (pFsbFilterEntry == NULL)
        {
            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbRedSyncUpFIPFLOGIAccept: pFsbFilterEntry is NULL\r\n");
            RM_FREE (pMsg);
            return FSB_FAILURE;
        }
        /* Fill the FIP Sess Filter Entry to be synced up. */
        /* The following are the index for the table */
        FSB_RM_PUT_N_BYTE (pMsg, (pFsbFilterEntry->SrcMac), &u4Offset,
                           MAC_ADDR_LEN);
        FSB_RM_PUT_N_BYTE (pMsg, (pFsbFilterEntry->DstMac), &u4Offset,
                           MAC_ADDR_LEN);
        FSB_RM_PUT_2_BYTE (pMsg, &u4Offset,
                           (pFsbFilterEntry->u2OpcodeFilterOffsetValue));
        FSB_RM_PUT_2_BYTE (pMsg, &u4Offset,
                           (pFsbFilterEntry->u2SubOpcodeFilterOffsetValue));
        /* Fill and send the Sw Filter Id, Hw Filter Id */
        FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, (pFsbFilterEntry->u4HwFilterId));
        FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, (pFsbFilterEntry->u4FilterId));
        u1NoOFilter++;
    }
    FSB_RM_PUT_1_BYTE (pMsg, &u4FilterOffset, u1NoOFilter);
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedProcessFIPFLOGIAccept                          */
/*                                                                           */
/* Description        : This function handles the FIP FLOGI Accept           */
/*                      message recieved in the standby                      */
/*                                                                           */
/* Input(s)           : pMsg      - Pointer to the sync up message.          */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedProcessFIPFLOGIAccept (tRmMsg * pMsg, UINT4 *pu4Offset, UINT2 *pu2Length)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFipSessEntry    FsbFipSessEntry;
    tFsbFilterEntry     FsbFilterEntry;
    tFSBPktInfo         FSBPktInfo;
    UINT4               u4ENodeIfIndex = 0;
    UINT2               u2MesgSize = 0;
    UINT2               u2VNPortHouseKeepingTimePeriod = 0;
    UINT2               u2ENodeHouseKeepingTimePeriod = 0;
    UINT1               u1HouseKeepingTimerFlag = 0;
    UINT1               u1NoOfFilter = 0;
    UINT1               u1PacketType = 0;

    FSB_MEMSET (&FsbFipSessEntry, 0, sizeof (tFsbFipSessEntry));
    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));
    FSB_MEMSET (&FSBPktInfo, 0, sizeof (tFSBPktInfo));

    /* Get the size of the syncup message */
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, u2MesgSize);
    *pu2Length = (UINT2) (*pu2Length - FSB_RED_LENGTH_SIZE);

    if (*pu2Length != u2MesgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedProcessFIPFLOGIAccept: Data corruption \r\n");
        return FSB_FAILURE;
    }

    /* Get the index of FIP Session Entry Table filled in message */
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, (FSBPktInfo.u2VlanId));
    FSB_RM_GET_4_BYTE (pMsg, pu4Offset, u4ENodeIfIndex);
    FSB_RM_GET_N_BYTE (pMsg, (FSBPktInfo.DestAddr), pu4Offset, MAC_ADDR_LEN);
    FSB_RM_GET_N_BYTE (pMsg, (FSBPktInfo.SrcAddr), pu4Offset, MAC_ADDR_LEN);
    FSB_RM_GET_4_BYTE (pMsg, pu4Offset, (FSBPktInfo.u4IfIndex));
    FSB_RM_GET_N_BYTE (pMsg,
                       (FSBPktInfo.FsbFipsPktFields.FIPLogiInfo.FCoEMacAddr),
                       pu4Offset, MAC_ADDR_LEN);
    FSB_RM_GET_1_BYTE (pMsg, pu4Offset, (u1PacketType));
    FSBPktInfo.PacketType = u1PacketType;
    FSB_RM_GET_1_BYTE (pMsg, pu4Offset, u1HouseKeepingTimerFlag);
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, u2ENodeHouseKeepingTimePeriod);
    FSB_RM_GET_1_BYTE (pMsg, pu4Offset, u1NoOfFilter);
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, u2VNPortHouseKeepingTimePeriod);

    if (FsbHandleFIPSession (&FSBPktInfo) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                         FSB_DEBUGGING_LEVEL, FSB_SESSION_TRC,
                         "FsbRedProcessFIPFLOGIAccept: "
                         "FsbHandleFIPSession failed\r\n");
        return FSB_FAILURE;
    }

    pFsbFipSessFCoEEntry = FsbGetFIPSessionFCoEEntry (FSBPktInfo.u2VlanId,
                                                      u4ENodeIfIndex,
                                                      (FSBPktInfo.DestAddr),
                                                      (FSBPktInfo.SrcAddr),
                                                      FSBPktInfo.
                                                      FsbFipsPktFields.
                                                      FIPLogiInfo.FCoEMacAddr);

    if (pFsbFipSessFCoEEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedProcessFIPFLOGIAccept: pFsbFipSessFCoEEntry is NULL\r\n");
        return FSB_FAILURE;
    }
    pFsbFipSessFCoEEntry->pFsbFipSessEntry->u1HouseKeepingTimerFlag = FSB_FALSE;
    pFsbFipSessFCoEEntry->pFsbFipSessEntry->u2HouseKeepingTimePeriod =
        u2ENodeHouseKeepingTimePeriod;
    pFsbFipSessFCoEEntry->u2HouseKeepingTimePeriod =
        u2VNPortHouseKeepingTimePeriod;
    pFsbFipSessFCoEEntry->u1HouseKeepingTimerFlag = FSB_FALSE;
    pFsbFipSessFCoEEntry->u1SyncTimerStatus = u1HouseKeepingTimerFlag;

    while (u1NoOfFilter != 0)
    {
        /* Get the index of FIP Session Filter Entry */
        FSB_RM_GET_N_BYTE (pMsg, (FsbFilterEntry.SrcMac), pu4Offset,
                           MAC_ADDR_LEN);
        FSB_RM_GET_N_BYTE (pMsg, (FsbFilterEntry.DstMac), pu4Offset,
                           MAC_ADDR_LEN);
        FSB_RM_GET_2_BYTE (pMsg, pu4Offset,
                           (FsbFilterEntry.u2OpcodeFilterOffsetValue));
        FSB_RM_GET_2_BYTE (pMsg, pu4Offset,
                           (FsbFilterEntry.u2SubOpcodeFilterOffsetValue));
        /* Get pointer to FIP Session Filter Entry */
        pFsbFilterEntry = (tFsbFilterEntry *)
            RBTreeGet (pFsbFipSessFCoEEntry->FsbSessFCoEFilterEntry,
                       (tRBElem *) & FsbFilterEntry);
        if (pFsbFilterEntry == NULL)
        {
            FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                             FSB_DEBUGGING_LEVEL, FSB_OS_RESOURCE_TRC,
                             "FsbRedProcessFIPFLOGIAccept: pFsbFilterEntry is NULL\r\n");
            return FSB_FAILURE;
        }
        /* Update Sw and Hw Filter Id in FIP Session Filter Entry */
        FSB_RM_GET_4_BYTE (pMsg, pu4Offset, (pFsbFilterEntry->u4HwFilterId));
        FSB_RM_GET_4_BYTE (pMsg, pu4Offset, (pFsbFilterEntry->u4FilterId));
        u1NoOfFilter--;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedSyncUpClearFIPSession                          */
/*                                                                           */
/* Description        : This function populates the FIP Clear Session        */
/*                      indication to be sent to the standby                 */
/*                                                                           */
/* Input(s)           : pFsbFipSessEntry - Pointer to pFsbFipSessEntry       */
/*                      pMsg             - Ponter to RM Message              */
/*                      u1PacketType     - Specifies the packet type         */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpClearFIPSession (tFsbFipSessEntry * pFsbFipSessEntry, tRmMsg * pMsg,
                             UINT1 u1PacketType)
{
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT2               u2SyncMsgLen = 0;

    if (pFsbFipSessEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedSyncUpClearFIPSession: pFsbFipSessEntry is NULL\r\n");
        RM_FREE (pMsg);
        return FSB_FAILURE;
    }

    u2SyncMsgLen = FSB_SYNC_FIP_CLEAR_SESSION_MSG_SIZE;

    /* Fill the message type. */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYNC_FIP_CLEAR_SESSION_MSG);
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);

    /* Fill the fsb sess information to be synced up. */
    /* The following are the index for the table */
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, pFsbFipSessEntry->u2VlanId);
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFipSessEntry->ENodeMacAddr, &u4Offset,
                       MAC_ADDR_LEN);

    FSB_RM_PUT_1_BYTE (pMsg, &u4Offset, u1PacketType);

    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedProcessFIPClearSession                         */
/*                                                                           */
/* Description        : This function handles the FIP Clear Session          */
/*                      message recieved in the standby                      */
/*                                                                           */
/* Input(s)           : pMsg      - Pointer to the sync up message.          */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedProcessFIPClearSession (tRmMsg * pMsg, UINT4 *pu4Offset, UINT2 *pu2Length)
{
    tFSBPktInfo         FSBPktInfo;
    UINT2               u2MesgSize = 0;
    UINT1               u1PacketType = 0;

    FSB_MEMSET (&FSBPktInfo, 0, sizeof (tFSBPktInfo));

    /* Get the size of the syncup message */
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, u2MesgSize);
    *pu2Length = (UINT2) (*pu2Length - FSB_RED_LENGTH_SIZE);

    if (*pu2Length != u2MesgSize)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedProcessFIPClearSession: Invalid Length \r\n");
        /* Data corruption, hence ignore the whole sync up message. */
        return FSB_FAILURE;
    }
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, (FSBPktInfo.u2VlanId));
    FSB_RM_GET_N_BYTE (pMsg, (FSBPktInfo.DestAddr), pu4Offset, MAC_ADDR_LEN);

    FSB_RM_GET_1_BYTE (pMsg, pu4Offset, (u1PacketType));
    FSBPktInfo.PacketType = u1PacketType;

    if (FsbHandleFIPSession (&FSBPktInfo) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                         FSB_DEBUGGING_LEVEL, FSB_SESSION_TRC,
                         "FsbRedProcessFIPClearSession: "
                         "FsbHandleFIPSession failed\r\n");
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedSyncUpClearVirtualLink                         */
/*                                                                           */
/* Description        : This function populates the FIP Clear Virtual Link   */
/*                      indication to be sent to the standby                 */
/*                                                                           */
/* Input(s)           : pFsbFipSessEntry - Pointer to FsbFipSessEntry        */
/*                      pMsg             - Ponter to RM Message              */
/*                      pFSBPktInfo      - Pointer to FSBPktInfo             */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpClearVirtualLink (tFsbFipSessEntry * pFsbFipSessEntry,
                              tRmMsg * pMsg, tFSBPktInfo * pFSBPktInfo,
                              UINT2 *pu2BufSize)
{
    tMacAddr            ZeroMacAddr = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT4               u4SyncMsgLenOffset = 0;
    UINT4               u4VNMacCountOffset = 0;
    UINT2               u2VNMacCount = 0;
    UINT2               u2SyncMsgLen = 0;

    if (pFsbFipSessEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedSyncUpClearVirtualLink: pFsbFipSessEntry is NULL\r\n");
        RM_FREE (pMsg);
        return FSB_FAILURE;
    }

    /* Fill the message type. */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYNC_FIP_CLEAR_LINK_MSG);
    u4SyncMsgLenOffset = u4Offset;
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);

    /* Fill the Clear Virutal Link information to be synced up. */
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, pFsbFipSessEntry->u2VlanId);
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFipSessEntry->ENodeMacAddr, &u4Offset,
                       MAC_ADDR_LEN);
    FSB_RM_PUT_N_BYTE (pMsg, pFSBPktInfo->SrcAddr, &u4Offset, MAC_ADDR_LEN);
    u4VNMacCountOffset = u4Offset;
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2VNMacCount);

    /* pFSBPktInfo->FsbFipsPktFields.FIPClearLink.pVNMacAddr = NULL means CVL received from
     * FCF is ENode CVL. i.e, No VN_Port MAC in the Descriptor field. Hence no need to
     * fill VNPort MAC in TLV */
    if (pFSBPktInfo->FsbFipsPktFields.FIPClearLink.pVNMacAddr != NULL)
    {
        while ((FSB_MEMCMP
                (pFSBPktInfo->FsbFipsPktFields.FIPClearLink.
                 pVNMacAddr[u2VNMacCount], ZeroMacAddr, MAC_ADDR_LEN) != 0)
               && (u2VNMacCount <= MAX_FSB_FCOE_MAC_ADDR))
        {
            FSB_RM_PUT_N_BYTE (pMsg, pFSBPktInfo->FsbFipsPktFields.FIPClearLink.
                               pVNMacAddr[u2VNMacCount], &u4Offset,
                               MAC_ADDR_LEN);
            u2VNMacCount++;
        }
    }

    FSB_RM_PUT_2_BYTE (pMsg, &u4VNMacCountOffset, u2VNMacCount);
    FSB_RM_PUT_1_BYTE (pMsg, &u4Offset, pFSBPktInfo->PacketType);

    u2SyncMsgLen =
        (UINT2) (u4Offset - (FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE));
    FSB_RM_PUT_2_BYTE (pMsg, &u4SyncMsgLenOffset, u2SyncMsgLen);
    *pu2BufSize = (UINT2) u4Offset;
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedProcessClearVirtualLink                        */
/*                                                                           */
/* Description        : This function handles the FIP Clear Virtual Link     */
/*                      message recieved in the standby                      */
/*                                                                           */
/* Input(s)           : pMsg      - Pointer to the sync up message.          */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedProcessClearVirtualLink (tRmMsg * pMsg, UINT4 *pu4Offset,
                               UINT2 *pu2Length)
{
    tFSBPktInfo         FSBPktInfo;
    UINT2               u2VNMacCount = 0;
    UINT2               u2VNMacIndex = 0;
    UINT2               u2MesgSize = 0;
    UINT1               u1PacketType = 0;

    FSB_MEMSET (&FSBPktInfo, 0, sizeof (tFSBPktInfo));

    /* Get the size of the syncup message */
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, u2MesgSize);
    *pu2Length = (UINT2) (*pu2Length - FSB_RED_LENGTH_SIZE);

    if (*pu2Length != u2MesgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbRedProcessClearVirtualLink : Invalid Message Length ... \r\n");
        return FSB_FAILURE;
    }
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, (FSBPktInfo.u2VlanId));
    FSB_RM_GET_N_BYTE (pMsg, (FSBPktInfo.DestAddr), pu4Offset, MAC_ADDR_LEN);
    FSB_RM_GET_N_BYTE (pMsg, (FSBPktInfo.SrcAddr), pu4Offset, MAC_ADDR_LEN);
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, u2VNMacCount);

    /* u2VNMacCount = 0, means that ENode CVL packet is received from FCF. 
     * i.e, No VN_Port MAC in the Descriptor field. Hence no need to allocate
     * memory for pVNMacAddr in FSBPktInfo */
    if (u2VNMacCount != 0)
    {
        FSBPktInfo.FsbFipsPktFields.FIPClearLink.pVNMacAddr = (tMacAddr *)
            MemAllocMemBlk (FSB_FCOE_MAC_ADDR_MEMPOOL_ID);

        if (FSBPktInfo.FsbFipsPktFields.FIPClearLink.pVNMacAddr == NULL)
        {
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                             FSB_DEBUGGING_LEVEL, FSB_OS_RESOURCE_TRC,
                             "FsbRedProcessClearVirtualLink: Memalloc failed "
                             "for FSB_FCOE_MAC_ADDR_MEMPOOL_ID\r\n");
            return FSB_FAILURE;
        }

        FSB_MEMSET (FSBPktInfo.FsbFipsPktFields.FIPClearLink.pVNMacAddr, 0,
                    MAX_FSB_FCOE_MAC_SIZE);
    }

    while (u2VNMacCount != 0)
    {
        FSB_RM_GET_N_BYTE (pMsg, (FSBPktInfo.FsbFipsPktFields.FIPClearLink.
                                  pVNMacAddr[u2VNMacIndex]), pu4Offset,
                           MAC_ADDR_LEN);
        u2VNMacIndex++;
        u2VNMacCount--;
    }
    FSB_RM_GET_1_BYTE (pMsg, pu4Offset, (u1PacketType));
    FSBPktInfo.PacketType = u1PacketType;

    if (FsbHandleFIPSession (&FSBPktInfo) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                         FSB_DEBUGGING_LEVEL, FSB_SESSION_TRC,
                         "FsbRedProcessFIPClearSession: "
                         "FsbHandleFIPSession failed\r\n");
        if (FSBPktInfo.FsbFipsPktFields.FIPClearLink.pVNMacAddr != NULL)
        {
            MemReleaseMemBlock (FSB_FCOE_MAC_ADDR_MEMPOOL_ID,
                                (UINT1 *) FSBPktInfo.FsbFipsPktFields.
                                FIPClearLink.pVNMacAddr);
        }
        return FSB_FAILURE;
    }
    if (FSBPktInfo.FsbFipsPktFields.FIPClearLink.pVNMacAddr != NULL)
    {
        MemReleaseMemBlock (FSB_FCOE_MAC_ADDR_MEMPOOL_ID,
                            (UINT1 *) FSBPktInfo.FsbFipsPktFields.FIPClearLink.
                            pVNMacAddr);
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedSyncUpFIPSessionDelete                         */
/*                                                                           */
/* Description        : This function populates the Delete FIP Session       */
/*                      indication to be sent to the standby                 */
/*                                                                           */
/* Input(s)           : pFsbFipSessEntry - Pointer to pFsbFipSessEntry       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpFIPSessionDelete (tFsbFipSessEntry * pFsbFipSessEntry)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT2               u2BufSize = 0;

    if ((FSB_IS_STANDBY_UP () == FSB_FALSE) ||
        (FSB_NODE_STATUS () != FSB_NODE_ACTIVE))
    {
        /* Standby node is not present, so no need to send the
         * sync up message. */
        return FSB_SUCCESS;
    }

    if (pFsbFipSessEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedSyncUpFIPSessionDelete: pFsbFipSessEntry is NULL\r\n");
        return FSB_FAILURE;
    }

    u2BufSize =
        FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE +
        FSB_SYNC_FIP_SESS_DEL_MSG_SIZE;
    pMsg = FsbRedGetMsgBuffer (u2BufSize);
    if (pMsg == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                         "\r\n FsbRedSyncUpFIPSessionDelete() - FsbRedGetMsgBuffer returns NULL \r\n");
        return FSB_FAILURE;
    }
    /* Fill the message type. */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYNC_FIP_SESS_DEL_MSG);
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, FSB_SYNC_FIP_SESS_DEL_MSG_SIZE);

    /* Fill the fsb sess information to be synced up. */
    /* The following are the index for the table */
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, pFsbFipSessEntry->u2VlanId);
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, pFsbFipSessEntry->u4ENodeIfIndex);
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFipSessEntry->ENodeMacAddr, &u4Offset,
                       MAC_ADDR_LEN);

    if (FsbRedSendMsgToRm (pMsg, u2BufSize) == FSB_FAILURE)
    {
        FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedSendMsgToRm: Sending message to RM failed.\n");
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedProcessFipSessDelete                           */
/*                                                                           */
/* Description        : This function handles the Delete FIP Clear           */
/*                      message recieved in the standby                      */
/*                                                                           */
/* Input(s)           : pMsg      - Pointer to the sync up message.          */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedProcessFipSessDelete (tRmMsg * pMsg, UINT4 *pu4Offset, UINT2 *pu2Length)
{
    tFsbFipSessEntry   *pFsbFipSessEntry = NULL;
    tMacAddr            ENodeMacAddr;
    UINT4               u4ENodeIfIndex = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2MesgSize = 0;

    FSB_MEMSET (&ENodeMacAddr, 0, MAC_ADDR_LEN);

    /* Get the size of the syncup message */
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, u2MesgSize);
    *pu2Length = (UINT2) (*pu2Length - FSB_RED_LENGTH_SIZE);

    if (*pu2Length != u2MesgSize)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbRedProcessFipSessDelete: Invalid Message Length ... \r\n");
        /* Data corruption, hence ignore the whole sync up message. */
        return FSB_FAILURE;
    }
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, u2VlanId);
    FSB_RM_GET_4_BYTE (pMsg, pu4Offset, u4ENodeIfIndex);
    FSB_RM_GET_N_BYTE (pMsg, ENodeMacAddr, pu4Offset, MAC_ADDR_LEN);

    pFsbFipSessEntry = FsbGetFIPSessionEntry (u2VlanId, u4ENodeIfIndex,
                                              ENodeMacAddr);

    if (pFsbFipSessEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedProcessFipSessDelete: "
                         "pFsbFipSessFCoEEntry is NULL\r\n");
        return FSB_FAILURE;
    }

    if (FsbClearFIPSession (pFsbFipSessEntry) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                         FSB_DEBUGGING_LEVEL, FSB_SESSION_TRC,
                         "FsbRedProcessFipSessDelete: "
                         "FsbClearFIPSession failed\r\n");
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/******************************************************************************/
/* Function Name      : FsbRedSyncUpDeleteFCoEEntry                           */
/*                                                                            */
/* Description        : This function populate Delete FCoE Entry  indication  */
/*                      to be sent to the standby                             */
/*                                                                            */
/* Input(s)           : pFsbFipSessFCoEEntry - Pointer to FsbFipSessFCoEEntry */
/*                      pMsg                 - Pointer to RM Message          */
/*                                                                            */
/* Output(s)          : None                                                  */
/*                                                                            */
/* Global Variables                                                           */
/* Referred           : None.                                                 */
/*                                                                            */
/* Global Variables                                                           */
/* Modified           : None.                                                 */
/*                                                                            */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                            */
/******************************************************************************/
INT4
FsbRedSyncUpDeleteFCoEEntry (tFsbFipSessFCoEEntry * pFsbFipSessFCoEEntry)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT2               u2SyncMsgLen = 0;
    UINT2               u2BufSize = 0;

    if ((FSB_IS_STANDBY_UP () == FSB_FALSE) ||
        (FSB_NODE_STATUS () != FSB_NODE_ACTIVE))
    {
        /* Standby node is not present, so no need to send the
         * sync up message. */
        return FSB_SUCCESS;
    }

    if (pFsbFipSessFCoEEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedSyncUpDeleteFCoEEntry: "
                         "pFsbFipSessFCoEEntry is NULL\r\n");
        return FSB_FAILURE;
    }

    u2BufSize =
        FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE +
        FSB_SYN_FCOE_DEL_MSG_SIZE;
    pMsg = FsbRedGetMsgBuffer (u2BufSize);
    if (pMsg == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_BUFFER_TRC,
                         "\r\n FsbRedSyncUpDeleteFCoEEntry() - FsbRedGetMsgBuffer returns NULL \r\n");
        return FSB_FAILURE;
    }
    u2SyncMsgLen = FSB_SYN_FCOE_DEL_MSG_SIZE;

    /* Fill the message type and message length */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYN_FCOE_DEL_MSG);
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);

    /* Fill the FSB FCoE information to be synced up */
    /* The following are the index for the table */
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, pFsbFipSessFCoEEntry->u2VlanId);
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, pFsbFipSessFCoEEntry->u4ENodeIfIndex);
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFipSessFCoEEntry->ENodeMacAddr, &u4Offset,
                       MAC_ADDR_LEN);
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFipSessFCoEEntry->FcfMacAddr, &u4Offset,
                       MAC_ADDR_LEN);
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFipSessFCoEEntry->FCoEMacAddr, &u4Offset,
                       MAC_ADDR_LEN);

    if (FsbRedSendMsgToRm (pMsg, u2BufSize) == FSB_FAILURE)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedSendMsgToRm: Sending message to RM failed.\n");
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedProcessDeleteFCoEEntry                         */
/*                                                                           */
/* Description        : This function process the received Delete FCoEEntry  */
/*                                                                           */
/* Input(s)           : pMesg     - Pointer to the sync up message.          */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS/ FSB_FAILURE                             */
/*****************************************************************************/
INT4
FsbRedProcessDeleteFCoEEntry (tRmMsg * pMesg, UINT4 *pu4Offset,
                              UINT2 *pu2Length)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;
    tFsbFipSessFCoEEntry FsbFipSessFCoEEntry;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2MesgSize = 0;

    FSB_MEMSET (&FsbFipSessFCoEEntry, 0, sizeof (tFsbFipSessFCoEEntry));

    ProtoEvt.u4AppId = RM_FSB_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (FSB_NODE_STATUS () != FSB_NODE_STANDBY)
    {
        /* Not a Standby node, hence don't process the received
         * sync message. */
        return FSB_SUCCESS;
    }

    /* Get the Message size encoded by the FSB module in Active node. */
    FSB_RM_GET_2_BYTE (pMesg, pu4Offset, u2MesgSize);
    *pu2Length = (UINT2) (*pu2Length - FSB_RED_LENGTH_SIZE);

    if (*pu2Length != u2MesgSize)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbRedProcessDeleteFCoEEntry: Invalid Message Length ... \r\n");
        /* Data corruption, hence ignore the whole sync up message. */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        FsbRmHandleProtocolEvent (&ProtoEvt);
        return FSB_FAILURE;
    }

    FSB_RM_GET_2_BYTE (pMesg, pu4Offset, (FsbFipSessFCoEEntry.u2VlanId));
    FSB_RM_GET_4_BYTE (pMesg, pu4Offset, (FsbFipSessFCoEEntry.u4ENodeIfIndex));
    FSB_RM_GET_N_BYTE (pMesg, (FsbFipSessFCoEEntry.ENodeMacAddr), pu4Offset,
                       MAC_ADDR_LEN);
    FSB_RM_GET_N_BYTE (pMesg, (FsbFipSessFCoEEntry.FcfMacAddr), pu4Offset,
                       MAC_ADDR_LEN);
    FSB_RM_GET_N_BYTE (pMesg, (FsbFipSessFCoEEntry.FCoEMacAddr), pu4Offset,
                       MAC_ADDR_LEN);

    pFsbFipSessFCoEEntry = (tFsbFipSessFCoEEntry *)
        RBTreeGet (gFsbGlobals.FsbFipSessFCoETable, &FsbFipSessFCoEEntry);
    if (FsbDeleteFCoEEntry (pFsbFipSessFCoEEntry) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRedProcessDeleteFCoEEntry: "
                         "FsbDeleteFCoEEntry failed\r\n");
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : FsbRedSyncUpFIPFDISCReject                           */
/*                                                                           */
/* Description        : This function populates the FIP FDISC Reject         */
/*                      message to be sent to the standby.                   */
/*                                                                           */
/* Input(s)           : pFsbFipSessEntry - Pointer to FsbFipSessEntry        */
/*                      pMsg             - Pointer to RM message             */
/*                      pFSBPktInfo      - Pointer to FSBPktInfo             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpFIPFDISCReject (tFsbFipSessEntry * pFsbFipSessEntry, tRmMsg * pMsg,
                            tFSBPktInfo * pFSBPktInfo)
{
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT2               u2SyncMsgLen = 0;

    if (pFsbFipSessEntry == NULL)
    {
        FSB_GLOBAL_TRC
            ("FsbRedSyncUpFIPFDISCReject: pFsbFipSessEntry is NULL\r\n");
        RM_FREE (pMsg);
        return FSB_FAILURE;
    }

    u2SyncMsgLen = FSB_SYNC_FIP_FDISC_REJECT_MSG_SIZE;

    /* Fill the message type. */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYNC_FIP_FDISC_REJECT_MSG);
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);

    /* Fill the fsb sess information to be synced up. */
    /* The following are the index for the table */
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, pFsbFipSessEntry->u2VlanId);
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFipSessEntry->ENodeMacAddr, &u4Offset,
                       MAC_ADDR_LEN);
    FSB_RM_PUT_1_BYTE (pMsg, &u4Offset, pFSBPktInfo->PacketType);

    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedProcessFIPFDISCReject                          */
/*                                                                           */
/* Description        : This function handles the FIP FDISC Reject           */
/*                      message recieved in the standby                      */
/*                                                                           */
/* Input(s)           : pMsg      - Pointer to the sync up message.          */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedProcessFIPFDISCReject (tRmMsg * pMsg, UINT4 *pu4Offset, UINT2 *pu2Length)
{
    tFSBPktInfo         FSBPktInfo;
    UINT2               u2MesgSize = 0;
    UINT1               u1PacketType = 0;

    if (FSB_NODE_STATUS () != FSB_NODE_STANDBY)
    {
        /* Not a Standby node, hence don't process the received
         * sync message. */
        return FSB_SUCCESS;
    }

    FSB_MEMSET (&FSBPktInfo, 0, sizeof (tFSBPktInfo));

    /* Get the size of the syncup message */
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, u2MesgSize);
    *pu2Length = (UINT2) (*pu2Length - FSB_RED_LENGTH_SIZE);

    if (*pu2Length != u2MesgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        FSB_GLOBAL_TRC ("FsbRedProcessFIPFDISCReject: Data corruption \r\n");
        return FSB_FAILURE;
    }

    /* Get the index of FIP Session Entry Table filled in message */
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, (FSBPktInfo.u2VlanId));
    FSB_RM_GET_N_BYTE (pMsg, (FSBPktInfo.DestAddr), pu4Offset, MAC_ADDR_LEN);
    FSB_RM_GET_1_BYTE (pMsg, pu4Offset, (u1PacketType));
    FSBPktInfo.PacketType = u1PacketType;

    if (FsbHandleFIPSession (&FSBPktInfo) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                         FSB_DEBUGGING_LEVEL, FSB_SESSION_TRC,
                         "FsbRedProcessFIPFDISCReject: "
                         "FsbHandleFIPSession failed\r\n");
        return FSB_FAILURE;
    }

    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedSyncUpFIPLOGORequest                           */
/*                                                                           */
/* Description        : This function populates the FIP LOGO Request         */
/*                      message to be sent to the standby.                   */
/*                                                                           */
/* Input(s)           : pFsbFipSessEntry - Pointer to FsbFipSessEntry        */
/*                      pMsg             - Pointer to RM message             */
/*                      pFSBPktInfo      - Pointer to FSBPktInfo             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpFIPLOGORequest (tFsbFipSessEntry * pFsbFipSessEntry, tRmMsg * pMsg,
                            tFSBPktInfo * pFSBPktInfo)
{
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT2               u2SyncMsgLen = 0;

    if (pFsbFipSessEntry == NULL)
    {
        FSB_GLOBAL_TRC
            ("FsbRedSyncUpFIPLOGORequest: pFsbFipSessEntry is NULL\r\n");
        RM_FREE (pMsg);
        return FSB_FAILURE;
    }

    u2SyncMsgLen = FSB_SYNC_FIP_LOGO_REQUEST_MSG_SIZE;

    /* Fill the message type. */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYNC_FIP_LOGO_REQUEST_MSG);
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);

    /* Fill the fsb sess information to be synced up. */
    /* The following are the index for the table */
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, pFsbFipSessEntry->u2VlanId);
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, pFsbFipSessEntry->u4ENodeIfIndex);
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFipSessEntry->ENodeMacAddr, &u4Offset,
                       MAC_ADDR_LEN);
    FSB_RM_PUT_1_BYTE (pMsg, &u4Offset, pFSBPktInfo->PacketType);

    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedProcessFIPLOGORequest                          */
/*                                                                           */
/* Description        : This function handles the FIP LOGO Request           */
/*                      message recieved in the standby                      */
/*                                                                           */
/* Input(s)           : pMsg      - Pointer to the sync up message.          */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedProcessFIPLOGORequest (tRmMsg * pMsg, UINT4 *pu4Offset, UINT2 *pu2Length)
{
    tFSBPktInfo         FSBPktInfo;
    UINT2               u2MesgSize = 0;
    UINT1               u1PacketType = 0;

    if (FSB_NODE_STATUS () != FSB_NODE_STANDBY)
    {
        /* Not a Standby node, hence don't process the received
         * sync message. */
        return FSB_SUCCESS;
    }

    FSB_MEMSET (&FSBPktInfo, 0, sizeof (tFSBPktInfo));

    /* Get the size of the syncup message */
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, u2MesgSize);
    *pu2Length = (UINT2) (*pu2Length - FSB_RED_LENGTH_SIZE);

    if (*pu2Length != u2MesgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        FSB_GLOBAL_TRC ("FsbRedProcessFIPLOGORequest: Data corruption \r\n");
        return FSB_FAILURE;
    }

    /* Get the index of FIP Session Entry Table filled in message */
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, (FSBPktInfo.u2VlanId));
    FSB_RM_GET_4_BYTE (pMsg, pu4Offset, (FSBPktInfo.u4IfIndex));
    FSB_RM_GET_N_BYTE (pMsg, (FSBPktInfo.SrcAddr), pu4Offset, MAC_ADDR_LEN);
    FSB_RM_GET_1_BYTE (pMsg, pu4Offset, (u1PacketType));
    FSBPktInfo.PacketType = u1PacketType;

    if (FsbHandleFIPSession (&FSBPktInfo) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                         FSB_DEBUGGING_LEVEL, FSB_SESSION_TRC,
                         "FsbRedProcessFIPLOGORequest: "
                         "FsbHandleFIPSession failed\r\n");
        return FSB_FAILURE;
    }

    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedSyncUpFIPLOGOReject                            */
/*                                                                           */
/* Description        : This function populates the FIP LOGO Reject          */
/*                      message to be sent to the standby.                   */
/*                                                                           */
/* Input(s)           : pFsbFipSessEntry - Pointer to FsbFipSessEntry        */
/*                      pMsg             - Pointer to RM message             */
/*                      pFSBPktInfo      - Pointer to FSBPktInfo             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpFIPLOGOReject (tFsbFipSessEntry * pFsbFipSessEntry, tRmMsg * pMsg,
                           tFSBPktInfo * pFSBPktInfo)
{
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT2               u2SyncMsgLen = 0;

    if (pFsbFipSessEntry == NULL)
    {
        FSB_GLOBAL_TRC
            ("FsbRedSyncUpFIPLOGOReject: pFsbFipSessEntry is NULL\r\n");
        RM_FREE (pMsg);
        return FSB_FAILURE;
    }

    u2SyncMsgLen = FSB_SYNC_FIP_LOGO_REJECT_MSG_SIZE;

    /* Fill the message type. */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYNC_FIP_LOGO_REJECT_MSG);
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);

    /* Fill the fsb sess information to be synced up. */
    /* The following are the index for the table */
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, pFsbFipSessEntry->u2VlanId);
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFipSessEntry->ENodeMacAddr, &u4Offset,
                       MAC_ADDR_LEN);
    FSB_RM_PUT_1_BYTE (pMsg, &u4Offset, pFSBPktInfo->PacketType);

    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedProcessFIPLOGOReject                           */
/*                                                                           */
/* Description        : This function handles the FIP LOGO Reject            */
/*                      message recieved in the standby                      */
/*                                                                           */
/* Input(s)           : pMsg      - Pointer to the sync up message.          */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedProcessFIPLOGOReject (tRmMsg * pMsg, UINT4 *pu4Offset, UINT2 *pu2Length)
{
    tFSBPktInfo         FSBPktInfo;
    UINT2               u2MesgSize = 0;
    UINT1               u1PacketType = 0;

    if (FSB_NODE_STATUS () != FSB_NODE_STANDBY)
    {
        /* Not a Standby node, hence don't process the received
         * sync message. */
        return FSB_SUCCESS;
    }

    FSB_MEMSET (&FSBPktInfo, 0, sizeof (tFSBPktInfo));

    /* Get the size of the syncup message */
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, u2MesgSize);
    *pu2Length = (UINT2) (*pu2Length - FSB_RED_LENGTH_SIZE);

    if (*pu2Length != u2MesgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        FSB_GLOBAL_TRC ("FsbRedProcessFIPLOGOReject: Data corruption \r\n");
        return FSB_FAILURE;
    }

    /* Get the index of FIP Session Entry Table filled in message */
    FSB_RM_GET_2_BYTE (pMsg, pu4Offset, (FSBPktInfo.u2VlanId));
    FSB_RM_GET_N_BYTE (pMsg, (FSBPktInfo.DestAddr), pu4Offset, MAC_ADDR_LEN);
    FSB_RM_GET_1_BYTE (pMsg, pu4Offset, (u1PacketType));
    FSBPktInfo.PacketType = u1PacketType;

    if (FsbHandleFIPSession (&FSBPktInfo) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                         FSB_DEBUGGING_LEVEL, FSB_SESSION_TRC,
                         "FsbRedProcessFIPLOGOReject: "
                         "FsbHandleFIPSession failed\r\n");
        return FSB_FAILURE;
    }

    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedSendBulkReq.                                   */
/*                                                                           */
/* Description        : This function sends a bulk request to the active     */
/*                      node in the system.                                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                            */
/*****************************************************************************/
INT4
FsbRedSendBulkReq (VOID)
{
    tRmMsg             *pMsg;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4SyncMsgType;
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT2               u2BufSize;

    ProtoEvt.u4AppId = RM_FSB_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2BufSize = FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    pMsg = FsbRedGetMsgBuffer (u2BufSize);
    if (pMsg == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                         "\r\n FsbRedSendBulkReq() - FsbRedGetMsgBuffer returns NULL \r\n");
        return FSB_FAILURE;
    }

    /* Fill the message type. */
    u4SyncMsgType = FSB_BULK_REQ_MSG;
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, u4SyncMsgType);

    /* This length field will not be used. It is just to keep the
     * this message structure similar to that of other sync msgs. */
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, FSB_BULK_REQ_MSG);
    FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_CRITICAL_LEVEL, FSB_NONE,
                     "[STANDBY]: Send Bulk Request Message \r\n");

    if (FsbRedSendMsgToRm (pMsg, u2BufSize) == FSB_FAILURE)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_CRITICAL_LEVEL, FSB_NONE,
                         "FsbRedSendBulkReq: Sending message to RM failed.\r\n");
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        FsbRmHandleProtocolEvent (&ProtoEvt);
        return FSB_FAILURE;
    }

    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedHandleBulkUpdateEvent.                         */
/*                                                                           */
/* Description        : It Handles the bulk update event. This event is used */
/*                      to start the next sub bulk update. So                */
/*                      FsbRedHandleBulkReqEvent is triggered.               */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
FsbRedHandleBulkUpdateEvent (VOID)
{
    FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_CRITICAL_LEVEL, FSB_NONE,
                     "[ACTIVE]: Received Bulk Request \r\n");
    if (FSB_NODE_STATUS () == FSB_NODE_ACTIVE)
    {
        FsbRedHandleBulkReqEvent ();
    }
    else
    {
        /* Only Active node can process bulk request and can
         * send the bulk update.*/
        FSB_BULK_REQ_RCVD () = FSB_TRUE;
    }
}

/*****************************************************************************/
/* Function Name      : FsbRedSendBulkUpdateTailMsg                          */
/*                                                                           */
/* Description        : This function will send the tail msg to the standy   */
/*                      node, which indicates the completion of Bulk update  */
/*                      process.                                             */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSendBulkUpdateTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT2               u2BufSize;

    ProtoEvt.u4AppId = RM_FSB_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (FSB_NODE_STATUS () != FSB_NODE_ACTIVE)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_CRITICAL_LEVEL, FSB_NONE,
                         "FSB: Node is not active. Bulk Update tail msg "
                         "not sent.\n");
        return FSB_SUCCESS;
    }

    u2BufSize = FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    pMsg = FsbRedGetMsgBuffer (u2BufSize);
    if (pMsg == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\n FsbRedSendBulkUpdateTailMsg() : FsbRedGetMsgBuffer() - returns Failure  ... \r\n");

        return FSB_FAILURE;
    }
    /* Form a bulk update tail message. 

     *        <--------4 Byte--------><---2 Byte--->
     *********************************************** 
     *        *                       *             *
     * RM Hdr * FSB_BULK_UPD_TAIL_MSG * Msg Length  *
     *        *                       *             * 
     ***********************************************

     * The RM Hdr shall be included by RM.
     */

    /* Fill the message type. */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_BULK_UPD_TAIL_MSG);

    /* This length field will not be used. It is just to keep the
     * this message structure similar to that of other sync msgs. */
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, FSB_BULK_UPD_TAIL_SIZE);
    FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_CRITICAL_LEVEL, FSB_NONE,
                     "[ACTIVE]: Send Bulk Update Tail Message \r\n");
    if (FsbRedSendMsgToRm (pMsg, u2BufSize) == FSB_FAILURE)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_CRITICAL_LEVEL, FSB_NONE,
                         "FsbRedSendBulkUpdateTailMsg: Sending message to RM failed.\r\n");
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        FsbRmHandleProtocolEvent (&ProtoEvt);
        return FSB_FAILURE;
    }

    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedInitBulkUpdateFlags.                           */
/*                                                                           */
/* Description        : This function is used to initialize the Bulk Update  */
/*                      flag for all table.                                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
FsbRedInitBulkUpdateFlags ()
{
    FSB_RED_FCF_BULK_UPD_STATUS () = OSIX_FALSE;
    FSB_RED_DEF_FIL_ID_BULK_UPD_STATUS () = OSIX_FALSE;
    FSB_RED_FIP_SESS_BULK_UPD_STATUS () = OSIX_FALSE;
    FSB_RED_FCOE_BULK_UPD_STATUS () = OSIX_FALSE;
    FSB_RED_CXT_FIL_ID_BULK_UPD_STATUS () = OSIX_FALSE;
}

/*****************************************************************************/
/* Function Name      : FsbRedHandleBulkReqEvent.                            */
/*                                                                           */
/* Description        : This function gets the dynamic information from all  */
/*                      ports whose Receive sem is in CURRENT state.         */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
FsbRedHandleBulkReqEvent (VOID)
{
    /* This flag is used to indicate whether the trigger for next bulk update
     * has been given or not*/
    BOOL1               bBulkUpdEvtSent = OSIX_FALSE;

    if (FSB_RED_FCF_BULK_UPD_STATUS () != OSIX_TRUE)
    {
        /* Bulk Update of FCF Entry */
        FsbFCFBulkUpdate (&bBulkUpdEvtSent);
    }
    if (bBulkUpdEvtSent == OSIX_TRUE)
    {
        return;
    }

    if (FSB_RED_CXT_FIL_ID_BULK_UPD_STATUS () != OSIX_TRUE)
    {
        /* Bulk Update of Default VLAN filter's Hardware Filter Id */
        FsbDefVLANHwFilterIdBulkUpdate (&bBulkUpdEvtSent);
    }
    if (bBulkUpdEvtSent == OSIX_TRUE)
    {
        return;
    }

    if (FSB_RED_DEF_FIL_ID_BULK_UPD_STATUS () != OSIX_TRUE)
    {
        /* Bulk Update of Default filter's Hardware Filter Id */
        FsbDefHwFilterIdBulkUpdate (&bBulkUpdEvtSent);
    }
    if (bBulkUpdEvtSent == OSIX_TRUE)
    {
        return;
    }

    if (FSB_RED_SCHAN_FIL_ID_BULK_UPD_STATUS () != OSIX_TRUE)
    {
        /* Bulk Update of SChannel filter's Hardware Filter Id */
        FsbSChanHwFilterIdBulkUpdate (&bBulkUpdEvtSent);
    }
    if (bBulkUpdEvtSent == OSIX_TRUE)
    {
        return;
    }

    if (FSB_RED_FIP_SESS_BULK_UPD_STATUS () != OSIX_TRUE)
    {
        /* Bulk Update of FIP Session and its Filter Entries */
        FsbFIPSessionBulkUpdate (&bBulkUpdEvtSent);
    }
    if (bBulkUpdEvtSent == OSIX_TRUE)
    {
        return;
    }

    if (FSB_RED_FCOE_BULK_UPD_STATUS () != OSIX_TRUE)
    {
        /* Bulk Update of FCoE and its Filter Entries */
        FsbFCoEBulkUpdate (&bBulkUpdEvtSent);
    }
    if (bBulkUpdEvtSent == OSIX_TRUE)
    {
        return;
    }

    /* FSB completes it's sync up during standby boot-up and this
     * needs to be informed to RMGR */
    if (FSB_RED_BULK_UPD_STATUS () == FSB_BULK_UPD_COMPLETED)
    {
        FsbRmSetBulkUpdatesStatus (RM_FSB_APP_ID);

        /* Send the tail msg to indicate the completion of Bulk
         * update process.*/
        FsbRedSendBulkUpdateTailMsg ();
    }
    return;
}

/*****************************************************************************/
/* Function Name      : FsbFCFBulkUpdate                                     */
/*                                                                           */
/* Description        : This function will send FCF Entry Bulk update        */
/*                      to the standby                                       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
FsbFCFBulkUpdate (BOOL1 * pbBulkUpdEvtSent)
{
    tFsbFcfEntry       *pFsbNextFcfEntry = NULL;
    tFsbFcfEntry       *pFsbFcfEntry = NULL;
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4MaxBulkUpdSize = FSB_BULK_SPIT_MSG_SIZE;
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT2               u2BulkUpdCount = FSB_RED_FCF_CNT_PER_BULK_UPDT;
    UINT4               u4SyncMsgLenOffset = 0;
    UINT4               u4NoOfFcfOffest = 0;
    UINT2               u2SyncMsgLen = 0;
    UINT2               u2NoOfFcf = 0;

    ProtoEvt.u4AppId = RM_FSB_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (FSB_RED_NEXT_FCF_ENTRY () == NULL)
    {
        FSB_RED_NEXT_FCF_ENTRY () = (tFsbFcfEntry *)
            RBTreeGetFirst (gFsbGlobals.FsbFcfTable);
        if (FSB_RED_NEXT_FCF_ENTRY () == NULL)
        {
            /* No entry is present in FsbFcfEntry Table.
             * Set the bulk updated status as OSIX_TRUE
             * and proceed bulk sync-up of next table.*/
            FSB_RED_FCF_BULK_UPD_STATUS () = OSIX_TRUE;
            return;
        }
    }

    pFsbNextFcfEntry = FSB_RED_NEXT_FCF_ENTRY ();

    pMsg = FsbRedGetMsgBuffer ((UINT2) u4MaxBulkUpdSize);
    if (pMsg == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_BUFFER_TRC,
                         "\r\nFsbFCFBulkUpdate:  FsbRedGetMsgBuffer Memory Allocation Failed ... \r\n");

        return;
    }

    /* Fill the message type and message length */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYN_FCF_ADD_MSG);
    u4SyncMsgLenOffset = u4Offset;
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);
    u4NoOfFcfOffest = u4Offset;
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2NoOfFcf);

    while ((pFsbNextFcfEntry != NULL) && (u2BulkUpdCount > 0))
    {
        pFsbFcfEntry = pFsbNextFcfEntry;
        if ((u4MaxBulkUpdSize - u4Offset) < FSB_RED_FCF_MSG_LEN)
        {
            u2SyncMsgLen =
                (UINT2) (u4Offset -
                         (FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE));
            FSB_RM_PUT_2_BYTE (pMsg, &u4SyncMsgLenOffset, u2SyncMsgLen);
            FSB_RM_PUT_2_BYTE (pMsg, &u4NoOfFcfOffest, u2NoOfFcf);
            if (FsbRedSendMsgToRm (pMsg, (UINT2) u4Offset) == FSB_FAILURE)
            {
                ProtoEvt.u4Error = RM_SENDTO_FAIL;
                FsbRmHandleProtocolEvent (&ProtoEvt);
            }
            u4Offset = 0;
            u2NoOfFcf = 0;
            pMsg = FsbRedGetMsgBuffer ((UINT2) u4MaxBulkUpdSize);
            if (pMsg == NULL)
            {
                FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                                 FSB_BUFFER_TRC,
                                 "\r\nFsbFCFBulkUpdate:  FsbRedGetMsgBuffer Memory Allocation Failed ... 1\r\n");
                return;
            }
            /* Fill the message type and message length */
            FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYN_FCF_ADD_MSG);
            u4SyncMsgLenOffset = u4Offset;
            FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);
            u4NoOfFcfOffest = u4Offset;
            FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2NoOfFcf);
        }
        FsbRedConstructFCFEntry (pFsbFcfEntry, pMsg, &u4Offset);
        u2NoOfFcf++;
        pFsbNextFcfEntry = (tFsbFcfEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFcfTable,
                           (tRBElem *) pFsbFcfEntry, NULL);
        u2BulkUpdCount--;
    }

    u2SyncMsgLen =
        (UINT2) (u4Offset - (FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE));

    FSB_RM_PUT_2_BYTE (pMsg, &u4SyncMsgLenOffset, u2SyncMsgLen);
    FSB_RM_PUT_2_BYTE (pMsg, &u4NoOfFcfOffest, u2NoOfFcf);

    if (FsbRedSendMsgToRm (pMsg, (UINT2) u4Offset) == FSB_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        FsbRmHandleProtocolEvent (&ProtoEvt);
    }
    FSB_RED_NEXT_FCF_ENTRY () = pFsbNextFcfEntry;

    if (FSB_RED_NEXT_FCF_ENTRY () == NULL)
    {
        FSB_RED_FCF_BULK_UPD_STATUS () = OSIX_TRUE;
        FSB_RED_BULK_UPD_STATUS () = FSB_BULK_UPD_COMPLETED;
    }
    /* Send an event to start the next bulk update */
    OsixEvtSend (FSB_TASK_ID, FSB_RED_BULK_UPD_EVENT);
    *pbBulkUpdEvtSent = OSIX_TRUE;
    return;
}

/*****************************************************************************/
/* Function Name      : FsbDefVLANHwFilterIdBulkUpdate                       */
/*                                                                           */
/* Description        : This function will send Bulk update of Hardware      */
/*                      Filter Id of Default VLAN filter to the standby      */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
FsbDefVLANHwFilterIdBulkUpdate (BOOL1 * pbBulkUpdEvtSent)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4MaxBulkUpdSize = FSB_BULK_SPIT_MSG_SIZE;
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT4               u4ContextId = 0;
    UINT4               u4NoOfSyncMsgLenOffset = 0;
    UINT4               u4SyncMsgLenOffset = 0;
    UINT2               u2NoOfSyncMsg = 0;
    UINT2               u2SyncMsgLen = 0;

    ProtoEvt.u4AppId = RM_FSB_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    pMsg = FsbRedGetMsgBuffer ((UINT2) FSB_BULK_SPIT_MSG_SIZE);
    if (pMsg == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_BUFFER_TRC,
                         "\r\nFsbDefVLANHwFilterIdBulkUpdate:  FsbRedGetMsgBuffer Memory Allocation Failed ... 1\r\n");
        return;
    }

    /* Fill the message type */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYN_DEF_VLAN_UPT_MSG);

    u4SyncMsgLenOffset = u4Offset;
    /* Fill the message length */
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);

    u4NoOfSyncMsgLenOffset = u4Offset;
    /* Fill the number of Syn-Up message */
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2NoOfSyncMsg);

    for (u4ContextId = 0; u4ContextId < FSB_MAX_CONTEXT_COUNT; u4ContextId++)
    {
        if ((u4MaxBulkUpdSize - u4Offset) < FSB_RED_VLAN_FIL_ID_MSG_LEN)
        {
            u2SyncMsgLen =
                (UINT2) (u4Offset -
                         (FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE));
            FSB_RM_PUT_2_BYTE (pMsg, &u4SyncMsgLenOffset, u2SyncMsgLen);
            FSB_RM_PUT_2_BYTE (pMsg, &u4NoOfSyncMsgLenOffset, u2NoOfSyncMsg);
            if (FsbRedSendMsgToRm (pMsg, (UINT2) u4Offset) == FSB_FAILURE)
            {
                ProtoEvt.u4Error = RM_SENDTO_FAIL;
                FsbRmHandleProtocolEvent (&ProtoEvt);
            }
            u4Offset = 0;
            u2NoOfSyncMsg = 0;
            pMsg = FsbRedGetMsgBuffer ((UINT2) u4MaxBulkUpdSize);
            if (pMsg == NULL)
            {
                FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                                 FSB_BUFFER_TRC,
                                 "\r\nFsbDefVLANHwFilterIdBulkUpdate:  FsbRedGetMsgBuffer Memory Allocation Failed ... 2\r\n");

                return;
            }
            /* Fill the message type and message length */
            FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYN_DEF_VLAN_UPT_MSG);
            u4SyncMsgLenOffset = u4Offset;
            FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);
            u4NoOfSyncMsgLenOffset = u4Offset;
            FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2NoOfSyncMsg);
        }
        pFsbContextInfo = FsbCxtGetContextEntry (u4ContextId);
        if (pFsbContextInfo != NULL)
        {
            /* Fill the Context Id  - Index for ContextInfo Table */
            FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, u4ContextId);
            /* Fill Hw filter Id for Local Filter of Vlan Discovery */
            FSB_RM_PUT_4_BYTE (pMsg, &u4Offset,
                               pFsbContextInfo->FsbLocFilterEntry.
                               FsbVlanDiscovery.u4HwFilterId);
            /* Fill Hw filter Id for Remote Filter of Vlan Discovery */
            FSB_RM_PUT_4_BYTE (pMsg, &u4Offset,
                               pFsbContextInfo->FsbRemFilterEntry.
                               FsbVlanDiscovery.u4HwFilterId);
            /* Fill Hw filter Id for Local Filter of Vlan Response Tagged */
            FSB_RM_PUT_4_BYTE (pMsg, &u4Offset,
                               pFsbContextInfo->FsbLocFilterEntry.
                               FsbVlanResponseTagged.u4HwFilterId);
            /* Fill Hw filter Id for Remote Filter of Vlan Response Tagged */
            FSB_RM_PUT_4_BYTE (pMsg, &u4Offset,
                               pFsbContextInfo->FsbRemFilterEntry.
                               FsbVlanResponseTagged.u4HwFilterId);
            u2NoOfSyncMsg++;
        }
    }

    u2SyncMsgLen =
        (UINT2) (u4Offset - (FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE));
    /* Update the message length */
    FSB_RM_PUT_2_BYTE (pMsg, &u4SyncMsgLenOffset, u2SyncMsgLen);
    /* Update number of sync message */
    FSB_RM_PUT_2_BYTE (pMsg, &u4NoOfSyncMsgLenOffset, u2NoOfSyncMsg);

    if (FsbRedSendMsgToRm (pMsg, (UINT2) u4Offset) == FSB_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        FsbRmHandleProtocolEvent (&ProtoEvt);
    }
    FSB_RED_CXT_FIL_ID_BULK_UPD_STATUS () = OSIX_TRUE;
    FSB_RED_BULK_UPD_STATUS () = FSB_BULK_UPD_COMPLETED;
    /* Send an event to start the next bulk update */
    OsixEvtSend (FSB_TASK_ID, FSB_RED_BULK_UPD_EVENT);
    *pbBulkUpdEvtSent = OSIX_TRUE;
    return;
}

/*****************************************************************************/
/* Function Name      : FsbDefHwFilterIdBulkUpdate                           */
/*                                                                           */
/* Description        : This function will send Bulk update of Hardware      */
/*                      Filter Id of Default filter to the standby           */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
FsbDefHwFilterIdBulkUpdate (BOOL1 * pbBulkUpdEvtSent)
{
    tFsbFipSnoopingEntry *pFsbNextFipSnoopingEntry = NULL;
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4MaxBulkUpdSize = FSB_BULK_SPIT_MSG_SIZE;
    UINT4               u4NoOfFilter = 0;
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT2               u2BulkUpdCount = FSB_RED_DEF_FIL_ID_PER_BULK_UPDT;
    UINT4               u4SyncMsgLenOffset = 0;
    UINT2               u2DefFilIdMsgLen = 0;
    UINT4               u4NoOfFipOffset = 0;
    UINT2               u2NoOfFipEntry = 0;
    UINT2               u2SyncMsgLen = 0;

    ProtoEvt.u4AppId = RM_FSB_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (FSB_RED_NEXT_SNOOPING_ENTRY () == NULL)
    {
        FSB_RED_NEXT_SNOOPING_ENTRY () = (tFsbFipSnoopingEntry *)
            RBTreeGetFirst (gFsbGlobals.FsbFipSnoopingTable);
        if (FSB_RED_NEXT_SNOOPING_ENTRY () == NULL)
        {
            /* No entry is present in FsbFipSnoopingEntry Table.
             * Set the bulk updated status as OSIX_TRUE
             * and proceed bulk sync-up of next table.*/
            FSB_RED_DEF_FIL_ID_BULK_UPD_STATUS () = OSIX_TRUE;
            return;
        }
    }

    pFsbNextFipSnoopingEntry = FSB_RED_NEXT_SNOOPING_ENTRY ();

    pMsg = FsbRedGetMsgBuffer ((UINT2) u4MaxBulkUpdSize);
    if (pMsg == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_BUFFER_TRC,
                         "\r\n FsbDefHwFilterIdBulkUpdate :  FsbRedGetMsgBuffer Memory Allocation Failed ... 2\r\n");

        return;
    }

    /* Fill the message type and message length */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYN_DEF_FILTER_UPT_MSG);
    u4SyncMsgLenOffset = u4Offset;
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);
    u4NoOfFipOffset = u4Offset;
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2NoOfFipEntry);

    while ((pFsbNextFipSnoopingEntry != NULL) && (u2BulkUpdCount > 0))
    {
        pFsbFipSnoopingEntry = pFsbNextFipSnoopingEntry;
        if (RBTreeCount (pFsbFipSnoopingEntry->FsbFilterEntry,
                         &u4NoOfFilter) != RB_SUCCESS)
        {
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                             "\r\n FsbDefHwFilterIdBulkUpdate :  No more filter entry \r\n");
            RM_FREE (pMsg);
            return;
        }
        u2DefFilIdMsgLen =
            (UINT2) (FSB_RED_DEF_FIL_ID_MSG_LEN ((UINT2) u4NoOfFilter));
        if ((u4MaxBulkUpdSize - u4Offset) < u2DefFilIdMsgLen)
        {
            u2SyncMsgLen =
                (UINT2) (u4Offset -
                         (FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE));
            FSB_RM_PUT_2_BYTE (pMsg, &u4SyncMsgLenOffset, u2SyncMsgLen);
            FSB_RM_PUT_2_BYTE (pMsg, &u4NoOfFipOffset, u2NoOfFipEntry);
            if (FsbRedSendMsgToRm (pMsg, (UINT2) u4Offset) == FSB_FAILURE)
            {
                ProtoEvt.u4Error = RM_SENDTO_FAIL;
                FsbRmHandleProtocolEvent (&ProtoEvt);
            }
            u4Offset = 0;
            u2NoOfFipEntry = 0;
            pMsg = FsbRedGetMsgBuffer ((UINT2) u4MaxBulkUpdSize);
            if (pMsg == NULL)
            {
                FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                                 FSB_BUFFER_TRC,
                                 "\r\n FsbDefHwFilterIdBulkUpdate :  pMsg is NULL \r\n");
                return;
            }
            /* Fill the message type and message length */
            FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYN_DEF_FILTER_UPT_MSG);
            u4SyncMsgLenOffset = u4Offset;
            FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);
            u4NoOfFipOffset = u4Offset;
            FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2NoOfFipEntry);

        }
        FsbRedUpdateDefFilterId (pFsbFipSnoopingEntry, pMsg, &u4Offset,
                                 (UINT2) u4NoOfFilter);
        u2NoOfFipEntry++;
        pFsbNextFipSnoopingEntry = (tFsbFipSnoopingEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFipSnoopingTable,
                           (tRBElem *) pFsbFipSnoopingEntry, NULL);
        u2BulkUpdCount--;
    }

    u2SyncMsgLen =
        (UINT2) (u4Offset - (FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE));

    FSB_RM_PUT_2_BYTE (pMsg, &u4SyncMsgLenOffset, u2SyncMsgLen);
    FSB_RM_PUT_2_BYTE (pMsg, &u4NoOfFipOffset, u2NoOfFipEntry);

    if (FsbRedSendMsgToRm (pMsg, (UINT2) u4Offset) == FSB_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        FsbRmHandleProtocolEvent (&ProtoEvt);
    }
    FSB_RED_NEXT_SNOOPING_ENTRY () = pFsbNextFipSnoopingEntry;

    if (FSB_RED_NEXT_SNOOPING_ENTRY () == NULL)
    {
        FSB_RED_DEF_FIL_ID_BULK_UPD_STATUS () = OSIX_TRUE;
        FSB_RED_BULK_UPD_STATUS () = FSB_BULK_UPD_COMPLETED;
    }
    /* Send an event to start the next bulk update */
    OsixEvtSend (FSB_TASK_ID, FSB_RED_BULK_UPD_EVENT);
    *pbBulkUpdEvtSent = OSIX_TRUE;
    return;
}

/******************************************************************************/
/* Function Name      : FsbRedUpdateDefFilterId                               */
/*                                                                            */
/* Description        : This function update Default Filter Id in             */
/*                      FIP Snooping Table to be sent to the standby          */
/*                                                                            */
/* Input(s)           : pFsbFipSnoopingEntry - Pointer to FsbFipSnoopingEntry */
/*                      pMsg                 - Pointer to RM Message          */
/*                      pu4Offset            - Pointer to Offset              */
/*                                                                            */
/* Output(s)          : None                                                  */
/*                                                                            */
/* Global Variables                                                           */
/* Referred           : None.                                                 */
/*                                                                            */
/* Global Variables                                                           */
/* Modified           : None.                                                 */
/*                                                                            */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                            */
/******************************************************************************/
VOID
FsbRedUpdateDefFilterId (tFsbFipSnoopingEntry * pFsbFipSnoopingEntry,
                         tRmMsg * pMsg, UINT4 *pu4Offset, UINT2 u2NoOfFilter)
{
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;

    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    FSB_RM_PUT_4_BYTE (pMsg, pu4Offset, (pFsbFipSnoopingEntry->u4ContextId));
    FSB_RM_PUT_2_BYTE (pMsg, pu4Offset, (pFsbFipSnoopingEntry->u2VlanId));
    FSB_RM_PUT_2_BYTE (pMsg, pu4Offset, u2NoOfFilter);

    pFsbFilterEntry = (tFsbFilterEntry *)
        RBTreeGetFirst (pFsbFipSnoopingEntry->FsbFilterEntry);
    if (pFsbFilterEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_OS_RESOURCE_TRC,
                         "\r\n FsbRedUpdateDefFilterId:  pFsbFilterEntry is NULL \r\n");
        return;
    }
    do
    {
        FSB_MEMCPY (FsbFilterEntry.DstMac, pFsbFilterEntry->DstMac,
                    MAC_ADDR_LEN);

        FSB_RM_PUT_N_BYTE (pMsg, (pFsbFilterEntry->DstMac), pu4Offset,
                           MAC_ADDR_LEN);
        FSB_RM_PUT_4_BYTE (pMsg, pu4Offset, (pFsbFilterEntry->u4HwFilterId));

        pFsbFilterEntry = (tFsbFilterEntry *)
            RBTreeGetNext (pFsbFipSnoopingEntry->FsbFilterEntry,
                           (tRBElem *) & FsbFilterEntry, NULL);
    }
    while (pFsbFilterEntry != NULL);
}

/*****************************************************************************/
/* Function Name      : FsbSChanHwFilterIdBulkUpdate                         */
/*                                                                           */
/* Description        : This function will send Bulk update of Hardware      */
/*                      Filter Id of SChannel filter to the standby          */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
FsbSChanHwFilterIdBulkUpdate (BOOL1 * pbBulkUpdEvtSent)
{
    tFsbSChannelFilterEntry *pFsbNextSChannelFilterEntry = NULL;
    tFsbSChannelFilterEntry *pFsbSChannelFilterEntry = NULL;
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4MaxBulkUpdSize = FSB_BULK_SPIT_MSG_SIZE;
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT4               u4SyncMsgLenOffset = 0;
    UINT4               u4NoOfSChanOffset = 0;
    UINT2               u2BulkUpdCount = FSB_RED_SCHAN_FIL_ID_PER_BULK_UPDT;
    UINT2               u2NoOfSChanEntry = 0;
    UINT2               u2SyncMsgLen = 0;

    ProtoEvt.u4AppId = RM_FSB_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (FSB_RED_NEXT_SCHAN_ENTRY () == NULL)
    {
        FSB_RED_NEXT_SCHAN_ENTRY () = (tFsbSChannelFilterEntry *)
            RBTreeGetFirst (gFsbGlobals.FsbSChannelFilterTable);
        if (FSB_RED_NEXT_SCHAN_ENTRY () == NULL)
        {
            /* No entry is present in FsbSChannelFilterEntry Table.
             * Set the bulk updated status as OSIX_TRUE
             * and proceed bulk sync-up of next table.*/
            FSB_RED_SCHAN_FIL_ID_BULK_UPD_STATUS () = OSIX_TRUE;
            return;
        }
    }

    pFsbNextSChannelFilterEntry = FSB_RED_NEXT_SCHAN_ENTRY ();

    pMsg = FsbRedGetMsgBuffer ((UINT2) u4MaxBulkUpdSize);
    if (pMsg == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_BUFFER_TRC,
                         "\r\n FsbSChanHwFilterIdBulkUpdate:  FsbRedGetMsgBuffer Memory Allocation Failed ... 2\r\n");

        return;
    }

    /* Fill the message type and message length */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYN_SCHAN_FILTER_UPT_MSG);
    u4SyncMsgLenOffset = u4Offset;
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);
    u4NoOfSChanOffset = u4Offset;
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2NoOfSChanEntry);

    while ((pFsbNextSChannelFilterEntry != NULL) && (u2BulkUpdCount > 0))
    {
        pFsbSChannelFilterEntry = pFsbNextSChannelFilterEntry;
        if ((u4MaxBulkUpdSize - u4Offset) < FSB_RED_SCHAN_FIL_ID_MSG_LEN)
        {
            u2SyncMsgLen =
                (UINT2) (u4Offset -
                         (FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE));
            FSB_RM_PUT_2_BYTE (pMsg, &u4SyncMsgLenOffset, u2SyncMsgLen);
            FSB_RM_PUT_2_BYTE (pMsg, &u4NoOfSChanOffset, u2NoOfSChanEntry);
            if (FsbRedSendMsgToRm (pMsg, (UINT2) u4Offset) == FSB_FAILURE)
            {
                ProtoEvt.u4Error = RM_SENDTO_FAIL;
                FsbRmHandleProtocolEvent (&ProtoEvt);
            }
            u4Offset = 0;
            u2NoOfSChanEntry = 0;
            pMsg = FsbRedGetMsgBuffer ((UINT2) u4MaxBulkUpdSize);
            if (pMsg == NULL)
            {
                FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                                 FSB_BUFFER_TRC,
                                 "\r\n FsbSChanHwFilterIdBulkUpdate:  pMsg is NULL \r\n");
                return;
            }
            /* Fill the message type and message length */
            FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYN_SCHAN_FILTER_UPT_MSG);
            u4SyncMsgLenOffset = u4Offset;
            FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);
            u4NoOfSChanOffset = u4Offset;
            FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2NoOfSChanEntry);

        }

        /* Fill the S Channel Hw filter Id to be synced up */
        /* Fill the If Index and Vlan Id - Index for FsbSChannelFilterTable */
        FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, pFsbSChannelFilterEntry->u4IfIndex);
        FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, pFsbSChannelFilterEntry->u2VlanId);

        /* Fill the Hw Filter Id */
        FSB_RM_PUT_4_BYTE (pMsg, &u4Offset,
                           pFsbSChannelFilterEntry->u4HwFilterId);

        u2NoOfSChanEntry++;
        pFsbNextSChannelFilterEntry = (tFsbSChannelFilterEntry *)
            RBTreeGetNext (gFsbGlobals.FsbSChannelFilterTable,
                           (tRBElem *) pFsbSChannelFilterEntry, NULL);
        u2BulkUpdCount--;
    }

    u2SyncMsgLen =
        (UINT2) (u4Offset - (FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE));

    FSB_RM_PUT_2_BYTE (pMsg, &u4SyncMsgLenOffset, u2SyncMsgLen);
    FSB_RM_PUT_2_BYTE (pMsg, &u4NoOfSChanOffset, u2NoOfSChanEntry);

    if (FsbRedSendMsgToRm (pMsg, (UINT2) u4Offset) == FSB_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        FsbRmHandleProtocolEvent (&ProtoEvt);
    }
    FSB_RED_NEXT_SCHAN_ENTRY () = pFsbNextSChannelFilterEntry;

    if (FSB_RED_NEXT_SCHAN_ENTRY () == NULL)
    {
        FSB_RED_SCHAN_FIL_ID_BULK_UPD_STATUS () = OSIX_TRUE;
        FSB_RED_BULK_UPD_STATUS () = FSB_BULK_UPD_COMPLETED;
    }
    /* Send an event to start the next bulk update */
    OsixEvtSend (FSB_TASK_ID, FSB_RED_BULK_UPD_EVENT);
    *pbBulkUpdEvtSent = OSIX_TRUE;
    return;
}

/******************************************************************************/
/* Function Name      : FsbRedFormFipSessBulkUpdMsg                           */
/*                                                                            */
/* Description        : This function is used to form Fip Session Bulk        */
/*                      Update Message to be sent to the standby              */
/*                                                                            */
/* Input(s)           : pFsbFipSessEntry     - Pointer to FsbFipSessEntry     */
/*                      pMsg                 - Pointer to RM Message          */
/*                      pu4Offset            - Pointer to Offset              */
/*                                                                            */
/* Output(s)          : None                                                  */
/*                                                                            */
/* Global Variables                                                           */
/* Referred           : None.                                                 */
/*                                                                            */
/* Global Variables                                                           */
/* Modified           : None.                                                 */
/*                                                                            */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                            */
/******************************************************************************/
VOID
FsbRedFormFipSessBulkUpdMsg (tFsbFipSessEntry * pFsbFipSessEntry, tRmMsg * pMsg,
                             UINT4 *pu4Offset, UINT2 u2NoOfFilter)
{
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;

    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    /* Fill the index(VlanId, ENodeIfIndex, ENodeMac) of FIP Sesssion Table */
    FSB_RM_PUT_2_BYTE (pMsg, pu4Offset, pFsbFipSessEntry->u2VlanId);
    FSB_RM_PUT_4_BYTE (pMsg, pu4Offset, pFsbFipSessEntry->u4ENodeIfIndex);
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFipSessEntry->ENodeMacAddr, pu4Offset,
                       MAC_ADDR_LEN);
    /* Fill other entries in FIP Session Table */
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFipSessEntry->au1FcMap, pu4Offset,
                       FSB_FCMAP_LEN);
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFipSessEntry->au1NameId, pu4Offset,
                       FSB_NAME_ID_LEN);
    FSB_RM_PUT_4_BYTE (pMsg, pu4Offset, pFsbFipSessEntry->u4ContextId);
    FSB_RM_PUT_4_BYTE (pMsg, pu4Offset, pFsbFipSessEntry->u4FcfIfIndex);
    FSB_RM_PUT_4_BYTE (pMsg, pu4Offset, pFsbFipSessEntry->u4FCoECount);
    FSB_RM_PUT_2_BYTE (pMsg, pu4Offset,
                       pFsbFipSessEntry->u2HouseKeepingTimePeriod);
    FSB_RM_PUT_1_BYTE (pMsg, pu4Offset, pFsbFipSessEntry->u1State);

    /* Fill No of filters in FIP Session Table */
    FSB_RM_PUT_2_BYTE (pMsg, pu4Offset, u2NoOfFilter);
    pFsbFilterEntry = (tFsbFilterEntry *)
        RBTreeGetFirst (pFsbFipSessEntry->FsbSessFilterEntry);
    if (pFsbFilterEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_OS_RESOURCE_TRC,
                         "\r\n FsbRedFormFipSessBulkUpdMsg:  pFsbFilterEntry is NULL... \r\n");
        return;
    }

    do
    {
        FSB_MEMCPY (FsbFilterEntry.DstMac, pFsbFilterEntry->DstMac,
                    MAC_ADDR_LEN);
        FSB_MEMCPY (FsbFilterEntry.SrcMac, pFsbFilterEntry->SrcMac,
                    MAC_ADDR_LEN);
        FsbFilterEntry.u2OpcodeFilterOffsetValue =
            pFsbFilterEntry->u2OpcodeFilterOffsetValue;
        FsbFilterEntry.u2SubOpcodeFilterOffsetValue =
            pFsbFilterEntry->u2SubOpcodeFilterOffsetValue;
        /* Fill the index (DstMac, SrcMac, Opcode, SubOpcode) of 
         * Filter Entry in FIP Session Table */
        FSB_RM_PUT_N_BYTE (pMsg, pFsbFilterEntry->DstMac, pu4Offset,
                           MAC_ADDR_LEN);
        FSB_RM_PUT_N_BYTE (pMsg, pFsbFilterEntry->SrcMac, pu4Offset,
                           MAC_ADDR_LEN);
        FSB_RM_PUT_2_BYTE (pMsg, pu4Offset,
                           pFsbFilterEntry->u2OpcodeFilterOffsetValue);
        FSB_RM_PUT_2_BYTE (pMsg, pu4Offset,
                           pFsbFilterEntry->u2SubOpcodeFilterOffsetValue);
        FSB_RM_PUT_4_BYTE (pMsg, pu4Offset, pFsbFilterEntry->u4IfIndex);
        FSB_RM_PUT_4_BYTE (pMsg, pu4Offset, pFsbFilterEntry->u4AggIndex);
        FSB_RM_PUT_2_BYTE (pMsg, pu4Offset, pFsbFilterEntry->u2ClassId);
        FSB_RM_PUT_4_BYTE (pMsg, pu4Offset, pFsbFilterEntry->u4FilterId);
        FSB_RM_PUT_4_BYTE (pMsg, pu4Offset, pFsbFilterEntry->u4HwFilterId);

        pFsbFilterEntry = (tFsbFilterEntry *)
            RBTreeGetNext (pFsbFipSessEntry->FsbSessFilterEntry,
                           (tRBElem *) & FsbFilterEntry, NULL);
    }
    while (pFsbFilterEntry != NULL);
}

/*****************************************************************************/
/* Function Name      : FsbFIPSessionBulkUpdate                              */
/*                                                                           */
/* Description        : This function will send FIP SessionEntry Bulk update */
/*                      to the standby                                       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
FsbFIPSessionBulkUpdate (BOOL1 * pbBulkUpdEvtSent)
{
    tFsbFipSessEntry   *pFsbNextFipSessEntry = NULL;
    tFsbFipSessEntry   *pFsbFipSessEntry = NULL;
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4MaxBulkUpdSize = FSB_BULK_SPIT_MSG_SIZE;
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT4               u4NoOfFilter = 0;
    UINT4               u4NoOfFipSessOffset = 0;
    UINT4               u4SyncMsgLenOffset = 0;
    UINT2               u2FipSessMsgLen = 0;
    UINT2               u2BulkUpdCount = FSB_RED_SESS_CNT_PER_BULK_UPDT;
    UINT4               u2NoOfFipSess = 0;
    UINT2               u2SyncMsgLen = 0;

    ProtoEvt.u4AppId = RM_FSB_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (FSB_RED_NEXT_SESS_ENTRY () == NULL)
    {
        FSB_RED_NEXT_SESS_ENTRY () = (tFsbFipSessEntry *)
            RBTreeGetFirst (gFsbGlobals.FsbFipSessTable);
        if (FSB_RED_NEXT_SESS_ENTRY () == NULL)
        {
            /* No entry is present in FsbFipSessEntry Table.
             * Set the bulk updated status as OSIX_TRUE
             * and proceed bulk sync-up of next table.*/
            FSB_RED_FIP_SESS_BULK_UPD_STATUS () = OSIX_TRUE;
            return;
        }
    }

    pFsbNextFipSessEntry = FSB_RED_NEXT_SESS_ENTRY ();

    pMsg = FsbRedGetMsgBuffer ((UINT2) u4MaxBulkUpdSize);
    if (pMsg == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_BUFFER_TRC,
                         "\r\n FsbFIPSessionBulkUpdate: FsbRedGetMsgBuffer Memory Allocation Failed ... 2\r\n");
        return;
    }

    /* Fill the message type and message length */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_FIP_SESS_BULK_UPDT_MSG);
    u4SyncMsgLenOffset = u4Offset;
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);
    /* Fill the number of FIP Session Entries */
    u4NoOfFipSessOffset = u4Offset;
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2NoOfFipSess);

    while ((pFsbNextFipSessEntry != NULL) && (u2BulkUpdCount > 0))
    {
        pFsbFipSessEntry = pFsbNextFipSessEntry;
        if (RBTreeCount (pFsbFipSessEntry->FsbSessFilterEntry,
                         &u4NoOfFilter) != RB_SUCCESS)
        {
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                             "\r\n FsbFIPSessionBulkUpdate:  No more Session entry \r\n");
            RM_FREE (pMsg);
            return;
        }
        u2FipSessMsgLen =
            (UINT2) (FSB_RED_FIP_SESS_MSG_LEN ((UINT2) u4NoOfFilter));
        if ((u4MaxBulkUpdSize - u4Offset) < u2FipSessMsgLen)
        {
            u2SyncMsgLen =
                (UINT2) (u4Offset -
                         (FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE));
            FSB_RM_PUT_2_BYTE (pMsg, &u4SyncMsgLenOffset, u2SyncMsgLen);
            FSB_RM_PUT_2_BYTE (pMsg, &u4NoOfFipSessOffset, u2NoOfFipSess);
            if (FsbRedSendMsgToRm (pMsg, (UINT2) u4Offset) == FSB_FAILURE)
            {
                ProtoEvt.u4Error = RM_SENDTO_FAIL;
                FsbRmHandleProtocolEvent (&ProtoEvt);
            }
            u4Offset = 0;
            u2NoOfFipSess = 0;
            pMsg = FsbRedGetMsgBuffer ((UINT2) u4MaxBulkUpdSize);
            if (pMsg == NULL)
            {
                FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                                 FSB_BUFFER_TRC,
                                 "\r\n FsbFIPSessionBulkUpdate: FsbRedGetMsgBuffer Memory Allocation Failed ... 2\r\n");
                return;
            }
            /* Fill the message type and message length */
            FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_FIP_SESS_BULK_UPDT_MSG);
            u4SyncMsgLenOffset = (UINT2) u4Offset;
            FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);
            u4NoOfFipSessOffset = (UINT2) u4Offset;
            FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2NoOfFipSess);
        }
        FsbRedFormFipSessBulkUpdMsg (pFsbFipSessEntry, pMsg, &u4Offset,
                                     (UINT2) u4NoOfFilter);
        u2NoOfFipSess++;
        pFsbNextFipSessEntry = (tFsbFipSessEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFipSessTable,
                           (tRBElem *) pFsbFipSessEntry, NULL);
        u2BulkUpdCount--;
    }

    u2SyncMsgLen =
        (UINT2) (u4Offset - (FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE));

    FSB_RM_PUT_2_BYTE (pMsg, &u4SyncMsgLenOffset, u2SyncMsgLen);
    FSB_RM_PUT_2_BYTE (pMsg, &u4NoOfFipSessOffset, u2NoOfFipSess);

    if (FsbRedSendMsgToRm (pMsg, (UINT2) u4Offset) == FSB_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        FsbRmHandleProtocolEvent (&ProtoEvt);
    }
    FSB_RED_NEXT_SESS_ENTRY () = pFsbNextFipSessEntry;

    if (FSB_RED_NEXT_SESS_ENTRY () == NULL)
    {
        FSB_RED_FIP_SESS_BULK_UPD_STATUS () = OSIX_TRUE;
        FSB_RED_BULK_UPD_STATUS () = FSB_BULK_UPD_COMPLETED;
    }
    /* Send an event to start the next bulk update */
    OsixEvtSend (FSB_TASK_ID, FSB_RED_BULK_UPD_EVENT);
    *pbBulkUpdEvtSent = OSIX_TRUE;
    return;
}

/*****************************************************************************/
/* Function Name      : FsbRedProcessFipSessBulkUpdate                       */
/*                                                                           */
/* Description        : This function process the received FIP Session Entry */
/*                                                                           */
/* Input(s)           : pMesg     - Pointer to the sync up message.          */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS/FSB_FAILURE                              */
/*****************************************************************************/
INT4
FsbRedProcessFipSessBulkUpdate (tRmMsg * pMesg, UINT4 *pu4Offset,
                                UINT2 *pu2Length)
{
    tFsbFipSessEntry   *pFsbFipSessEntry = NULL;
    tFsbFipSessEntry   *pFsbGetFipSessEntry = NULL;
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tRmProtoEvt         ProtoEvt;
    tPortList           PortList;
    UINT1               au1RBName[OSIX_NAME_LEN + 4];
    UINT4               u4Index = 0;
    UINT4               u4IfIndex = 0;
    UINT2               u2NoOfFipSess = 0;
    UINT2               u2NoOfFilter = 0;
    UINT2               u2MesgSize = 0;

    FSB_MEMSET (PortList, 0, sizeof (tPortList));

    ProtoEvt.u4AppId = RM_FSB_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (FSB_NODE_STATUS () != FSB_NODE_STANDBY)
    {
        /* Not a Standby node, hence don't process the received
         * sync message. */
        return FSB_SUCCESS;
    }

    /* Get the Message size encoded by the FSB module in Active node. */
    FSB_RM_GET_2_BYTE (pMesg, pu4Offset, u2MesgSize);
    *pu2Length = (UINT2) (*pu2Length - FSB_RED_LENGTH_SIZE);

    if (*pu2Length != u2MesgSize)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbRedProcessFipSessBulkUpdate: Invalid Message Length ... \r\n");
        /* Data corruption, hence ignore the whole sync up message. */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        FsbRmHandleProtocolEvent (&ProtoEvt);
        return FSB_FAILURE;
    }
    /* Copy Number of Sync Message from the received TLV */
    FSB_RM_GET_2_BYTE (pMesg, pu4Offset, u2NoOfFipSess);
    while (u2NoOfFipSess != 0)
    {
        pFsbFipSessEntry = (tFsbFipSessEntry *)
            MemAllocMemBlk (FSB_SESSION_ENTRIES_MEMPOOL_ID);
        if (pFsbFipSessEntry == NULL)
        {
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                             FSB_BUFFER_TRC,
                             "\r\nFsbRedProcessFipSessBulkUpdate: Memory Allocation failed for pFsbFipSessEntry... \r\n");
            return FSB_FAILURE;
        }

        FSB_MEMSET (pFsbFipSessEntry, 0, sizeof (tFsbFipSessEntry));

        /* Copy the index for FIP Session Table (VlanId, ENodeIfIndex and ENode Mac Address)
         * received in TLV */
        FSB_RM_GET_2_BYTE (pMesg, pu4Offset, pFsbFipSessEntry->u2VlanId);
        FSB_RM_GET_4_BYTE (pMesg, pu4Offset, pFsbFipSessEntry->u4ENodeIfIndex);
        FSB_RM_GET_N_BYTE (pMesg, pFsbFipSessEntry->ENodeMacAddr, pu4Offset,
                           MAC_ADDR_LEN);
        /* Copy FcMap from the received TLV */
        FSB_RM_GET_N_BYTE (pMesg, pFsbFipSessEntry->au1FcMap, pu4Offset,
                           FSB_FCMAP_LEN);
        /* Copy Name Id from the received TLV */
        FSB_RM_GET_N_BYTE (pMesg, pFsbFipSessEntry->au1NameId, pu4Offset,
                           FSB_NAME_ID_LEN);
        /* Copy Context Id from the received TLV */
        FSB_RM_GET_4_BYTE (pMesg, pu4Offset, (pFsbFipSessEntry->u4ContextId));
        /* Copy FcfIfIndex from the received TLV */
        FSB_RM_GET_4_BYTE (pMesg, pu4Offset, pFsbFipSessEntry->u4FcfIfIndex);
        /* Copy FCoECount from the received TLV */
        FSB_RM_GET_4_BYTE (pMesg, pu4Offset, pFsbFipSessEntry->u4FCoECount);
        FSB_RM_GET_2_BYTE (pMesg, pu4Offset,
                           pFsbFipSessEntry->u2HouseKeepingTimePeriod);
        /* Copy the State of FIP Session from the received TLV */
        FSB_RM_GET_1_BYTE (pMesg, pu4Offset, pFsbFipSessEntry->u1State);

        u4Index = 0;
        FSB_MEMSET (au1RBName, '\0', OSIX_NAME_LEN + 4);
        /* Get the Free Index */
        FsbGetFreeIndex (gFsbGlobals.SessFilterEntryList, &u4Index);
        pFsbFipSessEntry->u4Index = u4Index;

        SNPRINTF ((CHR1 *) au1RBName, OSIX_NAME_LEN + 1, "%s%d",
                  FSB_SESSION_RB_PREFIX_NAME, pFsbFipSessEntry->u4Index);

        pFsbFipSessEntry->FsbSessFilterEntry =
            RBTreeCreateEmbeddedExtended ((FSAP_OFFSETOF (tFsbFilterEntry,
                                                          FsbFilterNode)),
                                          FsbCompareSessFilterEntryIndex,
                                          (UINT1 *) au1RBName);

        if (RBTreeAdd
            (gFsbGlobals.FsbFipSessTable,
             (tRBElem *) pFsbFipSessEntry) == RB_FAILURE)
        {
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                             FSB_DEBUGGING_LEVEL, FSB_OS_RESOURCE_TRC,
                             "FsbRedProcessFipSessBulkUpdate:"
                             " RBTree Add Failed for RB name - %s\r\n",
                             au1RBName);
            FsbResetFreeIndex (gFsbGlobals.SessFilterEntryList,
                               pFsbFipSessEntry->u4Index);
            MemReleaseMemBlock (FSB_SESSION_ENTRIES_MEMPOOL_ID,
                                (UINT1 *) pFsbFipSessEntry);
            return FSB_FAILURE;
        }

        /* Configure Enode Mac as Static Entry since Mac-learing is disabled
         * for all FCoE VLAN */
        if (FsbConfigUcastEntry (pFsbFipSessEntry->u4ContextId,
                                 pFsbFipSessEntry->ENodeMacAddr,
                                 pFsbFipSessEntry->u2VlanId,
                                 pFsbFipSessEntry->u4ENodeIfIndex,
                                 VLAN_CREATE) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                             FSB_NONE,
                             "FsbRedProcessFipSessBulkUpdate: Configuring Static Enode Mac for"
                             " FIP Session Failed \r\n");
            return FSB_FAILURE;
        }

        pFsbGetFipSessEntry = FsbGetFIPSessionEntry (pFsbFipSessEntry->u2VlanId,
                                                     pFsbFipSessEntry->
                                                     u4ENodeIfIndex,
                                                     pFsbFipSessEntry->
                                                     ENodeMacAddr);

        if (pFsbGetFipSessEntry == NULL)
        {
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                             FSB_NONE,
                             "FsbRedProcessFipSessBulkUpdate: FsbGetFIPSessionEntry failed for"
                             "Vlan: %d, ENodeIfIndex: %d and ENodeMAC: "
                             "%02x:%02x:%02x:%02x:%02x:%02x\r\n",
                             pFsbFipSessEntry->u2VlanId,
                             pFsbFipSessEntry->u4ENodeIfIndex,
                             pFsbFipSessEntry->ENodeMacAddr[0],
                             pFsbFipSessEntry->ENodeMacAddr[1],
                             pFsbFipSessEntry->ENodeMacAddr[2],
                             pFsbFipSessEntry->ENodeMacAddr[3],
                             pFsbFipSessEntry->ENodeMacAddr[4],
                             pFsbFipSessEntry->ENodeMacAddr[5]);
            return FSB_FAILURE;
        }
        /* Copy Number of Filter Entries from the received TLV */
        FSB_RM_GET_2_BYTE (pMesg, pu4Offset, u2NoOfFilter);
        while (u2NoOfFilter != 0)
        {
            pFsbFilterEntry = MemAllocMemBlk (FSB_FILTER_ENTRIES_MEMPOOL_ID);

            if (pFsbFilterEntry == NULL)
            {
                FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                                 FSB_BUFFER_TRC,
                                 "\r\nFsbRedProcessFipSessBulkUpdate: Memory Allocation failed for pFsbFilterEntry... \r\n");
                return FSB_FAILURE;
            }

            FSB_MEMSET (pFsbFilterEntry, 0, sizeof (tFsbFilterEntry));

            /* Copy the index for FIP Session Filter Entry(DstMac, SrcMac, Opcode, SubOpcode)
             * from the received TLV */
            FSB_RM_GET_N_BYTE (pMesg, pFsbFilterEntry->DstMac, pu4Offset,
                               MAC_ADDR_LEN);
            FSB_RM_GET_N_BYTE (pMesg, pFsbFilterEntry->SrcMac, pu4Offset,
                               MAC_ADDR_LEN);
            FSB_RM_GET_2_BYTE (pMesg, pu4Offset,
                               pFsbFilterEntry->u2OpcodeFilterOffsetValue);
            FSB_RM_GET_2_BYTE (pMesg, pu4Offset,
                               pFsbFilterEntry->u2SubOpcodeFilterOffsetValue);
            /* Copy the IfIndex from the received TLV to populate the portlist */
            FSB_RM_GET_4_BYTE (pMesg, pu4Offset, u4IfIndex);
            /* Copy AggIndex from the received TLV */
            FSB_RM_GET_4_BYTE (pMesg, pu4Offset, pFsbFilterEntry->u4AggIndex);
            FSB_RM_GET_2_BYTE (pMesg, pu4Offset, pFsbFilterEntry->u2ClassId);
            /* Copy Sw and Hw Filter Id from the received TLV */
            FSB_RM_GET_4_BYTE (pMesg, pu4Offset, pFsbFilterEntry->u4FilterId);
            FSB_RM_GET_4_BYTE (pMesg, pu4Offset, pFsbFilterEntry->u4HwFilterId);
            OSIX_BITLIST_SET_BIT (PortList, pFsbFilterEntry->u4IfIndex,
                                  sizeof (tPortList));
            FSB_MEMCPY (pFsbFilterEntry->PortList, PortList,
                        sizeof (tPortList));
            FSB_MEMCPY (pFsbFilterEntry->au1FcMap,
                        pFsbGetFipSessEntry->au1FcMap, FSB_FCMAP_LEN);
            FSB_MEMCPY (pFsbFilterEntry->au1NameId,
                        pFsbGetFipSessEntry->au1NameId, FSB_NAME_ID_LEN);
            pFsbFilterEntry->u4ContextId = pFsbGetFipSessEntry->u4ContextId;
            pFsbFilterEntry->u2VlanId = pFsbGetFipSessEntry->u2VlanId;
            pFsbFilterEntry->u2Ethertype = FIP_ETHER_TYPE;
            pFsbFilterEntry->u1Priority = FSB_FIELD_ENTRY_PRIO_HIGHEST;
            pFsbFilterEntry->u1FilterAction = FSB_FILTER_ALLOW_AND_COPYTOCPU;
            pFsbFilterEntry->u1FilterType = FSB_FILTER_PORT_MODE;

            if (RBTreeAdd (pFsbGetFipSessEntry->FsbSessFilterEntry,
                           (tRBElem *) pFsbFilterEntry) != RB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbGetFipSessEntry->u4ContextId,
                                 FSB_DEBUGGING_LEVEL, FSB_OS_RESOURCE_TRC,
                                 "FsbRedProcessFipSessBulkUpdate:"
                                 " RBTreeAdd failed \r\n");
                MemReleaseMemBlock (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                                    (UINT1 *) pFsbFilterEntry);
                return FSB_FAILURE;
            }
            u2NoOfFilter--;
        }
        u2NoOfFipSess--;
    }
    return FSB_SUCCESS;
}

/******************************************************************************/
/* Function Name      : FsbRedFormFCoEBulkUpdMsg                              */
/*                                                                            */
/* Description        : This function is used to form FCoE Entries Bulk       */
/*                      Update Message to be sent to the standby              */
/*                                                                            */
/* Input(s)           : pFsbFipSessFCoEEntry - Pointer to FsbFipSessFCoEEntry */
/*                      pMsg                 - Pointer to RM Message          */
/*                      pu4Offset            - Pointer to Offset              */
/*                                                                            */
/* Output(s)          : None                                                  */
/*                                                                            */
/* Global Variables                                                           */
/* Referred           : None.                                                 */
/*                                                                            */
/* Global Variables                                                           */
/* Modified           : None.                                                 */
/*                                                                            */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                            */
/******************************************************************************/
VOID
FsbRedFormFCoEBulkUpdMsg (tFsbFipSessFCoEEntry * pFsbFipSessFCoEEntry,
                          tRmMsg * pMsg, UINT4 *pu4Offset, UINT2 u2NoOfFilter)
{
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;

    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    /* Fill the index(VlanId, ENodeIfIndex, ENodeMac, FcfMac, FCoEMac) of FCoE Table */
    FSB_RM_PUT_2_BYTE (pMsg, pu4Offset, pFsbFipSessFCoEEntry->u2VlanId);
    FSB_RM_PUT_4_BYTE (pMsg, pu4Offset, pFsbFipSessFCoEEntry->u4ENodeIfIndex);
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFipSessFCoEEntry->ENodeMacAddr, pu4Offset,
                       MAC_ADDR_LEN);
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFipSessFCoEEntry->FcfMacAddr, pu4Offset,
                       MAC_ADDR_LEN);
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFipSessFCoEEntry->FCoEMacAddr, pu4Offset,
                       MAC_ADDR_LEN);
    /* Fill other entries in FCoE Table */
    FSB_RM_PUT_N_BYTE (pMsg, pFsbFipSessFCoEEntry->au1FcfId, pu4Offset,
                       FSB_FCID_LEN);
    FSB_RM_PUT_1_BYTE (pMsg, pu4Offset, pFsbFipSessFCoEEntry->u1EnodeConnType);
    FSB_RM_PUT_2_BYTE (pMsg, pu4Offset,
                       pFsbFipSessFCoEEntry->u2HouseKeepingTimePeriod);
    FSB_RM_PUT_1_BYTE (pMsg, pu4Offset,
                       pFsbFipSessFCoEEntry->u1HouseKeepingTimerFlag);
    FSB_RM_PUT_1_BYTE (pMsg, pu4Offset,
                       pFsbFipSessFCoEEntry->u1SyncTimerStatus);

    /* Fill No of filters in FCoE Table */
    FSB_RM_PUT_2_BYTE (pMsg, pu4Offset, u2NoOfFilter);

    pFsbFilterEntry = (tFsbFilterEntry *)
        RBTreeGetFirst (pFsbFipSessFCoEEntry->FsbSessFCoEFilterEntry);
    if (pFsbFilterEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_OS_RESOURCE_TRC,
                         "\r\nFsbRedFormFCoEBulkUpdMsg: pFsbFilterEntry is NULL... \r\n");
        return;
    }

    do
    {
        FSB_MEMCPY (FsbFilterEntry.DstMac, pFsbFilterEntry->DstMac,
                    MAC_ADDR_LEN);
        FSB_MEMCPY (FsbFilterEntry.SrcMac, pFsbFilterEntry->SrcMac,
                    MAC_ADDR_LEN);
        FsbFilterEntry.u2OpcodeFilterOffsetValue =
            pFsbFilterEntry->u2OpcodeFilterOffsetValue;
        FsbFilterEntry.u2SubOpcodeFilterOffsetValue =
            pFsbFilterEntry->u2SubOpcodeFilterOffsetValue;
        /* Fill the index (DstMac, SrcMac, Opcode, SubOpcode) of 
         * Filter Entry in FCoE Table */
        FSB_RM_PUT_N_BYTE (pMsg, pFsbFilterEntry->DstMac, pu4Offset,
                           MAC_ADDR_LEN);
        FSB_RM_PUT_N_BYTE (pMsg, pFsbFilterEntry->SrcMac, pu4Offset,
                           MAC_ADDR_LEN);
        FSB_RM_PUT_2_BYTE (pMsg, pu4Offset,
                           pFsbFilterEntry->u2OpcodeFilterOffsetValue);
        FSB_RM_PUT_2_BYTE (pMsg, pu4Offset,
                           pFsbFilterEntry->u2SubOpcodeFilterOffsetValue);
        FSB_RM_PUT_4_BYTE (pMsg, pu4Offset, pFsbFilterEntry->u4IfIndex);
        FSB_RM_PUT_2_BYTE (pMsg, pu4Offset, pFsbFilterEntry->u2Ethertype);
        FSB_RM_PUT_1_BYTE (pMsg, pu4Offset, pFsbFilterEntry->u1FilterAction);
        FSB_RM_PUT_4_BYTE (pMsg, pu4Offset, pFsbFilterEntry->u4AggIndex);
        FSB_RM_PUT_2_BYTE (pMsg, pu4Offset, pFsbFilterEntry->u2ClassId);
        FSB_RM_PUT_4_BYTE (pMsg, pu4Offset, pFsbFilterEntry->u4FilterId);
        FSB_RM_PUT_4_BYTE (pMsg, pu4Offset, pFsbFilterEntry->u4HwFilterId);

        pFsbFilterEntry = (tFsbFilterEntry *)
            RBTreeGetNext (pFsbFipSessFCoEEntry->FsbSessFCoEFilterEntry,
                           (tRBElem *) & FsbFilterEntry, NULL);
    }
    while (pFsbFilterEntry != NULL);
}

/*****************************************************************************/
/* Function Name      : FsbFCoEBulkUpdate                                    */
/*                                                                           */
/* Description        : This function will send FCoE Bulk update             */
/*                      to the standby                                       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
FsbFCoEBulkUpdate (BOOL1 * pbBulkUpdEvtSent)
{
    tFsbFipSessFCoEEntry *pFsbNextFipSessFCoEEntry = NULL;
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4MaxBulkUpdSize = FSB_BULK_SPIT_MSG_SIZE;
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT4               u4NoOfFilter = 0;
    UINT2               u2BulkUpdCount = FSB_RED_FCOE_CNT_PER_BULK_UPDT;
    UINT4               u4SyncMsgLenOffset = 0;
    UINT4               u4NoOfFCoEOffset = 0;
    UINT2               u2FipSessMsgLen = 0;
    UINT2               u2SyncMsgLen = 0;
    UINT2               u2NoOfFCoE = 0;

    ProtoEvt.u4AppId = RM_FSB_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (FSB_RED_NEXT_FCOE_ENTRY () == NULL)
    {
        FSB_RED_NEXT_FCOE_ENTRY () = (tFsbFipSessFCoEEntry *)
            RBTreeGetFirst (gFsbGlobals.FsbFipSessFCoETable);
        if (FSB_RED_NEXT_FCOE_ENTRY () == NULL)
        {
            /* No entry is present in FsbFipSessFCoEEntry Table.
             * Set the bulk updated status as OSIX_TRUE
             * and proceed bulk sync-up of next table.*/
            FSB_RED_FCOE_BULK_UPD_STATUS () = OSIX_TRUE;
            return;
        }
    }
    pFsbNextFipSessFCoEEntry = FSB_RED_NEXT_FCOE_ENTRY ();

    pMsg = FsbRedGetMsgBuffer ((UINT2) u4MaxBulkUpdSize);
    if (pMsg == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_BUFFER_TRC,
                         "\r\nFsbFCoEBulkUpdate: FsbRedGetMsgBuffer returns NULL... \r\n");
        return;
    }

    /* Fill the message type and message length */
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_FCOE_BULK_UPDT_MSG);
    u4SyncMsgLenOffset = u4Offset;
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);
    /* Fill the number of FIP Session Entries */
    u4NoOfFCoEOffset = u4Offset;
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2NoOfFCoE);

    while ((pFsbNextFipSessFCoEEntry != NULL) && (u2BulkUpdCount > 0))
    {
        pFsbFipSessFCoEEntry = pFsbNextFipSessFCoEEntry;
        if (RBTreeCount (pFsbFipSessFCoEEntry->FsbSessFCoEFilterEntry,
                         &u4NoOfFilter) != RB_SUCCESS)
        {
            RM_FREE (pMsg);
            return;
        }
        u2FipSessMsgLen = (UINT2) (FSB_RED_FCOE_MSG_LEN ((UINT2) u4NoOfFilter));
        if ((u4MaxBulkUpdSize - u4Offset) < u2FipSessMsgLen)
        {
            u2SyncMsgLen =
                (UINT2) (u4Offset -
                         (FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE));
            FSB_RM_PUT_2_BYTE (pMsg, &u4SyncMsgLenOffset, u2SyncMsgLen);
            FSB_RM_PUT_2_BYTE (pMsg, &u4NoOfFCoEOffset, u2NoOfFCoE);
            if (FsbRedSendMsgToRm (pMsg, (UINT2) u4Offset) == FSB_FAILURE)
            {
                ProtoEvt.u4Error = RM_SENDTO_FAIL;
                FsbRmHandleProtocolEvent (&ProtoEvt);
            }
            u4Offset = 0;
            u2NoOfFCoE = 0;
            pMsg = FsbRedGetMsgBuffer ((UINT2) u4MaxBulkUpdSize);
            if (pMsg == NULL)
            {
                FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                                 FSB_BUFFER_TRC,
                                 "\r\nFsbFCoEBulkUpdate: FsbRedGetMsgBuffer returns NULL... \r\n");
                return;
            }
            /* Fill the message type and message length */
            FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_FCOE_BULK_UPDT_MSG);
            u4SyncMsgLenOffset = u4Offset;
            FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);
            u4NoOfFCoEOffset = u4Offset;
            FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, u2NoOfFCoE);
        }
        FsbRedFormFCoEBulkUpdMsg (pFsbNextFipSessFCoEEntry, pMsg, &u4Offset,
                                  (UINT2) u4NoOfFilter);
        u2NoOfFCoE++;
        pFsbNextFipSessFCoEEntry = (tFsbFipSessFCoEEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFipSessFCoETable,
                           (tRBElem *) pFsbFipSessFCoEEntry, NULL);
        u2BulkUpdCount--;
    }

    u2SyncMsgLen =
        (UINT2) (u4Offset - (FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE));

    FSB_RM_PUT_2_BYTE (pMsg, &u4SyncMsgLenOffset, u2SyncMsgLen);
    FSB_RM_PUT_2_BYTE (pMsg, &u4NoOfFCoEOffset, u2NoOfFCoE);

    if (FsbRedSendMsgToRm (pMsg, (UINT2) u4Offset) == FSB_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        FsbRmHandleProtocolEvent (&ProtoEvt);
    }
    FSB_RED_NEXT_FCOE_ENTRY () = pFsbNextFipSessFCoEEntry;

    if (FSB_RED_NEXT_FCOE_ENTRY () == NULL)
    {
        FSB_RED_FCOE_BULK_UPD_STATUS () = OSIX_TRUE;
        FSB_RED_BULK_UPD_STATUS () = FSB_BULK_UPD_COMPLETED;
    }
    /* Send an event to start the next bulk update */
    OsixEvtSend (FSB_TASK_ID, FSB_RED_BULK_UPD_EVENT);
    *pbBulkUpdEvtSent = OSIX_TRUE;
    return;
}

/*****************************************************************************/
/* Function Name      : FsbRedProcessFCoEBulkUpdate                          */
/*                                                                           */
/* Description        : This function process the received FCoE Entries      */
/*                                                                           */
/* Input(s)           : pMesg     - Pointer to the sync up message.          */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS/FSB_FAILURE                              */
/*****************************************************************************/
INT4
FsbRedProcessFCoEBulkUpdate (tRmMsg * pMesg, UINT4 *pu4Offset, UINT2 *pu2Length)
{
    tFsbFipSessFCoEEntry *pFsbGetFipSessFCoEEntry = NULL;
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;
    tFsbFipSessEntry   *pFsbFipSessEntry = NULL;
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFcfEntry       *pFsbFcfEntry = NULL;
    tFsbFcfEntry        FsbFcfEntry;
    tRmProtoEvt         ProtoEvt;
    tPortList           PortList;
    UINT1               au1RBName[OSIX_NAME_LEN + 4];
    UINT4               u4Index = 0;
    UINT2               u2NoOfFCoE = 0;
    UINT2               u2NoOfFilter = 0;
    UINT2               u2MesgSize = 0;

    FSB_MEMSET (PortList, 0, sizeof (tPortList));
    FSB_MEMSET (&FsbFcfEntry, 0, sizeof (tFsbFcfEntry));

    ProtoEvt.u4AppId = RM_FSB_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (FSB_NODE_STATUS () != FSB_NODE_STANDBY)
    {
        /* Not a Standby node, hence don't process the received
         * sync message. */
        return FSB_SUCCESS;
    }

    /* Get the Message size encoded by the FSB module in Active node. */
    FSB_RM_GET_2_BYTE (pMesg, pu4Offset, u2MesgSize);
    *pu2Length = (UINT2) (*pu2Length - FSB_RED_LENGTH_SIZE);

    if (*pu2Length != u2MesgSize)
    {

        /* Data corruption, hence ignore the whole sync up message. */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        FsbRmHandleProtocolEvent (&ProtoEvt);

        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbRedProcessFCoEBulkUpdate () - Invalid message size ... \r\n");

        return FSB_FAILURE;
    }
    /* Copy Number of Sync Message from the received TLV */
    FSB_RM_GET_2_BYTE (pMesg, pu4Offset, u2NoOfFCoE);
    while (u2NoOfFCoE != 0)
    {
        pFsbFipSessFCoEEntry = (tFsbFipSessFCoEEntry *)
            MemAllocMemBlk (FSB_FCOE_ENTRIES_MEMPOOL_ID);
        if (pFsbFipSessFCoEEntry == NULL)
        {
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                             FSB_BUFFER_TRC,
                             "\r\nFsbRedProcessFCoEBulkUpdate: Memory Allocation failed for pFsbFipSessFCoEEntry... \r\n");
            return FSB_FAILURE;
        }

        FSB_MEMSET (pFsbFipSessFCoEEntry, 0, sizeof (tFsbFipSessFCoEEntry));

        /* Copy the index for FCoE Table (VlanId, ENodeIfIndex, ENode Mac, Fcf Mac and FCoE Mac)
         * received in TLV */
        FSB_RM_GET_2_BYTE (pMesg, pu4Offset, pFsbFipSessFCoEEntry->u2VlanId);
        FSB_RM_GET_4_BYTE (pMesg, pu4Offset,
                           pFsbFipSessFCoEEntry->u4ENodeIfIndex);
        FSB_RM_GET_N_BYTE (pMesg, pFsbFipSessFCoEEntry->ENodeMacAddr, pu4Offset,
                           MAC_ADDR_LEN);
        FSB_RM_GET_N_BYTE (pMesg, pFsbFipSessFCoEEntry->FcfMacAddr, pu4Offset,
                           MAC_ADDR_LEN);
        FSB_RM_GET_N_BYTE (pMesg, pFsbFipSessFCoEEntry->FCoEMacAddr, pu4Offset,
                           MAC_ADDR_LEN);
        /* Copy FcfId from the received TLV */
        FSB_RM_GET_N_BYTE (pMesg, pFsbFipSessFCoEEntry->au1FcfId, pu4Offset,
                           FSB_FCID_LEN);
        /* Copy the ENode Connect Type from the received TLV */
        FSB_RM_GET_1_BYTE (pMesg, pu4Offset,
                           pFsbFipSessFCoEEntry->u1EnodeConnType);
        FSB_RM_GET_2_BYTE (pMesg, pu4Offset,
                           pFsbFipSessFCoEEntry->u2HouseKeepingTimePeriod);
        FSB_RM_GET_1_BYTE (pMesg, pu4Offset,
                           pFsbFipSessFCoEEntry->u1HouseKeepingTimerFlag);
        FSB_RM_GET_1_BYTE (pMesg, pu4Offset,
                           pFsbFipSessFCoEEntry->u1SyncTimerStatus);
        pFsbFipSessEntry =
            FsbGetFIPSessionEntry (pFsbFipSessFCoEEntry->u2VlanId,
                                   pFsbFipSessFCoEEntry->u4ENodeIfIndex,
                                   pFsbFipSessFCoEEntry->ENodeMacAddr);
        if (pFsbFipSessEntry == NULL)
        {
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FCoE Bulk Update: pFsbFipSessEntry is NULL\r\n");
            MemReleaseMemBlock (FSB_FCOE_ENTRIES_MEMPOOL_ID,
                                (UINT1 *) pFsbFipSessFCoEEntry);
            return FSB_FAILURE;
        }
        pFsbFipSessEntry->u1HouseKeepingTimerFlag =
            pFsbFipSessFCoEEntry->u1HouseKeepingTimerFlag;
        pFsbFipSessFCoEEntry->pFsbFipSessEntry = pFsbFipSessEntry;

        /* Get the Free Index */
        u4Index = 0;
        FSB_MEMSET (au1RBName, '\0', OSIX_NAME_LEN + 4);

        FsbGetFreeIndex (gFsbGlobals.FcoeFilterEntryList, &u4Index);
        pFsbFipSessFCoEEntry->u4Index = u4Index;

        SNPRINTF ((CHR1 *) au1RBName, OSIX_NAME_LEN + 1, "%s%d",
                  FSB_FCOE_RB_PREFIX_NAME, pFsbFipSessFCoEEntry->u4Index);

        pFsbFipSessFCoEEntry->FsbSessFCoEFilterEntry =
            RBTreeCreateEmbeddedExtended ((FSAP_OFFSETOF (tFsbFilterEntry,
                                                          FsbFilterNode)),
                                          FsbCompareSessFilterEntryIndex,
                                          (UINT1 *) au1RBName);

        if (RBTreeAdd (gFsbGlobals.FsbFipSessFCoETable,
                       (tRBElem *) pFsbFipSessFCoEEntry) == RB_FAILURE)
        {
            FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                             u4ContextId, FSB_DEBUGGING_LEVEL,
                             FSB_OS_RESOURCE_TRC,
                             "FsbRedProcessFCoEBulkUpdate:"
                             " RBTree Add Failed for RB name - %s \r\n",
                             au1RBName);
            FsbResetFreeIndex (gFsbGlobals.FcoeFilterEntryList,
                               pFsbFipSessFCoEEntry->u4Index);
            MemReleaseMemBlock (FSB_FCOE_ENTRIES_MEMPOOL_ID,
                                (UINT1 *) pFsbFipSessFCoEEntry);
            return FSB_FAILURE;
        }
        /* Configure FCoE Mac Address as Static Entry */
        if (FsbConfigUcastEntry (pFsbFipSessEntry->u4ContextId,
                                 pFsbFipSessFCoEEntry->FCoEMacAddr,
                                 pFsbFipSessFCoEEntry->u2VlanId,
                                 pFsbFipSessFCoEEntry->u4ENodeIfIndex,
                                 VLAN_CREATE) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbRedProcessFCoEBulkUpdate: "
                             "Configuring Static FCoE Mac failed\r\n");
            return FSB_FAILURE;
        }

        FsbFcfEntry.u4FcfIfIndex = pFsbFipSessEntry->u4FcfIfIndex;
        FsbFcfEntry.u2VlanId = pFsbFipSessEntry->u2VlanId;
        FSB_MEMCPY (FsbFcfEntry.FcfMacAddr, pFsbFipSessFCoEEntry->FcfMacAddr,
                    MAC_ADDR_LEN);

        pFsbFcfEntry = (tFsbFcfEntry *)
            RBTreeGet (gFsbGlobals.FsbFcfTable, (tRBElem *) & FsbFcfEntry);

        /* After obtaining the entry, increment the ENodeLoginCount variable */
        if (pFsbFcfEntry != NULL)
        {
            pFsbFcfEntry->u4EnodeLoginCount++;
        }

        pFsbGetFipSessFCoEEntry =
            FsbGetFIPSessionFCoEEntry (pFsbFipSessFCoEEntry->u2VlanId,
                                       pFsbFipSessFCoEEntry->u4ENodeIfIndex,
                                       pFsbFipSessFCoEEntry->ENodeMacAddr,
                                       pFsbFipSessFCoEEntry->FcfMacAddr,
                                       pFsbFipSessFCoEEntry->FCoEMacAddr);

        if (pFsbGetFipSessFCoEEntry == NULL)
        {
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                             "\r\n FsbRedProcessFCoEBulkUpdate:FsbGetFIPSessionFCoEEntry returns failure  ... \r\n");
            return FSB_FAILURE;
        }

        /* Copy Number of Filter Entries from the received TLV */
        FSB_RM_GET_2_BYTE (pMesg, pu4Offset, u2NoOfFilter);
        while (u2NoOfFilter != 0)
        {
            pFsbFilterEntry = MemAllocMemBlk (FSB_FILTER_ENTRIES_MEMPOOL_ID);

            if (pFsbFilterEntry == NULL)
            {
                FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                                 FSB_BUFFER_TRC,
                                 "\r\n FsbRedProcessFCoEBulkUpdate: MemAllocMemBlk  returns failure  ... \r\n");
                return FSB_FAILURE;
            }

            FSB_MEMSET (pFsbFilterEntry, 0, sizeof (tFsbFilterEntry));

            /* Copy the index for FCoE Filter Entry(DstMac, SrcMac, Opcode, SubOpcode)
             * from the received TLV */
            FSB_RM_GET_N_BYTE (pMesg, pFsbFilterEntry->DstMac, pu4Offset,
                               MAC_ADDR_LEN);
            FSB_RM_GET_N_BYTE (pMesg, pFsbFilterEntry->SrcMac, pu4Offset,
                               MAC_ADDR_LEN);
            FSB_RM_GET_2_BYTE (pMesg, pu4Offset,
                               pFsbFilterEntry->u2OpcodeFilterOffsetValue);
            FSB_RM_GET_2_BYTE (pMesg, pu4Offset,
                               pFsbFilterEntry->u2SubOpcodeFilterOffsetValue);
            /* Copy the IfIndex from the received TLV to populate the portlist */
            FSB_RM_GET_4_BYTE (pMesg, pu4Offset, pFsbFilterEntry->u4IfIndex);
            /* Copy EtherType from the received TLV */
            FSB_RM_GET_2_BYTE (pMesg, pu4Offset, pFsbFilterEntry->u2Ethertype);
            /* Copy Filter Action from the received TLV */
            FSB_RM_GET_1_BYTE (pMesg, pu4Offset,
                               pFsbFilterEntry->u1FilterAction);
            /* Copy AggIndex from the received TLV */
            FSB_RM_GET_4_BYTE (pMesg, pu4Offset, pFsbFilterEntry->u4AggIndex);
            /* Copy ClassId from the received TLV */
            FSB_RM_GET_2_BYTE (pMesg, pu4Offset, pFsbFilterEntry->u2ClassId);
            /* Copy Sw and Hw Filter Id from the received TLV */
            FSB_RM_GET_4_BYTE (pMesg, pu4Offset, pFsbFilterEntry->u4FilterId);
            FSB_RM_GET_4_BYTE (pMesg, pu4Offset, pFsbFilterEntry->u4HwFilterId);
            OSIX_BITLIST_SET_BIT (PortList, pFsbFilterEntry->u4IfIndex,
                                  sizeof (tPortList));
            FSB_MEMCPY (pFsbFilterEntry->PortList, PortList,
                        sizeof (tPortList));
            FSB_MEMCPY (pFsbFilterEntry->au1FcMap, pFsbFipSessEntry->au1FcMap,
                        FSB_FCMAP_LEN);
            FSB_MEMCPY (pFsbFilterEntry->au1NameId, pFsbFipSessEntry->au1NameId,
                        FSB_NAME_ID_LEN);
            pFsbFilterEntry->u4ContextId = pFsbFipSessEntry->u4ContextId;
            pFsbFilterEntry->u2VlanId = pFsbGetFipSessFCoEEntry->u2VlanId;
            pFsbFilterEntry->u1Priority = FSB_FIELD_ENTRY_PRIO_HIGHEST;
            pFsbFilterEntry->u1FilterType = FSB_FILTER_PORT_MODE;

            if (RBTreeAdd (pFsbGetFipSessFCoEEntry->FsbSessFCoEFilterEntry,
                           (tRBElem *) pFsbFilterEntry) != RB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_DEBUGGING_LEVEL, FSB_OS_RESOURCE_TRC,
                                 "FsbRedProcessFCoEBulkUpdate:"
                                 " RBTreeAdd failed \r\n");
                MemReleaseMemBlock (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                                    (UINT1 *) pFsbFilterEntry);
                return FSB_FAILURE;
            }
            u2NoOfFilter--;
        }
        u2NoOfFCoE--;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedStartFcfTimers                                 */
/*                                                                           */
/* Description        : This function starts Fcf timers when the Standby     */
/*                      node becomes Active                                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
FsbRedStartFcfTimer ()
{
    tFsbFcfEntry       *pFsbFcfEntry;
    tFsbFcfEntry        FsbFcfEntry;

    pFsbFcfEntry = (tFsbFcfEntry *) RBTreeGetFirst (gFsbGlobals.FsbFcfTable);

    if (pFsbFcfEntry == NULL)
    {
        /* No FsbFcfEntry were created and therefore
         * no need to start timers. */
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_OS_RESOURCE_TRC,
                         "\r\nFsbRedStartFcfTimer() - No Entry in FsbFcfTable ... \r\n");
        return;
    }
    do
    {
        FsbFcfEntry.u2VlanId = pFsbFcfEntry->u2VlanId;
        FsbFcfEntry.u4FcfIfIndex = pFsbFcfEntry->u4FcfIfIndex;
        FSB_MEMCPY (&FsbFcfEntry.FcfMacAddr, pFsbFcfEntry->FcfMacAddr,
                    MAC_ADDR_LEN);

        if (FsbTmrStartTimer (&(pFsbFcfEntry->FCFAliveTimer),
                              FSB_FCF_KEEP_ALIVE_TIMER_ID,
                              pFsbFcfEntry->u4FcfKeepAliveTimerValue,
                              pFsbFcfEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                             "\r\nFsbRedStartFcfTimer() - FsbTmrStartTimer failed ... \r\n");
            return;
        }
        pFsbFcfEntry = (tFsbFcfEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFcfTable,
                           (tRBElem *) & FsbFcfEntry, NULL);
    }
    while (pFsbFcfEntry != NULL);
    return;
}

/*****************************************************************************/
/* Function Name      : FsbRedStartFIPFCoESessionTimers                      */
/*                                                                           */
/* Description        : This function starts FCoE Session timers when the    */
/*                      Standby node becomes Active                          */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
FsbRedStartFIPFCoESessionTimers ()
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;
    tFsbFipSessFCoEEntry FsbFipSessFCoEEntry;

    FSB_MEMSET (&FsbFipSessFCoEEntry, 0, sizeof (tFsbFipSessFCoEEntry));

    pFsbFipSessFCoEEntry = (tFsbFipSessFCoEEntry *)
        RBTreeGetFirst (gFsbGlobals.FsbFipSessFCoETable);
    if (pFsbFipSessFCoEEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_OS_RESOURCE_TRC,
                         "\r\nFsbRedStartFIPFCoESessionTimers - No Entry in FsbFipSessFCoETable ... \r\n");
        return;
    }
    do
    {
        FsbFipSessFCoEEntry.u2VlanId = pFsbFipSessFCoEEntry->u2VlanId;
        FsbFipSessFCoEEntry.u4ENodeIfIndex =
            pFsbFipSessFCoEEntry->u4ENodeIfIndex;
        FSB_MEMCPY (FsbFipSessFCoEEntry.ENodeMacAddr,
                    pFsbFipSessFCoEEntry->ENodeMacAddr, MAC_ADDR_LEN);
        FSB_MEMCPY (FsbFipSessFCoEEntry.FcfMacAddr,
                    pFsbFipSessFCoEEntry->FcfMacAddr, MAC_ADDR_LEN);
        FSB_MEMCPY (FsbFipSessFCoEEntry.FCoEMacAddr,
                    pFsbFipSessFCoEEntry->FCoEMacAddr, MAC_ADDR_LEN);

        pFsbFipSessFCoEEntry->u1HouseKeepingTimerFlag =
            pFsbFipSessFCoEEntry->u1SyncTimerStatus;
        pFsbFipSessFCoEEntry->pFsbFipSessEntry->u1HouseKeepingTimerFlag =
            pFsbFipSessFCoEEntry->u1SyncTimerStatus;
        if (pFsbFipSessFCoEEntry->u1SyncTimerStatus == FSB_TRUE)
        {
            if (pFsbFipSessFCoEEntry->u1EnodeConnType == FSB_FIP_FLOGI)
            {
                /* Start ENode House Keeping Timer only  when the 
                 * ENode Connect Type is FLOGI. */
                if (FsbTmrStartTimer
                    (&
                     (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                      FsbHouseKeepingTmr), FSB_ENODE_HOUSE_KEEPING_TIMER_ID,
                     pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                     u2HouseKeepingTimePeriod,
                     pFsbFipSessFCoEEntry->pFsbFipSessEntry) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                                     u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbRedStartFIPFCoESessionTimers : ENode House Keeping Timer"
                                     "FsbTmrStartTimer failed\r\n");
                    return;
                }
            }
            else
            {
                /* Start VNMac House Keeping Timer */
                if (FsbTmrStartTimer
                    (&(pFsbFipSessFCoEEntry->FsbHouseKeepingTmr),
                     FSB_VNMAC_HOUSE_KEEPING_TIMER_ID,
                     pFsbFipSessFCoEEntry->u2HouseKeepingTimePeriod,
                     pFsbFipSessFCoEEntry) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                                     u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbRedStartFIPFCoESessionTimers : VNMac House Keeping Timer"
                                     "FsbTmrStartTimer failed\r\n");
                    return;
                }
            }
            /* This object is to indicate the administrator whether the
             * house-keeping timer functionality is enabled in the system */
            pFsbFipSessFCoEEntry->pFsbFipSessEntry->u1HouseKeepingTimerStatus =
                FSB_TRUE;
            pFsbFipSessFCoEEntry->u1HouseKeepingTimerStatus = FSB_TRUE;
        }
        pFsbFipSessFCoEEntry = (tFsbFipSessFCoEEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFipSessFCoETable,
                           (tRBElem *) & FsbFipSessFCoEEntry, NULL);
    }
    while (pFsbFipSessFCoEEntry != NULL);
}

/*****************************************************************************/
/* Function Name      : FsbRedSyncUpOperStatus                               */
/*                                                                           */
/* Description        : This function sends the Oper Status change           */
/*                      indication to the Standby.                           */
/*                                                                           */
/* Input(s)           : u4IfIndex    - IfIndex                               */
/*                      u1OperStatus - Oper Status                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpOperStatus (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = FSB_RM_OFFSET;
    UINT2               u2MaxBufSize = 0;

    if ((FSB_IS_STANDBY_UP () == FSB_FALSE) ||
        (FSB_NODE_STATUS () != FSB_NODE_ACTIVE))
    {
        return FSB_SUCCESS;
    }

    u2MaxBufSize =
        FSB_SYNC_MSG_TYPE_SIZE + FSB_RED_LENGTH_SIZE +
        FSB_SYN_OPER_STATUS_MSG_SIZE;
    pMsg = FsbRedGetMsgBuffer (u2MaxBufSize);
    if (pMsg == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_BUFFER_TRC,
                         "\r\nFsbRedSyncUpOperStatus: FsbRedGetMsgBuffer() returns failure  ... \r\n");
        return FSB_FAILURE;
    }

    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, FSB_SYN_OPER_STATUS_MSG);
    FSB_RM_PUT_2_BYTE (pMsg, &u4Offset, FSB_SYN_OPER_STATUS_MSG_SIZE);
    FSB_RM_PUT_4_BYTE (pMsg, &u4Offset, u4IfIndex);
    FSB_RM_PUT_1_BYTE (pMsg, &u4Offset, u1OperStatus);

    if (FsbRedSendMsgToRm (pMsg, (UINT2) u4Offset) == FSB_FAILURE)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nnFsbRedSyncUpOperStatus: FsbRedSendMsgToRm returns failure ... \r\n");
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedProcessOperStatus                              */
/*                                                                           */
/* Description        : This function process the Oper Status Change         */
/*                      in the Standby Node.                                 */
/*                                                                           */
/* Input(s)           : pMesg     - Pointer to the sync up message.          */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS/FSB_FAILURE                              */
/*****************************************************************************/
INT4
FsbRedProcessOperStatus (tRmMsg * pMesg, UINT4 *pu4Offset, UINT2 *pu2Length)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = 0;
    UINT2               u2MesgSize = 0;
    UINT1               u1OperStatus = 0;

    ProtoEvt.u4AppId = RM_FSB_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (FSB_NODE_STATUS () != FSB_NODE_STANDBY)
    {
        /* Not a Standby node, hence don't process the received
         * sync message. */
        return FSB_SUCCESS;
    }

    /* Get the Message size encoded by the FSB module in Active node. */
    FSB_RM_GET_2_BYTE (pMesg, pu4Offset, u2MesgSize);
    *pu2Length = (UINT2) (*pu2Length - FSB_RED_LENGTH_SIZE);

    if (*pu2Length != u2MesgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        FsbRmHandleProtocolEvent (&ProtoEvt);
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbRedProcessOperStatus: Invalid Message Size  ... \r\n");
        return FSB_FAILURE;
    }
    /* Copy the Port Index and Oper Status */
    FSB_RM_GET_4_BYTE (pMesg, pu4Offset, u4IfIndex);
    FSB_RM_GET_1_BYTE (pMesg, pu4Offset, u1OperStatus);

    if (u1OperStatus == FSB_RED_PORT_DOWN)
    {
        if (FsbHandlePortDown (u4IfIndex) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                             "\r\nFsbRedProcessOperStatus: FsbHandlePortDown failed... \r\n");
            return FSB_FAILURE;
        }
    }
    return FSB_SUCCESS;
}

/******************************************************************************/
/* FUNCTION NAME    : FsbRedGetMsgBuffer                                      */
/*                                                                            */
/* DESCRIPTION      : This routine returns CRU buffer memory from RM. In case */
/*                    of failure, it sends indication to RM about it.         */
/*                                                                            */
/* INPUT            : u2BufSize - Buffer Size.                                */
/*                                                                            */
/* OUTPUT           : None                                                    */
/*                                                                            */
/* RETURNS          : pMsg - Allocated Tx Buffer                              */
/*                                                                            */
/******************************************************************************/

tRmMsg             *
FsbRedGetMsgBuffer (UINT2 u2BufSize)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_FSB_APP_ID;

    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_BUFFER_TRC,
                         "\r\nFsbRedGetMsgBuffer Memory Allocation Failed ... \r\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;

        if (FsbRmHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                             "\r\nFsbRedGetMsgBuffer FsbRmHandleProtocolEvent() Failed ... \r\n");
        }
    }
    return pMsg;
}
