/********************************************************************
*  Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: fsbfcf.c,v 1.11 2017/11/02 13:44:38 siva Exp $
*
* Description: This file contains the init and de-init routines for
*              FIP-snooping Task,MemPool and Timers
*
*******************************************************************/
#ifndef _FSBFCF_C_
#define _FSBFCF_C_

#include "fsbinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbHandleFcfDiscovery                            */
/*                                                                           */
/*    Description         : This function adds FCF entry in the database     */
/*                          and starts the FCF Keep Alive Timer              */
/*                                                                           */
/*    Input(s)            : pFSBPktInfo - Pointer to FSBPktInfo structure    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbHandleFcfDiscovery (tFSBPktInfo * pFSBPktInfo)
{
    tFsbFcfEntry       *pFsbFcfEntry = NULL;
    tFsbFcfEntry        FsbFcfEntry;

    MEMSET (&FsbFcfEntry, 0, sizeof (tFsbFcfEntry));

    /* Check whether the entry is already present, by 
     * retrieving from the RB Tree*/
    FsbFcfEntry.u2VlanId = pFSBPktInfo->u2VlanId;
    FsbFcfEntry.u4FcfIfIndex = pFSBPktInfo->u4IfIndex;
    MEMCPY (&FsbFcfEntry.FcfMacAddr, pFSBPktInfo->SrcAddr, MAC_ADDR_LEN);

    pFsbFcfEntry = (tFsbFcfEntry *)
        RBTreeGet (gFsbGlobals.FsbFcfTable, (tRBElem *) & FsbFcfEntry);

    /* If the entry is already added in the database,
     * stop the keep alive timer and start again*/
    if (pFsbFcfEntry != NULL)
    {
        /* Stop and start the FCF KeepAlive Timer */
        if (FsbTmrStopTimer (&(pFsbFcfEntry->FCFAliveTimer)) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId, FSB_ERROR_LEVEL,
                             FSB_NONE,
                             "FsbHandleFcfDiscovery : FsbTmrStopTimer for VLAN %d"
                             " IfIndex %d failed\r\n", pFSBPktInfo->u2VlanId,
                             pFSBPktInfo->u4IfIndex);
            return FSB_FAILURE;
        }

        /* Validate Fc-map for the FCF before addiing it */
        if (pFSBPktInfo->u1AddressMode == FSB_FPMA_ADDRESSING_MODE)
        {
            if (FsbValidateFcMap (pFSBPktInfo) != FSB_SUCCESS)
            {
                /* Before deleting the FCF entry,
                 * delete any FIP session entry present for this FCF*/
                FsbDeleteFIPSessEntryForFcf (pFsbFcfEntry);

                FsbDeleteFcfEntry (pFsbFcfEntry);
                FsbUpdateVLANStatistics (pFSBPktInfo->u4ContextId,
                                         pFSBPktInfo->u2VlanId,
                                         FSB_FCMAP_MISMATCH_STATS);

                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbHandleFcfDiscovery : Validation of FCMAP for VLAN %d"
                                 " IfIndex %d failed\r\n",
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
                return FSB_FAILURE;
            }
        }

        if (FsbTmrStartTimer (&(pFsbFcfEntry->FCFAliveTimer),
                              FSB_FCF_KEEP_ALIVE_TIMER_ID,
                              pFsbFcfEntry->u4FcfKeepAliveTimerValue,
                              pFsbFcfEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbHandleFcfDiscovery : FsbTmrStartTimer for VLAN %d"
                             " IfIndex %d failed\r\n", pFSBPktInfo->u2VlanId,
                             pFSBPktInfo->u4IfIndex);
            return FSB_FAILURE;
        }

        if (FsbIsFcfEntryModified (pFsbFcfEntry, pFSBPktInfo) == FSB_TRUE)
        {
            /* Update the entry in the database with the recieved packet value */
            /* Copy the FCMAP value */
            MEMCPY (pFsbFcfEntry->au1FcMap,
                    pFSBPktInfo->FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.
                    au1FcMap, FSB_FCMAP_LEN);

            /* FCF Discovery Advertisement is originated from FCF, so copying the 
             * source MAC address*/
            MEMCPY (pFsbFcfEntry->FcfMacAddr, pFSBPktInfo->SrcAddr,
                    MAC_ADDR_LEN);

            /* Copy the Name ID recieved in the packet */
            MEMCPY (pFsbFcfEntry->au1NameId,
                    pFSBPktInfo->FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.
                    au1NameId, FSB_NAME_ID_LEN);

            /* Copy the fabric name recieved in the packet */
            MEMCPY (pFsbFcfEntry->au1FabricName,
                    pFSBPktInfo->FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.
                    au1FabricName, FSB_FABRIC_NAME_LEN);

            /* Copy the Dbit Flag received in the packet */
            pFsbFcfEntry->u2DbitFlag =
                pFSBPktInfo->FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.
                u2DbitFlag;

            /* FCF Keep Alive Timer value is in milliseconds. So converting into seconds
             * and storing it in the database*/
            /* The timer value is given an extra buffer of time to expire, since FSB
             * is an intermediate switch between an ENode and an FCF. The time given is
             * 3 times the value recieved in the packet*/

            pFsbFcfEntry->u4FcfKeepAliveFieldValue =
                pFSBPktInfo->FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.
                u4FkaAdvPeriod;

            pFsbFcfEntry->u4FcfKeepAliveTimerValue =
                (pFSBPktInfo->FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.
                 u4FkaAdvPeriod / FSB_NUM_OF_MSEC_IN_A_SEC) *
                FSB_FCF_TIMEOUT_MULTIPLIER;

            /* Synup FCF Entry to StandBy */
            if (FsbRedSyncUpFCFEntry (pFsbFcfEntry, FSB_FCF_ADD) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "FsbHandleFcfDiscovery: FsbRedSyncUpFCFEntry for VLAN %d"
                                 " IfIndex %d failed\r\n",
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
                return FSB_FAILURE;
            }
        }
    }
    else
    {
        /* Validate Fc-map for the FCF before addiing it */
        if (pFSBPktInfo->u1AddressMode == FSB_FPMA_ADDRESSING_MODE)
        {
            if (FsbValidateFcMap (pFSBPktInfo) != FSB_SUCCESS)
            {
                FsbUpdateVLANStatistics (pFSBPktInfo->u4ContextId,
                                         pFSBPktInfo->u2VlanId,
                                         FSB_FCMAP_MISMATCH_STATS);
                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbHandleFcfDiscovery : Validation of FCMAP for VLAN %d"
                                 " IfIndex %d failed\r\n",
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
                return FSB_FAILURE;
            }
        }

        /* Entry to be newly added, allocate the memory pool */
        pFsbFcfEntry = MemAllocMemBlk (FSB_FCF_ENTRIES_MEMPOOL_ID);

        if (pFsbFcfEntry == NULL)
        {
            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_DEBUGGING_LEVEL,
                             FSB_OS_RESOURCE_TRC,
                             "FsbHandleFcfDiscovery: MemAllocMemBlk for "
                             " pFsbFcfEntry failed\r\n");
            return FSB_FAILURE;
        }
        MEMSET (pFsbFcfEntry, 0, sizeof (tFsbFcfEntry));

        /* Fill the context id, VLAN id and interface index from the 
         * recieved packet*/
        pFsbFcfEntry->u4ContextId = pFSBPktInfo->u4ContextId;
        pFsbFcfEntry->u2VlanId = pFSBPktInfo->u2VlanId;
        pFsbFcfEntry->u4FcfIfIndex = pFSBPktInfo->u4IfIndex;

        /* Fill the addressing mode(FPMA/SPMA) */
        pFsbFcfEntry->u1AddressingMode = pFSBPktInfo->u1AddressMode;

        /* Copy the FCMAP value */
        MEMCPY (pFsbFcfEntry->au1FcMap,
                pFSBPktInfo->FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.
                au1FcMap, FSB_FCMAP_LEN);

        /* FCF Discovery Advertisement is originated from FCF, so copying the 
         * source MAC address*/
        MEMCPY (pFsbFcfEntry->FcfMacAddr, pFSBPktInfo->SrcAddr, MAC_ADDR_LEN);

        /* Copy the Name ID recieved in the packet */
        MEMCPY (pFsbFcfEntry->au1NameId,
                pFSBPktInfo->FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.
                au1NameId, FSB_NAME_ID_LEN);

        /* Copy the fabric name recieved in the packet */
        MEMCPY (pFsbFcfEntry->au1FabricName,
                pFSBPktInfo->FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.
                au1FabricName, FSB_FABRIC_NAME_LEN);

        pFsbFcfEntry->u1IssuMaintenanceMode = FSB_FALSE;

        /* Copy the Dbit Flag received in the packet */
        pFsbFcfEntry->u2DbitFlag =
            pFSBPktInfo->FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.
            u2DbitFlag;

        /* FCF Keep Alive Timer value is in milliseconds. So converting into
         * seconds and storing it in the database */
        /* The timer value is given an extra buffer of time to expire, since
         * FSB is an intermediate switch between an ENode and an FCF.
         * The time given is 3 times the value recieved in the packet*/
        pFsbFcfEntry->u4FcfKeepAliveFieldValue =
            pFSBPktInfo->FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.
            u4FkaAdvPeriod;

        pFsbFcfEntry->u4FcfKeepAliveTimerValue =
            (pFSBPktInfo->FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.
             u4FkaAdvPeriod / FSB_NUM_OF_MSEC_IN_A_SEC) *
            FSB_FCF_TIMEOUT_MULTIPLIER;

        /* Add the entry into RBTree */
        if (RBTreeAdd (gFsbGlobals.FsbFcfTable,
                       (tRBElem *) pFsbFcfEntry) != RB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                             FSB_DEBUGGING_LEVEL, FSB_OS_RESOURCE_TRC,
                             "FsbHandleFcfDiscovery: RBTreeAdd for "
                             " pFsbFcfEntry VLAN %d IfIndex %d failed\r\n",
                             pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
            MemReleaseMemBlock (FSB_FCF_ENTRIES_MEMPOOL_ID,
                                (UINT1 *) pFsbFcfEntry);
            return FSB_FAILURE;
        }
        /* Start the FCF Keep Alive TIMER */
        if (FsbTmrStartTimer (&(pFsbFcfEntry->FCFAliveTimer),
                              FSB_FCF_KEEP_ALIVE_TIMER_ID,
                              pFsbFcfEntry->u4FcfKeepAliveTimerValue,
                              pFsbFcfEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbHandleFcfDiscovery : FsbTmrStartTimer for"
                             " VLAN %d IfIndex %d failed\r\n",
                             pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
            return FSB_FAILURE;

        }
        /* Install filter whenever FCF entry is being populated 
         * to allow Discovery solicitation unicast*/
        if (FsbInstallDefaultFCFFilter (FSB_ALLOW_DIS_SOLICITATION,
                                        pFsbFcfEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbHandleFcfDiscovery: FsbInstallDefaultFCFFilter for"
                             " VLAN %d IfIndex %d failed\r\n",
                             pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
            return FSB_FAILURE;
        }

        /* Configure FCf Address as Static Entry since Mac-learning
         * is disabled for all FCoE VALN */
        if (FsbConfigUcastEntry (pFsbFcfEntry->u4ContextId,
                                 pFsbFcfEntry->FcfMacAddr,
                                 pFsbFcfEntry->u2VlanId,
                                 pFsbFcfEntry->u4FcfIfIndex,
                                 VLAN_CREATE) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbHandleFcfDiscovery : Failure in configuring"
                             " Static FCF MAC for VLAN %d IfIndex %d\r\n",
                             pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
            return FSB_FAILURE;
        }
        /* Synup FCF Entry to StandBy */
        if (FsbRedSyncUpFCFEntry (pFsbFcfEntry, FSB_FCF_ADD) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbHandleFcfDiscovery: FsbRedSyncUpFCFEntry"
                             " for VLAN %d IfIndex %d failed\r\n",
                             pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
            return FSB_FAILURE;
        }
        if (FsbRedSyncUpFCFFilterId (pFsbFcfEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbHandleFcfDiscovery: FsbRedSyncUpFCFFilterId"
                             " for VLAN %d IfIndex %d failed\r\n",
                             pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
            return FSB_FAILURE;
        }
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDeleteFcfEntry                                */
/*                                                                           */
/*    Description         : This function will delete the matching           */
/*                          FCF entry for the given table indexes            */
/*                                                                           */
/*    Input(s)            : u2VlanId - VLAN identifier                       */
/*                          u4FcfIfIndex - FCF if index                      */
/*                          FcfMacAddr - FCF Mac Addr                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbDeleteFcfEntry (tFsbFcfEntry * pFsbFcfEntry)
{
    if (pFsbFcfEntry != NULL)
    {
        /* Stop FCF KeepAlive Timer */
        if (FsbTmrStopTimer (&(pFsbFcfEntry->FCFAliveTimer)) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbDeleteFcfEntry : FsbTmrStopTimer for VLAN %d"
                             " IfIndex %d failed\r\n", pFsbFcfEntry->u2VlanId,
                             pFsbFcfEntry->u4FcfIfIndex);
            return FSB_FAILURE;
        }
        FsbDeleteDefaultFCFFilter (pFsbFcfEntry);

        /* Remove Static FCF Mac as FCF is deleted */
        if (FsbConfigUcastEntry (pFsbFcfEntry->u4ContextId,
                                 pFsbFcfEntry->FcfMacAddr,
                                 pFsbFcfEntry->u2VlanId,
                                 pFsbFcfEntry->u4FcfIfIndex,
                                 VLAN_DELETE) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbDeleteFcfEntry : Deleting Static FCF Mac Entry"
                             " for VLAN %d IfIndex %d failed\r\n",
                             pFsbFcfEntry->u2VlanId,
                             pFsbFcfEntry->u4FcfIfIndex);
            return FSB_FAILURE;
        }

        /* Delete the entry if it is present and
         * release the memory */
        if (RBTreeRemove (gFsbGlobals.FsbFcfTable,
                          (tRBElem *) pFsbFcfEntry) != RB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId,
                             FSB_DEBUGGING_LEVEL, FSB_OS_RESOURCE_TRC,
                             "FsbDeleteFcfEntry : RBTreeRemove for "
                             " pFsbFcfEntry  for VLAN %d IfIndex %d failed\r\n",
                             pFsbFcfEntry->u2VlanId,
                             pFsbFcfEntry->u4FcfIfIndex);
            return FSB_FAILURE;
        }
        MemReleaseMemBlock (FSB_FCF_ENTRIES_MEMPOOL_ID, (UINT1 *) pFsbFcfEntry);
        return FSB_SUCCESS;
    }
    return FSB_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbGetFsbFcfEntry                                */
/*                                                                           */
/*    Description         : This function is used to get pointer to          */
/*                          FCF entry                                        */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2VlanIndex - VLAN Index                         */
/*                          FCFMacAddress - FCF Mac Address                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : pointer to tFsbFcfEntry                          */
/*                                                                           */
/*****************************************************************************/
tFsbFcfEntry       *
FsbGetFsbFcfEntry (UINT2 u2VlanIndex, UINT4 u4IfIndex, tMacAddr FCFMacAddress)
{
    tFsbFcfEntry        FsbFcfEntry;
    tFsbFcfEntry       *pFsbFcfEntry = NULL;

    FSB_MEMSET (&FsbFcfEntry, 0, sizeof (tFsbFcfEntry));

    FsbFcfEntry.u2VlanId = u2VlanIndex;
    FsbFcfEntry.u4FcfIfIndex = u4IfIndex;
    FSB_MEMCPY (&FsbFcfEntry.FcfMacAddr, FCFMacAddress, MAC_ADDR_LEN);

    pFsbFcfEntry = RBTreeGet (gFsbGlobals.FsbFcfTable,
                              (tRBElem *) & FsbFcfEntry);
    if (pFsbFcfEntry == NULL)
    {
        return NULL;
    }
    return pFsbFcfEntry;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbGetFcfInfo                                    */
/*                                                                           */
/*    Description         : This function is used to get Fcf Info            */
/*                          in FsbFcfEntry                                   */
/*                                                                           */
/*    Input(s)            : u2VlanIndex - Vlan Id                            */
/*                          FCFMacAddress - FCF Mac Address                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : pFsbFcfEntry                                     */
/*                                                                           */
/*****************************************************************************/
tFsbFcfEntry       *
FsbGetFcfInfo (UINT2 u2VlanIndex, tMacAddr FCFMacAddress)
{
    tFsbFcfEntry       *pFsbFcfEntry = NULL;
    tFsbFcfEntry        FsbFcfEntry;

    FSB_MEMSET (&FsbFcfEntry, 0, sizeof (tFsbFcfEntry));

    pFsbFcfEntry = (tFsbFcfEntry *) RBTreeGetFirst (gFsbGlobals.FsbFcfTable);

    if (pFsbFcfEntry == NULL)
    {
        return NULL;
    }
    do
    {
        FsbFcfEntry.u2VlanId = pFsbFcfEntry->u2VlanId;
        FsbFcfEntry.u4FcfIfIndex = pFsbFcfEntry->u4FcfIfIndex;
        FSB_MEMCPY (FsbFcfEntry.FcfMacAddr, pFsbFcfEntry->FcfMacAddr,
                    MAC_ADDR_LEN);

        if ((pFsbFcfEntry->u2VlanId == u2VlanIndex) &&
            (FSB_MEMCMP (pFsbFcfEntry->FcfMacAddr, FCFMacAddress, MAC_ADDR_LEN)
             == 0))
        {
            return pFsbFcfEntry;
        }

        pFsbFcfEntry = RBTreeGetNext (gFsbGlobals.FsbFcfTable,
                                      (tRBElem *) & FsbFcfEntry, NULL);
    }
    while (pFsbFcfEntry != NULL);

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDeleteAllFcfEntry                             */
/*                                                                           */
/*    Description         : This function will delete all FcfEntry           */
/*                          mapped to the context                            */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context index                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbDeleteAllFcfEntry (UINT4 u4ContextId)
{
    tFsbFcfEntry       *pFsbFcfEntry;
    tFsbFcfEntry        FsbFcfEntry;

    pFsbFcfEntry = (tFsbFcfEntry *) RBTreeGetFirst (gFsbGlobals.FsbFcfTable);

    if (pFsbFcfEntry == NULL)
    {
        /* No FsbFcfEntry were created and therefore, no entries
         * are present to delete */
        return FSB_SUCCESS;
    }
    do
    {
        FsbFcfEntry.u2VlanId = pFsbFcfEntry->u2VlanId;
        FsbFcfEntry.u4FcfIfIndex = pFsbFcfEntry->u4FcfIfIndex;
        MEMCPY (&FsbFcfEntry.FcfMacAddr, pFsbFcfEntry->FcfMacAddr,
                MAC_ADDR_LEN);
        /* For the matching FCF entry, remove from RBTree
         * and release the memory*/
        if (pFsbFcfEntry->u4ContextId == u4ContextId)
        {
            FsbDeleteFcfEntry (pFsbFcfEntry);
        }
        pFsbFcfEntry = (tFsbFcfEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFcfTable,
                           (tRBElem *) & FsbFcfEntry, NULL);
    }
    while (pFsbFcfEntry != NULL);
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDeleteFCFEntryForFcoeVlan                     */
/*                                                                           */
/*    Description         : This function will delete all FcfEntry           */
/*                          mapped to the deleted Fcoe Vlan                  */
/*                                                                           */
/*    Input(s)            : u2VlanId      - Vlan index                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbDeleteFCFEntryForFcoeVlan (UINT4 u4ContextId, UINT2 u2VlanId)
{
    tFsbFcfEntry       *pFsbFcfEntry;
    tFsbFcfEntry        FsbFcfEntry;

    pFsbFcfEntry = (tFsbFcfEntry *) RBTreeGetFirst (gFsbGlobals.FsbFcfTable);

    if (pFsbFcfEntry == NULL)
    {
        /* No FsbFcfEntry was created and therefore, no entries
         * are present to delete */
        return FSB_SUCCESS;
    }
    do
    {
        FsbFcfEntry.u2VlanId = pFsbFcfEntry->u2VlanId;
        FsbFcfEntry.u4FcfIfIndex = pFsbFcfEntry->u4FcfIfIndex;
        MEMCPY (&FsbFcfEntry.FcfMacAddr, pFsbFcfEntry->FcfMacAddr,
                MAC_ADDR_LEN);

        /* For the matching FCF entry, remove from RBTree
         * and release the memory*/
        if ((pFsbFcfEntry->u4ContextId == u4ContextId)
            && (pFsbFcfEntry->u2VlanId == u2VlanId))
        {
            FsbDeleteFcfEntry (pFsbFcfEntry);
        }
        pFsbFcfEntry = (tFsbFcfEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFcfTable,
                           (tRBElem *) & FsbFcfEntry, NULL);
    }
    while (pFsbFcfEntry != NULL);
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDeleteFcfEntryForIfIndex                      */
/*                                                                           */
/*    Description         : This function will delete all FcfEntry           */
/*                          mapped to the port                               */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Interface index                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbDeleteFcfEntryForIfIndex (UINT4 u4IfIndex)
{
    tFsbFcfEntry       *pFsbFcfEntry;
    tFsbFcfEntry        FsbFcfEntry;

    pFsbFcfEntry = (tFsbFcfEntry *) RBTreeGetFirst (gFsbGlobals.FsbFcfTable);

    if (pFsbFcfEntry == NULL)
    {
        /* No FsbFcfEntry were created and therefore, no entries
         * are present to delete */
        return FSB_SUCCESS;
    }
    do
    {
        FsbFcfEntry.u2VlanId = pFsbFcfEntry->u2VlanId;
        FsbFcfEntry.u4FcfIfIndex = pFsbFcfEntry->u4FcfIfIndex;
        MEMCPY (&FsbFcfEntry.FcfMacAddr, pFsbFcfEntry->FcfMacAddr,
                MAC_ADDR_LEN);

        /* For the matching FCF entry, remove from RBTree
         * and release the memory*/
        if (pFsbFcfEntry->u4FcfIfIndex == u4IfIndex)
        {
            FsbDeleteFcfEntry (pFsbFcfEntry);
        }
        pFsbFcfEntry = (tFsbFcfEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFcfTable,
                           (tRBElem *) & FsbFcfEntry, NULL);
    }
    while (pFsbFcfEntry != NULL);
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDeleteFcfEntryForVLANIfIndex                  */
/*                                                                           */
/*    Description         : This function will delete all FcfEntry           */
/*                          mapped to the port of particular VLAN.           */
/*                                                                           */
/*    Input(s)            : VlanId - VLAN Index, u4IfIndex - Interface index */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbDeleteFcfEntryForVLANIfIndex (tVlanId VlanId, UINT4 u4IfIndex)
{
    tFsbFcfEntry       *pFsbFcfEntry;
    tFsbFcfEntry        FsbFcfEntry;

    pFsbFcfEntry = (tFsbFcfEntry *) RBTreeGetFirst (gFsbGlobals.FsbFcfTable);

    if (pFsbFcfEntry == NULL)
    {
        /* No FsbFcfEntry were created and therefore, no entries
         *          * are present to delete */
        return FSB_SUCCESS;
    }
    do
    {
        FsbFcfEntry.u2VlanId = pFsbFcfEntry->u2VlanId;
        FsbFcfEntry.u4FcfIfIndex = pFsbFcfEntry->u4FcfIfIndex;
        MEMCPY (&FsbFcfEntry.FcfMacAddr, pFsbFcfEntry->FcfMacAddr,
                MAC_ADDR_LEN);

        /* For the matching FCF entry, remove from RBTree
         *          * and release the memory*/
        if ((pFsbFcfEntry->u4FcfIfIndex == u4IfIndex) &&
            (pFsbFcfEntry->u2VlanId == VlanId))
        {
            FsbDeleteFcfEntry (pFsbFcfEntry);
        }
        pFsbFcfEntry = (tFsbFcfEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFcfTable,
                           (tRBElem *) & FsbFcfEntry, NULL);
    }
    while (pFsbFcfEntry != NULL);
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbIsFcfEntryModified                                */
/*                                                                           */
/* Description        : This function is used to check whether there is a    */
/*                      difference in the information recieved in the packet */
/*                      and the information in the RBTree datastructure.     */
/*                                                                           */
/* Input(s)           : pFsbFcfEntry - Pointer to FsbFcfEntry                */
/*                      pFSBPktInfo - Pointer to pFSBPktInfo                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
BOOL1
FsbIsFcfEntryModified (tFsbFcfEntry * pFsbFcfEntry, tFSBPktInfo * pFSBPktInfo)
{
    if (FSB_MEMCMP (pFsbFcfEntry->au1FcMap,
                    pFSBPktInfo->FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.
                    au1FcMap, FSB_FCMAP_LEN) != 0)
    {
        return FSB_TRUE;
    }
    if (FSB_MEMCMP
        (pFsbFcfEntry->FcfMacAddr, pFSBPktInfo->SrcAddr, MAC_ADDR_LEN) != 0)
    {
        return FSB_TRUE;
    }
    if (FSB_MEMCMP (pFsbFcfEntry->au1NameId,
                    pFSBPktInfo->FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.
                    au1NameId, FSB_NAME_ID_LEN) != 0)
    {
        return FSB_TRUE;
    }
    if (FSB_MEMCMP (pFsbFcfEntry->au1FabricName,
                    pFSBPktInfo->FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.
                    au1FabricName, FSB_FABRIC_NAME_LEN) != 0)
    {
        return FSB_TRUE;
    }

    if (pFsbFcfEntry->u4FcfKeepAliveFieldValue !=
        pFSBPktInfo->FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.
        u4FkaAdvPeriod)
    {
        return FSB_TRUE;
    }
    return FSB_FALSE;
}

/*****************************************************************************
 *                            End  of file fsbfcf.c                           */
/******************************************************************************/
#endif /* _FSBFCF_C_ */
