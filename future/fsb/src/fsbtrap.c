/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: fsbtrap.c,v 1.2 2016/03/05 12:03:43 siva Exp $
 *  
 * Description: This file contains the FSB trap generation
 *              related code.
 *                            
 ********************************************************************/

#ifndef __FSBSNMP_C__
#define __FSBSNMP_C__

#include "fsbinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbSnmpIfSendTrap                                */
/*                                                                           */
/*    Description         : Routine to send trap to SNMP Agent               */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u1TrapId - Trap Identifier                       */
/*                          pTrapInfo -  Pointer to tFsbTrapInfo             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
FsbSnmpIfSendTrap (UINT1 u1TrapId, VOID *pTrapInfo)
{
    tSNMP_OCTET_STRING_TYPE     *pOMacString  = NULL;
    tSNMP_COUNTER64_TYPE        u8CounterVal = { 0, 0 };
    tSNMP_VAR_BIND              *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE              *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE              *pSnmpTrapOid = NULL;
    tFsbTrapInfo                *pFsbTrapInfo = NULL;
    UINT4                       au4snmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT4                       au4FsbTrapOid[] = {1,3,6,1,4,1,29601,2,102,4,1}; 
    UINT4                       au4FsbSessionEnodeIfIndexOid[] = {1,3,6,1,4,1,29601,2,102,4,1,2};
    UINT4                       au4FsbSessionEnodeMacAddressOid[] = {1,3,6,1,4,1,29601,2,102,4,1,3};
    UINT4                       au4FsbSessionVlanIdOid[] = {1,3,6,1,4,1,29601,2,102,4,1,1};
    UINT1                       au1MacBuf[MAC_ADDR_LEN];
    INT4                        i4TrapStatus = 0;


    pFsbTrapInfo = (tFsbTrapInfo *) pTrapInfo;

    /* The format of SNMP Trap varbinding which is being send using
     * SNMP_AGT_RIF_Notify_v2Trap() is as given below:
     *
     *
     ********************************************************************************
     *                   *                   *    Varbind     *     Varbind    *
     * sysUpTime = Time  * snmpTrapOID = OID * OID# 1 = Value * OID# 2 = Value *
     *                   *                   *                *                *
     ********************************************************************************
     * 
     * First Var binding sysUpTime = Time - is filled by the SNMP_AGT_RIF_Notify_v2Trap().
     * Second Var binding snmpTrapOID - OID should be first Varbind in our list to the api SNMP_AGT_RIF_Notify_v2Trap().
     * The remaining Varbind are updated based on the type of the trap, which
     * are going to be generated.
     *
     */

    /* Allocate the memory for the Varbind for snmpTrapOID MIB object OID and 
     * the OID of the MIB object for those trap is going to be generated.
     */

    if (nmhGetFsMIFsbTrapStatus (pFsbTrapInfo->u4ContextId, &i4TrapStatus) != SNMP_SUCCESS)
    {
        return;
    }
    /* Checking whether Trap status is enabled or not */
    if (i4TrapStatus !=  FSB_ENABLE)
    {
        return;
    }  
    /* Allocate a memory for storing snmpTrapOID OID and assign it */
    pSnmpTrapOid = alloc_oid (FSB_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        return;
    }

    /* Assigning the OID value and its length of snmpTrapOid to psnmpTrapOid */ 
    FSB_MEMCPY (pSnmpTrapOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pSnmpTrapOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);


    /* Allocate a memory for storing OID Value which is the OID of the
     * Notification MIB object and assign it */
    pEnterpriseOid = alloc_oid (FSB_TRAP_OID_LEN);
    if (pEnterpriseOid == NULL)
    {
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    /* Assigning TrapOid to EnterpriseOid and binding it to packet. */
    FSB_MEMCPY (pEnterpriseOid->pu4_OidList,au4FsbTrapOid , sizeof (au4FsbTrapOid));
    pEnterpriseOid->u4_Length = sizeof (au4FsbTrapOid) / sizeof (UINT4);

    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u1TrapId;

    /* Form a VarBind for snmpTrapOID OID and its value */
    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
            SNMP_DATA_TYPE_OBJECT_ID,
            0L, 0, NULL,
            pEnterpriseOid,
            u8CounterVal);

    /* If the pVbList is empty or values are binded then free the memory
       allocated to pEnterpriseOid and pSnmpTrapOid. */
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }
    /* Assigning pVbList to pStartVb */
    pStartVb = pVbList;
    switch (u1TrapId)
    {
        case  FSB_SESSION_CLEAR:

            /* Allocating the memory for pSnmpTrapOid to store OID of
             * FsbSessionEnodeIfIndex object and length of the OID. */
            pSnmpTrapOid = alloc_oid (FSB_TRAP_OID_LEN);


            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */

            if (pSnmpTrapOid == NULL)
            {    
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the OID value and its length of FsbSessionEnodeIfIndex to pSnmpTrapOid */

            FSB_MEMCPY (pSnmpTrapOid->pu4_OidList, au4FsbSessionEnodeIfIndexOid, sizeof (au4FsbSessionEnodeIfIndexOid));
            pSnmpTrapOid->u4_Length = sizeof (au4FsbSessionEnodeIfIndexOid) / sizeof (UINT4);

            /* Forming a VarBind for OID of FsbSessionEnodeIfIndex and its value */ 
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                        SNMP_DATA_TYPE_INTEGER,
                        0L, (INT4)pFsbTrapInfo->u4FsbSessionEnodeIfIndex,
                        NULL,NULL,
                        u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {  
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            } 
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            /* Allocating memory for storing OID of FsbSessionEnodeMacAddress object
             * and its length*/
            pSnmpTrapOid = alloc_oid (FSB_TRAP_OID_LEN);

            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */
            if (pSnmpTrapOid == NULL)
            {   
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the oid and oid length of FsbSessionEnodeMacAddressOid to pSnmpTrapOid */ 
            FSB_MEMCPY (pSnmpTrapOid->pu4_OidList, 
                    au4FsbSessionEnodeMacAddressOid, 
                    sizeof (au4FsbSessionEnodeMacAddressOid));

            pSnmpTrapOid->u4_Length = sizeof (au4FsbSessionEnodeMacAddressOid) / sizeof (UINT4);

            FSB_MEMSET (au1MacBuf, 0, MAC_ADDR_LEN);

            /* Assigning MacAddress to MacBuf */
            FSB_MEMCPY(au1MacBuf, pFsbTrapInfo->FsbSessionEnodeMacAddress, MAC_ADDR_LEN); 

            /* Converting the normal string to OctetString using SNMP_AGT_FormOctetString function */
            pOMacString = SNMP_AGT_FormOctetString ((UINT1 *) au1MacBuf, (INT4)(MAC_ADDR_LEN));

            /* Forming a VarBind for FsbSessionEnodeMacAddress, its value and storing it in pVbList */  

            pVbList->pNextVarBind =  SNMP_AGT_FormVarBind (pSnmpTrapOid,
                    SNMP_DATA_TYPE_OCTET_PRIM,
                    0, 0, pOMacString,
                    NULL,
                    u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {   
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            /* Allocating the memory for pSnmpTrapOid to store OID of
             * FsbSessionVlanId object and length of the OID. */
            pSnmpTrapOid = alloc_oid (FSB_TRAP_OID_LEN);


            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */

            if (pSnmpTrapOid == NULL)
            {    
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the OID value and its length of FsbSessionVlanOId to pSnmpTrapOid */

            FSB_MEMCPY (pSnmpTrapOid->pu4_OidList, au4FsbSessionVlanIdOid, sizeof (au4FsbSessionVlanIdOid));
            pSnmpTrapOid->u4_Length = sizeof (au4FsbSessionVlanIdOid) / sizeof (UINT4);

            /* Forming a VarBind for OID of FSBSessionVlan and its value */ 
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                        SNMP_DATA_TYPE_INTEGER,
                        0L, (INT4)pFsbTrapInfo->u2FsbSessionVlanId,
                        NULL,NULL,
                        u8CounterVal);


            /* Checking if the pVbList is empty and freeing the memory allocated 
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {  
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            } 
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            break;

        case  FSB_FCMAP_MISMATCH:

            /* Allocating the memory for pSnmpTrapOid to store OID of
             * FsbSessionEnodeIfIndex object and length of the OID. */
            pSnmpTrapOid = alloc_oid (FSB_TRAP_OID_LEN);


            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */

            if (pSnmpTrapOid == NULL)
            {    
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the OID value and its length of FsbSessionEnodeIfIndexOid to pSnmpTrapOid */

            FSB_MEMCPY (pSnmpTrapOid->pu4_OidList, au4FsbSessionEnodeIfIndexOid, sizeof (au4FsbSessionEnodeIfIndexOid));
            pSnmpTrapOid->u4_Length = sizeof (au4FsbSessionEnodeIfIndexOid) / sizeof (UINT4);

            /* Forming a VarBind for OID of FsbSessionEnodeIfIndex and its value */ 
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                        SNMP_DATA_TYPE_INTEGER,
                        0L, (INT4)pFsbTrapInfo->u4FsbSessionEnodeIfIndex,
                        NULL,NULL,
                        u8CounterVal);


            /* Checking if the pVbList is empty and freeing the memory allocated
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {  
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            } 
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            /* Allocating memory for storing OID of  object FsbSessionEnodeMacAddress
             * and its length*/
            pSnmpTrapOid = alloc_oid (FSB_TRAP_OID_LEN);

            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */
            if (pSnmpTrapOid == NULL)
            {   
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the oid and oid length of FsbSessionEnodeMacAddressOid to pSnmpTrapOid */ 
            FSB_MEMCPY (pSnmpTrapOid->pu4_OidList, 
                    au4FsbSessionEnodeMacAddressOid, 
                    sizeof (au4FsbSessionEnodeMacAddressOid));

            pSnmpTrapOid->u4_Length = sizeof (au4FsbSessionEnodeMacAddressOid) / sizeof (UINT4);


            FSB_MEMSET (au1MacBuf, 0, MAC_ADDR_LEN);

            /* Assigning MacAddress to MacBuf */
            FSB_MEMCPY(au1MacBuf, pFsbTrapInfo->FsbSessionEnodeMacAddress, MAC_ADDR_LEN); 

            /* Converting the normal string to OctetString using SNMP_AGT_FormOctetString function */
            pOMacString = SNMP_AGT_FormOctetString ((UINT1 *) au1MacBuf, (INT4)(MAC_ADDR_LEN));

            /* Forming a VarBind for FsbSessionEnodeMacAddress, its value and storing it in pVbList */  

            pVbList->pNextVarBind =  SNMP_AGT_FormVarBind (pSnmpTrapOid,
                    SNMP_DATA_TYPE_OCTET_PRIM,
                    0, 0, pOMacString,
                    NULL,
                    u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {   
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;



            /* Allocating the memory for pSnmpTrapOid to store OID of
             * FsbSessionVlanId object and length of the OID. */
            pSnmpTrapOid = alloc_oid (FSB_TRAP_OID_LEN);


            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */

            if (pSnmpTrapOid == NULL)
            {    
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the OID value and its length of FsbSessionVlanIdOid to pSnmpTrapOid */

            FSB_MEMCPY (pSnmpTrapOid->pu4_OidList, au4FsbSessionVlanIdOid, sizeof (au4FsbSessionVlanIdOid));
            pSnmpTrapOid->u4_Length = sizeof (au4FsbSessionVlanIdOid) / sizeof (UINT4);

            /* Forming a VarBind for OID of FsbSessionVlanId and its value */ 
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                        SNMP_DATA_TYPE_INTEGER,
                        0L, (INT4)pFsbTrapInfo->u2FsbSessionVlanId,
                        NULL,NULL,
                        u8CounterVal);


            /* Checking if the pVbList is empty and freeing the memory allocated 
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {  
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            } 
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            break;

        case  FSB_MTU_MISMATCH:

            /* Allocating the memory for pSnmpTrapOid to store OID of
             * FsbSessionEnodeIfIndex object and length of the OID. */
            pSnmpTrapOid = alloc_oid (FSB_TRAP_OID_LEN);


            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */

            if (pSnmpTrapOid == NULL)
            {    
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the OID value and its length of FsbSessionEnodeIfIndexOid to pSnmpTrapOid */

            FSB_MEMCPY (pSnmpTrapOid->pu4_OidList, au4FsbSessionEnodeIfIndexOid, sizeof (au4FsbSessionEnodeIfIndexOid));
            pSnmpTrapOid->u4_Length = sizeof (au4FsbSessionEnodeIfIndexOid) / sizeof (UINT4);

            /* Forming a VarBind for OID of FsbSessionEnodeIfIndex and its value */ 
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                        SNMP_DATA_TYPE_INTEGER,
                        0L, (INT4)pFsbTrapInfo->u4FsbSessionEnodeIfIndex,
                        NULL,NULL,
                        u8CounterVal);


            /* Checking if the pVbList is empty and freeing the memory allocated
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {  
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            } 
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            /* Allocating memory for storing OID of  object
             * FsbSessionEnodeMacAddress and its length*/
            pSnmpTrapOid = alloc_oid (FSB_TRAP_OID_LEN);

            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */
            if (pSnmpTrapOid == NULL)
            {   
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the oid and oid length of FsbSessionEnodeMacAddressOid to pSnmpTrapOid */ 
            FSB_MEMCPY (pSnmpTrapOid->pu4_OidList, 
                    au4FsbSessionEnodeMacAddressOid, 
                    sizeof (au4FsbSessionEnodeMacAddressOid));

            pSnmpTrapOid->u4_Length = sizeof (au4FsbSessionEnodeMacAddressOid) / sizeof (UINT4);


            FSB_MEMSET (au1MacBuf, 0, MAC_ADDR_LEN);

            /* Assigning MacAddress to MacBuf */
            FSB_MEMCPY(au1MacBuf, pFsbTrapInfo->FsbSessionEnodeMacAddress, MAC_ADDR_LEN); 

            /* Converting the normal string to OctetString using SNMP_AGT_FormOctetString function */
            pOMacString = SNMP_AGT_FormOctetString ((UINT1 *) au1MacBuf, (INT4)(MAC_ADDR_LEN));

            /* Forming a VarBind for FsbSessionEnodeMacAddress, its value and storing it in pVbList */  

            pVbList->pNextVarBind =  SNMP_AGT_FormVarBind (pSnmpTrapOid,
                    SNMP_DATA_TYPE_OCTET_PRIM,
                    0, 0, pOMacString,
                    NULL,
                    u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {   
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            /* Allocating the memory for pSnmpTrapOid to store OID of
             * FsbSessionVlanId object and length of the OID. */
            pSnmpTrapOid = alloc_oid (FSB_TRAP_OID_LEN);


            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */

            if (pSnmpTrapOid == NULL)
            {    
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the OID value and its length of FsbSessionVlanIdOid to pSnmpTrapOid */

            FSB_MEMCPY (pSnmpTrapOid->pu4_OidList, au4FsbSessionVlanIdOid, sizeof (au4FsbSessionVlanIdOid));
            pSnmpTrapOid->u4_Length = sizeof (au4FsbSessionVlanIdOid) / sizeof (UINT4);

            /* Forming a VarBind for OID of FsbSessionVlanId and its value */ 
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                        SNMP_DATA_TYPE_INTEGER,
                        0L, (INT4)pFsbTrapInfo->u2FsbSessionVlanId,
                        NULL,NULL,
                        u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated 
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {  
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            } 
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            break;

        case  FSB_ACL_FAILURE:

            /* Allocating the memory for pSnmpTrapOid to store OID of
             * FsbSessionEnodeIfIndex object and length of the OID. */
            pSnmpTrapOid = alloc_oid (FSB_TRAP_OID_LEN);


            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */

            if (pSnmpTrapOid == NULL)
            {    
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the OID value and its length of FsbSessionEnodeIfIndexOid to pSnmpTrapOid */

            FSB_MEMCPY (pSnmpTrapOid->pu4_OidList, au4FsbSessionEnodeIfIndexOid, sizeof (au4FsbSessionEnodeIfIndexOid));
            pSnmpTrapOid->u4_Length = sizeof (au4FsbSessionEnodeIfIndexOid) / sizeof (UINT4);

            /* Forming a VarBind for OID of FsbSessionEnodeIfIndex and its value */ 
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                        SNMP_DATA_TYPE_INTEGER,
                        0L, (INT4)pFsbTrapInfo->u4FsbSessionEnodeIfIndex,
                        NULL,NULL,
                        u8CounterVal);


            /* Checking if the pVbList is empty and freeing the memory allocated
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {  
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            } 
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;


            /* Allocating the memory for pSnmpTrapOid to store OID of
             * FsbSessionVlanId object and length of the OID. */
            pSnmpTrapOid = alloc_oid (FSB_TRAP_OID_LEN);


            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */

            if (pSnmpTrapOid == NULL)
            {    
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the OID value and its length of FsbSessionVlanIdOid to pSnmpTrapOid */

            FSB_MEMCPY (pSnmpTrapOid->pu4_OidList, au4FsbSessionVlanIdOid, sizeof (au4FsbSessionVlanIdOid));
            pSnmpTrapOid->u4_Length = sizeof (au4FsbSessionVlanIdOid) / sizeof (UINT4);

            /* Forming a VarBind for OID of FsbSessionVlanId and its value */ 
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                        SNMP_DATA_TYPE_INTEGER,
                        0L, (INT4)pFsbTrapInfo->u2FsbSessionVlanId,
                        NULL,NULL,
                        u8CounterVal);


            /* Checking if the pVbList is empty and freeing the memory allocated 
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {  
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            } 
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            break;

        default:
            break;

    }
#ifdef SNMP_2_WANTED
    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);

    /* Replace this function with the Corresponding Function provided by 
     * SNMP Agent for Notifying Traps.
     */
#endif

#ifdef DEBUG_WANTED
    DbgNotifyTrap (u1TrapId, pTrapInfo);
#endif
}
#endif
