/*************************************************************************
* Copyright (C) 2007-2012 Aricent Group . All Rights Reserved
*
* $Id: fsbdefacl.c,v 1.12 2017/10/09 13:13:58 siva Exp $
*
* Description: This file contains install and remove filter function for 
*              Default VLAN and Default Filter
*
*************************************************************************/
#ifndef _FSBDEFACL_C_
#define _FSBDEFACL_C_

#include "fsbinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbInstallDefaultVLANFilter                      */
/*                                                                           */
/*    Description         : This function is used to form filter entry       */
/*                          and call appropriate wrapper function to install */
/*                          filter to allow FIP VLAN Discovery               */
/*                                                                           */
/*    Input(s)            : u4FsMIFsbContextId - Context Id                  */
/*                          pFsbContextInfo    - Pointer to Context Info     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbInstallDefaultVLANFilter (UINT4 u4FsMIFsbContextId,
                             tFsbContextInfo * pFsbContextInfo)
{
    tFsbLocRemFilterId  FsbLocRemFilterId;
    UINT2               u2DefaultVlanId = 0;

    FSB_MEMSET (&FsbLocRemFilterId, 0, sizeof (tFsbLocRemFilterId));
    /* Below function call is used to get Default VLAN Id */
    FsbGetUntaggedVlanId (u4FsMIFsbContextId, &u2DefaultVlanId);

    /* Below function call fills the information required 
     * to install filter to allow FIP VLAN Request on Default VLAN*/
    FsFsbFormDefaultVLANFilter (u4FsMIFsbContextId, u2DefaultVlanId,
                                &(pFsbContextInfo->FsbLocFilterEntry.
                                  FsbVlanDiscovery), FSB_VLAN_DISC_FILTER);

    pFsbContextInfo->FsbLocFilterEntry.FsbVlanDiscovery.u4FilterId =
        FSB_GET_FILTER_ID ();
    /* Below wrapper function inturn call appropriate NPAPI function 
     * to install filter and Hw Filter Id is returned which get populated 
     * in Filter Entry present Context Table*/
    if (FsMiNpFsbHwWrInit (&(pFsbContextInfo->FsbLocFilterEntry.
                             FsbVlanDiscovery),
                           &FsbLocRemFilterId) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbInstallDefaultVLANFilter for Vlan: %d: "
                         "FsMiNpFsbHwWrInit failed\r\n", u2DefaultVlanId);
        return FSB_FAILURE;
    }

    pFsbContextInfo->FsbLocFilterEntry.FsbVlanDiscovery.u4HwFilterId =
        FsbLocRemFilterId.u4LocalFilterId;
    pFsbContextInfo->FsbRemFilterEntry.FsbVlanDiscovery.u4HwFilterId =
        FsbLocRemFilterId.u4RemoteFilterId;

    /* Below function call fills the information required 
     * to install filter to allow FIP VLAN Response with tag on Default VLAN*/
    FsFsbFormDefaultVLANFilter (u4FsMIFsbContextId, u2DefaultVlanId,
                                &(pFsbContextInfo->FsbLocFilterEntry.
                                  FsbVlanResponseTagged),
                                FSB_VLAN_RESP_TAGGED_FILTER);
    pFsbContextInfo->FsbLocFilterEntry.FsbVlanResponseTagged.u4FilterId =
        FSB_GET_FILTER_ID ();
    /* Below wrapper function inturn call appropriate NPAPI function 
     * to install filter and Hw Filter Id is returned which get populated 
     * in Filter Entry present Context Table*/
    if (FsMiNpFsbHwWrInit (&(pFsbContextInfo->FsbLocFilterEntry.
                             FsbVlanResponseTagged),
                           &FsbLocRemFilterId) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbInstallDefaultVLANFilter for Vlan: %d: "
                         "FsMiNpFsbHwWrInit failed\r\n", u2DefaultVlanId);
        return FSB_FAILURE;
    }

    pFsbContextInfo->FsbLocFilterEntry.FsbVlanResponseTagged.u4HwFilterId =
        FsbLocRemFilterId.u4LocalFilterId;
    pFsbContextInfo->FsbRemFilterEntry.FsbVlanResponseTagged.u4HwFilterId =
        FsbLocRemFilterId.u4RemoteFilterId;
    if (FsbRedSyncUpDefaultVlanFilter (pFsbContextInfo,
                                       u4FsMIFsbContextId) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbInstallDefaultVLANFilter for Vlan: %d: "
                         "FsbRedSyncUpDefaultVlanFilter failed\r\n",
                         u2DefaultVlanId);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbRemoveDefaultVLANFilter                       */
/*                                                                           */
/*    Description         : This function is used to call appropriate        */
/*                          wrapper function to remove filter installed      */
/*                          to allow FIP VLAN Discovery                      */
/*                                                                           */
/*    Input(s)            : pFsbContextInfo - Pointer to Context Info        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbRemoveDefaultVLANFilter (tFsbContextInfo * pFsbContextInfo)
{
    tFsbLocRemFilterId  FsbLocRemFilterId;

    FSB_MEMSET (&FsbLocRemFilterId, 0, sizeof (tFsbLocRemFilterId));

    if ((pFsbContextInfo->FsbLocFilterEntry.FsbVlanDiscovery.u4HwFilterId != 0)
        || (pFsbContextInfo->FsbRemFilterEntry.FsbVlanDiscovery.u4HwFilterId !=
            0))
    {

        FsbLocRemFilterId.u4LocalFilterId =
            pFsbContextInfo->FsbLocFilterEntry.FsbVlanDiscovery.u4HwFilterId;
        FsbLocRemFilterId.u4RemoteFilterId =
            pFsbContextInfo->FsbRemFilterEntry.FsbVlanDiscovery.u4HwFilterId;

        /* Below wrapper function inturn call appropriate NPAPI function 
         * to remove filter using Hw Filter Id */
        if (FsMiNpFsbHwWrDeInit (&(pFsbContextInfo->FsbLocFilterEntry.
                                   FsbVlanDiscovery),
                                 FsbLocRemFilterId) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC
                (pFsbContextInfo->FsbLocFilterEntry.FsbVlanDiscovery.
                 u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                 "FsbRemoveDefaultVLANFilter : FsMiNpFsbHwWrDeInit failed\r\n");
            return FSB_FAILURE;
        }

        FsbLocRemFilterId.u4LocalFilterId =
            pFsbContextInfo->FsbLocFilterEntry.FsbVlanResponseTagged.
            u4HwFilterId;
        FsbLocRemFilterId.u4RemoteFilterId =
            pFsbContextInfo->FsbRemFilterEntry.FsbVlanResponseTagged.
            u4HwFilterId;

        /* Below wrapper function inturn call appropriate NPAPI function 
         * to remove filter using Hw Filter Id */
        if (FsMiNpFsbHwWrDeInit (&(pFsbContextInfo->FsbLocFilterEntry.
                                   FsbVlanResponseTagged),
                                 FsbLocRemFilterId) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC
                (pFsbContextInfo->FsbLocFilterEntry.FsbVlanResponseTagged.
                 u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                 "FsbRemoveDefaultVLANFilter : FsMiNpFsbHwWrDeInit failed \r\n");
            return FSB_FAILURE;
        }
        FSB_MEMSET (&(pFsbContextInfo->FsbLocFilterEntry), 0,
                    sizeof (tFsbFilterEntry));
        FSB_MEMSET (&(pFsbContextInfo->FsbRemFilterEntry), 0,
                    sizeof (tFsbFilterEntry));
    }
    return FSB_SUCCESS;
}

/*******************************************************************************/
/*                                                                             */
/*    Function Name       : FsbInstallDefaultFilter                            */
/*                                                                             */
/*    Description         : This function is used to form filter entry         */
/*                          and call appropriate wrapper function to install   */
/*                          filter to allow the below two packets and          */
/*                          deny all other packets:-                           */
/*                          1) FIP Discovery Solicititation Multicast          */
/*                          2) FIP Discovery Advertisement Multicast           */
/*                                                                             */
/*    Input(s)            : u2FilterType         - Filter Type                 */
/*                          pFsbFipSnoopingEntry - Pointer to FipSnoopingEntry */
/*                                                                             */
/*    Output(s)           : None                                               */
/*                                                                             */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                           */
/*                                                                             */
/*******************************************************************************/
INT4
FsbInstallDefaultFilter (UINT2 u2FilterType,
                         tFsbFipSnoopingEntry * pFsbFipSnoopingEntry)
{
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    UINT4               u4HwFilterId = 0;

    pFsbFilterEntry = MemAllocMemBlk (FSB_FILTER_ENTRIES_MEMPOOL_ID);

    if (pFsbFilterEntry == NULL)
    {
        FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                         FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                         "FsbInstallDefaultFilter: Memory allocation failed for "
                         "Filter entry\r\n");
        return FSB_FAILURE;
    }

    FSB_MEMSET (pFsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    /* Below function call fill the information required 
     * to install filter to allow Discovery Solicitaiton and 
     * Advertisement Multicast and to deny all other packets */
    FsFsbFormDefaultFilter (pFsbFipSnoopingEntry->u4ContextId,
                            pFsbFipSnoopingEntry->u2VlanId,
                            u2FilterType, pFsbFilterEntry);

    pFsbFilterEntry->u4FilterId = FSB_GET_FILTER_ID ();
    /* Below wrapper function inturn call appropriate NPAPI function 
     * to install filter and Hw Filter Id is returned which get populated 
     * in Filter Entry present FipSnooping Table*/
    if (FsFsbHwWrCreateFilter (pFsbFilterEntry, &u4HwFilterId) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbInstallDefaultFilter for FCoE Vlan: %d: "
                         "FsFsbHwWrCreateFilter failed\r\n",
                         pFsbFipSnoopingEntry->u2VlanId);
        MemReleaseMemBlock (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                            (UINT1 *) pFsbFilterEntry);
        return FSB_FAILURE;
    }
    pFsbFilterEntry->u4HwFilterId = u4HwFilterId;

    if (RBTreeAdd (pFsbFipSnoopingEntry->FsbFilterEntry,
                   (tRBElem *) pFsbFilterEntry) != RB_SUCCESS)
    {
        FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbInstallDefaultFilter for FCoE Vlan: %d: "
                         "RBTreeAdd failed \r\n",
                         pFsbFipSnoopingEntry->u2VlanId);
        MemReleaseMemBlock (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                            (UINT1 *) pFsbFilterEntry);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*******************************************************************************/
/*                                                                             */
/*    Function Name       : FsbRemoveDefaultFilter                             */
/*                                                                             */
/*    Description         : This function is used to remove filter list from   */
/*                          Fip Snooping Table and to call appropriate         */
/*                          wrapper function which removes filter installed    */
/*                          to allow the below packets & deny all              */
/*                          other packets:-                                    */
/*                          1) FIP Discovery Solicititation Multicast          */
/*                          2) FIP Discovery Advertisement Multicast           */
/*                                                                             */
/*    Input(s)            : pFsbFipSnoopingEntry - Pointer to FipSnoopingEntry */
/*                                                                             */
/*    Output(s)           : None                                               */
/*                                                                             */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                           */
/*                                                                             */
/*******************************************************************************/
INT4
FsbRemoveDefaultFilter (tFsbFipSnoopingEntry * pFsbFipSnoopingEntry)
{
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;

    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    if (pFsbFipSnoopingEntry == NULL)
    {
        return FSB_SUCCESS;
    }

    pFsbFilterEntry = (tFsbFilterEntry *)
        RBTreeGetFirst (pFsbFipSnoopingEntry->FsbFilterEntry);
    if (pFsbFilterEntry == NULL)
    {
        return FSB_SUCCESS;
    }

    do
    {
        FSB_MEMCPY (FsbFilterEntry.DstMac, pFsbFilterEntry->DstMac,
                    MAC_ADDR_LEN);
        /* Below wrapper function inturn call appropriate NPAPI function 
         * to remove filter using Hw Filter Id */
        if (FsFsbHwWrDeleteFilter (pFsbFilterEntry,
                                   pFsbFilterEntry->u4HwFilterId) !=
            FSB_SUCCESS)
        {
            FsbUpdateVLANStatistics (pFsbFipSnoopingEntry->u4ContextId,
                                     pFsbFipSnoopingEntry->u2VlanId,
                                     FSB_ACL_FAILURE_STATS);
            FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbRemoveDefaultFilter for Vlan: %d:"
                             " ACL Filter Deletion failed \r\n",
                             pFsbFipSnoopingEntry->u2VlanId);
            return FSB_FAILURE;
        }
        else
        {
            if (RBTreeRemove (pFsbFipSnoopingEntry->FsbFilterEntry,
                              (tRBElem *) pFsbFilterEntry) != RB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbRemoveDefaultFilter for Vlan: %d:"
                                 " RBTreeRemove failed \r\n",
                                 pFsbFipSnoopingEntry->u2VlanId);
                return FSB_FAILURE;
            }
            MemReleaseMemBlock (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                                (UINT1 *) pFsbFilterEntry);
        }
        pFsbFilterEntry = (tFsbFilterEntry *)
            RBTreeGetNext (pFsbFipSnoopingEntry->FsbFilterEntry,
                           (tRBElem *) & FsbFilterEntry, NULL);
    }
    while (pFsbFilterEntry != NULL);

    if (FsFsbHwWrSetFCoEParams (pFsbFipSnoopingEntry->u4ContextId,
                                pFsbFipSnoopingEntry->u2VlanId, FSB_DISABLE)
        != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbRemoveDefaultFilter:"
                         "FsFsbHwWrSetFCoEParams failed\r\n");
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbAddDefaultFilter                              */
/*                                                                           */
/*    Description         : This function is used to install filter for      */
/*                          FCoE enabled VLAN to allow Discovery             */
/*                          Solicitation and Advertisment Multicast and      */
/*                          deny all other packets                           */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbAddDefaultFilter (UINT4 u4ContextId)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbFipSnoopingEntry FsbFipSnoopingEntry;
    UINT2               u2FilTypeIndex = 0;

    FSB_MEMSET (&FsbFipSnoopingEntry, 0, sizeof (tFsbFipSnoopingEntry));

    FsbFipSnoopingEntry.u4ContextId = u4ContextId;
    FsbFipSnoopingEntry.u2VlanId = 0;

    while ((pFsbFipSnoopingEntry = (tFsbFipSnoopingEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFipSnoopingTable,
                           (tRBElem *) & FsbFipSnoopingEntry, NULL)) != NULL)
    {
        if (pFsbFipSnoopingEntry->u4ContextId == u4ContextId)
        {
            if (pFsbFipSnoopingEntry->u1EnabledStatus == FSB_ENABLE)
            {
                pFsbFipSnoopingEntry->FsbFilterEntry =
                    RBTreeCreateEmbedded ((FSAP_OFFSETOF (tFsbFilterEntry,
                                                          FsbFilterNode)),
                                          FsbCompareFipFilterEntryIndex);
                /* Below loop is to  install filter to allow Discovery
                 * Solicitation & Advertisment Multicast and
                 * deny all other packets only if that VLAN is enabled*/
                for (u2FilTypeIndex = FSB_ALLOW_DIS_SOLICITATION;
                     u2FilTypeIndex <= FSB_GLOBAL_DENY; u2FilTypeIndex++)
                {
                    if (FsbInstallDefaultFilter (u2FilTypeIndex,
                                                 pFsbFipSnoopingEntry) !=
                        FSB_SUCCESS)
                    {
                        FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                                         FSB_ERROR_LEVEL, FSB_NONE,
                                         "FsbAddDefaultFilter for Vlan: %d: "
                                         "FsbInstallDefaultFilter failed\r\n",
                                         pFsbFipSnoopingEntry->u2VlanId);
                        return FSB_FAILURE;
                    }
                    if (FsbRedSyncUpDefaultFilter (pFsbFipSnoopingEntry) !=
                        FSB_SUCCESS)
                    {
                        FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                                         FSB_ERROR_LEVEL, FSB_NONE,
                                         "FsbAddDefaultFilter for Vlan: %d: "
                                         "FsbRedSyncUpDefaultFilter failed\r\n",
                                         pFsbFipSnoopingEntry->u2VlanId);
                        return FSB_FAILURE;
                    }
                }
            }
        }
        FsbFipSnoopingEntry.u4ContextId = pFsbFipSnoopingEntry->u4ContextId;
        FsbFipSnoopingEntry.u2VlanId = pFsbFipSnoopingEntry->u2VlanId;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDeleteDefaultFilter                           */
/*                                                                           */
/*    Description         : This function is used to remove filter installed */
/*                          when FCoE VLAN is enabled                        */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbDeleteDefaultFilter (UINT4 u4ContextId)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbFipSnoopingEntry FsbFipSnoopingEntry;

    FSB_MEMSET (&FsbFipSnoopingEntry, 0, sizeof (tFsbFipSnoopingEntry));

    FsbFipSnoopingEntry.u4ContextId = u4ContextId;
    FsbFipSnoopingEntry.u2VlanId = 0;

    while ((pFsbFipSnoopingEntry = (tFsbFipSnoopingEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFipSnoopingTable,
                           (tRBElem *) & FsbFipSnoopingEntry, NULL)) != NULL)
    {
        if (pFsbFipSnoopingEntry->u4ContextId == u4ContextId)
        {
            if (pFsbFipSnoopingEntry->u1EnabledStatus == FSB_ENABLE)
            {
                /* When disable FSB module, delete default filter installed (to 
                 * allow Discovery Solicitation & Advertisement Multicast and 
                 * deny all other packets) only if that VLAN is enabled */
                FsbRemoveDefaultFilter (pFsbFipSnoopingEntry);
            }
        }
        FsbFipSnoopingEntry.u4ContextId = pFsbFipSnoopingEntry->u4ContextId;
        FsbFipSnoopingEntry.u2VlanId = pFsbFipSnoopingEntry->u2VlanId;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbInstallDefaultFCFFilter                       */
/*                                                                           */
/*    Description         : This function is used to install filter for FCF  */
/*                                                                           */
/*    Input(s)            : u2FilterType - Filter Type                       */
/*                          pFsbFcfEntry - Pointer to FCF Entry              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbInstallDefaultFCFFilter (UINT2 u2FilterType, tFsbFcfEntry * pFsbFcfEntry)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    UINT4               u4HwFilterId = 0;

    /* Below function call will return pointer to FipSnooping Entry 
     * for the given ContextId and VlanId*/
    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (pFsbFcfEntry->u4ContextId,
                                                   pFsbFcfEntry->u2VlanId);
    if (pFsbFipSnoopingEntry == NULL)
    {
        FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbInstallDefaultFCFFilter: "
                         "FsbGetFIPSnoopingEntry failed\r\n");
        return FSB_FAILURE;
    }

    pFsbFilterEntry = MemAllocMemBlk (FSB_FILTER_ENTRIES_MEMPOOL_ID);

    if (pFsbFilterEntry == NULL)
    {
        FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                         FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                         "FsbInstallDefaultFCFFilter of FCF with VlanId: %d"
                         ": Memory allocation failed for Filter entry\r\n",
                         pFsbFipSnoopingEntry->u2VlanId);
        return FSB_FAILURE;
    }

    FSB_MEMSET (pFsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    FSB_MEMCPY (pFsbFilterEntry->DstMac, pFsbFcfEntry->FcfMacAddr,
                MAC_ADDR_LEN);
    /* Below function call fill the information required to install 
     * filter to allow Discovery Solicitation Unicast */
    if (FsFsbFormDefaultFilter (pFsbFcfEntry->u4ContextId,
                                pFsbFcfEntry->u2VlanId,
                                u2FilterType, pFsbFilterEntry) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbInstallDefaultFCFFilter of FCF with VlanId: %d"
                         ": FsFsbFormDefaultFilter failed\r\n",
                         pFsbFipSnoopingEntry->u2VlanId);
        MemReleaseMemBlock (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                            (UINT1 *) pFsbFilterEntry);
        return FSB_FAILURE;

    }
    pFsbFilterEntry->u4FilterId = FSB_GET_FILTER_ID ();
    /* Below wrapper function inturn call appropriate NPAPI function 
     * to install filter and Hw Filter Id is returned which get populated 
     * in Filter Entry present FipSnooping Table*/
    if (FsFsbHwWrCreateFilter (pFsbFilterEntry, &u4HwFilterId) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbInstallDefaultFCFFilter of FCF with VlanId: %d"
                         ": FsFsbHwWrCreateFilter failed\r\n",
                         pFsbFipSnoopingEntry->u2VlanId);
        MemReleaseMemBlock (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                            (UINT1 *) pFsbFilterEntry);
        return FSB_FAILURE;
    }

    pFsbFilterEntry->u4HwFilterId = u4HwFilterId;

    if (RBTreeAdd (pFsbFipSnoopingEntry->FsbFilterEntry,
                   (tRBElem *) pFsbFilterEntry) != RB_SUCCESS)
    {
        FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbInstallDefaultFilter of FCF with VlanId: %d"
                         ": RBTreeAdd failed \r\n",
                         pFsbFipSnoopingEntry->u2VlanId);
        MemReleaseMemBlock (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                            (UINT1 *) pFsbFilterEntry);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDeleteDefaultFCFFilter                        */
/*                                                                           */
/*    Description         : This function is used to remove default filter   */
/*                          installed for FCF                                */
/*                                                                           */
/*    Input(s)            : u2FilterType - Filter Type                       */
/*                          pFsbFcfEntry - Pointer to FCF Entry              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbDeleteDefaultFCFFilter (tFsbFcfEntry * pFsbFcfEntry)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;

    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    /* Below function call will return pointer to FipSnooping Entry 
     * for the given ContextId and VlanId*/
    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (pFsbFcfEntry->u4ContextId,
                                                   pFsbFcfEntry->u2VlanId);
    if (pFsbFipSnoopingEntry == NULL)
    {
        FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbDeleteDefaultFCFFilter: "
                         "FsbGetFIPSnoopingEntry failed\r\n");
        return FSB_FAILURE;
    }
    FSB_MEMCPY (FsbFilterEntry.DstMac, pFsbFcfEntry->FcfMacAddr, MAC_ADDR_LEN);

    pFsbFilterEntry = RBTreeGet (pFsbFipSnoopingEntry->FsbFilterEntry,
                                 (tRBElem *) & FsbFilterEntry);
    if (pFsbFilterEntry == NULL)
    {
        FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbDeleteDefaultFCFFilter of FCF with VlanId: %d:"
                         " pFsbFilterEntry is NULL\r\n",
                         pFsbFipSnoopingEntry->u2VlanId);
        return FSB_FAILURE;
    }
    /* Below wrapper function inturn call appropriate NPAPI function
     * to remove filter using Hw Filter Id */
    if (FsFsbHwWrDeleteFilter (pFsbFilterEntry,
                               pFsbFilterEntry->u4HwFilterId) != FSB_SUCCESS)
    {
        FsbUpdateVLANStatistics (pFsbFipSnoopingEntry->u4ContextId,
                                 pFsbFipSnoopingEntry->u2VlanId,
                                 FSB_ACL_FAILURE_STATS);
        FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbDeleteDefaultFCFFilter of FCF with VlanId: %d:"
                         " ACL Filter Deletion failed \r\n",
                         pFsbFipSnoopingEntry->u2VlanId);
        return FSB_FAILURE;
    }
    else
    {
        if (RBTreeRemove (pFsbFipSnoopingEntry->FsbFilterEntry,
                          (tRBElem *) pFsbFilterEntry) != RB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbDeleteDefaultFCFFilter of FCF with VlanId: %d:"
                             " RBTreeRemove failed\r\n",
                             pFsbFipSnoopingEntry->u2VlanId);
            return FSB_FAILURE;
        }
        MemReleaseMemBlock (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                            (UINT1 *) pFsbFilterEntry);
    }

    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbRemoveSChannelFilters                         */
/*                                                                           */
/*    Description         : This function is used to remove all              */
/*                          S-Channel filter entries                         */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context identifer                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbRemoveSChannelFilters (UINT4 u4ContextId)
{
    tFsbSChannelFilterEntry *pFsbSChannelFilterEntry = NULL;
    tFsbSChannelFilterEntry FsbSChannelFilterEntry;

    FSB_MEMSET (&FsbSChannelFilterEntry, 0, sizeof (tFsbSChannelFilterEntry));

    pFsbSChannelFilterEntry = (tFsbSChannelFilterEntry *)
        RBTreeGetFirst (gFsbGlobals.FsbSChannelFilterTable);
    if (pFsbSChannelFilterEntry == NULL)
    {
        return FSB_SUCCESS;
    }
    do
    {
        FsbSChannelFilterEntry.u4IfIndex = pFsbSChannelFilterEntry->u4IfIndex;
        FsbSChannelFilterEntry.u2VlanId = pFsbSChannelFilterEntry->u2VlanId;

        if (pFsbSChannelFilterEntry->u4ContextId == u4ContextId)
        {
            if (FsFsbHwWrDeleteSChannelFilter (pFsbSChannelFilterEntry,
                                               pFsbSChannelFilterEntry->
                                               u4HwFilterId) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbSChannelFilterEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbRemoveSChannelFilters of SChannel filter with"
                                 "VlanId: %d and IfIndex: %d: ACL Filter Deletion "
                                 "failed \r\n",
                                 pFsbSChannelFilterEntry->u2VlanId,
                                 pFsbSChannelFilterEntry->u4IfIndex);
            }
            else
            {
                FSB_CONTEXT_TRC (pFsbSChannelFilterEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbRemoveSChannelFilters of SChannel filter with"
                                 "VlanId: %d and IfIndex: %d: ACL Filter Deletion "
                                 "successful\r\n",
                                 pFsbSChannelFilterEntry->u2VlanId,
                                 pFsbSChannelFilterEntry->u4IfIndex);
            }
            if (RBTreeRemove (gFsbGlobals.FsbSChannelFilterTable,
                              (tRBElem *) pFsbSChannelFilterEntry) !=
                RB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbSChannelFilterEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbRemoveSChannelFilters of SChannel filter with"
                                 "VlanId: %d and IfIndex: %d: RBTreeRemove failed\r\n",
                                 pFsbSChannelFilterEntry->u2VlanId,
                                 pFsbSChannelFilterEntry->u4IfIndex);
                return FSB_FAILURE;
            }
            MemReleaseMemBlock (FSB_SCHAN_FILT_ENTRIES_MEMPOOL_ID,
                                (UINT1 *) pFsbSChannelFilterEntry);
        }

        pFsbSChannelFilterEntry = (tFsbSChannelFilterEntry *)
            RBTreeGetNext (gFsbGlobals.FsbSChannelFilterTable,
                           (tRBElem *) & FsbSChannelFilterEntry, NULL);
    }
    while (pFsbSChannelFilterEntry != NULL);
    return FSB_SUCCESS;
}
#endif /*_FSBDEFACL_C_ */
