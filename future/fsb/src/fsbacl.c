/*************************************************************************
* Copyright (C) 2007-2012 Aricent Group . All Rights Reserved
*
* $Id: fsbacl.c,v 1.12 2017/10/09 13:13:58 siva Exp $
*
* Description: This file contain the utility function to form Filter Entry
*
*************************************************************************/
#ifndef _FSBACL_C_
#define _FSBACL_C_

#include "fsbinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsFsbFormDefaultVLANFilter                       */
/*                                                                           */
/*    Description         : This function will populate pFsbFilterEntry      */
/*                          with the information required for installing     */
/*                          Default VLAN filter to allow VLAN Discovery      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2VlanId    - VLAN Id                            */
/*                                                                           */
/*    Output(s)           : pFsbFilterEntry - Filter Entry                   */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsFsbFormDefaultVLANFilter (UINT4 u4ContextId, UINT2 u2VlanId,
                            tFsbFilterEntry * pFsbFilterEntry,
                            UINT1 u1FilterType)
{
    FSB_MEMSET (pFsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    pFsbFilterEntry->u1FilterType = FSB_FILTER_GLOBAL_MODE;
    pFsbFilterEntry->u4ContextId = u4ContextId;
    pFsbFilterEntry->u2VlanId = u2VlanId;
    pFsbFilterEntry->u2Ethertype = FIP_ETHER_TYPE;
    pFsbFilterEntry->u2OpcodeFilterOffsetValue = FSB_VLAN_DISCOVERY;
    pFsbFilterEntry->u1FilterAction = FSB_FILTER_ALLOW_AND_COPYTOCPU;

    if (u1FilterType == FSB_VLAN_DISC_FILTER)
    {
        FSB_MEMCPY (pFsbFilterEntry->DstMac, ALL_FCF_MAC, MAC_ADDR_LEN);
        pFsbFilterEntry->u1Priority = FSB_FIELD_ENTRY_VLAN_REQ_PRIO;
    }

    if (u1FilterType == FSB_VLAN_RESP_TAGGED_FILTER)
    {
        pFsbFilterEntry->u2SubOpcodeFilterOffsetValue = FSB_VLAN_REPLY;
        pFsbFilterEntry->u1Priority = FSB_FIELD_ENTRY_PRIO_HIGHEST;
    }
    pFsbFilterEntry->u1DefFilterType = u1FilterType;
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsFsbFormDefaultFilter                           */
/*                                                                           */
/*    Description         : This function will populate pFsbFilterEntry      */
/*                          with the required information based on           */
/*                          Filter Type                                      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2VlanId    - VLAN Id                            */
/*                          u2MsgType - Messgae Type                         */
/*                                                                           */
/*    Output(s)           : pFsbFilterEntry - Filter Entry                   */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsFsbFormDefaultFilter (UINT4 u4ContextId, UINT2 u2VlanId,
                        UINT2 u2FilterType, tFsbFilterEntry * pFsbFilterEntry)
{
    tPortList          *pNullPortList = NULL;
    tPortList          *pPortList = NULL;

    pPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPortList == NULL)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsFsbFormDefaultFilter: FsUtilAllocBitList"
                         " for portlist failed\r\n");
        return FSB_FAILURE;
    }

    pNullPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pNullPortList == NULL)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsFsbFormDefaultFilter : pNullPortList memalloc failed\r\n");
        FsUtilReleaseBitList ((UINT1 *) pPortList);
        return FSB_FAILURE;
    }

    FSB_MEMSET (*pNullPortList, 0, sizeof (tPortList));
    FSB_MEMSET (*pPortList, 0, sizeof (tPortList));

    switch (u2FilterType)
    {
            /* Below case is to allow Discovery Solicitation Multicast from ENode */
        case FSB_ALLOW_DIS_SOLICITATION:
            /* Below function call will populate PortList with ENode Facing ports */
            FsbUtilPopulatePortList (u2VlanId, u4ContextId,
                                     FSB_ENODE_FACING, *pPortList);
            /* Below check is to identify Discovery Solicitation - Multicast */
            if ((FSB_MEMCMP
                 (pFsbFilterEntry->DstMac, ALL_ZERO_MAC, MAC_ADDR_LEN)) == 0)
            {
                FSB_MEMCPY (pFsbFilterEntry->DstMac, ALL_FCF_MAC, MAC_ADDR_LEN);
                pFsbFilterEntry->u1Priority = FSB_FIELD_ENTRY_SOLI_MULTI_PRIO;
            }
            else
            {
                pFsbFilterEntry->u1Priority = FSB_FIELD_ENTRY_PRIO_HIGHEST;
            }
            pFsbFilterEntry->u2SubOpcodeFilterOffsetValue =
                FSB_DISCOVERY_SOLICITATION;
            break;

            /* Below case is to allow Discovery Advertisement Multicast from FCF */
        case FSB_ALLOW_DIS_ADVERTISEMENT:
            /* Below function call will populate PortList with FCF Facing ports */
            FsbUtilPopulatePortList (u2VlanId, u4ContextId,
                                     FSB_FCF_FACING, *pPortList);
            /* Below check is to identify Dicovery Advertisement - Multicast */
            if ((FSB_MEMCMP
                 (pFsbFilterEntry->DstMac, ALL_ZERO_MAC, MAC_ADDR_LEN)) == 0)
            {
                FSB_MEMCPY (pFsbFilterEntry->DstMac, ALL_ENODE_MAC,
                            MAC_ADDR_LEN);
                pFsbFilterEntry->u1Priority = FSB_FIELD_ENTRY_DISC_ADV_PRIO;
            }
            else
            {
                pFsbFilterEntry->u1Priority = FSB_FIELD_ENTRY_PRIO_HIGHEST;
            }
            pFsbFilterEntry->u2SubOpcodeFilterOffsetValue =
                FSB_DISCOVERY_ADVERTISEMENT;
            break;
            /* Below case is to deny all other packets other than
             * Discovery Solicitation from ENode and Advertisment Multicast from FCF */
        case FSB_GLOBAL_DENY:
            /* The PortList is populated to differentiate between Local
             * and Remote Node in Active-Active scenario
             * This filter will not be programmed with PortList and
             * it will be discarded in NPAPI */
            FsbUtilPopulatePortList (u2VlanId, u4ContextId,
                                     FSB_ENODE_FACING, *pPortList);
            if (FSB_MEMCMP (*pPortList, *pNullPortList, sizeof (tPortList)) ==
                0)
            {
                FsbUtilPopulatePortList (u2VlanId, u4ContextId,
                                         FSB_FCF_FACING, *pPortList);
            }
            else
            {
                FsbUtilPopulatePortList (u2VlanId, u4ContextId,
                                         FSB_ENODE_FCF_FACING, *pPortList);
            }

            MEMCPY (pFsbFilterEntry->PortList, pPortList, sizeof (tPortList));
            pFsbFilterEntry->u1FilterType = FSB_FILTER_GLOBAL_MODE;
            pFsbFilterEntry->u2VlanId = u2VlanId;
            pFsbFilterEntry->u1FilterAction = FSB_FILTER_DENY_AND_DONOTLEARN;
            pFsbFilterEntry->u4ContextId = u4ContextId;
            pFsbFilterEntry->u1Priority = FSB_FIELD_ENTRY_PRIO_LOWEST;
            /* Below function call will release the memory allocated for portlist */
            FsUtilReleaseBitList ((UINT1 *) pPortList);
            FsUtilReleaseBitList ((UINT1 *) pNullPortList);
            return FSB_SUCCESS;

        default:
            /* Below function call will release the memory allocated for portlist */
            FsUtilReleaseBitList ((UINT1 *) pPortList);
            FsUtilReleaseBitList ((UINT1 *) pNullPortList);
            return FSB_FAILURE;
    }
    /* Below entries are common for both Discovery 
     * Solicitation and Advertisement - Multicast */
    pFsbFilterEntry->u1FilterType = FSB_FILTER_PORT_MODE;
    FSB_MEMCPY (pFsbFilterEntry->PortList, pPortList, sizeof (tPortList));
    pFsbFilterEntry->u4ContextId = u4ContextId;
    pFsbFilterEntry->u2VlanId = u2VlanId;
    pFsbFilterEntry->u2Ethertype = FIP_ETHER_TYPE;
    pFsbFilterEntry->u2OpcodeFilterOffsetValue = FSB_DISCOVERY;
    pFsbFilterEntry->u1FilterAction = FSB_FILTER_ALLOW_AND_COPYTOCPU;
    pFsbFilterEntry->u4AggIndex = 0;
    pFsbFilterEntry->u2ClassId = 0;
    /* Below function call will release the memory allocated for portlist */
    FsUtilReleaseBitList ((UINT1 *) pPortList);
    FsUtilReleaseBitList ((UINT1 *) pNullPortList);
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsFsbFormFIPSessionFilter                        */
/*                                                                           */
/*    Description         : This function will populate pFsbFilterEntry      */
/*                          with the required information required for       */
/*                          installing filters for FIP Session               */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : pFsbFipSessEntry - FIP Session Entry             */
/*                          u2FilterType     - Filter Type                   */
/*                          FcfMacAddr       - FCF MAC Address               */
/*                          u2SubType        - Filter Sub Type               */
/*                                                                           */
/*    Output(s)           : pFsbFilterEntry  - Filter Entry                  */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsFsbFormFIPSessionFilter (tFsbFipSessEntry * pFsbFipSessEntry,
                           UINT2 u2FilterType, tMacAddr FcfMacAddr,
                           tFsbFilterEntry * pFsbFilterEntry)
{
    tPortList          *pPortList = NULL;
    tPortList           PortList;
    INT4                i4BrgPortType = 0;
    UINT4               u4UapIfIndex = 0;
    UINT2               u2SVID = 0;
    UINT1               u1IfType = 0;

    pPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPortList == NULL)
    {
        FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsFsbFormFIPSessionFilter: FsUtilAllocBitList for"
                         " portlist failed\r\n");
        return FSB_FAILURE;
    }

    FSB_MEMSET (*pPortList, 0, sizeof (tPortList));
    FSB_MEMSET (PortList, 0, sizeof (tPortList));

    pFsbFilterEntry->u1FilterType = FSB_FILTER_PORT_MODE;

    switch (u2FilterType)
    {
        case FSB_FIP_SOLICIT_UCAST:
            /* Populate pFsbFilterEntry with the information required 
             * to allow Discovery Advertisment Unicast Message from FCF */
            FsbUtilPopulatePortList (pFsbFipSessEntry->u2VlanId,
                                     pFsbFipSessEntry->u4ContextId,
                                     FSB_FCF_FACING, *pPortList);
            FSB_MEMCPY (pFsbFilterEntry->PortList, pPortList,
                        sizeof (tPortList));
            FSB_MEMCPY (pFsbFilterEntry->DstMac, pFsbFipSessEntry->ENodeMacAddr,
                        MAC_ADDR_LEN);
            FSB_MEMCPY (pFsbFilterEntry->SrcMac, FcfMacAddr, MAC_ADDR_LEN);
            pFsbFilterEntry->u2VlanId = pFsbFipSessEntry->u2VlanId;
            pFsbFilterEntry->u4ContextId = pFsbFipSessEntry->u4ContextId;
            pFsbFilterEntry->u2Ethertype = FIP_ETHER_TYPE;
            pFsbFilterEntry->u2OpcodeFilterOffsetValue = FSB_DISCOVERY;
            pFsbFilterEntry->u2SubOpcodeFilterOffsetValue =
                FSB_DISCOVERY_ADVERTISEMENT;
            pFsbFilterEntry->u1FilterAction = FSB_FILTER_ALLOW_AND_COPYTOCPU;
            pFsbFilterEntry->u4AggIndex = 0;
            pFsbFilterEntry->u2ClassId = 0;
            break;

        case FSB_FIP_SOLICIT_MCAST:
            /* Populate pFsbFilterEntry with the information required 
             * to allow Discovery Advertisment Unicast Message from multiple FCFs */
            FsbUtilPopulatePortList (pFsbFipSessEntry->u2VlanId,
                                     pFsbFipSessEntry->u4ContextId,
                                     FSB_FCF_FACING, *pPortList);
            FSB_MEMCPY (pFsbFilterEntry->PortList, pPortList,
                        sizeof (tPortList));
            FSB_MEMCPY (pFsbFilterEntry->DstMac, pFsbFipSessEntry->ENodeMacAddr,
                        MAC_ADDR_LEN);
            pFsbFilterEntry->u2VlanId = pFsbFipSessEntry->u2VlanId;
            pFsbFilterEntry->u4ContextId = pFsbFipSessEntry->u4ContextId;
            pFsbFilterEntry->u2Ethertype = FIP_ETHER_TYPE;
            pFsbFilterEntry->u2OpcodeFilterOffsetValue = FSB_DISCOVERY;
            pFsbFilterEntry->u2SubOpcodeFilterOffsetValue =
                FSB_DISCOVERY_ADVERTISEMENT;
            pFsbFilterEntry->u1FilterAction = FSB_FILTER_ALLOW_AND_COPYTOCPU;
            pFsbFilterEntry->u4AggIndex = 0;
            pFsbFilterEntry->u2ClassId = 0;
            break;

        case FSB_FIP_ADV_UCAST:
            /* Populate pFsbFilterEntry with the information required 
             * to allow FLOGI Request Message from ENode*/
            FsbCfaGetIfType (pFsbFipSessEntry->u4ENodeIfIndex, &u1IfType);
            FsbCfaGetInterfaceBrgPortType (pFsbFipSessEntry->u4ENodeIfIndex,
                                           &i4BrgPortType);

            if (u1IfType == CFA_LAGG)
            {
                pFsbFilterEntry->u4AggIndex = pFsbFipSessEntry->u4ENodeIfIndex;
            }
            else if (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
            {
                if (FsbVlanApiGetSChInfoFromSChIfIndex
                    (pFsbFipSessEntry->u4ENodeIfIndex, &u4UapIfIndex,
                     &u2SVID) != FSB_FAILURE)
                {
                    pFsbFilterEntry->u2ClassId = u2SVID;
                }
            }
            else
            {
                OSIX_BITLIST_SET_BIT (PortList,
                                      pFsbFipSessEntry->u4ENodeIfIndex,
                                      sizeof (tPortList));
                FSB_MEMCPY (pFsbFilterEntry->PortList, PortList,
                            sizeof (tPortList));
                pFsbFilterEntry->u4AggIndex = 0;
                pFsbFilterEntry->u2ClassId = 0;
            }
            FSB_MEMCPY (pFsbFilterEntry->SrcMac, pFsbFipSessEntry->ENodeMacAddr,
                        MAC_ADDR_LEN);
            FSB_MEMCPY (pFsbFilterEntry->DstMac, FcfMacAddr, MAC_ADDR_LEN);
            pFsbFilterEntry->u2VlanId = pFsbFipSessEntry->u2VlanId;
            pFsbFilterEntry->u4ContextId = pFsbFipSessEntry->u4ContextId;
            pFsbFilterEntry->u2Ethertype = FIP_ETHER_TYPE;
            pFsbFilterEntry->u2OpcodeFilterOffsetValue = FSB_VL_INSTANTIATION;
            pFsbFilterEntry->u2SubOpcodeFilterOffsetValue =
                FSB_VL_INSTANTIATION_REQUEST;
            pFsbFilterEntry->u1FilterAction = FSB_FILTER_ALLOW_AND_COPYTOCPU;
            FSB_MEMCPY (pFsbFilterEntry->au1NameId, pFsbFipSessEntry->au1NameId,
                        FSB_NAME_ID_LEN);
            FSB_MEMCPY (pFsbFilterEntry->au1FcMap, pFsbFipSessEntry->au1FcMap,
                        FSB_FCMAP_LEN);
            break;

        case FSB_FIP_FLOGI_REQUEST:
        case FSB_FIP_NPIV_FDISC_REQUEST:
            /* Populate pFsbFilterEntry with the information required 
             * to allow FLOGI Accept Message from FCF */
            FsbCfaGetIfType (pFsbFipSessEntry->u4FcfIfIndex, &u1IfType);

            if (u1IfType != CFA_LAGG)
            {
                OSIX_BITLIST_SET_BIT (PortList, pFsbFipSessEntry->u4FcfIfIndex,
                                      sizeof (tPortList));
                FSB_MEMCPY (pFsbFilterEntry->PortList, PortList,
                            sizeof (tPortList));
                pFsbFilterEntry->u4AggIndex = 0;
                pFsbFilterEntry->u2ClassId = 0;
            }
            else
            {
                pFsbFilterEntry->u4AggIndex = pFsbFipSessEntry->u4FcfIfIndex;
            }
            FSB_MEMCPY (pFsbFilterEntry->SrcMac, FcfMacAddr, MAC_ADDR_LEN);
            FSB_MEMCPY (pFsbFilterEntry->DstMac, pFsbFipSessEntry->ENodeMacAddr,
                        MAC_ADDR_LEN);
            pFsbFilterEntry->u2VlanId = pFsbFipSessEntry->u2VlanId;
            pFsbFilterEntry->u4ContextId = pFsbFipSessEntry->u4ContextId;
            pFsbFilterEntry->u2Ethertype = FIP_ETHER_TYPE;
            pFsbFilterEntry->u2OpcodeFilterOffsetValue = FSB_VL_INSTANTIATION;
            pFsbFilterEntry->u2SubOpcodeFilterOffsetValue =
                FSB_VL_INSTANTIATION_REPLY;
            pFsbFilterEntry->u1FilterAction = FSB_FILTER_ALLOW_AND_COPYTOCPU;
            break;
        default:
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                             "FsFsbFormFIPSessionFilter: "
                             "Invalid FIP message \r\n");
            break;
    }
    pFsbFilterEntry->u1Priority = FSB_FIELD_ENTRY_PRIO_HIGHEST;
    /* Below function call will release the memory allocated for portlist */
    FsUtilReleaseBitList ((UINT1 *) pPortList);
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsFsbFormFLOGIAcceptFilter                       */
/*                                                                           */
/*    Description         : This function will populate pFsbFilterEntry      */
/*                          with the required information required for       */
/*                          installing filters for FLOGI/DISC Accept         */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : pFsbFipSessFCoEEntry - FIP Session FCoE Entry    */
/*                          u2FilterType     - Filter Type                   */
/*                                                                           */
/*    Output(s)           : pFsbFilterEntry  - Filter Entry                  */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsFsbFormFLOGIAcceptFilter (tFsbFipSessFCoEEntry * pFsbFipSessFCoEEntry,
                            UINT2 u2FilterType,
                            tFsbFilterEntry * pFsbFilterEntry)
{
    tFsbFipSessEntry   *pFsbFipSessEntry = NULL;
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tPortList           PortList;
    INT4                i4BrgPortType = 0;
    UINT4               u4UapIfIndex = 0;
    UINT4               u4PinnnedPortIfIndex = 0;
    UINT2               u2SVID = 0;
    UINT1               u1IfType = 0;

    FSB_MEMSET (PortList, 0, sizeof (tPortList));

    pFsbFipSessEntry = pFsbFipSessFCoEEntry->pFsbFipSessEntry;
    pFsbFilterEntry->u1FilterType = FSB_FILTER_PORT_MODE;

    pFsbFilterEntry->u1Priority = FSB_FIELD_ENTRY_PRIO_HIGHEST;

    if (pFsbFipSessEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsFsbFormFLOGIAcceptFilter: pFsbFipSessEntry is NULL\r\n");
        return FSB_FAILURE;
    }

    FsbCfaGetIfType (pFsbFipSessEntry->u4FcfIfIndex, &u1IfType);
    if (u1IfType == CFA_LAGG)
    {
        if (FsbIsMLAGPortChannel ((UINT2) pFsbFipSessEntry->u4FcfIfIndex) ==
            FSB_SUCCESS)
        {
            pFsbFipSnoopingEntry =
                FsbGetFIPSnoopingEntry (pFsbFipSessEntry->u4ContextId,
                                        pFsbFipSessEntry->u2VlanId);

            if (pFsbFipSnoopingEntry != NULL)
            {
                u4PinnnedPortIfIndex =
                    pFsbFipSnoopingEntry->u4PinnnedPortIfIndex;
            }
        }
    }

    switch (u2FilterType)
    {
        case FSB_FCOE_TRAFFIC_FROM_FCF:
            /* Populate pFsbFilterEntry with the information required 
             * to allow FCoE Traffic from FCF */
            FsbCfaGetIfType (pFsbFipSessEntry->u4FcfIfIndex, &u1IfType);

            if (u1IfType != CFA_LAGG)
            {
                OSIX_BITLIST_SET_BIT (PortList, pFsbFipSessEntry->u4FcfIfIndex,
                                      sizeof (tPortList));
                FSB_MEMCPY (pFsbFilterEntry->PortList, PortList,
                            sizeof (tPortList));
                pFsbFilterEntry->u4AggIndex = 0;
                pFsbFilterEntry->u2ClassId = 0;
            }
            else
            {
                pFsbFilterEntry->u4AggIndex = pFsbFipSessEntry->u4FcfIfIndex;
            }
            FSB_MEMCPY (pFsbFilterEntry->SrcMac,
                        pFsbFipSessFCoEEntry->FcfMacAddr, MAC_ADDR_LEN);
            FSB_MEMCPY (pFsbFilterEntry->DstMac,
                        pFsbFipSessFCoEEntry->FCoEMacAddr, MAC_ADDR_LEN);
            pFsbFilterEntry->u2VlanId = pFsbFipSessFCoEEntry->u2VlanId;
            pFsbFilterEntry->u4ContextId = pFsbFipSessEntry->u4ContextId;
            pFsbFilterEntry->u2Ethertype = FCOE_ETHER_TYPE;
            pFsbFilterEntry->u1FilterAction = FSB_FILTER_ALLOW;
            pFsbFilterEntry->u4IfIndex = pFsbFipSessEntry->u4FcfIfIndex;
            pFsbFilterEntry->u4PinnnedPortIfIndex = 0;
            break;

        case FSB_FCOE_TRAFFIC_FROM_ENODE:
            /* Populate pFsbFilterEntry with the information required 
             * to allow FCoE Traffic from ENode */
            FsbCfaGetIfType (pFsbFipSessFCoEEntry->u4ENodeIfIndex, &u1IfType);
            FsbCfaGetInterfaceBrgPortType (pFsbFipSessFCoEEntry->u4ENodeIfIndex,
                                           &i4BrgPortType);

            if (u1IfType == CFA_LAGG)
            {
                pFsbFilterEntry->u4AggIndex =
                    pFsbFipSessFCoEEntry->u4ENodeIfIndex;
            }
            else if (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
            {
                if (FsbVlanApiGetSChInfoFromSChIfIndex
                    (pFsbFipSessFCoEEntry->u4ENodeIfIndex, &u4UapIfIndex,
                     &u2SVID) != FSB_FAILURE)
                {
                    pFsbFilterEntry->u2ClassId = u2SVID;
                }
            }
            else
            {
                OSIX_BITLIST_SET_BIT (PortList,
                                      pFsbFipSessFCoEEntry->u4ENodeIfIndex,
                                      sizeof (tPortList));
                FSB_MEMCPY (pFsbFilterEntry->PortList, PortList,
                            sizeof (tPortList));
                pFsbFilterEntry->u4AggIndex = 0;
                pFsbFilterEntry->u2ClassId = 0;
            }
            FSB_MEMCPY (pFsbFilterEntry->SrcMac,
                        pFsbFipSessFCoEEntry->FCoEMacAddr, MAC_ADDR_LEN);
            FSB_MEMCPY (pFsbFilterEntry->DstMac,
                        pFsbFipSessFCoEEntry->FcfMacAddr, MAC_ADDR_LEN);
            pFsbFilterEntry->u2VlanId = pFsbFipSessFCoEEntry->u2VlanId;
            pFsbFilterEntry->u4ContextId = pFsbFipSessEntry->u4ContextId;
            pFsbFilterEntry->u2Ethertype = FCOE_ETHER_TYPE;
            pFsbFilterEntry->u1FilterAction = FSB_FILTER_ALLOW;
            pFsbFilterEntry->u4IfIndex = pFsbFipSessFCoEEntry->u4ENodeIfIndex;
            pFsbFilterEntry->u4PinnnedPortIfIndex = u4PinnnedPortIfIndex;
            break;

        case FSB_VN_PORT_KEEP_ALIVE:
            /* Populate pFsbFilterEntry with the information required 
             * to allow VN_Port Keep Alive Message */
            FsbCfaGetIfType (pFsbFipSessFCoEEntry->u4ENodeIfIndex, &u1IfType);
            FsbCfaGetInterfaceBrgPortType (pFsbFipSessFCoEEntry->u4ENodeIfIndex,
                                           &i4BrgPortType);

            if (u1IfType == CFA_LAGG)
            {
                pFsbFilterEntry->u4AggIndex =
                    pFsbFipSessFCoEEntry->u4ENodeIfIndex;
            }
            else if (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
            {
                if (FsbVlanApiGetSChInfoFromSChIfIndex
                    (pFsbFipSessFCoEEntry->u4ENodeIfIndex, &u4UapIfIndex,
                     &u2SVID) != FSB_FAILURE)
                {
                    pFsbFilterEntry->u2ClassId = u2SVID;
                }
            }
            else
            {
                OSIX_BITLIST_SET_BIT (PortList,
                                      pFsbFipSessFCoEEntry->u4ENodeIfIndex,
                                      sizeof (tPortList));
                FSB_MEMCPY (pFsbFilterEntry->PortList, PortList,
                            sizeof (tPortList));
                pFsbFilterEntry->u4AggIndex = 0;
                pFsbFilterEntry->u2ClassId = 0;
            }
            FSB_MEMCPY (pFsbFilterEntry->SrcMac,
                        pFsbFipSessFCoEEntry->FCoEMacAddr, MAC_ADDR_LEN);
            FSB_MEMCPY (pFsbFilterEntry->DstMac,
                        pFsbFipSessFCoEEntry->FcfMacAddr, MAC_ADDR_LEN);
            pFsbFilterEntry->u2VlanId = pFsbFipSessFCoEEntry->u2VlanId;
            pFsbFilterEntry->u4ContextId = pFsbFipSessEntry->u4ContextId;
            pFsbFilterEntry->u2Ethertype = FIP_ETHER_TYPE;
            pFsbFilterEntry->u2OpcodeFilterOffsetValue = FSB_KEEP_ALIVE;
            pFsbFilterEntry->u2SubOpcodeFilterOffsetValue = FSB_FIP_KEEP_ALIVE;
            pFsbFilterEntry->u1FilterAction = FSB_FILTER_ALLOW_AND_COPYTOCPU;
            pFsbFilterEntry->u1Priority = FSB_FIELD_ENTRY_KEEP_ALIVE_PRIO;
            pFsbFilterEntry->u4IfIndex = pFsbFipSessFCoEEntry->u4ENodeIfIndex;
            pFsbFilterEntry->u4PinnnedPortIfIndex = u4PinnnedPortIfIndex;

            break;

        case FSB_ENODE_KEEP_ALIVE:
            /* Populate pFsbFilterEntry with the information required 
             * to allow ENode Keep Alive Message */
            FsbCfaGetIfType (pFsbFipSessFCoEEntry->u4ENodeIfIndex, &u1IfType);
            FsbCfaGetInterfaceBrgPortType (pFsbFipSessFCoEEntry->u4ENodeIfIndex,
                                           &i4BrgPortType);

            if (u1IfType == CFA_LAGG)
            {
                pFsbFilterEntry->u4AggIndex =
                    pFsbFipSessFCoEEntry->u4ENodeIfIndex;
            }
            else if (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
            {
                if (FsbVlanApiGetSChInfoFromSChIfIndex
                    (pFsbFipSessFCoEEntry->u4ENodeIfIndex, &u4UapIfIndex,
                     &u2SVID) != FSB_FAILURE)
                {
                    pFsbFilterEntry->u2ClassId = u2SVID;
                }
            }
            else
            {
                OSIX_BITLIST_SET_BIT (PortList,
                                      pFsbFipSessFCoEEntry->u4ENodeIfIndex,
                                      sizeof (tPortList));
                FSB_MEMCPY (pFsbFilterEntry->PortList, PortList,
                            sizeof (tPortList));
                pFsbFilterEntry->u4AggIndex = 0;
                pFsbFilterEntry->u2ClassId = 0;
            }

            FSB_MEMCPY (pFsbFilterEntry->SrcMac,
                        pFsbFipSessFCoEEntry->ENodeMacAddr, MAC_ADDR_LEN);
            FSB_MEMCPY (pFsbFilterEntry->DstMac,
                        pFsbFipSessFCoEEntry->FcfMacAddr, MAC_ADDR_LEN);
            pFsbFilterEntry->u2VlanId = pFsbFipSessFCoEEntry->u2VlanId;
            pFsbFilterEntry->u4ContextId = pFsbFipSessEntry->u4ContextId;
            pFsbFilterEntry->u2Ethertype = FIP_ETHER_TYPE;
            pFsbFilterEntry->u2OpcodeFilterOffsetValue = FSB_KEEP_ALIVE;
            pFsbFilterEntry->u2SubOpcodeFilterOffsetValue = FSB_FIP_KEEP_ALIVE;
            pFsbFilterEntry->u1FilterAction = FSB_FILTER_ALLOW_AND_COPYTOCPU;
            pFsbFilterEntry->u1Priority = FSB_FIELD_ENTRY_KEEP_ALIVE_PRIO;
            pFsbFilterEntry->u4IfIndex = pFsbFipSessFCoEEntry->u4ENodeIfIndex;
            pFsbFilterEntry->u4PinnnedPortIfIndex = u4PinnnedPortIfIndex;
            break;

        case FSB_FIP_CLEAR_VIRTUAL_LINK:
            /* Populate pFsbFilterEntry with the information required 
             * to allow Clear Virutal Link Message from FCF */
            FsbCfaGetIfType (pFsbFipSessEntry->u4FcfIfIndex, &u1IfType);

            if (u1IfType != CFA_LAGG)
            {
                OSIX_BITLIST_SET_BIT (PortList, pFsbFipSessEntry->u4FcfIfIndex,
                                      sizeof (tPortList));
                FSB_MEMCPY (pFsbFilterEntry->PortList, PortList,
                            sizeof (tPortList));
                pFsbFilterEntry->u4AggIndex = 0;
                pFsbFilterEntry->u2ClassId = 0;
            }
            else
            {
                pFsbFilterEntry->u4AggIndex = pFsbFipSessEntry->u4FcfIfIndex;
            }
            FSB_MEMCPY (pFsbFilterEntry->SrcMac,
                        pFsbFipSessFCoEEntry->FcfMacAddr, MAC_ADDR_LEN);
            FSB_MEMCPY (pFsbFilterEntry->DstMac,
                        pFsbFipSessFCoEEntry->ENodeMacAddr, MAC_ADDR_LEN);
            pFsbFilterEntry->u2VlanId = pFsbFipSessEntry->u2VlanId;
            pFsbFilterEntry->u4ContextId = pFsbFipSessEntry->u4ContextId;
            pFsbFilterEntry->u2Ethertype = FIP_ETHER_TYPE;
            pFsbFilterEntry->u2OpcodeFilterOffsetValue = FSB_KEEP_ALIVE;
            pFsbFilterEntry->u2SubOpcodeFilterOffsetValue =
                FSB_FIP_CLEAR_VIRTUAL_LINKS;
            pFsbFilterEntry->u1FilterAction = FSB_FILTER_ALLOW_AND_COPYTOCPU;
            pFsbFilterEntry->u4IfIndex = pFsbFipSessEntry->u4FcfIfIndex;
            pFsbFilterEntry->u4PinnnedPortIfIndex = 0;
            break;

        default:
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                             "FsFsbFormFLOGIAcceptFilter: "
                             "Invalid Sub type of FLOGI Accept \r\n");
            break;
    }
    return FSB_SUCCESS;
}

#endif /*_FSBACL_C_ */
