/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsbtmr.c,v 1.10 2017/10/09 13:13:59 siva Exp $
*
* Description: FSB timer file.
*********************************************************************/
#include "fsbinc.h"

extern UINT4        gu4Stups;

PRIVATE VOID        (*gaFsbTimerRoutines[]) (VOID *) =
{
NULL,
        FsbProcSessEstablishmentTmr,
        FsbProcENodeHouseKeepingTmr,
        FsbProcVNMacHouseKeepingTmr, FsbProcFcfKeepAliveTmr, FsbProcMbsmTmr};

/****************************************************************************
*                                                                           *
* Function     : FsbTmrInit                                                 *
*                                                                           *
* Description  : Function to create user timer list                         *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : FSB_SUCCESS/FSB_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
FsbTmrInit (VOID)
{

    if (TmrCreateTimerList (FSB_TASK_NAME, FSB_TIMER_EXP_EVENT,
                            NULL,
                            (tTimerListId) & gFsbGlobals.FsbTimerId)
        == TMR_FAILURE)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbTimerInit ()"
                         " TMR: Timer list creation failed\n");
        return (FSB_FAILURE);
    }

    return (FSB_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : FsbTmrStartTimer                                          *
*                                                                           *
* Description  : Function to start the timer                                *
*                                                                           *
* Input        : pFsbTimer :- Points to the user timer                     *
*                TimerId    :- Timer Id                                     *
*                u4Timeout  :- The valu for which the timer to be started   *
*                pTmrContext :- Refers to the context that will be used     *
*                               for processing the timer                    *
* Output       : None                                                       *
*                                                                           *
* Returns      : FSB_SUCCESS/FSB_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
FsbTmrStartTimer (tFsbTimer * pFsbTimer, eFsbTimerId TimerId,
                  UINT4 u4TimeOut, VOID *pTmrContext)
{
    if (FSB_NODE_STATUS () != FSB_NODE_ACTIVE)
    {
        /*Start the timers only in Active Node */
        return FSB_SUCCESS;
    }

    if (FSB_IS_TIMER_ACTIVE (pFsbTimer) == TRUE)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_DEBUGGING_LEVEL, FSB_OS_RESOURCE_TRC,
                         "UserTmrStartTimer:Attempt to start"
                         "already running timer\n");
        return FSB_FAILURE;
    }

    FSB_ACTIVATE_TIMER (pFsbTimer);
    pFsbTimer->u1TimerId = (UINT1) TimerId;
    pFsbTimer->pContext = pTmrContext;
    pFsbTimer->Node.u4Data = (FS_ULONG) pFsbTimer;

    switch (pFsbTimer->u1TimerId)
    {

        case FSB_SESS_ESTABLISHMENT_TIMER_ID:
        case FSB_ENODE_HOUSE_KEEPING_TIMER_ID:
        case FSB_VNMAC_HOUSE_KEEPING_TIMER_ID:
        case FSB_FCF_KEEP_ALIVE_TIMER_ID:
        case FSB_MBSM_TIMER_ID:
            break;
        default:
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbTmrStartTimer: Timer id invalid\n");
            return (FSB_FAILURE);

    }

    if (TmrStartTimer
        ((tTimerListId) gFsbGlobals.FsbTimerId, &(pFsbTimer->Node),
         (u4TimeOut * SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) == TMR_SUCCESS)
    {
        return (FSB_SUCCESS);
    }
    else
    {
        return (FSB_FAILURE);
    }
}

/****************************************************************************
*                                                                           *
* Function     : FsbTmrStopTimer                                            *
*                                                                           *
* Description  : Function invoked to stop timer                             *
*                                                                           *
* Input        : pFsbTimer :- Points to the timer to be stopped             *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : FSB_SUCCESS/FSB_FAILURE                                    *
*                                                                           *
****************************************************************************/
UINT4
FsbTmrStopTimer (tFsbTimer * pFsbTimer)
{
    if (FSB_IS_TIMER_ACTIVE (pFsbTimer) == TRUE)
    {
        if (TmrStopTimer
            ((tTimerListId) gFsbGlobals.FsbTimerId,
             &(pFsbTimer->Node)) == TMR_FAILURE)
        {
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                             FSB_DEBUGGING_LEVEL, FSB_OS_RESOURCE_TRC,
                             "FsbTmrStopTimer:TmrStopTimer failed!\n");
            return FSB_FAILURE;
        }
        FSB_INIT_TIMER (pFsbTimer);
    }
    return FSB_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : FsbTmrProcessTimer                                         *
*                                                                           *
* Description  : Function invoked to process the expired timers             *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
****************************************************************************/
VOID
FsbTmrProcessTimer (VOID)
{
    tFsbTimer          *pFsbTimer = NULL;
    tTmrAppTimer       *pExpTimer = NULL;

    pExpTimer = TmrGetNextExpiredTimer (gFsbGlobals.FsbTimerId);
    while (pExpTimer != NULL)
    {
        pFsbTimer = (tFsbTimer *) pExpTimer->u4Data;

        if (pFsbTimer != NULL)
        {
            FSB_INIT_TIMER (pFsbTimer);
            if ((pFsbTimer->u1TimerId > FSB_MIN_TIMER_ID) &&
                (pFsbTimer->u1TimerId < FSB_MAX_TIMER_ID))
            {
                (*gaFsbTimerRoutines[pFsbTimer->u1TimerId])
                    (pFsbTimer->pContext);
            }
        }
        pExpTimer = TmrGetNextExpiredTimer (gFsbGlobals.FsbTimerId);
    }
}

/****************************************************************************
*                                                                           *
* Function     : FsbProcSessEstablishmentTmr                                *
*                                                                           *
* Description  : Function invoked when session establishmen timer expires   *
*                                                                           *
* Input        : pTmrContext :- Context that will be used for processing    *
*                the session establishmen timer                             *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : FSB_SUCCESS/FSB_FAILURE                                    *
*                                                                           *
****************************************************************************/
VOID
FsbProcSessEstablishmentTmr (VOID *pTmrContext)
{
    tFsbFipSessEntry   *pFsbFipSessEntry = NULL;
    tFsbTrapInfo        FsbTrapInfo;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT4               u4ProgressStatus = OSIX_FALSE;

    FSB_MEMSET (&FsbTrapInfo, 0, sizeof (tFsbTrapInfo));
    FSB_MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);

    pFsbFipSessEntry = (tFsbFipSessEntry *) pTmrContext;
    /* When MBSM in progress restart the session Established Timer */
    L2IwfMbsmGetNpSyncProgressStatus (&u4ProgressStatus);

    /* When ISSU maintenance mode is in progress restart the session establishment timer */
    if ((IssuGetMaintModeOperation () == OSIX_TRUE) ||
        (u4ProgressStatus == OSIX_TRUE))
    {
        /* Start the ESTABLISHMENT TIMER */
        if (FsbTmrStartTimer (&(pFsbFipSessEntry->FsbSessEstablishmentTmr),
                              FSB_SESS_ESTABLISHMENT_TIMER_ID,
                              FSB_SESS_ESTABLISHMENT_TMR_VALUE,
                              pFsbFipSessEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbProcSessEstablishmentTmr: FsbTmrStartTimer failed\r\n");

        }
        return;
    }
    if (pFsbFipSessEntry != NULL)
    {
        /* If the session establishment timer is expired,
         * send trap and  delete the FIP session entry and 
         * its associated filters and also release the memory */
        FsbTrapInfo.u4ContextId = pFsbFipSessEntry->u4ContextId;
        FsbTrapInfo.u4FsbSessionEnodeIfIndex = pFsbFipSessEntry->u4ENodeIfIndex;
        FSB_MEMCPY (FsbTrapInfo.FsbSessionEnodeMacAddress,
                    pFsbFipSessEntry->ENodeMacAddr, MAC_ADDR_LEN);
        FsbTrapInfo.u2FsbSessionVlanId = pFsbFipSessEntry->u2VlanId;
        FsbSnmpIfSendTrap (FSB_SESSION_CLEAR, &FsbTrapInfo);

        if (CfaCliGetIfName (pFsbFipSessEntry->u4ENodeIfIndex,
                             (INT1 *) au1IfName) == CLI_FAILURE)
        {
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "Failed to get the Interface name for IfIndex - %d during "
                             "expiry of Session Establishment Timer\n",
                             pFsbFipSessEntry->u4ENodeIfIndex);
        }
        FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                         FSB_CRITICAL_LEVEL, FSB_NONE,
                         "\nFsbProcSessEstablishmentTmr: FIP Session"
                         "Cleared for Context id: %d, VLAN Id: %d, "
                         "Enode Interface name:[%s], ENode If"
                         "Index: %d, ENode Mac Addr:%x:%x:%x:%x:%x:%x\n",
                         pFsbFipSessEntry->u4ContextId,
                         pFsbFipSessEntry->u2VlanId,
                         au1IfName,
                         pFsbFipSessEntry->u4ENodeIfIndex,
                         pFsbFipSessEntry->ENodeMacAddr[0],
                         pFsbFipSessEntry->ENodeMacAddr[1],
                         pFsbFipSessEntry->ENodeMacAddr[2],
                         pFsbFipSessEntry->ENodeMacAddr[3],
                         pFsbFipSessEntry->ENodeMacAddr[4],
                         pFsbFipSessEntry->ENodeMacAddr[5]);
#ifdef SYSLOG_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gFsbGlobals.u4SyslogId,
                      "\nFIP Session Cleared for Context id: %d, VLAN Id: %d, ENode Intf: %s, ENode Mac Addr:%x:%x:%x:%x:%x:%x",
                      pFsbFipSessEntry->u4ContextId,
                      pFsbFipSessEntry->u2VlanId,
                      au1IfName,
                      pFsbFipSessEntry->ENodeMacAddr[0],
                      pFsbFipSessEntry->ENodeMacAddr[1],
                      pFsbFipSessEntry->ENodeMacAddr[2],
                      pFsbFipSessEntry->ENodeMacAddr[3],
                      pFsbFipSessEntry->ENodeMacAddr[4],
                      pFsbFipSessEntry->ENodeMacAddr[5]));
#endif

        pFsbFipSessEntry->u1DeleteReasonCode =
            FSB_REASON_CODE_ESTABLISHMENT_TMR_EXPIRY;
        if (FsbDeleteFIPSessionEntry (pFsbFipSessEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbProcSessEstablishmentTmr: "
                             "FsbDeleteFIPSessionEntry failed for VLAN ID: %d, If Index: %d\r\n",
                             FsbTrapInfo.u2FsbSessionVlanId,
                             FsbTrapInfo.u4FsbSessionEnodeIfIndex);
            return;
        }
        if (FsbRedSyncUpFIPSessionDelete (pFsbFipSessEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbProcSessEstablishmentTmr: "
                             "FsbRedSyncUpFIPSessEntry failed for VLAN ID: %d, If Index: %d\r\n",
                             FsbTrapInfo.u2FsbSessionVlanId,
                             FsbTrapInfo.u4FsbSessionEnodeIfIndex);
            return;
        }
    }
    return;
}

/****************************************************************************
*                                                                           *
* Function     : FsbProcENodeHouseKeepingTmr                                *
*                                                                           *
* Description  : Function invoked when house keeping timer of ENode expires *
*                                                                           *
* Input        : pTmrContext :- Context that will be used for processing    *
*                the house keeping timer of ENode                           *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : FSB_SUCCESS/FSB_FAILURE                                    *
*                                                                           *
*****************************************************************************/
VOID
FsbProcENodeHouseKeepingTmr (VOID *pTmrContext)
{
    tFsbFipSessEntry   *pFsbFipSessEntry = NULL;
    tFsbTrapInfo        FsbTrapInfo;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT4               u4ProgressStatus = OSIX_FALSE;

    FSB_MEMSET (&FsbTrapInfo, 0, sizeof (tFsbTrapInfo));
    FSB_MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);

    pFsbFipSessEntry = (tFsbFipSessEntry *) pTmrContext;

    if (pFsbFipSessEntry != NULL)
    {

        /* When MBSM in progress restart the session ENode house keeping timer */

        L2IwfMbsmGetNpSyncProgressStatus (&u4ProgressStatus);

        if (u4ProgressStatus == OSIX_TRUE)
        {
            /* Start the ENode house keeping TIMER */
            if (FsbTmrStartTimer (&(pFsbFipSessEntry->FsbHouseKeepingTmr),
                                  FSB_ENODE_HOUSE_KEEPING_TIMER_ID,
                                  pFsbFipSessEntry->u2HouseKeepingTimePeriod,
                                  pFsbFipSessEntry) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbProcENodeHouseKeepingTmr: FsbTmrStartTimer failed\r\n");
            }
            return;
        }
        /* When ISSU maintenance mode is in progress restart the session
         * ENode house keeping timer */
        if (IssuGetMaintModeOperation () == OSIX_TRUE)
        {
            /* Start the ENode house keeping TIMER */
            if (FsbTmrStartTimer (&(pFsbFipSessEntry->FsbHouseKeepingTmr),
                                  FSB_ENODE_HOUSE_KEEPING_TIMER_ID,
                                  pFsbFipSessEntry->u2HouseKeepingTimePeriod,
                                  pFsbFipSessEntry) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbProcENodeHouseKeepingTmr: FsbTmrStartTimer failed\r\n");
            }
            pFsbFipSessEntry->u1IssuMaintenanceMode = FSB_TRUE;
            return;
        }

        /* When ISSU maintenance mode is disabled (if we just come out of
         * the maintenance mode), restart the session
         * ENode house keeping timer. This is done to avoid
         * immediate expiry of house keeping timer, which will cause
         * the session to be deleted*/
        if (pFsbFipSessEntry->u1IssuMaintenanceMode == FSB_TRUE)
        {
            /* Start the ENode house keeping TIMER */
            if (FsbTmrStartTimer (&(pFsbFipSessEntry->FsbHouseKeepingTmr),
                                  FSB_ENODE_HOUSE_KEEPING_TIMER_ID,
                                  pFsbFipSessEntry->u2HouseKeepingTimePeriod,
                                  pFsbFipSessEntry) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbProcENodeHouseKeepingTmr: FsbTmrStartTimer failed\r\n");
            }
            /* Reset the varaible, as this restart needs to happen only once */
            pFsbFipSessEntry->u1IssuMaintenanceMode = FSB_FALSE;
            return;
        }

        /* If the house keeping timer is expired,
         * send trap and  delete the FIP session entry and 
         * its associated filters and also release the memory */
        FsbTrapInfo.u4ContextId = pFsbFipSessEntry->u4ContextId;
        FsbTrapInfo.u4FsbSessionEnodeIfIndex = pFsbFipSessEntry->u4ENodeIfIndex;
        FSB_MEMCPY (FsbTrapInfo.FsbSessionEnodeMacAddress,
                    pFsbFipSessEntry->ENodeMacAddr, MAC_ADDR_LEN);
        FsbTrapInfo.u2FsbSessionVlanId = pFsbFipSessEntry->u2VlanId;
        FsbSnmpIfSendTrap (FSB_SESSION_CLEAR, &FsbTrapInfo);

        if (CfaCliGetIfName
            (pFsbFipSessEntry->u4ENodeIfIndex,
             (INT1 *) au1IfName) == CLI_FAILURE)
        {
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "Failed to get the Interface name for IfIndex - %d during "
                             "expiry of ENode HouseKeeping Timer\n",
                             pFsbFipSessEntry->u4ENodeIfIndex);
        }

        FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                         FSB_CRITICAL_LEVEL, FSB_NONE,
                         "\nFsbProcENodeHouseKeepingTmr: FIP Session"
                         "Cleared for Context id: %d, VLAN Id: %d, "
                         "Enode Interface name: [%s], ENode If"
                         "Index: %d, ENode Mac Addr:%x:%x:%x:%x:%x:%x\n",
                         pFsbFipSessEntry->u4ContextId,
                         pFsbFipSessEntry->u2VlanId,
                         au1IfName,
                         pFsbFipSessEntry->u4ENodeIfIndex,
                         pFsbFipSessEntry->ENodeMacAddr[0],
                         pFsbFipSessEntry->ENodeMacAddr[1],
                         pFsbFipSessEntry->ENodeMacAddr[2],
                         pFsbFipSessEntry->ENodeMacAddr[3],
                         pFsbFipSessEntry->ENodeMacAddr[4],
                         pFsbFipSessEntry->ENodeMacAddr[5]);
#ifdef SYSLOG_WANTED
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gFsbGlobals.u4SyslogId,
                      "\nFIP Session Cleared for Context id: %d,VLAN Id: %d, ENode Intf: %s, ENode Mac Addr:%x:%x:%x:%x:%x:%x",
                      pFsbFipSessEntry->u4ContextId,
                      pFsbFipSessEntry->u2VlanId,
                      au1IfName,
                      pFsbFipSessEntry->ENodeMacAddr[0],
                      pFsbFipSessEntry->ENodeMacAddr[1],
                      pFsbFipSessEntry->ENodeMacAddr[2],
                      pFsbFipSessEntry->ENodeMacAddr[3],
                      pFsbFipSessEntry->ENodeMacAddr[4],
                      pFsbFipSessEntry->ENodeMacAddr[5]));
#endif
        pFsbFipSessEntry->u1DeleteReasonCode = FSB_REASON_CODE_ENODE_TMR_EXPIRY;
        if (FsbClearFIPSession (pFsbFipSessEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbProcENodeHouseKeepingTmr: "
                             "FsbClearFIPSession failed for VLAN ID: %d, If Index: %d\r\n",
                             FsbTrapInfo.u2FsbSessionVlanId,
                             FsbTrapInfo.u4FsbSessionEnodeIfIndex);
            return;
        }
        if (FsbRedSyncUpFIPSessionDelete (pFsbFipSessEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbProcENodeHouseKeepingTmr: "
                             "FsbRedSyncUpFIPSessEntry failed for VLAN ID: %d, If Index: %d\r\n",
                             FsbTrapInfo.u2FsbSessionVlanId,
                             FsbTrapInfo.u4FsbSessionEnodeIfIndex);
            return;
        }
    }
    return;
}

/*****************************************************************************
*                                                                            *
* Function     : FsbProcVNMacHouseKeepingTmr                                 *
*                                                                            *
* Description  : Function invoked when house keeping timer of VN Mac expires *
*                                                                            *
* Input        : pTmrContext :- Context that will be used for processing     *
*                the house keeping timer of VN Mac                           *
*                                                                            *
* Output       : None                                                        *
*                                                                            *
* Returns      : FSB_SUCCESS/FSB_FAILURE                                     *
*                                                                            *
*****************************************************************************/
VOID
FsbProcVNMacHouseKeepingTmr (VOID *pTmrContext)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;
    UINT4               u4ProgressStatus = OSIX_FALSE;

    pFsbFipSessFCoEEntry = (tFsbFipSessFCoEEntry *) pTmrContext;

    if (pFsbFipSessFCoEEntry != NULL)
    {

        /* When MBSM in progress restart the session VNMAC ENode house keeping timer */
        L2IwfMbsmGetNpSyncProgressStatus (&u4ProgressStatus);
        if (u4ProgressStatus == OSIX_TRUE)
        {
            /* Start the VNMAC house keeping TIMER */
            if (FsbTmrStartTimer (&(pFsbFipSessFCoEEntry->FsbHouseKeepingTmr),
                                  FSB_VNMAC_HOUSE_KEEPING_TIMER_ID,
                                  pFsbFipSessFCoEEntry->
                                  u2HouseKeepingTimePeriod,
                                  pFsbFipSessFCoEEntry) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                                 u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbProcVNMacHouseKeepingTmr: FsbTmrStartTimer failed\r\n");
            }
            return;
        }
        /* When ISSU maintenance mode is in progress restart the session
         * VNMAC house keeping timer */
        if (IssuGetMaintModeOperation () == OSIX_TRUE)
        {
            /* Start the VNMAC house keeping TIMER */
            if (FsbTmrStartTimer (&(pFsbFipSessFCoEEntry->FsbHouseKeepingTmr),
                                  FSB_VNMAC_HOUSE_KEEPING_TIMER_ID,
                                  pFsbFipSessFCoEEntry->
                                  u2HouseKeepingTimePeriod,
                                  pFsbFipSessFCoEEntry) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                                 u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbProcVNMacHouseKeepingTmr: FsbTmrStartTimer failed\r\n");
            }
            pFsbFipSessFCoEEntry->u1IssuMaintenanceMode = FSB_TRUE;
            return;
        }
        /* When ISSU maintenance mode is disabled (if we just come out of
         * the maintenance mode), restart the session
         * VNMAC house keeping timer. This is done to avoid
         * immediate expiry of house keeping timer, which will cause
         * the session to be deleted*/
        if (pFsbFipSessFCoEEntry->u1IssuMaintenanceMode == FSB_TRUE)
        {
            /* Start the VNMAC house keeping TIMER */
            if (FsbTmrStartTimer (&(pFsbFipSessFCoEEntry->FsbHouseKeepingTmr),
                                  FSB_VNMAC_HOUSE_KEEPING_TIMER_ID,
                                  pFsbFipSessFCoEEntry->
                                  u2HouseKeepingTimePeriod,
                                  pFsbFipSessFCoEEntry) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                                 u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbProcVNMacHouseKeepingTmr: FsbTmrStartTimer failed\r\n");
            }
            /* Reset the varaible, as this restart needs to happen only once */
            pFsbFipSessFCoEEntry->u1IssuMaintenanceMode = FSB_FALSE;
            return;
        }
        FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->u4ContextId,
                         FSB_CRITICAL_LEVEL, FSB_NONE,
                         "\nFsbProcVNMacHouseKeepingTmr: FIP FCoE Session"
                         "Cleared for Context id: %d, Vlan Id: %d, ENode If"
                         "Index: %d, ENode Mac Addr:%x:%x:%x:%x:%x:%x\n",
                         pFsbFipSessFCoEEntry->pFsbFipSessEntry->u4ContextId,
                         pFsbFipSessFCoEEntry->u2VlanId,
                         pFsbFipSessFCoEEntry->u4ENodeIfIndex,
                         pFsbFipSessFCoEEntry->FCoEMacAddr[0],
                         pFsbFipSessFCoEEntry->FCoEMacAddr[1],
                         pFsbFipSessFCoEEntry->FCoEMacAddr[2],
                         pFsbFipSessFCoEEntry->FCoEMacAddr[3],
                         pFsbFipSessFCoEEntry->FCoEMacAddr[4],
                         pFsbFipSessFCoEEntry->FCoEMacAddr[5]);

        pFsbFipSessFCoEEntry->u1DeleteReasonCode =
            FSB_REASON_CODE_VNMAC_TMR_EXPIRY;
        /* Delete FCoE Entry and its respective filters */
        if (FsbDeleteFCoEEntry (pFsbFipSessFCoEEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                             u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbProcVNMacHouseKeepingTmr: "
                             "FsbDeleteFCoEEntry failed\r\n");
            return;
        }
        if (FsbRedSyncUpDeleteFCoEEntry (pFsbFipSessFCoEEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                             u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbProcVNMacHouseKeepingTmr: "
                             "FsbRedSyncUpDeleteFCoEEntry failed\r\n");
            return;
        }
    }
    return;
}

/****************************************************************************
*                                                                           *
* Function     : FsbProcFcfKeepAliveTmr                                     *
*                                                                           *
* Description  : Function invoked when fcf keep timer expires               *
*                                                                           *
* Input        : pTmrContext :- Context that will be used for processing    *
*                the fcf keep alive timer                                   *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : FSB_SUCCESS/FSB_FAILURE                                    *
*                                                                           *
****************************************************************************/
VOID
FsbProcFcfKeepAliveTmr (VOID *pTmrContext)
{
    tFsbFcfEntry       *pFsbFcfEntry = NULL;
    UINT4               u4ProgressStatus = OSIX_FALSE;

    pFsbFcfEntry = (tFsbFcfEntry *) pTmrContext;

    /* When MBSM in progress restart the session VNMAC ENode house keeping timer */
    L2IwfMbsmGetNpSyncProgressStatus (&u4ProgressStatus);

    if (u4ProgressStatus == OSIX_TRUE)
    {
        if (FsbTmrStartTimer (&(pFsbFcfEntry->FCFAliveTimer),
                              FSB_FCF_KEEP_ALIVE_TIMER_ID,
                              pFsbFcfEntry->u4FcfKeepAliveTimerValue,
                              pFsbFcfEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbProcFcfKeepAliveTmr: FsbTmrStartTimer failed\r\n");
        }
        return;
    }
    /* When ISSU maintenance mode is in progress restart the keepAlive timer */
    if (IssuGetMaintModeOperation () == OSIX_TRUE)
    {
        if (FsbTmrStartTimer (&(pFsbFcfEntry->FCFAliveTimer),
                              FSB_FCF_KEEP_ALIVE_TIMER_ID,
                              pFsbFcfEntry->u4FcfKeepAliveTimerValue,
                              pFsbFcfEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbProcFcfKeepAliveTmr: FsbTmrStartTimer failed\r\n");
        }
        pFsbFcfEntry->u1IssuMaintenanceMode = FSB_TRUE;
        return;
    }
    /* When ISSU maintenance mode is disabled (if we just come out of
     * the maintenance mode), restart the
     * FCF timer. This is done to avoid
     * immediate expiry of FCF timer, which will cause
     * the FCF entry to be deleted*/
    if (pFsbFcfEntry->u1IssuMaintenanceMode == FSB_TRUE)
    {
        if (FsbTmrStartTimer (&(pFsbFcfEntry->FCFAliveTimer),
                              FSB_FCF_KEEP_ALIVE_TIMER_ID,
                              pFsbFcfEntry->u4FcfKeepAliveTimerValue,
                              pFsbFcfEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbProcFcfKeepAliveTmr: FsbTmrStartTimer failed\r\n");
        }
        /* Reset the varaible, as this restart needs to happen only once */
        pFsbFcfEntry->u1IssuMaintenanceMode = FSB_FALSE;
        return;
    }

    if (pFsbFcfEntry != NULL)
    {
        /* Before deleting the FCF entry,
         * delete any FIP session entry present for this FCF*/
        FsbDeleteFIPSessEntryForFcf (pFsbFcfEntry);
        FsbDeleteDefaultFCFFilter (pFsbFcfEntry);

        if (FsbRedSyncUpFCFEntry (pFsbFcfEntry, FSB_FCF_DEL) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbProcFcfKeepAliveTmr: FsbRedSyncUpAddFCFEntry failed\r\n");
            return;
        }

        /* Remove Static FCF Mac as FCF is deleted */
        if (FsbConfigUcastEntry (pFsbFcfEntry->u4ContextId,
                                 pFsbFcfEntry->FcfMacAddr,
                                 pFsbFcfEntry->u2VlanId,
                                 pFsbFcfEntry->u4FcfIfIndex,
                                 VLAN_DELETE) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbProcFcfKeepAliveTmr : Deleting Static FCF Mac failed for"
                             "VLAN ID: %d, If Index: %d\r\n",
                             pFsbFcfEntry->u2VlanId,
                             pFsbFcfEntry->u4FcfIfIndex);
            return;
        }
        /*The FCF timer is expired and hence FCF discovery timout 
         *  occurs and hence corresponding object in vlan statistics table is
         * incremented*/
        FsbUpdateVLANStatistics (pFsbFcfEntry->u4ContextId,
                                 pFsbFcfEntry->u2VlanId,
                                 FSB_FCF_DISCOVERY_TIMEOUT);

        /* If the FCF keep alive timer is expired,
         * delete the FCF entry and also release the memory*/

        if (RBTreeRemove (gFsbGlobals.FsbFcfTable,
                          (tRBElem *) pFsbFcfEntry) != RB_SUCCESS)
        {
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                             FSB_OS_RESOURCE_TRC,
                             "FsbProcFcfKeepAliveTmr: RBTreeRemove for "
                             " pFsbFcfEntry failed\r\n");
            return;
        }
        MemReleaseMemBlock (FSB_FCF_ENTRIES_MEMPOOL_ID, (UINT1 *) pFsbFcfEntry);
    }
    return;
}

/****************************************************************************
*                                                                           *
* Function     : FsbProcMbsmTmr                                             *
*                                                                           *
* Description  : Function invoked when Msbm Stagerring Timer expires        *
*                                                                           *
* Input        : pTmrContext :- Context that will be used for processing    *
*                the Mbsm stagerring timer                                  *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : FSB_SUCCESS/FSB_FAILURE                                    *
*                                                                           *
****************************************************************************/
VOID
FsbProcMbsmTmr (VOID *pTmrContext)
{
    tFsbNotifyParams   *pMsg = NULL;
    tFsbIntfEntry      *pFsbIntfEntry = NULL;

    pFsbIntfEntry = (tFsbIntfEntry *) pTmrContext;

    if (pFsbIntfEntry != NULL)
    {
        if ((pMsg = (tFsbNotifyParams *) MemAllocMemBlk
             (gFsbGlobals.FsbNotifyParamsPoolId)) == NULL)
        {
            FSB_CONTEXT_TRC (pFsbIntfEntry->u4ContextId, FSB_DEBUGGING_LEVEL,
                             FSB_BUFFER_TRC, "FsbProcMbsmTmr : MemAllocMemBlk"
                             " for pMsg failed\r\n");
            return;
        }
        pMsg->u4ContextId = pFsbIntfEntry->u4ContextId;
        pMsg->u2VlanId = pFsbIntfEntry->u2VlanId;
        pMsg->u4IfIndex = pFsbIntfEntry->u4IfIndex;
        pMsg->u4MsgType = FSB_MBSM_STAGERRING_MSG;
        FsbEnQFrameToFsbTask (pMsg);

    }
    return;
}
