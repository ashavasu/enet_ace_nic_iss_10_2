/********************************************************************
*  Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: fsbmain.c,v 1.17 2017/10/09 13:13:58 siva Exp $
*
* Description: This file contains the init and de-init routines for
*              FIP-snooping Task,MemPool and Timers
*
*******************************************************************/
#ifndef _FSBMAIN_C_
#define _FSBMAIN_C_

#include "fsbinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbTaskMain                                      */
/*                                                                           */
/*    Description         : This function will perform following task in FSB */
/*                          Module:                                          */
/*                          o  Initialized the FSB task (Sem, queue,        */
/*                             mempool creation).                            */
/*                          o  Register Fsb Mib with SNMP module            */
/*                          o  Wait for external event and call the          */
/*                             corresponding even handler routines           */
/*                             on receiving the events.                      */
/*                                                                           */
/*    Input(s)            : pi1Arg -  Pointer to input arguments             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
FsbTaskMain (INT1 *pi1Arg)
{
    UNUSED_PARAM (pi1Arg);

    UINT4               u4Events = 0;

    if (FsbInit () != FSB_SUCCESS)
    {
        /* De-initialization of FSB Task */
        FsbDeInit ();
        FSB_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

#ifdef SNMP_2_WANTED
    RegisterFSFSB ();
#endif
#ifdef SYSLOG_WANTED
    /* Create SYS_LOG Handler for logging FIP-snooping Module */
    gFsbGlobals.u4SyslogId = (UINT4) (SYS_LOG_REGISTER ((CONST UINT1 *) "FSB",
                                                        SYSLOG_CRITICAL_LEVEL |
                                                        SYSLOG_INFO_LEVEL));
#endif

    FSB_INIT_COMPLETE (OSIX_SUCCESS);

    while (1)
    {
        if (OsixEvtRecv (FSB_TASK_ID, FSB_ALL_EVENTS,
                         OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            /* Take the FSB lock */
            FsbLock ();

            if ((u4Events & FSB_QUEUE_EVENT) != 0)
            {
                /* Calls the function to handle various events which
                 * will take care differnt kind of messages 
                 * FIP_PACKET_RECV message, PORT_DOWN message */
                FsbInterfaceHandleQueueEvent ();
            }
            if ((u4Events & FSB_TIMER_EXP_EVENT) != 0)
            {
                FsbTmrProcessTimer ();
            }
            if ((u4Events & FSB_RED_BULK_UPD_EVENT) != 0)
            {
                FsbRedHandleBulkReqEvent ();
            }
            /* Release the lock, after parsing the recieved packet */
            FsbUnLock ();
        }
    }
}

/*****************************************************************************/
/* Function Name      : FsbInterfaceHandleQueueEvent                         */
/*                                                                           */
/* Description        : This Initiates the Action for the Queue Event. It    */
/*                      receives the information from the buffer received    */
/*                      from the Queue and passes it to FIP-snooping parsing */
/*                      module to Process It.                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
FsbInterfaceHandleQueueEvent (VOID)
{
    tVlanEvbSbpArray    SbpArray;
    tFsbNotifyParams   *pMsg = NULL;
    INT4                i4BrgPortType = 0;
    UINT1               u1LoopIndex = 0;
#ifdef MBSM_WANTED
    INT4                i4RetStatus = 0;
#endif

    while (OsixQueRecv (FSB_QUEUE_ID,
                        (UINT1 *) &pMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pMsg == NULL)
        {
            continue;
        }
        switch (pMsg->u4MsgType)
        {
            case FSB_PACKET_RECV_MSG:
                if (FsbHandleFIPFrame (pMsg) == FSB_SUCCESS)
                {
                    /* Call the FsbDepacketizer function to 
                     * parse the FIP packet recieved*/
                    FsbDepacketizer (pMsg);
                }
                break;

            case FSB_OPER_STATUS_CHG_MSG:
                /* Call the FsbHandlePortDown 
                 * when port is blocked to delete
                 * the dynamically learnt FCF entries and
                 * FIP session entries */
                if (pMsg->u1OperStatus == FSB_IF_DOWN)
                {
                    FSB_CONTEXT_TRC (pMsg->u4ContextId, FSB_INFORMATIONAL_LEVEL,
                                     FSB_NONE,
                                     "Oper status down notification received for"
                                     " IfIndex : %d\r\n", pMsg->u4IfIndex);
                    FsbHandlePortDown (pMsg->u4IfIndex);
                }
                FsbCfaGetInterfaceBrgPortType (pMsg->u4IfIndex, &i4BrgPortType);

                if (i4BrgPortType == CFA_UPLINK_ACCESS_PORT)
                {
                    MEMSET (&SbpArray, 0, sizeof (tVlanEvbSbpArray));
                    FsbVlanApiEvbGetSbpPortsOnUap (pMsg->u4IfIndex, &SbpArray);
                    while (u1LoopIndex < VLAN_EVB_MAX_SBP_PER_UAP)
                    {
                        if (SbpArray.au4SbpArray[u1LoopIndex] == 0)
                        {
                            /* No more SBPs are present on this interface */
                            break;
                        }
                        if ((pMsg->u1OperStatus == FSB_IF_DOWN) ||
                            (pMsg->u1OperStatus == FSB_IF_NP))
                        {
                            FsbHandlePortDown (SbpArray.
                                               au4SbpArray[u1LoopIndex]);
                        }
                        u1LoopIndex++;
                    }
                }
                L2_SYNC_GIVE_SEM ();
                break;

            case FSB_PORT_UNMAP_MSG:
                /* When the context is deleted in the switch,
                 * delete all the databased maintained for 
                 * this context(FIP Snooping VLAN entries,
                 * FCF entries, Interface entries and 
                 * FIP session entries*/
                FSB_CONTEXT_TRC (pMsg->u4ContextId, FSB_INFORMATIONAL_LEVEL,
                                 FSB_NONE,
                                 "Port unmap notification received for"
                                 " IfIndex : %d\r\n", pMsg->u4IfIndex);
                FsbHandleDeletePort (pMsg->u4IfIndex);
                L2_SYNC_GIVE_SEM ();
                break;

            case FSB_PORT_DISCARDING_MSG:
                /* Call the FsbHandlePortDown 
                 * when port is blocked to delete
                 * the dynamically learnt FCF entries and
                 * FIP session entries */
                FSB_CONTEXT_TRC (pMsg->u4ContextId, FSB_INFORMATIONAL_LEVEL,
                                 FSB_NONE,
                                 "Port discarding notification received for"
                                 " IfIndex : %d\r\n", pMsg->u4IfIndex);
                FsbHandlePortDown (pMsg->u4IfIndex);
                break;

            case FSB_CONTEXT_DELETE_MSG:
                /* When the context is deleted in the switch,
                 * delete all the databased maintained for 
                 * this context(FIP Snooping VLAN entries,
                 * FCF entries, Interface entries and 
                 * FIP session entries*/
                FSB_CONTEXT_TRC (pMsg->u4ContextId, FSB_INFORMATIONAL_LEVEL,
                                 FSB_NONE,
                                 "Context delete notification received for"
                                 " Context : %d\r\n", pMsg->u4ContextId);
                FsbCxtHandleDeleteContext (pMsg->u4ContextId);
                break;

            case FSB_VLAN_DELETE_MSG:
                /* When a VLAN delete notification is recieved from L2IWf,
                 * below actions need to be taken
                 * 1) Delete fip session entries mapped to the deleted Fcoe Vlan
                 * 2) Delete all FCF entries mapped to the deleted Fcoe Vlan
                 * 3) Delete all the tFsbIntfEntry mapped to VLAN in the Context,
                 * 4) Delete tFsbFipSnoopingEntry as the VLAN no longer exists in L2IWF */
                FSB_CONTEXT_TRC (pMsg->u4ContextId, FSB_INFORMATIONAL_LEVEL,
                                 FSB_NONE,
                                 "VLAN delete notification received for"
                                 " Context : %d VLAN : %d\r\n",
                                 pMsg->u4ContextId, pMsg->u2VlanId);
                FsbHandleDeleteVlanIndication (pMsg->u4ContextId,
                                               pMsg->u2VlanId);
                break;

            case FSB_ADD_PORT_TO_AGG:
                FSB_CONTEXT_TRC (pMsg->u4ContextId, FSB_INFORMATIONAL_LEVEL,
                                 FSB_NONE,
                                 "Add Port to LAG notification received for"
                                 " IfIndex : %d LAG Index %d\r\n",
                                 pMsg->u4IfIndex, pMsg->u4AggIndex);
                FsbHandleAddPortToAgg (pMsg->u4IfIndex,
                                       (UINT2) (pMsg->u4AggIndex));
                break;

            case FSB_REMOVE_PORT_FROM_AGG:
                FSB_CONTEXT_TRC (pMsg->u4ContextId, FSB_INFORMATIONAL_LEVEL,
                                 FSB_NONE,
                                 "Remove Port from LAG notification received for"
                                 " IfIndex : %d LAG Index %d\r\n",
                                 pMsg->u4IfIndex, pMsg->u4AggIndex);
                FsbHandleRemovePortFromAgg (pMsg->u4IfIndex,
                                            (UINT2) (pMsg->u4AggIndex));
                break;

#ifdef MBSM_WANTED
            case FSB_MBSM_CARD_INSERT_MSG:
                FSB_CONTEXT_TRC (FSB_DEFAULT_CONTEXT_ID,
                                 FSB_INFORMATIONAL_LEVEL, FSB_NONE,
                                 "MBSM card insert notificatin received");
                FSB_MEMSET (&gFsbMbsmPortInfo, 0, sizeof (tMbsmPortInfo));

                gi4FsbProtoId = pMsg->pMbsmProtoMsg->i4ProtoCookie;
                gi4FsbMbsmSlotId = pMsg->pMbsmProtoMsg->MbsmSlotInfo.i4SlotId;
                FSB_MEMCPY (&gFsbMbsmPortInfo,
                            &(pMsg->pMbsmProtoMsg->MbsmPortInfo),
                            sizeof (tMbsmPortInfo));

                /* ACK is sent to MBSM, irrespective of success/failure case,
                 * Programming is done later, so that the other modules 
                 * are not affected.*/
                FSB_CONTEXT_TRC (FSB_DEFAULT_CONTEXT_ID, FSB_DEBUGGING_LEVEL,
                                 FSB_SESSION_TRC,
                                 "FsbInterfaceHandleQueueEvent: ACK is sent to MBSM module from FSB");
                FsbSendAckToMbsm (MBSM_SUCCESS);

                i4RetStatus =
                    FsbMbsmUpdateOnCardInsertion (&pMsg->pMbsmProtoMsg->
                                                  MbsmPortInfo,
                                                  &pMsg->pMbsmProtoMsg->
                                                  MbsmSlotInfo);

                MemReleaseMemBlock (FSB_MBSM_MSG_MEMPOOL_ID,
                                    (UINT1 *) (pMsg->pMbsmProtoMsg));
                break;

            case FSB_MBSM_CARD_REMOVE_MSG:
                FSB_CONTEXT_TRC (FSB_DEFAULT_CONTEXT_ID,
                                 FSB_INFORMATIONAL_LEVEL, FSB_NONE,
                                 "MBSM card remove notificatin received");
                FSB_MEMSET (&gFsbMbsmPortInfo, 0, sizeof (tMbsmPortInfo));
                gi4FsbProtoId = pMsg->pMbsmProtoMsg->i4ProtoCookie;
                gi4FsbMbsmSlotId = pMsg->pMbsmProtoMsg->MbsmSlotInfo.i4SlotId;
                FSB_MEMCPY (&gFsbMbsmPortInfo,
                            &(pMsg->pMbsmProtoMsg->MbsmPortInfo),
                            sizeof (tMbsmPortInfo));

                i4RetStatus =
                    FsbMbsmUpdateOnCardRemoval (&pMsg->pMbsmProtoMsg->
                                                MbsmPortInfo,
                                                &pMsg->pMbsmProtoMsg->
                                                MbsmSlotInfo);

                /* When MBSM_SUCCESS or MBSM_FAILURE is returned,
                 * Ack needs to be sent to MSBM */
                if ((i4RetStatus == MBSM_SUCCESS) ||
                    (i4RetStatus == MBSM_FAILURE))
                {
                    FSB_CONTEXT_TRC (FSB_DEFAULT_CONTEXT_ID, FSB_CRITICAL_LEVEL,
                                     FSB_NONE,
                                     "FsbInterfaceHandleQueueEvent: ACK is sent to MBSM module from FSB");
                    FsbSendAckToMbsm (i4RetStatus);
                }
                MemReleaseMemBlock (FSB_MBSM_MSG_MEMPOOL_ID,
                                    (UINT1 *) (pMsg->pMbsmProtoMsg));
                break;

            case FSB_MBSM_STAGERRING_MSG:
                FsbMbsmConfigDefaultFilter (pMsg->u4ContextId, pMsg->u2VlanId,
                                            pMsg->u4IfIndex, &gFsbMbsmPortInfo);
                break;
#endif
            case FSB_RM_MSG:
                FSB_CONTEXT_TRC (FSB_DEFAULT_CONTEXT_ID,
                                 FSB_INFORMATIONAL_LEVEL, FSB_NONE,
                                 "RM message received");
                FsbRedHandleRmEvents (pMsg);
                break;

            case FSB_PORTLIST_UPDATE_MSG:
                FSB_CONTEXT_TRC (pMsg->u4ContextId, FSB_INFORMATIONAL_LEVEL,
                                 FSB_NONE,
                                 "PortList update notification received for"
                                 " Context : %d VLAN : %d\r\n",
                                 pMsg->u4ContextId, pMsg->u2VlanId);
                FsbUpdateVlanPortList (pMsg->u4ContextId, pMsg->u2VlanId,
                                       pMsg->AddedPorts, pMsg->DeletedPorts);
                break;
            case FSB_PORT_UPDATE_MSG:
                FSB_CONTEXT_TRC (pMsg->u4ContextId, FSB_INFORMATIONAL_LEVEL,
                                 FSB_CONTROL_PATH_TRC,
                                 "Port update notification received for"
                                 " Context : %d VLAN : %d\r\n",
                                 pMsg->u4ContextId, pMsg->u2VlanId);
                FsbUpdateVlanPorts (pMsg->u4ContextId, pMsg->u2VlanId,
                                    pMsg->u4IfIndex);
                break;
            default:
                break;
        }

        MemReleaseMemBlock (FSB_NOTIFY_PARAMS_INFO_MEMPOOL_ID, (UINT1 *) pMsg);
    }
}

/*****************************************************************************/
/* Function Name      : FsbEnQFrameToFsbTask                                 */
/*                                                                           */
/* Description        : This enqueues a received frame to Fsb Interface      */
/*                      Queue and sends an event FSB_QUEUE_EVENT to the      */
/*                      Interface task.                                      */
/*                                                                           */
/* Input(s)           : Pointer to tFsbNotifyParams structure               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
FsbEnQFrameToFsbTask (tFsbNotifyParams * pMsg)
{
    UINT4               u4MsgsInQ = 0;

    if (OsixGetNumMsgsInQ ((UINT4) 0, FSB_TASK_QUEUE_NAME,
                           &u4MsgsInQ) == OSIX_SUCCESS)
    {
        if (pMsg->u4MsgType == FSB_PACKET_RECV_MSG)
        {
            if (u4MsgsInQ > FSB_TASK_QUEUE_LIMIT)
            {
                FSB_CONTEXT_TRC (pMsg->u4ContextId, FSB_DEBUGGING_LEVEL,
                                 FSB_OS_RESOURCE_TRC,
                                 "FsbEnQFrameToFsbTask: "
                                 "Queue size exceeds the limit of received packet."
                                 "Hence dropping!\r\n");
                MemReleaseMemBlock (FSB_NOTIFY_PARAMS_INFO_MEMPOOL_ID,
                                    (UINT1 *) pMsg);
                return;
            }
        }
    }
    else
    {
        FSB_CONTEXT_TRC (pMsg->u4ContextId, FSB_DEBUGGING_LEVEL,
                         FSB_OS_RESOURCE_TRC,
                         "FsbEnQFrameToFsbTask: "
                         "OsixGetNumMsgsInQ failed!\r\n");
    }

    if (OsixQueSend (FSB_QUEUE_ID, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
#ifdef MBSM_WANTED
        /* Ack needs to be sent to MSBM in failure case */
        if ((pMsg->u4MsgType == FSB_MBSM_CARD_INSERT_MSG) ||
            (pMsg->u4MsgType == FSB_MBSM_CARD_REMOVE_MSG))
        {
            FSB_CONTEXT_TRC (pMsg->u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbEnQFrameToFsbTask: "
                             "Osix Queue Send Failed!\r\n");
            FsbSendAckToMbsm (MBSM_FAILURE);
            MemReleaseMemBlock (FSB_MBSM_MSG_MEMPOOL_ID,
                                (UINT1 *) (pMsg->pMbsmProtoMsg));

        }
#endif
        FSB_CONTEXT_TRC (pMsg->u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbEnQFrameToFsbTask: "
                         "Osix Queue Send Failed!\r\n");
        MemReleaseMemBlock (FSB_NOTIFY_PARAMS_INFO_MEMPOOL_ID, (UINT1 *) pMsg);
        return;
    }

    if (OsixEvtSend (FSB_TASK_ID, FSB_QUEUE_EVENT) == OSIX_FAILURE)
    {
#ifdef MBSM_WANTED
        /* Ack needs to be sent to MSBM in failure case */
        if ((pMsg->u4MsgType == FSB_MBSM_CARD_INSERT_MSG) ||
            (pMsg->u4MsgType == FSB_MBSM_CARD_REMOVE_MSG))
        {
            FSB_CONTEXT_TRC (pMsg->u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbEnQFrameToFsbTask: "
                             "Osix Queue Send Failed!\r\n");
            FsbSendAckToMbsm (MBSM_FAILURE);
            MemReleaseMemBlock (FSB_MBSM_MSG_MEMPOOL_ID,
                                (UINT1 *) (pMsg->pMbsmProtoMsg));
        }
#endif
        FSB_CONTEXT_TRC (pMsg->u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbEnQFrameToFsbTask: "
                         "Osix Event Send Failed!\r\n");
        MemReleaseMemBlock (FSB_NOTIFY_PARAMS_INFO_MEMPOOL_ID, (UINT1 *) pMsg);
        return;
    }

    if ((pMsg->u4MsgType == FSB_OPER_STATUS_CHG_MSG) ||
        (pMsg->u4MsgType == FSB_PORT_UNMAP_MSG))
    {
        L2_SYNC_TAKE_SEM ();
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDepacketizer                                  */
/*                                                                           */
/*    Description         : This functions parses the incoming              */
/*                          FSB packet and dispatches to the appropriate     */
/*                          functions                                        */
/*                                                                           */
/*    Input(s)            : pFsbNotifyParams -  Pointer to tFsbNotifyParams  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS/FSB_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
FsbDepacketizer (tFsbNotifyParams * pFsbNotifyParams)
{
    tFSBPktInfo         FSBPktInfo;    /*FSB pkt structure */
    UINT4               u4ContextId = 0;
    UINT4               u4CurrentTime = 0;
    UINT4               u4CurrentSec = 0;
    UINT4               u4CurrentMin = 0;
    UINT4               u4CurrentHrs = 0;
    UINT2               u2VNMacCount = 0;
    UINT2               u2NoOfVNMac = 0;
    UINT2               u2DescLen = 0;
    UINT1               u1Descriptor = 0;
    UINT1               u1CmdCode = 0;
    UINT1               u1VNType = 0;
    UINT1               u1Type = 0;
    UINT1              *pu1ReadPtr = NULL;

    if (pFsbNotifyParams == NULL)
    {
        return FSB_FAILURE;
    }

    /* When ISSU maintenance mode is in progress, request for new FCoE sessions
     * should not be supported. Hence skipping the parses the incoming FSB packet*/
    if (IssuGetMaintModeOperation () == OSIX_TRUE)
    {
        return FSB_SUCCESS;
    }

    FSB_MEMSET (&FSBPktInfo, 0, sizeof (tFSBPktInfo));

    /* FIP packet is being recieved in the au1Databuf
     * Assigining it to a linear pointer, so traversing will be easier*/
    pu1ReadPtr = pFsbNotifyParams->au1DataBuf;

    /* Copy the destination and src mac address */
    FSB_MEMCPY (FSBPktInfo.DestAddr, pu1ReadPtr, MAC_ADDR_LEN);
    pu1ReadPtr += MAC_ADDR_LEN;

    FSB_MEMCPY (FSBPktInfo.SrcAddr, pu1ReadPtr, MAC_ADDR_LEN);
    pu1ReadPtr += MAC_ADDR_LEN;

    /* Retrieve the context id from the Interface Index */
    if (FsbGetContextIdFromCfaIfIndex (pFsbNotifyParams->u4IfIndex,
                                       &u4ContextId) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbDepacketizer : FsbGetContextIdFromCfaIfIndex return failure");
        return FSB_FAILURE;
    }
    FSBPktInfo.u4ContextId = u4ContextId;
    FSBPktInfo.u4IfIndex = pFsbNotifyParams->u4IfIndex;

    FSBPktInfo.u2VlanId = pFsbNotifyParams->u2VlanId;

    /*****************************************************************************/
    /*FIP Protocol             FIP Subcode                  FIP Operation        */
    /*       0001h             01h                      Discovery Solicitation   */
    /*                         02h                      Discovery Advertisement  */
    /*---------------------------------------------------------------------------*/
    /*       0002h             01h                      FLOGI Request            */
    /*                         02h                      FLOGI Response           */
    /*---------------------------------------------------------------------------*/
    /*       0003h             01h                      Keep Alive               */
    /*                         02h                      Clear Virtual Link       */
    /*---------------------------------------------------------------------------*/
    /*       0004h             01h                      VLAN Request             */
    /*                         02h                      VLAN Notification        */
    /*****************************************************************************/

    /* Moving four bytes to reach ethertype of the packet */
    pu1ReadPtr += FSB_FOUR_BYTE;
    FSB_GET_2BYTE (FSBPktInfo.u2EtherType, pu1ReadPtr);
    /* Opcode is four bytes from Ethertype, so moving the pointer two more bytes */
    pu1ReadPtr += FSB_TWO_BYTE;
    FSB_GET_2BYTE (FSBPktInfo.u2Opcode, pu1ReadPtr);
    FSB_GET_2BYTE (FSBPktInfo.u2SubOpcode, pu1ReadPtr);
    FSB_GET_2BYTE (u2DescLen, pu1ReadPtr);
    FSB_GET_2BYTE (FSBPktInfo.u2Flag, pu1ReadPtr);
    /*Copy the flag */
    /*Check whether it is FPMA or SPMA using the bit value in Flag */
    if (FSBPktInfo.u2Flag & FSB_FPMA_BIT_MASK)
    {
        FSBPktInfo.u1AddressMode = FSB_FPMA_ADDRESSING_MODE;
    }
    else if (FSBPktInfo.u2Flag & FSB_SPMA_BIT_MASK)
    {
        FSBPktInfo.u1AddressMode = FSB_SPMA_ADDRESSING_MODE;
    }

    /* Get the SysUpTime to compute the duration of the session */
    u4CurrentTime = OsixGetSysUpTime ();

    /* Convert the ElapsedTime in Seconds - to hours, minutes and seconds, to
     * to be displayed in syslog */
    u4CurrentSec = (UINT4) (u4CurrentTime % SECS_IN_MINUTE);
    u4CurrentMin = (UINT4) ((u4CurrentTime / SECS_IN_MINUTE) % MINUTES_IN_HOUR);
    u4CurrentHrs = (UINT4) ((u4CurrentTime / SECS_IN_HOUR) % HOURS_IN_DAY);

    /* Check the opcode of the packet */
    /* Discovery packets will be of type 0x0001 */
    if (FSBPktInfo.u2Opcode == FSB_DISCOVERY_ADV_OPCODE)    /* FIP Protocol */
    {
        /* Discovery Advertisement - Multicast/unicast */
        if (FSBPktInfo.u2SubOpcode == FSB_DISCOVERY_ADV_SUB_OPCODE)    /* FIP Subcode */
        {
            /* Priority is obtained after moving 3 bytes from Flag */
            pu1ReadPtr += FSB_THREE_BYTE;
            FSB_GET_1BYTE (FSBPktInfo.FsbFipsPktFields.
                           FcfDiscoveryAdvertisementInfo.u1Priority,
                           pu1ReadPtr);

            /* Name Identifier is obtained after moving 12 bytes from Priority */
            pu1ReadPtr += FSB_NAME_ID_OFFSET_FROM_PRIO;
            FSB_MEMCPY ((&FSBPktInfo.FsbFipsPktFields.
                         FcfDiscoveryAdvertisementInfo.au1NameId), pu1ReadPtr,
                        FSB_NAME_ID_LEN);
            pu1ReadPtr += FSB_NAME_ID_LEN;

            /* FCMAP is obtained after moving 5 bytes from  Name Identifier */
            pu1ReadPtr += FSB_FIVE_BYTE;
            FSB_MEMCPY ((&FSBPktInfo.FsbFipsPktFields.
                         FcfDiscoveryAdvertisementInfo.au1FcMap), pu1ReadPtr,
                        FSB_FCMAP_LEN);

            /* Fabric name is obtained after moving 3 bytes from FCMAP */
            pu1ReadPtr += FSB_FCMAP_LEN;
            FSB_MEMCPY ((&FSBPktInfo.FsbFipsPktFields.
                         FcfDiscoveryAdvertisementInfo.au1FabricName),
                        pu1ReadPtr, FSB_FABRIC_NAME_LEN);
            pu1ReadPtr += FSB_FABRIC_NAME_LEN;

            pu1ReadPtr += FSB_TWO_BYTE;
            FSB_GET_2BYTE (FSBPktInfo.FsbFipsPktFields.
                           FcfDiscoveryAdvertisementInfo.u2DbitFlag,
                           pu1ReadPtr);

            /* Offset points to Advertisement interval now */
            FSB_GET_4BYTE (FSBPktInfo.FsbFipsPktFields.
                           FcfDiscoveryAdvertisementInfo.u4FkaAdvPeriod,
                           pu1ReadPtr);

            if ((FSB_MEMCMP (&FSBPktInfo.DestAddr, ALL_ENODE_MAC,
                             MAC_ADDR_LEN)) == 0)
            {
                FSBPktInfo.PacketType = FSB_FIP_ADV_MCAST;
                FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId, FSB_DEBUGGING_LEVEL,
                                 FSB_CONTROL_PATH_TRC,
                                 "[TS(HH:MM:SS):%02d:%02d:%02d] Packet Type: %s received in Context %d in port %d of "
                                 "VLAN %d  \r\n", u4CurrentHrs, u4CurrentMin,
                                 u4CurrentSec,
                                 FsbErrString[FSBPktInfo.PacketType],
                                 FSBPktInfo.u4ContextId, FSBPktInfo.u4IfIndex,
                                 FSBPktInfo.u2VlanId);
            }
            else
            {
                FSBPktInfo.PacketType = FSB_FIP_ADV_UCAST;
                FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId, FSB_DEBUGGING_LEVEL,
                                 FSB_CONTROL_PATH_TRC,
                                 "Packet Type: %s received in Context %d in port %d of "
                                 "VLAN %d  \r\n",
                                 FsbErrString[FSBPktInfo.PacketType],
                                 FSBPktInfo.u4ContextId, FSBPktInfo.u4IfIndex,
                                 FSBPktInfo.u2VlanId);
            }

        }
        /* Discovery Solicitation - Multicast/unicast */
        else if (FSBPktInfo.u2SubOpcode == FSB_SOLICITATION_SUB_OPCODE)    /* FIP Subcode */
        {
            pu1ReadPtr += FSB_DIS_SOL_MTU_OFFSET_FROM_FLAG;

            FSB_GET_2BYTE (FSBPktInfo.FsbFipsPktFields.
                           FCFSolicitationUnicastInfo.u2MaxFrameSize,
                           pu1ReadPtr);

            if ((FSB_MEMCMP (&FSBPktInfo.DestAddr, ALL_FCF_MAC,
                             MAC_ADDR_LEN)) != 0)
            {
                FSBPktInfo.PacketType = FSB_FIP_SOLICIT_UCAST;
                FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId, FSB_DEBUGGING_LEVEL,
                                 FSB_CONTROL_PATH_TRC,
                                 "Packet Type: %s received in Context %d in port %d of "
                                 "VLAN %d  \r\n",
                                 FsbErrString[FSBPktInfo.PacketType],
                                 FSBPktInfo.u4ContextId, FSBPktInfo.u4IfIndex,
                                 FSBPktInfo.u2VlanId);
            }
            else
            {
                FSBPktInfo.PacketType = FSB_FIP_SOLICIT_MCAST;
                FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId, FSB_DEBUGGING_LEVEL,
                                 FSB_CONTROL_PATH_TRC,
                                 "Packet Type: %s received in Context %d in port %d of "
                                 "VLAN %d  \r\n",
                                 FsbErrString[FSBPktInfo.PacketType],
                                 FSBPktInfo.u4ContextId, FSBPktInfo.u4IfIndex,
                                 FSBPktInfo.u2VlanId);
            }

        }
    }
    /* FLOGI/FDISC packets will be of opcode 0x0002 */
    else if (FSBPktInfo.u2Opcode == FSB_LOGI_OPCODE)    /* FIP Protocol - flogi opcode */
    {
        /*Assigning the descriptor offset */
        FSB_GET_1BYTE (u1Descriptor, pu1ReadPtr);

        if (FSBPktInfo.u2SubOpcode == FSB_FLOGI_REQ_SUB_OPCODE)    /* FIP Subcode */
        {
            /*Differentiating the request packets based on Descriptor */
            if (u1Descriptor == FSB_FIP_LOGI_DESCRIPTOR)
            {
                FSBPktInfo.PacketType = FSB_FIP_FLOGI_REQUEST;
                FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId, FSB_DEBUGGING_LEVEL,
                                 FSB_CONTROL_PATH_TRC,
                                 "Packet Type: %s received in Context %d in port %d of "
                                 "VLAN %d  \r\n",
                                 FsbErrString[FSBPktInfo.PacketType],
                                 FSBPktInfo.u4ContextId, FSBPktInfo.u4IfIndex,
                                 FSBPktInfo.u2VlanId);
            }
            else if (u1Descriptor == FSB_NPIV_FDISC_DESCRIPTOR)
            {
                FSBPktInfo.PacketType = FSB_FIP_NPIV_FDISC_REQUEST;
                FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId, FSB_DEBUGGING_LEVEL,
                                 FSB_CONTROL_PATH_TRC,
                                 "Packet Type: %s received in Context %d in port %d of "
                                 "VLAN %d  \r\n",
                                 FsbErrString[FSBPktInfo.PacketType],
                                 FSBPktInfo.u4ContextId, FSBPktInfo.u4IfIndex,
                                 FSBPktInfo.u2VlanId);
            }
            else if (u1Descriptor == FSB_FIP_LOGO_DESCRIPTOR)
            {
                FSBPktInfo.PacketType = FSB_FIP_LOGO_REQUEST;
                FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId, FSB_DEBUGGING_LEVEL,
                                 FSB_CONTROL_PATH_TRC,
                                 "Packet Type: %s received in Context %d in port %d of "
                                 "VLAN %d  \r\n",
                                 FsbErrString[FSBPktInfo.PacketType],
                                 FSBPktInfo.u4ContextId, FSBPktInfo.u4IfIndex,
                                 FSBPktInfo.u2VlanId);
            }
        }
        /*Differentiating the accept packets based on Descriptor */
        else if (FSBPktInfo.u2SubOpcode == FSB_FLOGI_ACCEPT_SUB_OPCODE)    /* FIP Subcode */
        {
            if (u1Descriptor == FSB_FIP_LOGI_DESCRIPTOR)
            {
                /* To differentiate FSB_FIP_FLOGI_ACCEPT & FSB_FIP_FLOGI_REJECT */
                pu1ReadPtr += FLOGI_CMD_CODE_OFFSET_FROM_FLAG;
                FSB_GET_1BYTE (u1CmdCode, pu1ReadPtr);

                if (u1CmdCode == FSB_FLOGI_ACC_CMD_CODE)
                {
                    pu1ReadPtr += FCOE_MAC_ADDR_OFFSET_FROM_DESC;
                    FSB_MEMCPY (FSBPktInfo.FsbFipsPktFields.FIPLogiInfo.
                                FCoEMacAddr, pu1ReadPtr, MAC_ADDR_LEN);
                    FSBPktInfo.PacketType = FSB_FIP_FLOGI_ACCEPT;
                    FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                                     FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                                     "Packet Type: %s received in Context %d in port %d of "
                                     "VLAN %d  \r\n",
                                     FsbErrString[FSBPktInfo.PacketType],
                                     FSBPktInfo.u4ContextId,
                                     FSBPktInfo.u4IfIndex, FSBPktInfo.u2VlanId);
                }
                else if (u1CmdCode == FSB_FLOGI_RJT_CMD_CODE)
                {
                    FSBPktInfo.PacketType = FSB_FIP_FLOGI_REJECT;
                    FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                                     FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                                     "Packet Type: %s received in Context %d in port %d of "
                                     "VLAN %d  \r\n",
                                     FsbErrString[FSBPktInfo.PacketType],
                                     FSBPktInfo.u4ContextId,
                                     FSBPktInfo.u4IfIndex, FSBPktInfo.u2VlanId);
                }
            }
            else if (u1Descriptor == FSB_NPIV_FDISC_DESCRIPTOR)
            {
                /* To differentiate FSB_FIP_NPIV_FDISC_ACCEPT & FSB_FIP_NPIV_FDISC_REJECT */
                pu1ReadPtr += FLOGI_CMD_CODE_OFFSET_FROM_FLAG;
                FSB_GET_1BYTE (u1CmdCode, pu1ReadPtr);

                if (u1CmdCode == FSB_FLOGI_ACC_CMD_CODE)
                {
                    pu1ReadPtr += FCOE_MAC_ADDR_OFFSET_FROM_DESC;
                    FSB_MEMCPY (FSBPktInfo.FsbFipsPktFields.FIPLogiInfo.
                                FCoEMacAddr, pu1ReadPtr, MAC_ADDR_LEN);
                    FSBPktInfo.PacketType = FSB_FIP_NPIV_FDISC_ACCEPT;
                    FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                                     FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                                     "Packet Type: %s received in Context %d in port %d of "
                                     "VLAN %d  \r\n",
                                     FsbErrString[FSBPktInfo.PacketType],
                                     FSBPktInfo.u4ContextId,
                                     FSBPktInfo.u4IfIndex, FSBPktInfo.u2VlanId);
                }
                else if (u1CmdCode == FSB_FLOGI_RJT_CMD_CODE)
                {
                    FSBPktInfo.PacketType = FSB_FIP_NPIV_FDISC_REJECT;
                    FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                                     FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                                     "Packet Type: %s received in Context %d in port %d of "
                                     "VLAN %d  \r\n",
                                     FsbErrString[FSBPktInfo.PacketType],
                                     FSBPktInfo.u4ContextId,
                                     FSBPktInfo.u4IfIndex, FSBPktInfo.u2VlanId);
                }
            }
            else if (u1Descriptor == FSB_FIP_LOGO_DESCRIPTOR)
            {
                pu1ReadPtr += FSB_FLOGO_MAC_DESC_OFFSET_FROM_FLAG;
                FSB_GET_1BYTE (u1Type, pu1ReadPtr);
                pu1ReadPtr += FSB_ONE_BYTE;

                if (u1Type == FSB_FIP_MAC_DESCRIPTOR)
                {
                    FSBPktInfo.PacketType = FSB_FIP_LOGO_ACCEPT;
                    FSB_MEMCPY (FSBPktInfo.FsbFipsPktFields.FIPLogoInfo.
                                FCoEMacAddr, pu1ReadPtr, MAC_ADDR_LEN);
                    FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                                     FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                                     "Packet Type: %s received in Context %d in port %d of "
                                     "VLAN %d  \r\n",
                                     FsbErrString[FSBPktInfo.PacketType],
                                     FSBPktInfo.u4ContextId,
                                     FSBPktInfo.u4IfIndex, FSBPktInfo.u2VlanId);
                }
                else
                {
                    FSBPktInfo.PacketType = FSB_FIP_LOGO_REJECT;
                    FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId,
                                     FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                                     "Packet Type: %s received in Context %d in port %d of "
                                     "VLAN %d  \r\n",
                                     FsbErrString[FSBPktInfo.PacketType],
                                     FSBPktInfo.u4ContextId,
                                     FSBPktInfo.u4IfIndex, FSBPktInfo.u2VlanId);
                }
            }
        }
    }
    else if (FSBPktInfo.u2Opcode == FSB_KEEP_ALIVE_OPCODE)    /* FIP Protocol - keep alive opcode */
    {
        if (FSBPktInfo.u2SubOpcode == FSB_FIP_KEEP_ALIVE_SUB_OPCODE)    /* FIP Subcode */
        {
            FSB_GET_1BYTE (u1Type, pu1ReadPtr);
            pu1ReadPtr += FSB_ONE_BYTE;
            FSB_MEMCPY (FSBPktInfo.FsbFipsPktFields.FIPVNKeepAlive.ENodeMacAddr,
                        pu1ReadPtr, MAC_ADDR_LEN);
            /* VN_Port type is obtained after moving 7 bytes from Enode Type */
            pu1ReadPtr += FSB_SIX_BYTE;
            FSB_GET_1BYTE (u1VNType, pu1ReadPtr);

            if (u1VNType == FSB_VN_MAC_TYPE)
            {
                /* VN_Port Mac Address is obtained by moving 1 byte from ENode
                 * Type field */
                pu1ReadPtr += FSB_ONE_BYTE;
                FSB_MEMCPY (FSBPktInfo.FsbFipsPktFields.FIPVNKeepAlive.
                            VNMacAddr, pu1ReadPtr, MAC_ADDR_LEN);
                FSBPktInfo.PacketType = FSB_FIP_VNPORT_KEEP_ALIVE;
                FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId, FSB_DEBUGGING_LEVEL,
                                 FSB_CONTROL_PATH_TRC,
                                 "[TS(HH:MM:SS):%02d:%02d:%02d] Packet Type: %s received in Context %d in port %d of "
                                 "VLAN %d  \r\n", u4CurrentHrs, u4CurrentMin,
                                 u4CurrentSec,
                                 FsbErrString[FSBPktInfo.PacketType],
                                 FSBPktInfo.u4ContextId, FSBPktInfo.u4IfIndex,
                                 FSBPktInfo.u2VlanId);
            }
            else if (u1Type == FSB_ENODE_MAC_TYPE)
            {
                FSBPktInfo.PacketType = FSB_FIP_ENODE_KEEP_ALIVE;
                FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId, FSB_DEBUGGING_LEVEL,
                                 FSB_CONTROL_PATH_TRC,
                                 "[TS(HH:MM:SS):%02d:%02d:%02d] Packet Type: %s received in Context %d in port %d of "
                                 "VLAN %d  \r\n", u4CurrentHrs, u4CurrentMin,
                                 u4CurrentSec,
                                 FsbErrString[FSBPktInfo.PacketType],
                                 FSBPktInfo.u4ContextId, FSBPktInfo.u4IfIndex,
                                 FSBPktInfo.u2VlanId);
            }
        }
        else if (FSBPktInfo.u2SubOpcode == FSB_CLEAR_VIRTUAL_LINK_SUB_OPCODE)    /* FIP Subcode */
        {
            /*--------------------------------------------------------------------------------
             * 1 Byte  | 1 Byte |        6 Byte        |  2Byte  |   3 Byte   |   8 Byte     |
             *--------------------------------------------------------------------------------
             *Type = 11| Len = 5| VN_Port MAC Address#1| Reserved| N_Port_ID#1| N_Port_Name#1|
             *---------|--------|----------------------|---------|------------|---------------
             *Type = 11| Len = 5| VN_Port MAC Address#2| Reserved| N_Port_ID#2| N_Port_Name#2|
             *---------|--------|----------------------|---------|------------|---------------
             *    .    |   .    |        .             |    .    |     .      |      .       |          
             *    .    |   .    |        .             |    .    |     .      |      .       |
             *Type = 11| Len = 5| VN_Port MAC Address#N| Reserved| N_Port_ID#N| N_Port_Name#N|
             *-------------------------------------------------------------------------------*/
            FSBPktInfo.PacketType = FSB_FIP_CLEAR_LINK;
            FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId, FSB_DEBUGGING_LEVEL,
                             FSB_CONTROL_PATH_TRC,
                             "Packet Type: %s received in Context %d in port %d of "
                             "VLAN %d  \r\n",
                             FsbErrString[FSBPktInfo.PacketType],
                             FSBPktInfo.u4ContextId, FSBPktInfo.u4IfIndex,
                             FSBPktInfo.u2VlanId);
            pu1ReadPtr += FSB_VN_MAC_OFFSET_FROM_FLAG;

            FSB_GET_1BYTE (u1Type, pu1ReadPtr);
            u2NoOfVNMac = (UINT2) ((u2DescLen / 5) - 1);

            /* u2VNMacCount = 0, means that ENode CVL packet is received from FCF.
             * i.e, No VN_Port MAC in the Descriptor field. Hence no need to allocate
             * memory for pVNMacAddr in FSBPktInfo */
            if (u2NoOfVNMac != 0)
            {
                FSBPktInfo.FsbFipsPktFields.FIPClearLink.pVNMacAddr =
                    (tMacAddr *) MemAllocMemBlk (FSB_FCOE_MAC_ADDR_MEMPOOL_ID);

                if (FSBPktInfo.FsbFipsPktFields.FIPClearLink.pVNMacAddr == NULL)
                {
                    FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId, FSB_ERROR_LEVEL,
                                     FSB_NONE,
                                     "FsbDepacketizer: Memalloc failed for FSB_FCOE_MAC_ADDR_MEMPOOL_ID\r\n");
                    return FSB_FAILURE;
                }

                FSB_MEMSET (FSBPktInfo.FsbFipsPktFields.FIPClearLink.pVNMacAddr,
                            0, MAX_FSB_FCOE_MAC_SIZE);

                for (u2VNMacCount = 0; u2VNMacCount < u2NoOfVNMac;
                     u2VNMacCount++)
                {
                    if (u2VNMacCount > MAX_FSB_FCOE_MAC_ADDR)
                    {
                        break;
                    }
                    if (u1Type == FSB_VN_MAC_TYPE)
                    {
                        pu1ReadPtr += FSB_ONE_BYTE;

                        FSB_MEMCPY (FSBPktInfo.FsbFipsPktFields.FIPClearLink.
                                    pVNMacAddr[u2VNMacCount], pu1ReadPtr,
                                    MAC_ADDR_LEN);
                    }
                    pu1ReadPtr += FSB_NEXT_VN_MAC_ADDR_OFFSET;
                    FSB_GET_1BYTE (u1Type, pu1ReadPtr);
                }
            }
        }
    }
    else if (FSBPktInfo.u2Opcode == FSB_FIP_VLAN_OPCODE)    /* FIP Protocol - VLAN opcode */
    {
        if (FSBPktInfo.u2SubOpcode == FSB_FIP_VLAN_REQ_SUB_OPCODE)    /* FIP Subcode */
        {
            FSBPktInfo.PacketType = FSB_FIP_VLAN_DISCOVERY;
            FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId, FSB_DEBUGGING_LEVEL,
                             FSB_CONTROL_PATH_TRC,
                             "Packet Type: %s received in Context %d in port %d of "
                             "VLAN %d  \r\n",
                             FsbErrString[FSBPktInfo.PacketType],
                             FSBPktInfo.u4ContextId, FSBPktInfo.u4IfIndex,
                             FSBPktInfo.u2VlanId);
        }
        else if (FSBPktInfo.u2SubOpcode == FSB_FIP_VLAN_NOTIFICATION_SUB_OPCODE)    /* FIP Subcode */
        {
            FSBPktInfo.PacketType = FSB_FIP_VLAN_NOTIFICATION;
            FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId, FSB_DEBUGGING_LEVEL,
                             FSB_CONTROL_PATH_TRC,
                             "Packet Type: %s received in Context %d in port %d of "
                             "VLAN %d  \r\n",
                             FsbErrString[FSBPktInfo.PacketType],
                             FSBPktInfo.u4ContextId, FSBPktInfo.u4IfIndex,
                             FSBPktInfo.u2VlanId);
        }
    }

    /* Validate FIP packet received before further processing */
    if (FsbValidateFIPFrame (&FSBPktInfo) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbDepacketizer : FsbValidateFIPFrame return FAILURE\r\n");
        return FSB_FAILURE;
    }

    /* Dispatch the filled packet info structure to the Dispatcher function */

    if (FsbDispatcher (&FSBPktInfo) == FSB_SUCCESS)
    {
        /* Since FIP Keep Alive and Discovery Advertisement are
         * switched in the Data Path, TX of these packets is not
         * required */
        if ((FSBPktInfo.PacketType == FSB_FIP_ENODE_KEEP_ALIVE) ||
            (FSBPktInfo.PacketType == FSB_FIP_VNPORT_KEEP_ALIVE))
        {
            return FSB_SUCCESS;
        }
        if (FsbPacketTxFrame (pFsbNotifyParams, &FSBPktInfo) == FSB_FAILURE)
        {
            FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                             "\nFsbDepacketizer : FsbPacketTxFrame return FAILURE \r\n");
            return FSB_FAILURE;
        }
    }
    else
    {
        /*
           FIP Snooping Bridge (FSB) module is designed to switch FIP ENode/VN-Port Keep Alive and CVL, 
           even when there is no FIP Session present in it. This for the following reasons :-

           1) Server have FIP stale entry – in FCF Facing port port-shut/unplug and no-shut/plug-in scenario:-

           In the scenario of the FCF facing port being port-shut/unplug and no-shut/plug-in scenario, 
           FIP Session entry in the FSB module is removed. Also, it causes the FIP Session entry in TOR/FCF to be removed. 
           But the FIP Session entry in the Server shall not removed. This causes a stale entry in the Server.  
           Server shall Keep sending ENode and VN-Port Keep Alive, for which FIP Session is lost in the TOR side.  
           This should be made to removed and Server should re-initiate a new FIP Session. 

           2) Server's stale FIP Session entry is removed by the following :-

           a)  Allowing FSB to switch ENode/VN-Port Keep Alive to the TOR, although FIP Session entry is not present.
           b)  TOR, once receiving a ENode/VN-Port Keep Alive for it having no FIP Session entry, shall initiate a CVL to the server.
           c)  Again, once CVL is received in the FSB, it is allowed to switch, even if there is no FIP Session entry.
           d)  The above shall help in CVL been switched in the ENode facing port and causing the Server to remove the FIPS session stale entry.
         */
        if ((FSBPktInfo.PacketType == FSB_FIP_ENODE_KEEP_ALIVE) ||
            (FSBPktInfo.PacketType == FSB_FIP_VNPORT_KEEP_ALIVE))
        {
            if (FsbPacketTxFrame (pFsbNotifyParams, &FSBPktInfo) == FSB_FAILURE)
            {
                FSB_CONTEXT_TRC (FSBPktInfo.u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "\nFsbDepacketizer : FsbPacketTxFrame return FAILURE \r\n");
                return FSB_FAILURE;
            }
        }
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDispatcher                                    */
/*                                                                           */
/*    Description         : This function dispatches pFSBPktInfo to various  */
/*                          Table                                            */
/*                                                                           */
/*    Input(s)            : pFSBPktInfo - Pointer to tFSBPktInfo             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbDispatcher (tFSBPktInfo * pFSBPktInfo)
{

    if (pFSBPktInfo == NULL)
    {
        return FSB_FAILURE;
    }
    if (pFSBPktInfo->PacketType == FSB_FIP_ADV_MCAST)
    {
        /* Handling of FSB_FIP_ADV_MCAST frame */
        /* Call to add new entry in fcf table, as discovery 
         * advertisement multicast packet is recieved*/
        if (FsbHandleFcfDiscovery (pFSBPktInfo) == FSB_FAILURE)
        {
            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL,
                             FSB_NONE,
                             "FsbDispatcher: FsbHandleFcfDiscovery with VlanId: %d "
                             "return failure\r\n", pFSBPktInfo->u2VlanId);
            return FSB_FAILURE;
        }

        /* Update the statistics for FSB_FIP_ADV_MCAST */
        FsbUpdateVLANStatistics (pFSBPktInfo->u4ContextId,
                                 pFSBPktInfo->u2VlanId, FSB_FIP_ADV_MCAST);
    }
    else if ((pFSBPktInfo->PacketType == FSB_FIP_VLAN_DISCOVERY) ||
             (pFSBPktInfo->PacketType == FSB_FIP_VLAN_NOTIFICATION))
    {
        /* Handling of FSB_FIP_VLAN_DISCOVERY & 
         * FSB_FIP_VLAN_NOTIFICATION frame*/
        if (FsbHandleFIPVlanPackets (pFSBPktInfo) == FSB_FAILURE)
        {
            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL,
                             FSB_NONE,
                             "FsbDispatcher: FsbHandleFIPVlanPackets with VlanId: %d "
                             "return failure\r\n", pFSBPktInfo->u2VlanId);
            return FSB_FAILURE;
        }
    }
    else
    {
        /* Packet information is dispatched to handle session information */
        if (FsbHandleFIPSession (pFSBPktInfo) == FSB_FAILURE)
        {
            if (pFSBPktInfo->PacketType == FSB_FIP_CLEAR_LINK)
            {
                /* Update the statistics for the CVL recieved */
                FsbUpdateVLANStatistics (pFSBPktInfo->u4ContextId,
                                         pFSBPktInfo->u2VlanId,
                                         pFSBPktInfo->PacketType);
                if (pFSBPktInfo->FsbFipsPktFields.FIPClearLink.pVNMacAddr !=
                    NULL)
                {
                    MemReleaseMemBlock (FSB_FCOE_MAC_ADDR_MEMPOOL_ID,
                                        (UINT1 *) pFSBPktInfo->FsbFipsPktFields.
                                        FIPClearLink.pVNMacAddr);
                }
                return FSB_SUCCESS;    /* Success is returned, because Tx function will be called, only if the FsbDispatcher function returns success. */
            }
            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL,
                             FSB_NONE,
                             "FsbDispatcher: FsbHandleFIPSession with VlanId: %d "
                             "return failure\r\n", pFSBPktInfo->u2VlanId);
            return FSB_FAILURE;
        }
        if ((pFSBPktInfo->PacketType == FSB_FIP_CLEAR_LINK) &&
            (pFSBPktInfo->FsbFipsPktFields.FIPClearLink.pVNMacAddr != NULL))
        {
            MemReleaseMemBlock (FSB_FCOE_MAC_ADDR_MEMPOOL_ID,
                                (UINT1 *) pFSBPktInfo->FsbFipsPktFields.
                                FIPClearLink.pVNMacAddr);
        }
        /* Update the statistics for the corresponding FIP packet recieved */
        FsbUpdateVLANStatistics (pFSBPktInfo->u4ContextId,
                                 pFSBPktInfo->u2VlanId,
                                 pFSBPktInfo->PacketType);
    }
    return FSB_SUCCESS;
}

#endif /* _FSBMAIN_C_ */
