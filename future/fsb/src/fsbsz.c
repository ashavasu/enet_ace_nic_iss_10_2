/*************************************************************************
* Copyright (C) 2007-2015 Aricent Group . All Rights Reserved
*
* $Id: fsbsz.c,v 1.3 2017/10/09 13:13:59 siva Exp $
*
* Description:  This file contains FSB mempool INIT routines.
*
*************************************************************************/
#define _FSBSZ_C
#include "fsbinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
FsbSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < FSB_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (FsFSBSizingParams[i4SizingId].u4StructSize,
                                     FsFSBSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(FSBMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            FsbSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
FsbSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsFSBSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, FSBMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
FsbSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < FSB_MAX_SIZING_ID; i4SizingId++)
    {
        if (FSBMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (FSBMemPoolIds[i4SizingId]);
            FSBMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
