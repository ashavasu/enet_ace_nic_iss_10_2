/*************************************************************************
 * Copyright (C) 2007-2015 Aricent Group . All Rights Reserved
 *
 * $Id: fsbnpwr.c,v 1.5 2017/10/09 13:13:59 siva Exp $
 *
 * Description: This file contains the FIP-snooping NPAPI related wrapper
 *              routines
 *
 ***************************************************************************/

#ifndef _FSBNPWR_C_
#define  _FSBNPWR_C_
#include "fsbinc.h"
#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : FsbNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tFsbNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
FsbNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tFsbNpModInfo      *pFsbNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pFsbNpModInfo = &(pFsHwNp->FsbNpModInfo);

    if (NULL == pFsbNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_MI_NP_FSB_HW_INIT:
        {
            tFsbNpWrFsMiNpFsbHwInit *pEntry = NULL;
            pEntry = &pFsbNpModInfo->FsbNpFsMiNpFsbHwInit;
            u1RetVal =
                (UINT1) (FsMiNpFsbHwInit (pEntry->pFsbHwFilterEntry,
                                          pEntry->pFsbLocRemFilterId));
            break;
        }
        case FS_FSB_HW_CREATE_FILTER:
        {
            tFsbNpWrFsFsbHwCreateFilter *pEntry = NULL;
            pEntry = &pFsbNpModInfo->FsbNpFsFsbHwCreateFilter;
            u1RetVal =
                (UINT1) (FsFsbHwCreateFilter (pEntry->pFsbHwFilterEntry,
                                              pEntry->pu4FilterId));
            break;
        }
        case FS_FSB_HW_DELETE_FILTER:
        {
            tFsbNpWrFsFsbHwDeleteFilter *pEntry = NULL;
            pEntry = &pFsbNpModInfo->FsbNpFsFsbHwDeleteFilter;
            u1RetVal =
                (UINT1) (FsFsbHwDeleteFilter (pEntry->pFsbHwFilterEntry,
                                              pEntry->u4FilterId));
            break;
        }
        case FS_MI_NP_FSB_HW_DE_INIT:
        {
            tFsbNpWrFsMiNpFsbHwDeInit *pEntry = NULL;
            pEntry = &pFsbNpModInfo->FsbNpFsMiNpFsbHwDeInit;
            u1RetVal =
                (UINT1) (FsMiNpFsbHwDeInit (pEntry->pFsbHwFilterEntry,
                                            pEntry->FsbLocRemFilterId));
            break;
        }
        case FS_FSB_HW_CREATE_S_CHANNEL_FILTER:
        {
            tFsbNpWrFsFsbHwCreateSChannelFilter *pEntry = NULL;
            pEntry = &pFsbNpModInfo->FsbNpFsFsbHwCreateSChannelFilter;
            u1RetVal =
                (UINT1) (FsFsbHwCreateSChannelFilter
                         (pEntry->pFsbHwSChannelFilterEntry,
                          pEntry->pu4FilterId));
            break;
        }
        case FS_FSB_HW_DELETE_S_CHANNEL_FILTER:
        {
            tFsbNpWrFsFsbHwDeleteSChannelFilter *pEntry = NULL;
            pEntry = &pFsbNpModInfo->FsbNpFsFsbHwDeleteSChannelFilter;
            u1RetVal =
                (UINT1) (FsFsbHwDeleteSChannelFilter
                         (pEntry->pFsbHwSChannelFilterEntry,
                          pEntry->u4FilterId));
            break;
        }
        case FS_FSB_HW_SET_FCOE_PARAMS:
        {
            tFsbNpWrFsFsbHwSetFCoEParams *pEntry = NULL;
            pEntry = &pFsbNpModInfo->FsbNpFsFsbHwSetFCoEParams;
            u1RetVal = (UINT1) (FsFsbHwSetFCoEParams (pEntry->pFsbHwVlanEntry));
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_NP_FSB_MBSM_HW_INIT:
        {
            tFsbNpWrFsMiNpFsbMbsmHwInit *pEntry = NULL;
            pEntry = &pFsbNpModInfo->FsbNpFsMiNpFsbMbsmHwInit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                (UINT1) (FsMiNpFsbMbsmHwInit (pEntry->pFsbHwFilterEntry,
                                              pEntry->pFsbLocRemFilterId));
            break;
        }
        case FS_FSB_MBSM_HW_CREATE_FILTER:
        {
            tFsbNpWrFsFsbMbsmHwCreateFilter *pEntry = NULL;
            pEntry = &pFsbNpModInfo->FsbNpFsFsbMbsmHwCreateFilter;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                (UINT1) (FsFsbMbsmHwCreateFilter (pEntry->pFsbHwFilterEntry,
                                                  pEntry->pu4FilterId));
            break;
        }
#endif
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif /* _FSBNPWR_C_ */
