/*************************************************************************
* Copyright (C) 2007-2015 Aricent Group . All Rights Reserved
*
* $Id: fsbapi.c,v 1.14 2017/10/09 13:13:58 siva Exp $
*
* Description: This file contains the FIP-snooping API related functions
*
*************************************************************************/
#ifndef _FSBAPI_C_
#define _FSBAPI_C_

#include "fsbinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbLock                                          */
/*                                                                           */
/*    Description         : Function to take the mutual exclusion protocol   */
/*                          semaphore                                        */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : SNMP_SUCCESS / SNMP_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
FsbLock (VOID)
{
    if (OsixSemTake (FSB_SEM_ID) == OSIX_FAILURE)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_OS_RESOURCE_TRC,
                         "FsbLock : OsixSemTake failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbUnLock                                        */
/*                                                                           */
/*    Description         : Function to release the mutual exclusion protocol*/
/*                          semaphore                                        */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : SNMP_SUCCESS / SNMP_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
FsbUnLock (VOID)
{
    if (OsixSemGive (FSB_SEM_ID) == OSIX_FAILURE)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_OS_RESOURCE_TRC,
                         "FsbUnLock : OsixSemGive failed\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbSendPacketToFsbQueue                          */
/*                                                                           */
/*    Description         : Function to Enqueue the incoming packet          */
/*                                                                           */
/*    Input(s)            : pBuf - Pointer to the incoming packet            */
/*                          u4PacketLen - Packet length                      */
/*                          u4IfIndex - Interface Index                      */
/*                          VlanId - VLAN Index                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS / OSIX_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
FsbSendPacketToFsbQueue (tCRU_BUF_CHAIN_HEADER * pBuf,
                         UINT4 u4PacketLen, UINT4 u4IfIndex, tVlanId VlanId)
{
    tFsbNotifyParams   *pMsg = NULL;
    UINT4               u4ContextId = 0;

    /* When ISSU maintenance mode is in progress, request for new FCoE sessions
     * should not be supported. Hence skipping the parses the incoming FSB packet*/
    if (IssuGetMaintModeOperation () == OSIX_TRUE)
    {
        /* Release the CRU buf chain for Ethernet frame */
        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) != CRU_SUCCESS)

        {
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbSendPacketToFsbQueue :Release Buffer returned Failure\n");
        }
        return OSIX_SUCCESS;
    }

    if (pBuf == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbSendPacketToFsbQueue : pBuf is NULL\r\n");
        return OSIX_FAILURE;
    }

    if (u4PacketLen > FSB_MAX_FIP_PKT_LEN)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbSendPacketToFsbQueue : Packet Size "
                         "exceeds FSB_MAX_FIP_PKT_LEN\r\n");
        return OSIX_FAILURE;
    }

    /* Retrieve the context id from the Interface Index */
    if (FsbGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                         "FsbSendPacketToFsbQueue: IfIndex: %d is not "
                         "associated with any ContextId\r\n", u4IfIndex);
        return OSIX_FAILURE;
    }

    if (FsbUtilIsFsbStarted (u4ContextId) != FSB_TRUE)
    {
        /* If FSB is not started in this context,
         * return success*/
        return OSIX_FAILURE;
    }

    if ((pMsg = (tFsbNotifyParams *) MemAllocMemBlk
         (FSB_NOTIFY_PARAMS_INFO_MEMPOOL_ID)) == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                         "FsbSendPacketToFsbQueue : MemAllocMemBlk"
                         " for pMsg failed\r\n");
        return (OSIX_FAILURE);
    }

    FSB_MEMSET (pMsg, 0, sizeof (tFsbNotifyParams));

    CRU_BUF_Copy_FromBufChain (pBuf, pMsg->au1DataBuf, 0, u4PacketLen);
    pMsg->u4MsgType = FSB_PACKET_RECV_MSG;
    pMsg->u4PacketLen = u4PacketLen;
    pMsg->u4IfIndex = u4IfIndex;
    pMsg->u2VlanId = VlanId;

    FsbEnQFrameToFsbTask (pMsg);

    /* Release the CRU buf chain for Ethernet frame */
    if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) != CRU_SUCCESS)

    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbSendPacketToFsbQueue :Release Buffer returned Failure\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDeleteVlanIndication                          */
/*                                                                           */
/*    Description         : This function is used to process VLAN deletion   */
/*                          indication from L2IWF                            */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId - VLAN Index                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbDeleteVlanIndication (UINT4 u4ContextId, tVlanId VlanId)
{
    tFsbNotifyParams   *pMsg = NULL;

    if (FsbUtilIsFsbStarted (u4ContextId) != FSB_TRUE)
    {
        /* If FSB is not started in this context,
         * return success*/
        return OSIX_SUCCESS;
    }
    if ((pMsg = (tFsbNotifyParams *) MemAllocMemBlk
         (gFsbGlobals.FsbNotifyParamsPoolId)) == NULL)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                         "FsbDeleteVlanIndication: MemAllocMemBlk"
                         " for pMsg failed\r\n");
        return (OSIX_FAILURE);
    }
    pMsg->u4ContextId = u4ContextId;
    pMsg->u2VlanId = (UINT2) VlanId;
    pMsg->u4MsgType = FSB_VLAN_DELETE_MSG;
    FsbEnQFrameToFsbTask (pMsg);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbApiNotifyPortDown                             */
/*                                                                           */
/*    Description         : This function is used to notify the operational  */
/*                          status the port                                  */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbApiNotifyPortDown (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1OperStatus);
    /*
       The handling of Interface – Oper down scenario for the FIP Session learnt.
       It is implemented to have FIP Session not to be deleted because of interface – Oper down indication been received.
       This is due to the following reason :-

       When the physical port in ISS is been plug-out and plug-in (or any link fluctuation scenario), 
       shall indicate a Oper down indication to the FSB module. However, at the other end (either TOR or Server)  
       shall not have made / declare the interface as down because of it timeout not been expired.  
       This shall cause the FIP Session entry to remain in TOR / Server inconsistent with ISS, if it has deleted 
       the FIP Session for interface Oper down. This causes FIP Session been lost between the Server and TOR.   
       Hence to avoid, it is implemented to have FIP Session not being disrupted in interface Oper down indication.
       In the real cause of interface Oper down scenario, FIP Session in the FSB shall be removed via House Keeping Timer in FSB.
     */
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbApiNotifyPortDiscarding*/
/*                                                                           */
/*    Description         : This function is used to notify the operational  */
/*                          status the port                                  */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbApiNotifyPortDiscarding (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    /*
       The handling of Spanning Tree (STP) – DISCARDING state received for port to the FSB Module.
       It is implemented to have FIP Session not to be deleted for the STP state moved to DISCARDING state. 

       This is decided based on the consideration of the scenario - Port with FORDWARDING state for any reason 
       (Protocol determined or fluctuation) moves to DISCARDING state and move back to FORWARDING State, 
       within the Keep Alive timer expirty (Enode side) or Advertisement Timer expiry (TOR side). In the case of 
       fast transition (Forwarding to Discarding to Forwarding), it shall remove the FIP Session entry in FSB.  
       But TOR and Server might still have the session entry. Keep Alive and Multicast Advertisement will also 
       switched even if the FIP session is not being present in the FSB module.  This causes TOR and Server to retain 
       the FLOGI session entry in it, although FSB impacts the FCoE traffic.

       To avoid this, FIP frames are not processed or transmitted when the port is in Blocking State.  
       Also removal of the FSB entry for DISCARDING notification is prevented. In the scenario of 
       Port state moving to DISCARDING state, eventually FIP Session if required to be removed, 
       shall be handed by the House Keeping Timer. 
     */
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbApiUnmapPort                                  */
/*                                                                           */
/*    Description         : This function is used to notify the operational  */
/*                          status the port                                  */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbApiUnmapPort (UINT4 u4IfIndex)
{
    tFsbNotifyParams   *pMsg = NULL;
    UINT4               u4ContextId = 0;

    /* Retrieve the context id from the Interface Index */
    if (FsbGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId) != FSB_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (FsbUtilIsFsbStarted (u4ContextId) != FSB_TRUE)
    {
        /* If FSB is not started in this context,
         * return success*/
        return OSIX_SUCCESS;
    }
    if ((pMsg = (tFsbNotifyParams *) MemAllocMemBlk
         (gFsbGlobals.FsbNotifyParamsPoolId)) == NULL)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                         "FsbApiUnmapPort: MemAllocMemBlk"
                         " for pMsg failed\r\n");
        return (OSIX_FAILURE);
    }
    pMsg->u4ContextId = u4ContextId;
    pMsg->u4IfIndex = u4IfIndex;
    pMsg->u4MsgType = FSB_PORT_UNMAP_MSG;
    FsbEnQFrameToFsbTask (pMsg);
    return OSIX_SUCCESS;
}

/******************************************************************************/
/* FUNCTION NAME    : FsbApiDeleteContext                                     */
/*                                                                            */
/* DESCRIPTION      : This function notifies the context status               */
/*                                                                            */
/* INPUT            : u4ContextId - Context Identifier                        */
/*                                                                            */
/* OUTPUT           : None                                                    */
/*                                                                            */
/* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                               */
/*                                                                            */
/******************************************************************************/
INT4
FsbApiDeleteContext (UINT4 u4ContextId)
{
    tFsbNotifyParams   *pMsg = NULL;

    if (FsbUtilIsFsbStarted (u4ContextId) != FSB_TRUE)
    {
        /* If FSB is not started in this context,
         * return success*/
        return OSIX_SUCCESS;
    }
    if ((pMsg = (tFsbNotifyParams *) MemAllocMemBlk
         (gFsbGlobals.FsbNotifyParamsPoolId)) == NULL)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                         "FsbApiDeleteContext : MemAllocMemBlk"
                         " for pMsg failed\r\n");
        return (OSIX_FAILURE);
    }
    pMsg->u4ContextId = u4ContextId;
    pMsg->u4MsgType = FSB_CONTEXT_DELETE_MSG;
    FsbEnQFrameToFsbTask (pMsg);
    return OSIX_SUCCESS;
}

/*****************************************************************************
*                                                                           *
*    Function Name       : FsbApiIsFsbEnabled                               *
*                                                                           *
*    Description         : This function is to check whether                *
*                          FSB module is enabled.                           *
*                                                                           *
*    Input(s)            : None                                             *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : FSB_SUCCESS/FSB_FAILURE                          *
*                                                                           *
*****************************************************************************/
UINT1
FsbApiIsFsbEnabled (VOID)
{
    UINT4               u4ContextId = 0;
    tFsbContextInfo    *pFsbContextInfo = NULL;

    FsbLock ();

    /* Check if FSB is started in any one of the context
     * If FSB is started in any of the context, break 
     * the loop and return success, else return failure*/
    for (u4ContextId = 0; u4ContextId < FSB_MAX_CONTEXT_COUNT; u4ContextId++)
    {
        pFsbContextInfo = FsbCxtGetContextEntry (u4ContextId);
        if (pFsbContextInfo != NULL)
        {
            if (pFsbContextInfo->u1ModuleStatus == FSB_ENABLE)
            {
                FsbUnLock ();
                return OSIX_SUCCESS;
            }
        }
    }
    FsbUnLock ();
    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCustSetIfMtu                                  */
/*                                                                           */
/*    Description         : This function is used to set the MTU for all the */
/*                          member ports of VLAN in which FIP Snooping       */
/*                          is enabled.                                      */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Interface index                      */
/*                                                                           */
/*    Output(s)           : NONE                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAIURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCustSetIfMtu (UINT4 u4IfIndex)
{
    /* MTU is set as 2500 for all the ports of the Vlan 
     * in which FIP Snooping is enabled */

    if (CfaApiSetInterfaceMtu (u4IfIndex, FSB_MTU_FRAME_SIZE) != CFA_SUCCESS)
    {
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbCallBackRegister                                  */
/*                                                                           */
/* Description        : This function registers the call backs from          */
/*                      application                                          */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                      pFsCbInfo - Call Back Function                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS/FSB_FAILURE                              */
/*****************************************************************************/
INT4
FsbCallBackRegister (UINT4 u4Event, tFsCbInfo * pFsCbInfo)
{
    INT4                i4RetVal = FSB_SUCCESS;

    FsbLock ();

    switch (u4Event)
    {
        case FSB_SET_IF_MTU_EVENT:
            FSB_CALLBACK[u4Event].pFsbSetIfMtuCallBack =
                pFsCbInfo->pFsbSetIfMtuCallBack;
            break;
        default:
            i4RetVal = FSB_FAILURE;
            break;
    }

    FsbUnLock ();
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbApiAddPortToPortChannel                       */
/*                                                                           */
/*    Description         : This function is used to notify FSB module       */
/*                          when a port is added to a port-channel           */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                          u2AggId  - Port Channel index                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbApiAddPortToPortChannel (UINT4 u4IfIndex, UINT2 u2AggId)
{
    tFsbNotifyParams   *pMsg = NULL;
    UINT4               u4ContextId = 0;

    /* Retrieve the context id from the Interface Index */
    if (FsbGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId) != FSB_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (FsbUtilIsFsbStarted (u4ContextId) != FSB_TRUE)
    {
        /* If FSB is not started in this context,
         * return success*/
        return OSIX_SUCCESS;
    }

    if ((pMsg = (tFsbNotifyParams *) MemAllocMemBlk
         (gFsbGlobals.FsbNotifyParamsPoolId)) == NULL)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                         "FsbApiAddPortToPortChannel: MemAllocMemBlk"
                         " for pMsg failed\r\n");
        return (OSIX_FAILURE);
    }
    pMsg->u4ContextId = u4ContextId;
    pMsg->u4IfIndex = u4IfIndex;
    pMsg->u4AggIndex = (UINT4) u2AggId;
    pMsg->u4MsgType = FSB_ADD_PORT_TO_AGG;

    FsbEnQFrameToFsbTask (pMsg);
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbApiRemovePortFromPortChannel                  */
/*                                                                           */
/*    Description         : This function is used to notify FSB module       */
/*                          when a port is removed from a port-channel       */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                          u2AggId  - Port Channel index                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbApiRemovePortFromPortChannel (UINT4 u4IfIndex, UINT2 u2AggId)
{
    tFsbNotifyParams   *pMsg = NULL;
    UINT4               u4ContextId = 0;

    /* Retrieve the context id from the Interface Index */
    if (FsbGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId) != FSB_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (FsbUtilIsFsbStarted (u4ContextId) != FSB_TRUE)
    {
        /* If FSB is not started in this context,
         * return success*/
        return OSIX_SUCCESS;
    }

    if ((pMsg = (tFsbNotifyParams *) MemAllocMemBlk
         (gFsbGlobals.FsbNotifyParamsPoolId)) == NULL)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                         "FsbApiRemovePortFromPortChannel : MemAllocMemBlk"
                         " for pMsg failed\r\n");
        return (OSIX_FAILURE);
    }
    pMsg->u4ContextId = u4ContextId;
    pMsg->u4IfIndex = u4IfIndex;
    pMsg->u4AggIndex = (UINT4) u2AggId;
    pMsg->u4MsgType = FSB_REMOVE_PORT_FROM_AGG;

    FsbEnQFrameToFsbTask (pMsg);
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbPortListUpdateNotification                    */
/*                                                                           */
/*    Description         : This function is used to process VLAN deletion   */
/*                          indication from L2IWF                            */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId - VLAN Index                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbPortListUpdateNotification (UINT4 u4ContextId, tVlanId VlanId,
                               tLocalPortList AddedPorts,
                               tLocalPortList DeletedPorts)
{
    tFsbNotifyParams   *pMsg = NULL;

    if (FsbUtilIsFsbStarted (u4ContextId) != FSB_TRUE)
    {
        /* If FSB is not started in this context,
         * return success*/
        return OSIX_SUCCESS;
    }
    if ((pMsg = (tFsbNotifyParams *) MemAllocMemBlk
         (gFsbGlobals.FsbNotifyParamsPoolId)) == NULL)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                         "FsbPortListUpdateNotification: MemAllocMemBlk"
                         " for pMsg failed\r\n");
        return (OSIX_FAILURE);
    }
    pMsg->u4ContextId = u4ContextId;
    pMsg->u2VlanId = (UINT2) VlanId;
    FSB_MEMCPY (pMsg->AddedPorts, AddedPorts, sizeof (pMsg->AddedPorts));
    FSB_MEMCPY (pMsg->DeletedPorts, DeletedPorts, sizeof (pMsg->DeletedPorts));
    pMsg->u4MsgType = FSB_PORTLIST_UPDATE_MSG;
    FsbEnQFrameToFsbTask (pMsg);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbPortUpdateNotification                        */
/*                                                                           */
/*    Description         : This function is used to process VLAN deletion   */
/*                          indication from L2IWF                            */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId - VLAN Index                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbPortUpdateNotification (UINT4 u4ContextId, tVlanId VlanId,
                           UINT2 u2LocalPortId)
{
    tFsbNotifyParams   *pMsg = NULL;

    if (FsbUtilIsFsbStarted (u4ContextId) != FSB_TRUE)
    {
        /* If FSB is not started in this context,
         * return success*/
        return OSIX_SUCCESS;
    }
    if ((pMsg = (tFsbNotifyParams *) MemAllocMemBlk
         (gFsbGlobals.FsbNotifyParamsPoolId)) == NULL)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                         "FsbPortUpdateNotification: MemAllocMemBlk"
                         " for pMsg failed\r\n");
        return (OSIX_FAILURE);
    }
    pMsg->u4ContextId = u4ContextId;
    pMsg->u2VlanId = (UINT2) VlanId;
    pMsg->u4IfIndex = (UINT2) u2LocalPortId;
    pMsg->u4MsgType = FSB_PORT_UPDATE_MSG;
    FsbEnQFrameToFsbTask (pMsg);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbGetPhyIfIndexforVlan                          */
/*                                                                           */
/*    Description         : This function is used to retreive a physical     */
/*                          index that is used in the masking logic of filter*/
/*                          creation.                                        */
/*                                                                           */
/*    Input(s)            : VlanId - VLAN Index                              */
/*                                                                           */
/*    Output(s)           : pu4IfIndex - Physical Index                      */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbGetPhyIfIndexforVlan (UINT2 u2VlanIndex, UINT4 *pu4IfIndex)
{
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbIntfEntry       FsbIntfEntry;
    INT4                i4BrgPortType = 0;
    UINT4               u4UapIfIndex = 0;
    UINT2               u2SVID = 0;
    UINT1               u1IfType = 0;

    FSB_MEMSET (&FsbIntfEntry, 0, sizeof (tFsbIntfEntry));

    /* Walk through the interface table for
     * the incoming VLAN*/

    FsbIntfEntry.u2VlanId = u2VlanIndex;
    FsbIntfEntry.u4IfIndex = 0;

    while ((pFsbIntfEntry = (tFsbIntfEntry *)
            RBTreeGetNext (gFsbGlobals.FsbIntfTable,
                           (tRBElem *) & FsbIntfEntry, NULL)) != NULL)
    {
        FsbIntfEntry.u2VlanId = pFsbIntfEntry->u2VlanId;
        FsbIntfEntry.u4IfIndex = pFsbIntfEntry->u4IfIndex;

        if (FsbIntfEntry.u2VlanId == u2VlanIndex)
        {
            FsbCfaGetIfType (FsbIntfEntry.u4IfIndex, &u1IfType);
            FsbCfaGetInterfaceBrgPortType (FsbIntfEntry.u4IfIndex,
                                           &i4BrgPortType);

            if (u1IfType == CFA_LAGG)
            {
                /* Skip if the interface is a LAG interface */
                continue;
            }
            else if (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
            {
                if (FsbVlanApiGetSChInfoFromSChIfIndex (FsbIntfEntry.u4IfIndex,
                                                        &u4UapIfIndex,
                                                        &u2SVID) != FSB_FAILURE)
                {
                    *pu4IfIndex = u4UapIfIndex;
                    break;
                }
            }
            else
            {
                *pu4IfIndex = FsbIntfEntry.u4IfIndex;
                break;
            }
        }
    }
    return FSB_SUCCESS;
}

/********************************************************************************
 *  Function Name      : FsbProcessFsbFrame 
 *
 *  Description        : This function is called whenver a packet is recieved 
 *                       in the NPAPi module.
 *                       The packet is then checked, whether it is an FSB packet 
 *                       based on the ethertype(0x8914). If it is an FSB packet, 
 *                       the packet is directly posted to the FSB queue. 
 *
 *  Input(s)           : u2VlanId   - VLAN Identifer
 *                       u4IfIndex  - Interface Index
 *                       pCruBuf    - Packet Data
 *
 *
 *  Output(s)          : None
 *
 *  Returns            : FSB_SUCCESS/FSB_FAILURE
 **********************************************************************************/
INT4
FsbProcessFsbFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                    UINT2 u2VlanId)
{
    UINT4               u4MesgLength = 0;
    UINT2               u2EtherType = 0;
    UINT2               u2FsbEtherType = 0;
    UINT2               u2NewVlanId = 0;
    UINT2               u2VlanIdMask = 0xfff;

    if (pBuf == NULL)
    {
        return FSB_FAILURE;
    }

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2EtherType,
                               FSB_ETHER_TYPE_OFFSET, FSB_ETHER_TYPE_LEN);
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2FsbEtherType,
                               FSB_SCHANNEL_ETHER_TYPE_OFFSET,
                               FSB_ETHER_TYPE_LEN);
    if ((OSIX_NTOHS (u2EtherType) == VLAN_ETHER_TYPE)
        && (OSIX_NTOHS (u2FsbEtherType) == FSB_ETHER_TYPE))
    {
        if (u2VlanId == 0)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2NewVlanId,
                                       VLAN_ID_OFFSET, VLAN_ETHER_TYPE_LEN);
            u2VlanId = OSIX_NTOHS (u2NewVlanId);
            u2VlanId = u2VlanId & u2VlanIdMask;
        }
        u4MesgLength = (UINT4) CRU_BUF_Get_ChainValidByteCount (pBuf);
        if (FsbSendPacketToFsbQueue (pBuf, u4MesgLength,
                                     u4IfIndex, u2VlanId) != OSIX_SUCCESS)
        {
            return FSB_FAILURE;
        }
    }
    else if (OSIX_NTOHS (u2EtherType) == FSB_ETHER_TYPE)
    {
        if (u2VlanId == 0)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2NewVlanId,
                                       VLAN_ID_OFFSET, VLAN_ETHER_TYPE_LEN);
            u2VlanId = OSIX_NTOHS (u2NewVlanId);
            u2VlanId = u2VlanId & u2VlanIdMask;
        }
        u4MesgLength = (UINT4) CRU_BUF_Get_ChainValidByteCount (pBuf);
        if (FsbSendPacketToFsbQueue (pBuf, u4MesgLength,
                                     u4IfIndex, u2VlanId) != OSIX_SUCCESS)
        {
            return FSB_FAILURE;
        }
    }
    else
    {
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbGetValidPhyIfIndexforVlan                     */
/*                                                                           */
/*    Description         : This function is used to retreive a physical     */
/*                          index that is used in the masking logic of filter*/
/*                          creation.                                        */
/*                                                                           */
/*    Input(s)            : VlanId - VLAN Index                              */
/*                                                                           */
/*    Output(s)           : pu4IfIndex - Physical Index                      */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbGetValidPhyIfIndexforVlan (UINT2 u2VlanIndex, UINT4 *pu4IfIndex)
{
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbIntfEntry       FsbIntfEntry;
    UINT2               au2ConfPorts[L2IWF_MAX_PORTS_PER_CONTEXT];
    INT4                i4BrgPortType = 0;
    UINT4               u4UapIfIndex = 0;
    UINT2               u2SVID = 0;
    UINT2               u2PortIndex = 0;
    UINT2               u2NumPorts = 0;
    UINT1               u1IfType = 0;

    FSB_MEMSET (&FsbIntfEntry, 0, sizeof (tFsbIntfEntry));
    FSB_MEMSET (au2ConfPorts, 0, sizeof (au2ConfPorts));

    /* Walk through the interface table for
     * the incoming VLAN*/

    FsbIntfEntry.u2VlanId = u2VlanIndex;
    FsbIntfEntry.u4IfIndex = 0;

    while ((pFsbIntfEntry = (tFsbIntfEntry *)
            RBTreeGetNext (gFsbGlobals.FsbIntfTable,
                           (tRBElem *) & FsbIntfEntry, NULL)) != NULL)
    {
        FsbIntfEntry.u2VlanId = pFsbIntfEntry->u2VlanId;
        FsbIntfEntry.u4IfIndex = pFsbIntfEntry->u4IfIndex;

        if (FsbIntfEntry.u2VlanId == u2VlanIndex)
        {
            FsbCfaGetIfType (FsbIntfEntry.u4IfIndex, &u1IfType);
            FsbCfaGetInterfaceBrgPortType (FsbIntfEntry.u4IfIndex,
                                           &i4BrgPortType);

            if (u1IfType == CFA_LAGG)
            {
                if (FsbIsMLAGPortChannel ((UINT2) FsbIntfEntry.u4IfIndex) ==
                    FSB_SUCCESS)
                {
                    continue;
                }
                if (FsbL2IwfGetConfiguredPortsForPortChannel
                    (FsbIntfEntry.u4IfIndex, au2ConfPorts,
                     &u2NumPorts) == FSB_SUCCESS)
                {
                    if (u2NumPorts == 0)
                    {
                        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                                         FSB_DEBUGGING_LEVEL, FSB_SESSION_TRC,
                                         "Number of ports in LAG is zero, so continuing");
                        continue;
                    }
                    *pu4IfIndex = (UINT4) (au2ConfPorts[u2PortIndex]);
                    break;
                }
                continue;
            }
            else if (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
            {
                if (FsbVlanApiGetSChInfoFromSChIfIndex (FsbIntfEntry.u4IfIndex,
                                                        &u4UapIfIndex,
                                                        &u2SVID) != FSB_FAILURE)
                {
                    *pu4IfIndex = u4UapIfIndex;
                    break;
                }
            }
            else
            {
                *pu4IfIndex = FsbIntfEntry.u4IfIndex;
                break;
            }
        }
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbApiPortRemoveFromVlan                         */
/*                                                                           */
/*    Description         : This function is used to process single port     */
/*                          deletion indication from L2IWF                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId - VLAN Index                              */
/*                          u4IfIndex - Interface Index                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbApiPortRemoveFromVlan (UINT4 u4ContextId, tVlanId VlanId, UINT4 u4IfIndex)
{
    tFsbNotifyParams   *pMsg = NULL;

    if (FsbUtilIsFsbStarted (u4ContextId) != FSB_TRUE)
    {
        /* If FSB is not started in this context,
         * return success*/
        return OSIX_SUCCESS;
    }
    if ((pMsg = (tFsbNotifyParams *) MemAllocMemBlk
         (gFsbGlobals.FsbNotifyParamsPoolId)) == NULL)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                         "FsbApiPortRemoveFromVlan: MemAllocMemBlk"
                         " for pMsg failed\r\n");
        return (OSIX_FAILURE);
    }
    pMsg->u4ContextId = u4ContextId;
    pMsg->u2VlanId = (UINT2) VlanId;
    pMsg->u4IfIndex = u4IfIndex;
    pMsg->u4MsgType = FSB_PORT_DELETE_NOTIFY_MSG;
    FsbEnQFrameToFsbTask (pMsg);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbIsFCoEVlan                                    */
/*                                                                           */
/*    Description         : This function is used to validate whether the    */
/*                          given VLAN is an FCoE VLAN                       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2VlanIndex - VLAN Index                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_TRUE - If Vlan is FCoE VLAN                  */
/*                          FSB_FALSE - If Vlan is not FCoE VLAN             */
/*                                                                           */
/*****************************************************************************/
INT4
FsbIsFCoEVlan (UINT4 u4ContextId, UINT2 u2VlanIndex)
{
    if (FsbGetFIPSnoopingEntry (u4ContextId, u2VlanIndex) == NULL)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                         "FsbIsFCoEVlan: u2VlanIndex: %d is not "
                         "an FCoE VLAN\r\n", u2VlanIndex);
        return FSB_FALSE;
    }
    return FSB_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbGetPinnedPortForFCoEVlan                       */
/*                                                                           */
/*    Description         : This function is used to validate whether the    */
/*                          FCoE VLAN contains any MLAG.                     */
/*                          MLAG is determined based on the local and remote */
/*                          member ports of the LAG                          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2VlanIndex - VLAN Index                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbGetPinnedPortForFCoEVlan (UINT4 u4ContextId, UINT2 u2VlanId,
                             UINT4 *pu4IfIndex)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    UINT4               u4Index = 0;
    BOOL1               bRetVal = OSIX_FALSE;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4ContextId, u2VlanId);

    if (pFsbFipSnoopingEntry != NULL)
    {
        if (pFsbFipSnoopingEntry->u4PinnedPortCount == 1)
        {
            *pu4IfIndex = (UINT4) pFsbFipSnoopingEntry->u4PinnnedPortIfIndex;
            return FSB_SUCCESS;
        }
        else if (pFsbFipSnoopingEntry->u4PinnedPortCount > 1)
        {
            for (u4Index = 1; u4Index < SYS_DEF_MAX_PHYSICAL_INTERFACES;
                 u4Index++)
            {
                OSIX_BITLIST_IS_BIT_SET (pFsbFipSnoopingEntry->FsbPinnedPorts,
                                         u4Index, BRG_PORT_LIST_SIZE, bRetVal);
                if (bRetVal == OSIX_TRUE)
                {
                    *pu4IfIndex = u4Index;
                    return FSB_SUCCESS;
                }
            }
        }
    }
    return FSB_FAILURE;
}
#endif /* _FSBAPI_C_ */
