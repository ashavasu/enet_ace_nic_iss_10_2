/*************************************************************************
 * * Copyright (C) 2007-2015 Aricent Group . All Rights Reserved
 * *
 * * $Id: fsbmbsm.c,v 1.10 2017/11/02 13:44:19 siva Exp $
 * *
 * * Description: This file contains the FIP-snooping MBSM support routines
 * *              and utility routines
 * *************************************************************************/
#ifndef _FSBMBSM_C_
#define _FSBMBSM_C_

#include "fsbinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbApiMbsmNotification                           */
/*                                                                           */
/*    Description         : This function constructs the FSB Queue msg and   */
/*                          calls the Queue Message event to process the     */
/*                          event                                            */
/*                                                                           */
/*    Input(s)            : tMbsmProtoMsg - Structure containing the SlotInfo*/
/*                          PortInfo and the Protocol Id                     */
/*                          i4Event       - Event to be processed            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : MBSM_FAILURE / MBSM_SUCCESS                      */
/*                                                                           */
/*****************************************************************************/
INT4
FsbApiMbsmNotification (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{
    tFsbNotifyParams   *pMsg = NULL;

    FSB_CONTEXT_TRC (FSB_DEFAULT_CONTEXT_ID, FSB_DEBUGGING_LEVEL,
                     FSB_SESSION_TRC,
                     "FsbApiMbsmNotification: Event received from MBSM: %d \r\n",
                     i4Event);

    if (pProtoMsg == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbApiMbsmNotification : pProtoMsg is NULL");
        return MBSM_FAILURE;
    }

    if ((pMsg = (tFsbNotifyParams *) MemAllocMemBlk
         (FSB_NOTIFY_PARAMS_INFO_MEMPOOL_ID)) == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                         "FsbApiMbsmNotification : MemAllocMemBlk"
                         " for pMsg failed\r\n");
        return MBSM_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tFsbNotifyParams));

    if ((pMsg->pMbsmProtoMsg = (tMbsmProtoMsg *) MemAllocMemBlk
         (FSB_MBSM_MSG_MEMPOOL_ID)) == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                         "FsbApiMbsmNotification : MemAllocMemBlk"
                         " for MbsmProtoMsg failed\r\n");
        MemReleaseMemBlock (FSB_NOTIFY_PARAMS_INFO_MEMPOOL_ID, (UINT1 *) pMsg);
        return MBSM_FAILURE;
    }

    if (i4Event == MBSM_MSG_CARD_INSERT)
    {
        pMsg->u4MsgType = FSB_MBSM_CARD_INSERT_MSG;
    }
    else if (i4Event == MBSM_MSG_CARD_REMOVE)
    {
        pMsg->u4MsgType = FSB_MBSM_CARD_REMOVE_MSG;
    }
    MEMCPY (pMsg->pMbsmProtoMsg, pProtoMsg, sizeof (tMbsmProtoMsg));

    FSB_CONTEXT_TRC (FSB_DEFAULT_CONTEXT_ID, FSB_DEBUGGING_LEVEL,
                     FSB_SESSION_TRC,
                     "FsbApiMbsmNotification: Calling the function FsbEnQFrameToFsbTask\r\n");
    FsbEnQFrameToFsbTask (pMsg);
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbMbsmUpdateOnCardInsertion                     */
/*                                                                           */
/*    Description         : This function does the below set of operations   */
/*                          1) Installs Default VLAN filets                  */
/*                          2) Installs Default Filters applicable of this   */
/*                             slot.                                         */
/*                                                                           */
/*    Input(s)            : pMbsmPortInfo - Pointer to list ports of inserted*/
/*                                          card.                            */
/*                          pMbsmSlotInfo - Pointer to the inserted Slot     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : MBSM_SUCCESS / MBSM_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
FsbMbsmUpdateOnCardInsertion (tMbsmPortInfo * pMbsmPortInfo,
                              tMbsmSlotInfo * pMbsmSlotInfo)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbSChannelFilterEntry *pFsbSChannelFilterEntry = NULL;
    tFsbIntfEntry      *pFsbNextIntfEntry = NULL;
    tFsbContextInfo    *pFsbContextInfo = NULL;
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbLocRemFilterId  FsbLocRemFilterId;
    tFsbSChannelFilterEntry FsbSChannelFilterEntry;
    tFsbFilterEntry     FsbFilterEntry;
    tFsbIntfEntry       FsbIntfEntry;
    INT4                i4BrgPortType = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4UapIfIndex = 0;
    UINT4               u4CurrContextId = 0;
    UINT4               u4HwFilterId = 0;
    UINT2               u2CurrVlanId = 0;
    UINT2               au2ConfPorts[L2IWF_MAX_PORTS_PER_CONTEXT];
    UINT2               u2Index = 0;
    UINT2               u2NumPorts = 0;
    UINT2               u2SVID = 0;
    UINT1               u1IsSetInPortList = 0;
    UINT1               u1IfType = 0;

    UNUSED_PARAM (pMbsmSlotInfo);

    FSB_CONTEXT_TRC (FSB_DEFAULT_CONTEXT_ID, FSB_DEBUGGING_LEVEL,
                     FSB_SESSION_TRC,
                     "FsbMbsmUpdateOnCardInsertion: Entering the function\r\n");

    /* Durring ISSU maintenance mode, avoid the FSB card-insertion notification to
     * avoid filters Ids overwrite with value '0' */
    if (IssuGetMaintModeOperation () == OSIX_TRUE)
    {
        return MBSM_SUCCESS;
    }
    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));
    FSB_MEMSET (&FsbIntfEntry, 0, sizeof (tFsbIntfEntry));
    FSB_MEMSET (&FsbSChannelFilterEntry, 0, sizeof (tFsbSChannelFilterEntry));

    /****
     * Install Default VLAN filter
     ****/

    /* When a new card is inserted,, Default VLAN filters needs to be installed. *
     * Loop through below, to find the the 1st virtual context for 
     * which FIP-snooping is Started (no shut)
     */
    for (u4ContextId = 0; u4ContextId < FSB_MAX_CONTEXT_COUNT; u4ContextId++)
    {

        pFsbContextInfo = FsbCxtGetContextEntry (u4ContextId);

        if (pFsbContextInfo != NULL)
        {
            if (pFsbContextInfo->u1ModuleStatus == FSB_ENABLE)
            {
                /* Install default VLAN Filters for VLAN Discovery */
                if (FsMiNpFsbMbsmHwWrInit (&(pFsbContextInfo->FsbLocFilterEntry.
                                             FsbVlanDiscovery),
                                           &FsbLocRemFilterId) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbMbsmUpdateOnCardInsertion : FsMiNpFsbMbsmHwWrInit failed"
                                     " for FsbVlanDiscovery filter\r\n");
                    return MBSM_FAILURE;
                }

                pFsbContextInfo->FsbRemFilterEntry.FsbVlanDiscovery.
                    u4HwFilterId = FsbLocRemFilterId.u4RemoteFilterId;

                /* Install default VLAN Filters for VLAN tagged response */
                if (FsMiNpFsbMbsmHwWrInit (&(pFsbContextInfo->FsbLocFilterEntry.
                                             FsbVlanResponseTagged),
                                           &FsbLocRemFilterId) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbMbsmUpdateOnCardInsertion : FsMiNpFsbMbsmHwWrInit failed"
                                     " for FsbVlanResponseTagged filter\r\n");
                    return MBSM_FAILURE;
                }

                pFsbContextInfo->FsbRemFilterEntry.FsbVlanResponseTagged.
                    u4HwFilterId = FsbLocRemFilterId.u4RemoteFilterId;

                if (FsbRedSyncUpDefaultVlanFilter (pFsbContextInfo,
                                                   u4ContextId) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbMbsmUpdateOnCardInsertion : "
                                     "FsbRedSyncUpDefaultVlanFilter failed\r\n");
                    return MBSM_FAILURE;
                }
                break;
            }
        }
    }

    /****
     * Install Default filter
     ****/

    /* To install Default Filters with the received Slot and Port Info of MBSM Attach Notification, 
     * the following steps are followed :-
     * 1) For each entry in FsbIntfTable, check if its port belongs to the card
     *    for which MBSM Attach Notification is received.
     * 2) For the match ports, it corresponding VLAN is verified for if FIP
     *     Snooping is ENABLED.  If then install the Default filter
     */

    pFsbIntfEntry = (tFsbIntfEntry *) RBTreeGetFirst (gFsbGlobals.FsbIntfTable);

    if (pFsbIntfEntry == NULL)
    {
        /* No FsbIntfEntry were created and therefore, no entries
         * to program */
        return MBSM_SUCCESS;
    }

    while (pFsbIntfEntry != NULL)
    {
        FsbIntfEntry.u2VlanId = pFsbIntfEntry->u2VlanId;
        FsbIntfEntry.u4IfIndex = pFsbIntfEntry->u4IfIndex;

        pFsbNextIntfEntry = (tFsbIntfEntry *) RBTreeGetNext
            (gFsbGlobals.FsbIntfTable, (tRBElem *) & FsbIntfEntry, NULL);

        FsbCfaGetInterfaceBrgPortType (pFsbIntfEntry->u4IfIndex,
                                       &i4BrgPortType);
        FsbCfaGetIfType (pFsbIntfEntry->u4IfIndex, &u1IfType);

        if (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
        {
            if (FsbVlanApiGetSChInfoFromSChIfIndex (pFsbIntfEntry->u4IfIndex,
                                                    &u4UapIfIndex,
                                                    &u2SVID) != FSB_FAILURE)

            {
                /* Check if the Interface retreived from FsbIntfEntry is in
                 * the inserted card */
                OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST
                                         (pMbsmPortInfo), u4UapIfIndex,
                                         sizeof (tPortListExt),
                                         u1IsSetInPortList);

                if (u1IsSetInPortList == OSIX_TRUE)
                {
                    FsbSChannelFilterEntry.u4IfIndex = u4UapIfIndex;
                    FsbSChannelFilterEntry.u2VlanId = u2SVID;

                    pFsbSChannelFilterEntry =
                        (RBTreeGet
                         (gFsbGlobals.FsbSChannelFilterTable,
                          (tRBElem *) & FsbSChannelFilterEntry));

                    /* Check if S-channel filter entry alraedy exists for the
                     * for the interface */
                    if (pFsbSChannelFilterEntry != NULL)
                    {
                        /* If the entry is present, program the hardware */
                        if (FsFsbHwWrCreateSChannelFilter
                            (pFsbSChannelFilterEntry,
                             &u4HwFilterId) != FSB_SUCCESS)
                        {
                            FSB_CONTEXT_TRC (pFsbIntfEntry->u4ContextId,
                                             FSB_ERROR_LEVEL, FSB_NONE,
                                             "FsbMbsmUpdateOnCardInsertion: "
                                             "FsFsbHwWrCreateSChannelFilter failed\r\n");
                            RBTreeRemove (gFsbGlobals.FsbSChannelFilterTable,
                                          (tRBElem *) pFsbSChannelFilterEntry);
                            MemReleaseMemBlock
                                (FSB_SCHAN_FILT_ENTRIES_MEMPOOL_ID,
                                 (UINT1 *) pFsbSChannelFilterEntry);
                            return MBSM_FAILURE;
                        }
                        pFsbSChannelFilterEntry->u4HwFilterId = u4HwFilterId;
                    }
                    else
                    {
                        /* If not present, then create a new Filter entry for S-Channel */
                        pFsbSChannelFilterEntry =
                            (tFsbSChannelFilterEntry *)
                            MemAllocMemBlk (FSB_SCHAN_FILT_ENTRIES_MEMPOOL_ID);

                        if (pFsbSChannelFilterEntry == NULL)
                        {
                            FSB_CONTEXT_TRC (pFsbIntfEntry->u4ContextId,
                                             FSB_DEBUGGING_LEVEL,
                                             FSB_BUFFER_TRC,
                                             "FsbMbsmUpdateOnCardInsertion: "
                                             "mem alloc for pFsbSChannelFilterEntry failed\r\n");
                            return MBSM_FAILURE;
                        }
                        FSB_MEMSET (pFsbSChannelFilterEntry, 0,
                                    sizeof (tFsbSChannelFilterEntry));

                        pFsbSChannelFilterEntry->u4ContextId =
                            pFsbIntfEntry->u4ContextId;
                        pFsbSChannelFilterEntry->u4IfIndex = u4UapIfIndex;
                        pFsbSChannelFilterEntry->u2VlanId = u2SVID;
                        pFsbSChannelFilterEntry->u1Priority =
                            FSB_FIELD_ENTRY_PRIO_HIGHEST;
                        pFsbSChannelFilterEntry->u4FilterId =
                            FSB_GET_FILTER_ID ();

                        if (FsFsbHwWrCreateSChannelFilter
                            (pFsbSChannelFilterEntry,
                             &u4HwFilterId) != FSB_SUCCESS)
                        {
                            FSB_CONTEXT_TRC (pFsbIntfEntry->u4ContextId,
                                             FSB_ERROR_LEVEL, FSB_NONE,
                                             "FsbMbsmUpdateOnCardInsertion: "
                                             "FsFsbHwWrCreateSChannelFilter failed\r\n");
                            MemReleaseMemBlock
                                (FSB_SCHAN_FILT_ENTRIES_MEMPOOL_ID,
                                 (UINT1 *) pFsbSChannelFilterEntry);
                            return MBSM_FAILURE;
                        }
                        pFsbSChannelFilterEntry->u4HwFilterId = u4HwFilterId;

                        if (RBTreeAdd (gFsbGlobals.FsbSChannelFilterTable,
                                       (tRBElem *) pFsbSChannelFilterEntry) !=
                            RB_SUCCESS)
                        {
                            FSB_CONTEXT_TRC (pFsbIntfEntry->u4ContextId,
                                             FSB_DEBUGGING_LEVEL,
                                             FSB_OS_RESOURCE_TRC,
                                             "FsbMbsmUpdateOnCardInsertion: "
                                             "RBTreeAdd for SChannelFilter failed\r\n");
                            MemReleaseMemBlock
                                (FSB_SCHAN_FILT_ENTRIES_MEMPOOL_ID,
                                 (UINT1 *) pFsbSChannelFilterEntry);
                            return MBSM_FAILURE;
                        }
                    }

                    /* Sync the S-Channel Hw filter ID to Standby */
                    if (FsbRedSyncUpSChannelFilterId
                        (pFsbSChannelFilterEntry->u4IfIndex,
                         pFsbSChannelFilterEntry->u2VlanId,
                         pFsbSChannelFilterEntry->u4HwFilterId) != FSB_SUCCESS)
                    {
                        FSB_CONTEXT_TRC (pFsbIntfEntry->u4ContextId,
                                         FSB_ERROR_LEVEL, FSB_NONE,
                                         "FsbMbsmUpdateOnCardInsertion: "
                                         "FsbRedSyncUpSChannelFilterId failed\r\n");
                        return MBSM_FAILURE;
                    }
                }
            }
        }
        if (u1IfType == CFA_LAGG)
        {
            /* Assuming all the ports of the LAG are in the same line card */
            /* Retrieve the configured ports in the port-channel */
            if (FsbL2IwfGetConfiguredPortsForPortChannel ((UINT2)
                                                          pFsbIntfEntry->
                                                          u4IfIndex,
                                                          au2ConfPorts,
                                                          &u2NumPorts) !=
                FSB_FAILURE)
            {
                if (u2NumPorts != 0)
                {
                    OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST
                                             (pMbsmPortInfo),
                                             au2ConfPorts[u2Index],
                                             sizeof (tPortListExt),
                                             u1IsSetInPortList);
                }
            }
        }
        else
        {
            /* Check if the Interface retreived from FsbIntfEntry is in
             * the inserted card */
            OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST (pMbsmPortInfo),
                                     pFsbIntfEntry->u4IfIndex,
                                     sizeof (tPortListExt), u1IsSetInPortList);
        }

        if (u1IsSetInPortList == OSIX_FALSE)
        {
            /* If the entry is not present, then proceed to the next entry */
            pFsbIntfEntry = pFsbNextIntfEntry;
            continue;
        }

        /* Since, default Filter will be installed for all the member ports
         * of the VLAN in the context on the First entry itself,
         * skip directly to Interface Entry with a different VLAN */
        if ((pFsbIntfEntry->u4ContextId == u4CurrContextId) &&
            (pFsbIntfEntry->u2VlanId == u2CurrVlanId))
        {
            pFsbIntfEntry = pFsbNextIntfEntry;
            continue;
        }
        /* If FsbIntfEntry matches then get its corresponding 
         * tFsbFipSnoopingEntry and tFsbContextInfo entry.
         * If the Module Status of the Context and the Enabled status of
         * FIP-snooping vlan is set, then, 
         * install default filters for the VLAN */
        pFsbFipSnoopingEntry =
            FsbGetFIPSnoopingEntry (pFsbIntfEntry->u4ContextId,
                                    pFsbIntfEntry->u2VlanId);

        pFsbContextInfo = FsbCxtGetContextEntry (pFsbIntfEntry->u4ContextId);

        if ((pFsbFipSnoopingEntry != NULL) && (pFsbContextInfo != NULL))
        {
            /* Set Mac-learnig as disable for FCoE VlAN */
            if (FsbPortSetVlanMacLearningStatus (pFsbIntfEntry->u4ContextId,
                                                 pFsbIntfEntry->u2VlanId,
                                                 VLAN_DISABLED) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbIntfEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbMbsmUpdateOnCardInsertion: failure in disabling"
                                 " Mac-learing status for VLAN %d\r\n",
                                 pFsbIntfEntry->u2VlanId);
                return MBSM_FAILURE;
            }

            if ((pFsbFipSnoopingEntry->u1EnabledStatus != FSB_ENABLE) ||
                (pFsbContextInfo->u1ModuleStatus != FSB_ENABLE))
            {
                pFsbIntfEntry = pFsbNextIntfEntry;
                continue;
            }
        }

        if (pFsbFipSnoopingEntry != NULL)
        {
            pFsbFilterEntry = (tFsbFilterEntry *)
                RBTreeGetFirst (pFsbFipSnoopingEntry->FsbFilterEntry);
        }

        while (pFsbFilterEntry != NULL)
        {
            FSB_MEMCPY (FsbFilterEntry.DstMac, pFsbFilterEntry->DstMac,
                        MAC_ADDR_LEN);

            if (FsFsbMbsmHwWrCreateFilter (pFsbFilterEntry, &u4HwFilterId)
                != FSB_SUCCESS)
            {
                /* Incase of FAILURE, remove from RBTree and release MemBlock */
                FSB_CONTEXT_TRC (pFsbIntfEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbMbsmUpdateOnCardInsertion: FsFsbMbsmHwWrCreateFilter failed\r\n");
                RBTreeRemove (pFsbFipSnoopingEntry->FsbFilterEntry,
                              (tRBElem *) pFsbFilterEntry);
                MemReleaseMemBlock (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                                    (UINT1 *) pFsbFilterEntry);
                return MBSM_FAILURE;
            }

            pFsbFilterEntry->u4HwFilterId = u4HwFilterId;

            pFsbFilterEntry = (tFsbFilterEntry *)
                RBTreeGetNext (pFsbFipSnoopingEntry->FsbFilterEntry,
                               (tRBElem *) & FsbFilterEntry, NULL);
        }

        /* Sync the hw filter ID returned for each FCoE VLAN */
        if (FsbRedSyncUpDefaultFilter (pFsbFipSnoopingEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbMbsmUpdateOnCardInsertion :"
                             "FsbRedSyncUpDefaultFilter failed\r\n");
            return MBSM_FAILURE;
        }

        /* Increment the counter for the completion of ACL installation for a
         * VLAN.  This is required to stagger the loop for ACL installation */
        u4CurrContextId = pFsbIntfEntry->u4ContextId;
        u2CurrVlanId = pFsbIntfEntry->u2VlanId;
        pFsbIntfEntry = pFsbNextIntfEntry;
    }
    FSB_CONTEXT_TRC (FSB_DEFAULT_CONTEXT_ID, FSB_DEBUGGING_LEVEL,
                     FSB_SESSION_TRC,
                     "FsbMbsmUpdateOnCardInsertion: Exiting the function\r\n");
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbMbsmUpdateOnCardRemoval                       */
/*                                                                           */
/*    Description         : This function does the below set of operation    */
/*                          1) Deletes FCF Entry and its filter for the port */
/*                          in the pMbsmPortInfo                             */
/*                          2) Delets Session Entry and its filter for the   */
/*                          port in the pMbsmPortInfo                        */
/*                                                                           */
/*    Input(s)            : pMbsmPortInfo - Pointer to list ports of inserted*/
/*                                          card.                            */
/*                          pMbsmSlotInfo - Pointer to the inserted Slot     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : MBSM_SUCCESS / MBSM_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
FsbMbsmUpdateOnCardRemoval (tMbsmPortInfo * pMbsmPortInfo,
                            tMbsmSlotInfo * pMbsmSlotInfo)
{
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbIntfEntry       FsbIntfEntry;
    UINT4               u4Port = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;
    UINT1               u1IsSetInPortList = 0;
    UINT1               u1IfType = 0;
    UNUSED_PARAM (pMbsmSlotInfo);

    FSB_MEMSET (&FsbIntfEntry, 0, sizeof (tFsbIntfEntry));

    /* FsbMbsmUpdateOnCardRemoval() takes care of only removing 
     * the FCF entires and FIP Session entry learnt or formed 
     * on the physical port. The removal of these entries for the 
     * logical ports which are formed out of the physical port 
     * contained in this Card being removed, is handled by the 
     * oper down indication in the FSB module.
     */
    for (u2ByteIndex = 0; u2ByteIndex < BRG_PORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = pMbsmPortInfo->PortList[u2ByteIndex];

        for (u2BitIndex = 0; ((u2BitIndex < BITS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & VLAN_BIT8) != 0)
            {
                u4Port = (UINT4) ((u2ByteIndex * BITS_PER_BYTE) +
                                  u2BitIndex + 1);

                OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST
                                         (pMbsmPortInfo), u4Port,
                                         sizeof (tPortList), u1IsSetInPortList);

                if (OSIX_FALSE == u1IsSetInPortList)
                {
                    continue;
                }

                /*Check if the port is preset in Interface Entry */
                FsbIntfEntry.u2VlanId = 0;
                FsbIntfEntry.u4IfIndex = u4Port;

                pFsbIntfEntry = (tFsbIntfEntry *) RBTreeGetNext
                    (gFsbGlobals.FsbIntfTable,
                     (tRBElem *) & FsbIntfEntry, FsbMbsmCompareIntfEntryIndex);

                if ((pFsbIntfEntry != NULL) &&
                    (pFsbIntfEntry->u4IfIndex == u4Port))
                {
                    FsbCfaGetIfType (pFsbIntfEntry->u4IfIndex, &u1IfType);

                    if (u1IfType != CFA_LAGG)
                    {
                        if (FsbDeleteFcfEntryForIfIndex
                            (pFsbIntfEntry->u4IfIndex) != FSB_SUCCESS)
                        {
                            FSB_CONTEXT_TRC (pFsbIntfEntry->u4ContextId,
                                             FSB_ERROR_LEVEL, FSB_NONE,
                                             "FsbMbsmUpdateOnCardRemoval : FsbDeleteFcfEntryForIfIndex "
                                             "returns failure in VLAN ID: %d, If Index: %d\r\n",
                                             pFsbIntfEntry->u2VlanId,
                                             pFsbIntfEntry->u4IfIndex);

                            return MBSM_FAILURE;
                        }
                        if (FsbDeleteSessEntryForIfIndex
                            (pFsbIntfEntry->u4IfIndex) != FSB_SUCCESS)
                        {
                            FSB_CONTEXT_TRC (pFsbIntfEntry->u4ContextId,
                                             FSB_ERROR_LEVEL, FSB_NONE,
                                             "FsbMbsmUpdateOnCardRemoval : FsbDeleteSessEntryForIfIndex"
                                             "returns failure in VLAN ID: %d, If Index: %d\r\n",
                                             pFsbIntfEntry->u2VlanId,
                                             pFsbIntfEntry->u4IfIndex);
                            return MBSM_FAILURE;
                        }
                    }
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbMbsmConfigDefaultFilter                       */
/*                                                                           */
/*    Description         : This function does the below set of operation    */
/*                          1) The next entry of tFsbIntfEntry for which     */
/*                          default filters were last programmed is          */
/*                          retreived from u2VlanId and u4IfIndex            */
/*                          2) Default filters are installed for the VLAN    */
/*                          if it is not already programmed                  */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2VlanId - VLAN index                            */
/*                          u4IfIndex - Interface Index                      */
/*                          pMbsmPortInfo - Pointer to list ports of inserted*/
/*                                          card.                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : MBSM_SUCCESS / MBSM_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
FsbMbsmConfigDefaultFilter (UINT4 u4ContextId, UINT2 u2VlanId,
                            UINT4 u4IfIndex, tMbsmPortInfo * pMbsmPortInfo)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbIntfEntry      *pFsbNextIntfEntry = NULL;
    tFsbContextInfo    *pFsbContextInfo = NULL;
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;
    tFsbIntfEntry       FsbIntfEntry;
    UINT4               u4HwFilterId;
    UINT2               u2VlanCount = 0;
    UINT1               u1IsSetInPortList = 0;

    /* GetNext the InterfaceEntry for which Default filters was last programmed */
    FsbIntfEntry.u2VlanId = u2VlanId;
    FsbIntfEntry.u4IfIndex = u4IfIndex;
    pFsbIntfEntry = (tFsbIntfEntry *) RBTreeGetNext (gFsbGlobals.FsbIntfTable,
                                                     (tRBElem *) & FsbIntfEntry,
                                                     NULL);

    if (pFsbIntfEntry == NULL)
    {
        return FSB_SUCCESS;
    }

    while (pFsbIntfEntry != NULL)
    {
        FsbIntfEntry.u2VlanId = pFsbIntfEntry->u2VlanId;
        FsbIntfEntry.u4IfIndex = pFsbIntfEntry->u4IfIndex;

        pFsbNextIntfEntry = (tFsbIntfEntry *) RBTreeGetNext
            (gFsbGlobals.FsbIntfTable, (tRBElem *) & FsbIntfEntry, NULL);

        /* Check if the Interface retreived from FsbIntfEntry is in the
         * inserted card */
        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST (pMbsmPortInfo),
                                 pFsbIntfEntry->u4IfIndex,
                                 sizeof (tPortListExt), u1IsSetInPortList);

        if (u1IsSetInPortList == OSIX_FALSE)
        {
            pFsbIntfEntry = pFsbNextIntfEntry;
            continue;
        }
        /* Since, default Filter will be installed for all the member ports
         * of the VLAN in the context on the First entry itself,
         * skip directly to Interface Entry with a different
         * VLAN */
        if ((pFsbIntfEntry->u4ContextId == u4ContextId) &&
            (pFsbIntfEntry->u2VlanId == u2VlanId))
        {
            pFsbIntfEntry = pFsbNextIntfEntry;
            continue;
        }
        u2VlanCount++;

        /* If FsbIntfEntry matches then get its corresponding
         * tFsbFipSnoopingEntry and tFsbContextInfo entry.
         * If the Module Status of the Context and the Enabled status of
         * FIP-snooping vlan is set, then, install default filters
         * for the VLAN */
        pFsbFipSnoopingEntry =
            FsbGetFIPSnoopingEntry (pFsbIntfEntry->u4ContextId,
                                    pFsbIntfEntry->u2VlanId);

        pFsbContextInfo = FsbCxtGetContextEntry (pFsbIntfEntry->u4ContextId);

        if ((pFsbFipSnoopingEntry != NULL) && pFsbContextInfo != NULL)
        {
            if ((pFsbFipSnoopingEntry->u1EnabledStatus != FSB_ENABLE) ||
                (pFsbContextInfo->u1ModuleStatus != FSB_ENABLE))
            {
                pFsbIntfEntry = pFsbNextIntfEntry;
                continue;
            }
        }

        if (pFsbFipSnoopingEntry != NULL)
        {
            pFsbFilterEntry = (tFsbFilterEntry *)
                RBTreeGetFirst (pFsbFipSnoopingEntry->FsbFilterEntry);
        }

        while (pFsbFilterEntry != NULL)
        {
            FSB_MEMCPY (FsbFilterEntry.DstMac, pFsbFilterEntry->DstMac,
                        MAC_ADDR_LEN);

            if (FsFsbMbsmHwWrCreateFilter (pFsbFilterEntry, &u4HwFilterId)
                != FSB_SUCCESS)
            {
                /* Incase of FAILURE, remove from RBTree
                 * and release MemBlock */
                FSB_CONTEXT_TRC (pFsbIntfEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbMbsmConfigDefaultFilter: FsFsbMbsmHwWrCreateFilter failed\r\n");
                RBTreeRemove (pFsbFipSnoopingEntry->FsbFilterEntry,
                              (tRBElem *) pFsbFilterEntry);
                MemReleaseMemBlock (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                                    (UINT1 *) pFsbFilterEntry);
                return MBSM_FAILURE;
            }

            pFsbFilterEntry->u4HwFilterId = u4HwFilterId;

            pFsbFilterEntry = (tFsbFilterEntry *)
                RBTreeGetNext (pFsbFipSnoopingEntry->FsbFilterEntry,
                               (tRBElem *) & FsbFilterEntry, NULL);
        }

        /* Start MBSM stagerring timer */
        if (u2VlanCount >= FSB_MBSM_MAX_STAGGERING_VLAN_COUNT)
        {
            if (FsbTmrStartTimer (&(gFsbGlobals.FsbMbsmTmr),
                                  FSB_MBSM_TIMER_ID,
                                  FSB_MBSM_STAGERRING_TIME,
                                  pFsbIntfEntry) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbIntfEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbMbsmUpdateOnCardInsertion: FsbTmrStartTimer failed\r\n");
                return MBSM_FAILURE;

            }
            return FSB_IN_PROGRESS;
        }
        u4ContextId = pFsbIntfEntry->u4ContextId;
        u2VlanId = pFsbIntfEntry->u2VlanId;
        pFsbIntfEntry = pFsbNextIntfEntry;
    }
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbSendAckToMbsm                                 */
/*                                                                           */
/*    Description         : This function is used to send Ack to MBSM on     */
/*                          processing card attach or card remove event      */
/*                                                                           */
/*    Input(s)            : i4RetStatus - Return value on processing         */
/*                                        card attach or card remove event   */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : MBSM_SUCCESS / MBSM_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
FsbSendAckToMbsm (INT4 i4RetStatus)
{
    tMbsmProtoAckMsg    MbsmProtoAckMsg;

    FSB_MEMSET (&MbsmProtoAckMsg, 0, sizeof (tMbsmProtoAckMsg));
    MbsmProtoAckMsg.i4ProtoCookie = gi4FsbProtoId;
    MbsmProtoAckMsg.i4SlotId = gi4FsbMbsmSlotId;
    MbsmProtoAckMsg.i4RetStatus = i4RetStatus;
    return MbsmSendAckFromProto (&MbsmProtoAckMsg);
}
#endif /* _FSBMBSM_C_ */
