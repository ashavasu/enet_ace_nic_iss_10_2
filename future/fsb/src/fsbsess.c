/*************************************************************************
* Copyright (C) 2007-2012 Aricent Group . All Rights Reserved
*
* $Id: fsbsess.c,v 1.16 2017/11/07 12:23:59 siva Exp $
*
* Description: This file contains the utility function related
* to FIP SESSION for FSB module
*
*************************************************************************/
#ifndef _FSBSESS_C_
#define _FSBSESS_C_

#include "fsbinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbHandleFIPSession                              */
/*                                                                           */
/*    Description         : This function receives the following FIP frames, */
/*                          to maintain a FIP Session and install ACLs :-    */
/*                          1. FSB_FIP_SOLICIT_MCAST                         */
/*                          2. FSB_FIP_SOLICIT_UCAST                         */
/*                          3. FSB_FIP_ADV_UCAST                             */
/*                          4. FSB_FIP_FLOGI_REQUEST                         */
/*                          5. FSB_FIP_NPIV_FDISC_REQUEST                    */
/*                          6. FSB_FIP_FLOGI_ACCEPT                          */
/*                          7. FSB_FIP_NPIV_FDISC_ACCEPT                     */
/*                          8. FSB_FIP_FLOGI_REJECT                          */
/*                          9. FSB_FIP_LOGO_REQUEST                          */
/*                          10.FSB_FIP_LOGO_ACCEPT                           */
/*                          11.FSB_FIP_LOGO_REJECT                           */
/*                          12.FSB_FIP_CLEAR_LINK                            */
/*                          13.FSB_FIP_ENODE_KEEP_ALIVE                      */
/*                          14.FSB_FIP_VNPORT_KEEP_ALIVE                     */
/*                                                                           */
/*    Input(s)            : pFSBPktInfo - Pointer to tFSBPktInfo             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbHandleFIPSession (tFSBPktInfo * pFSBPktInfo)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;
    tFsbFipSessEntry   *pFsbFipSessEntry = NULL;
    tFsbFipSessEntry    FsbFipSessEntry;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];

    FSB_MEMSET (&FsbFipSessEntry, 0, sizeof (tFsbFipSessEntry));
    FSB_MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);

    pFsbFipSessEntry = FsbGetFIPSessionInfo (pFSBPktInfo);
    if (pFsbFipSessEntry == NULL)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbHandleFIPSession: Failed to get "
                         "FIP Session Entry for the Packet: %s received on Vlan: %d "
                         "with IfIndex: %d\r\n",
                         FsbErrString[pFSBPktInfo->PacketType],
                         pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
        return FSB_FAILURE;
    }

    /* Handle FIP messages for each of session identified by the FSB.  The
     * FIP messages are handled based on state maintained for each of the FIP
     * Session. */
    switch (pFSBPktInfo->PacketType)
    {
            /* Intentional fall through - Since both Discovery Solicitation Unicast and
             * Multicast messages are handled similarly */
        case FSB_FIP_SOLICIT_MCAST:
        case FSB_FIP_SOLICIT_UCAST:
            /* When Discovery Solicitation Unicast/Multicast from ENode is received, 
             * previous state must be FSB_FIP_INITIAL_STATE. FIP Session entry will be 
             * created with VlanId, ENodeIfIndex, ENodeMac as index and 
             * the state of that FIP Session is set to FSB_FIP_INITIAL_STATE */
            if (pFsbFipSessEntry->u1State == FSB_FIP_INITIAL_STATE)
            {
                if (FsbHandleFIPSolicit (pFSBPktInfo, pFsbFipSessEntry) !=
                    FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                     FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbHandleFIPSession: "
                                     "Handling FIP Solicitation message received on "
                                     "Vlan: %d with IfIndex: %d failed\r\n",
                                     pFSBPktInfo->u2VlanId,
                                     pFSBPktInfo->u4IfIndex);
                    return FSB_FAILURE;
                }
            }
            else
            {
                /* Incase successive FSB_FIP_SOLICIT_MCAST packets are 
                 * received */
                if ((pFsbFipSessEntry->u1State == FSB_FIP_SOLICIT_MCAST)
                    && (pFSBPktInfo->PacketType == FSB_FIP_SOLICIT_MCAST))
                {
                    return FSB_SUCCESS;
                }
                if (pFsbFipSessEntry->u1State == FSB_FIP_FLOGI_ACCEPT)
                {
                    FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                     FSB_DEBUGGING_LEVEL, FSB_SESSION_TRC,
                                     "FsbHandleFIPSession: "
                                     "Transmitting the solicitation packet"
                                     "for Vlan Id: %d, IfIndex: %d\r\n",
                                     pFSBPktInfo->u2VlanId,
                                     pFSBPktInfo->u4IfIndex);
                    return FSB_SUCCESS;
                }

                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbHandleFIPSession:"
                                 " Invalid State - Received Packet Type:%s Context:%d"
                                 " VLAN:%d ENode IfIndex:%d Current State:%s \n",
                                 FsbErrString[pFSBPktInfo->PacketType],
                                 pFsbFipSessEntry->u4ContextId,
                                 pFsbFipSessEntry->u2VlanId,
                                 pFsbFipSessEntry->u4ENodeIfIndex,
                                 FsbErrString[pFsbFipSessEntry->u1State]);
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "ENode Mac Address: " "%x:%x:%x:%x:%x:%x\n",
                                 pFsbFipSessEntry->ENodeMacAddr[0],
                                 pFsbFipSessEntry->ENodeMacAddr[1],
                                 pFsbFipSessEntry->ENodeMacAddr[2],
                                 pFsbFipSessEntry->ENodeMacAddr[3],
                                 pFsbFipSessEntry->ENodeMacAddr[4],
                                 pFsbFipSessEntry->ENodeMacAddr[5]);
                return FSB_FAILURE;
            }
            pFsbFipSessEntry->u1State = pFSBPktInfo->PacketType;
            break;

        case FSB_FIP_ADV_UCAST:
            /* When Discovery Advertisement Unicast from FCF is received, 
             * previous state must be either Discovery Solicitation Multicast/Unicast 
             * and also Discovery Advertisement Unicast when mutliple FCFs responds for 
             * Discovery Solicitaion Multicast */
            if ((pFsbFipSessEntry->u1State == FSB_FIP_SOLICIT_MCAST) ||
                (pFsbFipSessEntry->u1State == FSB_FIP_SOLICIT_UCAST) ||
                (pFsbFipSessEntry->u1State == FSB_FIP_ADV_UCAST))
            {
                if (FsbHandleFIPAdvUcast (pFSBPktInfo, pFsbFipSessEntry)
                    != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                     FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbHandleFIPSession:"
                                     " Handling FIP Advertisement message received on"
                                     " Vlan: %d with IfIndex: %d failed\r\n",
                                     pFSBPktInfo->u2VlanId,
                                     pFSBPktInfo->u4IfIndex);
                    return FSB_FAILURE;
                }
                if (FsbHandleFcfDiscovery (pFSBPktInfo) == FSB_FAILURE)
                {
                    FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                     FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbHandleFIPSession:"
                                     "FsbHandleFcfDiscovery with VlanId: %d "
                                     " and IfIndex: %d return failure\r\n",
                                     pFSBPktInfo->u2VlanId,
                                     pFSBPktInfo->u4IfIndex);
                    return FSB_FAILURE;
                }
            }
            else
            {
                if (pFsbFipSessEntry->u1State == FSB_FIP_FLOGI_ACCEPT)
                {
                    FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                     FSB_DEBUGGING_LEVEL, FSB_SESSION_TRC,
                                     "FsbHandleFIPSession: Transmitting the advertisement"
                                     "unicast packetfor Vlan Id: %d, IfIndex: %d\r\n",
                                     pFSBPktInfo->u2VlanId,
                                     pFSBPktInfo->u4IfIndex);
                    return FSB_SUCCESS;
                }
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbHandleFIPSession:"
                                 " Invalid State - Received Packet Type:%s Context:%d"
                                 " VLAN:%d ENode IfIndex:%d Current State:%s \n",
                                 FsbErrString[pFSBPktInfo->PacketType],
                                 pFsbFipSessEntry->u4ContextId,
                                 pFsbFipSessEntry->u2VlanId,
                                 pFsbFipSessEntry->u4ENodeIfIndex,
                                 FsbErrString[pFsbFipSessEntry->u1State]);
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "ENode Mac Address: " "%x:%x:%x:%x:%x:%x\n",
                                 pFsbFipSessEntry->ENodeMacAddr[0],
                                 pFsbFipSessEntry->ENodeMacAddr[1],
                                 pFsbFipSessEntry->ENodeMacAddr[2],
                                 pFsbFipSessEntry->ENodeMacAddr[3],
                                 pFsbFipSessEntry->ENodeMacAddr[4],
                                 pFsbFipSessEntry->ENodeMacAddr[5]);
                return FSB_FAILURE;
            }
            pFsbFipSessEntry->u1State = pFSBPktInfo->PacketType;
            break;

        case FSB_FIP_FLOGI_REQUEST:
            /* When FLOGI Request from ENode is received, previous state can be either
             * 1) FSB_FIP_ADV_UCAST, if FIP Session is initiated from 
             *    FSB_FIP_SOLICIT_MCAST/FSB_FIP_SOLICIT_UCAST.
             * 2) FSB_FIP_INITIAL_STATE, if FIP Session is initiated from
             *    FSB_FIP_FLOGI_REQUEST. */
            if (pFsbFipSessEntry->u1State == FSB_FIP_ADV_UCAST)
            {
                if (FsbHandleFLOGIRequest (pFSBPktInfo,
                                           pFsbFipSessEntry) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                     FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbHandleFIPSession:"
                                     " Handling FLOGI Request message received on Vlan: %d"
                                     " with IfIndex: %d failed\r\n",
                                     pFSBPktInfo->u2VlanId,
                                     pFSBPktInfo->u4IfIndex);
                    return FSB_FAILURE;
                }
            }
            else if (pFsbFipSessEntry->u1State == FSB_FIP_INITIAL_STATE)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                                 "FsbHandleFIPSession:"
                                 " Handling FLOGI Request message from Initial State"
                                 " received on Vlan: %d with IfIndex: %d\r\n",
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);

                if (FsbHandleFLOGIReqFromInitState (pFSBPktInfo,
                                                    pFsbFipSessEntry) !=
                    FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                     FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbHandleFIPSession:"
                                     " Handling FLOGI Request message from Initial State"
                                     " received on Vlan: %d with IfIndex: %d failed\r\n",
                                     pFSBPktInfo->u2VlanId,
                                     pFSBPktInfo->u4IfIndex);
                    return FSB_FAILURE;
                }
            }
            else
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbHandleFIPSession:"
                                 " Invalid State - Received Packet Type:%s Context:%d"
                                 " VLAN:%d ENode IfIndex:%d Current State:%s \n",
                                 FsbErrString[pFSBPktInfo->PacketType],
                                 pFsbFipSessEntry->u4ContextId,
                                 pFsbFipSessEntry->u2VlanId,
                                 pFsbFipSessEntry->u4ENodeIfIndex,
                                 FsbErrString[pFsbFipSessEntry->u1State]);
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "ENode Mac Address: " "%x:%x:%x:%x:%x:%x\n",
                                 pFsbFipSessEntry->ENodeMacAddr[0],
                                 pFsbFipSessEntry->ENodeMacAddr[1],
                                 pFsbFipSessEntry->ENodeMacAddr[2],
                                 pFsbFipSessEntry->ENodeMacAddr[3],
                                 pFsbFipSessEntry->ENodeMacAddr[4],
                                 pFsbFipSessEntry->ENodeMacAddr[5]);
                return FSB_FAILURE;
            }
            pFsbFipSessEntry->u1State = pFSBPktInfo->PacketType;
            break;

        case FSB_FIP_NPIV_FDISC_REQUEST:
            /* When NPIV_DISC Request from ENode is received, previous state must be 
             * either FLOGI Accept or FDISC Accept */
            if ((pFsbFipSessEntry->u1State != FSB_FIP_FLOGI_ACCEPT) &&
                (pFsbFipSessEntry->u1State != FSB_FIP_NPIV_FDISC_ACCEPT) &&
                (pFsbFipSessEntry->u1State != FSB_FIP_NPIV_FDISC_REQUEST))
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbHandleFIPSession:"
                                 " Invalid State - Received Packet Type:%s Context:%d"
                                 " VLAN:%d ENode IfIndex:%d Current State:%s \n",
                                 FsbErrString[pFSBPktInfo->PacketType],
                                 pFsbFipSessEntry->u4ContextId,
                                 pFsbFipSessEntry->u2VlanId,
                                 pFsbFipSessEntry->u4ENodeIfIndex,
                                 FsbErrString[pFsbFipSessEntry->u1State]);
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "ENode Mac Address: " "%x:%x:%x:%x:%x:%x\n",
                                 pFsbFipSessEntry->ENodeMacAddr[0],
                                 pFsbFipSessEntry->ENodeMacAddr[1],
                                 pFsbFipSessEntry->ENodeMacAddr[2],
                                 pFsbFipSessEntry->ENodeMacAddr[3],
                                 pFsbFipSessEntry->ENodeMacAddr[4],
                                 pFsbFipSessEntry->ENodeMacAddr[5]);
                return FSB_FAILURE;
            }
            pFsbFipSessEntry->u1State = pFSBPktInfo->PacketType;
            break;

            /* Intentional fall through - Since both FLOGI/FDISC Accept messages 
             * are handled similarly */
        case FSB_FIP_FLOGI_ACCEPT:
        case FSB_FIP_NPIV_FDISC_ACCEPT:
            /* When FLOGI/FDISC Accept from FCF is received, previous state must be
             * either FLOGI Request or FDISC Request */
            if ((pFsbFipSessEntry->u1State == FSB_FIP_FLOGI_REQUEST) ||
                (pFsbFipSessEntry->u1State == FSB_FIP_NPIV_FDISC_REQUEST) ||
                (pFsbFipSessEntry->u1State == FSB_FIP_NPIV_FDISC_ACCEPT))
            {
                pFsbFipSessFCoEEntry = FsbGetFIPSessionFCoEInfo (pFSBPktInfo,
                                                                 pFsbFipSessEntry);
                if (FsbHandleFLOGIAccept (pFSBPktInfo, pFsbFipSessFCoEEntry) !=
                    FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                     FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbHandleFIPSession:"
                                     " Handling FLOGI Accept message received on Vlan: %d"
                                     " with IfIndex: %d failed\r\n",
                                     pFSBPktInfo->u2VlanId,
                                     pFSBPktInfo->u4IfIndex);
                    return FSB_FAILURE;
                }
            }
            else
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbHandleFIPSession:"
                                 " Invalid State - Received Packet Type:%s Context:%d"
                                 " VLAN:%d ENode IfIndex:%d Current State:%s \n",
                                 FsbErrString[pFSBPktInfo->PacketType],
                                 pFsbFipSessEntry->u4ContextId,
                                 pFsbFipSessEntry->u2VlanId,
                                 pFsbFipSessEntry->u4ENodeIfIndex,
                                 FsbErrString[pFsbFipSessEntry->u1State]);
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "ENode Mac Address: " "%x:%x:%x:%x:%x:%x\n",
                                 pFsbFipSessEntry->ENodeMacAddr[0],
                                 pFsbFipSessEntry->ENodeMacAddr[1],
                                 pFsbFipSessEntry->ENodeMacAddr[2],
                                 pFsbFipSessEntry->ENodeMacAddr[3],
                                 pFsbFipSessEntry->ENodeMacAddr[4],
                                 pFsbFipSessEntry->ENodeMacAddr[5]);
                return FSB_FAILURE;
            }
            pFsbFipSessEntry->u1State = pFSBPktInfo->PacketType;
            break;

        case FSB_FIP_FLOGI_REJECT:
            /* When FLOGI/FDISC Reject from FCF is received, previous state must be
             * FLOGI Request */
            if (pFsbFipSessEntry->u1State == FSB_FIP_FLOGI_REQUEST)
            {
                FsbFipSessEntry.u2VlanId = pFsbFipSessEntry->u2VlanId;
                FSB_MEMCPY ((FsbFipSessEntry.ENodeMacAddr),
                            pFsbFipSessEntry->ENodeMacAddr, MAC_ADDR_LEN);
                pFsbFipSessEntry->u1DeleteReasonCode =
                    FSB_REASON_CODE_FLOGI_REJECT;
                if (FsbDeleteFIPSessionEntry (pFsbFipSessEntry) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                     FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbHandleFIPSession:"
                                     " Handling FLOGI Reject message received on Vlan: %d"
                                     " with IfIndex: %d failed\r\n",
                                     pFSBPktInfo->u2VlanId,
                                     pFSBPktInfo->u4IfIndex);
                    return FSB_FAILURE;
                }
                /* Sync-Up FIP Session Entry to Stand-By */
                if (FsbRedSyncUpFIPSessEntry (&FsbFipSessEntry,
                                              pFSBPktInfo) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                     FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbHandleFIPSession: FsbRedSyncUpFIPSessEntry with VlanId: %d"
                                     " and IfIndex: %d failed\r\n",
                                     pFSBPktInfo->u2VlanId,
                                     pFSBPktInfo->u4IfIndex);
                    return FSB_FAILURE;
                }
                else
                {
                    return FSB_SUCCESS;
                }
            }
            else
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbHandleFIPSession:"
                                 " Invalid State - Received Packet Type:%s Context:%d"
                                 " VLAN:%d ENode IfIndex:%d Current State:%s \n",
                                 FsbErrString[pFSBPktInfo->PacketType],
                                 pFsbFipSessEntry->u4ContextId,
                                 pFsbFipSessEntry->u2VlanId,
                                 pFsbFipSessEntry->u4ENodeIfIndex,
                                 FsbErrString[pFsbFipSessEntry->u1State]);
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "ENode Mac Address: " "%x:%x:%x:%x:%x:%x\n",
                                 pFsbFipSessEntry->ENodeMacAddr[0],
                                 pFsbFipSessEntry->ENodeMacAddr[1],
                                 pFsbFipSessEntry->ENodeMacAddr[2],
                                 pFsbFipSessEntry->ENodeMacAddr[3],
                                 pFsbFipSessEntry->ENodeMacAddr[4],
                                 pFsbFipSessEntry->ENodeMacAddr[5]);
                return FSB_FAILURE;
            }
            break;

        case FSB_FIP_NPIV_FDISC_REJECT:
            /* When FDISC Reject from FCF is received, previous state must be
             * FDISC Request */
            if (pFsbFipSessEntry->u1State != FSB_FIP_NPIV_FDISC_REQUEST)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbHandleFIPSession:"
                                 " Invalid State - Received Packet Type:%s Context:%d"
                                 " VLAN:%d ENode IfIndex:%d Current State:%s \n",
                                 FsbErrString[pFSBPktInfo->PacketType],
                                 pFsbFipSessEntry->u4ContextId,
                                 pFsbFipSessEntry->u2VlanId,
                                 pFsbFipSessEntry->u4ENodeIfIndex,
                                 FsbErrString[pFsbFipSessEntry->u1State]);
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "ENode Mac Address: " "%x:%x:%x:%x:%x:%x\n",
                                 pFsbFipSessEntry->ENodeMacAddr[0],
                                 pFsbFipSessEntry->ENodeMacAddr[1],
                                 pFsbFipSessEntry->ENodeMacAddr[2],
                                 pFsbFipSessEntry->ENodeMacAddr[3],
                                 pFsbFipSessEntry->ENodeMacAddr[4],
                                 pFsbFipSessEntry->ENodeMacAddr[5]);
                return FSB_FAILURE;
            }
            pFsbFipSessEntry->u1State = FSB_FIP_FLOGI_ACCEPT;
            break;
        case FSB_FIP_LOGO_REQUEST:
            /* When FLOGO Request from ENode is received, previous state must be
             * either FLOGI Accept or FDISC Accept */
            if ((pFsbFipSessEntry->u1State != FSB_FIP_FLOGI_ACCEPT) &&
                (pFsbFipSessEntry->u1State != FSB_FIP_NPIV_FDISC_ACCEPT) &&
                (pFsbFipSessEntry->u1State != FSB_FIP_LOGO_REQUEST))
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbHandleFIPSession:"
                                 " Invalid State - Received Packet Type:%s Context:%d"
                                 " VLAN:%d ENode IfIndex:%d Current State:%s \n",
                                 FsbErrString[pFSBPktInfo->PacketType],
                                 pFsbFipSessEntry->u4ContextId,
                                 pFsbFipSessEntry->u2VlanId,
                                 pFsbFipSessEntry->u4ENodeIfIndex,
                                 FsbErrString[pFsbFipSessEntry->u1State]);
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "ENode Mac Address: " "%x:%x:%x:%x:%x:%x\n",
                                 pFsbFipSessEntry->ENodeMacAddr[0],
                                 pFsbFipSessEntry->ENodeMacAddr[1],
                                 pFsbFipSessEntry->ENodeMacAddr[2],
                                 pFsbFipSessEntry->ENodeMacAddr[3],
                                 pFsbFipSessEntry->ENodeMacAddr[4],
                                 pFsbFipSessEntry->ENodeMacAddr[5]);
                return FSB_FAILURE;
            }
            pFsbFipSessEntry->u1State = pFSBPktInfo->PacketType;
            break;

        case FSB_FIP_LOGO_ACCEPT:
            /* When LOGO Accept from FCF is received, previous state must be
             * LOGO Request */
            if (pFsbFipSessEntry->u1State == FSB_FIP_LOGO_REQUEST)
            {
                /* Delete FIP Session Entry, FCoE Entry and Remove respective 
                 * filters for that session */
                FsbFipSessEntry.u2VlanId = pFsbFipSessEntry->u2VlanId;
                FSB_MEMCPY ((FsbFipSessEntry.ENodeMacAddr),
                            pFsbFipSessEntry->ENodeMacAddr, MAC_ADDR_LEN);

                if (FsbHandleFLOGOAccept (pFsbFipSessEntry, pFSBPktInfo) !=
                    FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                     FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbHandleFIPSession:"
                                     " Handling FLOGO Accept message received on"
                                     " Vlan: %d with IfIndex: %d failed\r\n",
                                     pFSBPktInfo->u2VlanId,
                                     pFSBPktInfo->u4IfIndex);
                    return FSB_FAILURE;
                }

                /* Get the Interface name for the EnodeIfIndex to display in FSB trace */
                if (CfaCliGetIfName (pFSBPktInfo->u4IfIndex, (INT1 *) au1IfName)
                    == CLI_FAILURE)
                {
                    FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                     FSB_ERROR_LEVEL, FSB_NONE,
                                     "Failed to get the interface name for IfIndex - %d during "
                                     "FIP session log out\n",
                                     pFSBPktInfo->u4IfIndex);
                }

                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_CRITICAL_LEVEL,
                                 FSB_NONE,
                                 "FIP Session Logged out successfully in Context id: %d,VLAN Id: %d, Enode Interface name: [%s],"
                                 "ENode If Index: %d, ENode Mac Addr:%x:%x:%x:%x:%x:%x",
                                 pFSBPktInfo->u4ContextId,
                                 pFSBPktInfo->u2VlanId, au1IfName,
                                 pFSBPktInfo->u4IfIndex,
                                 pFSBPktInfo->SrcAddr[0],
                                 pFSBPktInfo->SrcAddr[1],
                                 pFSBPktInfo->SrcAddr[2],
                                 pFSBPktInfo->SrcAddr[3],
                                 pFSBPktInfo->SrcAddr[4],
                                 pFSBPktInfo->SrcAddr[5]);
#ifdef SYSLOG_WANTED
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gFsbGlobals.u4SyslogId,
                              "\nFIP Session Logged out successfully in Context Id: %d,VLAN Id: %d, ENode Intf: %s, "
                              "ENode Mac Addr:%x:%x:%x:%x:%x:%x",
                              pFSBPktInfo->u4ContextId,
                              pFSBPktInfo->u2VlanId,
                              au1IfName,
                              pFSBPktInfo->SrcAddr[0],
                              pFSBPktInfo->SrcAddr[1],
                              pFSBPktInfo->SrcAddr[2],
                              pFSBPktInfo->SrcAddr[3],
                              pFSBPktInfo->SrcAddr[4],
                              pFSBPktInfo->SrcAddr[5]));
#endif
                /* Sync-Up FIP Session Entry to Stand-By */
                if (FsbRedSyncUpFIPSessEntry (&FsbFipSessEntry,
                                              pFSBPktInfo) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                     FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbHandleFIPSession: FsbRedSyncUpFIPSessEntry with VlanId: %d"
                                     " and IfIndex: %d failed\r\n",
                                     pFSBPktInfo->u2VlanId,
                                     pFSBPktInfo->u4IfIndex);;
                    return FSB_FAILURE;
                }
                else
                {
                    return FSB_SUCCESS;
                }
            }
            else
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbHandleFIPSession:"
                                 " Invalid State - Received Packet Type:%s Context:%d"
                                 " VLAN:%d ENode IfIndex:%d Current State:%s \n",
                                 FsbErrString[pFSBPktInfo->PacketType],
                                 pFsbFipSessEntry->u4ContextId,
                                 pFsbFipSessEntry->u2VlanId,
                                 pFsbFipSessEntry->u4ENodeIfIndex,
                                 FsbErrString[pFsbFipSessEntry->u1State]);
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "ENode Mac Address: " "%x:%x:%x:%x:%x:%x\n",
                                 pFsbFipSessEntry->ENodeMacAddr[0],
                                 pFsbFipSessEntry->ENodeMacAddr[1],
                                 pFsbFipSessEntry->ENodeMacAddr[2],
                                 pFsbFipSessEntry->ENodeMacAddr[3],
                                 pFsbFipSessEntry->ENodeMacAddr[4],
                                 pFsbFipSessEntry->ENodeMacAddr[5]);
                return FSB_FAILURE;
            }
            break;

            /* Intentional fall through - Since both ENode and VN_Port Keep Alive
             * messages are handled similarly */
        case FSB_FIP_ENODE_KEEP_ALIVE:
        case FSB_FIP_VNPORT_KEEP_ALIVE:
            /* ENode/VNPORT Keep Alive from ENode will be received only after 
             * successful completion of FLOGI/FDISC Accept */
            if ((pFsbFipSessEntry->u1State == FSB_FIP_FLOGI_ACCEPT) ||
                (pFsbFipSessEntry->u1State == FSB_FIP_NPIV_FDISC_ACCEPT) ||
                (pFsbFipSessEntry->u1State == FSB_FIP_NPIV_FDISC_REQUEST))
            {
                if (FsbHandleKeepAlive
                    (pFsbFipSessEntry, pFSBPktInfo) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                     FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbHandleFIPSession:"
                                     " Hnadling Keep Alive meeasge received on Vlan: %d"
                                     " with IfIndex: %d failed\r\n",
                                     pFSBPktInfo->u2VlanId,
                                     pFSBPktInfo->u4IfIndex);
                    return FSB_FAILURE;
                }
            }
            else
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbHandleFIPSession:"
                                 " Invalid State - Received Packet Type:%s Context:%d"
                                 " VLAN:%d ENode IfIndex:%d Current State:%s \n",
                                 FsbErrString[pFSBPktInfo->PacketType],
                                 pFsbFipSessEntry->u4ContextId,
                                 pFsbFipSessEntry->u2VlanId,
                                 pFsbFipSessEntry->u4ENodeIfIndex,
                                 FsbErrString[pFsbFipSessEntry->u1State]);
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "ENode Mac Address: " "%x:%x:%x:%x:%x:%x\n",
                                 pFsbFipSessEntry->ENodeMacAddr[0],
                                 pFsbFipSessEntry->ENodeMacAddr[1],
                                 pFsbFipSessEntry->ENodeMacAddr[2],
                                 pFsbFipSessEntry->ENodeMacAddr[3],
                                 pFsbFipSessEntry->ENodeMacAddr[4],
                                 pFsbFipSessEntry->ENodeMacAddr[5]);
                return FSB_FAILURE;
            }
            break;

        case FSB_FIP_CLEAR_LINK:
            /* When Clear Link message from FCF is received, stop all timer which are all
             * running (Establishment timer and House keeping timer) and delete all filters 
             * specific to that session and also remove FIP Session Entry */

            FsbFipSessEntry.u2VlanId = pFsbFipSessEntry->u2VlanId;
            FSB_MEMCPY ((FsbFipSessEntry.ENodeMacAddr),
                        pFsbFipSessEntry->ENodeMacAddr, MAC_ADDR_LEN);
            if (FsbHandleClearLink (pFsbFipSessEntry, pFSBPktInfo) !=
                FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbHandleFIPSession:"
                                 " Clear Virtual Link message received on Vlan: %d"
                                 " with IfIndex: %d failed\r\n",
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
                return FSB_FAILURE;
            }
            /* Sync-Up FIP Session Entry to Stand-By */
            if (FsbRedSyncUpFIPSessEntry (&FsbFipSessEntry,
                                          pFSBPktInfo) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "FsbHandleFIPSession: FsbRedSyncUpFIPSessEntry with VlanId: %d"
                                 " and IfIndex: %d failed\r\n",
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
                return FSB_FAILURE;
            }
            else
            {
                return FSB_SUCCESS;
            }
            break;

        case FSB_FIP_LOGO_REJECT:

            /* When LOGO Reject from FCF is received, previous state must be
             * LOGO Request */
            if (pFsbFipSessEntry->u1State == FSB_FIP_LOGO_REQUEST)
            {
                pFsbFipSessEntry->u1State = FSB_FIP_FLOGI_ACCEPT;
            }
            else
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbHandleFIPSession:"
                                 " Invalid State - Received Packet Type:%s Context:%d"
                                 " VLAN:%d ENode IfIndex:%d Current State:%s \n",
                                 FsbErrString[pFSBPktInfo->PacketType],
                                 pFsbFipSessEntry->u4ContextId,
                                 pFsbFipSessEntry->u2VlanId,
                                 pFsbFipSessEntry->u4ENodeIfIndex,
                                 FsbErrString[pFsbFipSessEntry->u1State]);
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "ENode Mac Address: " "%x:%x:%x:%x:%x:%x\n",
                                 pFsbFipSessEntry->ENodeMacAddr[0],
                                 pFsbFipSessEntry->ENodeMacAddr[1],
                                 pFsbFipSessEntry->ENodeMacAddr[2],
                                 pFsbFipSessEntry->ENodeMacAddr[3],
                                 pFsbFipSessEntry->ENodeMacAddr[4],
                                 pFsbFipSessEntry->ENodeMacAddr[5]);
                return FSB_FAILURE;
            }
            break;

            /* Below cases are left unimplemented since,
             * 1.FSB_FIP_ADV_MCAST messgae is handled by FsbHandleFcfDiscovery
             * 2.FSB_FIP_VLAN_DISCOVERY & FSB_FIP_VLAN_NOTIFICATION messages are
             *   handle by FsbHandleFIPVlanPackets
             * 3.FSB_FIP_INITIAL_STATE specifies the initial state */
        case FSB_FIP_INITIAL_STATE:
        case FSB_FIP_VLAN_DISCOVERY:
        case FSB_FIP_VLAN_NOTIFICATION:
        case FSB_FIP_ADV_MCAST:
            break;

        default:
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE, "FsbHandleFIPSession:"
                             " Invalid FIP Message \r\n");
            break;
    }
    /* Sync-Up FIP Session Entry to Stand-By */
    if (FsbRedSyncUpFIPSessEntry (pFsbFipSessEntry, pFSBPktInfo) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                         FSB_NONE,
                         "FsbHandleFIPSession: FsbRedSyncUpFIPSessEntry with VlanId: %d"
                         " and IfIndex: %d failed\r\n", pFSBPktInfo->u2VlanId,
                         pFSBPktInfo->u4IfIndex);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbGetFIPSessionInfo                             */
/*                                                                           */
/*    Description         : This function is used to create a new FIP        */
/*                          Session Entry or get pointer to the already      */
/*                          existing FIP Session Entry                       */
/*                                                                           */
/*    Input(s)            : pFSBPktInfo - Pointer to FSB Packet Information  */
/*                                                                           */
/*    Output(s)           : pFsbFipSessEntry - Pointer to FIP Session Entry  */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
tFsbFipSessEntry   *
FsbGetFIPSessionInfo (tFSBPktInfo * pFSBPktInfo)
{
    tFsbFipSessEntry   *pFsbFipSessEntry = NULL;
    tFsbFcfEntry       *pFsbFcfEntry = NULL;
    tFsbFipSessEntry    FsbFipSessEntry;
    UINT1               au1RBName[OSIX_NAME_LEN + 4];
    UINT4               u4Index = 0;

    FSB_MEMSET (au1RBName, '\0', OSIX_NAME_LEN + 4);
    FSB_MEMSET (&FsbFipSessEntry, 0, sizeof (tFsbFipSessEntry));

    switch (pFSBPktInfo->PacketType)
    {
            /* Packet from ENode contain ENodeIfIndex and SrcAddr as ENodeMacAddr 
             * Create a FIP Session with ENodeIfIndex, VLANId and ENodeMacAddr */
        case FSB_FIP_SOLICIT_UCAST:
        case FSB_FIP_SOLICIT_MCAST:
            if (gFsbGlobals.gu4FsbFIPSessionCount == MAX_FSB_FCOE_ENTRIES)
            {
                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbGetFIPSessionInfo"
                                 "with VlanId: %d and IfIndex: %d: Exceeds Maximum "
                                 "Number of FIP Sessions\r\n",
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
                return NULL;
            }
            FsbFipSessEntry.u4ENodeIfIndex = pFSBPktInfo->u4IfIndex;
            FsbFipSessEntry.u2VlanId = pFSBPktInfo->u2VlanId;
            FSB_MEMCPY (FsbFipSessEntry.ENodeMacAddr, pFSBPktInfo->SrcAddr,
                        MAC_ADDR_LEN);

            pFsbFipSessEntry = (tFsbFipSessEntry *)
                RBTreeGet (gFsbGlobals.FsbFipSessTable,
                           (tRBElem *) & FsbFipSessEntry);
            if (pFsbFipSessEntry != NULL)
            {
                if (pFsbFipSessEntry->u1State == FSB_FIP_FLOGI_ACCEPT)
                {
                    /* Special case handling in which server sends solicitation, when in FLOGI_ACCEPT case */
                    return pFsbFipSessEntry;

                }
                pFsbFipSessEntry->u1DeleteReasonCode =
                    FSB_REASON_CODE_SESS_REESTABLISHMENT_FROM_SERVER;
                if (FsbClearFIPSession (pFsbFipSessEntry) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                     FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbGetFIPSessionInfo"
                                     "with VlanId: %d and IfIndex: %d: FsbClearFIPSession failed\r\n",
                                     pFSBPktInfo->u2VlanId,
                                     pFSBPktInfo->u4IfIndex);
                    return NULL;
                }
            }

            pFsbFipSessEntry = (tFsbFipSessEntry *)
                MemAllocMemBlk (FSB_SESSION_ENTRIES_MEMPOOL_ID);
            if (pFsbFipSessEntry == NULL)
            {
                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                 FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                                 "FsbGetFIPSessionInfo: Memory Allocation Failed \r\n");
                return NULL;
            }

            FSB_MEMSET (pFsbFipSessEntry, 0, sizeof (tFsbFipSessEntry));

            pFsbFipSessEntry->u4ENodeIfIndex = pFSBPktInfo->u4IfIndex;
            pFsbFipSessEntry->u2VlanId = pFSBPktInfo->u2VlanId;
            FSB_MEMCPY (pFsbFipSessEntry->ENodeMacAddr, pFSBPktInfo->SrcAddr,
                        MAC_ADDR_LEN);
            pFsbFipSessEntry->u4ContextId = pFSBPktInfo->u4ContextId;
            pFsbFipSessEntry->u1State = FSB_FIP_INITIAL_STATE;
            pFsbFipSessEntry->u1IssuMaintenanceMode = FSB_FALSE;

            /* Get the Free Index */
            FsbGetFreeIndex (gFsbGlobals.SessFilterEntryList, &u4Index);
            pFsbFipSessEntry->u4Index = u4Index;

            SNPRINTF ((CHR1 *) au1RBName, OSIX_NAME_LEN + 1, "%s%d",
                      FSB_SESSION_RB_PREFIX_NAME, pFsbFipSessEntry->u4Index);

            pFsbFipSessEntry->FsbSessFilterEntry =
                RBTreeCreateEmbeddedExtended ((FSAP_OFFSETOF (tFsbFilterEntry,
                                                              FsbFilterNode)),
                                              FsbCompareSessFilterEntryIndex,
                                              (UINT1 *) au1RBName);

            if (pFsbFipSessEntry->FsbSessFilterEntry == NULL)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbGetFIPSessionInfo: FSB_FIP_SOLICIT_MCAST case, Session Entry RB count: %d,"
                                 "FCoE Entry RB count: %d, FIP Snooping Entry RB Count: %d\n",
                                 gFsbGlobals.u4FsbSessFilterEntryRBCount,
                                 gFsbGlobals.u4FsbFCoEFilterEntryRBCount,
                                 gFsbGlobals.u4FsbFilterEntryRBCount);

                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbGetFIPSessionInfo:RBTreeCreateEmbeddedExtended for tFsbFilterEntry"
                                 " with VlanId: %d, IfIndex: %d, RB name - %s failed \r\n",
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex,
                                 au1RBName);
                FsbResetFreeIndex (gFsbGlobals.SessFilterEntryList,
                                   pFsbFipSessEntry->u4Index);
                MemReleaseMemBlock (FSB_SESSION_ENTRIES_MEMPOOL_ID,
                                    (UINT1 *) pFsbFipSessEntry);
                return NULL;
            }
            gFsbGlobals.u4FsbSessFilterEntryRBCount++;
            if (RBTreeAdd (gFsbGlobals.FsbFipSessTable,
                           (tRBElem *) pFsbFipSessEntry) == RB_FAILURE)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbGetFIPSessionInfo: RBTree Add of FsbFipSessEntry"
                                 " with VlanId: %d and IfIndex: %d failed \r\n",
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
                FsbResetFreeIndex (gFsbGlobals.SessFilterEntryList,
                                   pFsbFipSessEntry->u4Index);
                MemReleaseMemBlock (FSB_SESSION_ENTRIES_MEMPOOL_ID,
                                    (UINT1 *) pFsbFipSessEntry);
                return NULL;
            }
            /* Start the ESTABLISHMENT TIMER */
            if (FsbTmrStartTimer (&(pFsbFipSessEntry->FsbSessEstablishmentTmr),
                                  FSB_SESS_ESTABLISHMENT_TIMER_ID,
                                  FSB_SESS_ESTABLISHMENT_TMR_VALUE,
                                  pFsbFipSessEntry) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "FsbGetFIPSessionInfo: FsbTmrStartTimer of FsbFipSessEntry"
                                 " with VlanId: %d and IfIndex: %d failed\r\n",
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
                return NULL;

            }
            break;

            /* Packet from ENode contain ENodeIfIndex, VLANId, ENodeMacAddr.
             * Using the index fetch and return pointer to FIP Session Entry */
        case FSB_FIP_LOGO_REQUEST:
        case FSB_FIP_NPIV_FDISC_REQUEST:
        case FSB_FIP_ENODE_KEEP_ALIVE:
            FsbFipSessEntry.u4ENodeIfIndex = pFSBPktInfo->u4IfIndex;
            FsbFipSessEntry.u2VlanId = pFSBPktInfo->u2VlanId;
            FSB_MEMCPY (FsbFipSessEntry.ENodeMacAddr, pFSBPktInfo->SrcAddr,
                        MAC_ADDR_LEN);

            pFsbFipSessEntry = (tFsbFipSessEntry *)
                RBTreeGet (gFsbGlobals.FsbFipSessTable,
                           (tRBElem *) & FsbFipSessEntry);
            if (pFsbFipSessEntry == NULL)
            {
                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "FsbGetFIPSessionInfo: RBTree Get of FsbFipSessEntry"
                                 "for the packet: %s received on Vlan: %d with IfIndex: %d "
                                 "Failed \r\n",
                                 FsbErrString[pFSBPktInfo->PacketType],
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
                return NULL;
            }
            break;

        case FSB_FIP_FLOGI_REQUEST:
            FsbFipSessEntry.u4ENodeIfIndex = pFSBPktInfo->u4IfIndex;
            FsbFipSessEntry.u2VlanId = pFSBPktInfo->u2VlanId;
            FSB_MEMCPY (FsbFipSessEntry.ENodeMacAddr, pFSBPktInfo->SrcAddr,
                        MAC_ADDR_LEN);

            pFsbFipSessEntry = (tFsbFipSessEntry *)
                RBTreeGet (gFsbGlobals.FsbFipSessTable,
                           (tRBElem *) & FsbFipSessEntry);
            if (pFsbFipSessEntry != NULL)
            {
                if ((pFsbFipSessEntry->u1State == pFSBPktInfo->PacketType)
                    || (pFsbFipSessEntry->u1State == FSB_FIP_FLOGI_ACCEPT)
                    || (pFsbFipSessEntry->u1State == FSB_FIP_SOLICIT_MCAST)
                    || (pFsbFipSessEntry->u1State == FSB_FIP_SOLICIT_UCAST))
                {
                    pFsbFipSessEntry->u1DeleteReasonCode =
                        FSB_REASON_CODE_SESS_REESTABLISHMENT_FROM_SERVER;
                    if (FsbClearFIPSession (pFsbFipSessEntry) != FSB_SUCCESS)
                    {
                        FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                         FSB_ERROR_LEVEL, FSB_NONE,
                                         "FsbGetFIPSessionInfo"
                                         "with VlanId: %d and IfIndex: %d: FsbClearFIPSession failed\r\n",
                                         pFSBPktInfo->u2VlanId,
                                         pFSBPktInfo->u4IfIndex);
                        return NULL;
                    }
                }
                else
                {
                    /* FIP Session already exists. Hence no need to create new entry */
                    return pFsbFipSessEntry;
                }
            }
            if (gFsbGlobals.gu4FsbFIPSessionCount == MAX_FSB_FCOE_ENTRIES)
            {
                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbGetFIPSessionInfo"
                                 "with VlanId: %d and IfIndex: %d: Exceeds Maximum "
                                 "Number of FIP Sessions\r\n",
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
                return NULL;
            }

            pFsbFipSessEntry = (tFsbFipSessEntry *)
                MemAllocMemBlk (FSB_SESSION_ENTRIES_MEMPOOL_ID);
            if (pFsbFipSessEntry == NULL)
            {
                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbGetFIPSessionInfo: Memory Allocation Failed \r\n");
                return NULL;
            }

            FSB_MEMSET (pFsbFipSessEntry, 0, sizeof (tFsbFipSessEntry));

            pFsbFipSessEntry->u4ENodeIfIndex = pFSBPktInfo->u4IfIndex;
            pFsbFipSessEntry->u2VlanId = pFSBPktInfo->u2VlanId;
            FSB_MEMCPY (pFsbFipSessEntry->ENodeMacAddr, pFSBPktInfo->SrcAddr,
                        MAC_ADDR_LEN);
            pFsbFipSessEntry->u4ContextId = pFSBPktInfo->u4ContextId;
            pFsbFipSessEntry->u1State = FSB_FIP_INITIAL_STATE;
            pFsbFipSessEntry->u1IssuMaintenanceMode = FSB_FALSE;
            pFsbFipSessEntry->u1HouseKeepingTimerFlag = FSB_TRUE;

            pFsbFcfEntry =
                FsbGetFcfInfo (pFSBPktInfo->u2VlanId, pFSBPktInfo->DestAddr);
            if (pFsbFcfEntry == NULL)
            {
                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "FsbGetFIPSessionInfo: Failed to get Fcf Entry with"
                                 "Vlan Id: %d and FcfMac: %02x:%02x:%02x:%02x:%02x:%02x\r\n",
                                 pFSBPktInfo->u2VlanId,
                                 pFSBPktInfo->DestAddr[0],
                                 pFSBPktInfo->DestAddr[1],
                                 pFSBPktInfo->DestAddr[2],
                                 pFSBPktInfo->DestAddr[3],
                                 pFSBPktInfo->DestAddr[4],
                                 pFSBPktInfo->DestAddr[5]);
                MemReleaseMemBlock (FSB_SESSION_ENTRIES_MEMPOOL_ID,
                                    (UINT1 *) pFsbFipSessEntry);
                return NULL;
            }
            FSB_MEMCPY (pFsbFipSessEntry->au1FcMap, pFsbFcfEntry->au1FcMap,
                        FSB_FCMAP_LEN);
            FSB_MEMCPY (pFsbFipSessEntry->au1NameId, pFsbFcfEntry->au1NameId,
                        FSB_NAME_ID_LEN);
            pFsbFipSessEntry->u4FcfIfIndex = pFsbFcfEntry->u4FcfIfIndex;
            if (pFsbFcfEntry->u2DbitFlag & FSB_DBIT_MASK)
            {
                pFsbFipSessEntry->u1HouseKeepingTimerFlag = FSB_FALSE;
            }
            else
            {
                pFsbFipSessEntry->u1HouseKeepingTimerFlag = FSB_TRUE;
            }

            /* Get the Free Index */
            FsbGetFreeIndex (gFsbGlobals.SessFilterEntryList, &u4Index);
            pFsbFipSessEntry->u4Index = u4Index;

            SNPRINTF ((CHR1 *) au1RBName, OSIX_NAME_LEN + 1, "%s%d",
                      FSB_SESSION_RB_PREFIX_NAME, pFsbFipSessEntry->u4Index);

            pFsbFipSessEntry->FsbSessFilterEntry =
                RBTreeCreateEmbeddedExtended ((FSAP_OFFSETOF (tFsbFilterEntry,
                                                              FsbFilterNode)),
                                              FsbCompareSessFilterEntryIndex,
                                              (UINT1 *) au1RBName);

            if (pFsbFipSessEntry->FsbSessFilterEntry == NULL)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbGetFIPSessionInfo: FLOGI_REQUEST case, Session Entry RB count: %d,"
                                 "FCoE Entry RB count: %d, FIP Snooping Entry RB Count: %d\n",
                                 gFsbGlobals.u4FsbSessFilterEntryRBCount,
                                 gFsbGlobals.u4FsbFCoEFilterEntryRBCount,
                                 gFsbGlobals.u4FsbFilterEntryRBCount);
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbGetFIPSessionInfo:RBTreeCreateEmbeddedExtended for tFsbFilterEntry"
                                 " with VlanId: %d, IfIndex: %d, RB name - %s failed \r\n",
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex,
                                 au1RBName);
                FsbResetFreeIndex (gFsbGlobals.SessFilterEntryList,
                                   pFsbFipSessEntry->u4Index);
                MemReleaseMemBlock (FSB_SESSION_ENTRIES_MEMPOOL_ID,
                                    (UINT1 *) pFsbFipSessEntry);
                return NULL;
            }
            gFsbGlobals.u4FsbSessFilterEntryRBCount++;

            if (RBTreeAdd (gFsbGlobals.FsbFipSessTable,
                           (tRBElem *) pFsbFipSessEntry) == RB_FAILURE)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbGetFIPSessionInfo: RBTree Add of FsbFipSessEntry"
                                 " with VlanId: %d and IfIndex: %d failed \r\n",
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
                FsbResetFreeIndex (gFsbGlobals.SessFilterEntryList,
                                   pFsbFipSessEntry->u4Index);
                MemReleaseMemBlock (FSB_SESSION_ENTRIES_MEMPOOL_ID,
                                    (UINT1 *) pFsbFipSessEntry);
                return NULL;
            }
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                             FSB_NONE,
                             "FsbGetFIPSessionInfo: Session Establishment timer started "
                             " for VlanId: %d and IfIndex: %d failed\r\n",
                             pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
            /* Start the ESTABLISHMENT TIMER */
            if (FsbTmrStartTimer (&(pFsbFipSessEntry->FsbSessEstablishmentTmr),
                                  FSB_SESS_ESTABLISHMENT_TIMER_ID,
                                  FSB_SESS_ESTABLISHMENT_TMR_VALUE,
                                  pFsbFipSessEntry) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "FsbGetFIPSessionInfo: FsbTmrStartTimer of FsbFipSessEntry"
                                 " with VlanId: %d and IfIndex: %d failed\r\n",
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
                return NULL;

            }
            break;
        case FSB_FIP_VNPORT_KEEP_ALIVE:
            FsbFipSessEntry.u4ENodeIfIndex = pFSBPktInfo->u4IfIndex;
            FsbFipSessEntry.u2VlanId = pFSBPktInfo->u2VlanId;
            FSB_MEMCPY (FsbFipSessEntry.ENodeMacAddr,
                        pFSBPktInfo->FsbFipsPktFields.FIPVNKeepAlive.
                        ENodeMacAddr, MAC_ADDR_LEN);
            pFsbFipSessEntry =
                (tFsbFipSessEntry *) RBTreeGet (gFsbGlobals.FsbFipSessTable,
                                                (tRBElem *) & FsbFipSessEntry);
            if (pFsbFipSessEntry == NULL)
            {
                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "FsbGetFIPSessionInfo: RBTree Get of FsbFipSessEntry"
                                 "for the packet: %s received on Vlan: %d with IfIndex: %d "
                                 "Failed \r\n",
                                 FsbErrString[pFSBPktInfo->PacketType],
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
                return NULL;
            }
            break;

            /* Packet from FCF contain VLANId and DestAddr as ENodeMacAddr 
             * and as ENodeIfIndex as 0 fetch FIP Session Entry. If VLANId and
             * ENodeMAC Address matches return pointer of that FIP Session Entry */
        case FSB_FIP_ADV_UCAST:
        case FSB_FIP_FLOGI_ACCEPT:
        case FSB_FIP_CLEAR_LINK:
        case FSB_FIP_LOGO_ACCEPT:
        case FSB_FIP_FLOGI_REJECT:
        case FSB_FIP_NPIV_FDISC_ACCEPT:
        case FSB_FIP_NPIV_FDISC_REJECT:
        case FSB_FIP_LOGO_REJECT:
            FsbFipSessEntry.u4ENodeIfIndex = 0;
            FsbFipSessEntry.u2VlanId = pFSBPktInfo->u2VlanId;
            FSB_MEMCPY (FsbFipSessEntry.ENodeMacAddr, pFSBPktInfo->DestAddr,
                        MAC_ADDR_LEN);
            while ((pFsbFipSessEntry = (tFsbFipSessEntry *)
                    RBTreeGetNext (gFsbGlobals.FsbFipSessTable,
                                   (tRBElem *) & FsbFipSessEntry,
                                   NULL)) != NULL)
            {
                if ((pFsbFipSessEntry->u2VlanId == pFSBPktInfo->u2VlanId) &&
                    (FSB_MEMCMP (pFsbFipSessEntry->ENodeMacAddr,
                                 pFSBPktInfo->DestAddr, MAC_ADDR_LEN) == 0))
                {
                    break;
                }
                FsbFipSessEntry.u4ENodeIfIndex =
                    pFsbFipSessEntry->u4ENodeIfIndex;
                FsbFipSessEntry.u2VlanId = pFsbFipSessEntry->u2VlanId;
                FSB_MEMCPY (FsbFipSessEntry.ENodeMacAddr,
                            pFsbFipSessEntry->ENodeMacAddr, MAC_ADDR_LEN);
            }
            break;
            /* Below cases are left unimplemented since,
             * 1.FSB_FIP_ADV_MCAST message is handled by FsbHandleFcfDiscovery
             * 2.FSB_FIP_VLAN_DISCOVERY & FSB_FIP_VLAN_NOTIFICATION messages are
             *   handle by FsbHandleFIPVlanPackets
             * 3.FSB_FIP_INITIAL_STATE specifies the initial state */
        case FSB_FIP_INITIAL_STATE:
        case FSB_FIP_VLAN_DISCOVERY:
        case FSB_FIP_VLAN_NOTIFICATION:
        case FSB_FIP_ADV_MCAST:
            break;
        default:
            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE, "FsbGetFIPSessionInfo:"
                             " Invalid FIP Message \r\n");
            break;

    }
    return pFsbFipSessEntry;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbGetFIPSessionFCoEInfo                         */
/*                                                                           */
/*    Description         : This function is used to create a new FIP        */
/*                          Session FCoE Entry                               */
/*                                                                           */
/*    Input(s)            : pFSBPktInfo - Pointer to FSB Packet Information  */
/*                          pFsbFipSessEntry - Pointer to FIP Session Entry  */
/*                                                                           */
/*    Output(s)           : pFsbFipSessFCoEEntry                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
tFsbFipSessFCoEEntry *
FsbGetFIPSessionFCoEInfo (tFSBPktInfo * pFSBPktInfo,
                          tFsbFipSessEntry * pFsbFipSessEntry)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;
    UINT4               u4Index = 0;
    UINT1               au1RBName[OSIX_NAME_LEN + 4];

    FSB_MEMSET (au1RBName, '\0', OSIX_NAME_LEN + 4);

    if (gFsbGlobals.gu4FsbFIPSessionCount == MAX_FSB_FCOE_ENTRIES)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE, "FsbGetFIPSessionFCoEInfo"
                         "with VlanId: %d and IfIndex: %d: Exceeds Maximum "
                         "Number of FIP Sessions\r\n",
                         pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
        return NULL;
    }
    pFsbFipSessFCoEEntry = (tFsbFipSessFCoEEntry *)
        MemAllocMemBlk (FSB_FCOE_ENTRIES_MEMPOOL_ID);

    if (pFsbFipSessFCoEEntry == NULL)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE, "FsbGetFIPSessionFCoEInfo: "
                         "Memory Allocation Failed \r\n");
        return NULL;
    }

    FSB_MEMSET (pFsbFipSessFCoEEntry, 0, sizeof (tFsbFipSessFCoEEntry));

    /* Index for FIP Session FCoE Entries (VlanId, ENodeIfIndex, FCoE
     * Mac Address, ENode Mac Address)*/
    pFsbFipSessFCoEEntry->u4ENodeIfIndex = pFsbFipSessEntry->u4ENodeIfIndex;
    pFsbFipSessFCoEEntry->u2VlanId = pFsbFipSessEntry->u2VlanId;
    FSB_MEMCPY (pFsbFipSessFCoEEntry->ENodeMacAddr,
                pFsbFipSessEntry->ENodeMacAddr, MAC_ADDR_LEN);
    FSB_MEMCPY (pFsbFipSessFCoEEntry->FcfMacAddr,
                pFSBPktInfo->SrcAddr, MAC_ADDR_LEN);
    FSB_MEMCPY (pFsbFipSessFCoEEntry->FCoEMacAddr,
                pFSBPktInfo->FsbFipsPktFields.FIPLogiInfo.FCoEMacAddr,
                MAC_ADDR_LEN);

    FSB_MEMCPY (pFsbFipSessFCoEEntry->au1FcfId,
                &(pFSBPktInfo->FsbFipsPktFields.
                  FIPLogiInfo.FCoEMacAddr[FSB_FCID_LEN]), FSB_FCID_LEN);
    pFsbFipSessFCoEEntry->u1HouseKeepingTimerFlag =
        pFsbFipSessEntry->u1HouseKeepingTimerFlag;
    if (pFSBPktInfo->PacketType == FSB_FIP_FLOGI_ACCEPT)
    {
        pFsbFipSessFCoEEntry->u1EnodeConnType = FSB_FIP_FLOGI;
    }
    else
    {
        pFsbFipSessFCoEEntry->u1EnodeConnType = FSB_FIP_FDISC;
    }
    pFsbFipSessFCoEEntry->u1IssuMaintenanceMode = FSB_FALSE;
    pFsbFipSessFCoEEntry->pFsbFipSessEntry = pFsbFipSessEntry;

    /* Get the Free Index */
    FsbGetFreeIndex (gFsbGlobals.FcoeFilterEntryList, &u4Index);
    pFsbFipSessFCoEEntry->u4Index = u4Index;

    SNPRINTF ((CHR1 *) au1RBName, OSIX_NAME_LEN + 1, "%s%d",
              FSB_FCOE_RB_PREFIX_NAME, pFsbFipSessFCoEEntry->u4Index);

    pFsbFipSessFCoEEntry->FsbSessFCoEFilterEntry =
        RBTreeCreateEmbeddedExtended ((FSAP_OFFSETOF (tFsbFilterEntry,
                                                      FsbFilterNode)),
                                      FsbCompareSessFilterEntryIndex,
                                      (UINT1 *) au1RBName);
    if (pFsbFipSessFCoEEntry->FsbSessFCoEFilterEntry == NULL)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                         FSB_CRITICAL_LEVEL, FSB_NONE,
                         "FsbGetFIPSessionFCoEInfo: FLOGI_ACCEPT case, Session Entry RB count: %d,"
                         "FCoE Entry RB count: %d, FIP Snooping Entry RB Count: %d\n",
                         gFsbGlobals.u4FsbSessFilterEntryRBCount,
                         gFsbGlobals.u4FsbFCoEFilterEntryRBCount,
                         gFsbGlobals.u4FsbFilterEntryRBCount);
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE, "FsbGetFIPSessionFCoEInfo: "
                         "RBTreeCreateEmbeddedExtended for tFsbFilterEntry "
                         "with Vlan: %d with IfIndex: %d, RB name - %s failed\r\n",
                         pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex,
                         au1RBName);
        FsbResetFreeIndex (gFsbGlobals.FcoeFilterEntryList,
                           pFsbFipSessFCoEEntry->u4Index);
        MemReleaseMemBlock (FSB_FCOE_ENTRIES_MEMPOOL_ID,
                            (UINT1 *) pFsbFipSessFCoEEntry);
        return NULL;
    }
    gFsbGlobals.u4FsbFCoEFilterEntryRBCount++;
    if (RBTreeAdd (gFsbGlobals.FsbFipSessFCoETable,
                   (tRBElem *) pFsbFipSessFCoEEntry) == RB_FAILURE)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE, "FsbGetFIPSessionFCoEInfo: "
                         "RBTree Add of FsbFipSessFCoEEntry for the packet: %s "
                         "received on Vlan: %d with IfIndex: %d failed\r\n",
                         FsbErrString[pFSBPktInfo->PacketType],
                         pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
        FsbResetFreeIndex (gFsbGlobals.FcoeFilterEntryList,
                           pFsbFipSessFCoEEntry->u4Index);
        MemReleaseMemBlock (FSB_FCOE_ENTRIES_MEMPOOL_ID,
                            (UINT1 *) pFsbFipSessFCoEEntry);
        return NULL;
    }
    pFsbFipSessEntry->u4FCoECount++;
    return pFsbFipSessFCoEEntry;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbGetFIPSessionEntry                            */
/*                                                                           */
/*    Description         : This function is used to get pointer to          */
/*                          FIP SEssion Entry                                */
/*                                                                           */
/*    Input(s)            : u2VlanIndex     - VLAN Index                     */
/*                          u4IfIndex       - If Index                       */
/*                          ENodeMacAddress - ENode Mac Address              */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : pointer to tFsbFipSessEntry                      */
/*                                                                           */
/*****************************************************************************/
tFsbFipSessEntry   *
FsbGetFIPSessionEntry (UINT2 u2VlanIndex, UINT4 u4IfIndex,
                       tMacAddr ENodeMacAddr)
{
    tFsbFipSessEntry   *pFsbFipSessEntry;
    tFsbFipSessEntry    FsbFipSessEntry;

    FSB_MEMSET (&FsbFipSessEntry, 0, sizeof (tFsbFipSessEntry));

    /* Using the index (VLANId, ENodeIfIndex and ENodeMACAddress) fetch the
     * correponding FIP Session Entry and return the pointer */
    FsbFipSessEntry.u2VlanId = u2VlanIndex;
    FsbFipSessEntry.u4ENodeIfIndex = u4IfIndex;
    FSB_MEMCPY (&FsbFipSessEntry.ENodeMacAddr, ENodeMacAddr, MAC_ADDR_LEN);

    pFsbFipSessEntry = RBTreeGet (gFsbGlobals.FsbFipSessTable,
                                  (tRBElem *) & FsbFipSessEntry);

    if (pFsbFipSessEntry == NULL)
    {
        return NULL;
    }
    return pFsbFipSessEntry;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbGetFIPSessionFCoEEntry                        */
/*                                                                           */
/*    Description         : This function is used to get pointer to          */
/*                          FIP SEssion Entry                                */
/*                                                                           */
/*    Input(s)            : u2VlanIndex     - VLAN Index                     */
/*                          u4IfIndex       - If Index                       */
/*                          ENodeMacAddress - ENode Mac Address              */
/*                          FCoEMacAddress  - FCoE Mac Address               */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : pointer to tFsbFipSessFCoEEntry                  */
/*                                                                           */
/*****************************************************************************/
tFsbFipSessFCoEEntry *
FsbGetFIPSessionFCoEEntry (UINT2 u2VlanIndex, UINT4 u4IfIndex,
                           tMacAddr ENodeMacAddr, tMacAddr FcfMacAddr,
                           tMacAddr FCoEMacAddr)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;
    tFsbFipSessFCoEEntry FsbFipSessFCoEEntry;

    FSB_MEMSET (&FsbFipSessFCoEEntry, 0, sizeof (tFsbFipSessFCoEEntry));

    /* Using the index (VLANId, ENodeIfIndex and ENodeMACAddress) fetch the
     * correponding FIP Session Entry and return the pointer */
    FsbFipSessFCoEEntry.u2VlanId = u2VlanIndex;
    FsbFipSessFCoEEntry.u4ENodeIfIndex = u4IfIndex;
    FSB_MEMCPY (&FsbFipSessFCoEEntry.ENodeMacAddr, ENodeMacAddr, MAC_ADDR_LEN);
    FSB_MEMCPY (&FsbFipSessFCoEEntry.FcfMacAddr, FcfMacAddr, MAC_ADDR_LEN);
    FSB_MEMCPY (&FsbFipSessFCoEEntry.FCoEMacAddr, FCoEMacAddr, MAC_ADDR_LEN);

    pFsbFipSessFCoEEntry = RBTreeGet (gFsbGlobals.FsbFipSessFCoETable,
                                      (tRBElem *) & FsbFipSessFCoEEntry);

    if (pFsbFipSessFCoEEntry == NULL)
    {
        return NULL;
    }
    return pFsbFipSessFCoEEntry;
}

/********************************************************************************/
/*                                                                              */
/*    Function Name       : FsbHandleFIPSolicit                                 */
/*                                                                              */
/*    Description         : In this function, for the received FIP Soliciation  */
/*                          message from the Enode, FSB modue installs a filter */
/*                          to accept next FIP Discovery Advertisement message  */
/*                          from the FCF. This function  is used to install     */
/*                          approriate filter to switch and copy to CPU,        */
/*                          the received FIP Discovery Advertisement Unicast    */
/*                          Message.  Also starts the Session Establishment     */
/*                          Timer for deleting the session entries, if the      */
/*                          session entries are half way held during the        */
/*                          E-Node Login process.                               */
/*                                                                              */
/*    Input(s)            : pFSBPktInfo - Pointer to FSB Packet Information     */
/*                          pFsbFipSessEntry - Pointer to FIP Session Entry     */
/*                                                                              */
/*    Output(s)           : None                                                */
/*                                                                              */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                            */
/*                                                                              */
/********************************************************************************/
INT4
FsbHandleFIPSolicit (tFSBPktInfo * pFSBPktInfo,
                     tFsbFipSessEntry * pFsbFipSessEntry)
{
    tFsbTrapInfo        FsbTrapInfo;
#ifdef SYSLOG_WANTED
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
#endif

    /* This function is invoked to handle both Discovery Solicitation Multicast and Unicast */
    FSB_MEMSET (&FsbTrapInfo, 0, sizeof (tFsbTrapInfo));
#ifdef SYSLOG_WANTED
    FSB_MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
#endif

    /* Below function call is to validate the MTU size received in the Packet */
    if (FsbValidateMtuSize (pFSBPktInfo) != FSB_SUCCESS)
    {
        /* Calling Trap function when received frame size 
         * is not in the allowed range */
        FsbTrapInfo.u4ContextId = pFsbFipSessEntry->u4ContextId;
        FsbTrapInfo.u4FsbSessionEnodeIfIndex = pFsbFipSessEntry->u4ENodeIfIndex;
        FSB_MEMCPY (FsbTrapInfo.FsbSessionEnodeMacAddress,
                    pFsbFipSessEntry->ENodeMacAddr, MAC_ADDR_LEN);
        FsbTrapInfo.u2FsbSessionVlanId = pFsbFipSessEntry->u2VlanId;
        FsbSnmpIfSendTrap (FSB_MTU_MISMATCH, &FsbTrapInfo);

#ifdef SYSLOG_WANTED
        if (CfaCliGetIfName
            (pFsbFipSessEntry->u4ENodeIfIndex,
             (INT1 *) au1IfName) == CLI_FAILURE)
        {
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                             FSB_NONE,
                             "Failed to get the Interface name for IfIndex - %d during MTU validation\n",
                             pFsbFipSessEntry->u4ENodeIfIndex);
        }
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gFsbGlobals.u4SyslogId,
                      "\nMTU mismatch for the session with Context id: %d,VLAN Id: %d, ENode Intf: %s, ENode Mac Addr:%x:%x:%x:%x:%x:%x",
                      pFsbFipSessEntry->u4ContextId,
                      pFsbFipSessEntry->u2VlanId,
                      au1IfName,
                      pFsbFipSessEntry->ENodeMacAddr[0],
                      pFsbFipSessEntry->ENodeMacAddr[1],
                      pFsbFipSessEntry->ENodeMacAddr[2],
                      pFsbFipSessEntry->ENodeMacAddr[3],
                      pFsbFipSessEntry->ENodeMacAddr[4],
                      pFsbFipSessEntry->ENodeMacAddr[5]));
#endif
        FsbUpdateVLANStatistics (pFsbFipSessEntry->u4ContextId,
                                 pFsbFipSessEntry->u2VlanId,
                                 FSB_MTU_MISMATCH_STATS);
        FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                         FSB_NONE,
                         "FsbHandleFIPSolicit with VlanId: %d and IfIndex:%d "
                         ": Validation of MTU size failed\r\n",
                         pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
        return FSB_FAILURE;
    }
    /* Below function call is used to populate the Filter Entry structure and  
     * call the wrap function to fill the Hw Filter Entry which inturn call NPAPI rountine
     * to install filter to allow Discovery Advertisement Unicast from FCF */
    if (FsbInstallFIPSessionFilter (pFSBPktInfo, pFsbFipSessEntry) !=
        FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbHandleFIPSolicit with VlanId: %d and IfIndex:%d "
                         ": Installing filter for FIP Session Failed \r\n",
                         pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
        return FSB_FAILURE;
    }

    /* Configure Enode Mac as Static Entry since Mac-learing is disabled 
     * for all FCoE VLAN */
    if (FsbConfigUcastEntry (pFsbFipSessEntry->u4ContextId,
                             pFsbFipSessEntry->ENodeMacAddr,
                             pFsbFipSessEntry->u2VlanId,
                             pFsbFipSessEntry->u4ENodeIfIndex,
                             VLAN_CREATE) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbHandleFIPSolicit with VlanId: %d and IfIndex:%d "
                         ": Configuring Static Enode Mac for "
                         "FIP Session Failed \r\n",
                         pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
        return FSB_FAILURE;
    }

    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbHandleFIPAdvUcast                             */
/*                                                                           */
/*    Description         : This function will update FIP Session Entry      */
/*                          with the information in Discovery Advertisement  */
/*                          and install filter for FLOGI Request             */
/*                                                                           */
/*    Input(s)            : pFSBPktInfo - Pointer to FSB Packet Information  */
/*                          pFsbFipSessEntry - Pointer to FIP Session Entry  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbHandleFIPAdvUcast (tFSBPktInfo * pFSBPktInfo,
                      tFsbFipSessEntry * pFsbFipSessEntry)
{
    tFsbFcfEntry       *pFsbFcfEntry = NULL;
    tFsbTrapInfo        FsbTrapInfo;
#ifdef SYSLOG_WANTED
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
#endif

#ifdef SYSLOG_WANTED
    FSB_MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    if (CfaCliGetIfName (pFsbFipSessEntry->u4ENodeIfIndex,
                         (INT1 *) au1IfName) == CLI_FAILURE)
    {
        FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                         FSB_NONE,
                         "Failed to get the Interface name for IfIndex - %d, when "
                         "handling Discovery advertisement Unicast packet\n",
                         pFsbFipSessEntry->u4ENodeIfIndex);
    }
#endif

    pFsbFcfEntry = FsbGetFsbFcfEntry (pFSBPktInfo->u2VlanId,
                                      pFSBPktInfo->u4IfIndex,
                                      pFSBPktInfo->SrcAddr);
    /* When Discovery Advertisement is received from FCF whose addressing mode is
     * FPMA, FCMAP received in Advertisement message must be validated */
    if (pFsbFcfEntry == NULL)
    {
        if (pFSBPktInfo->u1AddressMode == FSB_FPMA_ADDRESSING_MODE)
        {
            if (FsbValidateFcMap (pFSBPktInfo) != FSB_SUCCESS)
            {
                /* Calling Trap function when the received FcMap is invalid */
                FSB_MEMSET (&FsbTrapInfo, 0, sizeof (tFsbTrapInfo));
                FsbTrapInfo.u4ContextId = pFsbFipSessEntry->u4ContextId;
                FsbTrapInfo.u4FsbSessionEnodeIfIndex =
                    pFsbFipSessEntry->u4ENodeIfIndex;
                FSB_MEMCPY (FsbTrapInfo.FsbSessionEnodeMacAddress,
                            pFsbFipSessEntry->ENodeMacAddr, MAC_ADDR_LEN);
                FsbTrapInfo.u2FsbSessionVlanId = pFsbFipSessEntry->u2VlanId;
                FsbSnmpIfSendTrap (FSB_FCMAP_MISMATCH, &FsbTrapInfo);

#ifdef SYSLOG_WANTED
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gFsbGlobals.u4SyslogId,
                              "\nFCMAP mismatch for the session with Context id: %d,VLAN Id: %d, ENode Intf: %s, "
                              "ENode Mac Addr:%x:%x:%x:%x:%x:%x",
                              pFsbFipSessEntry->u4ContextId,
                              pFsbFipSessEntry->u2VlanId,
                              au1IfName,
                              pFsbFipSessEntry->ENodeMacAddr[0],
                              pFsbFipSessEntry->ENodeMacAddr[1],
                              pFsbFipSessEntry->ENodeMacAddr[2],
                              pFsbFipSessEntry->ENodeMacAddr[3],
                              pFsbFipSessEntry->ENodeMacAddr[4],
                              pFsbFipSessEntry->ENodeMacAddr[5]));
#endif
                FsbUpdateVLANStatistics (pFsbFipSessEntry->u4ContextId,
                                         pFsbFipSessEntry->u2VlanId,
                                         FSB_FCMAP_MISMATCH_STATS);
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "FsbHandleFIPAdvUcast with VlanId: %d and IfIndex:%d "
                                 ": Validation of FCMAP failed\r\n",
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
                return FSB_FAILURE;
            }
        }
        /* Configure FCf Address as Static Entry since Mac-learning
         * is disabled for all FCoE VALN */
        if (FsbConfigUcastEntry (pFSBPktInfo->u4ContextId,
                                 pFSBPktInfo->SrcAddr,
                                 pFSBPktInfo->u2VlanId,
                                 pFSBPktInfo->u4IfIndex,
                                 VLAN_CREATE) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId, FSB_ERROR_LEVEL,
                             FSB_NONE,
                             "FsbHandleFIPAdvUcast with VlanId: %d and IfIndex:%d "
                             ": Failure in configuring Static FCF MAC \r\n",
                             pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
            return FSB_FAILURE;
        }
    }
    else if (pFsbFcfEntry->u1AddressingMode == FSB_FPMA_ADDRESSING_MODE)
    {
        if (FsbValidateFcMap (pFSBPktInfo) != FSB_SUCCESS)
        {
            /* Calling Trap function when the received FcMap is invalid */
            FSB_MEMSET (&FsbTrapInfo, 0, sizeof (tFsbTrapInfo));
            FsbTrapInfo.u4ContextId = pFsbFipSessEntry->u4ContextId;
            FsbTrapInfo.u4FsbSessionEnodeIfIndex =
                pFsbFipSessEntry->u4ENodeIfIndex;
            FSB_MEMCPY (FsbTrapInfo.FsbSessionEnodeMacAddress,
                        pFsbFipSessEntry->ENodeMacAddr, MAC_ADDR_LEN);
            FsbTrapInfo.u2FsbSessionVlanId = pFsbFipSessEntry->u2VlanId;
            FsbSnmpIfSendTrap (FSB_FCMAP_MISMATCH, &FsbTrapInfo);

#ifdef SYSLOG_WANTED
            SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gFsbGlobals.u4SyslogId,
                          "\nFCMAP mismatch for the session with Context id: %d,Vlan Id: %d, ENode Intf: %s,ENode Mac Addr:%x:%x:%x:%x:%x:%x",
                          pFsbFipSessEntry->u4ContextId,
                          pFsbFipSessEntry->u2VlanId,
                          au1IfName,
                          pFsbFipSessEntry->ENodeMacAddr[0],
                          pFsbFipSessEntry->ENodeMacAddr[1],
                          pFsbFipSessEntry->ENodeMacAddr[2],
                          pFsbFipSessEntry->ENodeMacAddr[3],
                          pFsbFipSessEntry->ENodeMacAddr[4],
                          pFsbFipSessEntry->ENodeMacAddr[5]));
#endif
            FsbUpdateVLANStatistics (pFsbFipSessEntry->u4ContextId,
                                     pFsbFipSessEntry->u2VlanId,
                                     FSB_FCMAP_MISMATCH_STATS);
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                             FSB_NONE,
                             "FsbHandleFIPAdvUcast with VlanId: %d and IfIndex:%d "
                             ": Validation of FCMAP failed\r\n",
                             pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
            return FSB_FAILURE;
        }
    }
    /* Populate FIP Session Entry with FCFIfIndex, FcMap and Name Id */
    pFsbFipSessEntry->u4FcfIfIndex = pFSBPktInfo->u4IfIndex;
    FSB_MEMCPY (pFsbFipSessEntry->au1FcMap,
                pFSBPktInfo->FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.
                au1FcMap, FSB_FCMAP_LEN);
    FSB_MEMCPY (pFsbFipSessEntry->au1NameId,
                pFSBPktInfo->FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.
                au1NameId, FSB_NAME_ID_LEN);

    /* Set the house-keeping timer flag as true, if the d-bit is not set
     * Set the house keeping timer flag as false, if the d-bit is set. 
     * If the d-bit is set, then keep alive messages will not be
     * transmitted by the ENode, therefore house keeping timer should
     * not be started*/
    if (pFSBPktInfo->FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.u2DbitFlag &
        FSB_DBIT_MASK)
    {
        pFsbFipSessEntry->u1HouseKeepingTimerFlag = FSB_FALSE;
    }
    else
    {
        pFsbFipSessEntry->u1HouseKeepingTimerFlag = FSB_TRUE;

    }

    /* Below function call is used to populate the Filter Entry structure and  
     * call the wrap function to fill the Hw Filter Entry which inturn call NPAPI rountine
     * to install filter to allow FLOGI Request from ENode */
    if (FsbInstallFIPSessionFilter (pFSBPktInfo, pFsbFipSessEntry) !=
        FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbHandleFIPAdvUcast with VlanId and IfIndex: %d"
                         ": Installing filter for FIP Session Failed \r\n",
                         pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbHandleFLOGIRequest                            */
/*                                                                           */
/*    Description         : This funtion will update FIP Session Entry       */
/*                          with the information in FLOGI Request            */
/*                          and install filter for FLOGI Accept              */
/*                                                                           */
/*    Input(s)            : pFSBPktInfo - Pointer to FSB Packet Information  */
/*                          pFsbFipSessEntry - Pointer to FIP Session Entry  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbHandleFLOGIRequest (tFSBPktInfo * pFSBPktInfo,
                       tFsbFipSessEntry * pFsbFipSessEntry)
{
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;

    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    /* In multiple FCF responding case, populate FIP Session entry
     * with selected FCF's Name Id and FcMap and also
     * delete filters installed for all other except the filter corresponds to
     * selected FCF for FIP Session */
    pFsbFilterEntry = (tFsbFilterEntry *)
        RBTreeGetFirst (pFsbFipSessEntry->FsbSessFilterEntry);

    if (pFsbFilterEntry == NULL)
    {
        return FSB_SUCCESS;
    }

    do
    {
        FSB_MEMCPY (FsbFilterEntry.SrcMac, pFsbFilterEntry->SrcMac,
                    MAC_ADDR_LEN);
        FSB_MEMCPY (FsbFilterEntry.DstMac, pFsbFilterEntry->DstMac,
                    MAC_ADDR_LEN);
        FsbFilterEntry.u2OpcodeFilterOffsetValue =
            pFsbFilterEntry->u2OpcodeFilterOffsetValue;
        FsbFilterEntry.u2SubOpcodeFilterOffsetValue =
            pFsbFilterEntry->u2SubOpcodeFilterOffsetValue;

        if (FSB_MEMCMP (pFsbFilterEntry->DstMac,
                        pFSBPktInfo->DestAddr, MAC_ADDR_LEN) == 0)
        {
            FSB_MEMCPY (pFsbFipSessEntry->au1NameId,
                        pFsbFilterEntry->au1NameId, FSB_NAME_ID_LEN);
            FSB_MEMCPY (pFsbFipSessEntry->au1FcMap,
                        pFsbFilterEntry->au1FcMap, FSB_FCMAP_LEN);
            pFsbFilterEntry = (tFsbFilterEntry *)
                RBTreeGetNext (pFsbFipSessEntry->FsbSessFilterEntry,
                               (tRBElem *) & FsbFilterEntry, NULL);
            continue;
        }
        FsFsbHwWrDeleteFilter (pFsbFilterEntry, pFsbFilterEntry->u4HwFilterId);
        if (RBTreeRemove (pFsbFipSessEntry->FsbSessFilterEntry,
                          (tRBElem *) pFsbFilterEntry) != RB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE, "FsbHandleFLOGIRequest:"
                             " RBTreeRemove of FsbFipSessEntry with VlanId: %d and IfIndex: %d"
                             " failed \r\n", pFSBPktInfo->u2VlanId,
                             pFSBPktInfo->u4IfIndex);
            return FSB_FAILURE;
        }
        MemReleaseMemBlock (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                            (UINT1 *) pFsbFilterEntry);
        pFsbFilterEntry = (tFsbFilterEntry *)
            RBTreeGetNext (pFsbFipSessEntry->FsbSessFilterEntry,
                           (tRBElem *) & FsbFilterEntry, NULL);
    }
    while (pFsbFilterEntry != NULL);

    /* Below function call is used to populate the Filter Entry structure and  
     * call the wrap function to fill the Hw Filter Entry which inturn call NPAPI rountine
     * to install filter to allow FLOGI Accept from FCF */
    if (FsbInstallFIPSessionFilter (pFSBPktInfo, pFsbFipSessEntry) !=
        FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbHandleFLOGIRequest with VlanId: %d and IfIndex: %d"
                         ": Installing filter for FIP Session Failed \r\n",
                         pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*    Function Name       : FsbHandleFLOGIReqFromInitState                    */
/*                                                                            */
/*    Description         : This funtion will install filter for LOGO Request */
/*                          and also install filter for FLOGI Accept          */
/*                                                                            */
/*    Input(s)            : pFSBPktInfo - Pointer to FSB Packet Information   */
/*                          pFsbFipSessEntry - Pointer to FIP Session Entry   */
/*                                                                            */
/*    Output(s)           : None                                              */
/*                                                                            */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                          */
/*                                                                            */
/******************************************************************************/
INT4
FsbHandleFLOGIReqFromInitState (tFSBPktInfo * pFSBPktInfo,
                                tFsbFipSessEntry * pFsbFipSessEntry)
{
    tFSBPktInfo         FSBPktInfo;

    FSB_MEMSET (&FSBPktInfo, 0, sizeof (FSBPktInfo));

    FSBPktInfo.u4ContextId = pFSBPktInfo->u4ContextId;
    FSBPktInfo.PacketType = FSB_FIP_ADV_UCAST;
    FSB_MEMCPY (FSBPktInfo.SrcAddr, pFSBPktInfo->DestAddr, MAC_ADDR_LEN);

    /* Below function call is used to install filter to lift LOGO Request packet from ENode */
    if (FsbInstallFIPSessionFilter (&FSBPktInfo, pFsbFipSessEntry) !=
        FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbHandleFLOGIReqFromInitState with VlanId: %d and IfIndex: %d"
                         ": Installing FLOGO Request filter for FIP Session Failed \r\n",
                         pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
        return FSB_FAILURE;
    }

    /* Configure Enode Mac as Static Entry since Mac-learing is disabled 
     * for all FCoE VLAN */
    if (FsbConfigUcastEntry (pFsbFipSessEntry->u4ContextId,
                             pFsbFipSessEntry->ENodeMacAddr,
                             pFsbFipSessEntry->u2VlanId,
                             pFsbFipSessEntry->u4ENodeIfIndex,
                             VLAN_CREATE) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbHandleFLOGIReqFromInitState with VlanId: %d and IfIndex:%d "
                         ": Configuring Static Enode Mac for "
                         "FIP Session Failed \r\n",
                         pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
        return FSB_FAILURE;
    }

    /* Below function call is used to install filter to lift FLOGI Accept packet from FCF */
    if (FsbInstallFIPSessionFilter (pFSBPktInfo, pFsbFipSessEntry) !=
        FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbHandleFLOGIReqFromInitState with VlanId: %d and IfIndex: %d"
                         ": Installing FLOGI Accept filter for FIP Session Failed \r\n",
                         pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbHandleFLOGIAccept                             */
/*                                                                           */
/*    Description         : This function will update FIP Session Entry      */
/*                          with the information in FLOGI Accept message     */
/*                          and install filter to allow for next packet      */
/*                          transmission to happen                           */
/*                                                                           */
/*    Input(s)            : pFSBPktInfo - Pointer to FSB Packet Information  */
/*                          pFsbFipSessEntry - Pointer to FIP Session Entry  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbHandleFLOGIAccept (tFSBPktInfo * pFSBPktInfo,
                      tFsbFipSessFCoEEntry * pFsbFipSessFCoEEntry)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;
    tFsbFcfEntry       *pFsbFcfEntry = NULL;
    tFsbFcfEntry        FsbFcfEntry;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];

    FSB_MEMSET (&FsbFcfEntry, 0, sizeof (tFsbFcfEntry));
    FSB_MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    if (pFsbFipSessFCoEEntry != NULL)
    {
        if (FsbInstallFLOGIAcceptFilter (pFsbFipSessFCoEEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbHandleFLOGIAccept with VlanId: %d and IfIndex:%d"
                             ": FLOGI Accept Filter installation failed\r\n",
                             pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
            return FSB_FAILURE;
        }

        /* Configure FCoE Mac Address as Static Entry */
        if (FsbConfigUcastEntry (pFSBPktInfo->u4ContextId,
                                 pFsbFipSessFCoEEntry->FCoEMacAddr,
                                 pFsbFipSessFCoEEntry->u2VlanId,
                                 pFsbFipSessFCoEEntry->u4ENodeIfIndex,
                                 VLAN_CREATE) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbHandleFLOGIAccept with VlanId: %d and IfIndex:%d"
                             ": Configuring Static FCoE Mac failed\r\n",
                             pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
            return FSB_FAILURE;
        }

        /* Stop the session establishment timer and start the house keeping timer */
        if (FsbTmrStopTimer
            (&(pFsbFipSessFCoEEntry->pFsbFipSessEntry->FsbSessEstablishmentTmr))
            != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbHandleFLOGIAccept with VlanId: %d and IfIndex:%d"
                             ": FsbTmrStopTimer failed\r\n",
                             pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
            return FSB_FAILURE;
        }

        FsbFcfEntry.u4FcfIfIndex = pFSBPktInfo->u4IfIndex;
        FsbFcfEntry.u2VlanId = pFSBPktInfo->u2VlanId;
        FSB_MEMCPY (FsbFcfEntry.FcfMacAddr, pFSBPktInfo->SrcAddr, MAC_ADDR_LEN);

        pFsbFcfEntry = (tFsbFcfEntry *)
            RBTreeGet (gFsbGlobals.FsbFcfTable, (tRBElem *) & FsbFcfEntry);
        if (pFsbFcfEntry == NULL)
        {
            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbHandleFLOGIAccept with VlanId: %d and IfIndex:%d"
                             "pFsbFcfEntry is NULL\r\n",
                             pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
            return FSB_FAILURE;
        }

        if (pFsbFipSessFCoEEntry->pFsbFipSessEntry->u1HouseKeepingTimerFlag ==
            FSB_TRUE)
        {
            pFsbContextInfo =
                FsbCxtGetContextEntry (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                                       u4ContextId);
            if (pFsbContextInfo != NULL)
            {
                if (pFsbContextInfo->u2HouseKeepingTimePeriod != 0)
                {
                    if (pFSBPktInfo->PacketType == FSB_FIP_FLOGI_ACCEPT)
                    {
                        /* HouseKeepingTimer Period in Session Entry will be *
                         * (FKAV + BufferTime {5})*/
                        pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                            u2HouseKeepingTimePeriod =
                            (UINT2) ((pFsbFcfEntry->u4FcfKeepAliveTimerValue) +
                                     5);
                        if (FsbTmrStartTimer
                            (&
                             (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                              FsbHouseKeepingTmr),
                             FSB_ENODE_HOUSE_KEEPING_TIMER_ID,
                             pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                             u2HouseKeepingTimePeriod,
                             pFsbFipSessFCoEEntry->pFsbFipSessEntry) !=
                            FSB_SUCCESS)
                        {
                            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                             FSB_ERROR_LEVEL, FSB_NONE,
                                             "FsbHandleFLOGIAccept with VlanId: %d and IfIndex:%d"
                                             ": FsbTmrStartTimer of ENodeHouseKeeping Timer failed\r\n",
                                             pFSBPktInfo->u2VlanId,
                                             pFSBPktInfo->u4IfIndex);
                            return FSB_FAILURE;
                        }

                        pFsbFipSessFCoEEntry->u2HouseKeepingTimePeriod
                            = pFsbContextInfo->u2HouseKeepingTimePeriod;
                        if (FsbTmrStartTimer
                            (&(pFsbFipSessFCoEEntry->FsbHouseKeepingTmr),
                             FSB_VNMAC_HOUSE_KEEPING_TIMER_ID,
                             pFsbContextInfo->u2HouseKeepingTimePeriod,
                             pFsbFipSessFCoEEntry) != FSB_SUCCESS)
                        {
                            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                             FSB_ERROR_LEVEL, FSB_NONE,
                                             "FsbHandleFLOGIAccept with VlanId: %d and IfIndex:%d"
                                             ": FsbTmrStartTimer of VNMacHouseKeeping Timer failed\r\n",
                                             pFSBPktInfo->u2VlanId,
                                             pFSBPktInfo->u4IfIndex);
                            return FSB_FAILURE;
                        }
                    }
                    else
                    {
                        pFsbFipSessFCoEEntry->u2HouseKeepingTimePeriod
                            = pFsbContextInfo->u2HouseKeepingTimePeriod;
                        if (FsbTmrStartTimer
                            (&(pFsbFipSessFCoEEntry->FsbHouseKeepingTmr),
                             FSB_VNMAC_HOUSE_KEEPING_TIMER_ID,
                             pFsbContextInfo->u2HouseKeepingTimePeriod,
                             pFsbFipSessFCoEEntry) != FSB_SUCCESS)
                        {
                            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                             FSB_ERROR_LEVEL, FSB_NONE,
                                             "FsbHandleFLOGIAccept with VlanId: %d and IfIndex:%d"
                                             ": FsbTmrStartTimer of VNMacHouseKeeping Timer failed\r\n",
                                             pFSBPktInfo->u2VlanId,
                                             pFSBPktInfo->u4IfIndex);
                            return FSB_FAILURE;
                        }
                    }
                    /* This object is to indicate the administrator whether the
                     * house-keeping timer functionality is enabled in the system */
                    pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                        u1HouseKeepingTimerStatus = FSB_TRUE;
                    pFsbFipSessFCoEEntry->u1HouseKeepingTimerStatus = FSB_TRUE;
                    pFsbFipSessFCoEEntry->u1SyncTimerStatus = FSB_TRUE;
                }
            }
        }
        else
        {
            /* Set this object as false, as the house keeping timer functionality
             * is not enabled on the system, since D-bit flag is being set in
             * the FCF Discovery Advertisement packet*/
            pFsbFipSessFCoEEntry->pFsbFipSessEntry->u1HouseKeepingTimerStatus =
                FSB_FALSE;
            pFsbFipSessFCoEEntry->u1HouseKeepingTimerStatus = FSB_FALSE;
        }

        /* Update the Enode Login Count in the FCF table */
        /* Fill the indexes of FsbFcfTable (Vlan Id, FCF MacAddr and FCF Index)
         * and get the matching entry*/

        /* After obtaining the entry, increment the ENodeLoginCount variable */
        if (pFsbFcfEntry != NULL)
        {
            pFsbFcfEntry->u4EnodeLoginCount++;
        }

        /* Get the Interface name for the EnodeIfIndex to display in FSB trace */
        if (CfaCliGetIfName
            (pFsbFipSessFCoEEntry->pFsbFipSessEntry->u4ENodeIfIndex,
             (INT1 *) au1IfName) == CLI_FAILURE)
        {
            FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                             u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                             "Failed to get Interface Name for IfIndex - %d during "
                             "FIP session establishment\n",
                             pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                             u4ENodeIfIndex);
        }
        FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->u4ContextId,
                         FSB_CRITICAL_LEVEL, FSB_NONE,
                         "FIP Session is established in Context id: %d,VLAN Id: %d, Enode Interface name: [%s], "
                         "ENode If Index: %d, ENode Mac Addr:%x:%x:%x:%x:%x:%x\r\n",
                         pFsbFipSessFCoEEntry->pFsbFipSessEntry->u4ContextId,
                         pFsbFipSessFCoEEntry->pFsbFipSessEntry->u2VlanId,
                         au1IfName,
                         pFsbFipSessFCoEEntry->pFsbFipSessEntry->u4ENodeIfIndex,
                         pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                         ENodeMacAddr[0],
                         pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                         ENodeMacAddr[1],
                         pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                         ENodeMacAddr[2],
                         pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                         ENodeMacAddr[3],
                         pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                         ENodeMacAddr[4],
                         pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                         ENodeMacAddr[5]);
#ifdef SYSLOG_WANTED
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gFsbGlobals.u4SyslogId,
                      "\nFIP Session is established in Context id: %d,VLAN Id: %d, ENode Intf: %s, ENode Mac Addr:%x:%x:%x:%x:%x:%x",
                      pFsbFipSessFCoEEntry->pFsbFipSessEntry->u4ContextId,
                      pFsbFipSessFCoEEntry->pFsbFipSessEntry->u2VlanId,
                      au1IfName,
                      pFsbFipSessFCoEEntry->pFsbFipSessEntry->ENodeMacAddr[0],
                      pFsbFipSessFCoEEntry->pFsbFipSessEntry->ENodeMacAddr[1],
                      pFsbFipSessFCoEEntry->pFsbFipSessEntry->ENodeMacAddr[2],
                      pFsbFipSessFCoEEntry->pFsbFipSessEntry->ENodeMacAddr[3],
                      pFsbFipSessFCoEEntry->pFsbFipSessEntry->ENodeMacAddr[4],
                      pFsbFipSessFCoEEntry->pFsbFipSessEntry->ENodeMacAddr[5]));
#endif
        gFsbGlobals.gu4FsbFIPSessionCount++;
        return FSB_SUCCESS;
    }
    return FSB_FAILURE;
}

/********************************************************************************/
/*                                                                              */
/*    Function Name       : FsbHandleKeepAlive                                  */
/*                                                                              */
/*    Description         : This function is used to restart HouseKeeping Timer */
/*                          Timer and if                                        */
/*                          1. ENode Keep Alive is received increment           */
/*                             ENodeKeepAliveCount                              */
/*                          2. VNPort Keep Alive is received increment          */
/*                             VNKeepAliveCount                                 */
/*                                                                              */
/*    Input(s)            : u2PacketType - Packet Type                          */
/*                          pFsbFipSessEntry - Pointer to FIP Session Entry     */
/*                                                                              */
/*    Output(s)           : None                                                */
/*                                                                              */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                            */
/*                                                                              */
/********************************************************************************/
INT4
FsbHandleKeepAlive (tFsbFipSessEntry * pFsbFipSessEntry,
                    tFSBPktInfo * pFSBPktInfo)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;

    if (pFSBPktInfo->PacketType == FSB_FIP_ENODE_KEEP_ALIVE)
    {
        if (pFsbFipSessEntry->u1HouseKeepingTimerFlag == FSB_TRUE)
        {
            /* Stop and Start the House Keeping Timer */
            if (FsbTmrStopTimer (&(pFsbFipSessEntry->FsbHouseKeepingTmr)) !=
                FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbHandleKeepAlive with VlanId: %d and IfIndex:%d"
                                 ": FsbTmrStopTimer failed\r\n",
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
                return FSB_FAILURE;
            }
            /* Start the house keeping TIMER */
            if (FsbTmrStartTimer (&(pFsbFipSessEntry->FsbHouseKeepingTmr),
                                  FSB_ENODE_HOUSE_KEEPING_TIMER_ID,
                                  pFsbFipSessEntry->u2HouseKeepingTimePeriod,
                                  pFsbFipSessEntry) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbHandleKeepAlive with VlanId: %d and IfIndex: %d"
                                 ": FsbTmrStartTimer failed\r\n",
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
                return FSB_FAILURE;
            }
            /* This object is to indicate the administrator whether the
             * house-keeping timer functionality is enabled in the system */
            pFsbFipSessEntry->u1HouseKeepingTimerStatus = FSB_TRUE;
            pFsbFipSessEntry->u4KeepAliveCount++;
        }
    }

    else if (pFSBPktInfo->PacketType == FSB_FIP_VNPORT_KEEP_ALIVE)
    {
        pFsbFipSessFCoEEntry =
            FsbGetFIPSessionFCoEEntry (pFsbFipSessEntry->u2VlanId,
                                       pFsbFipSessEntry->u4ENodeIfIndex,
                                       pFsbFipSessEntry->ENodeMacAddr,
                                       pFSBPktInfo->DestAddr,
                                       pFSBPktInfo->FsbFipsPktFields.
                                       FIPVNKeepAlive.VNMacAddr);
        if (pFsbFipSessFCoEEntry != NULL)
        {
            if (pFsbFipSessFCoEEntry->u1HouseKeepingTimerFlag == FSB_TRUE)
            {
                /* Stop and Start the House Keeping Timer */
                if (FsbTmrStopTimer
                    (&(pFsbFipSessFCoEEntry->FsbHouseKeepingTmr)) !=
                    FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                                     u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbHandleKeepAlive"
                                     " with VlanId: %d and IfIndex: %d: FsbTmrStopTimer failed\r\n",
                                     pFSBPktInfo->u2VlanId,
                                     pFSBPktInfo->u4IfIndex);
                    return FSB_FAILURE;
                }
                /* Start the house keeping TIMER */
                if (FsbTmrStartTimer
                    (&(pFsbFipSessFCoEEntry->FsbHouseKeepingTmr),
                     FSB_VNMAC_HOUSE_KEEPING_TIMER_ID,
                     pFsbFipSessFCoEEntry->u2HouseKeepingTimePeriod,
                     pFsbFipSessFCoEEntry) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                                     u4ContextId,
                                     FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbHandleKeepAlive"
                                     " with VlanId: %d and IfIndex: %d: FsbTmrStartTimer failed\r\n",
                                     pFSBPktInfo->u2VlanId,
                                     pFSBPktInfo->u4IfIndex);
                    return FSB_FAILURE;
                }
                /* This object is to indicate the administrator whether the
                 * house-keeping timer functionality is enabled in the system */
                pFsbFipSessFCoEEntry->u1HouseKeepingTimerStatus = FSB_TRUE;
                pFsbFipSessFCoEEntry->u4VNKeepAliveCount++;
            }
        }
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbHandleClearLink                               */
/*                                                                           */
/*    Description         : This function will Remove FIP Session Entry,     */
/*                          FCoE Entries and delete all the filters          */
/*                          installed for that particular session            */
/*                                                                           */
/*    Input(s)            : pFsbFipSessEntry - Pointer to FIP Session Entry  */
/*                          u1PacketType     - Packet Type                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbHandleClearLink (tFsbFipSessEntry * pFsbFipSessEntry,
                    tFSBPktInfo * pFSBPktInfo)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry;
    tFsbFipSessFCoEEntry FsbFipSessFCoEEntry;
    tMacAddr            ZeroMacAddr = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
    UINT2               u2VNMacCount = 0;

    FSB_MEMSET (&FsbFipSessFCoEEntry, 0, sizeof (tFsbFipSessFCoEEntry));

    /* Handling ENode CVL from FCF. i.e, Without list of Vx_Port Identifier in Descriptor field */
    if (pFSBPktInfo->FsbFipsPktFields.FIPClearLink.pVNMacAddr == NULL)
    {
        pFsbFipSessEntry->u1DeleteReasonCode = FSB_REASON_CODE_ENODE_CVL;
        if (FsbClearFIPSession (pFsbFipSessEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE, "FsbHandleClearLink"
                             "with VlanId: %d and IfIndex: %d: FsbClearFIPSession failed\r\n",
                             pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
            return FSB_FAILURE;

        }
    }
    /* Handling VNPort CVL from FCF. i.e, With list of Vx_Port Identifier in Descriptor field */
    else
    {
        FsbFipSessFCoEEntry.u2VlanId = pFsbFipSessEntry->u2VlanId;
        FsbFipSessFCoEEntry.u4ENodeIfIndex = pFsbFipSessEntry->u4ENodeIfIndex;
        FSB_MEMCPY (FsbFipSessFCoEEntry.ENodeMacAddr,
                    pFsbFipSessEntry->ENodeMacAddr, MAC_ADDR_LEN);
        FSB_MEMCPY (FsbFipSessFCoEEntry.FcfMacAddr, pFSBPktInfo->SrcAddr,
                    MAC_ADDR_LEN);
        while ((FSB_MEMCMP
                (pFSBPktInfo->FsbFipsPktFields.FIPClearLink.
                 pVNMacAddr[u2VNMacCount], ZeroMacAddr, MAC_ADDR_LEN) != 0)
               && (u2VNMacCount <= MAX_FSB_FCOE_MAC_ADDR))
        {
            FSB_MEMCPY (FsbFipSessFCoEEntry.FCoEMacAddr,
                        pFSBPktInfo->FsbFipsPktFields.FIPClearLink.
                        pVNMacAddr[u2VNMacCount], MAC_ADDR_LEN);
            pFsbFipSessFCoEEntry =
                (tFsbFipSessFCoEEntry *) RBTreeGet (gFsbGlobals.
                                                    FsbFipSessFCoETable,
                                                    &FsbFipSessFCoEEntry);
            if (pFsbFipSessFCoEEntry != NULL)
            {
                pFsbFipSessFCoEEntry->u1DeleteReasonCode =
                    FSB_REASON_CODE_VNPORT_CVL;
                if (FsbDeleteFCoEEntry (pFsbFipSessFCoEEntry) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                                     u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbHandleClearLink"
                                     "with VlanId: %d and IfIndex: %d: FsbDeleteFCoEEntry failed\r\n",
                                     pFSBPktInfo->u2VlanId,
                                     pFSBPktInfo->u4IfIndex);
                    return FSB_FAILURE;
                }
            }

            FSB_MEMSET (FsbFipSessFCoEEntry.FCoEMacAddr, 0, MAC_ADDR_LEN);
            u2VNMacCount++;
        }

        if (pFsbFipSessEntry->u4FCoECount == 0)
        {
            pFsbFipSessEntry->u1DeleteReasonCode = FSB_REASON_CODE_VNPORT_CVL;
            if (FsbDeleteFIPSessionEntry (pFsbFipSessEntry) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE, "FsbHandleClearLink"
                                 " with VlanId: %d and IfIndex: %d:"
                                 " FsbDeleteFIPSessionEntry failed\r\n",
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
                return FSB_FAILURE;
            }
        }
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbClearFIPSession                               */
/*                                                                           */
/*    Description         : This function will Remove FIP Session Entry,     */
/*                          FCoE Entries and delete all the filters          */
/*                          installed for that particular session            */
/*                                                                           */
/*    Input(s)            : pFsbFipSessEntry - Pointer to FIP Session Entry  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbClearFIPSession (tFsbFipSessEntry * pFsbFipSessEntry)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;
    tFsbFipSessFCoEEntry FsbFipSessFCoEEntry;

    FSB_MEMSET (&FsbFipSessFCoEEntry, 0, sizeof (tFsbFipSessFCoEEntry));

    FsbFipSessFCoEEntry.u2VlanId = pFsbFipSessEntry->u2VlanId;
    FsbFipSessFCoEEntry.u4ENodeIfIndex = pFsbFipSessEntry->u4ENodeIfIndex;
    FSB_MEMCPY (FsbFipSessFCoEEntry.ENodeMacAddr,
                pFsbFipSessEntry->ENodeMacAddr, MAC_ADDR_LEN);

    if (pFsbFipSessEntry != NULL)
    {
        while ((pFsbFipSessFCoEEntry = (tFsbFipSessFCoEEntry *)
                RBTreeGetNext (gFsbGlobals.FsbFipSessFCoETable,
                               (tRBElem *) & FsbFipSessFCoEEntry,
                               NULL)) != NULL)
        {
            FsbFipSessFCoEEntry.u2VlanId = pFsbFipSessFCoEEntry->u2VlanId;
            FsbFipSessFCoEEntry.u4ENodeIfIndex =
                pFsbFipSessFCoEEntry->u4ENodeIfIndex;
            FSB_MEMCPY (FsbFipSessFCoEEntry.ENodeMacAddr,
                        pFsbFipSessFCoEEntry->ENodeMacAddr, MAC_ADDR_LEN);
            FSB_MEMCPY (FsbFipSessFCoEEntry.FcfMacAddr,
                        pFsbFipSessFCoEEntry->FcfMacAddr, MAC_ADDR_LEN);
            FSB_MEMCPY (FsbFipSessFCoEEntry.FCoEMacAddr,
                        pFsbFipSessFCoEEntry->FCoEMacAddr, MAC_ADDR_LEN);
            if ((FsbFipSessFCoEEntry.u2VlanId == pFsbFipSessEntry->u2VlanId) &&
                (FsbFipSessFCoEEntry.u4ENodeIfIndex ==
                 pFsbFipSessEntry->u4ENodeIfIndex) &&
                (FSB_MEMCMP (FsbFipSessFCoEEntry.ENodeMacAddr,
                             pFsbFipSessEntry->ENodeMacAddr,
                             MAC_ADDR_LEN) == 0))
            {
                pFsbFipSessFCoEEntry->u1DeleteReasonCode =
                    pFsbFipSessEntry->u1DeleteReasonCode;
                /* Delete FCoE Entry and its respective filters */
                if (FsbDeleteFCoEEntry (pFsbFipSessFCoEEntry) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                                     u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbClearFIPSession: "
                                     "Deletion of FCoE Entry with VlanId: %d and "
                                     "ENodeIfIndex: %d failed\r\n",
                                     pFsbFipSessFCoEEntry->u2VlanId,
                                     pFsbFipSessFCoEEntry->u4ENodeIfIndex);
                    return FSB_FAILURE;
                }
            }
        }
        /* Delete FIP Session Entry and its respective filters */
        if (FsbDeleteFIPSessionEntry (pFsbFipSessEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbDeleteFIPSessionEntry : "
                             "Deletion of FIPSessionEntry with VlanId: %d, "
                             "ENodeIfIndex: %d and FcfIfIndex: %d failed\r\n",
                             pFsbFipSessEntry->u2VlanId,
                             pFsbFipSessEntry->u4ENodeIfIndex,
                             pFsbFipSessEntry->u4FcfIfIndex);
            return FSB_FAILURE;
        }
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDeleteFIPSessionEntry                         */
/*                                                                           */
/*    Description         : This function will Remove FIP Session Entry      */
/*                          and delete all the filters installed for that    */
/*                          particular session                               */
/*                                                                           */
/*                                                                           */
/*    Input(s)            :  pFsbFipSessEntry - Pointer to FIP Session Entry */
/*                           u1PacketType     - Packet Type                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbDeleteFIPSessionEntry (tFsbFipSessEntry * pFsbFipSessEntry)
{
    tFsbTrapInfo        FsbTrapInfo;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];

    FSB_MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    FSB_MEMSET (&FsbTrapInfo, 0, sizeof (tFsbTrapInfo));
    if (pFsbFipSessEntry != NULL)
    {
        /* Stop the Establishment Timer */
        if (FsbTmrStopTimer (&(pFsbFipSessEntry->FsbSessEstablishmentTmr)) !=
            FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbDeleteFIPSessionEntry of FIP Session Entry with"
                             " VlanId: %d, EnodeIfIndex: %d and FcfIfIndex: %d:"
                             " FsbTmrStopTimer of Establishment Timer failed\r\n",
                             pFsbFipSessEntry->u2VlanId,
                             pFsbFipSessEntry->u4ENodeIfIndex,
                             pFsbFipSessEntry->u4FcfIfIndex);
            return FSB_FAILURE;
        }
        /* Stop the House Keeping Timer */
        if (FsbTmrStopTimer (&(pFsbFipSessEntry->FsbHouseKeepingTmr)) !=
            FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbDeleteFIPSessionEntry of FIP Session Entry with"
                             " VlanId: %d, EnodeIfIndex: %d and FcfIfIndex: %d:"
                             " FsbTmrStopTimer of HouseKeeping Timer failed\r\n",
                             pFsbFipSessEntry->u2VlanId,
                             pFsbFipSessEntry->u4ENodeIfIndex,
                             pFsbFipSessEntry->u4FcfIfIndex);
            return FSB_FAILURE;
        }
        /* Remove filters installed during Discovery Advertisement Unicast
         * and FLOGI Request */
        if (FsbRemoveFIPSessionFilter (pFsbFipSessEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbDeleteFIPSessionEntry"
                             " of FIP Session Entry with VlanId: %d, EnodeIfIndex: %d"
                             " and FcfIfIndex: %d: Remove FIP Session Filter Failed \r\n",
                             pFsbFipSessEntry->u2VlanId,
                             pFsbFipSessEntry->u4ENodeIfIndex,
                             pFsbFipSessEntry->u4FcfIfIndex);
            return FSB_FAILURE;
        }

        /* Static MAC Address of the ENode programmed should not be deleted in the
         * following packets,
         * 1. FSB_FIP_CLEAR_LINK from FCF
         * 2. FSB_FIP_LOGO_ACCEPT from FCF
         * 3. FSB_FIP_FLOGI_REJECT from FCF
         * This is done to avoid flooding of the above packets*/
        if ((pFsbFipSessEntry->u1DeleteReasonCode != FSB_REASON_CODE_ENODE_CVL)
            && (pFsbFipSessEntry->u1DeleteReasonCode !=
                FSB_REASON_CODE_VNPORT_CVL)
            && (pFsbFipSessEntry->u1DeleteReasonCode !=
                FSB_REASON_CODE_FLOGO_ACCEPT)
            && (pFsbFipSessEntry->u1DeleteReasonCode !=
                FSB_REASON_CODE_FLOGI_REJECT))
        {
            /* Remove Enode Mac Address Configured since the session is clearned */
            if (FsbConfigUcastEntry (pFsbFipSessEntry->u4ContextId,
                                     pFsbFipSessEntry->ENodeMacAddr,
                                     pFsbFipSessEntry->u2VlanId,
                                     pFsbFipSessEntry->u4ENodeIfIndex,
                                     VLAN_DELETE) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbDeleteFIPSessionEntry"
                                 " of FIP Session Entry with VlanId: %d, EnodeIfIndex: %d"
                                 " and FcfIfIndex: %d: Removing Static ENode Mac failed\r\n",
                                 pFsbFipSessEntry->u2VlanId,
                                 pFsbFipSessEntry->u4ENodeIfIndex,
                                 pFsbFipSessEntry->u4FcfIfIndex);
                return FSB_FAILURE;
            }
        }

        /* Get the Interface name for the EnodeIfIndex to display in FSB trace */
        if (CfaCliGetIfName (pFsbFipSessEntry->u4ENodeIfIndex,
                             (INT1 *) au1IfName) == CLI_FAILURE)
        {
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "Failed to get Interface name for IfIndex - %d during "
                             "FIP session delete\n",
                             pFsbFipSessEntry->u4ENodeIfIndex);
        }

        FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                         FSB_CRITICAL_LEVEL, FSB_NONE,
                         "\nFsbDeleteFIPSessionEntry: FIP Session"
                         "Cleared for Context id: %d, VLAN Id: %d, Enode Interface name: [%s], "
                         "ENode If Index: %d, ENode Mac Addr:%x:%x:%x:%x:%x:%x Reason : %s\n",
                         pFsbFipSessEntry->u4ContextId,
                         pFsbFipSessEntry->u2VlanId,
                         au1IfName,
                         pFsbFipSessEntry->u4ENodeIfIndex,
                         pFsbFipSessEntry->ENodeMacAddr[0],
                         pFsbFipSessEntry->ENodeMacAddr[1],
                         pFsbFipSessEntry->ENodeMacAddr[2],
                         pFsbFipSessEntry->ENodeMacAddr[3],
                         pFsbFipSessEntry->ENodeMacAddr[4],
                         pFsbFipSessEntry->ENodeMacAddr[5],
                         FsbDeleteReasonString[pFsbFipSessEntry->
                                               u1DeleteReasonCode]);

        /* Before the session entry is removed,
         * FilterEntry RBTree needs to be completely destroyed */
        if (pFsbFipSessEntry->FsbSessFilterEntry != NULL)
        {
            gFsbGlobals.u4FsbSessFilterEntryRBCount--;
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                             FSB_DEBUGGING_LEVEL, FSB_SESSION_TRC,
                             "FsbDeleteFIPSessionEntry: Calling RBTree Delete for FIP session entry \n");

            RBTreeDelete (pFsbFipSessEntry->FsbSessFilterEntry);
            FsbResetFreeIndex (gFsbGlobals.SessFilterEntryList,
                               pFsbFipSessEntry->u4Index);
            pFsbFipSessEntry->FsbSessFilterEntry = NULL;
        }

        if (RBTreeRemove (gFsbGlobals.FsbFipSessTable,
                          (tRBElem *) pFsbFipSessEntry) != RB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbDeleteFIPSessionEntry: "
                             "RBTree Remove of FIP Session Entry with  "
                             "VlanId: %d, EnodeIfIndex: %d and FcfIfIndex: %d "
                             "Failed \r\n", pFsbFipSessEntry->u2VlanId,
                             pFsbFipSessEntry->u4ENodeIfIndex,
                             pFsbFipSessEntry->u4FcfIfIndex);
            return FSB_FAILURE;
        }
        MemReleaseMemBlock (FSB_SESSION_ENTRIES_MEMPOOL_ID,
                            (UINT1 *) pFsbFipSessEntry);
        return FSB_SUCCESS;
    }
    return FSB_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDeleteFCoEEntry                               */
/*                                                                           */
/*    Description         : This function will Remove FIP Session FCoE       */
/*                          Entry and delete all the filters installed       */
/*                          for that particular session                      */
/*                                                                           */
/*    Input(s)            : pFsbFipSessFCoEEntry - Pointer to FIP Session    */
/*                          FCoE Entry                                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbDeleteFCoEEntry (tFsbFipSessFCoEEntry * pFsbFipSessFCoEEntry)
{
    tFsbFipSessEntry   *pFsbFipSessEntry = NULL;
    tFsbFcfEntry       *pFsbFcfEntry = NULL;
    tFsbFcfEntry        FsbFcfEntry;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];

    FSB_MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    FSB_MEMSET (&FsbFcfEntry, 0, sizeof (tFsbFcfEntry));

    if (pFsbFipSessFCoEEntry != NULL)
    {
        pFsbFipSessEntry = pFsbFipSessFCoEEntry->pFsbFipSessEntry;
        /* Stop the House Keeping Timer */
        if (FsbTmrStopTimer (&(pFsbFipSessFCoEEntry->FsbHouseKeepingTmr)) !=
            FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                             u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbDeleteFCoEEntry of FCoE Entry with VlanId: %d and "
                             "ENodeIfIndex: %d: FsbTmrStopTimer failed\r\n",
                             pFsbFipSessFCoEEntry->u2VlanId,
                             pFsbFipSessFCoEEntry->u4ENodeIfIndex);
            return FSB_FAILURE;
        }
        /* Delete Filters installed during FLOGI/FDISC Accept */
        if (FsbRemoveFLOGIAcceptFilter (pFsbFipSessFCoEEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                             u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbDeleteFCoEEntry: "
                             "of FCoE Entry with VlanId: %d and ENodeIfIndex: %d: "
                             "Remove FLOGI Accept Filter Failed \r\n",
                             pFsbFipSessFCoEEntry->u2VlanId,
                             pFsbFipSessFCoEEntry->u4ENodeIfIndex);
            return FSB_FAILURE;
        }
        /* Remove the Static FCOE Mac Address configured since the 
         * session is no longer valid */
        if (FsbConfigUcastEntry
            (pFsbFipSessFCoEEntry->pFsbFipSessEntry->u4ContextId,
             pFsbFipSessFCoEEntry->FCoEMacAddr, pFsbFipSessFCoEEntry->u2VlanId,
             pFsbFipSessFCoEEntry->u4ENodeIfIndex, VLAN_DELETE) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                             u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbDeleteFCoEEntry: "
                             "of FCoE Entry with VlanId: %d and ENodeIfIndex: %d: "
                             "Removing Static FCoE Mac failed\r\n",
                             pFsbFipSessFCoEEntry->u2VlanId,
                             pFsbFipSessFCoEEntry->u4ENodeIfIndex);
            return FSB_FAILURE;
        }

        /* Update the Enode Login Count in the FCF table */
        FsbFcfEntry.u4FcfIfIndex = pFsbFipSessEntry->u4FcfIfIndex;
        FsbFcfEntry.u2VlanId = pFsbFipSessFCoEEntry->u2VlanId;
        FSB_MEMCPY (FsbFcfEntry.FcfMacAddr, pFsbFipSessFCoEEntry->FcfMacAddr,
                    MAC_ADDR_LEN);

        pFsbFcfEntry = (tFsbFcfEntry *)
            RBTreeGet (gFsbGlobals.FsbFcfTable, (tRBElem *) & FsbFcfEntry);

        if (pFsbFcfEntry != NULL)
        {
            pFsbFcfEntry->u4EnodeLoginCount--;
        }

        /* Get the Interface name for the EnodeIfIndex to display in FSB trace */
        if (CfaCliGetIfName (pFsbFipSessFCoEEntry->u4ENodeIfIndex,
                             (INT1 *) au1IfName) == CLI_FAILURE)
        {
            FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                             u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                             "Failed to Get Interface name for IfIndex - %d during "
                             "FIP FCoE session delete\n",
                             pFsbFipSessFCoEEntry->u4ENodeIfIndex);
        }
        FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->u4ContextId,
                         FSB_CRITICAL_LEVEL, FSB_NONE,
                         "\n FsbDeleteFCoEEntry: FIP FCoE Session"
                         "Cleared for Context id: %d, VLAN Id: %d, Enode Interface name: [%s], "
                         "ENode If Index: %d, ENode Mac Addr:%x:%x:%x:%x:%x:%x Reason : %s\n",
                         pFsbFipSessFCoEEntry->pFsbFipSessEntry->u4ContextId,
                         pFsbFipSessFCoEEntry->u2VlanId,
                         au1IfName,
                         pFsbFipSessFCoEEntry->u4ENodeIfIndex,
                         pFsbFipSessFCoEEntry->FCoEMacAddr[0],
                         pFsbFipSessFCoEEntry->FCoEMacAddr[1],
                         pFsbFipSessFCoEEntry->FCoEMacAddr[2],
                         pFsbFipSessFCoEEntry->FCoEMacAddr[3],
                         pFsbFipSessFCoEEntry->FCoEMacAddr[4],
                         pFsbFipSessFCoEEntry->FCoEMacAddr[5],
                         FsbDeleteReasonString[pFsbFipSessFCoEEntry->
                                               u1DeleteReasonCode]);
        /* Before the session entry is removed,
         * FilterEntry RBTree needs to be completely destroyed */
        if (pFsbFipSessFCoEEntry->FsbSessFCoEFilterEntry != NULL)
        {
            gFsbGlobals.u4FsbFCoEFilterEntryRBCount--;
            FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                             u4ContextId, FSB_DEBUGGING_LEVEL,
                             FSB_CONTROL_PATH_TRC,
                             "\n FsbDeleteFCoEEntry: Calling RBTreeDelete for FCoE entry \n");

            RBTreeDelete (pFsbFipSessFCoEEntry->FsbSessFCoEFilterEntry);
            FsbResetFreeIndex (gFsbGlobals.FcoeFilterEntryList,
                               pFsbFipSessFCoEEntry->u4Index);
            pFsbFipSessFCoEEntry->FsbSessFCoEFilterEntry = NULL;
        }

        if (RBTreeRemove (gFsbGlobals.FsbFipSessFCoETable,
                          (tRBElem *) pFsbFipSessFCoEEntry) != RB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                             u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbDeleteFCoEEntry: "
                             "of FCoE Entry with VlanId: %d and ENodeIfIndex: %d: "
                             "RBTree Remove of FCoE Table Failed \r\n",
                             pFsbFipSessFCoEEntry->u2VlanId,
                             pFsbFipSessFCoEEntry->u4ENodeIfIndex);
            return FSB_FAILURE;
        }
        pFsbFipSessEntry->u4FCoECount--;
        MemReleaseMemBlock (FSB_FCOE_ENTRIES_MEMPOOL_ID,
                            (UINT1 *) pFsbFipSessFCoEEntry);
        gFsbGlobals.gu4FsbFIPSessionCount--;
        return FSB_SUCCESS;
    }
    return FSB_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbInstallFIPSessionFilter                       */
/*                                                                           */
/*    Description         : This function is used to install filters to      */
/*                          allow next packet transmission to happen         */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : pFSBPktInfo - Pointer to FSB Packet Information  */
/*                          pFsbFipSessEntry - Pointer to FIP Session Entry  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbInstallFIPSessionFilter (tFSBPktInfo * pFSBPktInfo,
                            tFsbFipSessEntry * pFsbFipSessEntry)
{
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry    *pFsbGetFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;
    tMacAddr            ZeroMacAddr;
    UINT4               u4HwFilterId = 0;

    FSB_MEMSET (ZeroMacAddr, 0, MAC_ADDR_LEN);
    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    if (pFsbFipSessEntry == NULL)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbInstallFIPSessionFilter: "
                         "pFsbFipSessEntry is NULL\r\n");
        return FSB_FAILURE;
    }

    pFsbFilterEntry = MemAllocMemBlk (FSB_FILTER_ENTRIES_MEMPOOL_ID);

    if (pFsbFilterEntry == NULL)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbInstallFIPSessionFilter: "
                         "Memory allocation for pFsbFilterEntry failed\r\n");
        return FSB_FAILURE;
    }

    FSB_MEMSET (pFsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    if ((pFSBPktInfo->PacketType == FSB_FIP_SOLICIT_UCAST) ||
        (pFSBPktInfo->PacketType == FSB_FIP_FLOGI_REQUEST) ||
        (pFSBPktInfo->PacketType == FSB_FIP_NPIV_FDISC_REQUEST))
    {

        /* Below function call will fill pFsbFipSessEntry with appropriate 
         * values with respective to the information in received FIP frames */
        FsFsbFormFIPSessionFilter (pFsbFipSessEntry, pFSBPktInfo->PacketType,
                                   pFSBPktInfo->DestAddr, pFsbFilterEntry);
    }
    else if (pFSBPktInfo->PacketType == FSB_FIP_ADV_UCAST)
    {
        FsFsbFormFIPSessionFilter (pFsbFipSessEntry, pFSBPktInfo->PacketType,
                                   pFSBPktInfo->SrcAddr, pFsbFilterEntry);
    }
    else
    {
        FsFsbFormFIPSessionFilter (pFsbFipSessEntry, pFSBPktInfo->PacketType,
                                   ZeroMacAddr, pFsbFilterEntry);
    }

    FSB_MEMCPY ((FsbFilterEntry.SrcMac), pFsbFilterEntry->SrcMac, MAC_ADDR_LEN);
    FSB_MEMCPY ((FsbFilterEntry.DstMac), pFsbFilterEntry->DstMac, MAC_ADDR_LEN);
    FsbFilterEntry.u2OpcodeFilterOffsetValue =
        pFsbFilterEntry->u2OpcodeFilterOffsetValue;
    FsbFilterEntry.u2SubOpcodeFilterOffsetValue =
        pFsbFilterEntry->u2SubOpcodeFilterOffsetValue;

    pFsbGetFilterEntry = (tFsbFilterEntry *)
        RBTreeGet (pFsbFipSessEntry->FsbSessFilterEntry,
                   (tRBElem *) & FsbFilterEntry);
    if (pFsbGetFilterEntry != NULL)
    {
        /* The filter entry is already installed and added in RBTree.
         * Hence, returning SUCCESS */
        MemReleaseMemBlock (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                            (UINT1 *) pFsbFilterEntry);
        return FSB_SUCCESS;
    }

    /* Below function call will fill hardware filter entry and call 
     * appropriate NPAPI to install filter and will return 
     * hardware filter id */
    pFsbFilterEntry->u4FilterId = FSB_GET_FILTER_ID ();
    if (FsFsbHwWrCreateFilter (pFsbFilterEntry, &u4HwFilterId) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                         FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                         "FsbInstallFIPSessionFilter: "
                         "Installation of filter failed\r\n");
    }

    pFsbFilterEntry->u4HwFilterId = u4HwFilterId;

    if (RBTreeAdd (pFsbFipSessEntry->FsbSessFilterEntry,
                   (tRBElem *) pFsbFilterEntry) != RB_SUCCESS)
    {
        FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbInstallFIPSessionFilter:"
                         " RBTreeAdd failed \r\n");
        FsFsbHwWrDeleteFilter (pFsbFilterEntry, pFsbFilterEntry->u4HwFilterId);
        MemReleaseMemBlock (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                            (UINT1 *) pFsbFilterEntry);
        /* Error scenario happened, so deleting the existing FIP session entries.
         * FSB will drop this packet and clears all the FIP session database,
         * so that the next packet from the server will be sent properly. */
        pFsbFipSessEntry->u1DeleteReasonCode =
            FSB_REASON_CODE_FILTER_INSTALLATION_FAILURE;
        if (FsbDeleteFIPSessionEntry (pFsbFipSessEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbInstallFIPSessionFilter"
                             "with VlanId: %d and IfIndex: %d: FsbDeleteFIPSessionEntry failed\r\n",
                             pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
        }
        return FSB_FAILURE;
    }

    return FSB_SUCCESS;
}

/**************************************************************************************/
/*                                                                                    */
/*    Function Name       : FsbInstallFLOGIAcceptFilter                               */
/*                                                                                    */
/*    Description         : This function is used to install filters to               */
/*                          allow next packet transmission to happen                  */
/*                                                                                    */
/*                                                                                    */
/*    Input(s)            : pFSBPktInfo - Pointer to FSB Packet Information           */
/*                          pFsbFipSessFCoEEntry - Pointer to FIP Session FCoE Entry  */
/*                                                                                    */
/*    Output(s)           : None                                                      */
/*                                                                                    */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                                  */
/*                                                                                    */
/**************************************************************************************/
INT4
FsbInstallFLOGIAcceptFilter (tFsbFipSessFCoEEntry * pFsbFipSessFCoEEntry)
{
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    UINT4               u4HwFilterId = 0;
    UINT2               u2FilterType = 0;

    if (pFsbFipSessFCoEEntry == NULL)
    {
        return FSB_FAILURE;
    }

    /* In case of FLOGI/FDISC ACCEPT we need to install filters for the below scenarios
     * 1. FSB_FCOE_TRAFFIC_FROM_FCF
     * 2. FSB_FCOE_TRAFFIC_FROM_ENODE
     * 3. FSB_VN_PORT_KEEP_ALIVE
     * 4. FSB_ENODE_KEEP_ALIVE
     * 5. FSB_FIP_CLEAR_VIRTUAL_LINK */
    for (u2FilterType = FSB_FCOE_TRAFFIC_FROM_FCF;
         u2FilterType <= FSB_FIP_CLEAR_VIRTUAL_LINK; u2FilterType++)
    {
        pFsbFilterEntry = MemAllocMemBlk (FSB_FILTER_ENTRIES_MEMPOOL_ID);

        if (pFsbFilterEntry == NULL)
        {
            FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                             u4ContextId, FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                             "FsbInstallFLOGIAcceptFilter: "
                             "Memory allocation for pFsbFilterEntry failed\r\n");
            return FSB_FAILURE;
        }

        FSB_MEMSET (pFsbFilterEntry, 0, sizeof (tFsbFilterEntry));

        /* Below function call will fill pFsbFipSessFCoEEntry with appropriate 
         * values with respective to the information in received FIP frames */
        FsFsbFormFLOGIAcceptFilter (pFsbFipSessFCoEEntry, u2FilterType,
                                    pFsbFilterEntry);
        /* Below function call will fill hardware filter entry and call 
         * appropriate NPAPI to install filter and will return 
         * hardware filter id */
        pFsbFilterEntry->u4FilterId = FSB_GET_FILTER_ID ();
        if (FsFsbHwWrCreateFilter (pFsbFilterEntry, &u4HwFilterId)
            != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                             u4ContextId, FSB_DEBUGGING_LEVEL,
                             FSB_CONTROL_PATH_TRC,
                             "FsbInstallFLOGIAcceptFilter: "
                             "Installation of filter failed\r\n");
        }

        pFsbFilterEntry->u4HwFilterId = u4HwFilterId;

        if (RBTreeAdd (pFsbFipSessFCoEEntry->FsbSessFCoEFilterEntry,
                       (tRBElem *) pFsbFilterEntry) != RB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                             u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbInstallFLOGIAcceptFilter:"
                             " RBTreeAdd failed \r\n");
            FsFsbHwWrDeleteFilter (pFsbFilterEntry,
                                   pFsbFilterEntry->u4HwFilterId);
            MemReleaseMemBlock (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                                (UINT1 *) pFsbFilterEntry);
            return FSB_FAILURE;
        }
    }

    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbRemoveFIPSessionFilter                        */
/*                                                                           */
/*    Description         : This function will remove filters for the        */
/*                          respective session                               */
/*                                                                           */
/*    Input(s)            : pFsbFipSessEntry - Pointer to FIP Session Entry  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbRemoveFIPSessionFilter (tFsbFipSessEntry * pFsbFipSessEntry)
{
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;
    tFsbTrapInfo        FsbTrapInfo;
#ifdef SYSLOG_WANTED
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
#endif

    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));
    FSB_MEMSET (&FsbTrapInfo, 0, sizeof (tFsbTrapInfo));
#ifdef SYSLOG_WANTED
    FSB_MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
#endif

    pFsbFilterEntry = (tFsbFilterEntry *)
        RBTreeGetFirst (pFsbFipSessEntry->FsbSessFilterEntry);

    if (pFsbFilterEntry == NULL)
    {
        return FSB_SUCCESS;
    }

    do
    {
        FSB_MEMCPY (FsbFilterEntry.SrcMac, pFsbFilterEntry->SrcMac,
                    MAC_ADDR_LEN);
        FSB_MEMCPY (FsbFilterEntry.DstMac, pFsbFilterEntry->DstMac,
                    MAC_ADDR_LEN);
        FsbFilterEntry.u2OpcodeFilterOffsetValue =
            pFsbFilterEntry->u2OpcodeFilterOffsetValue;
        FsbFilterEntry.u2SubOpcodeFilterOffsetValue =
            pFsbFilterEntry->u2SubOpcodeFilterOffsetValue;

        if (FsFsbHwWrDeleteFilter (pFsbFilterEntry,
                                   pFsbFilterEntry->u4HwFilterId) !=
            FSB_SUCCESS)
        {
            /* Calling Trap function when ACL filter deletion failed */
            FsbTrapInfo.u4ContextId = pFsbFipSessEntry->u4ContextId;
            FsbTrapInfo.u4FsbSessionEnodeIfIndex =
                pFsbFipSessEntry->u4ENodeIfIndex;
            FSB_MEMCPY (FsbTrapInfo.FsbSessionEnodeMacAddress,
                        pFsbFipSessEntry->ENodeMacAddr, MAC_ADDR_LEN);
            FsbTrapInfo.u2FsbSessionVlanId = pFsbFipSessEntry->u2VlanId;
            FsbSnmpIfSendTrap (FSB_ACL_FAILURE, &FsbTrapInfo);

#ifdef SYSLOG_WANTED
            if (CfaCliGetIfName (pFsbFipSessEntry->u4ENodeIfIndex,
                                 (INT1 *) au1IfName) == CLI_FAILURE)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "Failed to get Interface name for IfIndex - %d, when removing "
                                 "FIP session ACL\n",
                                 pFsbFipSessEntry->u4ENodeIfIndex);
            }
            SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gFsbGlobals.u4SyslogId,
                          "\n ACL failure for Context id: %d,VLAN Id: %d, ENode Intf: %s,ENode Mac Addr:%x:%x:%x:%x:%x:%x",
                          pFsbFipSessEntry->u4ContextId,
                          pFsbFipSessEntry->u2VlanId,
                          au1IfName,
                          pFsbFipSessEntry->ENodeMacAddr[0],
                          pFsbFipSessEntry->ENodeMacAddr[1],
                          pFsbFipSessEntry->ENodeMacAddr[2],
                          pFsbFipSessEntry->ENodeMacAddr[3],
                          pFsbFipSessEntry->ENodeMacAddr[4],
                          pFsbFipSessEntry->ENodeMacAddr[5]));
#endif
            FsbUpdateVLANStatistics (pFsbFipSessEntry->u4ContextId,
                                     pFsbFipSessEntry->u2VlanId,
                                     FSB_ACL_FAILURE_STATS);
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbRemoveFIPSessionFilter:"
                             " ACL Filter deletion failed \r\n");
        }
        else
        {
            if (RBTreeRemove (pFsbFipSessEntry->FsbSessFilterEntry,
                              (tRBElem *) pFsbFilterEntry) != RB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbRemoveFIPSessionFilter:"
                                 " RBTreeRemove failed \r\n");
                return FSB_FAILURE;
            }
            MemReleaseMemBlock (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                                (UINT1 *) pFsbFilterEntry);
        }
        pFsbFilterEntry = (tFsbFilterEntry *)
            RBTreeGetNext (pFsbFipSessEntry->FsbSessFilterEntry,
                           (tRBElem *) & FsbFilterEntry, NULL);
    }
    while (pFsbFilterEntry != NULL);
    return FSB_SUCCESS;
}

/*************************************************************************************/
/*                                                                                   */
/*    Function Name       : FsbRemoveFLOGIAcceptFilter                               */
/*                                                                                   */
/*    Description         : This function will remove filters for the                */
/*                          respective session                                       */
/*                                                                                   */
/*    Input(s)            : pFsbFipSessFCoEEntry - Pointer to FIP Session FCoEEntry  */
/*                                                                                   */
/*    Output(s)           : None                                                     */
/*                                                                                   */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                                 */
/*                                                                                   */
/*************************************************************************************/
INT4
FsbRemoveFLOGIAcceptFilter (tFsbFipSessFCoEEntry * pFsbFipSessFCoEEntry)
{
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;
    tFsbTrapInfo        FsbTrapInfo;
#ifdef SYSLOG_WANTED
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
#endif

    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));
    FSB_MEMSET (&FsbTrapInfo, 0, sizeof (tFsbTrapInfo));
#ifdef SYSLOG_WANTED
    FSB_MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
#endif

    pFsbFilterEntry = (tFsbFilterEntry *)
        RBTreeGetFirst (pFsbFipSessFCoEEntry->FsbSessFCoEFilterEntry);

    if (pFsbFilterEntry == NULL)
    {
        return FSB_SUCCESS;
    }

    do
    {
        FSB_MEMCPY (FsbFilterEntry.SrcMac, pFsbFilterEntry->SrcMac,
                    MAC_ADDR_LEN);
        FSB_MEMCPY (FsbFilterEntry.DstMac, pFsbFilterEntry->DstMac,
                    MAC_ADDR_LEN);
        FsbFilterEntry.u2OpcodeFilterOffsetValue =
            pFsbFilterEntry->u2OpcodeFilterOffsetValue;
        FsbFilterEntry.u2SubOpcodeFilterOffsetValue =
            pFsbFilterEntry->u2SubOpcodeFilterOffsetValue;

        if (FsFsbHwWrDeleteFilter (pFsbFilterEntry,
                                   pFsbFilterEntry->u4HwFilterId) !=
            FSB_SUCCESS)
        {
            /* Calling Trap function when ACL filter deletion failed */
            FsbTrapInfo.u4ContextId =
                pFsbFipSessFCoEEntry->pFsbFipSessEntry->u4ContextId;
            FsbTrapInfo.u4FsbSessionEnodeIfIndex =
                pFsbFipSessFCoEEntry->u4ENodeIfIndex;
            FSB_MEMCPY (FsbTrapInfo.FsbSessionEnodeMacAddress,
                        pFsbFipSessFCoEEntry->ENodeMacAddr, MAC_ADDR_LEN);
            FsbTrapInfo.u2FsbSessionVlanId = pFsbFipSessFCoEEntry->u2VlanId;
            FsbSnmpIfSendTrap (FSB_ACL_FAILURE, &FsbTrapInfo);

#ifdef SYSLOG_WANTED
            if (CfaCliGetIfName (pFsbFipSessFCoEEntry->u4ENodeIfIndex,
                                 (INT1 *) au1IfName) == CLI_FAILURE)
            {
                FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                                 u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                 "Failed to get the Interface name for IfIndex - %d, when removing "
                                 "FLOGI accept filter\n",
                                 pFsbFipSessFCoEEntry->u4ENodeIfIndex);
            }
            SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gFsbGlobals.u4SyslogId,
                          "\n ACL failure for Context id: %d,VLAN Id: %d, ENode Intf: %s, ENode Mac Addr:%x:%x:%x:%x:%x:%x",
                          pFsbFipSessFCoEEntry->pFsbFipSessEntry->u4ContextId,
                          pFsbFipSessFCoEEntry->u2VlanId,
                          au1IfName,
                          pFsbFipSessFCoEEntry->ENodeMacAddr[0],
                          pFsbFipSessFCoEEntry->ENodeMacAddr[1],
                          pFsbFipSessFCoEEntry->ENodeMacAddr[2],
                          pFsbFipSessFCoEEntry->ENodeMacAddr[3],
                          pFsbFipSessFCoEEntry->ENodeMacAddr[4],
                          pFsbFipSessFCoEEntry->ENodeMacAddr[5]));
#endif
            FsbUpdateVLANStatistics (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                                     u4ContextId,
                                     pFsbFipSessFCoEEntry->u2VlanId,
                                     FSB_ACL_FAILURE_STATS);
            FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                             u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbRemoveFLOGIAcceptFilter:"
                             " ACL Filter deletion failed \r\n");
        }
        else
        {
            if (RBTreeRemove (pFsbFipSessFCoEEntry->FsbSessFCoEFilterEntry,
                              (tRBElem *) pFsbFilterEntry) != RB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                                 u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbRemoveFLOGIAcceptFilter:"
                                 " RBTreeRemove failed \r\n");
                return FSB_FAILURE;
            }
            MemReleaseMemBlock (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                                (UINT1 *) pFsbFilterEntry);
        }
        pFsbFilterEntry = (tFsbFilterEntry *)
            RBTreeGetNext (pFsbFipSessFCoEEntry->FsbSessFCoEFilterEntry,
                           (tRBElem *) & FsbFilterEntry, NULL);
    }
    while (pFsbFilterEntry != NULL);
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbValidateFcMap                                 */
/*                                                                           */
/*    Description         : This function is used to validate FcMap          */
/*                          received in FIP Discovery Advertisement.         */
/*                          If FcMapMode is                                  */
/*                          1. Global then compare received FcMap            */
/*                             with FcMap in Context Table                    */
/*                          2. VLAN then compare received FcMap              */
/*                             with FcMap in FIPSnooping Table                */
/*                                                                           */
/*    Input(s)            : pFSBPktInfo                                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbValidateFcMap (tFSBPktInfo * pFSBPktInfo)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbContextInfo    *pFsbContextInfo = NULL;
    UINT1               au1FcMap[FSB_FCMAP_LEN];

    FSB_MEMSET (&au1FcMap, 0, FSB_FCMAP_LEN);

    if (FSB_NODE_STATUS () != FSB_NODE_ACTIVE)
    {
        /* Validate FCMAP only in Active Node */
        return FSB_SUCCESS;
    }

    pFsbContextInfo = FsbCxtGetContextEntry (pFSBPktInfo->u4ContextId);

    if (pFsbContextInfo == NULL)
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbValidateFcMap : FsbCxtGetContextEntry returns NULL");
        return FSB_FAILURE;
    }
    /* If FcMapMode is GLOBAL, compare FcMap in pFSBPktInfo 
     * with FcMap in Context Table */
    if (pFsbContextInfo->u1FcMapMode == FSB_FCMAP_GLOBAL)
    {
        if (FSB_MEMCMP (pFsbContextInfo->au1FcMap, au1FcMap, FSB_FCMAP_LEN) ==
            0)
        {
            /* If the FCMAP value is set to zero by the administrator,
             * FCMAP validation should not be done*/
            return FSB_SUCCESS;
        }
        if (FSB_MEMCMP
            (pFSBPktInfo->FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.
             au1FcMap, pFsbContextInfo->au1FcMap, FSB_FCMAP_LEN) != 0)
        {
            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL,
                             FSB_NONE,
                             "FsbValidateFcMap: Global FcMap MisMatch Failed \r\n");
            return FSB_FAILURE;
        }
    }
    /* If FcMapMode is VLAN, compare FcMap in pFSBPktInfo 
     * with FcMap in FIP Snooping Table */
    else
    {
        pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (pFSBPktInfo->u4ContextId,
                                                       pFSBPktInfo->u2VlanId);
        if (pFsbFipSnoopingEntry == NULL)
        {
            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL,
                             FSB_NONE,
                             "FsbValidateFcMap: FIPSnoopingEntry Get Failed \r\n");
            return FSB_FAILURE;
        }
        if (FSB_MEMCMP (pFsbFipSnoopingEntry->au1FcMap, au1FcMap, FSB_FCMAP_LEN)
            == 0)
        {
            /* If the FCMAP value is set to zero by the administrator,
             * FCMAP validation should not be done*/
            return FSB_SUCCESS;
        }
        if (FSB_MEMCMP
            (pFSBPktInfo->FsbFipsPktFields.FcfDiscoveryAdvertisementInfo.
             au1FcMap, pFsbFipSnoopingEntry->au1FcMap, FSB_FCMAP_LEN) != 0)
        {
            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL,
                             FSB_NONE,
                             "FsbValidateFcMap: VLAN FcMap MisMatch Failed \r\n");
            return FSB_FAILURE;
        }
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbValidateMtuSize                               */
/*                                                                           */
/*    Description         : This function is used to validate Frame size     */
/*                          received in FIP Discovery Solicitation           */
/*                                                                           */
/*    Input(s)            : pFSBPktInfo                                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbValidateMtuSize (tFSBPktInfo * pFSBPktInfo)
{
    INT4                i4BrgPortType = 0;
    UINT4               u4IfMtu = 0;
    UINT4               u4UapIfIndex = 0;
    UINT2               u2SVID = 0;

    if (FSB_NODE_STATUS () != FSB_NODE_ACTIVE)
    {
        /*Validate MTU size only in Active Node */
        return FSB_SUCCESS;
    }
    FsbCfaGetInterfaceBrgPortType (pFSBPktInfo->u4IfIndex, &i4BrgPortType);

    if (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        if (FsbVlanApiGetSChInfoFromSChIfIndex (pFSBPktInfo->u4IfIndex,
                                                &u4UapIfIndex,
                                                &u2SVID) != FSB_FAILURE)
        {
            /* Get Mtu size for the IfIndex in the received FIP Discovery Solicitation */
            if (CfaGetIfMtu (u4UapIfIndex, &u4IfMtu) != CFA_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "FsbValidateMtuSize: CfaGetIfMtu failed\r\n");
                return FSB_FAILURE;
            }
        }
    }
    else
    {
        /* Get Mtu size for the IfIndex in the received FIP Discovery Solicitation */
        if (CfaGetIfMtu (pFSBPktInfo->u4IfIndex, &u4IfMtu) != CFA_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL,
                             FSB_NONE,
                             "FsbValidateMtuSize: CfaGetIfMtu failed\r\n");
            return FSB_FAILURE;
        }
    }
    if (u4IfMtu <
        ((UINT4)
         (pFSBPktInfo->FsbFipsPktFields.FCFSolicitationUnicastInfo.
          u2MaxFrameSize)))
    {
        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbValidateMtuSize: Exceeding Max Frame Size \r\n");
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDeleteAllFipSessEntry                         */
/*                                                                           */
/*    Description         : This function will delete all FipSessEntry       */
/*                          mapped to the context                            */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context index                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbDeleteAllFipSessEntry (UINT4 u4ContextId)
{
    tFsbFipSessEntry   *pFsbNextFipSessEntry = NULL;
    tFsbFipSessEntry   *pFsbFipSessEntry = NULL;
    tFsbFipSessEntry    FsbFipSessEntry;

    FSB_MEMSET (&FsbFipSessEntry, 0, sizeof (tFsbFipSessEntry));

    pFsbFipSessEntry = (tFsbFipSessEntry *)
        RBTreeGetFirst (gFsbGlobals.FsbFipSessTable);

    if (pFsbFipSessEntry == NULL)
    {
        /* No FipSessEntry were created and therefore, no entries
         * to delete */
        return FSB_SUCCESS;
    }
    while (pFsbFipSessEntry != NULL)
    {
        FsbFipSessEntry.u2VlanId = pFsbFipSessEntry->u2VlanId;
        FsbFipSessEntry.u4ENodeIfIndex = pFsbFipSessEntry->u4ENodeIfIndex;
        FSB_MEMCPY (&FsbFipSessEntry.ENodeMacAddr,
                    pFsbFipSessEntry->ENodeMacAddr, MAC_ADDR_LEN);

        pFsbNextFipSessEntry = (tFsbFipSessEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFipSessTable,
                           (tRBElem *) & FsbFipSessEntry, NULL);
        if (pFsbFipSessEntry->u4ContextId == u4ContextId)
        {
            pFsbFipSessEntry->u1DeleteReasonCode =
                FSB_REASON_CODE_CONTEXT_DELETE;
            /* Delete FIP Session Entry and Remove filters for that session */
            if (FsbClearFIPSession (pFsbFipSessEntry) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbDeleteAllFipSessEntry: FsbClearFIPSession failed\r\n");
                return FSB_FAILURE;
            }
        }
        pFsbFipSessEntry = pFsbNextFipSessEntry;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDeleteFIPSessEntryForFcf                      */
/*                                                                           */
/*    Description         : This function will delete all FipSessEntry       */
/*                          mapped to the context                            */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context index                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbDeleteFIPSessEntryForFcf (tFsbFcfEntry * pFsbFcfEntry)
{
    tFsbFipSessEntry   *pFsbNextFipSessEntry = NULL;
    tFsbFipSessEntry   *pFsbFipSessEntry = NULL;
    tFsbFipSessEntry    FsbFipSessEntry;

    FSB_MEMSET (&FsbFipSessEntry, 0, sizeof (tFsbFipSessEntry));

    pFsbFipSessEntry = (tFsbFipSessEntry *)
        RBTreeGetFirst (gFsbGlobals.FsbFipSessTable);

    if (pFsbFipSessEntry == NULL)
    {
        /* No FipSessEntry were created and therefore, no entries
         * to delete */
        return FSB_SUCCESS;
    }
    while (pFsbFipSessEntry != NULL)
    {
        FsbFipSessEntry.u2VlanId = pFsbFipSessEntry->u2VlanId;
        FsbFipSessEntry.u4ENodeIfIndex = pFsbFipSessEntry->u4ENodeIfIndex;
        FSB_MEMCPY (&FsbFipSessEntry.ENodeMacAddr,
                    pFsbFipSessEntry->ENodeMacAddr, MAC_ADDR_LEN);

        pFsbNextFipSessEntry = (tFsbFipSessEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFipSessTable,
                           (tRBElem *) & FsbFipSessEntry, NULL);

        if ((pFsbFipSessEntry->u4FcfIfIndex == pFsbFcfEntry->u4FcfIfIndex) &&
            (pFsbFipSessEntry->u2VlanId == pFsbFcfEntry->u2VlanId))
        {
            pFsbFipSessEntry->u1DeleteReasonCode = FSB_REASON_CODE_FCF_DELETE;
            /* Delete FIP Session Entry and Remove filters for that session */
            if (FsbClearFIPSession (pFsbFipSessEntry) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFcfEntry->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "FsbDeleteFIPSessEntryForFcf: FsbClearFIPSession failed\r\n");
                return FSB_FAILURE;
            }
        }
        pFsbFipSessEntry = pFsbNextFipSessEntry;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDeleteFipSessionEntryForFcoeVlan              */
/*                                                                           */
/*    Description         : This function will delete all FipSessEntry       */
/*                          mapped to the deleted Fcoe Vlan                  */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context index                      */
/*                          u2VlanId    - Vlan Index                         */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbDeleteFipSessionEntryForFcoeVlan (UINT4 u4ContextId, UINT2 u2VlanId)
{
    tFsbFipSessEntry   *pFsbNextFipSessEntry = NULL;
    tFsbFipSessEntry   *pFsbFipSessEntry = NULL;
    tFsbFipSessEntry    FsbFipSessEntry;

    FSB_MEMSET (&FsbFipSessEntry, 0, sizeof (tFsbFipSessEntry));

    pFsbFipSessEntry = (tFsbFipSessEntry *)
        RBTreeGetFirst (gFsbGlobals.FsbFipSessTable);

    if (pFsbFipSessEntry == NULL)
    {
        /* No FipSessEntry were created and therefore, no entries
         * to delete */
        return FSB_SUCCESS;
    }
    while (pFsbFipSessEntry != NULL)
    {
        FsbFipSessEntry.u2VlanId = pFsbFipSessEntry->u2VlanId;
        FsbFipSessEntry.u4ENodeIfIndex = pFsbFipSessEntry->u4ENodeIfIndex;
        FSB_MEMCPY (&FsbFipSessEntry.ENodeMacAddr,
                    pFsbFipSessEntry->ENodeMacAddr, MAC_ADDR_LEN);

        pFsbNextFipSessEntry = (tFsbFipSessEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFipSessTable,
                           (tRBElem *) & FsbFipSessEntry, NULL);

        if ((pFsbFipSessEntry->u4ContextId == u4ContextId) &&
            (pFsbFipSessEntry->u2VlanId == u2VlanId))
        {
            /* Delete FIP Session Entry and Remove filters for that session */
            pFsbFipSessEntry->u1DeleteReasonCode = FSB_REASON_CODE_VLAN_DELETE;
            if (FsbClearFIPSession (pFsbFipSessEntry) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbDeleteFipSessionEntryForFcoeVlan with VlanId: %d "
                                 ": FsbClearFIPSession failed\r\n", u2VlanId);
                return FSB_FAILURE;
            }
        }
        pFsbFipSessEntry = pFsbNextFipSessEntry;
    }

    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDeleteSessEntryForIfIndex                     */
/*                                                                           */
/*    Description         : This function will delete all FipSessEntry       */
/*                          mapped to the port                               */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Interface index                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbDeleteSessEntryForIfIndex (UINT4 u4IfIndex)
{
    tFsbFipSessEntry   *pFsbNextFipSessEntry = NULL;
    tFsbFipSessEntry   *pFsbFipSessEntry = NULL;
    tFsbFipSessEntry    FsbFipSessEntry;

    FSB_MEMSET (&FsbFipSessEntry, 0, sizeof (tFsbFipSessEntry));

    pFsbFipSessEntry = (tFsbFipSessEntry *)
        RBTreeGetFirst (gFsbGlobals.FsbFipSessTable);

    if (pFsbFipSessEntry == NULL)
    {
        /* No FipSessEntry were created and therefore, no entries
         * to delete */
        return FSB_SUCCESS;
    }
    while (pFsbFipSessEntry != NULL)
    {
        FsbFipSessEntry.u2VlanId = pFsbFipSessEntry->u2VlanId;
        FsbFipSessEntry.u4ENodeIfIndex = pFsbFipSessEntry->u4ENodeIfIndex;
        MEMCPY (&FsbFipSessEntry.ENodeMacAddr, pFsbFipSessEntry->ENodeMacAddr,
                MAC_ADDR_LEN);

        pFsbNextFipSessEntry = (tFsbFipSessEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFipSessTable,
                           (tRBElem *) & FsbFipSessEntry, NULL);

        if (pFsbFipSessEntry->u4ENodeIfIndex == u4IfIndex ||
            pFsbFipSessEntry->u4FcfIfIndex == u4IfIndex)
        {
            /* Delete FIP Session Entry, FCoE Entries and Remove filters 
             * for that session */
            pFsbFipSessEntry->u1DeleteReasonCode = FSB_REASON_CODE_PORT_DOWN;
            if (FsbClearFIPSession (pFsbFipSessEntry) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbDeleteSessEntryForIfIndex with IfIndex: %d "
                                 ": FsbClearFIPSession failed\r\n", u4IfIndex);
                return FSB_FAILURE;
            }
        }
        pFsbFipSessEntry = pFsbNextFipSessEntry;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDeleteSessEntryForVLANIfIndex                 */
/*                                                                           */
/*    Description         : This function will delete all FipSessEntry       */
/*                          mapped to the Port of Particular VLAN            */
/*                                                                           */
/*    Input(s)            : VlanId - VLAN Index, u4IfIndex - Interface index */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbDeleteSessEntryForVLANIfIndex (tVlanId VlanId, UINT4 u4IfIndex)
{
    tFsbFipSessEntry   *pFsbNextFipSessEntry = NULL;
    tFsbFipSessEntry   *pFsbFipSessEntry = NULL;
    tFsbFipSessEntry    FsbFipSessEntry;

    FSB_MEMSET (&FsbFipSessEntry, 0, sizeof (tFsbFipSessEntry));

    pFsbFipSessEntry = (tFsbFipSessEntry *)
        RBTreeGetFirst (gFsbGlobals.FsbFipSessTable);

    if (pFsbFipSessEntry == NULL)
    {
        /* No FipSessEntry were created and therefore, no entries
         * to delete */
        return FSB_SUCCESS;
    }
    while (pFsbFipSessEntry != NULL)
    {
        FsbFipSessEntry.u2VlanId = pFsbFipSessEntry->u2VlanId;
        FsbFipSessEntry.u4ENodeIfIndex = pFsbFipSessEntry->u4ENodeIfIndex;
        MEMCPY (&FsbFipSessEntry.ENodeMacAddr, pFsbFipSessEntry->ENodeMacAddr,
                MAC_ADDR_LEN);

        pFsbNextFipSessEntry = (tFsbFipSessEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFipSessTable,
                           (tRBElem *) & FsbFipSessEntry, NULL);

        if (((pFsbFipSessEntry->u4ENodeIfIndex == u4IfIndex) ||
             (pFsbFipSessEntry->u4FcfIfIndex == u4IfIndex)) &&
            (pFsbFipSessEntry->u2VlanId == VlanId))
        {
            /* Delete FIP Session Entry, FCoE Entries and Remove filters 
             * for that session */
            pFsbFipSessEntry->u1DeleteReasonCode = FSB_REASON_CODE_PORT_DOWN;
            if (FsbClearFIPSession (pFsbFipSessEntry) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "FsbDeleteSessEntryForVLANIfIndex with IfIndex: %d and VlanId: %d"
                                 ": FsbClearFIPSession failed\r\n", u4IfIndex,
                                 VlanId);
                return FSB_FAILURE;
            }
        }
        pFsbFipSessEntry = pFsbNextFipSessEntry;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbHandleFLOGOAccept                             */
/*                                                                           */
/*    Description         : This function will Remove FIP Session Entry,     */
/*                          FCoE Entries and delete all the filters          */
/*                          installed for that particular session            */
/*                                                                           */
/*    Input(s)            : pFsbFipSessEntry - Pointer to FIP Session Entry  */
/*                          u1PacketType     - Packet Type                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS /FSB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbHandleFLOGOAccept (tFsbFipSessEntry * pFsbFipSessEntry,
                      tFSBPktInfo * pFSBPktInfo)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry;
    tFsbFipSessFCoEEntry FsbFipSessFCoEEntry;

    FSB_MEMSET (&FsbFipSessFCoEEntry, 0, sizeof (tFsbFipSessFCoEEntry));

    FsbFipSessFCoEEntry.u2VlanId = pFsbFipSessEntry->u2VlanId;
    FsbFipSessFCoEEntry.u4ENodeIfIndex = pFsbFipSessEntry->u4ENodeIfIndex;
    FSB_MEMCPY (FsbFipSessFCoEEntry.ENodeMacAddr,
                pFsbFipSessEntry->ENodeMacAddr, MAC_ADDR_LEN);
    FSB_MEMCPY (FsbFipSessFCoEEntry.FcfMacAddr, pFSBPktInfo->SrcAddr,
                MAC_ADDR_LEN);
    FSB_MEMCPY (FsbFipSessFCoEEntry.FCoEMacAddr,
                pFSBPktInfo->FsbFipsPktFields.FIPLogoInfo.FCoEMacAddr,
                MAC_ADDR_LEN);

    pFsbFipSessFCoEEntry = (tFsbFipSessFCoEEntry *)
        RBTreeGet (gFsbGlobals.FsbFipSessFCoETable, &FsbFipSessFCoEEntry);

    if (pFsbFipSessFCoEEntry != NULL)
    {
        pFsbFipSessFCoEEntry->u1DeleteReasonCode = FSB_REASON_CODE_FLOGO_ACCEPT;
        if (FsbDeleteFCoEEntry (pFsbFipSessFCoEEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessFCoEEntry->pFsbFipSessEntry->
                             u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbHandleFLOGOAccept received on"
                             " Vlan: %d with IfIndex: %d: FsbDeleteFCoEEntry failed\r\n",
                             pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
            return FSB_FAILURE;
        }
    }

    if (pFsbFipSessEntry->u4FCoECount == 0)
    {
        pFsbFipSessEntry->u1DeleteReasonCode = FSB_REASON_CODE_FLOGO_ACCEPT;
        if (FsbDeleteFIPSessionEntry (pFsbFipSessEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFipSessEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbHandleFLOGOAccept received on"
                             " Vlan: %d with IfIndex: %d: FsbDeleteFIPSessionEntry failed\r\n",
                             pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
            return FSB_FAILURE;
        }
    }
    else
    {
        /* In multiple Session scenario(FDISC), if LOGO Accept
         * is received for one FCoE Session means the State 
         * in pFsbFipSessEntry will remain in FSB_FIP_LOGO_REQUEST state. 
         * Hence Keep Alive packets are dropped. To avoid this
         * setting the state in pFsbFipSessEntry as FSB_FIP_FLOGI_ACCEPT 
         */
        pFsbFipSessEntry->u1State = FSB_FIP_FLOGI_ACCEPT;
    }
    return FSB_SUCCESS;
}
#endif /*_FSBSESS_C_ */
