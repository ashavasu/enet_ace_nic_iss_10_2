/*************************************************************************
* Copyright (C) 2007-2015 Aricent Group . All Rights Reserved
*
* $Id: fsbinit.c,v 1.10 2017/11/07 12:23:59 siva Exp $
*
* Description:  This file contains init functions for FIP-snooping Module
*
*************************************************************************/
#ifndef _FSBINIT_C_
#define _FSBINIT_C_

#include "fsbinc.h"

/****************************************************************************
*                                                                           *
* Function     : FsbInit                                                    *
*                                                                           *
* Description  : This function creates queues, semaphore, mempools used by  *
*                the FSB module                                             *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : FSB_SUCCESS / FSB_FAILURE                                  *
*                                                                           *
*****************************************************************************/
INT4
FsbInit (VOID)
{
    FSB_MEMSET (&gFsbGlobals, 0, sizeof (tFsbGlobals));
    gFsbGlobals.u4FsbFCoEFilterEntryRBCount = 0;
    gFsbGlobals.u4FsbSessFilterEntryRBCount = 0;
    gFsbGlobals.u4FsbFilterEntryRBCount = 0;

    FSB_MEMSET (&gFsbGlobals.SessFilterEntryList, 0, FSB_RB_FREE_LIST_SIZE);
    FSB_MEMSET (&gFsbGlobals.FcoeFilterEntryList, 0, FSB_RB_FREE_LIST_SIZE);

    /* Get Task Id of the FIP-snooping Module */
    if (OsixGetTaskId (SELF, FSB_TASK_NAME, &(FSB_TASK_ID)) != OSIX_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_OS_RESOURCE_TRC,
                         "FsbInit: Get Task ID failed\r\n");
        return FSB_FAILURE;
    }

    /* Create Semaphore for FIP_snooping Module */
    if (OsixSemCrt (FSB_SEM_NAME, &(FSB_SEM_ID)) == OSIX_FAILURE)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_OS_RESOURCE_TRC,
                         "FsbInit : Sem Creation failed\r\n");
        /* Release of above created resources are taken care in FsbDeInit() */
        return FSB_FAILURE;
    }

    OsixSemGive (FSB_SEM_ID);

    /* Create Queue for FIP-snooping module */
    if (OsixQueCrt ((UINT1 *) FSB_TASK_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    FSB_TASK_QUEUE_DEPTH, &(FSB_QUEUE_ID)) == OSIX_FAILURE)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_OS_RESOURCE_TRC,
                         "FsbInit : Queue creation failed\r\n");
        /* Release of above created resources are taken care in FsbDeInit() */
        return FSB_FAILURE;
    }
    /* Register with Redundancy manager */
    if (FsbRedRegisterWithRM () != OSIX_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbInit: Registration with RM FAILED!!!\r\n");
        return OSIX_FAILURE;
    }

    if (FsbSizingMemCreateMemPools () != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                         "FsbInit : Creating mem pool for Fsb failed\r\n");
        return FSB_FAILURE;
    }

    /* Assign MempoolIds created in sz.c to global MempoolIds */
    FsbAssignMempoolIds ();

    if (FsbTmrInit () != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_DEBUGGING_LEVEL, FSB_OS_RESOURCE_TRC,
                         "FsbInit : FsbTmrInit failed\r\n");
        return FSB_FAILURE;
    }

    /* Create RBTree to store FipSnoopingEntry */
    gFsbGlobals.FsbFipSnoopingTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tFsbFipSnoopingEntry,
                                              FsbFipSnoopingNode)),
                              FsbCompareFIPSnoopingEntryIndex);

    if (gFsbGlobals.FsbFipSnoopingTable == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_DEBUGGING_LEVEL, FSB_OS_RESOURCE_TRC,
                         "FsbInit : RB Tree Creation for"
                         " FsbFipSnoopingNode failed\r\n");
        /* Release of above created resources are taken care in FsbDeInit() */
        return FSB_FAILURE;
    }

    /* Create RBTree to store tFsbIntfEntry */
    gFsbGlobals.FsbIntfTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tFsbIntfEntry,
                                              FsbIntfEntryNode)),
                              FsbCompareIntfEntryIndex);

    if (gFsbGlobals.FsbIntfTable == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_OS_RESOURCE_TRC,
                         "FsbInit : RB Tree Creation for"
                         " FsbIntfEntryNode failed\r\n");
        /* Release of above created resources are taken care in FsbDeInit() */
        RBTreeDelete (gFsbGlobals.FsbFipSnoopingTable);
        return FSB_FAILURE;
    }

    /* Create RBTree to store tFsbFipSessEntry */
    gFsbGlobals.FsbFipSessTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tFsbFipSessEntry,
                                              FipSessEntryNode)),
                              FsbCompareFipSessEntryIndex);

    if (gFsbGlobals.FsbFipSessTable == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_OS_RESOURCE_TRC,
                         "FsbInit : RB Tree Creation for"
                         " FsbFipSessTable failed\r\n");
        /* Release of above created resources are taken care in FsbDeInit() */
        RBTreeDelete (gFsbGlobals.FsbFipSnoopingTable);
        RBTreeDelete (gFsbGlobals.FsbIntfTable);
        return FSB_FAILURE;
    }

    /* Create RBTree to store tFsbFipSessFCoEEntry */
    gFsbGlobals.FsbFipSessFCoETable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tFsbFipSessFCoEEntry,
                                              FipSessEntryFCoENode)),
                              FsbCompareFipSessFCoEEntryIndex);

    if (gFsbGlobals.FsbFipSessFCoETable == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_DEBUGGING_LEVEL, FSB_OS_RESOURCE_TRC,
                         "FsbInit : RB Tree Creation for"
                         " FsbFipSessFCoETable failed\r\n");
        /* Release of above created resources are taken care in FsbDeInit() */
        RBTreeDelete (gFsbGlobals.FsbFipSnoopingTable);
        RBTreeDelete (gFsbGlobals.FsbIntfTable);
        RBTreeDelete (gFsbGlobals.FsbFipSessTable);
        return FSB_FAILURE;
    }
    /* Create RBTree to store tFsbFcfEntry */
    gFsbGlobals.FsbFcfTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tFsbFcfEntry,
                                              FsbFcfEntryNode)),
                              FsbCompareFcfEntryIndex);

    if (gFsbGlobals.FsbFcfTable == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CONTEXT_ID, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbInit : RB Tree Creation for"
                         " FsbFcfEntry failed\r\n");
        /* Release of above created resources are taken care in FsbDeInit() */
        RBTreeDelete (gFsbGlobals.FsbFipSnoopingTable);
        RBTreeDelete (gFsbGlobals.FsbIntfTable);
        RBTreeDelete (gFsbGlobals.FsbFipSessTable);
        RBTreeDelete (gFsbGlobals.FsbFipSessFCoETable);
        return FSB_FAILURE;
    }
    /* Create RBTree to store tFsbSChannelFilterEntry */
    gFsbGlobals.FsbSChannelFilterTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tFsbSChannelFilterEntry,
                                              FsbSChannelFilterNode)),
                              FsbCompareSChannelEntryIndex);

    if (gFsbGlobals.FsbSChannelFilterTable == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CONTEXT_ID, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbInit : RB Tree Creation for"
                         " FsbSChannelFilterEntry failed\r\n");
        /* Release of above created resources are taken care in FsbDeInit() */
        RBTreeDelete (gFsbGlobals.FsbFipSnoopingTable);
        RBTreeDelete (gFsbGlobals.FsbIntfTable);
        RBTreeDelete (gFsbGlobals.FsbFipSessTable);
        RBTreeDelete (gFsbGlobals.FsbFipSessFCoETable);
        RBTreeDelete (gFsbGlobals.FsbFcfTable);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDeInit                                        */
/*                                                                           */
/*    Description         : This function will perform deinitialization of   */
/*                          FSB Module                                       */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
FsbDeInit (VOID)
{
    /* Deletion of RBTree for FsbFipSnoopingTable */
    if (gFsbGlobals.FsbFipSnoopingTable != NULL)
    {
        RBTreeDelete (gFsbGlobals.FsbFipSnoopingTable);
    }

    /* Deletion of RBTree for FsbIntfTable */
    if (gFsbGlobals.FsbIntfTable != NULL)
    {
        RBTreeDelete (gFsbGlobals.FsbIntfTable);
    }

    /* Deletion of RBTree for FsbFipSessTable */
    if (gFsbGlobals.FsbFipSessTable != NULL)
    {
        RBTreeDelete (gFsbGlobals.FsbFipSessTable);
    }

    /* Deletion of RBTree for FsbFcfTable */
    if (gFsbGlobals.FsbFcfTable != NULL)
    {
        RBTreeDelete (gFsbGlobals.FsbFcfTable);
    }

    /* Deletion of RBTree for FsbSChannelFilterTable */
    if (gFsbGlobals.FsbSChannelFilterTable != NULL)
    {
        RBTreeDelete (gFsbGlobals.FsbSChannelFilterTable);
    }

    /* Deletion of Queue */
    if (FSB_QUEUE_ID != 0)
    {
        OsixQueDel (FSB_QUEUE_ID);
    }

    /* Deletion of Semaphore */
    if (FSB_SEM_ID != 0)
    {
        OsixSemDel (FSB_SEM_ID);
    }

    /* Deletion of MEM Pool */
    FsbSizingMemDeleteMemPools ();
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbAssignMempoolIds                              */
/*                                                                           */
/*    Description         : This function assign mempools from sz.c ie.      */
/*                          FsbMemPoolIds to global mempoolIds.              */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
FsbAssignMempoolIds (VOID)
{

    /*tFsbContextInfo */
    FSB_CONTEXT_MEMPOOL_ID = FSBMemPoolIds[MAX_FSB_CONTEXT_SIZING_ID];

    /* tFsbFipSnoopingEntry */
    FSB_FCOE_VLAN_ENTRIES_MEMPOOL_ID =
        FSBMemPoolIds[MAX_FSB_FCOE_VLAN_ENTRIES_SIZING_ID];

    /* tFsbIntfEntry */
    FSB_INTF_ENTRIES_MEMPOOL_ID = FSBMemPoolIds[MAX_FSB_INTF_ENTRIES_SIZING_ID];

    /* tFsbFipSessEntry */
    FSB_SESSION_ENTRIES_MEMPOOL_ID =
        FSBMemPoolIds[MAX_FSB_FIP_SESS_ENTRIES_SIZING_ID];

    /* tFsbFipSessFCoEEntry */
    FSB_FCOE_ENTRIES_MEMPOOL_ID = FSBMemPoolIds[MAX_FSB_FCOE_ENTRIES_SIZING_ID];

    /* tFCFEntry */
    FSB_FCF_ENTRIES_MEMPOOL_ID = FSBMemPoolIds[MAX_FSB_FCF_ENTRIES_SIZING_ID];

    /* tFsbNotifyParams */
    FSB_NOTIFY_PARAMS_INFO_MEMPOOL_ID =
        FSBMemPoolIds[MAX_FSB_NOTIFY_PARAMS_INFO_SIZING_ID];

    /* tMbsmProtoMsg */
    FSB_MBSM_MSG_MEMPOOL_ID = FSBMemPoolIds[MAX_FSB_MBSM_MSG_SIZING_ID];

    /* tFsbFilterEntry */
    FSB_FILTER_ENTRIES_MEMPOOL_ID =
        FSBMemPoolIds[MAX_FSB_FILTER_ENTRIES_SIZING_ID];

    /* tMacAddr */
    FSB_FCOE_MAC_ADDR_MEMPOOL_ID =
        FSBMemPoolIds[MAX_FSB_FCOE_MAC_COUNT_SIZING_ID];
    /* tFsbSChannelFilterEntry */
    FSB_SCHAN_FILT_ENTRIES_MEMPOOL_ID =
        FSBMemPoolIds[MAX_FSB_SCHANNEL_FILTER_ENTRIES_SIZING_ID];
}
#endif /* _FSBINIT_C_ */
