/*************************************************************************
* Copyright (C) 2007-2015 Aricent Group . All Rights Reserved
*
* $Id: fsbutil.c,v 1.18 2017/11/07 12:23:59 siva Exp $
*
* Description: This file contains the utility functions for FIp-snooping Module
*
*************************************************************************/
#ifndef _FSBUTIL_C_
#define _FSBUTIL_C_

#include "fsbinc.h"

static UINT1        gau1TxBuf[FSB_MAX_FIP_PKT_LEN];
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCompareFIPSnoopingEntryIndex                  */
/*                                                                           */
/*    Description         : This function compares FsbFipSnoopingEntry       */
/*                          based on Context Id and Vlan Index               */
/*                                                                           */
/*    Input(s)            : e1 - Pointer to First FsbFipSnoopingEntry        */
/*                          e2 - Pointer to Second FsbFipSnoopingEntry       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_GREATER - If key of first element is greater */
/*                                        than key of second element         */
/*                          FSB_EQUAL   - If key of first element is equal   */
/*                                        to key of second element           */
/*                          FSB_LESSER  - If key of first element is lesser  */
/*                                          than key of second element       */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCompareFIPSnoopingEntryIndex (tRBElem * e1, tRBElem * e2)
{
    tFsbFipSnoopingEntry *pRBNode1 = (tFsbFipSnoopingEntry *) e1;
    tFsbFipSnoopingEntry *pRBNode2 = (tFsbFipSnoopingEntry *) e2;

    if (pRBNode1->u4ContextId < pRBNode2->u4ContextId)
    {
        return FSB_LESSER;
    }
    else if (pRBNode1->u4ContextId > pRBNode2->u4ContextId)
    {
        return FSB_GREATER;
    }
    if (pRBNode1->u2VlanId < pRBNode2->u2VlanId)
    {
        return FSB_LESSER;
    }
    else if (pRBNode1->u2VlanId > pRBNode2->u2VlanId)
    {
        return FSB_GREATER;
    }
    return FSB_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCompareIntfEntryIndex                         */
/*                                                                           */
/*    Description         : This function compares FsbIntfEntry              */
/*                          based on VLAN Index and Interface Index          */
/*                                                                           */
/*    Input(s)            : e1 - Pointer to First FsbIntfEntry               */
/*                          e2 - Pointer to Second FsbIntfEntry              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_GREATER - If key of first element is greater */
/*                                        than key of second element         */
/*                          FSB_EQUAL   - If key of first element is equal   */
/*                                        to key of second element           */
/*                          FSB_LESSER  - If key of first element is lesser  */
/*                                          than key of second element       */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCompareIntfEntryIndex (tRBElem * e1, tRBElem * e2)
{
    tFsbIntfEntry      *pRBNode1 = (tFsbIntfEntry *) e1;
    tFsbIntfEntry      *pRBNode2 = (tFsbIntfEntry *) e2;

    if (pRBNode1->u2VlanId < pRBNode2->u2VlanId)
    {
        return FSB_LESSER;
    }
    else if (pRBNode1->u2VlanId > pRBNode2->u2VlanId)
    {
        return FSB_GREATER;
    }
    if (pRBNode1->u4IfIndex < pRBNode2->u4IfIndex)
    {
        return FSB_LESSER;
    }
    else if (pRBNode1->u4IfIndex > pRBNode2->u4IfIndex)
    {
        return FSB_GREATER;
    }
    return FSB_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCompareFipSessEntryIndex                      */
/*                                                                           */
/*    Description         : This function compares FipSessEntry              */
/*                          based on VLAN Index, Interface Index and         */
/*                          MAC Address                                      */
/*                                                                           */
/*    Input(s)            : e1 - Pointer to First FsbFipSessEntry            */
/*                          e2 - Pointer to Second FsbFipSessEntry           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_GREATER - If key of first element is greater */
/*                                        than key of second element         */
/*                          FSB_EQUAL   - If key of first element is equal   */
/*                                        to key of second element           */
/*                          FSB_LESSER  - If key of first element is lesser  */
/*                                          than key of second element       */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCompareFipSessEntryIndex (tRBElem * e1, tRBElem * e2)
{
    tFsbFipSessEntry   *pRBNode1 = (tFsbFipSessEntry *) e1;
    tFsbFipSessEntry   *pRBNode2 = (tFsbFipSessEntry *) e2;

    if (pRBNode1->u2VlanId < pRBNode2->u2VlanId)
    {
        return FSB_LESSER;
    }
    else if (pRBNode1->u2VlanId > pRBNode2->u2VlanId)
    {
        return FSB_GREATER;
    }
    if (pRBNode1->u4ENodeIfIndex < pRBNode2->u4ENodeIfIndex)
    {
        return FSB_LESSER;
    }
    else if (pRBNode1->u4ENodeIfIndex > pRBNode2->u4ENodeIfIndex)
    {
        return FSB_GREATER;
    }
    if (MEMCMP (pRBNode1->ENodeMacAddr, pRBNode2->ENodeMacAddr,
                sizeof (tMacAddr)) > 0)
    {
        return FSB_GREATER;
    }
    else if (MEMCMP (pRBNode1->ENodeMacAddr, pRBNode2->ENodeMacAddr,
                     sizeof (tMacAddr)) < 0)
    {
        return FSB_LESSER;
    }
    return FSB_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCompareFipSessFCoEEntryIndex                  */
/*                                                                           */
/*    Description         : This function compares FipSessFCoEEntry          */
/*                          based on VLAN Index, Interface Index and         */
/*                          FCoE and ENode MAC Address                       */
/*                                                                           */
/*    Input(s)            : e1 - Pointer to First FsbFipSessFCoEEntry        */
/*                          e2 - Pointer to Second FsbFipSessFCoEEntry       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_GREATER - If key of first element is greater */
/*                                        than key of second element         */
/*                          FSB_EQUAL   - If key of first element is equal   */
/*                                        to key of second element           */
/*                          FSB_LESSER  - If key of first element is lesser  */
/*                                          than key of second element       */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCompareFipSessFCoEEntryIndex (tRBElem * e1, tRBElem * e2)
{
    tFsbFipSessFCoEEntry *pRBNode1 = (tFsbFipSessFCoEEntry *) e1;
    tFsbFipSessFCoEEntry *pRBNode2 = (tFsbFipSessFCoEEntry *) e2;

    if (pRBNode1->u2VlanId < pRBNode2->u2VlanId)
    {
        return FSB_LESSER;
    }
    else if (pRBNode1->u2VlanId > pRBNode2->u2VlanId)
    {
        return FSB_GREATER;
    }
    if (pRBNode1->u4ENodeIfIndex < pRBNode2->u4ENodeIfIndex)
    {
        return FSB_LESSER;
    }
    else if (pRBNode1->u4ENodeIfIndex > pRBNode2->u4ENodeIfIndex)
    {
        return FSB_GREATER;
    }
    if (MEMCMP (pRBNode1->ENodeMacAddr, pRBNode2->ENodeMacAddr,
                sizeof (tMacAddr)) > 0)
    {
        return FSB_GREATER;
    }
    else if (MEMCMP (pRBNode1->ENodeMacAddr, pRBNode2->ENodeMacAddr,
                     sizeof (tMacAddr)) < 0)
    {
        return FSB_LESSER;
    }
    if (MEMCMP (pRBNode1->FcfMacAddr, pRBNode2->FcfMacAddr,
                sizeof (tMacAddr)) > 0)
    {
        return FSB_GREATER;
    }
    else if (MEMCMP (pRBNode1->FcfMacAddr, pRBNode2->FcfMacAddr,
                     sizeof (tMacAddr)) < 0)
    {
        return FSB_LESSER;
    }
    if (MEMCMP (pRBNode1->FCoEMacAddr, pRBNode2->FCoEMacAddr,
                sizeof (tMacAddr)) > 0)
    {
        return FSB_GREATER;
    }
    else if (MEMCMP (pRBNode1->FCoEMacAddr, pRBNode2->FCoEMacAddr,
                     sizeof (tMacAddr)) < 0)
    {
        return FSB_LESSER;
    }
    return FSB_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCompareFcfEntryIndex                          */
/*                                                                           */
/*    Description         : This function compares FcfEntry                  */
/*                          based on VLAN Index, Interface Index and         */
/*                          MAC Address                                      */
/*                                                                           */
/*    Input(s)            : e1 - Pointer to First FsbFsbFcfEntry             */
/*                          e2 - Pointer to Second FsbFsbFcfEntry            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_GREATER - If key of first element is greater */
/*                                        than key of second element         */
/*                          FSB_EQUAL   - If key of first element is equal   */
/*                                        to key of second element           */
/*                          FSB_LESSER  - If key of first element is lesser  */
/*                                          than key of second element       */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCompareFcfEntryIndex (tRBElem * e1, tRBElem * e2)
{
    tFsbFcfEntry       *pRBNode1 = (tFsbFcfEntry *) e1;
    tFsbFcfEntry       *pRBNode2 = (tFsbFcfEntry *) e2;

    if (pRBNode1->u2VlanId < pRBNode2->u2VlanId)
    {
        return FSB_LESSER;
    }
    else if (pRBNode1->u2VlanId > pRBNode2->u2VlanId)
    {
        return FSB_GREATER;
    }
    if (pRBNode1->u4FcfIfIndex < pRBNode2->u4FcfIfIndex)
    {
        return FSB_LESSER;
    }
    else if (pRBNode1->u4FcfIfIndex > pRBNode2->u4FcfIfIndex)
    {
        return FSB_GREATER;
    }
    if (MEMCMP (pRBNode1->FcfMacAddr, pRBNode2->FcfMacAddr,
                sizeof (tMacAddr)) > 0)
    {
        return FSB_GREATER;
    }
    else if (MEMCMP (pRBNode1->FcfMacAddr, pRBNode2->FcfMacAddr,
                     sizeof (tMacAddr)) < 0)
    {
        return FSB_LESSER;
    }
    return FSB_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCompareFipFilterEntryIndex                    */
/*                                                                           */
/*    Description         : This function compares FIP Filter Entry          */
/*                          based on MAC Address                             */
/*                                                                           */
/*    Input(s)            : e1 - Pointer to First FsbFilterEntry             */
/*                          e2 - Pointer to Second FsbFilterEntry            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_GREATER - If key of first element is greater */
/*                                        than key of second element         */
/*                          FSB_EQUAL   - If key of first element is equal   */
/*                                        to key of second element           */
/*                          FSB_LESSER  - If key of first element is lesser  */
/*                                          than key of second element       */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCompareFipFilterEntryIndex (tRBElem * e1, tRBElem * e2)
{
    tFsbFilterEntry    *pRBNode1 = (tFsbFilterEntry *) e1;
    tFsbFilterEntry    *pRBNode2 = (tFsbFilterEntry *) e2;

    if (MEMCMP (pRBNode1->DstMac, pRBNode2->DstMac, sizeof (tMacAddr)) < 0)
    {
        return FSB_LESSER;
    }
    else if (MEMCMP (pRBNode1->DstMac, pRBNode2->DstMac, sizeof (tMacAddr)) > 0)
    {
        return FSB_GREATER;
    }
    return FSB_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCompareSessFilterEntryIndex                   */
/*                                                                           */
/*    Description         : This function compares FIP Session Filter Entry  */
/*                          based on Source MAC, Dest MAC, Opcode and        */
/*                          SubOpcode                                        */
/*                                                                           */
/*    Input(s)            : e1 - Pointer to First FsbFilterEntry             */
/*                          e2 - Pointer to Second FsbFilterEntry            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_GREATER - If key of first element is greater */
/*                                        than key of second element         */
/*                          FSB_EQUAL   - If key of first element is equal   */
/*                                        to key of second element           */
/*                          FSB_LESSER  - If key of first element is lesser  */
/*                                          than key of second element       */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCompareSessFilterEntryIndex (tRBElem * e1, tRBElem * e2)
{
    tFsbFilterEntry    *pRBNode1 = (tFsbFilterEntry *) e1;
    tFsbFilterEntry    *pRBNode2 = (tFsbFilterEntry *) e2;

    if (MEMCMP (pRBNode1->SrcMac, pRBNode2->SrcMac, sizeof (tMacAddr)) < 0)
    {
        return FSB_LESSER;
    }
    else if (MEMCMP (pRBNode1->SrcMac, pRBNode2->SrcMac, sizeof (tMacAddr)) > 0)
    {
        return FSB_GREATER;
    }
    if (MEMCMP (pRBNode1->DstMac, pRBNode2->DstMac, sizeof (tMacAddr)) < 0)
    {
        return FSB_LESSER;
    }
    else if (MEMCMP (pRBNode1->DstMac, pRBNode2->DstMac, sizeof (tMacAddr)) > 0)
    {
        return FSB_GREATER;
    }
    if (pRBNode1->u2OpcodeFilterOffsetValue <
        pRBNode2->u2OpcodeFilterOffsetValue)
    {
        return FSB_LESSER;
    }
    else if (pRBNode1->u2OpcodeFilterOffsetValue >
             pRBNode2->u2OpcodeFilterOffsetValue)
    {
        return FSB_GREATER;
    }
    if (pRBNode1->u2SubOpcodeFilterOffsetValue <
        pRBNode2->u2SubOpcodeFilterOffsetValue)
    {
        return FSB_LESSER;
    }
    else if (pRBNode1->u2SubOpcodeFilterOffsetValue >
             pRBNode2->u2SubOpcodeFilterOffsetValue)
    {
        return FSB_GREATER;
    }
    return FSB_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCompareSChannelEntryIndex                     */
/*                                                                           */
/*    Description         : This function compares FsbSChannelFilterEntry    */
/*                          based on VLAN Index and Interface index          */
/*                                                                           */
/*    Input(s)            : e1 - Pointer to First FsbFsbFcfEntry             */
/*                          e2 - Pointer to Second FsbFsbFcfEntry            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_GREATER - If key of first element is greater */
/*                                        than key of second element         */
/*                          FSB_EQUAL   - If key of first element is equal   */
/*                                        to key of second element           */
/*                          FSB_LESSER  - If key of first element is lesser  */
/*                                          than key of second element       */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCompareSChannelEntryIndex (tRBElem * e1, tRBElem * e2)
{
    tFsbSChannelFilterEntry *pRBNode1 = (tFsbSChannelFilterEntry *) e1;
    tFsbSChannelFilterEntry *pRBNode2 = (tFsbSChannelFilterEntry *) e2;

    if (pRBNode1->u2VlanId < pRBNode2->u2VlanId)
    {
        return FSB_LESSER;
    }
    else if (pRBNode1->u2VlanId > pRBNode2->u2VlanId)
    {
        return FSB_GREATER;
    }
    if (pRBNode1->u4IfIndex < pRBNode2->u4IfIndex)
    {
        return FSB_LESSER;
    }
    else if (pRBNode1->u4IfIndex > pRBNode2->u4IfIndex)
    {
        return FSB_GREATER;
    }
    return FSB_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbUtilIsFsbStarted                              */
/*                                                                           */
/*    Description         : This function will check whether FSB is started  */
/*                          in the context                                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_TRUE / FSB_FALSE                             */
/*                                                                           */
/*****************************************************************************/
UINT1
FsbUtilIsFsbStarted (UINT4 u4ContextId)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    if (u4ContextId >= FSB_MAX_CONTEXT_COUNT)
    {
        return FSB_FALSE;
    }

    pFsbContextInfo = FsbCxtGetContextEntry (u4ContextId);

    if (pFsbContextInfo == NULL)
    {
        return FSB_FALSE;
    }

    if (gFsbGlobals.apFsbContextInfo[u4ContextId] == NULL)
    {
        /* FSB is not initialized in this context */
        return FSB_FALSE;
    }
    return ((pFsbContextInfo->u1SystemControl
             == FSB_START) ? FSB_TRUE : FSB_FALSE);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbGetFIPSnoopingEntry                           */
/*                                                                           */
/*    Description         : This function is used to get pointer to          */
/*                          FIP-snooping entry                               */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2VlanIndex - VLAN Index                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : pointer to tFsbFipSnoopingEntry                  */
/*                                                                           */
/*****************************************************************************/
tFsbFipSnoopingEntry *
FsbGetFIPSnoopingEntry (UINT4 u4ContextId, UINT2 u2VlanIndex)
{
    tFsbFipSnoopingEntry FsbFipSnoopingEntry;
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    FSB_MEMSET (&FsbFipSnoopingEntry, 0, sizeof (tFsbFipSnoopingEntry));

    FsbFipSnoopingEntry.u4ContextId = u4ContextId;
    FsbFipSnoopingEntry.u2VlanId = u2VlanIndex;

    pFsbFipSnoopingEntry = RBTreeGet (gFsbGlobals.FsbFipSnoopingTable,
                                      (tRBElem *) & FsbFipSnoopingEntry);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return NULL;
    }
    return pFsbFipSnoopingEntry;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbMbsmCompareIntfEntryIndex                     */
/*                                                                           */
/*    Description         : This function compares FsbIntfEntry              */
/*                          based on VLAN Index and Interface Index          */
/*                                                                           */
/*    Input(s)            : e1 - Pointer to First FsbIntfEntry               */
/*                          e2 - Pointer to Second FsbIntfEntry              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_GREATER - If key of first element is greater */
/*                                        than key of second element         */
/*                          FSB_EQUAL   - If key of first element is equal   */
/*                                        to key of second element           */
/*                          FSB_LESSER  - If key of first element is lesser  */
/*                                          than key of second element       */
/*                                                                           */
/*****************************************************************************/
INT4
FsbMbsmCompareIntfEntryIndex (tRBElem * e1, tRBElem * e2)
{
    tFsbIntfEntry      *pRBNode1 = (tFsbIntfEntry *) e1;
    tFsbIntfEntry      *pRBNode2 = (tFsbIntfEntry *) e2;

    if (pRBNode1->u4IfIndex < pRBNode2->u4IfIndex)
    {
        return FSB_LESSER;
    }
    else if (pRBNode1->u4IfIndex > pRBNode2->u4IfIndex)
    {
        return FSB_GREATER;
    }
    if (pRBNode1->u2VlanId < pRBNode2->u2VlanId)
    {
        return FSB_LESSER;
    }
    else if (pRBNode1->u2VlanId > pRBNode2->u2VlanId)
    {
        return FSB_GREATER;
    }
    return FSB_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbGetVlanPortsAndPopulateIntfEntry              */
/*                                                                           */
/*    Description         : This function is used to retreive PortList for   */
/*                          the vlan in the context from L2IWF and populate  */
/*                          tFsbIntfEntry                                    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2VlanIndex - VLAN Index                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbGetVlanPortsAndPopulateIntfEntry (UINT4 u4ContextId, UINT2 u2VlanIndex)
{
    tPortList          *pPortList = NULL;
    UINT4               u4LocalPort = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;

    pPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPortList == NULL)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbGetVlanPortsAndPopulateIntfEntry: FsUtilAllocBitList failed\r\n");
        return FSB_FAILURE;
    }

    FSB_MEMSET (*pPortList, 0, sizeof (tPortList));

    /* Get the list of ports from L2IWF */
    if (FsbPortGetVlanLocalEgressPorts (u4ContextId, u2VlanIndex, *pPortList)
        != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbGetVlanPortsAndPopulateIntfEntry : "
                         "FsbPortGetVlanLocalEgressPorts with VlanId: %d "
                         "failed\r\n", u2VlanIndex);
        FsUtilReleaseBitList (*pPortList);
        return FSB_FAILURE;
    }

    /* Interface index will be returned as portlist from L2IWF.
     * The following loop is to obtain the exact list of ports from the returned
     * portlist*/
    for (u2ByteIndex = 0; u2ByteIndex < BRG_PORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = *(*pPortList + u2ByteIndex);
        for (u2BitIndex = 0; ((u2BitIndex < VLAN_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & VLAN_BIT8) != 0)
            {
                u4LocalPort = (UINT4) ((u2ByteIndex * VLAN_PORTS_PER_BYTE) +
                                       u2BitIndex + 1);
                /* After obtaining the interface index, populate the 
                 * interface table with Port Index and Vlan Index*/
                FsbPopulateIntfEntry (u4ContextId, u2VlanIndex, u4LocalPort);
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    FsUtilReleaseBitList (*pPortList);
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbValidatePortEntryInFcoeVlan                   */
/*                                                                           */
/*    Description         : This function is used to validate the based on   */
/*                          the below set of conditions                      */
/*                          1) If the Vlan has ports mapped to ports         */
/*                          2) If the Oper Status of the Ports are up        */
/*                          These conditions are placed to ensure the sucess */
/*                          in ACL Filter installation                       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2VlanIndex - VLAN Index                         */
/*                                                                           */
/*    Output(s)           : pu4ErrorCode - Error Code                        */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbValidatePortEntryInFcoeVlan (UINT4 *pu4ErrorCode,
                                UINT4 u4ContextId, UINT2 u2VlanIndex)
{
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbIntfEntry       FsbIntfEntry;
    UINT4               u4PortCount = 0;

    FSB_MEMSET (&FsbIntfEntry, 0, sizeof (tFsbIntfEntry));

    pFsbIntfEntry = (tFsbIntfEntry *) RBTreeGetFirst (gFsbGlobals.FsbIntfTable);

    if (pFsbIntfEntry == NULL)
    {
        /* Interface Table is empty, meaning no ports are mapped
         * to Vlan. Therefore, return failure */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_FSB_ERR_NO_PORT_IN_VLAN);
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbValidatePortEntryInFcoeVlan: pFsbIntfEntry is NULL ... \r\n");
        return FSB_FAILURE;
    }

    do
    {
        /* If the Context Id and Vlan Id Matches */
        if ((pFsbIntfEntry->u4ContextId == u4ContextId) &&
            (pFsbIntfEntry->u2VlanId == u2VlanIndex))
        {
            u4PortCount++;
        }

        /* GetNext Entry */
        FsbIntfEntry.u2VlanId = pFsbIntfEntry->u2VlanId;
        FsbIntfEntry.u4IfIndex = pFsbIntfEntry->u4IfIndex;

        pFsbIntfEntry = (tFsbIntfEntry *)
            RBTreeGetNext (gFsbGlobals.FsbIntfTable,
                           (tRBElem *) & FsbIntfEntry, NULL);

    }
    while (pFsbIntfEntry != NULL);

    /* Incase No ports are mapped to the vlan  */
    if (u4PortCount == 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_FSB_ERR_NO_PORT_IN_VLAN);
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbValidatePortEntryInFcoeVlan: No ports "
                         "are mapped to vlan :%d ... \r\n", u2VlanIndex);
        return FSB_FAILURE;
    }

    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDeleteFipSnoopingEntry                        */
/*                                                                           */
/*    Description         : This function is used to delete FipSnoopingEntry */
/*                          tFsbIntfEntry for the context and vlan           */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2VlanIndex - VLAN Index                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbDeleteFipSnoopingEntry (UINT4 u4ContextId, UINT2 u2VlanIndex)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4ContextId, u2VlanIndex);

    if (pFsbFipSnoopingEntry != NULL)
    {
        if (FsbRemoveDefaultFilter (pFsbFipSnoopingEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                             "\r\nFsbDeleteFipSnoopingEntry: FsbRemoveDefaultFilter "
                             "with VlanId: %d returns FAILURE... \r\n",
                             pFsbFipSnoopingEntry->u2VlanId);
            return FSB_FAILURE;
        }
        if (pFsbFipSnoopingEntry->FsbFilterEntry != NULL)
        {
            gFsbGlobals.u4FsbFilterEntryRBCount--;
            RBTreeDelete (pFsbFipSnoopingEntry->FsbFilterEntry);
            pFsbFipSnoopingEntry->FsbFilterEntry = NULL;
        }

        if (RBTreeRemove (gFsbGlobals.FsbFipSnoopingTable,
                          (tRBElem *) pFsbFipSnoopingEntry) != RB_SUCCESS)
        {
            FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                             "\r\nFsbDeleteFipSnoopingEntry: RBTree Remove of "
                             "FipSnooping Entry with VlanId: %d failed... \r\n",
                             pFsbFipSnoopingEntry->u2VlanId);
            return FSB_FAILURE;
        }
        MemReleaseMemBlock (FSB_FCOE_VLAN_ENTRIES_MEMPOOL_ID,
                            (UINT1 *) pFsbFipSnoopingEntry);
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbPopulateIntfEntry                             */
/*                                                                           */
/*    Description         : This function is used to initialise              */
/*                          tFsbIntfEntry for the context and vlan           */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2VlanIndex - VLAN Index                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbPopulateIntfEntry (UINT4 u4ContextId, UINT2 u2VlanIndex, UINT4 u4IfIndex)
{
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbCallBackArgs    FsbCallBackArgs;
    tFsbSChannelFilterEntry *pFsbSChannelFilterEntry = NULL;
    tFsbSChannelFilterEntry FsbSChannelFilterEntry;

    INT4                i4BrgPortType = 0;
    UINT4               u4UapIfIndex = 0;
    UINT4               u4HwFilterId = 0;
    UINT2               u2SVID = 0;

    MEMSET (&FsbCallBackArgs, 0, sizeof (tFsbCallBackArgs));

    pFsbIntfEntry = (tFsbIntfEntry *) MemAllocMemBlk
        (FSB_INTF_ENTRIES_MEMPOOL_ID);

    if (pFsbIntfEntry == NULL)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                         "FsbPopulateIntfEntry : pFsbIntfEntry mem alloc failed\r\n");
        return FSB_FAILURE;
    }

    FSB_MEMSET (pFsbIntfEntry, 0, sizeof (tFsbIntfEntry));
    FSB_MEMSET (&FsbSChannelFilterEntry, 0, sizeof (tFsbSChannelFilterEntry));

    pFsbIntfEntry->u4ContextId = u4ContextId;
    pFsbIntfEntry->u2VlanId = u2VlanIndex;    /* RBTree Index */
    pFsbIntfEntry->u4IfIndex = u4IfIndex;    /* RBTree Index */
    pFsbIntfEntry->u1PortRole = FSB_DEFAULT_PORT_ROLE;
    pFsbIntfEntry->u1RowStatus = ACTIVE;

    /* Populate the contents and add it in the RB Tree */
    if (RBTreeAdd (gFsbGlobals.FsbIntfTable,
                   (tRBElem *) pFsbIntfEntry) != RB_SUCCESS)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbPopulateIntfEntry : RBTree Add of pFsbIntfEntry with "
                         "VlanId: %d and IfIndex: %d failed\r\n", u2VlanIndex,
                         u4IfIndex);
        MemReleaseMemBlock (FSB_INTF_ENTRIES_MEMPOOL_ID,
                            (UINT1 *) pFsbIntfEntry);
        return FSB_FAILURE;
    }

    /*Call Back function to set the MTU for all the ports 
     * of Fip Snooping enabled Vlan */
    FsbCfaGetInterfaceBrgPortType (u4IfIndex, &i4BrgPortType);

    if (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        if (FsbVlanApiGetSChInfoFromSChIfIndex (u4IfIndex,
                                                &u4UapIfIndex,
                                                &u2SVID) != FSB_FAILURE)
        {
            u4IfIndex = u4UapIfIndex;
        }
    }
    FsbCallBackArgs.u4IfIndex = u4IfIndex;
    FsbCustCallBack (FSB_SET_IF_MTU_EVENT, &FsbCallBackArgs);

    if (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        FsbSChannelFilterEntry.u4IfIndex = u4UapIfIndex;
        FsbSChannelFilterEntry.u2VlanId = u2SVID;

        pFsbSChannelFilterEntry =
            (RBTreeGet
             (gFsbGlobals.FsbSChannelFilterTable,
              (tRBElem *) & FsbSChannelFilterEntry));

        if (pFsbSChannelFilterEntry == NULL)
        {
            pFsbSChannelFilterEntry = (tFsbSChannelFilterEntry *) MemAllocMemBlk
                (FSB_SCHAN_FILT_ENTRIES_MEMPOOL_ID);

            if (pFsbSChannelFilterEntry == NULL)
            {
                FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL,
                                 FSB_BUFFER_TRC,
                                 "FsbPopulateIntfEntry : pFsbSChannelFilterEntry mem alloc failed\r\n");
                return FSB_FAILURE;
            }
            FSB_MEMSET (pFsbSChannelFilterEntry, 0,
                        sizeof (tFsbSChannelFilterEntry));

            pFsbSChannelFilterEntry->u4ContextId = u4ContextId;
            pFsbSChannelFilterEntry->u4IfIndex = u4UapIfIndex;
            pFsbSChannelFilterEntry->u2VlanId = u2SVID;
            pFsbSChannelFilterEntry->u1Priority = FSB_FIELD_ENTRY_PRIO_HIGHEST;
            pFsbSChannelFilterEntry->u4FilterId = FSB_GET_FILTER_ID ();

            /* Program the hardware */
            if (FsFsbHwWrCreateSChannelFilter
                (pFsbSChannelFilterEntry, &u4HwFilterId) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbIntfEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbPopulateIntfEntry:"
                                 " FsFsbHwWrCreateSChannelFilter  with VlanId: %d and IfIndex: %d"
                                 " failed \r\n",
                                 pFsbSChannelFilterEntry->u2VlanId,
                                 pFsbSChannelFilterEntry->u4IfIndex);
                MemReleaseMemBlock (FSB_SCHAN_FILT_ENTRIES_MEMPOOL_ID,
                                    (UINT1 *) pFsbSChannelFilterEntry);
                return FSB_FAILURE;
            }
            pFsbSChannelFilterEntry->u4HwFilterId = u4HwFilterId;

            if (RBTreeAdd (gFsbGlobals.FsbSChannelFilterTable,
                           (tRBElem *) pFsbSChannelFilterEntry) != RB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbIntfEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbPopulateIntfEntry:"
                                 " RBTreeAdd of SChannelFilterEntry with VlanId: %d and IfIndex: %d"
                                 " failed \r\n",
                                 pFsbSChannelFilterEntry->u2VlanId,
                                 pFsbSChannelFilterEntry->u4IfIndex);
                MemReleaseMemBlock (FSB_SCHAN_FILT_ENTRIES_MEMPOOL_ID,
                                    (UINT1 *) pFsbSChannelFilterEntry);
                return FSB_FAILURE;
            }
            if (FsbRedSyncUpSChannelFilterId
                (pFsbSChannelFilterEntry->u4IfIndex,
                 pFsbSChannelFilterEntry->u2VlanId,
                 pFsbSChannelFilterEntry->u4HwFilterId) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbSChannelFilterEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbPopulateIntfEntry: "
                                 "FsbRedSyncUpSChannelFilterId of SChannelFilterEntry with VlanId: %d "
                                 "and IfIndex: %d failed\r\n",
                                 pFsbSChannelFilterEntry->u2VlanId,
                                 pFsbSChannelFilterEntry->u4IfIndex);
                return FSB_FAILURE;
            }
        }
    }
    else
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                         FSB_CONTROL_PATH_TRC,
                         "FsbPopulateIntfEntry: S-channel entry already present for IfIndex: %d, vlan id: %d",
                         u4IfIndex, u2VlanIndex);
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbRemoveIntfEntryForFcoeVlan                    */
/*                                                                           */
/*    Description         : This function is used to remove                  */
/*                          tFsbIntfEntry for the context and vlan           */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2VlanIndex - VLAN Index                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbRemoveIntfEntryForFcoeVlan (UINT4 u4ContextId, UINT2 u2VlanIndex)
{
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbIntfEntry       FsbIntfEntry;

    FSB_MEMSET (&FsbIntfEntry, 0, sizeof (tFsbIntfEntry));

    /* Walk through the interface table and delete
     * the entries matching with the incoming VLAN and context ID*/
    pFsbIntfEntry = (tFsbIntfEntry *) RBTreeGetFirst (gFsbGlobals.FsbIntfTable);
    if (pFsbIntfEntry == NULL)
    {
        /* If the vlan doesnot have any ports mapped to it,
         * then success should be returned as this a valid 
         * scenario*/
        return FSB_SUCCESS;
    }
    do
    {
        FsbIntfEntry.u2VlanId = pFsbIntfEntry->u2VlanId;
        FsbIntfEntry.u4IfIndex = pFsbIntfEntry->u4IfIndex;

        if ((pFsbIntfEntry->u4ContextId == u4ContextId)
            && (pFsbIntfEntry->u2VlanId == u2VlanIndex))
        {
            /* Matching entry found, delete the interface entry */
            FsbDeleteIntfEntry (u2VlanIndex, pFsbIntfEntry->u4IfIndex);
        }

        pFsbIntfEntry = (tFsbIntfEntry *)
            RBTreeGetNext (gFsbGlobals.FsbIntfTable,
                           (tRBElem *) & FsbIntfEntry, NULL);
    }
    while (pFsbIntfEntry != NULL);

    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbGetIntfCountForFcoeVlan                       */
/*                                                                           */
/*    Description         : This function is used to get number of           */
/*                          interfaces in the given vlan                     */
/*                                                                           */
/*    Input(s)            : u2VlanIndex - VLAN Index                         */
/*                                                                           */
/*    Output(s)           : pu4IntfCount - Interface Count                   */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbGetIntfCountForFcoeVlan (UINT2 u2VlanIndex, UINT4 *pu4IntfCount)
{
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbIntfEntry       FsbIntfEntry;
    UINT4               u4Count = 0;

    FSB_MEMSET (&FsbIntfEntry, 0, sizeof (tFsbIntfEntry));
    *pu4IntfCount = 0;

    /* Walk through the interface table get the interface count 
     *  for the incoming VLAN */
    pFsbIntfEntry = (tFsbIntfEntry *) RBTreeGetFirst (gFsbGlobals.FsbIntfTable);
    if (pFsbIntfEntry == NULL)
    {
        /* If the vlan doesnot have any ports mapped to it,
         * then success should be returned as this a valid 
         * scenario*/
        return FSB_SUCCESS;
    }
    do
    {
        FsbIntfEntry.u2VlanId = pFsbIntfEntry->u2VlanId;
        FsbIntfEntry.u4IfIndex = pFsbIntfEntry->u4IfIndex;

        if (pFsbIntfEntry->u2VlanId == u2VlanIndex)
        {
            /* Matching entry found, increment the counter */
            u4Count++;
        }

        pFsbIntfEntry = (tFsbIntfEntry *)
            RBTreeGetNext (gFsbGlobals.FsbIntfTable,
                           (tRBElem *) & FsbIntfEntry, NULL);
    }
    while (pFsbIntfEntry != NULL);
    *pu4IntfCount = u4Count;

    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDeleteIntfEntry                               */
/*                                                                           */
/*    Description         : This function is used to remove tFsbIntfEntry    */
/*                                                                           */
/*    Input(s)            : u2VlanIndex - VLAN Index                         */
/*                          u4IfIndex - Interface Index                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbDeleteIntfEntry (UINT2 u2VlanIndex, UINT4 u4IfIndex)
{
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbSChannelFilterEntry *pFsbSChannelFilterEntry = NULL;
    tFsbSChannelFilterEntry FsbSChannelFilterEntry;

    UINT4               u4Count = 0;
    INT4                i4BrgPortType = 0;
    UINT4               u4UapIfIndex = 0;
    UINT2               u2SVID = 0;

    FSB_MEMSET (&FsbSChannelFilterEntry, 0, sizeof (tFsbSChannelFilterEntry));

    pFsbIntfEntry = FsbGetFsbIntfEntry (u2VlanIndex, u4IfIndex);

    if (pFsbIntfEntry != NULL)
    {
        if (FsbGetIntfCountForFcoeVlan (u2VlanIndex, &u4Count) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbIntfEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE, "FsbDeleteIntfEntry: "
                             "FsbGetIntfCountForFcoeVlan with VlanId: %d "
                             "and IfIndex: %d failed\r\n",
                             pFsbIntfEntry->u2VlanId, pFsbIntfEntry->u4IfIndex);
            return FSB_FAILURE;
        }
        if (u4Count == 1)
        {
            pFsbFipSnoopingEntry =
                FsbGetFIPSnoopingEntry (pFsbIntfEntry->u4ContextId,
                                        pFsbIntfEntry->u2VlanId);
            if (pFsbFipSnoopingEntry != NULL)
            {
                if (FsbRemoveDefaultFilter (pFsbFipSnoopingEntry) !=
                    FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (pFsbIntfEntry->u4ContextId,
                                     FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbDeleteIntfEntry: "
                                     "FsbRemoveDefaultFilter with VlanId: %d "
                                     "and IfIndex: %d failed\r\n",
                                     pFsbIntfEntry->u2VlanId,
                                     pFsbIntfEntry->u4IfIndex);
                    return FSB_FAILURE;
                }
            }
        }
        /* Matching entry found, delete the interface entry */
        FsbCfaGetInterfaceBrgPortType (pFsbIntfEntry->u4IfIndex,
                                       &i4BrgPortType);
        if (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
        {
            if (FsbVlanApiGetSChInfoFromSChIfIndex (pFsbIntfEntry->u4IfIndex,
                                                    &u4UapIfIndex,
                                                    &u2SVID) != FSB_FAILURE)
            {
                FsbSChannelFilterEntry.u4IfIndex = u4UapIfIndex;
                FsbSChannelFilterEntry.u2VlanId = u2SVID;

                pFsbSChannelFilterEntry =
                    RBTreeGet (gFsbGlobals.FsbSChannelFilterTable,
                               (tRBElem *) & FsbSChannelFilterEntry);

                if (pFsbSChannelFilterEntry != NULL)
                {

                    if (FsFsbHwWrDeleteSChannelFilter (pFsbSChannelFilterEntry,
                                                       pFsbSChannelFilterEntry->
                                                       u4HwFilterId) !=
                        FSB_SUCCESS)
                    {
                        FSB_CONTEXT_TRC (pFsbSChannelFilterEntry->u4ContextId,
                                         FSB_ERROR_LEVEL, FSB_NONE,
                                         "FsbDeleteIntfEntry:"
                                         " ACL Filter Deletion with VlanId: %d and IfIndex: %d failed \r\n",
                                         pFsbSChannelFilterEntry->u2VlanId,
                                         pFsbSChannelFilterEntry->u4IfIndex);
                    }
                    else
                    {
                        if (RBTreeRemove (gFsbGlobals.FsbSChannelFilterTable,
                                          (tRBElem *) pFsbSChannelFilterEntry)
                            != RB_SUCCESS)
                        {
                            FSB_CONTEXT_TRC (pFsbSChannelFilterEntry->
                                             u4ContextId, FSB_ERROR_LEVEL,
                                             FSB_NONE,
                                             "FsbDeleteIntfEntry:"
                                             "RBTreeRemove failed for SChannel Filter with VlanId: %d"
                                             " and IfIndex: %d\r\n",
                                             pFsbSChannelFilterEntry->u2VlanId,
                                             pFsbSChannelFilterEntry->
                                             u4IfIndex);
                            return FSB_FAILURE;
                        }
                        MemReleaseMemBlock (FSB_SCHAN_FILT_ENTRIES_MEMPOOL_ID,
                                            (UINT1 *) pFsbSChannelFilterEntry);
                    }
                }
            }
        }
        if (RBTreeRemove (gFsbGlobals.FsbIntfTable,
                          (tRBElem *) pFsbIntfEntry) == RB_FAILURE)
        {
            FSB_CONTEXT_TRC (pFsbIntfEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE, "FsbDeleteIntfEntry: "
                             "RBTree Remove of IntfEntry with VlanId: %d "
                             "and IfIndex: %d failed\r\n",
                             pFsbIntfEntry->u2VlanId, pFsbIntfEntry->u4IfIndex);
            return FSB_FAILURE;
        }
        MemReleaseMemBlock (FSB_INTF_ENTRIES_MEMPOOL_ID,
                            (UINT1 *) pFsbIntfEntry);
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbGetFsbIntfEntry                               */
/*                                                                           */
/*    Description         : This function return pointer to tFsbIntfEntry    */
/*                          if exists for the given VLAN Index and Interface */
/*                                                                           */
/*    Input(s)            : u2VlanIndex - VLAN Index                         */
/*                          u4IfIndex - Interace Index                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : Pointer to tFsbIntfEntry                         */
/*                                                                           */
/*****************************************************************************/
tFsbIntfEntry      *
FsbGetFsbIntfEntry (UINT2 u2VlanIndex, UINT4 u4IfIndex)
{
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbIntfEntry       FsbIntfEntry;

    FSB_MEMSET (&FsbIntfEntry, 0, sizeof (tFsbIntfEntry));

    FsbIntfEntry.u2VlanId = u2VlanIndex;
    FsbIntfEntry.u4IfIndex = u4IfIndex;

    pFsbIntfEntry = (RBTreeGet (gFsbGlobals.FsbIntfTable,
                                (tRBElem *) & FsbIntfEntry));

    if (pFsbIntfEntry == NULL)
    {
        return NULL;
    }
    return pFsbIntfEntry;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbPopulateDefFilterAndIntfEntry                 */
/*                                                                           */
/*    Description         : This function is used to populate defualt filter */
/*                          entry data structure and the interface entry     */
/*                          data structure                                   */
/*                                                                           */
/*    Input(s)            : u2VlanId - VLAN Id                               */
/*                          u4IfIndex - Interface Index                      */
/*                          u4ContextId - Context Id                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbPopulateDefFilterAndIntfEntry (UINT4 u4ContextId, UINT2 u2VlanId,
                                  UINT4 u4IfIndex)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    UINT2               u2FilTypeIndex = 0;

    /* Populate the interface entry */
    FsbPopulateIntfEntry (u4ContextId, u2VlanId, u4IfIndex);

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4ContextId, u2VlanId);

    /* If the filter entries are not present, 
     * add the default filter entries. As the filter entries
     * might have been deleted, when there is no ports
     * present in the VLAN */
    if (pFsbFipSnoopingEntry != NULL)
    {
        pFsbFilterEntry = (tFsbFilterEntry *)
            RBTreeGetFirst (pFsbFipSnoopingEntry->FsbFilterEntry);
        if (pFsbFilterEntry == NULL)
        {
            pFsbFipSnoopingEntry->FsbFilterEntry =
                RBTreeCreateEmbedded ((FSAP_OFFSETOF (tFsbFilterEntry,
                                                      FsbFilterNode)),
                                      FsbCompareFipFilterEntryIndex);

            for (u2FilTypeIndex = FSB_ALLOW_DIS_SOLICITATION;
                 u2FilTypeIndex <= FSB_GLOBAL_DENY; u2FilTypeIndex++)
            {
                if (FsbInstallDefaultFilter
                    (u2FilTypeIndex, pFsbFipSnoopingEntry) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                                     FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbPopulateDefFilterAndIntfEntry:"
                                     "FsbInstallDefaultFilter failed\r\n");
                    return FSB_FAILURE;
                }
                if (FsbRedSyncUpDefaultFilter (pFsbFipSnoopingEntry) !=
                    FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                                     FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbPopulateDefFilterAndIntfEntry:"
                                     "FsbRedSyncUpDefaultFilter failed\r\n");
                    return FSB_FAILURE;
                }
            }
        }
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbHandleFIPVlanPackets                          */
/*                                                                           */
/*    Description         : This function used to handle the FIP VLAN        */
/*                          packets                                          */
/*                                                                           */
/*    Input(s)            : pFSBPktInfo Pointer to tFSBPktInfo               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/

INT4
FsbHandleFIPVlanPackets (tFSBPktInfo * pFSBPktInfo)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (pFSBPktInfo->u4ContextId);

    if (pFsbContextInfo != NULL)
    {
        if (pFSBPktInfo->PacketType == FSB_FIP_VLAN_DISCOVERY)
        {
            pFsbContextInfo->u4VlanRequestCount++;
        }
        else if (pFSBPktInfo->PacketType == FSB_FIP_VLAN_NOTIFICATION)
        {
            pFsbContextInfo->u4VlanNotificationCount++;
        }
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsbUtilContextTrc                                */
/*                                                                           */
/*    Description         : This function prints the context traces          */
/*                                                                           */
/*    Input(s)            : Context Identifier,Trace Mask                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
FsbUtilContextTrc (UINT4 u4ContextId, UINT4 u4TraceSeverityLevel, UINT4 u4Mask,
                   const char *fmt, ...)
{
    va_list             ap;
    CHR1                ac1Buf[FSB_TRC_MAX_SIZE];
    UINT4               u4OffSet = 0;

    FSB_MEMSET (ac1Buf, 0, sizeof (ac1Buf));

    if (FsbUtilIsFsbStarted (u4ContextId) != FSB_TRUE)
    {
        return;
    }

    SPRINTF (ac1Buf, "[Switch:%d]", u4ContextId);

    u4OffSet = STRLEN (ac1Buf);

    va_start (ap, fmt);
    vsprintf (&ac1Buf[u4OffSet], fmt, ap);
    va_end (ap);

    if (u4TraceSeverityLevel <= FSB_TRC_LEVEL_FLAG (u4ContextId))
    {
        UtlTrcLog (FSB_MODULE_TRACE (u4ContextId), u4Mask,
                   FSB_MODULE_NAME, ac1Buf);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbUpdateVLANStatistics                          */
/*                                                                           */
/*    Description         : This function used to update FIP VLAN            */
/*                          statistics                                       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2VlanId    - VLAN Id                            */
/*                          u1PacketType - Packet Type                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbUpdateVLANStatistics (UINT4 u4ContextId, UINT2 u2VlanId, UINT1 u1PacketType)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4ContextId, u2VlanId);
    if (pFsbFipSnoopingEntry == NULL)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "\r\nFsbUpdateVLANStatistics: FsbGetFIPSnoopingEntry returns NULL ... \r\n");
        return FSB_FAILURE;
    }

    switch (u1PacketType)
    {
        case FSB_FIP_SOLICIT_UCAST:
            pFsbFipSnoopingEntry->u4UnicastDisSolCount++;
            break;
        case FSB_FIP_SOLICIT_MCAST:
            pFsbFipSnoopingEntry->u4MulticastDisSolCount++;
            break;
        case FSB_FIP_ADV_UCAST:
            pFsbFipSnoopingEntry->u4UnicastDisAdvCount++;
            break;
        case FSB_FIP_ADV_MCAST:
            pFsbFipSnoopingEntry->u4MulticastDisAdvCount++;
            break;
        case FSB_FIP_FLOGI_REQUEST:
            pFsbFipSnoopingEntry->u4FLOGICount++;
            break;
        case FSB_FIP_NPIV_FDISC_REQUEST:
            pFsbFipSnoopingEntry->u4FDISCCount++;
            break;
        case FSB_FIP_LOGO_REQUEST:
            pFsbFipSnoopingEntry->u4LOGOCount++;
            break;
        case FSB_FIP_FLOGI_ACCEPT:
            pFsbFipSnoopingEntry->u4FLOGIAcceptCount++;
            break;
        case FSB_FIP_FLOGI_REJECT:
            pFsbFipSnoopingEntry->u4FLOGIRejectCount++;
            break;
        case FSB_FIP_NPIV_FDISC_ACCEPT:
            pFsbFipSnoopingEntry->u4FDISCAcceptCount++;
            break;
        case FSB_FIP_NPIV_FDISC_REJECT:
            pFsbFipSnoopingEntry->u4FDISCRejectCount++;
            break;
        case FSB_FIP_LOGO_ACCEPT:
            pFsbFipSnoopingEntry->u4LOGOAcceptCount++;
            break;
        case FSB_FIP_LOGO_REJECT:
            pFsbFipSnoopingEntry->u4LOGORejectCount++;
            break;
        case FSB_FIP_CLEAR_LINK:
            pFsbFipSnoopingEntry->u4ClearLinkCount++;
            break;
        case FSB_FCMAP_MISMATCH_STATS:
            pFsbFipSnoopingEntry->u4FcMapMisMatchCount++;
            break;
        case FSB_MTU_MISMATCH_STATS:
            pFsbFipSnoopingEntry->u4MTUMisMatchCount++;
            break;
        case FSB_ACL_FAILURE_STATS:
            pFsbFipSnoopingEntry->u4ACLFailureCount++;
            break;
        case FSB_INVAILD_FIP_FRAMES:
            pFsbFipSnoopingEntry->u4InvalidFIPFramesCount++;
            break;
        case FSB_FCF_DISCOVERY_TIMEOUT:
            pFsbFipSnoopingEntry->u4FCFDiscoveryTimeoutsCount++;
            break;
        default:
            break;
    }
    return FSB_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbUtilPopulatePortList                          */
/*                                                                           */
/*    Description         : This function is used to populate                */
/*                          Port List                                        */
/*                                                                           */
/*    Input(s)            : u2VlanId - VLAN Id                               */
/*                          u4ContextId - Context Id                         */
/*                          u1PortRole - Port Role                           */
/*                                                                           */
/*    Output(s)           : pPortList - Port List                            */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbUtilPopulatePortList (UINT2 u2VlanId, UINT4 u4ContextId,
                         UINT1 u1PortRole, tPortList pPortList)
{
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbIntfEntry      *pGetFsbIntfEntry = NULL;
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbIntfEntry       FsbIntfEntry;
    INT4                i4BrgPortType = 0;
    UINT4               u4UapIfIndex = 0;
    UINT2               u2SVID = 0;
    UINT2               au2ConfPorts[L2IWF_MAX_PORTS_PER_CONTEXT];
    UINT2               u2Index = 0;
    UINT2               u2NumPorts = 0;
    UINT1               u1IfType = 0;

    FSB_MEMSET (au2ConfPorts, 0, sizeof (au2ConfPorts));
    FSB_MEMSET (&FsbIntfEntry, 0, sizeof (tFsbIntfEntry));

    pFsbIntfEntry = (tFsbIntfEntry *) RBTreeGetFirst (gFsbGlobals.FsbIntfTable);
    if (pFsbIntfEntry == NULL)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL, FSB_OS_RESOURCE_TRC,
                         "FsbUtilPopulatePortList: RBTreeGetFirst failed\r\n");
        return FSB_FAILURE;
    }
    /* Below loop is to scan IntfEntry Table and populate the portlist only
     * if the given port role and port role in IntftTable are same or 
     * the given port role is both FCF and ENode facing */
    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4ContextId, u2VlanId);

    if (pFsbFipSnoopingEntry == NULL)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbUtilPopulatePortList: FsbGetFIPSnoopingEntry failed\r\n");
        return FSB_FAILURE;
    }
    if ((pFsbFipSnoopingEntry->u4PinnedPortCount != 0) &&
        (u1PortRole == FSB_FCF_FACING))
    {
        FSB_MEMCPY (pPortList,
                    pFsbFipSnoopingEntry->FsbPinnedPorts, BRG_PORT_LIST_SIZE);
        return FSB_SUCCESS;
    }
    do
    {
        FsbIntfEntry.u2VlanId = pFsbIntfEntry->u2VlanId;
        FsbIntfEntry.u4IfIndex = pFsbIntfEntry->u4IfIndex;

        pGetFsbIntfEntry = RBTreeGet (gFsbGlobals.FsbIntfTable,
                                      (tRBElem *) & FsbIntfEntry);
        if (pGetFsbIntfEntry == NULL)
        {
            FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL,
                             FSB_OS_RESOURCE_TRC,
                             "FsbUtilPopulatePortList: RBTreeGet failed\r\n");
            return FSB_FAILURE;
        }
        if ((pGetFsbIntfEntry->u2VlanId == u2VlanId) &&
            (pGetFsbIntfEntry->u4ContextId == u4ContextId))
        {
            if ((pGetFsbIntfEntry->u1PortRole == u1PortRole) ||
                (pGetFsbIntfEntry->u1PortRole == FSB_ENODE_FCF_FACING))
            {
                /* Retrieve the interface type */
                FsbCfaGetIfType (pGetFsbIntfEntry->u4IfIndex, &u1IfType);
                FsbCfaGetInterfaceBrgPortType (pGetFsbIntfEntry->u4IfIndex,
                                               &i4BrgPortType);

                /* If it is an LAG interface, set the portlist with
                 * all the configured ports in the port-channel*/
                if (u1IfType == CFA_LAGG)
                {
                    /* Retrieve the configured ports in the port-channel */
                    if (FsbL2IwfGetConfiguredPortsForPortChannel ((UINT2)
                                                                  pGetFsbIntfEntry->
                                                                  u4IfIndex,
                                                                  au2ConfPorts,
                                                                  &u2NumPorts)
                        != FSB_FAILURE)
                    {
                        for (u2Index = 0; u2Index < u2NumPorts; u2Index++)
                        {
                            /* Set the port-list with the configured port in the port-channel */
                            OSIX_BITLIST_SET_BIT (pPortList,
                                                  au2ConfPorts[u2Index],
                                                  BRG_PORT_LIST_SIZE);
                        }
                    }
                }
                else if (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
                {
                    if (FsbVlanApiGetSChInfoFromSChIfIndex
                        (pGetFsbIntfEntry->u4IfIndex, &u4UapIfIndex,
                         &u2SVID) != FSB_FAILURE)
                    {
                        OSIX_BITLIST_SET_BIT (pPortList, u4UapIfIndex,
                                              BRG_PORT_LIST_SIZE);
                    }
                }
                else
                {
                    /* If it is not an LAG interface, set the port-list
                     * directly with the retrieved index*/
                    OSIX_BITLIST_SET_BIT (pPortList,
                                          pGetFsbIntfEntry->u4IfIndex,
                                          BRG_PORT_LIST_SIZE);
                }
            }
        }
        pFsbIntfEntry = (tFsbIntfEntry *)
            RBTreeGetNext (gFsbGlobals.FsbIntfTable,
                           (tRBElem *) & FsbIntfEntry, NULL);

    }
    while (pFsbIntfEntry != NULL);

    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbHandlePortDown                                */
/*                                                                           */
/*    Description         : This function used to delete FIP entries,        */
/*                          when port is down                                */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbHandlePortDown (UINT4 u4IfIndex)
{
    if (FsbDeleteFcfEntryForIfIndex (u4IfIndex) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbHandlePortDown: FsbDeleteFcfEntryForIfIndex"
                         " failed for IfIndex: %d\r\n", u4IfIndex);
        return FSB_FAILURE;
    }
    if (FsbDeleteSessEntryForIfIndex (u4IfIndex) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbHandlePortDown: FsbDeleteSessEntryForIfIndex"
                         " failed for IfIndex: %d\r\n", u4IfIndex);
        return FSB_FAILURE;
    }
    if (FsbRedSyncUpOperStatus (u4IfIndex, FSB_RED_PORT_DOWN) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbHandlePortDown: FsbRedSyncUpOperStatus"
                         " failed for IfIndex: %d\r\n", u4IfIndex);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbHandleVLANPortDelete                          */
/*                                                                           */
/*    Description         : This function used to delete FIP entries,        */
/*                          when Port in the VLAN is removed/deleted         */
/*                                                                           */
/*    Input(s)            : VlanId - Vlan Index, u4IfIndex - Interface Index */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbHandleVLANPortDelete (tVlanId VlanId, UINT4 u4IfIndex)
{
    if (FsbDeleteFcfEntryForVLANIfIndex (VlanId, u4IfIndex) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbHandleVLANPortDelete: FsbDeleteFcfEntryForVLANIfIndex"
                         " failed for IfIndex: %d\r\n", u4IfIndex);
        return FSB_FAILURE;
    }

    if (FsbDeleteSessEntryForVLANIfIndex (VlanId, u4IfIndex) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbHandleVLANPortDelete: FsbDeleteSessEntryForVLANIfIndex"
                         " failed for IfIndex: %d\r\n", u4IfIndex);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbHandleDeletePort                              */
/*                                                                           */
/*    Description         : This function used to delete FIP entries,        */
/*                          when port is down                                */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbHandleDeletePort (UINT4 u4IfIndex)
{
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbIntfEntry       FsbIntfEntry;

    FSB_MEMSET (&FsbIntfEntry, 0, sizeof (tFsbIntfEntry));

    /* Walk through the interface table and delete
     * the entries matching with the incoming VLAN and context ID*/
    pFsbIntfEntry = (tFsbIntfEntry *) RBTreeGetFirst (gFsbGlobals.FsbIntfTable);
    if (pFsbIntfEntry == NULL)
    {
        /* If the vlan doesnot have any ports mapped to it,
         * then success should be returned as this a valid 
         * scenario*/
        return FSB_SUCCESS;
    }
    do
    {
        FsbIntfEntry.u2VlanId = pFsbIntfEntry->u2VlanId;
        FsbIntfEntry.u4IfIndex = pFsbIntfEntry->u4IfIndex;

        if (pFsbIntfEntry->u4IfIndex == u4IfIndex)
        {
            FsbDeleteIntfEntry (pFsbIntfEntry->u2VlanId,
                                pFsbIntfEntry->u4IfIndex);
        }
        pFsbIntfEntry = (tFsbIntfEntry *)
            RBTreeGetNext (gFsbGlobals.FsbIntfTable,
                           (tRBElem *) & FsbIntfEntry, NULL);
    }
    while (pFsbIntfEntry != NULL);

    if (FsbDeleteFcfEntryForIfIndex (u4IfIndex) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_ERROR_LEVEL, FSB_NONE, "FsbHandleDeletePort:"
                         " FsbDeleteFcfEntryForIfIndex for IfIndex: %d failed\r\n",
                         u4IfIndex);
        return FSB_FAILURE;
    }
    if (FsbDeleteSessEntryForIfIndex (u4IfIndex) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_ERROR_LEVEL, FSB_NONE, "FsbHandleDeletePort:"
                         " FsbDeleteSessEntryForIfIndex for IfIndex: %d failed\r\n",
                         u4IfIndex);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbHandleDeleteVlanIndication                    */
/*                                                                           */
/*    Description         : This function is used to process VLAN deletion   */
/*                          indication from L2IWF                            */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId - VLAN Index                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbHandleDeleteVlanIndication (UINT4 u4ContextId, tVlanId VlanId)
{
    /* If delete VLAN notification is received for the FCoE VLAN 
     * below actions need to be taken,
     * 1) Delete fip session entries mapped to the deleted Fcoe Vlan
     * 2) Delete all FCF entries mapped to the deleted Fcoe Vlan
     * 3) Delete all the tFsbIntfEntry mapped to VLAN in the Context,
     * 4) Delete tFsbFipSnoopingEntry as the VLAN no longer exists in L2IWF */

    FsbDeleteFipSessionEntryForFcoeVlan (u4ContextId, VlanId);

    FsbDeleteFCFEntryForFcoeVlan (u4ContextId, VlanId);

    FsbRemoveIntfEntryForFcoeVlan (u4ContextId, VlanId);

    FsbDeleteFipSnoopingEntry (u4ContextId, VlanId);

    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name        : FsbCustCallBack                                    */
/*                                                                           */
/* Description          : This function calls the registered function        */
/*                        pointer for specific events.                       */
/*                                                                           */
/* Input(s)             : u4Event - Event for which Call back is invoked     */
/*                        pFsbCallBackArgs - Input parameters required for   */
/*                                          call back functions              */
/* Output(s)            : None                                               */
/* Global Variables                                                          */
/* Referred             : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Modified             : None                                               */
/*                                                                           */
/* Return Value(s)      : FSB_SUCCESS/FSB_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCustCallBack (UINT4 u4Event, tFsbCallBackArgs * pFsbCallBackArgs)
{
    INT4                i4RetVal = FSB_SUCCESS;

    switch (u4Event)
    {
        case FSB_SET_IF_MTU_EVENT:
            if (FSB_CALLBACK[u4Event].pFsbSetIfMtuCallBack != NULL)
            {
                i4RetVal = FSB_CALLBACK[u4Event].pFsbSetIfMtuCallBack
                    (pFsbCallBackArgs->u4IfIndex);
            }
            break;

        default:
            i4RetVal = FSB_FAILURE;
            break;
    }

    return i4RetVal;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbHandleAddPortToAgg                            */
/*                                                                           */
/*    Description         : This function used to handle the scenario when   */
/*                          port is added to the port-channel                */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                          u2AggId - Aggregate Index                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbHandleAddPortToAgg (UINT4 u4IfIndex, UINT2 u2AggId)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbContextInfo    *pFsbContextInfo = NULL;
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;
    tFsbIntfEntry       FsbIntfEntry;
    UINT4               u4HwFilterId = 0;
    UINT1               u1IfType = 0;
    UINT1               u1IsSetInPortList = 0;

    pFsbIntfEntry = (tFsbIntfEntry *) RBTreeGetFirst (gFsbGlobals.FsbIntfTable);

    if (pFsbIntfEntry == NULL)
    {
        /* No FsbIntfEntry were created and therefore, no entries
         * to program */
        return FSB_SUCCESS;
    }

    while (pFsbIntfEntry != NULL)
    {
        /* Get the interface type */
        FsbCfaGetIfType (pFsbIntfEntry->u4IfIndex, &u1IfType);

        /* If the interface type is LAG and if the LAG interface index
         * matches with the LAG interface index passed to this function, then
         * proceed, else skip to the next index in the Interface table*/
        if ((u1IfType == CFA_LAGG)
            && (pFsbIntfEntry->u4IfIndex == (UINT4) u2AggId))
        {
            pFsbFipSnoopingEntry =
                FsbGetFIPSnoopingEntry (pFsbIntfEntry->u4ContextId,
                                        pFsbIntfEntry->u2VlanId);

            pFsbContextInfo =
                FsbCxtGetContextEntry (pFsbIntfEntry->u4ContextId);

            /* Proceed only if ModuleStatus and VlanEnabled Status
             * are enabled, which means that filters are already installed for this
             * VLAN. We need to update the installed filter entry.
             * Else skip the entry and move to the next index in the Interface table*/
            if ((pFsbFipSnoopingEntry != NULL) && (pFsbContextInfo != NULL))
            {
                if ((pFsbFipSnoopingEntry->u1EnabledStatus == FSB_ENABLE) &&
                    (pFsbContextInfo->u1ModuleStatus == FSB_ENABLE))
                {
                    /* Scan the complete filter entry RBTree starting
                     * from the first entry and update the port-list
                     * for each of the filter entries accordingly*/

                    pFsbFilterEntry = (tFsbFilterEntry *)
                        RBTreeGetFirst (pFsbFipSnoopingEntry->FsbFilterEntry);

                    while (pFsbFilterEntry != NULL)
                    {
                        OSIX_BITLIST_IS_BIT_SET (pFsbFilterEntry->PortList,
                                                 u4IfIndex,
                                                 sizeof (tPortList),
                                                 u1IsSetInPortList);

                        if (u1IsSetInPortList == OSIX_FALSE)
                        {
                            if ((pFsbFilterEntry->u2OpcodeFilterOffsetValue ==
                                 FSB_DISCOVERY)
                                && (pFsbFilterEntry->
                                    u2SubOpcodeFilterOffsetValue ==
                                    FSB_DISCOVERY_SOLICITATION))
                            {
                                if ((pFsbIntfEntry->u1PortRole ==
                                     FSB_ENODE_FACING)
                                    || (pFsbIntfEntry->u1PortRole ==
                                        FSB_ENODE_FCF_FACING))
                                {
                                    /* Delete the existing filter entry */
                                    if (FsFsbHwWrDeleteFilter (pFsbFilterEntry,
                                                               pFsbFilterEntry->
                                                               u4HwFilterId) !=
                                        FSB_SUCCESS)
                                    {
                                        FSB_CONTEXT_TRC (pFsbIntfEntry->
                                                         u4ContextId,
                                                         FSB_ERROR_LEVEL,
                                                         FSB_NONE,
                                                         "FsbHandleAddPortToAgg: FsFsbHwWrDeleteFilter with "
                                                         "VlanId: %d failed\r\n",
                                                         pFsbFilterEntry->
                                                         u2VlanId);
                                        return FSB_FAILURE;
                                    }

                                    /* Update the portlist with the newly added port
                                     * index. The remaining elements in the Filter
                                     * entry remains the same. */
                                    OSIX_BITLIST_SET_BIT (pFsbFilterEntry->
                                                          PortList, u4IfIndex,
                                                          BRG_PORT_LIST_SIZE);

                                    if (FsFsbHwWrCreateFilter (pFsbFilterEntry,
                                                               &u4HwFilterId) !=
                                        FSB_SUCCESS)
                                    {
                                        FSB_CONTEXT_TRC (pFsbIntfEntry->
                                                         u4ContextId,
                                                         FSB_ERROR_LEVEL,
                                                         FSB_NONE,
                                                         "FsbHandleAddPortToAgg : FsFsbHwWrCreateFilter with "
                                                         "VlanId: %d failed\r\n",
                                                         pFsbFilterEntry->
                                                         u2VlanId);
                                        RBTreeRemove (pFsbFipSnoopingEntry->
                                                      FsbFilterEntry,
                                                      (tRBElem *)
                                                      pFsbFilterEntry);
                                        MemReleaseMemBlock
                                            (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                                             (UINT1 *) pFsbFilterEntry);
                                        return FSB_FAILURE;
                                    }
                                    pFsbFilterEntry->u4HwFilterId =
                                        u4HwFilterId;
                                }
                            }
                            else if ((pFsbFilterEntry->
                                      u2OpcodeFilterOffsetValue ==
                                      FSB_DISCOVERY)
                                     && (pFsbFilterEntry->
                                         u2SubOpcodeFilterOffsetValue ==
                                         FSB_DISCOVERY_ADVERTISEMENT))
                            {
                                if ((pFsbIntfEntry->u1PortRole ==
                                     FSB_FCF_FACING)
                                    || (pFsbIntfEntry->u1PortRole ==
                                        FSB_ENODE_FCF_FACING))
                                {
                                    /* Delete the existing filter entry */
                                    if (FsFsbHwWrDeleteFilter (pFsbFilterEntry,
                                                               pFsbFilterEntry->
                                                               u4HwFilterId) !=
                                        FSB_SUCCESS)
                                    {
                                        FSB_CONTEXT_TRC (pFsbIntfEntry->
                                                         u4ContextId,
                                                         FSB_ERROR_LEVEL,
                                                         FSB_NONE,
                                                         "FsbHandleAddPortToAgg: FsFsbHwWrDeleteFilter with "
                                                         "VlanId: %d failed\r\n",
                                                         pFsbFilterEntry->
                                                         u2VlanId);
                                        return FSB_FAILURE;
                                    }

                                    /* Update the portlist with the newly added port
                                     * index. The remaining elements in the Filter
                                     * entry remains the same. */
                                    if ((pFsbFipSnoopingEntry->
                                         u4PinnedPortCount != 0)
                                        && (pFsbIntfEntry->u1PortRole ==
                                            FSB_FCF_FACING))
                                    {
                                        FSB_MEMSET (pFsbFilterEntry->PortList,
                                                    0, sizeof (tPortList));

                                        FSB_MEMCPY (pFsbFilterEntry->PortList,
                                                    pFsbFipSnoopingEntry->
                                                    FsbPinnedPorts,
                                                    BRG_PORT_LIST_SIZE);

                                        OSIX_BITLIST_IS_BIT_SET
                                            (pFsbFilterEntry->PortList,
                                             u4IfIndex, sizeof (tPortList),
                                             u1IsSetInPortList);

                                        /* Ports added to MLAG should also be configured as Pinned Port List
                                         * The below failure will indicate us, that the administrator
                                         * has not configured the port added to LAG, as Pinned port*/
                                        if (u1IsSetInPortList == OSIX_FALSE)
                                        {
                                            FSB_CONTEXT_TRC (pFsbIntfEntry->
                                                             u4ContextId,
                                                             FSB_ERROR_LEVEL,
                                                             FSB_NONE,
                                                             "FsbHandleAddPortToAgg: IfIndex %d added to aggregator "
                                                             "is not part of Pinned PortList. Pinned Port Count:%d\r\n",
                                                             u4IfIndex,
                                                             pFsbFipSnoopingEntry->
                                                             u4PinnedPortCount);
                                        }
                                    }
                                    else
                                    {
                                        OSIX_BITLIST_SET_BIT (pFsbFilterEntry->
                                                              PortList,
                                                              u4IfIndex,
                                                              BRG_PORT_LIST_SIZE);
                                    }
                                    if (FsFsbHwWrCreateFilter
                                        (pFsbFilterEntry,
                                         &u4HwFilterId) != FSB_SUCCESS)
                                    {
                                        FSB_CONTEXT_TRC (pFsbIntfEntry->
                                                         u4ContextId,
                                                         FSB_ERROR_LEVEL,
                                                         FSB_NONE,
                                                         "FsbHandleAddPortToAgg : FsFsbHwWrCreateFilter with "
                                                         "VlanId: %d failed\r\n",
                                                         pFsbFilterEntry->
                                                         u2VlanId);
                                        RBTreeRemove (pFsbFipSnoopingEntry->
                                                      FsbFilterEntry,
                                                      (tRBElem *)
                                                      pFsbFilterEntry);
                                        MemReleaseMemBlock
                                            (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                                             (UINT1 *) pFsbFilterEntry);
                                        return FSB_FAILURE;
                                    }
                                    pFsbFilterEntry->u4HwFilterId =
                                        u4HwFilterId;
                                }
                            }
                        }
                        /* Move to the next filter entry in the Filter Entry RBTree */
                        FSB_MEMCPY (FsbFilterEntry.DstMac,
                                    pFsbFilterEntry->DstMac, MAC_ADDR_LEN);

                        pFsbFilterEntry = (tFsbFilterEntry *)
                            RBTreeGetNext (pFsbFipSnoopingEntry->FsbFilterEntry,
                                           (tRBElem *) & FsbFilterEntry, NULL);
                    }
                }
            }
        }
        /* Move to the next interface entry in the FSB interface table */
        FsbIntfEntry.u2VlanId = pFsbIntfEntry->u2VlanId;
        FsbIntfEntry.u4IfIndex = pFsbIntfEntry->u4IfIndex;
        pFsbIntfEntry = (tFsbIntfEntry *) RBTreeGetNext
            (gFsbGlobals.FsbIntfTable, (tRBElem *) & FsbIntfEntry, NULL);
    }
    if (FsbHandleDeletePort (u4IfIndex) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbHandleAddPortToAgg : FsbHandleDeletePort for "
                         "IfIndex: %d failed\r\n", u4IfIndex);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbHandleRemovePortFromAgg                       */
/*                                                                           */
/*    Description         : This function used to handle the scenario when   */
/*                          port is removed from the port-channel            */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                          u2AggId - Aggregate Index                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbHandleRemovePortFromAgg (UINT4 u4IfIndex, UINT2 u2AggId)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbContextInfo    *pFsbContextInfo = NULL;
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;
    tFsbIntfEntry       FsbIntfEntry;
    UINT4               u4HwFilterId = 0;
    UINT1               u1IfType = 0;
    UINT1               u1IsSetInPortList = 0;

    pFsbIntfEntry = (tFsbIntfEntry *) RBTreeGetFirst (gFsbGlobals.FsbIntfTable);

    if (pFsbIntfEntry == NULL)
    {
        /* No FsbIntfEntry were created and therefore, no entries
         * to program */
        return FSB_SUCCESS;
    }

    while (pFsbIntfEntry != NULL)
    {
        /* Get the interface type */
        FsbCfaGetIfType (pFsbIntfEntry->u4IfIndex, &u1IfType);
        /* If the interface type is LAG and if the LAG interface index
         * matches with the LAG interface index passed to this function, then
         * proceed, else skip to the next index in the Interface table*/
        if ((u1IfType == CFA_LAGG)
            && (pFsbIntfEntry->u4IfIndex == (UINT4) u2AggId))
        {
            pFsbFipSnoopingEntry =
                FsbGetFIPSnoopingEntry (pFsbIntfEntry->u4ContextId,
                                        pFsbIntfEntry->u2VlanId);

            pFsbContextInfo =
                FsbCxtGetContextEntry (pFsbIntfEntry->u4ContextId);

            /* Proceed only if ModuleStatus and VlanEnabled Status
             * are enabled, which means that filters are already installed for this
             * VLAN. We need to update the installed filter entry.
             * Else skip the entry and move to the next index in the Interface table*/
            if ((pFsbFipSnoopingEntry != NULL) && (pFsbContextInfo != NULL))
            {
                if ((pFsbFipSnoopingEntry->u1EnabledStatus == FSB_ENABLE) &&
                    (pFsbContextInfo->u1ModuleStatus == FSB_ENABLE))
                {
                    /* Scan the complete filter entry RBTree starting
                     * from the first entry and update the port-list
                     * for each of the filter entries accordingly*/

                    pFsbFilterEntry = (tFsbFilterEntry *)
                        RBTreeGetFirst (pFsbFipSnoopingEntry->FsbFilterEntry);

                    while (pFsbFilterEntry != NULL)
                    {
                        OSIX_BITLIST_IS_BIT_SET (pFsbFilterEntry->PortList,
                                                 u4IfIndex,
                                                 sizeof (tPortList),
                                                 u1IsSetInPortList);

                        if (u1IsSetInPortList == OSIX_TRUE)
                        {
                            if ((pFsbFilterEntry->u2OpcodeFilterOffsetValue ==
                                 FSB_DISCOVERY)
                                && (pFsbFilterEntry->
                                    u2SubOpcodeFilterOffsetValue ==
                                    FSB_DISCOVERY_SOLICITATION))
                            {
                                if ((pFsbIntfEntry->u1PortRole ==
                                     FSB_ENODE_FACING)
                                    || (pFsbIntfEntry->u1PortRole ==
                                        FSB_ENODE_FCF_FACING))
                                {
                                    /* Delete the existing filter entry */
                                    if (FsFsbHwWrDeleteFilter (pFsbFilterEntry,
                                                               pFsbFilterEntry->
                                                               u4HwFilterId) !=
                                        FSB_SUCCESS)
                                    {
                                        FSB_CONTEXT_TRC (pFsbIntfEntry->
                                                         u4ContextId,
                                                         FSB_ERROR_LEVEL,
                                                         FSB_NONE,
                                                         "FsbHandleRemovePortFromAgg: FsFsbHwWrDeleteFilter with"
                                                         "VlanId: %d failed\r\n",
                                                         pFsbFilterEntry->
                                                         u2VlanId);
                                        return FSB_FAILURE;
                                    }

                                    /* Update the portlist with the removed port
                                     * index. The remaining elements in the Filter
                                     * entry remains the same. */
                                    OSIX_BITLIST_RESET_BIT (pFsbFilterEntry->
                                                            PortList, u4IfIndex,
                                                            BRG_PORT_LIST_SIZE);

                                    if (FsFsbHwWrCreateFilter (pFsbFilterEntry,
                                                               &u4HwFilterId) !=
                                        FSB_SUCCESS)
                                    {
                                        FSB_CONTEXT_TRC (pFsbIntfEntry->
                                                         u4ContextId,
                                                         FSB_ERROR_LEVEL,
                                                         FSB_NONE,
                                                         "FsbHandleRemovePortFromAgg : FsFsbHwWrCreateFilter with"
                                                         "VlanId: %d failed\r\n",
                                                         pFsbFilterEntry->
                                                         u2VlanId);
                                        RBTreeRemove (pFsbFipSnoopingEntry->
                                                      FsbFilterEntry,
                                                      (tRBElem *)
                                                      pFsbFilterEntry);
                                        MemReleaseMemBlock
                                            (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                                             (UINT1 *) pFsbFilterEntry);
                                        return FSB_FAILURE;
                                    }
                                    pFsbFilterEntry->u4HwFilterId =
                                        u4HwFilterId;
                                }
                            }
                            else if ((pFsbFilterEntry->
                                      u2OpcodeFilterOffsetValue ==
                                      FSB_DISCOVERY)
                                     && (pFsbFilterEntry->
                                         u2SubOpcodeFilterOffsetValue ==
                                         FSB_DISCOVERY_ADVERTISEMENT))
                            {
                                if ((pFsbIntfEntry->u1PortRole ==
                                     FSB_FCF_FACING)
                                    || (pFsbIntfEntry->u1PortRole ==
                                        FSB_ENODE_FCF_FACING))
                                {
                                    /* Delete the existing filter entry */
                                    if (FsFsbHwWrDeleteFilter (pFsbFilterEntry,
                                                               pFsbFilterEntry->
                                                               u4HwFilterId) !=
                                        FSB_SUCCESS)
                                    {
                                        FSB_CONTEXT_TRC (pFsbIntfEntry->
                                                         u4ContextId,
                                                         FSB_ERROR_LEVEL,
                                                         FSB_NONE,
                                                         "FsbHandleRemovePortFromAgg : FsFsbHwWrDeleteFilter with "
                                                         "VlanId: %d failed\r\n",
                                                         pFsbFilterEntry->
                                                         u2VlanId);
                                        return FSB_FAILURE;
                                    }

                                    /* Update the portlist with the removed port
                                     * index. The remaining elements in the Filter
                                     * entry remains the same. */
                                    if ((pFsbFipSnoopingEntry->
                                         u4PinnedPortCount != 0)
                                        && (pFsbIntfEntry->u1PortRole ==
                                            FSB_FCF_FACING))
                                    {
                                        FSB_MEMSET (pFsbFilterEntry->PortList,
                                                    0, sizeof (tPortList));

                                        FSB_MEMCPY (pFsbFilterEntry->PortList,
                                                    pFsbFipSnoopingEntry->
                                                    FsbPinnedPorts,
                                                    BRG_PORT_LIST_SIZE);
                                    }
                                    OSIX_BITLIST_RESET_BIT (pFsbFilterEntry->
                                                            PortList, u4IfIndex,
                                                            BRG_PORT_LIST_SIZE);

                                    if (FsFsbHwWrCreateFilter
                                        (pFsbFilterEntry,
                                         &u4HwFilterId) != FSB_SUCCESS)
                                    {
                                        FSB_CONTEXT_TRC (pFsbIntfEntry->
                                                         u4ContextId,
                                                         FSB_ERROR_LEVEL,
                                                         FSB_NONE,
                                                         "FsbHandleAddPortToAgg : FsFsbHwWrCreateFilter with "
                                                         "VlanId: %d failed\r\n",
                                                         pFsbFilterEntry->
                                                         u2VlanId);
                                        RBTreeRemove (pFsbFipSnoopingEntry->
                                                      FsbFilterEntry,
                                                      (tRBElem *)
                                                      pFsbFilterEntry);
                                        MemReleaseMemBlock
                                            (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                                             (UINT1 *) pFsbFilterEntry);
                                        return FSB_FAILURE;
                                    }
                                    pFsbFilterEntry->u4HwFilterId =
                                        u4HwFilterId;
                                }
                            }
                        }
                        /* Move to the next filter entry in the Filter Entry RBTree */
                        FSB_MEMCPY (FsbFilterEntry.DstMac,
                                    pFsbFilterEntry->DstMac, MAC_ADDR_LEN);

                        pFsbFilterEntry = (tFsbFilterEntry *)
                            RBTreeGetNext (pFsbFipSnoopingEntry->FsbFilterEntry,
                                           (tRBElem *) & FsbFilterEntry, NULL);
                    }
                }
            }
        }
        /* Move to the next interface entry in the FSB interface table */
        FsbIntfEntry.u2VlanId = pFsbIntfEntry->u2VlanId;
        FsbIntfEntry.u4IfIndex = pFsbIntfEntry->u4IfIndex;
        pFsbIntfEntry = (tFsbIntfEntry *) RBTreeGetNext
            (gFsbGlobals.FsbIntfTable, (tRBElem *) & FsbIntfEntry, NULL);
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDisable                                       */
/*                                                                           */
/*    Description         : This function deletes all the configurations in  */
/*                          FSB and deletes all dynamically populated tables */
/*                          and its respective filter entries                */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
FsbDisable ()
{
    tFsbContextInfo    *pFsbContextInfo = NULL;
    UINT4               u4ContextId = 0;

    for (u4ContextId = 0; u4ContextId < FSB_MAX_CONTEXT_COUNT; u4ContextId++)
    {
        pFsbContextInfo = FsbCxtGetContextEntry (u4ContextId);

        if (pFsbContextInfo != NULL)
        {
            if (pFsbContextInfo->u1ModuleStatus == FSB_ENABLE)
            {
                /* Delete all FcfEntry learnt in this context */
                if (FsbDeleteAllFcfEntry (u4ContextId) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbDisable: FsbDeleteAllFcfEntry failed \r\n");
                    return;
                }
                /* Delete all FipSessEntry learnt in this context */
                if (FsbDeleteAllFipSessEntry (u4ContextId) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbDisable: FsbDeleteAllFipSessEntry failed \r\n");
                    return;
                }
                /* If Enabled Status of FCoE VLAN is enable, delete
                 * Default filter for that FCoE VLAN */
                if (FsbDeleteDefaultFilter (u4ContextId) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbDisable: FsbDeleteDefaultFilter failed \r\n");
                    return;
                }
                /* Module Status is disabled, remove Default VLAN filter */
                if (FsbRemoveDefaultVLANFilter (pFsbContextInfo) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbDisable: FsbRemoveDefaultVLANFilter failed \r\n");
                    return;
                }
                pFsbContextInfo->u1ModuleStatus = FSB_DISABLE;
            }
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbEnable                                        */
/*                                                                           */
/*    Description         : This function is called to enable the FSB module */
/*                          from the disabled state.                         */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
FsbEnable ()
{
    tFsbContextInfo    *pFsbContextInfo = NULL;
    UINT4               u4ContextId = 0;

    for (u4ContextId = 0; u4ContextId < FSB_MAX_CONTEXT_COUNT; u4ContextId++)
    {
        pFsbContextInfo = FsbCxtGetContextEntry (u4ContextId);

        if (pFsbContextInfo != NULL)
        {
            if (pFsbContextInfo->u1ModuleStatus == FSB_DISABLE)
            {
                /* Module Status is enabled, install Default VLAN filter */
                if (FsbInstallDefaultVLANFilter (u4ContextId,
                                                 pFsbContextInfo) !=
                    FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbEnable: FsbInstallDefaultVLANFilter failed\r\n");
                    return;
                }
                /* Install Default filter for FCoE enabled VLAN by checking the
                 * enabled status of the vlan */
                if (FsbAddDefaultFilter (u4ContextId) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbEnable: FsbAddDefaultFilter failed\r\n");
                    return;
                }
                pFsbContextInfo->u1ModuleStatus = FSB_ENABLE;
            }
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : FsbConfigUcastEntry                                  */
/*                                                                           */
/* Description        : This function calls the VLAN module to configure     */
/*                      (Add or delete) the static unicast entry.            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      DestMac - Mac Address                                */
/*                      u2VlanId - VLAN Index                                */
/*                      u4IfIndex - Interace Index                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE                            */
/*****************************************************************************/
INT4
FsbConfigUcastEntry (UINT4 u4ContextId, tMacAddr MacAddress,
                     tVlanId u2VlanId, UINT4 u4IfIndex, UINT1 u1Action)
{
    tConfigStUcastInfo  StaticUnicastInfo;
    UINT1               u1ErrorCode = 0;

    FSB_MEMSET (&StaticUnicastInfo, 0, sizeof (tConfigStUcastInfo));

    /* Static MAC programmed is synced by VLAN module.
     * Hence, returning success if the node status is Standby
     */

    if (FSB_NODE_STATUS () != FSB_NODE_ACTIVE)
    {
        return FSB_SUCCESS;
    }

    StaticUnicastInfo.u4ContextId = u4ContextId;
    StaticUnicastInfo.VlanId = u2VlanId;
    FSB_MEMCPY (StaticUnicastInfo.DestMac, MacAddress, MAC_ADDR_LEN);
    StaticUnicastInfo.u4EgressIfIndex = u4IfIndex;

    if (u1Action == VLAN_CREATE)
    {
        /* VLAN_DELETE_ON_RESET status is set, to prevent
         * restortion of static MAC programmed via MSR.
         */
        StaticUnicastInfo.u4Status = VLAN_DELETE_ON_RESET;
    }
    else
    {
        /* Static entry will be deleted, only if its status is
         * VLAN_OTHER */
        StaticUnicastInfo.u4Status = VLAN_OTHER;
    }

    if (FsbPortVlanConfigStaticUcastEntry (&StaticUnicastInfo,
                                           u1Action,
                                           &u1ErrorCode) != FSB_SUCCESS)
    {
        /* When the call is made to delete a static Mac entry,
         * FSB_SUCCESS is returned, irrespective of the return status
         * of the FsbPortVlanConfigStaticUcastEntry.
         * This is done to avoid FSB_FAILURE being returned
         * in case of no static MAC configured or the configured
         * Static entry is deleted by module other than FSB 
         * When a call is made to configure the static mac when the
         * peer node is up, VLAN module would have already synced the
         * MAC to the standby node. So when standby node tries to
         * progamme the already configured Static MAC, FSB_FAILURE is
         * returned. Therfore ignoring this failure */
        return FSB_SUCCESS;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbPacketTxFrame                                     */
/*                                                                           */
/* Description        : This function transmits the trapped FIPS packet once */
/*                      the processing of the packet is complete and         */
/*                      successful.                                          */
/*                                                                           */
/* Input(s)            : pFsbNotifyParams -  Pointer to tFsbNotifyParams     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE                            */
/*****************************************************************************/
INT4
FsbPacketTxFrame (tFsbNotifyParams * pFsbNotifyParams,
                  tFSBPktInfo * pFSBPktInfo)
{
    tCRU_BUF_CHAIN_HEADER *pFsbMsgBuf = NULL;
    tPortList          *pPortList = NULL;
    INT4                i4BrgPortType = 0;
    UINT4               u4LocalPort = 0;
    UINT4               u4PacketLen = 0;
    UINT4               u4UapIfIndex = 0;
    UINT4               u4ContextId = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2SVID = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2Port = 0;
    UINT2               u2PortIndex = 0;
    UINT1               u1Status = 0;
    UINT1               u1PortFlag = 0;
    UINT1               u1IfType = 0;
    UINT1              *pu1ReadPtr = NULL;

    if (FSB_NODE_STATUS () != FSB_NODE_ACTIVE)
    {
        /* Transmission occurs only in Active node */
        return FSB_SUCCESS;
    }

    /* Packet transmission is done as per below.
     * 1) Get the VLAN member ports
     * 2) Scan the FDB entry database and find the outgoing port
     * 3) If the packet is a tagged member port, tx the packet directly
     * 4) If the packet is an untagged member port, tx the packet
     *    after stripping the VLAN tag
     * 5) If the FDB entry database search returns failure,
     *    loop through the list of interfaces in the switch and flood 
     *    it to all the ports.
     * 6) In the EVB scenario, if the packet is sent to the ENode server,
     *    add a S-VLAN tag to it before doing the tx. 
     */

    pPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPortList == NULL)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbPacketTxFrame: FsUtilAllocBitList failed\r\n");
        return FSB_FAILURE;
    }

    FSB_MEMSET (*pPortList, 0, sizeof (tPortList));

    /* Get the list of ports from L2IWF */
    if (FsbPortGetVlanLocalEgressPorts
        (pFsbNotifyParams->u4ContextId, pFsbNotifyParams->u2VlanId,
         *pPortList) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbPacketTxFrame : "
                         "FsbPortGetVlanLocalEgressPorts with VlanId: %d "
                         "failed\n", pFsbNotifyParams->u2VlanId);
        FsUtilReleaseBitList (*pPortList);
        return FSB_FAILURE;
    }

    if ((FSB_SUCCESS ==
         FsbPortVlanGetFdbEntryDetails (pFsbNotifyParams->u2VlanId,
                                        pFSBPktInfo->DestAddr, &u2Port,
                                        &u1Status))
        && (u1Status == VLAN_FDB_MGMT))
    {

        /* Since fdb entry is present, packet is forwarded to only one port */
        FsbCfaGetInterfaceBrgPortType (u2Port, &i4BrgPortType);

        /* EVB port scenario */
        if (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
        {
            /* Get SVID and UAP If Index */
            if (FsbVlanApiGetSChInfoFromSChIfIndex (u2Port, &u4UapIfIndex,
                                                    &u2SVID) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "FsbPacketTxFrame: FsbVlanApiGetSChInfoFromSChIfIndex return failure");
                FsUtilReleaseBitList (*pPortList);
                return FSB_FAILURE;
            }

            pu1ReadPtr = pFsbNotifyParams->au1DataBuf;

            /* Allocate a CRU buffer message chain for this Ethernet frame */
            if ((pFsbMsgBuf =
                 CRU_BUF_Allocate_MsgBufChain (pFsbNotifyParams->u4PacketLen,
                                               FSB_OFFSET_NONE)) == NULL)

            {
                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_DEBUGGING_LEVEL,
                                 FSB_BUFFER_TRC,
                                 "FsbPacketTxFrame :Allocate Buffer returned Failure\n");
                FsUtilReleaseBitList (*pPortList);
                return FSB_FAILURE;
            }

            /* Copy the passed Ethernet frame into the CRU buffer */
            if (CRU_BUF_Copy_OverBufChain
                (pFsbMsgBuf, pu1ReadPtr, FSB_OFFSET_NONE,
                 pFsbNotifyParams->u4PacketLen) == CRU_FAILURE)

            {
                /* Release the CRU buf chain for Ethernet frame */
                if (CRU_BUF_Release_MsgBufChain (pFsbMsgBuf, FALSE)
                    != CRU_SUCCESS)
                {
                    FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                     FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbPacketTxFrame :Release Buffer returned Failure\n");
                }
                FsUtilReleaseBitList (*pPortList);
                return FSB_FAILURE;

            }

            if (FsbPortIsVlanUntagMemberPort (pFsbNotifyParams->u4ContextId,
                                              pFsbNotifyParams->u2VlanId,
                                              u2Port) == FSB_TRUE)
            {
                /* Strip the C-VLAN tag, if it is an untagged member port */
                FsbStripVlanTag (pFsbMsgBuf);
            }

            /* Add S-Tag on the frame */
            if (FsbVlanAddOuterTagInFrame (pFsbMsgBuf, u2SVID, 0, u4UapIfIndex)
                != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "FsbPacketTxFrame: FsbVlanAddOuterTagInFrame return failure");

                /* Release the CRU buf chain for Ethernet frame */
                if (CRU_BUF_Release_MsgBufChain (pFsbMsgBuf, FALSE)
                    != CRU_SUCCESS)

                {
                    FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                     FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                                     "FsbPacketTxFrame :Release Buffer returned Failure\n");
                }

                FsUtilReleaseBitList (*pPortList);
                return FSB_FAILURE;
            }

            /* Resetting the UAP IfIndex to transmit out the pkt over physical
             * interface. */
            u2Port = (UINT2) u4UapIfIndex;

            MEMSET (pFsbNotifyParams->au1DataBuf, 0, FSB_MAX_FIP_PKT_LEN);

            pFsbNotifyParams->u4PacketLen =
                (UINT4) CRU_BUF_Get_ChainValidByteCount (pFsbMsgBuf);
            CRU_BUF_Copy_FromBufChain (pFsbMsgBuf, pFsbNotifyParams->au1DataBuf,
                                       0, pFsbNotifyParams->u4PacketLen);

            /* Release the CRU buf chain for Ethernet frame */
            if (CRU_BUF_Release_MsgBufChain (pFsbMsgBuf, FALSE) != CRU_SUCCESS)

            {
                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbPacketTxFrame :Release Buffer returned Failure\n");
                FsUtilReleaseBitList (*pPortList);
                return FSB_FAILURE;
            }
            if ((FsbPortCfaGddEthWrite (pFsbNotifyParams->au1DataBuf, u2Port,
                                        pFsbNotifyParams->u4PacketLen)) ==
                FSB_FAILURE)
            {
                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbPacketTxFrame :FsbPortCfaGddEthWrite failed\n");
                FsUtilReleaseBitList (*pPortList);
                return FSB_FAILURE;
            }
            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_DEBUGGING_LEVEL,
                             FSB_CONTROL_PATH_TRC,
                             "Packet Type: %s of length %d transmitted in context %d in port %d of "
                             "VLAN %d  \r\n",
                             FsbErrString[pFSBPktInfo->PacketType],
                             pFsbNotifyParams->u4PacketLen,
                             pFSBPktInfo->u4ContextId, u2Port,
                             pFSBPktInfo->u2VlanId);
        }
        else
        {
            /* Normal port scenario */
            if (FsbPortIsVlanUntagMemberPort (pFsbNotifyParams->u4ContextId,
                                              pFsbNotifyParams->u2VlanId,
                                              u2Port) != FSB_TRUE)
            {
                FsbCfaGetIfType (u2Port, &u1IfType);
                if (u1IfType == CFA_LAGG)
                {
                    if (FsbL2IwfGetPotentialTxPortForAgg
                        (pFsbNotifyParams->u4ContextId,
                         pFsbNotifyParams->u2VlanId, u2Port,
                         &u2PortIndex) == FSB_SUCCESS)
                    {
                        /* send through the active port of the portchannel */
                        u2Port = u2PortIndex;
                    }
                }
                if ((FsbPortCfaGddEthWrite
                     (pFsbNotifyParams->au1DataBuf, u2Port,
                      pFsbNotifyParams->u4PacketLen)) == FSB_FAILURE)
                {
                    FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_ERROR_LEVEL,
                                     FSB_NONE,
                                     "FsbPacketTxFrame :FsbPortCfaGddEthWrite failed\n");
                    FsUtilReleaseBitList (*pPortList);
                    return FSB_FAILURE;
                }
                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId, FSB_DEBUGGING_LEVEL,
                                 FSB_CONTROL_PATH_TRC,
                                 "Packet Type: %s of length %d transmitted in context %d in port %d of "
                                 "VLAN %d  \r\n",
                                 FsbErrString[pFSBPktInfo->PacketType],
                                 pFsbNotifyParams->u4PacketLen,
                                 pFSBPktInfo->u4ContextId, u2Port,
                                 pFSBPktInfo->u2VlanId);
            }
            else
            {
                /* Remove the VLAN tag if the port is an untagged member of the VLAN */
                pu1ReadPtr = pFsbNotifyParams->au1DataBuf;
                /* Allocate a CRU buffer message chain for this Ethernet frame */
                if ((pFsbMsgBuf =
                     CRU_BUF_Allocate_MsgBufChain (pFsbNotifyParams->
                                                   u4PacketLen,
                                                   FSB_OFFSET_NONE)) == NULL)

                {
                    FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                     FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                                     "FsbPacketTxFrame :Allocate Buffer returned Failure\n");
                    FsUtilReleaseBitList (*pPortList);
                    return FSB_FAILURE;
                }

                /* Copy the passed Ethernet frame into the CRU buffer */
                if (CRU_BUF_Copy_OverBufChain
                    (pFsbMsgBuf, pu1ReadPtr, FSB_OFFSET_NONE,
                     pFsbNotifyParams->u4PacketLen) == CRU_FAILURE)

                {
                    /* Release the CRU buf chain for Ethernet frame */
                    if (CRU_BUF_Release_MsgBufChain (pFsbMsgBuf, FALSE)
                        != CRU_SUCCESS)

                    {
                        FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                         FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                                         "FsbPacketTxFrame :Release Buffer returned Failure\n");
                    }
                    FsUtilReleaseBitList (*pPortList);
                    return FSB_FAILURE;
                }

                FsbStripVlanTag (pFsbMsgBuf);

                MEMSET (pFsbNotifyParams->au1DataBuf, 0, FSB_MAX_FIP_PKT_LEN);
                pFsbNotifyParams->u4PacketLen =
                    (UINT4) CRU_BUF_Get_ChainValidByteCount (pFsbMsgBuf);
                CRU_BUF_Copy_FromBufChain (pFsbMsgBuf,
                                           pFsbNotifyParams->au1DataBuf, 0,
                                           pFsbNotifyParams->u4PacketLen);

                /* Release the CRU buf chain for Ethernet frame */
                if (CRU_BUF_Release_MsgBufChain (pFsbMsgBuf, FALSE)
                    != CRU_SUCCESS)

                {
                    FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                     FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                                     "FsbPacketTxFrame :Release Buffer returned Failure\n");
                    FsUtilReleaseBitList (*pPortList);
                    return FSB_FAILURE;
                }
                FsbCfaGetIfType (u2Port, &u1IfType);
                if (u1IfType == CFA_LAGG)
                {
                    if (FsbL2IwfGetPotentialTxPortForAgg
                        (pFsbNotifyParams->u4ContextId,
                         pFsbNotifyParams->u2VlanId, u2Port,
                         &u2PortIndex) == FSB_SUCCESS)
                    {
                        /* send through the active port of the portchannel */
                        u2Port = u2PortIndex;
                    }
                }
                if ((FsbPortCfaGddEthWrite
                     (pFsbNotifyParams->au1DataBuf, u2Port,
                      pFsbNotifyParams->u4PacketLen)) == FSB_FAILURE)
                {
                    FsUtilReleaseBitList (*pPortList);
                    return FSB_FAILURE;
                }
                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                 FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                                 "Packet Type: %s of length %d transmitted in context %d in port %d of "
                                 "VLAN %d  \r\n",
                                 FsbErrString[pFSBPktInfo->PacketType],
                                 pFsbNotifyParams->u4PacketLen,
                                 pFSBPktInfo->u4ContextId, u2Port,
                                 pFSBPktInfo->u2VlanId);
            }
        }

        /* Deletion of static Enode MAC configured is prevented
         * during session deletion on reception of CVL, LOGO_ACCEPT
         * and LOGI_REJECT. This is to avoid flodding.
         * After the Tx of the above mentioned packets, 
         * delete the static ENode MAC configured */
        if ((pFSBPktInfo->PacketType == FSB_FIP_CLEAR_LINK) ||
            (pFSBPktInfo->PacketType == FSB_FIP_LOGO_ACCEPT) ||
            (pFSBPktInfo->PacketType == FSB_FIP_FLOGI_REJECT))
        {
            if (FsbConfigUcastEntry (pFSBPktInfo->u4ContextId,
                                     pFSBPktInfo->DestAddr,
                                     pFSBPktInfo->u2VlanId,
                                     u2Port, VLAN_DELETE) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbDeleteFIPSessionEntry"
                                 " of FIP Session Entry with VlanId: %d, EnodeIfIndex: %d"
                                 " and FcfIfIndex: %d: Removing Static ENode Mac failed\r\n",
                                 pFSBPktInfo->u2VlanId, u2Port,
                                 pFSBPktInfo->u4IfIndex);
                FsUtilReleaseBitList (*pPortList);
                return FSB_FAILURE;
            }
        }
    }
    else
    {
        /* Interface index will be returned as portlist from L2IWF.
         * The following loop is to obtain the exact list of ports from the returned
         * portlist*/
        for (u2ByteIndex = 0; u2ByteIndex < BRG_PORT_LIST_SIZE; u2ByteIndex++)
        {
            u1PortFlag = *(*pPortList + u2ByteIndex);
            for (u2BitIndex = 0; ((u2BitIndex < VLAN_PORTS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
            {
                if ((u1PortFlag & VLAN_BIT8) != 0)
                {
                    u4LocalPort = (UINT4) ((u2ByteIndex * VLAN_PORTS_PER_BYTE) +
                                           u2BitIndex + 1);
                    if (pFsbNotifyParams->u4IfIndex != u4LocalPort)
                    {
                        FsbCfaGetInterfaceBrgPortType (u4LocalPort,
                                                       &i4BrgPortType);
                        if (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
                        {
                            /* EVB port scenario */
                            /* Get SVID and UAP If Index */
                            if (FsbVlanApiGetSChInfoFromSChIfIndex
                                (u4LocalPort, &u4UapIfIndex,
                                 &u2SVID) != FSB_SUCCESS)
                            {
                                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                                 FSB_ERROR_LEVEL, FSB_NONE,
                                                 "FsbPacketTxFrame: FsbVlanApiGetSChInfoFromSChIfIndex return failure");
                                FsUtilReleaseBitList (*pPortList);
                                return FSB_FAILURE;
                            }

                            u4PacketLen = pFsbNotifyParams->u4PacketLen;
                            /* Allocate a CRU buffer message chain for this Ethernet frame */
                            if ((pFsbMsgBuf =
                                 CRU_BUF_Allocate_MsgBufChain (u4PacketLen,
                                                               FSB_OFFSET_NONE))
                                == NULL)

                            {
                                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                                 FSB_DEBUGGING_LEVEL,
                                                 FSB_BUFFER_TRC,
                                                 "FsbPacketTxFrame :Allocate Buffer returned Failure\n");
                                FsUtilReleaseBitList (*pPortList);
                                return FSB_FAILURE;
                            }

                            /* Copy the passed Ethernet frame into the CRU buffer */
                            if (CRU_BUF_Copy_OverBufChain
                                (pFsbMsgBuf, pFsbNotifyParams->au1DataBuf,
                                 FSB_OFFSET_NONE, u4PacketLen) == CRU_FAILURE)

                            {
                                /* Release the CRU buf chain for Ethernet frame */
                                if (CRU_BUF_Release_MsgBufChain
                                    (pFsbMsgBuf, FALSE) != CRU_SUCCESS)

                                {
                                    FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                                     FSB_DEBUGGING_LEVEL,
                                                     FSB_BUFFER_TRC,
                                                     "FsbPacketTxFrame :Release Buffer returned Failure\n");
                                }

                                FsUtilReleaseBitList (*pPortList);
                                return FSB_FAILURE;
                            }

                            if (FsbPortIsVlanUntagMemberPort
                                (pFsbNotifyParams->u4ContextId,
                                 pFsbNotifyParams->u2VlanId,
                                 u4LocalPort) == FSB_TRUE)
                            {
                                /* Strip the C-VLAN tag, if it is an untagged member port */
                                FsbStripVlanTag (pFsbMsgBuf);
                            }

                            /* Add S-Tag on the frame */
                            if (FsbVlanAddOuterTagInFrame
                                (pFsbMsgBuf, u2SVID, 0,
                                 u4UapIfIndex) != FSB_SUCCESS)
                            {
                                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                                 FSB_ERROR_LEVEL, FSB_NONE,
                                                 "FsbPacketTxFrame: FsbVlanAddOuterTagInFrame return failure");

                                /* Release the CRU buf chain for Ethernet frame */
                                if (CRU_BUF_Release_MsgBufChain
                                    (pFsbMsgBuf, FALSE) != CRU_SUCCESS)

                                {
                                    FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                                     FSB_DEBUGGING_LEVEL,
                                                     FSB_BUFFER_TRC,
                                                     "FsbPacketTxFrame :Release Buffer returned Failure\n");
                                }

                                FsUtilReleaseBitList (*pPortList);
                                return FSB_FAILURE;
                            }

                            /* Resetting the UAP IfIndex to transmit out the pkt over physical
                             * interface. */
                            u4LocalPort = u4UapIfIndex;

                            MEMSET (gau1TxBuf, 0, FSB_MAX_FIP_PKT_LEN);

                            u4PacketLen =
                                (UINT4)
                                CRU_BUF_Get_ChainValidByteCount (pFsbMsgBuf);
                            CRU_BUF_Copy_FromBufChain (pFsbMsgBuf, gau1TxBuf, 0,
                                                       u4PacketLen);

                            /* Release the CRU buf chain for Ethernet frame */
                            if (CRU_BUF_Release_MsgBufChain (pFsbMsgBuf, FALSE)
                                != CRU_SUCCESS)

                            {
                                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                                 FSB_DEBUGGING_LEVEL,
                                                 FSB_BUFFER_TRC,
                                                 "FsbPacketTxFrame :Release Buffer returned Failure\n");
                                FsUtilReleaseBitList (*pPortList);
                                return FSB_FAILURE;
                            }
                            if ((FsbPortCfaGddEthWrite (gau1TxBuf, u4LocalPort,
                                                        u4PacketLen)) ==
                                FSB_FAILURE)
                            {
                                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                                 FSB_ERROR_LEVEL, FSB_NONE,
                                                 "FsbPacketTxFrame : FsbPortCfaGddEthWrite failed\n");
                                FsUtilReleaseBitList (*pPortList);
                                return FSB_FAILURE;
                            }
                            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                             FSB_DEBUGGING_LEVEL,
                                             FSB_CONTROL_PATH_TRC,
                                             "Packet Type: %s of length %d transmitted in context %d in port %d of "
                                             "VLAN %d  \r\n",
                                             FsbErrString[pFSBPktInfo->
                                                          PacketType],
                                             u4PacketLen,
                                             pFSBPktInfo->u4ContextId,
                                             u4LocalPort,
                                             pFSBPktInfo->u2VlanId);
                        }
                        else
                        {
                            /* Normal  port scenario */
                            if (FsbPortIsVlanUntagMemberPort
                                (pFsbNotifyParams->u4ContextId,
                                 pFsbNotifyParams->u2VlanId,
                                 u4LocalPort) != FSB_TRUE)
                            {
                                FsbCfaGetIfType (u4LocalPort, &u1IfType);
                                if (u1IfType == CFA_LAGG)
                                {

                                    if (FsbL2IwfGetPotentialTxPortForAgg
                                        (pFsbNotifyParams->u4ContextId,
                                         pFsbNotifyParams->u2VlanId,
                                         (UINT2) u4LocalPort,
                                         &u2PortIndex) == FSB_SUCCESS)
                                    {
                                        u4LocalPort = (UINT2) u2PortIndex;
                                    }
                                }
                                if ((FsbPortCfaGddEthWrite
                                     (pFsbNotifyParams->au1DataBuf, u4LocalPort,
                                      pFsbNotifyParams->u4PacketLen)) ==
                                    FSB_FAILURE)
                                {
                                    FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                                     FSB_ERROR_LEVEL, FSB_NONE,
                                                     "FsbPacketTxFrame : FsbPortCfaGddEthWrite failed\n");
                                    FsUtilReleaseBitList (*pPortList);
                                    return FSB_FAILURE;
                                }
                                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                                 FSB_DEBUGGING_LEVEL,
                                                 FSB_CONTROL_PATH_TRC,
                                                 "Packet Type: %s of length %d transmitted in context %d in port %d of "
                                                 "VLAN %d  \r\n",
                                                 FsbErrString[pFSBPktInfo->
                                                              PacketType],
                                                 pFsbNotifyParams->u4PacketLen,
                                                 pFSBPktInfo->u4ContextId,
                                                 u4LocalPort,
                                                 pFSBPktInfo->u2VlanId);
                            }
                            else
                            {
                                u4PacketLen = pFsbNotifyParams->u4PacketLen;

                                /* Allocate a CRU buffer message chain for this Ethernet frame */
                                if ((pFsbMsgBuf =
                                     CRU_BUF_Allocate_MsgBufChain (u4PacketLen,
                                                                   FSB_OFFSET_NONE))
                                    == NULL)

                                {
                                    FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                                     FSB_DEBUGGING_LEVEL,
                                                     FSB_BUFFER_TRC,
                                                     "FsbPacketTxFrame :Allocate Buffer returned Failure\n");
                                    FsUtilReleaseBitList (*pPortList);
                                    return FSB_FAILURE;
                                }

                                /* Copy the passed Ethernet frame into the CRU buffer */
                                if (CRU_BUF_Copy_OverBufChain
                                    (pFsbMsgBuf, pFsbNotifyParams->au1DataBuf,
                                     FSB_OFFSET_NONE,
                                     u4PacketLen) == CRU_FAILURE)

                                {
                                    /* Release the CRU buf chain for Ethernet frame */
                                    if (CRU_BUF_Release_MsgBufChain
                                        (pFsbMsgBuf, FALSE) != CRU_SUCCESS)

                                    {
                                        FSB_CONTEXT_TRC (pFSBPktInfo->
                                                         u4ContextId,
                                                         FSB_DEBUGGING_LEVEL,
                                                         FSB_BUFFER_TRC,
                                                         "FsbPacketTxFrame :Release Buffer returned Failure\n");
                                    }
                                    FsUtilReleaseBitList (*pPortList);
                                    return FSB_FAILURE;
                                }

                                FsbStripVlanTag (pFsbMsgBuf);

                                MEMSET (gau1TxBuf, 0, FSB_MAX_FIP_PKT_LEN);
                                u4PacketLen =
                                    (UINT4)
                                    CRU_BUF_Get_ChainValidByteCount
                                    (pFsbMsgBuf);
                                CRU_BUF_Copy_FromBufChain (pFsbMsgBuf,
                                                           gau1TxBuf, 0,
                                                           u4PacketLen);

                                /* Release the CRU buf chain for Ethernet frame */
                                if (CRU_BUF_Release_MsgBufChain
                                    (pFsbMsgBuf, FALSE) != CRU_SUCCESS)

                                {
                                    FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                                     FSB_DEBUGGING_LEVEL,
                                                     FSB_BUFFER_TRC,
                                                     "FsbPacketTxFrame :Release Buffer returned Failure\n");
                                    FsUtilReleaseBitList (*pPortList);
                                    return FSB_FAILURE;
                                }
                                FsbCfaGetIfType (u4LocalPort, &u1IfType);
                                if (u1IfType == CFA_LAGG)
                                {
                                    if (FsbL2IwfGetPotentialTxPortForAgg
                                        (pFsbNotifyParams->u4ContextId,
                                         pFsbNotifyParams->u2VlanId,
                                         (UINT2) u4LocalPort,
                                         &u2PortIndex) == FSB_SUCCESS)
                                    {
                                        /* send through the active port of the portchannel */
                                        u4LocalPort = (UINT4) u2PortIndex;
                                    }
                                }
                                if ((FsbPortCfaGddEthWrite
                                     (gau1TxBuf, u4LocalPort,
                                      u4PacketLen)) == FSB_FAILURE)
                                {
                                    FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                                     FSB_ERROR_LEVEL, FSB_NONE,
                                                     "FsbPacketTxFrame : FsbPortCfaGddEthWrite failed\n");
                                    FsUtilReleaseBitList (*pPortList);
                                    return FSB_FAILURE;
                                }
                                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                                 FSB_DEBUGGING_LEVEL,
                                                 FSB_CONTROL_PATH_TRC,
                                                 "Packet Type: %s of length %d transmitted in context %d in port %d of "
                                                 "VLAN %d  \r\n",
                                                 FsbErrString[pFSBPktInfo->
                                                              PacketType],
                                                 u4PacketLen,
                                                 pFSBPktInfo->u4ContextId,
                                                 u4LocalPort,
                                                 pFSBPktInfo->u2VlanId);
                            }
                        }
                    }
                }
                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }
        }
    }
    FsUtilReleaseBitList (*pPortList);
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbGetUntaggedVlan                                   */
/*                                                                           */
/* Description        : This function is used to retrieve the value of       */
/*                      Untagged VLAN configured                             */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : pu2VlanIndex - VLAN Identifier                       */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE                            */
/*****************************************************************************/
INT4
FsbGetUntaggedVlanId (UINT4 u4ContextId, UINT2 *pu2VlanIndex)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4ContextId);

    if (pFsbContextInfo != NULL)
    {
        *pu2VlanIndex = pFsbContextInfo->u2DefaultVlanId;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbUpdateVlanPortList                            */
/*                                                                           */
/*    Description         : This function is used to update tFsbIntfEntry    */
/*                          when a port is added/deleted to a VLAN           */
/*                          that has been marked as FCcE VLAN in L2IWF       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId - VLAN Index                              */
/*                          AddedPorts - Added Port List                     */
/*                          DeletedPorts - Deleted Port List                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbUpdateVlanPortList (UINT4 u4ContextId, tVlanId VlanId,
                       tLocalPortList AddedPorts, tLocalPortList DeletedPorts)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4Port = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;
    UINT1              *pNullLocalPortList = NULL;

    pNullLocalPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pNullLocalPortList == NULL)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbUpdateVlanPortList: pNullLocalPortList memalloc failed\r\n");
        return FSB_FAILURE;
    }

    FSB_MEMSET (pNullLocalPortList, 0, sizeof (tLocalPortList));

    if (FSB_MEMCMP (DeletedPorts, pNullLocalPortList, sizeof (tLocalPortList))
        != 0)
    {
        for (u2ByteIndex = 0; u2ByteIndex < CONTEXT_PORT_LIST_SIZE;
             u2ByteIndex++)
        {
            /* Looping to find for Index in DeletedPortList */
            u1PortFlag = DeletedPorts[u2ByteIndex];

            for (u2BitIndex = 0; ((u2BitIndex < BITS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
            {
                if ((u1PortFlag & VLAN_BIT8) != 0)
                {
                    u4Port = (UINT4) ((u2ByteIndex * BITS_PER_BYTE) +
                                      u2BitIndex + 1);
                    if (FsbGetIfIndexFromLocalPort (u4ContextId,
                                                    (UINT2) u4Port,
                                                    &u4IfIndex) != FSB_SUCCESS)
                    {
                        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                         "FsbUpdateVlanPortList : FsbGetIfIndexFromLocalPort failed"
                                         " for localPort Index %d for deleted ports in VLAN\r\n",
                                         u4Port, VlanId);
                        UtilPlstReleaseLocalPortList (pNullLocalPortList);
                        return FSB_FAILURE;
                    }
                    FsbDeleteIntfEntry (VlanId, u4IfIndex);
                    FsbHandleVLANPortDelete (VlanId, u4IfIndex);
                }
                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }
        }
    }

    if (FSB_MEMCMP (AddedPorts, pNullLocalPortList, sizeof (tLocalPortList))
        != 0)
    {
        for (u2ByteIndex = 0; u2ByteIndex < CONTEXT_PORT_LIST_SIZE;
             u2ByteIndex++)
        {
            /* Looping to find for Index in AddedPortList */
            u1PortFlag = AddedPorts[u2ByteIndex];

            for (u2BitIndex = 0; ((u2BitIndex < BITS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
            {
                if ((u1PortFlag & VLAN_BIT8) != 0)
                {
                    u4Port = (UINT4) ((u2ByteIndex * BITS_PER_BYTE) +
                                      u2BitIndex + 1);
                    if (FsbGetIfIndexFromLocalPort (u4ContextId,
                                                    (UINT2) u4Port,
                                                    &u4IfIndex) != FSB_SUCCESS)
                    {
                        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                         "FsbUpdateVlanPortList : FsbGetIfIndexFromLocalPort failed"
                                         " for localPort Index %d for Added ports in VLAN %d\r\n",
                                         u4Port, VlanId);
                        UtilPlstReleaseLocalPortList (pNullLocalPortList);
                        return FSB_FAILURE;
                    }
                    FsbPopulateDefFilterAndIntfEntry (u4ContextId, VlanId,
                                                      u4IfIndex);
                }
                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }
        }
    }

    if (FsbUpdateVLANFilter (u4ContextId, VlanId) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbUpdateVlanPortList : FsbUpdateVLANFilter failed\r\n");
        UtilPlstReleaseLocalPortList (pNullLocalPortList);
        return FSB_FAILURE;
    }

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4ContextId, VlanId);
    if (FsbRedSyncUpDefaultFilter (pFsbFipSnoopingEntry) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbUpdateVlanPortList: FsbRedSyncUpDefaultFilter "
                         "for VlanId: %d\r\n", pFsbFipSnoopingEntry->u2VlanId);
        UtilPlstReleaseLocalPortList (pNullLocalPortList);
        return FSB_FAILURE;
    }

    UtilPlstReleaseLocalPortList (pNullLocalPortList);
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbUpdateVlanPorts                               */
/*                                                                           */
/*    Description         : This function is used to update tFsbIntfEntry    */
/*                          when a port is added/deleted to a VLAN           */
/*                          that has been marked as FCcE VLAN in L2IWF       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId - VLAN Index                              */
/*                          AddedPorts - Added Port List                     */
/*                          DeletedPorts - Deleted Port List                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbUpdateVlanPorts (UINT4 u4ContextId, tVlanId VlanId, UINT4 u4LocalPortId)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    UINT4               u4IfIndex = 0;

    FsbGetIfIndexFromLocalPort (u4ContextId, (UINT2) u4LocalPortId, &u4IfIndex);
    FsbPopulateDefFilterAndIntfEntry (u4ContextId, VlanId, u4IfIndex);

    if (FsbUpdateVLANFilter (u4ContextId, VlanId) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbUpdateVlanPortList : FsbUpdateVLANFilter failed\r\n");
        return FSB_FAILURE;
    }

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4ContextId, VlanId);
    if (FsbRedSyncUpDefaultFilter (pFsbFipSnoopingEntry) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbUpdateVlanPortList: FsbRedSyncUpDefaultFilter "
                         "for VlanId: %d\r\n", pFsbFipSnoopingEntry->u2VlanId);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbUpdateVLANFilter                              */
/*                                                                           */
/*    Description         : This function is used to update PortList of the  */
/*                          already installed filters                        */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2VlanId - VLAN Index                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbUpdateVLANFilter (UINT4 u4ContextId, UINT2 u2VlanId)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbContextInfo    *pFsbContextInfo = NULL;
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;
    tPortList          *pSolicitationPortList = NULL;
    tPortList          *pAdvertisementPortList = NULL;
    UINT4               u4HwFilterId = 0;

    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    pSolicitationPortList = (tPortList *) FsUtilAllocBitList
        (sizeof (tPortList));

    if (pSolicitationPortList == NULL)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbUpdateVLANFilter : FsUtilAllocBitList"
                         " for pSolicitationPortList failed\r\n");
        return FSB_FAILURE;
    }

    pAdvertisementPortList = (tPortList *) FsUtilAllocBitList
        (sizeof (tPortList));

    if (pAdvertisementPortList == NULL)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbUpdateVLANFilter : FsUtilAllocBitList"
                         " for pAdvertisementPortList failed\r\n");
        FsUtilReleaseBitList ((UINT1 *) pSolicitationPortList);
        return FSB_FAILURE;
    }

    FSB_MEMSET (*pSolicitationPortList, 0, sizeof (tPortList));
    FSB_MEMSET (*pAdvertisementPortList, 0, sizeof (tPortList));

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4ContextId, u2VlanId);

    pFsbContextInfo = FsbCxtGetContextEntry (u4ContextId);

    if ((pFsbFipSnoopingEntry != NULL) && (pFsbContextInfo != NULL))
    {
        if ((pFsbFipSnoopingEntry->u1EnabledStatus != FSB_ENABLE) ||
            (pFsbContextInfo->u1ModuleStatus != FSB_ENABLE))
        {
            /* If either Enabled status of VLAN and Module status
             * is not enabled, then VLAN filters will not be added.
             * Hence, there iis no need to update Filter Entries */
            FsUtilReleaseBitList ((UINT1 *) pSolicitationPortList);
            FsUtilReleaseBitList ((UINT1 *) pAdvertisementPortList);
            return FSB_SUCCESS;
        }

        /* Below function call will populate PortList with
         * ENode Facing ports for solicitation and FCF facing
         * ports for advertisement */
        FsbUtilPopulatePortList (u2VlanId, u4ContextId,
                                 FSB_ENODE_FACING, *pSolicitationPortList);
        FsbUtilPopulatePortList (u2VlanId, u4ContextId,
                                 FSB_FCF_FACING, *pAdvertisementPortList);

        pFsbFilterEntry = (tFsbFilterEntry *)
            RBTreeGetFirst (pFsbFipSnoopingEntry->FsbFilterEntry);

        while (pFsbFilterEntry != NULL)
        {
            if ((pFsbFilterEntry->u2OpcodeFilterOffsetValue == FSB_DISCOVERY)
                && (pFsbFilterEntry->u2SubOpcodeFilterOffsetValue ==
                    FSB_DISCOVERY_SOLICITATION))
            {
                if (FSB_MEMCMP
                    (pFsbFilterEntry->PortList, pSolicitationPortList,
                     sizeof (tPortList)) != 0)
                {
                    /* Delete the existing filter entry */
                    if (FsFsbHwWrDeleteFilter (pFsbFilterEntry,
                                               pFsbFilterEntry->u4HwFilterId) !=
                        FSB_SUCCESS)
                    {
                        FSB_CONTEXT_TRC (u4ContextId,
                                         FSB_DEBUGGING_LEVEL,
                                         FSB_CONTROL_PATH_TRC,
                                         "FsbUpdateVLANFilter: FsFsbHwWrDeleteFilter failed\r\n");
                    }
                    /* The portList for discovery solicitaion
                     * filters has been changed */
                    FSB_MEMCPY (pFsbFilterEntry->PortList,
                                pSolicitationPortList, sizeof (tPortList));

                    /* Install Filter with the updated PortList */
                    if (FsFsbHwWrCreateFilter (pFsbFilterEntry, &u4HwFilterId)
                        != FSB_SUCCESS)
                    {
                        FSB_CONTEXT_TRC (u4ContextId,
                                         FSB_ERROR_LEVEL, FSB_NONE,
                                         "FsbUpdateVLANFilter: FsFsbHwWrCreateFilter"
                                         " with VlanId: %d failed\r\n",
                                         pFsbFilterEntry->u2VlanId);
                        RBTreeRemove (pFsbFipSnoopingEntry->FsbFilterEntry,
                                      (tRBElem *) pFsbFilterEntry);
                        MemReleaseMemBlock (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                                            (UINT1 *) pFsbFilterEntry);
                        FsUtilReleaseBitList ((UINT1 *) pSolicitationPortList);
                        FsUtilReleaseBitList ((UINT1 *) pAdvertisementPortList);
                        return FSB_FAILURE;
                    }
                    pFsbFilterEntry->u4HwFilterId = u4HwFilterId;
                }
            }
            if ((pFsbFilterEntry->u2OpcodeFilterOffsetValue == FSB_DISCOVERY)
                && (pFsbFilterEntry->u2SubOpcodeFilterOffsetValue ==
                    FSB_DISCOVERY_ADVERTISEMENT))
            {
                if (FSB_MEMCMP
                    (pFsbFilterEntry->PortList, pAdvertisementPortList,
                     sizeof (tPortList)) != 0)
                {
                    /* Delete the existing filter entry */
                    if (FsFsbHwWrDeleteFilter (pFsbFilterEntry,
                                               pFsbFilterEntry->u4HwFilterId) !=
                        FSB_SUCCESS)
                    {
                        FSB_CONTEXT_TRC (u4ContextId,
                                         FSB_DEBUGGING_LEVEL,
                                         FSB_CONTROL_PATH_TRC,
                                         "FsbUpdateVLANFilter:Deleting existing"
                                         "filter failed\r\n");
                    }

                    /* The portList for discovery advertisement 
                     * filters has been changed */
                    FSB_MEMCPY (pFsbFilterEntry->PortList,
                                pAdvertisementPortList, sizeof (tPortList));

                    /* Install Filter with the updated PortList */
                    if (FsFsbHwWrCreateFilter (pFsbFilterEntry, &u4HwFilterId)
                        != FSB_SUCCESS)
                    {
                        FSB_CONTEXT_TRC (u4ContextId,
                                         FSB_ERROR_LEVEL, FSB_NONE,
                                         "FsbUpdateVLANFilter:FsFsbHwWrCreateFilter with "
                                         "VlanId: %d failed\r\n",
                                         pFsbFilterEntry->u2VlanId);
                        RBTreeRemove (pFsbFipSnoopingEntry->FsbFilterEntry,
                                      (tRBElem *) pFsbFilterEntry);
                        MemReleaseMemBlock (FSB_FILTER_ENTRIES_MEMPOOL_ID,
                                            (UINT1 *) pFsbFilterEntry);
                        FsUtilReleaseBitList ((UINT1 *) pSolicitationPortList);
                        FsUtilReleaseBitList ((UINT1 *) pAdvertisementPortList);
                        return FSB_FAILURE;
                    }
                    pFsbFilterEntry->u4HwFilterId = u4HwFilterId;
                }
            }

            FSB_MEMCPY (FsbFilterEntry.DstMac, pFsbFilterEntry->DstMac,
                        MAC_ADDR_LEN);
            pFsbFilterEntry = (tFsbFilterEntry *)
                RBTreeGetNext (pFsbFipSnoopingEntry->FsbFilterEntry,
                               (tRBElem *) & FsbFilterEntry, NULL);
        }
    }
    FsUtilReleaseBitList ((UINT1 *) pSolicitationPortList);
    FsUtilReleaseBitList ((UINT1 *) pAdvertisementPortList);
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbGetFcoeVlanForIfIndex                             */
/*                                                                           */
/* Description        : This function is used to retrieve the FCoE VLAN for  */
/*                      the given Interface Index                            */
/*                                                                           */
/* Input(s)           : u4IfIndex- Interface Index                           */
/*                                                                           */
/* Output(s)          : pu2VlanIndex - VLAN Identifier                       */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE                            */
/*****************************************************************************/
INT4
FsbGetFcoeVlanForIfIndex (UINT4 u4IfIndex, UINT2 *pu2FcoeVlanId)
{
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbIntfEntry       FsbIntfEntry;

    FSB_MEMSET (&FsbIntfEntry, 0, sizeof (tFsbIntfEntry));

    FsbIntfEntry.u4IfIndex = u4IfIndex;
    FsbIntfEntry.u2VlanId = 0;

    *pu2FcoeVlanId = 0;

    while ((pFsbIntfEntry = (tFsbIntfEntry *)
            RBTreeGetNext (gFsbGlobals.FsbIntfTable,
                           (tRBElem *) & FsbIntfEntry, NULL)) != NULL)
    {
        if (pFsbIntfEntry->u4IfIndex == u4IfIndex)
        {
            *pu2FcoeVlanId = pFsbIntfEntry->u2VlanId;
            break;
        }
        FsbIntfEntry.u4IfIndex = pFsbIntfEntry->u4IfIndex;
        FsbIntfEntry.u2VlanId = pFsbIntfEntry->u2VlanId;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbValidateFIPFrame                              */
/*                                                                           */
/*    Description         : This function is used to validate FIP packet     */
/*                          based on the port role of the interface and MAC  */
/*                          address                                          */
/*                                                                           */
/*    Input(s)            : pFSBPktInfo - Pointer to tFSBPktInfo             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbValidateFIPFrame (tFSBPktInfo * pFSBPktInfo)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbIntfEntry       FsbIntfEntry;
    FSB_MEMSET (&FsbIntfEntry, 0, sizeof (tFsbIntfEntry));

    if (pFSBPktInfo == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbValidateFIPFrame: pFSBPktInfo is NULL\r\n");
        return FSB_FAILURE;
    }

    if ((pFSBPktInfo->PacketType != FSB_FIP_VLAN_DISCOVERY)
        && (pFSBPktInfo->PacketType != FSB_FIP_VLAN_NOTIFICATION))
    {
        pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (pFSBPktInfo->u4ContextId,
                                                       pFSBPktInfo->u2VlanId);
        if (pFsbFipSnoopingEntry == NULL)
        {
            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbValidateFIPFrame : Packet %d received on VLAN %d"
                             " is not a FCoE VLAN : pFsbFipSnoopingEntry is NULL\r\n",
                             pFSBPktInfo->PacketType, pFSBPktInfo->u2VlanId);
            return FSB_FAILURE;
        }

        if (pFsbFipSnoopingEntry->u1EnabledStatus != FSB_ENABLE)
        {
            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbValidateFIPFrame :  FCoE VLAN %d is not enabled\r\n",
                             pFSBPktInfo->u2VlanId);
            return FSB_FAILURE;
        }

        FsbIntfEntry.u2VlanId = pFSBPktInfo->u2VlanId;
        FsbIntfEntry.u4IfIndex = pFSBPktInfo->u4IfIndex;

        pFsbIntfEntry = RBTreeGet (gFsbGlobals.FsbIntfTable,
                                   (tRBElem *) & FsbIntfEntry);

        if (pFsbIntfEntry == NULL)
        {
            FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                             FSB_DEBUGGING_LEVEL, FSB_OS_RESOURCE_TRC,
                             "FsbValidateFIPFrame for the packet %d received on IfIndex: %d "
                             "is not a member of FCoE Vlan: %d : pFsbIntfEntry is NULL\r\n",
                             pFSBPktInfo->PacketType, pFSBPktInfo->u2VlanId,
                             pFSBPktInfo->u4IfIndex);
            return FSB_FAILURE;
        }

    }
    switch (pFSBPktInfo->PacketType)
    {
        case FSB_FIP_ADV_MCAST:
        case FSB_FIP_ADV_UCAST:
        case FSB_FIP_FLOGI_ACCEPT:
        case FSB_FIP_FLOGI_REJECT:
        case FSB_FIP_NPIV_FDISC_ACCEPT:
        case FSB_FIP_NPIV_FDISC_REJECT:
        case FSB_FIP_LOGO_ACCEPT:
        case FSB_FIP_LOGO_REJECT:
            if (pFsbIntfEntry->u1PortRole == FSB_ENODE_FACING)
            {
                /* If the above packets are recevied are ENode
                 * facing port, return FAILURE */
                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbValidateFIPFrame :"
                                 " Invalid Packet Received"
                                 " Type:%d Context:%d VLAN:%d IfIndex : %d\r\n",
                                 pFSBPktInfo->PacketType,
                                 pFSBPktInfo->u4ContextId,
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
                return FSB_FAILURE;
            }
            break;

        case FSB_FIP_CLEAR_LINK:
            if (pFsbIntfEntry->u1PortRole == FSB_ENODE_FACING)
            {
                /* If the above packets are recevied are ENode
                 * facing port, return FAILURE */
                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbValidateFIPFrame :"
                                 " Invalid Packet Received"
                                 " Type:%s Context:%d VLAN:%d IfIndex : %d\r\n",
                                 FsbErrString[pFSBPktInfo->PacketType],
                                 pFSBPktInfo->u4ContextId,
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
                MemReleaseMemBlock (FSB_FCOE_MAC_ADDR_MEMPOOL_ID,
                                    (UINT1 *) pFSBPktInfo->FsbFipsPktFields.
                                    FIPClearLink.pVNMacAddr);
                return FSB_FAILURE;
            }
            break;

        case FSB_FIP_SOLICIT_MCAST:
        case FSB_FIP_SOLICIT_UCAST:
        case FSB_FIP_FLOGI_REQUEST:
        case FSB_FIP_NPIV_FDISC_REQUEST:
        case FSB_FIP_LOGO_REQUEST:
        case FSB_FIP_ENODE_KEEP_ALIVE:
        case FSB_FIP_VNPORT_KEEP_ALIVE:
            if (pFsbIntfEntry->u1PortRole == FSB_FCF_FACING)
            {
                /* If the above packets are recevied are FCF
                 * facing port, return FAILURE */
                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbValidateFIPFrame :"
                                 " Invalid Packet Received"
                                 " Type:%s Context:%d VLAN:%d IfIndex : %d\r\n",
                                 FsbErrString[pFSBPktInfo->PacketType],
                                 pFSBPktInfo->u4ContextId,
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
                return FSB_FAILURE;
            }
            break;

        case FSB_FIP_VLAN_DISCOVERY:
            if (FSB_MEMCMP (pFSBPktInfo->DestAddr, ALL_FCF_MAC,
                            MAC_ADDR_LEN) != 0)
            {
                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbValidateFIPFrame :"
                                 " Invalid Packet Received"
                                 " Type:%s Context:%d VLAN:%d IfIndex : %d\r\n",
                                 FsbErrString[pFSBPktInfo->PacketType],
                                 pFSBPktInfo->u4ContextId,
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
                return FSB_FAILURE;
            }
            break;

        case FSB_FIP_VLAN_NOTIFICATION:
            if ((FSB_MEMCMP (pFSBPktInfo->DestAddr, ALL_FCF_MAC,
                             MAC_ADDR_LEN) == 0) ||
                (FSB_MEMCMP (pFSBPktInfo->DestAddr, ALL_ENODE_MAC,
                             MAC_ADDR_LEN) == 0))
            {
                FSB_CONTEXT_TRC (pFSBPktInfo->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbValidateFIPFrame :"
                                 " Invalid Packet Received"
                                 " Type:%s Context:%d VLAN:%d IfIndex : %d\r\n",
                                 FsbErrString[pFSBPktInfo->PacketType],
                                 pFSBPktInfo->u4ContextId,
                                 pFSBPktInfo->u2VlanId, pFSBPktInfo->u4IfIndex);
                return FSB_FAILURE;
            }
            break;

        case FSB_FIP_INITIAL_STATE:
            return FSB_FAILURE;
            break;

        default:
            break;

    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbHandleFIPFrame                                */
/*                                                                           */
/*    Description         : This functions is used to handle the incoming    */
/*                          FIP frame                                        */
/*                                                                           */
/*    Input(s)            : pFsbNotifyParams -  Pointer to tFsbNotifyParams  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS/FSB_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
FsbHandleFIPFrame (tFsbNotifyParams * pMsg)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
#ifdef FSB_CUSTOM_FCOE_TAGGED_WANTED
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
#endif
    tVlanTag            VlanTag;
    INT4                i4BrgPortType = 0;
    UINT4               u4SChIfIndex = 0;
    UINT4               u4ContextId = 0;
    UINT2               u2AggId = 0;
    UINT2               u2EtherType = 0;
    UINT2               u2SVlanId = 0;
    UINT2               u2VlanIdMask = 0xfff;
    UINT2               u2OpCode = 0;
#ifdef FSB_CUSTOM_FCOE_TAGGED_WANTED
    UINT2               u2SubOpCode = 0;
    UINT1              *pu1ReadPtr = NULL;
#endif
    UINT2               u2VlanId = 0;
    UINT1               u1PortState = 0;
    tMacAddr            DestAddr;

    FSB_MEMSET (&DestAddr, 0, sizeof (tMacAddr));

/*
    Incoming FIP frame shall be of the following types .

    With EVB
    ===========
    1.  Only S-VLAN Tagged (VLAN Discovery)
    2.  S-VLAN & C-VLAN Tagged (All other FIP frames)

    Without EVB
    =======
    3.  Untagged /Priority tagged (VLAN Discovery)
    4.  C-VLAN Tagged (All other FIP frames)

    Scenario 1:
     ======
     . When only S-VLAN Tagged packet is received, the packet will either be
       classified to PVID/or it will be classified to FIP Snooping Untagged VLAN
       after the S-Tag is removed.

     . The above classification will be done based on the flag
       FSB_CUSTOM_FCOE_TAGGED_WANTED - which specifies that the 
       untagged packet is to be classified to FCoE VLAN for the which the 
       incoming ports is the member.  
       This is done irrespective of the PVID configured for the port.
       
     . The classified VLAN ID is used as CVLAN for further processing.

     Scenario 2:
     =========
     . When a double tagged packet is received, S-Tag in the packet is removed
       and the packet is given to FSB module with single C-VLAN tagged packet.

     Scenario 3:
     ========

     . When only untagged packet is received on the switch port, 
       the packet is tagged based on the switch port pvid (in the hardware).

       This have to retained as CVLAN or classified to FCOE VLAN for which 
       this port is member port.

     . The above classification will be done based on the flag
       FSB_CUSTOM_FCOE_TAGGED_WANTED - which specifies that the 
       untagged packet is to be classified to FCoE VLAN for the which the
       incoming ports is the member. 
       This is done irrespective of the PVID configured for the port.

     Scenario 4:
     =========
     . When a single tagged packet is received, the packet is directly given
       to FSB module with single C-VLAN tagged packet.

*/

    /* Retrieve the context id from the Interface Index */
    if (FsbGetContextIdFromCfaIfIndex (pMsg->u4IfIndex,
                                       &u4ContextId) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                         FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                         "FsbSendPacketToFsbQueue: IfIndex: %d is not "
                         "associated with any ContextId\r\n", pMsg->u4IfIndex);
        return FSB_FAILURE;
    }

    FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL, FSB_DUMP_TRC,
                     "FsbHandleFIPFrame : Received frame of size of %d"
                     " on port %d\r\n", pMsg->u4PacketLen, pMsg->u4IfIndex);
    FSB_PKT_DUMP (u4ContextId, FSB_DUMP_TRC, pMsg->au1DataBuf,
                  pMsg->u4PacketLen);

#ifdef FSB_CUSTOM_FCOE_TAGGED_WANTED
    pu1ReadPtr = pMsg->au1DataBuf;
#endif

    FsbCfaGetInterfaceBrgPortType (pMsg->u4IfIndex, &i4BrgPortType);
    if (i4BrgPortType == CFA_UPLINK_ACCESS_PORT)
    {
        /* EVB scenario */
        if ((pBuf =
             CRU_BUF_Allocate_MsgBufChain (pMsg->u4PacketLen,
                                           FSB_OFFSET_NONE)) == NULL)
        {
            FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                             "FsbHandleFIPFrame : Allocate Buffer"
                             " returned Failure\r\n");
            return FSB_FAILURE;
        }

        /* Copy the passed Ethernet frame into the CRU buffer */
        if (CRU_BUF_Copy_OverBufChain
            (pBuf, pMsg->au1DataBuf, FSB_OFFSET_NONE,
             pMsg->u4PacketLen) == CRU_FAILURE)

        {
            /* Release the CRU buf chain for Ethernet frame */
            if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) != CRU_SUCCESS)

            {
                FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbHandleFIPFrame :Release Buffer"
                                 " returned Failure\r\n");
            }

            return FSB_FAILURE;
        }

        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2EtherType,
                                   FSB_ETHER_TYPE_OFFSET, FSB_ETHER_TYPE_LEN);

        if (OSIX_NTOHS (u2EtherType) == FSB_ETHER_TYPE)
        {

            /* Scenario 1 - Packet in EVB Port, SVLAN tag only. */

            /* Retrieve the S-VLAN ID from the packet */
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2SVlanId,
                                       VLAN_ID_OFFSET, FSB_TWO_BYTE);
            u2SVlanId = OSIX_NTOHS (u2SVlanId);
            u2SVlanId = u2SVlanId & u2VlanIdMask;

            /* Strip the S-VLAN tag from the packet */
            FsbStripVlanTag (pBuf);

            /* Retrieve the S-channel interface index, based on UAP
             * and S-VLAN id*/
            if (FsbVlanApiGetSChIfIndex (u4ContextId, pMsg->u4IfIndex,
                                         u2SVlanId,
                                         &u4SChIfIndex) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (u4ContextId,
                                 FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                                 "FsbHandleFIPFrame : FsbVlanApiGetSChIfIndex"
                                 " return failure\r\n");
                /* Release the CRU buf chain for Ethernet frame */
                if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) != CRU_SUCCESS)

                {
                    FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                     "FsbHandleFIPFrame :Release Buffer"
                                     " returned Failure\r\n");
                    return FSB_FAILURE;
                }
                return FSB_FAILURE;
            }
            else
            {
                pMsg->u4IfIndex = u4SChIfIndex;
                /* Get the corresponding FCoE VLAN from the FIP 
                 * Snooping Module/ Get the FIP snooping untagged
                 * VLAN */
#ifdef FSB_CUSTOM_FCOE_TAGGED_WANTED
                /* Get the FCoE VLAN for the received interface */
                FsbGetFcoeVlanForIfIndex (pMsg->u4IfIndex, &u2VlanId);

                if (u2VlanId == 0)
                {
                    FSB_CONTEXT_TRC (u4ContextId,
                                     FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                                     "FsbHandleFIPFrame : Packet recieved in IfIndex: %d is not "
                                     "a member of any FCOE Vlan\r\n",
                                     pMsg->u4IfIndex);
                    /* Release the CRU buf chain for Ethernet frame */
                    if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE)
                        != CRU_SUCCESS)

                    {
                        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                         "FsbHandleFIPFrame :Release Buffer"
                                         " returned Failure\r\n");
                        return FSB_FAILURE;
                    }
                    return FSB_FAILURE;
                }
#else
                /* Get the Untagged VLAN used in the sytem, for which this 
                 *untagged packets needs to be classified */
                FsbGetUntaggedVlanId (u4ContextId, &u2VlanId);
#endif
            }
            pMsg->u2VlanId = u2VlanId;

            /* Tag the incoming frame with the classified CVLAN tag */
            FsbVlanTagFrame (pBuf, pMsg->u2VlanId, 0);
        }
        else
        {

            /* Scenario 2 - Packet in EVB Port, SVLAN and CVLAN tag */

            /* Double tagged packet is recieved, so stripping the outer tag
             * and sending the packet to FSB module, with the corresponding
             * s-channel interface and C-VLAN id (Scenario 2)*/

            /* The below function FsbVlanGetVlanInfoAndUntagFrame() - 
             *  1) Removes the Stag from the buffer.
             *  2) Fills the VlanTag structure with Outer and Inner VLAN Id.
             */

            /* There are scenarios in which the CNA sends VLAN_DISCOVERY as
             * double tagged packet. The packet can be of following types,
             * 
             * 1) S-VLAN (Valid), C-VLAN tagged (Valid)
             * 2) S-VLAN (Valid), C-VLAN tagged (0)
             * 3) S-VLAN (Valid), C-VLAN tagged (1)
             * 4) S-VLAN (Valid), C-VLAN tagged (VLAN not present in system)
             * 5) S-VLAN (Valid) (Handled in above If case) 
             */
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) DestAddr,
                                       0, MAC_ADDR_LEN);
            /* Retrieve the S-VLAN ID from the packet */
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2OpCode,
                                       FSB_EVB_OPCODE_OFFSET, FSB_TWO_BYTE);

            /* Destination MAC should be ALL-FCF-MAC and Opcode should be
             * VLAN_DISCOVERY_OPCODE */
            if ((FSB_MEMCMP (DestAddr, ALL_FCF_MAC, MAC_ADDR_LEN) == 0)
                && (OSIX_NTOHS (u2OpCode) == FSB_FIP_VLAN_OPCODE))
            {
                /* This is a VLAN discovery packet. */
                /* Retrieve the S-VLAN ID from the packet */
                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2SVlanId,
                                           VLAN_ID_OFFSET, FSB_TWO_BYTE);
                u2SVlanId = OSIX_NTOHS (u2SVlanId);
                u2SVlanId = u2SVlanId & u2VlanIdMask;
                /* Strip the S-VLAN tag from the packet */
                FsbStripVlanTag (pBuf);

                /* Retreive the S-Channel interface index */
                if (FsbVlanApiGetSChIfIndex (u4ContextId, pMsg->u4IfIndex,
                                             u2SVlanId,
                                             &u4SChIfIndex) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (u4ContextId,
                                     FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                                     "FsbHandleFIPFrame : FsbVlanApiGetSChIfIndex"
                                     " return failure\r\n");
                    /* Release the CRU buf chain for Ethernet frame */
                    if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE)
                        != CRU_SUCCESS)

                    {
                        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                         "FsbHandleFIPFrame :Release Buffer"
                                         " returned Failure\r\n");
                        return FSB_FAILURE;
                    }
                    return FSB_FAILURE;
                }
                else
                {
                    /* Get the FCoE VLAN for the received S-Channel interface */
                    /* This is for handling all the cases with C-VLAN value 
                     * as 0,1,non-existing VLANs.Replace it with the FCoE VLAN*/
                    FsbGetFcoeVlanForIfIndex (u4SChIfIndex, &u2VlanId);
                    if (u2VlanId == 0)
                    {
                        FSB_CONTEXT_TRC (u4ContextId,
                                         FSB_DEBUGGING_LEVEL,
                                         FSB_CONTROL_PATH_TRC,
                                         "FsbHandleFIPFrame : Packet recieved in IfIndex: %d is not "
                                         "a member of any FCOE Vlan\r\n",
                                         pMsg->u4IfIndex);
                        /* Release the CRU buf chain for Ethernet frame */
                        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE)
                            != CRU_SUCCESS)

                        {
                            FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL,
                                             FSB_BUFFER_TRC,
                                             "FsbHandleFIPFrame :Release Buffer"
                                             " returned Failure\r\n");
                            return FSB_FAILURE;
                        }
                        return FSB_FAILURE;
                    }

                    /* Fill the S-Channel Interface Index and CVLAN Id */
                    pMsg->u4IfIndex = u4SChIfIndex;
                    pMsg->u2VlanId = u2VlanId;

                    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2VlanId,
                                               VLAN_ID_OFFSET, FSB_TWO_BYTE);
                }
            }
            else
            {

                /* Double-tagged FIP frames(except VLAN discovery) 
                 * will be covered in this case*/
                if (FsbVlanGetVlanInfoAndUntagFrame (pBuf, pMsg->u4IfIndex,
                                                     &VlanTag) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (u4ContextId,
                                     FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                                     "FsbHandleFIPFrame : FsbVlanGetVlanInfoAndUntagFrame"
                                     " return failure\r\n");
                    if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE)
                        != CRU_SUCCESS)

                    {
                        FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL,
                                         FSB_BUFFER_TRC,
                                         "FsbHandleFIPFrame :Release Buffer"
                                         " returned Failure\r\n");
                        return FSB_FAILURE;
                    }
                    return FSB_FAILURE;
                }

                /* With SVLAN ID (Stag) and IfIndex, fetch the S-Channel Interface */

                if (FsbVlanApiGetSChIfIndex (u4ContextId, pMsg->u4IfIndex,
                                             VlanTag.OuterVlanTag.u2VlanId,
                                             &u4SChIfIndex) != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (u4ContextId,
                                     FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                                     "FsbHandleFIPFrame : FsbVlanApiGetSChIfIndex"
                                     " return failure\r\n");
                    /* Release the CRU buf chain for Ethernet frame */
                    if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE)
                        != CRU_SUCCESS)

                    {
                        FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL,
                                         FSB_BUFFER_TRC,
                                         "FsbHandleFIPFrame :Release Buffer"
                                         " returned Failure\r\n");
                        return FSB_FAILURE;
                    }
                    return FSB_FAILURE;
                }
                else
                {
                    /* Fill the S-Channel Interface Index and CVLAN Id */
                    pMsg->u4IfIndex = u4SChIfIndex;
                    pMsg->u2VlanId = VlanTag.InnerVlanTag.u2VlanId;
                }
            }
        }
        MEMSET (pMsg->au1DataBuf, 0, FSB_MAX_FIP_PKT_LEN);
        pMsg->u4PacketLen = (UINT4) CRU_BUF_Get_ChainValidByteCount (pBuf);
        CRU_BUF_Copy_FromBufChain (pBuf, pMsg->au1DataBuf, 0,
                                   pMsg->u4PacketLen);

        /* Release the CRU buf chain for Ethernet frame */
        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) != CRU_SUCCESS)

        {
            FSB_CONTEXT_TRC (u4ContextId,
                             FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                             "FsbHandleFIPFrame :Release Buffer"
                             " returned Failure\r\n");
            return FSB_FAILURE;
        }
    }
    else
    {
        /* NON-EVB scenarios (LAG/physical port cases) */

        if (FsbPortIsPortInPortChannel (pMsg->u4IfIndex) == FSB_SUCCESS)
        {
            if (FsbPortL2IwfGetPortChannelForPort (pMsg->u4IfIndex, &u2AggId)
                == FSB_SUCCESS)
            {
                pMsg->u4IfIndex = u2AggId;
            }
        }
        /* For other FIP frames, no action needs to be taken.
         * (Scneario 4) */

#ifdef FSB_CUSTOM_FCOE_TAGGED_WANTED
        FSB_MEMCPY (&DestAddr, pu1ReadPtr, MAC_ADDR_LEN);
        pu1ReadPtr += FSB_OPCODE_OFFSET;

        FSB_GET_2BYTE (u2OpCode, pu1ReadPtr);
        FSB_GET_2BYTE (u2SubOpCode, pu1ReadPtr);

        /* VLAN ID in the packet to replaced with FCoE VLAN in the 
         * following scenarios.
         *
         * 1) VLAN Discovery Packet
         * 2) Priority-Tagged VLAN Discovery packet
         * 3) Priority-Tagged Multicast Discovery Solicitation (VLAN ID = 0) */

        if (((FSB_MEMCMP (DestAddr, ALL_FCF_MAC, MAC_ADDR_LEN) == 0)
             && (OSIX_NTOHS (u2OpCode) == FSB_FIP_VLAN_OPCODE))
            || ((pMsg->u2VlanId == 0)
                && (OSIX_NTOHS (u2OpCode) == FSB_DISCOVERY_ADV_OPCODE)
                && (OSIX_NTOHS (u2OpCode) == FSB_SOLICITATION_SUB_OPCODE)))
        {
            FsbGetFcoeVlanForIfIndex (pMsg->u4IfIndex, &u2VlanId);
            if (u2VlanId == 0)
            {
                FSB_CONTEXT_TRC (u4ContextId,
                                 FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                                 "FsbHandleFIPFrame : Packet recieved in IfIndex: %d is not "
                                 "a member of any FCOE Vlan\r\n",
                                 pMsg->u4IfIndex);
                return FSB_FAILURE;
            }
            pMsg->u2VlanId = u2VlanId;
            pu1ReadPtr = pMsg->au1DataBuf;
            pu1ReadPtr += VLAN_ID_OFFSET;
            FSB_MEMCPY (pu1ReadPtr, &u2VlanId, FSB_TWO_BYTE);
        }
#endif
    }

#ifdef FSB_CUSTOM_FCOE_TAGGED_WANTED

    /* The following checks are added as a part of client specific
     * scenarios, in which FIP snooping and NPIV proxy coexists.
     * Also interface table check is added for the breakout scenario/
     * Uplink set LAG scenario. 
     */
    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4ContextId, pMsg->u2VlanId);
    if (pFsbFipSnoopingEntry == NULL)
    {
        /* If the packet is recieved in the VLAN,
         * other than the FCoE VLAN or Untagged VLAN, return success*/
        return FSB_SUCCESS;
    }
    if (FsbGetFsbIntfEntry (pMsg->u2VlanId, pMsg->u4IfIndex) == NULL)
    {
        return FSB_SUCCESS;
    }

    /* If the FCoE VLAN contains any MLAG interface,
     * it means that Pinned Port Configuration should be done
     * If the Pinned Port Configuration is not done, FIP frames needs to be 
     * dropped in the FSB module*/
    if (FsbIsMLAGPortInFCoEVlan (u4ContextId, pMsg->u2VlanId) == FSB_SUCCESS)
    {
        if (pFsbFipSnoopingEntry->u4PinnedPortCount == 0)
        {
            /* Drop the FIP packet if the Pinned Port Configuration is not done */
            FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbHandleFIPFrame: Prevent RX on FCoE VLAN id: %d"
                             " containing MLAG without Pinned Ports Configuration\n",
                             pMsg->u2VlanId);
            return FSB_FAILURE;
        }
    }
#endif

    u1PortState = L2IwfGetVlanPortState (pMsg->u2VlanId, pMsg->u4IfIndex);

    if (u1PortState == AST_PORT_STATE_DISCARDING)
    {
        /* Drop the FIP packet if it is received on the Blocked port */
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbHandleFIPFrame: Prevent RX on discarding ports\n");
        return FSB_FAILURE;
    }

    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbHandlePortRoleChange                          */
/*                                                                           */
/*    Description         : This function is used to handle port role change */
/*                          of the interface                                 */
/*                                                                           */
/*    Input(s)            : pFsbIntfEntry - Pointer to tFsbIntfEntry         */
/*                          u1PrevPortRole - Previously configured port role */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS/FSB_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
FsbHandlePortRoleChange (tFsbIntfEntry * pFsbIntfEntry, UINT1 u1PrevPortRole)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbContextInfo    *pFsbContextInfo = NULL;

    if (pFsbIntfEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbHandlePortRoleChange : "
                         "pFsbIntfEntry is NULL\r\n");
        return FSB_FAILURE;
    }
    pFsbFipSnoopingEntry =
        FsbGetFIPSnoopingEntry (pFsbIntfEntry->u4ContextId,
                                pFsbIntfEntry->u2VlanId);

    pFsbContextInfo = FsbCxtGetContextEntry (pFsbIntfEntry->u4ContextId);

    if ((pFsbFipSnoopingEntry != NULL) && (pFsbContextInfo != NULL))
    {
        if ((pFsbContextInfo->u1ModuleStatus != FSB_ENABLE) ||
            (pFsbFipSnoopingEntry->u1EnabledStatus != FSB_ENABLE))
        {
            /* If the module status and VLAN status is enabled,
               then VLAN filters will not be installed.
               Therefore, no FCF or Session will be populated and
               VLAN filters need not be updated */
            return FSB_SUCCESS;
        }

        /* If both the module status and VLAN status is
           enabled, the FCF entry or the session entry should
           be deleted depending upon the previous port role
           and current port role. Also, VLAN filters should be
           updated and installed */

        /* Table 2 : This Table denotes posible combination
           for changing the port-role and the corresponding
           action to be taken */
        /***************************************************/
        /* Previous    Current        Entries to           */
        /* PortRole    PortRole       be deleted           */
        /*-------------------------------------------------*/
        /*                                                 */
        /* FCF         Enode         FCF and Session       */
        /*                                                 */
        /* FCF         Both          No deletion           */
        /*                                                 */
        /* Enode       FCF           Session               */
        /*                                                 */
        /* Enode       Both          No deletion           */
        /*                                                 */
        /* Both        FCF           Session               */
        /*                                                 */
        /* Both        Enode         FCF and session       */
        /*                                                 */
        /***************************************************/
        if (((u1PrevPortRole == FSB_FCF_FACING) &&
             (pFsbIntfEntry->u1PortRole == FSB_ENODE_FACING)) ||
            ((u1PrevPortRole == FSB_ENODE_FCF_FACING) &&
             (pFsbIntfEntry->u1PortRole == FSB_ENODE_FACING)))
        {
            if (FsbDeleteFcfEntryForVLANIfIndex (pFsbIntfEntry->u2VlanId,
                                                 pFsbIntfEntry->u4IfIndex)
                != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbIntfEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbHandlePortRoleChange"
                                 " :FsbDeleteFcfEntryForVLANIfIndex for VLAN Id: %d,"
                                 " IfIndex: %d failed\r\n",
                                 pFsbIntfEntry->u2VlanId,
                                 pFsbIntfEntry->u4IfIndex);
                return FSB_FAILURE;
            }
            if (FsbDeleteSessEntryForVLANIfIndex (pFsbIntfEntry->u2VlanId,
                                                  pFsbIntfEntry->u4IfIndex)
                != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbIntfEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbHandlePortRoleChange"
                                 " :FsbDeleteSessEntryForVLANIfIndex for VLAN Id: %d,"
                                 " IfIndex: %d failed\r\n",
                                 pFsbIntfEntry->u2VlanId,
                                 pFsbIntfEntry->u4IfIndex);
                return FSB_FAILURE;
            }
        }
        else if (((u1PrevPortRole == FSB_ENODE_FACING) &&
                  (pFsbIntfEntry->u1PortRole == FSB_FCF_FACING)) ||
                 ((u1PrevPortRole == FSB_ENODE_FCF_FACING) &&
                  (pFsbIntfEntry->u1PortRole == FSB_FCF_FACING)))
        {
            if (FsbDeleteSessEntryForVLANIfIndex (pFsbIntfEntry->u2VlanId,
                                                  pFsbIntfEntry->u4IfIndex)
                != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbIntfEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbHandlePortRoleChange"
                                 " : FsbDeleteSessEntryForVLANIfIndex for VLAN Id: %d,"
                                 " IfIndex: %d failed\r\n",
                                 pFsbIntfEntry->u2VlanId,
                                 pFsbIntfEntry->u4IfIndex);
                return FSB_FAILURE;
            }
        }

        /* Update the VLAN Filter and sync the filter id */
        if (FsbUpdateVLANFilter (pFsbIntfEntry->u4ContextId,
                                 pFsbIntfEntry->u2VlanId) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbIntfEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbHandlePortRoleChange"
                             " : FsbUpdateVLANFilter for VlanId: %d failed\r\n",
                             pFsbIntfEntry->u2VlanId);
            return FSB_FAILURE;
        }
        if (FsbRedSyncUpDefaultFilter (pFsbFipSnoopingEntry) != FSB_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbIntfEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbHandlePortRoleChange"
                             " : FsbRedSyncUpDefaultFilter with VlanId: %d failed\r\n",
                             pFsbFipSnoopingEntry->u2VlanId);
            return FSB_FAILURE;
        }
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsbDumpPacket                                    */
/*                                                                           */
/*    Description         : This function is used to dump packet             */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u4Mask - Trace Mask                              */
/*                          pBuf - CRU buffer                                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
FsbDumpPacket (UINT4 u4ContextId, UINT4 u4Mask,
               UINT1 *pu1DataBuf, UINT4 u4PktSize)
{
    UINT4               u4Count = 0;
    CHR1                au1Buf[FSB_TRC_MAX_SIZE];
    CHR1               *pu1BufPtr = NULL;
    UINT4               u4Len = 0;
    MEMSET (au1Buf, 0, sizeof (au1Buf));

    if (FsbUtilIsFsbStarted (u4ContextId) != FSB_TRUE)
    {
        return;
    }
    if (!(FSB_MODULE_TRACE (u4ContextId) & u4Mask))
    {
        return;
    }

    if (u4PktSize > FSB_MAX_FIP_PKT_LEN)
    {
        return;
    }

    /* Print in chunk of 16 bytes */
    pu1BufPtr = au1Buf;
    while (u4Count < u4PktSize)
    {
        u4Len =
            (UINT4) SPRINTF ((CHR1 *) pu1BufPtr, "%02x ",
                             *(pu1DataBuf + u4Count));
        pu1BufPtr += u4Len;
        u4Count++;

        if ((u4Count % 16) == 0)
        {
            UtlTrcPrint (au1Buf);
            UtlTrcPrint ("\n");
            pu1BufPtr = au1Buf;
            MEMSET (au1Buf, 0, u4Len);
            u4Len = 0;
        }
    }
    /* Take care of any remaining bytes */
    if ((u4Count % 16) != 0)
    {
        UtlTrcPrint (au1Buf);
        UtlTrcPrint ("\n");
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbValidatePortsInFCoEVlan                       */
/*                                                                           */
/*    Description         : This function is used to validate the based on   */
/*                          the below set of conditions                      */
/*                          1) If the Vlan has ports mapped to ports         */
/*                          2) If the Oper Status of the Ports are up        */
/*                          These conditions are placed to ensure the sucess */
/*                          in ACL Filter installation                       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2VlanIndex - VLAN Index                         */
/*                                                                           */
/*    Output(s)           : pu4ErrorCode - Error Code                        */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbValidatePortsInFCoEVlan (UINT4 u4ContextId, UINT2 u2VlanIndex)
{
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbIntfEntry       FsbIntfEntry;
    UINT4               u4PortCount = 0;

    FSB_MEMSET (&FsbIntfEntry, 0, sizeof (tFsbIntfEntry));

    pFsbIntfEntry = (tFsbIntfEntry *) RBTreeGetFirst (gFsbGlobals.FsbIntfTable);

    if (pFsbIntfEntry == NULL)
    {
        return FSB_FAILURE;
    }

    do
    {
        /* If the Context Id and Vlan Id Matches */
        if ((pFsbIntfEntry->u4ContextId == u4ContextId) &&
            (pFsbIntfEntry->u2VlanId == u2VlanIndex))
        {
            u4PortCount++;
        }
        /* GetNext Entry */
        FsbIntfEntry.u2VlanId = pFsbIntfEntry->u2VlanId;
        FsbIntfEntry.u4IfIndex = pFsbIntfEntry->u4IfIndex;

        pFsbIntfEntry = (tFsbIntfEntry *)
            RBTreeGetNext (gFsbGlobals.FsbIntfTable,
                           (tRBElem *) & FsbIntfEntry, NULL);

    }
    while (pFsbIntfEntry != NULL);

    /* Incase No ports are mapped to the vlan  */
    if (u4PortCount == 0)
    {
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbHandlePortRemoveFromVlan                      */
/*                                                                           */
/*    Description         : This function is used to delete a single port    */
/*                          from FSB interface entry table that has been     */
/*                          marked as FCOE VLAN in L2IWF                     */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId - VLAN Index                              */
/*                          u4IfIndex - Interface Index                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbHandlePortRemoveFromVlan (UINT4 u4ContextId, tVlanId VlanId, UINT4 u4IfIndex)
{
    /* To delete a single port from FSB interface entry table */
    if (FsbDeleteIntfEntry (VlanId, u4IfIndex) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbHandlePortRemoveFromVlan: FsbDeleteIntfEntry"
                         " failed for IfIndex: %d, Vlan Id: %d\r\n", u4IfIndex,
                         VlanId);
        return FSB_FAILURE;
    }

    if (FsbDeleteFCFEntryForFcoeVlan (u4ContextId, VlanId) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbHandlePortRemoveFromVlan: FsbDeleteFCFEntryForFcoeVlan"
                         " failed for IfIndex: %d, Vlan Id: %d\r\n", u4IfIndex,
                         VlanId);
        return FSB_FAILURE;
    }

    if (FsbDeleteFipSessionEntryForFcoeVlan (u4ContextId, VlanId) !=
        FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbHandlePortRemoveFromVlan: FsbDeleteFipSessionEntryForFcoeVlan"
                         " failed for IfIndex: %d, Vlan Id: %d\r\n", u4IfIndex,
                         VlanId);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbCfaCliGetIfList                                  */
/*                                                                           */
/* Description        : This function calls the functions which convert      */
/*                      the given string to interface list                   */
/*                                                                           */
/* Input(s)           : pi1IfName :Port Number                               */
/*                      pi1IfListStr : Pointer to the string                 */
/*                      pu1IfListArray:Pointer to the string in which the    */
/*                      bits for the port list will be set                   */
/*                      u4IfListArrayLen:Array Length                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

INT4
FsbCfaCliGetIfList (INT1 *pi1IfName, INT1 *pi1IfListStr, UINT1 *pu1IfListArray,
                    UINT4 u4IfListArrayLen)
{
#ifdef CLI_WANTED
    UINT4               u4IfType = 0;
    UINT4               u4SubType = 0;

    if (CfaCliValidateXInterfaceName (pi1IfName, NULL, &u4IfType)
        == CLI_FAILURE)
    {
        if (CfaCliValidateXSubTypeIntName (pi1IfName, NULL, &u4IfType,
                                           &u4SubType) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if ((u4IfType == CFA_ENET) ||
#ifndef HP_CUSTOMIZATION_WANTED
        (u4IfType == CFA_FA_ENET) || (u4IfType == CFA_GI_ENET) ||
        (u4IfType == CFA_XE_ENET) || (u4IfType == CFA_XL_ENET) ||
#else
        (u4IfType == CFA_FORTYG_ENET) || (u4IfType == CFA_TWENTYG_ENET) ||
        (u4IfType == CFA_TENG_ENET) || (u4IfType ==
                                        CFA_STATION_FACING_BRIDGE_PORT) ||
#endif
        (u4IfType == CFA_LVI_ENET))
    {
        if (CliStrToIfaceList ((UINT1 *) pi1IfListStr, pu1IfListArray,
                               u4IfListArrayLen, u4IfType) == OSIX_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        return CLI_FAILURE;
    }
#else
    UNUSED_PARAM (pi1IfName);
    UNUSED_PARAM (pi1IfListStr);
    UNUSED_PARAM (pu1IfListArray);
    UNUSED_PARAM (u4IfListArrayLen);
#endif
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbIsMLAGPortChannel                             */
/*                                                                           */
/*    Description         : This function is used to validate whether the    */
/*                          given Port Channel is an MLAG port-channel       */
/*                          MLAG is determined based on the local and remote */
/*                          member ports of the LAG                          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2VlanIndex - VLAN Index                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbIsMLAGPortChannel (UINT2 u2AggId)
{
    UINT2               au2ConfPorts[L2IWF_MAX_PORTS_PER_CONTEXT];
    UINT2               u2NumPorts = 0;
#ifdef MBSM_WANTED
    UINT2               u2Index = 0;
    UINT4               u4LocalPortCount = 0;
    UINT4               u4RemotePortCount = 0;
    UINT4               u4SlotId = MBSM_INVALID_SLOT_ID;
#endif

    /* Retrieve the configured ports in the port-channel */
    if (FsbL2IwfGetConfiguredPortsForPortChannel (u2AggId,
                                                  au2ConfPorts, &u2NumPorts)
        != FSB_FAILURE)
    {
#ifdef MBSM_WANTED
        for (u2Index = 0; u2Index < u2NumPorts; u2Index++)
        {
            u4SlotId = MBSM_INVALID_SLOT_ID;

            MbsmGetSlotIdFromRemotePort ((UINT4) au2ConfPorts[u2Index],
                                         &u4SlotId);
            if (u4SlotId == (UINT4) IssGetSwitchid ())
            {
                u4LocalPortCount++;
            }
            else
            {
                u4RemotePortCount++;
            }
        }
        if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
        {
            return FSB_SUCCESS;
        }
        else
        {
            return FSB_FAILURE;
        }
#endif
    }
    return FSB_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbIsMLAGPortInFCoEVlan                         */
/*                                                                           */
/*    Description         : This function is used to validate whether the    */
/*                          FCoE VLAN contains any MLAG.                     */
/*                          MLAG is determined based on the local and remote */
/*                          member ports of the LAG                          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2VlanIndex - VLAN Index                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbIsMLAGPortInFCoEVlan (UINT4 u4ContextId, UINT2 u2VlanIndex)
{
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbIntfEntry       FsbIntfEntry;
    UINT1               u1IfType = 0;

    FSB_MEMSET (&FsbIntfEntry, 0, sizeof (tFsbIntfEntry));

    pFsbIntfEntry = (tFsbIntfEntry *) RBTreeGetFirst (gFsbGlobals.FsbIntfTable);

    if (pFsbIntfEntry == NULL)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbGetLAGPortInFCoEVlan: RBTreeGetFirst failed for the"
                         " Vlan Id: %d\r\n", u2VlanIndex);
        return FSB_FAILURE;
    }

    do
    {
        /* If the Context Id and Vlan Id Matches */
        if ((pFsbIntfEntry->u4ContextId == u4ContextId) &&
            (pFsbIntfEntry->u2VlanId == u2VlanIndex))
        {
            /* Get the interface type */
            FsbCfaGetIfType (pFsbIntfEntry->u4IfIndex, &u1IfType);
            if (u1IfType == CFA_LAGG)
            {
                if (FsbIsMLAGPortChannel ((UINT2) pFsbIntfEntry->u4IfIndex) ==
                    FSB_SUCCESS)
                {
                    return FSB_SUCCESS;
                }
            }
        }
        /* GetNext Entry */
        FsbIntfEntry.u2VlanId = pFsbIntfEntry->u2VlanId;
        FsbIntfEntry.u4IfIndex = pFsbIntfEntry->u4IfIndex;

        pFsbIntfEntry = (tFsbIntfEntry *)
            RBTreeGetNext (gFsbGlobals.FsbIntfTable,
                           (tRBElem *) & FsbIntfEntry, NULL);

    }
    while (pFsbIntfEntry != NULL);
    return FSB_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbUpdateDefaultFilter                           */
/*                                                                           */
/*    Description         : This function is used to update the default      */
/*                          filter installed on the FCF facing ports, based  */
/*                          on the Pinned PortList updation                  */
/*                                                                           */
/*    Input(s)            : pFsbFipSnoopingEntry - FsbFipSnoopingEnt         */
/*                          FsbPinnedPorts - Updated Pinned PortList         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbUpdateDefaultFilter (tFsbFipSnoopingEntry * pFsbFipSnoopingEntry,
                        tPortList FsbPinnedPorts)
{
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;
    UINT4               u4HwFilterId = 0;

    if (pFsbFipSnoopingEntry == NULL)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbUpdateDefaultFilter: pFsbFipSnoopingEntry is NULL\r\n");
        return FSB_FAILURE;
    }

    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    pFsbFilterEntry = (tFsbFilterEntry *)
        RBTreeGetFirst (pFsbFipSnoopingEntry->FsbFilterEntry);

    while (pFsbFilterEntry != NULL)
    {
        /* Loop through the existing FSB default filter entries
         * and find the Discovery Advertisement filter installed in
         * the FCF facing ports.
         * Delete the existing filter entry and install the filter
         * with the Updated Pinned PortList*/
        if ((pFsbFilterEntry->u2OpcodeFilterOffsetValue == FSB_DISCOVERY)
            && (pFsbFilterEntry->u2SubOpcodeFilterOffsetValue ==
                FSB_DISCOVERY_ADVERTISEMENT))
        {
            /* Delete the existing filter entry */
            if (FsFsbHwWrDeleteFilter (pFsbFilterEntry,
                                       pFsbFilterEntry->u4HwFilterId) !=
                FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                                 FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                                 "FsbUpdateDefaultFilter: Deleting existing"
                                 "filter failed\r\n");
            }

            /* The portList for discovery advertisement
             * filters has been changed */
            FSB_MEMCPY (pFsbFilterEntry->PortList, FsbPinnedPorts,
                        sizeof (tPortList));

            /* Install Filter with the updated PortList */
            if (FsFsbHwWrCreateFilter (pFsbFilterEntry, &u4HwFilterId)
                != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbUpdateDefaultFilter:FsFsbHwWrCreateFilter with "
                                 "VlanId: %d failed\r\n",
                                 pFsbFilterEntry->u2VlanId);
                return FSB_FAILURE;
            }
            pFsbFilterEntry->u4HwFilterId = u4HwFilterId;
        }

        FSB_MEMCPY (FsbFilterEntry.DstMac, pFsbFilterEntry->DstMac,
                    MAC_ADDR_LEN);
        pFsbFilterEntry = (tFsbFilterEntry *)
            RBTreeGetNext (pFsbFipSnoopingEntry->FsbFilterEntry,
                           (tRBElem *) & FsbFilterEntry, NULL);
    }
    if (FsbRedSyncUpDefaultFilter (pFsbFipSnoopingEntry) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbUpdateDefaultFilter:"
                         "FsbRedSyncUpDefaultFilter failed\r\n");
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbGetFreeIndex                                  */
/*                                                                           */
/*    Description         : This function is used to get the free index      */
/*                                                                           */
/*    Input(s)            : FsbRBFreeList - Free list maintained for RB Tree */
/*                                                                           */
/*    Output(s)           : pu4Index -  Free Interface Index                 */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbGetFreeIndex (tFsbRBFreeList FsbRBFreeList, UINT4 *pu4Index)
{
    UINT4               u4Index = 0;
    UINT1               u1IsSetInPortList = OSIX_FALSE;

    for (u4Index = 1; u4Index <= MAX_FSB_FIP_SESS_ENTRIES; u4Index++)
    {
        OSIX_BITLIST_IS_BIT_SET (FsbRBFreeList,
                                 u4Index,
                                 sizeof (tFsbRBFreeList), u1IsSetInPortList);

        if (u1IsSetInPortList == OSIX_FALSE)
        {
            OSIX_BITLIST_SET_BIT (FsbRBFreeList, u4Index,
                                  FSB_RB_FREE_LIST_SIZE);
            *pu4Index = u4Index;
            return FSB_SUCCESS;
        }
    }
    return FSB_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbResetFreeIndex                                */
/*                                                                           */
/*    Description         : This function is used to release the free index  */
/*                                                                           */
/*    Input(s)            : FsbRBFreeList - Free list maintained for RB Tree */
/*                          u4Index - Free Index Allocated                   */
/*                                                                           */
/*    Output(s)           : NONE                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbResetFreeIndex (tFsbRBFreeList FsbRBFreeList, UINT4 u4Index)
{
    /* Reset the FreeList */
    OSIX_BITLIST_RESET_BIT (FsbRBFreeList, u4Index, FSB_RB_FREE_LIST_SIZE);

    return FSB_SUCCESS;
}

#endif /* _FSBUTIL_C_ */
