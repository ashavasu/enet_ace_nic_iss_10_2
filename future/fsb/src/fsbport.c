/*************************************************************************
* Copyright (C) 2007-2012 Aricent Group . All Rights Reserved
*
* $Id: fsbport.c,v 1.10 2017/10/09 13:13:59 siva Exp $
*
* Description: This file contains functions that interact with other module
*
*************************************************************************/
#ifndef _FSBPORT_C_
#define _FSBPORT_C_

#include "fsbinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbPortIsSwitchExist                             */
/*                                                                           */
/*    Description         : This function is used to get if the context for  */
/*                          given Context name                               */
/*                                                                           */
/*    Input(s)            : pu1ContextName - Context Name                    */
/*                                                                           */
/*    Output(s)           : pu4ContextId - Context Identifier                */
/*                                                                           */
/*    Returns             : FSB_TRUE /FSB_FALSE                              */
/*                                                                           */
/*****************************************************************************/
INT4
FsbPortIsSwitchExist (UINT1 *pu1ContextName, UINT4 *pu4ContextId)
{
    INT4                i4RetVal = 0;

    i4RetVal = VcmIsSwitchExist (pu1ContextName, pu4ContextId);

    return (((i4RetVal != VCM_TRUE) ? FSB_FALSE : FSB_TRUE));

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbPortGetSwitchAliasName                        */
/*                                                                           */
/*    Description         : This function is used to get the Alias Name for  */
/*                          the context                                      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : pu1Alias - Alias Name                            */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbPortGetSwitchAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    INT4                i4RetVal = VCM_FAILURE;

    i4RetVal = VcmGetAliasName (u4ContextId, pu1Alias);

    return (((i4RetVal != VCM_SUCCESS) ? FSB_FAILURE : FSB_SUCCESS));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbPortIsVcExist                                 */
/*                                                                           */
/*    Description         : This function will return whether the given      */
/*                          context exist or not                             */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_TRUE /FSB_FALSE                              */
/*                                                                           */
/*****************************************************************************/
INT4
FsbPortIsVcExist (UINT4 u4ContextId)
{
    INT4                i4RetVal = VCM_FALSE;

    i4RetVal = VcmIsL2VcExist (u4ContextId);

    return ((i4RetVal != VCM_TRUE) ? FSB_FALSE : FSB_TRUE);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbPortIsVlanActive                              */
/*                                                                           */
/*    Description         : This function is used to check whether this VLAN */
/*                          is active or not                                 */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2VlanId - VLAN Identifier                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_TRUE / FSB_FALSE                             */
/*                                                                           */
/*****************************************************************************/
INT4
FsbPortIsVlanActive (UINT4 u4ContextId, tVlanId u2VlanId)
{
    BOOL1               bRetval;

    bRetval = L2IwfMiIsVlanActive (u4ContextId, u2VlanId);

    return (((bRetval == OSIX_TRUE) ? FSB_TRUE : FSB_FALSE));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbSetFCoEVlanType                               */
/*                                                                           */
/*    Description         : This function is used to set the VLAN as FCoE    */
/*                          enabled.                                         */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2VlanId - VLAN Identifier                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : L2_VLAN / FSB_VLAN                               */
/*                                                                           */
/*****************************************************************************/
INT4
FsbSetFCoEVlanType (UINT4 u4ContextId, tVlanId u2VlanId, UINT1 u1FCoEVlanType)
{
    if (L2IwfSetFCoEVlanType (u4ContextId, u2VlanId, u1FCoEVlanType)
        != L2IWF_SUCCESS)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbSetFCoEVlanType : L2IwfSetFCoEVlanType for"
                         "Context  %d VLAN %d failed\r\n", u4ContextId,
                         u2VlanId);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbGetContextIdFromCfaIfIndex                    */
/*                                                                           */
/*    Description         : This functions gets the context id associated    */
/*                          with the given cfa ifIndex                       */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                                                                           */
/*    Output(s)           : pu4ContextId - Context Identifier                */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbGetContextIdFromCfaIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId)
{
    INT4                i4RetVal = 0;

    i4RetVal = VcmGetContextIdFromCfaIfIndex (u4IfIndex, pu4ContextId);

    return (((i4RetVal == VCM_SUCCESS) ? FSB_SUCCESS : FSB_FAILURE));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : CfaGetIfOperStatus                               */
/*                                                                           */
/*    Description         : This function is used to get Oper Status of the  */
/*                          Interface                                        */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                                                                           */
/*    Output(s)           : pu1OperStatus - Oper Status of IfIndex           */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbGetIfOperStatus (UINT4 u4IfIndex, UINT1 *pu1OperStatus)
{
    INT4                i4RetVal = 0;

    i4RetVal = CfaGetIfOperStatus (u4IfIndex, pu1OperStatus);

    return (((i4RetVal == CFA_SUCCESS) ? FSB_SUCCESS : FSB_FAILURE));

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbPortGetVlanLocalEgressPorts                   */
/*                                                                           */
/*    Description         : This function is used to retrieve local port list*/
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2VlanId - VLAN Identifier                       */
/*                                                                           */
/*    Output(s)           : PortList - Member Ports for the VLAN             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAIURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbPortGetVlanLocalEgressPorts (UINT4 u4ContextId, tVlanId u2VlanId,
                                tPortList PortList)
{
    if (L2IwfMiGetVlanEgressPorts (u4ContextId, u2VlanId, PortList)
        != L2IWF_SUCCESS)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbPortGetVlanLocalEgressPorts : L2IwfMiGetVlanEgressPorts"
                         " for Context %d VLAN %d failed\r\n", u4ContextId,
                         u2VlanId);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbGetIfIndexFromLocalPort                       */
/*                                                                           */
/*    Description         : This function is used to check whether this VLAN */
/*                          is active or not                                 */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2LocalPort - Local Port                         */
/*                                                                           */
/*    Output(s)           : pu4IfIndex - Interface Index                     */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAIURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbGetIfIndexFromLocalPort (UINT4 u4ContextId, UINT2 u2LocalPort,
                            UINT4 *pu4IfIndex)
{
    if (VcmGetIfIndexFromLocalPort (u4ContextId, u2LocalPort, pu4IfIndex)
        != VCM_SUCCESS)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbGetIfIndexFromLocalPort : VcmGetIfIndexFromLocalPort failed for"
                         "if index: %d\r\n", u2LocalPort);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbCfaGetInterfaceBrgPortType                        */
/*                                                                           */
/* Description        : This function is used to get the interface bridge    */
/*                      port type for the given Interface index.             */
/*                                                                           */
/* Input(s)           : u4IfIndex - If index                                 */
/*                                                                           */
/* Output(s)          : pi4IfBrgPortType - Bridge Port Type.                 */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS/FSB_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCfaGetInterfaceBrgPortType (UINT4 u4IfIndex, INT4 *pi4IfBrgPortType)
{
    if (CfaGetInterfaceBrgPortType (u4IfIndex, pi4IfBrgPortType) == CFA_FAILURE)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbCfaGetInterfaceBrgPortType: CfaGetInterfaceBrgPortType"
                         " failed for IfIndex : %d\r\n", u4IfIndex);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbVlanApiEvbGetSbpPortsOnUap                    */
/*                                                                           */
/*    Description         : This function retrieves the SBP ports present on */
/*                          the UAP.                                         */
/*                                                                           */
/*    Input(s)            : u4UapIfIndex - UAP If Index.                     */
/*                                                                           */
/*    Output(s)           : pSbpArray    - SBP ports.                        */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
FsbVlanApiEvbGetSbpPortsOnUap (UINT4 u4UapIfIndex, tVlanEvbSbpArray * pSbpArray)
{
    return (VlanApiEvbGetSbpPortsOnUap (u4UapIfIndex, pSbpArray));
}

/***************************************************************************
 *  FUNCTION NAME : FsbPortIsDcbxEnabled  
 * 
 *  DESCRIPTION   : This API is used to check whether the DCBX module 
 *                  is enabled. 
 * 
 *  INPUT         : None
 *  
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
INT4
FsbPortIsDcbxEnabled (VOID)
{
#ifdef DCBX_WANTED
    if (DcbxApiIsDcbxEnabled () != OSIX_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbPortIsDcbxEnabled : DcbxApiIsDcbxEnabled"
                         " returns failure\r\n");
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
#else
    return FSB_SUCCESS;
#endif
}

/***************************************************************************
 *  FUNCTION NAME : FsbPortIsDcbxOperStateUp 
 *
 *  DESCRIPTION   : This API is used to check whether the Dcbx oper state
 *                  is up for the given interface index
 *
 *  INPUT         : Interface Index
 *
 *  OUTPUT        : None
 *
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 *
 * **************************************************************************/
INT4
FsbPortIsDcbxOperStateUp (UINT4 u4IfIndex)
{
#ifdef DCBX_WANTED
    if (DcbxApiIsDcbxOperStateUp (u4IfIndex) != OSIX_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbPortIsDcbxOperStateUp: DcbxApiIsDcbxOperStateUp",
                         " returns failure for IfIndex: %d\r\n", u4IfIndex);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
#else
    UNUSED_PARAM (u4IfIndex);
    return FSB_SUCCESS;
#endif
}

/***************************************************************************
 *  FUNCTION NAME : FsbPortIsDcbxAdminStateUp 
 *
 *  DESCRIPTION   : This API is used to check whether the Dcbx admin state
 *                  is up for the given interface index
 *
 *  INPUT         : Interface Index
 *
 *  OUTPUT        : None
 *
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 *
 * **************************************************************************/
INT4
FsbPortIsDcbxAdminStateUp (UINT4 u4IfIndex)
{
#ifdef DCBX_WANTED
    if (DcbxApiIsDcbxAdminStateUp (u4IfIndex) != OSIX_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbPortIsDcbxAdminStateUp : DcbxApiIsDcbxAdminStateUp",
                         " returns failure for If Index: %d\r\n", u4IfIndex);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
#else
    UNUSED_PARAM (u4IfIndex);
    return FSB_SUCCESS;
#endif
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCfaGetIfType                                  */
/*                                                                           */
/*    Description         : This function returns the interface type         */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                                                                           */
/*    Output(s)           : *pu1IfType                                       */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAIURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCfaGetIfType (UINT4 u4IfIndex, UINT1 *pu1IfType)
{
    if (CfaGetIfaceType (u4IfIndex, pu1IfType) != CFA_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbCfaGetIfType : CfaGetIfaceType returns failure"
                         " for If Index: %d\r\n", u4IfIndex);
        return FSB_FAILURE;
    }
    else
    {
        return FSB_SUCCESS;
    }
}

/*****************************************************************************/
/* Function Name      : FsbL2IwfGetConfiguredPortsForPortChannel             */
/*                                                                           */
/* Description        : This routine returns the list of ports configured as */
/*                      members of the given PortChannel. It accesses the    */
/*                      L2Iwf common database                                */
/*                                                                           */
/* Input(s)           : AggId - Index of the aggregator                      */
/*                                                                           */
/* Output(s)          : au2ConfPorts    - Array of configured ports for the  */
/*                                        given aggregator                   */
/*                      u2NumPorts      - Number of member ports returned in */
/*                                        the array.                         */
/*                                                                           */
/*Returns             : FSB_SUCCESS / FSB_FAIURE                             */
/*****************************************************************************/
INT4
FsbL2IwfGetConfiguredPortsForPortChannel (UINT4 u4IfIndex, UINT2 au2ConfPorts[],
                                          UINT2 *pu2NumPorts)
{
    if (L2IwfGetConfiguredPortsForPortChannel ((UINT2) u4IfIndex,
                                               au2ConfPorts,
                                               pu2NumPorts) != L2IWF_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbL2IwfGetConfiguredPortsForPortChannel :"
                         " L2IwfGetConfiguredPortsForPortChannel"
                         " returns failure for If Index: %d\r\n", u4IfIndex);
        return FSB_FAILURE;
    }
    else
    {
        return FSB_SUCCESS;
    }
}

/*****************************************************************************/
/* Function Name      : FsbRmEnqMsgToRmFromAppl                              */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : pRmMsg - msg from appl.                              */
/*                      u2DataLen - Len of msg.                              */
/*                      u4SrcEntId - EntId from which the msg has arrived.   */
/*                      u4DestEntId - EntId to which the msg has to be sent. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
FsbRmEnqMsgToRmFromAppl (tRmMsg * pRmMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                         UINT4 u4DestEntId)
{
#ifdef L2RED_WANTED
    return (RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen, u4SrcEntId, u4DestEntId));
#else
    UNUSED_PARAM (pRmMsg);
    UNUSED_PARAM (u2DataLen);
    UNUSED_PARAM (u4SrcEntId);
    UNUSED_PARAM (u4DestEntId);
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : FsbRmGetNodeState                                    */
/*                                                                           */
/* Description        : This function calls the RM module to get the node    */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
FsbRmGetNodeState (VOID)
{
#ifdef L2RED_WANTED
    return (RmGetNodeState ());
#else
    return RM_ACTIVE;
#endif
}

/*****************************************************************************/
/* Function Name      : FsbRmGetStandbyNodeCount                             */
/*                                                                           */
/* Description        : This function calls the RM module to get the number  */
/*                      of peer nodes that are up.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
FsbRmGetStandbyNodeCount (VOID)
{
#ifdef L2RED_WANTED
    return (RmGetStandbyNodeCount ());
#else
    return 0;
#endif
}

/*****************************************************************************/
/* Function Name      : FsbRmHandleProtocolEvent                             */
/*                                                                           */
/* Description        : This function calls the RM module to intimate about  */
/*                      the protocol operations                              */
/*                                                                           */
/* Input(s)           : pEvt->u4AppId - Application Id                       */
/*                      pEvt->u4Event - Event send by protocols to RM        */
/*                                      (RM_PROTOCOL_BULK_UPDT_COMPLETION /  */
/*                                      RM_INITIATE_BULK_UPDATE /            */
/*                                      RM_BULK_UPDT_ABORT /                 */
/*                                      RM_STANDBY_TO_ACTIVE_EVT_PROCESSED / */
/*                                      RM_IDLE_TO_ACTIVE_EVT_PROCESSED /    */
/*                                      RM_STANDBY_EVT_PROCESSED)            */
/*                      pEvt->u4Err   - Error code (RM_MEMALLOC_FAIL /       */
/*                                      RM_SENDTO_FAIL / RM_PROCESS_FAIL)    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
FsbRmHandleProtocolEvent (tRmProtoEvt * pEvt)
{
#ifdef L2RED_WANTED
    return (RmApiHandleProtocolEvent (pEvt));
#else
    UNUSED_PARAM (pEvt);
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : FsbRmRegisterProtocols                               */
/*                                                                           */
/* Description        : This function calls the RM module to register.       */
/*                                                                           */
/* Input(s)           : tRmRegParams - Reg. params to be provided by         */
/*                      protocols.                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
FsbRmRegisterProtocols (tRmRegParams * pRmReg)
{
#ifdef L2RED_WANTED
    return (RmRegisterProtocols (pRmReg));
#else
    UNUSED_PARAM (pRmReg);
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : FsbRmDeRegisterProtocols                             */
/*                                                                           */
/* Description        : This function is to deregister FSB module with RM.   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
UINT4
FsbRmDeRegisterProtocols (VOID)
{
#ifdef L2RED_WANTED
    return (RmDeRegisterProtocols (RM_FSB_APP_ID));
#else
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : FsbRmReleaseMemoryForMsg                             */
/*                                                                           */
/* Description        : This function calls the RM module to release the     */
/*                      memory allocated for sending the Standby node count. */
/*                                                                           */
/* Input(s)           : pu1Block - Mmemory block.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
FsbRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
#ifdef L2RED_WANTED
    return (RmReleaseMemoryForMsg (pu1Block));
#else
    UNUSED_PARAM (pu1Block);
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : FsbRmSetBulkUpdatesStatus                            */
/*                                                                           */
/* Description        : This function calls the RM module to set the Status  */
/*                      of the bulk updates.                                 */
/*                                                                           */
/* Input(s)           : u4AppId - Application Identifier.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
FsbRmSetBulkUpdatesStatus (UINT4 u4AppId)
{
#ifdef L2RED_WANTED
    return (RmSetBulkUpdatesStatus (u4AppId));
#else
    UNUSED_PARAM (u4AppId);
    return OSIX_SUCCESS;
#endif

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbPortSetVlanMacLearningStatus                  */
/*                                                                           */
/*    Description         : This function is used to set Mac-learning status */
/*                          of VLAN as enable / disable                      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2VlanId - VLAN Identifier                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAIURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbPortSetVlanMacLearningStatus (UINT4 u4ContextId, tVlanId u2VlanId,
                                 UINT1 u1MacLearningStatus)
{
    if (VlanApiSetVlanMacLearningStatus (u4ContextId, u2VlanId,
                                         u1MacLearningStatus) != VLAN_SUCCESS)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbPortSetVlanMacLearningStatus : VlanApiSetVlanMacLearningStatus"
                         " returns failure for VLAN %d\r\n", u2VlanId);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbPortVlanConfigStaticUcastEntry                */
/*                                                                           */
/*    Description         : This function is used to add / delete static MAC */
/*                          address entry                                    */
/*                                                                           */
/*    Input(s)            : pStaticUnicastInfo - Pointer to the structure    */
/*                          containing the static unicast address info       */
/*                          u1Action - Denotes, whether the entry needs to be*/
/*                          deleted or added                                 */
/*                                                                           */
/*    Output(s)           : pu1ErrorCode                                     */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAIURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbPortVlanConfigStaticUcastEntry (tConfigStUcastInfo * pStaticUnicastInfo,
                                   UINT1 u1Action, UINT1 *pu1ErrorCode)
{
    if (VlanConfigStaticUcastEntry (pStaticUnicastInfo, u1Action,
                                    pu1ErrorCode) != VLAN_SUCCESS)
    {
        FSB_CONTEXT_TRC (pStaticUnicastInfo->u4ContextId, FSB_ERROR_LEVEL,
                         FSB_NONE,
                         "FsbPortVlanConfigStaticUcastEntry : VlanConfigStaticUcastEntry"
                         " returns failure for VLAN %d\r\n",
                         pStaticUnicastInfo->VlanId);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : FsbPortCfaGddEthWrite
 *
 *    Description        : Writes the data to the ethernet driver.
 *
 *    Input(s)            : pu1DataBuf - Pointer to the linear buffer.
 *                          u4IfIndex - MIB-2 interface index
 *                          u4PktSize - Size of the buffer.
 *
 *    Output(s)            : None.
 *
 *    Returns             : FSB_SUCCESS / FSB_FAIURE                         
 *
 *****************************************************************************/
INT4
FsbPortCfaGddEthWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{

    UINT1               u1PortState = 0;
    UINT2               u2AggId = 0;
    UINT2               u2VlanId = 0;
    UINT4               u4AggIndex = 0;

    if (FsbPortIsPortInPortChannel (u4IfIndex) == FSB_SUCCESS)
    {
        if (FsbPortL2IwfGetPortChannelForPort (u4IfIndex, &u2AggId)
            == FSB_SUCCESS)
        {

            u4AggIndex = (UINT4) u2AggId;

            FsbGetFcoeVlanForIfIndex (u4AggIndex, &u2VlanId);

            u1PortState = L2IwfGetVlanPortState (u2VlanId, u4AggIndex);

            if (u1PortState == AST_PORT_STATE_DISCARDING)
            {
                /* Prevent TX of FIP packet on the Blocked port */
                FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL,
                                 FSB_NONE,
                                 "FsbPortCfaGddEthWrite : Prevent TX on discarding ports for"
                                 "LAG if index: %d, If index:%d, port state:%d\n",
                                 u2AggId, u4IfIndex, u1PortState);
                return FSB_SUCCESS;
            }
        }
    }
    else
    {
        FsbGetFcoeVlanForIfIndex (u4IfIndex, &u2VlanId);

        u1PortState = L2IwfGetVlanPortState (u2VlanId, u4IfIndex);

        if (u1PortState == AST_PORT_STATE_DISCARDING)
        {
            /* Prevent TX of FIP packet on the Blocked port */
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                             "FsbPortCfaGddEthWrite : Prevent TX on discarding ports for"
                             "if index: %d, port state:%d\n", u4IfIndex,
                             u1PortState);
            return FSB_SUCCESS;
        }
    }

    FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL, FSB_DUMP_TRC,
                     "FsbPortCfaGddEthWrite : Frame of size of %d"
                     " transmitted on port %d\r\n", u4PktSize, u4IfIndex);
    FSB_PKT_DUMP (FSB_DEFAULT_CXT_TRACE, FSB_DUMP_TRC, pu1DataBuf, u4PktSize);

    if (CfaFsbGddEthWrite (pu1DataBuf, u4IfIndex, u4PktSize) == CFA_FAILURE)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbPortCfaGddEthWrite : CfaFsbGddEthWrite fails for"
                         " IfIndex %d, Packet size: %d\r\n", u4IfIndex,
                         u4PktSize);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : FsbPortVlanGetFdbEntryDetails
 *
 *    Description        : This function returns Fdb Entry Information. 
 *
 *    Input(s)            : u4FdbId - Fdb Id.                                
 *                           MacAddr - Mac Address.                           
 *                                                                            
 *    Output(s)           : pu2Port - Port on which the Mac Addr is learnt.  
 *                         : pu1Status - Flag for Learnt or not.              
 *                                                                           
 *    Returns             : FSB_SUCCESS / FSB_FAIURE                         
 *
 *****************************************************************************/
INT4
FsbPortVlanGetFdbEntryDetails (UINT4 u4FdbId, tMacAddr MacAddr,
                               UINT2 *pu2Port, UINT1 *pu1Status)
{
#ifdef VLAN_WANTED
    if (VlanGetFdbEntryDetails (u4FdbId, MacAddr,
                                pu2Port, pu1Status) == VLAN_SUCCESS)
    {
        return FSB_SUCCESS;
    }
    else
    {
        return FSB_FAILURE;
    }
#endif
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbPortL2IwfGetPortChannelForPort                */
/*                                                                           */
/*    Description         : This function calls the L2IWF module to get the  */
/*                          Port channel ID to which the port belongs to.    */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Global IfIndex of the port whose     */
/*                          Port channel ID to which the port belongs to.    */
/*                                                                           */
/*    Output(s)           : u2AggId     - Port Channel Index                 */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAIURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FsbPortL2IwfGetPortChannelForPort (UINT4 u4IfIndex, UINT2 *pu2AggId)
{
    if (L2IwfGetPortChannelForPort (u4IfIndex, pu2AggId) != L2IWF_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbPortL2IwfGetPortChannelForPort :"
                         " L2IwfGetPortChannelForPort fails for IfIndex %d\r\n",
                         u4IfIndex);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbPortIsVlanUntagMemberPort                         */
/*                                                                           */
/* Description        : This routine is called to check whether the given    */
/*                      port is a untag member of the given Vlan in the      */
/*                      given context.                                       */
/*                                                                           */
/* Input(s)           : ContextId, VlanId, Global IfIndex                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
BOOL1
FsbPortIsVlanUntagMemberPort (UINT4 u4ContextId, tVlanId VlanId,
                              UINT4 u4IfIndex)
{
    if (L2IwfMiIsVlanUntagMemberPort (u4ContextId, VlanId, u4IfIndex) !=
        OSIX_TRUE)
    {
        return FSB_FALSE;
    }
    return FSB_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbStripVlanTag                                  */
/*                                                                           */
/*    Description         : This function removes the Vlan Tag from the      */
/*                          incoming packet.                                 */
/*                                                                           */
/*    Input(s)            : pFrame - Pointer to the frame.                   */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
FsbStripVlanTag (tCRU_BUF_CHAIN_DESC * pFrame)
{
    VlanUnTagFrame (pFrame);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbVlanTagFrame                                  */
/*                                                                           */
/*    Description         : This function takes care of adding the VLAN tag  */
/*                          to the given frame.                              */
/*                                                                           */
/*    Input(s)            : pFrame - Pointer to the frame.                   */
/*                          VlanId - VlanID to be tagged                     */
/*                          u1Priority - Priority to be tagged.              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
FsbVlanTagFrame (tCRU_BUF_CHAIN_DESC * pBuf, tVlanId VlanId, UINT1 u1Priority)
{
    VlanTagFrame (pBuf, VlanId, u1Priority);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbVlanApiGetSChInfoFromSChIfIndex               */
/*                                                                           */
/*    Description         : This function provides the UAP IfIndex and SVID  */
/*                          for the given SCh IfIndex.                       */
/*                                                                           */
/*    Input(s)            : u4SChIfIndex - S-Channel IfIndex.                */
/*                          u4ContextId  - Context Identifier                */
/*                                                                           */
/*    Output(s)           : pu4UapIfIndex- UAP IfIndex                       */
/*                          pu2SVID      - SVID                              */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbVlanApiGetSChInfoFromSChIfIndex (UINT4 u4SChIfIndex, UINT4 *pu4UapIfIndex,
                                    UINT2 *pu2SVID)
{
    if (VlanApiGetSChInfoFromSChIfIndex (u4SChIfIndex, pu4UapIfIndex,
                                         pu2SVID) != VLAN_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbVlanApiGetSChInfoFromSChIfIndex :"
                         " VlanApiGetSChInfoFromSChIfIndex fails for IfIndex"
                         " %d\r\n", u4SChIfIndex);
        return FSB_FAILURE;
    }
    else
    {
        return FSB_SUCCESS;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbVlanApiGetSChIfIndex                          */
/*                                                                           */
/*    Description         : This function provides the S-Channel IfIndex for */
/*                          the given SVID and UAP IfIndex.                  */
/*                                                                           */
/*    Input(s)            : u4UapIfIndex - UAP If Index.                     */
/*                          u2SVID       - u2SVID                            */
/*                          u4ContextId  - Context Identifier                */
/*                                                                           */
/*    Output(s)           : pu4SChIfIndex- S-Channel IfIndex                 */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbVlanApiGetSChIfIndex (UINT4 u4ContextId, UINT4 u4UapIfIndex,
                         UINT2 u2SVID, UINT4 *pu4SChIfIndex)
{
    if (VlanApiGetSChIfIndex (u4ContextId, u4UapIfIndex, u2SVID, pu4SChIfIndex)
        != VLAN_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbVlanApiGetSChIfIndex : VlanApiGetSChIfIndex"
                         " fails for UAP IfIndex %d SVID %d\r\n", u4UapIfIndex,
                         u2SVID);
        return FSB_FAILURE;
    }
    else
    {
        return FSB_SUCCESS;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbVlanGetVlanInfoAndUntagFrame                  */
/*                                                                           */
/*    Description         : This function is called by L2Iwf to get VLAN     */
/*                          Information and strip of the outer-vlan from the */
/*                          packet before posting the packet to Snoop.       */
/*                          Assumption is that buffer will be released by    */
/*                          the calling function                             */
/*                                                                           */
/*    Input(s)            : pFrame   - Pointer to the incoming packet        */
/*                          u4InPort - Incoming interface Port Number        */
/*                                                                           */
/*    Output(s)           : pFrame   - Pointer to the updated packet         */
/*                          pVlantag  - Pointer to TAG Info of the packet    */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            :   FSB_SUCCESS/ FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbVlanGetVlanInfoAndUntagFrame (tCRU_BUF_CHAIN_DESC * pFrame,
                                 UINT4 u4InPort, tVlanTag * pVlanTag)
{
    if (VlanGetVlanInfoAndUntagFrame (pFrame, u4InPort,
                                      pVlanTag) == VLAN_NO_FORWARD)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbVlanGetVlanInfoAndUntagFrame :"
                         " VlanGetVlanInfoAndUntagFrame for InPort %d"
                         " returns failure\r\n", u4InPort);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbVlanAddOuterTagInFrame                        */
/*                                                                           */
/*    Description         : This function takes care of adding the VLAN tag  */
/*                          to the given frame since the ethertype will be   */
/*                          based on the port type                           */
/*                                                                           */
/*    Input(s)            : pFrame - Pointer to the frame.                   */
/*                          VlanId - VlanID to be tagged                     */
/*                          u1Priority - Priority to be tagged.              */
/*                          u4IfIndex  - Interface Index                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_FORWARD/VLAN_NO_FORWARD                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
FsbVlanAddOuterTagInFrame (tCRU_BUF_CHAIN_DESC * pBuf, tVlanId VlanId,
                           UINT1 u1Priority, UINT4 u4IfIndex)
{
    if (VlanAddOuterTagInFrame (pBuf, VlanId, u1Priority, u4IfIndex) !=
        VLAN_FORWARD)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbVlanAddOuterTagInFrame : VlanAddOuterTagInFrame for"
                         " VLAN %d IfIndex %d returns failure\r\n", VlanId,
                         u4IfIndex);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbL2IwfGetPotentialTxPortForAgg                     */
/*                                                                           */
/* Description        : This routine is invoked from CFA to obtain one       */
/*                      physical interface corresponding to the given        */
/*                      Port channel for frame transmission.                 */
/*                                                                           */
/* Input(s)           : u2AggId - Port channel index                         */
/*                                                                           */
/* Output(s)          : u2PhyPort - Global IfIndex of the Physical port      */
/*                                  corresponding to the port channel        */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
FsbL2IwfGetPotentialTxPortForAgg (UINT4 u4ContextId, UINT2 u2VlanId,
                                  UINT2 u2AggId, UINT2 *pu2PhyPort)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    UINT4               u4Index = 0;
    UINT1               u1OperStatus = CFA_IF_DOWN;
    BOOL1               bRetVal = OSIX_FALSE;

    if (FsbIsMLAGPortChannel (u2AggId) == FSB_SUCCESS)
    {
        pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4ContextId, u2VlanId);

        if (pFsbFipSnoopingEntry != NULL)
        {
            if (pFsbFipSnoopingEntry->u4PinnnedPortIfIndex != 0)
            {
                if (FsbGetIfOperStatus
                    (pFsbFipSnoopingEntry->u4PinnnedPortIfIndex,
                     &u1OperStatus) == FSB_SUCCESS)
                {
                    if (u1OperStatus == CFA_IF_UP)
                    {
                        *pu2PhyPort =
                            (UINT2) pFsbFipSnoopingEntry->u4PinnnedPortIfIndex;
                        return FSB_SUCCESS;
                    }
                }
            }
            else
            {
                for (u4Index = 1; u4Index < SYS_DEF_MAX_PHYSICAL_INTERFACES;
                     u4Index++)
                {
                    OSIX_BITLIST_IS_BIT_SET (pFsbFipSnoopingEntry->
                                             FsbPinnedPorts, u4Index,
                                             BRG_PORT_LIST_SIZE, bRetVal);
                    if (bRetVal == OSIX_TRUE)
                    {
                        if (FsbGetIfOperStatus (u4Index, &u1OperStatus) ==
                            FSB_SUCCESS)
                        {
                            if (u1OperStatus == CFA_IF_UP)
                            {
                                *pu2PhyPort = (UINT2) u4Index;
                                return FSB_SUCCESS;
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        if (L2IwfGetPotentialTxPortForAgg (u2AggId, pu2PhyPort) !=
            L2IWF_SUCCESS)
        {
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_DEBUGGING_LEVEL,
                             FSB_CONTROL_PATH_TRC,
                             "FsbL2IwfGetPotentialTxPortForAgg :"
                             " L2IwfGetPotentialTxPortForAgg for Agg Index %d"
                             " returns failure\r\n", u2AggId);
            return FSB_FAILURE;
        }
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbPortIsPortInPortChannel                           */
/*                                                                           */
/* Description        : This routine inovokes L2IWF to check whether the port*/
/*                      is a member of any Port Channel and if so returns    */
/*                      SUCCESS.                                             */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS/ FSB_FAILURE                             */
/*****************************************************************************/
INT4
FsbPortIsPortInPortChannel (UINT4 u4IfIndex)
{
    if (L2IwfIsPortInPortChannel (u4IfIndex) != L2IWF_SUCCESS)
    {
        FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE, FSB_NOTIFICATION_LEVEL,
                         FSB_NONE,
                         "FsbPortIsPortInPortChannel: L2IwfIsPortInPortChannel"
                         " returns failure\r\n");
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
}
#endif /* _FSBPORT_C_ */
