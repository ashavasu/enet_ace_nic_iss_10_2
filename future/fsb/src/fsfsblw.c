/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsfsblw.c,v 1.12 2017/10/09 13:13:59 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsbinc.h"

/* LOW LEVEL Routines for Table : FsMIFsbContextTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsbContextTable
 Input       :  The Indices
                FsMIFsbContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsbContextTable (UINT4 u4FsMIFsbContextId)
{
    if (FsbCxtGetContextEntry (u4FsMIFsbContextId) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsbContextTable
 Input       :  The Indices
                FsMIFsbContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsbContextTable (UINT4 *pu4FsMIFsbContextId)
{
    UINT4               u4ContextId = 0;

    *pu4FsMIFsbContextId = 0;

    /* Loop through, to find the the 1st Instance for which FIP-snooping is 
     * not shut */
    for (u4ContextId = 0; u4ContextId < FSB_MAX_CONTEXT_COUNT; u4ContextId++)
    {
        if (FsbCxtGetContextEntry (u4ContextId) != NULL)
        {
            *pu4FsMIFsbContextId = u4ContextId;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsbContextTable
 Input       :  The Indices
                FsMIFsbContextId
                nextFsMIFsbContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsbContextTable (UINT4 u4FsMIFsbContextId,
                                    UINT4 *pu4NextFsMIFsbContextId)
{
    UINT4               u4ContextId = 0;

    *pu4NextFsMIFsbContextId = 0;

    /* Loop through, to find the the next Instance from the given Instance 
     * for which FIP-snooping is not shut */

    for (u4ContextId = u4FsMIFsbContextId + 1;
         u4ContextId < FSB_MAX_CONTEXT_COUNT; u4ContextId++)
    {
        if (FsbCxtGetContextEntry (u4ContextId) != NULL)
        {
            *pu4NextFsMIFsbContextId = u4ContextId;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsbSystemControl
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                retValFsMIFsbSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbSystemControl (UINT4 u4FsMIFsbContextId,
                            INT4 *pi4RetValFsMIFsbSystemControl)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIFsbSystemControl = (INT4) (pFsbContextInfo->u1SystemControl);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbModuleStatus
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                retValFsMIFsbModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbModuleStatus (UINT4 u4FsMIFsbContextId,
                           INT4 *pi4RetValFsMIFsbModuleStatus)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIFsbModuleStatus = (INT4) (pFsbContextInfo->u1ModuleStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbFcMapMode
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                retValFsMIFsbFcMapMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbFcMapMode (UINT4 u4FsMIFsbContextId,
                        INT4 *pi4RetValFsMIFsbFcMapMode)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIFsbFcMapMode = (INT4) (pFsbContextInfo->u1FcMapMode);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbFcmap
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                retValFsMIFsbFcmap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbFcmap (UINT4 u4FsMIFsbContextId,
                    tSNMP_OCTET_STRING_TYPE * pRetValFsMIFsbFcmap)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRetValFsMIFsbFcmap->i4_Length = FSB_FCMAP_LEN;
    FSB_MEMCPY (pRetValFsMIFsbFcmap->pu1_OctetList, pFsbContextInfo->au1FcMap,
                FSB_FCMAP_LEN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbHouseKeepingTimePeriod
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                retValFsMIFsbHouseKeepingTimePeriod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbHouseKeepingTimePeriod (UINT4 u4FsMIFsbContextId,
                                     UINT4
                                     *pu4RetValFsMIFsbHouseKeepingTimePeriod)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMIFsbHouseKeepingTimePeriod =
        (UINT4) pFsbContextInfo->u2HouseKeepingTimePeriod;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbTraceOption
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                retValFsMIFsbTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbTraceOption (UINT4 u4FsMIFsbContextId,
                          INT4 *pi4RetValFsMIFsbTraceOption)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIFsbTraceOption = (INT4) (pFsbContextInfo->u4TraceOption);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbTrapStatus
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                retValFsMIFsbTrapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbTrapStatus (UINT4 u4FsMIFsbContextId,
                         INT4 *pi4RetValFsMIFsbTrapStatus)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIFsbTrapStatus = (INT4) (pFsbContextInfo->u1TrapStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbClearStats
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                retValFsMIFsbClearStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbClearStats (UINT4 u4FsMIFsbContextId,
                         INT4 *pi4RetValFsMIFsbClearStats)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIFsbClearStats = (INT4) (pFsbContextInfo->bClearStats);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbDefaultVlanId
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                retValFsMIFsbDefaultVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbDefaultVlanId (UINT4 u4FsMIFsbContextId,
                            INT4 *pi4RetValFsMIFsbDefaultVlanId)
{
#ifndef FSB_CUSTOM_FCOE_TAGGED_WANTED
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIFsbDefaultVlanId = (INT4) (pFsbContextInfo->u2DefaultVlanId);
#else
    UNUSED_PARAM (u4FsMIFsbContextId);
    UNUSED_PARAM (pi4RetValFsMIFsbDefaultVlanId);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbRowStatus
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                retValFsMIFsbRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbRowStatus (UINT4 u4FsMIFsbContextId,
                        INT4 *pi4RetValFsMIFsbRowStatus)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIFsbRowStatus = (INT4) (pFsbContextInfo->u1RowStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbTraceSeverityLevel
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                retValFsMIFsbTraceSeverityLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbTraceSeverityLevel (UINT4 u4FsMIFsbContextId,
                                 INT4 *pi4RetValFsMIFsbTraceSeverityLevel)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIFsbTraceSeverityLevel =
        (INT4) pFsbContextInfo->u4TraceSeverityLevel;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIFsbSystemControl
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                setValFsMIFsbSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsbSystemControl (UINT4 u4FsMIFsbContextId,
                            INT4 i4SetValFsMIFsbSystemControl)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo != NULL)
    {
        if (pFsbContextInfo->u1SystemControl ==
            (UINT1) i4SetValFsMIFsbSystemControl)
        {
            FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_DEBUGGING_LEVEL,
                             FSB_MGMT_TRC,
                             "FSB System Control Set done for Context = %d Value = %d \r\n",
                             u4FsMIFsbContextId, i4SetValFsMIFsbSystemControl);

            /* If the same System Control is set in that context then return
             * success */
            return SNMP_SUCCESS;
        }

        if (i4SetValFsMIFsbSystemControl == FSB_SHUTDOWN)
        {
            /* If FSB SystemControl is being SHUTDOWN, then delete all the
             * entries mapped to the context */
            if (FsbDeleteAllTablesInContext (u4FsMIFsbContextId) != FSB_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            /* If FSB SystemControl is being SHUTDOWN, then delete all the
             * s-channel filter entries */
            FsbRemoveSChannelFilters (u4FsMIFsbContextId);
            FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_DEBUGGING_LEVEL,
                             FSB_INIT_SHUT_TRC,
                             "FIP-snooping Module is shutdown\r\n");
        }
        pFsbContextInfo->u1SystemControl = (UINT1) i4SetValFsMIFsbSystemControl;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsbModuleStatus
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                setValFsMIFsbModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsbModuleStatus (UINT4 u4FsMIFsbContextId,
                           INT4 i4SetValFsMIFsbModuleStatus)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo != NULL)
    {
        if (pFsbContextInfo->u1ModuleStatus ==
            (UINT1) i4SetValFsMIFsbModuleStatus)
        {
            FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_DEBUGGING_LEVEL,
                             FSB_MGMT_TRC,
                             "FSB Module Status Set done for Context = %d Value = %d \r\n",
                             u4FsMIFsbContextId, i4SetValFsMIFsbModuleStatus);
            /* If the same Module Status is set in that context then return
             * success */
            return SNMP_SUCCESS;
        }

        if (i4SetValFsMIFsbModuleStatus == FSB_ENABLE)
        {
            /* Module Status is enabled, install Default VLAN filter */
            if (FsbInstallDefaultVLANFilter (u4FsMIFsbContextId,
                                             pFsbContextInfo) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbInstallDefaultVLANFilter returns failure\r\n");
                return SNMP_FAILURE;
            }
            /* Install Default filter for FCoE enabled VLAN by checking the 
             * enabled status of the vlan */
            if (FsbAddDefaultFilter (u4FsMIFsbContextId) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (u4FsMIFsbContextId,
                                 FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbAddDefaultFilter returns failure\r\n");
                return SNMP_FAILURE;
            }
            FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_DEBUGGING_LEVEL,
                             FSB_INIT_SHUT_TRC,
                             "FIP-snooping Module is enabled\r\n");
        }
        else
        {
            /* Delete all FcfEntry learnt in this context */
            if (FsbDeleteAllFcfEntry (u4FsMIFsbContextId) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbDeleteAllFcfEntry returns failure\r\n");
                return SNMP_FAILURE;
            }

            /* Delete all FipSessEntry learnt in this context */
            if (FsbDeleteAllFipSessEntry (u4FsMIFsbContextId) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbDeleteAllFipSessEntry returns failure\r\n");
                return SNMP_FAILURE;
            }
            /* If Enabled Status of FCoE VLAN is enable, delete 
             * Default filter for that FCoE VLAN */
            if (FsbDeleteDefaultFilter (u4FsMIFsbContextId) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbDeleteDefaultFilter returns failure\r\n");
                return SNMP_FAILURE;
            }

            /* If FSB module status is being disabled, then delete all the
             * s-channel filter entries */
            if (FsbRemoveSChannelFilters (u4FsMIFsbContextId) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbRemoveSChannelFilters returns failure\r\n");
                return SNMP_FAILURE;
            }

            /* Module Status is disabled, remove Default VLAN filter */
            if (FsbRemoveDefaultVLANFilter (pFsbContextInfo) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbRemoveDefaultVLANFilter returns failure\r\n");
                return SNMP_FAILURE;
            }
            FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_DEBUGGING_LEVEL,
                             FSB_INIT_SHUT_TRC,
                             "FIP-snooping Module is disabled\r\n");
        }
        pFsbContextInfo->u1ModuleStatus = (UINT1) i4SetValFsMIFsbModuleStatus;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsbFcMapMode
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                setValFsMIFsbFcMapMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsbFcMapMode (UINT4 u4FsMIFsbContextId, INT4 i4SetValFsMIFsbFcMapMode)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo != NULL)
    {
        if (pFsbContextInfo->u1FcMapMode == (UINT1) i4SetValFsMIFsbFcMapMode)
        {
            FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_DEBUGGING_LEVEL,
                             FSB_MGMT_TRC,
                             "FCMAP Mode is already set for context = %d Value = %d \r\n",
                             u4FsMIFsbContextId, i4SetValFsMIFsbFcMapMode);
            /* If same FCMAP Mode is already set in that context then return
             * success */
            return SNMP_SUCCESS;
        }
        pFsbContextInfo->u1FcMapMode = (UINT1) i4SetValFsMIFsbFcMapMode;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsbFcmap
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                setValFsMIFsbFcmap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsbFcmap (UINT4 u4FsMIFsbContextId,
                    tSNMP_OCTET_STRING_TYPE * pSetValFsMIFsbFcmap)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo != NULL)
    {
        FSB_MEMSET (pFsbContextInfo->au1FcMap, 0, FSB_FCMAP_LEN);
        FSB_MEMCPY (pFsbContextInfo->au1FcMap,
                    pSetValFsMIFsbFcmap->pu1_OctetList, FSB_FCMAP_LEN);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsbHouseKeepingTimePeriod
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                setValFsMIFsbHouseKeepingTimePeriod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsbHouseKeepingTimePeriod (UINT4 u4FsMIFsbContextId,
                                     UINT4
                                     u4SetValFsMIFsbHouseKeepingTimePeriod)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo != NULL)
    {
        if (pFsbContextInfo->u2HouseKeepingTimePeriod
            == (UINT2) u4SetValFsMIFsbHouseKeepingTimePeriod)
        {
            FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_DEBUGGING_LEVEL,
                             FSB_MGMT_TRC,
                             "Housekeeping Timer is already set  for Context = %d Value = %d \r\n",
                             u4FsMIFsbContextId,
                             u4SetValFsMIFsbHouseKeepingTimePeriod);
            /* If the same Housekeepimer timer value is already set then return
             * success */
            return SNMP_SUCCESS;
        }
        pFsbContextInfo->u2HouseKeepingTimePeriod =
            (UINT2) u4SetValFsMIFsbHouseKeepingTimePeriod;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsbTraceOption
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                setValFsMIFsbTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsbTraceOption (UINT4 u4FsMIFsbContextId,
                          INT4 i4SetValFsMIFsbTraceOption)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo != NULL)
    {
        if (pFsbContextInfo->u4TraceOption ==
            (UINT2) i4SetValFsMIFsbTraceOption)
        {
            FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_DEBUGGING_LEVEL,
                             FSB_MGMT_TRC,
                             "Trace Option is already set for Context = %d Value = %d \r\n",
                             u4FsMIFsbContextId, i4SetValFsMIFsbTraceOption);
            /* If the same trace value is already set then return
             * success */
            return SNMP_SUCCESS;
        }
        pFsbContextInfo->u4TraceOption = (UINT4) i4SetValFsMIFsbTraceOption;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsbTrapStatus
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                setValFsMIFsbTrapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsbTrapStatus (UINT4 u4FsMIFsbContextId,
                         INT4 i4SetValFsMIFsbTrapStatus)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo != NULL)
    {
        if (pFsbContextInfo->u1TrapStatus == (UINT1) i4SetValFsMIFsbTrapStatus)
        {
            FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_DEBUGGING_LEVEL,
                             FSB_MGMT_TRC,
                             "Trap Status is already set for Context = %d Value = %d \r\n",
                             u4FsMIFsbContextId, i4SetValFsMIFsbTrapStatus);
            /* If the same trap status is already set then return
             * success */
            return SNMP_SUCCESS;
        }
        pFsbContextInfo->u1TrapStatus = (UINT1) i4SetValFsMIFsbTrapStatus;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsbClearStats
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                setValFsMIFsbClearStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsbClearStats (UINT4 u4FsMIFsbContextId,
                         INT4 i4SetValFsMIFsbClearStats)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo != NULL)
    {
        if (i4SetValFsMIFsbClearStats != FSB_TRUE)
        {
            /* If Set value is not FSB_TRUE, then Statistics Table need not
             * be cleared */
            return SNMP_SUCCESS;
        }
        pFsbContextInfo->bClearStats = (BOOL1) i4SetValFsMIFsbClearStats;

        /* Clear fsMIFsbGlobalStatsTable MIB Table */
        FsbClearGlobalStats (u4FsMIFsbContextId);

        /* Clear fsMIFsbVlanStatsTable MIB Table */
        FsbClearVlanStats (u4FsMIFsbContextId);

        /* Clear fsMIFsbSessStatsTable MIB Table */
        FsbClearSessionStats (u4FsMIFsbContextId);

        pFsbContextInfo->bClearStats = FSB_FALSE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsbDefaultVlanId
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                setValFsMIFsbDefaultVlanId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsbDefaultVlanId (UINT4 u4FsMIFsbContextId,
                            INT4 i4SetValFsMIFsbDefaultVlanId)
{
#ifndef FSB_CUSTOM_FCOE_TAGGED_WANTED
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo != NULL)
    {
        if (pFsbContextInfo->u2DefaultVlanId ==
            (UINT2) i4SetValFsMIFsbDefaultVlanId)
        {
            FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_DEBUGGING_LEVEL,
                             FSB_MGMT_TRC,
                             "Same VLAN is configured again for Context = %d Value = %d \r\n",
                             u4FsMIFsbContextId, i4SetValFsMIFsbDefaultVlanId);
            /* If the same VLAN is again configured then return
             * success */
            return SNMP_SUCCESS;
        }
        pFsbContextInfo->u2DefaultVlanId = (UINT2) i4SetValFsMIFsbDefaultVlanId;
    }
#else
    UNUSED_PARAM (u4FsMIFsbContextId);
    UNUSED_PARAM (i4SetValFsMIFsbDefaultVlanId);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsbRowStatus
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                setValFsMIFsbRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsbRowStatus (UINT4 u4FsMIFsbContextId, INT4 i4SetValFsMIFsbRowStatus)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    switch (i4SetValFsMIFsbRowStatus)
    {
        case CREATE_AND_WAIT:    /* Intentional fall through for MSR */
        case CREATE_AND_GO:

            /* Memblock creation and initializing the default values
             * to the context are handled inside the below function and
             * rowstatus is initialized here */
            pFsbContextInfo = FsbCxtMemInit (u4FsMIFsbContextId);

            if (pFsbContextInfo == NULL)
            {
                /* Trace is added Inside FsbCxtMemInit */
                return SNMP_FAILURE;
            }

            pFsbContextInfo->u1RowStatus = ACTIVE;

            break;

        case DESTROY:

            pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

            if (pFsbContextInfo != NULL)
            {
                /* Delete the context entry */
                FsbCxtHandleDeleteContext (u4FsMIFsbContextId);
            }
            break;

        default:
            break;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsbTraceSeverityLevel
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                setValFsMIFsbTraceSeverityLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsbTraceSeverityLevel (UINT4 u4FsMIFsbContextId,
                                 INT4 i4SetValFsMIFsbTraceSeverityLevel)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo != NULL)
    {
        if (pFsbContextInfo->u4TraceSeverityLevel ==
            (UINT4) i4SetValFsMIFsbTraceSeverityLevel)
        {
            return SNMP_SUCCESS;
        }

        pFsbContextInfo->u4TraceSeverityLevel =
            (UINT4) i4SetValFsMIFsbTraceSeverityLevel;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIFsbSystemControl
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                testValFsMIFsbSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsbSystemControl (UINT4 *pu4ErrorCode,
                               UINT4 u4FsMIFsbContextId,
                               INT4 i4TestValFsMIFsbSystemControl)
{
    if ((i4TestValFsMIFsbSystemControl != FSB_START) &&
        (i4TestValFsMIFsbSystemControl != FSB_SHUTDOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (FsbCxtGetContextEntry (u4FsMIFsbContextId) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsbModuleStatus
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                testValFsMIFsbModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsbModuleStatus (UINT4 *pu4ErrorCode,
                              UINT4 u4FsMIFsbContextId,
                              INT4 i4TestValFsMIFsbModuleStatus)
{
    if ((i4TestValFsMIFsbModuleStatus != FSB_ENABLE) &&
        (i4TestValFsMIFsbModuleStatus != FSB_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (FsbUtilIsFsbStarted (u4FsMIFsbContextId) != FSB_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_FSB_ERR_MODULE_SHUTDOWN);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsbFcMapMode
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                testValFsMIFsbFcMapMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsbFcMapMode (UINT4 *pu4ErrorCode,
                           UINT4 u4FsMIFsbContextId,
                           INT4 i4TestValFsMIFsbFcMapMode)
{
    if ((i4TestValFsMIFsbFcMapMode != FSB_FCMAP_GLOBAL) &&
        (i4TestValFsMIFsbFcMapMode != FSB_FCMAP_VLAN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (FsbUtilIsFsbStarted (u4FsMIFsbContextId) != FSB_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_FSB_ERR_MODULE_SHUTDOWN);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsbFcmap
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                testValFsMIFsbFcmap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsbFcmap (UINT4 *pu4ErrorCode,
                       UINT4 u4FsMIFsbContextId,
                       tSNMP_OCTET_STRING_TYPE * pTestValFsMIFsbFcmap)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    if (pTestValFsMIFsbFcmap->i4_Length != FSB_FCMAP_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (FsbUtilIsFsbStarted (u4FsMIFsbContextId) != FSB_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_FSB_ERR_MODULE_SHUTDOWN);
        return SNMP_FAILURE;
    }

    /* Table 1 : This Table denotes the possible comibination for the 
     * configuration of FsbFcmap and FIPSnoopingFcmap */
    /********************************************************************************/
    /* FC-MAP Mode  Module Status  Enabled Status   Configuration  Configuration    */
    /*                                              of FsbFcmap    FIPSnoopingFcmap */
    /*------------------------------------------------------------------------------*/
    /*                                                                              */
    /* Global       Disabled       Enabled/Diabled  Allowed        Allowed          */
    /*                                                                              */
    /* Global       Enabled        Enabled/Diabled  Denied         Allowed          */
    /*                                                                              */
    /* Vlan         Disabled       Disabled         Allowed        Allowed          */
    /*                                                                              */
    /* Vlan         Disabled       Enabled          Allowed        Allowed          */
    /*                                                                              */
    /* Vlan         Enabled        Disabled         Allowed        Allowed          */
    /*                                                                              */
    /* Vlan         Enabled        Enabled          Allowed        Denied           */
    /*                                                                              */
    /********************************************************************************/

    /* The below validation is done based on the above Table */
    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo != NULL)
    {
        if ((pFsbContextInfo->u1ModuleStatus == FSB_ENABLE)
            && (pFsbContextInfo->u1FcMapMode == FSB_FCMAP_GLOBAL))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_FSB_ERR_MODULE_ENABLED);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsbHouseKeepingTimePeriod
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                testValFsMIFsbHouseKeepingTimePeriod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsbHouseKeepingTimePeriod (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsMIFsbContextId,
                                        UINT4
                                        u4TestValFsMIFsbHouseKeepingTimePeriod)
{
    if (((u4TestValFsMIFsbHouseKeepingTimePeriod < FSB_MIN_HOUSEKEEPING_TIME) ||
         (u4TestValFsMIFsbHouseKeepingTimePeriod > FSB_MAX_HOUSEKEEPING_TIME))
        && (u4TestValFsMIFsbHouseKeepingTimePeriod != FSB_NO_HOUSEKEEPING_TIME))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (FsbUtilIsFsbStarted (u4FsMIFsbContextId) != FSB_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_FSB_ERR_MODULE_SHUTDOWN);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsbTraceOption
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                testValFsMIFsbTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsbTraceOption (UINT4 *pu4ErrorCode,
                             UINT4 u4FsMIFsbContextId,
                             INT4 i4TestValFsMIFsbTraceOption)
{

    if ((i4TestValFsMIFsbTraceOption < FSB_TRC_MIN_VAL) ||
        (i4TestValFsMIFsbTraceOption > FSB_TRC_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (FsbUtilIsFsbStarted (u4FsMIFsbContextId) != FSB_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_FSB_ERR_MODULE_SHUTDOWN);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsbTrapStatus
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                testValFsMIFsbTrapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsbTrapStatus (UINT4 *pu4ErrorCode,
                            UINT4 u4FsMIFsbContextId,
                            INT4 i4TestValFsMIFsbTrapStatus)
{
    if ((i4TestValFsMIFsbTrapStatus != FSB_ENABLE) &&
        (i4TestValFsMIFsbTrapStatus != FSB_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (FsbUtilIsFsbStarted (u4FsMIFsbContextId) != FSB_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_FSB_ERR_MODULE_SHUTDOWN);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsbClearStats
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                testValFsMIFsbClearStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsbClearStats (UINT4 *pu4ErrorCode,
                            UINT4 u4FsMIFsbContextId,
                            INT4 i4TestValFsMIFsbClearStats)
{
    if ((i4TestValFsMIFsbClearStats != FSB_TRUE) &&
        (i4TestValFsMIFsbClearStats != FSB_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (FsbUtilIsFsbStarted (u4FsMIFsbContextId) != FSB_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_FSB_ERR_MODULE_SHUTDOWN);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsbDefaultVlanId
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                testValFsMIFsbDefaultVlanId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsbDefaultVlanId (UINT4 *pu4ErrorCode,
                               UINT4 u4FsMIFsbContextId,
                               INT4 i4TestValFsMIFsbDefaultVlanId)
{
#ifndef FSB_CUSTOM_FCOE_TAGGED_WANTED
    tFsbContextInfo    *pFsbContextInfo = NULL;

    /* Check if the VLAN is an Active VLAN */
    if (FsbPortIsVlanActive (u4FsMIFsbContextId,
                             (UINT2) i4TestValFsMIFsbDefaultVlanId) != FSB_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_FSB_ERR_VLAN_NOT_ACTIVE);
        return SNMP_FAILURE;
    }

    if (FsbUtilIsFsbStarted (u4FsMIFsbContextId) != FSB_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_FSB_ERR_MODULE_SHUTDOWN);
        return SNMP_FAILURE;
    }

    /* If module status is enabled, then untagged vlan cannot
     * be configured */
    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo != NULL)
    {
        if (pFsbContextInfo->u1ModuleStatus == FSB_ENABLE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_FSB_ERR_MODULE_ENABLED);
            return SNMP_FAILURE;
        }
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4FsMIFsbContextId);
    UNUSED_PARAM (i4TestValFsMIFsbDefaultVlanId);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsbRowStatus
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                testValFsMIFsbRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsbRowStatus (UINT4 *pu4ErrorCode,
                           UINT4 u4FsMIFsbContextId,
                           INT4 i4TestValFsMIFsbRowStatus)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    if (u4FsMIFsbContextId >= FSB_MAX_CONTEXT_COUNT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsMIFsbRowStatus)
    {
        case CREATE_AND_GO:
            if (FsbPortIsVcExist (u4FsMIFsbContextId) != FSB_TRUE)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

            /* If Context is present and trying to create the same entry then
             * return failure */
            if (pFsbContextInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case DESTROY:
            pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

            /* If the entry is not created and trying to delete the same, then
             * return failure */
            if (pFsbContextInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }

            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsbTraceSeverityLevel
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                testValFsMIFsbTraceSeverityLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsbTraceSeverityLevel (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsMIFsbContextId,
                                    INT4 i4TestValFsMIFsbTraceSeverityLevel)
{
    if (u4FsMIFsbContextId >= FSB_MAX_CONTEXT_COUNT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (FsbUtilIsFsbStarted (u4FsMIFsbContextId) != FSB_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_FSB_ERR_MODULE_SHUTDOWN);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMIFsbTraceSeverityLevel < FSB_EMERGENCY_LEVEL)
        || (i4TestValFsMIFsbTraceSeverityLevel > FSB_DEBUGGING_LEVEL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFsbContextTable
 Input       :  The Indices
                FsMIFsbContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsbContextTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIFsbFIPSnoopingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsbFIPSnoopingTable
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsbFIPSnoopingTable (UINT4 u4FsMIFsbContextId,
                                                 INT4
                                                 i4FsMIFsbFIPSnoopingVlanIndex)
{
    if (FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                (UINT2) i4FsMIFsbFIPSnoopingVlanIndex) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsbFIPSnoopingTable
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsbFIPSnoopingTable (UINT4 *pu4FsMIFsbContextId,
                                         INT4 *pi4FsMIFsbFIPSnoopingVlanIndex)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = (tFsbFipSnoopingEntry *)
        RBTreeGetFirst (gFsbGlobals.FsbFipSnoopingTable);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4FsMIFsbContextId = pFsbFipSnoopingEntry->u4ContextId;
    *pi4FsMIFsbFIPSnoopingVlanIndex = (INT4) (pFsbFipSnoopingEntry->u2VlanId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsbFIPSnoopingTable
 Input       :  The Indices
                FsMIFsbContextId
                nextFsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex
                nextFsMIFsbFIPSnoopingVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsbFIPSnoopingTable (UINT4 u4FsMIFsbContextId,
                                        UINT4 *pu4NextFsMIFsbContextId,
                                        INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                        INT4
                                        *pi4NextFsMIFsbFIPSnoopingVlanIndex)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbFipSnoopingEntry FsbFipSnoopingEntry;

    FSB_MEMSET (&FsbFipSnoopingEntry, 0, sizeof (tFsbFipSnoopingEntry));

    FsbFipSnoopingEntry.u4ContextId = u4FsMIFsbContextId;
    FsbFipSnoopingEntry.u2VlanId = (UINT2) i4FsMIFsbFIPSnoopingVlanIndex;

    pFsbFipSnoopingEntry = (tFsbFipSnoopingEntry *)
        RBTreeGetNext (gFsbGlobals.FsbFipSnoopingTable,
                       (tRBElem *) & FsbFipSnoopingEntry, NULL);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4NextFsMIFsbContextId = pFsbFipSnoopingEntry->u4ContextId;
    *pi4NextFsMIFsbFIPSnoopingVlanIndex =
        (INT4) (pFsbFipSnoopingEntry->u2VlanId);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsbFIPSnoopingFcmap
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbFIPSnoopingFcmap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbFIPSnoopingFcmap (UINT4 u4FsMIFsbContextId,
                               INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMIFsbFIPSnoopingFcmap)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRetValFsMIFsbFIPSnoopingFcmap->i4_Length = FSB_FCMAP_LEN;

    FSB_MEMCPY (pRetValFsMIFsbFIPSnoopingFcmap->pu1_OctetList,
                pFsbFipSnoopingEntry->au1FcMap, FSB_FCMAP_LEN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbFIPSnoopingEnabledStatus
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbFIPSnoopingEnabledStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbFIPSnoopingEnabledStatus (UINT4 u4FsMIFsbContextId,
                                       INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                       INT4
                                       *pi4RetValFsMIFsbFIPSnoopingEnabledStatus)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsMIFsbFIPSnoopingEnabledStatus =
        (INT4) (pFsbFipSnoopingEntry->u1EnabledStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbFIPSnoopingRowStatus
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbFIPSnoopingRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbFIPSnoopingRowStatus (UINT4 u4FsMIFsbContextId,
                                   INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                   INT4 *pi4RetValFsMIFsbFIPSnoopingRowStatus)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsMIFsbFIPSnoopingRowStatus =
        (INT4) (pFsbFipSnoopingEntry->u1RowStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbFIPSnoopingPinnedPorts
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbFIPSnoopingPinnedPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbFIPSnoopingPinnedPorts (UINT4 u4FsMIFsbContextId,
                                     INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsMIFsbFIPSnoopingPinnedPorts)
{

    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRetValFsMIFsbFIPSnoopingPinnedPorts->i4_Length = BRG_PORT_LIST_SIZE;
    FSB_MEMCPY (pRetValFsMIFsbFIPSnoopingPinnedPorts->pu1_OctetList,
                pFsbFipSnoopingEntry->FsbPinnedPorts, BRG_PORT_LIST_SIZE);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIFsbFIPSnoopingFcmap
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                setValFsMIFsbFIPSnoopingFcmap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsbFIPSnoopingFcmap (UINT4 u4FsMIFsbContextId,
                               INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsMIFsbFIPSnoopingFcmap)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry != NULL)
    {
        FSB_MEMCPY (pFsbFipSnoopingEntry->au1FcMap,
                    pSetValFsMIFsbFIPSnoopingFcmap->pu1_OctetList,
                    FSB_FCMAP_LEN);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsbFIPSnoopingEnabledStatus
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                setValFsMIFsbFIPSnoopingEnabledStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsbFIPSnoopingEnabledStatus (UINT4 u4FsMIFsbContextId,
                                       INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                       INT4
                                       i4SetValFsMIFsbFIPSnoopingEnabledStatus)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbContextInfo    *pFsbContextInfo = NULL;
    UINT2               u2FilTypeIndex = 0;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry != NULL)
    {
        if (pFsbFipSnoopingEntry->u1EnabledStatus
            == (UINT1) i4SetValFsMIFsbFIPSnoopingEnabledStatus)
        {
            /* If the enabled status is already set then return
             * success */
            return SNMP_SUCCESS;
        }
        pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

        if (pFsbContextInfo == NULL)
        {
            /* If context is NULL, return FAILURE */
            return SNMP_FAILURE;
        }

        if (i4SetValFsMIFsbFIPSnoopingEnabledStatus == FSB_ENABLE)
        {
            if (pFsbContextInfo->u1ModuleStatus == FSB_ENABLE)
            {
                if (FsbValidatePortsInFCoEVlan
                    (u4FsMIFsbContextId,
                     (UINT2) i4FsMIFsbFIPSnoopingVlanIndex) == FSB_SUCCESS)
                {
                    if (FsFsbHwWrSetFCoEParams
                        (u4FsMIFsbContextId,
                         (UINT2) i4FsMIFsbFIPSnoopingVlanIndex,
                         (UINT1) i4SetValFsMIFsbFIPSnoopingEnabledStatus) !=
                        FSB_SUCCESS)
                    {
                        FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                                         FSB_ERROR_LEVEL, FSB_NONE,
                                         "nmhSetFsMIFsbFIPSnoopingEnabledStatus:"
                                         "FsFsbHwWrSetFCoEParams failed\r\n");
                        return SNMP_FAILURE;
                    }
                    pFsbFipSnoopingEntry->FsbFilterEntry =
                        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tFsbFilterEntry,
                                                              FsbFilterNode)),
                                              FsbCompareFipFilterEntryIndex);
                    gFsbGlobals.u4FsbFilterEntryRBCount++;
                    for (u2FilTypeIndex = FSB_ALLOW_DIS_SOLICITATION;
                         u2FilTypeIndex <= FSB_GLOBAL_DENY; u2FilTypeIndex++)
                    {
                        if (FsbInstallDefaultFilter
                            (u2FilTypeIndex,
                             pFsbFipSnoopingEntry) != FSB_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                        if (FsbRedSyncUpDefaultFilter (pFsbFipSnoopingEntry) !=
                            FSB_SUCCESS)
                        {
                            FSB_CONTEXT_TRC (pFsbFipSnoopingEntry->u4ContextId,
                                             FSB_ERROR_LEVEL, FSB_NONE,
                                             "nmhSetFsMIFsbFIPSnoopingEnabledStatus:"
                                             "FsbRedSyncUpDefaultFilter failed\r\n");
                            return SNMP_FAILURE;
                        }
                    }
                }
            }
        }
        else
        {
            if (pFsbContextInfo->u1ModuleStatus == FSB_ENABLE)
            {
                if (FsbRemoveDefaultFilter (pFsbFipSnoopingEntry) !=
                    FSB_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                if (FsbDeleteFipSessionEntryForFcoeVlan
                    (pFsbFipSnoopingEntry->u4ContextId,
                     pFsbFipSnoopingEntry->u2VlanId) != FSB_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                if (FsbDeleteFCFEntryForFcoeVlan
                    (pFsbFipSnoopingEntry->u4ContextId,
                     pFsbFipSnoopingEntry->u2VlanId) != FSB_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
        }

        pFsbFipSnoopingEntry->u1EnabledStatus =
            (UINT1) i4SetValFsMIFsbFIPSnoopingEnabledStatus;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsbFIPSnoopingRowStatus
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                setValFsMIFsbFIPSnoopingRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsbFIPSnoopingRowStatus (UINT4 u4FsMIFsbContextId,
                                   INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                   INT4 i4SetValFsMIFsbFIPSnoopingRowStatus)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    switch (i4SetValFsMIFsbFIPSnoopingRowStatus)
    {
        case CREATE_AND_WAIT:    /* Intentional fall through for MSR */
        case CREATE_AND_GO:

            if ((pFsbFipSnoopingEntry = (tFsbFipSnoopingEntry *) MemAllocMemBlk
                 (FSB_FCOE_VLAN_ENTRIES_MEMPOOL_ID)) == NULL)
            {
                FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                 "nmhSetFsMIFsbFIPSnoopingRowStatus :"
                                 " tFsbFipSnoopingEntry memory allocation failed\r\n");
                return SNMP_FAILURE;
            }
            FSB_MEMSET (pFsbFipSnoopingEntry, 0, sizeof (tFsbFipSnoopingEntry));

            /* Initialise to default value */
            pFsbFipSnoopingEntry->u4ContextId = (UINT4) u4FsMIFsbContextId;    /* RBTree INDEX */
            pFsbFipSnoopingEntry->u2VlanId = (UINT2) i4FsMIFsbFIPSnoopingVlanIndex;    /* RBTree Index */
            pFsbFipSnoopingEntry->u1EnabledStatus =
                FSB_DEFAULT_VLAN_ENABLED_STATUS;
            FSB_MEMCPY (pFsbFipSnoopingEntry->au1FcMap, gau1DefaultFcMapValue,
                        FSB_FCMAP_LEN);
            pFsbFipSnoopingEntry->u1RowStatus = ACTIVE;
            FSB_MEMSET (pFsbFipSnoopingEntry->FsbPinnedPorts, 0,
                        BRG_PORT_LIST_SIZE);
            if (RBTreeAdd (gFsbGlobals.FsbFipSnoopingTable,
                           (tRBElem *) pFsbFipSnoopingEntry) != RB_SUCCESS)
            {
                MemReleaseMemBlock (FSB_FCOE_VLAN_ENTRIES_MEMPOOL_ID,
                                    (UINT1 *) pFsbFipSnoopingEntry);
                return SNMP_FAILURE;
            }

            /* Mark the VLAN as FCoE VLAN in L2IWF */
            FsbSetFCoEVlanType (u4FsMIFsbContextId,
                                (UINT2) i4FsMIFsbFIPSnoopingVlanIndex,
                                FCOE_VLAN);

            /* Set Mac-learnig as disable for FCoE VlAN */
            if (FsbPortSetVlanMacLearningStatus (u4FsMIFsbContextId,
                                                 (UINT2)
                                                 i4FsMIFsbFIPSnoopingVlanIndex,
                                                 VLAN_DISABLED) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                 "nmhSetFsMIFsbFIPSnoopingRowStatus :"
                                 " failure in disabling Mac-learing status\r\n");
                return SNMP_FAILURE;
            }

            /* This function is used to retreive the port list and populate
             * FsbIntfEntry  */
            FsbGetVlanPortsAndPopulateIntfEntry (u4FsMIFsbContextId,
                                                 (UINT2)
                                                 i4FsMIFsbFIPSnoopingVlanIndex);
            break;

        case DESTROY:
            pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                           (UINT2)
                                                           i4FsMIFsbFIPSnoopingVlanIndex);

            if (pFsbFipSnoopingEntry != NULL)
            {
                /* This function is used to delete FIP session entries
                 * mapped to the Fcoe Vlan */
                FsbDeleteFipSessionEntryForFcoeVlan (u4FsMIFsbContextId,
                                                     (UINT2)
                                                     i4FsMIFsbFIPSnoopingVlanIndex);

                /* This function is used to delete all FCF entries
                 * mapped to the deleted Fcoe Vlan */
                FsbDeleteFCFEntryForFcoeVlan (u4FsMIFsbContextId,
                                              (UINT2)
                                              i4FsMIFsbFIPSnoopingVlanIndex);

                /* This function is used to remove FsbIntfEntry  */
                FsbRemoveIntfEntryForFcoeVlan (u4FsMIFsbContextId,
                                               (UINT2)
                                               i4FsMIFsbFIPSnoopingVlanIndex);

                /* This function is used to remove Default filter */
                if (FsbRemoveDefaultFilter (pFsbFipSnoopingEntry)
                    != FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_DEBUGGING_LEVEL,
                                     FSB_MGMT_TRC,
                                     "nmhSetFsMIFsbFIPSnoopingRowStatus :"
                                     "Removing Default filter failed\r\n");
                }

                /* Enable Mac-learning status since the VLAN is no longer
                 * an FCoE VlAN */
                if (FsbPortSetVlanMacLearningStatus (u4FsMIFsbContextId,
                                                     (UINT2)
                                                     i4FsMIFsbFIPSnoopingVlanIndex,
                                                     VLAN_ENABLED) !=
                    FSB_SUCCESS)
                {
                    FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_ERROR_LEVEL,
                                     FSB_NONE,
                                     "nmhSetFsMIFsbFIPSnoopingRowStatus :"
                                     " failure in enabling Mac-learing status\r\n");
                    return SNMP_FAILURE;
                }

                FsbSetFCoEVlanType (u4FsMIFsbContextId,
                                    (UINT2) i4FsMIFsbFIPSnoopingVlanIndex,
                                    L2_VLAN);
                if (pFsbFipSnoopingEntry->FsbFilterEntry != NULL)
                {
                    gFsbGlobals.u4FsbFilterEntryRBCount--;
                    RBTreeDelete (pFsbFipSnoopingEntry->FsbFilterEntry);
                    pFsbFipSnoopingEntry->FsbFilterEntry = NULL;
                }
                if (RBTreeRemove (gFsbGlobals.FsbFipSnoopingTable,
                                  (tRBElem *) pFsbFipSnoopingEntry) !=
                    RB_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                MemReleaseMemBlock (FSB_FCOE_VLAN_ENTRIES_MEMPOOL_ID,
                                    (UINT1 *) pFsbFipSnoopingEntry);
            }
            break;

        default:
            break;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsbFIPSnoopingPinnedPorts
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                setValFsMIFsbFIPSnoopingPinnedPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsbFIPSnoopingPinnedPorts (UINT4 u4FsMIFsbContextId,
                                     INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValFsMIFsbFIPSnoopingPinnedPorts)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tPortList           FsbPinnedPorts;
    UINT4               u4Index = 0;
    UINT4               u4PinnnedPortIfIndex = 0;
    BOOL1               bRetVal = OSIX_FALSE;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry != NULL)
    {

        FSB_MEMSET (FsbPinnedPorts, 0, sizeof (tPortList));

        FSB_MEMCPY (FsbPinnedPorts,
                    pSetValFsMIFsbFIPSnoopingPinnedPorts->pu1_OctetList,
                    BRG_PORT_LIST_SIZE);

        if (FSB_MEMCMP (pFsbFipSnoopingEntry->FsbPinnedPorts,
                        FsbPinnedPorts, BRG_PORT_LIST_SIZE) == 0)
        {
            /* If the administartor has configued the same entry
             * which is in database, then return without doing
             * any updation*/
            return SNMP_SUCCESS;
        }

        if (FsUtilBitListIsAllZeros
            (pSetValFsMIFsbFIPSnoopingPinnedPorts->pu1_OctetList,
             BRG_PORT_LIST_SIZE) == OSIX_TRUE)
        {
            /* If the Pinned Ports given by the Administrator is all zeroes,
             * update the Discovery Advertisement filter (Delete and install),
             * before updation of the Database entry. 
             * This is done because, NPUTIL logic (local/remote) is based on the 
             * Pinned Ports Database entry.*/

            if (pFsbFipSnoopingEntry->u1EnabledStatus == FSB_ENABLE)
            {
                FsbUpdateDefaultFilter (pFsbFipSnoopingEntry, FsbPinnedPorts);
            }

            FSB_MEMSET (pFsbFipSnoopingEntry->FsbPinnedPorts, 0,
                        BRG_PORT_LIST_SIZE);
            /* Update the database entry with the given input by the Administrator */
            /* In this case, it will be all zeroes. Therefore Pinned Port Count 
             * and Pinned Port Index is also initialized to Zero */

            FSB_MEMCPY (pFsbFipSnoopingEntry->FsbPinnedPorts,
                        pSetValFsMIFsbFIPSnoopingPinnedPorts->pu1_OctetList,
                        BRG_PORT_LIST_SIZE);

            pFsbFipSnoopingEntry->u4PinnedPortCount = 0;
            pFsbFipSnoopingEntry->u4PinnnedPortIfIndex = 0;
            FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_DEBUGGING_LEVEL,
                             FSB_MGMT_TRC,
                             "FSB: No Pinned Ports configured for VLAN - %d \r\n",
                             i4FsMIFsbFIPSnoopingVlanIndex);

            return SNMP_SUCCESS;
        }

        /* Update the database entry, if the value configured by the Administrator
         * is non-zero and populate the Pinned Port Count and Pinned Port If Index
         * based on the given value*/

        FSB_MEMSET (pFsbFipSnoopingEntry->FsbPinnedPorts, 0,
                    BRG_PORT_LIST_SIZE);
        pFsbFipSnoopingEntry->u4PinnedPortCount = 0;
        pFsbFipSnoopingEntry->u4PinnnedPortIfIndex = 0;

        FSB_MEMCPY (pFsbFipSnoopingEntry->FsbPinnedPorts,
                    pSetValFsMIFsbFIPSnoopingPinnedPorts->pu1_OctetList,
                    BRG_PORT_LIST_SIZE);

        for (u4Index = 1; u4Index < SYS_DEF_MAX_PHYSICAL_INTERFACES; u4Index++)
        {
            OSIX_BITLIST_IS_BIT_SET (pFsbFipSnoopingEntry->FsbPinnedPorts,
                                     u4Index, BRG_PORT_LIST_SIZE, bRetVal);
            if (bRetVal == OSIX_TRUE)
            {
                pFsbFipSnoopingEntry->u4PinnedPortCount++;
                if (pFsbFipSnoopingEntry->u4PinnedPortCount == 1)
                {
                    u4PinnnedPortIfIndex = u4Index;
                    FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_DEBUGGING_LEVEL,
                                     FSB_MGMT_TRC,
                                     "FSB Set Pinned Port Index - %d for VLAN - %d \r\n",
                                     u4PinnnedPortIfIndex,
                                     i4FsMIFsbFIPSnoopingVlanIndex);
                }
            }
        }
        if (pFsbFipSnoopingEntry->u4PinnedPortCount == 1)
        {
            /* Pinned Port If index will be populated, only if the Pinned Port Count is 1 */
            pFsbFipSnoopingEntry->u4PinnnedPortIfIndex = u4PinnnedPortIfIndex;
            FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_DEBUGGING_LEVEL,
                             FSB_MGMT_TRC,
                             "FSB Set Pinned Port Index - %d for VLAN - %d\r\n",
                             pFsbFipSnoopingEntry->u4PinnnedPortIfIndex,
                             i4FsMIFsbFIPSnoopingVlanIndex);
        }

        /* If the Pinned PortList is updated and if the FIP Snooping
         * Enabled Status is already enabled, Discovery Advertisement filter will be 
         * already installed.
         * Therefore Discovery Advertisement filter  neeeds to be updated with
         * the updated Pinned port-list.*/

        if (pFsbFipSnoopingEntry->u1EnabledStatus == FSB_ENABLE)
        {
            /* Filter updation should be done after the updation of Database entry,
             * if the Administrator has configured non-zero entry for the PortList.
             * This is done because, NPUTIL logic (local/remote) is based on the 
             * Pinned Ports Database entry.*/
            FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_DEBUGGING_LEVEL,
                             FSB_MGMT_TRC,
                             "[FSB] FsbUpdateDefaultFilter called with"
                             "Pinned Port Index  - %d for VLAN - %d \r\n",
                             u4PinnnedPortIfIndex,
                             i4FsMIFsbFIPSnoopingVlanIndex);
            FsbUpdateDefaultFilter (pFsbFipSnoopingEntry,
                                    pFsbFipSnoopingEntry->FsbPinnedPorts);
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIFsbFIPSnoopingFcmap
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                testValFsMIFsbFIPSnoopingFcmap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsbFIPSnoopingFcmap (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsMIFsbContextId,
                                  INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsMIFsbFIPSnoopingFcmap)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbContextInfo    *pFsbContextInfo = NULL;

    if (pTestValFsMIFsbFIPSnoopingFcmap->i4_Length != FSB_FCMAP_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (FsbUtilIsFsbStarted (u4FsMIFsbContextId) != FSB_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_FSB_ERR_MODULE_SHUTDOWN);
        return SNMP_FAILURE;
    }

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);
    if (pFsbFipSnoopingEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_FSB_ERR_NO_FCOE_VLAN);
        return SNMP_FAILURE;
    }

    /* FIPSnoopingFcmap value cannot be configured, if the FC-MAP mode
     * is VLAN, and Module Status is Enabled and the Vlan Enabled Status
     * is also Enabled.
     * For further understanding, refer the Table 1 */
    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if ((pFsbContextInfo->u1FcMapMode == FSB_FCMAP_VLAN) &&
        (pFsbContextInfo->u1ModuleStatus == FSB_ENABLE) &&
        (pFsbFipSnoopingEntry->u1EnabledStatus == FSB_ENABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_FSB_ERR_MODULE_ENABLED);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsbFIPSnoopingEnabledStatus
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                testValFsMIFsbFIPSnoopingEnabledStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsbFIPSnoopingEnabledStatus (UINT4 *pu4ErrorCode,
                                          UINT4 u4FsMIFsbContextId,
                                          INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                          INT4
                                          i4TestValFsMIFsbFIPSnoopingEnabledStatus)
{
    if ((i4TestValFsMIFsbFIPSnoopingEnabledStatus != FSB_DISABLE) &&
        (i4TestValFsMIFsbFIPSnoopingEnabledStatus != FSB_ENABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (FsbUtilIsFsbStarted (u4FsMIFsbContextId) != FSB_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_FSB_ERR_MODULE_SHUTDOWN);
        return SNMP_FAILURE;
    }

    if (FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                (UINT2) i4FsMIFsbFIPSnoopingVlanIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_FSB_ERR_NO_FCOE_VLAN);
        return SNMP_FAILURE;
    }

    /* This validation is to ensure the following set of conditions, 
     * 1) Vlan has member ports 
     * 2) The Oper Status of the ports are up */
    if (FsbValidatePortEntryInFcoeVlan (pu4ErrorCode,
                                        u4FsMIFsbContextId,
                                        (UINT2) i4FsMIFsbFIPSnoopingVlanIndex)
        != FSB_SUCCESS)
    {
        /* Error Code will be set in the above function */
        return SNMP_FAILURE;;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsbFIPSnoopingRowStatus
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                testValFsMIFsbFIPSnoopingRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsbFIPSnoopingRowStatus (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsMIFsbContextId,
                                      INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                      INT4 i4TestValFsMIFsbFIPSnoopingRowStatus)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    UINT4               u4Count = 0;

    if (FsbUtilIsFsbStarted (u4FsMIFsbContextId) != FSB_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_FSB_ERR_MODULE_SHUTDOWN);
        return SNMP_FAILURE;
    }

    switch (i4TestValFsMIFsbFIPSnoopingRowStatus)
    {
        case CREATE_AND_GO:
            /* VLAN active status is checked to ensure that 
             * ports are mapped to it */
            if (FsbPortIsVlanActive (u4FsMIFsbContextId,
                                     (UINT2) i4FsMIFsbFIPSnoopingVlanIndex) !=
                FSB_TRUE)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_FSB_ERR_VLAN_NOT_ACTIVE);
                return SNMP_FAILURE;
            }
            pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                           (UINT2)
                                                           i4FsMIFsbFIPSnoopingVlanIndex);
            /* If FIP-snooping is present and the same entry is being created,
             * then return FAILURE */
            if (pFsbFipSnoopingEntry != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            /* Check if MAX no of FCoE VLAN that can be configured is exceeded */
            if (RBTreeCount (gFsbGlobals.FsbFipSnoopingTable, &u4Count) !=
                RB_SUCCESS)
            {
                FSB_CONTEXT_TRC (u4FsMIFsbContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                 "nmhTestv2FsMIFsbFIPSnoopingRowStatus : RBTreeCount failed\r\n");
                return SNMP_FAILURE;
            }
            if (u4Count == MAX_FSB_FCOE_VLAN_ENTRIES)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_FSB_ERR_MAX_FCOE_VLAN_EXCEEDS);
                return SNMP_FAILURE;
            }
            break;

        case DESTROY:
            pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                           (UINT2)
                                                           i4FsMIFsbFIPSnoopingVlanIndex);
            if (pFsbFipSnoopingEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_FSB_ERR_NO_FCOE_VLAN);
                return SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsbFIPSnoopingPinnedPorts
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                testValFsMIFsbFIPSnoopingPinnedPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsbFIPSnoopingPinnedPorts (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsMIFsbContextId,
                                        INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValFsMIFsbFIPSnoopingPinnedPorts)
{
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    UINT4               u4Index = 0;
    BOOL1               bRetVal = OSIX_FALSE;
    UINT4               u4PortChannelIndex = 0;
#ifdef MBSM_WANTED
    UINT4               u4LocalPortCount = 0;
    UINT4               u4RemotePortCount = 0;
    UINT4               u4SlotId = MBSM_INVALID_SLOT_ID;
#endif
    UINT2               u2AggId = 0;

    if (FsbUtilIsFsbStarted (u4FsMIFsbContextId) != FSB_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_FSB_ERR_MODULE_SHUTDOWN);
        return SNMP_FAILURE;
    }

    if (pTestValFsMIFsbFIPSnoopingPinnedPorts->i4_Length == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);
    if (pFsbFipSnoopingEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_FSB_ERR_NO_FCOE_VLAN);
        return SNMP_FAILURE;
    }
    for (u4Index = 1; u4Index < SYS_DEF_MAX_PHYSICAL_INTERFACES; u4Index++)
    {
        OSIX_BITLIST_IS_BIT_SET (pTestValFsMIFsbFIPSnoopingPinnedPorts->
                                 pu1_OctetList, u4Index, BRG_PORT_LIST_SIZE,
                                 bRetVal);
        if (bRetVal == OSIX_TRUE)
        {
            if (FsbPortIsPortInPortChannel (u4Index) == FSB_SUCCESS)
            {
                if (FsbPortL2IwfGetPortChannelForPort (u4Index, &u2AggId)
                    == FSB_SUCCESS)
                {
                    u4PortChannelIndex = u2AggId;
                }
                pFsbIntfEntry =
                    FsbGetFsbIntfEntry ((UINT2) i4FsMIFsbFIPSnoopingVlanIndex,
                                        u4PortChannelIndex);
            }
            else
            {
                pFsbIntfEntry =
                    FsbGetFsbIntfEntry ((UINT2) i4FsMIFsbFIPSnoopingVlanIndex,
                                        u4Index);
            }
            if (pFsbIntfEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_FSB_ERR_NO_FCOE_PORT);
                return SNMP_FAILURE;
            }
#ifdef MBSM_WANTED
            u4SlotId = MBSM_INVALID_SLOT_ID;

            MbsmGetSlotIdFromRemotePort (u4Index, &u4SlotId);
            if (u4SlotId == (UINT4) IssGetSwitchid ())
            {
                u4LocalPortCount++;
            }
            else
            {
                u4RemotePortCount++;
            }
#endif
        }
    }
#ifdef MBSM_WANTED
    if ((u4LocalPortCount != 0) && (u4RemotePortCount != 0))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_FSB_ERR_INVALID_PINNED_PORTS);
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFsbFIPSnoopingTable
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsbFIPSnoopingTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIFsbIntfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsbIntfTable
 Input       :  The Indices
                FsMIFsbIntfVlanIndex
                FsMIFsbIntfIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsbIntfTable (INT4 i4FsMIFsbIntfVlanIndex,
                                          INT4 i4FsMIFsbIntfIfIndex)
{
    if (FsbGetFsbIntfEntry ((UINT2) i4FsMIFsbIntfVlanIndex,
                            (UINT4) i4FsMIFsbIntfIfIndex) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsbIntfTable
 Input       :  The Indices
                FsMIFsbIntfVlanIndex
                FsMIFsbIntfIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsbIntfTable (INT4 *pi4FsMIFsbIntfVlanIndex,
                                  INT4 *pi4FsMIFsbIntfIfIndex)
{
    tFsbIntfEntry      *pFsbIntfEntry = NULL;

    pFsbIntfEntry = (tFsbIntfEntry *) RBTreeGetFirst (gFsbGlobals.FsbIntfTable);

    if (pFsbIntfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4FsMIFsbIntfVlanIndex = (INT4) (pFsbIntfEntry->u2VlanId);
    *pi4FsMIFsbIntfIfIndex = (INT4) (pFsbIntfEntry->u4IfIndex);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsbIntfTable
 Input       :  The Indices
                FsMIFsbIntfVlanIndex
                nextFsMIFsbIntfVlanIndex
                FsMIFsbIntfIfIndex
                nextFsMIFsbIntfIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsbIntfTable (INT4 i4FsMIFsbIntfVlanIndex,
                                 INT4 *pi4NextFsMIFsbIntfVlanIndex,
                                 INT4 i4FsMIFsbIntfIfIndex,
                                 INT4 *pi4NextFsMIFsbIntfIfIndex)
{
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbIntfEntry       FsbIntfEntry;

    FSB_MEMSET (&FsbIntfEntry, 0, sizeof (tFsbIntfEntry));

    FsbIntfEntry.u2VlanId = (UINT2) i4FsMIFsbIntfVlanIndex;
    FsbIntfEntry.u4IfIndex = (UINT4) i4FsMIFsbIntfIfIndex;

    pFsbIntfEntry = (tFsbIntfEntry *)
        RBTreeGetNext (gFsbGlobals.FsbIntfTable,
                       (tRBElem *) & FsbIntfEntry, NULL);

    if (pFsbIntfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsMIFsbIntfVlanIndex = (INT4) (pFsbIntfEntry->u2VlanId);
    *pi4NextFsMIFsbIntfIfIndex = (INT4) (pFsbIntfEntry->u4IfIndex);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsbIntfPortRole
 Input       :  The Indices
                FsMIFsbIntfVlanIndex
                FsMIFsbIntfIfIndex

                The Object 
                retValFsMIFsbIntfPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbIntfPortRole (INT4 i4FsMIFsbIntfVlanIndex,
                           INT4 i4FsMIFsbIntfIfIndex,
                           INT4 *pi4RetValFsMIFsbIntfPortRole)
{
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbIntfEntry       FsbIntfEntry;
    FSB_MEMSET (&FsbIntfEntry, 0, sizeof (tFsbIntfEntry));

    FsbIntfEntry.u2VlanId = (UINT2) i4FsMIFsbIntfVlanIndex;
    FsbIntfEntry.u4IfIndex = (UINT4) i4FsMIFsbIntfIfIndex;

    pFsbIntfEntry = RBTreeGet (gFsbGlobals.FsbIntfTable,
                               (tRBElem *) & FsbIntfEntry);

    if (pFsbIntfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsMIFsbIntfPortRole = (INT4) (pFsbIntfEntry->u1PortRole);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbIntfRowStatus
 Input       :  The Indices
                FsMIFsbIntfVlanIndex
                FsMIFsbIntfIfIndex

                The Object 
                retValFsMIFsbIntfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbIntfRowStatus (INT4 i4FsMIFsbIntfVlanIndex,
                            INT4 i4FsMIFsbIntfIfIndex,
                            INT4 *pi4RetValFsMIFsbIntfRowStatus)
{
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbIntfEntry       FsbIntfEntry;
    FSB_MEMSET (&FsbIntfEntry, 0, sizeof (tFsbIntfEntry));

    FsbIntfEntry.u2VlanId = (UINT2) i4FsMIFsbIntfVlanIndex;
    FsbIntfEntry.u4IfIndex = (UINT4) i4FsMIFsbIntfIfIndex;

    pFsbIntfEntry = RBTreeGet (gFsbGlobals.FsbIntfTable,
                               (tRBElem *) & FsbIntfEntry);

    if (pFsbIntfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsMIFsbIntfRowStatus = (INT4) (pFsbIntfEntry->u1RowStatus);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIFsbIntfPortRole
 Input       :  The Indices
                FsMIFsbIntfVlanIndex
                FsMIFsbIntfIfIndex

                The Object 
                setValFsMIFsbIntfPortRole
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsbIntfPortRole (INT4 i4FsMIFsbIntfVlanIndex,
                           INT4 i4FsMIFsbIntfIfIndex,
                           INT4 i4SetValFsMIFsbIntfPortRole)
{
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    UINT1               u1PrevPortRole = 0;

    pFsbIntfEntry = FsbGetFsbIntfEntry ((UINT2) i4FsMIFsbIntfVlanIndex,
                                        (UINT4) i4FsMIFsbIntfIfIndex);
    if (pFsbIntfEntry != NULL)
    {
        if (pFsbIntfEntry->u1PortRole == (UINT1) i4SetValFsMIFsbIntfPortRole)
        {
            /* If the port role is already set then return
             * success */
            return SNMP_SUCCESS;
        }

        u1PrevPortRole = pFsbIntfEntry->u1PortRole;
        pFsbIntfEntry->u1PortRole = (UINT1) i4SetValFsMIFsbIntfPortRole;
        if (FsbHandlePortRoleChange (pFsbIntfEntry, u1PrevPortRole)
            != FSB_SUCCESS)
        {
            /* If there is failure is updateing the port role,
             * assign it to the previous port role */
            pFsbIntfEntry->u1PortRole = u1PrevPortRole;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsbIntfRowStatus
 Input       :  The Indices
                FsMIFsbIntfVlanIndex
                FsMIFsbIntfIfIndex

                The Object 
                setValFsMIFsbIntfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsbIntfRowStatus (INT4 i4FsMIFsbIntfVlanIndex,
                            INT4 i4FsMIFsbIntfIfIndex,
                            INT4 i4SetValFsMIFsbIntfRowStatus)
{

    /* Changing the port role is the only configuration allowed.
     * The Administrator cannot configure the row status */
    UNUSED_PARAM (i4FsMIFsbIntfVlanIndex);
    UNUSED_PARAM (i4FsMIFsbIntfIfIndex);
    UNUSED_PARAM (i4SetValFsMIFsbIntfRowStatus);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIFsbIntfPortRole
 Input       :  The Indices
                FsMIFsbIntfVlanIndex
                FsMIFsbIntfIfIndex

                The Object 
                testValFsMIFsbIntfPortRole
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsbIntfPortRole (UINT4 *pu4ErrorCode,
                              INT4 i4FsMIFsbIntfVlanIndex,
                              INT4 i4FsMIFsbIntfIfIndex,
                              INT4 i4TestValFsMIFsbIntfPortRole)
{
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    UINT4               u4ContextId = 0;

    if ((i4TestValFsMIFsbIntfPortRole != FSB_ENODE_FACING) &&
        (i4TestValFsMIFsbIntfPortRole != FSB_FCF_FACING) &&
        (i4TestValFsMIFsbIntfPortRole != FSB_ENODE_FCF_FACING))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Get Context Id from Interface Index and Check If Fip-snooping
     * System Control is started for the Context retreived */
    if (FsbGetContextIdFromCfaIfIndex ((UINT4) i4FsMIFsbIntfIfIndex,
                                       &u4ContextId) != FSB_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_FSB_ERR_NO_CONTEXT_MAPPED_TO_PORT);
        return SNMP_FAILURE;
    }

    if (FsbUtilIsFsbStarted (u4ContextId) != FSB_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_FSB_ERR_MODULE_SHUTDOWN);
        return SNMP_FAILURE;
    }

    pFsbIntfEntry = FsbGetFsbIntfEntry ((UINT2) i4FsMIFsbIntfVlanIndex,
                                        (UINT4) i4FsMIFsbIntfIfIndex);
    if (pFsbIntfEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_FSB_ERR_NO_FCOE_PORT);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsbIntfRowStatus
 Input       :  The Indices
                FsMIFsbIntfVlanIndex
                FsMIFsbIntfIfIndex

                The Object 
                testValFsMIFsbIntfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsbIntfRowStatus (UINT4 *pu4ErrorCode,
                               INT4 i4FsMIFsbIntfVlanIndex,
                               INT4 i4FsMIFsbIntfIfIndex,
                               INT4 i4TestValFsMIFsbIntfRowStatus)
{
    /* Changing the port role is the only configuration allowed.
     * The Administrator cannot configure the row status */
    switch (i4TestValFsMIFsbIntfRowStatus)
    {
        case ACTIVE:
        case NOT_IN_SERVICE:
        case NOT_READY:
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
        case DESTROY:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;
    }
    UNUSED_PARAM (i4FsMIFsbIntfVlanIndex);
    UNUSED_PARAM (i4FsMIFsbIntfIfIndex);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFsbIntfTable
 Input       :  The Indices
                FsMIFsbIntfVlanIndex
                FsMIFsbIntfIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsbIntfTable (UINT4 *pu4ErrorCode,
                          tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIFsbFIPSessionTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsbFIPSessionTable
 Input       :  The Indices
                FsMIFsbFIPSessionVlanId
                FsMIFsbFIPSessionEnodeIfIndex
                FsMIFsbFIPSessionEnodeMacAddress
                FsMIFsbFIPSessionFcfMacAddress
                FsMIFsbFIPSessionFCoEMacAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsbFIPSessionTable (INT4 i4FsMIFsbFIPSessionVlanId,
                                                INT4
                                                i4FsMIFsbFIPSessionEnodeIfIndex,
                                                tMacAddr
                                                FsMIFsbFIPSessionEnodeMacAddress,
                                                tMacAddr
                                                FsMIFsbFIPSessionFcfMacAddress,
                                                tMacAddr
                                                FsMIFsbFIPSessionFCoEMacAddress)
{
    if (FsbGetFIPSessionFCoEEntry ((UINT2) i4FsMIFsbFIPSessionVlanId,
                                   (UINT4) i4FsMIFsbFIPSessionEnodeIfIndex,
                                   FsMIFsbFIPSessionEnodeMacAddress,
                                   FsMIFsbFIPSessionFcfMacAddress,
                                   FsMIFsbFIPSessionFCoEMacAddress) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsbFIPSessionTable
 Input       :  The Indices
                FsMIFsbFIPSessionVlanId
                FsMIFsbFIPSessionEnodeIfIndex
                FsMIFsbFIPSessionEnodeMacAddress
                FsMIFsbFIPSessionFcfMacAddress
                FsMIFsbFIPSessionFCoEMacAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsbFIPSessionTable (INT4 *pi4FsMIFsbFIPSessionVlanId,
                                        INT4 *pi4FsMIFsbFIPSessionEnodeIfIndex,
                                        tMacAddr *
                                        pFsMIFsbFIPSessionEnodeMacAddress,
                                        tMacAddr *
                                        pFsMIFsbFIPSessionFcfMacAddress,
                                        tMacAddr *
                                        pFsMIFsbFIPSessionFCoEMacAddress)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;

    pFsbFipSessFCoEEntry = (tFsbFipSessFCoEEntry *)
        RBTreeGetFirst (gFsbGlobals.FsbFipSessFCoETable);
    if (pFsbFipSessFCoEEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4FsMIFsbFIPSessionEnodeIfIndex =
        (INT4) (pFsbFipSessFCoEEntry->u4ENodeIfIndex);
    *pi4FsMIFsbFIPSessionVlanId = (INT4) (pFsbFipSessFCoEEntry->u2VlanId);
    FSB_MEMCPY (pFsMIFsbFIPSessionEnodeMacAddress,
                pFsbFipSessFCoEEntry->ENodeMacAddr, MAC_ADDR_LEN);
    FSB_MEMCPY (pFsMIFsbFIPSessionFcfMacAddress,
                pFsbFipSessFCoEEntry->FcfMacAddr, MAC_ADDR_LEN);
    FSB_MEMCPY (pFsMIFsbFIPSessionFCoEMacAddress,
                pFsbFipSessFCoEEntry->FCoEMacAddr, MAC_ADDR_LEN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsbFIPSessionTable
 Input       :  The Indices
                FsMIFsbFIPSessionVlanId
                nextFsMIFsbFIPSessionVlanId
                FsMIFsbFIPSessionEnodeIfIndex
                nextFsMIFsbFIPSessionEnodeIfIndex
                FsMIFsbFIPSessionEnodeMacAddress
                nextFsMIFsbFIPSessionEnodeMacAddress
                FsMIFsbFIPSessionFcfMacAddress
                nextFsMIFsbFIPSessionFcfMacAddress
                FsMIFsbFIPSessionFCoEMacAddress
                nextFsMIFsbFIPSessionFCoEMacAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsbFIPSessionTable (INT4 i4FsMIFsbFIPSessionVlanId,
                                       INT4 *pi4NextFsMIFsbFIPSessionVlanId,
                                       INT4 i4FsMIFsbFIPSessionEnodeIfIndex,
                                       INT4
                                       *pi4NextFsMIFsbFIPSessionEnodeIfIndex,
                                       tMacAddr
                                       FsMIFsbFIPSessionEnodeMacAddress,
                                       tMacAddr *
                                       pNextFsMIFsbFIPSessionEnodeMacAddress,
                                       tMacAddr FsMIFsbFIPSessionFcfMacAddress,
                                       tMacAddr *
                                       pNextFsMIFsbFIPSessionFcfMacAddress,
                                       tMacAddr FsMIFsbFIPSessionFCoEMacAddress,
                                       tMacAddr *
                                       pNextFsMIFsbFIPSessionFCoEMacAddress)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;
    tFsbFipSessFCoEEntry FsbFipSessFCoEEntry;

    FSB_MEMSET (&FsbFipSessFCoEEntry, 0, sizeof (tFsbFipSessFCoEEntry));

    FsbFipSessFCoEEntry.u2VlanId = (UINT2) i4FsMIFsbFIPSessionVlanId;
    FsbFipSessFCoEEntry.u4ENodeIfIndex =
        (UINT4) i4FsMIFsbFIPSessionEnodeIfIndex;
    FSB_MEMCPY (FsbFipSessFCoEEntry.ENodeMacAddr,
                FsMIFsbFIPSessionEnodeMacAddress, MAC_ADDR_LEN);
    FSB_MEMCPY (FsbFipSessFCoEEntry.FcfMacAddr, FsMIFsbFIPSessionFcfMacAddress,
                MAC_ADDR_LEN);
    FSB_MEMCPY (FsbFipSessFCoEEntry.FCoEMacAddr,
                FsMIFsbFIPSessionFCoEMacAddress, MAC_ADDR_LEN);

    pFsbFipSessFCoEEntry = (tFsbFipSessFCoEEntry *)
        RBTreeGetNext (gFsbGlobals.FsbFipSessFCoETable,
                       (tRBElem *) & FsbFipSessFCoEEntry, NULL);

    if (pFsbFipSessFCoEEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4NextFsMIFsbFIPSessionVlanId = (INT4) (pFsbFipSessFCoEEntry->u2VlanId);
    *pi4NextFsMIFsbFIPSessionEnodeIfIndex =
        (INT4) (pFsbFipSessFCoEEntry->u4ENodeIfIndex);
    FSB_MEMCPY (pNextFsMIFsbFIPSessionEnodeMacAddress,
                pFsbFipSessFCoEEntry->ENodeMacAddr, MAC_ADDR_LEN);
    FSB_MEMCPY (pNextFsMIFsbFIPSessionFcfMacAddress,
                pFsbFipSessFCoEEntry->FcfMacAddr, MAC_ADDR_LEN);
    FSB_MEMCPY (pNextFsMIFsbFIPSessionFCoEMacAddress,
                pFsbFipSessFCoEEntry->FCoEMacAddr, MAC_ADDR_LEN);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsbFIPSessionFcMap
 Input       :  The Indices
                FsMIFsbFIPSessionVlanId
                FsMIFsbFIPSessionEnodeIfIndex
                FsMIFsbFIPSessionEnodeMacAddress
                FsMIFsbFIPSessionFcfMacAddress
                FsMIFsbFIPSessionFCoEMacAddress

                The Object 
                retValFsMIFsbFIPSessionFcMap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbFIPSessionFcMap (INT4 i4FsMIFsbFIPSessionVlanId,
                              INT4 i4FsMIFsbFIPSessionEnodeIfIndex,
                              tMacAddr FsMIFsbFIPSessionEnodeMacAddress,
                              tMacAddr FsMIFsbFIPSessionFcfMacAddress,
                              tMacAddr FsMIFsbFIPSessionFCoEMacAddress,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsMIFsbFIPSessionFcMap)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;

    pFsbFipSessFCoEEntry =
        FsbGetFIPSessionFCoEEntry ((UINT2) i4FsMIFsbFIPSessionVlanId,
                                   (UINT4) i4FsMIFsbFIPSessionEnodeIfIndex,
                                   FsMIFsbFIPSessionEnodeMacAddress,
                                   FsMIFsbFIPSessionFcfMacAddress,
                                   FsMIFsbFIPSessionFCoEMacAddress);

    if (pFsbFipSessFCoEEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRetValFsMIFsbFIPSessionFcMap->i4_Length = FSB_FCMAP_LEN;
    FSB_MEMCPY (pRetValFsMIFsbFIPSessionFcMap->pu1_OctetList,
                pFsbFipSessFCoEEntry->pFsbFipSessEntry->au1FcMap,
                pRetValFsMIFsbFIPSessionFcMap->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbFIPSessionFcfIfIndex
 Input       :  The Indices
                FsMIFsbFIPSessionVlanId
                FsMIFsbFIPSessionEnodeIfIndex
                FsMIFsbFIPSessionEnodeMacAddress
                FsMIFsbFIPSessionFcfMacAddress
                FsMIFsbFIPSessionFCoEMacAddress

                The Object 
                retValFsMIFsbFIPSessionFcfIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbFIPSessionFcfIfIndex (INT4 i4FsMIFsbFIPSessionVlanId,
                                   INT4 i4FsMIFsbFIPSessionEnodeIfIndex,
                                   tMacAddr FsMIFsbFIPSessionEnodeMacAddress,
                                   tMacAddr FsMIFsbFIPSessionFcfMacAddress,
                                   tMacAddr FsMIFsbFIPSessionFCoEMacAddress,
                                   INT4 *pi4RetValFsMIFsbFIPSessionFcfIfIndex)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;

    pFsbFipSessFCoEEntry =
        FsbGetFIPSessionFCoEEntry ((UINT2) i4FsMIFsbFIPSessionVlanId,
                                   (UINT4) i4FsMIFsbFIPSessionEnodeIfIndex,
                                   FsMIFsbFIPSessionEnodeMacAddress,
                                   FsMIFsbFIPSessionFcfMacAddress,
                                   FsMIFsbFIPSessionFCoEMacAddress);

    if (pFsbFipSessFCoEEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsMIFsbFIPSessionFcfIfIndex =
        (INT4) pFsbFipSessFCoEEntry->pFsbFipSessEntry->u4FcfIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbFIPSessionFcfNameId
 Input       :  The Indices
                FsMIFsbFIPSessionVlanId
                FsMIFsbFIPSessionEnodeIfIndex
                FsMIFsbFIPSessionEnodeMacAddress
                FsMIFsbFIPSessionFcfMacAddress
                FsMIFsbFIPSessionFCoEMacAddress

                The Object 
                retValFsMIFsbFIPSessionFcfNameId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbFIPSessionFcfNameId (INT4 i4FsMIFsbFIPSessionVlanId,
                                  INT4 i4FsMIFsbFIPSessionEnodeIfIndex,
                                  tMacAddr FsMIFsbFIPSessionEnodeMacAddress,
                                  tMacAddr FsMIFsbFIPSessionFcfMacAddress,
                                  tMacAddr FsMIFsbFIPSessionFCoEMacAddress,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsMIFsbFIPSessionFcfNameId)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEEntry = NULL;

    pFsbFipSessFCoEEEntry =
        FsbGetFIPSessionFCoEEntry ((UINT2) i4FsMIFsbFIPSessionVlanId,
                                   (UINT4) i4FsMIFsbFIPSessionEnodeIfIndex,
                                   FsMIFsbFIPSessionEnodeMacAddress,
                                   FsMIFsbFIPSessionFcfMacAddress,
                                   FsMIFsbFIPSessionFCoEMacAddress);

    if (pFsbFipSessFCoEEEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRetValFsMIFsbFIPSessionFcfNameId->i4_Length = FSB_NAME_ID_LEN;
    FSB_MEMCPY (pRetValFsMIFsbFIPSessionFcfNameId->pu1_OctetList,
                pFsbFipSessFCoEEEntry->pFsbFipSessEntry->au1NameId,
                pRetValFsMIFsbFIPSessionFcfNameId->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbFIPSessionFcId
 Input       :  The Indices
                FsMIFsbFIPSessionVlanId
                FsMIFsbFIPSessionEnodeIfIndex
                FsMIFsbFIPSessionEnodeMacAddress
                FsMIFsbFIPSessionFcfMacAddress
                FsMIFsbFIPSessionFCoEMacAddress

                The Object 
                retValFsMIFsbFIPSessionFcId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbFIPSessionFcId (INT4 i4FsMIFsbFIPSessionVlanId,
                             INT4 i4FsMIFsbFIPSessionEnodeIfIndex,
                             tMacAddr FsMIFsbFIPSessionEnodeMacAddress,
                             tMacAddr FsMIFsbFIPSessionFcfMacAddress,
                             tMacAddr FsMIFsbFIPSessionFCoEMacAddress,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsMIFsbFIPSessionFcId)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;

    pFsbFipSessFCoEEntry =
        FsbGetFIPSessionFCoEEntry ((UINT2) i4FsMIFsbFIPSessionVlanId,
                                   (UINT4) i4FsMIFsbFIPSessionEnodeIfIndex,
                                   FsMIFsbFIPSessionEnodeMacAddress,
                                   FsMIFsbFIPSessionFcfMacAddress,
                                   FsMIFsbFIPSessionFCoEMacAddress);

    if (pFsbFipSessFCoEEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRetValFsMIFsbFIPSessionFcId->i4_Length = FSB_FCID_LEN;

    FSB_MEMCPY (pRetValFsMIFsbFIPSessionFcId->pu1_OctetList,
                pFsbFipSessFCoEEntry->au1FcfId,
                pRetValFsMIFsbFIPSessionFcId->i4_Length);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbFIPSessionEnodeConnectType
 Input       :  The Indices
                FsMIFsbFIPSessionVlanId
                FsMIFsbFIPSessionEnodeIfIndex
                FsMIFsbFIPSessionEnodeMacAddress
                FsMIFsbFIPSessionFcfMacAddress
                FsMIFsbFIPSessionFCoEMacAddress

                The Object 
                retValFsMIFsbFIPSessionEnodeConnectType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbFIPSessionEnodeConnectType (INT4 i4FsMIFsbFIPSessionVlanId,
                                         INT4 i4FsMIFsbFIPSessionEnodeIfIndex,
                                         tMacAddr
                                         FsMIFsbFIPSessionEnodeMacAddress,
                                         tMacAddr
                                         FsMIFsbFIPSessionFcfMacAddress,
                                         tMacAddr
                                         FsMIFsbFIPSessionFCoEMacAddress,
                                         INT4
                                         *pi4RetValFsMIFsbFIPSessionEnodeConnectType)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;

    pFsbFipSessFCoEEntry =
        FsbGetFIPSessionFCoEEntry ((UINT2) i4FsMIFsbFIPSessionVlanId,
                                   (UINT4) i4FsMIFsbFIPSessionEnodeIfIndex,
                                   FsMIFsbFIPSessionEnodeMacAddress,
                                   FsMIFsbFIPSessionFcfMacAddress,
                                   FsMIFsbFIPSessionFCoEMacAddress);

    if (pFsbFipSessFCoEEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsMIFsbFIPSessionEnodeConnectType =
        pFsbFipSessFCoEEntry->u1EnodeConnType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbFIPSessionHouseKeepingTimerStatus
 Input       :  The Indices
                FsMIFsbFIPSessionVlanId
                FsMIFsbFIPSessionEnodeIfIndex
                FsMIFsbFIPSessionEnodeMacAddress
                FsMIFsbFIPSessionFcfMacAddress
                FsMIFsbFIPSessionFCoEMacAddress

                The Object 
                retValFsMIFsbFIPSessionHouseKeepingTimerStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbFIPSessionHouseKeepingTimerStatus (INT4 i4FsMIFsbFIPSessionVlanId,
                                                INT4
                                                i4FsMIFsbFIPSessionEnodeIfIndex,
                                                tMacAddr
                                                FsMIFsbFIPSessionEnodeMacAddress,
                                                tMacAddr
                                                FsMIFsbFIPSessionFcfMacAddress,
                                                tMacAddr
                                                FsMIFsbFIPSessionFCoEMacAddress,
                                                INT4
                                                *pi4RetValFsMIFsbFIPSessionHouseKeepingTimerStatus)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;

    pFsbFipSessFCoEEntry =
        FsbGetFIPSessionFCoEEntry ((UINT2) i4FsMIFsbFIPSessionVlanId,
                                   (UINT4) i4FsMIFsbFIPSessionEnodeIfIndex,
                                   FsMIFsbFIPSessionEnodeMacAddress,
                                   FsMIFsbFIPSessionFcfMacAddress,
                                   FsMIFsbFIPSessionFCoEMacAddress);

    if (pFsbFipSessFCoEEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsMIFsbFIPSessionHouseKeepingTimerStatus =
        pFsbFipSessFCoEEntry->u1HouseKeepingTimerStatus;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIFsbFcfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsbFcfTable
 Input       :  The Indices
                FsMIFsbFcfVlanId
                FsMIFsbFcfIfIndex
                FsMIFsbFcfMacAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsbFcfTable (INT4 i4FsMIFsbFcfVlanId,
                                         INT4 i4FsMIFsbFcfIfIndex,
                                         tMacAddr FsMIFsbFcfMacAddress)
{
    if (FsbGetFsbFcfEntry
        ((UINT2) i4FsMIFsbFcfVlanId, (UINT4) i4FsMIFsbFcfIfIndex,
         FsMIFsbFcfMacAddress) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsbFcfTable
 Input       :  The Indices
                FsMIFsbFcfVlanId
                FsMIFsbFcfIfIndex
                FsMIFsbFcfMacAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsbFcfTable (INT4 *pi4FsMIFsbFcfVlanId,
                                 INT4 *pi4FsMIFsbFcfIfIndex,
                                 tMacAddr * pFsMIFsbFcfMacAddress)
{
    tFsbFcfEntry       *pFsbFcfEntry = NULL;

    pFsbFcfEntry = (tFsbFcfEntry *) RBTreeGetFirst (gFsbGlobals.FsbFcfTable);

    if (pFsbFcfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4FsMIFsbFcfVlanId = (INT4) (pFsbFcfEntry->u2VlanId);
    *pi4FsMIFsbFcfIfIndex = (INT4) (pFsbFcfEntry->u4FcfIfIndex);
    FSB_MEMCPY (pFsMIFsbFcfMacAddress, pFsbFcfEntry->FcfMacAddr, MAC_ADDR_LEN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsbFcfTable
 Input       :  The Indices
                FsMIFsbFcfVlanId
                nextFsMIFsbFcfVlanId
                FsMIFsbFcfIfIndex
                nextFsMIFsbFcfIfIndex
                FsMIFsbFcfMacAddress
                nextFsMIFsbFcfMacAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsbFcfTable (INT4 i4FsMIFsbFcfVlanId,
                                INT4 *pi4NextFsMIFsbFcfVlanId,
                                INT4 i4FsMIFsbFcfIfIndex,
                                INT4 *pi4NextFsMIFsbFcfIfIndex,
                                tMacAddr FsMIFsbFcfMacAddress,
                                tMacAddr * pNextFsMIFsbFcfMacAddress)
{
    tFsbFcfEntry       *pFsbFcfEntry = NULL;
    tFsbFcfEntry        FsbFcfEntry;

    FSB_MEMSET (&FsbFcfEntry, 0, sizeof (FsbFcfEntry));

    FsbFcfEntry.u2VlanId = (UINT2) i4FsMIFsbFcfVlanId;
    FsbFcfEntry.u4FcfIfIndex = (UINT4) i4FsMIFsbFcfIfIndex;
    FSB_MEMCPY (&FsbFcfEntry.FcfMacAddr, FsMIFsbFcfMacAddress, MAC_ADDR_LEN);

    pFsbFcfEntry = (tFsbFcfEntry *)
        RBTreeGetNext (gFsbGlobals.FsbFcfTable,
                       (tRBElem *) & FsbFcfEntry, NULL);

    if (pFsbFcfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsMIFsbFcfVlanId = (INT4) (pFsbFcfEntry->u2VlanId);
    *pi4NextFsMIFsbFcfIfIndex = (INT4) (pFsbFcfEntry->u4FcfIfIndex);
    FSB_MEMCPY (pNextFsMIFsbFcfMacAddress, pFsbFcfEntry->FcfMacAddr,
                MAC_ADDR_LEN);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsbFcfFcMap
 Input       :  The Indices
                FsMIFsbFcfVlanId
                FsMIFsbFcfIfIndex
                FsMIFsbFcfMacAddress

                The Object 
                retValFsMIFsbFcfFcMap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbFcfFcMap (INT4 i4FsMIFsbFcfVlanId,
                       INT4 i4FsMIFsbFcfIfIndex,
                       tMacAddr FsMIFsbFcfMacAddress,
                       tSNMP_OCTET_STRING_TYPE * pRetValFsMIFsbFcfFcMap)
{
    tFsbFcfEntry       *pFsbFcfEntry = NULL;

    pFsbFcfEntry = FsbGetFsbFcfEntry ((UINT2) i4FsMIFsbFcfVlanId,
                                      (UINT4) i4FsMIFsbFcfIfIndex,
                                      FsMIFsbFcfMacAddress);

    if (pFsbFcfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRetValFsMIFsbFcfFcMap->i4_Length = sizeof (pFsbFcfEntry->au1FcMap);
    FSB_MEMCPY (pRetValFsMIFsbFcfFcMap->pu1_OctetList,
                pFsbFcfEntry->au1FcMap, pRetValFsMIFsbFcfFcMap->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbFcfAddressingMode
 Input       :  The Indices
                FsMIFsbFcfVlanId
                FsMIFsbFcfIfIndex
                FsMIFsbFcfMacAddress

                The Object 
                retValFsMIFsbFcfAddressingMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbFcfAddressingMode (INT4 i4FsMIFsbFcfVlanId,
                                INT4 i4FsMIFsbFcfIfIndex,
                                tMacAddr FsMIFsbFcfMacAddress,
                                INT4 *pi4RetValFsMIFsbFcfAddressingMode)
{
    tFsbFcfEntry       *pFsbFcfEntry = NULL;

    pFsbFcfEntry = FsbGetFsbFcfEntry ((UINT2) i4FsMIFsbFcfVlanId,
                                      (UINT4) i4FsMIFsbFcfIfIndex,
                                      FsMIFsbFcfMacAddress);

    if (pFsbFcfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsMIFsbFcfAddressingMode =
        (INT4) (pFsbFcfEntry->u1AddressingMode);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbFcfEnodeLoginCount
 Input       :  The Indices
                FsMIFsbFcfVlanId
                FsMIFsbFcfIfIndex
                FsMIFsbFcfMacAddress

                The Object 
                retValFsMIFsbFcfEnodeLoginCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbFcfEnodeLoginCount (INT4 i4FsMIFsbFcfVlanId,
                                 INT4 i4FsMIFsbFcfIfIndex,
                                 tMacAddr FsMIFsbFcfMacAddress,
                                 INT4 *pi4RetValFsMIFsbFcfEnodeLoginCount)
{
    tFsbFcfEntry       *pFsbFcfEntry = NULL;

    pFsbFcfEntry = FsbGetFsbFcfEntry ((UINT2) i4FsMIFsbFcfVlanId,
                                      (UINT4) i4FsMIFsbFcfIfIndex,
                                      FsMIFsbFcfMacAddress);

    if (pFsbFcfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsMIFsbFcfEnodeLoginCount =
        (INT4) (pFsbFcfEntry->u4EnodeLoginCount);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbFcfNameId
 Input       :  The Indices
                FsMIFsbFcfVlanId
                FsMIFsbFcfIfIndex
                FsMIFsbFcfMacAddress

                The Object 
                retValFsMIFsbFcfNameId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbFcfNameId (INT4 i4FsMIFsbFcfVlanId,
                        INT4 i4FsMIFsbFcfIfIndex,
                        tMacAddr FsMIFsbFcfMacAddress,
                        tSNMP_OCTET_STRING_TYPE * pRetValFsMIFsbFcfNameId)
{
    tFsbFcfEntry       *pFsbFcfEntry = NULL;

    pFsbFcfEntry = FsbGetFsbFcfEntry ((UINT2) i4FsMIFsbFcfVlanId,
                                      (UINT4) i4FsMIFsbFcfIfIndex,
                                      FsMIFsbFcfMacAddress);

    if (pFsbFcfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRetValFsMIFsbFcfNameId->i4_Length = sizeof (pFsbFcfEntry->au1NameId);
    FSB_MEMCPY (pRetValFsMIFsbFcfNameId->pu1_OctetList,
                pFsbFcfEntry->au1NameId, pRetValFsMIFsbFcfNameId->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbFcfFabricName
 Input       :  The Indices
                FsMIFsbFcfVlanId
                FsMIFsbFcfIfIndex
                FsMIFsbFcfMacAddress

                The Object 
                retValFsMIFsbFcfFabricName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbFcfFabricName (INT4 i4FsMIFsbFcfVlanId,
                            INT4 i4FsMIFsbFcfIfIndex,
                            tMacAddr FsMIFsbFcfMacAddress,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsMIFsbFcfFabricName)
{
    tFsbFcfEntry       *pFsbFcfEntry = NULL;

    pFsbFcfEntry = FsbGetFsbFcfEntry ((UINT2) i4FsMIFsbFcfVlanId,
                                      (UINT4) i4FsMIFsbFcfIfIndex,
                                      FsMIFsbFcfMacAddress);

    if (pFsbFcfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRetValFsMIFsbFcfFabricName->i4_Length =
        sizeof (pFsbFcfEntry->au1FabricName);
    FSB_MEMCPY (pRetValFsMIFsbFcfFabricName->pu1_OctetList,
                pFsbFcfEntry->au1FabricName,
                pRetValFsMIFsbFcfFabricName->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbFcfPinnedPorts
 Input       :  The Indices
                FsMIFsbFcfVlanId
                FsMIFsbFcfIfIndex
                FsMIFsbFcfMacAddress

                The Object 
                retValFsMIFsbFcfPinnedPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbFcfPinnedPorts (INT4 i4FsMIFsbFcfVlanId,
                             INT4 i4FsMIFsbFcfIfIndex,
                             tMacAddr FsMIFsbFcfMacAddress,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsMIFsbFcfPinnedPorts)
{
    tFsbFcfEntry       *pFsbFcfEntry = NULL;
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    UINT1               u1IfType = 0;

    pFsbFcfEntry = FsbGetFsbFcfEntry ((UINT2) i4FsMIFsbFcfVlanId,
                                      (UINT4) i4FsMIFsbFcfIfIndex,
                                      FsMIFsbFcfMacAddress);

    if (pFsbFcfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    FSB_MEMSET (pRetValFsMIFsbFcfPinnedPorts->pu1_OctetList, 0,
                BRG_PORT_LIST_SIZE);
    pRetValFsMIFsbFcfPinnedPorts->i4_Length = BRG_PORT_LIST_SIZE;

    /* Get the interface type */
    FsbCfaGetIfType (pFsbFcfEntry->u4FcfIfIndex, &u1IfType);
    if (u1IfType == CFA_LAGG)
    {
        if (FsbIsMLAGPortChannel ((UINT2) pFsbFcfEntry->u4FcfIfIndex) ==
            FSB_SUCCESS)
        {
            pFsbFipSnoopingEntry =
                FsbGetFIPSnoopingEntry (pFsbFcfEntry->u4ContextId,
                                        pFsbFcfEntry->u2VlanId);

            if (pFsbFipSnoopingEntry != NULL)
            {
                FSB_MEMCPY (pRetValFsMIFsbFcfPinnedPorts->pu1_OctetList,
                            pFsbFipSnoopingEntry->FsbPinnedPorts,
                            BRG_PORT_LIST_SIZE);
            }
        }
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIFsbGlobalStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsbGlobalStatsTable
 Input       :  The Indices
                FsMIFsbContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsbGlobalStatsTable (UINT4 u4FsMIFsbContextId)
{
    if (FsbCxtGetContextEntry (u4FsMIFsbContextId) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsbGlobalStatsTable
 Input       :  The Indices
                FsMIFsbContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsbGlobalStatsTable (UINT4 *pu4FsMIFsbContextId)
{
    UINT4               u4ContextId = 0;

    *pu4FsMIFsbContextId = 0;

    for (; u4ContextId < FSB_MAX_CONTEXT_COUNT; u4ContextId++)
    {
        if (FsbCxtGetContextEntry (u4ContextId) != NULL)
        {
            *pu4FsMIFsbContextId = u4ContextId;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsbGlobalStatsTable
 Input       :  The Indices
                FsMIFsbContextId
                nextFsMIFsbContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsbGlobalStatsTable (UINT4 u4FsMIFsbContextId,
                                        UINT4 *pu4NextFsMIFsbContextId)
{
    UINT4               u4ContextId = u4FsMIFsbContextId + 1;

    *pu4NextFsMIFsbContextId = 0;

    for (; u4ContextId < FSB_MAX_CONTEXT_COUNT; u4ContextId++)
    {
        if (FsbCxtGetContextEntry (u4ContextId) != NULL)
        {
            *pu4NextFsMIFsbContextId = u4ContextId;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsbGlobalStatsVlanRequests
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                retValFsMIFsbGlobalStatsVlanRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbGlobalStatsVlanRequests (UINT4 u4FsMIFsbContextId,
                                      UINT4
                                      *pu4RetValFsMIFsbGlobalStatsVlanRequests)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMIFsbGlobalStatsVlanRequests =
        pFsbContextInfo->u4VlanRequestCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbGlobalStatsVlanNotification
 Input       :  The Indices
                FsMIFsbContextId

                The Object 
                retValFsMIFsbGlobalStatsVlanNotification
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbGlobalStatsVlanNotification (UINT4 u4FsMIFsbContextId,
                                          UINT4
                                          *pu4RetValFsMIFsbGlobalStatsVlanNotification)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4FsMIFsbContextId);

    if (pFsbContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMIFsbGlobalStatsVlanNotification =
        pFsbContextInfo->u4VlanNotificationCount;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIFsbVlanStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsbVlanStatsTable
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsbVlanStatsTable (UINT4 u4FsMIFsbContextId,
                                               INT4
                                               i4FsMIFsbFIPSnoopingVlanIndex)
{
    if (FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                (UINT2) i4FsMIFsbFIPSnoopingVlanIndex) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsbVlanStatsTable
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsbVlanStatsTable (UINT4 *pu4FsMIFsbContextId,
                                       INT4 *pi4FsMIFsbFIPSnoopingVlanIndex)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = (tFsbFipSnoopingEntry *)
        RBTreeGetFirst (gFsbGlobals.FsbFipSnoopingTable);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4FsMIFsbContextId = pFsbFipSnoopingEntry->u4ContextId;
    *pi4FsMIFsbFIPSnoopingVlanIndex = (INT4) (pFsbFipSnoopingEntry->u2VlanId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsbVlanStatsTable
 Input       :  The Indices
                FsMIFsbContextId
                nextFsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex
                nextFsMIFsbFIPSnoopingVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsbVlanStatsTable (UINT4 u4FsMIFsbContextId,
                                      UINT4 *pu4NextFsMIFsbContextId,
                                      INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                      INT4 *pi4NextFsMIFsbFIPSnoopingVlanIndex)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbFipSnoopingEntry FsbFipSnoopingEntry;

    FSB_MEMSET (&FsbFipSnoopingEntry, 0, sizeof (tFsbFipSnoopingEntry));

    FsbFipSnoopingEntry.u4ContextId = u4FsMIFsbContextId;

    FsbFipSnoopingEntry.u2VlanId = (UINT2) i4FsMIFsbFIPSnoopingVlanIndex;

    pFsbFipSnoopingEntry = (tFsbFipSnoopingEntry *)
        RBTreeGetNext (gFsbGlobals.FsbFipSnoopingTable,
                       (tRBElem *) & FsbFipSnoopingEntry, NULL);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4NextFsMIFsbContextId = pFsbFipSnoopingEntry->u4ContextId;
    *pi4NextFsMIFsbFIPSnoopingVlanIndex =
        (INT4) (pFsbFipSnoopingEntry->u2VlanId);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsbVlanStatsUnicastDisAdv
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbVlanStatsUnicastDisAdv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbVlanStatsUnicastDisAdv (UINT4 u4FsMIFsbContextId,
                                     INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                     UINT4
                                     *pu4RetValFsMIFsbVlanStatsUnicastDisAdv)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIFsbVlanStatsUnicastDisAdv =
        pFsbFipSnoopingEntry->u4UnicastDisAdvCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbVlanStatsMulticastDisAdv
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbVlanStatsMulticastDisAdv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbVlanStatsMulticastDisAdv (UINT4 u4FsMIFsbContextId,
                                       INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                       UINT4
                                       *pu4RetValFsMIFsbVlanStatsMulticastDisAdv)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIFsbVlanStatsMulticastDisAdv =
        pFsbFipSnoopingEntry->u4MulticastDisAdvCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbVlanStatsUnicastDisSol
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbVlanStatsUnicastDisSol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbVlanStatsUnicastDisSol (UINT4 u4FsMIFsbContextId,
                                     INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                     UINT4
                                     *pu4RetValFsMIFsbVlanStatsUnicastDisSol)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIFsbVlanStatsUnicastDisSol =
        pFsbFipSnoopingEntry->u4UnicastDisSolCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbVlanStatsMulticastDisSol
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbVlanStatsMulticastDisSol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbVlanStatsMulticastDisSol (UINT4 u4FsMIFsbContextId,
                                       INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                       UINT4
                                       *pu4RetValFsMIFsbVlanStatsMulticastDisSol)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIFsbVlanStatsMulticastDisSol =
        pFsbFipSnoopingEntry->u4MulticastDisSolCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbVlanStatsFLOGICount
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbVlanStatsFLOGICount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbVlanStatsFLOGICount (UINT4 u4FsMIFsbContextId,
                                  INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                  UINT4 *pu4RetValFsMIFsbVlanStatsFLOGICount)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIFsbVlanStatsFLOGICount = pFsbFipSnoopingEntry->u4FLOGICount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbVlanStatsFDISCCount
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbVlanStatsFDISCCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbVlanStatsFDISCCount (UINT4 u4FsMIFsbContextId,
                                  INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                  UINT4 *pu4RetValFsMIFsbVlanStatsFDISCCount)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIFsbVlanStatsFDISCCount = pFsbFipSnoopingEntry->u4FDISCCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbVlanStatsLOGOCount
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbVlanStatsLOGOCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbVlanStatsLOGOCount (UINT4 u4FsMIFsbContextId,
                                 INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                 UINT4 *pu4RetValFsMIFsbVlanStatsLOGOCount)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIFsbVlanStatsLOGOCount = pFsbFipSnoopingEntry->u4LOGOCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbVlanStatsFLOGIAcceptCount
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbVlanStatsFLOGIAcceptCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbVlanStatsFLOGIAcceptCount (UINT4 u4FsMIFsbContextId,
                                        INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                        UINT4
                                        *pu4RetValFsMIFsbVlanStatsFLOGIAcceptCount)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIFsbVlanStatsFLOGIAcceptCount =
        pFsbFipSnoopingEntry->u4FLOGIAcceptCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbVlanStatsFLOGIRejectCount
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbVlanStatsFLOGIRejectCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbVlanStatsFLOGIRejectCount (UINT4 u4FsMIFsbContextId,
                                        INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                        UINT4
                                        *pu4RetValFsMIFsbVlanStatsFLOGIRejectCount)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIFsbVlanStatsFLOGIRejectCount =
        pFsbFipSnoopingEntry->u4FLOGIRejectCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbVlanStatsFDISCAcceptCount
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbVlanStatsFDISCAcceptCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbVlanStatsFDISCAcceptCount (UINT4 u4FsMIFsbContextId,
                                        INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                        UINT4
                                        *pu4RetValFsMIFsbVlanStatsFDISCAcceptCount)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIFsbVlanStatsFDISCAcceptCount =
        pFsbFipSnoopingEntry->u4FDISCAcceptCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbVlanStatsFDISCRejectCount
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbVlanStatsFDISCRejectCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbVlanStatsFDISCRejectCount (UINT4 u4FsMIFsbContextId,
                                        INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                        UINT4
                                        *pu4RetValFsMIFsbVlanStatsFDISCRejectCount)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIFsbVlanStatsFDISCRejectCount =
        pFsbFipSnoopingEntry->u4FDISCRejectCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbVlanStatsLOGOAcceptCount
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbVlanStatsLOGOAcceptCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbVlanStatsLOGOAcceptCount (UINT4 u4FsMIFsbContextId,
                                       INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                       UINT4
                                       *pu4RetValFsMIFsbVlanStatsLOGOAcceptCount)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIFsbVlanStatsLOGOAcceptCount =
        pFsbFipSnoopingEntry->u4LOGOAcceptCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbVlanStatsLOGORejectCount
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbVlanStatsLOGORejectCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbVlanStatsLOGORejectCount (UINT4 u4FsMIFsbContextId,
                                       INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                       UINT4
                                       *pu4RetValFsMIFsbVlanStatsLOGORejectCount)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIFsbVlanStatsLOGORejectCount =
        pFsbFipSnoopingEntry->u4LOGORejectCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbVlanStatsClearLinkCount
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbVlanStatsClearLinkCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbVlanStatsClearLinkCount (UINT4 u4FsMIFsbContextId,
                                      INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                      UINT4
                                      *pu4RetValFsMIFsbVlanStatsClearLinkCount)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIFsbVlanStatsClearLinkCount =
        pFsbFipSnoopingEntry->u4ClearLinkCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbVlanFcMapMisMatchCount
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbVlanFcMapMisMatchCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbVlanFcMapMisMatchCount (UINT4 u4FsMIFsbContextId,
                                     INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                     UINT4
                                     *pu4RetValFsMIFsbVlanFcMapMisMatchCount)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIFsbVlanFcMapMisMatchCount =
        pFsbFipSnoopingEntry->u4FcMapMisMatchCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbVlanMTUMisMatchCount
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbVlanMTUMisMatchCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbVlanMTUMisMatchCount (UINT4 u4FsMIFsbContextId,
                                   INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                   UINT4 *pu4RetValFsMIFsbVlanMTUMisMatchCount)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIFsbVlanMTUMisMatchCount =
        pFsbFipSnoopingEntry->u4MTUMisMatchCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbVlanACLFailureCount
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbVlanACLFailureCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbVlanACLFailureCount (UINT4 u4FsMIFsbContextId,
                                  INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                  UINT4 *pu4RetValFsMIFsbVlanACLFailureCount)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIFsbVlanACLFailureCount =
        pFsbFipSnoopingEntry->u4ACLFailureCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbVlanInvalidFIPFramesCount
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbVlanInvalidFIPFramesCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbVlanInvalidFIPFramesCount (UINT4 u4FsMIFsbContextId,
                                        INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                        UINT4
                                        *pu4RetValFsMIFsbVlanInvalidFIPFramesCount)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIFsbVlanInvalidFIPFramesCount =
        pFsbFipSnoopingEntry->u4InvalidFIPFramesCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbVlanFCFDiscoveryTimeoutsCount
 Input       :  The Indices
                FsMIFsbContextId
                FsMIFsbFIPSnoopingVlanIndex

                The Object 
                retValFsMIFsbVlanFCFDiscoveryTimeoutsCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbVlanFCFDiscoveryTimeoutsCount (UINT4 u4FsMIFsbContextId,
                                            INT4 i4FsMIFsbFIPSnoopingVlanIndex,
                                            UINT4
                                            *pu4RetValFsMIFsbVlanFCFDiscoveryTimeoutsCount)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;

    pFsbFipSnoopingEntry = FsbGetFIPSnoopingEntry (u4FsMIFsbContextId,
                                                   (UINT2)
                                                   i4FsMIFsbFIPSnoopingVlanIndex);

    if (pFsbFipSnoopingEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsMIFsbVlanFCFDiscoveryTimeoutsCount =
        pFsbFipSnoopingEntry->u4FCFDiscoveryTimeoutsCount;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIFsbSessStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsbSessStatsTable
 Input       :  The Indices
                FsMIFsbSessStatsVlanId
                FsMIFsbSessStatsEnodeIfIndex
                FsMIFsbSessStatsEnodeMacAddress
                FsMIFsbSessStatsFcfMacAddress
                FsMIFsbSessStatsFCoEMacAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsbSessStatsTable (INT4 i4FsMIFsbSessStatsVlanId,
                                               INT4
                                               i4FsMIFsbSessStatsEnodeIfIndex,
                                               tMacAddr
                                               FsMIFsbSessStatsEnodeMacAddress,
                                               tMacAddr
                                               FsMIFsbSessStatsFcfMacAddress,
                                               tMacAddr
                                               FsMIFsbSessStatsFCoEMacAddress)
{
    if (FsbGetFIPSessionFCoEEntry ((UINT2) i4FsMIFsbSessStatsVlanId,
                                   (UINT4) i4FsMIFsbSessStatsEnodeIfIndex,
                                   FsMIFsbSessStatsEnodeMacAddress,
                                   FsMIFsbSessStatsFcfMacAddress,
                                   FsMIFsbSessStatsFCoEMacAddress) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsbSessStatsTable
 Input       :  The Indices
                FsMIFsbSessStatsVlanId
                FsMIFsbSessStatsEnodeIfIndex
                FsMIFsbSessStatsEnodeMacAddress
                FsMIFsbSessStatsFcfMacAddress
                FsMIFsbSessStatsFCoEMacAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsbSessStatsTable (INT4 *pi4FsMIFsbSessStatsVlanId,
                                       INT4 *pi4FsMIFsbSessStatsEnodeIfIndex,
                                       tMacAddr *
                                       pFsMIFsbSessStatsEnodeMacAddress,
                                       tMacAddr *
                                       pFsMIFsbSessStatsFcfMacAddress,
                                       tMacAddr *
                                       pFsMIFsbSessStatsFCoEMacAddress)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;

    pFsbFipSessFCoEEntry = (tFsbFipSessFCoEEntry *)
        RBTreeGetFirst (gFsbGlobals.FsbFipSessFCoETable);
    if (pFsbFipSessFCoEEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4FsMIFsbSessStatsEnodeIfIndex =
        (INT4) (pFsbFipSessFCoEEntry->u4ENodeIfIndex);
    *pi4FsMIFsbSessStatsVlanId = (INT4) (pFsbFipSessFCoEEntry->u2VlanId);
    FSB_MEMCPY (pFsMIFsbSessStatsEnodeMacAddress,
                pFsbFipSessFCoEEntry->ENodeMacAddr, MAC_ADDR_LEN);
    FSB_MEMCPY (pFsMIFsbSessStatsFcfMacAddress,
                pFsbFipSessFCoEEntry->FcfMacAddr, MAC_ADDR_LEN);
    FSB_MEMCPY (pFsMIFsbSessStatsFCoEMacAddress,
                pFsbFipSessFCoEEntry->FCoEMacAddr, MAC_ADDR_LEN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsbSessStatsTable
 Input       :  The Indices
                FsMIFsbSessStatsVlanId
                nextFsMIFsbSessStatsVlanId
                FsMIFsbSessStatsEnodeIfIndex
                nextFsMIFsbSessStatsEnodeIfIndex
                FsMIFsbSessStatsEnodeMacAddress
                nextFsMIFsbSessStatsEnodeMacAddress
                FsMIFsbSessStatsFcfMacAddress
                nextFsMIFsbSessStatsFcfMacAddress
                FsMIFsbSessStatsFCoEMacAddress
                nextFsMIFsbSessStatsFCoEMacAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsbSessStatsTable (INT4 i4FsMIFsbSessStatsVlanId,
                                      INT4 *pi4NextFsMIFsbSessStatsVlanId,
                                      INT4 i4FsMIFsbSessStatsEnodeIfIndex,
                                      INT4 *pi4NextFsMIFsbSessStatsEnodeIfIndex,
                                      tMacAddr FsMIFsbSessStatsEnodeMacAddress,
                                      tMacAddr *
                                      pNextFsMIFsbSessStatsEnodeMacAddress,
                                      tMacAddr FsMIFsbSessStatsFcfMacAddress,
                                      tMacAddr *
                                      pNextFsMIFsbSessStatsFcfMacAddress,
                                      tMacAddr FsMIFsbSessStatsFCoEMacAddress,
                                      tMacAddr *
                                      pNextFsMIFsbSessStatsFCoEMacAddress)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;
    tFsbFipSessFCoEEntry FsbFipSessFCoEEntry;

    FSB_MEMSET (&FsbFipSessFCoEEntry, 0, sizeof (tFsbFipSessFCoEEntry));

    FsbFipSessFCoEEntry.u2VlanId = (UINT2) i4FsMIFsbSessStatsVlanId;
    FsbFipSessFCoEEntry.u4ENodeIfIndex = (UINT4) i4FsMIFsbSessStatsEnodeIfIndex;
    FSB_MEMCPY (FsbFipSessFCoEEntry.ENodeMacAddr,
                FsMIFsbSessStatsEnodeMacAddress, MAC_ADDR_LEN);
    FSB_MEMCPY (FsbFipSessFCoEEntry.FcfMacAddr,
                FsMIFsbSessStatsFcfMacAddress, MAC_ADDR_LEN);
    FSB_MEMCPY (FsbFipSessFCoEEntry.FCoEMacAddr,
                FsMIFsbSessStatsFCoEMacAddress, MAC_ADDR_LEN);

    pFsbFipSessFCoEEntry = (tFsbFipSessFCoEEntry *)
        RBTreeGetNext (gFsbGlobals.FsbFipSessFCoETable,
                       (tRBElem *) & FsbFipSessFCoEEntry, NULL);

    if (pFsbFipSessFCoEEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4NextFsMIFsbSessStatsVlanId = (INT4) (pFsbFipSessFCoEEntry->u2VlanId);
    *pi4NextFsMIFsbSessStatsEnodeIfIndex =
        (INT4) (pFsbFipSessFCoEEntry->u4ENodeIfIndex);
    FSB_MEMCPY (pNextFsMIFsbSessStatsEnodeMacAddress,
                pFsbFipSessFCoEEntry->ENodeMacAddr, MAC_ADDR_LEN);
    FSB_MEMCPY (pNextFsMIFsbSessStatsFcfMacAddress,
                pFsbFipSessFCoEEntry->FcfMacAddr, MAC_ADDR_LEN);
    FSB_MEMCPY (pNextFsMIFsbSessStatsFCoEMacAddress,
                pFsbFipSessFCoEEntry->FCoEMacAddr, MAC_ADDR_LEN);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsbSessStatsEnodeKeepAliveCount
 Input       :  The Indices
                FsMIFsbSessStatsVlanId
                FsMIFsbSessStatsEnodeIfIndex
                FsMIFsbSessStatsEnodeMacAddress
                FsMIFsbSessStatsFcfMacAddress
                FsMIFsbSessStatsFCoEMacAddress

                The Object 
                retValFsMIFsbSessStatsEnodeKeepAliveCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbSessStatsEnodeKeepAliveCount (INT4 i4FsMIFsbSessStatsVlanId,
                                           INT4 i4FsMIFsbSessStatsEnodeIfIndex,
                                           tMacAddr
                                           FsMIFsbSessStatsEnodeMacAddress,
                                           tMacAddr
                                           FsMIFsbSessStatsFcfMacAddress,
                                           tMacAddr
                                           FsMIFsbSessStatsFCoEMacAddress,
                                           UINT4
                                           *pu4RetValFsMIFsbSessStatsEnodeKeepAliveCount)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;

    pFsbFipSessFCoEEntry =
        FsbGetFIPSessionFCoEEntry ((UINT2) i4FsMIFsbSessStatsVlanId,
                                   (UINT4) i4FsMIFsbSessStatsEnodeIfIndex,
                                   FsMIFsbSessStatsEnodeMacAddress,
                                   FsMIFsbSessStatsFcfMacAddress,
                                   FsMIFsbSessStatsFCoEMacAddress);

    if (pFsbFipSessFCoEEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMIFsbSessStatsEnodeKeepAliveCount =
        pFsbFipSessFCoEEntry->pFsbFipSessEntry->u4KeepAliveCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsbSessStatsVNPortKeepAliveCount
 Input       :  The Indices
                FsMIFsbSessStatsVlanId
                FsMIFsbSessStatsEnodeIfIndex
                FsMIFsbSessStatsEnodeMacAddress
                FsMIFsbSessStatsFcfMacAddress
                FsMIFsbSessStatsFCoEMacAddress

                The Object 
                retValFsMIFsbSessStatsVNPortKeepAliveCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsbSessStatsVNPortKeepAliveCount (INT4 i4FsMIFsbSessStatsVlanId,
                                            INT4 i4FsMIFsbSessStatsEnodeIfIndex,
                                            tMacAddr
                                            FsMIFsbSessStatsEnodeMacAddress,
                                            tMacAddr
                                            FsMIFsbSessStatsFcfMacAddress,
                                            tMacAddr
                                            FsMIFsbSessStatsFCoEMacAddress,
                                            UINT4
                                            *pu4RetValFsMIFsbSessStatsVNPortKeepAliveCount)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;

    pFsbFipSessFCoEEntry =
        FsbGetFIPSessionFCoEEntry ((UINT2) i4FsMIFsbSessStatsVlanId,
                                   (UINT4) i4FsMIFsbSessStatsEnodeIfIndex,
                                   FsMIFsbSessStatsEnodeMacAddress,
                                   FsMIFsbSessStatsFcfMacAddress,
                                   FsMIFsbSessStatsFCoEMacAddress);

    if (pFsbFipSessFCoEEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMIFsbSessStatsVNPortKeepAliveCount =
        pFsbFipSessFCoEEntry->u4VNKeepAliveCount;

    return SNMP_SUCCESS;
}
