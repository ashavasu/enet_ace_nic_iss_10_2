/*************************************************************************
* Copyright (C) 2007-2015 Aricent Group . All Rights Reserved
*
* $Id: fsbcxt.c,v 1.7 2017/10/09 13:13:58 siva Exp $
*
* Description: This file contains the FIP-snooping Context related functions
*
*************************************************************************/
#ifndef _FSBCXT_C_
#define _FSBCXT_C_

#include "fsbinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCxtMemInit                                    */
/*                                                                           */
/*    Description         : This function used to create and do the proper   */
/*                          initialization for the context entry.            */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context index                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : pointer to tFsbContextInfo                       */
/*                                                                           */
/*****************************************************************************/
tFsbContextInfo    *
FsbCxtMemInit (UINT4 u4ContextId)
{
    tFsbContextInfo    *pFsbContextEntry = NULL;

    if ((pFsbContextEntry = (tFsbContextInfo *) MemAllocMemBlk
         (FSB_CONTEXT_MEMPOOL_ID)) == NULL)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL, FSB_BUFFER_TRC,
                         "FsbCtxtMemInit ctxt memory allocation failed\r\n");
        return NULL;
    }

    FSB_MEMSET (pFsbContextEntry, 0, sizeof (tFsbContextInfo));

    /* Initialise pFsbContextEntry to Default Values */
    pFsbContextEntry->u1SystemControl = FSB_DEFAULT_SYSTEM_CONTROL;
    pFsbContextEntry->u1ModuleStatus = FSB_DEFAULT_MODULE_STATUS;
    pFsbContextEntry->u1FcMapMode = FSB_DEFAULT_FCMAP_MODE;
    pFsbContextEntry->u2HouseKeepingTimePeriod = FSB_DEFAULT_HOUSEKEEPING_TIME;
    pFsbContextEntry->u4TraceOption = FSB_DEFAULT_TRACE_OPTION;
    pFsbContextEntry->u1TrapStatus = FSB_DEFAULT_TRAP_OPTION;
    pFsbContextEntry->bClearStats = FSB_DEFAULT_CLEAR_STATS;
    pFsbContextEntry->u2DefaultVlanId = FSB_DEFAULT_VLAN_INIT_VAL;
    pFsbContextEntry->u4TraceSeverityLevel = FSB_CRITICAL_LEVEL;

    FSB_MEMCPY (pFsbContextEntry->au1FcMap, gau1DefaultFcMapValue,
                FSB_FCMAP_LEN);

    gFsbGlobals.apFsbContextInfo[u4ContextId] = pFsbContextEntry;

    return pFsbContextEntry;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCxtGetContextEntry                            */
/*                                                                           */
/*    Description         : This function used to get the pointer to the     */
/*                          context info entry.                              */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context index                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : pointer to tFsbContextInfo                       */
/*                                                                           */
/*****************************************************************************/
tFsbContextInfo    *
FsbCxtGetContextEntry (UINT4 u4ContextId)
{

    /* If the Context Id exceeds the Max no. of Context allowed, return NULL */
    if (u4ContextId >= FSB_MAX_CONTEXT_COUNT)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                         "FsbCxtGetContextEntry : u4ContextId exceeds FSB_MAX_CONTEXT_COUNT\r\n");
        return NULL;
    }
    return gFsbGlobals.apFsbContextInfo[u4ContextId];
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCxtHandleDeleteContext                        */
/*                                                                           */
/*    Description         : This function will shutdown FSB for this context */
/*                          and delete the context.                          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context index                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCxtHandleDeleteContext (UINT4 u4ContextId)
{
    tFsbContextInfo    *pFsbContextEntry = NULL;

    pFsbContextEntry = gFsbGlobals.apFsbContextInfo[u4ContextId];

    if (pFsbContextEntry == NULL)
    {
        /* It is possible that context is not created in the FSB,
         * but a delete indication is received */
        return FSB_SUCCESS;
    }

    /* Delete All Table entries for the the Context */
    if (FsbDeleteAllTablesInContext (u4ContextId) != FSB_SUCCESS)
    {
        /* Trace is handled inside */
        FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL, FSB_CONTROL_PATH_TRC,
                         "FsbCxtHandleDeleteContext: FsbDeleteAllTablesInContext returns failure\r\n");
        return FSB_FAILURE;
    }

    MemReleaseMemBlock (FSB_CONTEXT_MEMPOOL_ID, (UINT1 *) pFsbContextEntry);

    gFsbGlobals.apFsbContextInfo[u4ContextId] = NULL;

    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDeleteAllTablesInContext                      */
/*                                                                           */
/*    Description         : This function will shutdown FSB for this context */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context index                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbDeleteAllTablesInContext (UINT4 u4ContextId)
{
    tFsbContextInfo    *pFsbContextEntry = NULL;

    /* Delete FcfEntry */
    if (FsbDeleteAllFcfEntry (u4ContextId) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbDeleteAllTablesInContext :FsbDeleteAllFcfEntry"
                         "returns failure\r\n");
        return FSB_FAILURE;
    }

    /* Delete FipSessEntry */
    if (FsbDeleteAllFipSessEntry (u4ContextId) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbDeleteAllTablesInContext :FsbDeleteAllFipSessEntry"
                         "returns failure\r\n");
        return FSB_FAILURE;
    }

    /* Remove Default filters, before deleting the contxt */
    if (FsbDeleteDefaultFilter (u4ContextId) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbDeleteAllTablesInContext :FsbDeleteDefaultFilter"
                         "returns failure\r\n");
        return FSB_FAILURE;
    }

    /* Remove all the s-channel filter entries */
    if (FsbRemoveSChannelFilters (u4ContextId) != FSB_SUCCESS)
    {
        return FSB_SUCCESS;
    }

    pFsbContextEntry = gFsbGlobals.apFsbContextInfo[u4ContextId];
    if (pFsbContextEntry != NULL)
    {
        /*Remove Default VLAN filter, before deleting the context */
        FsbRemoveDefaultVLANFilter (pFsbContextEntry);
    }

    /* Delete IntfEntry */
    if (FsbDeleteAllIntfEntry (u4ContextId) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbDeleteAllTablesInContext :FsbDeleteAllIntfEntry"
                         "returns failure\r\n");
        return FSB_FAILURE;
    }

    /* Delete FipSnoopingEntry */
    if (FsbDeleteAllFipSnoopingEntry (u4ContextId) != FSB_SUCCESS)
    {
        FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                         "FsbDeleteAllTablesInContext :FsbDeleteAllFipSnoopingEntry"
                         "returns failure\r\n");
        return FSB_FAILURE;
    }

    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDeleteAllFipSnoopingEntry                     */
/*                                                                           */
/*    Description         : This function will delete all FipSnoopingEntry   */
/*                          mapped to the context                            */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context index                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbDeleteAllFipSnoopingEntry (UINT4 u4ContextId)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbFipSnoopingEntry FsbFipSnoopingEntry;

    FSB_MEMSET (&FsbFipSnoopingEntry, 0, sizeof (tFsbFipSnoopingEntry));

    pFsbFipSnoopingEntry = (tFsbFipSnoopingEntry *)
        RBTreeGetFirst (gFsbGlobals.FsbFipSnoopingTable);

    if (pFsbFipSnoopingEntry == NULL)
    {
        /* No FipSnoopingEntry were created and therefore, no entries
         * to delete */
        return FSB_SUCCESS;
    }
    do
    {
        FsbFipSnoopingEntry.u4ContextId = pFsbFipSnoopingEntry->u4ContextId;
        FsbFipSnoopingEntry.u2VlanId = pFsbFipSnoopingEntry->u2VlanId;

        if (pFsbFipSnoopingEntry->u4ContextId == u4ContextId)
        {
            /* Unmark FCoE VLAN In L2IWF */
            FsbSetFCoEVlanType (u4ContextId, pFsbFipSnoopingEntry->u2VlanId,
                                L2_VLAN);

            /* Set Mac-learnig as enable since the vlan is no longer 
             * an FCoE VLAN */
            if (FsbPortSetVlanMacLearningStatus (u4ContextId,
                                                 pFsbFipSnoopingEntry->u2VlanId,
                                                 VLAN_ENABLED) != FSB_SUCCESS)
            {
                FSB_CONTEXT_TRC (u4ContextId, FSB_ERROR_LEVEL, FSB_NONE,
                                 "FsbDeleteAllTablesInContext:"
                                 " failure in enabling Mac-learning status for vlan id: %d\r\n",
                                 pFsbFipSnoopingEntry->u2VlanId);
                return FSB_FAILURE;
            }

            if (RBTreeRemove (gFsbGlobals.FsbFipSnoopingTable,
                              (tRBElem *) pFsbFipSnoopingEntry) != RB_SUCCESS)
            {
                FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL,
                                 FSB_OS_RESOURCE_TRC,
                                 "FsbDeleteAllTablesInContext : RBTreeRemove failed\r\n");
                return FSB_FAILURE;
            }
            MemReleaseMemBlock (FSB_FCOE_VLAN_ENTRIES_MEMPOOL_ID,
                                (UINT1 *) pFsbFipSnoopingEntry);
        }
        pFsbFipSnoopingEntry = (tFsbFipSnoopingEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFipSnoopingTable,
                           (tRBElem *) & FsbFipSnoopingEntry, NULL);
    }
    while (pFsbFipSnoopingEntry != NULL);

    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbDeleteAllIntfEntry                            */
/*                                                                           */
/*    Description         : This function will delete all FsbIntfEntry       */
/*                          mapped to the context                            */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context index                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbDeleteAllIntfEntry (UINT4 u4ContextId)
{
    tFsbIntfEntry      *pFsbIntfEntry = NULL;
    tFsbIntfEntry       FsbIntfEntry;

    FSB_MEMSET (&FsbIntfEntry, 0, sizeof (tFsbIntfEntry));

    pFsbIntfEntry = (tFsbIntfEntry *) RBTreeGetFirst (gFsbGlobals.FsbIntfTable);

    if (pFsbIntfEntry == NULL)
    {
        /* No FsbIntfEntry were created and therefore, no entries
         * to delete */
        return FSB_SUCCESS;
    }
    do
    {
        FsbIntfEntry.u2VlanId = pFsbIntfEntry->u2VlanId;
        FsbIntfEntry.u4IfIndex = pFsbIntfEntry->u4IfIndex;

        if (pFsbIntfEntry->u4ContextId == u4ContextId)
        {
            if (RBTreeRemove (gFsbGlobals.FsbIntfTable,
                              (tRBElem *) pFsbIntfEntry) != RB_SUCCESS)
            {
                FSB_CONTEXT_TRC (u4ContextId, FSB_DEBUGGING_LEVEL,
                                 FSB_OS_RESOURCE_TRC,
                                 "FsbDeleteAllIntfEntry : RBTreeRemove failed for VLAN Id: %d",
                                 "If Index: %d\r\n", FsbIntfEntry.u2VlanId,
                                 FsbIntfEntry.u4IfIndex);
                return FSB_FAILURE;
            }
            MemReleaseMemBlock (FSB_INTF_ENTRIES_MEMPOOL_ID,
                                (UINT1 *) pFsbIntfEntry);
        }

        pFsbIntfEntry = (tFsbIntfEntry *)
            RBTreeGetNext (gFsbGlobals.FsbIntfTable,
                           (tRBElem *) & FsbIntfEntry, NULL);
    }
    while (pFsbIntfEntry != NULL);

    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbClearGlobalStats                              */
/*                                                                           */
/*    Description         : This function is used clear Global Statistics    */
/*                          for the context                                  */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbClearGlobalStats (UINT4 u4ContextId)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;

    pFsbContextInfo = FsbCxtGetContextEntry (u4ContextId);

    if (pFsbContextInfo != NULL)
    {
        /* Clearing the global statistics */
        pFsbContextInfo->u4VlanRequestCount = 0;
        pFsbContextInfo->u4VlanNotificationCount = 0;
    }
    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbClearVlanStats                                */
/*                                                                           */
/*    Description         : This function is used clear VLAN Statistics      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbClearVlanStats (UINT4 u4ContextId)
{

    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbFipSnoopingEntry FsbFipSnoopingEntry;

    FSB_MEMSET (&FsbFipSnoopingEntry, 0, sizeof (tFsbFipSnoopingEntry));

    pFsbFipSnoopingEntry = (tFsbFipSnoopingEntry *)
        RBTreeGetFirst (gFsbGlobals.FsbFipSnoopingTable);

    if (pFsbFipSnoopingEntry == NULL)
    {
        /* No FipSnoopingEntry was created and therefore, no entry
         * to clear statistics */
        return FSB_SUCCESS;
    }
    do
    {
        /* If the Context Index Matches, Clear Stats for the
           FIP-snooping Entry */
        if (pFsbFipSnoopingEntry->u4ContextId == u4ContextId)
        {
            pFsbFipSnoopingEntry->u4UnicastDisAdvCount = 0;
            pFsbFipSnoopingEntry->u4MulticastDisAdvCount = 0;
            pFsbFipSnoopingEntry->u4UnicastDisSolCount = 0;
            pFsbFipSnoopingEntry->u4MulticastDisSolCount = 0;
            pFsbFipSnoopingEntry->u4FLOGICount = 0;
            pFsbFipSnoopingEntry->u4FDISCCount = 0;
            pFsbFipSnoopingEntry->u4LOGOCount = 0;
            pFsbFipSnoopingEntry->u4FLOGIAcceptCount = 0;
            pFsbFipSnoopingEntry->u4FLOGIRejectCount = 0;
            pFsbFipSnoopingEntry->u4FDISCAcceptCount = 0;
            pFsbFipSnoopingEntry->u4FDISCRejectCount = 0;
            pFsbFipSnoopingEntry->u4LOGOAcceptCount = 0;
            pFsbFipSnoopingEntry->u4LOGORejectCount = 0;
            pFsbFipSnoopingEntry->u4ClearLinkCount = 0;
            pFsbFipSnoopingEntry->u4FcMapMisMatchCount = 0;
            pFsbFipSnoopingEntry->u4MTUMisMatchCount = 0;
            pFsbFipSnoopingEntry->u4ACLFailureCount = 0;
            pFsbFipSnoopingEntry->u4InvalidFIPFramesCount = 0;
            pFsbFipSnoopingEntry->u4FCFDiscoveryTimeoutsCount = 0;
        }
        /* GetNext FIP-snooping Entry */
        FsbFipSnoopingEntry.u4ContextId = pFsbFipSnoopingEntry->u4ContextId;
        FsbFipSnoopingEntry.u2VlanId = pFsbFipSnoopingEntry->u2VlanId;

        pFsbFipSnoopingEntry = (tFsbFipSnoopingEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFipSnoopingTable,
                           (tRBElem *) & FsbFipSnoopingEntry, NULL);
    }
    while (pFsbFipSnoopingEntry != NULL);

    return FSB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbClearSessionStats                             */
/*                                                                           */
/*    Description         : This function is used clear Session Statistics   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbClearSessionStats (UINT4 u4ContextId)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;
    tFsbFipSessFCoEEntry FsbFipSessFCoEEntry;

    FSB_MEMSET (&FsbFipSessFCoEEntry, 0, sizeof (tFsbFipSessFCoEEntry));

    pFsbFipSessFCoEEntry = (tFsbFipSessFCoEEntry *)
        RBTreeGetFirst (gFsbGlobals.FsbFipSessFCoETable);

    if (pFsbFipSessFCoEEntry == NULL)
    {
        return FSB_SUCCESS;
    }
    do
    {
        /* If the Context Index Matches, Clear Stats for the
         * FIP Session Entry*/
        if (pFsbFipSessFCoEEntry->pFsbFipSessEntry->u4ContextId == u4ContextId)
        {
            pFsbFipSessFCoEEntry->pFsbFipSessEntry->u4KeepAliveCount = 0;
            pFsbFipSessFCoEEntry->u4VNKeepAliveCount = 0;
        }
        /* GetNext FIP Session  Entry */
        FsbFipSessFCoEEntry.u2VlanId = pFsbFipSessFCoEEntry->u2VlanId;
        FsbFipSessFCoEEntry.u4ENodeIfIndex =
            pFsbFipSessFCoEEntry->u4ENodeIfIndex;
        FSB_MEMCPY (FsbFipSessFCoEEntry.ENodeMacAddr,
                    pFsbFipSessFCoEEntry->ENodeMacAddr, MAC_ADDR_LEN);
        FSB_MEMCPY (FsbFipSessFCoEEntry.FcfMacAddr,
                    pFsbFipSessFCoEEntry->FcfMacAddr, MAC_ADDR_LEN);
        FSB_MEMCPY (FsbFipSessFCoEEntry.FCoEMacAddr,
                    pFsbFipSessFCoEEntry->FCoEMacAddr, MAC_ADDR_LEN);

        pFsbFipSessFCoEEntry = (tFsbFipSessFCoEEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFipSessFCoETable,
                           (tRBElem *) & FsbFipSessFCoEEntry, NULL);
    }
    while (pFsbFipSessFCoEEntry != NULL);

    return FSB_SUCCESS;
}

#endif /* _FSBCXT_C_ */
