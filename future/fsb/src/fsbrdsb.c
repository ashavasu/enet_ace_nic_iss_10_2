/*************************************************************************
* Copyright (C) 2007-2015 Aricent Group . All Rights Reserved
*
* $Id: fsbrdsb.c,v 1.3 2017/01/17 14:10:28 siva Exp $
*
* Description: This file contains redundancy stubs
*********************************************************************/

#include "fsbinc.h"

/*****************************************************************************/
/* Function Name      : FsbRegisterWithRM                                    */
/*                                                                           */
/* Description        : Registers FSB with RM by providing an application    */
/*                      ID for FSB and a call back function to be called     */
/*                      whenever RM needs to send an event to FSB.           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then FSB_SUCCESS          */
/*                      Otherwise FSB_FAILURE                                */
/*****************************************************************************/
INT4
FsbRedRegisterWithRM (VOID)
{
    FSB_NODE_STATUS () = FSB_NODE_ACTIVE;
    return FSB_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : FsbRedHandleRmEvents                                 */
/*                                                                           */
/* Description        : This function is invoked to process the following    */
/*                      from RM module:-                                     */
/*                           - RM events and                                 */
/*                           - update messages.                              */
/*                      This function interprets the RM events and calls the */
/*                      corresponding module functions to process those      */
/*                      events.                                              */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to the  input buffer.                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
FsbRedHandleRmEvents (tFsbNotifyParams * pMsg)
{
    UNUSED_PARAM (pMsg);
    return;
}
/*****************************************************************************/
/* Function Name      : FsbRedSyncUpDefaultVlanFilter                        */
/*                                                                           */
/* Description        : This function sends the Default VLAN filter's        */
/*                      Hardware Filter Id to the Standby.                   */
/*                                                                           */
/* Input(s)           : pFsbContextInfo - Pointer to Context Info            */
/*                      u4ContextId     - Context Id                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpDefaultVlanFilter (tFsbContextInfo *pFsbContextInfo, UINT4 u4ContextId)
{
    UNUSED_PARAM (pFsbContextInfo);
    UNUSED_PARAM (u4ContextId);
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedSyncUpDefaultFilter                            */
/*                                                                           */
/* Description        : This function sends the Default filter's             */
/*                      Hardware Filter Id to the Standby.                   */
/*                                                                           */
/* Input(s)           : pFsbFipSnoopingEntry - Pointer to Fip Snooping       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpDefaultFilter (tFsbFipSnoopingEntry *pFsbFipSnoopingEntry)    
{
    UNUSED_PARAM (pFsbFipSnoopingEntry);
    return FSB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsbRedSyncUpFCFEntry                                 */
/*                                                                           */
/* Description        : This function sends the dynamically populated FCF    */
/*                      entry to the Standby.                                */
/*                                                                           */
/* Input(s)           : pFsbFcfEntry - Pointer to FsbFcfEntry                */
/*                      u1MsgType    - Message Type                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpFCFEntry (tFsbFcfEntry *pFsbFcfEntry, UINT1 u1MsgType)
{
    UNUSED_PARAM (pFsbFcfEntry);
    UNUSED_PARAM (u1MsgType);
    return FSB_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : FsbRedSyncUpFCFFilterId                              */
/*                                                                           */
/* Description        : This function populate FCF Filter Id which is        */
/*                      to be sent to the standby                            */
/*                                                                           */
/* Input(s)           : pFsbFcfEntry - Pointer to FsbFcfEntry                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpFCFFilterId (tFsbFcfEntry *pFsbFcfEntry)
{
    UNUSED_PARAM (pFsbFcfEntry);
    return FSB_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : FsbRedSyncUpFIPSessEntry                             */
/*                                                                           */
/* Description        : This function sends the dynamically populated FIP    */
/*                      session entry to the Standby.                        */
/*                                                                           */
/* Input(s)           : pFsbFipSessEntry - Pointer to FsbFipSessEntry        */
/*                      pFSBPktInfo      - Pointer to FSBPktInfo             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpFIPSessEntry (tFsbFipSessEntry *pFsbFipSessEntry, tFSBPktInfo *pFSBPktInfo)
{
    UNUSED_PARAM (pFsbFipSessEntry);
    UNUSED_PARAM (pFSBPktInfo);
    return FSB_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : FsbRedSyncUpFIPSessionDelete                         */
/*                                                                           */
/* Description        : This function populates the Delete FIP Session       */
/*                      indication to be sent to the standby                 */
/*                                                                           */
/* Input(s)           : pFsbFipSessEntry - Pointer to pFsbFipSessEntry       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpFIPSessionDelete (tFsbFipSessEntry *pFsbFipSessEntry)
{
    UNUSED_PARAM (pFsbFipSessEntry);
    return FSB_SUCCESS;
}
/******************************************************************************/
/* Function Name      : FsbRedSyncUpDeleteFCoEEntry                           */
/*                                                                            */
/* Description        : This function populate Delete FCoE Entry  indication  */
/*                      to be sent to the standby                             */
/*                                                                            */
/* Input(s)           : pFsbFipSessFCoEEntry - Pointer to FsbFipSessFCoEEntry */
/*                      pMsg                 - Pointer to RM Message          */
/*                                                                            */
/* Output(s)          : None                                                  */
/*                                                                            */
/* Global Variables                                                           */
/* Referred           : None.                                                 */
/*                                                                            */
/* Global Variables                                                           */
/* Modified           : None.                                                 */
/*                                                                            */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                            */
/******************************************************************************/
INT4
FsbRedSyncUpDeleteFCoEEntry (tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry)
{
    UNUSED_PARAM (pFsbFipSessFCoEEntry);
    return FSB_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : FsbRedSyncUpSChannelFilterId                         */
/*                                                                           */
/* Description        : This function sends the S Channel Hardware Filter Id */
/*                      to the Standby.                                      */
/*                                                                           */
/* Input(s)           : pFsbFipSnoopingEntry - Pointer to Fip Snooping       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpSChannelFilterId (UINT4 u4IfIndex, UINT2 u2VlanId, UINT4 u4HwFilterId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (u4HwFilterId);
    return FSB_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : FsbRedSyncUpOperStatus                               */
/*                                                                           */
/* Description        : This function sends the Oper Status change           */
/*                      indication to the Standby.                           */
/*                                                                           */
/* Input(s)           : u4IfIndex    - IfIndex                               */
/*                      u1OperStatus - Oper Status                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FSB_SUCCESS / FSB_FAILURE.                           */
/*****************************************************************************/
INT4
FsbRedSyncUpOperStatus (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    UNUSED_PARAM (u4IfIndex); 
    UNUSED_PARAM (u1OperStatus);
    return FSB_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : FsbRedHandleBulkReqEvent.                            */
/*                                                                           */
/* Description        : This function gets the dynamic information from all  */
/*                      ports whose Receive sem is in CURRENT state.         */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
FsbRedHandleBulkReqEvent (VOID)
{
    return;
}
