/*************************************************************************
* Copyright (C) 2007-2012 Aricent Group . All Rights Reserved
*
* $Id: fsbwrap.c,v 1.10 2017/10/09 13:13:59 siva Exp $
*
* Description: This file contain wrapper function through which NPAPI function
*              call will happen
*
*************************************************************************/
#ifndef _FSBWRAP_C_
#define _FSBWRAP_C_

#include "fsbinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsMiNpFsbHwWrInit                                */
/*                                                                           */
/*    Description         : Wrapper function through which Hw Filter Entry   */
/*                          structure get populated and NPAPI function       */
/*                          FsMiNpFsbHwInit is called                        */
/*                                                                           */
/*    Input(s)            : pFsbFilterEntry - Filter Entry                   */
/*                                                                           */
/*    Output(s)           : pu4HwFilterId   - Filter Id                      */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsMiNpFsbHwWrInit (tFsbFilterEntry * pFsbFilterEntry,
                   tFsbLocRemFilterId * pFsbLocRemFilterId)
{
#ifdef NPAPI_WANTED
    tFsbHwFilterEntry   FsbHwFilterEntry;

    FSB_MEMSET (&FsbHwFilterEntry, 0, sizeof (tFsbHwFilterEntry));

    FsbHwFilterEntry.u4ContextId = pFsbFilterEntry->u4ContextId;
    FsbHwFilterEntry.u2VlanId = pFsbFilterEntry->u2VlanId;
    FsbHwFilterEntry.u2Ethertype = pFsbFilterEntry->u2Ethertype;
    FsbHwFilterEntry.u2OpcodeFilterOffsetValue =
        pFsbFilterEntry->u2OpcodeFilterOffsetValue;
    FsbHwFilterEntry.u1FilterAction = pFsbFilterEntry->u1FilterAction;
    FsbHwFilterEntry.u1FilterType = pFsbFilterEntry->u1FilterType;
    FsbHwFilterEntry.u1DefFilterType = pFsbFilterEntry->u1DefFilterType;
    FsbHwFilterEntry.u1Priority = pFsbFilterEntry->u1Priority;
    FsbHwFilterEntry.u2SubOpcodeFilterOffsetValue =
        pFsbFilterEntry->u2SubOpcodeFilterOffsetValue;
    FSB_MEMCPY (FsbHwFilterEntry.DstMac, pFsbFilterEntry->DstMac, MAC_ADDR_LEN);

    if (FSB_IS_NP_PROGRAMMING_ALLOWED () == FSB_TRUE)
    {
        if (FsbFsMiNpFsbHwInit (&FsbHwFilterEntry, pFsbLocRemFilterId) !=
            FNP_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFilterEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsMiNpFsbHwWrInit of Filter Entry with VlanId: %d"
                             ": FsbFsMiNpFsbHwInit failed\r\n",
                             pFsbFilterEntry->u2VlanId);
            return FSB_FAILURE;
        }
    }
    return FSB_SUCCESS;
#else
    UNUSED_PARAM (pFsbLocRemFilterId);
    UNUSED_PARAM (pFsbFilterEntry);
    return FSB_SUCCESS;
#endif

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsFsbHwWrCreateFilter                            */
/*                                                                           */
/*    Description         : Wrapper function through which Hw Filter Entry   */
/*                          structure get populated and NPAPI function       */
/*                          FsFsbHwCreateFilter is called                    */
/*                                                                           */
/*    Input(s)            : pFsbFilterEntry - Filter Entry                   */
/*                                                                           */
/*    Output(s)           : pu4HwFilterId   - Filter Id                      */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsFsbHwWrCreateFilter (tFsbFilterEntry * pFsbFilterEntry, UINT4 *pu4HwFilterId)
{
#ifdef NPAPI_WANTED
    tFsbHwFilterEntry   FsbHwFilterEntry;

    FSB_MEMSET (&FsbHwFilterEntry, 0, sizeof (tFsbHwFilterEntry));

    FsbHwFilterEntry.u4ContextId = pFsbFilterEntry->u4ContextId;
    FsbHwFilterEntry.u4AggIndex = pFsbFilterEntry->u4AggIndex;
    FSB_MEMCPY (FsbHwFilterEntry.PortList, pFsbFilterEntry->PortList,
                sizeof (tPortList));
    FSB_MEMCPY (FsbHwFilterEntry.DstMac, pFsbFilterEntry->DstMac, MAC_ADDR_LEN);
    FSB_MEMCPY (FsbHwFilterEntry.SrcMac, pFsbFilterEntry->SrcMac, MAC_ADDR_LEN);
    FsbHwFilterEntry.u2VlanId = pFsbFilterEntry->u2VlanId;
    FsbHwFilterEntry.u2ClassId = pFsbFilterEntry->u2ClassId;
    FsbHwFilterEntry.u2Ethertype = pFsbFilterEntry->u2Ethertype;
    FsbHwFilterEntry.u2OpcodeFilterOffsetValue =
        pFsbFilterEntry->u2OpcodeFilterOffsetValue;
    FsbHwFilterEntry.u2SubOpcodeFilterOffsetValue =
        pFsbFilterEntry->u2SubOpcodeFilterOffsetValue;
    FsbHwFilterEntry.u1FilterAction = pFsbFilterEntry->u1FilterAction;
    FsbHwFilterEntry.u1FilterType = pFsbFilterEntry->u1FilterType;
    FsbHwFilterEntry.u1Priority = pFsbFilterEntry->u1Priority;
    FsbHwFilterEntry.u4PinnnedPortIfIndex =
        pFsbFilterEntry->u4PinnnedPortIfIndex;

    if (FSB_IS_NP_PROGRAMMING_ALLOWED () == FSB_TRUE)
    {
        if (FsbFsFsbHwCreateFilter (&FsbHwFilterEntry, pu4HwFilterId) !=
            FNP_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFilterEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsFsbHwWrCreateFilter of Filter Entry with VlanId: %d"
                             ": FsbFsFsbHwCreateFilter failed\r\n",
                             pFsbFilterEntry->u2VlanId);
            return FSB_FAILURE;
        }
    }
    return FSB_SUCCESS;
#else
    UNUSED_PARAM (pu4HwFilterId);
    UNUSED_PARAM (pFsbFilterEntry);
    return FSB_SUCCESS;
#endif
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsMiNpFsbHwWrDeInit                              */
/*                                                                           */
/*    Description         : Wrapper function through which Hw Filter Entry   */
/*                          structure get populated and NPAPI function       */
/*                          FsMiNpFsbHwDeInit is called                      */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : pFsbFilterEntry - Filter Entry                   */
/*                          u4HwFilterId    - Filter Id                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsMiNpFsbHwWrDeInit (tFsbFilterEntry * pFsbFilterEntry,
                     tFsbLocRemFilterId FsbLocRemFilterId)
{
#ifdef NPAPI_WANTED
    tFsbHwFilterEntry   FsbHwFilterEntry;

    FSB_MEMSET (&FsbHwFilterEntry, 0, sizeof (tFsbHwFilterEntry));

    FsbHwFilterEntry.u4ContextId = pFsbFilterEntry->u4ContextId;
    FsbHwFilterEntry.u2VlanId = pFsbFilterEntry->u2VlanId;
    FsbHwFilterEntry.u2Ethertype = pFsbFilterEntry->u2Ethertype;
    FsbHwFilterEntry.u2OpcodeFilterOffsetValue =
        pFsbFilterEntry->u2OpcodeFilterOffsetValue;
    FsbHwFilterEntry.u1FilterAction = pFsbFilterEntry->u1FilterAction;
    FsbHwFilterEntry.u1FilterType = pFsbFilterEntry->u1FilterType;
    FsbHwFilterEntry.u1DefFilterType = pFsbFilterEntry->u1DefFilterType;
    FsbHwFilterEntry.u1Priority = pFsbFilterEntry->u1Priority;

    if (FSB_IS_NP_PROGRAMMING_ALLOWED () == FSB_TRUE)
    {
        if (FsbFsMiNpFsbHwDeInit (&FsbHwFilterEntry, FsbLocRemFilterId) !=
            FNP_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFilterEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsMiNpFsbHwWrDeInit of Filter Entry with VlanId: %d"
                             ": FsbFsMiNpFsbHwDeInit failed\r\n",
                             pFsbFilterEntry->u2VlanId);
            return FSB_FAILURE;
        }
    }
    return FSB_SUCCESS;
#else
    UNUSED_PARAM (FsbLocRemFilterId);
    UNUSED_PARAM (pFsbFilterEntry);
    return FSB_SUCCESS;
#endif

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsFsbHwWrDeleteFilter                            */
/*                                                                           */
/*    Description         : Wrapper function through which Hw Filter Entry   */
/*                          structure get populated and NPAPI function       */
/*                          FsFsbHwDeleteFilter is called                    */
/*                                                                           */
/*    Input(s)            : pFsbFilterEntry - Filter Entry                   */
/*                          u4HwFilterId    - Filter Id                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsFsbHwWrDeleteFilter (tFsbFilterEntry * pFsbFilterEntry, UINT4 u4HwFilterId)
{
#ifdef NPAPI_WANTED
    tFsbHwFilterEntry   FsbHwFilterEntry;

    FSB_MEMSET (&FsbHwFilterEntry, 0, sizeof (tFsbHwFilterEntry));

    FsbHwFilterEntry.u4ContextId = pFsbFilterEntry->u4ContextId;
    FsbHwFilterEntry.u4AggIndex = pFsbFilterEntry->u4AggIndex;
    FSB_MEMCPY (FsbHwFilterEntry.PortList, pFsbFilterEntry->PortList,
                sizeof (tPortList));
    FSB_MEMCPY (FsbHwFilterEntry.DstMac, pFsbFilterEntry->DstMac, MAC_ADDR_LEN);
    FSB_MEMCPY (FsbHwFilterEntry.SrcMac, pFsbFilterEntry->SrcMac, MAC_ADDR_LEN);
    FsbHwFilterEntry.u2VlanId = pFsbFilterEntry->u2VlanId;
    FsbHwFilterEntry.u2ClassId = pFsbFilterEntry->u2ClassId;
    FsbHwFilterEntry.u2Ethertype = pFsbFilterEntry->u2Ethertype;
    FsbHwFilterEntry.u2OpcodeFilterOffsetValue =
        pFsbFilterEntry->u2OpcodeFilterOffsetValue;
    FsbHwFilterEntry.u2SubOpcodeFilterOffsetValue =
        pFsbFilterEntry->u2SubOpcodeFilterOffsetValue;
    FsbHwFilterEntry.u1FilterAction = pFsbFilterEntry->u1FilterAction;
    FsbHwFilterEntry.u1FilterType = pFsbFilterEntry->u1FilterType;
    FsbHwFilterEntry.u1Priority = pFsbFilterEntry->u1Priority;

    if (FSB_IS_NP_PROGRAMMING_ALLOWED () == FSB_TRUE)
    {
        if (FsbFsFsbHwDeleteFilter (&FsbHwFilterEntry, u4HwFilterId) !=
            FNP_SUCCESS)
        {
            FSB_CONTEXT_TRC (pFsbFilterEntry->u4ContextId,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsFsbHwWrDeleteFilter of Filter Entry with VlanId: %d"
                             ": FsbFsFsbHwDeleteFilter failed\r\n",
                             pFsbFilterEntry->u2VlanId);
            return FSB_FAILURE;
        }
    }
    return FSB_SUCCESS;
#else
    UNUSED_PARAM (u4HwFilterId);
    UNUSED_PARAM (pFsbFilterEntry);
    return FSB_SUCCESS;
#endif
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsFsbHwWrCreateSChannelFilter                    */
/*                                                                           */
/*    Description         : Wrapper function through which Hw Filter Entry   */
/*                          structure get populated and NPAPI function       */
/*                          FsFsbHwCreateFilter is called                    */
/*                                                                           */
/*    Input(s)            : pFsbSChannelFilterEntry - Filter Entry           */
/*                                                                           */
/*    Output(s)           : pu4HwFilterId   - Filter Id                      */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsFsbHwWrCreateSChannelFilter (tFsbSChannelFilterEntry *
                               pFsbSChannelFilterEntry, UINT4 *pu4HwFilterId)
{
#ifdef NPAPI_WANTED
    tFsbHwSChannelFilterEntry FsbHwSChannelFilterEntry;

    FSB_MEMSET (&FsbHwSChannelFilterEntry, 0,
                sizeof (tFsbHwSChannelFilterEntry));

    FsbHwSChannelFilterEntry.u4IfIndex = pFsbSChannelFilterEntry->u4IfIndex;
    FsbHwSChannelFilterEntry.u2VlanId = pFsbSChannelFilterEntry->u2VlanId;
    FsbHwSChannelFilterEntry.u1Priority = pFsbSChannelFilterEntry->u1Priority;

    if (FSB_IS_NP_PROGRAMMING_ALLOWED () == FSB_TRUE)
    {
        if (FsbFsFsbHwCreateSChannelFilter
            (&FsbHwSChannelFilterEntry, pu4HwFilterId) != FNP_SUCCESS)
        {
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsFsbHwWrCreateSChannelFilter of Filter Entry with"
                             " VlanId: %d and IfIndex: %d: FsbFsFsbHwCreateSChannelFilter"
                             " failed\r\n", pFsbSChannelFilterEntry->u2VlanId,
                             pFsbSChannelFilterEntry->u4IfIndex);
            return FSB_FAILURE;
        }
    }
    return FSB_SUCCESS;
#else
    UNUSED_PARAM (pu4HwFilterId);
    UNUSED_PARAM (pFsbSChannelFilterEntry);
    return FSB_SUCCESS;
#endif
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsFsbHwWrDeleteSChannelFilter                    */
/*                                                                           */
/*    Description         : Wrapper function through which Hw Filter Entry   */
/*                          structure get populated and NPAPI function       */
/*                          FsbFsFsbHwDeleteSChannelFilter is called         */
/*                                                                           */
/*    Input(s)            : pFsbSChannelFilterEntry - Filter Entry           */
/*                                                                           */
/*    Output(s)           : u4HwFilterId   - Filter Id                       */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsFsbHwWrDeleteSChannelFilter (tFsbSChannelFilterEntry *
                               pFsbSChannelFilterEntry, UINT4 u4HwFilterId)
{
#ifdef NPAPI_WANTED
    tFsbHwSChannelFilterEntry FsbHwSChannelFilterEntry;

    FSB_MEMSET (&FsbHwSChannelFilterEntry, 0,
                sizeof (tFsbHwSChannelFilterEntry));
    FsbHwSChannelFilterEntry.u4IfIndex = pFsbSChannelFilterEntry->u4IfIndex;
    FsbHwSChannelFilterEntry.u2VlanId = pFsbSChannelFilterEntry->u2VlanId;

    if (FSB_IS_NP_PROGRAMMING_ALLOWED () == FSB_TRUE)
    {
        if (FsbFsFsbHwDeleteSChannelFilter
            (&FsbHwSChannelFilterEntry, u4HwFilterId) != FNP_SUCCESS)
        {
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsFsbHwWrDeleteSChannelFilter of Filter Entry with "
                             "VlanId: %d and IfIndex: %d: FsbFsFsbHwDeleteSChannelFilter"
                             "failed\r\n", pFsbSChannelFilterEntry->u2VlanId,
                             pFsbSChannelFilterEntry->u4IfIndex);
            return FSB_FAILURE;
        }
    }
    return FSB_SUCCESS;
#else
    UNUSED_PARAM (u4HwFilterId);
    UNUSED_PARAM (pFsbSChannelFilterEntry);
    return FSB_SUCCESS;
#endif
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsFsbHwWrSetFCoEParams                           */
/*                                                                           */
/*    Description         : Wrapper function through which VLAN Entry        */
/*                          structure get populated and NPAPI function       */
/*                          FsbFsFsbHwSetFCoEParams is called                */
/*                                                                           */
/*    Input(s)            : u4FsbContextId - Context Id                  */
/*                          u2VlanIndex - VLAN Id                            */
/*                          u1FIPSnoopingEnabledStatus - FIP Snooping status */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsFsbHwWrSetFCoEParams (UINT4 u4FsbContextId,
                        UINT2 u2VlanIndex, UINT1 u1FIPSnoopingEnabledStatus)
{
#ifdef NPAPI_WANTED
    tFsbHwVlanEntry     FsbHwVlanEntry;

    FSB_MEMSET (&FsbHwVlanEntry, 0, sizeof (tFsbHwVlanEntry));
    FsbHwVlanEntry.u4ContextId = u4FsbContextId;
    FsbHwVlanEntry.u2VlanId = u2VlanIndex;
    FsbHwVlanEntry.u1EnabledStatus = u1FIPSnoopingEnabledStatus;

    if (FSB_IS_NP_PROGRAMMING_ALLOWED () == FSB_TRUE)
    {
        if (FsbFsFsbHwSetFCoEParams (&FsbHwVlanEntry) != FNP_SUCCESS)
        {
            FSB_CONTEXT_TRC (FSB_DEFAULT_CXT_TRACE,
                             FSB_ERROR_LEVEL, FSB_NONE,
                             "FsFsbHwWrSetFCoEParams of Vlan Entry with "
                             "VlanId: %d and EnableStatus: %d: FsFsbHwWrSetFCoEParams"
                             "failed\r\n", u2VlanIndex,
                             u1FIPSnoopingEnabledStatus);
            return FSB_FAILURE;
        }
    }
    return FSB_SUCCESS;
#else
    UNUSED_PARAM (u4FsbContextId);
    UNUSED_PARAM (u2VlanIndex);
    UNUSED_PARAM (u1FIPSnoopingEnabledStatus);
    return FSB_SUCCESS;
#endif
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsMiNpFsbMbsmHwWrInit                            */
/*                                                                           */
/*    Description         : Wrapper function through which Hw Filter Entry   */
/*                          structure get populated and NPAPI function       */
/*                          FsMiNpFsbHwInit is called                        */
/*                                                                           */
/*    Input(s)            : pFsbFilterEntry - Filter Entry                   */
/*                                                                           */
/*    Output(s)           : pu4HwFilterId   - Filter Id                      */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsMiNpFsbMbsmHwWrInit (tFsbFilterEntry * pFsbFilterEntry,
                       tFsbLocRemFilterId * pFsbLocRemFilterId)
{
#ifdef MBSM_WANTED
    tFsbHwFilterEntry   FsbHwFilterEntry;

    FSB_MEMSET (&FsbHwFilterEntry, 0, sizeof (tFsbHwFilterEntry));

    FsbHwFilterEntry.u4ContextId = pFsbFilterEntry->u4ContextId;
    FsbHwFilterEntry.u2VlanId = pFsbFilterEntry->u2VlanId;
    FsbHwFilterEntry.u2Ethertype = pFsbFilterEntry->u2Ethertype;
    FsbHwFilterEntry.u2OpcodeFilterOffsetValue =
        pFsbFilterEntry->u2OpcodeFilterOffsetValue;
    FsbHwFilterEntry.u1FilterAction = pFsbFilterEntry->u1FilterAction;
    FsbHwFilterEntry.u1FilterType = pFsbFilterEntry->u1FilterType;
    FsbHwFilterEntry.u1DefFilterType = pFsbFilterEntry->u1DefFilterType;
    FsbHwFilterEntry.u1Priority = pFsbFilterEntry->u1Priority;
    FsbHwFilterEntry.u2SubOpcodeFilterOffsetValue =
        pFsbFilterEntry->u2SubOpcodeFilterOffsetValue;
    FSB_MEMCPY (FsbHwFilterEntry.DstMac, pFsbFilterEntry->DstMac, MAC_ADDR_LEN);

    if (FsbFsMiNpFsbMbsmHwInit (&FsbHwFilterEntry, pFsbLocRemFilterId) !=
        FNP_SUCCESS)
    {
        FSB_CONTEXT_TRC (pFsbFilterEntry->u4ContextId,
                         FSB_ERROR_LEVEL, FSB_NONE,
                         "FsMiNpFsbMbsmHwWrInit of Filter Entry with VlanId: %d"
                         ": FsbFsMiNpFsbMbsmHwInit failed\r\n",
                         pFsbFilterEntry->u2VlanId);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
#else
    UNUSED_PARAM (pFsbLocRemFilterId);
    UNUSED_PARAM (pFsbFilterEntry);
    return FSB_SUCCESS;
#endif

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsFsbMbsmHwWrCreateFilter                        */
/*                                                                           */
/*    Description         : Wrapper function through which Hw Filter Entry   */
/*                          structure get populated and NPAPI function       */
/*                          FsFsbHwCreateFilter is called                    */
/*                                                                           */
/*    Input(s)            : pFsbFilterEntry - Filter Entry                   */
/*                                                                           */
/*    Output(s)           : pu4HwFilterId   - Filter Id                      */
/*                                                                           */
/*    Returns             : FSB_SUCCESS / FSB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsFsbMbsmHwWrCreateFilter (tFsbFilterEntry * pFsbFilterEntry,
                           UINT4 *pu4HwFilterId)
{
#ifdef MBSM_WANTED
    tFsbHwFilterEntry   FsbHwFilterEntry;

    FSB_MEMSET (&FsbHwFilterEntry, 0, sizeof (tFsbHwFilterEntry));

    FsbHwFilterEntry.u4ContextId = pFsbFilterEntry->u4ContextId;
    FsbHwFilterEntry.u4AggIndex = pFsbFilterEntry->u4AggIndex;
    FSB_MEMCPY (FsbHwFilterEntry.PortList, pFsbFilterEntry->PortList,
                sizeof (tPortList));
    FSB_MEMCPY (FsbHwFilterEntry.DstMac, pFsbFilterEntry->DstMac, MAC_ADDR_LEN);
    FSB_MEMCPY (FsbHwFilterEntry.SrcMac, pFsbFilterEntry->SrcMac, MAC_ADDR_LEN);
    FsbHwFilterEntry.u2VlanId = pFsbFilterEntry->u2VlanId;
    FsbHwFilterEntry.u2ClassId = pFsbFilterEntry->u2ClassId;
    FsbHwFilterEntry.u2Ethertype = pFsbFilterEntry->u2Ethertype;
    FsbHwFilterEntry.u2OpcodeFilterOffsetValue =
        pFsbFilterEntry->u2OpcodeFilterOffsetValue;
    FsbHwFilterEntry.u2SubOpcodeFilterOffsetValue =
        pFsbFilterEntry->u2SubOpcodeFilterOffsetValue;
    FsbHwFilterEntry.u1FilterAction = pFsbFilterEntry->u1FilterAction;
    FsbHwFilterEntry.u1FilterType = pFsbFilterEntry->u1FilterType;
    FsbHwFilterEntry.u1Priority = pFsbFilterEntry->u1Priority;

    if (FsbFsFsbMbsmHwCreateFilter (&FsbHwFilterEntry, pu4HwFilterId) !=
        FNP_SUCCESS)
    {
        FSB_CONTEXT_TRC (pFsbFilterEntry->u4ContextId, FSB_ERROR_LEVEL,
                         FSB_NONE,
                         "FsFsbMbsmHwWrCreateFilter of Filter Entry with VlanId: %d"
                         ": FsbFsFsbMbsmHwCreateFilter failed\r\n",
                         pFsbFilterEntry->u2VlanId);
        return FSB_FAILURE;
    }
    return FSB_SUCCESS;
#else
    UNUSED_PARAM (pu4HwFilterId);
    UNUSED_PARAM (pFsbFilterEntry);
    return FSB_SUCCESS;
#endif
}
#endif /* _FSBWRAP_C_ */
