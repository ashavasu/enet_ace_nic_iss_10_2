/*************************************************************************
 * * Copyright (C) 2007-2015 Aricent Group . All Rights Reserved
 *
 * $Id: fsbnpapi.c,v 1.4 2017/10/09 13:13:58 siva Exp $
 *
 * Description: All Network Processor API Function calls are done here
 *
 * *************************************************************************/
#ifndef _FSBNPAPI_C_
#define _FSBNPAPI_C_

#include "fsbinc.h"
#include "nputil.h"
#include "npapi.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : FsbFsMiNpFsbHwInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiNpFsbHwInit
 *                                                                          
 *    Input(s)            : Arguments of FsMiNpFsbHwInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
FsbFsMiNpFsbHwInit (tFsbHwFilterEntry * pFsbHwFilterEntry,
                    tFsbLocRemFilterId * pFsbLocRemFilterId)
{
    tFsHwNp             FsHwNp;
    tFsbNpModInfo      *pFsbNpModInfo = NULL;
    tFsbNpWrFsMiNpFsbHwInit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_FSB_MOD,    /* Module ID */
                         FS_MI_NP_FSB_HW_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pFsbNpModInfo = &(FsHwNp.FsbNpModInfo);
    pEntry = &pFsbNpModInfo->FsbNpFsMiNpFsbHwInit;

    pEntry->pFsbHwFilterEntry = pFsbHwFilterEntry;
    pEntry->pFsbLocRemFilterId = pFsbLocRemFilterId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : FsbFsFsbHwCreateFilter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsFsbHwCreateFilter
 *                                                                          
 *    Input(s)            : Arguments of FsFsbHwCreateFilter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
FsbFsFsbHwCreateFilter (tFsbHwFilterEntry * pFsbHwFilterEntry,
                        UINT4 *pu4FilterId)
{
    tFsHwNp             FsHwNp;
    tFsbNpModInfo      *pFsbNpModInfo = NULL;
    tFsbNpWrFsFsbHwCreateFilter *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_FSB_MOD,    /* Module ID */
                         FS_FSB_HW_CREATE_FILTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pFsbNpModInfo = &(FsHwNp.FsbNpModInfo);
    pEntry = &pFsbNpModInfo->FsbNpFsFsbHwCreateFilter;

    pEntry->pFsbHwFilterEntry = pFsbHwFilterEntry;
    pEntry->pu4FilterId = pu4FilterId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : FsbFsFsbHwDeleteFilter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsFsbHwDeleteFilter
 *                                                                          
 *    Input(s)            : Arguments of FsFsbHwDeleteFilter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
FsbFsFsbHwDeleteFilter (tFsbHwFilterEntry * pFsbHwFilterEntry, UINT4 u4FilterId)
{
    tFsHwNp             FsHwNp;
    tFsbNpModInfo      *pFsbNpModInfo = NULL;
    tFsbNpWrFsFsbHwDeleteFilter *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_FSB_MOD,    /* Module ID */
                         FS_FSB_HW_DELETE_FILTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pFsbNpModInfo = &(FsHwNp.FsbNpModInfo);
    pEntry = &pFsbNpModInfo->FsbNpFsFsbHwDeleteFilter;

    pEntry->pFsbHwFilterEntry = pFsbHwFilterEntry;
    pEntry->u4FilterId = u4FilterId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : FsbFsMiNpFsbHwDeInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiNpFsbHwDeInit
 *                                                                          
 *    Input(s)            : Arguments of FsMiNpFsbHwDeInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
FsbFsMiNpFsbHwDeInit (tFsbHwFilterEntry * pFsbHwFilterEntry,
                      tFsbLocRemFilterId FsbLocRemFilterId)
{
    tFsHwNp             FsHwNp;
    tFsbNpModInfo      *pFsbNpModInfo = NULL;
    tFsbNpWrFsMiNpFsbHwDeInit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_FSB_MOD,    /* Module ID */
                         FS_MI_NP_FSB_HW_DE_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pFsbNpModInfo = &(FsHwNp.FsbNpModInfo);
    pEntry = &pFsbNpModInfo->FsbNpFsMiNpFsbHwDeInit;

    pEntry->pFsbHwFilterEntry = pFsbHwFilterEntry;
    pEntry->FsbLocRemFilterId = FsbLocRemFilterId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : FsbFsFsbHwCreateSChannelFilter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsFsbHwCreateSChannelFilter
 *                                                                          
 *    Input(s)            : Arguments of FsFsbHwCreateSChannelFilter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
FsbFsFsbHwCreateSChannelFilter (tFsbHwSChannelFilterEntry *
                                pFsbHwSChannelFilterEntry, UINT4 *pu4FilterId)
{
    tFsHwNp             FsHwNp;
    tFsbNpModInfo      *pFsbNpModInfo = NULL;
    tFsbNpWrFsFsbHwCreateSChannelFilter *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_FSB_MOD,    /* Module ID */
                         FS_FSB_HW_CREATE_S_CHANNEL_FILTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pFsbNpModInfo = &(FsHwNp.FsbNpModInfo);
    pEntry = &pFsbNpModInfo->FsbNpFsFsbHwCreateSChannelFilter;

    pEntry->pFsbHwSChannelFilterEntry = pFsbHwSChannelFilterEntry;
    pEntry->pu4FilterId = pu4FilterId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : FsbFsFsbHwDeleteSChannelFilter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsFsbHwDeleteSChannelFilter
 *                                                                          
 *    Input(s)            : Arguments of FsFsbHwDeleteSChannelFilter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
FsbFsFsbHwDeleteSChannelFilter (tFsbHwSChannelFilterEntry *
                                pFsbHwSChannelFilterEntry, UINT4 u4FilterId)
{
    tFsHwNp             FsHwNp;
    tFsbNpModInfo      *pFsbNpModInfo = NULL;
    tFsbNpWrFsFsbHwDeleteSChannelFilter *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_FSB_MOD,    /* Module ID */
                         FS_FSB_HW_DELETE_S_CHANNEL_FILTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pFsbNpModInfo = &(FsHwNp.FsbNpModInfo);
    pEntry = &pFsbNpModInfo->FsbNpFsFsbHwDeleteSChannelFilter;

    pEntry->pFsbHwSChannelFilterEntry = pFsbHwSChannelFilterEntry;
    pEntry->u4FilterId = u4FilterId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : FsbFsFsbHwSetFCoEParams                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsFsbHwSetFCoEParams
 *                                                                          
 *    Input(s)            : Arguments of FsFsbHwSetFCoEParams
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
FsbFsFsbHwSetFCoEParams (tFsbHwVlanEntry * pFsbHwVlanEntry)
{
    tFsHwNp             FsHwNp;
    tFsbNpModInfo      *pFsbNpModInfo = NULL;
    tFsbNpWrFsFsbHwSetFCoEParams *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_FSB_MOD,    /* Module ID */
                         FS_FSB_HW_SET_FCOE_PARAMS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pFsbNpModInfo = &(FsHwNp.FsbNpModInfo);
    pEntry = &pFsbNpModInfo->FsbNpFsFsbHwSetFCoEParams;

    pEntry->pFsbHwVlanEntry = pFsbHwVlanEntry;

    return (NpUtilHwProgram (&FsHwNp));
}

#ifdef MBSM_WANTED
/***************************************************************************
 *                                                                          
 *    Function Name       : FsbFsMiNpFsbMbsmHwInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiNpFsbMbsmHwInit
 *                                                                          
 *    Input(s)            : Arguments of FsMiNpFsbMbsmHwInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
FsbFsMiNpFsbMbsmHwInit (tFsbHwFilterEntry * pFsbHwFilterEntry,
                        tFsbLocRemFilterId * pFsbLocRemFilterId)
{
    tFsHwNp             FsHwNp;
    tFsbNpModInfo      *pFsbNpModInfo = NULL;
    tFsbNpWrFsMiNpFsbMbsmHwInit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_FSB_MOD,    /* Module ID */
                         FS_MI_NP_FSB_MBSM_HW_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pFsbNpModInfo = &(FsHwNp.FsbNpModInfo);
    pEntry = &pFsbNpModInfo->FsbNpFsMiNpFsbMbsmHwInit;

    pEntry->pFsbHwFilterEntry = pFsbHwFilterEntry;
    pEntry->pFsbLocRemFilterId = pFsbLocRemFilterId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : FsbFsFsbMbsmHwCreateFilter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsFsbMbsmHwCreateFilter
 *                                                                          
 *    Input(s)            : Arguments of FsFsbMbsmHwCreateFilter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
FsbFsFsbMbsmHwCreateFilter (tFsbHwFilterEntry * pFsbHwFilterEntry,
                            UINT4 *pu4FilterId)
{
    tFsHwNp             FsHwNp;
    tFsbNpModInfo      *pFsbNpModInfo = NULL;
    tFsbNpWrFsFsbMbsmHwCreateFilter *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_FSB_MOD,    /* Module ID */
                         FS_FSB_MBSM_HW_CREATE_FILTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pFsbNpModInfo = &(FsHwNp.FsbNpModInfo);
    pEntry = &pFsbNpModInfo->FsbNpFsFsbMbsmHwCreateFilter;

    pEntry->pFsbHwFilterEntry = pFsbHwFilterEntry;
    pEntry->pu4FilterId = pu4FilterId;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif
#endif /* _FSBNPAPI_C_ */
