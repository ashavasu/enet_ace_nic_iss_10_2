/*************************************************************************
* Copyright (C) 2007-2015 Aricent Group . All Rights Reserved
*
* $Id: fsbcli.c,v 1.13 2017/10/09 13:13:58 siva Exp $
*
* Description: This file contains the FIP-snooping CLI related routines
*
*************************************************************************/
#ifndef _FSBCLI_C_
#define _FSBCLI_C_

#include "fsbinc.h"
#include "fsfsbcli.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : cli_process_fsb_cmd                              */
/*                                                                           */
/*    Description         : This function takes in variable no. of arguments */
/*                          and process the commands for the FIP-snooping    */
/*                          module.                                          */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          u4Command -  Command Identifier                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
cli_process_fsb_command (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[CLI_MAX_ARGS] = { NULL };
    UINT1               au1Fcmap[FSB_FCMAP_LEN];
    UINT4               u4ContextId = CLI_GET_CXT_ID ();
    UINT4               u4HouseKeepingTime = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RetStatus = 0;
    INT4                i4IfIndex = 0;
    INT4                i4VlanId = 0;
    UINT1               u1Argno = 0;
    tPortList          *pMemberPorts = NULL;

    tSNMP_OCTET_STRING_TYPE FcMap;

    FSB_MEMSET (&FcMap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    FSB_MEMSET (au1Fcmap, 0, FSB_FCMAP_LEN);

    FcMap.pu1_OctetList = &au1Fcmap[0];
    FcMap.i4_Length = 0;

    /* Check if the command is a switch mode command */
    if (u4ContextId == FSB_CLI_INVALID_CONTEXT)
    {
        u4ContextId = FSB_DEFAULT_CONTEXT_ID;
    }

    va_start (ap, u4Command);

    /* Walk through the rest of the arguements and store in args array.
     * Store CLI_MAX_ARGS arguements at the max */

    FSB_MEMSET (args, 0, sizeof (args));
    while (u1Argno < CLI_MAX_ARGS)
    {
        args[u1Argno++] = va_arg (ap, UINT1 *);
    }

    va_end (ap);
    CLI_SET_ERR (0);

    CliRegisterLock (CliHandle, FsbLock, FsbUnLock);
    FsbLock ();

    switch (u4Command)
    {
            /* Configuration Commands */
            /* Configuration commands corresponding to fsMIFsbContextTable MIB Table */
        case CLI_FSB_SHUTDOWN:
            i4RetStatus = FsbCliSetSystemControl (CliHandle,
                                                  u4ContextId, FSB_SHUTDOWN);
            break;

        case CLI_FSB_START:
            i4RetStatus = FsbCliSetSystemControl (CliHandle,
                                                  u4ContextId, FSB_START);
            break;

        case CLI_FSB_MODULE_STATUS:
            i4RetStatus = FsbCliSetModuleStatus (CliHandle,
                                                 u4ContextId,
                                                 CLI_PTR_TO_I4 (args[1]));
            break;

        case CLI_FSB_FCMAP_MODE:
            i4RetStatus = FsbCliSetFcMapMode (CliHandle,
                                              u4ContextId,
                                              CLI_PTR_TO_I4 (args[1]));
            break;

        case CLI_FSB_SET_FCMAP:
            if (args[1] != NULL)
            {
                CLI_CONVERT_DOT_STR_TO_ARRAY ((UINT1 *) args[1],
                                              FcMap.pu1_OctetList);
                FcMap.i4_Length = FSB_FCMAP_LEN;
            }
            else
            {
                /* If No value is given , restore Default FC-MAP Value */
                FcMap.i4_Length = FSB_FCMAP_LEN;
                FSB_MEMCPY (FcMap.pu1_OctetList, gau1DefaultFcMapValue,
                            FSB_FCMAP_LEN);
            }
            i4RetStatus = FsbCliSetFcMap (CliHandle, u4ContextId, &FcMap);
            break;
        case CLI_FSB_HOUSE_KEEPING_PERIOD:

            if (args[1] != NULL)
            {
                FSB_MEMCPY (&u4HouseKeepingTime, args[1], sizeof (UINT4));
            }
            i4RetStatus = FsbCliSetHouseKeepingTime (CliHandle,
                                                     u4ContextId,
                                                     u4HouseKeepingTime);
            break;

        case CLI_FSB_DEBUG:
            if (args[1] != NULL)
            {
                if (FsbPortIsSwitchExist (args[1], &u4ContextId) != OSIX_TRUE)
                {
                    CliPrintf (CliHandle, "\r%% Switch %s Does not "
                               "exist.\r\n", args[1]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            i4RetStatus = FsbCliSetDebugs (CliHandle,
                                           u4ContextId,
                                           CLI_PTR_TO_I4 (args[2]),
                                           CLI_PTR_TO_I4 (args[3]));
            break;

        case CLI_FSB_SEVERITY_LEVEL:
            if (args[1] != NULL)
            {
                i4RetStatus = FsbCliSetSeverityLevel (CliHandle, u4ContextId,
                                                      CLI_PTR_TO_U4 (args[1]));
            }
            else
            {
                i4RetStatus = FsbCliSetSeverityLevel (CliHandle, u4ContextId,
                                                      CLI_PTR_TO_U4 (args[2]));
            }

            break;

        case CLI_FSB_TRAP_STATUS:
            i4RetStatus = FsbCliSetTrapStatus (CliHandle,
                                               u4ContextId,
                                               CLI_PTR_TO_I4 (args[1]));
            break;

        case CLI_FSB_CLEAR_STATS:
            if (args[1] != NULL)
            {
                if (FsbPortIsSwitchExist (args[1], &u4ContextId) != OSIX_TRUE)
                {
                    CliPrintf (CliHandle, "\r%% Switch %s Does not "
                               "exist.\r\n", args[1]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            i4RetStatus = FsbCliClearStats (CliHandle, u4ContextId);
            break;

        case CLI_FSB_UNTAGGED_VLAN:
            if (args[1] != NULL)
            {
                FSB_MEMCPY (&i4VlanId, args[1], sizeof (INT4));
            }

            i4RetStatus = FsbCliSetUntaggedVlan (CliHandle,
                                                 u4ContextId, i4VlanId);
            break;
            /* Configuration commands corresponding to fsMIFsbFIPSnoopingTable MIB Table */
        case CLI_FSB_SET_FCOE_VLAN:
            if (args[1] != NULL)
            {
                FSB_MEMCPY (&i4VlanId, args[1], sizeof (INT4));
            }
            i4RetStatus = FsbCliSetFCoEVlan (CliHandle,
                                             u4ContextId,
                                             i4VlanId, CLI_PTR_TO_I4 (args[2]));
            break;
        case CLI_FSB_SET_VLAN_STATUS:
            if (args[1] != NULL)
            {
                FSB_MEMCPY (&i4VlanId, args[1], sizeof (INT4));
            }
            i4RetStatus = FsbCliSetVlanEnabledStatus (CliHandle,
                                                      u4ContextId,
                                                      i4VlanId,
                                                      CLI_PTR_TO_I4 (args[2]));
            break;

        case CLI_FSB_SET_VLAN_FCMAP:
            if (args[1] != NULL)
            {
                FSB_MEMCPY (&i4VlanId, args[1], sizeof (INT4));
            }

            if (args[2] != NULL)
            {
                CLI_CONVERT_DOT_STR_TO_ARRAY ((UINT1 *) args[2],
                                              FcMap.pu1_OctetList);
                FcMap.i4_Length = FSB_FCMAP_LEN;
            }
            else
            {
                /* If No value is given , restore Default FC-MAP Value */
                FcMap.i4_Length = FSB_FCMAP_LEN;
                FSB_MEMCPY (FcMap.pu1_OctetList, gau1DefaultFcMapValue,
                            FSB_FCMAP_LEN);
            }

            i4RetStatus = FsbCliSetVlanFcMap (CliHandle,
                                              u4ContextId, i4VlanId, &FcMap);
            break;

            /* Configuration commands corresponding to fsMIFsbIntfTable MIB Table */
        case CLI_FSB_PORT_ROLE:
            i4IfIndex = CLI_GET_IFINDEX ();
            if (args[1] != NULL)
            {
                FSB_MEMCPY (&i4VlanId, args[1], sizeof (INT4));
            }
            i4RetStatus = FsbCliSetIntfPortRole (CliHandle,
                                                 i4VlanId,
                                                 i4IfIndex,
                                                 CLI_PTR_TO_I4 (args[2]));
            break;

        case CLI_FSB_ADD_PINNED_PORTS:
            /* args[1] - FCoE Vlan Id */
            /* args[2] - interface type for member ports */
            /* args[3] - interface identifier for member ports */
            if (args[1] != NULL)
            {
                FSB_MEMCPY (&i4VlanId, args[1], sizeof (INT4));
            }
            pMemberPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pMemberPorts == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% Error in Allocating memory for bitlist \r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            if (args[3] != NULL)
            {
                i4RetStatus =
                    FsbCfaCliGetIfList ((INT1 *) args[2], (INT1 *) args[3],
                                        *pMemberPorts, sizeof (tPortList));
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Invalid Member" " Port(s) \r\n");
                }
            }

            i4RetStatus = FsbCliSetFCoEVlanPinnedPorts (CliHandle, u4ContextId,
                                                        i4VlanId,
                                                        (UINT1 *) pMemberPorts);
            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            break;

        case CLI_FSB_DELETE_PINNED_PORTS:

            pMemberPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pMemberPorts == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% Error in Allocating memory for bitlist \r\n");
                i4RetStatus = CLI_FAILURE;
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                break;
            }
            FSB_MEMSET (*pMemberPorts, 0, sizeof (tPortList));
            if (args[1] != NULL)
            {
                FSB_MEMCPY (&i4VlanId, args[1], sizeof (INT4));
            }

            i4RetStatus = FsbCliSetFCoEVlanPinnedPorts (CliHandle, u4ContextId,
                                                        i4VlanId,
                                                        (UINT1 *) pMemberPorts);
            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            break;
            /* Show Commands */
            /* Show command corresponding to fsMIFsbContextTable MIB Table */
        case CLI_FSB_SHOW_FIP_STATUS:

            i4RetStatus = FsbCliShowStatusInfo (CliHandle, args[1]);

            break;

            /* Show command corresponding to fsMIFsbFIPSnoopingTable MIB Table */
        case CLI_FSB_SHOW_VLAN:
            if (args[2] != NULL)
            {
                FSB_MEMCPY (&i4VlanId, args[2], sizeof (INT4));
            }
            i4RetStatus = FsbCliShowFIPSnoopingVlan (CliHandle,
                                                     args[1], i4VlanId);
            break;

            /* Show command corresponding to fsMIFsbIntfTable MIB Table */
        case CLI_FSB_SHOW_INT:
            if (args[1] != NULL)
            {
                FSB_MEMCPY (&i4VlanId, args[1], sizeof (INT4));
            }
            i4RetStatus = FsbCliShowInterface (CliHandle,
                                               i4VlanId,
                                               CLI_PTR_TO_I4 (args[2]));
            break;

            /* Show command corresponding to fsMIFsbFIPSessionTable MIB Table */
        case CLI_FSB_SHOW_FIP_SESSION:
            if (args[1] != NULL)
            {
                FSB_MEMCPY (&i4VlanId, args[1], sizeof (INT4));
            }
            i4RetStatus = FsbCliShowFIPSession (CliHandle,
                                                i4VlanId,
                                                CLI_PTR_TO_I4 (args[2]));
            break;

            /* Show command correspoding to fsMIFsbFcfTable MIB Table */
        case CLI_FSB_SHOW_FIP_FCF:
            if (args[1] != NULL)
            {
                FSB_MEMCPY (&i4VlanId, args[1], sizeof (INT4));
            }
            i4RetStatus = FsbCliShowFcf (CliHandle,
                                         i4VlanId, CLI_PTR_TO_I4 (args[2]));
            break;

            /* Show command corresponding to fsMIFsbGlobalStatsTable MIB Table */
        case CLI_FSB_SHOW_GLOBAL_STATS:
            i4RetStatus = FsbCliShowGlobalStatistics (CliHandle, args[1]);

            break;

            /* Show command corresponding to fsMIFsbVlanStatsTable MIB Table */
        case CLI_FSB_SHOW_VLAN_STATS:
            if (args[2] != NULL)
            {
                FSB_MEMCPY (&i4VlanId, args[2], sizeof (INT4));
            }
            i4RetStatus = FsbCliShowVlanStatistics (CliHandle,
                                                    args[1], i4VlanId);

            break;

            /* Show command corresponding to fsMIFsbSessStatsTable MIB Table */
        case CLI_FSB_SHOW_SESSION_STATS:
            if (args[1] != NULL)
            {
                FSB_MEMCPY (&i4VlanId, args[1], sizeof (INT4));
            }
            i4RetStatus = FsbCliShowSessionStatistics (CliHandle,
                                                       i4VlanId,
                                                       CLI_PTR_TO_I4 (args[2]));
            break;
#ifdef FSB_RED_DEBUG
            /* The below case statements will be removed later after testing */
            /* Show command to display filter details in tFsbContextInfo Table */
        case CLI_FSB_SHOW_CXT_FILTER:
            FsbCliShowContextFilterEntry (CliHandle);
            break;

            /* Show command to display filter details in tFsbFipSnoopingEntry Table */
        case CLI_FSB_SHOW_VLAN_FILTER:
            FsbCliShowVlanFilterEntry (CliHandle);
            break;

            /* Show command to display filter details in  tFsbFipSessEntry and 
             * tFsbFipSessFCoEEntry Table */
        case CLI_FSB_SHOW_SESSION_FILTER:
            FsbCliShowSessionFilterEntry (CliHandle);
            break;

            /* Show command to display filter details of S-Channel filter */
        case CLI_FSB_SHOW_SCHANNEL_FILTER:
            FsbCliShowSchannelFilterEntry (CliHandle);
            break;

            /* Show command to display filter details of FCF */
        case CLI_FSB_SHOW_FCF_FILTER:
            FsbCliShowFcfFilterEntry (CliHandle);
            break;

            /* Show command to display the session entries */
        case CLI_FSB_SHOW_SESSION_ENTRIES:
            i4RetStatus = FsbCliShowSessionEntries (CliHandle);
            break;
#endif
        default:
            CliPrintf (CliHandle, "ERROR: Unknown Command !\r\n");
            i4RetStatus = CLI_ERROR;

    }

    /* To print Error string in case of failure */
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_FSB_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", FsbCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    FsbUnLock ();
    CliUnRegisterLock (CliHandle);

    return i4RetStatus;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliSetSystemControl                           */
/*                                                                           */
/*    Description         : This function sets the system control status of  */
/*                          FSB module as configured from CLI.               */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          u4ContextId - Context Identifier                 */
/*                          i4SystemControl - FSB_START / FSB_SHUTDOWN       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliSetSystemControl (tCliHandle CliHandle, UINT4 u4ContextId,
                        INT4 i4SystemControl)
{
    INT4                i4RowStatus = -1;
    UINT4               u4ErrorCode = 0;

    if (i4SystemControl == FSB_START)
    {
        if (nmhGetFsMIFsbRowStatus (u4ContextId, &i4RowStatus) != SNMP_SUCCESS)
        {
            /* Create FSB context, if it does not exist */
            if (nmhTestv2FsMIFsbRowStatus (&u4ErrorCode, u4ContextId,
                                           CREATE_AND_GO) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsMIFsbRowStatus (u4ContextId, CREATE_AND_GO)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
        }
    }

    if (nmhTestv2FsMIFsbSystemControl (&u4ErrorCode, u4ContextId,
                                       i4SystemControl) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMIFsbSystemControl (u4ContextId, i4SystemControl)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* When System Control is shutdown, delete the FSB context table entry */
    if (i4SystemControl == FSB_SHUTDOWN)
    {
        if (nmhTestv2FsMIFsbRowStatus (&u4ErrorCode, u4ContextId,
                                       DESTROY) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsMIFsbRowStatus (u4ContextId, DESTROY) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliSetModuleStatus                            */
/*                                                                           */
/*    Description         : This function sets the FSB module status for the */
/*                          Context                                          */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          u4ContextId - Context Identifier                 */
/*                          i4Status - FSB_ENABLE / FSB_DIASABLE             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliSetModuleStatus (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMIFsbModuleStatus (&u4ErrorCode, u4ContextId,
                                      i4Status) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMIFsbModuleStatus (u4ContextId, i4Status) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliSetFcMapMode                               */
/*                                                                           */
/*    Description         : This function sets FC-MAP mode supported for     */
/*                          teh Context                                      */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          u4ContextId - Context Identifier                 */
/*                          i4FcMapMode - FSB_FCMAP_GLOBAL / FSB_FCMAP_VLAN  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliSetFcMapMode (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4FcMapMode)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMIFsbFcMapMode (&u4ErrorCode, u4ContextId,
                                   i4FcMapMode) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMIFsbFcMapMode (u4ContextId, i4FcMapMode) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliSetFcMap                                   */
/*                                                                           */
/*    Description         : This function set FC-MAP value for the Context   */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          u4ContextId - Context Identifier                 */
/*                          pFsbFcMap - FC-MAP                               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/

INT4
FsbCliSetFcMap (tCliHandle CliHandle, UINT4 u4ContextId,
                tSNMP_OCTET_STRING_TYPE * pFsbFcMap)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMIFsbFcmap (&u4ErrorCode, u4ContextId,
                               pFsbFcMap) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsMIFsbFcmap (u4ContextId, pFsbFcMap) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliSetHouseKeepingTime                        */
/*                                                                           */
/*    Description         : This function sets the Housekeeping timer period */
/*                          for the context                                  */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          u4ContextId - Context Identifier                 */
/*                          i4HouseKeepingTime - Housekeeping Timer Period   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliSetHouseKeepingTime (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 u4HouseKeepingTime)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMIFsbHouseKeepingTimePeriod (&u4ErrorCode, u4ContextId,
                                                u4HouseKeepingTime) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsMIFsbHouseKeepingTimePeriod (u4ContextId, u4HouseKeepingTime)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliSetDebugs                                  */
/*                                                                           */
/*    Description         : This function sets the trace value               */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          u4ContextId - Context Identifier                 */
/*                          i4CliTraceVal - Trace Level to be set            */
/*                          u1TraceFlag - FSB_ENABLE / FSB_DISABLE           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliSetDebugs (tCliHandle CliHandle, UINT4 u4ContextId,
                 INT4 i4CliTraceVal, INT4 u1TraceFlag)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4TraceVal = 0;
    INT4                i4RetVal = 0;

    nmhGetFsMIFsbTraceOption (u4ContextId, &i4TraceVal);

    if (i4CliTraceVal == (FSB_TRC_ALL))
    {
        /* Warning message to enable the all FSB traces */
        i4RetVal = CliDisplayMessageAndUserPromptResponse
            ("This will enable all FSB traces ", 1, FsbLock, FsbUnLock);
        if (i4RetVal != CLI_SUCCESS)
        {
            return CLI_SUCCESS;
        }

    }

    if (u1TraceFlag == FSB_ENABLE)
    {
        i4TraceVal |= i4CliTraceVal;
    }

    else
    {
        i4TraceVal &= (~(i4CliTraceVal));
        i4TraceVal |= 0;
    }

    if (nmhTestv2FsMIFsbTraceOption (&u4ErrorCode, u4ContextId, i4TraceVal) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMIFsbTraceOption (u4ContextId, i4TraceVal) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliSetSeverityLevel                           */
/*                                                                           */
/*    Description         : This function sets the severity level for the    */
/*                          for the traces in the context                    */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          u4ContextId - Context Identifier                 */
/*                          u4TraceSeverityLevel - Severity level for traces */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliSetSeverityLevel (tCliHandle CliHandle, UINT4 u4ContextId,
                        UINT4 u4TraceSeverityLevel)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMIFsbTraceSeverityLevel (&u4ErrorCode, u4ContextId,
                                            (INT4) u4TraceSeverityLevel) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsMIFsbTraceSeverityLevel
        (u4ContextId, (INT4) u4TraceSeverityLevel) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliSetTrapStatus                              */
/*                                                                           */
/*    Description         : This function sets the trap status               */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          u4ContextId - Context Identifier                 */
/*                          i4TrapStatus - Trap Status                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliSetTrapStatus (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4TrapStatus)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMIFsbTrapStatus (&u4ErrorCode, u4ContextId,
                                    i4TrapStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsMIFsbTrapStatus (u4ContextId, i4TrapStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliClearStats                                 */
/*                                                                           */
/*    Description         : This function clears FIP-snooping counters for   */
/*                          the context                                      */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliClearStats (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMIFsbClearStats (&u4ErrorCode, u4ContextId,
                                    FSB_TRUE) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsMIFsbClearStats (u4ContextId, FSB_TRUE) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliSetUntaggedVlan                            */
/*                                                                           */
/*    Description         : This function is used to configure a VLAN as     */
/*                          FIP-snooping Untagged VLAN                       */
/*                          the context                                      */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          u4ContextId - Context Identifier                 */
/*                          i4VlanIndex - VLAN Index                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliSetUntaggedVlan (tCliHandle CliHandle, UINT4 u4ContextId,
                       INT4 i4VlanIndex)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMIFsbDefaultVlanId (&u4ErrorCode, u4ContextId,
                                       i4VlanIndex) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsMIFsbDefaultVlanId (u4ContextId, i4VlanIndex) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliSetFCoEVlan                                */
/*                                                                           */
/*    Description         : This function is used to configure a VLAN as     */
/*                          FCoE VLAN                                        */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          u4ContextId - Context Identifier                 */
/*                          i4VlanIndex - Vlan Index                         */
/*                          i4Status - FSB_ENABLE / FSB_DISABLE              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliSetFCoEVlan (tCliHandle CliHandle, UINT4 u4ContextId,
                   INT4 i4VlanIndex, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (i4Status == FSB_ENABLE)
    {
        if (nmhTestv2FsMIFsbFIPSnoopingRowStatus (&u4ErrorCode, u4ContextId,
                                                  i4VlanIndex,
                                                  CREATE_AND_GO) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsMIFsbFIPSnoopingRowStatus (u4ContextId, i4VlanIndex,
                                               CREATE_AND_GO) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsMIFsbFIPSnoopingRowStatus (&u4ErrorCode, u4ContextId,
                                                  i4VlanIndex,
                                                  DESTROY) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsMIFsbFIPSnoopingRowStatus (u4ContextId, i4VlanIndex,
                                               DESTROY) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }

    UNUSED_PARAM (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliSetVlanEnabledStatus                       */
/*                                                                           */
/*    Description         : This function is used to enable FIP-snooping     */
/*                          functionality for the FCoE VLAN                  */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          u4ContextId - Context Identifier                 */
/*                          i4VlanIndex - Vlan Index                         */
/*                          i4Status - FSB_ENABLE / FSB_DISABLE              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliSetVlanEnabledStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                            INT4 i4VlanIndex, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMIFsbFIPSnoopingEnabledStatus (&u4ErrorCode, u4ContextId,
                                                  i4VlanIndex,
                                                  i4Status) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsMIFsbFIPSnoopingEnabledStatus (u4ContextId, i4VlanIndex,
                                               i4Status) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliSetVlanFcMap                               */
/*                                                                           */
/*    Description         : This function is used to set FC-MAP value for    */
/*                          FCoE VLAN                                        */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          u4ContextId - Context Identifier                 */
/*                          i4VlanIndex - VLAN Index                         */
/*                          pFsbFcMap - FC-MAP                               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliSetVlanFcMap (tCliHandle CliHandle, UINT4 u4ContextId,
                    INT4 i4VlanIndex, tSNMP_OCTET_STRING_TYPE * pFsbFcMap)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMIFsbFIPSnoopingFcmap (&u4ErrorCode, u4ContextId,
                                          i4VlanIndex,
                                          pFsbFcMap) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsMIFsbFIPSnoopingFcmap (u4ContextId, i4VlanIndex,
                                       pFsbFcMap) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliSetIntfPortRole                            */
/*                                                                           */
/*    Description         : This function is used to set the port role of a  */
/*                          interface                                        */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          i4VlanIndex - Vlan Index                         */
/*                          i4IfIndex - Interface Index                      */
/*                          i4Status - FSB_ENODE_FACING / FSB_FCF_FACING /   */
/*                                     FSB_ENODE_FCF_FACING                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliSetIntfPortRole (tCliHandle CliHandle, INT4 i4VlanIndex,
                       INT4 i4IfIndex, INT4 i4PortRole)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMIFsbIntfPortRole (&u4ErrorCode, i4VlanIndex,
                                      i4IfIndex, i4PortRole) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsMIFsbIntfPortRole (i4VlanIndex, i4IfIndex,
                                   i4PortRole) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliSetFCoEVlanPinnedPorts                     */
/*                                                                           */
/*    Description         : This function is used to set the pinned ports    */
/*                          of an FCoE configured VLAN                       */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          i4VlanIndex - Vlan Index                         */
/*                          i4IfIndex - Interface Index                      */
/*                          i4Status - FSB_ENODE_FACING / FSB_FCF_FACING /   */
/*                                     FSB_ENODE_FCF_FACING                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliSetFCoEVlanPinnedPorts (tCliHandle CliHandle, UINT4 u4ContextId,
                              INT4 i4VlanIndex, UINT1 *pu1MemberPorts)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE FsbPinnedPorts;

    FSB_MEMSET (&FsbPinnedPorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    FsbPinnedPorts.i4_Length = BRG_PORT_LIST_SIZE;
    if (pu1MemberPorts != NULL)
    {
        FsbPinnedPorts.pu1_OctetList = pu1MemberPorts;
    }

    if (nmhTestv2FsMIFsbFIPSnoopingPinnedPorts
        (&u4ErrorCode, u4ContextId, i4VlanIndex,
         &FsbPinnedPorts) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMIFsbFIPSnoopingPinnedPorts (u4ContextId, i4VlanIndex,
                                             &FsbPinnedPorts) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliShowStatusInfo                             */
/*                                                                           */
/*    Description         : This function display the status for all or a    */
/*                          specified Context                                */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          pu1ContextName - Context Name                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliShowStatusInfo (tCliHandle CliHandle, UINT1 *pu1ContextName)
{
    tSNMP_OCTET_STRING_TYPE FcMap;
    UINT1               au1ContextName[FSB_ALIAS_MAX_LEN];
    UINT1               au1FcMap[FSB_FCMAP_LEN];
    UINT4               u4HouseKeepingTime = 0;
    UINT4               u4CurrContextId = 0;
    UINT4               u4ContextId = 0;
    INT4                i4DefaultVlanId = 0;
    INT4                i4SystemControl = 0;
    INT4                i4ModuleStatus = 0;
    INT4                i4TraceOption = 0;
    INT4                i4TrapStatus = 0;
    INT4                i4FcMapMode = 0;
    INT4                i4TraceSeverityLevel = 0;

    FSB_MEMSET (&FcMap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    /* If Switch-name is given then get the Context-Id from it */
    if (pu1ContextName != NULL)
    {
        if (FsbPortIsSwitchExist (pu1ContextName, &u4ContextId) != FSB_TRUE)
        {
            CliPrintf (CliHandle, "\r%% Switch %s Does not exist.\r\n",
                       pu1ContextName);
            return CLI_FAILURE;
        }
        if (nmhValidateIndexInstanceFsMIFsbContextTable (u4ContextId)
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% FSB Module is ShutDown for"
                       " Switch %s\r\n", pu1ContextName);
            return CLI_FAILURE;
        }
        u4CurrContextId = u4ContextId;
    }
    else
    {
        if (nmhGetFirstIndexFsMIFsbContextTable (&u4ContextId) != SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }
    do
    {
        /* To break the loop if Switch name is given */
        if ((pu1ContextName != NULL) && (u4CurrContextId != u4ContextId))
        {
            break;
        }

        FSB_MEMSET (au1ContextName, 0, sizeof (au1ContextName));
        FSB_MEMSET (au1FcMap, 0, sizeof (au1FcMap));
        FcMap.pu1_OctetList = au1FcMap;
        FcMap.i4_Length = 0;

        FsbPortGetSwitchAliasName (u4ContextId, au1ContextName);

        CliPrintf (CliHandle, "\r\n\rSwitch %s", au1ContextName);

        nmhGetFsMIFsbSystemControl (u4ContextId, &i4SystemControl);
        nmhGetFsMIFsbModuleStatus (u4ContextId, &i4ModuleStatus);
        nmhGetFsMIFsbFcMapMode (u4ContextId, &i4FcMapMode);
        nmhGetFsMIFsbFcmap (u4ContextId, &FcMap);
        nmhGetFsMIFsbHouseKeepingTimePeriod (u4ContextId, &u4HouseKeepingTime);
        nmhGetFsMIFsbTraceOption (u4ContextId, &i4TraceOption);
        nmhGetFsMIFsbTraceSeverityLevel (u4ContextId, &i4TraceSeverityLevel);
        nmhGetFsMIFsbTrapStatus (u4ContextId, &i4TrapStatus);
        nmhGetFsMIFsbDefaultVlanId (u4ContextId, &i4DefaultVlanId);
        CliPrintf (CliHandle, "\r\nFIP-snooping Global Configuration\r\n");
        CliPrintf (CliHandle, "---------------------------------\r\n");

        CliPrintf (CliHandle, "System Control    : ");
        if (i4SystemControl == FSB_ENABLE)
        {
            CliPrintf (CliHandle, "%-19s\r\n", "Start");
        }
        else
        {
            CliPrintf (CliHandle, "%-19s\r\n", "Shutdown");
        }

        CliPrintf (CliHandle, "Module Status     : ");
        if (i4ModuleStatus == FSB_ENABLE)
        {
            CliPrintf (CliHandle, "%-19s\r\n", "Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "%-19s\r\n", "Disabled");
        }

        CliPrintf (CliHandle, "FC-MAP Mode       : ");
        if (i4FcMapMode == FSB_FCMAP_GLOBAL)
        {
            CliPrintf (CliHandle, "%-19s\r\n", "Global");
        }
        else
        {
            CliPrintf (CliHandle, "%-19s\r\n", "VLAN");
        }

        CliPrintf (CliHandle, "FC-MAP            : %02x:%02x:%02x\r\n",
                   FcMap.pu1_OctetList[0], FcMap.pu1_OctetList[1],
                   FcMap.pu1_OctetList[2]);

        if (u4HouseKeepingTime != FSB_NO_HOUSEKEEPING_TIME)
        {
            CliPrintf (CliHandle, "HouseKeeping Time : %d seconds\r\n",
                       u4HouseKeepingTime);
        }
        else
        {
            CliPrintf (CliHandle, "HouseKeeping Time : Disabled\r\n");
        }

        CliPrintf (CliHandle, "Severity Level    : ");
        switch (i4TraceSeverityLevel)
        {
            case FSB_EMERGENCY_LEVEL:
                CliPrintf (CliHandle, "%-19s\r\n", "Emergency");
                break;

            case FSB_ALERT_LEVEL:
                CliPrintf (CliHandle, "%-19s\r\n", "Alert");
                break;

            case FSB_CRITICAL_LEVEL:
                CliPrintf (CliHandle, "%-19s\r\n", "Critical");
                break;

            case FSB_ERROR_LEVEL:
                CliPrintf (CliHandle, "%-19s\r\n", "Error");
                break;

            case FSB_WARNING_LEVEL:
                CliPrintf (CliHandle, "%-19s\r\n", "Warning");
                break;

            case FSB_NOTIFICATION_LEVEL:
                CliPrintf (CliHandle, "%-19s\r\n", "Notification");
                break;

            case FSB_INFORMATIONAL_LEVEL:
                CliPrintf (CliHandle, "%-19s\r\n", "Informational");
                break;

            case FSB_DEBUGGING_LEVEL:
                CliPrintf (CliHandle, "%-19s\r\n", "Debugging");
                break;

            default:
                break;
        }

        CliPrintf (CliHandle, "Trace Option      : ");
        if ((i4TraceOption != 0) && (i4TraceOption != 1))
        {
            CliPrintf (CliHandle, "%-19s\r\n", "Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "%-19s\r\n", "Disabled");
        }

        CliPrintf (CliHandle, "Trap Status       : ");
        if (i4TrapStatus == FSB_ENABLE)
        {
            CliPrintf (CliHandle, "%-19s\r\n", "Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "%-19s\r\n", "Disabled");
        }

#ifndef FSB_CUSTOM_FCOE_TAGGED_WANTED
        CliPrintf (CliHandle, "Untagged VLAN     : %d\r\n", i4DefaultVlanId);
#endif

        u4CurrContextId = u4ContextId;
    }
    while (nmhGetNextIndexFsMIFsbContextTable (u4CurrContextId,
                                               &u4ContextId) == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliShowFIPSnoopingVlan                        */
/*                                                                           */
/*    Description         : This function is used to display the status  of  */
/*                          FCoE VLAN configured in switch                   */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          pu1ContextName - Context Name                    */
/*                          i4VlanIndex - VLAN Index                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliShowFIPSnoopingVlan (tCliHandle CliHandle, UINT1 *pu1ContextName,
                           INT4 i4VlanIndex)
{
    tSNMP_OCTET_STRING_TYPE FcMap;
    tSNMP_OCTET_STRING_TYPE FsbPinnedPorts;
    tPortList          *pMemberPorts = NULL;
    UINT1               au1ContextName[FSB_ALIAS_MAX_LEN];
    UINT1               au1FcMap[FSB_FCMAP_LEN];
    UINT4               u4CurrContextId = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4EnabledStatus = 0;
    INT4                i4CurrVlanId = 0;

    FSB_MEMSET (&FcMap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    FSB_MEMSET (&FsbPinnedPorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    pMemberPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pMemberPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    MEMSET (*pMemberPorts, 0, sizeof (tPortList));
    FsbPinnedPorts.pu1_OctetList = *pMemberPorts;
    FsbPinnedPorts.i4_Length = BRG_PORT_LIST_SIZE;

    /* If Switch-name is given then get the Context-Id from it */
    if (pu1ContextName != NULL)
    {
        if (FsbPortIsSwitchExist (pu1ContextName, &u4ContextId) != FSB_TRUE)
        {
            CliPrintf (CliHandle, "\r%% Switch %s Does not exist.\r\n",
                       pu1ContextName);
            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            return CLI_FAILURE;
        }
        if (nmhValidateIndexInstanceFsMIFsbFIPSnoopingTable (u4ContextId,
                                                             i4VlanIndex) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% VLAN %d is not configured as FCoE VLAN"
                       " in Switch %s\r\n", i4VlanIndex, pu1ContextName);
            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            return CLI_FAILURE;
        }
        u4CurrContextId = u4ContextId;
        i4CurrVlanId = i4VlanIndex;
    }
    else
    {
        if (nmhGetFirstIndexFsMIFsbFIPSnoopingTable (&u4ContextId,
                                                     &i4VlanIndex) ==
            SNMP_FAILURE)
        {
            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            return CLI_SUCCESS;
        }
    }
    CliPrintf (CliHandle, "\r\nFCoE-Vlan database \r\n");
    CliPrintf (CliHandle, "------------------------------------------ \r\n");

    do
    {
        /* To break for loop when a switch name and Vlan Index is given */
        if ((pu1ContextName != NULL) &&
            ((u4CurrContextId != u4ContextId) || (i4CurrVlanId != i4VlanIndex)))
        {
            break;
        }

        FSB_MEMSET (au1ContextName, 0, sizeof (au1ContextName));
        FSB_MEMSET (au1FcMap, 0, sizeof (au1FcMap));
        FcMap.pu1_OctetList = au1FcMap;
        FcMap.i4_Length = 0;

        FsbPortGetSwitchAliasName (u4ContextId, au1ContextName);
        nmhGetFsMIFsbFIPSnoopingEnabledStatus (u4ContextId, i4VlanIndex,
                                               &i4EnabledStatus);
        nmhGetFsMIFsbFIPSnoopingFcmap (u4ContextId, i4VlanIndex, &FcMap);

        nmhGetFsMIFsbFIPSnoopingPinnedPorts (u4ContextId, i4VlanIndex,
                                             &FsbPinnedPorts);

        CliPrintf (CliHandle, "Context Name     : %s\r\n", au1ContextName);
        CliPrintf (CliHandle, "Vlan ID          : %d\r\n", i4VlanIndex);
        CliPrintf (CliHandle, "FC-MAP           : %02x:%02x:%02x\r\n",
                   FcMap.pu1_OctetList[0],
                   FcMap.pu1_OctetList[1], FcMap.pu1_OctetList[2]);

        if (FsUtilBitListIsAllZeros (FsbPinnedPorts.pu1_OctetList,
                                     BRG_PORT_LIST_SIZE) == OSIX_TRUE)
        {
            CliPrintf (CliHandle, "Pinned Port(s)   : None\r\n");
        }
        else if (CliOctetToIfName
                 (CliHandle, "\rPinned Port(s)   :",
                  &FsbPinnedPorts, BRG_PORT_LIST_SIZE,
                  BRG_PORT_LIST_SIZE, 0, &u4PagingStatus, 6) == CLI_FAILURE)
        {
            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            return CLI_FAILURE;
        }

        if (i4EnabledStatus == FSB_ENABLE)
        {
            CliPrintf (CliHandle, "Vlan Status      : %s\r\n", "Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "Vlan Status      : %s\r\n", "Disabled");
        }
        CliPrintf (CliHandle,
                   "------------------------------------------ \r\n");

        u4CurrContextId = u4ContextId;
        i4CurrVlanId = i4VlanIndex;

    }
    while (nmhGetNextIndexFsMIFsbFIPSnoopingTable (u4CurrContextId,
                                                   &u4ContextId, i4CurrVlanId,
                                                   &i4VlanIndex) ==
           SNMP_SUCCESS);

    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliShowInterface                              */
/*                                                                           */
/*    Description         : This function is used to display port role of    */
/*                          FCoE interfaces                                  */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          i4VlanIndex - Vlan Index                         */
/*                          i4IfIndex - Interface Index                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliShowInterface (tCliHandle CliHandle, INT4 i4VlanIndex, INT4 i4IfIndex)
{
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    INT4                i4CurrIfIndex = 0;
    INT4                i4CurrVlanId = 0;
    INT4                i4PortRole = 0;
    UINT1               u1DispFlag = 0;
    INT1               *piIfName = NULL;

    piIfName = (INT1 *) &au1IfName[0];

    /* If Vlan-Id is given, then validate the FIp-snooping interface entry */
    if (i4VlanIndex != 0)
    {
        if (nmhValidateIndexInstanceFsMIFsbIntfTable (i4VlanIndex, i4IfIndex)
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% FSB Interface Entry does not exist");
            return CLI_FAILURE;
        }

        i4CurrVlanId = i4VlanIndex;
        i4CurrIfIndex = i4IfIndex;
        u1DispFlag = FSB_TRUE;
    }
    else
    {
        if (nmhGetFirstIndexFsMIFsbIntfTable (&i4VlanIndex,
                                              &i4IfIndex) == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }
    }
    CliPrintf (CliHandle, "\r\n%-9s%-26s%-10s\r\n",
               "Vlan ID", "Interface", "Port-Role");
    CliPrintf (CliHandle, "%-9s%-26s%-10s\r\n",
               "-------", "---------", "---------");

    do
    {
        /* To break for the loop when a VLAN Index and IfIndex is given */
        if ((u1DispFlag == FSB_TRUE) && ((i4CurrVlanId != i4VlanIndex) ||
                                         (i4CurrIfIndex != i4IfIndex)))
        {
            break;
        }

        FSB_MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
        if (CfaCliGetIfName ((UINT4) i4IfIndex, piIfName) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
        nmhGetFsMIFsbIntfPortRole (i4VlanIndex, i4IfIndex, &i4PortRole);

        CliPrintf (CliHandle, "%-9d", i4VlanIndex);
        CliPrintf (CliHandle, "%-26s", piIfName);

        if (i4PortRole == FSB_ENODE_FACING)
        {
            CliPrintf (CliHandle, "%-10s\r\n", "Enode Facing");
        }
        else if (i4PortRole == FSB_FCF_FACING)
        {
            CliPrintf (CliHandle, "%-10s\r\n", "FCF Facing");
        }
        else
        {
            CliPrintf (CliHandle, "%-10s\r\n", "Enode and FCF Facing");
        }

        i4CurrVlanId = i4VlanIndex;
        i4CurrIfIndex = i4IfIndex;

    }
    while (nmhGetNextIndexFsMIFsbIntfTable (i4CurrVlanId,
                                            &i4VlanIndex, i4CurrIfIndex,
                                            &i4IfIndex) == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliShowFIPSession                             */
/*                                                                           */
/*    Description         : This function is used to display FIP-snooping    */
/*                          session entries                                  */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          i4VlanIndex - Vlan Index                         */
/*                          i4IfIndex - Interface Index                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliShowFIPSession (tCliHandle CliHandle, INT4 i4VlanIndex, INT4 i4IfIndex)
{
    tSNMP_OCTET_STRING_TYPE FIPSessionNameId;
    tSNMP_OCTET_STRING_TYPE FIPSessionFcfId;
    tSNMP_OCTET_STRING_TYPE FIPSessionFcMap;
    tMacAddr            FIPSessionENodeMacAddr;
    tMacAddr            FIPSessionFcfMacAddr;
    tMacAddr            FIPSessionFCoEMacAddr;
    tMacAddr            ENodeMacAddr;
    tMacAddr            FCoEMacAddr;
    tMacAddr            FcfMacAddr;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1ENodeMacAddr[MAC_TO_STRING_LEN];
    UINT1               au1FCFMacAddr[MAC_TO_STRING_LEN];
    UINT1               au1FCoEMacAddr[MAC_TO_STRING_LEN];
    UINT1               au1Fcmap[FSB_FCMAP_LEN];
    UINT1               au1FcfId[FSB_FCID_LEN];
    UINT1               au1NameId[FSB_NAME_ID_LEN];
    INT4                i4FIPSessionEnodeIfIndex = 0;
    INT4                i4HouseKeepingTmrStatus = 0;
    INT4                i4FIPSessionFcfIfIndex = 0;
    INT4                i4FIPSessionVlanId = 0;
    INT4                i4EnodeConnectType = 0;
    UINT1               u1DispFlag = FSB_FALSE;
    INT1               *piIfName = NULL;

    FSB_MEMSET (&FIPSessionFcMap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    FSB_MEMSET (&FIPSessionNameId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    FSB_MEMSET (&FIPSessionFcfId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    FSB_MEMSET (&FIPSessionENodeMacAddr, 0, MAC_ADDR_LEN);
    FSB_MEMSET (&FIPSessionFcfMacAddr, 0, MAC_ADDR_LEN);
    FSB_MEMSET (&FIPSessionFCoEMacAddr, 0, MAC_ADDR_LEN);
    FSB_MEMSET (&ENodeMacAddr, 0, MAC_ADDR_LEN);
    FSB_MEMSET (&FCoEMacAddr, 0, MAC_ADDR_LEN);
    FSB_MEMSET (&FcfMacAddr, 0, MAC_ADDR_LEN);
    FSB_MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);

    piIfName = (INT1 *) &au1IfName[0];

    if (i4VlanIndex == 0)
    {
        if (nmhGetFirstIndexFsMIFsbFIPSessionTable (&i4FIPSessionVlanId,
                                                    &i4FIPSessionEnodeIfIndex,
                                                    &FIPSessionENodeMacAddr,
                                                    &FIPSessionFcfMacAddr,
                                                    &FIPSessionFCoEMacAddr) !=
            SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIFsbFIPSessionTable
            (i4VlanIndex, &i4FIPSessionVlanId, i4IfIndex,
             &i4FIPSessionEnodeIfIndex, ENodeMacAddr, &FIPSessionENodeMacAddr,
             FcfMacAddr, &FIPSessionFcfMacAddr, FCoEMacAddr,
             &FIPSessionFCoEMacAddr) != SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        u1DispFlag = FSB_TRUE;
    }

    do
    {
        /* To break for the loop when a VLAN Index and IfIndex is given
         */
        if ((u1DispFlag == FSB_TRUE) && ((i4FIPSessionVlanId != i4VlanIndex) ||
                                         (i4FIPSessionEnodeIfIndex !=
                                          i4IfIndex)))
        {
            break;
        }

        FSB_MEMSET (au1ENodeMacAddr, 0, MAC_TO_STRING_LEN);
        FSB_MEMSET (au1FCFMacAddr, 0, MAC_TO_STRING_LEN);
        FSB_MEMSET (au1FCoEMacAddr, 0, MAC_TO_STRING_LEN);
        FIPSessionFcMap.pu1_OctetList = &au1Fcmap[0];
        FIPSessionFcMap.i4_Length = 0;
        FIPSessionFcfId.pu1_OctetList = &au1FcfId[0];
        FIPSessionFcfId.i4_Length = 0;
        FIPSessionNameId.pu1_OctetList = &au1NameId[0];
        FIPSessionNameId.i4_Length = 0;

        nmhGetFsMIFsbFIPSessionFcMap (i4FIPSessionVlanId,
                                      i4FIPSessionEnodeIfIndex,
                                      FIPSessionENodeMacAddr,
                                      FIPSessionFcfMacAddr,
                                      FIPSessionFCoEMacAddr, &FIPSessionFcMap);
        nmhGetFsMIFsbFIPSessionFcfIfIndex (i4FIPSessionVlanId,
                                           i4FIPSessionEnodeIfIndex,
                                           FIPSessionENodeMacAddr,
                                           FIPSessionFcfMacAddr,
                                           FIPSessionFCoEMacAddr,
                                           &i4FIPSessionFcfIfIndex);
        nmhGetFsMIFsbFIPSessionFcfNameId (i4FIPSessionVlanId,
                                          i4FIPSessionEnodeIfIndex,
                                          FIPSessionENodeMacAddr,
                                          FIPSessionFcfMacAddr,
                                          FIPSessionFCoEMacAddr,
                                          &FIPSessionNameId);
        nmhGetFsMIFsbFIPSessionFcId (i4FIPSessionVlanId,
                                     i4FIPSessionEnodeIfIndex,
                                     FIPSessionENodeMacAddr,
                                     FIPSessionFcfMacAddr,
                                     FIPSessionFCoEMacAddr, &FIPSessionFcfId);
        nmhGetFsMIFsbFIPSessionEnodeConnectType (i4FIPSessionVlanId,
                                                 i4FIPSessionEnodeIfIndex,
                                                 FIPSessionENodeMacAddr,
                                                 FIPSessionFcfMacAddr,
                                                 FIPSessionFCoEMacAddr,
                                                 &i4EnodeConnectType);
        nmhGetFsMIFsbFIPSessionHouseKeepingTimerStatus (i4FIPSessionVlanId,
                                                        i4FIPSessionEnodeIfIndex,
                                                        FIPSessionENodeMacAddr,
                                                        FIPSessionFcfMacAddr,
                                                        FIPSessionFCoEMacAddr,
                                                        &i4HouseKeepingTmrStatus);

        if (CfaCliGetIfName ((UINT4) i4FIPSessionEnodeIfIndex, piIfName) ==
            CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        PrintMacAddress (FIPSessionFCoEMacAddr, au1FCoEMacAddr);
        PrintMacAddress (FIPSessionFcfMacAddr, au1FCFMacAddr);
        PrintMacAddress (FIPSessionENodeMacAddr, au1ENodeMacAddr);

        CliPrintf (CliHandle, "\n");
        CliPrintf (CliHandle, "\rVlanId                      : %d\n",
                   i4FIPSessionVlanId);
        CliPrintf (CliHandle, "\rENode IfIndex               : %s\n", piIfName);
        CliPrintf (CliHandle, "\rENode Mac Address           : %s\n",
                   au1ENodeMacAddr);
        CliPrintf (CliHandle, "\rFCF Mac Address             : %s\n",
                   au1FCFMacAddr);
        CliPrintf (CliHandle, "\rFCoE Mac Address            : %s\n",
                   au1FCoEMacAddr);
        CliPrintf (CliHandle,
                   "\rFC-MAP                      : %02x:%02x:%02x\n",
                   FIPSessionFcMap.pu1_OctetList[0],
                   FIPSessionFcMap.pu1_OctetList[1],
                   FIPSessionFcMap.pu1_OctetList[2]);
        FSB_MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);

        piIfName = (INT1 *) &au1IfName[0];
        if (CfaCliGetIfName ((UINT4) i4FIPSessionFcfIfIndex, piIfName) ==
            CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, "\rFCF IfIndex                 : %s\n", piIfName);
        CliPrintf (CliHandle,
                   "\rName Id                     : %02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x\n",
                   FIPSessionNameId.pu1_OctetList[0],
                   FIPSessionNameId.pu1_OctetList[1],
                   FIPSessionNameId.pu1_OctetList[2],
                   FIPSessionNameId.pu1_OctetList[3],
                   FIPSessionNameId.pu1_OctetList[4],
                   FIPSessionNameId.pu1_OctetList[5],
                   FIPSessionNameId.pu1_OctetList[6],
                   FIPSessionNameId.pu1_OctetList[7], "");
        CliPrintf (CliHandle,
                   "\rFC Id                       : %02x:%02x:%02x\n",
                   FIPSessionFcfId.pu1_OctetList[0],
                   FIPSessionFcfId.pu1_OctetList[1],
                   FIPSessionFcfId.pu1_OctetList[2], "");

        if (i4EnodeConnectType == FSB_FIP_FLOGI)
        {
            CliPrintf (CliHandle, "\rENode Connect Type          : %s\n",
                       "FLOGI");
        }
        else
        {
            CliPrintf (CliHandle, "\rENode Connect Type          : %s\n",
                       "FDISC");
        }
        if (i4HouseKeepingTmrStatus == FSB_TRUE)
        {
            CliPrintf (CliHandle, "\rHouse Keeping Timer Status  : Running\n");
        }
        else if (i4HouseKeepingTmrStatus == FSB_FALSE)
        {
            CliPrintf (CliHandle, "\rHouse Keeping Timer Status  : Disabled\n");
        }

        i4VlanIndex = i4FIPSessionVlanId;
        i4IfIndex = i4FIPSessionEnodeIfIndex;
        FSB_MEMCPY (ENodeMacAddr, FIPSessionENodeMacAddr, MAC_ADDR_LEN);
        FSB_MEMCPY (FcfMacAddr, FIPSessionFcfMacAddr, MAC_ADDR_LEN);
        FSB_MEMCPY (FCoEMacAddr, FIPSessionFCoEMacAddr, MAC_ADDR_LEN);
        i4FIPSessionVlanId = 0;
        i4FIPSessionEnodeIfIndex = 0;
        FSB_MEMSET (FIPSessionENodeMacAddr, 0, MAC_ADDR_LEN);
        FSB_MEMSET (FIPSessionFcfMacAddr, 0, MAC_ADDR_LEN);
        FSB_MEMSET (FIPSessionFCoEMacAddr, 0, MAC_ADDR_LEN);

    }
    while (nmhGetNextIndexFsMIFsbFIPSessionTable (i4VlanIndex,
                                                  &i4FIPSessionVlanId,
                                                  i4IfIndex,
                                                  &i4FIPSessionEnodeIfIndex,
                                                  ENodeMacAddr,
                                                  &FIPSessionENodeMacAddr,
                                                  FcfMacAddr,
                                                  &FIPSessionFcfMacAddr,
                                                  FCoEMacAddr,
                                                  &FIPSessionFCoEMacAddr) ==
           SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliShowFcf                                    */
/*                                                                           */
/*    Description         : This function is used to display details of      */
/*                          Fibre Channel Forwarder (FCF)                    */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          i4VlanIndex - Vlan Index                         */
/*                          i4IfIndex - Interface Index                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliShowFcf (tCliHandle CliHandle, INT4 i4ShowVlanIndex, INT4 i4ShowIfIndex)
{
    tSNMP_OCTET_STRING_TYPE FcfFcMap;
    tSNMP_OCTET_STRING_TYPE NameId;
    tSNMP_OCTET_STRING_TYPE FabricName;
    tSNMP_OCTET_STRING_TYPE FsbPinnedPorts;
    tPortList          *pMemberPorts = NULL;
    tMacAddr            FCFMacAddress;
    tMacAddr            FcfCurrMacAddress;
    UINT1               au1FabricName[FSB_FABRIC_NAME_LEN];
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1NameId[FSB_NAME_ID_LEN];
    UINT1               au1FcMap[FSB_FCMAP_LEN];
    UINT1               au1String[MAC_TO_STRING_LEN];
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4VlanIndex = 0;
    INT4                i4IfIndex = 0;
    INT4                i4CurrIfIndex = 0;
    INT4                i4CurrVlanId = 0;
    INT4                i4AddressingMode = 0;
    INT4                i4EnodeLoginCode = 0;
    UINT1               u1DispFlag = FSB_FALSE;
    INT1               *piIfName;

    FSB_MEMSET (au1FcMap, 0, sizeof (au1FcMap));
    FSB_MEMSET (au1NameId, 0, sizeof (au1NameId));
    FSB_MEMSET (au1FabricName, 0, sizeof (au1FabricName));
    FSB_MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    FSB_MEMSET (FCFMacAddress, 0, sizeof (tMacAddr));
    FSB_MEMSET (FcfCurrMacAddress, 0, sizeof (tMacAddr));
    FSB_MEMSET (&FcfFcMap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    FSB_MEMSET (&NameId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    FSB_MEMSET (&FabricName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    FSB_MEMSET (&FsbPinnedPorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    FcfFcMap.pu1_OctetList = au1FcMap;
    FcfFcMap.i4_Length = (INT4) STRLEN (au1FcMap);
    NameId.pu1_OctetList = au1NameId;
    NameId.i4_Length = (INT4) STRLEN (au1NameId);
    FabricName.pu1_OctetList = au1FabricName;
    FabricName.i4_Length = (INT4) STRLEN (au1FabricName);
    piIfName = (INT1 *) &au1IfName[0];

    pMemberPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pMemberPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    MEMSET (*pMemberPorts, 0, sizeof (tPortList));
    FsbPinnedPorts.pu1_OctetList = *pMemberPorts;
    FsbPinnedPorts.i4_Length = BRG_PORT_LIST_SIZE;

    if ((i4ShowVlanIndex != 0) && (i4ShowIfIndex != 0))
    {
        u1DispFlag = FSB_TRUE;
    }

    if (nmhGetNextIndexFsMIFsbFcfTable (i4ShowVlanIndex, &i4VlanIndex,
                                        i4ShowIfIndex, &i4IfIndex,
                                        FcfCurrMacAddress,
                                        &FCFMacAddress) == SNMP_FAILURE)
    {
        FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
        return CLI_SUCCESS;
    }
    do
    {
        if ((u1DispFlag == FSB_TRUE) && ((i4ShowVlanIndex != i4VlanIndex) ||
                                         (i4ShowIfIndex != i4IfIndex)))
        {
            break;
        }

        MEMSET (au1String, 0, MAC_TO_STRING_LEN);
        PrintMacAddress (FCFMacAddress, au1String);

        nmhGetFsMIFsbFcfFcMap (i4VlanIndex, i4IfIndex, FCFMacAddress,
                               &FcfFcMap);
        nmhGetFsMIFsbFcfAddressingMode (i4VlanIndex, i4IfIndex, FCFMacAddress,
                                        &i4AddressingMode);
        nmhGetFsMIFsbFcfEnodeLoginCount (i4VlanIndex, i4IfIndex, FCFMacAddress,
                                         &i4EnodeLoginCode);
        nmhGetFsMIFsbFcfNameId (i4VlanIndex, i4IfIndex, FCFMacAddress, &NameId);
        nmhGetFsMIFsbFcfFabricName (i4VlanIndex, i4IfIndex, FCFMacAddress,
                                    &FabricName);

        if (CfaCliGetIfName ((UINT4) i4IfIndex, piIfName) != CLI_SUCCESS)
        {
            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            return CLI_FAILURE;
        }

        nmhGetFsMIFsbFcfPinnedPorts (i4VlanIndex, i4IfIndex, FCFMacAddress,
                                     &FsbPinnedPorts);

        CliPrintf (CliHandle, "\n");
        CliPrintf (CliHandle, "\rVlan index          : %d\n", i4VlanIndex);
        CliPrintf (CliHandle, "\rFCF IfIndex         : %s\n", piIfName);
        CliPrintf (CliHandle, "\rFC-MAP              : %02x:%02x:%02x\n",
                   FcfFcMap.pu1_OctetList[0], FcfFcMap.pu1_OctetList[1],
                   FcfFcMap.pu1_OctetList[2]);

        CliPrintf (CliHandle, "\rFCF Mac Address     : %s\n", au1String);

        if (i4AddressingMode == FSB_FPMA_ADDRESSING_MODE)
        {
            CliPrintf (CliHandle, "\rAddressing Mode     : %s\n", "FPMA");
        }
        else
        {
            CliPrintf (CliHandle, "\rAddressing Mode     : %s\n", "SPMA");
        }
        CliPrintf (CliHandle, "\rEnode Login Count   : %d\n", i4EnodeLoginCode);
        CliPrintf (CliHandle,
                   "\rName Id             : %02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x\n",
                   NameId.pu1_OctetList[0], NameId.pu1_OctetList[1],
                   NameId.pu1_OctetList[2], NameId.pu1_OctetList[3],
                   NameId.pu1_OctetList[4], NameId.pu1_OctetList[5],
                   NameId.pu1_OctetList[6], NameId.pu1_OctetList[7], "");

        CliPrintf (CliHandle,
                   "\rFabric Name         : %02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x\n",
                   FabricName.pu1_OctetList[0], FabricName.pu1_OctetList[1],
                   FabricName.pu1_OctetList[2], FabricName.pu1_OctetList[3],
                   FabricName.pu1_OctetList[4], FabricName.pu1_OctetList[5],
                   FabricName.pu1_OctetList[6], FabricName.pu1_OctetList[7]);

        if (FsUtilBitListIsAllZeros (FsbPinnedPorts.pu1_OctetList,
                                     BRG_PORT_LIST_SIZE) == OSIX_TRUE)
        {
            CliPrintf (CliHandle, "Pinned Port(s)      : None\r\n");
        }
        else if (CliOctetToIfName
                 (CliHandle, "\rPinned Port(s)      :",
                  &FsbPinnedPorts, BRG_PORT_LIST_SIZE,
                  BRG_PORT_LIST_SIZE, 0, &u4PagingStatus, 6) == CLI_FAILURE)
        {
            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            return CLI_FAILURE;
        }

        i4CurrVlanId = i4VlanIndex;
        i4CurrIfIndex = i4IfIndex;
        FSB_MEMCPY (&FcfCurrMacAddress, FCFMacAddress, MAC_ADDR_LEN);

    }
    while (nmhGetNextIndexFsMIFsbFcfTable (i4CurrVlanId, &i4VlanIndex,
                                           i4CurrIfIndex, &i4IfIndex,
                                           FcfCurrMacAddress,
                                           &FCFMacAddress) == SNMP_SUCCESS);

    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliShowGlobalStatistics                       */
/*                                                                           */
/*    Description         : This function display Global statistics for the  */
/*                          Context                                          */
/*                                                                           */
/*    Input(s)            : CliHandle -  CliHandler                          */
/*                          pu1ContextName - Context Name                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliShowGlobalStatistics (tCliHandle CliHandle, UINT1 *pu1ContextName)
{
    UINT1               au1ContextName[FSB_ALIAS_MAX_LEN];
    UINT4               u4VlanNotificationCount = 0;
    UINT4               u4VlanRequestCount = 0;
    UINT4               u4CurrContextId = 0;
    UINT4               u4ContextId = 0;

    /* If Switch-name is given then get the Context-Id from it */
    if (pu1ContextName != NULL)
    {
        if (FsbPortIsSwitchExist (pu1ContextName, &u4ContextId) != FSB_TRUE)
        {
            CliPrintf (CliHandle, "\r%% Switch %s Does not exist.\r\n",
                       pu1ContextName);
            return CLI_FAILURE;
        }
        if (nmhValidateIndexInstanceFsMIFsbGlobalStatsTable (u4ContextId)
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% FSB Module is ShutDown for"
                       " Switch %s\r\n", pu1ContextName);
            return CLI_FAILURE;
        }
        u4CurrContextId = u4ContextId;
    }
    else
    {
        if (nmhGetFirstIndexFsMIFsbGlobalStatsTable (&u4ContextId)
            != SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }

    do
    {
        if ((pu1ContextName != NULL) && (u4CurrContextId != u4ContextId))
        {
            break;
        }

        FSB_MEMSET (au1ContextName, 0, sizeof (au1ContextName));
        FsbPortGetSwitchAliasName (u4ContextId, au1ContextName);

        CliPrintf (CliHandle, "\r\n\rSwitch %s", au1ContextName);
        CliPrintf (CliHandle, "\r\nFIP-snooping Global Statistics");
        CliPrintf (CliHandle, "\r\n------------------------------\r\n");

        nmhGetFsMIFsbGlobalStatsVlanRequests (u4ContextId, &u4VlanRequestCount);
        nmhGetFsMIFsbGlobalStatsVlanNotification (u4ContextId,
                                                  &u4VlanNotificationCount);

        CliPrintf (CliHandle, "\rNumber of Vlan Requests      : %d\r\n",
                   u4VlanRequestCount);
        CliPrintf (CliHandle, "\rNumber of Vlan Notifications : %d\r\n",
                   u4VlanNotificationCount);
        u4CurrContextId = u4ContextId;
    }
    while (nmhGetNextIndexFsMIFsbGlobalStatsTable (u4CurrContextId,
                                                   &u4ContextId) ==
           SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliShowVlanStatistics                         */
/*                                                                           */
/*    Description         : This function is used to display VLAN statistics */
/*                          for FCoE VLAN                                    */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          pu1ContextName - Context Name                    */
/*                          i4VlanIndex - VLAN Index                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliShowVlanStatistics (tCliHandle CliHandle, UINT1 *pu1ContextName,
                          INT4 i4VlanIndex)
{
    UINT1               au1ContextName[FSB_ALIAS_MAX_LEN];
    UINT4               u4InvalidFIPFramesCount = 0;
    UINT4               u4MulticastDisSolCount = 0;
    UINT4               u4MulticastDisAdvCount = 0;
    UINT4               u4UnicastDisAdvCount = 0;
    UINT4               u4UnicastDisSolCount = 0;
    UINT4               u4FcMapMisMatchCount = 0;
    UINT4               u4ACLFailureCount = 0;
    UINT4               u4MTUMisMatchCount = 0;
    UINT4               u4FLOGIAcceptCount = 0;
    UINT4               u4FLOGIRejectCount = 0;
    UINT4               u4FDISCAcceptCount = 0;
    UINT4               u4FDISCRejectCount = 0;
    UINT4               u4LOGOAcceptCount = 0;
    UINT4               u4LOGORejectCount = 0;
    UINT4               u4ClearLinkCount = 0;
    UINT4               u4FCFDiscoveryTimeoutsCount = 0;
    UINT4               u4CurrContextId = 0;
    UINT4               u4ContextId = 0;
    INT4                i4CurrVlanId = 0;
    UINT4               u4FLOGICount = 0;
    UINT4               u4FDISCCount = 0;
    UINT4               u4LOGOCount = 0;

    /* If Switch-name is given then get the Context-Id from it */
    if (pu1ContextName != NULL)
    {
        if (FsbPortIsSwitchExist (pu1ContextName, &u4ContextId) != FSB_TRUE)
        {
            CliPrintf (CliHandle, "\r%% Switch %s Does not exist.\r\n",
                       pu1ContextName);
            return CLI_FAILURE;
        }
        if (nmhValidateIndexInstanceFsMIFsbVlanStatsTable
            (u4ContextId, i4VlanIndex) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% VLAN %d is not configured as FCoE VLAN"
                       " in Switch %s\r\n", i4VlanIndex, pu1ContextName);
            return CLI_FAILURE;
        }
        u4CurrContextId = u4ContextId;
        i4CurrVlanId = i4VlanIndex;
    }
    else
    {
        if (nmhGetFirstIndexFsMIFsbVlanStatsTable (&u4ContextId,
                                                   &i4VlanIndex) ==
            SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }
        i4CurrVlanId = i4VlanIndex;
    }

    do
    {
        /* To break for the loop when Switch and Vlan ID is given */
        if ((pu1ContextName != NULL) &&
            ((u4CurrContextId != u4ContextId) || (i4CurrVlanId != i4VlanIndex)))
        {
            break;
        }

        FSB_MEMSET (au1ContextName, 0, sizeof (au1ContextName));
        FsbPortGetSwitchAliasName (u4ContextId, au1ContextName);

        /* To print the Switch name only once for each Context */
        if (((u4CurrContextId != u4ContextId)) ||
            ((pu1ContextName != NULL) && (u4CurrContextId == u4ContextId) &&
             (i4CurrVlanId == i4VlanIndex)) ||
            ((u4ContextId == 0) && (i4CurrVlanId == i4VlanIndex)))
        {
            CliPrintf (CliHandle, "\r\n\rSwitch %s\r\n", au1ContextName);
        }
        CliPrintf (CliHandle, "\r\nVlan Statistics", i4VlanIndex);
        CliPrintf (CliHandle, "\r\n---------------");
        CliPrintf (CliHandle, "\r\nVlan ID                       : %d\r\n",
                   i4VlanIndex);

        nmhGetFsMIFsbVlanStatsUnicastDisAdv (u4ContextId, i4VlanIndex,
                                             &u4UnicastDisAdvCount);
        nmhGetFsMIFsbVlanStatsMulticastDisAdv (u4ContextId, i4VlanIndex,
                                               &u4MulticastDisAdvCount);
        nmhGetFsMIFsbVlanStatsUnicastDisSol (u4ContextId, i4VlanIndex,
                                             &u4UnicastDisSolCount);
        nmhGetFsMIFsbVlanStatsMulticastDisSol (u4ContextId, i4VlanIndex,
                                               &u4MulticastDisSolCount);
        nmhGetFsMIFsbVlanStatsFLOGICount (u4ContextId, i4VlanIndex,
                                          &u4FLOGICount);
        nmhGetFsMIFsbVlanStatsFDISCCount (u4ContextId, i4VlanIndex,
                                          &u4FDISCCount);
        nmhGetFsMIFsbVlanStatsLOGOCount (u4ContextId, i4VlanIndex,
                                         &u4LOGOCount);
        nmhGetFsMIFsbVlanStatsFLOGIAcceptCount (u4ContextId, i4VlanIndex,
                                                &u4FLOGIAcceptCount);
        nmhGetFsMIFsbVlanStatsFLOGIRejectCount (u4ContextId, i4VlanIndex,
                                                &u4FLOGIRejectCount);
        nmhGetFsMIFsbVlanStatsFDISCAcceptCount (u4ContextId, i4VlanIndex,
                                                &u4FDISCAcceptCount);
        nmhGetFsMIFsbVlanStatsFDISCRejectCount (u4ContextId, i4VlanIndex,
                                                &u4FDISCRejectCount);
        nmhGetFsMIFsbVlanStatsLOGOAcceptCount (u4ContextId, i4VlanIndex,
                                               &u4LOGOAcceptCount);
        nmhGetFsMIFsbVlanStatsLOGORejectCount (u4ContextId, i4VlanIndex,
                                               &u4LOGORejectCount);
        nmhGetFsMIFsbVlanStatsClearLinkCount (u4ContextId, i4VlanIndex,
                                              &u4ClearLinkCount);
        nmhGetFsMIFsbVlanFcMapMisMatchCount (u4ContextId, i4VlanIndex,
                                             &u4FcMapMisMatchCount);
        nmhGetFsMIFsbVlanMTUMisMatchCount (u4ContextId, i4VlanIndex,
                                           &u4MTUMisMatchCount);
        nmhGetFsMIFsbVlanACLFailureCount (u4ContextId, i4VlanIndex,
                                          &u4ACLFailureCount);
        nmhGetFsMIFsbVlanInvalidFIPFramesCount (u4ContextId, i4VlanIndex,
                                                &u4InvalidFIPFramesCount);
        nmhGetFsMIFsbVlanFCFDiscoveryTimeoutsCount (u4ContextId, i4VlanIndex,
                                                    &u4FCFDiscoveryTimeoutsCount);

        CliPrintf (CliHandle, "\rNumber of Unicast Discovery "
                   "\r\nAdvertisement                 : %d\r\n",
                   u4UnicastDisAdvCount);
        CliPrintf (CliHandle,
                   "\rNumber of Multicast Discovery"
                   "\r\nAdvertisement                 : %d\r\n",
                   u4MulticastDisAdvCount);
        CliPrintf (CliHandle,
                   "\rNumber of Unicast Discovery"
                   "\r\nSolicitation                  : %d\r\n",
                   u4UnicastDisSolCount);
        CliPrintf (CliHandle,
                   "\rNumber of Multicast Discovery"
                   "\r\nSolicitation                  : %d\r\n",
                   u4MulticastDisSolCount);
        CliPrintf (CliHandle, "\rNumber of FLOGI               : %d\r\n",
                   u4FLOGICount);
        CliPrintf (CliHandle, "\rNumber of FDISC               : %d\r\n",
                   u4FDISCCount);
        CliPrintf (CliHandle, "\rNumber of LOGO                : %d\r\n",
                   u4LOGOCount);
        CliPrintf (CliHandle, "\rNumber of FLOGI Accept        : %d\r\n",
                   u4FLOGIAcceptCount);
        CliPrintf (CliHandle, "\rNumber of FLOGI Reject        : %d\r\n",
                   u4FLOGIRejectCount);
        CliPrintf (CliHandle, "\rNumber of FDISC Accept        : %d\r\n",
                   u4FDISCAcceptCount);
        CliPrintf (CliHandle, "\rNumber of FDISC Reject        : %d\r\n",
                   u4FDISCRejectCount);
        CliPrintf (CliHandle, "\rNumber of LOGO Accept         : %d\r\n",
                   u4LOGOAcceptCount);
        CliPrintf (CliHandle, "\rNumber of LOGO Reject         : %d\r\n",
                   u4LOGORejectCount);
        CliPrintf (CliHandle, "\rNumber of Clear Link          : %d\r\n",
                   u4ClearLinkCount);
        CliPrintf (CliHandle, "\rNumber of FC-MAP MisMatch     : %d\r\n",
                   u4FcMapMisMatchCount);
        CliPrintf (CliHandle, "\rNumber of MTU MisMatch        : %d\r\n",
                   u4MTUMisMatchCount);
        CliPrintf (CliHandle, "\rNumber of ACL Failure         : %d\r\n",
                   u4ACLFailureCount);
        CliPrintf (CliHandle,
                   "\rNumber of Invalid"
                   "\r\nFIP Frames                    : %d\r\n",
                   u4InvalidFIPFramesCount);
        CliPrintf (CliHandle,
                   "\rNumber of FCF Discovery"
                   "\r\nTimeouts                      : %d\r\n",
                   u4FCFDiscoveryTimeoutsCount);

        u4CurrContextId = u4ContextId;
        i4CurrVlanId = i4VlanIndex;

    }
    while (nmhGetNextIndexFsMIFsbVlanStatsTable (u4CurrContextId, &u4ContextId,
                                                 i4CurrVlanId,
                                                 &i4VlanIndex) == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliShowSessionStatistics                      */
/*                                                                           */
/*    Description         : This function is used to display session         */
/*                          statistics                                       */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                          i4VlanIndex - Vlan Index                         */
/*                          i4IfIndex - Interface Index                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliShowSessionStatistics (tCliHandle CliHandle, INT4 i4VlanIndex,
                             INT4 i4IfIndex)
{
    tMacAddr            EnodeMacAddress;
    tMacAddr            FcfMacAddress;
    tMacAddr            FCoEMacAddress;
    tMacAddr            EnodeSessStatsMacAddress;
    tMacAddr            FcfSessStatsMacAddress;
    tMacAddr            FCoESessStatsMacAddress;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1ENodeMacAddr[MAC_TO_STRING_LEN];
    UINT1               au1FcfMacAddr[MAC_TO_STRING_LEN];
    UINT1               au1FCoEMacAddr[MAC_TO_STRING_LEN];
    UINT4               u4VNPortKeepAliveCount = 0;
    UINT4               u4EnodeKeepAliveCount = 0;
    INT4                i4SessStatsIfIndex = 0;
    INT4                i4SessStatsVlanId = 0;
    UINT1               u1DispFlag = FSB_FALSE;
    INT1               *piIfName = NULL;

    FSB_MEMSET (&EnodeMacAddress, 0, MAC_ADDR_LEN);
    FSB_MEMSET (&FcfMacAddress, 0, MAC_ADDR_LEN);
    FSB_MEMSET (&FCoEMacAddress, 0, MAC_ADDR_LEN);
    FSB_MEMSET (&EnodeSessStatsMacAddress, 0, MAC_ADDR_LEN);
    FSB_MEMSET (&FcfSessStatsMacAddress, 0, MAC_ADDR_LEN);
    FSB_MEMSET (&FCoESessStatsMacAddress, 0, MAC_ADDR_LEN);
    FSB_MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);

    piIfName = (INT1 *) &au1IfName[0];

    if (i4VlanIndex == 0)
    {
        if (nmhGetFirstIndexFsMIFsbSessStatsTable (&i4SessStatsVlanId,
                                                   &i4SessStatsIfIndex,
                                                   &EnodeSessStatsMacAddress,
                                                   &FcfSessStatsMacAddress,
                                                   &FCoESessStatsMacAddress) !=
            SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIFsbSessStatsTable
            (i4VlanIndex, &i4SessStatsVlanId, i4IfIndex, &i4SessStatsIfIndex,
             EnodeMacAddress, &EnodeSessStatsMacAddress, FcfMacAddress,
             &FcfSessStatsMacAddress, FCoEMacAddress,
             &FCoESessStatsMacAddress) != SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        u1DispFlag = FSB_TRUE;
    }

    do
    {
        /* To break the for loop when a VLAN Index and IfIndex is given
         * */
        if ((u1DispFlag == FSB_TRUE) && ((i4SessStatsVlanId != i4VlanIndex) ||
                                         (i4SessStatsIfIndex != i4IfIndex)))
        {
            break;
        }

        nmhGetFsMIFsbSessStatsEnodeKeepAliveCount (i4SessStatsVlanId,
                                                   i4SessStatsIfIndex,
                                                   EnodeSessStatsMacAddress,
                                                   FcfSessStatsMacAddress,
                                                   FCoESessStatsMacAddress,
                                                   &u4EnodeKeepAliveCount);
        nmhGetFsMIFsbSessStatsVNPortKeepAliveCount (i4SessStatsVlanId,
                                                    i4SessStatsIfIndex,
                                                    EnodeSessStatsMacAddress,
                                                    FcfSessStatsMacAddress,
                                                    FCoESessStatsMacAddress,
                                                    &u4VNPortKeepAliveCount);
        if (CfaCliGetIfName ((UINT4) i4SessStatsIfIndex, piIfName) ==
            CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
        FSB_MEMSET (au1ENodeMacAddr, 0, MAC_TO_STRING_LEN);
        FSB_MEMSET (au1FcfMacAddr, 0, MAC_TO_STRING_LEN);
        FSB_MEMSET (au1FCoEMacAddr, 0, MAC_TO_STRING_LEN);
        PrintMacAddress (EnodeSessStatsMacAddress, au1ENodeMacAddr);
        PrintMacAddress (FcfSessStatsMacAddress, au1FcfMacAddr);
        PrintMacAddress (FCoESessStatsMacAddress, au1FCoEMacAddr);
        CliPrintf (CliHandle, "\rVlan ID                       : %d\r\n",
                   i4SessStatsVlanId);
        CliPrintf (CliHandle, "\rENodeIfIndex                  : %s\r\n",
                   piIfName);
        CliPrintf (CliHandle, "\rENodeMAC                      : %s\r\n",
                   au1ENodeMacAddr);
        CliPrintf (CliHandle, "\rFcfMAC                        : %s\r\n",
                   au1FcfMacAddr);
        CliPrintf (CliHandle, "\rFCoEMAC                       : %s\r\n",
                   au1FCoEMacAddr);
        CliPrintf (CliHandle, "\rEnodeKeepAlive                : %d\r\n",
                   u4EnodeKeepAliveCount);
        CliPrintf (CliHandle, "\rVNPortKeepAlive               : %d\r\n",
                   u4VNPortKeepAliveCount);
        CliPrintf (CliHandle, "\n");

        i4VlanIndex = i4SessStatsVlanId;
        i4IfIndex = i4SessStatsIfIndex;
        FSB_MEMCPY (&EnodeMacAddress, EnodeSessStatsMacAddress, MAC_ADDR_LEN);
        FSB_MEMCPY (&FcfMacAddress, FcfSessStatsMacAddress, MAC_ADDR_LEN);
        FSB_MEMCPY (&FCoEMacAddress, FCoESessStatsMacAddress, MAC_ADDR_LEN);

    }
    while (nmhGetNextIndexFsMIFsbSessStatsTable (i4VlanIndex,
                                                 &i4SessStatsVlanId, i4IfIndex,
                                                 &i4SessStatsIfIndex,
                                                 EnodeMacAddress,
                                                 &EnodeSessStatsMacAddress,
                                                 FcfMacAddress,
                                                 &FcfSessStatsMacAddress,
                                                 FCoEMacAddress,
                                                 &FCoESessStatsMacAddress) ==
           SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliShowRunningConfig                          */
/*                                                                           */
/*    Description         : This function displays the FIP-snooping Module's */
/*                          current configuration details                    */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliShowRunningConfig (tCliHandle CliHandle)
{

    FsbCliShowRunningStatusInfoConfig (CliHandle);
    FsbCliShowRunningFIPSnoopingVlanConfig (CliHandle);
    FsbCliShowRunningInterfaceConfig (CliHandle);
    FsbCliShowRunningFIPSnoopingModuleStatusConfig (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliShowRunningStatusInfoConfig                */
/*                                                                           */
/*    Description         : This finction is used to display Global          */
/*                          FIP-snooping configuration                       */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
VOID
FsbCliShowRunningStatusInfoConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE FcMap;
    UINT1               au1ContextName[FSB_ALIAS_MAX_LEN];
    UINT1               au1FcMap[FSB_FCMAP_LEN];
    UINT4               u4HouseKeepingTime = 0;
    UINT4               u4CurrContextId = 0;
    UINT4               u4ContextId = 0;
    INT4                i4SystemControl = 0;
    INT4                i4DefaultVlanId = 0;
    INT4                i4TraceOption = 0;
    INT4                i4TrapStatus = 0;
    INT4                i4FcMapMode = 0;

    FSB_MEMSET (&FcMap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (nmhGetFirstIndexFsMIFsbContextTable (&u4ContextId) != SNMP_SUCCESS)
    {
        return;
    }
    CliPrintf (CliHandle, "!\n\r");
    do
    {
        FSB_MEMSET (au1ContextName, 0, sizeof (au1ContextName));
        FSB_MEMSET (au1FcMap, 0, sizeof (au1FcMap));
        FcMap.pu1_OctetList = au1FcMap;
        FcMap.i4_Length = (INT4) STRLEN (au1FcMap);

        FsbPortGetSwitchAliasName (u4ContextId, au1ContextName);

        CliPrintf (CliHandle, "switch %s\r\n", au1ContextName);

        nmhGetFsMIFsbSystemControl (u4ContextId, &i4SystemControl);
        nmhGetFsMIFsbFcMapMode (u4ContextId, &i4FcMapMode);
        nmhGetFsMIFsbFcmap (u4ContextId, &FcMap);
        nmhGetFsMIFsbHouseKeepingTimePeriod (u4ContextId, &u4HouseKeepingTime);
        nmhGetFsMIFsbTraceOption (u4ContextId, &i4TraceOption);
        nmhGetFsMIFsbTrapStatus (u4ContextId, &i4TrapStatus);
        nmhGetFsMIFsbDefaultVlanId (u4ContextId, &i4DefaultVlanId);

        if (i4SystemControl != FSB_DEFAULT_SYSTEM_CONTROL)
        {
            CliPrintf (CliHandle, "no shutdown fip-snooping\r\n");
        }

        if (i4FcMapMode != FSB_DEFAULT_FCMAP_MODE)
        {
            CliPrintf (CliHandle, "fip-snooping fc-map mode vlan\r\n");
        }

        if (FSB_MEMCMP
            (FcMap.pu1_OctetList, gau1DefaultFcMapValue, FcMap.i4_Length) != 0)
        {
            CliPrintf (CliHandle, "fip-snooping fc-map %02x:%02x:%02x\r\n",
                       FcMap.pu1_OctetList[0], FcMap.pu1_OctetList[1],
                       FcMap.pu1_OctetList[2]);
        }

        if (u4HouseKeepingTime != FSB_DEFAULT_HOUSEKEEPING_TIME)
        {
            CliPrintf (CliHandle,
                       "fip-snooping house-keeping-time-period %d\r\n",
                       u4HouseKeepingTime);
        }

        if (i4TrapStatus != FSB_DEFAULT_TRAP_OPTION)
        {
            CliPrintf (CliHandle, "fip-snooping traps enable\r\n");
        }

#ifndef FSB_CUSTOM_FCOE_TAGGED_WANTED
        if (i4DefaultVlanId != FSB_DEFAULT_VLAN_INIT_VAL)
        {
            CliPrintf (CliHandle, "fip-snooping untagged vlan %d\r\n",
                       i4DefaultVlanId);
        }
#endif

        CliPrintf (CliHandle, "!\n\r");
        u4CurrContextId = u4ContextId;

    }
    while (nmhGetNextIndexFsMIFsbContextTable (u4CurrContextId,
                                               &u4ContextId) == SNMP_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliShowRunningFIPSnoopingVlanConfig           */
/*                                                                           */
/*    Description         : This finction is used to display FCoE VLAN       */
/*                          and its configuration                            */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
VOID
FsbCliShowRunningFIPSnoopingVlanConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE FcMap;
    tSNMP_OCTET_STRING_TYPE FsbPinnedPorts;
    tPortList          *pMemberPorts = NULL;
    INT4                i4VlanIndex = 0;
    UINT1               au1ContextName[FSB_ALIAS_MAX_LEN];
    UINT1               au1FcMap[FSB_FCMAP_LEN];
    UINT4               u4CurrContextId = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4EnabledStatus = 0;
    INT4                i4CurrVlanId = 0;

    FSB_MEMSET (&FsbPinnedPorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    pMemberPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pMemberPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return;
    }

    MEMSET (*pMemberPorts, 0, sizeof (tPortList));
    FsbPinnedPorts.pu1_OctetList = *pMemberPorts;
    FsbPinnedPorts.i4_Length = BRG_PORT_LIST_SIZE;

    if (nmhGetFirstIndexFsMIFsbFIPSnoopingTable (&u4ContextId,
                                                 &i4VlanIndex) == SNMP_FAILURE)
    {
        FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
        return;
    }

    do
    {
        FSB_MEMSET (au1ContextName, 0, sizeof (au1ContextName));
        FSB_MEMSET (au1FcMap, 0, sizeof (au1FcMap));
        FSB_MEMSET (&FcMap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        FcMap.pu1_OctetList = au1FcMap;
        FcMap.i4_Length = 0;

        FsbPortGetSwitchAliasName (u4ContextId, au1ContextName);

        nmhGetFsMIFsbFIPSnoopingEnabledStatus (u4ContextId, i4VlanIndex,
                                               &i4EnabledStatus);
        nmhGetFsMIFsbFIPSnoopingFcmap (u4ContextId, i4VlanIndex, &FcMap);

        nmhGetFsMIFsbFIPSnoopingPinnedPorts (u4ContextId, i4VlanIndex,
                                             &FsbPinnedPorts);

        CliPrintf (CliHandle, "switch %s\n\r", au1ContextName);
        CliPrintf (CliHandle, "fip-snooping fcoe vlan %d\n\r", i4VlanIndex);

        if (FSB_MEMCMP
            (FcMap.pu1_OctetList, gau1DefaultFcMapValue, FcMap.i4_Length) != 0)
        {
            CliPrintf (CliHandle,
                       "fip-snooping fc-map %02x:%02x:%02x vlan %d\r\n",
                       FcMap.pu1_OctetList[0], FcMap.pu1_OctetList[1],
                       FcMap.pu1_OctetList[2], i4VlanIndex);
        }

        if (FsUtilBitListIsAllZeros (FsbPinnedPorts.pu1_OctetList,
                                     BRG_PORT_LIST_SIZE) != OSIX_TRUE)
        {
            CliPrintf (CliHandle, "\rfip-snooping fcoe-vlan %d pinned-ports ",
                       i4VlanIndex);
            if ((CliConfOctetToIfName
                 (CliHandle, " ", NULL, &FsbPinnedPorts,
                  &u4PagingStatus)) == CLI_FAILURE)
            {
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                return;
            }

        }
        CliPrintf (CliHandle, "\n");
        if (i4EnabledStatus != FSB_DEFAULT_VLAN_ENABLED_STATUS)
        {
            CliPrintf (CliHandle, "fip-snooping enable vlan %d\n\r",
                       i4VlanIndex);
        }

        CliPrintf (CliHandle, "!\n\r");

        u4CurrContextId = u4ContextId;
        i4CurrVlanId = i4VlanIndex;

    }
    while (nmhGetNextIndexFsMIFsbFIPSnoopingTable (u4CurrContextId,
                                                   &u4ContextId, i4CurrVlanId,
                                                   &i4VlanIndex) ==
           SNMP_SUCCESS);

    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliShowRunningInterfaceConfig                 */
/*                                                                           */
/*    Description         : This finction is used to display FIP-snooping    */
/*                          interface and its configuration                  */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
VOID
FsbCliShowRunningInterfaceConfig (tCliHandle CliHandle)
{
    INT1               *piIfName;
    INT4                i4PortRole = 0;
    INT4                i4VlanIndex = 0;
    INT4                i4CurrIfIndex = 0;
    INT4                i4CurrVlanId = 0;
    INT4                i4IfIndex = 0;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];

    piIfName = (INT1 *) &au1IfName[0];
    if (nmhGetFirstIndexFsMIFsbIntfTable (&i4VlanIndex,
                                          &i4IfIndex) == SNMP_FAILURE)
    {
        return;
    }
    do
    {
        FSB_MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
        CfaCliConfGetIfName ((UINT4) i4IfIndex, piIfName);
        nmhGetFsMIFsbIntfPortRole (i4VlanIndex, i4IfIndex, &i4PortRole);

        if (i4PortRole == FSB_FCF_FACING)
        {
            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
            CliPrintf (CliHandle, "fip-snooping port-mode %s vlan %d\r\n",
                       "fcf", i4VlanIndex);
        }
        else if (i4PortRole == FSB_ENODE_FCF_FACING)
        {
            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
            CliPrintf (CliHandle, "fip-snooping port-mode %s vlan %d\r\n",
                       "both", i4VlanIndex);
        }

        CliPrintf (CliHandle, "!\n\r");

        i4CurrVlanId = i4VlanIndex;
        i4CurrIfIndex = i4IfIndex;

    }
    while (nmhGetNextIndexFsMIFsbIntfTable (i4CurrVlanId,
                                            &i4VlanIndex, i4CurrIfIndex,
                                            &i4IfIndex) == SNMP_SUCCESS);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliShowRunningFIPSnoopingModuleStatusConfig   */
/*                                                                           */
/*    Description         : This finction is used to display Module Status   */
/*                          for teh Context                                  */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
VOID
FsbCliShowRunningFIPSnoopingModuleStatusConfig (tCliHandle CliHandle)
{
    UINT1               au1ContextName[FSB_ALIAS_MAX_LEN];
    UINT4               u4ContextId = 0;
    UINT4               u4CurrContextId = 0;
    INT4                i4ModuleStatus = 0;

    if (nmhGetFirstIndexFsMIFsbContextTable (&u4ContextId) != SNMP_SUCCESS)
    {
        return;
    }
    do
    {
        FSB_MEMSET (au1ContextName, 0, sizeof (au1ContextName));
        FsbPortGetSwitchAliasName (u4ContextId, au1ContextName);
        nmhGetFsMIFsbModuleStatus (u4ContextId, &i4ModuleStatus);

        if (i4ModuleStatus == FSB_ENABLE)
        {
            CliPrintf (CliHandle, "switch %s\r\n", au1ContextName);
            CliPrintf (CliHandle, "feature fip-snooping enable\r\n");
        }
        CliPrintf (CliHandle, "!\n\r");
        u4CurrContextId = u4ContextId;

    }
    while (nmhGetNextIndexFsMIFsbContextTable (u4CurrContextId,
                                               &u4ContextId) == SNMP_SUCCESS);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbShowRunningConfigInterfaceDetails             */
/*                                                                           */
/*    Description         : This function is used to display FSB             */
/*                         configurations at the port level                  */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler,                         */
/*                          i4Index -   Interfaceindex                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
VOID
FsbShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index)
{
    INT1               *piIfName;
    INT4                i4PortRole = 0;
    INT4                i4VlanIndex = 0;
    INT4                i4CurrIfIndex = 0;
    INT4                i4CurrVlanId = 0;
    INT4                i4IfIndex = 0;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];

    piIfName = (INT1 *) &au1IfName[0];
    if (nmhGetFirstIndexFsMIFsbIntfTable (&i4VlanIndex,
                                          &i4IfIndex) == SNMP_FAILURE)
    {
        return;
    }
    do
    {
        FSB_MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
        CfaCliConfGetIfName ((UINT4) i4IfIndex, piIfName);
        nmhGetFsMIFsbIntfPortRole (i4VlanIndex, i4IfIndex, &i4PortRole);
        if (i4Index == i4IfIndex)
        {
            if (i4PortRole == FSB_FCF_FACING)
            {
                CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                CliPrintf (CliHandle, "fip-snooping port-mode %s vlan %d\r\n",
                           "fcf", i4VlanIndex);
            }
            else if (i4PortRole == FSB_ENODE_FCF_FACING)
            {
                CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                CliPrintf (CliHandle, "fip-snooping port-mode %s vlan %d\r\n",
                           "both", i4VlanIndex);
            }

            CliPrintf (CliHandle, "!\n\r");
        }

        i4CurrVlanId = i4VlanIndex;
        i4CurrIfIndex = i4IfIndex;

    }
    while (nmhGetNextIndexFsMIFsbIntfTable (i4CurrVlanId,
                                            &i4VlanIndex, i4CurrIfIndex,
                                            &i4IfIndex) == SNMP_SUCCESS);

    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : IssFsbShowDebugging                                */
/*                                                                           */
/*     DESCRIPTION      : This function prints FSB Debug level               */
/*                        for interfaces                                     */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IssFsbShowDebugging (tCliHandle CliHandle)
{
    INT4                i4TraceLevel = 0;

    nmhGetFsMIFsbTraceOption (FSB_DEFAULT_CONTEXT_ID, &i4TraceLevel);

    CliPrintf (CliHandle, "\r\nFSB :\n");

    if (i4TraceLevel == FSB_TRC_MAX_VAL)
    {
        CliPrintf (CliHandle, "  FSB All debugging is on \r\n");
        return;
    }
    if (i4TraceLevel & FSB_INIT_SHUT_TRC)
    {
        CliPrintf (CliHandle, "  FSB Init-Shut debugging is on \r\n");
    }
    if (i4TraceLevel & FSB_MGMT_TRC)
    {
        CliPrintf (CliHandle, "  FSB Management debugging is on \r\n");
    }
    if (i4TraceLevel & FSB_CONTROL_PATH_TRC)
    {
        CliPrintf (CliHandle, "  FSB Control Plane debugging is on \r\n");
    }
    if (i4TraceLevel & FSB_OS_RESOURCE_TRC)
    {
        CliPrintf (CliHandle, "  FSB Resource debugging is on \r\n");
    }
    if (i4TraceLevel & FSB_BUFFER_TRC)
    {
        CliPrintf (CliHandle, "  FSB Buffer debugging is on \r\n");
    }
    if (i4TraceLevel & FSB_DUMP_TRC)
    {
        CliPrintf (CliHandle, "  FSB Packet debugging is on \r\n");
    }

    return;
}

#ifdef FSB_RED_DEBUG
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliShowContextFilterEntry                     */
/*                                                                           */
/*    Description         : This function is used to display information     */
/*                          about the filters installed when Fip Snooping    */
/*                          module is enabled                                */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
FsbCliShowContextFilterEntry (tCliHandle CliHandle)
{
    tFsbContextInfo    *pFsbContextInfo = NULL;
    UINT4               u4ContextId = 0;

    CliPrintf (CliHandle, "\r\nFilter Details - tFsbContextInfo Table");
    CliPrintf (CliHandle, "\r\n--------------------------------------");
    CliPrintf (CliHandle, "\r\n%-12s%-20s%-21s%-21s%-22s",
               "Context Id", "Local Filter Id-VD", "Remote Filter Id-VD",
               "Local Filter Id-VRT", "Remote Filter Id-VRT");
    CliPrintf (CliHandle, "\r\n%-12s%-20s%-21s%-21s%-22s\r\n", "----------",
               "------------------", "-------------------",
               "-------------------", "--------------------");

    for (u4ContextId = 0; u4ContextId < FSB_MAX_CONTEXT_COUNT; u4ContextId++)
    {
        pFsbContextInfo = FsbCxtGetContextEntry (u4ContextId);
        if (pFsbContextInfo != NULL)
        {
            CliPrintf (CliHandle, "%-12d", u4ContextId);
            CliPrintf (CliHandle, "%-20d",
                       pFsbContextInfo->FsbLocFilterEntry.FsbVlanDiscovery.
                       u4HwFilterId);
            CliPrintf (CliHandle, "%-21d",
                       pFsbContextInfo->FsbRemFilterEntry.FsbVlanDiscovery.
                       u4HwFilterId);
            CliPrintf (CliHandle, "%-21d",
                       pFsbContextInfo->FsbLocFilterEntry.FsbVlanResponseTagged.
                       u4HwFilterId);
            CliPrintf (CliHandle, "%-22d\n",
                       pFsbContextInfo->FsbRemFilterEntry.FsbVlanResponseTagged.
                       u4HwFilterId);
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliShowVlanFilterEntry                        */
/*                                                                           */
/*    Description         : This function is used to display information     */
/*                          about the filters installed when Fip Snooping    */
/*                          Vlan is enabled                                  */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
FsbCliShowVlanFilterEntry (tCliHandle CliHandle)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbFipSnoopingEntry FsbFipSnoopingEntry;
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;
    UINT1               au1DstMac[MAC_TO_STRING_LEN];

    FSB_MEMSET (&FsbFipSnoopingEntry, 0, sizeof (tFsbFipSnoopingEntry));
    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    pFsbFipSnoopingEntry = (tFsbFipSnoopingEntry *)
        RBTreeGetFirst (gFsbGlobals.FsbFipSnoopingTable);
    if (pFsbFipSnoopingEntry == NULL)
    {
        return;
    }

    CliPrintf (CliHandle, "\r\nFilter Details - tFsbFipSnoopingEntry Table");
    CliPrintf (CliHandle, "\r\n--------------------------------------");
    CliPrintf (CliHandle, "\r\n%-12s%-9s%-19s%-8s%-11s%-11s",
               "Context Id", "Vlan Id", "Destination MAC", "Opcode",
               "SubOpcode", "Filter Id");
    CliPrintf (CliHandle, "\r\n%-12s%-9s%-19s%-8s%-11s%-11s\r\n", "----------",
               "-------", "-----------------", "------", "---------",
               "---------");
    do
    {
        FsbFipSnoopingEntry.u4ContextId = pFsbFipSnoopingEntry->u4ContextId;
        FsbFipSnoopingEntry.u2VlanId = pFsbFipSnoopingEntry->u2VlanId;

        pFsbFilterEntry = (tFsbFilterEntry *)
            RBTreeGetFirst (pFsbFipSnoopingEntry->FsbFilterEntry);
        if (pFsbFilterEntry == NULL)
        {
            return;
        }
        do
        {
            FSB_MEMCPY (FsbFilterEntry.DstMac, pFsbFilterEntry->DstMac,
                        MAC_ADDR_LEN);
            FSB_MEMSET (au1DstMac, 0, MAC_TO_STRING_LEN);
            PrintMacAddress (pFsbFilterEntry->DstMac, au1DstMac);
            CliPrintf (CliHandle, "%-12d", pFsbFipSnoopingEntry->u4ContextId);
            CliPrintf (CliHandle, "%-9d", pFsbFipSnoopingEntry->u2VlanId);
            CliPrintf (CliHandle, "%-19s", au1DstMac);
            CliPrintf (CliHandle, "%-8d",
                       pFsbFilterEntry->u2OpcodeFilterOffsetValue);
            CliPrintf (CliHandle, "%-11d",
                       pFsbFilterEntry->u2SubOpcodeFilterOffsetValue);
            CliPrintf (CliHandle, "%-11d\n", pFsbFilterEntry->u4HwFilterId);

            pFsbFilterEntry = (tFsbFilterEntry *)
                RBTreeGetNext (pFsbFipSnoopingEntry->FsbFilterEntry,
                               (tRBElem *) & FsbFilterEntry, NULL);

        }
        while (pFsbFilterEntry != NULL);

        pFsbFipSnoopingEntry = (tFsbFipSnoopingEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFipSnoopingTable,
                           (tRBElem *) & FsbFipSnoopingEntry, NULL);
    }
    while (pFsbFipSnoopingEntry != NULL);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliShowSessionFilterEntry                     */
/*                                                                           */
/*    Description         : This function is used to display information     */
/*                          about the filters installed when Fip Snooping    */
/*                          Session is established                           */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
FsbCliShowSessionFilterEntry (tCliHandle CliHandle)
{
    FsbPrintSessionFilterEntry (CliHandle);
    FsbPrintFCoEFilterEntry (CliHandle);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbPrintSessionFilterEntry                       */
/*                                                                           */
/*    Description         : This function is used to display information     */
/*                          about the filters installed when Fip Snooping    */
/*                          Session is established                           */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
FsbPrintSessionFilterEntry (tCliHandle CliHandle)
{
    tFsbFipSessEntry   *pFsbFipSessEntry = NULL;
    tFsbFipSessEntry    FsbFipSessEntry;
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;
    UINT1               au1DstMac[MAC_TO_STRING_LEN];
    UINT1               au1SrcMac[MAC_TO_STRING_LEN];

    FSB_MEMSET (&FsbFipSessEntry, 0, sizeof (tFsbFipSessEntry));
    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    pFsbFipSessEntry = (tFsbFipSessEntry *)
        RBTreeGetFirst (gFsbGlobals.FsbFipSessTable);
    if (pFsbFipSessEntry == NULL)
    {
        return;
    }

    CliPrintf (CliHandle, "\r\nFilter Details - tFsbFipSessEntry Table");
    CliPrintf (CliHandle, "\r\n--------------------------------------");
    CliPrintf (CliHandle, "\r\n%-12s%-9s%-19s%-19s%-8s%-11s%-11s%-11s%-11s",
               "Context Id", "Vlan Id", "Destination MAC", "Source MAC",
               "Opcode", "SubOpcode", "AggIndex", "ClassId", "Filter Id");
    CliPrintf (CliHandle, "\r\n%-12s%-9s%-19s%-19s%-8s%-11s%-11s%-11s%-11s\r\n",
               "----------", "-------", "-----------------",
               "-----------------", "------", "---------", "---------",
               "---------", "---------");

    do
    {
        FsbFipSessEntry.u2VlanId = pFsbFipSessEntry->u2VlanId;
        FsbFipSessEntry.u4ENodeIfIndex = pFsbFipSessEntry->u4ENodeIfIndex;
        FSB_MEMCPY (FsbFipSessEntry.ENodeMacAddr,
                    pFsbFipSessEntry->ENodeMacAddr, MAC_ADDR_LEN);

        pFsbFilterEntry = (tFsbFilterEntry *)
            RBTreeGetFirst (pFsbFipSessEntry->FsbSessFilterEntry);
        if (pFsbFilterEntry == NULL)
        {
            return;
        }
        do
        {
            FSB_MEMCPY (FsbFilterEntry.SrcMac, pFsbFilterEntry->SrcMac,
                        MAC_ADDR_LEN);
            FSB_MEMCPY (FsbFilterEntry.DstMac, pFsbFilterEntry->DstMac,
                        MAC_ADDR_LEN);
            FsbFilterEntry.u2OpcodeFilterOffsetValue =
                pFsbFilterEntry->u2OpcodeFilterOffsetValue;
            FsbFilterEntry.u2SubOpcodeFilterOffsetValue =
                pFsbFilterEntry->u2SubOpcodeFilterOffsetValue;
            FSB_MEMSET (au1SrcMac, 0, MAC_TO_STRING_LEN);
            FSB_MEMSET (au1DstMac, 0, MAC_TO_STRING_LEN);
            PrintMacAddress (pFsbFilterEntry->SrcMac, au1SrcMac);
            PrintMacAddress (pFsbFilterEntry->DstMac, au1DstMac);

            CliPrintf (CliHandle, "%-12d", pFsbFipSessEntry->u4ContextId);
            CliPrintf (CliHandle, "%-9d", pFsbFipSessEntry->u2VlanId);
            CliPrintf (CliHandle, "%-19s", au1DstMac);
            CliPrintf (CliHandle, "%-19s", au1SrcMac);
            CliPrintf (CliHandle, "%-8d",
                       pFsbFilterEntry->u2OpcodeFilterOffsetValue);
            CliPrintf (CliHandle, "%-11d",
                       pFsbFilterEntry->u2SubOpcodeFilterOffsetValue);
            CliPrintf (CliHandle, "%-11d", pFsbFilterEntry->u4AggIndex);
            CliPrintf (CliHandle, "%-11d", pFsbFilterEntry->u2ClassId);
            CliPrintf (CliHandle, "%-11d\n", pFsbFilterEntry->u4HwFilterId);

            pFsbFilterEntry = (tFsbFilterEntry *)
                RBTreeGetNext (pFsbFipSessEntry->FsbSessFilterEntry,
                               (tRBElem *) & FsbFilterEntry, NULL);

        }
        while (pFsbFilterEntry != NULL);

        pFsbFipSessEntry = (tFsbFipSessEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFipSessTable,
                           (tRBElem *) & FsbFipSessEntry, NULL);
    }
    while (pFsbFipSessEntry != NULL);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbPrintFCoEFilterEntry                          */
/*                                                                           */
/*    Description         : This function is used to display information     */
/*                          about the filters installed when Fip Snooping    */
/*                          Session is established                           */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
FsbPrintFCoEFilterEntry (tCliHandle CliHandle)
{
    tFsbFipSessFCoEEntry *pFsbFipSessFCoEEntry = NULL;
    tFsbFipSessFCoEEntry FsbFipSessFCoEEntry;
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;
    UINT1               au1DstMac[MAC_TO_STRING_LEN];
    UINT1               au1SrcMac[MAC_TO_STRING_LEN];

    FSB_MEMSET (&FsbFipSessFCoEEntry, 0, sizeof (tFsbFipSessFCoEEntry));
    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    pFsbFipSessFCoEEntry = (tFsbFipSessFCoEEntry *)
        RBTreeGetFirst (gFsbGlobals.FsbFipSessFCoETable);
    if (pFsbFipSessFCoEEntry == NULL)
    {
        return;
    }
    CliPrintf (CliHandle, "\r\nFilter Details - tFsbFipSessFCoEEntry Table");
    CliPrintf (CliHandle, "\r\n-------------------------------------------");
    CliPrintf (CliHandle, "\r\n%-12s%-9s%-19s%-19s%-8s%-11s%-11s%-11s%-11s",
               "Context Id", "Vlan Id", "Destination MAC", "Source MAC",
               "Opcode", "SubOpcode", "AggIndex", "ClassId", "Filter Id");
    CliPrintf (CliHandle, "\r\n%-12s%-9s%-19s%-19s%-8s%-11s%-11s%-11s%-11s\r\n",
               "----------", "-------", "-----------------",
               "-----------------", "------", "---------", "---------",
               "---------", "---------");
    do
    {
        FsbFipSessFCoEEntry.u2VlanId = pFsbFipSessFCoEEntry->u2VlanId;
        FsbFipSessFCoEEntry.u4ENodeIfIndex =
            pFsbFipSessFCoEEntry->u4ENodeIfIndex;
        FSB_MEMCPY (FsbFipSessFCoEEntry.ENodeMacAddr,
                    pFsbFipSessFCoEEntry->ENodeMacAddr, MAC_ADDR_LEN);
        FSB_MEMCPY (FsbFipSessFCoEEntry.FcfMacAddr,
                    pFsbFipSessFCoEEntry->FcfMacAddr, MAC_ADDR_LEN);
        FSB_MEMCPY (FsbFipSessFCoEEntry.FCoEMacAddr,
                    pFsbFipSessFCoEEntry->FCoEMacAddr, MAC_ADDR_LEN);

        pFsbFilterEntry = (tFsbFilterEntry *)
            RBTreeGetFirst (pFsbFipSessFCoEEntry->FsbSessFCoEFilterEntry);
        if (pFsbFilterEntry == NULL)
        {
            return;
        }
        do
        {
            FSB_MEMCPY (FsbFilterEntry.SrcMac, pFsbFilterEntry->SrcMac,
                        MAC_ADDR_LEN);
            FSB_MEMCPY (FsbFilterEntry.DstMac, pFsbFilterEntry->DstMac,
                        MAC_ADDR_LEN);
            FsbFilterEntry.u2OpcodeFilterOffsetValue =
                pFsbFilterEntry->u2OpcodeFilterOffsetValue;
            FsbFilterEntry.u2SubOpcodeFilterOffsetValue =
                pFsbFilterEntry->u2SubOpcodeFilterOffsetValue;
            FSB_MEMSET (au1SrcMac, 0, MAC_TO_STRING_LEN);
            FSB_MEMSET (au1DstMac, 0, MAC_TO_STRING_LEN);
            PrintMacAddress (pFsbFilterEntry->SrcMac, au1SrcMac);
            PrintMacAddress (pFsbFilterEntry->DstMac, au1DstMac);

            CliPrintf (CliHandle, "%-12d",
                       pFsbFipSessFCoEEntry->pFsbFipSessEntry->u4ContextId);
            CliPrintf (CliHandle, "%-9d", pFsbFipSessFCoEEntry->u2VlanId);
            CliPrintf (CliHandle, "%-19s", au1DstMac);
            CliPrintf (CliHandle, "%-19s", au1SrcMac);
            CliPrintf (CliHandle, "%-8d",
                       pFsbFilterEntry->u2OpcodeFilterOffsetValue);
            CliPrintf (CliHandle, "%-11d",
                       pFsbFilterEntry->u2SubOpcodeFilterOffsetValue);
            CliPrintf (CliHandle, "%-11d", pFsbFilterEntry->u4AggIndex);
            CliPrintf (CliHandle, "%-11d", pFsbFilterEntry->u2ClassId);
            CliPrintf (CliHandle, "%-11d\n", pFsbFilterEntry->u4HwFilterId);

            pFsbFilterEntry = (tFsbFilterEntry *)
                RBTreeGetNext (pFsbFipSessFCoEEntry->FsbSessFCoEFilterEntry,
                               (tRBElem *) & FsbFilterEntry, NULL);

        }
        while (pFsbFilterEntry != NULL);

        pFsbFipSessFCoEEntry = (tFsbFipSessFCoEEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFipSessFCoETable,
                           (tRBElem *) & FsbFipSessFCoEEntry, NULL);
    }
    while (pFsbFipSessFCoEEntry != NULL);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliShowSchannelFilterEntry                    */
/*                                                                           */
/*    Description         : This function is used to display information     */
/*                          about the SChannel filters                       */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
FsbCliShowSchannelFilterEntry (tCliHandle CliHandle)
{
    tFsbSChannelFilterEntry *pFsbSChannelFilterEntry = NULL;
    tFsbSChannelFilterEntry FsbSChannelFilterEntry;

    FSB_MEMSET (&FsbSChannelFilterEntry, 0, sizeof (tFsbSChannelFilterEntry));

    pFsbSChannelFilterEntry = (tFsbSChannelFilterEntry *)
        RBTreeGetFirst (gFsbGlobals.FsbSChannelFilterTable);

    if (pFsbSChannelFilterEntry == NULL)
    {
        return;
    }

    CliPrintf (CliHandle, "\r\nFilter Details - tFsbSChannelFilterEntry Table");
    CliPrintf (CliHandle, "\r\n-------------------------------------------");
    CliPrintf (CliHandle, "\r\n%-12s%-9s%-9s%-14s",
               "Context Id", "Vlan Id", "IfIndex", "Hw Filter Id");
    CliPrintf (CliHandle, "\r\n%-12s%-9s%-9s%-14s\r\n",
               "----------", "-------", "-------", "------------");
    do
    {
        FsbSChannelFilterEntry.u2VlanId = pFsbSChannelFilterEntry->u2VlanId;
        FsbSChannelFilterEntry.u4IfIndex = pFsbSChannelFilterEntry->u4IfIndex;

        CliPrintf (CliHandle, "%-12d", pFsbSChannelFilterEntry->u4ContextId);
        CliPrintf (CliHandle, "%-9d", pFsbSChannelFilterEntry->u2VlanId);
        CliPrintf (CliHandle, "%-9d", pFsbSChannelFilterEntry->u4IfIndex);
        CliPrintf (CliHandle, "%-14d\n", pFsbSChannelFilterEntry->u4HwFilterId);

        pFsbSChannelFilterEntry = (tFsbSChannelFilterEntry *)
            RBTreeGetNext (gFsbGlobals.FsbSChannelFilterTable,
                           (tRBElem *) & FsbSChannelFilterEntry, NULL);
    }
    while (pFsbSChannelFilterEntry != NULL);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliShowFcfFilterEntry                         */
/*                                                                           */
/*    Description         : This function is used to display information     */
/*                          about the filter details of FCF                  */
/*                                                                           */
/*    Input(s)            : CliHandle -  CLIHandler                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
FsbCliShowFcfFilterEntry (tCliHandle CliHandle)
{
    tFsbFipSnoopingEntry *pFsbFipSnoopingEntry = NULL;
    tFsbFipSnoopingEntry FsbFipSnoopingEntry;
    tFsbFilterEntry    *pFsbFilterEntry = NULL;
    tFsbFilterEntry     FsbFilterEntry;
    UINT1               au1DstMac[MAC_TO_STRING_LEN];

    FSB_MEMSET (&FsbFipSnoopingEntry, 0, sizeof (tFsbFipSnoopingEntry));
    FSB_MEMSET (&FsbFilterEntry, 0, sizeof (tFsbFilterEntry));

    pFsbFipSnoopingEntry = (tFsbFipSnoopingEntry *)
        RBTreeGetFirst (gFsbGlobals.FsbFipSnoopingTable);
    if (pFsbFipSnoopingEntry == NULL)
    {
        return;
    }

    CliPrintf (CliHandle, "\r\nFilter Details - FCF");
    CliPrintf (CliHandle, "\r\n--------------------------------------");
    CliPrintf (CliHandle, "\r\n%-12s%-9s%-19s%-8s%-11s%-11s",
               "Context Id", "Vlan Id", "Destination MAC", "Opcode",
               "SubOpcode", "Filter Id");
    CliPrintf (CliHandle, "\r\n%-12s%-9s%-19s%-8s%-11s%-11s\r\n", "----------",
               "-------", "-----------------", "------", "---------",
               "---------");
    do
    {
        FsbFipSnoopingEntry.u4ContextId = pFsbFipSnoopingEntry->u4ContextId;
        FsbFipSnoopingEntry.u2VlanId = pFsbFipSnoopingEntry->u2VlanId;

        pFsbFilterEntry = (tFsbFilterEntry *)
            RBTreeGetFirst (pFsbFipSnoopingEntry->FsbFilterEntry);
        if (pFsbFilterEntry == NULL)
        {
            return;
        }
        do
        {
            FSB_MEMCPY (FsbFilterEntry.DstMac, pFsbFilterEntry->DstMac,
                        MAC_ADDR_LEN);
            FSB_MEMSET (au1DstMac, 0, MAC_TO_STRING_LEN);

            if ((FSB_MEMCMP
                 (pFsbFilterEntry->DstMac, ALL_ZERO_MAC, MAC_ADDR_LEN) != 0)
                &&
                (FSB_MEMCMP
                 (pFsbFilterEntry->DstMac, ALL_ENODE_MAC, MAC_ADDR_LEN) != 0)
                &&
                (FSB_MEMCMP (pFsbFilterEntry->DstMac, ALL_FCF_MAC, MAC_ADDR_LEN)
                 != 0))
            {
                PrintMacAddress (pFsbFilterEntry->DstMac, au1DstMac);
                CliPrintf (CliHandle, "%-12d",
                           pFsbFipSnoopingEntry->u4ContextId);
                CliPrintf (CliHandle, "%-9d", pFsbFipSnoopingEntry->u2VlanId);
                CliPrintf (CliHandle, "%-19s", au1DstMac);
                CliPrintf (CliHandle, "%-8d",
                           pFsbFilterEntry->u2OpcodeFilterOffsetValue);
                CliPrintf (CliHandle, "%-11d",
                           pFsbFilterEntry->u2SubOpcodeFilterOffsetValue);
                CliPrintf (CliHandle, "%-11d\n", pFsbFilterEntry->u4HwFilterId);
            }

            pFsbFilterEntry = (tFsbFilterEntry *)
                RBTreeGetNext (pFsbFipSnoopingEntry->FsbFilterEntry,
                               (tRBElem *) & FsbFilterEntry, NULL);

        }
        while (pFsbFilterEntry != NULL);

        pFsbFipSnoopingEntry = (tFsbFipSnoopingEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFipSnoopingTable,
                           (tRBElem *) & FsbFipSnoopingEntry, NULL);
    }
    while (pFsbFipSnoopingEntry != NULL);

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbCliShowSessionEntries                         */
/*                                                                           */
/*    Description         : This function is used to display session         */
/*                          entries                                          */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS / CLI_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsbCliShowSessionEntries (tCliHandle CliHandle)
{
    tFsbFipSessEntry   *pFsbFipSessEntry = NULL;
    tFsbFipSessEntry    FsbFipSessEntry;

    FSB_MEMSET (&FsbFipSessEntry, 0, sizeof (tFsbFipSessEntry));

    pFsbFipSessEntry = (tFsbFipSessEntry *)
        RBTreeGetFirst (gFsbGlobals.FsbFipSessTable);
    if (pFsbFipSessEntry == NULL)
    {
        return CLI_SUCCESS;
    }
    CliPrintf (CliHandle, "\r\nFIP-snooping Session Entries\r\n");
    CliPrintf (CliHandle, "---------------------------------\r\n");

    do
    {
        FsbFipSessEntry.u2VlanId = pFsbFipSessEntry->u2VlanId;
        FsbFipSessEntry.u4ENodeIfIndex = pFsbFipSessEntry->u4ENodeIfIndex;
        FSB_MEMCPY (&FsbFipSessEntry.ENodeMacAddr,
                    pFsbFipSessEntry->ENodeMacAddr, MAC_ADDR_LEN);

        CliPrintf (CliHandle, "VlanId       : %d\r\n",
                   pFsbFipSessEntry->u2VlanId);
        CliPrintf (CliHandle, "ENodeIfIndex : %d\r\n",
                   pFsbFipSessEntry->u4ENodeIfIndex);
        CliPrintf (CliHandle,
                   "ENodeMacAddr : %02x:%02x:%02x:%02x:%02x:%02x\r\n",
                   pFsbFipSessEntry->ENodeMacAddr[0],
                   pFsbFipSessEntry->ENodeMacAddr[1],
                   pFsbFipSessEntry->ENodeMacAddr[2],
                   pFsbFipSessEntry->ENodeMacAddr[3],
                   pFsbFipSessEntry->ENodeMacAddr[4],
                   pFsbFipSessEntry->ENodeMacAddr[5]);
        CliPrintf (CliHandle, "FcMap        : %02x:%02x:%02x\r\n",
                   pFsbFipSessEntry->au1FcMap[0], pFsbFipSessEntry->au1FcMap[1],
                   pFsbFipSessEntry->au1FcMap[2]);
        CliPrintf (CliHandle,
                   "Name Id      : %02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x\r\n",
                   pFsbFipSessEntry->au1NameId[0],
                   pFsbFipSessEntry->au1NameId[1],
                   pFsbFipSessEntry->au1NameId[2],
                   pFsbFipSessEntry->au1NameId[3],
                   pFsbFipSessEntry->au1NameId[4],
                   pFsbFipSessEntry->au1NameId[5],
                   pFsbFipSessEntry->au1NameId[6],
                   pFsbFipSessEntry->au1NameId[7]);
        CliPrintf (CliHandle, "Context Id   : %d\r\n",
                   pFsbFipSessEntry->u4ContextId);
        CliPrintf (CliHandle, "FcfIfIndex   : %d\r\n",
                   pFsbFipSessEntry->u4FcfIfIndex);
        CliPrintf (CliHandle, "State        : %s\r\n\n",
                   FsbErrString[pFsbFipSessEntry->u1State]);

        pFsbFipSessEntry = (tFsbFipSessEntry *)
            RBTreeGetNext (gFsbGlobals.FsbFipSessTable,
                           (tRBElem *) & FsbFipSessEntry, NULL);
    }
    while (pFsbFipSessEntry != NULL);
    return CLI_SUCCESS;

}
#endif

#endif
