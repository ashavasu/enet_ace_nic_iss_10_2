
# Copyright (C) 2012 Aricent Inc . All Rights Reserved
# $Id: make.h,v 1.1
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            included for building the FutureNTP	     |
# |                            product.                                      |
# |                                                                          |
# +--------------------------------------------------------------------------+


include ../LR/make.h
include ../LR/make.rule


# Set the PROJ_BASE_DIR as the directory where you untar the project files
PROJECT_NAME		= FutureNTP
PROJECT_BASE_DIR	= ${BASE_DIR}/ntps
PROJECT_SOURCE_DIR	= ${PROJECT_BASE_DIR}/src
PROJECT_INCLUDE_DIR	= ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR	= ${PROJECT_BASE_DIR}/obj

PROJECT_COMMON		=${BASE_DIR}/inc/
SYSLOG_INCLUDES		=${BASE_DIR}/syslog/inc
FSIP_INCLUDES       =$(BASE_DIR)/netip/fsip/ip/inc
CLI_INCLUDES        =$(BASE_DIR)/inc/cli/

OPENNTP_INCLUDE         =${OPENSOURCE_DIR}/ntps/include

# Specify the project level compilation switches here
PROJECT_COMPILATION_SWITCHES = 


PROJECT_FINAL_INCLUDES_DIRS	= -I$(PROJECT_INCLUDE_DIR) -I$(COMMON_INCLUDE_DIRS) -I$(OPENNTP_INCLUDE) -I$(SYSLOG_INCLUDES) -I$(FSIP_INCLUDES) -I$(CLI_INCLUDES)

PROJECT_DEPENDENCIES	= $(COMMON_DEPENDENCIES) \
				$(PROJECT_FINAL_INCLUDE_FILES) \
				$(PROJECT_BASE_DIR)/Makefile \
				$(PROJECT_BASE_DIR)/make.h \
