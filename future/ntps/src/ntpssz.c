/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: ntpscli.c, 2016/06/07
 *
 * Description:This file contains the CLI's routines.
 *
 * *******************************************************************/
#define _NTPSZ_C
#include "ntpsinc.h"
extern INT4 IssSzRegisterModuleSizingParams(CHR1 * pu1ModName,
											tFsModSizingParams *
											pModSizingParams);
extern INT4 IssSzRegisterModulePoolId(CHR1 * pu1ModName,
									  tMemPoolId * pModPoolId);

/************************************************************************/
/* Function Name : NtpsSizingMemCreateMemPools */
/* Description : Allocates the memory Pool for the Ntp module */
/* Input(s) : None */
/* Output(s) : None.  */
/* Returns : OSIX_SUCCESS or OSIX_FAILURE */
/************************************************************************/
INT4 NtpSizingMemCreateMemPools()
{
	UINT4 u4RetVal;
	INT4 i4SizingId;

	for (i4SizingId = 0; i4SizingId < NTP_MAX_SIZING_ID; i4SizingId++)
	{
		u4RetVal =
			MemCreateMemPool(FsNTPSizingParams[i4SizingId].u4StructSize,
							 FsNTPSizingParams
							 [i4SizingId].u4PreAllocatedUnits,
							 MEM_DEFAULT_MEMORY_TYPE,
							 &(NTPMemPoolIds[i4SizingId]));
		if (u4RetVal ==  MEM_FAILURE)
		{
			NtpSizingMemDeleteMemPools();
			return OSIX_FAILURE;
		}
	}
	return OSIX_SUCCESS;
}


/************************************************************************/
/* Function Name : NtpsSzRegisterModuleSizingParams */
/* Description : Registers NTP module to sizing module */
/* Input(s) : Module Name */
/* Output(s) : None.  */
/* Returns : OSIX_SUCCESS or OSIX_FAILURE */
/************************************************************************/
INT4 NtpSzRegisterModuleSizingParams(CHR1 * pu1ModName)
{
	/* Copy the Module Name */
	IssSzRegisterModuleSizingParams(pu1ModName, FsNTPSizingParams);
	IssSzRegisterModulePoolId(pu1ModName, NTPMemPoolIds);
	return OSIX_SUCCESS;
}


/************************************************************************/
/* Function Name : NtpsSizingMemDeleteMemPools */
/* Description : Deletes All the Memory Pools reserved for NTP module */
/* Input(s) : None */
/* Output(s) : None.  */
/* Returns : None */
/************************************************************************/
VOID NtpSizingMemDeleteMemPools()
{
	INT4 i4SizingId;

	for (i4SizingId = 0; i4SizingId < NTP_MAX_SIZING_ID; i4SizingId++)
	{
		if (NTPMemPoolIds[i4SizingId] != 0)
		{
			MemDeleteMemPool(NTPMemPoolIds[i4SizingId]);
			NTPMemPoolIds[i4SizingId] = 0;
		}
	}
	return;
}
