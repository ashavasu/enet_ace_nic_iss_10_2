/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: ntpscli.c, 2016/06/07
 *
 * Description:This file contains the CLI's routines.
 *
 * *******************************************************************/
#ifndef _NTPS_CLI_C
#define _NTPS_CLI_C
#include "ntpsinc.h"
#include "fsntpscli.h"

UINT1 au1FsNtpUnicastServerAddr[SNMP_MAX_OCTETSTRING_SIZE + 1];
UINT1 au1FsNtpUnicastNextServerAddr[SNMP_MAX_OCTETSTRING_SIZE + 1];


/********************************************************************
*    Function Name      : cli_process_ntps_cmd									
*    Description        : This function takes in variable no of arguments	
*                         and process the commands for the NTP module		
*                         defined in ntpscli.h.
*
*    Input(s)           : CliHandle, u4Command								 
*    Output(s)          :														
*    Returns            : CLI_SUCCESS/CLI_FAILURE                        
*********************************************************************/

INT4 cli_process_ntps_cmd(tCliHandle CliHandle, UINT4 u4Command, ...)
{
	va_list ap;
	UINT4 *args[CLI_MAX_ARGS];
	INT1 argno = 0;
	INT4 i4RetStatus = CLI_FAILURE;
	INT4 i4KeyId = 0;

	UINT4 u4ErrorCode = 0;
	UINT1 au1UniServAddr[DNS_MAX_QUERY_LEN + 2];
	UINT4 u4Err = 0;
	UINT4 u4authtype = 0;

	tNtpsServer NtpUcastServer;
	UINT1 u1RetVal = 0;
	tNtpsServer NtpsServer;
	tNtpsServer *pNtpsServerDB;
	UINT1 u1DefaultRestrict = CLI_DISABLE;
	UINT4 u4Flag = 0;
	UINT4 u4Mask = 0;
	tIPvXAddr Ipvxaddr;
	tIPvXAddr NullAddr;
	UINT4 u4Addr = 0;
	UINT4 u4TmpAddress = 0;
	INT4 i4IfaceIndex = 0;
#ifdef  IP6_WANTED
	tIp6Addr Ip6Addr;
#endif
	tSNMP_OCTET_STRING_TYPE fsNtpUnicastServerAddr;
	MEMSET(&NtpsServer, 0, sizeof(tNtpsServer));
	MEMSET(&fsNtpUnicastServerAddr, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
	MEMSET(au1UniServAddr, 0, DNS_MAX_QUERY_LEN + 2);
	MEMSET(&NtpUcastServer, 0, sizeof(tNtpsServer));

	CliRegisterLock(CliHandle, NtpsLock, NtpsUnLock);
	NtpsLock();

	fsNtpUnicastServerAddr.pu1_OctetList = au1UniServAddr;

	va_start(ap, u4Command);
	va_arg(ap, UINT1 *);


	while (1)
	{
		args[argno++] = va_arg(ap, UINT4 *);
		if (argno == CLI_MAX_ARGS)
		{
			break;
		}
	}
	va_end(ap);
	switch (u4Command)
	{
	case CLI_NTPS_SET_ADMIN_STATUS:
		{
			i4RetStatus = NtpsSetAdminStatus(CLI_PTR_TO_I4(args[0]));
		}
		break;
	case CLI_NTPS_SET_INT_STATUS:
		{
			i4IfaceIndex = CLI_GET_IFINDEX();

			if (i4IfaceIndex == CLI_ERROR)
			{
				CliPrintf(CliHandle, "%% Invalid Interface. \r\n");
				i4RetStatus = CLI_FAILURE;
			}
			else
			{
				i4RetStatus =
					NtpsSetIntStatus((UINT4)i4IfaceIndex, CLI_PTR_TO_I4(args[0]));
			}
		}
		break;
	case NTPS_CLI_SET_AUTH:

		i4RetStatus = NtpsCliSetAuth(NTP_AUTH_ENABLE);
		break;

	case NTPS_CLI_SET_NO_AUTH:
		i4RetStatus = NtpsCliSetAuth(NTP_AUTH_DISABLE);
		break;


	case NTPS_CLI_SET_AUTH_KEY:
		if (args[0] != NULL && args[1] != NULL)
		{
			i4KeyId = *(INT4 *) (args[0]);
		}
		if (args[2] != NULL)
		{
			u4authtype = CLI_PTR_TO_U4(args[2]);
		}
		i4RetStatus =
			NtpsCliSetAuthKey(CliHandle, i4KeyId, (UINT1 *) args[1],
							  u4authtype);
		break;

	case NTPS_CLI_SET_NO_AUTH_KEY:
		i4KeyId = *(INT4 *) (args[0]);
		i4RetStatus = NtpsCliRemoveKey(CliHandle, i4KeyId);
		break;

	case NTPS_CLI_TRUSTED_KEY:
		i4RetStatus = NtpsCliConfigureTrustedkey(CliHandle,
												 (UINT2 *) args[0],
												 (INT4) NTP_KEY_TRUSTED,
												 CLI_PTR_TO_U4(args[1]));
		break;

	case NTPS_CLI_NO_TRUSTED_KEY:
		i4RetStatus = NtpsCliConfigureTrustedkey(CliHandle,
												 (UINT2 *) args[0],
												 (INT4) NTP_NO_KEY_TRUSTED,
												 CLI_PTR_TO_U4(args[1]));
		break;
	case CLI_NTPS_SERVER_EXECUTE:
		i4RetStatus = nmhTestv2FsNtpsServerExecute(&u4ErrorCode, CLI_PTR_TO_I4(args[0]));
		if (i4RetStatus != SNMP_SUCCESS)
        {
		    NTP_TRC(NTP_MGMT_TRC, "nmhTestv2FsNtpsServerExecute failed to set\n");
            CliPrintf(CliHandle,"%% NTP Service not installed \r\n");
            i4RetStatus = CLI_FAILURE;
        }
        else
        {
		    i4RetStatus = nmhSetFsNtpsServerExecute(CLI_PTR_TO_I4(args[0]));
    		if (i4RetStatus != SNMP_SUCCESS)
	    	{
		    	NTP_TRC(NTP_MGMT_TRC, "nmhSetFsNtpsServerExecute failed to set\n");
			    i4RetStatus = CLI_FAILURE;
    		}
        }
		break;
	case CLI_NTPS_SET_RESTRICT:
	case CLI_NTPS_NO_SET_RESTRICT:
#ifdef IP6_WANTED
		MEMSET(&Ip6Addr, 0, sizeof(tIp6Addr));
#endif
		MEMSET(&Ipvxaddr, 0, sizeof(tIPvXAddr));
		MEMSET(&NullAddr, 0, sizeof(tIPvXAddr));


		if (CLI_PTR_TO_U4(args[3]) == CLI_DISABLE)
		{
			u1DefaultRestrict = CLI_DISABLE;
		}
		else
		{
			u1DefaultRestrict = CLI_ENABLE;
		}
		if (u1DefaultRestrict == CLI_DISABLE)
		{
			if (args[0] != NULL)
			{					/* Ipv4 */
				Ipvxaddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
				MEMCPY(&u4TmpAddress, args[0], sizeof(UINT4));
				u4Addr = OSIX_HTONL(u4TmpAddress);
				MEMCPY((Ipvxaddr.au1Addr), &u4Addr, sizeof(UINT4));
				Ipvxaddr.u1AddrLen = sizeof(UINT4);
			}
#ifdef IP6_WANTED
			else if (args[1] != NULL)
			{					/* Ipv6 */
				Ipvxaddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
				if (0 != (INET_ATON6(args[1], &Ip6Addr)))
				{
					MEMCPY(Ipvxaddr.au1Addr, &Ip6Addr, IPVX_IPV6_ADDR_LEN);
					Ipvxaddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;
				}
				else
				{
					CLI_SET_ERR(CLI_IPV6_INVALID_ADDRESS);
					break;
				}

			}
#else
			else if (args[2] != NULL)
			{					/* Ipv6 */
				CLI_SET_ERR(CLI_NTP_ERR_INVALID_IP_ADDR);
				break;
			}
#endif
			if (args[2] != NULL)
			{					/* mask */

                u4Mask = (UINT4) CLI_ATOI (args[2]);
				if (((Ipvxaddr.u1Afi == IPVX_ADDR_FMLY_IPV4) ||
					 (Ipvxaddr.u1Afi == IPVX_ADDR_FMLY_IPV4)) &&
					(u4Mask == 0) &&
					(MEMCMP
					 (Ipvxaddr.au1Addr, NullAddr.au1Addr,
					  IPVX_MAX_INET_ADDR_LEN) == 0))
				{
					CLI_SET_ERR(CLI_NTP_ERR_INVALID_IP_ADDR);
				}

				if (u4Mask == 0)
				{
					CLI_SET_ERR(CLI_NTP_ERR_INVALID_IP_MASK);
					break;
				}
				if ((Ipvxaddr.u1Afi == IPVX_ADDR_FMLY_IPV4) && (u4Mask > 32))
				{
					CLI_SET_ERR(CLI_NTP_ERR_INVALID_IP_MASK);
					break;
				}
#ifdef IP6_WANTED
				if ((Ipvxaddr.u1Afi == IPVX_ADDR_FMLY_IPV6) && (u4Mask > 128))
				{
					CLI_SET_ERR(CLI_NTP_ERR_INVALID_IP_MASK);
					break;
				}
#endif
			}
			else
			{
				if (Ipvxaddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
				{
					u4Mask = IPVX_IPV4_MAX_MASK_LEN;
				}
#ifdef IP6_WANTED
				if ((Ipvxaddr.u1Afi == IPVX_ADDR_FMLY_IPV6) && (u4Mask == 0))
				{
					u4Mask = MAX_IPV6_MASK_LEN;
				}
#endif
			}
		}
		else
		{						/* default restrict */

			if (CLI_PTR_TO_U4(args[6]) == IPVX_ADDR_FMLY_IPV4)
			{
				Ipvxaddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
				Ipvxaddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
				u4Mask = 32;
			}
			if (CLI_PTR_TO_U4(args[6]) == IPVX_ADDR_FMLY_IPV6)
			{
				Ipvxaddr.u1Afi = IPVX_ADDR_FMLY_IPV6;
				Ipvxaddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;
				u4Mask = 128;
			}
		}
		u4Flag = CLI_PTR_TO_U4(args[4]);

		if (CLI_PTR_TO_U4(args[5]) == CLI_ENABLE)
		{
			NtpsSetRestrict(&Ipvxaddr, (UINT2)u4Mask, u1DefaultRestrict, (UINT2)u4Flag);
		}
		else
		{
			NtpsNoSetRestrict(&Ipvxaddr, (UINT2)u4Mask, u1DefaultRestrict, (UINT2)u4Flag);
		}
		break;
	case SHOW_NTPS_SERVER_STATUS:
		i4RetStatus = NtpShowServersStatus(CliHandle);
		break;

	case SHOW_NTPS_SERVER_DETAIL:
		i4RetStatus = ShowNtpsDetail(CliHandle);
		break;
	case SHOW_NTPS_SERVER_STATISTICS:
		i4RetStatus = NtpShowServersStatistics(CliHandle);
		break;
	case SHOW_NTPS_SYNCHRONIZATION:
		i4RetStatus = NtpShowSync(CliHandle);
		break;
	case SHOW_NTPS_SERVER_ASSOCIATIONS:
		i4RetStatus = NtpShowAssociation(CliHandle);
		break;
	case SHOW_NTPS_CLIENTS:
		i4RetStatus = NtpShowclients(CliHandle);
		break;

	case CLI_NTPS_ADD_UCAST_SERVER:

		if (IPVX_ADDR_FMLY_IPV4 == CLI_PTR_TO_U4(args[1]))
		{
			MEMCPY(au1UniServAddr, (UINT1 *) args[0], IPVX_IPV4_ADDR_LEN);
			NTP_INET_NTOHL(au1UniServAddr);
			fsNtpUnicastServerAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
		}
#ifdef IP6_WANTED
		else if (IPVX_ADDR_FMLY_IPV6 == CLI_PTR_TO_U4(args[1]))
		{
			MEMSET(&Ip6Addr, 0, sizeof(tIp6Addr));
			if (INET_ATON6((UINT1 *) args[0], &Ip6Addr) == 0)
			{
				CLI_SET_ERR(CLI_IPV6_INVALID_ADDRESS);
				break;
			}
			MEMCPY(au1UniServAddr, &Ip6Addr, IPVX_IPV6_ADDR_LEN);
			fsNtpUnicastServerAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
			if (IS_ADDR_MULTI(Ip6Addr) || IS_ADDR_UNSPECIFIED(Ip6Addr)
				|| (IS_ADDR_LLOCAL(Ip6Addr)))
			{
				CLI_SET_ERR(CLI_IPV6_INVALID_ADDRESS);
				break;
			}
			MEMCPY(au1UniServAddr, &Ip6Addr, IPVX_IPV6_ADDR_LEN);

		}
#endif /* IP6_WANTED */
		MEMCPY(fsNtpUnicastServerAddr.pu1_OctetList, au1UniServAddr,
			   fsNtpUnicastServerAddr.i4_Length);
		NtpsServer.u4NtpsServerAddrType = CLI_PTR_TO_U4(args[1]);
		NtpsServer.NtpsServerAddr.u1AddrLen = (UINT1)fsNtpUnicastServerAddr.i4_Length;
		MEMCPY(NtpsServer.NtpsServerAddr.au1Addr,
			   fsNtpUnicastServerAddr.pu1_OctetList,
			   fsNtpUnicastServerAddr.i4_Length);
		pNtpsServerDB =
			RBTreeGet(gNtpsServerParams.NtpsServerList,
					  (tRBElem *) & NtpsServer);
		if (pNtpsServerDB == NULL)
		{
			if (nmhTestv2FsNtpsServerRowStatus
				(&u4Err, CLI_PTR_TO_I4(args[1]), &fsNtpUnicastServerAddr,
				 NTP_CREATE_AND_GO) == SNMP_SUCCESS)
			{
				if (nmhSetFsNtpsServerRowStatus
					(CLI_PTR_TO_I4(args[1]), &fsNtpUnicastServerAddr,
					 NTP_CREATE_AND_GO) != SNMP_SUCCESS)
				{
					NTP_TRC(NTP_MGMT_TRC,
							"nmhSetFsNtpsServerRowStatus with row status NTP_CREATE_AND_GO Failed\n");
					i4RetStatus = CLI_FAILURE;
					break;
				}
			}
			else
			{
				i4RetStatus = CLI_FAILURE;
				break;
			}
		}
		else
		{
			NTP_TRC(NTP_MGMT_TRC,
					"CLI NTP Server Configuration - Server Already exists, Update other parameters \n");
		}
		if (CLI_PTR_TO_I4(args[2]) > 0)
		{
			if (nmhTestv2FsNtpsServerVersion
				(&u4Err, CLI_PTR_TO_I4(args[1]), &fsNtpUnicastServerAddr,
				 CLI_PTR_TO_I4(args[2])) == SNMP_SUCCESS)
			{
				if (nmhSetFsNtpsServerVersion
					(CLI_PTR_TO_I4(args[1]), &fsNtpUnicastServerAddr,
					 CLI_PTR_TO_I4(args[2])) == SNMP_SUCCESS)
				{
					NTP_TRC(NTP_MGMT_TRC,
							"nmhSetFsNtpsServerVersion processed successfully \n");
				}
			}
			else
			{
				NTP_TRC(NTP_MGMT_TRC,
						"nmhTestv2FsNtpsServerVersion Failed \n");
			}
		}
		if (CLI_PTR_TO_I4(args[3]) > 0)
		{
			if (nmhTestv2FsNtpsServerBurstMode
				(&u4Err, CLI_PTR_TO_I4(args[1]), &fsNtpUnicastServerAddr,
				 CLI_PTR_TO_I4(args[3])) == SNMP_SUCCESS)
			{
				if (nmhSetFsNtpsServerBurstMode
					(CLI_PTR_TO_I4(args[1]), &fsNtpUnicastServerAddr,
					 CLI_PTR_TO_I4(args[3])) == SNMP_SUCCESS)
				{
					NTP_TRC(NTP_MGMT_TRC,
							"nmhSetFsNtpsServerBurstMode processed successfully \n");
				}
			}
			else
			{
				NTP_TRC(NTP_MGMT_TRC,
						"nmhTestv2FsNtpsServerBurstMode Failed \n");
			}
		}

		if (CLI_PTR_TO_I4(args[4]) > 0)
		{
			if (nmhTestv2FsNtpsServerKeyId(&u4Err, CLI_PTR_TO_I4(args[1]),
										   &fsNtpUnicastServerAddr,
										   CLI_PTR_TO_I4(args[4])) ==
				SNMP_SUCCESS)
			{
				if (nmhSetFsNtpsServerKeyId(CLI_PTR_TO_I4(args[1]),
											&fsNtpUnicastServerAddr,
											CLI_PTR_TO_I4(args[4])) ==
					SNMP_SUCCESS)
				{
					NTP_TRC(NTP_MGMT_TRC,
							"nmhSetFsNtpsServerKeyId processed successfully \n");
				}
			}
			else
			{
				NTP_TRC(NTP_MGMT_TRC, "nmhTestv2FsNtpsServerKeyId Failed \n");
			}
		}
		if (CLI_PTR_TO_U4(args[5]) > 0)
		{
			if (nmhTestv2FsNtpsServerMaxPollInt
				(&u4Err, CLI_PTR_TO_I4(args[1]), &fsNtpUnicastServerAddr,
				 CLI_PTR_TO_U4(args[5])) == SNMP_SUCCESS)
			{
				if (nmhSetFsNtpsServerMaxPollInt
					(CLI_PTR_TO_I4(args[1]), &fsNtpUnicastServerAddr,
					 CLI_PTR_TO_U4(args[5])) == SNMP_SUCCESS)
				{
					NTP_TRC(NTP_MGMT_TRC,
							"nmhSetFsNtpsServerMaxPollInt processed successfully \n");
				}
			}
			else
			{
				NTP_TRC(NTP_MGMT_TRC,
						"nmhTestv2FsNtpsServerMaxPollInt Failed \n");
			}
		}
		if (CLI_PTR_TO_I4(args[6]) > 0)
		{
			if (nmhTestv2FsNtpsServerMinPollInt
				(&u4Err, CLI_PTR_TO_I4(args[1]), &fsNtpUnicastServerAddr,
				 CLI_PTR_TO_U4(args[6])) == SNMP_SUCCESS)
			{
				if (nmhSetFsNtpsServerMinPollInt
					(CLI_PTR_TO_I4(args[1]), &fsNtpUnicastServerAddr,
					 CLI_PTR_TO_U4(args[6])) == SNMP_SUCCESS)
				{
					NTP_TRC(NTP_MGMT_TRC,
							"nmhSetFsNtpsServerMinPollInt processed successfully \n");
				}
			}
			else
			{
				NTP_TRC(NTP_MGMT_TRC,
						"nmhTestv2FsNtpsServerMinPollInt Failed \n");
			}

		}
		if (CLI_PTR_TO_I4(args[7]) > 0)
		{
			if (nmhTestv2FsNtpsServerPrefer(&u4Err,
											CLI_PTR_TO_I4(args[1]),
											&fsNtpUnicastServerAddr,
											CLI_PTR_TO_I4(args[7])) ==
				SNMP_SUCCESS)
			{

				if (nmhSetFsNtpsServerPrefer(CLI_PTR_TO_I4(args[1]),
											 &fsNtpUnicastServerAddr,
											 CLI_PTR_TO_I4(args[7])) ==
					SNMP_SUCCESS)
				{
					NTP_TRC(NTP_MGMT_TRC,
							"nmhSetFsNtpsServerPrefer processed successfully \n");
				}
			}
			else
			{
				NTP_TRC(NTP_MGMT_TRC, "nmhTestv2FsNtpsServerPrefer Failed \n");
			}
		}
		break;
			 /**2*/
	case CLI_NTPS_DEL_UCAST_SERVER:
		if (IPVX_ADDR_FMLY_IPV4 == CLI_PTR_TO_U4(args[1]))
		{
			MEMCPY(au1UniServAddr, (UINT1 *) args[0], IPVX_IPV4_ADDR_LEN);
			NTP_INET_NTOHL(au1UniServAddr);
			fsNtpUnicastServerAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
		}
#ifdef IP6_WANTED
		else if (IPVX_ADDR_FMLY_IPV6 == CLI_PTR_TO_U4(args[1]))
		{
			MEMSET(&Ip6Addr, 0, sizeof(tIp6Addr));
			if (INET_ATON6((UINT1 *) args[0], &Ip6Addr) == 0)
			{
				CLI_SET_ERR(CLI_IPV6_INVALID_ADDRESS);
				break;
			}
			MEMCPY(au1UniServAddr, &Ip6Addr, IPVX_IPV6_ADDR_LEN);
			fsNtpUnicastServerAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
			if (IS_ADDR_MULTI(Ip6Addr) || IS_ADDR_UNSPECIFIED(Ip6Addr)
				|| (IS_ADDR_LLOCAL(Ip6Addr)))
			{
				CLI_SET_ERR(CLI_IPV6_INVALID_ADDRESS);
				break;
			}
			MEMCPY(au1UniServAddr, &Ip6Addr, IPVX_IPV6_ADDR_LEN);

		}
#endif /* IP6_WANTED */
		MEMCPY(fsNtpUnicastServerAddr.pu1_OctetList, au1UniServAddr,
			   fsNtpUnicastServerAddr.i4_Length);
		NtpUcastServer.NtpsServerAddr.u1AddrLen =
			(UINT1)fsNtpUnicastServerAddr.i4_Length;
		MEMCPY(NtpUcastServer.NtpsServerAddr.au1Addr,
			   fsNtpUnicastServerAddr.pu1_OctetList,
			   fsNtpUnicastServerAddr.i4_Length);
		NtpUcastServer.u4NtpsServerAddrType = CLI_PTR_TO_U4(args[1]);

		u1RetVal = NtpsServerDB(NTPS_GET_SERVER_TABLE_ENTRY, &NtpUcastServer);
		if (u1RetVal == NTP_SUCCESS)
		{
			u1RetVal = NtpsServerDB(NTPS_DESTROY_SERVER_TABLE_ENTRY,
									&NtpUcastServer);
		}
		else
		{
			CLI_SET_ERR(CLI_NTP_SERVER_NOT_CONFIGURED);
		}

		break;
	case CLI_NTPS_DEBUG:
		/* args[0] contains Trace flag for the Modules */
		i4RetStatus = NtpsCliSetDebug(CliHandle, CLI_PTR_TO_I4(args[0]));
		break;
	case CLI_NTPS_SERVER_CREATE_SOURCE:
		i4RetStatus = NtpsCliCreateSource(CLI_PTR_TO_I4(args[0]));
		break;
	case CLI_NTPS_SERVER_DELETE_SOURCE:
		i4RetStatus = NtpsCliDeleteSource();
		break;
	default:
		break;
	}

	if ((i4RetStatus == CLI_FAILURE)
		&& (CLI_GET_ERR(&u4ErrorCode) == CLI_SUCCESS))
	{
		if ((u4ErrorCode > 0) && (u4ErrorCode < CLI_NTP_ERR_MAX_CLI_CMD))
		{
			CliPrintf(CliHandle, "\r%s", gau1NtpCliErrString[u4ErrorCode]);
		}
		CLI_SET_ERR(0);
	}
	CLI_SET_CMD_STATUS((UINT4)i4RetStatus);

	NtpsUnLock();
	CliUnRegisterLock(CliHandle);
	return i4RetStatus;
}

/*************************************************************************/
/* Function Name : NtpsCliCreateSource */
/* Description : This procedure will set i4Index to */
/* nmhsetFsNtpsSourceInterface */
/* Input(s) : i4Index - NTP Feature's Interface index */
/* Output(s) : None.  */
/* Returns : CLI_SUCCESS /CLI_FAILURE */
/*************************************************************************/
INT4 NtpsCliCreateSource(INT4 i4Index)
{
	UINT4 u4Err = 0;
	INT1 i1RetVal = 0;
	i1RetVal = nmhTestv2FsNtpsSourceInterface(&u4Err, i4Index);
	if (i1RetVal == SNMP_SUCCESS)
	{
		nmhSetFsNtpsSourceInterface(i4Index);
		NTP_TRC(NTP_MGMT_TRC, "NtpsCliCreateSource SET - Success\n");
		return CLI_SUCCESS;
	}
	NTP_TRC(NTP_MGMT_TRC, "NtpsCliCreateSource SET - FAILURE\n");
	return CLI_FAILURE;
}

/*************************************************************************/
/* Function Name : NtpsCliDeleteSource */
/* Description : This procedure will delete from */
/* nmhsetFsNtpsSourceInterface */
/* Input(s) : None.  */
/* Output(s) : None.  */
/* Returns : CLI_SUCCESS /CLI_FAILURE */
/*************************************************************************/
INT4 NtpsCliDeleteSource()
{
	INT4 i4Index = 0;
	if (nmhSetFsNtpsSourceInterface(i4Index) == SNMP_SUCCESS)
	{
		NTP_TRC(NTP_MGMT_TRC, "NtpsCliDeleteSource SET - SUCCESS\n");
	}
	else
	{
		NTP_TRC(NTP_MGMT_TRC, "NtpsCliDeleteSource SET - FAILURE\n");
	}
	return CLI_SUCCESS;
}

/*************************************************************************/
/* Function Name : NtpsSetIntStatus */
/* Description : This procedure will set Interface index in */
/* nmhSetFsNtpsInterfaceNtpStatus */
/* Input(s) : Interface Index, Interface Status */
/* Output(s) : None.  */
/* Returns : CLI_SUCCESS /CLI_FAILURE */
/*************************************************************************/
INT4 NtpsSetIntStatus(UINT4 u4IfIndex, INT4 i4FsNtpsInterfaceNtpStatus)
{
	UINT4 u4ErrCode = 0;

	if ((TMO_SLL_Count(&(gNtpsIntfParams.NtpsIntfList)) + 1) ==
		MAX_NTPS_INTERFACES)
	{
		CLI_SET_ERR(CLI_NTP_MAX_INTERFACE);
		NTP_TRC(NTP_MGMT_TRC,
				"NtpsSetIntStatus Max. interfaces configured. SET - Failure\n");
		return CLI_FAILURE;
	}

	if (nmhTestv2FsNtpsInterfaceNtpStatus
		(&u4ErrCode, (INT4) u4IfIndex,
		 i4FsNtpsInterfaceNtpStatus) == SNMP_SUCCESS)
	{
		if (nmhSetFsNtpsInterfaceNtpStatus
			(u4IfIndex, i4FsNtpsInterfaceNtpStatus) == SNMP_FAILURE)
		{
			NTP_TRC(NTP_MGMT_TRC, "NtpsSetIntStatus SET - Failure\n");
			return CLI_FAILURE;
		}
	}
	else
	{
		NTP_TRC(NTP_MGMT_TRC, "NtpsSetIntStatus TEST - Failure\n");
		return CLI_FAILURE;
	}
	NTP_TRC(NTP_MGMT_TRC, "NtpsSetIntStatus SET - SUCCESS\n");
	return CLI_SUCCESS;
}

/*************************************************************************/
/* Function Name : NtpsSetAdminStatus */
/* Description : This procedure will set status to global flag to */
/* know the feature is enabled or disabled */
/* Input(s) : u4SetValNtpsAdminStatus - NTP Feature's Status */
/* u4Status - NTP Server's Status */
/* Output(s) : None.  */
/* Returns : CLI_SUCCESS /CLI_FAILURE */
/*************************************************************************/

INT4 NtpsSetAdminStatus(INT4 i4SetValNtpsAdminStatus)
{
	UINT4 u4ErrCode = 0;
	if (nmhTestv2FsNtpsGlobalStatus(&u4ErrCode, i4SetValNtpsAdminStatus) ==
		SNMP_SUCCESS)
	{
		if (nmhSetFsNtpsGlobalStatus(i4SetValNtpsAdminStatus) == SNMP_SUCCESS)
		{
			NTP_TRC(NTP_MGMT_TRC, "nmhSetFsNtpsGlobalStatus is Success\n");
		}
		else
		{
			NTP_TRC(NTP_MGMT_TRC, "NtpsSetAdminStatus SET - Failure\n");
			return CLI_FAILURE;
		}
	}
	else
	{
		NTP_TRC(NTP_MGMT_TRC, "NtpsSetAdminStatus TEST - Failure\n");
		return CLI_FAILURE;
	}
	NTP_TRC(NTP_MGMT_TRC, "NtpsSetAdminStatus SET- SUCCESS\n");
	return CLI_SUCCESS;
}

/*************************************************************************/
/* Function Name : NtpsGetAdminStatus */
/* Description : This procedure will set status to global flag to */
/* know the feature is enabled or disabled */
/* Input(s) : pu4GetValNtpsAdminStatus - NTP Feature's Status */
/* u4Status - NTP Server's Status */
/* Output(s) : None.  */
/* Returns : CLI_SUCCESS /CLI_FAILURE */
/*************************************************************************/

INT4 NtpsGetAdminStatus(UINT4 * pu4GetValNtpsAdminStatus)
{
	*pu4GetValNtpsAdminStatus = gu4NtpEnableFlag;
	NTP_TRC(NTP_MGMT_TRC, "NtpsSetAdminStatus SET- SUCCESS\n");
	return CLI_SUCCESS;
}

/************************************************************************
 *  Function Name   : NtpsCliSetAuth
 *  Description     : This function will configure the ntp authetication 
 *                     enable/disable
 *  Input           : enable/disable
 *  Output          : None
 *  Returns         : CLI_FAILURE or CLI_SUCCESS
 ***********************************************************************/
INT4 NtpsCliSetAuth(INT4 i4Status)
{
	UINT4 u4ErrorCode = NTP_ZERO;
	if (nmhTestv2FsNtpsAuthStatus(&u4ErrorCode, i4Status) == SNMP_FAILURE)
	{
		NTP_TRC(NTP_MGMT_TRC, "NtpsCliSetAuth TEST- FAILURE\n");
		return CLI_FAILURE;
	}

	if (nmhSetFsNtpsAuthStatus(i4Status) == SNMP_FAILURE)
	{
		NTP_TRC(NTP_MGMT_TRC, "NtpsCliSetAuth SET- FAILURE\n");
		return CLI_FAILURE;
	}
	NTP_TRC(NTP_MGMT_TRC, "NtpsCliSetAuth SET- SUCCESS\n");
	return CLI_SUCCESS;

}

/************************************************************************
 *  Function Name   : NtpsCliSetAuthKey
 *  Description     : This function will configure the ntp authetication 
 *                    Key 
 *  Input           : Key id, Key, Authentication type 
 *  Output          : None
 *  Returns         : CLI_FAILURE or CLI_SUCCESS
 ***********************************************************************/
INT4
NtpsCliSetAuthKey(tCliHandle CliHandle, INT4 i4KeyId,
				  UINT1 * pu1Key, UINT4 u4AuthType)
{
	UINT4 u4err = 0;
	tSNMP_OCTET_STRING_TYPE fsNtpKeyValue;
	UINT1 au1KeyString[NTP_KEY_MAX_LENGTH];

	fsNtpKeyValue.i4_Length = (INT4)STRLEN(pu1Key);
	fsNtpKeyValue.pu1_OctetList = au1KeyString;

	MEMSET(au1KeyString, 0, NTP_KEY_MAX_LENGTH);
	MEMCPY(fsNtpKeyValue.pu1_OctetList, pu1Key, fsNtpKeyValue.i4_Length);

	if ((TMO_SLL_Count(&(gNtpsAuthParams.NtpsAuthList)) + 1) ==
		SYS_MEM_NTP_AUTH_BLOCK_NUM)
	{
		CLI_SET_ERR(CLI_NTP_AUTH_MAX_KEY);
		NTP_TRC(NTP_MGMT_TRC,
				"NtpsCliSetAuthKey SET- FAILURE, Max. Keys configured\n");
		return CLI_FAILURE;
	}

	if (nmhTestv2FsNtpsAuthRowStatus
		(&u4err, i4KeyId, (INT4) NTP_CREATE_AND_WAIT) == SNMP_SUCCESS)
	{
		if (nmhSetFsNtpsAuthRowStatus(i4KeyId, (INT4) NTP_CREATE_AND_WAIT)
			== SNMP_FAILURE)
		{
			NTP_TRC(NTP_MGMT_TRC,
					"NtpsCliSetAuthKey SET Auth Status - FAILURE \n");
			return CLI_FAILURE;
		}
	}
	else
	{
		return CLI_FAILURE;
	}

	if (nmhTestv2FsNtpsAuthAlgorithm(&u4err, i4KeyId, (INT4) u4AuthType) ==
		SNMP_SUCCESS)
	{
		if (nmhSetFsNtpsAuthAlgorithm(i4KeyId, (INT4) u4AuthType) ==
			SNMP_FAILURE)
		{
			NTP_TRC(NTP_MGMT_TRC,
					"NtpsCliSetAuthKey SET Auth Algorithm - FAILURE \n");
			return CLI_FAILURE;
		}
	}
	else
	{
		return CLI_FAILURE;
	}

	if (nmhTestv2FsNtpsAuthKey(&u4err, i4KeyId, &fsNtpKeyValue) ==
		SNMP_SUCCESS)
	{
		if (nmhSetFsNtpsAuthKey(i4KeyId, &fsNtpKeyValue) == SNMP_FAILURE)
		{
			NTP_TRC(NTP_MGMT_TRC,
					"NtpsCliSetAuthKey SET Auth Key id - FAILURE \n");
			return CLI_FAILURE;
		}
	}
	else
	{
		if (u4err == SNMP_ERR_WRONG_LENGTH)
		{
			NTP_TRC(NTP_MGMT_TRC,
					"NtpsCliSetAuthKey TEST Auth Key id - FAILURE \n");
			CliPrintf(CliHandle, "\r%% Invalid Key string\n");
		}
		return CLI_FAILURE;
	}
	if (nmhTestv2FsNtpsAuthRowStatus(&u4err, i4KeyId, (INT4) NTP_ACTIVE) ==
		SNMP_SUCCESS)
	{
		if (nmhSetFsNtpsAuthRowStatus(i4KeyId, (INT4) NTP_ACTIVE) ==
			SNMP_FAILURE)
		{
			NTP_TRC(NTP_MGMT_TRC,
					"NtpsCliSetAuthKey SET Auth Key status - FAILURE \n");
			return CLI_FAILURE;
		}
	}

	NTP_TRC(NTP_MGMT_TRC,
			"NtpsCliSetAuthKey SET Auth Key status - SUCCESS \n");
	return CLI_SUCCESS;
}

/***********************************************************************
 *  Function Name   : NtpsCliRemoveKey
 *  Description     : This function will configure the ntp authentication
 *  Input           : CliHandle, key , string
 *  Output          : None
 *  Returns         : CLI_FAILURE or CLI_SUCCESS
 ***********************************************************************/

INT4 NtpsCliRemoveKey(tCliHandle CliHandle, INT4 i4KeyId)
{
	UINT4 u4err = 0;

	UNUSED_PARAM(CliHandle);

	if (nmhTestv2FsNtpsAuthRowStatus(&u4err, i4KeyId, (INT4) NTP_DESTROY)
		== SNMP_SUCCESS)
	{
		if (nmhSetFsNtpsAuthRowStatus(i4KeyId, (INT4) NTP_DESTROY) ==
			SNMP_FAILURE)
		{
			NTP_TRC(NTP_MGMT_TRC,
					"NtpsCliRemoveKey RESET Auth Key status - FAILURE \n");
			return CLI_FAILURE;
		}
	}
	else
	{
		NTP_TRC(NTP_MGMT_TRC,
				"NtpsCliRemoveKey RESET Auth Key status TEST - FAILURE \n");
		return CLI_FAILURE;
	}
	NTP_TRC(NTP_MGMT_TRC,
			"NtpsCliRemoveKey RESET Auth Key status - SUCCESS \n");
	return CLI_SUCCESS;
}

/***********************************************************************
 *  Function Name   : NtpsCliConfigureTrustedkey
 *  Description     : This function will configure the trusted key
 *  Input           : CliHandle,key
 *  Output          : None
 *  Returns         : CLI_FAILURE or CLI_SUCCESS
 ***********************************************************************/

INT4 NtpsCliConfigureTrustedkey(tCliHandle CliHandle, UINT2 * pu2KeyList,
								INT4 i4Trust, UINT4 u4KeyCount)
{
	UINT4 u4err = 0;
	tNtpsAuth *pNtpsNode = NULL;
	UINT1 u1KeyIndex = 0;
	UNUSED_PARAM(CliHandle);
	for (u1KeyIndex = 0; u1KeyIndex < u4KeyCount; u1KeyIndex++)
	{
		/* Get the Authentication Node of particular key */
		pNtpsNode = NtpGetAuthKeyNode(pu2KeyList[u1KeyIndex]);
		if (pNtpsNode == NULL)
		{
			CliPrintf(CliHandle,
					  "\r%d Authentication Key not configured for Key index\n",
					  pu2KeyList[u1KeyIndex]);
			NTP_TRC(NTP_MGMT_TRC,
					"NtpsCliConfigureTrustedKey Fetch Auth Info - FAILURE \n");
			return CLI_FAILURE;
		}

		/* If Key matches , set the Trusted key Flag in the Auth Node */
		if (i4Trust == NTP_KEY_TRUSTED)
		{
			if (nmhTestv2FsNtpsAuthTrustedKey
				((&u4err), pu2KeyList[u1KeyIndex],
				 NTP_KEY_TRUSTED) == SNMP_SUCCESS)
			{
				if (nmhSetFsNtpsAuthTrustedKey
					(pu2KeyList[u1KeyIndex], NTP_KEY_TRUSTED) == SNMP_FAILURE)
				{
					NTP_TRC(NTP_MGMT_TRC,
							"NtpsCliConfigureTrustedKey SET Trusted Key - FAILURE \n");
					return CLI_FAILURE;

				}
			}
		}
		else if (i4Trust == NTP_NO_KEY_TRUSTED)
		{
			if (nmhTestv2FsNtpsAuthTrustedKey
				((&u4err), pu2KeyList[u1KeyIndex],
				 NTP_NO_KEY_TRUSTED) == SNMP_SUCCESS)
			{
				if (nmhSetFsNtpsAuthTrustedKey
					(pu2KeyList[u1KeyIndex],
					 NTP_NO_KEY_TRUSTED) == SNMP_FAILURE)
				{
					NTP_TRC(NTP_MGMT_TRC,
							"NtpsCliConfigureTrustedKey SET Trusted Key - FAILURE \n");
					return CLI_FAILURE;

				}
			}
		}
	}
	NTP_TRC(NTP_MGMT_TRC,
			"NtpsCliConfigureTrustedKey SET Trusted Key - SUCCESS \n");
	return CLI_SUCCESS;
}



/**********************************************************************
*    Function Name      : NtpsSetRestrict           
*    Description        : This functions adds an entry to SLL 
*                         of network address and mask  info using add operation
*    Input(s)           : NetworkAddress and Mask
*
*    Output(s)          :     
*    Returns            : CLI_SUCCESS/CLI_FAILURE
*************************************************************************/
INT4
NtpsSetRestrict(tIPvXAddr * IpAddr, UINT2 u2Mask, UINT1 u1DefaultRestrict,
				UINT2 u2Flag)
{

	tNtpsAccess NtpAccessNode;
	UINT1 u1RetVal = 0;
	UINT4 u4ErrorCode = 0;
	tSNMP_OCTET_STRING_TYPE fsNtpsAccessAddr;
	UINT1 au1AccessSetAddr[IPVX_IPV6_ADDR_LEN];

	MEMSET(&fsNtpsAccessAddr, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
	UNUSED_PARAM(u1DefaultRestrict);
	MEMSET(&NtpAccessNode, 0, sizeof(tNtpsAccess));
	NtpAccessNode.NtpsAccessIp.u1AddrLen = IpAddr->u1AddrLen;
	MEMCPY(NtpAccessNode.NtpsAccessIp.au1Addr, IpAddr->au1Addr,
		   IpAddr->u1AddrLen);
	NtpAccessNode.NtpsAccessIp.u1Afi = IpAddr->u1Afi;
	NtpAccessNode.u2NtpsAccessIpMask = u2Mask;
	NtpAccessNode.u2NtpsAccessFlag = u2Flag;
	u1RetVal = NtpsAccessDB(NTPS_GET_ACCESS_TABLE_ENTRY, &NtpAccessNode);
	MEMSET(au1AccessSetAddr, 0, IPVX_IPV6_ADDR_LEN);
	fsNtpsAccessAddr.pu1_OctetList = au1AccessSetAddr;
	fsNtpsAccessAddr.i4_Length = IpAddr->u1AddrLen;
	MEMCPY(fsNtpsAccessAddr.pu1_OctetList, IpAddr->au1Addr, IpAddr->u1AddrLen);
	if (u1RetVal == NTP_FAILURE)
	{
        if (u1DefaultRestrict == CLI_DISABLE) 
        {
		if (nmhTestv2FsNtpsAccessRowStatus
			(&u4ErrorCode, NtpAccessNode.NtpsAccessIp.u1Afi,
			 &fsNtpsAccessAddr, (INT4) NtpAccessNode.u2NtpsAccessIpMask,
			 CREATE_AND_GO) == SNMP_SUCCESS)
		{
			if (nmhSetFsNtpsAccessRowStatus
				(NtpAccessNode.NtpsAccessIp.u1Afi, &fsNtpsAccessAddr,
				 (INT4) NtpAccessNode.u2NtpsAccessIpMask,
				 CREATE_AND_GO) == SNMP_FAILURE)
			{
				NTP_TRC(NTP_MGMT_TRC,
						"nmhSetFsNtpsAccessRowStatus with row status NTP_CREATE_AND_GO Failed\n");
				return CLI_FAILURE;
			}
		}
		else
		{
			NTP_TRC(NTP_MGMT_TRC, "nmhSetFsNtpsAccessRowStatus TEST Failed\n");
			return CLI_FAILURE;
		}
	}
      else 
       {
           if (nmhSetFsNtpsAccessRowStatus
                   (NtpAccessNode.NtpsAccessIp.u1Afi, &fsNtpsAccessAddr,
                    (INT4) NtpAccessNode.u2NtpsAccessIpMask,
                    CREATE_AND_GO) == SNMP_FAILURE)
           {
               NTP_TRC(NTP_MGMT_TRC,
                       "nmhSetFsNtpsAccessRowStatus with row status NTP_CREATE_AND_GO Failed\n");
               return CLI_FAILURE;
           }
       }

    }
	if (nmhTestv2FsNtpsAccessFlag
		(&u4ErrorCode, NtpAccessNode.NtpsAccessIp.u1Afi, &fsNtpsAccessAddr,
		 (INT4) NtpAccessNode.u2NtpsAccessIpMask,
		 (INT4) u2Flag) == SNMP_SUCCESS)
	{
		u2Flag = u2Flag | NtpAccessNode.u2NtpsAccessFlag;
		if (nmhSetFsNtpsAccessFlag(NtpAccessNode.NtpsAccessIp.u1Afi,
								   &fsNtpsAccessAddr,
								   (INT4) NtpAccessNode.u2NtpsAccessIpMask,
								   (INT4) u2Flag) == SNMP_FAILURE)
		{
			NTP_TRC(NTP_MGMT_TRC,
					"nmhSetFsNtpsAccessFlag processing failred \n");
			return CLI_FAILURE;
		}
	}
	else
	{
		NTP_TRC(NTP_MGMT_TRC, "nmhSetFsNtpsAccessRowStatus TEST Failed\n");
		return CLI_FAILURE;
	}
	return CLI_SUCCESS;

}

/**********************************************************************
*    Function Name      : NtpsSetNoRestrict           
*    Description        : This functions adds an entry to SLL 
*                         of network address and mask  info using add operation
*    Input(s)           : NetworkAddress and Mask
*
*    Output(s)          :     
*    Returns            : CLI_SUCCESS/CLI_FAILURE
*************************************************************************/

INT4
NtpsNoSetRestrict(tIPvXAddr * IpAddr, UINT2 u2Mask,
				  UINT1 u1DefaultRestrict, UINT2 u2Flag)
{
	tNtpsAccess NtpAccessNode;
	UINT1 u1RetVal = 0;
	tSNMP_OCTET_STRING_TYPE fsNtpsAccessAddr;
	UINT1 au1AccessNoSetAddr[IPVX_IPV6_ADDR_LEN];

	MEMSET(&fsNtpsAccessAddr, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
	UNUSED_PARAM(u1DefaultRestrict);
	MEMSET(&NtpAccessNode, 0, sizeof(tNtpsAccess));
	NtpAccessNode.NtpsAccessIp.u1AddrLen = IpAddr->u1AddrLen;
	MEMCPY(NtpAccessNode.NtpsAccessIp.au1Addr, IpAddr->au1Addr,
		   IpAddr->u1AddrLen);
	NtpAccessNode.NtpsAccessIp.u1Afi = IpAddr->u1Afi;
	NtpAccessNode.u2NtpsAccessIpMask = u2Mask;
	NtpAccessNode.u2NtpsAccessFlag = u2Flag;
	u1RetVal = NtpsAccessDB(NTPS_GET_ACCESS_TABLE_ENTRY, &NtpAccessNode);
	MEMSET(au1AccessNoSetAddr, 0, IPVX_IPV4_ADDR_LEN);
	fsNtpsAccessAddr.pu1_OctetList = au1AccessNoSetAddr;
	fsNtpsAccessAddr.i4_Length = IpAddr->u1AddrLen;
	MEMCPY(fsNtpsAccessAddr.pu1_OctetList, IpAddr->au1Addr, IpAddr->u1AddrLen);
	if (u2Flag == 0)
	{
		if (nmhSetFsNtpsAccessRowStatus(NtpAccessNode.NtpsAccessIp.u1Afi,
										&fsNtpsAccessAddr,
										(INT4)
										NtpAccessNode.u2NtpsAccessIpMask,
										DESTROY) == SNMP_FAILURE)
		{
			NTP_TRC(NTP_MGMT_TRC,
					"nmhSetFsNtpsAccessRowStatus with row status NTP_CREATE_AND_GO Failed\n");
			return CLI_FAILURE;
		}

	}
	else
	{
		u2Flag = u2Flag ^ NtpAccessNode.u2NtpsAccessFlag;
		if (u1RetVal == NTP_SUCCESS)
		{
			if (nmhSetFsNtpsAccessFlag(NtpAccessNode.NtpsAccessIp.u1Afi,
									   &fsNtpsAccessAddr,
									   (INT4) NtpAccessNode.u2NtpsAccessIpMask,
									   (INT4) u2Flag) == SNMP_FAILURE)
			{
				NTP_TRC(NTP_MGMT_TRC,
						"nmhSetFsNtpsAccessFlag processing failred \n");
				return CLI_FAILURE;
			}
		}
	}
	return CLI_SUCCESS;

}

/***************************************************************************/
/* Function Name : NtpsCliSetDebug */
/* Description : Confiures NTP trace flag */
/* INPUT : u4Trace - Debug flags */
/* OUTPUT : CliHandle - Contains error messages */
/* RETURNS : CLI_SUCCESS/CLI_FAILURE */
/***************************************************************************/
INT4 NtpsCliSetDebug(tCliHandle CliHandle, INT4 i4TraceStatus)
{
	UINT4 u4ErrorCode = 0;
	UNUSED_PARAM(CliHandle);
	if (nmhTestv2FsNtpsDebug(&u4ErrorCode, i4TraceStatus) == SNMP_FAILURE)
	{
		return CLI_FAILURE;
	}

	if (nmhSetFsNtpsDebug(i4TraceStatus) == SNMP_FAILURE)
	{
		CLI_FATAL_ERROR(CliHandle);
		return CLI_FAILURE;
	}

	return CLI_SUCCESS;
}


/****************************************************************************
 * Function         : IssNtpsShowRunningConfig                              *
 * Description      : This function used in show running config of ntp mod  *
 * Input            : CliHandle   - Handle to the CLI                       *
 * Output           : None.                                                 *
 * Returns          : CLI_SUCCESS / CLI_FAILURE                             *
 ***************************************************************************/

INT4 IssNtpsShowRunningConfig(tCliHandle CliHandle)
{
	INT4 i1RetVal = SNMP_FAILURE;
	INT4 i4RetVal = 0;
	tNtpsAuth *pNtpAuth = NULL;
	INT4 i4NtpsServerVersion = 0;
	INT4 i4NtpsServerKeyId = 0;
	INT4 i4NtpsServerBurstMode = 0;
	UINT4 u4NtpsServerMinPollInt = 0;
	UINT4 u4NtpsServerMaxPollInt = 0;
	UINT4 u4NtpsAdminStatus = NTP_ENABLE;
	UINT1 u1IfaceType = 0;
	INT4 i4NtpsAuthStatus = NTP_AUTH_ENABLE;
	tSNMP_OCTET_STRING_TYPE GetValFsNtpUnicastServerAddr;
	tSNMP_OCTET_STRING_TYPE GetValFsNtpUnicastNextServerAddr;
	INT4 i4FsNtpUnicastServerAddrType = IPVX_ADDR_FMLY_IPV4;
	INT4 i4FsNextNtpUnicastServerAddrType = IPVX_ADDR_FMLY_IPV4;
	INT4 i4NtpsSourceInterface = 0;
	UINT4 u4GetStat = 0;
	INT1 *pi1IfName = NULL;
	UINT2 u2VlanId = 0;

	UINT1 au1AuthKey[NTP_KEY_MAX_LENGTH + 1];
	UINT1 au1IfName[CFA_MAX_IFALIAS_LENGTH];
#ifdef IP6_WANTED
	UINT1 au1Ip6Addr[IPVX_IPV6_ADDR_LEN];
#endif
	UINT1 au1AccessNullAddr[IPVX_IPV6_ADDR_LEN];
	UINT1 au1AccessAddr[IPVX_IPV6_ADDR_LEN];

	INT4 i4FsNtpsAuthKeyId = 0;
	INT4 i4NtpsServerPrefer = 0;
	CHR1 *pu1String = NULL;
	UINT4 u4IpAddr = 0;
	INT4 i4FsNtpsAccessAddrType = 0;
	tSNMP_OCTET_STRING_TYPE NtpsAccessIpAddr;
	tSNMP_OCTET_STRING_TYPE NullAddrStr;
	INT4 i4FsNtpsAccessIpMask = 0;
	INT4 i4NextFsNtpsAccessAddrType = 0;
	tSNMP_OCTET_STRING_TYPE NextNtpsAccessIpAddr;
	INT4 i4NextFsNtpsAccessIpMask = 0;
	INT4 i4FsNtpsAccessFlag = 0;
	tIPvXAddr NullAddr;
	UINT4 u4TmpAddr = 0;
	MEMSET(au1AccessAddr, 0, IPVX_IPV4_ADDR_LEN);
	MEMSET(au1AccessNullAddr, 0, IPVX_IPV4_ADDR_LEN);
	MEMSET(&NullAddr, 0, sizeof(tIPvXAddr));
	MEMSET(&NtpsAccessIpAddr, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
	MEMSET(&NextNtpsAccessIpAddr, 0, sizeof(tSNMP_OCTET_STRING_TYPE));

#ifdef IP6_WANTED
	MEMSET(au1Ip6Addr, 0, IPVX_MAX_INET_ADDR_LEN);
#endif
	MEMSET(au1FsNtpUnicastServerAddr, 0, SNMP_MAX_OCTETSTRING_SIZE + 1);
	MEMSET(au1FsNtpUnicastNextServerAddr, 0, SNMP_MAX_OCTETSTRING_SIZE + 1);
	MEMSET(&GetValFsNtpUnicastServerAddr, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
	MEMSET(&GetValFsNtpUnicastNextServerAddr, 0,
		   sizeof(tSNMP_OCTET_STRING_TYPE));
	GetValFsNtpUnicastServerAddr.pu1_OctetList = au1FsNtpUnicastServerAddr;
	GetValFsNtpUnicastNextServerAddr.pu1_OctetList =
		au1FsNtpUnicastNextServerAddr;


	NtpsGetAdminStatus(&u4NtpsAdminStatus);

	if (u4NtpsAdminStatus == NTP_ENABLE)
	{
		CliPrintf(CliHandle, "ntp serve all \r\n");
	}

	if (nmhGetFirstIndexFsNtpsServerTable
		(&i4FsNextNtpUnicastServerAddrType,
		 &GetValFsNtpUnicastNextServerAddr) == SNMP_SUCCESS)
	{
		do
		{
			i4FsNtpUnicastServerAddrType = i4FsNextNtpUnicastServerAddrType;
			MEMCPY(&GetValFsNtpUnicastServerAddr,
				   &GetValFsNtpUnicastNextServerAddr,
				   sizeof(tSNMP_OCTET_STRING_TYPE));
			if (GetValFsNtpUnicastServerAddr.pu1_OctetList != NULL)
			{
				CliPrintf(CliHandle, "ntp server ");
				if (i4FsNtpUnicastServerAddrType == IPVX_ADDR_FMLY_IPV4)
				{
					u4IpAddr = OSIX_HTONL(*(UINT4 *) (VOID *)
										  (GetValFsNtpUnicastServerAddr.pu1_OctetList));
					CLI_CONVERT_IPADDR_TO_STR(pu1String, u4IpAddr);
					CliPrintf(CliHandle, "%s", pu1String);
				}
#ifdef IP6_WANTED
				if (i4FsNtpUnicastServerAddrType == IPVX_ADDR_FMLY_IPV6)
				{
					MEMSET(au1Ip6Addr, 0, IPVX_MAX_INET_ADDR_LEN);
					MEMCPY(au1Ip6Addr,
						   GetValFsNtpUnicastServerAddr.pu1_OctetList,
						   IPVX_IPV6_ADDR_LEN);
					CliPrintf(CliHandle, "%s",
							  Ip6PrintNtop((tIp6Addr *) (VOID *) au1Ip6Addr));
				}
#endif
				nmhGetFsNtpsServerVersion(i4FsNtpUnicastServerAddrType,
										  &GetValFsNtpUnicastServerAddr,
										  &i4NtpsServerVersion);
				if (i4NtpsServerVersion != 0)
				{
					CliPrintf(CliHandle, " version %d", i4NtpsServerVersion);
				}
				nmhGetFsNtpsServerKeyId(i4FsNtpUnicastServerAddrType,
										&GetValFsNtpUnicastServerAddr,
										&i4NtpsServerKeyId);
				if (i4NtpsServerKeyId != 0)
				{
					CliPrintf(CliHandle, " authkey %d", i4NtpsServerKeyId);
				}
				nmhGetFsNtpsServerBurstMode(i4FsNtpUnicastServerAddrType,
											&GetValFsNtpUnicastServerAddr,
											&i4NtpsServerBurstMode);
				if (i4NtpsServerBurstMode == 1)
				{
					CliPrintf(CliHandle, " burst");
				}
				else if (i4NtpsServerBurstMode == 2)
				{
					CliPrintf(CliHandle, " iburst");
				}

				nmhGetFsNtpsServerMinPollInt(i4FsNtpUnicastServerAddrType,
											 &GetValFsNtpUnicastServerAddr,
											 &u4NtpsServerMinPollInt);
				if (u4NtpsServerMinPollInt != 0)
				{
					CliPrintf(CliHandle, " minpoll %d",
							  u4NtpsServerMinPollInt);
				}
				nmhGetFsNtpsServerMaxPollInt(i4FsNtpUnicastServerAddrType,
											 &GetValFsNtpUnicastServerAddr,
											 &u4NtpsServerMaxPollInt);
				if (u4NtpsServerMaxPollInt != 0)
				{
					CliPrintf(CliHandle, " maxpoll %d",
							  u4NtpsServerMaxPollInt);
				}
				nmhGetFsNtpsServerPrefer(i4FsNtpUnicastServerAddrType,
										 &GetValFsNtpUnicastServerAddr,
										 &i4NtpsServerPrefer);
				if (i4NtpsServerPrefer != 0)
				{
					CliPrintf(CliHandle, " prefer");
				}
				CliPrintf(CliHandle, "\r\n");
			}
		}
		while (nmhGetNextIndexFsNtpsServerTable
			   (i4FsNtpUnicastServerAddrType,
				&i4FsNextNtpUnicastServerAddrType,
				&GetValFsNtpUnicastServerAddr,
				&GetValFsNtpUnicastNextServerAddr) == SNMP_SUCCESS);
	}

	nmhGetFsNtpsAuthStatus(&i4NtpsAuthStatus);
	if (i4NtpsAuthStatus == NTP_AUTH_ENABLE)
	{
		CliPrintf(CliHandle, "\r ntp authenticate\r\n");
	}
	i4RetVal = nmhGetFirstIndexFsNtpsAuthTable(&i4FsNtpsAuthKeyId);
	while (i4RetVal == SNMP_SUCCESS)
	{
		pNtpAuth = NtpGetAuthKeyNode(i4FsNtpsAuthKeyId);
		if (pNtpAuth != NULL)
		{
			MEMSET(au1AuthKey, 0, sizeof(au1AuthKey));
			MEMCPY(au1AuthKey, pNtpAuth->u1NtpsAuthKey,
				   sizeof(pNtpAuth->u1NtpsAuthKey));

			CliPrintf(CliHandle,
					  "\rntp authentication-key %u auth_type %s Encrypted_Text %s\r\n",
					  pNtpAuth->u4NtpsAuthKeyId,
					  au1NtpAuthTypeName[pNtpAuth->u4NtpsAuthAlgorithm],
					  pNtpAuth->u1NtpsAuthKey);
			if (pNtpAuth->u4NtpsAuthTrustedKey == NTP_KEY_TRUSTED)
			{
				CliPrintf(CliHandle, "\rntp trusted-key %u\r\n",
						  pNtpAuth->u4NtpsAuthKeyId);
			}
		}

		i4RetVal = nmhGetNextIndexFsNtpsAuthTable(i4FsNtpsAuthKeyId,
												  &i4FsNtpsAuthKeyId);
	}
	i1RetVal = nmhGetFsNtpsSourceInterface(&i4NtpsSourceInterface);
	if ((i1RetVal == SNMP_SUCCESS) && (i4NtpsSourceInterface > 0))
	{
		CliPrintf(CliHandle, "ntp source interface ");
		CfaGetIfType((UINT4)i4NtpsSourceInterface, &u1IfaceType);
		if (u1IfaceType == CFA_ENET)
		{
			MEMSET(au1IfName, 0, CFA_MAX_IFALIAS_LENGTH);
			pi1IfName = (INT1 *) & au1IfName[0];
			CfaCliConfGetIfName((UINT4) i4NtpsSourceInterface, pi1IfName);
			CliPrintf(CliHandle, "%s \r\n", pi1IfName);
		}
		if (u1IfaceType == CFA_L3IPVLAN)
		{
			i1RetVal = CfaGetVlanId((UINT4) i4NtpsSourceInterface, &u2VlanId);
			CliPrintf(CliHandle, "vlan %d \r\n", u2VlanId);
		}
	}
	NtpsAccessIpAddr.pu1_OctetList = au1AccessAddr;
	NullAddrStr.pu1_OctetList = au1AccessNullAddr;
	i1RetVal =
		nmhGetFirstIndexFsNtpsAccessTable(&i4FsNtpsAccessAddrType,
										  &NtpsAccessIpAddr,
										  &i4FsNtpsAccessIpMask);
	MEMCPY(&NextNtpsAccessIpAddr, &NtpsAccessIpAddr,
		   sizeof(tSNMP_OCTET_STRING_TYPE));
	if (i1RetVal == SNMP_SUCCESS)
	{
		do
		{
			CliPrintf(CliHandle, "ntp restrict ");
			nmhGetFsNtpsAccessFlag(i4FsNtpsAccessAddrType,
								   &NtpsAccessIpAddr,
								   i4FsNtpsAccessIpMask, &i4FsNtpsAccessFlag);
			if ((MEMCMP
				 (NullAddrStr.pu1_OctetList,
				  NtpsAccessIpAddr.pu1_OctetList,
				  NtpsAccessIpAddr.i4_Length) == 0))
			{
				if (i4FsNtpsAccessAddrType == IPVX_ADDR_FMLY_IPV4)
				{
					CliPrintf(CliHandle, "default ipv4 ");
				}
				else if (i4FsNtpsAccessAddrType == IPVX_ADDR_FMLY_IPV6)
				{
					CliPrintf(CliHandle, "default ipv6 ");
				}

			}
			else
			{
				if (i4FsNtpsAccessAddrType == IPVX_ADDR_FMLY_IPV4)
				{
					u4TmpAddr = OSIX_HTONL(*(UINT4 *) (VOID *)
										   NtpsAccessIpAddr.pu1_OctetList);
					CLI_CONVERT_IPADDR_TO_STR(pu1String, u4TmpAddr);
					CliPrintf(CliHandle, "address %s ", pu1String);
				}
				else if (i4FsNtpsAccessAddrType == IPVX_ADDR_FMLY_IPV6)
				{
					CliPrintf(CliHandle, "address %s ",
							  Ip6PrintNtop((tIp6Addr *) (VOID *)
										   NtpsAccessIpAddr.pu1_OctetList));
				}
				CliPrintf(CliHandle, "mask %d ", i4FsNtpsAccessIpMask);
			}
			if (i4FsNtpsAccessFlag > 0)
			{
				if ((i4FsNtpsAccessFlag & NTP_RES_IGNORE) == NTP_RES_IGNORE)
				{
					CliPrintf(CliHandle, "%s ", au1ResFlag[NTP_RES_IGNORE]);
				}
				if ((i4FsNtpsAccessFlag & NTP_RES_KOD) == NTP_RES_KOD)
				{
					CliPrintf(CliHandle, "%s ", au1ResFlag[NTP_RESTRICT_KOD]);
				}
				if ((i4FsNtpsAccessFlag & NTP_RES_LIMITED) == NTP_RES_LIMITED)
				{
					CliPrintf(CliHandle, "%s ",
							  au1ResFlag[NTP_RESTRICT_LIMITED]);
				}
				if ((i4FsNtpsAccessFlag & NTP_RES_LOWPRIOTRAP) ==
					NTP_RES_LOWPRIOTRAP)
				{
					CliPrintf(CliHandle, "%s ",
							  au1ResFlag[NTP_RESTRICT_LOWPRIOTRAP]);
				}
				if ((i4FsNtpsAccessFlag & NTP_RES_NOMODIFY) ==
					NTP_RES_NOMODIFY)
				{
					CliPrintf(CliHandle, "%s ",
							  au1ResFlag[NTP_RESTRICT_NOMODIFY]);
				}
				if ((i4FsNtpsAccessFlag & NTP_RES_NOQUERY) == NTP_RES_NOQUERY)
				{
					CliPrintf(CliHandle, "%s ",
							  au1ResFlag[NTP_RESTRICT_NOQUERY]);
				}
				if ((i4FsNtpsAccessFlag & NTP_RES_NOPEER) == NTP_RES_NOPEER)
				{
					CliPrintf(CliHandle, "%s ",
							  au1ResFlag[NTP_RESTRICT_NOPEER]);
				}
				if ((i4FsNtpsAccessFlag & NTP_RES_NOSERVE) == NTP_RES_NOSERVE)
				{
					CliPrintf(CliHandle, "%s ",
							  au1ResFlag[NTP_RESTRICT_NOSERVE]);
				}
				if ((i4FsNtpsAccessFlag & NTP_RES_NOTRAP) == NTP_RES_NOTRAP)
				{
					CliPrintf(CliHandle, "%s ",
							  au1ResFlag[NTP_RESTRICT_NOTRAP]);
				}
				if ((i4FsNtpsAccessFlag & NTP_RES_NOTRUST) == NTP_RES_NOTRUST)
				{
					CliPrintf(CliHandle, "%s ",
							  au1ResFlag[NTP_RESTRICT_NOTRUST]);
				}
				if ((i4FsNtpsAccessFlag & NTP_RES_NTPPORT) == NTP_RES_NTPPORT)
				{
					CliPrintf(CliHandle, "%s ",
							  au1ResFlag[NTP_RESTRICT_NTPPORT]);
				}
				if ((i4FsNtpsAccessFlag & NTP_RES_VERSION) == NTP_RES_VERSION)
				{
					CliPrintf(CliHandle, "%s ",
							  au1ResFlag[NTP_RESTRICT_VERSION]);
				}
			}
			CliPrintf(CliHandle, "\r\n");
			i1RetVal =
				nmhGetNextIndexFsNtpsAccessTable(i4FsNtpsAccessAddrType,
												 &i4NextFsNtpsAccessAddrType,
												 &NtpsAccessIpAddr,
												 &NextNtpsAccessIpAddr,
												 i4FsNtpsAccessIpMask,
												 &i4NextFsNtpsAccessIpMask);
			i4FsNtpsAccessAddrType = i4NextFsNtpsAccessAddrType;
			i4FsNtpsAccessIpMask = i4NextFsNtpsAccessIpMask;
			MEMCPY(&NtpsAccessIpAddr, &NextNtpsAccessIpAddr,
				   sizeof(tSNMP_OCTET_STRING_TYPE));
		}
		while (i1RetVal == SNMP_SUCCESS);
	}

	NtpsInterfaceRunningConfig(CliHandle);
	nmhGetFsNtpsServerExecute((INT4 *) & u4GetStat);
    if(u4GetStat == NTP_SERVER_START)
    {
        CliPrintf(CliHandle,"ntp server start \r\n");
    }
    else if(u4GetStat == NTP_SERVER_RESTART)
    {
        CliPrintf(CliHandle,"ntp server restart \r\n");
    }
	CliPrintf(CliHandle, "\r\n");
	return CLI_SUCCESS;
}

/****************************************************************************
 * Function         : NtpsInterfaceRunningConfig                            *
 * Description      : This function prints NTP configuration of ALL L3 i/f  *
 * Input            : CliHandle   - Handle to the CLI                       *
 * Output           : None.                                                 *
 * Returns          : None                                                  *
 ***************************************************************************/
VOID NtpsInterfaceRunningConfig(tCliHandle CliHandle)
{
	INT4 i4FsNtpsIntfId = 0;
	INT1 *pi1IfName = NULL;
	UINT1 u1IfaceType = 0;
	UINT2 u2VlanId = 0;
	INT4 i4RetVal = 0;
	UINT1 u1Flag = 0;
	UINT1 au1IfName[CFA_MAX_IFALIAS_LENGTH];
	tNtpsIntf *pNtpIntf = NULL;
	i4RetVal = (INT1) nmhGetFirstIndexFsNtpsInterfaceTable(&i4FsNtpsIntfId);
	if (i4RetVal == SNMP_FAILURE)
	{
		NTP_TRC(NTP_MGMT_TRC,
				"NtpsInterfaceRunningConfig: Fetching information from Interface table failed - GetFirst\r\n");
		return;
	}
	do
	{
		CliPrintf(CliHandle, "!\r\n");
		CfaGetIfType((UINT4)i4FsNtpsIntfId, &u1IfaceType);
		pNtpIntf = NtpGetIntfNode((UINT4)i4FsNtpsIntfId);
		if (pNtpIntf != NULL)
		{
			if (pNtpIntf->u4NtpsIntfStatus == NTP_ENABLE)
			{
				if (u1IfaceType == CFA_ENET)
				{
					MEMSET(au1IfName, 0, CFA_MAX_IFALIAS_LENGTH);
					pi1IfName = (INT1 *) & au1IfName[0];
					CfaCliConfGetIfName((UINT4) i4FsNtpsIntfId, pi1IfName);
					CliPrintf(CliHandle, "interface %s \r\n", pi1IfName);
					NtpsShowInterfaceRunningConfig(CliHandle, (UINT4)i4FsNtpsIntfId);
					u1Flag = 1;
				}
				else if (u1IfaceType == CFA_L3IPVLAN)
				{
					CfaGetVlanId((UINT4) i4FsNtpsIntfId, &u2VlanId);
					CliPrintf(CliHandle, "interface vlan %d \r\n", u2VlanId);
					NtpsShowInterfaceRunningConfig(CliHandle, (UINT4)i4FsNtpsIntfId);
					u1Flag = 1;
				}
			}
			else
			{
				NTP_TRC_ARG1(NTP_MGMT_TRC,
							 "NtpsInterfaceRunningConfig: Interface index %u skipped as NTP is disabled \r\n",
							 i4FsNtpsIntfId);
			}
		}
		i4RetVal =
			(INT1) nmhGetNextIndexFsNtpsInterfaceTable(i4FsNtpsIntfId,
													   &i4FsNtpsIntfId);
	}
	while (i4RetVal == SNMP_SUCCESS);
	if (u1Flag == 1)
	{
		CliPrintf(CliHandle, "!\r\n");
	}
	return;
}

/****************************************************************************
 * Function         : NtpsInterfaceRunningConfig                            *
 * Description      : This function prints the NTP configuration of given   *
                      L3 i/f                                                *
 * Input            : CliHandle   - Handle to the CLI                       *
 * Output           : None.                                                 *
 * Returns          : None                                                  *
 ***************************************************************************/
VOID NtpsShowInterfaceRunningConfig(tCliHandle CliHandle, UINT4 u4IfIndex)
{
	tNtpsIntf *pNtpIntf = NULL;
	pNtpIntf = NtpGetIntfNode(u4IfIndex);
	if ((pNtpIntf != NULL) && (pNtpIntf->u4NtpsIntfStatus == NTP_ENABLE))
	{
		CliPrintf(CliHandle, "ntp serve \r\n");
	}
}

/****************************************************************************
 * Function         : NtpsShowServerStatus                                  *
 * Description      : This function displays the NTP daemon running status  *
 * Input            : CliHandle   - Handle to the CLI                       *
 * Output           : None.                                                 *
 * Returns          : None                                                  *
 ***************************************************************************/
INT4 NtpShowServersStatus(tCliHandle CliHandle)
{
	UINT1 au1BufStatus[NTPS_TEMP_BUF];
	INT4 i4RetVal = 0;
	UNUSED_PARAM(CliHandle);
	SNPRINTF((CHR1 *) au1BufStatus, sizeof(au1BufStatus),
			 "%s", NTP_SERVER_STATUS);

	CliPrintf(CliHandle, "NTP Server Running Status \r\n");
	CliFlush(CliHandle);
	if (NtpsExecuteNTPCommand(au1BufStatus, &i4RetVal) == NTP_FAILURE)
	{
		NTP_TRC(NTP_MGMT_TRC, "NtpShowServersStatus failed to show \n");
		return CLI_FAILURE;
	}
	CliPrintf(CliHandle, "\r\n");
	return CLI_SUCCESS;
}

/****************************************************************************
 * Function         : NtpsShowServerStatistics                              *
 * Description      : This function displays the NTP daemon statistics      *
 * Input            : CliHandle   - Handle to the CLI                       *
 * Output           : None.                                                 *
 * Returns          : None                                                  *
 ***************************************************************************/
INT4 NtpShowServersStatistics(tCliHandle CliHandle)
{
	UINT1 au1BufConfig[NTPS_TEMP_BUF];
	INT4 i4RetVal = 0;
    MEMSET(au1BufConfig, 0, NTPS_TEMP_BUF);
	SNPRINTF((CHR1 *) au1BufConfig, sizeof(au1BufConfig), "%s", NTP_STATS);

	CliPrintf(CliHandle, "NTP Server Statistics \r\n");
	CliFlush(CliHandle);
	if (NtpsExecuteNTPCommand(au1BufConfig, &i4RetVal) == NTP_FAILURE)
	{

		NTP_TRC(NTP_MGMT_TRC, "NtpShowServersStatistics failed to show \n");
		return CLI_FAILURE;
	}
	CliPrintf(CliHandle, "\r\n");
	return CLI_SUCCESS;
}

/****************************************************************************
 * Function         : NtpsShowSync                                          *
 * Description      : This function displays the NTP daemon server sync     *
 *                    status                                                *
 * Input            : CliHandle   - Handle to the CLI                       *
 * Output           : None.                                                 *
 * Returns          : None                                                  *
 ***************************************************************************/
INT4 NtpShowSync(tCliHandle CliHandle)
{
	UINT1 au1BufStatus[NTPS_TEMP_BUF];
	INT4 i4RetVal = 0;
	SNPRINTF((CHR1 *) au1BufStatus, sizeof(au1BufStatus), "%s", NTP_SYNC);

	CliPrintf(CliHandle, "NTP Server Sync Status\r\n");
	CliFlush(CliHandle);
	if (NtpsExecuteNTPCommand(au1BufStatus, &i4RetVal) == NTP_FAILURE)
	{
		NTP_TRC(NTP_MGMT_TRC, "NtpShowSync failed to show \n");
		return CLI_FAILURE;
	}
	CliPrintf(CliHandle, "\r\n");
	return CLI_SUCCESS;
}

/****************************************************************************
 * Function         : NtpsShowAssociation                                   *
 * Description      : This function displays the NTP daemon server          *
 *                    association status                                    *
 * Input            : CliHandle   - Handle to the CLI                       *
 * Output           : None.                                                 *
 * Returns          : None                                                  *
 ***************************************************************************/
INT4 NtpShowAssociation(tCliHandle CliHandle)
{
	UINT1 au1BufConfig[NTPS_TEMP_BUF];
	INT4 i4RetVal = 0;
    MEMSET(au1BufConfig, 0, NTPS_TEMP_BUF);
	SNPRINTF((CHR1 *) au1BufConfig, sizeof(au1BufConfig), "%s", NTP_PEERS);

	CliPrintf(CliHandle, "NTP Server Server Associations\r\n");
	CliFlush(CliHandle);
	if (NtpsExecuteNTPCommand(au1BufConfig, &i4RetVal) == NTP_FAILURE)
	{
		NTP_TRC(NTP_MGMT_TRC, "NtpShowAssociation failed to show \n");
		return CLI_FAILURE;
	}
	CliPrintf(CliHandle, "\r\n");
	return CLI_SUCCESS;
}

/****************************************************************************
 * Function         : NtpsShowClients                                       *
 * Description      : This function displays the NTP clients connected      *
 *                    to this NTP server                                    *
 * Input            : CliHandle   - Handle to the CLI                       *
 * Output           : None.                                                 *
 * Returns          : None                                                  *
 ***************************************************************************/
INT4 NtpShowclients(tCliHandle CliHandle)
{
	UINT1 au1BufStatus[NTPS_TEMP_BUF];
	INT4 i4RetVal = 0;
    MEMSET(au1BufStatus, 0, NTPS_TEMP_BUF);
	SNPRINTF((CHR1 *) au1BufStatus, sizeof(au1BufStatus), "%s", NTP_CLIENTS);

	CliPrintf(CliHandle, "NTP Server Client Status\r\n");
	CliFlush(CliHandle);
	if (NtpsExecuteNTPCommand(au1BufStatus, &i4RetVal) == NTP_FAILURE)
	{
		NTP_TRC(NTP_MGMT_TRC, "NtpShowclients failed to show \n");
		return CLI_FAILURE;
	}
	CliPrintf(CliHandle, "\r\n");
	return CLI_SUCCESS;
}

/****************************************************************************
 * Function         : NtpsShowDetail                                        *
 * Description      : This function displays the NTP daemon's timer stats   *
 *                    system info and io stats                              *
 * Input            : CliHandle   - Handle to the CLI                       *
 * Output           : None.                                                 *
 * Returns          : None                                                  *
 ***************************************************************************/
INT4 ShowNtpsDetail(tCliHandle CliHandle)
{
	INT4 i4NtpsAuthStatus = NTP_AUTH_ENABLE;
	UINT4 u4GetStat = 0;
	INT4 i4RetVal = 0;
	UINT1 au1BufStatus[NTPS_TEMP_BUF];


    MEMSET(au1BufStatus, 0, NTPS_TEMP_BUF);
	nmhGetFsNtpsServerExecute((INT4 *) & u4GetStat);
	if (u4GetStat == NTP_SERVER_STOP)
	{
		CliPrintf(CliHandle,
				  "\rNTP Server Configuration Status  : Stopped\r\n");
	}
	else
	{
		CliPrintf(CliHandle,
				  "\rNTP Server Configuration Status  : Started\r\n");
	}
	nmhGetFsNtpsAuthStatus(&i4NtpsAuthStatus);
	if (i4NtpsAuthStatus == NTP_AUTH_ENABLE)
	{
		CliPrintf(CliHandle, "\rNTP Authenticate  : Enabled\r\n");
	}
	else
	{
		CliPrintf(CliHandle, "\rNTP Authenticate  : Disabled\r\n");
	}
	CliPrintf(CliHandle, "\nNTP Server Version:\r\n");
	CliFlush(CliHandle);
	SNPRINTF((CHR1 *) au1BufStatus, sizeof(au1BufStatus), "%s", NTPQ_VERSION);
	if (NtpsExecuteNTPCommand(au1BufStatus, &i4RetVal) == NTP_FAILURE)
	{
		NTP_TRC_ARG1(NTP_MGMT_TRC, "NtpShowDetail failed to fetch %s \n",
					 NTPQ_VERSION);
		return CLI_FAILURE;
	}
	CliPrintf(CliHandle, "\nNTP Server TimerStats:\r\n");
	CliFlush(CliHandle);
    MEMSET(au1BufStatus, 0, NTPS_TEMP_BUF);
	SNPRINTF((CHR1 *) au1BufStatus, sizeof(au1BufStatus), "%s",
			 NTPQ_TIMERSTATS);
	if (NtpsExecuteNTPCommand(au1BufStatus, &i4RetVal) == NTP_FAILURE)
	{
		NTP_TRC_ARG1(NTP_MGMT_TRC, "NtpShowDetail failed to fetch %s \n",
					 NTPQ_TIMERSTATS);
		return CLI_FAILURE;
	}
	CliPrintf(CliHandle, "\nNTP Server SysInfo:\r\n");
	CliFlush(CliHandle);
    MEMSET(au1BufStatus, 0, NTPS_TEMP_BUF);
	SNPRINTF((CHR1 *) au1BufStatus, sizeof(au1BufStatus), "%s", NTPQ_SYSINFO);
	if (NtpsExecuteNTPCommand(au1BufStatus, &i4RetVal) == NTP_FAILURE)
	{
		NTP_TRC_ARG1(NTP_MGMT_TRC, "NtpShowDetail failed to fetch %s \n",
					 NTPQ_SYSINFO);
		return CLI_FAILURE;
	}
	CliPrintf(CliHandle, "\nNTP Server IOSTATS:\r\n");
	CliFlush(CliHandle);
    MEMSET(au1BufStatus, 0, NTPS_TEMP_BUF);
	SNPRINTF((CHR1 *) au1BufStatus, sizeof(au1BufStatus), "%s", NTPQ_IOSTATS);
	if (NtpsExecuteNTPCommand(au1BufStatus, &i4RetVal) == NTP_FAILURE)
	{
		NTP_TRC_ARG1(NTP_MGMT_TRC, "NtpShowDetail failed to fetch %s \n",
					 NTPQ_IOSTATS);
		return CLI_FAILURE;
	}
	return CLI_SUCCESS;
}



#endif
