#include  "lr.h"
#include  "fssnmp.h"
#include  "fsntpswr.h"
#include  "fsntpsdb.h"
#include  "ntpsinc.h"

VOID RegisterFSNTPS ()
{
    SNMPRegisterMib (&FsNtpsServerTableOID, &FsNtpsServerTableEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsNtpsAuthKeyTableOID, &FsNtpsAuthKeyTableEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsNtpsAccessTableOID, &FsNtpsAccessTableEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsNtpsInterfaceTableOID, &FsNtpsInterfaceTableEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsNtpsGlobalStatusOID, &FsNtpsGlobalStatusEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsNtpsServerExecuteOID, &FsNtpsServerExecuteEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsNtpsServerStatusOID, &FsNtpsServerStatusEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsNtpsDebugOID, &FsNtpsDebugEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsNtpsAuthStatusOID, &FsNtpsAuthStatusEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsNtpsSourceInterfaceOID, &FsNtpsSourceInterfaceEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsntpsOID, (const UINT1 *) "fsntps");
}

VOID UnRegisterFSNTPS ()
{
    SNMPUnRegisterMib (&FsNtpsServerTableOID, &FsNtpsServerTableEntry);
    SNMPUnRegisterMib (&FsNtpsAuthKeyTableOID, &FsNtpsAuthKeyTableEntry);
    SNMPUnRegisterMib (&FsNtpsAccessTableOID, &FsNtpsAccessTableEntry);
    SNMPUnRegisterMib (&FsNtpsInterfaceTableOID, &FsNtpsInterfaceTableEntry);
    SNMPUnRegisterMib (&FsNtpsGlobalStatusOID, &FsNtpsGlobalStatusEntry);
    SNMPUnRegisterMib (&FsNtpsServerExecuteOID, &FsNtpsServerExecuteEntry);
    SNMPUnRegisterMib (&FsNtpsServerStatusOID, &FsNtpsServerStatusEntry);
    SNMPUnRegisterMib (&FsNtpsDebugOID, &FsNtpsDebugEntry);
    SNMPUnRegisterMib (&FsNtpsAuthStatusOID, &FsNtpsAuthStatusEntry);
    SNMPUnRegisterMib (&FsNtpsSourceInterfaceOID, &FsNtpsSourceInterfaceEntry);
    SNMPDelSysorEntry (&fsntpsOID, (const UINT1 *) "fsntps");
}

INT4 FsNtpsGlobalStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return (nmhGetFsNtpsGlobalStatus(&(pMultiData->i4_SLongValue)));
}

INT4 FsNtpsServerExecuteGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return (nmhGetFsNtpsServerExecute(&(pMultiData->i4_SLongValue)));
}

INT4 FsNtpsServerStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return (nmhGetFsNtpsServerStatus(&(pMultiData->i4_SLongValue)));
}

INT4 FsNtpsDebugGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return (nmhGetFsNtpsDebug(&(pMultiData->i4_SLongValue)));
}

INT4 FsNtpsAuthStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return (nmhGetFsNtpsAuthStatus(&(pMultiData->i4_SLongValue)));
}

INT4 FsNtpsSourceInterfaceGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return (nmhGetFsNtpsSourceInterface(&(pMultiData->i4_SLongValue)));
}

INT4 FsNtpsGlobalStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return (nmhSetFsNtpsGlobalStatus(pMultiData->i4_SLongValue));
}


INT4 FsNtpsServerExecuteSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return (nmhSetFsNtpsServerExecute(pMultiData->i4_SLongValue));
}


INT4 FsNtpsDebugSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return (nmhSetFsNtpsDebug(pMultiData->i4_SLongValue));
}


INT4 FsNtpsAuthStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return (nmhSetFsNtpsAuthStatus(pMultiData->i4_SLongValue));
}


INT4 FsNtpsSourceInterfaceSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return (nmhSetFsNtpsSourceInterface(pMultiData->i4_SLongValue));
}


INT4 FsNtpsGlobalStatusTest(UINT4 * pu4Error, tSnmpIndex * pMultiIndex,
							tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return (nmhTestv2FsNtpsGlobalStatus(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsNtpsServerExecuteTest(UINT4 * pu4Error, tSnmpIndex * pMultiIndex,
							 tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return (nmhTestv2FsNtpsServerExecute(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsNtpsDebugTest(UINT4 * pu4Error, tSnmpIndex * pMultiIndex,
					 tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return (nmhTestv2FsNtpsDebug(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsNtpsAuthStatusTest(UINT4 * pu4Error, tSnmpIndex * pMultiIndex,
						  tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return (nmhTestv2FsNtpsAuthStatus(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsNtpsSourceInterfaceTest(UINT4 * pu4Error, tSnmpIndex * pMultiIndex,
							   tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return (nmhTestv2FsNtpsSourceInterface
			(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsNtpsGlobalStatusDep(UINT4 * pu4Error,
						   tSnmpIndexList * pSnmpIndexList,
						   tSNMP_VAR_BIND * pSnmpvarbinds)
{
	return (nmhDepv2FsNtpsGlobalStatus
			(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsNtpsServerExecuteDep(UINT4 * pu4Error,
							tSnmpIndexList * pSnmpIndexList,
							tSNMP_VAR_BIND * pSnmpvarbinds)
{
	return (nmhDepv2FsNtpsServerExecute
			(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsNtpsDebugDep(UINT4 * pu4Error, tSnmpIndexList * pSnmpIndexList,
					tSNMP_VAR_BIND * pSnmpvarbinds)
{
	return (nmhDepv2FsNtpsDebug(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsNtpsAuthStatusDep(UINT4 * pu4Error, tSnmpIndexList * pSnmpIndexList,
						 tSNMP_VAR_BIND * pSnmpvarbinds)
{
	return (nmhDepv2FsNtpsAuthStatus(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsNtpsSourceInterfaceDep(UINT4 * pu4Error,
							  tSnmpIndexList * pSnmpIndexList,
							  tSNMP_VAR_BIND * pSnmpvarbinds)
{
	return (nmhDepv2FsNtpsSourceInterface
			(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsNtpsServerTable(tSnmpIndex * pFirstMultiIndex,
								   tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL)
	{
		if (nmhGetFirstIndexFsNtpsServerTable
			(&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			 pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsNtpsServerTable
			(pFirstMultiIndex->pIndex[0].i4_SLongValue,
			 &(pNextMultiIndex->pIndex[0].i4_SLongValue),
			 pFirstMultiIndex->pIndex[1].pOctetStrValue,
			 pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}

	return SNMP_SUCCESS;
}

INT4 FsNtpsServerVersionGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsNtpsServerTable
		(pMultiIndex->pIndex[0].i4_SLongValue,
		 pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return (nmhGetFsNtpsServerVersion(pMultiIndex->pIndex[0].i4_SLongValue,
									  pMultiIndex->pIndex[1].pOctetStrValue,
									  &(pMultiData->i4_SLongValue)));

}

INT4 FsNtpsServerKeyIdGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsNtpsServerTable
		(pMultiIndex->pIndex[0].i4_SLongValue,
		 pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return (nmhGetFsNtpsServerKeyId(pMultiIndex->pIndex[0].i4_SLongValue,
									pMultiIndex->pIndex[1].pOctetStrValue,
									&(pMultiData->i4_SLongValue)));

}

INT4 FsNtpsServerBurstModeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsNtpsServerTable
		(pMultiIndex->pIndex[0].i4_SLongValue,
		 pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return (nmhGetFsNtpsServerBurstMode
			(pMultiIndex->pIndex[0].i4_SLongValue,
			 pMultiIndex->pIndex[1].pOctetStrValue,
			 &(pMultiData->i4_SLongValue)));

}

INT4 FsNtpsServerMinPollIntGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsNtpsServerTable
		(pMultiIndex->pIndex[0].i4_SLongValue,
		 pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return (nmhGetFsNtpsServerMinPollInt
			(pMultiIndex->pIndex[0].i4_SLongValue,
			 pMultiIndex->pIndex[1].pOctetStrValue,
			 &(pMultiData->u4_ULongValue)));

}

INT4 FsNtpsServerMaxPollIntGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsNtpsServerTable
		(pMultiIndex->pIndex[0].i4_SLongValue,
		 pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return (nmhGetFsNtpsServerMaxPollInt
			(pMultiIndex->pIndex[0].i4_SLongValue,
			 pMultiIndex->pIndex[1].pOctetStrValue,
			 &(pMultiData->u4_ULongValue)));

}

INT4 FsNtpsServerRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsNtpsServerTable
		(pMultiIndex->pIndex[0].i4_SLongValue,
		 pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return (nmhGetFsNtpsServerRowStatus
			(pMultiIndex->pIndex[0].i4_SLongValue,
			 pMultiIndex->pIndex[1].pOctetStrValue,
			 &(pMultiData->i4_SLongValue)));

}

INT4 FsNtpsServerPreferGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsNtpsServerTable
		(pMultiIndex->pIndex[0].i4_SLongValue,
		 pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return (nmhGetFsNtpsServerPrefer(pMultiIndex->pIndex[0].i4_SLongValue,
									 pMultiIndex->pIndex[1].pOctetStrValue,
									 &(pMultiData->i4_SLongValue)));

}

INT4 FsNtpsServerVersionSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsNtpsServerVersion(pMultiIndex->pIndex[0].i4_SLongValue,
									  pMultiIndex->pIndex[1].pOctetStrValue,
									  pMultiData->i4_SLongValue));

}

INT4 FsNtpsServerKeyIdSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsNtpsServerKeyId(pMultiIndex->pIndex[0].i4_SLongValue,
									pMultiIndex->pIndex[1].pOctetStrValue,
									pMultiData->i4_SLongValue));

}

INT4 FsNtpsServerBurstModeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsNtpsServerBurstMode
			(pMultiIndex->pIndex[0].i4_SLongValue,
			 pMultiIndex->pIndex[1].pOctetStrValue,
			 pMultiData->i4_SLongValue));

}

INT4 FsNtpsServerMinPollIntSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsNtpsServerMinPollInt
			(pMultiIndex->pIndex[0].i4_SLongValue,
			 pMultiIndex->pIndex[1].pOctetStrValue,
			 pMultiData->u4_ULongValue));

}

INT4 FsNtpsServerMaxPollIntSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsNtpsServerMaxPollInt
			(pMultiIndex->pIndex[0].i4_SLongValue,
			 pMultiIndex->pIndex[1].pOctetStrValue,
			 pMultiData->u4_ULongValue));

}

INT4 FsNtpsServerRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsNtpsServerRowStatus
			(pMultiIndex->pIndex[0].i4_SLongValue,
			 pMultiIndex->pIndex[1].pOctetStrValue,
			 pMultiData->i4_SLongValue));

}

INT4 FsNtpsServerPreferSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsNtpsServerPrefer(pMultiIndex->pIndex[0].i4_SLongValue,
									 pMultiIndex->pIndex[1].pOctetStrValue,
									 pMultiData->i4_SLongValue));

}

INT4 FsNtpsServerVersionTest(UINT4 * pu4Error, tSnmpIndex * pMultiIndex,
							 tRetVal * pMultiData)
{
	return (nmhTestv2FsNtpsServerVersion(pu4Error,
										 pMultiIndex->pIndex[0].i4_SLongValue,
										 pMultiIndex->pIndex[1].pOctetStrValue,
										 pMultiData->i4_SLongValue));

}

INT4 FsNtpsServerKeyIdTest(UINT4 * pu4Error, tSnmpIndex * pMultiIndex,
						   tRetVal * pMultiData)
{
	return (nmhTestv2FsNtpsServerKeyId(pu4Error,
									   pMultiIndex->pIndex[0].i4_SLongValue,
									   pMultiIndex->pIndex[1].pOctetStrValue,
									   pMultiData->i4_SLongValue));

}

INT4 FsNtpsServerBurstModeTest(UINT4 * pu4Error, tSnmpIndex * pMultiIndex,
							   tRetVal * pMultiData)
{
	return (nmhTestv2FsNtpsServerBurstMode(pu4Error,
										   pMultiIndex->
										   pIndex[0].i4_SLongValue,
										   pMultiIndex->
										   pIndex[1].pOctetStrValue,
										   pMultiData->i4_SLongValue));

}

INT4 FsNtpsServerMinPollIntTest(UINT4 * pu4Error, tSnmpIndex * pMultiIndex,
								tRetVal * pMultiData)
{
	return (nmhTestv2FsNtpsServerMinPollInt(pu4Error,
											pMultiIndex->
											pIndex[0].i4_SLongValue,
											pMultiIndex->
											pIndex[1].pOctetStrValue,
											pMultiData->u4_ULongValue));

}

INT4 FsNtpsServerMaxPollIntTest(UINT4 * pu4Error, tSnmpIndex * pMultiIndex,
								tRetVal * pMultiData)
{
	return (nmhTestv2FsNtpsServerMaxPollInt(pu4Error,
											pMultiIndex->
											pIndex[0].i4_SLongValue,
											pMultiIndex->
											pIndex[1].pOctetStrValue,
											pMultiData->u4_ULongValue));

}

INT4 FsNtpsServerRowStatusTest(UINT4 * pu4Error, tSnmpIndex * pMultiIndex,
							   tRetVal * pMultiData)
{
	return (nmhTestv2FsNtpsServerRowStatus(pu4Error,
										   pMultiIndex->
										   pIndex[0].i4_SLongValue,
										   pMultiIndex->
										   pIndex[1].pOctetStrValue,
										   pMultiData->i4_SLongValue));

}

INT4 FsNtpsServerPreferTest(UINT4 * pu4Error, tSnmpIndex * pMultiIndex,
							tRetVal * pMultiData)
{
	return (nmhTestv2FsNtpsServerPrefer(pu4Error,
										pMultiIndex->pIndex[0].i4_SLongValue,
										pMultiIndex->pIndex[1].pOctetStrValue,
										pMultiData->i4_SLongValue));

}

INT4 FsNtpsServerTableDep(UINT4 * pu4Error,
						  tSnmpIndexList * pSnmpIndexList,
						  tSNMP_VAR_BIND * pSnmpvarbinds)
{
	return (nmhDepv2FsNtpsServerTable
			(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsNtpsAuthKeyTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL)
	{
		if (nmhGetFirstIndexFsNtpsAuthKeyTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsNtpsAuthKeyTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}

	return SNMP_SUCCESS;
}

INT4 FsNtpsAuthAlgorithmGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsNtpsAuthKeyTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return (nmhGetFsNtpsAuthAlgorithm(pMultiIndex->pIndex[0].i4_SLongValue,
									  &(pMultiData->i4_SLongValue)));

}

INT4 FsNtpsAuthKeyGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsNtpsAuthTable
		(pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return (nmhGetFsNtpsAuthKey(pMultiIndex->pIndex[0].i4_SLongValue,
								pMultiData->pOctetStrValue));

}

INT4 FsNtpsAuthTrustedKeyGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsNtpsAuthKeyTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return (nmhGetFsNtpsAuthTrustedKey
			(pMultiIndex->pIndex[0].i4_SLongValue,
			 &(pMultiData->i4_SLongValue)));

}

INT4 FsNtpsAuthRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsNtpsAuthKeyTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return (nmhGetFsNtpsAuthRowStatus(pMultiIndex->pIndex[0].i4_SLongValue,
									  &(pMultiData->i4_SLongValue)));

}

INT4 FsNtpsAuthAlgorithmSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsNtpsAuthAlgorithm(pMultiIndex->pIndex[0].i4_SLongValue,
									  pMultiData->i4_SLongValue));

}

INT4 FsNtpsAuthKeySet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsNtpsAuthKey(pMultiIndex->pIndex[0].i4_SLongValue,
								pMultiData->pOctetStrValue));

}

INT4 FsNtpsAuthTrustedKeySet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsNtpsAuthTrustedKey
			(pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4 FsNtpsAuthRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsNtpsAuthRowStatus(pMultiIndex->pIndex[0].i4_SLongValue,
									  pMultiData->i4_SLongValue));

}

INT4 FsNtpsAuthAlgorithmTest(UINT4 * pu4Error, tSnmpIndex * pMultiIndex,
							 tRetVal * pMultiData)
{
	return (nmhTestv2FsNtpsAuthAlgorithm(pu4Error,
										 pMultiIndex->pIndex[0].i4_SLongValue,
										 pMultiData->i4_SLongValue));

}

INT4 FsNtpsAuthKeyTest(UINT4 * pu4Error, tSnmpIndex * pMultiIndex,
					   tRetVal * pMultiData)
{
	return (nmhTestv2FsNtpsAuthKey(pu4Error,
								   pMultiIndex->pIndex[0].i4_SLongValue,
								   pMultiData->pOctetStrValue));

}

INT4 FsNtpsAuthTrustedKeyTest(UINT4 * pu4Error, tSnmpIndex * pMultiIndex,
							  tRetVal * pMultiData)
{
	return (nmhTestv2FsNtpsAuthTrustedKey(pu4Error,
										  pMultiIndex->pIndex[0].i4_SLongValue,
										  pMultiData->i4_SLongValue));

}

INT4 FsNtpsAuthRowStatusTest(UINT4 * pu4Error, tSnmpIndex * pMultiIndex,
							 tRetVal * pMultiData)
{
	return (nmhTestv2FsNtpsAuthRowStatus(pu4Error,
										 pMultiIndex->pIndex[0].i4_SLongValue,
										 pMultiData->i4_SLongValue));

}

INT4 FsNtpsAuthKeyTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsNtpsAuthKeyTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsNtpsAccessTable(tSnmpIndex * pFirstMultiIndex,
								   tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL)
	{
		if (nmhGetFirstIndexFsNtpsAccessTable
			(&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			 pNextMultiIndex->pIndex[1].pOctetStrValue,
			 &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsNtpsAccessTable
			(pFirstMultiIndex->pIndex[0].i4_SLongValue,
			 &(pNextMultiIndex->pIndex[0].i4_SLongValue),
			 pFirstMultiIndex->pIndex[1].pOctetStrValue,
			 pNextMultiIndex->pIndex[1].pOctetStrValue,
			 pFirstMultiIndex->pIndex[2].i4_SLongValue,
			 &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}

	return SNMP_SUCCESS;
}

INT4 FsNtpsAccessRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsNtpsAccessTable
		(pMultiIndex->pIndex[0].i4_SLongValue,
		 pMultiIndex->pIndex[1].pOctetStrValue,
		 pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return (nmhGetFsNtpsAccessRowStatus
			(pMultiIndex->pIndex[0].i4_SLongValue,
			 pMultiIndex->pIndex[1].pOctetStrValue,
			 pMultiIndex->pIndex[2].i4_SLongValue,
			 &(pMultiData->i4_SLongValue)));

}

INT4 FsNtpsAccessFlagGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsNtpsAccessTable
		(pMultiIndex->pIndex[0].i4_SLongValue,
		 pMultiIndex->pIndex[1].pOctetStrValue,
		 pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return (nmhGetFsNtpsAccessFlag(pMultiIndex->pIndex[0].i4_SLongValue,
								   pMultiIndex->pIndex[1].pOctetStrValue,
								   pMultiIndex->pIndex[2].i4_SLongValue,
								   &(pMultiData->i4_SLongValue)));

}

INT4 FsNtpsAccessRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsNtpsAccessRowStatus
			(pMultiIndex->pIndex[0].i4_SLongValue,
			 pMultiIndex->pIndex[1].pOctetStrValue,
			 pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4 FsNtpsAccessFlagSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsNtpsAccessFlag(pMultiIndex->pIndex[0].i4_SLongValue,
								   pMultiIndex->pIndex[1].pOctetStrValue,
								   pMultiIndex->pIndex[2].i4_SLongValue,
								   pMultiData->i4_SLongValue));

}

INT4 FsNtpsAccessRowStatusTest(UINT4 * pu4Error, tSnmpIndex * pMultiIndex,
							   tRetVal * pMultiData)
{
	return (nmhTestv2FsNtpsAccessRowStatus(pu4Error,
										   pMultiIndex->
										   pIndex[0].i4_SLongValue,
										   pMultiIndex->
										   pIndex[1].pOctetStrValue,
										   pMultiIndex->
										   pIndex[2].i4_SLongValue,
										   pMultiData->i4_SLongValue));

}

INT4 FsNtpsAccessFlagTest(UINT4 * pu4Error, tSnmpIndex * pMultiIndex,
						  tRetVal * pMultiData)
{
	return (nmhTestv2FsNtpsAccessFlag(pu4Error,
									  pMultiIndex->pIndex[0].i4_SLongValue,
									  pMultiIndex->pIndex[1].pOctetStrValue,
									  pMultiIndex->pIndex[2].i4_SLongValue,
									  pMultiData->i4_SLongValue));

}

INT4 FsNtpsAccessTableDep(UINT4 * pu4Error,
						  tSnmpIndexList * pSnmpIndexList,
						  tSNMP_VAR_BIND * pSnmpvarbinds)
{
	return (nmhDepv2FsNtpsAccessTable
			(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsNtpsInterfaceTable(tSnmpIndex * pFirstMultiIndex,
									  tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL)
	{
		if (nmhGetFirstIndexFsNtpsInterfaceTable
			(&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsNtpsInterfaceTable
			(pFirstMultiIndex->pIndex[0].i4_SLongValue,
			 &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}

	return SNMP_SUCCESS;
}

INT4 FsNtpsInterfaceNtpStatusGet(tSnmpIndex * pMultiIndex,
								 tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsNtpsInterfaceTable
		(pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return (nmhGetFsNtpsInterfaceNtpStatus
			(pMultiIndex->pIndex[0].i4_SLongValue,
			 &(pMultiData->i4_SLongValue)));

}

INT4 FsNtpsInterfaceNtpStatusSet(tSnmpIndex * pMultiIndex,
								 tRetVal * pMultiData)
{
	return (nmhSetFsNtpsInterfaceNtpStatus
			(pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4 FsNtpsInterfaceNtpStatusTest(UINT4 * pu4Error,
								  tSnmpIndex * pMultiIndex,
								  tRetVal * pMultiData)
{
	return (nmhTestv2FsNtpsInterfaceNtpStatus(pu4Error,
											  pMultiIndex->
											  pIndex[0].i4_SLongValue,
											  pMultiData->i4_SLongValue));

}

INT4 FsNtpsInterfaceTableDep(UINT4 * pu4Error,
							 tSnmpIndexList * pSnmpIndexList,
							 tSNMP_VAR_BIND * pSnmpvarbinds)
{
	return (nmhDepv2FsNtpsInterfaceTable
			(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
