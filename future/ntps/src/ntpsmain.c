/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: ntpsmain.c, 2016/09/27
 *
 * Description:This file contains the NTP routines.
 *
 * *******************************************************************/
#ifndef _NTPS_MAIN_C
#define _NTPS_MAIN_C
#define NTP_GLOB
#include "ntpsinc.h"
/************************************************************************/
/* Function Name : NtpsMemInit */
/* Description : Allocates the memory for the Ntp server */
/* Input(s) : None */
/* Output(s) : None.  */
/* Returns : OSIX_SUCCESS or OSIX_FAILURE */
/************************************************************************/
INT4 NtpsMemInit(VOID)
{
	/* Memory Pool for ntp server table */
	if (NtpSizingMemCreateMemPools() == OSIX_FAILURE)
	{

		NTP_TRC(NTP_OS_RESOURCE_TRC, "NtpsMemInit creating Mempool failed \n");
		return OSIX_FAILURE;
	}
	gNtpsServerParams.NtpsSerPoolId =
		NTPMemPoolIds[MAX_DEFINED_SERVERS_SIZING_ID];
	gNtpsAccessParams.NtpsAccessPoolId =
		NTPMemPoolIds[MAX_DEFINED_ACCESS_SIZING_ID];
	gNtpsAuthParams.NtpsPoolId = NTPMemPoolIds[MAX_DEFINED_AUTH_SIZING_ID];
	gNtpsIntfParams.NtpsPoolId = NTPMemPoolIds[MAX_DEFINED_INTF_SIZING_ID];
	gNtpsQPoolId = NTPMemPoolIds[MAX_DEFINED_QMSG_SIZING_ID];
	NTP_TRC(NTP_OS_RESOURCE_TRC, "NtpsMemInit creating Mempool SUCCESS \n");
	return OSIX_SUCCESS;
}

/*************************************************************************/
/* Function Name : NtpsMainInit */
/* Description : This procedure is to initialize NTP server */
/* Input(s) : None */
/* Output(s) : None */
/* Returns : NTP_SUCCESS if successful */
/* NTP_FAILURE if failure */
/*************************************************************************/
INT4 NtpsMainInit(VOID)
{
    UINT1 au1TmpBuf[NTP_PATH_LENGTH];
	UINT4 u4Duration = 0;
	INT4 i4TmrStatus = 0;
#ifndef LNXIP4_WANTED
	tNetIpRegInfo RegInfo;
	MEMSET(&RegInfo, 0, sizeof(tNetIpRegInfo));

	RegInfo.u4ContextId = NTP_ZERO;
#endif
	/* Create semaphore for mutual exclusion */
	if (OsixCreateSem(NTP_TASK_SEM, NTP_SEM_COUNT,
					  OSIX_GLOBAL, &gNtpsSemId) != OSIX_SUCCESS)
	{
		NTP_TRC(NTP_OS_RESOURCE_TRC,
				"NtpsMainInit creating semaphore FAILED \n");
		return NTP_FAILURE;
	}
	MEMSET(&gNtpsServerParams, NTP_ZERO, sizeof(tNtpsServerParams));

	TMO_SLL_Init(&(gNtpsAuthParams.NtpsAuthList));
	TMO_SLL_Init(&(gNtpsIntfParams.NtpsIntfList));
	/* NTPS RBTree Initialization */
	if (NtpsServerCreateRBTree() == NTP_FAILURE)
	{
		NTP_TRC(NTP_OS_RESOURCE_TRC,
				"NtpsMainInit creating Server RBTree FAILED \n");
		return NTP_FAILURE;
	}
	if (NtpsAccessCreateRBTree() == NTP_FAILURE)
	{
		NTP_TRC(NTP_OS_RESOURCE_TRC,
				"NtpsMainInit creating Access RBTree FAILED \n");
		return NTP_FAILURE;
	}

	if (NtpsMemInit() == OSIX_FAILURE)
	{
		NTP_TRC(NTP_OS_RESOURCE_TRC,
				"NtpsMainInit Memory initialization failed \n");
		return NTP_FAILURE;
	}

	if (TmrCreateTimerList
		((const UINT1 *)NTP_TASK_NAME, NTPS_TIMER_EXP_EVENT, NULL,
		 &gNTPsTmrListId) == TMR_FAILURE)
	{
		NTP_TRC(NTP_OS_RESOURCE_TRC,
				"NtpsMainInit Unable to crate TIMER components \n");
		return NTP_FAILURE;
	}
    STRNCPY(gNtpGblParams.au1NtpsStartCmd,NTP_START_PRESTR,STRLEN(NTP_START_PRESTR));
    STRNCPY(gNtpGblParams.au1NtpsStopCmd,NTP_START_PRESTR,STRLEN(NTP_START_PRESTR));
    STRNCPY(gNtpGblParams.au1NtpsStatusCmd,NTP_START_PRESTR,STRLEN(NTP_START_PRESTR));
    MEMSET(au1TmpBuf,NTP_ZERO,NTP_PATH_LENGTH);
    IssGetNtpsCommandFromNvRam (au1TmpBuf);
    STRNCAT(gNtpGblParams.au1NtpsStartCmd,au1TmpBuf,STRLEN(au1TmpBuf));
    STRNCAT(gNtpGblParams.au1NtpsStartCmd,NTP_EXE_START_STR,STRLEN(NTP_EXE_START_STR));
    STRNCAT(gNtpGblParams.au1NtpsStopCmd,au1TmpBuf,STRLEN(au1TmpBuf));
    STRNCAT(gNtpGblParams.au1NtpsStopCmd,NTP_EXE_STOP_STR,STRLEN(NTP_EXE_STOP_STR));
    STRNCAT(gNtpGblParams.au1NtpsStatusCmd,au1TmpBuf,STRLEN(au1TmpBuf));
    STRNCAT(gNtpGblParams.au1NtpsStatusCmd,NTP_SERVER_STATUS_STR,STRLEN(NTP_SERVER_STATUS_STR));
    MEMSET(au1TmpBuf,NTP_ZERO,NTP_PATH_LENGTH);
    IssGetNtpsNtpqPathFromNvRam (au1TmpBuf);
    STRNCPY(gNtpGblParams.au1NtpqPath,au1TmpBuf,STRLEN(au1TmpBuf));
    MEMSET(au1TmpBuf,NTP_ZERO,NTP_PATH_LENGTH);
    IssGetNtpsNtpstatPathFromNvRam (au1TmpBuf);
    STRNCPY(gNtpGblParams.au1NtpstatPath,au1TmpBuf,STRLEN(au1TmpBuf));
#ifndef LNXIP4_WANTED
	RegInfo.u1ProtoId = NTPS_PROTOCOL_ID;
	RegInfo.u2InfoMask |= (NETIPV4_IFCHG_REQ);
	RegInfo.pIfStChng = NtpsHandleIfChg;

	if (NTPS_IP_REGISTER_WITH_IP(&RegInfo) == FAILURE)
	{
		NTP_TRC(NTP_OS_RESOURCE_TRC,
				"NtpsMainInit Register with IP failed \n");
		return NTP_FAILURE;
	}
#endif
	u4Duration = NTPS_TIMER_VALUE;
	NTPS_START_TIMER(NTPS_STATUS_TIMER_ID, &(gNtpGblParams.NtpsStatusTmr),
					 u4Duration, i4TmrStatus);
	if (i4TmrStatus == NTP_FAILURE)
	{
		NTP_TRC(NTP_OS_RESOURCE_TRC,
				"NtpsMainInit NTP Status Timer start FAILED\n");
		return NTP_FAILURE;
	}

	NTP_TRC(NTP_OS_RESOURCE_TRC, "NtpsMainInit Initialization SUCCESS \n");
	return NTP_SUCCESS;
}

/*************************************************************************/
/* Function Name : NtpsServerCreateRBTree */
/* Description : This function creates all the RBTree required.  */
/* Input(s) : None */
/* Output(s) : None */
/* Returns : NTP_SUCCESS or NTP_FAILURE */
/*************************************************************************/
INT4 NtpsServerCreateRBTree(VOID)
{
	UINT4 u4RBNodeOffset = 0;
	u4RBNodeOffset = FSAP_OFFSETOF(tNtpsServer, RBSerNode);

	if ((gNtpsServerParams.NtpsServerList =
		 RBTreeCreateEmbedded(u4RBNodeOffset,
							  NtpsServerConfigDBRBCmp)) == NULL)
	{
		NTP_TRC(NTP_OS_RESOURCE_TRC,
				"NtpsServerCreateRBTree creating Server RBTree FAILED \n");
		return NTP_FAILURE;
	}
	NTP_TRC(NTP_OS_RESOURCE_TRC,
			"NtpsServerCreateRBTree creating Server RBTree SUCCESS \n");
	return NTP_SUCCESS;
}

/*************************************************************************/
/* Function Name : NtpsAccessCreateRBTree */
/* Description : This function creates all the RBTree required.  */
/* Input(s) : None */
/* Output(s) : None */
/* Returns : NTP_SUCCESS or NTP_FAILURE */
/*************************************************************************/
INT4 NtpsAccessCreateRBTree(VOID)
{
	UINT4 u4RBNodeOffset = 0;
	u4RBNodeOffset = FSAP_OFFSETOF(tNtpsAccess, RBAccNode);

	if ((gNtpsAccessParams.NtpsAccessList =
		 RBTreeCreateEmbedded(u4RBNodeOffset,
							  NtpsAccessConfigDBRBCmp)) == NULL)
	{
		NTP_TRC(NTP_OS_RESOURCE_TRC,
				"NtpsAccessCreateRBTree creating Access RBTree FAILED \n");
		return NTP_FAILURE;
	}
	NTP_TRC(NTP_OS_RESOURCE_TRC,
			"NtpsAccessCreateRBTree creating Access RBTree SUCCESS \n");
	return NTP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : NtpsServerConfigDBRBCmp */
/* Description : This function compares the two Radio Interface Index */
/* RB Trees */
/* Output(s) : None */
/* Returns : NTP_SUCCESS or NTP_FAILURE */
/*****************************************************************************/

INT4 NtpsServerConfigDBRBCmp(tRBElem * e1, tRBElem * e2)
{
	tNtpsServer *pNode1 = e1;
	tNtpsServer *pNode2 = e2;
	if (pNode1->u4NtpsServerAddrType > pNode2->u4NtpsServerAddrType)
	{
		return 1;
	}
	else if (pNode1->u4NtpsServerAddrType < pNode2->u4NtpsServerAddrType)
	{
		return -1;
	}
	if (MEMCMP
		(pNode1->NtpsServerAddr.au1Addr, pNode2->NtpsServerAddr.au1Addr,
		 pNode1->NtpsServerAddr.u1AddrLen) > 0)
	{
		return 1;
	}
	else if (MEMCMP
			 (pNode1->NtpsServerAddr.au1Addr,
			  pNode2->NtpsServerAddr.au1Addr,
			  pNode1->NtpsServerAddr.u1AddrLen) < 0)
	{
		return -1;
	}
	return NTP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : NtpsAccessConfigDBRBCmp */
/* Description : This function compares the two Access Node */
/* RB Trees */
/* Output(s) : None */
/* Returns : NTP_SUCCESS or NTP_FAILURE */
/*****************************************************************************/

INT4 NtpsAccessConfigDBRBCmp(tRBElem * e1, tRBElem * e2)
{
	tNtpsAccess *pNode1 = e1;
	tNtpsAccess *pNode2 = e2;
	if (pNode1->NtpsAccessIp.u1Afi > pNode2->NtpsAccessIp.u1Afi)
	{
		return 1;
	}
	else if (pNode1->NtpsAccessIp.u1Afi < pNode2->NtpsAccessIp.u1Afi)
	{
		return -1;
	}
	if (MEMCMP(pNode1->NtpsAccessIp.au1Addr, pNode2->NtpsAccessIp.au1Addr,
			   pNode1->NtpsAccessIp.u1AddrLen) > 0)
	{
		return 1;
	}
	else if (MEMCMP
			 (pNode1->NtpsAccessIp.au1Addr, pNode2->NtpsAccessIp.au1Addr,
			  pNode1->NtpsAccessIp.u1AddrLen) < 0)
	{
		return -1;
	}
	if (pNode1->u2NtpsAccessIpMask > pNode2->u2NtpsAccessIpMask)
	{
		return 1;
	}
	else if (pNode1->u2NtpsAccessIpMask < pNode2->u2NtpsAccessIpMask)
	{
		return -1;
	}
	return NTP_SUCCESS;
}

/*****************************************************************************/
/* Function : NtpsServerProcessDBMsg */
/* Description : Used to invoke the coreesponding DB to perform set or get */
/* operation.  */
/* Input : Opcode and Pointer to NtpsServerDB */
/* Output : None */
/* Returns : NTP_SUCCESS or NTP_FAILURE */
/*****************************************************************************/

UINT1 NtpsServerDB(UINT1 u1Opcode, tNtpsServer * pNtpsServerDB)
{
	tNtpsServer *pNtpsServerProcessDB = NULL;
	switch (u1Opcode)
	{
	case NTPS_CREATE_SERVER_TABLE_ENTRY:
		/* Memory Allocation for the node to be added in the tree */

		if ((pNtpsServerProcessDB = (tNtpsServer *) MemAllocMemBlk
			 (NTPS_SERVER_DB_POOLID)) == NULL)
		{
			NTP_TRC(NTP_OS_RESOURCE_TRC,
					"NtpsServerDB Server DB Memory allocation - FAILED \n");
			return NTP_FAILURE;
		}
		MEMSET(pNtpsServerProcessDB, 0, sizeof(tNtpsServer));
		pNtpsServerProcessDB->u4NtpsServerAddrType =
			pNtpsServerDB->u4NtpsServerAddrType;

		pNtpsServerProcessDB->u4NtpsServerVersion =
			pNtpsServerDB->u4NtpsServerVersion;
		pNtpsServerProcessDB->u4NtpsServerKeyId =
			pNtpsServerDB->u4NtpsServerKeyId;
		pNtpsServerProcessDB->u4NtpsServerBurstMode =
			pNtpsServerDB->u4NtpsServerBurstMode;
		MEMCPY(pNtpsServerProcessDB->NtpsServerAddr.au1Addr,
			   pNtpsServerDB->NtpsServerAddr.au1Addr,
			   pNtpsServerDB->NtpsServerAddr.u1AddrLen);
		pNtpsServerProcessDB->NtpsServerAddr.u1AddrLen =
			pNtpsServerDB->NtpsServerAddr.u1AddrLen;
		pNtpsServerProcessDB->NtpsServerAddr.u1Afi =
			pNtpsServerDB->NtpsServerAddr.u1Afi;
		pNtpsServerProcessDB->NtpsServerAddr.u2Pad =
			pNtpsServerDB->NtpsServerAddr.u2Pad;
		pNtpsServerProcessDB->u4NtpsServerMinPollInt =
			pNtpsServerDB->u4NtpsServerMinPollInt;
		pNtpsServerProcessDB->u4NtpsServerMaxPollInt =
			pNtpsServerDB->u4NtpsServerMaxPollInt;
		pNtpsServerProcessDB->i4NtpsServerPrefer =
			pNtpsServerDB->i4NtpsServerPrefer;
		pNtpsServerProcessDB->u4NtpsServerRowStatus =
			pNtpsServerDB->u4NtpsServerRowStatus;
		/* Add the node to the RBTree */
		if (RBTreeAdd
			(gNtpsServerParams.NtpsServerList,
			 (tRBElem *) pNtpsServerProcessDB) != RB_SUCCESS)
		{
			MemReleaseMemBlock(NTPS_SERVER_DB_POOLID,
							   (UINT1 *) pNtpsServerProcessDB);
			NTP_TRC(NTP_OS_RESOURCE_TRC,
					"NtpsServerDB Server DB Memory de-allocation - FAILED \n");
			return NTP_FAILURE;
		}
		break;
	case NTPS_DESTROY_SERVER_TABLE_ENTRY:
		pNtpsServerProcessDB = RBTreeGet(gNtpsServerParams.NtpsServerList,
										 (tRBElem *) pNtpsServerDB);
		if (pNtpsServerProcessDB == NULL)
		{
			NTP_TRC(NTP_OS_RESOURCE_TRC,
					"NtpsServerDB Server DB RBTree GET - FAILED \n");
			/* If not present, return error */
			return NTP_FAILURE;
		}
		RBTreeRem(gNtpsServerParams.NtpsServerList, pNtpsServerProcessDB);
		MemReleaseMemBlock(NTPS_SERVER_DB_POOLID,
						   (UINT1 *) pNtpsServerProcessDB);
		pNtpsServerProcessDB = NULL;
		break;
	case NTPS_SET_SERVER_TABLE_ENTRY:
		pNtpsServerProcessDB = RBTreeGet(gNtpsServerParams.NtpsServerList,
										 (tRBElem *) pNtpsServerDB);
		if (pNtpsServerProcessDB == NULL)
		{
			NTP_TRC(NTP_OS_RESOURCE_TRC,
					"NtpsServerDB Server DB RBTree GET - FAILED \n");
			/* If not present, return error */
			return NTP_FAILURE;
		}
        pNtpsServerProcessDB->u4NtpsServerAddrType =
            pNtpsServerDB->u4NtpsServerAddrType;        
		pNtpsServerProcessDB->u4NtpsServerVersion =
			pNtpsServerDB->u4NtpsServerVersion;
		pNtpsServerProcessDB->u4NtpsServerKeyId =
			pNtpsServerDB->u4NtpsServerKeyId;
		pNtpsServerProcessDB->u4NtpsServerBurstMode =
			pNtpsServerDB->u4NtpsServerBurstMode;
		MEMCPY(pNtpsServerProcessDB->NtpsServerAddr.au1Addr,
			   pNtpsServerDB->NtpsServerAddr.au1Addr,
			   pNtpsServerDB->NtpsServerAddr.u1AddrLen);
		pNtpsServerProcessDB->NtpsServerAddr.u1AddrLen =
			pNtpsServerDB->NtpsServerAddr.u1AddrLen;
		pNtpsServerProcessDB->NtpsServerAddr.u1Afi =
			pNtpsServerDB->NtpsServerAddr.u1Afi;
		pNtpsServerProcessDB->NtpsServerAddr.u2Pad =
			pNtpsServerDB->NtpsServerAddr.u2Pad;

		pNtpsServerProcessDB->u4NtpsServerMinPollInt =
			pNtpsServerDB->u4NtpsServerMinPollInt;
		pNtpsServerProcessDB->u4NtpsServerMaxPollInt =
			pNtpsServerDB->u4NtpsServerMaxPollInt;
		pNtpsServerProcessDB->i4NtpsServerPrefer =
			pNtpsServerDB->i4NtpsServerPrefer;
		pNtpsServerProcessDB->u4NtpsServerRowStatus =
			pNtpsServerDB->u4NtpsServerRowStatus;

		break;
	case NTPS_GET_SERVER_TABLE_ENTRY:
		pNtpsServerProcessDB = RBTreeGet(gNtpsServerParams.NtpsServerList,
										 (tRBElem *) pNtpsServerDB);
		if (pNtpsServerProcessDB == NULL)
		{
			NTP_TRC(NTP_OS_RESOURCE_TRC,
					"NtpsServerDB Server DB RBTree GET - FAILED \n");
			/* If not present, return error */
			return NTP_FAILURE;
		}
		pNtpsServerDB->u4NtpsServerAddrType =
			pNtpsServerProcessDB->u4NtpsServerAddrType;
		pNtpsServerDB->u4NtpsServerVersion =
			pNtpsServerProcessDB->u4NtpsServerVersion;
		pNtpsServerDB->u4NtpsServerKeyId =
			pNtpsServerProcessDB->u4NtpsServerKeyId;
		pNtpsServerDB->u4NtpsServerBurstMode =
			pNtpsServerProcessDB->u4NtpsServerBurstMode;
		MEMCPY(pNtpsServerDB->NtpsServerAddr.au1Addr,
			   pNtpsServerProcessDB->NtpsServerAddr.au1Addr,
			   pNtpsServerProcessDB->NtpsServerAddr.u1AddrLen);
		pNtpsServerDB->NtpsServerAddr.u1AddrLen =
			pNtpsServerProcessDB->NtpsServerAddr.u1AddrLen;
		pNtpsServerDB->NtpsServerAddr.u1Afi =
			pNtpsServerProcessDB->NtpsServerAddr.u1Afi;
		pNtpsServerDB->NtpsServerAddr.u2Pad =
			pNtpsServerProcessDB->NtpsServerAddr.u2Pad;
		pNtpsServerDB->u4NtpsServerMinPollInt =
			pNtpsServerProcessDB->u4NtpsServerMinPollInt;
		pNtpsServerDB->u4NtpsServerMaxPollInt =
			pNtpsServerProcessDB->u4NtpsServerMaxPollInt;
		pNtpsServerDB->i4NtpsServerPrefer =
			pNtpsServerProcessDB->i4NtpsServerPrefer;
		pNtpsServerDB->u4NtpsServerRowStatus =
			pNtpsServerProcessDB->u4NtpsServerRowStatus;
		break;
	case NTPS_GET_FIRST_SERVER_TABLE_ENTRY:
		pNtpsServerProcessDB =
			RBTreeGetFirst(gNtpsServerParams.NtpsServerList);
		if (pNtpsServerProcessDB == NULL)
		{
			NTP_TRC(NTP_OS_RESOURCE_TRC,
					"NtpsServerDB Server DB RBTree GET First - FAILED \n");
			/* If not present, return error */
			return NTP_FAILURE;
		}
		pNtpsServerDB->u4NtpsServerAddrType =
			pNtpsServerProcessDB->u4NtpsServerAddrType;
		pNtpsServerDB->u4NtpsServerVersion =
			pNtpsServerProcessDB->u4NtpsServerVersion;
		pNtpsServerDB->u4NtpsServerKeyId =
			pNtpsServerProcessDB->u4NtpsServerKeyId;
		pNtpsServerDB->u4NtpsServerBurstMode =
			pNtpsServerProcessDB->u4NtpsServerBurstMode;
		MEMCPY(pNtpsServerDB->NtpsServerAddr.au1Addr,
			   pNtpsServerProcessDB->NtpsServerAddr.au1Addr,
			   pNtpsServerProcessDB->NtpsServerAddr.u1AddrLen);
		pNtpsServerDB->NtpsServerAddr.u1AddrLen =
			pNtpsServerProcessDB->NtpsServerAddr.u1AddrLen;
		pNtpsServerDB->NtpsServerAddr.u1Afi =
			pNtpsServerProcessDB->NtpsServerAddr.u1Afi;
		pNtpsServerDB->NtpsServerAddr.u2Pad =
			pNtpsServerProcessDB->NtpsServerAddr.u2Pad;
		pNtpsServerDB->u4NtpsServerMinPollInt =
			pNtpsServerProcessDB->u4NtpsServerMinPollInt;
		pNtpsServerDB->u4NtpsServerMaxPollInt =
			pNtpsServerProcessDB->u4NtpsServerMaxPollInt;
		pNtpsServerDB->i4NtpsServerPrefer =
			pNtpsServerProcessDB->i4NtpsServerPrefer;
		pNtpsServerDB->u4NtpsServerRowStatus =
			pNtpsServerProcessDB->u4NtpsServerRowStatus;

		break;
	case NTPS_GET_NEXT_SERVER_TABLE_ENTRY:
		pNtpsServerProcessDB =
			RBTreeGetNext(gNtpsServerParams.NtpsServerList,
						  (tRBElem *) pNtpsServerDB, NULL);
		if (pNtpsServerProcessDB == NULL)
		{
			NTP_TRC(NTP_OS_RESOURCE_TRC,
					"NtpsServerDB Server DB RBTree GET NEXT - FAILED \n");
			/* If not present, return error */
			return NTP_FAILURE;
		}
        pNtpsServerDB->u4NtpsServerAddrType =
            pNtpsServerProcessDB->u4NtpsServerAddrType;
		MEMCPY(pNtpsServerDB->NtpsServerAddr.au1Addr,
			   pNtpsServerProcessDB->NtpsServerAddr.au1Addr,
			   pNtpsServerProcessDB->NtpsServerAddr.u1AddrLen);
		pNtpsServerDB->NtpsServerAddr.u1AddrLen =
			pNtpsServerProcessDB->NtpsServerAddr.u1AddrLen;
		pNtpsServerDB->NtpsServerAddr.u1Afi =
			pNtpsServerProcessDB->NtpsServerAddr.u1Afi;
		pNtpsServerDB->NtpsServerAddr.u2Pad =
			pNtpsServerProcessDB->NtpsServerAddr.u2Pad;

		pNtpsServerDB->u4NtpsServerVersion =
			pNtpsServerProcessDB->u4NtpsServerVersion;
		pNtpsServerDB->u4NtpsServerKeyId =
			pNtpsServerProcessDB->u4NtpsServerKeyId;
		pNtpsServerDB->u4NtpsServerBurstMode =
			pNtpsServerProcessDB->u4NtpsServerBurstMode;
		pNtpsServerDB->u4NtpsServerMinPollInt =
			pNtpsServerProcessDB->u4NtpsServerMinPollInt;
		pNtpsServerDB->u4NtpsServerMaxPollInt =
			pNtpsServerProcessDB->u4NtpsServerMaxPollInt;
		pNtpsServerDB->i4NtpsServerPrefer =
			pNtpsServerProcessDB->i4NtpsServerPrefer;
		pNtpsServerDB->u4NtpsServerRowStatus =
			pNtpsServerProcessDB->u4NtpsServerRowStatus;

		break;
	default:
		NTP_TRC(NTP_OS_RESOURCE_TRC,
				"NtpsServerDB Server DB Invalid Operation - FAILED \n");
		return NTP_FAILURE;
	}
	NTP_TRC(NTP_OS_RESOURCE_TRC,
			"NtpsServerDB Server DB Operation - SUCCESS \n");
	return NTP_SUCCESS;
}

/*****************************************************************************/
/* Function : NtpsAccessProcessDBMsg */
/* Description : Used to invoke the coreesponding DB to perform set or get */
/* operation.  */
/* Input : Opcode and Pointer to NtpsAccessDB */
/* Output : None */
/* Returns : NTP_SUCCESS or NTP_FAILURE */
/*****************************************************************************/

UINT1 NtpsAccessDB(UINT1 u1Opcode, tNtpsAccess * pNtpsAccessDB)
{
	tNtpsAccess *pNtpsAccessProcessDB = NULL;
	switch (u1Opcode)
	{
	case NTPS_CREATE_ACCESS_TABLE_ENTRY:
		/* Memory Allocation for the node to be added in the tree */

		if ((pNtpsAccessProcessDB = (tNtpsAccess *) MemAllocMemBlk
			 (NTPS_ACCESS_DB_POOLID)) == NULL)
		{
			NTP_TRC(NTP_OS_RESOURCE_TRC,
					"NtpsAccessDB Access DB Memory allocation - FAILED \n");
			return NTP_FAILURE;
		}
		MEMSET(pNtpsAccessProcessDB, 0, sizeof(tNtpsAccess));
		pNtpsAccessProcessDB->u2NtpsAccessFlag =
			pNtpsAccessDB->u2NtpsAccessFlag;

		MEMCPY(pNtpsAccessProcessDB->NtpsAccessIp.au1Addr,
			   pNtpsAccessDB->NtpsAccessIp.au1Addr,
			   pNtpsAccessDB->NtpsAccessIp.u1AddrLen);
		pNtpsAccessProcessDB->NtpsAccessIp.u1AddrLen =
			pNtpsAccessDB->NtpsAccessIp.u1AddrLen;
		pNtpsAccessProcessDB->NtpsAccessIp.u1Afi =
			pNtpsAccessDB->NtpsAccessIp.u1Afi;
		pNtpsAccessProcessDB->NtpsAccessIp.u2Pad =
			pNtpsAccessDB->NtpsAccessIp.u2Pad;
		pNtpsAccessProcessDB->u2NtpsAccessIpMask =
			pNtpsAccessDB->u2NtpsAccessIpMask;
		pNtpsAccessProcessDB->u2NtpsAccessRowStatus =
			pNtpsAccessDB->u2NtpsAccessRowStatus;
		pNtpsAccessProcessDB->u2NtpsAccessFlag =
			pNtpsAccessDB->u2NtpsAccessFlag;
		/* Add the node to the RBTree */
		if (RBTreeAdd
			(gNtpsAccessParams.NtpsAccessList,
			 (tRBElem *) pNtpsAccessProcessDB) != RB_SUCCESS)
		{
			MemReleaseMemBlock(NTPS_ACCESS_DB_POOLID,
							   (UINT1 *) pNtpsAccessProcessDB);
			NTP_TRC(NTP_OS_RESOURCE_TRC,
					"NtpsAccessDB Access DB Memory de-allocation - FAILED \n");
			return NTP_FAILURE;
		}
		break;
	case NTPS_DESTROY_ACCESS_TABLE_ENTRY:
		pNtpsAccessProcessDB = RBTreeGet(gNtpsAccessParams.NtpsAccessList,
										 (tRBElem *) pNtpsAccessDB);
		if (pNtpsAccessProcessDB == NULL)
		{
			NTP_TRC(NTP_OS_RESOURCE_TRC,
					"NtpsAccessDB Access DB RBTree GET - FAILED \n");
			/* If not present, return error */
			return NTP_FAILURE;
		}
		RBTreeRem(gNtpsAccessParams.NtpsAccessList, pNtpsAccessProcessDB);
		MemReleaseMemBlock(NTPS_ACCESS_DB_POOLID,
						   (UINT1 *) pNtpsAccessProcessDB);
		pNtpsAccessProcessDB = NULL;
		break;
	case NTPS_SET_ACCESS_TABLE_ENTRY:
		pNtpsAccessProcessDB = RBTreeGet(gNtpsAccessParams.NtpsAccessList,
										 (tRBElem *) pNtpsAccessDB);
		if (pNtpsAccessProcessDB == NULL)
		{
			NTP_TRC(NTP_OS_RESOURCE_TRC,
					"NtpsAccessDB Access DB RBTree GET - FAILED \n");
			/* If not present, return error */
			return NTP_FAILURE;
		}
		pNtpsAccessProcessDB->u2NtpsAccessFlag =
			pNtpsAccessDB->u2NtpsAccessFlag;

		MEMCPY(pNtpsAccessProcessDB->NtpsAccessIp.au1Addr,
			   pNtpsAccessDB->NtpsAccessIp.au1Addr,
			   pNtpsAccessDB->NtpsAccessIp.u1AddrLen);
		pNtpsAccessProcessDB->NtpsAccessIp.u1AddrLen =
			pNtpsAccessDB->NtpsAccessIp.u1AddrLen;
		pNtpsAccessProcessDB->NtpsAccessIp.u1Afi =
			pNtpsAccessDB->NtpsAccessIp.u1Afi;
		pNtpsAccessProcessDB->NtpsAccessIp.u2Pad =
			pNtpsAccessDB->NtpsAccessIp.u2Pad;
		pNtpsAccessProcessDB->u2NtpsAccessIpMask =
			pNtpsAccessDB->u2NtpsAccessIpMask;
		pNtpsAccessProcessDB->u2NtpsAccessRowStatus =
			pNtpsAccessDB->u2NtpsAccessRowStatus;
		pNtpsAccessProcessDB->u2NtpsAccessFlag =
			pNtpsAccessDB->u2NtpsAccessFlag;

		break;
	case NTPS_GET_ACCESS_TABLE_ENTRY:
		pNtpsAccessProcessDB = RBTreeGet(gNtpsAccessParams.NtpsAccessList,
										 (tRBElem *) pNtpsAccessDB);
		if (pNtpsAccessProcessDB == NULL)
		{
			NTP_TRC(NTP_OS_RESOURCE_TRC,
					"NtpsAccessDB Access DB RBTree GET - FAILED \n");
			/* If not present, return error */
			return NTP_FAILURE;
		}
		pNtpsAccessDB->u2NtpsAccessFlag =
			pNtpsAccessProcessDB->u2NtpsAccessFlag;

		MEMCPY(pNtpsAccessDB->NtpsAccessIp.au1Addr,
			   pNtpsAccessProcessDB->NtpsAccessIp.au1Addr,
			   pNtpsAccessProcessDB->NtpsAccessIp.u1AddrLen);
		pNtpsAccessDB->NtpsAccessIp.u1AddrLen =
			pNtpsAccessProcessDB->NtpsAccessIp.u1AddrLen;
		pNtpsAccessDB->NtpsAccessIp.u1Afi =
			pNtpsAccessProcessDB->NtpsAccessIp.u1Afi;
		pNtpsAccessDB->NtpsAccessIp.u2Pad =
			pNtpsAccessProcessDB->NtpsAccessIp.u2Pad;
		pNtpsAccessDB->u2NtpsAccessIpMask =
			pNtpsAccessProcessDB->u2NtpsAccessIpMask;
		pNtpsAccessDB->u2NtpsAccessRowStatus =
			pNtpsAccessProcessDB->u2NtpsAccessRowStatus;
		pNtpsAccessDB->u2NtpsAccessFlag =
			pNtpsAccessProcessDB->u2NtpsAccessFlag;


		break;
	case NTPS_GET_FIRST_ACCESS_TABLE_ENTRY:
		pNtpsAccessProcessDB =
			RBTreeGetFirst(gNtpsAccessParams.NtpsAccessList);
		if (pNtpsAccessProcessDB == NULL)
		{
			NTP_TRC(NTP_OS_RESOURCE_TRC,
					"NtpsAccessDB Access DB RBTree GET FIRST - FAILED \n");
			/* If not present, return error */
			return NTP_FAILURE;
		}
		pNtpsAccessDB->u2NtpsAccessFlag =
			pNtpsAccessProcessDB->u2NtpsAccessFlag;

		MEMCPY(pNtpsAccessDB->NtpsAccessIp.au1Addr,
			   pNtpsAccessProcessDB->NtpsAccessIp.au1Addr,
			   pNtpsAccessProcessDB->NtpsAccessIp.u1AddrLen);
		pNtpsAccessDB->NtpsAccessIp.u1AddrLen =
			pNtpsAccessProcessDB->NtpsAccessIp.u1AddrLen;
		pNtpsAccessDB->NtpsAccessIp.u1Afi =
			pNtpsAccessProcessDB->NtpsAccessIp.u1Afi;
		pNtpsAccessDB->NtpsAccessIp.u2Pad =
			pNtpsAccessProcessDB->NtpsAccessIp.u2Pad;
		pNtpsAccessDB->u2NtpsAccessIpMask =
			pNtpsAccessProcessDB->u2NtpsAccessIpMask;
		pNtpsAccessDB->u2NtpsAccessRowStatus =
			pNtpsAccessProcessDB->u2NtpsAccessRowStatus;
		pNtpsAccessDB->u2NtpsAccessFlag =
			pNtpsAccessProcessDB->u2NtpsAccessFlag;

		break;
	case NTPS_GET_NEXT_ACCESS_TABLE_ENTRY:
		pNtpsAccessProcessDB =
			RBTreeGetNext(gNtpsAccessParams.NtpsAccessList,
						  (tRBElem *) pNtpsAccessDB, NULL);
		if (pNtpsAccessProcessDB == NULL)
		{
			NTP_TRC(NTP_OS_RESOURCE_TRC,
					"NtpsAccessDB Access DB RBTree GET NEXT - FAILED \n");
			/* If not present, return error */
			return NTP_FAILURE;
		}
		pNtpsAccessDB->u2NtpsAccessFlag =
			pNtpsAccessProcessDB->u2NtpsAccessFlag;

		MEMCPY(pNtpsAccessDB->NtpsAccessIp.au1Addr,
			   pNtpsAccessProcessDB->NtpsAccessIp.au1Addr,
			   pNtpsAccessProcessDB->NtpsAccessIp.u1AddrLen);
		pNtpsAccessDB->NtpsAccessIp.u1AddrLen =
			pNtpsAccessProcessDB->NtpsAccessIp.u1AddrLen;
		pNtpsAccessDB->NtpsAccessIp.u1Afi =
			pNtpsAccessProcessDB->NtpsAccessIp.u1Afi;
		pNtpsAccessDB->NtpsAccessIp.u2Pad =
			pNtpsAccessProcessDB->NtpsAccessIp.u2Pad;
		pNtpsAccessDB->u2NtpsAccessIpMask =
			pNtpsAccessProcessDB->u2NtpsAccessIpMask;
		pNtpsAccessDB->u2NtpsAccessRowStatus =
			pNtpsAccessProcessDB->u2NtpsAccessRowStatus;
		pNtpsAccessDB->u2NtpsAccessFlag =
			pNtpsAccessProcessDB->u2NtpsAccessFlag;

		break;
	default:
		NTP_TRC(NTP_OS_RESOURCE_TRC,
				"NtpsAccessDB Access DB Invalid Operation - FAILED \n");
		return NTP_FAILURE;
	}
	NTP_TRC(NTP_OS_RESOURCE_TRC,
			"NtpsAccessDB Access DB Operation - SUCCESS \n");
	return NTP_SUCCESS;
}


/*************************************************************************/
/* Function Name : NtpsMain */
/* Description : This is the main funtion of NTPS server module.  */
/* It initialises ntps server and waits in */
/* a infinite loop for various events and messages.  */
/* Input(s) : pParam - UNUSED */
/* Output(s) : None */
/* Returns : void */
/*************************************************************************/
VOID NtpsMain(INT1 * pParam)
{
	UNUSED_PARAM(pParam);
	UINT4 u4EventReceived;
	tNtpsTmrNode *pTimer = NULL;

	if (OsixTskIdSelf(&gu4NtpsTaskId) != OSIX_SUCCESS)
	{
		NTP_TRC(NTP_OS_RESOURCE_TRC, "NtpsMain Task ID SET - FAILED \n");
		NTP_INIT_COMPLETE(OSIX_FAILURE);
		return;
	}

	if (NtpsMainInit() == NTP_FAILURE)
	{
		NTP_TRC(NTP_OS_RESOURCE_TRC, "NtpsMain Initialization - FAILED \n");
		NTP_INIT_COMPLETE(OSIX_FAILURE);
		return;
	}
#ifdef SNMP_2_WANTED
	RegisterFSNTPS();
#endif

	if (OsixQueCrt((UINT1 *) NTPQ, OSIX_MAX_Q_MSG_LEN,
				   NTPS_MAX_Q_DEPTH, &gu4NtpsQId) != OSIX_SUCCESS)
	{
		OsixTskDel(gu4NtpsTaskId);
		/* Indicate the status of initialization to the main routine */
		NTP_INIT_COMPLETE(OSIX_FAILURE);
		NTP_TRC(NTP_OS_RESOURCE_TRC, "NtpsMain Queue Create - FAILED \n");
		return;
	}

	NTP_INIT_COMPLETE(OSIX_SUCCESS);
	while (NTP_ONE)
	{
		/* Wait and receive for the event */
		OsixEvtRecv(gu4NtpsTaskId, (NTPS_INPUT_Q_EVENT | NTPS_TIMER_EXP_EVENT),
					NTPS_EVENT_WAIT_FLAG, &u4EventReceived);
		if (u4EventReceived & NTPS_INPUT_Q_EVENT)
		{
			NtpsLock();
			NtpsProcessQMsg();
			NtpsUnLock();
		}
		if (u4EventReceived & NTPS_TIMER_EXP_EVENT)
		{
			NtpsLock();
			while ((pTimer = ((tNtpsTmrNode *)
							  TmrGetNextExpiredTimer(gNTPsTmrListId))) != NULL)
			{
				NtpsStatusTmrExpHdlr(pTimer);
			}
			NtpsUnLock();
		}

	}
}

/***************************************************************************
 * Function Name    :  NtpsProcessQMsg
 * Description      :  This function process the messages received for
 *                     the NTP protocol Queue 
 * Input (s)        :  None  
 * Output (s)       :  None
 * Returns          :  None
 ****************************************************************************/
VOID NtpsProcessQMsg(VOID)
{
	tNtpsQMsg *pQMsg = NULL;
	tCRU_BUF_CHAIN_HEADER *pBuffer;
	UINT1 *pChgData = NULL;
	UINT1 u1MsgType = 0;
	tNtpsIfStatusInfo *pIfStatChgInfo = NULL;
	UINT4 u4IfIndex = 0;
	tNtpsIntf *pNtpsIntf = NULL;

	while ((OsixQueRecv(gu4NtpsQId, (UINT1 *) & pQMsg,
						OSIX_DEF_MSG_LEN, OSIX_NO_WAIT)) == OSIX_SUCCESS)
	{
		if (pQMsg == NULL)
		{
			return;
		}

		switch (pQMsg->u4MsgType)
		{
		case NTPS_IF_CHG_EVENT:
			pBuffer = pQMsg->NtpsIfParam;
			pChgData =
				CRU_BUF_Get_DataPtr_IfLinear(pBuffer, NTP_ZERO, NTP_ONE);

			/* Get the message type i.e Ucast Rt change or If status change */
			if (pChgData != NULL)
			{
				u1MsgType = *pChgData;
			}
			else
			{
				return;
			}
			pIfStatChgInfo = (tNtpsIfStatusInfo *) (VOID *) pChgData;
			u4IfIndex = pIfStatChgInfo->u4IfIndex;
			if (u1MsgType == NTPS_IP_DESTROY)
			{
				pNtpsIntf = NtpGetIntfNode(u4IfIndex);
				if (pNtpsIntf != NULL)
				{
					NtpDeleteIntfNode(pNtpsIntf);
				}
			}
			CRU_BUF_Release_MsgBufChain(pQMsg->NtpsIfParam, FALSE);
			break;
		default:
			break;
		}
		MemReleaseMemBlock(gNtpsQPoolId, (UINT1 *) pQMsg);

	}
}

/*****************************************************************************/
/* Function Name : NtpsLock */
/* Description : This function is used to take the Ntps mutual */
/* exclusion SEMa4 to avoid simultaneous access to */
/* protocol data structures by the protocol task and */
/* configuration task/thread.  */
/* Input(s) : None */
/* Global Variables */
/* Return Value(s) : SNMP_SUCCESS or SNMP_FAILURE */
/*****************************************************************************/
INT4 NtpsLock(VOID)
{
	if (OsixSemTake(gNtpsSemId) != OSIX_SUCCESS)
	{
		NTP_TRC(NTP_OS_RESOURCE_TRC, "NtpsLock Sem Take - FAILED \n");
		return SNMP_FAILURE;
	}
	return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : NtpsUnLock */
/* Description : This function is used to give the Ntps mutual */
/* exclusion SEMa4 to avoid simultaneous access to */
/* protocol data structures by the protocol task and */
/* configuration task/thread.  */
/* Input(s) : None */
/* Output(s) : None */
/* Return Value(s) : SNMP_SUCCESS or SNMP_FAILURE */
/*****************************************************************************/
INT4 NtpsUnLock(VOID)
{
	OsixSemGive(gNtpsSemId);
	return SNMP_SUCCESS;
}

/***************************************************************************
* Function Name           : NtpsStatusTmrExpHdlr                           
* Description             : Describes the action to be taken when the    
*                        STATUS timer expires. ie                      
*                        Check the status of the NTPd and create a log.
* Input (s)               : pTmrNode - Expired timer node                
* Output (s)              : None                                         
* Global Variables Modified : None                                         
* Returns                : None                                         
****************************************************************************/

VOID NtpsStatusTmrExpHdlr(tNtpsTmrNode * pTmrNode)
{
	UINT4 u4Duration = 0;
	INT4 i4TmrStatus = 0;
	INT4 i4RetVal = 0;
	UINT1 au1BufStatus[NTPS_TEMP_BUF];
	UNUSED_PARAM(pTmrNode);
	NTP_TRC(NTP_OS_RESOURCE_TRC, "NtpsStatusTmrExpHdlr - Invoked \n");
	SNPRINTF((CHR1 *) au1BufStatus, sizeof(au1BufStatus), "%s",
			 NTP_NTPD_STATUS);
	
    if ((gu4NtpServerExecStatus == NTP_SERVER_START) ||
                    (gu4NtpServerExecStatus == NTP_SERVER_RESTART))
    {
	if (NtpsExecuteNTPCommand(au1BufStatus, &i4RetVal) == NTP_FAILURE)
	{
		NTP_TRC(NTP_OS_RESOURCE_TRC,
				"NtpsSTatusTmrExpHdlr failed to fetch NTP daemon status \n");
		return;
	}

		if (i4RetVal == NTP_STATUS_FAIL)
		{
			SYSLOG_NTP_MSG(SYSLOG_ALERT_LEVEL, NTP_OS_RESOURCE_TRC,
						   "NTP Server: NTP Daemon NOT Running \n");
		}
		else if (i4RetVal == NTP_SUCCESS)
		{
			SYSLOG_NTP_MSG(SYSLOG_INFO_LEVEL, NTP_OS_RESOURCE_TRC,
						   "NTP Server: NTP Daemon Running Successfully\n");
		}
	}

	u4Duration = NTPS_TIMER_VALUE;
	gNtpGblParams.NtpsStatusTmr.u1TmrStatus = NTPS_TIMER_FLAG_RESET;
	NTPS_START_TIMER(NTPS_STATUS_TIMER_ID, &(gNtpGblParams.NtpsStatusTmr),
					 u4Duration, i4TmrStatus);
	if (i4TmrStatus == NTP_FAILURE)
	{
		NTP_TRC(NTP_OS_RESOURCE_TRC,
				"NtpsStatusTmrExpHdlr NTP Status Timer start FAILED\n");
		return;
	}
	return;
}
#endif /*_NTPS_MAIN_C*/
