/********************************************************************
 * Copyright (C) 2016 Aricent Inc . All Rights Reserved
 *
 * $Id: ntpsport.c,v 1.1 2017/04/03 15:16:42 siva Exp $
 *
 * Description:This file holds the functions for APIs invoked from external modules 
 *
 *******************************************************************/
#ifndef _NTPS_PORT_C
#define _NTPS_PORT_C
#include "ntpsinc.h"
/************************************************************************/
/* Function Name : NtpsHandleIfChg */
/* Description : Handles Interface state change message received from IP
   module */
/* Input(s) : Interface Info, Notification type */
/* Output(s) : None.  */
/* Returns : None */
/************************************************************************/
VOID NtpsHandleIfChg(tNetIpv4IfInfo * pNetIfInfo, UINT4 u4BitMap)
{
	tNtpsIfStatusInfo IfStatusInfo;
	tCRU_BUF_CHAIN_HEADER *pBuffer = NULL;
	tNtpsQMsg *pQMsg = NULL;
	UINT1 *pu1MemAlloc = NULL;

	if (u4BitMap == NTPS_IP_DESTROY)
	{
		IfStatusInfo.u1MsgType = NTPS_IP_DESTROY;
	}
	else
	{
		NTP_TRC_ARG1(NTP_CONTROL_PATH_TRC,
					 "NtpsHandleIfChg Received Unwanted event %u from IP \n",
					 u4BitMap);
		return;
	}

	IfStatusInfo.u1IfStatus = (UINT1) pNetIfInfo->u4Oper;
	IfStatusInfo.u4IfIndex = pNetIfInfo->u4CfaIfIndex;
	/* Allocate buffer to pass the information to the URM Q */
	pBuffer = CRU_BUF_Allocate_MsgBufChain(sizeof(tNtpsIfStatusInfo),
										   NTP_ZERO);

	/* Allocation Fails */
	if (pBuffer == NULL)
	{
		NTP_TRC(NTP_CONTROL_PATH_TRC,
				"NtpsHandleIfChg CRU Buffer allocation Failed \n");
		return;
	}
	MEMSET(pBuffer->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
	CRU_BUF_UPDATE_MODULE_INFO(pBuffer, "NtpsIfChg");
	/* Copy the message to the CRU buffer */
	if (CRU_BUF_Copy_OverBufChain
		(pBuffer, (UINT1 *) & IfStatusInfo, NTP_ZERO,
		 sizeof(tNtpsIfStatusInfo)) == CRU_FAILURE)
	{
		/* Free the CRU buffer */
		CRU_BUF_Release_MsgBufChain(pBuffer, FALSE);
		NTP_TRC(NTP_CONTROL_PATH_TRC,
				"NtpsHandleIfChg CRU Buffer copy Failed \n");
		return;
	}

	if ((pu1MemAlloc = (MemAllocMemBlk(gNtpsQPoolId))) == NULL)
	{
		/* Free the CRU buffer */
		CRU_BUF_Release_MsgBufChain(pBuffer, FALSE);
		NTP_TRC(NTP_CONTROL_PATH_TRC,
				"NtpsHandleIfChg CRU Buffer Release Failed \n");
		return;
	}

	pQMsg = (tNtpsQMsg *) (VOID *) pu1MemAlloc;

	pQMsg->u4MsgType = NTPS_IF_CHG_EVENT;
	pQMsg->NtpsIfParam = pBuffer;

	if (OsixQueSend(gu4NtpsQId, (UINT1 *) & pQMsg,
					OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
	{
		/* Free the CRU buffer */
		CRU_BUF_Release_MsgBufChain(pBuffer, FALSE);
		MemReleaseMemBlock(gNtpsQPoolId, pu1MemAlloc);
		NTP_TRC(NTP_CONTROL_PATH_TRC,
				"NtpsHandleIfChg Message send Failed \n");
		return;
	}
	else
	{
		/* Add Trace here */
	}

	OsixEvtSend(gu4NtpsTaskId, NTPS_INPUT_Q_EVENT);

}
#endif
