#include "ntpsinc.h"

extern INT4  CfaGetIfIpPortVlanId(UINT4 u4IfIndex , INT4 *pi4PortVlanId);
extern VOID ArpAddAllArpEntriesInKernel PROTO ((VOID));
/***********************************************************************
 * FUNCTION NAME    : NtpGetAuthKeyNode
 * DESCRIPTION      : Get the authentication key node
 * INPUT            : key
 * OUTPUT           : key node
 * RETURNS          : tNtpsAuth
 ***********************************************************************/
tNtpsAuth *NtpGetAuthKeyNode(INT4 i4KeyId)
{
	tNtpsAuth *pNtpAuth = NULL;
	UINT4 u4Count = 0;

	u4Count = TMO_SLL_Count(&(gNtpsAuthParams.NtpsAuthList));
	while (u4Count > 0)
	{
		TMO_SLL_Scan(&(gNtpsAuthParams.NtpsAuthList), pNtpAuth, tNtpsAuth *)
		{
			if (pNtpAuth->u4NtpsAuthKeyId == (UINT4)i4KeyId)
			{
				break;
			}
		}
		u4Count--;
	}
	return pNtpAuth;
}

/***********************************************************************
* FUNCTION NAME    : NtpCreateAuthKeyNode
* DESCRIPTION      : Create the autnetication key node
* INPUT            : key
* OUTPUT           : NILL
* RETURNS          : None
************************************************************************/

INT4 NtpCreateAuthKeyNode(INT4 i4KeyId)
{
	tNtpsAuth *pNewNtpAuth = NULL;

	if ((pNewNtpAuth =
		 (tNtpsAuth *) (MemAllocMemBlk(gNtpsAuthParams.NtpsPoolId))) == NULL)
	{
		NTP_TRC(NTP_OS_RESOURCE_TRC,
				"NtpsCreateAuthKeyNode Memory Allocation failed for Auth Node \n");
		return NTP_FAILURE;

	}

	pNewNtpAuth->u4NtpsAuthKeyId = (UINT4)i4KeyId;
	TMO_SLL_Add(&(gNtpsAuthParams.NtpsAuthList),
				(tTMO_SLL_NODE *) (pNewNtpAuth));

	return NTP_SUCCESS;
}

/************************************************************************
* FUNCTION NAME    : NtpDeleteAuthKeyNode
* DESCRIPTION      : Delete the authentication key node
* INPUT            : pNtpAuth
* OUTPUT           : NILL
* RETURNS          : None
************************************************************************/
VOID NtpDeleteAuthKeyNode(tNtpsAuth * pNtpAuth)
{

	TMO_SLL_Delete(&gNtpsAuthParams.NtpsAuthList, (tTMO_SLL_NODE *) pNtpAuth);

	MEMSET(pNtpAuth, NTP_ZERO, sizeof(tNtpsAuth));
	if (MemReleaseMemBlock(gNtpsAuthParams.NtpsPoolId,
						   (UINT1 *) pNtpAuth) == MEM_FAILURE)
	{
		NTP_TRC(NTP_OS_RESOURCE_TRC,
				"NtpsDeleteAuthKeyNode Memory Release failed for Auth Node \n");
	}
}

/***********************************************************************
 * FUNCTION NAME    : NtpGetIntfNode
 * DESCRIPTION      : Get the Interface node
 * INPUT            : key
 * OUTPUT           : key node
 * RETURNS          : tNtpsAuth
 ***********************************************************************/
tNtpsIntf *NtpGetIntfNode(UINT4 u4IfIndex)
{
	tNtpsIntf *pNtpIntf = NULL;
	UINT4 u4Count = 0;

	u4Count = TMO_SLL_Count(&(gNtpsIntfParams.NtpsIntfList));
	while (u4Count > 0)
	{
		TMO_SLL_Scan(&(gNtpsIntfParams.NtpsIntfList), pNtpIntf, tNtpsIntf *)
		{
			if (pNtpIntf->u4NtpsIntfIndex == u4IfIndex)
			{
				break;
			}
		}
		u4Count--;
	}
	return pNtpIntf;
}

/***********************************************************************
* FUNCTION NAME    : NtpCreateIntfNode
* DESCRIPTION      : Create the Interface node
* INPUT            : key
* OUTPUT           : NILL
* RETURNS          : None
************************************************************************/

INT4 NtpCreateIntfNode(UINT4 u4IfIndex)
{
	tNtpsIntf *pNewNtpIntf = NULL;

	if ((pNewNtpIntf =
		 (tNtpsIntf *) (MemAllocMemBlk(gNtpsIntfParams.NtpsPoolId))) == NULL)
	{
		NTP_TRC(NTP_OS_RESOURCE_TRC,
				"NtpsCreateIntfNode Memory Allocation failed for Intf Node \n");
		return NTP_FAILURE;

	}
	pNewNtpIntf->u4NtpsIntfIndex = u4IfIndex;
	pNewNtpIntf->u4NtpsIntfStatus = NTP_ENABLE;
	TMO_SLL_Add(&(gNtpsIntfParams.NtpsIntfList),
				(tTMO_SLL_NODE *) (pNewNtpIntf));

	return NTP_SUCCESS;
}

/************************************************************************
* FUNCTION NAME    : NtpDeleteIntfKeyNode
* DESCRIPTION      : Delete the Interface node
* INPUT            : pNtpAuth
* OUTPUT           : NILL
* RETURNS          : None
************************************************************************/
VOID NtpDeleteIntfNode(tNtpsIntf * pNtpIntf)
{

	TMO_SLL_Delete(&gNtpsIntfParams.NtpsIntfList, (tTMO_SLL_NODE *) pNtpIntf);

	MEMSET(pNtpIntf, NTP_ZERO, sizeof(tNtpsIntf));
	if (MemReleaseMemBlock(gNtpsIntfParams.NtpsPoolId,
						   (UINT1 *) pNtpIntf) == MEM_FAILURE)
	{
		NTP_TRC(NTP_OS_RESOURCE_TRC,
				"NtpsDeleteIntfNode Memory Release failed for Intf Node \n");
	}
}


/************************************************************************
* FUNCTION NAME    : NtpConvertAndPrintAuthFlags 
* DESCRIPTION      : Convert and Print Access Flag into FILE or CLI 
* INPUT            : DestPtr  - CliHandle or File *
*					 DestType - CliHandle or File *
*					 u2Flag   - NTP Access Flag value
* OUTPUT           : NILL
* RETURNS          : None
************************************************************************/
VOID NtpConvertAndPrintAuthFlags(VOID * DestPtr, UINT1 DestType, UINT2 u2Flag)
{
	CHR1 strBuf[20];
	MEMSET(strBuf, 0, 20);

	if (u2Flag > 0)
	{
		if ((u2Flag & NTP_RES_IGNORE) == NTP_RES_IGNORE)
		{
			SNPRINTF(strBuf, STRLEN(au1ResFlag[NTP_RES_IGNORE]), "%s ",
					 au1ResFlag[NTP_RES_IGNORE]);
		NTPFLAGPRINT(DestPtr, DestType, strBuf)}
		if ((u2Flag & NTP_RES_KOD) == NTP_RES_KOD)
		{
			SNPRINTF(strBuf, STRLEN(au1ResFlag[NTP_RESTRICT_KOD]),
					 "%s ", au1ResFlag[NTP_RESTRICT_KOD]);
			NTPFLAGPRINT(DestPtr, DestType, strBuf);
		}
		if ((u2Flag & NTP_RES_LIMITED) == NTP_RES_LIMITED)
		{
			SNPRINTF(strBuf, STRLEN(au1ResFlag[NTP_RESTRICT_LIMITED]),
					 "%s ", au1ResFlag[NTP_RESTRICT_LIMITED]);
			NTPFLAGPRINT(DestPtr, DestType, strBuf);
		}
		if ((u2Flag & NTP_RES_LOWPRIOTRAP) == NTP_RES_LOWPRIOTRAP)
		{
			SNPRINTF(strBuf, STRLEN(au1ResFlag[NTP_RESTRICT_LOWPRIOTRAP]),
					 "%s ", au1ResFlag[NTP_RESTRICT_LOWPRIOTRAP]);
			NTPFLAGPRINT(DestPtr, DestType, strBuf);
		}
		if ((u2Flag & NTP_RES_NOMODIFY) == NTP_RES_NOMODIFY)
		{
			SNPRINTF(strBuf, STRLEN(au1ResFlag[NTP_RESTRICT_NOMODIFY]),
					 "%s ", au1ResFlag[NTP_RESTRICT_NOMODIFY]);
			NTPFLAGPRINT(DestPtr, DestType, strBuf);
		}
		if ((u2Flag & NTP_RES_NOQUERY) == NTP_RES_NOQUERY)
		{
			SNPRINTF(strBuf, STRLEN(au1ResFlag[NTP_RESTRICT_NOQUERY]),
					 "%s ", au1ResFlag[NTP_RESTRICT_NOQUERY]);
			NTPFLAGPRINT(DestPtr, DestType, strBuf);
		}
		if ((u2Flag & NTP_RES_NOPEER) == NTP_RES_NOPEER)
		{
			SNPRINTF(strBuf, STRLEN(au1ResFlag[NTP_RESTRICT_NOPEER]),
					 "%s ", au1ResFlag[NTP_RESTRICT_NOPEER]);
			NTPFLAGPRINT(DestPtr, DestType, strBuf);
		}
		if ((u2Flag & NTP_RES_NOSERVE) == NTP_RES_NOSERVE)
		{
			SNPRINTF(strBuf, STRLEN(au1ResFlag[NTP_RESTRICT_NOSERVE]),
					 "%s ", au1ResFlag[NTP_RESTRICT_NOSERVE]);
			NTPFLAGPRINT(DestPtr, DestType, strBuf);
		}
		if ((u2Flag & NTP_RES_NOTRAP) == NTP_RES_NOTRAP)
		{
			SNPRINTF(strBuf, STRLEN(au1ResFlag[NTP_RESTRICT_NOTRAP]),
					 "%s ", au1ResFlag[NTP_RESTRICT_NOTRAP]);
			NTPFLAGPRINT(DestPtr, DestType, strBuf);
		}
		if ((u2Flag & NTP_RES_NOTRUST) == NTP_RES_NOTRUST)
		{
			SNPRINTF(strBuf, STRLEN(au1ResFlag[NTP_RESTRICT_NOTRUST]),
					 "%s ", au1ResFlag[NTP_RESTRICT_NOTRUST]);
			NTPFLAGPRINT(DestPtr, DestType, strBuf);
		}
		if ((u2Flag & NTP_RES_NTPPORT) == NTP_RES_NTPPORT)
		{
			SNPRINTF(strBuf, STRLEN(au1ResFlag[NTP_RESTRICT_NTPPORT]),
					 "%s ", au1ResFlag[NTP_RESTRICT_NTPPORT]);
			NTPFLAGPRINT(DestPtr, DestType, strBuf);
		}
		if ((u2Flag & NTP_RES_VERSION) == NTP_RES_VERSION)
		{
			SNPRINTF(strBuf, STRLEN(au1ResFlag[NTP_RESTRICT_VERSION]),
					 "%s ", au1ResFlag[NTP_RESTRICT_VERSION]);
			NTPFLAGPRINT(DestPtr, DestType, strBuf);
		}
	}
}


/****************************************************************************
 Function    :  NtpsExecuteNTPCommand
 Input       :  System Command 
 Output      :  NONE 
 Returns     :  NTP_SUCCESS or NTP_FAILURE 
****************************************************************************/
INT4 NtpsExecuteNTPCommand(UINT1 * pu1CmdBuf, INT4 * pi4Ret)
{
	INT4 i4Ret = 0;
	FILE *fp = NULL;
        CHR1 TmpBuf[100];
	UINT1 u1TmpFilePath[NTP_FILE_LENGTH] = { '\0' };
	MEMSET(&u1TmpFilePath, NTP_ZERO, sizeof(u1TmpFilePath));
	STRNCPY(u1TmpFilePath, NTP_TMP_FILE, sizeof(NTP_TMP_FILE));
        STRNCAT(pu1CmdBuf," > ",3);
        STRNCAT(pu1CmdBuf,NTP_TMP_FILE,sizeof(NTP_TMP_FILE));
	i4Ret = system((CHR1 *) pu1CmdBuf);

	if (i4Ret == -1)
	{
		NTP_TRC_ARG1(NTP_OS_RESOURCE_TRC,
					 "NtpsExecuteNTPCommand failed for command %s \n",
					 pu1CmdBuf);
		return NTP_FAILURE;
	}
	fp = fopen(NTP_TMP_FILE,"r+"); 
	while(fgets(TmpBuf,100,fp))
	{
		mmi_printf(TmpBuf);
	}
	*pi4Ret = i4Ret;
	return NTP_SUCCESS;
}

/****************************************************************************
 Function    :  NtpsExecuteNTPStartCommand
 Input       :  System Command 
 Output      :  NONE 
 Returns     :  NTP_SUCCESS or NTP_FAILURE 
****************************************************************************/
INT4 NtpsExecuteNTPStartCommand()
{
	UINT1 au1BufStart[NTPS_TEMP_BUF];
	INT4 i4RetVal = 0;


	MEMSET(au1BufStart, 0, NTPS_TEMP_BUF);
	SNPRINTF((CHR1 *) au1BufStart, sizeof(au1BufStart), "%s", NTP_EXE_START);
#ifdef NPAPI_WANTED        
        i4RetVal = NtpsConfigureNP();
        if (i4RetVal != NTP_SUCCESS)
        {
	   SYSLOG_NTP_MSG(SYSLOG_CRITICAL_LEVEL, NTP_MGMT_TRC,
			  "NTP Server: Creation of Knet Interface and Filer in NP - Failed \n");
           return NTP_FAILURE;
        }
#endif
	return (NtpsExecuteNTPCommand(au1BufStart, &i4RetVal));
}

/****************************************************************************
 Function    :  NtpsExecuteNTPStopCommand
 Input       :  System Command 
 Output      :  NONE 
 Returns     :  NTP_SUCCESS or NTP_FAILURE 
****************************************************************************/
INT4 NtpsExecuteNTPStopCommand()
{
	UINT1 au1BufStop[NTPS_TEMP_BUF];
	INT4 i4RetVal = 0;
	MEMSET(au1BufStop, 0, NTPS_TEMP_BUF);
	SNPRINTF((CHR1 *) au1BufStop, sizeof(au1BufStop), "%s", NTP_EXE_STOP);
#ifdef NPAPI_WANTED
        /*Deleting The KNET interface and Filter */
        i4RetVal = IpFsNpIpv4DeleteNtpsIntfAndFilter(gu4NtpSourceInterface);
        if (i4RetVal != NTP_SUCCESS)
        {
	   SYSLOG_NTP_MSG(SYSLOG_CRITICAL_LEVEL, NTP_MGMT_TRC,
			  "NTP Server: Deletion of Knet Interface and Filer in NP - Failed \n");
           return NTP_FAILURE;
        }
#endif
	return (NtpsExecuteNTPCommand(au1BufStop, &i4RetVal));
}

/*****************************************************************************/
/* FUNCTION NAME : IssNtpsShowDebugging */
/* DESCRIPTION : This function prints the NTPS debug level */
/* INPUT : CliHandle */
/* OUTPUT : None */
/* RETURNS : CLI_SUCCESS/CLI_FAILURE */
/*****************************************************************************/
VOID IssNtpsShowDebugging(tCliHandle CliHandle)
{
	INT4 i4DbgLevel = 0;

	i4DbgLevel = (INT4) gNtpGblParams.u4NtpDbgFlag;

	if ((i4DbgLevel == 0))
	{
		return;
	}
	CliPrintf(CliHandle, "\rNTPS :");

	if ((i4DbgLevel & CONTROL_PLANE_TRC) != 0)
	{
		CliPrintf(CliHandle,
				  "\r\n  NTPS Control Plane Operations Trace is on");
	}
	if ((i4DbgLevel & OS_RESOURCE_TRC) != 0)
	{
		CliPrintf(CliHandle, "\r\n  NTPS OS Operations Trace is on");
	}
	if ((i4DbgLevel & MGMT_TRC) != 0)
	{
		CliPrintf(CliHandle, "\r\n  NTPS function for Mgmt Trace is on");
	}
	if ((i4DbgLevel & DATA_PATH_TRC) != 0)
	{
		CliPrintf(CliHandle, "\r\n  NTPS NTP.CONF Operations Trace is on");
	}

	CliPrintf(CliHandle, "\r\n");
	return;
}
/****************************************************************************
 Function    :  NtpsGetSourceInterface
 Input       :  None
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  NTPS_SUCCESS or NTPS_FAILURE
****************************************************************************/
INT1 NtpsGetSourceInterface(UINT4 * pu4NtpsSourceInterface)
{
        *pu4NtpsSourceInterface = gu4NtpSourceInterface;
         return NTP_SUCCESS;
}
/****************************************************************************
 Function    :  NtpsConfigureNP
 Input       :  None
 Output      :  Programs the KNET Interface and Filter in hardware 
 Returns     :  NTP_SUCCESS or NTP_FAILURE 
****************************************************************************/
#ifdef NPAPI_WANTED
INT4 NtpsConfigureNP()
{
    tIpConfigInfo  IpInfo;
    tNpNtpsInfo    NtpsInfo;
    INT4       i4RetVal = 0;
    INT4       i4VlanId = 0;
    UINT4      u4NtpInterfaceIp = 0;
    UINT1      au1IpCommand[NTPS_TEMP_BUF];
    UINT1      au1IpIfName[NTPS_TEMP_BUF];
    UINT1      au1RtCommand[NTPS_TEMP_BUF];
    UINT1      *pcByte1;
    UINT1      *pcByte2;
    UINT1      u1IfaceType = 0;

    MEMSET(au1IpCommand, 0, NTPS_TEMP_BUF);
    MEMSET(au1RtCommand, 0, NTPS_TEMP_BUF);
    MEMSET(au1IpIfName, 0, NTPS_TEMP_BUF);
    MEMSET(&IpInfo, 0, sizeof(tIpConfigInfo));
    MEMSET(&NtpsInfo, 0, sizeof(tNpNtpsInfo));

    if (gu4NtpSourceInterface > 0)
    {
        if (CfaGetIfType(gu4NtpSourceInterface, &u1IfaceType) == CFA_SUCCESS)
        {
             if (u1IfaceType == CFA_ENET)
             {
                 CfaGetIfIpPortVlanId(gu4NtpSourceInterface, &i4VlanId);
             }
             if (u1IfaceType == CFA_L3IPVLAN)
             {
                 CfaGetVlanId(gu4NtpSourceInterface, (UINT2 *) &i4VlanId);
             }
        }
    }
    SNPRINTF((CHR1 *)au1IpIfName, sizeof(au1IpIfName), "vlan%d",i4VlanId);
    
    /*Populating the NP Structure for NPAPI Programming*/
    NtpsInfo.u4CfaIfIndex = gu4NtpSourceInterface;    
    CfaIpIfGetIfInfo (gu4NtpSourceInterface, &IpInfo);
    NtpsInfo.u4NetMask = OSIX_NTOHL(IpInfo.u4NetMask);
    CfaIfGetIpAddress(gu4NtpSourceInterface,&u4NtpInterfaceIp);
    NtpsInfo.u4IpAddr = OSIX_NTOHL(u4NtpInterfaceIp);
    NtpsInfo.u2VlanId = (UINT2) i4VlanId;

    pcByte1 = (UINT1 *) &(NtpsInfo.u4IpAddr);
    pcByte2 = (UINT1 *) &(NtpsInfo.u4NetMask);

    SNPRINTF ((CHR1 *) au1IpCommand, sizeof (au1IpCommand), "%s %s %d.%d.%d.%d netmask %d.%d.%d.%d %s",
               "ifconfig", au1IpIfName ,pcByte1[0], pcByte1[1],
                pcByte1[2], pcByte1[3], pcByte2[0], pcByte2[1],
                pcByte2[2], pcByte2[3], "up");

    SNPRINTF((CHR1 *) au1RtCommand, sizeof (au1RtCommand), "%s %s",
              "route add -net 0.0.0.0/0",au1IpIfName);

    /*Creating the KNET Interface and Filter to trap NTPS Packets to CPU*/
    i4RetVal = IpFsNpIpv4CreateNtpsIntfAndFilter(&NtpsInfo);
    /*Configuring IP in Kernel Interface*/
    NtpsExecuteNTPCommand(au1IpCommand, &i4RetVal);

    /*Configuring Default Route in Kernel*/
    NtpsExecuteNTPCommand(au1RtCommand, &i4RetVal);

    NtpsExecuteNTPCommand(au1IpCommand, &i4RetVal);
    /* Adding all ARP entries In Kernel*/
     ArpAddAllArpEntriesInKernel();
     return i4RetVal;
}
#endif
