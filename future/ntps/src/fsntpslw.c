/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsntpslw.c,v 1.1 2017/04/03 15:16:40 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include  "lr.h"
#include  "fssnmp.h"
#include  "ntpsinc.h"
/* Low Level GET Routine for All Objects */

/****************************************************************************
 Function    :  nmhGetFsNtpsGlobalStatus
 Input       :  The Indices

                The Object 
                retValFsNtpsGlobalStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsNtpsGlobalStatus(INT4 * pi4RetValFsNtpsGlobalStatus)
{
	*pi4RetValFsNtpsGlobalStatus = (INT4) gu4NtpEnableFlag;
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsNtpsServerExecute
 Input       :  The Indices

                The Object 
                retValFsNtpsServerExecute
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsNtpsServerExecute(INT4 * pi4RetValFsNtpsServerExecute)
{
	*pi4RetValFsNtpsServerExecute = (INT4) gu4NtpServerExecStatus;
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsNtpsServerStatus
 Input       :  The Indices

                The Object 
                retValFsNtpsServerStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsNtpsServerStatus(INT4 * pi4RetValFsNtpsServerStatus)
{
	*pi4RetValFsNtpsServerStatus = (INT4) gu4NtpServerStatus;
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsNtpsDebug
 Input       :  The Indices

                The Object 
                retValFsNtpsDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsNtpsDebug(INT4 * pi4RetValFsNtpsDebug)
{
	*pi4RetValFsNtpsDebug = (INT4) gNtpGblParams.u4NtpDbgFlag;
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsNtpsAuthStatus
 Input       :  The Indices

                The Object 
                retValFsNtpsAuthStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsNtpsAuthStatus(INT4 * pi4RetValFsNtpsAuthStatus)
{
	*pi4RetValFsNtpsAuthStatus = (INT4) gu4NtpAuthStatus;
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsNtpsSourceInterface
 Input       :  The Indices

                The Object 
                retValFsNtpsSourceInterface
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsNtpsSourceInterface(INT4 * pi4RetValFsNtpsSourceInterface)
{
	*pi4RetValFsNtpsSourceInterface = (INT4) gu4NtpSourceInterface;
	return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects */
/****************************************************************************
 Function    :  nmhSetFsNtpsGlobalStatus
 Input       :  The Indices

                The Object 
                setValFsNtpsGlobalStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsNtpsGlobalStatus(INT4 i4SetValFsNtpsGlobalStatus)
{
	gu4NtpEnableFlag = (UINT4)i4SetValFsNtpsGlobalStatus;
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsNtpsServerExecute
 Input       :  The Indices

                The Object 
                setValFsNtpsServerExecute
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsNtpsServerExecute(INT4 i4SetValFsNtpsServerExecute)
{
	INT4 i4Ret = 0;
	if ((i4SetValFsNtpsServerExecute == NTP_SERVER_STOP)
		|| (i4SetValFsNtpsServerExecute == NTP_SERVER_RESTART))
	{
		i4Ret = NtpsExecuteNTPStopCommand();
		if (i4Ret != NTP_FAILURE)
		{
			if (i4SetValFsNtpsServerExecute == NTP_SERVER_STOP)
			{
				gu4NtpServerExecStatus = (UINT4)i4SetValFsNtpsServerExecute;
				SYSLOG_NTP_MSG(SYSLOG_NOTICE_LEVEL, NTP_MGMT_TRC,
							   "NTP Server: NTP Daemon Stopped \n");
				return SNMP_SUCCESS;
			}
		}
		else
		{
			SYSLOG_NTP_MSG(SYSLOG_CRITICAL_LEVEL, NTP_MGMT_TRC,
						   "NTP Server: Operation Stop - Failed \n");
		    CLI_SET_ERR(CLI_NTP_STOP_FAIL);
			return SNMP_FAILURE;
		}
	}
	if ((i4SetValFsNtpsServerExecute == NTP_SERVER_RESTART)
		|| (i4SetValFsNtpsServerExecute == NTP_SERVER_START))
	{
		if (NtpsUpdateAllConfigdata() == NTP_SUCCESS)
		{
			i4Ret = NtpsExecuteNTPStopCommand();
			if (i4Ret != NTP_FAILURE)
			{
				i4Ret = NtpsExecuteNTPStartCommand();
				if (i4Ret != NTP_FAILURE)
				{
					gu4NtpServerExecStatus = (UINT4)i4SetValFsNtpsServerExecute;
					SYSLOG_NTP_MSG(SYSLOG_NOTICE_LEVEL, NTP_MGMT_TRC,
								   "NTP Server: NTP Daemon Started \n");
					return SNMP_SUCCESS;
				}
				else
				{
					SYSLOG_NTP_MSG(SYSLOG_ALERT_LEVEL, NTP_MGMT_TRC,
								   "NTP Server: Operation Start - Failed \n");
		            CLI_SET_ERR(CLI_NTP_START_FAIL);
					return SNMP_FAILURE;
				}
			}
			else
			{
				SYSLOG_NTP_MSG(SYSLOG_ALERT_LEVEL, NTP_MGMT_TRC,
							   "NTP Server: Operation Restart - Failed \n");
		        CLI_SET_ERR(CLI_NTP_STOP_FAIL);
				return SNMP_FAILURE;
			}
		}
		else
		{
			SYSLOG_NTP_MSG(SYSLOG_CRITICAL_LEVEL, NTP_MGMT_TRC,
						   "NTP Server: Updating ntp.conf - Failed \n");
			SYSLOG_NTP_MSG(SYSLOG_ALERT_LEVEL, NTP_MGMT_TRC,
						   "NTP Server: Operation Restart - Failed \n");
		    CLI_SET_ERR(CLI_NTP_START_CONF_FAIL);
			return SNMP_FAILURE;
		}
	}
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsNtpsDebug
 Input       :  The Indices

                The Object 
                setValFsNtpsDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsNtpsDebug(INT4 i4SetValFsNtpsDebug)
{
	if (i4SetValFsNtpsDebug > NTP_ZERO)
	{
		gNtpGblParams.u4NtpDbgFlag |= (UINT4) i4SetValFsNtpsDebug;
	}
	else if (i4SetValFsNtpsDebug == NTP_ZERO)
	{
		gNtpGblParams.u4NtpDbgFlag = NTP_ZERO;
	}
	else
	{
		i4SetValFsNtpsDebug &= NTPS_MAX_INT4;
		gNtpGblParams.u4NtpDbgFlag &= (UINT4) (~i4SetValFsNtpsDebug);
	}
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsNtpsAuthStatus
 Input       :  The Indices

                The Object 
                setValFsNtpsAuthStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsNtpsAuthStatus(INT4 i4SetValFsNtpsAuthStatus)
{
	gu4NtpAuthStatus = (UINT4)i4SetValFsNtpsAuthStatus;
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsNtpsSourceInterface
 Input       :  The Indices

                The Object 
                setValFsNtpsSourceInterface
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsNtpsSourceInterface(INT4 i4SetValFsNtpsSourceInterface)
{
	gu4NtpSourceInterface = (UINT4) i4SetValFsNtpsSourceInterface;
	return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects */

/****************************************************************************
 Function    :  nmhTestv2FsNtpsGlobalStatus
 Input       :  The Indices

                The Object 
                testValFsNtpsGlobalStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsNtpsGlobalStatus(UINT4 * pu4ErrorCode,
								 INT4 i4TestValFsNtpsGlobalStatus)
{
	if ((i4TestValFsNtpsGlobalStatus == NTP_ENABLE) ||
		(i4TestValFsNtpsGlobalStatus == NTP_DISABLE))
	{
		*pu4ErrorCode = SNMP_ERR_NO_ERROR;
		return SNMP_SUCCESS;
	}
	else
	{
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		return SNMP_FAILURE;
	}
}

/****************************************************************************
 Function    :  nmhTestv2FsNtpsServerExecute

 Input       :  The Indices

                The Object 
                testValFsNtpsServerExecute
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsNtpsServerExecute(UINT4 * pu4ErrorCode,
								  INT4 i4TestValFsNtpsServerExecute)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(i4TestValFsNtpsServerExecute);
    if(access((CHR1 *)NTP_NTPEXE_PATH,X_OK) != -1)
    {
        if(access((CHR1 *)NTP_NTPSTAT_PATH,X_OK) != -1)
        {
	        return SNMP_SUCCESS;
        }
        else
        {
    	    SYSLOG_NTP_MSG(SYSLOG_CRITICAL_LEVEL, NTP_MGMT_TRC,
    	                        "NTP Server: ntpstat executable not available \n");
            return SNMP_FAILURE; 
        }
    }
    else
    {
	    SYSLOG_NTP_MSG(SYSLOG_CRITICAL_LEVEL, NTP_MGMT_TRC,
	                        "NTP Server: ntpd package not available \n");
        return SNMP_FAILURE; 
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsNtpsDebug
 Input       :  The Indices

                The Object 
                testValFsNtpsDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsNtpsDebug(UINT4 * pu4ErrorCode, INT4 i4TestValFsNtpsDebug)
{
	UNUSED_PARAM(i4TestValFsNtpsDebug);
	*pu4ErrorCode = SNMP_ERR_NO_ERROR;
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsNtpsAuthStatus
 Input       :  The Indices

                The Object 
                testValFsNtpsAuthStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsNtpsAuthStatus(UINT4 * pu4ErrorCode,
							   INT4 i4TestValFsNtpsAuthStatus)
{
	if ((i4TestValFsNtpsAuthStatus != NTP_AUTH_ENABLE) &&
		(i4TestValFsNtpsAuthStatus != NTP_AUTH_DISABLE))
	{
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		return SNMP_FAILURE;
	}
	if (i4TestValFsNtpsAuthStatus == NTP_AUTH_DISABLE)
	{
		if (TMO_SLL_Count((&(gNtpsAuthParams.NtpsAuthList))) > 0)
		{
		    CLI_SET_ERR(CLI_NTP_AUTH_NO_DISABLE);
			*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
			return SNMP_FAILURE;
		}
	}
	*pu4ErrorCode = SNMP_ERR_NO_ERROR;
	return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsNtpsSourceInterface
 Input       :  The Indices

                The Object 
                testValFsNtpsSourceInterface
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsNtpsSourceInterface(UINT4 * pu4ErrorCode,
									INT4 i4TestValFsNtpsSourceInterface)
{
	UINT4 u4IfIndex = 0;
	UINT1 u1IfType = 0;
	u4IfIndex = (UINT4) i4TestValFsNtpsSourceInterface;

	CfaGetIfType(u4IfIndex, &u1IfType);
	if ((u1IfType == CFA_ENET) || (u1IfType == CFA_L3IPVLAN))
	{
		*pu4ErrorCode = SNMP_ERR_NO_ERROR;
		return SNMP_SUCCESS;
	}
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects */

/****************************************************************************
 Function    :  nmhDepv2FsNtpsGlobalStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsNtpsGlobalStatus(UINT4 * pu4ErrorCode,
								tSnmpIndexList * pSnmpIndexList,
								tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsNtpsServerExecute
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsNtpsServerExecute(UINT4 * pu4ErrorCode,
								 tSnmpIndexList * pSnmpIndexList,
								 tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsNtpsDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsNtpsDebug(UINT4 * pu4ErrorCode,
						 tSnmpIndexList * pSnmpIndexList,
						 tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsNtpsAuthStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsNtpsAuthStatus(UINT4 * pu4ErrorCode,
							  tSnmpIndexList * pSnmpIndexList,
							  tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsNtpsSourceInterface
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsNtpsSourceInterface(UINT4 * pu4ErrorCode,
								   tSnmpIndexList * pSnmpIndexList,
								   tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsNtpsServerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsNtpsServerTable
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsNtpsServerTable(INT4 i4FsNtpsServerAddrType,
											   tSNMP_OCTET_STRING_TYPE *
											   pFsNtpsServerAddr)
{
	UNUSED_PARAM(i4FsNtpsServerAddrType);
	UNUSED_PARAM(pFsNtpsServerAddr);
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsNtpsServerTable
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsNtpsServerTable(INT4 * pi4FsNtpsServerAddrType,
									   tSNMP_OCTET_STRING_TYPE *
									   pFsNtpsServerAddr)
{
	tNtpsServer NtpUcastServer;
	UINT1 u1RetVal = 0;

	MEMSET(&NtpUcastServer, 0, sizeof(tNtpsServer));
	u1RetVal = NtpsServerDB(NTPS_GET_FIRST_SERVER_TABLE_ENTRY,
							&NtpUcastServer);
	if (u1RetVal == NTP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	*pi4FsNtpsServerAddrType = (INT4) NtpUcastServer.u4NtpsServerAddrType;
	MEMCPY(pFsNtpsServerAddr->pu1_OctetList,
		   NtpUcastServer.NtpsServerAddr.au1Addr,
		   NtpUcastServer.NtpsServerAddr.u1AddrLen);
	pFsNtpsServerAddr->i4_Length = NtpUcastServer.NtpsServerAddr.u1AddrLen;
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsNtpsServerTable
 Input       :  The Indices
                FsNtpsServerAddrType
                nextFsNtpsServerAddrType
                FsNtpsServerAddr
                nextFsNtpsServerAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsNtpsServerTable(INT4 i4FsNtpsServerAddrType,
									  INT4 * pi4NextFsNtpsServerAddrType,
									  tSNMP_OCTET_STRING_TYPE *
									  pFsNtpsServerAddr,
									  tSNMP_OCTET_STRING_TYPE *
									  pNextFsNtpsServerAddr)
{
	tNtpsServer NtpUcastServer;
	UINT1 u1RetVal = 0;

	MEMSET(&NtpUcastServer, 0, sizeof(tNtpsServer));
	if ((pFsNtpsServerAddr == NULL) || (i4FsNtpsServerAddrType == 0))
	{
		return SNMP_FAILURE;
	}
	NtpUcastServer.u4NtpsServerAddrType = (UINT4)i4FsNtpsServerAddrType;
	MEMCPY(NtpUcastServer.NtpsServerAddr.au1Addr,
		   pFsNtpsServerAddr->pu1_OctetList, pFsNtpsServerAddr->i4_Length);

	u1RetVal = NtpsServerDB(NTPS_GET_NEXT_SERVER_TABLE_ENTRY, &NtpUcastServer);
	if (NTP_FAILURE == u1RetVal)
	{
		return SNMP_FAILURE;
	}
	*pi4NextFsNtpsServerAddrType = (INT4) NtpUcastServer.u4NtpsServerAddrType;
	MEMCPY(pNextFsNtpsServerAddr->pu1_OctetList,
		   NtpUcastServer.NtpsServerAddr.au1Addr,
		   NtpUcastServer.NtpsServerAddr.u1AddrLen);
	return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects */

/****************************************************************************
 Function    :  nmhGetFsNtpsServerVersion
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr

                The Object 
                retValFsNtpsServerVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsNtpsServerVersion(INT4 i4FsNtpsServerAddrType,
							   tSNMP_OCTET_STRING_TYPE * pFsNtpsServerAddr,
							   INT4 * pi4RetValFsNtpsServerVersion)
{
	tNtpsServer NtpsServer;
	UINT1 u1RetVal = 0;
	MEMSET(&NtpsServer, 0, sizeof(tNtpsServer));
	NtpsServer.u4NtpsServerAddrType = (UINT4) i4FsNtpsServerAddrType;
	NtpsServer.NtpsServerAddr.u1AddrLen = (UINT1)pFsNtpsServerAddr->i4_Length;
	MEMCPY(NtpsServer.NtpsServerAddr.au1Addr,
		   pFsNtpsServerAddr->pu1_OctetList, pFsNtpsServerAddr->i4_Length);
	u1RetVal = NtpsServerDB(NTPS_GET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	*pi4RetValFsNtpsServerVersion = (INT4) NtpsServer.u4NtpsServerVersion;
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsNtpsServerKeyId
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr

                The Object 
                retValFsNtpsServerKeyId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsNtpsServerKeyId(INT4 i4FsNtpsServerAddrType,
							 tSNMP_OCTET_STRING_TYPE * pFsNtpsServerAddr,
							 INT4 * pi4RetValFsNtpsServerKeyId)
{
	tNtpsServer NtpsServer;
	UINT1 u1RetVal = 0;
	MEMSET(&NtpsServer, 0, sizeof(tNtpsServer));
	NtpsServer.u4NtpsServerAddrType = (UINT4) i4FsNtpsServerAddrType;
	NtpsServer.NtpsServerAddr.u1AddrLen = (UINT1)pFsNtpsServerAddr->i4_Length;
	MEMCPY(NtpsServer.NtpsServerAddr.au1Addr,
		   pFsNtpsServerAddr->pu1_OctetList, pFsNtpsServerAddr->i4_Length);
	u1RetVal = NtpsServerDB(NTPS_GET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	*pi4RetValFsNtpsServerKeyId = (INT4) NtpsServer.u4NtpsServerKeyId;
	return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsNtpsServerBurstMode
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr

                The Object 
                retValFsNtpsServerBurstMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsNtpsServerBurstMode(INT4 i4FsNtpsServerAddrType,
								 tSNMP_OCTET_STRING_TYPE *
								 pFsNtpsServerAddr,
								 INT4 * pi4RetValFsNtpsServerBurstMode)
{
	tNtpsServer NtpsServer;
	UINT1 u1RetVal = 0;
	MEMSET(&NtpsServer, 0, sizeof(tNtpsServer));
	NtpsServer.u4NtpsServerAddrType = (UINT4) i4FsNtpsServerAddrType;
	MEMCPY(NtpsServer.NtpsServerAddr.au1Addr,
		   pFsNtpsServerAddr->pu1_OctetList, pFsNtpsServerAddr->i4_Length);
	NtpsServer.NtpsServerAddr.u1AddrLen = (UINT1)pFsNtpsServerAddr->i4_Length;
	u1RetVal = NtpsServerDB(NTPS_GET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	*pi4RetValFsNtpsServerBurstMode = (INT4) NtpsServer.u4NtpsServerBurstMode;
	return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsNtpsServerMinPollInt
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr

                The Object 
                retValFsNtpsServerMinPollInt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsNtpsServerMinPollInt(INT4 i4FsNtpsServerAddrType,
								  tSNMP_OCTET_STRING_TYPE *
								  pFsNtpsServerAddr,
								  UINT4 * pu4RetValFsNtpsServerMinPollInt)
{
	tNtpsServer NtpsServer;
	UINT1 u1RetVal = 0;
	MEMSET(&NtpsServer, 0, sizeof(tNtpsServer));
	NtpsServer.u4NtpsServerAddrType = (UINT4) i4FsNtpsServerAddrType;
	MEMCPY(NtpsServer.NtpsServerAddr.au1Addr,
		   pFsNtpsServerAddr->pu1_OctetList, pFsNtpsServerAddr->i4_Length);
	NtpsServer.NtpsServerAddr.u1AddrLen = (UINT1)pFsNtpsServerAddr->i4_Length;
	u1RetVal = NtpsServerDB(NTPS_GET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	*pu4RetValFsNtpsServerMinPollInt = NtpsServer.u4NtpsServerMinPollInt;
	return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsNtpsServerMaxPollInt
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr

                The Object 
                retValFsNtpsServerMaxPollInt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsNtpsServerMaxPollInt(INT4 i4FsNtpsServerAddrType,
								  tSNMP_OCTET_STRING_TYPE *
								  pFsNtpsServerAddr,
								  UINT4 * pu4RetValFsNtpsServerMaxPollInt)
{
	tNtpsServer NtpsServer;
	UINT1 u1RetVal = 0;
	MEMSET(&NtpsServer, 0, sizeof(tNtpsServer));
	NtpsServer.u4NtpsServerAddrType = (UINT4) i4FsNtpsServerAddrType;
	NtpsServer.NtpsServerAddr.u1AddrLen = (UINT1)pFsNtpsServerAddr->i4_Length;
	MEMCPY(NtpsServer.NtpsServerAddr.au1Addr,
		   pFsNtpsServerAddr->pu1_OctetList, pFsNtpsServerAddr->i4_Length);
	u1RetVal = NtpsServerDB(NTPS_GET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	*pu4RetValFsNtpsServerMaxPollInt = NtpsServer.u4NtpsServerMaxPollInt;
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsNtpsServerRowStatus
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr

                The Object 
                retValFsNtpsServerRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsNtpsServerRowStatus(INT4 i4FsNtpsServerAddrType,
								 tSNMP_OCTET_STRING_TYPE *
								 pFsNtpsServerAddr,
								 INT4 * pi4RetValFsNtpsServerRowStatus)
{
	tNtpsServer NtpsServer;
	UINT1 u1RetVal = 0;
	MEMSET(&NtpsServer, 0, sizeof(tNtpsServer));
	NtpsServer.u4NtpsServerAddrType = (UINT4) i4FsNtpsServerAddrType;
	NtpsServer.NtpsServerAddr.u1AddrLen = (UINT1)pFsNtpsServerAddr->i4_Length;
	MEMCPY(NtpsServer.NtpsServerAddr.au1Addr,
		   pFsNtpsServerAddr->pu1_OctetList, pFsNtpsServerAddr->i4_Length);
	u1RetVal = NtpsServerDB(NTPS_GET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	*pi4RetValFsNtpsServerRowStatus = (INT4) NtpsServer.u4NtpsServerRowStatus;
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsNtpsServerPrefer
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr

                The Object 
                retValFsNtpsServerPrefer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsNtpsServerPrefer(INT4 i4FsNtpsServerAddrType,
							  tSNMP_OCTET_STRING_TYPE * pFsNtpsServerAddr,
							  INT4 * pi4RetValFsNtpsServerPrefer)
{
	tNtpsServer NtpsServer;
	UINT1 u1RetVal = 0;
	MEMSET(&NtpsServer, 0, sizeof(tNtpsServer));
	NtpsServer.u4NtpsServerAddrType = (UINT4) i4FsNtpsServerAddrType;
	MEMCPY(NtpsServer.NtpsServerAddr.au1Addr,
		   pFsNtpsServerAddr->pu1_OctetList, pFsNtpsServerAddr->i4_Length);
	NtpsServer.NtpsServerAddr.u1AddrLen = (UINT1)pFsNtpsServerAddr->i4_Length;
	u1RetVal = NtpsServerDB(NTPS_GET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	*pi4RetValFsNtpsServerPrefer = NtpsServer.i4NtpsServerPrefer;
	return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects */

/****************************************************************************
 Function    :  nmhSetFsNtpsServerVersion
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr

                The Object 
                setValFsNtpsServerVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsNtpsServerVersion(INT4 i4FsNtpsServerAddrType,
							   tSNMP_OCTET_STRING_TYPE * pFsNtpsServerAddr,
							   INT4 i4SetValFsNtpsServerVersion)
{
	tNtpsServer NtpsServer;
	UINT1 u1RetVal = 0;
	MEMSET(&NtpsServer, 0, sizeof(tNtpsServer));
	NtpsServer.u4NtpsServerAddrType = (UINT4) i4FsNtpsServerAddrType;
	NtpsServer.NtpsServerAddr.u1AddrLen = (UINT1)pFsNtpsServerAddr->i4_Length;
	MEMCPY(NtpsServer.NtpsServerAddr.au1Addr,
		   pFsNtpsServerAddr->pu1_OctetList, pFsNtpsServerAddr->i4_Length);
	u1RetVal = NtpsServerDB(NTPS_GET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
	    CLI_SET_ERR(CLI_NTP_SERVER_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}
	NtpsServer.u4NtpsServerVersion = (UINT4) i4SetValFsNtpsServerVersion;
	u1RetVal = NtpsServerDB(NTPS_SET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
	    CLI_SET_ERR(CLI_NTP_SERVER_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsNtpsServerKeyId
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr

                The Object 
                setValFsNtpsServerKeyId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsNtpsServerKeyId(INT4 i4FsNtpsServerAddrType,
							 tSNMP_OCTET_STRING_TYPE * pFsNtpsServerAddr,
							 INT4 i4SetValFsNtpsServerKeyId)
{
	tNtpsServer NtpsServer;
	UINT1 u1RetVal = 0;
	MEMSET(&NtpsServer, 0, sizeof(tNtpsServer));
	NtpsServer.u4NtpsServerAddrType = (UINT4) i4FsNtpsServerAddrType;
	MEMCPY(NtpsServer.NtpsServerAddr.au1Addr,
		   pFsNtpsServerAddr->pu1_OctetList, pFsNtpsServerAddr->i4_Length);
	NtpsServer.NtpsServerAddr.u1AddrLen = (UINT1)pFsNtpsServerAddr->i4_Length;
	u1RetVal = NtpsServerDB(NTPS_GET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
	    CLI_SET_ERR(CLI_NTP_SERVER_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}
	NtpsServer.u4NtpsServerKeyId = (UINT4) i4SetValFsNtpsServerKeyId;
	u1RetVal = NtpsServerDB(NTPS_SET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
	    CLI_SET_ERR(CLI_NTP_SERVER_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}
	return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsNtpsServerBurstMode
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr

                The Object 
                setValFsNtpsServerBurstMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsNtpsServerBurstMode(INT4 i4FsNtpsServerAddrType,
								 tSNMP_OCTET_STRING_TYPE *
								 pFsNtpsServerAddr,
								 INT4 i4SetValFsNtpsServerBurstMode)
{
	tNtpsServer NtpsServer;
	UINT1 u1RetVal = 0;
	MEMSET(&NtpsServer, 0, sizeof(tNtpsServer));
	NtpsServer.u4NtpsServerAddrType = (UINT4) i4FsNtpsServerAddrType;
	MEMCPY(NtpsServer.NtpsServerAddr.au1Addr,
		   pFsNtpsServerAddr->pu1_OctetList, pFsNtpsServerAddr->i4_Length);
	NtpsServer.NtpsServerAddr.u1AddrLen = (UINT1)pFsNtpsServerAddr->i4_Length;
	u1RetVal = NtpsServerDB(NTPS_GET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
	    CLI_SET_ERR(CLI_NTP_SERVER_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}

	NtpsServer.u4NtpsServerBurstMode = (UINT4) i4SetValFsNtpsServerBurstMode;
	u1RetVal = NtpsServerDB(NTPS_SET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
	    CLI_SET_ERR(CLI_NTP_SERVER_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsNtpsServerMinPollInt
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr

                The Object 
                setValFsNtpsServerMinPollInt
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsNtpsServerMinPollInt(INT4 i4FsNtpsServerAddrType,
								  tSNMP_OCTET_STRING_TYPE *
								  pFsNtpsServerAddr,
								  UINT4 u4SetValFsNtpsServerMinPollInt)
{
	tNtpsServer NtpsServer;
	UINT1 u1RetVal = 0;
	MEMSET(&NtpsServer, 0, sizeof(tNtpsServer));
	NtpsServer.u4NtpsServerAddrType = (UINT4) i4FsNtpsServerAddrType;
	MEMCPY(NtpsServer.NtpsServerAddr.au1Addr,
		   pFsNtpsServerAddr->pu1_OctetList, pFsNtpsServerAddr->i4_Length);
	NtpsServer.NtpsServerAddr.u1AddrLen = (UINT1)pFsNtpsServerAddr->i4_Length;
	u1RetVal = NtpsServerDB(NTPS_GET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
	    CLI_SET_ERR(CLI_NTP_SERVER_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}

	NtpsServer.u4NtpsServerMinPollInt = u4SetValFsNtpsServerMinPollInt;
	u1RetVal = NtpsServerDB(NTPS_SET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
	    CLI_SET_ERR(CLI_NTP_SERVER_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsNtpsServerMaxPollInt
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr

                The Object 
                setValFsNtpsServerMaxPollInt
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsNtpsServerMaxPollInt(INT4 i4FsNtpsServerAddrType,
								  tSNMP_OCTET_STRING_TYPE *
								  pFsNtpsServerAddr,
								  UINT4 u4SetValFsNtpsServerMaxPollInt)
{
	tNtpsServer NtpsServer;
	UINT1 u1RetVal = 0;
	MEMSET(&NtpsServer, 0, sizeof(tNtpsServer));
	NtpsServer.u4NtpsServerAddrType = (UINT4) i4FsNtpsServerAddrType;
	MEMCPY(NtpsServer.NtpsServerAddr.au1Addr,
		   pFsNtpsServerAddr->pu1_OctetList, pFsNtpsServerAddr->i4_Length);
	NtpsServer.NtpsServerAddr.u1AddrLen = (UINT1)pFsNtpsServerAddr->i4_Length;
	u1RetVal = NtpsServerDB(NTPS_GET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
	    CLI_SET_ERR(CLI_NTP_SERVER_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}

	NtpsServer.u4NtpsServerMaxPollInt = u4SetValFsNtpsServerMaxPollInt;
	u1RetVal = NtpsServerDB(NTPS_SET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
	    CLI_SET_ERR(CLI_NTP_SERVER_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsNtpsServerRowStatus
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr

                The Object 
                setValFsNtpsServerRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsNtpsServerRowStatus(INT4 i4FsNtpsServerAddrType,
								 tSNMP_OCTET_STRING_TYPE *
								 pFsNtpsServerAddr,
								 INT4 i4SetValFsNtpsServerRowStatus)
{
	tNtpsServer NtpsServer;
	UINT4 u4ServerAdd;
	MEMSET(&NtpsServer, 0, sizeof(tNtpsServer));
#ifdef IP6_WANTED
	UINT4 u4Index = 0;
	tIp6Addr TempAddr;
#endif
	UINT1 u1RetVal = 0;

	MEMCPY(&u4ServerAdd, pFsNtpsServerAddr->pu1_OctetList, IPVX_IPV4_ADDR_LEN);
#ifdef IP6_WANTED
	MEMSET(&TempAddr, 0, sizeof(tIp6Addr));
	MEMCPY(&TempAddr, pFsNtpsServerAddr->pu1_OctetList, IPVX_IPV6_ADDR_LEN);
	/* Check for self Ip address */
	if (NetIpv6IsOurAddress(&TempAddr, &u4Index) == NETIPV6_SUCCESS)
	{
	    CLI_SET_ERR(CLI_IPV6_INVALID_ADDRESS);
		return SNMP_FAILURE;
	}
#endif
	MEMSET(&NtpsServer, 0, sizeof(tNtpsServer));
	NtpsServer.u4NtpsServerAddrType = (UINT4) i4FsNtpsServerAddrType;
	NtpsServer.NtpsServerAddr.u1AddrLen = (UINT1)pFsNtpsServerAddr->i4_Length;
	MEMCPY(NtpsServer.NtpsServerAddr.au1Addr,
		   pFsNtpsServerAddr->pu1_OctetList, pFsNtpsServerAddr->i4_Length);

	NtpsServer.u4NtpsServerRowStatus = (UINT4) i4SetValFsNtpsServerRowStatus;
	switch (i4SetValFsNtpsServerRowStatus)
	{

	case NTP_ACTIVE:
	case NTP_NOT_IN_SERVICE:
		u1RetVal = NtpsServerDB(NTPS_GET_SERVER_TABLE_ENTRY, &NtpsServer);
		if (u1RetVal == NTP_FAILURE)
		{
	        CLI_SET_ERR(CLI_NTP_SERVER_NOT_CONFIGURED);
			return SNMP_FAILURE;
		}
		NtpsServer.u4NtpsServerRowStatus = NTP_ACTIVE;
		u1RetVal = NtpsServerDB(NTPS_SET_SERVER_TABLE_ENTRY, &NtpsServer);
		if (u1RetVal == NTP_FAILURE)
		{
	        CLI_SET_ERR(CLI_NTP_SERVER_NOT_CONFIGURED);
			return SNMP_FAILURE;
		}

		break;
	case NTP_CREATE_AND_GO:
	case NTP_CREATE_AND_WAIT:
		u1RetVal = NtpsServerDB(NTPS_CREATE_SERVER_TABLE_ENTRY, &NtpsServer);
		if (u1RetVal == NTP_FAILURE)
		{
	        CLI_SET_ERR(CLI_NTP_SERVER_MAX_ERR);
			return SNMP_FAILURE;
		}
		break;
	case NTP_DESTROY:
		u1RetVal = NtpsServerDB(NTPS_DESTROY_SERVER_TABLE_ENTRY, &NtpsServer);
		if (u1RetVal == NTP_FAILURE)
		{
	        CLI_SET_ERR(CLI_NTP_SERVER_NOT_CONFIGURED);
			return SNMP_FAILURE;
		}
		break;
	default:
		return SNMP_FAILURE;
	}
	return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsNtpsServerPrefer
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr

                The Object 
                setValFsNtpsServerPrefer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsNtpsServerPrefer(INT4 i4FsNtpsServerAddrType,
							  tSNMP_OCTET_STRING_TYPE * pFsNtpsServerAddr,
							  INT4 i4SetValFsNtpsServerPrefer)
{
	tNtpsServer NtpsServer;
	UINT1 u1RetVal = 0;
	MEMSET(&NtpsServer, 0, sizeof(tNtpsServer));
	NtpsServer.u4NtpsServerAddrType = (UINT4) i4FsNtpsServerAddrType;
	MEMCPY(NtpsServer.NtpsServerAddr.au1Addr,
		   pFsNtpsServerAddr->pu1_OctetList, pFsNtpsServerAddr->i4_Length);
	NtpsServer.NtpsServerAddr.u1AddrLen = (UINT1)pFsNtpsServerAddr->i4_Length;
	u1RetVal = NtpsServerDB(NTPS_GET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
	    CLI_SET_ERR(CLI_NTP_SERVER_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}

	NtpsServer.i4NtpsServerPrefer = i4SetValFsNtpsServerPrefer;
	u1RetVal = NtpsServerDB(NTPS_SET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
	    CLI_SET_ERR(CLI_NTP_SERVER_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}

	return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects */

/****************************************************************************
 Function    :  nmhTestv2FsNtpsServerVersion
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr

                The Object 
                testValFsNtpsServerVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsNtpsServerVersion(UINT4 * pu4ErrorCode,
								  INT4 i4FsNtpsServerAddrType,
								  tSNMP_OCTET_STRING_TYPE *
								  pFsNtpsServerAddr,
								  INT4 i4TestValFsNtpsServerVersion)
{
	tNtpsServer NtpsServer;
	UINT1 u1RetVal = 0;
	MEMSET(&NtpsServer, 0, sizeof(tNtpsServer));
	NtpsServer.u4NtpsServerAddrType = (UINT4) i4FsNtpsServerAddrType;
	NtpsServer.NtpsServerAddr.u1AddrLen = (UINT1)pFsNtpsServerAddr->i4_Length;
	MEMCPY(NtpsServer.NtpsServerAddr.au1Addr,
		   pFsNtpsServerAddr->pu1_OctetList, pFsNtpsServerAddr->i4_Length);
	u1RetVal = NtpsServerDB(NTPS_GET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
	    CLI_SET_ERR(CLI_NTP_SERVER_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}
	if (i4TestValFsNtpsServerVersion <= NTP_VERSION4)
	{
		*pu4ErrorCode = SNMP_ERR_NO_ERROR;
		return SNMP_SUCCESS;
	}
	CLI_SET_ERR(CLI_NTP_ERR_INVALID_NTP_VERSION);
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsNtpsServerKeyId
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr

                The Object 
                testValFsNtpsServerKeyId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsNtpsServerKeyId(UINT4 * pu4ErrorCode,
								INT4 i4FsNtpsServerAddrType,
								tSNMP_OCTET_STRING_TYPE *
								pFsNtpsServerAddr,
								INT4 i4TestValFsNtpsServerKeyId)
{
	tNtpsServer NtpsServer;
	UINT1 u1RetVal = 0;
	MEMSET(&NtpsServer, 0, sizeof(tNtpsServer));
	NtpsServer.u4NtpsServerAddrType = (UINT4) i4FsNtpsServerAddrType;
	NtpsServer.NtpsServerAddr.u1AddrLen = (UINT1)pFsNtpsServerAddr->i4_Length;
	MEMCPY(NtpsServer.NtpsServerAddr.au1Addr,
		   pFsNtpsServerAddr->pu1_OctetList, pFsNtpsServerAddr->i4_Length);
	u1RetVal = NtpsServerDB(NTPS_GET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
	    CLI_SET_ERR(CLI_NTP_SERVER_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}
	if ((i4TestValFsNtpsServerKeyId > 0) &&
		(i4TestValFsNtpsServerKeyId <= NTP_AUTH_KEY_MAX))
	{
		*pu4ErrorCode = SNMP_ERR_NO_ERROR;
		return SNMP_SUCCESS;
	}
	CLI_SET_ERR(CLI_NTP_ERR_INVALID_NTP_AUTH_KEYID);
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsNtpsServerBurstMode
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr

                The Object 
                testValFsNtpsServerBurstMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsNtpsServerBurstMode(UINT4 * pu4ErrorCode,
									INT4 i4FsNtpsServerAddrType,
									tSNMP_OCTET_STRING_TYPE *
									pFsNtpsServerAddr,
									INT4 i4TestValFsNtpsServerBurstMode)
{
	tNtpsServer NtpsServer;
	UINT1 u1RetVal = 0;
	MEMSET(&NtpsServer, 0, sizeof(tNtpsServer));
	NtpsServer.u4NtpsServerAddrType = (UINT4) i4FsNtpsServerAddrType;
	NtpsServer.NtpsServerAddr.u1AddrLen = (UINT1)pFsNtpsServerAddr->i4_Length;
	MEMCPY(NtpsServer.NtpsServerAddr.au1Addr,
		   pFsNtpsServerAddr->pu1_OctetList, pFsNtpsServerAddr->i4_Length);
	u1RetVal = NtpsServerDB(NTPS_GET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
	    CLI_SET_ERR(CLI_NTP_SERVER_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}
	if ((i4TestValFsNtpsServerBurstMode == 1) ||
		((i4TestValFsNtpsServerBurstMode == 2)))
	{
		*pu4ErrorCode = SNMP_ERR_NO_ERROR;
		return SNMP_SUCCESS;
	}
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	CLI_SET_ERR(CLI_NTP_ERR_INVALID_BURST_VALUE);
	return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsNtpsServerMinPollInt
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr

                The Object 
                testValFsNtpsServerMinPollInt
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsNtpsServerMinPollInt(UINT4 * pu4ErrorCode,
									 INT4 i4FsNtpsServerAddrType,
									 tSNMP_OCTET_STRING_TYPE *
									 pFsNtpsServerAddr,
									 UINT4 u4TestValFsNtpsServerMinPollInt)
{
	tNtpsServer NtpsServer;
	UINT1 u1RetVal = 0;
	MEMSET(&NtpsServer, 0, sizeof(tNtpsServer));
	NtpsServer.u4NtpsServerAddrType = (UINT4) i4FsNtpsServerAddrType;
	NtpsServer.NtpsServerAddr.u1AddrLen = (UINT1)pFsNtpsServerAddr->i4_Length;
	MEMCPY(NtpsServer.NtpsServerAddr.au1Addr,
		   pFsNtpsServerAddr->pu1_OctetList, pFsNtpsServerAddr->i4_Length);
	u1RetVal = NtpsServerDB(NTPS_GET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
	    CLI_SET_ERR(CLI_NTP_SERVER_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}
	if ((u4TestValFsNtpsServerMinPollInt >= NTP_SERVER_MINPOLL_MIN) &&
		(u4TestValFsNtpsServerMinPollInt <= NTP_SERVER_MINPOLL_MAX))
	{
		*pu4ErrorCode = SNMP_ERR_NO_ERROR;
		return SNMP_SUCCESS;
	}
	CLI_SET_ERR(CLI_NTP_ERR_INVALID_NTP_SERVER_MINPOLL);
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsNtpsServerMaxPollInt
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr

                The Object 
                testValFsNtpsServerMaxPollInt
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsNtpsServerMaxPollInt(UINT4 * pu4ErrorCode,
									 INT4 i4FsNtpsServerAddrType,
									 tSNMP_OCTET_STRING_TYPE *
									 pFsNtpsServerAddr,
									 UINT4 u4TestValFsNtpsServerMaxPollInt)
{
	tNtpsServer NtpsServer;
	UINT1 u1RetVal = 0;
	MEMSET(&NtpsServer, 0, sizeof(tNtpsServer));
	NtpsServer.u4NtpsServerAddrType = (UINT4) i4FsNtpsServerAddrType;
	NtpsServer.NtpsServerAddr.u1AddrLen = (UINT1)pFsNtpsServerAddr->i4_Length;
	MEMCPY(NtpsServer.NtpsServerAddr.au1Addr,
		   pFsNtpsServerAddr->pu1_OctetList, pFsNtpsServerAddr->i4_Length);
	u1RetVal = NtpsServerDB(NTPS_GET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
	    CLI_SET_ERR(CLI_NTP_SERVER_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}

	if ((u4TestValFsNtpsServerMaxPollInt >= NTP_SERVER_MAXPOLL_MIN) &&
		(u4TestValFsNtpsServerMaxPollInt <= NTP_SERVER_MAXPOLL_MAX))
	{
		*pu4ErrorCode = SNMP_ERR_NO_ERROR;
		return SNMP_SUCCESS;
	}
	CLI_SET_ERR(CLI_NTP_ERR_INVALID_NTP_SERVER_MAXPOLL);
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsNtpsServerRowStatus
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr

                The Object 
                testValFsNtpsServerRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsNtpsServerRowStatus(UINT4 * pu4ErrorCode,
									INT4 i4FsNtpsServerAddrType,
									tSNMP_OCTET_STRING_TYPE *
									pFsNtpsServerAddr,
									INT4 i4TestValFsNtpsServerRowStatus)
{
	INT4 i4NumServers = 0;
	if (i4FsNtpsServerAddrType == IPVX_ADDR_FMLY_IPV4)
	{
		if (pFsNtpsServerAddr->pu1_OctetList[0] == 0 ||
			pFsNtpsServerAddr->pu1_OctetList[3] == 0 ||
			pFsNtpsServerAddr->pu1_OctetList[3] == 0xff)
		{
			*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
			CLI_SET_ERR(CLI_NTP_ERR_INVALID_IP_ADDR);
			return SNMP_FAILURE;
		}
	}
	if ((i4TestValFsNtpsServerRowStatus == NTP_ACTIVE) ||
		(i4TestValFsNtpsServerRowStatus == NTP_NOT_IN_SERVICE) ||
		(i4TestValFsNtpsServerRowStatus == NTP_NOT_READY) ||
		(i4TestValFsNtpsServerRowStatus == NTP_DESTROY) ||
		(i4TestValFsNtpsServerRowStatus == NTP_CREATE_AND_GO) ||
		(i4TestValFsNtpsServerRowStatus == NTP_CREATE_AND_WAIT))
	{
		if (i4NumServers <= MAX_NTPS_SERVERS)
		{
			*pu4ErrorCode = SNMP_ERR_NO_ERROR;
			return SNMP_SUCCESS;
		}
	}
	CLI_SET_ERR(CLI_NTP_ERR_INVALID_NTP_ROWSTATUS);
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsNtpsServerPrefer
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr

                The Object 
                testValFsNtpsServerPrefer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsNtpsServerPrefer(UINT4 * pu4ErrorCode,
								 INT4 i4FsNtpsServerAddrType,
								 tSNMP_OCTET_STRING_TYPE *
								 pFsNtpsServerAddr,
								 INT4 i4TestValFsNtpsServerPrefer)
{
	tNtpsServer NtpsServer;
	UINT1 u1RetVal = 0;
	MEMSET(&NtpsServer, 0, sizeof(tNtpsServer));
	NtpsServer.u4NtpsServerAddrType = (UINT4) i4FsNtpsServerAddrType;
	NtpsServer.NtpsServerAddr.u1AddrLen = (UINT1)pFsNtpsServerAddr->i4_Length;
	MEMCPY(NtpsServer.NtpsServerAddr.au1Addr,
		   pFsNtpsServerAddr->pu1_OctetList, pFsNtpsServerAddr->i4_Length);
	u1RetVal = NtpsServerDB(NTPS_GET_SERVER_TABLE_ENTRY, &NtpsServer);
	if (u1RetVal == NTP_FAILURE)
	{
	    CLI_SET_ERR(CLI_NTP_SERVER_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}

	if ((i4TestValFsNtpsServerPrefer == NTP_SERVER_PREFER_ENABLE) ||
		(i4TestValFsNtpsServerPrefer == NTP_SERVER_PREFER_DISABLE))
	{
		*pu4ErrorCode = SNMP_ERR_NO_ERROR;
		return SNMP_SUCCESS;
	}
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects */

/****************************************************************************
 Function    :  nmhDepv2FsNtpsServerTable
 Input       :  The Indices
                FsNtpsServerAddrType
                FsNtpsServerAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsNtpsServerTable(UINT4 * pu4ErrorCode,
							   tSnmpIndexList * pSnmpIndexList,
							   tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsNtpsAuthTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsNtpsAuthKeyTable
 Input       :  The Indices
                FsNtpsAuthKeyId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsNtpsAuthKeyTable(INT4 i4FsNtpsAuthKeyId)
{

	if ((TMO_SLL_Count(&(gNtpsAuthParams.NtpsAuthList)) == NTP_ZERO))
	{

		return SNMP_FAILURE;
	}

	if (i4FsNtpsAuthKeyId >= NTP_ZERO)
	{
		return SNMP_SUCCESS;
	}
	return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsNtpsAuthTable
 Input       :  The Indices
                FsNtpsAuthKeyId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsNtpsAuthTable(INT4 * pi4FsNtpsAuthKeyId)
{
	tNtpsAuth *pNtpAuth = NULL;
	INT4 i4Flag = NTP_ZERO;
	*pi4FsNtpsAuthKeyId = 0;
	/* Get first index (numerically first index) */
	TMO_SLL_Scan(&gNtpsAuthParams.NtpsAuthList, pNtpAuth, tNtpsAuth *)
	{
		if (i4Flag == NTP_ZERO)
		{
			*pi4FsNtpsAuthKeyId = NTP_AUTH_KEY_MAX + 1;
		}
		if ((pNtpAuth->u4NtpsAuthRowStatus != DESTROY) &&
			(pNtpAuth->u4NtpsAuthKeyId < (UINT4) * pi4FsNtpsAuthKeyId))
		{
			*pi4FsNtpsAuthKeyId = (INT4)pNtpAuth->u4NtpsAuthKeyId;
			i4Flag = 1;
		}
	}
	/* No entry is available */
	if (*pi4FsNtpsAuthKeyId == 0)
	{
		return (SNMP_FAILURE);
	}

	return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsNtpsAuthKeyTable
 Input       :  The Indices
                FsNtpsAuthKeyId
                nextFsNtpsAuthKeyId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsNtpsAuthKeyTable(INT4 i4FsNtpsAuthKeyId ,INT4 *pi4NextFsNtpsAuthKeyId )
{
	tNtpsAuth *pNtpAuth = NULL;
	UINT1 u1FirstNextFound = NTP_ZERO;

	*pi4NextFsNtpsAuthKeyId = NTP_ZERO;
	TMO_SLL_Scan(&gNtpsAuthParams.NtpsAuthList, pNtpAuth, tNtpsAuth *)
	{
		if ((pNtpAuth->u4NtpsAuthRowStatus != DESTROY) &&
			(pNtpAuth->u4NtpsAuthKeyId > (UINT4) i4FsNtpsAuthKeyId))
		{
			if (u1FirstNextFound == NTP_ZERO)
			{
				*pi4NextFsNtpsAuthKeyId = (INT4)pNtpAuth->u4NtpsAuthKeyId;
				u1FirstNextFound = 1;
			}
			else if (pNtpAuth->u4NtpsAuthKeyId <
					 (UINT4) * pi4NextFsNtpsAuthKeyId)
			{
				*pi4NextFsNtpsAuthKeyId = (INT4)pNtpAuth->u4NtpsAuthKeyId;
			}
		}
	}
	if ((i4FsNtpsAuthKeyId == *pi4NextFsNtpsAuthKeyId) ||
		(u1FirstNextFound == NTP_ZERO))
	{

		return (SNMP_FAILURE);
	}
	return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects */

/****************************************************************************
 Function    :  nmhGetFsNtpsAuthAlgorithm
 Input       :  The Indices
                FsNtpsAuthKeyId

                The Object 
                retValFsNtpsAuthAlgorithm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsNtpsAuthAlgorithm(INT4 i4FsNtpsAuthKeyId , INT4 *pi4RetValFsNtpsAuthAlgorithm)
{
	tNtpsAuth *pNtpAuth = NULL;

	pNtpAuth = NtpGetAuthKeyNode(i4FsNtpsAuthKeyId);
	if (pNtpAuth == NULL)
	{
		*pi4RetValFsNtpsAuthAlgorithm = NTP_ZERO;
		return SNMP_FAILURE;
	}

	*pi4RetValFsNtpsAuthAlgorithm = (INT4) pNtpAuth->u4NtpsAuthAlgorithm;

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsNtpsAuthKey
 Input       :  The Indices
                FsNtpsAuthKeyId

                The Object 
                retValFsNtpsAuthKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsNtpsAuthKey(INT4 i4FsNtpsAuthKeyId,
						 tSNMP_OCTET_STRING_TYPE * pRetValFsNtpsAuthKey)
{
	tNtpsAuth *pNtpAuth = NULL;

	pNtpAuth = NtpGetAuthKeyNode(i4FsNtpsAuthKeyId);
	if (pNtpAuth == NULL)
	{
		return SNMP_FAILURE;
	}
	pRetValFsNtpsAuthKey->i4_Length =
		(INT4) STRLEN(pNtpAuth->u1NtpsAuthKey) + 1;
	STRNCPY(pRetValFsNtpsAuthKey->pu1_OctetList, pNtpAuth->u1NtpsAuthKey,
			pRetValFsNtpsAuthKey->i4_Length);

	return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsNtpsAuthTrustedKey
 Input       :  The Indices
                FsNtpsAuthKeyId

                The Object 
                retValFsNtpsAuthTrustedKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsNtpsAuthTrustedKey(INT4 i4FsNtpsAuthKeyId,
								INT4 * pi4RetValFsNtpsAuthTrustedKey)
{
	tNtpsAuth *pNtpAuth = NULL;

	pNtpAuth = NtpGetAuthKeyNode(i4FsNtpsAuthKeyId);
	if (pNtpAuth == NULL)
	{
		*pi4RetValFsNtpsAuthTrustedKey = NTP_ZERO;
		return SNMP_FAILURE;
	}

	*pi4RetValFsNtpsAuthTrustedKey = (INT4) pNtpAuth->u4NtpsAuthTrustedKey;

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsNtpsAuthRowStatus
 Input       :  The Indices
                FsNtpsAuthKeyId

                The Object 
                retValFsNtpsAuthRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsNtpsAuthRowStatus(INT4 i4FsNtpsAuthKeyId,
							   INT4 * pi4RetValFsNtpsAuthRowStatus)
{
	tNtpsAuth *pNtpAuth = NULL;

	pNtpAuth = NtpGetAuthKeyNode(i4FsNtpsAuthKeyId);

	if (pNtpAuth == NULL)
	{
		*pi4RetValFsNtpsAuthRowStatus = NTP_ZERO;
		return SNMP_FAILURE;
	}

	*pi4RetValFsNtpsAuthRowStatus = (INT4) pNtpAuth->u4NtpsAuthRowStatus;

	return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects */

/****************************************************************************
 Function    :  nmhSetFsNtpsAuthAlgorithm
 Input       :  The Indices
                FsNtpsAuthKeyId

                The Object 
                setValFsNtpsAuthAlgorithm
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsNtpsAuthAlgorithm(INT4 i4FsNtpsAuthKeyId,
							   INT4 i4SetValFsNtpsAuthAlgorithm)
{
	tNtpsAuth *pNtpAuth = NULL;


	pNtpAuth = NtpGetAuthKeyNode(i4FsNtpsAuthKeyId);
	if (pNtpAuth == NULL)
	{
		CLI_SET_ERR(CLI_NTP_KEY_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}



	pNtpAuth->u4NtpsAuthAlgorithm = (UINT4) i4SetValFsNtpsAuthAlgorithm;
	return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsNtpsAuthKey
 Input       :  The Indices
                FsNtpsAuthKeyId

                The Object 
                setValFsNtpsAuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsNtpsAuthKey(INT4 i4FsNtpsAuthKeyId,
						 tSNMP_OCTET_STRING_TYPE * pSetValFsNtpsAuthKey)
{
	tNtpsAuth *pNtpAuth = NULL;


	pNtpAuth = NtpGetAuthKeyNode(i4FsNtpsAuthKeyId);
	if (pNtpAuth == NULL)
	{
		CLI_SET_ERR(CLI_NTP_KEY_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}
	else
	{
		if (STRCMP
			(pNtpAuth->u1NtpsAuthKey,
			 pSetValFsNtpsAuthKey->pu1_OctetList) == NTP_ZERO)
		{
			return SNMP_SUCCESS;
		}
		MEMSET(pNtpAuth->u1NtpsAuthKey, NTP_ZERO, NTP_AUTH_MAX_KEY_LENGTH);
		STRNCPY(pNtpAuth->u1NtpsAuthKey,
				pSetValFsNtpsAuthKey->pu1_OctetList,
				pSetValFsNtpsAuthKey->i4_Length);
		return SNMP_SUCCESS;
	}

}

/****************************************************************************
 Function    :  nmhSetFsNtpsAuthTrustedKey
 Input       :  The Indices
                FsNtpsAuthKeyId

                The Object 
                setValFsNtpsAuthTrustedKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsNtpsAuthTrustedKey(INT4 i4FsNtpsAuthKeyId,
								INT4 i4SetValFsNtpsAuthTrustedKey)
{
	tNtpsAuth *pNtpAuth = NULL;

	pNtpAuth = NtpGetAuthKeyNode(i4FsNtpsAuthKeyId);
	if (pNtpAuth == NULL)
	{
		CLI_SET_ERR(CLI_NTP_KEY_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}
	else
	{
		pNtpAuth->u4NtpsAuthTrustedKey = (UINT4) i4SetValFsNtpsAuthTrustedKey;
		return SNMP_SUCCESS;
	}

}

/****************************************************************************
 Function    :  nmhSetFsNtpsAuthRowStatus
 Input       :  The Indices
                FsNtpsAuthKeyId

                The Object 
                setValFsNtpsAuthRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsNtpsAuthRowStatus(INT4 i4FsNtpsAuthKeyId,
							   INT4 i4SetValFsNtpsAuthRowStatus)
{
	tNtpsAuth *pNtpAuth = NULL;

	if (gu4NtpAuthStatus == NTP_AUTH_DISABLE)
	{
		CLI_SET_ERR(CLI_NTP_AUTH_DISABLE);
		return SNMP_FAILURE;
	}

	switch (i4SetValFsNtpsAuthRowStatus)
	{

	case NTP_CREATE_AND_GO:
	case NTP_CREATE_AND_WAIT:
	case NTP_ACTIVE:
		pNtpAuth = NtpGetAuthKeyNode(i4FsNtpsAuthKeyId);
		if (pNtpAuth == NULL)
		{
			if (NtpCreateAuthKeyNode(i4FsNtpsAuthKeyId) == NTP_SUCCESS)
			{
				pNtpAuth = NtpGetAuthKeyNode(i4FsNtpsAuthKeyId);
				if (pNtpAuth == NULL)
				{
					return SNMP_FAILURE;
				}
			}
			else
			{
		        CLI_SET_ERR(CLI_NTP_AUTH_MAX_KEY);
				return SNMP_FAILURE;
			}
		}
		if (i4SetValFsNtpsAuthRowStatus == NTP_ACTIVE)
		{
			pNtpAuth->u4NtpsAuthRowStatus = NTP_ACTIVE;
		}
		break;

	case NTP_NOT_IN_SERVICE:

		pNtpAuth = NtpGetAuthKeyNode(i4FsNtpsAuthKeyId);
		if (pNtpAuth == NULL)
		{
		    CLI_SET_ERR(CLI_NTP_KEY_NOT_CONFIGURED);
			return SNMP_FAILURE;
		}

		pNtpAuth->u4NtpsAuthRowStatus = (UINT4) i4SetValFsNtpsAuthRowStatus;

		break;

	case NTP_DESTROY:

		pNtpAuth = NtpGetAuthKeyNode(i4FsNtpsAuthKeyId);
		if (pNtpAuth != NULL)
		{
			NtpDeleteAuthKeyNode(pNtpAuth);
		}
        else
        {   
		    CLI_SET_ERR(CLI_NTP_KEY_NOT_CONFIGURED);
		    return SNMP_FAILURE;
        }
		break;

	default:
		return SNMP_FAILURE;
	}

	return SNMP_SUCCESS;


}

/* Low Level TEST Routines for All Objects */

/****************************************************************************
 Function    :  nmhTestv2FsNtpsAuthAlgorithm
 Input       :  The Indices
                FsNtpsAuthKeyId

                The Object 
                testValFsNtpsAuthAlgorithm
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsNtpsAuthAlgorithm(UINT4 * pu4ErrorCode,
								  INT4 i4FsNtpsAuthKeyId,
								  INT4 i4TestValFsNtpsAuthAlgorithm)
{
	UNUSED_PARAM(i4FsNtpsAuthKeyId);
	if ((i4TestValFsNtpsAuthAlgorithm == NTP_AUTH_NONE) ||
		(i4TestValFsNtpsAuthAlgorithm == NTP_AUTH_MD5) ||
		(i4TestValFsNtpsAuthAlgorithm == NTP_AUTH_SHA1) ||
		(i4TestValFsNtpsAuthAlgorithm == NTP_AUTH_RSA_MD2) ||
		(i4TestValFsNtpsAuthAlgorithm == NTP_AUTH_RSA_MD5) ||
		(i4TestValFsNtpsAuthAlgorithm == NTP_AUTH_RSA_SHA) ||
		(i4TestValFsNtpsAuthAlgorithm == NTP_AUTH_RSA_SHA1) ||
		(i4TestValFsNtpsAuthAlgorithm == NTP_AUTH_RSA_MDC2) ||
		(i4TestValFsNtpsAuthAlgorithm == NTP_AUTH_DSA_SHA) ||
		(i4TestValFsNtpsAuthAlgorithm == NTP_AUTH_DSA_SHA1) ||
		(i4TestValFsNtpsAuthAlgorithm == NTP_AUTH_RSA_RIPEMD160))
	{

		*pu4ErrorCode = SNMP_ERR_NO_ERROR;
		return SNMP_SUCCESS;
	}

	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsNtpsAuthKey
 Input       :  The Indices
                FsNtpsAuthKeyId

                The Object 
                testValFsNtpsAuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsNtpsAuthKey(UINT4 * pu4ErrorCode, INT4 i4FsNtpsAuthKeyId,
							tSNMP_OCTET_STRING_TYPE * pTestValFsNtpsAuthKey)
{
	UNUSED_PARAM(i4FsNtpsAuthKeyId);
	if ((pTestValFsNtpsAuthKey->i4_Length < NTP_INVALID_KEY_LEN) ||
		(pTestValFsNtpsAuthKey->i4_Length > NTP_KEY_MAX_LENGTH ))
	{
		*pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
		return SNMP_FAILURE;
	}
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsNtpsAuthTrustedKey
 Input       :  The Indices
                FsNtpsAuthKeyId

                The Object 
                testValFsNtpsAuthTrustedKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsNtpsAuthTrustedKey(UINT4 * pu4ErrorCode,
								   INT4 i4FsNtpsAuthKeyId,
								   INT4 i4TestValFsNtpsAuthTrustedKey)
{
	if (i4FsNtpsAuthKeyId < NTP_ZERO)
	{
		return SNMP_FAILURE;
	}

	if (i4TestValFsNtpsAuthTrustedKey == NTP_KEY_TRUSTED ||
		i4TestValFsNtpsAuthTrustedKey == NTP_NO_KEY_TRUSTED)
	{
		*pu4ErrorCode = SNMP_ERR_NO_ERROR;
		return SNMP_SUCCESS;

	}

	return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsNtpsAuthRowStatus
 Input       :  The Indices
                FsNtpsAuthKeyId

                The Object 
                testValFsNtpsAuthRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsNtpsAuthRowStatus(UINT4 * pu4ErrorCode,
								  INT4 i4FsNtpsAuthKeyId,
								  INT4 i4TestValFsNtpsAuthRowStatus)
{
	tNtpsAuth *pNtpAuth = NULL;

	if (gu4NtpAuthStatus == NTP_AUTH_DISABLE)
	{
		CLI_SET_ERR(CLI_NTP_AUTH_DISABLE);
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		return SNMP_FAILURE;
	}

	pNtpAuth = NtpGetAuthKeyNode(i4FsNtpsAuthKeyId);

	if (((i4TestValFsNtpsAuthRowStatus == NTP_ACTIVE) ||
		 (i4TestValFsNtpsAuthRowStatus == NTP_CREATE_AND_GO) ||
		 (i4TestValFsNtpsAuthRowStatus == NTP_CREATE_AND_WAIT)))
	{
		/* If there is no key associated with this node ,Do not proceed with
		   nmhSet of this object */
		*pu4ErrorCode = SNMP_ERR_NO_ERROR;
		return SNMP_SUCCESS;
	}
	if ((i4TestValFsNtpsAuthRowStatus == NTP_DESTROY) && (pNtpAuth != NULL))
	{
		*pu4ErrorCode = SNMP_ERR_NO_ERROR;
		return SNMP_SUCCESS;
	}
	if ((i4TestValFsNtpsAuthRowStatus == NTP_DESTROY) && (pNtpAuth == NULL))
	{
		CLI_SET_ERR(CLI_NTP_KEY_NOT_CONFIGURED);
		*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		return SNMP_FAILURE;
	}
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	return SNMP_FAILURE;

}

/* Low Level Dependency Routines for All Objects */

/****************************************************************************
 Function    :  nmhDepv2FsNtpsAuthKeyTable
 Input       :  The Indices
                FsNtpsAuthKeyId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsNtpsAuthKeyTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsNtpsAccessTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsNtpsAccessTable
 Input       :  The Indices
                FsNtpsAccessAddrType
                FsNtpsAccessIpAddr
                FsNtpsAccessIpMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsNtpsAccessTable(INT4 i4FsNtpsAccessAddrType,
											   tSNMP_OCTET_STRING_TYPE *
											   pFsNtpsAccessIpAddr,
											   INT4 i4FsNtpsAccessIpMask)
{
	UNUSED_PARAM(i4FsNtpsAccessAddrType);
	UNUSED_PARAM(pFsNtpsAccessIpAddr);
	UNUSED_PARAM(i4FsNtpsAccessIpMask);
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsNtpsAccessTable
 Input       :  The Indices
                FsNtpsAccessAddrType
                FsNtpsAccessIpAddr
                FsNtpsAccessIpMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsNtpsAccessTable(INT4 * pi4FsNtpsAccessAddrType,
									   tSNMP_OCTET_STRING_TYPE *
									   pFsNtpsAccessIpAddr,
									   INT4 * pi4FsNtpsAccessIpMask)
{
	tNtpsAccess NtpAccessNode;
	UINT1 u1RetVal = 0;

	MEMSET(&NtpAccessNode, 0, sizeof(tNtpsAccess));
	u1RetVal = NtpsAccessDB(NTPS_GET_FIRST_ACCESS_TABLE_ENTRY, &NtpAccessNode);
	if (u1RetVal == NTP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	MEMCPY(pFsNtpsAccessIpAddr->pu1_OctetList,
		   NtpAccessNode.NtpsAccessIp.au1Addr,
		   NtpAccessNode.NtpsAccessIp.u1AddrLen);
	pFsNtpsAccessIpAddr->i4_Length =
		(INT4) NtpAccessNode.NtpsAccessIp.u1AddrLen;
	*pi4FsNtpsAccessIpMask = (UINT2) NtpAccessNode.u2NtpsAccessIpMask;
	*pi4FsNtpsAccessAddrType = (UINT4) NtpAccessNode.NtpsAccessIp.u1Afi;
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsNtpsAccessTable
 Input       :  The Indices
                FsNtpsAccessAddrType
                nextFsNtpsAccessAddrType
                FsNtpsAccessIpAddr
                nextFsNtpsAccessIpAddr
                FsNtpsAccessIpMask
                nextFsNtpsAccessIpMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsNtpsAccessTable(INT4 i4FsNtpsAccessAddrType,
									  INT4 * pi4NextFsNtpsAccessAddrType,
									  tSNMP_OCTET_STRING_TYPE *
									  pFsNtpsAccessIpAddr,
									  tSNMP_OCTET_STRING_TYPE *
									  pNextFsNtpsAccessIpAddr,
									  INT4 i4FsNtpsAccessIpMask,
									  INT4 * pi4NextFsNtpsAccessIpMask)
{
	tNtpsAccess NtpAccessNode;
	UINT1 u1RetVal = 0;

	MEMSET(&NtpAccessNode, 0, sizeof(tNtpsAccess));
	if ((pFsNtpsAccessIpAddr == NULL) || (i4FsNtpsAccessIpMask == 0))
	{
		return SNMP_FAILURE;
	}
	NtpAccessNode.NtpsAccessIp.u1AddrLen =
		(UINT1) pFsNtpsAccessIpAddr->i4_Length;
	MEMCPY(NtpAccessNode.NtpsAccessIp.au1Addr,
		   pFsNtpsAccessIpAddr->pu1_OctetList, pFsNtpsAccessIpAddr->i4_Length);
	NtpAccessNode.NtpsAccessIp.u1Afi = (UINT1) i4FsNtpsAccessAddrType;
	NtpAccessNode.u2NtpsAccessIpMask = (UINT2)i4FsNtpsAccessIpMask;
	u1RetVal = NtpsAccessDB(NTPS_GET_NEXT_ACCESS_TABLE_ENTRY, &NtpAccessNode);
	if (u1RetVal == NTP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	MEMCPY(pNextFsNtpsAccessIpAddr->pu1_OctetList,
		   NtpAccessNode.NtpsAccessIp.au1Addr,
		   NtpAccessNode.NtpsAccessIp.u1AddrLen);
	pNextFsNtpsAccessIpAddr->i4_Length =
		(INT4) NtpAccessNode.NtpsAccessIp.u1AddrLen;
	*pi4NextFsNtpsAccessIpMask = (INT4) NtpAccessNode.u2NtpsAccessIpMask;
	*pi4NextFsNtpsAccessAddrType = (UINT4) NtpAccessNode.NtpsAccessIp.u1Afi;
	return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects */

/****************************************************************************
 Function    :  nmhGetFsNtpsAccessRowStatus
 Input       :  The Indices
                FsNtpsAccessAddrType
                FsNtpsAccessIpAddr
                FsNtpsAccessIpMask

                The Object 
                retValFsNtpsAccessRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsNtpsAccessRowStatus(INT4 i4FsNtpsAccessAddrType,
								 tSNMP_OCTET_STRING_TYPE *
								 pFsNtpsAccessIpAddr,
								 INT4 i4FsNtpsAccessIpMask,
								 INT4 * pi4RetValFsNtpsAccessRowStatus)
{
	tNtpsAccess NtpAccessNode;
	UINT1 u1RetVal = 0;

	MEMSET(&NtpAccessNode, 0, sizeof(tNtpsAccess));
	if ((pFsNtpsAccessIpAddr == NULL) || (i4FsNtpsAccessIpMask == 0))
	{
		return SNMP_FAILURE;
	}
	NtpAccessNode.NtpsAccessIp.u1AddrLen =
		(UINT1) pFsNtpsAccessIpAddr->i4_Length;
	MEMCPY(NtpAccessNode.NtpsAccessIp.au1Addr,
		   pFsNtpsAccessIpAddr->pu1_OctetList, pFsNtpsAccessIpAddr->i4_Length);
	NtpAccessNode.NtpsAccessIp.u1Afi = (UINT1) i4FsNtpsAccessAddrType;
	NtpAccessNode.u2NtpsAccessIpMask = (UINT2) i4FsNtpsAccessIpMask;
	u1RetVal = NtpsAccessDB(NTPS_GET_ACCESS_TABLE_ENTRY, &NtpAccessNode);
	if (u1RetVal == NTP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	*pi4RetValFsNtpsAccessRowStatus =
		(INT4) NtpAccessNode.u2NtpsAccessRowStatus;
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsNtpsAccessFlag
 Input       :  The Indices
                FsNtpsAccessAddrType
                FsNtpsAccessIpAddr
                FsNtpsAccessIpMask

                The Object 
                retValFsNtpsAccessFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsNtpsAccessFlag(INT4 i4FsNtpsAccessAddrType,
							tSNMP_OCTET_STRING_TYPE * pFsNtpsAccessIpAddr,
							INT4 i4FsNtpsAccessIpMask,
							INT4 * pi4RetValFsNtpsAccessFlag)
{
	tNtpsAccess NtpAccessNode;
	UINT1 u1RetVal = 0;

	MEMSET(&NtpAccessNode, 0, sizeof(tNtpsAccess));
	if ((pFsNtpsAccessIpAddr == NULL) || (i4FsNtpsAccessIpMask == 0))
	{
		return SNMP_FAILURE;
	}
	NtpAccessNode.NtpsAccessIp.u1AddrLen =
		(UINT1) pFsNtpsAccessIpAddr->i4_Length;
	MEMCPY(NtpAccessNode.NtpsAccessIp.au1Addr,
		   pFsNtpsAccessIpAddr->pu1_OctetList, pFsNtpsAccessIpAddr->i4_Length);
	NtpAccessNode.NtpsAccessIp.u1Afi = (UINT1) i4FsNtpsAccessAddrType;
	NtpAccessNode.u2NtpsAccessIpMask = (UINT2) i4FsNtpsAccessIpMask;
	u1RetVal = NtpsAccessDB(NTPS_GET_ACCESS_TABLE_ENTRY, &NtpAccessNode);
	if (u1RetVal == NTP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	*pi4RetValFsNtpsAccessFlag = (INT4) NtpAccessNode.u2NtpsAccessFlag;
	return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects */

/****************************************************************************
 Function    :  nmhSetFsNtpsAccessRowStatus
 Input       :  The Indices
                FsNtpsAccessAddrType
                FsNtpsAccessIpAddr
                FsNtpsAccessIpMask

                The Object 
                setValFsNtpsAccessRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsNtpsAccessRowStatus(INT4 i4FsNtpsAccessAddrType,
								 tSNMP_OCTET_STRING_TYPE *
								 pFsNtpsAccessIpAddr,
								 INT4 i4FsNtpsAccessIpMask,
								 INT4 i4SetValFsNtpsAccessRowStatus)
{
	tNtpsAccess NtpAccessNode;
	UINT1 u1RetVal = 0;

	MEMSET(&NtpAccessNode, 0, sizeof(tNtpsAccess));
	NtpAccessNode.NtpsAccessIp.u1AddrLen =
		(UINT1) pFsNtpsAccessIpAddr->i4_Length;
	MEMCPY(NtpAccessNode.NtpsAccessIp.au1Addr,
		   pFsNtpsAccessIpAddr->pu1_OctetList, pFsNtpsAccessIpAddr->i4_Length);
	NtpAccessNode.NtpsAccessIp.u1Afi = (UINT1) i4FsNtpsAccessAddrType;
	NtpAccessNode.u2NtpsAccessIpMask = (UINT2)i4FsNtpsAccessIpMask;
	NtpAccessNode.u2NtpsAccessRowStatus =
		(UINT2) i4SetValFsNtpsAccessRowStatus;
	switch (i4SetValFsNtpsAccessRowStatus)
	{

	case NTP_CREATE_AND_GO:
	case NTP_CREATE_AND_WAIT:
		u1RetVal = NtpsAccessDB(NTPS_CREATE_ACCESS_TABLE_ENTRY,
								&NtpAccessNode);
		if (u1RetVal == NTP_FAILURE)
		{
	        CLI_SET_ERR(CLI_NTP_MAX_ACCESS_CONFIGURED);
			return SNMP_FAILURE;
		}
		break;
	case NTP_ACTIVE:
	case NTP_NOT_IN_SERVICE:
		u1RetVal = NtpsAccessDB(NTPS_GET_ACCESS_TABLE_ENTRY, &NtpAccessNode);
		if (u1RetVal == NTP_FAILURE)
		{
	        CLI_SET_ERR(CLI_NTP_ACCESS_NOT_CONFIGURED);
			return SNMP_FAILURE;
		}
		NtpAccessNode.u2NtpsAccessRowStatus =
			(UINT2) i4SetValFsNtpsAccessRowStatus;
		u1RetVal = NtpsAccessDB(NTPS_SET_ACCESS_TABLE_ENTRY, &NtpAccessNode);
		if (u1RetVal == NTP_FAILURE)
		{
	        CLI_SET_ERR(CLI_NTP_ACCESS_NOT_CONFIGURED);
			return SNMP_FAILURE;
		}

		break;
	case NTP_DESTROY:
		u1RetVal = NtpsAccessDB(NTPS_DESTROY_ACCESS_TABLE_ENTRY,
								&NtpAccessNode);
		if (u1RetVal == NTP_FAILURE)
		{
	        CLI_SET_ERR(CLI_NTP_ACCESS_NOT_CONFIGURED);
			return SNMP_FAILURE;
		}
		break;
	default:
		return SNMP_FAILURE;
	}
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsNtpsAccessFlag
 Input       :  The Indices
                FsNtpsAccessAddrType
                FsNtpsAccessIpAddr
                FsNtpsAccessIpMask

                The Object 
                setValFsNtpsAccessFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsNtpsAccessFlag(INT4 i4FsNtpsAccessAddrType,
							tSNMP_OCTET_STRING_TYPE * pFsNtpsAccessIpAddr,
							INT4 i4FsNtpsAccessIpMask,
							INT4 i4SetValFsNtpsAccessFlag)
{
	tNtpsAccess NtpAccessNode;
	UINT1 u1RetVal = 0;

	MEMSET(&NtpAccessNode, 0, sizeof(tNtpsAccess));
	if ((pFsNtpsAccessIpAddr == NULL) || (i4FsNtpsAccessIpMask == 0))
	{
	    CLI_SET_ERR(CLI_NTP_ERR_INVALID_IP_ADDR);
		return SNMP_FAILURE;
	}
	NtpAccessNode.NtpsAccessIp.u1AddrLen =
		(UINT1) pFsNtpsAccessIpAddr->i4_Length;
	MEMCPY(NtpAccessNode.NtpsAccessIp.au1Addr,
		   pFsNtpsAccessIpAddr->pu1_OctetList, pFsNtpsAccessIpAddr->i4_Length);
	NtpAccessNode.NtpsAccessIp.u1Afi = (UINT1) i4FsNtpsAccessAddrType;
	NtpAccessNode.u2NtpsAccessIpMask = (UINT2) i4FsNtpsAccessIpMask;
	u1RetVal = NtpsAccessDB(NTPS_GET_ACCESS_TABLE_ENTRY, &NtpAccessNode);
	if (u1RetVal == NTP_FAILURE)
	{
	    CLI_SET_ERR(CLI_NTP_ACCESS_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}
	NtpAccessNode.u2NtpsAccessFlag = (UINT2) i4SetValFsNtpsAccessFlag;
	u1RetVal = NtpsAccessDB(NTPS_SET_ACCESS_TABLE_ENTRY, &NtpAccessNode);
	if (u1RetVal == NTP_FAILURE)
	{    
	    CLI_SET_ERR(CLI_NTP_ACCESS_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}
	return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects */

/****************************************************************************
 Function    :  nmhTestv2FsNtpsAccessRowStatus
 Input       :  The Indices
                FsNtpsAccessAddrType
                FsNtpsAccessIpAddr
                FsNtpsAccessIpMask

                The Object 
                testValFsNtpsAccessRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsNtpsAccessRowStatus(UINT4 * pu4ErrorCode,
									INT4 i4FsNtpsAccessAddrType,
									tSNMP_OCTET_STRING_TYPE *
									pFsNtpsAccessIpAddr,
									INT4 i4FsNtpsAccessIpMask,
									INT4 i4TestValFsNtpsAccessRowStatus)
{
	tNtpsAccess NtpAccessNode;
	UINT1 u1RetVal = 0;
#ifdef IP6_WANTED
    tIp6Addr    ip6addr;
#endif 

	MEMSET(&NtpAccessNode, 0, sizeof(tNtpsAccess));
	if ((pFsNtpsAccessIpAddr == NULL) || (i4FsNtpsAccessIpMask == 0))
	{
	    CLI_SET_ERR(CLI_NTP_ERR_INVALID_IP_ADDR);
		return SNMP_FAILURE;
	}
#ifdef IP6_WANTED
    if (i4FsNtpsAccessAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        Ip6AddrCopy (&ip6addr,
                (tIp6Addr *) (VOID *) pFsNtpsAccessIpAddr->pu1_OctetList);

        if(IS_ADDR_UNSPECIFIED (ip6addr)|| IS_ADDR_V4_COMPAT (ip6addr))
        {
            /* Invalid Address. */
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_IPV6_INVALID_ADDRESS);
            return SNMP_FAILURE;
        }
    } 
#endif 
	NtpAccessNode.NtpsAccessIp.u1AddrLen =
		(UINT1) pFsNtpsAccessIpAddr->i4_Length;
	MEMCPY(NtpAccessNode.NtpsAccessIp.au1Addr,
		   pFsNtpsAccessIpAddr->pu1_OctetList, pFsNtpsAccessIpAddr->i4_Length);
	NtpAccessNode.NtpsAccessIp.u1Afi = (UINT1) i4FsNtpsAccessAddrType;
	NtpAccessNode.u2NtpsAccessIpMask = (UINT2) i4FsNtpsAccessIpMask;
	if ((i4TestValFsNtpsAccessRowStatus == NTP_ACTIVE) ||
		(i4TestValFsNtpsAccessRowStatus == NTP_CREATE_AND_GO) ||
		(i4TestValFsNtpsAccessRowStatus == NTP_CREATE_AND_WAIT))
	{
		*pu4ErrorCode = SNMP_ERR_NO_ERROR;
		return SNMP_SUCCESS;
	}

	if (i4TestValFsNtpsAccessRowStatus == NTP_DESTROY)
	{
		u1RetVal = NtpsAccessDB(NTPS_GET_ACCESS_TABLE_ENTRY, &NtpAccessNode);
		if (u1RetVal == NTP_FAILURE)
		{
	        CLI_SET_ERR(CLI_NTP_ACCESS_NOT_CONFIGURED);
			return SNMP_FAILURE;
		}
		else
		{
			*pu4ErrorCode = SNMP_ERR_NO_ERROR;
			return SNMP_SUCCESS;
		}
	}
	CLI_SET_ERR(CLI_NTP_ERR_INVALID_NTP_ROWSTATUS);
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsNtpsAccessFlag
 Input       :  The Indices
                FsNtpsAccessAddrType
                FsNtpsAccessIpAddr
                FsNtpsAccessIpMask

                The Object 
                testValFsNtpsAccessFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsNtpsAccessFlag(UINT4 * pu4ErrorCode,
							   INT4 i4FsNtpsAccessAddrType,
							   tSNMP_OCTET_STRING_TYPE *
							   pFsNtpsAccessIpAddr,
							   INT4 i4FsNtpsAccessIpMask,
							   INT4 i4TestValFsNtpsAccessFlag)
{
	tNtpsAccess NtpAccessNode;
	UINT1 u1RetVal = 0;

	MEMSET(&NtpAccessNode, 0, sizeof(tNtpsAccess));
	if ((pFsNtpsAccessIpAddr == NULL) || (i4FsNtpsAccessIpMask == 0))
	{
	    CLI_SET_ERR(CLI_NTP_ERR_INVALID_IP_ADDR);
		return SNMP_FAILURE;
	}
	NtpAccessNode.NtpsAccessIp.u1AddrLen =
		(UINT1) pFsNtpsAccessIpAddr->i4_Length;
	MEMCPY(NtpAccessNode.NtpsAccessIp.au1Addr,
		   pFsNtpsAccessIpAddr->pu1_OctetList, pFsNtpsAccessIpAddr->i4_Length);
	NtpAccessNode.NtpsAccessIp.u1Afi = (UINT1) i4FsNtpsAccessAddrType;
	NtpAccessNode.u2NtpsAccessIpMask = (UINT2) i4FsNtpsAccessIpMask;
	u1RetVal = NtpsAccessDB(NTPS_GET_ACCESS_TABLE_ENTRY, &NtpAccessNode);
	if (u1RetVal == NTP_FAILURE)
	{
	    CLI_SET_ERR(CLI_NTP_ACCESS_NOT_CONFIGURED);
		return SNMP_FAILURE;
	}
	if ((i4TestValFsNtpsAccessFlag >= 1) &&
		(i4TestValFsNtpsAccessFlag <= 65535))
	{
		*pu4ErrorCode = SNMP_ERR_NO_ERROR;
		return SNMP_SUCCESS;
	}
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects */

/****************************************************************************
 Function    :  nmhDepv2FsNtpsAccessTable
 Input       :  The Indices
                FsNtpsAccessAddrType
                FsNtpsAccessIpAddr
                FsNtpsAccessIpMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsNtpsAccessTable(UINT4 * pu4ErrorCode,
							   tSnmpIndexList * pSnmpIndexList,
							   tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsNtpsInterfaceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsNtpsInterfaceTable
 Input       :  The Indices
                FsNtpsInterfaceIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsNtpsInterfaceTable(INT4 i4FsNtpsInterfaceIndex)
{
	tNtpsIntf *pNtpIntf = NULL;
	if ((TMO_SLL_Count(&(gNtpsIntfParams.NtpsIntfList)) == NTP_ZERO))
	{

		return SNMP_FAILURE;
	}
	TMO_SLL_Scan(&gNtpsIntfParams.NtpsIntfList, pNtpIntf, tNtpsIntf *)
	{
		if (pNtpIntf->u4NtpsIntfIndex == (UINT4) i4FsNtpsInterfaceIndex)
		{
			return SNMP_SUCCESS;
		}
	}
	return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsNtpsInterfaceTable
 Input       :  The Indices
                FsNtpsInterfaceIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsNtpsInterfaceTable(INT4 * pi4FsNtpsInterfaceIndex)
{
	tNtpsIntf *pNtpIntf = NULL;
	UINT4 *pu4TmpIndex = NTP_ZERO;

	/* Get first index (numerically first index) */
	TMO_SLL_Scan(&gNtpsIntfParams.NtpsIntfList, pNtpIntf, tNtpsIntf *)
	{
		if (pu4TmpIndex == NULL)
		{
			pu4TmpIndex = &(pNtpIntf->u4NtpsIntfIndex);
		}
		else if (pNtpIntf->u4NtpsIntfIndex < *pu4TmpIndex)
		{
			pu4TmpIndex = &(pNtpIntf->u4NtpsIntfIndex);
		}
	}
	if (pu4TmpIndex == NULL)
	{
		return (SNMP_FAILURE);
	}
	*pi4FsNtpsInterfaceIndex = (INT4)*pu4TmpIndex;
	return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsNtpsInterfaceTable
 Input       :  The Indices
                FsNtpsInterfaceIndex
                nextFsNtpsInterfaceIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsNtpsInterfaceTable(INT4 i4FsNtpsInterfaceIndex,
										 INT4 * pi4NextFsNtpsInterfaceIndex)
{
	tNtpsIntf *pNtpIntf = NULL;
	INT4 u4Flag = NTP_ZERO;
	UINT4 *pu4TmpIndex = NTP_ZERO;
	*pi4NextFsNtpsInterfaceIndex = 0;
	pu4TmpIndex = (UINT4 *) & i4FsNtpsInterfaceIndex;
	/* Get first index (numerically first index) */
	TMO_SLL_Scan(&gNtpsIntfParams.NtpsIntfList, pNtpIntf, tNtpsIntf *)
	{
		if (pNtpIntf->u4NtpsIntfIndex > *pu4TmpIndex)
		{
			pu4TmpIndex = &(pNtpIntf->u4NtpsIntfIndex);
			u4Flag = 1;
		}
	}
	if (u4Flag == NTP_ZERO)
	{
		return (SNMP_FAILURE);
	}
	*pi4NextFsNtpsInterfaceIndex = (INT4)*pu4TmpIndex;
	return (SNMP_SUCCESS);

}

/* Low Level GET Routine for All Objects */

/****************************************************************************
 Function    :  nmhGetFsNtpsInterfaceNtpStatus
 Input       :  The Indices
                FsNtpsInterfaceIndex

                The Object 
                retValFsNtpsInterfaceNtpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsNtpsInterfaceNtpStatus(INT4 i4FsNtpsInterfaceIndex,
									INT4 * pi4RetValFsNtpsInterfaceNtpStatus)
{
	tNtpsIntf *pNtpIntf = NULL;

	/* Get first index (numerically first index) */
	TMO_SLL_Scan(&gNtpsIntfParams.NtpsIntfList, pNtpIntf, tNtpsIntf *)
	{
		if (pNtpIntf->u4NtpsIntfIndex == (UINT4) i4FsNtpsInterfaceIndex)
		{
			*pi4RetValFsNtpsInterfaceNtpStatus = (INT4)pNtpIntf->u4NtpsIntfStatus;
			return SNMP_SUCCESS;
		}
	}
	return (SNMP_FAILURE);

}

/* Low Level SET Routine for All Objects */

/****************************************************************************
 Function    :  nmhSetFsNtpsInterfaceNtpStatus
 Input       :  The Indices
                FsNtpsInterfaceIndex

                The Object 
                setValFsNtpsInterfaceNtpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsNtpsInterfaceNtpStatus(INT4 i4FsNtpsInterfaceIndex,
									INT4 i4SetValFsNtpsInterfaceNtpStatus)
{
	tNtpsIntf *pNtpIntf = NULL;

	pNtpIntf = NtpGetIntfNode((UINT4)i4FsNtpsInterfaceIndex);
	if (i4SetValFsNtpsInterfaceNtpStatus == NTP_ENABLE)
	{
		if (pNtpIntf == NULL)
		{
			if (NtpCreateIntfNode((UINT4)i4FsNtpsInterfaceIndex) == NTP_FAILURE)
			{
		        CLI_SET_ERR(CLI_NTP_MAX_INTERFACE);
				return SNMP_FAILURE;
			}
		}
		else
		{
			return SNMP_SUCCESS;
		}
	}
	if (i4SetValFsNtpsInterfaceNtpStatus == NTP_DISABLE)
	{
		pNtpIntf = NtpGetIntfNode((UINT4)i4FsNtpsInterfaceIndex);
		if (pNtpIntf != NULL)
		{
			pNtpIntf->u4NtpsIntfStatus = NTP_DISABLE;
		}
		else
		{
		    CLI_SET_ERR(CLI_NTP_NO_INTERFACE);
			return SNMP_FAILURE;
		}
	}
	return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects */

/****************************************************************************
 Function    :  nmhTestv2FsNtpsInterfaceNtpStatus
 Input       :  The Indices
                FsNtpsInterfaceIndex

                The Object 
                testValFsNtpsInterfaceNtpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsNtpsInterfaceNtpStatus(UINT4 * pu4ErrorCode,
									   INT4 i4FsNtpsInterfaceIndex,
									   INT4 i4TestValFsNtpsInterfaceNtpStatus)
{
	if ((TMO_SLL_Count(&(gNtpsIntfParams.NtpsIntfList)) < MAX_NTPS_INTERFACES))
	{
		if (i4FsNtpsInterfaceIndex > 0)
		{
			*pu4ErrorCode = SNMP_ERR_NO_ERROR;
			return SNMP_SUCCESS;
		}
	}
	if ((i4TestValFsNtpsInterfaceNtpStatus == NTP_ENABLE) ||
		(i4TestValFsNtpsInterfaceNtpStatus == NTP_DISABLE))
	{
		*pu4ErrorCode = SNMP_ERR_NO_ERROR;
		return SNMP_SUCCESS;
	}
	CLI_SET_ERR(CLI_NTP_MAX_INTERFACE);
	*pu4ErrorCode = SNMP_ERR_BAD_VALUE;
	return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects */

/****************************************************************************
 Function    :  nmhDepv2FsNtpsInterfaceTable
 Input       :  The Indices
                FsNtpsInterfaceIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsNtpsInterfaceTable(UINT4 * pu4ErrorCode,
								  tSnmpIndexList * pSnmpIndexList,
								  tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
