/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsntpslw.h,v 1.1 2017/04/03 15:16:32 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsNtpsGlobalStatus ARG_LIST((INT4 *));

INT1
nmhGetFsNtpsServerExecute ARG_LIST((INT4 *));

INT1
nmhGetFsNtpsServerStatus ARG_LIST((INT4 *));

INT1
nmhGetFsNtpsDebug ARG_LIST((INT4 *));

INT1
nmhGetFsNtpsAuthStatus ARG_LIST((INT4 *));

INT1
nmhGetFsNtpsSourceInterface ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsNtpsGlobalStatus ARG_LIST((INT4 ));

INT1
nmhSetFsNtpsServerExecute ARG_LIST((INT4 ));

INT1
nmhSetFsNtpsDebug ARG_LIST((INT4 ));

INT1
nmhSetFsNtpsAuthStatus ARG_LIST((INT4 ));

INT1
nmhSetFsNtpsSourceInterface ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsNtpsGlobalStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsNtpsServerExecute ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsNtpsDebug ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsNtpsAuthStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsNtpsSourceInterface ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsNtpsGlobalStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsNtpsServerExecute ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsNtpsDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsNtpsAuthStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsNtpsSourceInterface ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsNtpsServerTable. */
INT1
nmhValidateIndexInstanceFsNtpsServerTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsNtpsServerTable  */

INT1
nmhGetFirstIndexFsNtpsServerTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsNtpsServerTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsNtpsServerVersion ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsNtpsServerKeyId ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsNtpsServerBurstMode ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsNtpsServerMinPollInt ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsNtpsServerMaxPollInt ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsNtpsServerRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsNtpsServerPrefer ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsNtpsServerVersion ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsNtpsServerKeyId ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsNtpsServerBurstMode ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsNtpsServerMinPollInt ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsNtpsServerMaxPollInt ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsNtpsServerRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsNtpsServerPrefer ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsNtpsServerVersion ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsNtpsServerKeyId ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsNtpsServerBurstMode ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsNtpsServerMinPollInt ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2FsNtpsServerMaxPollInt ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2FsNtpsServerRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsNtpsServerPrefer ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsNtpsServerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsNtpsAuthKeyTable. */
INT1
nmhValidateIndexInstanceFsNtpsAuthKeyTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsNtpsAuthKeyTable  */

INT1
nmhGetFirstIndexFsNtpsAuthKeyTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsNtpsAuthKeyTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsNtpsAuthAlgorithm ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsNtpsAuthKey ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsNtpsAuthTrustedKey ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsNtpsAuthRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsNtpsAuthAlgorithm ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsNtpsAuthKey ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsNtpsAuthTrustedKey ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsNtpsAuthRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsNtpsAuthAlgorithm ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsNtpsAuthKey ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsNtpsAuthTrustedKey ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsNtpsAuthRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsNtpsAuthKeyTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsNtpsAccessTable. */
INT1
nmhValidateIndexInstanceFsNtpsAccessTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsNtpsAccessTable  */

INT1
nmhGetFirstIndexFsNtpsAccessTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsNtpsAccessTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsNtpsAccessRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsNtpsAccessFlag ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsNtpsAccessRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsNtpsAccessFlag ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsNtpsAccessRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsNtpsAccessFlag ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsNtpsAccessTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsNtpsInterfaceTable. */
INT1
nmhValidateIndexInstanceFsNtpsInterfaceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsNtpsInterfaceTable  */

INT1
nmhGetFirstIndexFsNtpsInterfaceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsNtpsInterfaceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsNtpsInterfaceNtpStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsNtpsInterfaceNtpStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsNtpsInterfaceNtpStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsNtpsInterfaceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
