/********************************************************************
 * Copyright (C)  2012 Aricent Inc . All Rights Reserved
 *
 *  $Id: ntpsmac.h,v 1.1 2017/04/03 15:16:36 siva Exp $
 *
 * Description: This file contains the macros required by NTP module.
 *******************************************************************/


#ifndef   _NTPSMAC_H
#define   _NTPSMAC_H 

#define   NTP_ZERO                           0
#define   NTP_ONE                            1
#define   NTP_STATUS_FAIL                    512 

#define   NTP_SUCCESS   		 		0
#define   NTP_FAILURE    				1

#define   SYS_MEM_NTP_AUTH_BLOCK_NUM 8
#define   MAX_IPV6_MASK_LEN 128
#define   NTP_START_PRESTR "service "
#define   NTP_EXE_START_STR " start >/dev/null 2>&1"
#define   NTP_EXE_STOP_STR " stop >/dev/null 2>&1"
#define   NTP_SERVER_STATUS_STR " status "
#define   NTP_FILE_LENGTH 20
#define   NTP_PATH_LENGTH 52

#define   NTP_EXE_START         gNtpGblParams.au1NtpsStartCmd
#define   NTP_EXE_STOP          gNtpGblParams.au1NtpsStopCmd
#define   NTP_SERVER_STATUS     gNtpGblParams.au1NtpsStatusCmd
#define   NTP_NTPEXE_PATH    gNtpGblParams.au1NtpqPath
#define   NTP_NTPSTAT_PATH   gNtpGblParams.au1NtpstatPath
#define   NTP_CONFIG_FILE          "/etc/ntp.conf"
#define   NTP_TMP_FILE             "/tmp/ntpout"
#define   NTP_TMP_CONFIG_FILE          		"/etc/ntp.conf.tmp"
#define   NTP_BKP_CONFIG_FILE          "/etc/ntp.conf.bkp"
#define   NTP_AUTH_CONFIG_FILE     "/etc/ntp.keys"
#define   NTP_TMP_AUTH_CONFIG_FILE     		"/etc/ntp.keys.tmp"
#define   NTP_BKP_AUTH_CONFIG_FILE     "/etc/ntp.keys.bkp"

#define   NTPS_TEMP_BUF 256

#define   NTPS_AUTH_ENABLE  1
#define   NTPS_AUTH_DISABLE 2
/* adding for show commands */
#define   NTP_STATS 					"ntpdc -c sysstats 2>/dev/null"

#define   NTP_PEERS 					"ntpq -p 2>/dev/null"
#define   NTP_ASSOCIATIONS				"ntpq -c associations 2>/dev/null"

#define   NTP_SYNC                      "ntpstat 2>/dev/null"
#define   NTP_NTPD_STATUS               "ntpstat >/dev/null 2>/dev/null"

#define   NTP_CLIENTS                   "ntpdc -c monlist 2>/dev/null"
#define   NTPQ_VERSION                   "ntpq -c 'version' 2>/dev/null"
#define   NTPQ_TIMERSTATS                "ntpdc -c 'timerstats' 2>/dev/null"
#define   NTPQ_SYSINFO                   "ntpdc -c 'sysinfo' 2>/dev/null"
#define   NTPQ_IOSTATS                   "ntpdc -c 'iostats' 2>/dev/null"
#define   NTPQ_STATUS                   "ntpdc -c 'iostats' 2>/dev/null"

#define   NTPS_MAX_INT4                  0x7fffffff
#define NTP_INET_NTOHL(SrvAddr1)\
{\
    UINT4   u4TmpAddr = NTP_ZERO;\
    UINT4   u4Count = NTP_ZERO;\
    UINT1   u1Index = NTP_ZERO;\
    UINT1   au1TmpIp[IPVX_MAX_INET_ADDR_LEN];\
        \
    MEMSET (au1TmpIp,NTP_ZERO , IPVX_MAX_INET_ADDR_LEN);\
    \
    for (u4Count = NTP_ZERO, u1Index = NTP_ZERO; u4Count < IPVX_MAX_INET_ADDR_LEN; \
                      u1Index++, u4Count = u4Count + 4)\
    {\
        MEMCPY (&u4TmpAddr, (SrvAddr1 + u4Count), sizeof(UINT4));\
        u4TmpAddr = OSIX_NTOHL(u4TmpAddr);\
        MEMCPY (&(au1TmpIp[u4Count]), &u4TmpAddr, sizeof(UINT4));\
    }\
    MEMCPY (SrvAddr1, au1TmpIp, IPVX_MAX_INET_ADDR_LEN);\
}

#define NTPFLAGPRINT(DestPtr,DestType,strBuf)\
{\
	if(DestType == 1)\
	{\
		CliPrintf(*(tCliHandle *)DestPtr,"%s ",strBuf);\
	}\
	else\
	{\
		fprintf((FILE*)DestPtr,"%s ",strBuf);\
	}\
	MEMSET(strBuf,0,20);\
}

            /* ROW STATES  */
#define  NTP_ACTIVE              1
#define  NTP_NOT_IN_SERVICE      2
#define  NTP_NOT_READY           3
#define  NTP_CREATE_AND_GO       4
#define  NTP_CREATE_AND_WAIT     5
#define  NTP_DESTROY             6
#define  NTP_SERVER_PREFER_ENABLE 1
#define  NTP_SERVER_PREFER_DISABLE 0
#define  NTP_VERSION1  1
#define  NTP_VERSION2  2
#define  NTP_VERSION3  3
#define  NTP_VERSION4  4 
#define  NTP_SERVER_MINPOLL_MIN  4
#define  NTP_SERVER_MINPOLL_MAX  6
#define  NTP_SERVER_MAXPOLL_MIN  10
#define  NTP_SERVER_MAXPOLL_MAX  17
#define NTP_AUTH_KEY_MAX 65535
#define NTP_AUTH_KEY_MIN 1
#define NTP_AUTH_NONE 0
#define NTP_INVALID_KEY_LEN 0
#define NTP_MAX_KEY_LEN 40
#define NTP_AUTH_MAX_KEY_LENGTH 40
#define NTP_SEM_COUNT    1
#define NTP_SEM_FLAGS    OSIX_DEFAULT_SEM_MODE
#define NTP_TASK_SEM      (const UINT1*)"ntps"
#define NTP_IPVX_LENGTH_FROM_TYPE(t) \
        (((t) == IPVX_ADDR_FMLY_IPV4 )?\
                  IPVX_IPV4_ADDR_LEN:IPVX_IPV6_ADDR_LEN)
#define   NTP_INIT_COMPLETE(u4Status)   lrInitComplete(u4Status)

#define NTPS_SERVER_DB_POOLID   \
        NTPMemPoolIds[MAX_DEFINED_SERVERS_SIZING_ID]
#define NTPS_ACCESS_DB_POOLID   \
        NTPMemPoolIds[MAX_DEFINED_ACCESS_SIZING_ID]
#define NTPS_MAX_KEYS_LIMIT     5
#define NTPS_IFACE_DELETED       0x00000020
#define NTPS_TIMER_EXP_EVENT     0x80000000
#define NTPS_IP_REGISTER_WITH_IP           NetIpv4RegisterHigherLayerProtocol
#define NTPS_IF_CHG_EVENT    	0x10
#define NTPS_INPUT_Q_EVENT      0x20
#define NTPS_IP_DESTROY           32
#define NTPQ 		            "NTPQ"
#define NTPS_TASK_NAME          "NTPS"
#define NTPS_MAX_Q_DEPTH		50
#define NTPS_EVENT_WAIT_FLAG     (OSIX_WAIT | OSIX_EV_ANY)
#define NTPS_IP6_ADDR_PTR_FROM_SLL(sll)  \
        (tIp6AddrInfo *) (VOID *)\
        ((UINT1 *)(sll) - FSAP_OFFSETOF(tIp6AddrInfo, nextAif))

#define NTPS_SET_SYS_TIME(Time) (Time) * (SYS_TIME_TICKS_IN_A_SEC)
#define  NTPS_TIMER_FLAG_SET              1
#define  NTPS_TIMER_FLAG_RESET            0
#define  NTPS_TIMER_FLAG_LOCKED           2
#define  NTPS_STATUS_TIMER_ID             1
#define  NTPS_TIMER_VALUE                 300
/* Timer related macros macro to start timer other than oif timer */
#define NTPS_START_TIMER(TimerId, pTimerNode, Duration, Status) \
{\
   Status = NTP_FAILURE;\
   if (NTPS_TIMER_FLAG_SET != (pTimerNode)->u1TmrStatus) {\
   (pTimerNode)->u1TimerId     = (TimerId);\
   Status = (INT4 ) TmrStartTimer (gNTPsTmrListId,\
                   &((pTimerNode)->TmrLink),\
                   (UINT4)(NTPS_SET_SYS_TIME(Duration)));\
   if (TMR_SUCCESS == Status) {\
       (pTimerNode)->u1TmrStatus   = NTPS_TIMER_FLAG_SET;\
       Status = NTP_SUCCESS;\
   }\
   else {\
       (pTimerNode)->u1TmrStatus   = NTPS_TIMER_FLAG_RESET;\
       Status = NTP_FAILURE;\
   }\
   }\
}
#endif 

