/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 *  $Id: ntpsprot.h,v 1.1 2017/04/03 15:16:36 siva Exp $
 *
 * Description: This file contains macros and structures which will be using in ntp module
 *******************************************************************/

#ifndef   _NTPSPROT_H
#define   _NTPSPROT_H 
INT4 NtpsCliStartServer(tCliHandle CliHandle, UINT4 u4Status);
INT4 NtpsCliStopServer(tCliHandle CliHandle, UINT4 u4Status);
INT4 NtpsCliReStartServer(tCliHandle CliHandle, UINT4 u4Status);
INT4 NtpsSetAdminStatus(INT4 i4SetValNtpsAdminStatus);
INT4 NtpsGetAdminStatus(UINT4 * pu4GetValNtpsAdminStatus);
INT4 NtpShowServersStatus(tCliHandle CliHandle);
INT4 ShowNtpsDetail(tCliHandle CliHandle);
INT4 NtpShowServersStatistics(tCliHandle CliHandle);

INT4 NtpShowSync(tCliHandle CliHandle);

INT4 NtpShowAssociation(tCliHandle CliHandle);

INT4 NtpShowclients(tCliHandle CliHandle);

extern INT4 FsNtpsHwEnableIntf (UINT4 u4status);
INT4 NtpsUpdateIPInfoConfFile(UINT1 *);
INT4 NtpsDeleteIPFromConfFile(UINT1 *);
INT4 NtpsCliUpdateCommandConfFile(UINT1 *);
INT4 NtpDeleteCommandFromConfFile(UINT1 *);
INT4 NtpsCliSetAuth(INT4 i4Status);
INT4
NtspCliSetAuthKey (tCliHandle CliHandle, INT4 i4KeyId,
                UINT1 *pu1Key,UINT4 u4AuthType);
INT4 NtpsCliRemoveKey(tCliHandle CliHandle, INT4 i4KeyId);
INT4
NtpsCliConfigureTrustedkey(tCliHandle CliHandle, UINT2 * pu2KeyList,
						   INT4 i4Trust, UINT4 u4KeyCount);
INT4 NtpUpdateTrustedKeyConfFile(INT4 i4TrustedKey);
INT4 NtpDeleteKeyFromKeyFile(tNtpsAuth * pNtpAuth);
INT4 NtpsCliSetDebug(tCliHandle CliHandle, INT4 i4TraceStatus);
tNtpsServer *GetServer (INT4 i4SrvAddrType, INT4 i4Len, UINT1 *au1IpAddr);
VOID NtpsCreateServerNode (INT4 i4AddrType,
        tSNMP_OCTET_STRING_TYPE * pNtpsNewSrvNode,
        INT4 i4RowStatus);
VOID NtpsDeleteServerNode (tNtpsServer * pNtpsServer);
VOID NtpDeleteAuthKeyNode (tNtpsAuth *pNtpAuth);
tNtpsAuth   * NtpGetAuthKeyNode (INT4 i4KeyId);
INT4 NtpCreateAuthKeyNode (INT4 i4KeyId);
VOID NtpDeleteIntfNode (tNtpsIntf *pNtpIntf);
tNtpsIntf   * NtpGetIntfNode (UINT4 u4IfIndex);
INT4 NtpCreateIntfNode (UINT4 u4IfIndex);
INT4 NtpUpdateKeyConfFile(tNtpsAuth * pNtpAuth);
VOID NtpDeleteKeyFile(VOID);

UINT1 NtpsUpdateServerInfoConfFile(FILE * fp, tNtpsServer * pNtpsServerInfo);
INT4 NtpsUpdateAuthInfoConfFile(tNtpsAuth *pNtpsAuthInfo);
INT4 NtpsMemInit(VOID);
INT4 NtpsMainInit(VOID);
INT4 NtpsLock(VOID);
INT4 NtpsUnLock(VOID);
INT4 NtpsCliSetSourceInt(tCliHandle CliHandle, UINT4 u4IfIndex);
INT4 NtpsServerConfigDBRBCmp(tRBElem * e1, tRBElem * e2);
INT4 NtpsServerCreateRBTree(VOID);
UINT1 NtpsServerDB(UINT1 u1Opcode, tNtpsServer * pNtpsServerDB);
INT4
NtpsSetRestrict(tIPvXAddr *, UINT2 u2Mask, UINT1 u1DefaultRestrict,
				UINT2 u2Flag);
INT4 NtpsNoSetRestrict(tIPvXAddr *, UINT2 u2Mask, UINT1 u1DefaultRestrict,
					   UINT2 u2Flag);
INT4 NtpsCliSetAuthKey(tCliHandle CliHandle, INT4 i4KeyId, UINT1 * pu1Key,
					   UINT4 u4AuthType);
INT4 NtpsCliCreateSource(INT4 i4Index);
INT4 NtpsCliDeleteSource(VOID);
INT4 NtpsAccessCreateRBTree(VOID);
INT4 NtpsAccessConfigDBRBCmp(tRBElem * e1, tRBElem * e2);
UINT1 NtpsAccessDB(UINT1 u1Opcode, tNtpsAccess * pNtpsAccessDB);
INT4 NtpsSetIntStatus(UINT4 u4IfIndex, INT4 i4FsNtpsInterfaceNtpStatus);
VOID NtpsInterfaceRunningConfig(tCliHandle CliHandle);
VOID NtpsShowInterfaceRunningConfig(tCliHandle CliHandle, UINT4 u4IfIndex);
INT4 NtpsUpdateAllConfigdata (VOID);
UINT1 NtpsUpdateAuthDataInConfFile(FILE * ConfFp);
UINT1 NtpsUpdateRestrictDataInConfFile(FILE * ConfFp);
VOID NtpConvertAndPrintAuthFlags(VOID *DestPtr,UINT1 DestType, UINT2 u2Flag);
VOID NtpsHandleIfChg(tNetIpv4IfInfo * pNetIfInfo, UINT4 u4BitMap);
VOID NtpsProcessQMsg (VOID);
INT4 NtpsExecuteNTPCommand(UINT1 *pu1CmdBuf,INT4 *pi4RetVal);
INT4 NtpsExecuteNTPStartCommand(VOID);
INT4 NtpsExecuteNTPStopCommand(VOID);
VOID
NtpsStatusTmrExpHdlr(tNtpsTmrNode * pTmrNode);
INT1 NtpsGetSourceInterface(UINT4 * pu4NtpsSourceInterface);
INT4 NtpsConfigureNP(VOID);
#endif
