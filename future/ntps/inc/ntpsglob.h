/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 *  $Id: ntpsglob.h,v 1.1 2017/04/03 15:16:34 siva Exp $
 *
 * Description: This file contains the global variables that are used  for ntp module
 *******************************************************************/

#ifndef _NTP_GLOBALS_H
#define _NTP_GLOBALS_H

#ifdef NTP_GLOB
/* Global structure variable for maintaining ntp unicast specific details*/
tNtpGlobalParams  gNtpGblParams;
tNtpsServerParams gNtpsServerParams;
tNtpsAuthParams   gNtpsAuthParams; 
tNtpsIntfParams   gNtpsIntfParams; 
tNtpsAccessParams gNtpsAccessParams;
tMemPoolId  gNtpsQPoolId;
UINT4    gu4NtpEnableFlag;
UINT4    gu4NtpServerStatus;
UINT4    gu4NtpServerExecStatus;
UINT4    gu4NtpAuthStatus;
UINT4    gu4NtpSourceInterface;
UINT1    gu1RestrictFlags[20];
/*Declaration of semaphore id which will be used through out ntps module */
tOsixSemId gNtpsSemId = 0;
/* Declaration of ntp task id*/
tOsixTaskId     gu4NtpsTaskId;
tOsixQId        gu4NtpsQId;
tTimerListId    gNTPsTmrListId;
CONST CHR1 *au1NtpAuthTypeName[MAX_AUTH_TYPES] = {
	"NONE",
	"MD5",
	"SHA1",
	"RSA-MD2",
	"RSA-MD5",
	"RSA-SHA",
	"RSA-SHA1",
	"RSA-MDC2",
	"RIPEMD160",
	"DSA-SHA",
	"DSA-SHA1"
};

CONST CHR1 *au1NtpAuthTypeEncName[MAX_AUTH_TYPES] = {
	"NONE",
	"MD5",
	"SHA1",
	"MD2",
	"MD5",
	"SHA",
	"SHA1",
	"MDC2",
	"RIPEMD160",
	"SHA",
	"SHA1"
};

CONST CHR1 *au1ResFlag[NTP_RESTRICT_MAX] =
	{ "NONE", "ignore ", "kod ", "limited ", "lowpriotrap ", "nomodify ",
	"noquery ",
	"nopeer ", "noserve ", "notrap ", "notrust ", "ntpport ", "version "
};


#else
extern tNtpGlobalParams  gNtpGblParams;
extern tMemPoolId  gNtpsQPoolId;
extern UINT4    gu4NtpEnableFlag;
extern UINT4    gu4NtpServerStatus;
extern UINT4    gu4NtpServerExecStatus;
extern UINT4    gu4NtpAuthStatus;
extern UINT4    gu4NtpSourceInterface;
extern tNtpsServerParams gNtpsServerParams;
extern tNtpsAuthParams   gNtpsAuthParams;
extern tNtpsAccessParams gNtpsAccessParams;
extern tNtpsIntfParams   gNtpsIntfParams; 
extern CONST CHR1 *au1NtpAuthTypeName[];
extern CONST CHR1 *au1NtpAuthTypeEncName[];
extern CONST CHR1 *au1ResFlag[];
extern tOsixQId        gu4NtpsQId;
extern tOsixTaskId     gu4NtpsTaskId;
extern tTimerListId    gNTPsTmrListId;
#endif

#endif 
