/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 *  $Id: ntpssz.h,v 1.1 2017/04/03 15:16:37 siva Exp $
 *
 * Description: This file contains macros and structures which will be using in ntp module
 *******************************************************************/

enum
{
    MAX_DEFINED_ACCESS_SIZING_ID,
    MAX_DEFINED_AUTH_SIZING_ID,
    MAX_DEFINED_SERVERS_SIZING_ID,
    MAX_DEFINED_INTF_SIZING_ID,
    MAX_DEFINED_QMSG_SIZING_ID,
    NTP_MAX_SIZING_ID
};


#ifdef  _NTPSZ_C
tMemPoolId NTPMemPoolIds[ NTP_MAX_SIZING_ID];
INT4  NtpSizingMemCreateMemPools(VOID);
VOID  NtpSizingMemDeleteMemPools(VOID);
INT4  NtpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _NTPSZ_C  */
extern tMemPoolId NTPMemPoolIds[ ];
extern INT4  NtpSizingMemCreateMemPools(VOID);
extern VOID  NtpSizingMemDeleteMemPools(VOID);
extern INT4  NtpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif /*  _NTPSZ_C  */


#ifdef  _NTPSZ_C
tFsModSizingParams FsNTPSizingParams [] = {
	{"tNtpsAccess", "MAX_NTPS_ACCESS", sizeof(tNtpsAccess), MAX_NTPS_ACCESS,
	 MAX_NTPS_ACCESS, 0}
	,
	{"tNtpsAuth", "MAX_NTPS_AUTH", sizeof(tNtpsAuth), MAX_NTPS_AUTH,
	 MAX_NTPS_AUTH, 0}
	,
	{"tNtpsServer", "MAX_NTPS_SERVERS", sizeof(tNtpsServer), MAX_NTPS_SERVERS,
	 MAX_NTPS_SERVERS, 0}
	,
	{"tNtpsIntf", "MAX_NTPS_INTERFACES", sizeof(tNtpsIntf),
	 MAX_NTPS_INTERFACES, MAX_NTPS_INTERFACES, 0}
	,
	{"tNtpsQMsg", "MAX_NTPS_Q_DEPTH", sizeof(tNtpsIntf), MAX_NTPS_Q_DEPTH,
	 MAX_NTPS_Q_DEPTH, 0}
	,
{"\0","\0",0,0,0,0}
};
#else  /*  _NTPSZ_C  */
extern tFsModSizingParams FsNTPSizingParams [];
#endif /*  _NTPSZ_C  */


