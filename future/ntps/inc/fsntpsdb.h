/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsntpsdb.h,v 1.1 2017/04/03 15:16:31 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSNTPSDB_H
#define _FSNTPSDB_H

UINT1 FsNtpsServerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsNtpsAuthKeyTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsNtpsAccessTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsNtpsInterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsntps [] ={1,3,6,1,4,1,2076,163};
tSNMP_OID_TYPE fsntpsOID = {8, fsntps};


/* Generated OID's for tables */
UINT4 FsNtpsServerTable [] ={1,3,6,1,4,1,2076,163,2,1};
tSNMP_OID_TYPE FsNtpsServerTableOID = {10, FsNtpsServerTable};


UINT4 FsNtpsAuthKeyTable [] ={1,3,6,1,4,1,2076,163,3,1};
tSNMP_OID_TYPE FsNtpsAuthKeyTableOID = {10, FsNtpsAuthKeyTable};


UINT4 FsNtpsAccessTable [] ={1,3,6,1,4,1,2076,163,4,1};
tSNMP_OID_TYPE FsNtpsAccessTableOID = {10, FsNtpsAccessTable};


UINT4 FsNtpsInterfaceTable [] ={1,3,6,1,4,1,2076,163,5,1};
tSNMP_OID_TYPE FsNtpsInterfaceTableOID = {10, FsNtpsInterfaceTable};




UINT4 FsNtpsGlobalStatus [ ] ={1,3,6,1,4,1,2076,163,1,1};
UINT4 FsNtpsServerExecute [ ] ={1,3,6,1,4,1,2076,163,1,2};
UINT4 FsNtpsServerStatus [ ] ={1,3,6,1,4,1,2076,163,1,3};
UINT4 FsNtpsDebug [ ] ={1,3,6,1,4,1,2076,163,1,4};
UINT4 FsNtpsAuthStatus [ ] ={1,3,6,1,4,1,2076,163,1,5};
UINT4 FsNtpsSourceInterface [ ] ={1,3,6,1,4,1,2076,163,1,6};
UINT4 FsNtpsServerAddrType [ ] ={1,3,6,1,4,1,2076,163,2,1,1,1};
UINT4 FsNtpsServerAddr [ ] ={1,3,6,1,4,1,2076,163,2,1,1,2};
UINT4 FsNtpsServerVersion [ ] ={1,3,6,1,4,1,2076,163,2,1,1,3};
UINT4 FsNtpsServerKeyId [ ] ={1,3,6,1,4,1,2076,163,2,1,1,4};
UINT4 FsNtpsServerBurstMode [ ] ={1,3,6,1,4,1,2076,163,2,1,1,5};
UINT4 FsNtpsServerMinPollInt [ ] ={1,3,6,1,4,1,2076,163,2,1,1,6};
UINT4 FsNtpsServerMaxPollInt [ ] ={1,3,6,1,4,1,2076,163,2,1,1,7};
UINT4 FsNtpsServerRowStatus [ ] ={1,3,6,1,4,1,2076,163,2,1,1,8};
UINT4 FsNtpsServerPrefer [ ] ={1,3,6,1,4,1,2076,163,2,1,1,9};
UINT4 FsNtpsAuthKeyId [ ] ={1,3,6,1,4,1,2076,163,3,1,1,1};
UINT4 FsNtpsAuthAlgorithm [ ] ={1,3,6,1,4,1,2076,163,3,1,1,2};
UINT4 FsNtpsAuthKey [ ] ={1,3,6,1,4,1,2076,163,3,1,1,3};
UINT4 FsNtpsAuthTrustedKey [ ] ={1,3,6,1,4,1,2076,163,3,1,1,4};
UINT4 FsNtpsAuthRowStatus [ ] ={1,3,6,1,4,1,2076,163,3,1,1,5};
UINT4 FsNtpsAccessAddrType [ ] ={1,3,6,1,4,1,2076,163,4,1,1,1};
UINT4 FsNtpsAccessIpAddr [ ] ={1,3,6,1,4,1,2076,163,4,1,1,2};
UINT4 FsNtpsAccessIpMask [ ] ={1,3,6,1,4,1,2076,163,4,1,1,3};
UINT4 FsNtpsAccessRowStatus [ ] ={1,3,6,1,4,1,2076,163,4,1,1,4};
UINT4 FsNtpsAccessFlag [ ] ={1,3,6,1,4,1,2076,163,4,1,1,5};
UINT4 FsNtpsInterfaceIndex [ ] ={1,3,6,1,4,1,2076,163,5,1,1,1};
UINT4 FsNtpsInterfaceNtpStatus [ ] ={1,3,6,1,4,1,2076,163,5,1,1,2};


tSNMP_OID_TYPE FsNtpsGlobalStatusOID = {10, FsNtpsGlobalStatus};


tSNMP_OID_TYPE FsNtpsServerExecuteOID = {10, FsNtpsServerExecute};


tSNMP_OID_TYPE FsNtpsServerStatusOID = {10, FsNtpsServerStatus};


tSNMP_OID_TYPE FsNtpsDebugOID = {10, FsNtpsDebug};


tSNMP_OID_TYPE FsNtpsAuthStatusOID = {10, FsNtpsAuthStatus};


tSNMP_OID_TYPE FsNtpsSourceInterfaceOID = {10, FsNtpsSourceInterface};




tMbDbEntry FsNtpsGlobalStatusMibEntry[]= {

{{10,FsNtpsGlobalStatus}, NULL, FsNtpsGlobalStatusGet, FsNtpsGlobalStatusSet, FsNtpsGlobalStatusTest, FsNtpsGlobalStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData FsNtpsGlobalStatusEntry = { 1, FsNtpsGlobalStatusMibEntry };

tMbDbEntry FsNtpsServerExecuteMibEntry[]= {

{{10,FsNtpsServerExecute}, NULL, FsNtpsServerExecuteGet, FsNtpsServerExecuteSet, FsNtpsServerExecuteTest, FsNtpsServerExecuteDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData FsNtpsServerExecuteEntry = { 1, FsNtpsServerExecuteMibEntry };

tMbDbEntry FsNtpsServerStatusMibEntry[]= {

{{10,FsNtpsServerStatus}, NULL, FsNtpsServerStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsNtpsServerStatusEntry = { 1, FsNtpsServerStatusMibEntry };

tMbDbEntry FsNtpsDebugMibEntry[]= {

{{10,FsNtpsDebug}, NULL, FsNtpsDebugGet, FsNtpsDebugSet, FsNtpsDebugTest, FsNtpsDebugDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData FsNtpsDebugEntry = { 1, FsNtpsDebugMibEntry };

tMbDbEntry FsNtpsAuthStatusMibEntry[]= {

{{10,FsNtpsAuthStatus}, NULL, FsNtpsAuthStatusGet, FsNtpsAuthStatusSet, FsNtpsAuthStatusTest, FsNtpsAuthStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},
};
tMibData FsNtpsAuthStatusEntry = { 1, FsNtpsAuthStatusMibEntry };

tMbDbEntry FsNtpsSourceInterfaceMibEntry[]= {

{{10,FsNtpsSourceInterface}, NULL, FsNtpsSourceInterfaceGet, FsNtpsSourceInterfaceSet, FsNtpsSourceInterfaceTest, FsNtpsSourceInterfaceDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData FsNtpsSourceInterfaceEntry = { 1, FsNtpsSourceInterfaceMibEntry };

tMbDbEntry FsNtpsServerTableMibEntry[]= {

{{12,FsNtpsServerAddrType}, GetNextIndexFsNtpsServerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsNtpsServerTableINDEX, 2, 0, 0, NULL},

{{12,FsNtpsServerAddr}, GetNextIndexFsNtpsServerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsNtpsServerTableINDEX, 2, 0, 0, NULL},

{{12,FsNtpsServerVersion}, GetNextIndexFsNtpsServerTable, FsNtpsServerVersionGet, FsNtpsServerVersionSet, FsNtpsServerVersionTest, FsNtpsServerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsNtpsServerTableINDEX, 2, 0, 0, "4"},

{{12,FsNtpsServerKeyId}, GetNextIndexFsNtpsServerTable, FsNtpsServerKeyIdGet, FsNtpsServerKeyIdSet, FsNtpsServerKeyIdTest, FsNtpsServerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsNtpsServerTableINDEX, 2, 0, 0, NULL},

{{12,FsNtpsServerBurstMode}, GetNextIndexFsNtpsServerTable, FsNtpsServerBurstModeGet, FsNtpsServerBurstModeSet, FsNtpsServerBurstModeTest, FsNtpsServerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsNtpsServerTableINDEX, 2, 0, 0, NULL},

{{12,FsNtpsServerMinPollInt}, GetNextIndexFsNtpsServerTable, FsNtpsServerMinPollIntGet, FsNtpsServerMinPollIntSet, FsNtpsServerMinPollIntTest, FsNtpsServerTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsNtpsServerTableINDEX, 2, 0, 0, NULL},

{{12,FsNtpsServerMaxPollInt}, GetNextIndexFsNtpsServerTable, FsNtpsServerMaxPollIntGet, FsNtpsServerMaxPollIntSet, FsNtpsServerMaxPollIntTest, FsNtpsServerTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsNtpsServerTableINDEX, 2, 0, 0, NULL},

{{12,FsNtpsServerRowStatus}, GetNextIndexFsNtpsServerTable, FsNtpsServerRowStatusGet, FsNtpsServerRowStatusSet, FsNtpsServerRowStatusTest, FsNtpsServerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsNtpsServerTableINDEX, 2, 0, 1, NULL},

{{12,FsNtpsServerPrefer}, GetNextIndexFsNtpsServerTable, FsNtpsServerPreferGet, FsNtpsServerPreferSet, FsNtpsServerPreferTest, FsNtpsServerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsNtpsServerTableINDEX, 2, 0, 0, "2"},
};
tMibData FsNtpsServerTableEntry = { 9, FsNtpsServerTableMibEntry };

tMbDbEntry FsNtpsAuthKeyTableMibEntry[]= {

{{12,FsNtpsAuthKeyId}, GetNextIndexFsNtpsAuthKeyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsNtpsAuthKeyTableINDEX, 1, 0, 0, NULL},

{{12,FsNtpsAuthAlgorithm}, GetNextIndexFsNtpsAuthKeyTable, FsNtpsAuthAlgorithmGet, FsNtpsAuthAlgorithmSet, FsNtpsAuthAlgorithmTest, FsNtpsAuthKeyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsNtpsAuthKeyTableINDEX, 1, 0, 0, "1"},

{{12,FsNtpsAuthKey}, GetNextIndexFsNtpsAuthKeyTable, FsNtpsAuthKeyGet, FsNtpsAuthKeySet, FsNtpsAuthKeyTest, FsNtpsAuthKeyTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsNtpsAuthKeyTableINDEX, 1, 0, 0, NULL},

{{12,FsNtpsAuthTrustedKey}, GetNextIndexFsNtpsAuthKeyTable, FsNtpsAuthTrustedKeyGet, FsNtpsAuthTrustedKeySet, FsNtpsAuthTrustedKeyTest, FsNtpsAuthKeyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsNtpsAuthKeyTableINDEX, 1, 0, 0, "0"},

{{12,FsNtpsAuthRowStatus}, GetNextIndexFsNtpsAuthKeyTable, FsNtpsAuthRowStatusGet, FsNtpsAuthRowStatusSet, FsNtpsAuthRowStatusTest, FsNtpsAuthKeyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsNtpsAuthKeyTableINDEX, 1, 0, 1, NULL},
};
tMibData FsNtpsAuthKeyTableEntry = { 5, FsNtpsAuthKeyTableMibEntry };

tMbDbEntry FsNtpsAccessTableMibEntry[]= {

{{12,FsNtpsAccessAddrType}, GetNextIndexFsNtpsAccessTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsNtpsAccessTableINDEX, 3, 0, 0, NULL},

{{12,FsNtpsAccessIpAddr}, GetNextIndexFsNtpsAccessTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsNtpsAccessTableINDEX, 3, 0, 0, NULL},

{{12,FsNtpsAccessIpMask}, GetNextIndexFsNtpsAccessTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsNtpsAccessTableINDEX, 3, 0, 0, NULL},

{{12,FsNtpsAccessRowStatus}, GetNextIndexFsNtpsAccessTable, FsNtpsAccessRowStatusGet, FsNtpsAccessRowStatusSet, FsNtpsAccessRowStatusTest, FsNtpsAccessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsNtpsAccessTableINDEX, 3, 0, 1, NULL},

{{12,FsNtpsAccessFlag}, GetNextIndexFsNtpsAccessTable, FsNtpsAccessFlagGet, FsNtpsAccessFlagSet, FsNtpsAccessFlagTest, FsNtpsAccessTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsNtpsAccessTableINDEX, 3, 0, 0, NULL},
};
tMibData FsNtpsAccessTableEntry = { 5, FsNtpsAccessTableMibEntry };

tMbDbEntry FsNtpsInterfaceTableMibEntry[]= {

{{12,FsNtpsInterfaceIndex}, GetNextIndexFsNtpsInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsNtpsInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsNtpsInterfaceNtpStatus}, GetNextIndexFsNtpsInterfaceTable, FsNtpsInterfaceNtpStatusGet, FsNtpsInterfaceNtpStatusSet, FsNtpsInterfaceNtpStatusTest, FsNtpsInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsNtpsInterfaceTableINDEX, 1, 0, 0, "0"},
};
tMibData FsNtpsInterfaceTableEntry = { 2, FsNtpsInterfaceTableMibEntry };

#endif /* _FSNTPSDB_H */

