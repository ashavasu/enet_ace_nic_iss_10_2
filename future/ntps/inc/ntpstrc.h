/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: ntpstrc.h,v 1.1 2017/04/03 15:16:39 siva Exp $
 * *
 * * Description: NTP File.
 * *********************************************************************/
#ifndef _NTPTRC_H_
#define _NTPTRC_H_
/* Trace and debug flags */
#define   NTP_TRC_FLAG      gNtpGblParams.u4NtpDbgFlag

/* Module names */
#define   NTP_MOD_NAME           ((const char *)"NTP")

#define SYSLOG_NTP_MSG(level, TraceType,fmt) \
{\
    if((STRLEN (fmt) ) < NTPS_TEMP_BUF)\
    {\
        UtlSysTrcLog(level, NTP_TRC_FLAG, TraceType, NTP_MOD_NAME, NTP_MOD_NAME, fmt);\
    }\
}


/* Trace definitions */
#ifdef  TRACE_WANTED

#define NTP_TRC(TraceType, Str)                                              \
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, NTP_TRC_FLAG, TraceType, NTP_MOD_NAME, NTP_MOD_NAME, (const char *)Str);\

#define NTP_TRC_ARG1(TraceType, Str, Arg1)                                   \
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, NTP_TRC_FLAG, TraceType, NTP_MOD_NAME, NTP_MOD_NAME, (const char *)Str, Arg1);\

#define NTP_TRC_ARG2(TraceType, Str, Arg1, Arg2)                             \
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, NTP_TRC_FLAG, TraceType, NTP_MOD_NAME, NTP_MOD_NAME, (const char *)Str, Arg1,Arg2);\

#define NTP_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)                       \
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, NTP_TRC_FLAG, TraceType, NTP_MOD_NAME, NTP_MOD_NAME, (const char *)Str, Arg1,Arg3);\

#define NTP_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3,Arg4)                       \
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, NTP_TRC_FLAG, TraceType, NTP_MOD_NAME, NTP_MOD_NAME, (const char *)Str, Arg1,Arg4);\

#define NTP_TRC_ARG5(TraceType, Str, Arg1, Arg2, Arg3,Arg4,Arg5)                       \
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, NTP_TRC_FLAG, TraceType, NTP_MOD_NAME, NTP_MOD_NAME, (const char *)Str, Arg1,Arg5);\

#define NTP_TRC_ARG6(TraceType, Str, Arg1, Arg2, Arg3,Arg4,Arg5,Arg6)                       \
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, NTP_TRC_FLAG, TraceType, NTP_MOD_NAME, NTP_MOD_NAME, (const char *)Str, Arg1,Arg5,Arg6);\

#define   NTP_DEBUG(x) {x}

#else  /* TRACE_WANTED */

#define NTP_TRC(TraceType, Str)
#define NTP_TRC_ARG1(TraceType, Str, Arg1)
#define NTP_TRC_ARG2(TraceType, Str, Arg1, Arg2)
#define NTP_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)
#define NTP_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3,Arg4)
#define NTP_TRC_ARG5(TraceType, Str, Arg1, Arg2, Arg3,Arg4,Arg5)
#define NTP_TRC_ARG6(TraceType, Str, Arg1, Arg2, Arg3,Arg4,Arg5,Arg6)

#define   NTP_DEBUG(x)

#endif /* TRACE_WANTED */

#endif/* _NTPTRC_H_ */

