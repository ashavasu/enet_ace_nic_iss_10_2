/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsntpswr.h,v 1.1 2017/04/03 15:16:33 siva Exp $
 *
 * Description: Proto types for Wrapper Routines
 * *********************************************************************/

#ifndef _FSNTPSWR_H
#define _FSNTPSWR_H

VOID RegisterFSNTPS(VOID);

VOID UnRegisterFSNTPS(VOID);
INT4 FsNtpsGlobalStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerExecuteGet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsDebugGet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsAuthStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsSourceInterfaceGet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsGlobalStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerExecuteSet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsDebugSet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsAuthStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsSourceInterfaceSet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsGlobalStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerExecuteTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNtpsDebugTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNtpsAuthStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNtpsSourceInterfaceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNtpsGlobalStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsNtpsServerExecuteDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsNtpsDebugDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsNtpsAuthStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsNtpsSourceInterfaceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsNtpsServerTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsNtpsServerVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerKeyIdGet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerBurstModeGet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerMinPollIntGet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerMaxPollIntGet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerPreferGet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerVersionSet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerKeyIdSet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerBurstModeSet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerMinPollIntSet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerMaxPollIntSet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerPreferSet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerVersionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerKeyIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerBurstModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerMinPollIntTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerMaxPollIntTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerPreferTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNtpsServerTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsNtpsAuthKeyTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsNtpsAuthAlgorithmGet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsAuthKeyGet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsAuthTrustedKeyGet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsAuthRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsAuthAlgorithmSet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsAuthKeySet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsAuthTrustedKeySet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsAuthRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsAuthAlgorithmTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNtpsAuthKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNtpsAuthTrustedKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNtpsAuthRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNtpsAuthKeyTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsNtpsAccessTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsNtpsAccessRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsAccessFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsAccessRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsAccessFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsAccessRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNtpsAccessFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNtpsAccessTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsNtpsInterfaceTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsNtpsInterfaceNtpStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsInterfaceNtpStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsNtpsInterfaceNtpStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNtpsInterfaceTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _FSNTPSWR_H */
