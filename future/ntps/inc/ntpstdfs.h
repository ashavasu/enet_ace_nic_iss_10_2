/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
*$Id: ntpstdfs.h,v 1.1 2017/04/03 15:16:38 siva Exp $
*
* Description: Data structure type definitions Routines
*********************************************************************/


#ifndef NTPSTDFS_H
#define NTPSTDFS_H

/* This structure holds the timer related information */ 
typedef struct _NtpsTmrNode {
    tTmrAppTimer  TmrLink; /* Link node in the list of timers 
                            * timers running for timer list created
                            * by NTP (gNTPsTmrListId)
                            */
    UINT1         u1TimerId; /* The timer ID of the timer that is running,
                              */
    UINT1         u1TmrStatus; /* The Status of the timer whether the
                                * timer is running (SET) or not running
                                * (RESET)
                                */
    UINT2         u2Reserved; /* Alignment Data */
    
} tNtpsTmrNode; 


typedef struct
{
    tNtpsTmrNode     NtpsStatusTmr;
	UINT1 au1NtpsStartCmd[NTP_PATH_LENGTH];
	UINT1 au1NtpsStopCmd[NTP_PATH_LENGTH];
	UINT1 au1NtpsStatusCmd[NTP_PATH_LENGTH];
	UINT1 au1NtpqPath[NTP_PATH_LENGTH];
	UINT1 au1NtpstatPath[NTP_PATH_LENGTH];
    UINT4      u4NtpTrcFlag;
    UINT4      u4NtpDbgFlag;
    INT4       i4SysLogId;
	UINT1	   au1Pad[4];
} tNtpGlobalParams;

/* Server table data structure */

typedef struct
{
    tRBNodeEmbd  RBSerNode;    
    tIPvXAddr  NtpsServerAddr;
    UINT4  u4NtpsServerAddrType ;
    UINT4  u4NtpsServerVersion;
    UINT4 u4NtpsServerKeyId ;
    UINT4 u4NtpsServerBurstMode;
    UINT4  u4NtpsServerMinPollInt ;
    UINT4  u4NtpsServerMaxPollInt ;
    INT4   i4NtpsServerPrefer;
    UINT4  u4NtpsServerRowStatus ;
	UINT1	   au1Pad[4];
}tNtpsServer;

typedef struct
{
    tRBTree NtpsServerList;
    tMemPoolId  NtpsSerPoolId;
	UINT1	   au1Pad[4];
}tNtpsServerParams;

typedef struct
{
    tTMO_SLL_NODE   NextNode;
    UINT4 u4NtpsIntfIndex;
    UINT4 u4NtpsIntfStatus;
}tNtpsIntf;

typedef struct
{
    tTMO_SLL NtpsIntfList;
    tMemPoolId  NtpsPoolId;
	UINT1	   au1Pad[4];
}tNtpsIntfParams;


/* Auth table data structure */
typedef struct
{
    tTMO_SLL_NODE   NextNode;
    UINT4 u4NtpsAuthKeyId;
    UINT4 u4NtpsAuthAlgorithm ;
	UINT1 u1NtpsAuthKey[NTP_KEY_MAX_LENGTH];
    UINT4  u4NtpsAuthTrustedKey;
    UINT4   u4NtpsAuthRowStatus;
}tNtpsAuth;

typedef struct
{
    tTMO_SLL NtpsAuthList;
    tMemPoolId  NtpsPoolId;
	UINT1	   au1Pad[4];
}tNtpsAuthParams;

/* Access table data structure */
typedef struct
{
    tRBNodeEmbd  RBAccNode;    
    tIPvXAddr  NtpsAccessIp;
    UINT2  u2NtpsAccessIpMask;
    UINT2  u2NtpsAccessFlag;
    UINT2  u2NtpsAccessRowStatus;
	UINT1	   au1Pad[6];
}tNtpsAccess;

typedef struct
{
    tRBTree NtpsAccessList;
    tMemPoolId  NtpsAccessPoolId;
	UINT1	   au1Pad[4];
}tNtpsAccessParams;

 /* Enum types for server RB Tree */
typedef enum
{
    NTPS_CREATE_SERVER_TABLE_ENTRY,
    NTPS_DESTROY_SERVER_TABLE_ENTRY,
    NTPS_SET_SERVER_TABLE_ENTRY,
    NTPS_GET_SERVER_TABLE_ENTRY,
    NTPS_GET_FIRST_SERVER_TABLE_ENTRY,
    NTPS_GET_NEXT_SERVER_TABLE_ENTRY
}eNtpsDBType;

 /* Enum types for ACCESS RB Tree */
typedef enum
{
    NTPS_CREATE_ACCESS_TABLE_ENTRY,
    NTPS_DESTROY_ACCESS_TABLE_ENTRY,
    NTPS_SET_ACCESS_TABLE_ENTRY,
    NTPS_GET_ACCESS_TABLE_ENTRY,
    NTPS_GET_FIRST_ACCESS_TABLE_ENTRY,
    NTPS_GET_NEXT_ACCESS_TABLE_ENTRY
}eNtpsAccDBType;

/* structure for Interface status */
typedef struct _NtpsIfStatusInfo
{
    UINT1  u1MsgType;
    UINT1  u1IfStatus;
    UINT2  u2AlignByte;
    UINT4  u4IfIndex;
    UINT4  u4CfaIfIndex;
    UINT2  u2VlanId;
    UINT1  u1CfaIfType;
    UINT1  u1Pad;
} tNtpsIfStatusInfo;

/* Structure for the NTPS Q Message */
/* --------------------------------*/
typedef struct _NtpsQMsg
{
        UINT4  u4MsgType ;
        UINT1  u1AddrType ;
        UINT1   au1Pad[3];
        tCRU_BUF_CHAIN_HEADER   *NtpsIfParam;
}tNtpsQMsg;

#endif  /* NTPSTDFS_H */
