
/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 *  $Id: ntpsinc.h,v 1.1 2017/04/03 15:16:35 siva Exp $
 *
 * Description: This file contains the "include files" required for ntp module. 
 *******************************************************************/

#ifndef _NTPINC_H
#define _NTPINC_H

#include "lr.h"
#include "cli.h"
#include "iss.h"
#include "ip.h"
#include "vcm.h"
#include "ipvx.h"
#include "utilipvx.h"
#include <selutil.h>
#include <netinet/in.h>
#include "ntps.h"
#include "ntpscli.h"
#include "fsntpslw.h"
#include "fsntpswr.h"
#include "ntpsmac.h"
#include "ntpstdfs.h"
#include "ntpsprot.h"
#include "ntpssz.h"
#include "ntpsglob.h"
#include "ntpstrc.h"
#include "ip6util.h"
#include "ip6cli.h"
#include "npapi/ipnp.h"
#include "ipnpwr.h"
#include "fssyslog.h"
#endif
