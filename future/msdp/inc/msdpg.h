
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: msdpg.h,v 1.1.1.1 2011/01/17 05:57:32 siva Exp $
 *
 * Description: This file contains prototypes of  MSDP module
 *              LOCK and UNLOCK functions.
 *******************************************************************/

#define MSDP_LOCK  MsdpMainTaskLock ()

#define MSDP_UNLOCK  MsdpMainTaskUnLock ()
