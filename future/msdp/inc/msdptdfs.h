/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: msdptdfs.h,v 1.4 2012/04/27 12:09:39 siva Exp $
*
* Description: This file contains type definitions for Msdp module.
*********************************************************************/
#ifndef __MSDPTDFS_H__
#define __MSDPTDFS_H__

/* -------------------------------------------------------
 *
 *                MSDP   Data Structures
 *
 * ------------------------------------------------------*/
typedef struct __MsdpEventTrap { 
    UINT1        au1RtrId[IPVX_IPV4_ADDR_LEN];
    UINT1        au1PeerAddr[IPVX_MAX_INET_ADDR_LEN];
    INT4         i4AddrType;
    UINT4        u4EventId;    
} tMsdpEventTrap;

typedef enum
{
  MSDP_REDIST_PERMIT,
  MSDP_REDIST_DENY
}eMdspRedisFiltStatus;

typedef enum {
     /* MSDP_V4_PASSIVE_CONN_EVT,
      MSDP_V6_PASSIVE_CONN_EVT,*/
      MSDP_ADMIN_EVT,
      MSDP_PEER_CONFIG_EVT,
      MSDP_ACTIVE_CONN_EVT,
      MSDP_SOCK_READ_WRITE_EVT,
      MSDP_PIM_UPDATE_SA_EVT,
      MSDP_PIM_REQUEST_SA_EVT,
      MSDP_IF_CHG_EVT
}eMsdpQMsgType;


/*structure for all scalars in the mib*/
typedef struct
{
 INT4 i4FsMsdpTraceLevel;
 INT4 i4FsMsdpIPv4AdminStat;
 INT4 i4FsMsdpIPv6AdminStat;
 UINT4 u4FsMsdpCacheLifetime;
 UINT4 u4FsMsdpNumSACacheEntries;
        UINT4 u4FsMsdpRtrId;
 INT4 i4FsMsdpMaxPeerSessions;
 INT4 i4FsMsdpMappingComponentId;
 INT4 i4FsMsdpListenerPort;
 INT4 i4FsMsdpPeerFilter;
        INT4 i4FsMsdpPeerCount;
        INT4 i4FsMsdpStatEstPeerCount;
 tRBTree   FsMsdpPeerTable;
 tRBTree   FsMsdpSACacheTable;
 tRBTree   FsMsdpMeshGroupTable;
 tRBTree   FsMsdpRPTable;
 tRBTree   FsMsdpPeerFilterTable;
 tRBTree   FsMsdpSARedistributionTable;
} tMsdpGlbMib;

/* constants for timer types */
typedef enum {
        MSDP_KEEP_ALIVE_TMR = 0,
        MSDP_CONN_RETRY_TMR ,
        MSDP_PEER_HOLD_TMR ,
        MSDP_CACHE_STATE_TMR ,
        MSDP_ACTIVE_SRC_ADVT_TMR ,
        MSDP_MAX_TMRS
} enMsdpTmrId;


typedef struct _MSDP_TMR_DESC {
    VOID                (*pTmrExpFunc)(VOID *);
    INT2                i2Offset;
                            /* If this field is -1 then the fn takes no
                             * parameter
                             */
    UINT2               u2Pad;
                            /* Included for 4-byte Alignment
                             */
} tMsdpTmrDesc;

typedef struct _MSDP_TIMER {
       tTmrAppTimer    tmrNode;
       enMsdpTmrId     eMsdpTmrId;
} tMsdpTmr;

typedef struct {
    UINT1         au1Msg[MSDP_PEER_MAX_RX_TX_BUF_LEN];
} tMsdpFsMsdpPeerRxTxBuf;

typedef struct {
    UINT1           *pu1Msg;  /* Pointer of the send buffer */
    UINT2           u2MsgSize; /* Message size of the residual buffer */
    UINT2           u2MsgOffset; /* Residual buffer message offset.*/
} tMsdpTxBuf;


typedef struct {
    UINT1           *pu1Msg;  /* Pointer of the Rx buffer */
    UINT2           u2MsgSize; /* Message size of the residual buffer */
    UINT2           u2MsgOffset; /* Residual buffer message offset.*/
} tMsdpRxBuf;

typedef struct
{
 tMsdpMibFsMsdpPeerEntry       MibObject;
        tMsdpTmr  MsdpPeerKATimer;
        tMsdpTmr  MsdpPeerHoldTimer;
        tMsdpTmr  MsdpPeerConnRetryTimer;
        tMsdpTxBuf  TxBufInfo;
        tMsdpRxBuf  RxBufInfo;
        INT4        i4SockFd;
        INT4        i4IfIndex;
} tMsdpFsMsdpPeerEntry;


/* for the msg from Sel task to MSDP */
typedef struct {
    INT4       i4Cmd;
    INT4       i4SockFd;
}tMsdpTcpMsg;



typedef struct {
       tIPvXAddr    PeerAddr;
       UINT1        u1AddrType;
       UINT1        u1PeerAdmin;
       UINT1        au1Pad[2];
}tMsdpConfigPeer;

typedef struct {
       INT4         i4MsdpAdminStatus;
       UINT1        u1AddrType;
       UINT1        u1Pad[3];
}tMsdpConfigAdmin;


typedef struct _MsdpIfStatusInfo {
    UINT4  u4IfIndex;
    UINT1  u1IfOperStatus;
    UINT1  u1IfAdminStatus;
    UINT1  u1AddrType;
    UINT1  u1BytePad;
} tMsdpIfStatusInfo;

typedef struct {
    eMsdpQMsgType  eMsgType;
    union {
       tMsdpIfStatusInfo   MsdpIfStatus;
       tMsdpTcpMsg         MsdpTcpMsg;
       tMsdpSAReqGrp       MsdpSAReqGrpAddr;
       tMsdpMrpSaAdvt      MsdpMrpSaInfo;
    }unMsdpMsgIfParam;
}tMsdpQueMsg;

typedef struct
{
 tMsdpMibFsMsdpSACacheEntry       MibObject;
        tMsdpTmr  CacheSaStateTimer;
} tMsdpFsMsdpSACacheEntry;

/* for the cache update to the cache handler */
typedef struct {
    tMsdpFsMsdpPeerEntry  *pLrndFrmPeer;
    tIPvXAddr                 OriginatorRP;
    tIPvXAddr                 SrcAddr;
    tIPvXAddr                 GrpAddr;
    INT1                      i1Flag;  /* From SA/ SA-Resp/ PIM */
    UINT1                     u1AddrType;
    UINT1                     u1Action;
    UINT1                     u1Pad;
}tMsdpCacheUpdt;




typedef struct
{
    UINT1             u1ProtId;          /* multicast routing protocol
                                            identifier */
    UINT1             u1Flag;            /* to tell whether already registered
                                            or not */
    UINT1             au1Pad[2];
    VOID (*pUpdateSrcActiveInfo) ( tMsdpMrpSaAdvt *);
} tMsdpRegister;


typedef struct
{
 tMsdpMibFsMsdpMeshGroupEntry       MibObject;
} tMsdpFsMsdpMeshGroupEntry;


typedef struct
{
 tMsdpMibFsMsdpRPEntry       MibObject;
        INT4   i4IfIndex;
} tMsdpFsMsdpRPEntry;


typedef struct
{
 tMsdpMibFsMsdpPeerFilterEntry       MibObject;
} tMsdpFsMsdpPeerFilterEntry;


typedef struct
{
 tMsdpMibFsMsdpSARedistributionEntry       MibObject;
} tMsdpFsMsdpSARedistributionEntry;

typedef struct {
    tIPvXAddr      OrgRpAddr;
    tIPvXAddr      SrcAddr;
    tIPvXAddr      GrpAddr;
}tMsdpCacheAdvtMarker; 

#ifndef PACK_REQUIRED
#pragma pack(1)
#endif 

typedef struct {
    UINT1   u1Type;
    UINT2   u2Len;
    UINT1   u1EntryCnt;
    UINT1   au1RpAddr[16];
}tMsdpSaAdvtHdr;

typedef struct {
    UINT1   au1Reserved[3];
    UINT1   u1Prefix;
    UINT1   au1GrpAddr[4];
    UINT1   au1SrcAddr[4];
}tMsdpSaAdvtEntry;

typedef struct {
    UINT1   au1Reserved[3];
    UINT1   u1Prefix;
    UINT1   au1GrpAddr[16];
    UINT1   au1SrcAddr[16];
}tMsdp6SaAdvtEntry;

typedef struct {
    UINT1   u1Type;
    UINT2   u2Len;
    UINT1   u1Rsvd;
    UINT1   au1GrpAddr[16];
}tMsdpSaReqPkt;


typedef struct  {
    tSNMP_OCTET_STRING_TYPE MeshStr;
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    UINT1               au1MeshStr[MSDP_MAX_BUFFER];
    UINT1               au1PeerAddr[MSDP_MAX_BUFFER];
    INT4                i4AddrType;
    } tMesh;



#ifndef PACK_REQUIRED
#pragma pack()
#endif 

typedef struct MSDP_GLOBALS {
 tTimerListId        msdpTmrLst;
 UINT4               u4MsdpTrc;
 tOsixTaskId         msdpTaskId;
 UINT1               au1TaskSemName[8];
 tOsixSemId          msdpTaskSemId;
 tOsixQId            msdpQueId;
 tMsdpGlbMib          MsdpGlbMib;
        tMsdpTmr             MsdpSaAdvtTimer;
        tMsdpCacheAdvtMarker CacheAdvtMarker;
        tMsdpRegister        aMsdpRegister[MSDP_MAX_REGISTER];
        INT4     i4MsdpSrvSockId;
        INT4     i4Msdpv6SrvSockId;
} tMsdpGlobals;


#endif  /* __MSDPTDFS_H__ */
/*-----------------------------------------------------------------------*/




