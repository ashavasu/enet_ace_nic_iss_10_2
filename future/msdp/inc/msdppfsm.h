/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: msdppfsm.h,v 1.2 2011/01/21 15:04:10 siva Exp $
 *
 * Description:This file contains constants, macros, typdefinitions
 *             and static varible declaration specific to msdpeer.c
 *
 *******************************************************************/


#ifndef _MSDPPFSM_H
#define _MSDPPFSM_H


/* PFSM Action (A) routine numbers */
#define  A0           0
#define  A1           1
#define  A2           2
#define  A3           3
#define  A4           4
#define  A5           5
#define  A6           6
#define  MSDP_MAX_PFSM_FUNC  7


static UINT1  au1PfsmTable[MSDP_MAX_PEER_EVENT][MSDP_MAX_PEER_STATE]  = {
/*______________________________________________________________________________

            INVALID   INACTIVE  LISTEN  CONNECTING  ESTABLISHED DISABLED
________________________________________________________________________________
START
*/         {  A0    ,   A0   ,   A0   ,      A0   ,       A0  ,   A1   },
/*______________________________________________________________________________
ACTIVE OPEN
*/         {  A0    ,  A2     ,  A0   ,      A0   ,       A0  ,   A0   },
/*______________________________________________________________________________
PASSIVE OPEN
*/         {  A0    ,  A3     ,  A0   ,      A0   ,       A0  ,   A0   },
/*______________________________________________________________________________
CONN ESTAB
*/         {  A0    ,  A0     ,  A4   ,      A4   ,       A0  ,   A0   },
/*______________________________________________________________________________
CONN RETRY  TMR
EXPIRY */  {  A0    ,  A0     ,  A0   ,      A6   ,       A0  ,   A0   },
/*______________________________________________________________________________
STOP
*/         {  A0    ,  A0     ,  A5   ,      A5   ,       A5  ,   A0   },
/*______________________________________________________________________________
HOLD TMR
EXPIRY */  {  A0    ,  A0     ,  A0  ,       A0   ,       A5  ,   A0   },
/*______________________________________________________________________________
TLV ERROR
*/         {  A0    ,  A0     ,  A0  ,       A0   ,       A5  ,   A0   },
/*______________________________________________________________________________
*/
          } ;
/* type of the functions which perform the action in NBRSM */
typedef  VOID (*tPfsmFunc)(tMsdpFsMsdpPeerEntry *);

/* array of pointers to functions performing actions during NBRSM transitions */

tPfsmFunc aPfsmFunc[MSDP_MAX_PFSM_FUNC] = {
                             /* A0 */           MsdpPeerFsmInvalid,
                             /* A1 */           MsdpPeerFsmPeerEnable,
                             /* A2 */           MsdpPeerFsmInitiateConnection,
                             /* A3 */           MsdpPeerFsmPassiveOpen,
                             /* A4 */           MsdpPeerFsmHandleEstablished,
                             /* A5 */           MsdpPeerFsmPeerStop,
                             /* A6 */           MsdpPeerFsmHdlConnRetryExpiry
                                         };

#endif

