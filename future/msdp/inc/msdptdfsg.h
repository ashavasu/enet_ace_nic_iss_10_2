/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
 *  $Id: msdptdfsg.h,v 1.3 2012/03/06 11:26:03 siva Exp $
*
* Description: This file contains data structures defined for Msdp module.
*********************************************************************/
/* Structure used by CLI to indicate which 
 all objects to be set in FsMsdpPeerEntry */

typedef struct
{
 BOOL1  bFsMsdpPeerAddrType;
 BOOL1  bFsMsdpPeerRemoteAddress;
 BOOL1  bFsMsdpPeerLocalAddress;
 BOOL1  bFsMsdpPeerConnectRetryInterval;
 BOOL1  bFsMsdpPeerHoldTimeConfigured;
 BOOL1  bFsMsdpPeerKeepAliveConfigured;
 BOOL1  bFsMsdpPeerDataTtl;
 BOOL1  bFsMsdpPeerStatus;
 BOOL1  bFsMsdpPeerEncapsulationType;
 BOOL1  bFsMsdpPeerMD5AuthPassword;
 BOOL1  bFsMsdpPeerMD5AuthPwdStat;
 BOOL1  bFsMsdpPeerAdminStatus;
} tMsdpIsSetFsMsdpPeerEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsMsdpSACacheEntry */

typedef struct
{
 BOOL1  bFsMsdpSACacheAddrType;
 BOOL1  bFsMsdpSACacheGroupAddr;
 BOOL1  bFsMsdpSACacheSourceAddr;
 BOOL1  bFsMsdpSACacheOriginRP;
 BOOL1  bFsMsdpSACacheStatus;
} tMsdpIsSetFsMsdpSACacheEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsMsdpMeshGroupEntry */

typedef struct
{
 BOOL1  bFsMsdpMeshGroupName;
 BOOL1  bFsMsdpMeshGroupAddrType;
 BOOL1  bFsMsdpMeshGroupPeerAddress;
 BOOL1  bFsMsdpMeshGroupStatus;
} tMsdpIsSetFsMsdpMeshGroupEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsMsdpRPEntry */

typedef struct
{
 BOOL1  bFsMsdpRPAddrType;
 BOOL1  bFsMsdpRPAddress;
 BOOL1  bFsMsdpRPStatus;
} tMsdpIsSetFsMsdpRPEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsMsdpPeerFilterEntry */

typedef struct
{
 BOOL1  bFsMsdpPeerFilterAddrType;
 BOOL1  bFsMsdpPeerFilterRouteMap;
 BOOL1  bFsMsdpPeerFilterStatus;
} tMsdpIsSetFsMsdpPeerFilterEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsMsdpSARedistributionEntry */

typedef struct
{
 BOOL1  bFsMsdpSARedistributionAddrType;
 BOOL1  bFsMsdpSARedistributionStatus;
 BOOL1  bFsMsdpSARedistributionRouteMap;
 BOOL1  bFsMsdpSARedistributionRouteMapStat;
} tMsdpIsSetFsMsdpSARedistributionEntry;



/* Structure used by Msdp protocol for FsMsdpPeerEntry */

typedef struct
{
 tRBNodeEmbd  FsMsdpPeerTableNode;
 UINT4 u4FsMsdpPeerRPFFailures;
 UINT4 u4FsMsdpPeerInSAs;
 UINT4 u4FsMsdpPeerOutSAs;
 UINT4 u4FsMsdpPeerInSARequests;
 UINT4 u4FsMsdpPeerOutSARequests;
 UINT4 u4FsMsdpPeerInControlMessages;
 UINT4 u4FsMsdpPeerOutControlMessages;
 UINT4 u4FsMsdpPeerInDataPackets;
 UINT4 u4FsMsdpPeerOutDataPackets;
 UINT4 u4FsMsdpPeerFsmEstablishedTransitions;
 UINT4 u4FsMsdpPeerFsmEstablishedTime;
 UINT4 u4FsMsdpPeerInMessageTime;
 UINT4 u4FsMsdpPeerConnectionAttempts;
 UINT4 u4FsMsdpPeerDiscontinuityTime;
 UINT4 u4FsMsdpPeerInSAResponses;
 UINT4 u4FsMsdpPeerOutSAResponses;
 UINT4 u4FsMsdpPeerUpTime;
 UINT4 u4FsMsdpPeerInKeepAliveCount;
 UINT4 u4FsMsdpPeerOutKeepAliveCount;
 UINT4 u4FsMsdpPeerDataTtlErrorCount;
 INT4 i4FsMsdpPeerAddrType;
 INT4 i4FsMsdpPeerState;
 INT4 i4FsMsdpPeerConnectRetryInterval;
 INT4 i4FsMsdpPeerHoldTimeConfigured;
 INT4 i4FsMsdpPeerKeepAliveConfigured;
 INT4 i4FsMsdpPeerDataTtl;
 INT4 i4FsMsdpPeerStatus;
 INT4 i4FsMsdpPeerRemotePort;
 INT4 i4FsMsdpPeerLocalPort;
 INT4 i4FsMsdpPeerEncapsulationType;
 INT4 i4FsMsdpPeerMD5AuthPwdStat;
 INT4 i4FsMsdpPeerMD5FailCount;
 INT4 i4FsMsdpPeerMD5SuccessCount;
 INT4 i4FsMsdpPeerAdminStatus;
 INT4 i4FsMsdpPeerRemoteAddressLen;
 INT4 i4FsMsdpPeerLocalAddressLen;
 INT4 i4FsMsdpPeerMD5AuthPasswordLen;
 UINT1 au1FsMsdpPeerRemoteAddress[16];
 UINT1 au1FsMsdpPeerLocalAddress[16];
 UINT1 au1FsMsdpPeerMD5AuthPassword[80];
} tMsdpMibFsMsdpPeerEntry;

/* Structure used by Msdp protocol for FsMsdpSACacheEntry */

typedef struct
{
 tRBNodeEmbd  FsMsdpSACacheTableNode;
 UINT4 u4FsMsdpSACacheInSAs;
 UINT4 u4FsMsdpSACacheInDataPackets;
 UINT4 u4FsMsdpSACacheUpTime;
 UINT4 u4FsMsdpSACacheExpiryTime;
 INT4 i4FsMsdpSACacheAddrType;
 INT4 i4FsMsdpSACacheStatus;
 INT4 i4FsMsdpSACacheGroupAddrLen;
 INT4 i4FsMsdpSACacheSourceAddrLen;
 INT4 i4FsMsdpSACacheOriginRPLen;
 INT4 i4FsMsdpSACachePeerLearnedFromLen;
 INT4 i4FsMsdpSACacheRPFPeerLen;
 UINT1 au1FsMsdpSACacheGroupAddr[16];
 UINT1 au1FsMsdpSACacheSourceAddr[16];
 UINT1 au1FsMsdpSACacheOriginRP[16];
 UINT1 au1FsMsdpSACachePeerLearnedFrom[16];
 UINT1 au1FsMsdpSACacheRPFPeer[16];
} tMsdpMibFsMsdpSACacheEntry;

/* Structure used by Msdp protocol for FsMsdpMeshGroupEntry */

typedef struct
{
 tRBNodeEmbd  FsMsdpMeshGroupTableNode;
 INT4 i4FsMsdpMeshGroupAddrType;
 INT4 i4FsMsdpMeshGroupStatus;
 INT4 i4FsMsdpMeshGroupNameLen;
 INT4 i4FsMsdpMeshGroupPeerAddressLen;
 UINT1 au1FsMsdpMeshGroupName[64];
 UINT1 au1FsMsdpMeshGroupPeerAddress[16];
} tMsdpMibFsMsdpMeshGroupEntry;

/* Structure used by Msdp protocol for FsMsdpRPEntry */

typedef struct
{
 tRBNodeEmbd  FsMsdpRPTableNode;
 INT4 i4FsMsdpRPAddrType;
 INT4 i4FsMsdpRPOperStatus;
 INT4 i4FsMsdpRPStatus;
 INT4 i4FsMsdpRPAddressLen;
 UINT1 au1FsMsdpRPAddress[16];
} tMsdpMibFsMsdpRPEntry;

/* Structure used by Msdp protocol for FsMsdpPeerFilterEntry */

typedef struct
{
 tRBNodeEmbd  FsMsdpPeerFilterTableNode;
 INT4 i4FsMsdpPeerFilterAddrType;
 INT4 i4FsMsdpPeerFilterStatus;
 INT4 i4FsMsdpPeerFilterRouteMapLen;
 UINT1 au1FsMsdpPeerFilterRouteMap[20];
} tMsdpMibFsMsdpPeerFilterEntry;

/* Structure used by Msdp protocol for FsMsdpSARedistributionEntry */

typedef struct
{
 tRBNodeEmbd  FsMsdpSARedistributionTableNode;
 INT4 i4FsMsdpSARedistributionAddrType;
 INT4 i4FsMsdpSARedistributionStatus;
 INT4 i4FsMsdpSARedistributionRouteMapStat;
 INT4 i4FsMsdpSARedistributionRouteMapLen;
 UINT1 au1FsMsdpSARedistributionRouteMap[20];
} tMsdpMibFsMsdpSARedistributionEntry;
