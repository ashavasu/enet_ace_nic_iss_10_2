/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *  $Id: msdpprot.h,v 1.4 2013/12/16 15:22:41 siva Exp $
 * Description: This file contains declaration of function 
 *              prototypes of msdp module.
 *******************************************************************/

#ifndef __MSDPPROT_H__
#define __MSDPPROT_H__ 

/* Add Prototypes here */

/**************************msdpmain.c*******************************/
PUBLIC VOID MsdpMainTask             PROTO ((VOID));
PUBLIC UINT4 MsdpMainTaskInit         PROTO ((VOID));
PUBLIC VOID MsdpMainDeInit            PROTO ((VOID));
PUBLIC INT4 MsdpMainTaskLock          PROTO ((VOID));
PUBLIC INT4 MsdpMainTaskUnLock        PROTO ((VOID));
INT4 MsdpMainHdlMsdpAdminStatusChg (tMsdpConfigAdmin *pMsdpConfigAdmin);
INT4 MsdpMainConfigAdminStatus (INT4 i4AdminStatus, UINT1 u1AddrType);
VOID MsdpMainHandleIfChange (UINT1, UINT4 u4IfIndex, UINT4 u4AdminStatus, 
                             UINT4 u4OperStatus);


/**************************msdpque.c********************************/
PUBLIC VOID MsdpQueProcessMsgs        PROTO ((VOID));
tMsdpQueMsg * MsdpQueAllocQueMsg (VOID);
INT4 MsdpQuePostQueueMsg (tMsdpQueMsg *pMsdpQueMsg);

/**************************msdppkt.c*******************************/
#if 0
PUBLIC tMsdpPkt *MsdpPktAlloc        PROTO(( VOID ));
PUBLIC VOID MsdpPktFree              PROTO(( tMsdpPkt *pMsdpPkt ));
PUBLIC UINT4  MsdpPktSend            PROTO(( tMsdpPkt *pMsdpPkt ));
PUBLIC UINT4 MsdpPktRecv             PROTO(( tMsdpPkt *pMsdpPkt));
#endif
/**************************msdptmr.c*******************************/
PUBLIC VOID  MsdpTmrInitTmrDesc      PROTO(( VOID));
PUBLIC VOID  MsdpTmrStartTmr         PROTO((tMsdpTmr * pMsdpTmr, 
                       enMsdpTmrId eMsdpTmrId, 
         UINT4 u4Secs));
PUBLIC VOID  MsdpTmrRestartTmr       PROTO((tMsdpTmr * pMsdpTmr, 
                 enMsdpTmrId eMsdpTmrId, 
          UINT4 u4Secs));
PUBLIC VOID  MsdpTmrStopTmr          PROTO((tMsdpTmr * pMsdpTmr));
PUBLIC VOID  MsdpTmrHandleExpiry     PROTO((VOID));
VOID
MsdpTmrConnRetryExpiryHdlr (VOID *pArg);

VOID
MsdpTmrHoldTimerExpiryHdlr (VOID *pArg);
VOID
MsdpTmrKeepAliveTmrExpiryHdlr (VOID *pArg);
VOID
MsdpTmrCacheStateTimerExpHdlr (VOID *pArg);
VOID
MsdTmrCacheSrcAdvtExpiryHandler (VOID *pArg);


/**************************msdptrc.c*******************************/


CHR1  *MsdpTrc           PROTO(( UINT4 u4Trc,  const char *fmt, ...));
VOID   MsdpTrcPrint      PROTO(( const char *fname, UINT4 u4Line, const char *s));
VOID   MsdpTrcWrite      PROTO(( CHR1 *s));

/**************************msdptask.c*******************************/

PUBLIC INT4 MsdpTaskRegisterMsdpMibs (VOID);

/*********************msdputil.c*************************************/

tSNMP_OID_TYPE     *
MsdpUtilMakeObjIdFromDotNew (INT1 *pi1TxtStr);
INT4
MsdpUtilParseSubIdNew (UINT1 **ppu1TempPtr);
VOID
MsdpUtilSnmpIfSendTrap (UINT1 u1TrapId, VOID *pTrapInfo);
VOID
MsdpUtilSendEventTrap (tMsdpEventTrap *pTrapEvent, UINT4 u4TrapId);
VOID MsdpUtilInitGlobals (UINT1);
INT4 MsdpUtilCopyOverBufChain (tCRU_BUF_CHAIN_HEADER **,
                               UINT1 *, UINT2 , UINT2 *);
INT4  MsdpUtilRemoveCacheEntry (tMsdpFsMsdpSACacheEntry *); 
PUBLIC INT4 MsdpUtilCacheTableRBCmpRpGrpSrc (tRBElem *, tRBElem *);
PUBLIC INT4 MsdpUtilMeshGroupTableRBCmpPeerAddr (tRBElem * pRBElem1, 
                                             tRBElem * pRBElem2);
INT4 MsdpUtilIsAllZeros(UINT1 *pu1Addr, INT4 i4MsdpAddrType);
INT4 MsdpUtilIsValidLocalAddress(UINT1 *pu1Addr, INT4 i4AddrType);
VOID
MsdpUtilGetUcastRtInfo (tIPvXAddr *pDest, tIPvXAddr *pNextHop, INT4 *pi4Metrics,
                         UINT4 *pu4Preference, INT4 *pi4Port);
INT4
MsdpUtilGetRegisterEntry (UINT1 u1ProtId);
INT4
MsdpUtilFindFreeRegisterEntry (VOID);

INT4 MsdpUtilIsBuffSizeReached (UINT1, INT4, INT4 );
INT4 MsdpUtilGetTxPktSize (UINT2, UINT1, INT4 *);
tMsdpFsMsdpSACacheEntry *MsdpUtilGetNextFsMsdpSACache 
       (tMsdpFsMsdpSACacheEntry *, INT4 (*CmpFn)(tRBElem*, tRBElem*));
INT4
MsdpUtilCacheTableRBCmpGrpRpSrc (tRBElem * pRBElem1, tRBElem * pRBElem2);
UINT1 *MsdpPrintAddress (UINT1 *pu1Addr, UINT1 u1Afi);
UINT1 *MsdpPrintIPvxAddress (tIPvXAddr Addr);

/*********************msdpsock.c*************************************/
INT4 MsdpSockServerStart (UINT2 u2Port);
INT4 MsdpSock6ServerStart (UINT2 u2Port);
VOID MsdpSockServerStop (UINT1 u1AddrType);
VOID MsdpSockV4NewPassiveConnCallBk (INT4 i4SockFd);
VOID MsdpSockV6NewPassiveConnCallBk (INT4 i4SockFd);
VOID MsdpSockHandleNewPassiveConnEvt (INT4 i4SrvSockFd);
VOID MsdpSock6HandleNewPassiveConnEvt (INT4 i4SrvSockFd);
INT4 MsdpSockAcceptNewConnection (INT4 i4SrvSockFd, INT4 *pi4ClientSockFd,
                                   tIPvXAddr * pPeerAddr);
INT4 MsdpSock6AcceptNewConnection (INT4 i4SrvSockFd, INT4 *pi4ClientSockFd,
                                   tIPvXAddr * pPeerAddr);
VOID MsdpSockNewActiveConnCallBk (INT4 i4SockFd);
VOID MsdpSockHandleNewActiveConnEvt (INT4 i4SockFd);
VOID MsdpSockIndicateSockWriteEvt(INT4 i4SockFd);
VOID MsdpSockHandleSockReadEvt (INT4 i4SockFd);
VOID MsdpSockHandleSockWriteEvt (INT4 i4SockFd);
VOID MsdpSockHandleSockReadWriteEvt (tMsdpTcpMsg *pMsdpTcpMsg);
INT4
MsdpSockOpenPeerConnection (tMsdpFsMsdpPeerEntry *pPeerEntry);
INT4
MsdpSock6OpenPeerConnection (tMsdpFsMsdpPeerEntry *pPeerEntry);
INT4
MsdpSockClosePeerConnection (tMsdpFsMsdpPeerEntry *pPeerEntry);
VOID
MsdpSockIndicateSockReadEvt(INT4 i4SockFd);
INT4
MsdpSockSend (INT4 i4SockFd, UINT1 *pu1Data, UINT2 u2PktLen, 
                  INT4 *pi4WrBytes);
INT4
MsdpSockRcv (INT4 i4SockFd, UINT1 *pu1Data, UINT2 u2Len, INT4 *pi4RdBytes);

/*********msdppeer.c**************************/
tMsdpFsMsdpSACacheEntry *
MsdpPimGetNextCacheWithReqGrp (tMsdpFsMsdpSACacheEntry * pCurCache,
                               tIPvXAddr * pGrpAddr); 
VOID
MsdpPeerHdlAdminStatusEnable (tMsdpFsMsdpPeerEntry * pPeer);
VOID
MsdpPeerDestroyCacheFromPeer (tMsdpFsMsdpPeerEntry *pPeer);
VOID
MsdpPeerRunPfsm (tMsdpFsMsdpPeerEntry *pPeer, UINT1 u1PeerEvent);
INT4
MsdpPeerHdlAdminStatusConfig (tMsdpConfigPeer *pConfigPeer);
VOID
MsdpPeerFsmInvalid (tMsdpFsMsdpPeerEntry* pPeer);
VOID
MsdpPeerFsmPeerEnable (tMsdpFsMsdpPeerEntry *pPeer);
VOID
MsdpPeerFsmInitiateConnection (tMsdpFsMsdpPeerEntry *pPeer);
VOID
MsdpPeerFsmPassiveOpen (tMsdpFsMsdpPeerEntry *pPeer);
VOID
MsdpPeerFsmHandleEstablished (tMsdpFsMsdpPeerEntry *pPeer);
VOID
MsdpPeerFsmHdlConnRetryExpiry  (tMsdpFsMsdpPeerEntry *pPeer);
VOID
MsdpPeerFsmPeerStop (tMsdpFsMsdpPeerEntry *pPeer);
INT4
MsdpPeerGetPeerMeshGrp (tMsdpFsMsdpPeerEntry *pPeer,
                         tMsdpFsMsdpMeshGroupEntry **ppPeerMeshGrp);
INT4
MsdpPeerUtilFindPeerEntry (INT4 i4SockFd, tMsdpFsMsdpPeerEntry **ppPeer);
INT4
MsdpPeerUtilGetPeerEntry (tIPvXAddr *pPeerAddr,UINT1 u1AddrType, 
                           tMsdpFsMsdpPeerEntry **ppPeer);
INT4
MsdpPeerValidateAndAcceptPeer( tIPvXAddr *pPeerAddr,UINT1 u1AddrType, 
                                INT4 i4ClientSockFd);
INT4
MsdpPeerAdminStatusConfig (tIPvXAddr *pPeerAddr, UINT1 u1PeerAdmin, 
                            UINT1 u1AddrType);
INT4
MsdpPeerChkPeerFilterStatus (tIPvXAddr * pPeerAddr, UINT1 u1AddrType);


VOID
MsdpPeerHandleKeepAliveTmrExpiry (tMsdpFsMsdpPeerEntry * pPeer);
/***************msdpin.c*********************/

INT4 MsdpInProcessRxdMsg(tMsdpFsMsdpPeerEntry *pPeer);
INT4
MsdpInReadPeerMsg (tMsdpFsMsdpPeerEntry *pPeer);
INT4
MsdpInHandleRcvdPacket (tMsdpFsMsdpPeerEntry *pPeer);
INT4
MsdpInHandleSaAdvtOrRespPkt (tMsdpFsMsdpPeerEntry *pPeer, UINT1 u1AddrType);
INT4
MsdpInProcessSaRequestPkt  (tMsdpFsMsdpPeerEntry *pPeer, UINT1 u1AddrType);
INT4
MsdpInProcessKeepAlivePkt (tMsdpFsMsdpPeerEntry *pPeer);
INT4
MsdpInChkRpfPeer (tMsdpFsMsdpPeerEntry *pSaAdvtPeer, tIPvXAddr *pOriginator);

/***************msdpout.c*********************/

VOID
MsdpOutCopyMsgInPeerTxBuf (tMsdpFsMsdpPeerEntry *pPeer, UINT1 *pu1SrcBuf,
                              UINT2 u2SrcMsgOffset, UINT2 u2SrcMsgLen,
                              UINT2 u2SrcBufSize);
INT4
MsdpOutChkForValidPeer (tMsdpFsMsdpPeerEntry * pRecvPeer,
                        tMsdpFsMsdpPeerEntry * pNextPeer);
INT4
MsdpOutValidateAndDecrementTtl (tMsdpFsMsdpPeerEntry * pInPeer, 
                                UINT1 * pu1SrcBuf, UINT2 * pu2PktLen);
VOID
MsdpOutUpdatePeerStatistics (tMsdpFsMsdpPeerEntry *pPeer, UINT1 *pu1SrcBuf,
                             UINT2 u2PktLen);
VOID
MsdpOutFwdSAPkt (tMsdpFsMsdpPeerEntry *, UINT1 *,INT4,
                 UINT2, UINT2, UINT2);
INT4
MsdpOutSendMsgtoPeer (tMsdpFsMsdpPeerEntry *pPeer, UINT1 *pu1Buf,
                      UINT2 u2MsgOffset, UINT2 u2MsgLen, UINT2 u2BufSize);
INT4
MsdpOutSendResidualMsgtoPeer (tMsdpFsMsdpPeerEntry *pPeer);
INT4
MsdpOutChkPeerInMeshGrp (tMsdpFsMsdpPeerEntry *pRcvPeer,
                          tMsdpFsMsdpPeerEntry *pPeer);
VOID
MsdpOutPeerKeepAlivePkt (tMsdpFsMsdpPeerEntry *pPeer);

/****************** msdcache.c *********************************/

VOID MsdCacheSetCurrentRPAddr (INT4, INT4, UINT1 *, UINT1 *, INT4 *, UINT1 *);
INT4 MsdCacheFormSAPkt (tMsdpFsMsdpSACacheEntry *, INT4 *, UINT1 *);

tMsdpFsMsdpSACacheEntry *
MsdCacheGetNextValidSACache (tMsdpFsMsdpSACacheEntry *, UINT1, INT4, UINT1 *);

INT4 MsdCacheAddValidCacheInSAPkt (UINT1, INT4 *, tMsdpFsMsdpSACacheEntry *, 
                                   UINT1 *, INT4 *, UINT1 *);
INT4 MsdCacheConstructSAEntry (UINT1 , UINT2 *, tMsdpCacheUpdt *, UINT1 *);
INT4 MsdCacheFormEntry (UINT4, tMsdpFsMsdpSACacheEntry *, tMsdpCacheUpdt *);
INT4 MsdCacheFormOriginSATlv (tMsdpCacheUpdt *, tCRU_BUF_CHAIN_HEADER *,
                               UINT2, UINT2 *);
INT4 MsdCacheFormSAHdr (UINT1, INT4, UINT1, UINT1 *);
INT4 MsdCacheSendSAAdvtEvent (VOID);
INT4 MsdCacheSAInfoBatchAdvt (tMsdpCacheAdvtMarker *);
INT4 MsdCacheUpdateCacheTbl (tMsdpCacheUpdt, UINT4, UINT1 *);
INT4 MsdCacheOriginateSAMsg (tMsdpCacheUpdt  *, tCRU_BUF_CHAIN_HEADER *, UINT2);
INT4 MsdCacheAmIOriginator (UINT1, INT4, UINT1 *);
INT4 MsdCacheSendSARequest (tMsdpSAReqGrp *);
INT4 MsdCacheSendSAResponseTlv (UINT1,tMsdpFsMsdpPeerEntry *,
                                UINT1, tMsdpSaReqPkt *);
INT4 MsdCacheHandleSARequest (tMsdpFsMsdpPeerEntry *, tMsdpSaReqPkt *, UINT1);
INT4 MsdCacheProcessSAMsg (tMsdpFsMsdpPeerEntry *, tMsdpSaAdvtHdr *, 
                                      UINT2, UINT1);
INT4
MsdCacheApplySAFilter (tMsdpCacheUpdt *);

INT4 MsdCacheDeleteEntry (tMsdpFsMsdpSACacheEntry *, INT1);
/********************** msdppim.c *************************************/
INT4 MsdpPimIfAllocateBuf (tMsdpMrpSaAdvt *, INT4, UINT1);

INT4 MsdpPimIfFillSAEntry (tMsdpMrpSaAdvt *, tIPvXAddr *, tIPvXAddr *,
                           INT4 *);

INT4 MsdpPimIfHandleSAReq  (tMsdpSAReqGrp *);
INT4 MsdpPimIfHandleSAInfo (tMsdpMrpSaAdvt *);
VOID MsdpPimIfExtractSGFromBuf (tMsdpMrpSaAdvt *, tMsdpCacheUpdt *);
#endif   /* __MSDPPROT_H__ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  msdpprot.h                     */
/*-----------------------------------------------------------------------*/
