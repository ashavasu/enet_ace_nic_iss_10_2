/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdmsdlw.h,v 1.1.1.1 2011/01/17 05:57:32 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMsdpEnabled ARG_LIST((INT4 *));

INT1
nmhGetMsdpCacheLifetime ARG_LIST((UINT4 *));

INT1
nmhGetMsdpNumSACacheEntries ARG_LIST((UINT4 *));

INT1
nmhGetMsdpRPAddress ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMsdpEnabled ARG_LIST((INT4 ));

INT1
nmhSetMsdpCacheLifetime ARG_LIST((UINT4 ));

INT1
nmhSetMsdpRPAddress ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MsdpEnabled ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2MsdpCacheLifetime ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2MsdpRPAddress ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MsdpEnabled ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2MsdpCacheLifetime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2MsdpRPAddress ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for MsdpPeerTable. */
INT1
nmhValidateIndexInstanceMsdpPeerTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MsdpPeerTable  */

INT1
nmhGetFirstIndexMsdpPeerTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMsdpPeerTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMsdpPeerState ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetMsdpPeerRPFFailures ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMsdpPeerInSAs ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMsdpPeerOutSAs ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMsdpPeerInSARequests ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMsdpPeerOutSARequests ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMsdpPeerInControlMessages ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMsdpPeerOutControlMessages ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMsdpPeerInDataPackets ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMsdpPeerOutDataPackets ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMsdpPeerFsmEstablishedTransitions ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMsdpPeerFsmEstablishedTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMsdpPeerInMessageTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMsdpPeerLocalAddress ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMsdpPeerConnectRetryInterval ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetMsdpPeerHoldTimeConfigured ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetMsdpPeerKeepAliveConfigured ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetMsdpPeerDataTtl ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetMsdpPeerStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetMsdpPeerRemotePort ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetMsdpPeerLocalPort ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetMsdpPeerEncapsulationType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetMsdpPeerConnectionAttempts ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMsdpPeerDiscontinuityTime ARG_LIST((UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMsdpPeerLocalAddress ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetMsdpPeerConnectRetryInterval ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetMsdpPeerHoldTimeConfigured ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetMsdpPeerKeepAliveConfigured ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetMsdpPeerDataTtl ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetMsdpPeerStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetMsdpPeerEncapsulationType ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MsdpPeerLocalAddress ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2MsdpPeerConnectRetryInterval ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2MsdpPeerHoldTimeConfigured ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2MsdpPeerKeepAliveConfigured ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2MsdpPeerDataTtl ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2MsdpPeerStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2MsdpPeerEncapsulationType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MsdpPeerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for MsdpSACacheTable. */
INT1
nmhValidateIndexInstanceMsdpSACacheTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MsdpSACacheTable  */

INT1
nmhGetFirstIndexMsdpSACacheTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMsdpSACacheTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMsdpSACachePeerLearnedFrom ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMsdpSACacheRPFPeer ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMsdpSACacheInSAs ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMsdpSACacheInDataPackets ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMsdpSACacheUpTime ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMsdpSACacheExpiryTime ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMsdpSACacheStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMsdpSACacheStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MsdpSACacheStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MsdpSACacheTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for MsdpMeshGroupTable. */
INT1
nmhValidateIndexInstanceMsdpMeshGroupTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MsdpMeshGroupTable  */

INT1
nmhGetFirstIndexMsdpMeshGroupTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMsdpMeshGroupTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMsdpMeshGroupStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMsdpMeshGroupStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MsdpMeshGroupStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MsdpMeshGroupTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
