/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: msdpinc.h,v 1.3 2011/10/13 10:16:08 siva Exp $
 *
 * Description: This file contains include files of msdp module.
 *******************************************************************/

#ifndef __MSDPINC_H__
#define __MSDPINC_H__ 

#include "lr.h"
#include "cfa.h"
#include "cli.h"
#include "fssnmp.h"
#include "params.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "utilipvx.h"
#include "ip6util.h"
#include "ip.h"
#include "rtm.h"
#include "rmap.h"
#include "msdp.h"
#include "msdpdefn.h"
#include "msdptdfsg.h"
#include "msdptdfs.h"
#include "msdptrc.h"
#include "msdpport.h"
#include "stdmsdwr.h"

#ifdef __MSDPMAIN_C__

#include "msdpglob.h"
#include "msdplwg.h"
#include "msdpdefg.h"
#include "msdpsz.h"
#include "msdpwrg.h"

#else

#include "msdpextn.h"
#include "msdpmibclig.h"
#include "msdplwg.h"
#include "msdpdefg.h"
#include "msdpsz.h"
#include "msdpwrg.h"

#endif /* __MSDPMAIN_C__*/

#include "msdpprot.h"
#include "msdpcliprot.h"
#include "msdpprotg.h"

#endif   /* __MSDPINC_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file msdpinc.h                       */
/*-----------------------------------------------------------------------*/

