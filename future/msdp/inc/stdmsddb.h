/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdmsddb.h,v 1.3 2012/08/09 09:00:51 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDMSDDB_H
#define _STDMSDDB_H

UINT1 MsdpPeerTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 MsdpSACacheTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 MsdpMeshGroupTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};

UINT4 stdmsd [] ={1,3,6,1,3,92};
tSNMP_OID_TYPE stdmsdOID = {6, stdmsd};


UINT4 MsdpEnabled [ ] ={1,3,6,1,3,92,1,1,1};
UINT4 MsdpCacheLifetime [ ] ={1,3,6,1,3,92,1,1,2};
UINT4 MsdpNumSACacheEntries [ ] ={1,3,6,1,3,92,1,1,3};
UINT4 MsdpRPAddress [ ] ={1,3,6,1,3,92,1,1,11};
UINT4 MsdpPeerRemoteAddress [ ] ={1,3,6,1,3,92,1,1,5,1,1};
UINT4 MsdpPeerState [ ] ={1,3,6,1,3,92,1,1,5,1,3};
UINT4 MsdpPeerRPFFailures [ ] ={1,3,6,1,3,92,1,1,5,1,4};
UINT4 MsdpPeerInSAs [ ] ={1,3,6,1,3,92,1,1,5,1,5};
UINT4 MsdpPeerOutSAs [ ] ={1,3,6,1,3,92,1,1,5,1,6};
UINT4 MsdpPeerInSARequests [ ] ={1,3,6,1,3,92,1,1,5,1,7};
UINT4 MsdpPeerOutSARequests [ ] ={1,3,6,1,3,92,1,1,5,1,8};
UINT4 MsdpPeerInControlMessages [ ] ={1,3,6,1,3,92,1,1,5,1,9};
UINT4 MsdpPeerOutControlMessages [ ] ={1,3,6,1,3,92,1,1,5,1,10};
UINT4 MsdpPeerInDataPackets [ ] ={1,3,6,1,3,92,1,1,5,1,11};
UINT4 MsdpPeerOutDataPackets [ ] ={1,3,6,1,3,92,1,1,5,1,12};
UINT4 MsdpPeerFsmEstablishedTransitions [ ] ={1,3,6,1,3,92,1,1,5,1,13};
UINT4 MsdpPeerFsmEstablishedTime [ ] ={1,3,6,1,3,92,1,1,5,1,14};
UINT4 MsdpPeerInMessageTime [ ] ={1,3,6,1,3,92,1,1,5,1,15};
UINT4 MsdpPeerLocalAddress [ ] ={1,3,6,1,3,92,1,1,5,1,16};
UINT4 MsdpPeerConnectRetryInterval [ ] ={1,3,6,1,3,92,1,1,5,1,17};
UINT4 MsdpPeerHoldTimeConfigured [ ] ={1,3,6,1,3,92,1,1,5,1,18};
UINT4 MsdpPeerKeepAliveConfigured [ ] ={1,3,6,1,3,92,1,1,5,1,19};
UINT4 MsdpPeerDataTtl [ ] ={1,3,6,1,3,92,1,1,5,1,20};
UINT4 MsdpPeerStatus [ ] ={1,3,6,1,3,92,1,1,5,1,21};
UINT4 MsdpPeerRemotePort [ ] ={1,3,6,1,3,92,1,1,5,1,22};
UINT4 MsdpPeerLocalPort [ ] ={1,3,6,1,3,92,1,1,5,1,23};
UINT4 MsdpPeerEncapsulationType [ ] ={1,3,6,1,3,92,1,1,5,1,24};
UINT4 MsdpPeerConnectionAttempts [ ] ={1,3,6,1,3,92,1,1,5,1,25};
UINT4 MsdpPeerDiscontinuityTime [ ] ={1,3,6,1,3,92,1,1,5,1,26};
UINT4 MsdpSACacheGroupAddr [ ] ={1,3,6,1,3,92,1,1,6,1,1};
UINT4 MsdpSACacheSourceAddr [ ] ={1,3,6,1,3,92,1,1,6,1,2};
UINT4 MsdpSACacheOriginRP [ ] ={1,3,6,1,3,92,1,1,6,1,3};
UINT4 MsdpSACachePeerLearnedFrom [ ] ={1,3,6,1,3,92,1,1,6,1,4};
UINT4 MsdpSACacheRPFPeer [ ] ={1,3,6,1,3,92,1,1,6,1,5};
UINT4 MsdpSACacheInSAs [ ] ={1,3,6,1,3,92,1,1,6,1,6};
UINT4 MsdpSACacheInDataPackets [ ] ={1,3,6,1,3,92,1,1,6,1,7};
UINT4 MsdpSACacheUpTime [ ] ={1,3,6,1,3,92,1,1,6,1,8};
UINT4 MsdpSACacheExpiryTime [ ] ={1,3,6,1,3,92,1,1,6,1,9};
UINT4 MsdpSACacheStatus [ ] ={1,3,6,1,3,92,1,1,6,1,10};
UINT4 MsdpMeshGroupName [ ] ={1,3,6,1,3,92,1,1,12,1,1};
UINT4 MsdpMeshGroupPeerAddress [ ] ={1,3,6,1,3,92,1,1,12,1,2};
UINT4 MsdpMeshGroupStatus [ ] ={1,3,6,1,3,92,1,1,12,1,3};




tMbDbEntry stdmsdMibEntry[]= {

{{9,MsdpEnabled}, NULL, MsdpEnabledGet, MsdpEnabledSet, MsdpEnabledTest, MsdpEnabledDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,MsdpCacheLifetime}, NULL, MsdpCacheLifetimeGet, MsdpCacheLifetimeSet, MsdpCacheLifetimeTest, MsdpCacheLifetimeDep, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,MsdpNumSACacheEntries}, NULL, MsdpNumSACacheEntriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,MsdpPeerRemoteAddress}, GetNextIndexMsdpPeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, MsdpPeerTableINDEX, 1, 0, 0, NULL},

{{11,MsdpPeerState}, GetNextIndexMsdpPeerTable, MsdpPeerStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MsdpPeerTableINDEX, 1, 0, 0, NULL},

{{11,MsdpPeerRPFFailures}, GetNextIndexMsdpPeerTable, MsdpPeerRPFFailuresGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MsdpPeerTableINDEX, 1, 0, 0, NULL},

{{11,MsdpPeerInSAs}, GetNextIndexMsdpPeerTable, MsdpPeerInSAsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MsdpPeerTableINDEX, 1, 0, 0, NULL},

{{11,MsdpPeerOutSAs}, GetNextIndexMsdpPeerTable, MsdpPeerOutSAsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MsdpPeerTableINDEX, 1, 0, 0, NULL},

{{11,MsdpPeerInSARequests}, GetNextIndexMsdpPeerTable, MsdpPeerInSARequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MsdpPeerTableINDEX, 1, 0, 0, NULL},

{{11,MsdpPeerOutSARequests}, GetNextIndexMsdpPeerTable, MsdpPeerOutSARequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MsdpPeerTableINDEX, 1, 0, 0, NULL},

{{11,MsdpPeerInControlMessages}, GetNextIndexMsdpPeerTable, MsdpPeerInControlMessagesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MsdpPeerTableINDEX, 1, 0, 0, NULL},

{{11,MsdpPeerOutControlMessages}, GetNextIndexMsdpPeerTable, MsdpPeerOutControlMessagesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MsdpPeerTableINDEX, 1, 0, 0, NULL},

{{11,MsdpPeerInDataPackets}, GetNextIndexMsdpPeerTable, MsdpPeerInDataPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MsdpPeerTableINDEX, 1, 0, 0, NULL},

{{11,MsdpPeerOutDataPackets}, GetNextIndexMsdpPeerTable, MsdpPeerOutDataPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MsdpPeerTableINDEX, 1, 0, 0, NULL},

{{11,MsdpPeerFsmEstablishedTransitions}, GetNextIndexMsdpPeerTable, MsdpPeerFsmEstablishedTransitionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MsdpPeerTableINDEX, 1, 0, 0, NULL},

{{11,MsdpPeerFsmEstablishedTime}, GetNextIndexMsdpPeerTable, MsdpPeerFsmEstablishedTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MsdpPeerTableINDEX, 1, 0, 0, NULL},

{{11,MsdpPeerInMessageTime}, GetNextIndexMsdpPeerTable, MsdpPeerInMessageTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MsdpPeerTableINDEX, 1, 0, 0, NULL},

{{11,MsdpPeerLocalAddress}, GetNextIndexMsdpPeerTable, MsdpPeerLocalAddressGet, MsdpPeerLocalAddressSet, MsdpPeerLocalAddressTest, MsdpPeerTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, MsdpPeerTableINDEX, 1, 0, 0, NULL},

{{11,MsdpPeerConnectRetryInterval}, GetNextIndexMsdpPeerTable, MsdpPeerConnectRetryIntervalGet, MsdpPeerConnectRetryIntervalSet, MsdpPeerConnectRetryIntervalTest, MsdpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, MsdpPeerTableINDEX, 1, 0, 0, "30"},

{{11,MsdpPeerHoldTimeConfigured}, GetNextIndexMsdpPeerTable, MsdpPeerHoldTimeConfiguredGet, MsdpPeerHoldTimeConfiguredSet, MsdpPeerHoldTimeConfiguredTest, MsdpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, MsdpPeerTableINDEX, 1, 0, 0, "75"},

{{11,MsdpPeerKeepAliveConfigured}, GetNextIndexMsdpPeerTable, MsdpPeerKeepAliveConfiguredGet, MsdpPeerKeepAliveConfiguredSet, MsdpPeerKeepAliveConfiguredTest, MsdpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, MsdpPeerTableINDEX, 1, 0, 0, "60"},

{{11,MsdpPeerDataTtl}, GetNextIndexMsdpPeerTable, MsdpPeerDataTtlGet, MsdpPeerDataTtlSet, MsdpPeerDataTtlTest, MsdpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, MsdpPeerTableINDEX, 1, 0, 0, "1"},

{{11,MsdpPeerStatus}, GetNextIndexMsdpPeerTable, MsdpPeerStatusGet, MsdpPeerStatusSet, MsdpPeerStatusTest, MsdpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MsdpPeerTableINDEX, 1, 0, 1, NULL},

{{11,MsdpPeerRemotePort}, GetNextIndexMsdpPeerTable, MsdpPeerRemotePortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, MsdpPeerTableINDEX, 1, 0, 0, "639"},

{{11,MsdpPeerLocalPort}, GetNextIndexMsdpPeerTable, MsdpPeerLocalPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, MsdpPeerTableINDEX, 1, 0, 0, "639"},

{{11,MsdpPeerEncapsulationType}, GetNextIndexMsdpPeerTable, MsdpPeerEncapsulationTypeGet, MsdpPeerEncapsulationTypeSet, MsdpPeerEncapsulationTypeTest, MsdpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MsdpPeerTableINDEX, 1, 0, 0, NULL},

{{11,MsdpPeerConnectionAttempts}, GetNextIndexMsdpPeerTable, MsdpPeerConnectionAttemptsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MsdpPeerTableINDEX, 1, 0, 0, NULL},

{{11,MsdpPeerDiscontinuityTime}, GetNextIndexMsdpPeerTable, MsdpPeerDiscontinuityTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MsdpPeerTableINDEX, 1, 0, 0, NULL},

{{11,MsdpSACacheGroupAddr}, GetNextIndexMsdpSACacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, MsdpSACacheTableINDEX, 3, 0, 0, NULL},

{{11,MsdpSACacheSourceAddr}, GetNextIndexMsdpSACacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, MsdpSACacheTableINDEX, 3, 0, 0, NULL},

{{11,MsdpSACacheOriginRP}, GetNextIndexMsdpSACacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, MsdpSACacheTableINDEX, 3, 0, 0, NULL},

{{11,MsdpSACachePeerLearnedFrom}, GetNextIndexMsdpSACacheTable, MsdpSACachePeerLearnedFromGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, MsdpSACacheTableINDEX, 3, 0, 0, NULL},

{{11,MsdpSACacheRPFPeer}, GetNextIndexMsdpSACacheTable, MsdpSACacheRPFPeerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, MsdpSACacheTableINDEX, 3, 0, 0, NULL},

{{11,MsdpSACacheInSAs}, GetNextIndexMsdpSACacheTable, MsdpSACacheInSAsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MsdpSACacheTableINDEX, 3, 0, 0, NULL},

{{11,MsdpSACacheInDataPackets}, GetNextIndexMsdpSACacheTable, MsdpSACacheInDataPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MsdpSACacheTableINDEX, 3, 0, 0, NULL},

{{11,MsdpSACacheUpTime}, GetNextIndexMsdpSACacheTable, MsdpSACacheUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MsdpSACacheTableINDEX, 3, 0, 0, NULL},

{{11,MsdpSACacheExpiryTime}, GetNextIndexMsdpSACacheTable, MsdpSACacheExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MsdpSACacheTableINDEX, 3, 0, 0, NULL},

{{11,MsdpSACacheStatus}, GetNextIndexMsdpSACacheTable, MsdpSACacheStatusGet, MsdpSACacheStatusSet, MsdpSACacheStatusTest, MsdpSACacheTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MsdpSACacheTableINDEX, 3, 0, 1, NULL},

{{9,MsdpRPAddress}, NULL, MsdpRPAddressGet, MsdpRPAddressSet, MsdpRPAddressTest, MsdpRPAddressDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,MsdpMeshGroupName}, GetNextIndexMsdpMeshGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MsdpMeshGroupTableINDEX, 2, 0, 0, NULL},

{{11,MsdpMeshGroupPeerAddress}, GetNextIndexMsdpMeshGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, MsdpMeshGroupTableINDEX, 2, 0, 0, NULL},

{{11,MsdpMeshGroupStatus}, GetNextIndexMsdpMeshGroupTable, MsdpMeshGroupStatusGet, MsdpMeshGroupStatusSet, MsdpMeshGroupStatusTest, MsdpMeshGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MsdpMeshGroupTableINDEX, 2, 0, 1, NULL},
};
tMibData stdmsdEntry = { 42, stdmsdMibEntry };

#endif /* _STDMSDDB_H */

