/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: msdpcliprot.h,v 1.4 2011/10/13 10:16:08 siva Exp $
*
* This file contains prototypes for the functions defined in Msdp.
*********************************************************************/

#ifndef __MSDPCLIPROT_H__
#define __MSDPCLIPROT_H__

#define MSDP_CLI_MAX_ARGS   15

#define MSDP_LOCK  MsdpMainTaskLock ()

#define MSDP_UNLOCK  MsdpMainTaskUnLock ()

INT4 MsdpCliSetFsMsdpTraceLevel (tCliHandle CliHandle,UINT4 *pFsMsdpTraceLevel);


INT4 MsdpCliSetFsMsdpIPv4AdminStat (tCliHandle CliHandle,UINT4 *pFsMsdpIPv4AdminStat);


INT4 MsdpCliSetFsMsdpIPv6AdminStat (tCliHandle CliHandle,UINT4 *pFsMsdpIPv6AdminStat);


INT4 MsdpCliSetFsMsdpCacheLifetime (tCliHandle CliHandle,UINT4 *pFsMsdpCacheLifetime);


INT4 MsdpCliSetFsMsdpMaxPeerSessions (tCliHandle CliHandle,UINT4 *pFsMsdpMaxPeerSessions);


INT4 MsdpCliSetFsMsdpMappingComponentId (tCliHandle CliHandle,UINT4 *pFsMsdpMappingComponentId);


INT4 MsdpCliSetFsMsdpListenerPort (tCliHandle CliHandle,UINT4 *pFsMsdpListenerPort);


INT4 MsdpCliSetFsMsdpPeerFilter (tCliHandle CliHandle,UINT4 *pFsMsdpPeerFilter);


INT4 MsdpCliSetFsMsdpPeerTable (tCliHandle CliHandle,tMsdpFsMsdpPeerEntry * pMsdpSetFsMsdpPeerEntry,tMsdpIsSetFsMsdpPeerEntry * pMsdpIsSetFsMsdpPeerEntry);

INT4 MsdpCliSetFsMsdpSACacheTable (tCliHandle CliHandle,tMsdpFsMsdpSACacheEntry * pMsdpSetFsMsdpSACacheEntry,tMsdpIsSetFsMsdpSACacheEntry * pMsdpIsSetFsMsdpSACacheEntry);

INT4 MsdpCliSetFsMsdpMeshGroupTable (tCliHandle CliHandle,tMsdpFsMsdpMeshGroupEntry * pMsdpSetFsMsdpMeshGroupEntry,tMsdpIsSetFsMsdpMeshGroupEntry * pMsdpIsSetFsMsdpMeshGroupEntry);

INT4 MsdpCliSetFsMsdpRPTable (tCliHandle CliHandle,tMsdpFsMsdpRPEntry * pMsdpSetFsMsdpRPEntry,tMsdpIsSetFsMsdpRPEntry * pMsdpIsSetFsMsdpRPEntry);

INT4 MsdpCliSetFsMsdpPeerFilterTable (tCliHandle CliHandle,tMsdpFsMsdpPeerFilterEntry * pMsdpSetFsMsdpPeerFilterEntry,tMsdpIsSetFsMsdpPeerFilterEntry * pMsdpIsSetFsMsdpPeerFilterEntry);

INT4 MsdpCliSetFsMsdpSARedistributionTable (tCliHandle CliHandle,tMsdpFsMsdpSARedistributionEntry * pMsdpSetFsMsdpSARedistributionEntry,tMsdpIsSetFsMsdpSARedistributionEntry * pMsdpIsSetFsMsdpSARedistributionEntry);


INT4
MsdpCliConvertIpvxToStr (UINT1 *u1IpvxAddr, INT4 i4AddrType, UINT1 *pIpStr);
INT4
MsdpCliGetOriginatorId (INT4 i4AddrType, CHR1 * pOriginatorId);
INT4
MsdpCliGetPeerStatistics (INT4 i4MsdpAddrType,
                          INT4 *pi4ConfiguredPeers,
                          INT4 *pi4EstablishedPeers, INT4 *pi4ShutdownPeers);

INT4 
MsdpCliShowSummary (tCliHandle CliHandle, INT4);
INT4 
MsdpCliShowMeshGrp (tCliHandle CliHandle, INT4 i4MsdpAddrType, UINT1 *);
INT4 
MsdpCliShowPeer (tCliHandle , INT4 , UINT1 *);

INT4
MsdpCliShowRpfPeer (tCliHandle CliHandle, INT4 i4MsdpAddrType, 
                    UINT1 *pu1Originator);

INT4 MsdpCliClearCache (INT4 i4MsdpAddrType, UINT1 *pGrpAddr);
INT4 MsdpCliClearPeer (tCliHandle CliHandle,INT4 i4MsdpAddrType, 
                       UINT1 *pPeerAddr);
INT4 
MsdpCliShowSACount (tCliHandle CliHandle, INT4 i4MsdpAddrType);
INT4 
MsdpShowRunningConfigMeshGrpTbl (tCliHandle CliHandle, INT4 i4MsdpAddrType);
INT4 
MsdpShowRunningConfigSAFilter(tCliHandle CliHandle, INT4 i4MsdpAddrType);
INT4
MsdpShowRunningConfigPeerTable (tCliHandle CliHandle, INT4 i4AddrType);
INT4
MsdpShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module, INT4 i4AddrType);
INT4 
MsdpShowRunningConfigScalar (tCliHandle CliHandle, INT4 i4AddrType);
INT4
MsdpShowRunningConfigPeerFilter (tCliHandle,INT4 i4AddrType);
INT4 
MsdpShowRunningConfigRPTable (tCliHandle CliHandle, INT4 i4MsdpAddrType);
INT4 
MsdpCliShowCache (tCliHandle CliHandle, INT4 i4MsdpAddrType,
                       UINT1 *pu1SrcAddr, UINT1 *pu1GrpAddr);

#endif
