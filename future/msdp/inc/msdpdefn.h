/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
*  $Id: msdpdefn.h,v 1.6 2012/04/27 12:09:39 siva Exp $
*
*
* This file contains  definitions in Msdp.
*********************************************************************/
#ifndef __MSDPDEFN_H__
#define __MSDPDEFN_H__

#define MSDP_PROTOCOL_ID        0x09

#define MSDP_MAX_REGISTER    2
#define MSDP_SUCCESS        (OSIX_SUCCESS)
#define MSDP_FAILURE        (OSIX_FAILURE)

#define  MSDP_TASK_PRIORITY              150
#define  MSDP_TASK_NAME                  "MSDP"
#define  MSDP_QUEUE_NAME                 (const UINT1 *) "MSDPQ"
#define  MSDP_MUT_EXCL_SEM_NAME          (const UINT1 *) "MSDPM"
#define  MSDP_QUEUE_DEPTH                100
#define  MSDP_SEM_CREATE_INIT_CNT        1

#define  MSDP_ESTABLISHED_TRAP_ID          1
#define  MSDP_BACKWARD_TRANSITION_TRAP_ID  2
#define  MSDP_RP_OPERSTATUS_TRAP_ID        3
#define SNMP_V2_TRAP_OID_LEN               15

#define MSDP_MAX_HEX_SINGLE_DIGIT         0xf
#define  MSDP_ZERO                       0
#define  MSDP_ONE                        1
#define  MSDP_TWO                        2
#define  MSDP_THREE                      3
#define  MSDP_NUMBER_TEN                 10
#define  MSDP_ENABLED                    1
#define  MSDP_DISABLED                   0

#define  MSDP_ACCEPT_ALL                 1
#define  MSDP_DENY_ALL                   0

#define  MSDP_TIMER_EVENT             0x00000001 
#define  MSDP_QUEUE_EVENT             0x00000002
#define  MSDP_V4_PASSIVE_CONN_EVT     0x00000004
#define  MSDP_V6_PASSIVE_CONN_EVT     0x00000008
#define  MSDP_SA_BATCH_ADVT_EVT       0x00000010

#define MSDP_MAX_PEER_SESSIONS            32
#define MSDP_DEF_LISTENER_PORT            639
#define MSDP_DEF_COMPONENT                1
#define MSDP_SA_ADVT_TIME                 60
#define MSDP_KAT_VAL                      75
#define MSDP_HOLDTIME                     60
#define MSDP_CONNECT_RETRY                30
#define MSDP_DATA_TTL                     1
#define MSDP_MAX_BUFFER                   256
#define MSDP_MAX_LISTEN                   5 
#define MSDP_INVALID                     -1
#define MSDP_PKT_ERROR                   -1

/*******MACROS FOR  MSDP PEER STATE ********/
#define MSDP_PEER_INACTIVE                1
#define MSDP_PEER_LISTEN                  2
#define MSDP_PEER_CONNECT                 3
#define MSDP_PEER_ESTABLISHED             4
#define MSDP_PEER_DISABLED                5
#define MSDP_MAX_PEER_STATE               6

/******************MACROS FOR GROUP ADDR RANGE**************/
#define   MSDP_START_OF_MCAST_GRP           0xe0000100
#define   MSDP_END_OF_MCAST_GRP             0xefffffff

/***********MSDP RANGE OF  VALUES********************************/
#define MSDP_MAX_CONNECT_RETRY         65535
#define MSDP_MAX_HOLDTIME              65535
#define MSDP_MAX_KEEPALIVE             21845
#define MSDP_MAX_DATA_TTL              255
#define MSDP_MIN_CACHELIFETIME         70
#define MSDP_MAX_MRP_COMPONENT         255
#define MSDP_MIN_LISTENER_PORT         1024
#define MSDP_MAX_LISTENER_PORT         65535
#define MSDP_MAX_ROUTEMAP_LEN          20
#define MSDP_MAX_MESH_LEN              64
#define MSDP_BUFFER_LEN                32
/*********** CACHE related macros *******************************/
#define MSDP_ADD_CACHE                   1
#define MSDP_DELETE_CACHE                0

#define MSDP_CACHE_NEW                  1
#define MSDP_CACHE_REFRESH              2
#define MSDP_CACHE_DELETED               3

#define MSDP_PIM_CACHE                  1
#define MSDP_SA_ADVT_CACHE              2
#define MSDP_SA_RESP_CACHE              3
/* Peer related Macros */
#define   MSDP_PEER_ADMIN_ENABLE          1
#define   MSDP_PEER_ADMIN_DISABLE         2

#define   MSDP_PEER_FILTER_ACCEPT         1
#define   MSDP_PEER_FILTER_DENY           2

/************ Macros for MSDP TLVs ************************/ 
#define   MSDP_TLV_SA                    1
#define   MSDP_TLV_SA_REQUEST            2
#define   MSDP_TLV_SA_RESPONSE           3
#define   MSDP_TLV_KEEPALIVE             4
#define   MSDP6_TLV_SA                   61
#define   MSDP6_TLV_SA_REQUEST           62
#define   MSDP6_TLV_SA_RESPONSE          63

#define   MSDP_SA_TLV_LEN(i4EntryCount, i4AddrLen, i4DataPktLen) \
         (4 + i4AddrLen + (i4EntryCount * ( 2 * i4AddrLen + 4 )) + i4DataPktLen)

#define   MSDP6_SA_REQUEST_TLV_LEN       20 
#define   MSDP_SA_REQUEST_TLV_LEN        8
#define   MSDP_SA_RESPONSE_TLV_LEN(i4EntryCount, i4AddrLen) \
          (4 + i4AddrLen + (i4EntryCount * ( 2 * i4AddrLen + 4 )))


#define  MSDP_V4_MDP_TTL_OFFSET         8 /* 9th byte */
#define  MSDP_V6_MDP_HOPLIMIT_OFFSET    7 /* 8th byte */

#define   MSDP_SA_ADVT_RP_OFFSET         MSDP_FOUR_BYTES
#define   MSDP_FOUR_BYTES                4
#define   MSDP_OCTET_IN_BITS             8

#define   MSDP_MAX_GRPSRC_BUF_SIZE       2000
#define   MSDP_MAX_MDATA_BUF_SIZE        2000

/* PEER EVENTS */
#define   MSDP_PEER_START_EVT            0
#define   MSDP_PEER_ACTIVE_OPEN_EVT      1
#define   MSDP_PEER_PASSIVE_OPEN_EVT     2
#define   MSDP_CONN_ESTAB_EVT            3
#define   MSDP_CONNECT_RETRY_EXPRY_EVT   4
#define   MSDP_PEER_STOP_EVT             5
#define   MSDP_PEER_HOLD_TMR_EXPRY       6
#define   MSDP_PEER_TLV_FORMAT_ERR_EVT   7
#define   MSDP_MAX_PEER_EVENT            8
#define   MSDP_PEER_OTHER_EVT            10


#define   MSDP_PEER_FORWARD              1
#define   MSDP_PEER_DONT_FORWARD         2

#define   MSDP_INVALID_SOCK_ID     -1

#define   MSDP_ACTIVE_CONNECTION   1
#define   MSDP_PASSIVE_CONNECTION  2

#define   MSDP_SKT_WRITE_READY     1
#define   MSDP_SKT_READ_READY      2

#define   MSDP_IF_OPERSTATUS_UP      1
#define   MSDP_IF_OPERSTATUS_DOWN    2

#define   MSDP_TCP_OPEN_FAILURE    0
#define   MSDP_TCP_OPEN_SUCCESS    1
#define   MSDP_TCP_OPEN_INPROGRESS 2

#define   MSDP_TMR_NOT_RUNNING     0
#define   MSDP_TMR_RUNNING         1

#define   NO_OF_TICKS_PER_SEC      SYS_NUM_OF_TIME_UNITS_IN_A_SEC

/* Mem pool Ids */
#define   MSDP_QUE_MEMPOOL_ID      MSDPMemPoolIds[MAX_MSDP_QUEUE_MSGS_SIZING_ID]
#define   MSDP_PEER_ENTRY_MEMPOOL_ID  MSDPMemPoolIds[MAX_MSDP_FSMSDPPEERTABLE_SIZING_ID]
#define   MSDP_FSMSDPPEER_RX_TX_BUF_POOLID   MSDPMemPoolIds[MAX_MSDP_PEER_RX_TX_BUFFER_SIZING_ID]
#define   MSDP_MESH_MEMPOOL_ID   MSDPMemPoolIds[MAX_MSDP_MESH_GROUPS_SIZING_ID]

#define   MSDP_PEER_MAX_RX_TX_BUF_LEN   (9*1024) /* has to be filled with dynamic pool size */
#define   MSDP_PEER_MAX_TEMP_TX_BUF_LEN  1500

#define   MSDP_IPV4_SA_ADVT_HDR_SIZE     8
#define   MSDP_V4_SA_ADVT_ONE_ENTRY_SIZE sizeof (tMsdpSaAdvtEntry) 
#define   MSDP_MIN_IPV4_SA_ADVT_SIZE \
           (MSDP_IPV4_SA_ADVT_HDR_SIZE + MSDP_V4_SA_ADVT_ONE_ENTRY_SIZE)

#define   MSDP_IPV6_SA_ADVT_HDR_SIZE     20 
#define   MSDP_V6_SA_ADVT_ONE_ENTRY_SIZE sizeof (tMsdp6SaAdvtEntry)
#define   MSDP_MIN_IPV6_SA_ADVT_SIZE     \
           (MSDP_IPV6_SA_ADVT_HDR_SIZE + MSDP_V6_SA_ADVT_ONE_ENTRY_SIZE) 

#define   MSDP_IPV4_SA_REQ_PKT_SIZE      8
#define   MSDP_IPV6_SA_REQ_PKT_SIZE      20

#define   MSDP_KEEP_ALIVE_PKT_SIZE       3

#define   MSDP_DEF_SRC_MASK_ADDR        0xFFFFFFFF
#define   MSDP_DEF_VRF_CTXT_ID           0
#define   MSDP_BITS_IN_ONE_BYTE          8

#define   MSDP_GET_OFFSET(x,y)   FSAP_OFFSETOF(x,y)

#endif  /* __MSDPDEFN_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file msdpdefn.h                      */
/*-----------------------------------------------------------------------*/

