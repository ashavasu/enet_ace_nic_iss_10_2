/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: msdpmibclig.h,v 1.2 2011/07/01 06:08:50 siva Exp $
*
* Description: Extern Declaration of the Mib objects
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */

extern UINT4 FsMsdpTraceLevel[11];

extern UINT4 FsMsdpIPv4AdminStat[11];

extern UINT4 FsMsdpIPv6AdminStat[11];

extern UINT4 FsMsdpCacheLifetime[11];

extern UINT4 FsMsdpMaxPeerSessions[11];

extern UINT4 FsMsdpMappingComponentId[11];

extern UINT4 FsMsdpListenerPort[11];

extern UINT4 FsMsdpPeerFilter[11];

extern UINT4 FsMsdpPeerAddrType[13];

extern UINT4 FsMsdpPeerRemoteAddress[13];

extern UINT4 FsMsdpPeerLocalAddress[13];

extern UINT4 FsMsdpPeerConnectRetryInterval[13];

extern UINT4 FsMsdpPeerHoldTimeConfigured[13];

extern UINT4 FsMsdpPeerKeepAliveConfigured[13];

extern UINT4 FsMsdpPeerDataTtl[13];

extern UINT4 FsMsdpPeerStatus[13];

extern UINT4 FsMsdpPeerEncapsulationType[13];

extern UINT4 FsMsdpPeerMD5AuthPassword[13];

extern UINT4 FsMsdpPeerMD5AuthPwdStat[13];

extern UINT4 FsMsdpPeerAdminStatus[13];

extern UINT4 FsMsdpSACacheAddrType[13];

extern UINT4 FsMsdpSACacheGroupAddr[13];

extern UINT4 FsMsdpSACacheSourceAddr[13];

extern UINT4 FsMsdpSACacheOriginRP[13];

extern UINT4 FsMsdpSACacheStatus[13];

extern UINT4 FsMsdpMeshGroupName[13];

extern UINT4 FsMsdpMeshGroupAddrType[13];

extern UINT4 FsMsdpMeshGroupPeerAddress[13];

extern UINT4 FsMsdpMeshGroupStatus[13];

extern UINT4 FsMsdpRPAddrType[13];

extern UINT4 FsMsdpRPAddress[13];

extern UINT4 FsMsdpRPStatus[13];

extern UINT4 FsMsdpPeerFilterAddrType[13];

extern UINT4 FsMsdpPeerFilterRouteMap[13];

extern UINT4 FsMsdpPeerFilterStatus[13];

extern UINT4 FsMsdpSARedistributionAddrType[13];

extern UINT4 FsMsdpSARedistributionStatus[13];

extern UINT4 FsMsdpSARedistributionRouteMap[13];

extern UINT4 FsMsdpSARedistributionRouteMapStat[13];

extern UINT4 FsMsdpRtrId[11];
