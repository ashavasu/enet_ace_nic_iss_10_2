/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
 * $Id: msdpwrg.h,v 1.2 2011/07/01 06:09:27 siva Exp $
*
* Description: This file contains the prototype(wr) for Msdp 
*********************************************************************/
#ifndef _MSDPWR_H
#define _MSDPWR_H


INT4 GetNextIndexFsMsdpPeerTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsMsdpSACacheTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsMsdpMeshGroupTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsMsdpRPTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsMsdpPeerFilterTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsMsdpSARedistributionTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMsdpTraceLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpIPv4AdminStatGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpIPv6AdminStatGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpCacheLifetimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpNumSACacheEntriesGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpMaxPeerSessionsGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpMappingComponentIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpListenerPortGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerFilterGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpStatEstPeerCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerRPFFailuresGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerInSAsGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerOutSAsGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerInSARequestsGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerOutSARequestsGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerInControlMessagesGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerOutControlMessagesGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerInDataPacketsGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerOutDataPacketsGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerFsmEstablishedTransitionsGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerFsmEstablishedTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerInMessageTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerLocalAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerConnectRetryIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerHoldTimeConfiguredGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerKeepAliveConfiguredGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerDataTtlGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerRemotePortGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerLocalPortGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerEncapsulationTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerConnectionAttemptsGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerDiscontinuityTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerMD5AuthPasswordGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerMD5AuthPwdStatGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerMD5FailCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerMD5SuccessCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerInSAResponsesGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerOutSAResponsesGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerUpTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerInKeepAliveCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerOutKeepAliveCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerDataTtlErrorCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpSACachePeerLearnedFromGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpSACacheRPFPeerGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpSACacheInSAsGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpSACacheInDataPacketsGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpSACacheUpTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpSACacheExpiryTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpSACacheStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpMeshGroupStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpRPAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpRPOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpRPStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerFilterRouteMapGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerFilterStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpSARedistributionStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpSARedistributionRouteMapGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpSARedistributionRouteMapStatGet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpTraceLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpIPv4AdminStatTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpIPv6AdminStatTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpCacheLifetimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpMaxPeerSessionsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpMappingComponentIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpListenerPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerFilterTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerLocalAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerConnectRetryIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerHoldTimeConfiguredTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerKeepAliveConfiguredTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerDataTtlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerEncapsulationTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerMD5AuthPasswordTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerMD5AuthPwdStatTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpSACacheStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpMeshGroupStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpRPAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpRPStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerFilterRouteMapTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerFilterStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpSARedistributionStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpSARedistributionRouteMapTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpSARedistributionRouteMapStatTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsdpTraceLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpIPv4AdminStatSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpIPv6AdminStatSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpCacheLifetimeSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpMaxPeerSessionsSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpMappingComponentIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpListenerPortSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerFilterSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerLocalAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerConnectRetryIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerHoldTimeConfiguredSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerKeepAliveConfiguredSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerDataTtlSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerEncapsulationTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerMD5AuthPasswordSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerMD5AuthPwdStatSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpSACacheStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpMeshGroupStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpRPAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpRPStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerFilterRouteMapSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpPeerFilterStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpSARedistributionStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpSARedistributionRouteMapSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpSARedistributionRouteMapStatSet(tSnmpIndex *, tRetVal *);
INT4 FsMsdpTraceLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMsdpIPv4AdminStatDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMsdpIPv6AdminStatDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMsdpCacheLifetimeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMsdpMaxPeerSessionsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMsdpMappingComponentIdDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMsdpListenerPortDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMsdpPeerFilterDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMsdpPeerTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsMsdpSACacheTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsMsdpMeshGroupTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsMsdpRPTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsMsdpPeerFilterTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsMsdpSARedistributionTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

VOID  RegisterFSMSDP PROTO ((VOID));
VOID  UnRegisterFSMSDP PROTO ((VOID));

#endif /* _MSDPWR_H */
