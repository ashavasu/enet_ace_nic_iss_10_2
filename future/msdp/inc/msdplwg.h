/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: msdplwg.h,v 1.2 2011/07/01 06:08:50 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsMsdpPeerTable. */
INT1
nmhValidateIndexInstanceFsMsdpPeerTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for FsMsdpSACacheTable. */
INT1
nmhValidateIndexInstanceFsMsdpSACacheTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for FsMsdpMeshGroupTable. */
INT1
nmhValidateIndexInstanceFsMsdpMeshGroupTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for FsMsdpRPTable. */
INT1
nmhValidateIndexInstanceFsMsdpRPTable ARG_LIST((INT4 ));

/* Proto Validate Index Instance for FsMsdpPeerFilterTable. */
INT1
nmhValidateIndexInstanceFsMsdpPeerFilterTable ARG_LIST((INT4 ));

/* Proto Validate Index Instance for FsMsdpSARedistributionTable. */
INT1
nmhValidateIndexInstanceFsMsdpSARedistributionTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMsdpPeerTable  */

INT1
nmhGetFirstIndexFsMsdpPeerTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto Type for Low Level GET FIRST fn for FsMsdpSACacheTable  */

INT1
nmhGetFirstIndexFsMsdpSACacheTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE * ));

/* Proto Type for Low Level GET FIRST fn for FsMsdpMeshGroupTable  */

INT1
nmhGetFirstIndexFsMsdpMeshGroupTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto Type for Low Level GET FIRST fn for FsMsdpRPTable  */

INT1
nmhGetFirstIndexFsMsdpRPTable ARG_LIST((INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsMsdpPeerFilterTable  */

INT1
nmhGetFirstIndexFsMsdpPeerFilterTable ARG_LIST((INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsMsdpSARedistributionTable  */

INT1
nmhGetFirstIndexFsMsdpSARedistributionTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMsdpPeerTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMsdpSACacheTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMsdpMeshGroupTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMsdpRPTable ARG_LIST((INT4 , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMsdpPeerFilterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMsdpSARedistributionTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpTraceLevel ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpIPv4AdminStat ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpIPv6AdminStat ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpCacheLifetime ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpNumSACacheEntries ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpMaxPeerSessions ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpMappingComponentId ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpListenerPort ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerFilter ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerCount ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpStatEstPeerCount ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerRPFFailures ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerInSAs ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerOutSAs ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerInSARequests ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerOutSARequests ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerInControlMessages ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerOutControlMessages ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerInDataPackets ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerOutDataPackets ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerFsmEstablishedTransitions ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerFsmEstablishedTime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerInMessageTime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerLocalAddress ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerConnectRetryInterval ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerHoldTimeConfigured ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerKeepAliveConfigured ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerDataTtl ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerRemotePort ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerLocalPort ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerEncapsulationType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerConnectionAttempts ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerDiscontinuityTime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerMD5AuthPassword ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerMD5AuthPwdStat ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerMD5FailCount ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerMD5SuccessCount ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerInSAResponses ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerOutSAResponses ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerUpTime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerInKeepAliveCount ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerOutKeepAliveCount ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerDataTtlErrorCount ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerAdminStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpSACachePeerLearnedFrom ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpSACacheRPFPeer ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpSACacheInSAs ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpSACacheInDataPackets ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpSACacheUpTime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpSACacheExpiryTime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpSACacheStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpMeshGroupStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpRPAddress ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpRPOperStatus ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpRPStatus ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerFilterRouteMap ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpPeerFilterStatus ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpSARedistributionStatus ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpSARedistributionRouteMap ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsdpSARedistributionRouteMapStat ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpTraceLevel ARG_LIST((INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpIPv4AdminStat ARG_LIST((INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpIPv6AdminStat ARG_LIST((INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpCacheLifetime ARG_LIST((UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpMaxPeerSessions ARG_LIST((INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpMappingComponentId ARG_LIST((INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpListenerPort ARG_LIST((INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpPeerFilter ARG_LIST((INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpPeerLocalAddress ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpPeerConnectRetryInterval ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpPeerHoldTimeConfigured ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpPeerKeepAliveConfigured ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpPeerDataTtl ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpPeerStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpPeerEncapsulationType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpPeerMD5AuthPassword ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpPeerMD5AuthPwdStat ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpPeerAdminStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpSACacheStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpMeshGroupStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpRPAddress ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpRPStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpPeerFilterRouteMap ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpPeerFilterStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpSARedistributionStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpSARedistributionRouteMap ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsdpSARedistributionRouteMapStat ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpTraceLevel ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpIPv4AdminStat ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpIPv6AdminStat ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpCacheLifetime ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpMaxPeerSessions ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpMappingComponentId ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpListenerPort ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpPeerFilter ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpPeerLocalAddress ARG_LIST((UINT4 * , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpPeerConnectRetryInterval ARG_LIST((UINT4 * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpPeerHoldTimeConfigured ARG_LIST((UINT4 * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpPeerKeepAliveConfigured ARG_LIST((UINT4 * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpPeerDataTtl ARG_LIST((UINT4 * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpPeerStatus ARG_LIST((UINT4 * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpPeerEncapsulationType ARG_LIST((UINT4 * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpPeerMD5AuthPassword ARG_LIST((UINT4 * , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpPeerMD5AuthPwdStat ARG_LIST((UINT4 * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpPeerAdminStatus ARG_LIST((UINT4 * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpSACacheStatus ARG_LIST((UINT4 * , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpMeshGroupStatus ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpRPAddress ARG_LIST((UINT4 * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpRPStatus ARG_LIST((UINT4 * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpPeerFilterRouteMap ARG_LIST((UINT4 * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpPeerFilterStatus ARG_LIST((UINT4 * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpSARedistributionStatus ARG_LIST((UINT4 * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpSARedistributionRouteMap ARG_LIST((UINT4 * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsdpSARedistributionRouteMapStat ARG_LIST((UINT4 * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMsdpTraceLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMsdpIPv4AdminStat ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMsdpIPv6AdminStat ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMsdpCacheLifetime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMsdpMaxPeerSessions ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMsdpMappingComponentId ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMsdpListenerPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMsdpPeerFilter ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMsdpPeerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMsdpSACacheTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMsdpMeshGroupTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMsdpRPTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMsdpPeerFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMsdpSARedistributionTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
