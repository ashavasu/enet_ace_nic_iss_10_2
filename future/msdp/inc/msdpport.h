/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: msdpport.h,v 1.4 2011/10/13 10:16:08 siva Exp $
*
* This file contains prototypes for the porting functions defined in Msdp.
*********************************************************************/
INT4 Msd6PortIsAddrMulti (UINT1 *);
INT4 MsdpPortGetVlanIdFromIpAddress(UINT4, UINT2 *);
INT4 MsdpPortIsValidLocalAddress (UINT1 *);
INT4 Msd6PortIsValidLocalAddress (UINT1 *);
INT4 Msd6PortGetVlanIdFromIp6Address(tSNMP_OCTET_STRING_TYPE *, UINT2 *);
INT4
Msd6PortIpv6GetRoute (tNetIpv6RtInfoQueryMsg * pNetIpv6RtQuery,
                      tNetIpv6RtInfo * pNetIpv6RtInfo);

PUBLIC VOID
MsdpPortGetRouterId (INT4 i4CtxtId, UINT4 *pu4RtrId);
INT4
MsdpPortGetIpPortFromIfIndex ( UINT4 u4IfIndex, UINT4 *pu4IfIndex);
INT4
MsdpPortIpGetRoute (tRtInfoQueryMsg * pRtQuery, tNetIpv4RtInfo * pNetIpRtInfo);

VOID MsdpPortInformSAToMrp (tMsdpMrpSaAdvt *pMsdpSaInfo);

VOID MsdpPortHandleIfChg (tNetIpv4IfInfo *pNetIfInfo, UINT4 u4BitMap);
VOID MsdpPortDeRegiterWithIP (VOID);
INT4 MsdpPortRegiterWithIP (VOID);
INT4 MsdpPortGetIfInfo (UINT4 u4IfIndex, UINT1 *pu1OperStatus);

PUBLIC VOID
Msd6PortHandleIPV6IfChange (tNetIpv6HliParams * pIp6HliParams);
INT4 Msd6PortDeRegiterWithIP6 (VOID);
INT4 Msd6PortRegiterWithIP6 (VOID);
VOID
Msd6PortHandleV6IfChg (tNetIpv6IfStatChange * pIfStatusChange);


PUBLIC INT4
MsdpPortSARtMapRule (tIPvXAddr *pSrcAddr, tIPvXAddr *pGrpAddr, UINT1 *pRtMap);
PUBLIC INT4 
MsdpPortPeerRMapRule (tIPvXAddr *pPeerAddr, UINT1 *pRtMap);
