/********************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: msdptmr.h,v 1.1.1.1 2011/01/17 05:57:32 siva Exp $
 *
 * Description: This file contains definitions for msdp Timer
 *******************************************************************/

#ifndef __MSDPTMR_H__
#define __MSDPTMR_H__



/* constants for timer types */
typedef enum {
	MSDP_TMR1 =0,
	MSDP_MAX_TMRS = 1 
} enMsdpTmrId;

#define     NO_OF_TICKS_PER_SEC             SYS_NUM_OF_TIME_UNITS_IN_A_SEC

typedef struct _MSDP_TMR_DESC {
    VOID                (*pTmrExpFunc)(VOID *);
    INT2                i2Offset;
                            /* If this field is -1 then the fn takes no
                             * parameter
                             */
    UINT2               u2Pad;
                            /* Included for 4-byte Alignment
                             */
} tMsdpTmrDesc;

typedef struct _MSDP_TIMER {
       tTmrAppTimer    tmrNode;
       enMsdpTmrId     eMsdpTmrId;
} tMsdpTmr;




#endif  /* __MSDPTMR_H__  */


/*-----------------------------------------------------------------------*/
/*                       End of the file  msdptmr.h                      */
/*-----------------------------------------------------------------------*/
