/********************************************************************
 * Copyright (C) Future Software Limited,1997-98,2001
 *
 * $Id: msdptrc.h,v 1.2 2011/01/21 15:04:10 siva Exp $
 *
 * Description:This file contains the prototypes and the macros  
 *             used for trace module.
 *******************************************************************/


#ifndef __MSDPTRC_H__
#define __MSDPTRC_H__

#define  MSDP_TRC_FLAG  gMsdpGlobals.MsdpGlbMib.i4FsMsdpTraceLevel
#define  MSDP_NAME      "MSDP"               

#ifdef TRACE_WANTED

#define  MSDP_TRC(x)       MsdpTrcPrint( __FILE__, __LINE__, MsdpTrc x)
#define  MSDP_TRC_FUNC(x)  MsdpTrcPrint( __FILE__, __LINE__, MsdpTrc x)
#define  MSDP_TRC_CRIT(x)  MsdpTrcPrint( __FILE__, __LINE__, MsdpTrc x)
#define  MSDP_TRC_PKT(x)   MsdpTrcWrite( MsdpTrc x)
#else
#define  MSDP_TRC(x) 
#define  MSDP_TRC_FUNC(x)
#define  MSDP_TRC_CRIT(x)
#define  MSDP_TRC_PKT(x)

#endif /* TRACE_WANTED */

#define  MSDP_TRC_CACHE (0x1 << 1)
#define  MSDP_TRC_PEER  (0x1 << 2)
#define  MSDP_TRC_IN    (0x1 << 3)
#define  MSDP_TRC_OUT   (0x1 << 4)
#define  MSDP_TRC_PIMIF (0x1 << 5)
#define  MSDP_TRC_PORT  (0x1 << 6)
#define  MSDP_TRC_SOCK  (0x1 << 7)
#define  MSDP_FN_ENTRY  (0x1 << 9)
#define  MSDP_FN_EXIT   (0x1 << 10)
#define  MSDP_CLI_TRC   (0x1 << 11)
#define  MSDP_MAIN_TRC  (0x1 << 12)
#define  MSDP_PKT_TRC   (0x1 << 13)
#define  MSDP_QUE_TRC   (0x1 << 14)
#define  MSDP_TASK_TRC  (0x1 << 15)
#define  MSDP_TMR_TRC   (0x1 << 16)
#define  MSDP_UTIL_TRC  (0x1 << 17)
#define  MSDP_ALL_TRC   MSDP_FN_ENTRY |\
                        MSDP_FN_EXIT  |\
                        MSDP_CLI_TRC  |\
                        MSDP_MAIN_TRC |\
                        MSDP_PKT_TRC  |\
                        MSDP_QUE_TRC  |\
                        MSDP_TASK_TRC |\
                        MSDP_TMR_TRC  |\
                        MSDP_UTIL_TRC


#endif /* _MSDPTRC_H */


/*-----------------------------------------------------------------------*/
/*                       End of the file  msdptrc.h                      */
/*-----------------------------------------------------------------------*/
