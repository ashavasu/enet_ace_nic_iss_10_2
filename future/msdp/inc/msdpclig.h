/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: msdpclig.h,v 1.1.1.1 2011/01/17 05:57:32 siva Exp $
 *
 * Description: This file contains prototypes of  MSDP module
 *              CLI related functions.
 *******************************************************************/
#define CLI_MSDP_MAX_ERR 10
#define MSDP_CLI_MAX_ARGS 16

INT4 MsdpCliSetFsMsdpTraceLevel (tCliHandle CliHandle,UINT4 *pFsMsdpTraceLevel);


INT4 MsdpCliSetFsMsdpIPv4AdminStat (tCliHandle CliHandle,UINT4 *pFsMsdpIPv4AdminStat);


INT4 MsdpCliSetFsMsdpIPv6AdminStat (tCliHandle CliHandle,UINT4 *pFsMsdpIPv6AdminStat);


INT4 MsdpCliSetFsMsdpCacheLifetime (tCliHandle CliHandle,UINT4 *pFsMsdpCacheLifetime);


INT4 MsdpCliSetFsMsdpMaxPeerSessions (tCliHandle CliHandle,UINT4 *pFsMsdpMaxPeerSessions);


INT4 MsdpCliSetFsMsdpMappingComponentId (tCliHandle CliHandle,UINT4 *pFsMsdpMappingComponentId);


INT4 MsdpCliSetFsMsdpListenerPort (tCliHandle CliHandle,UINT4 *pFsMsdpListenerPort);


INT4 MsdpCliSetFsMsdpPeerFilter (tCliHandle CliHandle,UINT4 *pFsMsdpPeerFilter);


INT4 MsdpCliSetFsMsdpPeerTable (tCliHandle CliHandle,tMsdpFsMsdpPeerTable * pMsdpSetFsMsdpPeerTable,tMsdpIsSetFsMsdpPeerTable * pMsdpIsSetFsMsdpPeerTable);

INT4 MsdpCliSetFsMsdpSACacheTable (tCliHandle CliHandle,tMsdpFsMsdpSACacheTable * pMsdpSetFsMsdpSACacheTable,tMsdpIsSetFsMsdpSACacheTable * pMsdpIsSetFsMsdpSACacheTable);

INT4 MsdpCliSetFsMsdpMeshGroupTable (tCliHandle CliHandle,tMsdpFsMsdpMeshGroupTable * pMsdpSetFsMsdpMeshGroupTable,tMsdpIsSetFsMsdpMeshGroupTable * pMsdpIsSetFsMsdpMeshGroupTable);

INT4 MsdpCliSetFsMsdpRPTable (tCliHandle CliHandle,tMsdpFsMsdpRPTable * pMsdpSetFsMsdpRPTable,tMsdpIsSetFsMsdpRPTable * pMsdpIsSetFsMsdpRPTable);

INT4 MsdpCliSetFsMsdpPeerFilterTable (tCliHandle CliHandle,tMsdpFsMsdpPeerFilterTable * pMsdpSetFsMsdpPeerFilterTable,tMsdpIsSetFsMsdpPeerFilterTable * pMsdpIsSetFsMsdpPeerFilterTable);

INT4 MsdpCliSetFsMsdpSARedistributionTable (tCliHandle CliHandle,tMsdpFsMsdpSARedistributionTable * pMsdpSetFsMsdpSARedistributionTable,tMsdpIsSetFsMsdpSARedistributionTable * pMsdpIsSetFsMsdpSARedistributionTable);


