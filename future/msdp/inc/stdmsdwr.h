/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdmsdwr.h,v 1.1.1.1 2011/01/17 05:57:32 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

#ifndef _STDMSDWR_H
#define _STDMSDWR_H

VOID RegisterSTDMSD(VOID);

VOID UnRegisterSTDMSD(VOID);
INT4 MsdpEnabledGet(tSnmpIndex *, tRetVal *);
INT4 MsdpCacheLifetimeGet(tSnmpIndex *, tRetVal *);
INT4 MsdpNumSACacheEntriesGet(tSnmpIndex *, tRetVal *);
INT4 MsdpRPAddressGet(tSnmpIndex *, tRetVal *);
INT4 MsdpEnabledSet(tSnmpIndex *, tRetVal *);
INT4 MsdpCacheLifetimeSet(tSnmpIndex *, tRetVal *);
INT4 MsdpRPAddressSet(tSnmpIndex *, tRetVal *);
INT4 MsdpEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MsdpCacheLifetimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MsdpRPAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MsdpEnabledDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 MsdpCacheLifetimeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 MsdpRPAddressDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexMsdpPeerTable(tSnmpIndex *, tSnmpIndex *);
INT4 MsdpPeerStateGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerRPFFailuresGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerInSAsGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerOutSAsGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerInSARequestsGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerOutSARequestsGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerInControlMessagesGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerOutControlMessagesGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerInDataPacketsGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerOutDataPacketsGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerFsmEstablishedTransitionsGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerFsmEstablishedTimeGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerInMessageTimeGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerLocalAddressGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerConnectRetryIntervalGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerHoldTimeConfiguredGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerKeepAliveConfiguredGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerDataTtlGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerStatusGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerRemotePortGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerLocalPortGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerEncapsulationTypeGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerConnectionAttemptsGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerDiscontinuityTimeGet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerLocalAddressSet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerConnectRetryIntervalSet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerHoldTimeConfiguredSet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerKeepAliveConfiguredSet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerDataTtlSet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerStatusSet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerEncapsulationTypeSet(tSnmpIndex *, tRetVal *);
INT4 MsdpPeerLocalAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MsdpPeerConnectRetryIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MsdpPeerHoldTimeConfiguredTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MsdpPeerKeepAliveConfiguredTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MsdpPeerDataTtlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MsdpPeerStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MsdpPeerEncapsulationTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MsdpPeerTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexMsdpSACacheTable(tSnmpIndex *, tSnmpIndex *);
INT4 MsdpSACachePeerLearnedFromGet(tSnmpIndex *, tRetVal *);
INT4 MsdpSACacheRPFPeerGet(tSnmpIndex *, tRetVal *);
INT4 MsdpSACacheInSAsGet(tSnmpIndex *, tRetVal *);
INT4 MsdpSACacheInDataPacketsGet(tSnmpIndex *, tRetVal *);
INT4 MsdpSACacheUpTimeGet(tSnmpIndex *, tRetVal *);
INT4 MsdpSACacheExpiryTimeGet(tSnmpIndex *, tRetVal *);
INT4 MsdpSACacheStatusGet(tSnmpIndex *, tRetVal *);
INT4 MsdpSACacheStatusSet(tSnmpIndex *, tRetVal *);
INT4 MsdpSACacheStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MsdpSACacheTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexMsdpMeshGroupTable(tSnmpIndex *, tSnmpIndex *);
INT4 MsdpMeshGroupStatusGet(tSnmpIndex *, tRetVal *);
INT4 MsdpMeshGroupStatusSet(tSnmpIndex *, tRetVal *);
INT4 MsdpMeshGroupStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MsdpMeshGroupTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _STDMSDWR_H */
