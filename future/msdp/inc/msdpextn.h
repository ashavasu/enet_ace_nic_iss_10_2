/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: msdpextn.h,v 1.1.1.1 2011/01/17 05:57:32 siva Exp $
 *
 * Description: This file contains extern declaration of global 
 *              variables of the msdp module.
 *******************************************************************/

#ifndef __MSDPEXTN_H__
#define __MSDPEXTN_H__

PUBLIC tMsdpGlobals gMsdpGlobals;
PUBLIC UINT1 gau1LinBuf[MSDP_PEER_MAX_TEMP_TX_BUF_LEN];
#endif/*__MSDPEXTN_H__*/
   
/*-----------------------------------------------------------------------*/
/*                       End of the file msdpextn.h                      */
/*-----------------------------------------------------------------------*/

