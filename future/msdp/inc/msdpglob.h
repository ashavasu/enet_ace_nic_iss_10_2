/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: msdpglob.h,v 1.1.1.1 2011/01/17 05:57:32 siva Exp $
 *
 * Description: This file contains declaration of global variables 
 *              of msdp module.
 *******************************************************************/

#ifndef __MSDPGLOBAL_H__
#define __MSDPGLOBAL_H__

tMsdpGlobals gMsdpGlobals;
UINT1 gau1LinBuf[MSDP_PEER_MAX_TEMP_TX_BUF_LEN];
#endif  /* __MSDPGLOBAL_H__ */


/*-----------------------------------------------------------------------*/
/*                       End of the file msdpglob.h                      */
/*-----------------------------------------------------------------------*/
