/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: msdpdefg.h,v 1.4 2012/03/30 13:54:31 siva Exp $
*
* Description: Macros used to fill the CLI structure and 
              index value in the respective structure.
*********************************************************************/


#define MSDP_FSMSDPPEERTABLE_POOLID   MSDPMemPoolIds[MAX_MSDP_FSMSDPPEERTABLE_SIZING_ID]


#define MSDP_FSMSDPPEERTABLE_ISSET_POOLID   MSDPMemPoolIds[MAX_MSDP_FSMSDPPEERTABLE_ISSET_SIZING_ID]


#define MSDP_FSMSDPSACACHETABLE_POOLID   MSDPMemPoolIds[MAX_MSDP_FSMSDPSACACHETABLE_SIZING_ID]


#define MSDP_FSMSDPSACACHETABLE_ISSET_POOLID   MSDPMemPoolIds[MAX_MSDP_FSMSDPSACACHETABLE_ISSET_SIZING_ID]


#define MSDP_FSMSDPMESHGROUPTABLE_POOLID   MSDPMemPoolIds[MAX_MSDP_FSMSDPMESHGROUPTABLE_SIZING_ID]


#define MSDP_FSMSDPMESHGROUPTABLE_ISSET_POOLID   MSDPMemPoolIds[MAX_MSDP_FSMSDPMESHGROUPTABLE_ISSET_SIZING_ID]


#define MSDP_FSMSDPRPTABLE_POOLID   MSDPMemPoolIds[MAX_MSDP_FSMSDPRPTABLE_SIZING_ID]


#define MSDP_FSMSDPRPTABLE_ISSET_POOLID   MSDPMemPoolIds[MAX_MSDP_FSMSDPRPTABLE_ISSET_SIZING_ID]


#define MSDP_FSMSDPPEERFILTERTABLE_POOLID   MSDPMemPoolIds[MAX_MSDP_FSMSDPPEERFILTERTABLE_SIZING_ID]


#define MSDP_FSMSDPPEERFILTERTABLE_ISSET_POOLID   MSDPMemPoolIds[MAX_MSDP_FSMSDPPEERFILTERTABLE_ISSET_SIZING_ID]


#define MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID   MSDPMemPoolIds[MAX_MSDP_FSMSDPSAREDISTRIBUTIONTABLE_SIZING_ID]


#define MSDP_FSMSDPSAREDISTRIBUTIONTABLE_ISSET_POOLID   MSDPMemPoolIds[MAX_MSDP_FSMSDPSAREDISTRIBUTIONTABLE_ISSET_SIZING_ID]
/* Macro used to fill the CLI structure for FsMsdpPeerEntry 
 using the input given in def file */

#define  MSDP_FILL_FSMSDPPEERTABLE_ARGS(pMsdpFsMsdpPeerEntry,\
   pMsdpIsSetFsMsdpPeerEntry,\
   pau1FsMsdpPeerAddrType,\
   pau1FsMsdpPeerRemoteAddress,\
   pau1FsMsdpPeerRemoteAddressLen,\
   pau1FsMsdpPeerLocalAddress,\
   pau1FsMsdpPeerLocalAddressLen,\
   pau1FsMsdpPeerConnectRetryInterval,\
   pau1FsMsdpPeerHoldTimeConfigured,\
   pau1FsMsdpPeerKeepAliveConfigured,\
   pau1FsMsdpPeerDataTtl,\
   pau1FsMsdpPeerStatus,\
   pau1FsMsdpPeerEncapsulationType,\
   pau1FsMsdpPeerMD5AuthPassword,\
   pau1FsMsdpPeerMD5AuthPasswordLen,\
   pau1FsMsdpPeerMD5AuthPwdStat,\
   pau1FsMsdpPeerAdminStatus)\
  {\
  if (pau1FsMsdpPeerAddrType != NULL)\
  {\
   pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType = *(INT4 *) (pau1FsMsdpPeerAddrType);\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerAddrType = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerAddrType = OSIX_FALSE;\
  }\
  if (pau1FsMsdpPeerRemoteAddress != NULL)\
  {\
   MEMCPY (pMsdpFsMsdpPeerEntry->MibObject.au1FsMsdpPeerRemoteAddress, pau1FsMsdpPeerRemoteAddress, *(INT4 *)pau1FsMsdpPeerRemoteAddressLen);\
   pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerRemoteAddressLen = *(INT4 *)pau1FsMsdpPeerRemoteAddressLen;\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerRemoteAddress = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerRemoteAddress = OSIX_FALSE;\
  }\
  if (pau1FsMsdpPeerLocalAddress != NULL)\
  {\
   MEMCPY (pMsdpFsMsdpPeerEntry->MibObject.au1FsMsdpPeerLocalAddress, pau1FsMsdpPeerLocalAddress, *(INT4 *)pau1FsMsdpPeerLocalAddressLen);\
   pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerLocalAddressLen = *(INT4 *)pau1FsMsdpPeerLocalAddressLen;\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerLocalAddress = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerLocalAddress = OSIX_FALSE;\
  }\
  if (pau1FsMsdpPeerConnectRetryInterval != NULL)\
  {\
   pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerConnectRetryInterval = *(INT4 *) (pau1FsMsdpPeerConnectRetryInterval);\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerConnectRetryInterval = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerConnectRetryInterval = OSIX_FALSE;\
  }\
  if (pau1FsMsdpPeerHoldTimeConfigured != NULL)\
  {\
   pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerHoldTimeConfigured = *(INT4 *) (pau1FsMsdpPeerHoldTimeConfigured);\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerHoldTimeConfigured = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerHoldTimeConfigured = OSIX_FALSE;\
  }\
  if (pau1FsMsdpPeerKeepAliveConfigured != NULL)\
  {\
   pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerKeepAliveConfigured = *(INT4 *) (pau1FsMsdpPeerKeepAliveConfigured);\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerKeepAliveConfigured = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerKeepAliveConfigured = OSIX_FALSE;\
  }\
  if (pau1FsMsdpPeerDataTtl != NULL)\
  {\
   pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerDataTtl = *(INT4 *) (pau1FsMsdpPeerDataTtl);\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerDataTtl = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerDataTtl = OSIX_FALSE;\
  }\
  if (pau1FsMsdpPeerStatus != NULL)\
  {\
   pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus = *(INT4 *) (pau1FsMsdpPeerStatus);\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerStatus = OSIX_FALSE;\
  }\
  if (pau1FsMsdpPeerEncapsulationType != NULL)\
  {\
   pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerEncapsulationType = *(INT4 *) (pau1FsMsdpPeerEncapsulationType);\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerEncapsulationType = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerEncapsulationType = OSIX_FALSE;\
  }\
  if (pau1FsMsdpPeerMD5AuthPassword != NULL)\
  {\
   MEMCPY (pMsdpFsMsdpPeerEntry->MibObject.au1FsMsdpPeerMD5AuthPassword, pau1FsMsdpPeerMD5AuthPassword, *(INT4 *)pau1FsMsdpPeerMD5AuthPasswordLen);\
   pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerMD5AuthPasswordLen = *(INT4 *)pau1FsMsdpPeerMD5AuthPasswordLen;\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerMD5AuthPassword = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerMD5AuthPassword = OSIX_FALSE;\
  }\
  if (pau1FsMsdpPeerMD5AuthPwdStat != NULL)\
  {\
   pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerMD5AuthPwdStat = *(INT4 *) (pau1FsMsdpPeerMD5AuthPwdStat);\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerMD5AuthPwdStat = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerMD5AuthPwdStat = OSIX_FALSE;\
  }\
  if (pau1FsMsdpPeerAdminStatus != NULL)\
  {\
   pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAdminStatus = *(INT4 *) (pau1FsMsdpPeerAdminStatus);\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerAdminStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerAdminStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsMsdpSACacheEntry 
 using the input given in def file */

#define  MSDP_FILL_FSMSDPSACACHETABLE_ARGS(pMsdpFsMsdpSACacheEntry,\
   pMsdpIsSetFsMsdpSACacheEntry,\
   pau1FsMsdpSACacheAddrType,\
   pau1FsMsdpSACacheGroupAddr,\
   pau1FsMsdpSACacheGroupAddrLen,\
   pau1FsMsdpSACacheSourceAddr,\
   pau1FsMsdpSACacheSourceAddrLen,\
   pau1FsMsdpSACacheOriginRP,\
   pau1FsMsdpSACacheOriginRPLen,\
   pau1FsMsdpSACacheStatus)\
  {\
  if (pau1FsMsdpSACacheAddrType != NULL)\
  {\
   pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheAddrType = *(INT4 *) (pau1FsMsdpSACacheAddrType);\
   pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheAddrType = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheAddrType = OSIX_FALSE;\
  }\
  if (pau1FsMsdpSACacheGroupAddr != NULL)\
  {\
   MEMCPY (pMsdpFsMsdpSACacheEntry->MibObject.au1FsMsdpSACacheGroupAddr, pau1FsMsdpSACacheGroupAddr, *(INT4 *)pau1FsMsdpSACacheGroupAddrLen);\
   pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheGroupAddrLen = *(INT4 *)pau1FsMsdpSACacheGroupAddrLen;\
   pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheGroupAddr = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheGroupAddr = OSIX_FALSE;\
  }\
  if (pau1FsMsdpSACacheSourceAddr != NULL)\
  {\
   MEMCPY (pMsdpFsMsdpSACacheEntry->MibObject.au1FsMsdpSACacheSourceAddr, pau1FsMsdpSACacheSourceAddr, *(INT4 *)pau1FsMsdpSACacheSourceAddrLen);\
   pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheSourceAddrLen = *(INT4 *)pau1FsMsdpSACacheSourceAddrLen;\
   pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheSourceAddr = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheSourceAddr = OSIX_FALSE;\
  }\
  if (pau1FsMsdpSACacheOriginRP != NULL)\
  {\
   MEMCPY (pMsdpFsMsdpSACacheEntry->MibObject.au1FsMsdpSACacheOriginRP, pau1FsMsdpSACacheOriginRP, *(INT4 *)pau1FsMsdpSACacheOriginRPLen);\
   pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheOriginRPLen = *(INT4 *)pau1FsMsdpSACacheOriginRPLen;\
   pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheOriginRP = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheOriginRP = OSIX_FALSE;\
  }\
  if (pau1FsMsdpSACacheStatus != NULL)\
  {\
   pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus = *(INT4 *) (pau1FsMsdpSACacheStatus);\
   pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsMsdpMeshGroupEntry 
 using the input given in def file */

#define  MSDP_FILL_FSMSDPMESHGROUPTABLE_ARGS(pMsdpFsMsdpMeshGroupEntry,\
   pMsdpIsSetFsMsdpMeshGroupEntry,\
   pau1FsMsdpMeshGroupName,\
   pau1FsMsdpMeshGroupNameLen,\
   pau1FsMsdpMeshGroupAddrType,\
   pau1FsMsdpMeshGroupPeerAddress,\
   pau1FsMsdpMeshGroupPeerAddressLen,\
   pau1FsMsdpMeshGroupStatus)\
  {\
  if (pau1FsMsdpMeshGroupName != NULL)\
  {\
   MEMCPY (pMsdpFsMsdpMeshGroupEntry->MibObject.au1FsMsdpMeshGroupName, pau1FsMsdpMeshGroupName, *(INT4 *)pau1FsMsdpMeshGroupNameLen);\
   pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupNameLen = *(INT4 *)pau1FsMsdpMeshGroupNameLen;\
   pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupName = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupName = OSIX_FALSE;\
  }\
  if (pau1FsMsdpMeshGroupAddrType != NULL)\
  {\
   pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupAddrType = *(INT4 *) (pau1FsMsdpMeshGroupAddrType);\
   pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupAddrType = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupAddrType = OSIX_FALSE;\
  }\
  if (pau1FsMsdpMeshGroupPeerAddress != NULL)\
  {\
   MEMCPY (pMsdpFsMsdpMeshGroupEntry->MibObject.au1FsMsdpMeshGroupPeerAddress, pau1FsMsdpMeshGroupPeerAddress, *(INT4 *)pau1FsMsdpMeshGroupPeerAddressLen);\
   pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupPeerAddressLen = *(INT4 *)pau1FsMsdpMeshGroupPeerAddressLen;\
   pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupPeerAddress = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupPeerAddress = OSIX_FALSE;\
  }\
  if (pau1FsMsdpMeshGroupStatus != NULL)\
  {\
   pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupStatus = *(INT4 *) (pau1FsMsdpMeshGroupStatus);\
   pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsMsdpRPEntry 
 using the input given in def file */

#define  MSDP_FILL_FSMSDPRPTABLE_ARGS(pMsdpFsMsdpRPEntry,\
   pMsdpIsSetFsMsdpRPEntry,\
   pau1FsMsdpRPAddrType,\
   pau1FsMsdpRPAddress,\
   pau1FsMsdpRPAddressLen,\
   pau1FsMsdpRPStatus)\
  {\
  if (pau1FsMsdpRPAddrType != NULL)\
  {\
   pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPAddrType = *(INT4 *) (pau1FsMsdpRPAddrType);\
   pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPAddrType = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPAddrType = OSIX_FALSE;\
  }\
  if (pau1FsMsdpRPAddress != NULL)\
  {\
   MEMCPY (pMsdpFsMsdpRPEntry->MibObject.au1FsMsdpRPAddress, pau1FsMsdpRPAddress, *(INT4 *)pau1FsMsdpRPAddressLen);\
   pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPAddressLen = *(INT4 *)pau1FsMsdpRPAddressLen;\
   pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPAddress = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPAddress = OSIX_FALSE;\
  }\
  if (pau1FsMsdpRPStatus != NULL)\
  {\
   pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus = *(INT4 *) (pau1FsMsdpRPStatus);\
   pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsMsdpPeerFilterEntry 
 using the input given in def file */

#define  MSDP_FILL_FSMSDPPEERFILTERTABLE_ARGS(pMsdpFsMsdpPeerFilterEntry,\
   pMsdpIsSetFsMsdpPeerFilterEntry,\
   pau1FsMsdpPeerFilterAddrType,\
   pau1FsMsdpPeerFilterRouteMap,\
   pau1FsMsdpPeerFilterRouteMapLen,\
   pau1FsMsdpPeerFilterStatus)\
  {\
  if (pau1FsMsdpPeerFilterAddrType != NULL)\
  {\
   pMsdpFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterAddrType = *(INT4 *) (pau1FsMsdpPeerFilterAddrType);\
   pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterAddrType = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterAddrType = OSIX_FALSE;\
  }\
  if (pau1FsMsdpPeerFilterRouteMap != NULL)\
  {\
   MEMCPY (pMsdpFsMsdpPeerFilterEntry->MibObject.au1FsMsdpPeerFilterRouteMap, pau1FsMsdpPeerFilterRouteMap, *(INT4 *)pau1FsMsdpPeerFilterRouteMapLen);\
   pMsdpFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterRouteMapLen = *(INT4 *)pau1FsMsdpPeerFilterRouteMapLen;\
   pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterRouteMap = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterRouteMap = OSIX_FALSE;\
  }\
  if (pau1FsMsdpPeerFilterStatus != NULL)\
  {\
   pMsdpFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterStatus = *(INT4 *) (pau1FsMsdpPeerFilterStatus);\
   pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsMsdpSARedistributionEntry 
 using the input given in def file */

#define  MSDP_FILL_FSMSDPSAREDISTRIBUTIONTABLE_ARGS(pMsdpFsMsdpSARedistributionEntry,\
   pMsdpIsSetFsMsdpSARedistributionEntry,\
   pau1FsMsdpSARedistributionAddrType,\
   pau1FsMsdpSARedistributionStatus,\
   pau1FsMsdpSARedistributionRouteMap,\
   pau1FsMsdpSARedistributionRouteMapLen,\
   pau1FsMsdpSARedistributionRouteMapStat)\
  {\
  if (pau1FsMsdpSARedistributionAddrType != NULL)\
  {\
   pMsdpFsMsdpSARedistributionEntry->MibObject.i4FsMsdpSARedistributionAddrType = *(INT4 *) (pau1FsMsdpSARedistributionAddrType);\
   pMsdpIsSetFsMsdpSARedistributionEntry->bFsMsdpSARedistributionAddrType = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpSARedistributionEntry->bFsMsdpSARedistributionAddrType = OSIX_FALSE;\
  }\
  if (pau1FsMsdpSARedistributionStatus != NULL)\
  {\
   pMsdpFsMsdpSARedistributionEntry->MibObject.i4FsMsdpSARedistributionStatus = *(INT4 *) (pau1FsMsdpSARedistributionStatus);\
   pMsdpIsSetFsMsdpSARedistributionEntry->bFsMsdpSARedistributionStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpSARedistributionEntry->bFsMsdpSARedistributionStatus = OSIX_FALSE;\
  }\
  if (pau1FsMsdpSARedistributionRouteMap != NULL)\
  {\
   MEMCPY (pMsdpFsMsdpSARedistributionEntry->MibObject.au1FsMsdpSARedistributionRouteMap, pau1FsMsdpSARedistributionRouteMap, *(INT4 *)pau1FsMsdpSARedistributionRouteMapLen);\
   pMsdpFsMsdpSARedistributionEntry->MibObject.i4FsMsdpSARedistributionRouteMapLen = *(INT4 *)pau1FsMsdpSARedistributionRouteMapLen;\
   pMsdpIsSetFsMsdpSARedistributionEntry->bFsMsdpSARedistributionRouteMap = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpSARedistributionEntry->bFsMsdpSARedistributionRouteMap = OSIX_FALSE;\
  }\
  if (pau1FsMsdpSARedistributionRouteMapStat != NULL)\
  {\
   pMsdpFsMsdpSARedistributionEntry->MibObject.i4FsMsdpSARedistributionRouteMapStat = *(INT4 *) (pau1FsMsdpSARedistributionRouteMapStat);\
   pMsdpIsSetFsMsdpSARedistributionEntry->bFsMsdpSARedistributionRouteMapStat = OSIX_TRUE;\
  }\
  else\
  {\
   pMsdpIsSetFsMsdpSARedistributionEntry->bFsMsdpSARedistributionRouteMapStat = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the index values in  structure for FsMsdpPeerEntry 
 using the input given in def file */

#define  MSDP_FILL_FSMSDPPEERTABLE_INDEX(pMsdpFsMsdpPeerEntry,\
   pau1FsMsdpPeerAddrType,\
   pau1FsMsdpPeerRemoteAddress)\
  {\
  if (pau1FsMsdpPeerAddrType != NULL)\
  {\
   pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType = *(INT4 *) (pau1FsMsdpPeerAddrType);\
  }\
  if (pau1FsMsdpPeerRemoteAddress != NULL)\
  {\
   MEMCPY (pMsdpFsMsdpPeerEntry->MibObject.au1FsMsdpPeerRemoteAddress, pau1FsMsdpPeerRemoteAddress, *(INT4 *)pau1FsMsdpPeerRemoteAddressLen);\
   pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerRemoteAddressLen = *(INT4 *)pau1FsMsdpPeerRemoteAddressLen;\
  }\
 }
/* Macro used to fill the index values in  structure for FsMsdpSACacheEntry 
 using the input given in def file */

#define  MSDP_FILL_FSMSDPSACACHETABLE_INDEX(pMsdpFsMsdpSACacheEntry,\
   pau1FsMsdpSACacheAddrType,\
   pau1FsMsdpSACacheGroupAddr,\
   pau1FsMsdpSACacheSourceAddr,\
   pau1FsMsdpSACacheOriginRP)\
  {\
  if (pau1FsMsdpSACacheAddrType != NULL)\
  {\
   pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheAddrType = *(INT4 *) (pau1FsMsdpSACacheAddrType);\
  }\
  if (pau1FsMsdpSACacheGroupAddr != NULL)\
  {\
   MEMCPY (pMsdpFsMsdpSACacheEntry->MibObject.au1FsMsdpSACacheGroupAddr, pau1FsMsdpSACacheGroupAddr, *(INT4 *)pau1FsMsdpSACacheGroupAddrLen);\
   pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheGroupAddrLen = *(INT4 *)pau1FsMsdpSACacheGroupAddrLen;\
  }\
  if (pau1FsMsdpSACacheSourceAddr != NULL)\
  {\
   MEMCPY (pMsdpFsMsdpSACacheEntry->MibObject.au1FsMsdpSACacheSourceAddr, pau1FsMsdpSACacheSourceAddr, *(INT4 *)pau1FsMsdpSACacheSourceAddrLen);\
   pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheSourceAddrLen = *(INT4 *)pau1FsMsdpSACacheSourceAddrLen;\
  }\
  if (pau1FsMsdpSACacheOriginRP != NULL)\
  {\
   MEMCPY (pMsdpFsMsdpSACacheEntry->MibObject.au1FsMsdpSACacheOriginRP, pau1FsMsdpSACacheOriginRP, *(INT4 *)pau1FsMsdpSACacheOriginRPLen);\
   pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheOriginRPLen = *(INT4 *)pau1FsMsdpSACacheOriginRPLen;\
  }\
 }
/* Macro used to fill the index values in  structure for FsMsdpMeshGroupEntry 
 using the input given in def file */

#define  MSDP_FILL_FSMSDPMESHGROUPTABLE_INDEX(pMsdpFsMsdpMeshGroupEntry,\
   pau1FsMsdpMeshGroupName,\
   pau1FsMsdpMeshGroupAddrType,\
   pau1FsMsdpMeshGroupPeerAddress)\
  {\
  if (pau1FsMsdpMeshGroupName != NULL)\
  {\
   MEMCPY (pMsdpFsMsdpMeshGroupEntry->MibObject.au1FsMsdpMeshGroupName, pau1FsMsdpMeshGroupName, *(INT4 *)pau1FsMsdpMeshGroupNameLen);\
   pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupNameLen = *(INT4 *)pau1FsMsdpMeshGroupNameLen;\
  }\
  if (pau1FsMsdpMeshGroupAddrType != NULL)\
  {\
   pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupAddrType = *(INT4 *) (pau1FsMsdpMeshGroupAddrType);\
  }\
  if (pau1FsMsdpMeshGroupPeerAddress != NULL)\
  {\
   MEMCPY (pMsdpFsMsdpMeshGroupEntry->MibObject.au1FsMsdpMeshGroupPeerAddress, pau1FsMsdpMeshGroupPeerAddress, *(INT4 *)pau1FsMsdpMeshGroupPeerAddressLen);\
   pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupPeerAddressLen = *(INT4 *)pau1FsMsdpMeshGroupPeerAddressLen;\
  }\
 }
/* Macro used to fill the index values in  structure for FsMsdpRPEntry 
 using the input given in def file */

#define  MSDP_FILL_FSMSDPRPTABLE_INDEX(pMsdpFsMsdpRPEntry,\
   pau1FsMsdpRPAddrType)\
  {\
  if (pau1FsMsdpRPAddrType != NULL)\
  {\
   pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPAddrType = *(INT4 *) (pau1FsMsdpRPAddrType);\
  }\
 }
/* Macro used to fill the index values in  structure for FsMsdpPeerFilterEntry 
 using the input given in def file */

#define  MSDP_FILL_FSMSDPPEERFILTERTABLE_INDEX(pMsdpFsMsdpPeerFilterEntry,\
   pau1FsMsdpPeerFilterAddrType)\
  {\
  if (pau1FsMsdpPeerFilterAddrType != NULL)\
  {\
   pMsdpFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterAddrType = *(INT4 *) (pau1FsMsdpPeerFilterAddrType);\
  }\
 }
/* Macro used to fill the index values in  structure for FsMsdpSARedistributionEntry 
 using the input given in def file */

#define  MSDP_FILL_FSMSDPSAREDISTRIBUTIONTABLE_INDEX(pMsdpFsMsdpSARedistributionEntry,\
   pau1FsMsdpSARedistributionAddrType)\
  {\
  if (pau1FsMsdpSARedistributionAddrType != NULL)\
  {\
   pMsdpFsMsdpSARedistributionEntry->MibObject.i4FsMsdpSARedistributionAddrType = *(INT4 *) (pau1FsMsdpSARedistributionAddrType);\
  }\
 }
/* Macro used to convert the input 
  to required datatype for FsMsdpTraceLevel*/

#define  MSDP_FILL_FSMSDPTRACELEVEL(i4FsMsdpTraceLevel,\
   pau1FsMsdpTraceLevel)\
  {\
  if (pau1FsMsdpTraceLevel != NULL)\
  {\
   i4FsMsdpTraceLevel = *(INT4 *) (pau1FsMsdpTraceLevel);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsMsdpIPv4AdminStat*/

#define  MSDP_FILL_FSMSDPIPV4ADMINSTAT(i4FsMsdpIPv4AdminStat,\
   pau1FsMsdpIPv4AdminStat)\
  {\
  if (pau1FsMsdpIPv4AdminStat != NULL)\
  {\
   i4FsMsdpIPv4AdminStat = *(INT4 *) (pau1FsMsdpIPv4AdminStat);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsMsdpIPv6AdminStat*/

#define  MSDP_FILL_FSMSDPIPV6ADMINSTAT(i4FsMsdpIPv6AdminStat,\
   pau1FsMsdpIPv6AdminStat)\
  {\
  if (pau1FsMsdpIPv6AdminStat != NULL)\
  {\
   i4FsMsdpIPv6AdminStat = *(INT4 *) (pau1FsMsdpIPv6AdminStat);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsMsdpCacheLifetime*/

#define  MSDP_FILL_FSMSDPCACHELIFETIME(u4FsMsdpCacheLifetime,\
   pau1FsMsdpCacheLifetime)\
  {\
  if (pau1FsMsdpCacheLifetime != NULL)\
  {\
   u4FsMsdpCacheLifetime = *(UINT4 *) (pau1FsMsdpCacheLifetime);\
  }\
  }




/* Macro used to convert the input 
  to required datatype for FsMsdpMaxPeerSessions*/

#define  MSDP_FILL_FSMSDPMAXPEERSESSIONS(i4FsMsdpMaxPeerSessions,\
   pau1FsMsdpMaxPeerSessions)\
  {\
  if (pau1FsMsdpMaxPeerSessions != NULL)\
  {\
   i4FsMsdpMaxPeerSessions = *(INT4 *) (pau1FsMsdpMaxPeerSessions);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsMsdpMappingComponentId*/

#define  MSDP_FILL_FSMSDPMAPPINGCOMPONENTID(i4FsMsdpMappingComponentId,\
   pau1FsMsdpMappingComponentId)\
  {\
  if (pau1FsMsdpMappingComponentId != NULL)\
  {\
   i4FsMsdpMappingComponentId = *(INT4 *) (pau1FsMsdpMappingComponentId);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsMsdpListenerPort*/

#define  MSDP_FILL_FSMSDPLISTENERPORT(i4FsMsdpListenerPort,\
   pau1FsMsdpListenerPort)\
  {\
  if (pau1FsMsdpListenerPort != NULL)\
  {\
   i4FsMsdpListenerPort = *(INT4 *) (pau1FsMsdpListenerPort);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsMsdpPeerFilter*/

#define  MSDP_FILL_FSMSDPPEERFILTER(i4FsMsdpPeerFilter,\
   pau1FsMsdpPeerFilter)\
  {\
  if (pau1FsMsdpPeerFilter != NULL)\
  {\
   i4FsMsdpPeerFilter = *(INT4 *) (pau1FsMsdpPeerFilter);\
  }\
  }






/* Macro used to convert the input 
  to required datatype for FsMsdpRtrId*/

#define  MSDP_FILL_FSMSDPRTRID(u4FsMsdpRtrId,\
   pau1FsMsdpRtrId)\
  {\
  if (pau1FsMsdpRtrId != NULL)\
  {\
   u4FsMsdpRtrId = *(UINT4 *) (pau1FsMsdpRtrId);\
  }\
  }


