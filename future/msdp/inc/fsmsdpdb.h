/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsdpdb.h,v 1.3 2011/07/01 06:08:46 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMSDPDB_H
#define _FSMSDPDB_H

UINT1 FsMsdpPeerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 FsMsdpSACacheTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 FsMsdpMeshGroupTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 FsMsdpRPTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsMsdpPeerFilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsMsdpSARedistributionTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 fsmsdp [] ={1,3,6,1,4,1,29601,2,61};
tSNMP_OID_TYPE fsmsdpOID = {9, fsmsdp};


UINT4 FsMsdpTraceLevel [ ] ={1,3,6,1,4,1,29601,2,61,1,1};
UINT4 FsMsdpIPv4AdminStat [ ] ={1,3,6,1,4,1,29601,2,61,1,2};
UINT4 FsMsdpIPv6AdminStat [ ] ={1,3,6,1,4,1,29601,2,61,1,3};
UINT4 FsMsdpCacheLifetime [ ] ={1,3,6,1,4,1,29601,2,61,1,4};
UINT4 FsMsdpNumSACacheEntries [ ] ={1,3,6,1,4,1,29601,2,61,1,5};
UINT4 FsMsdpMaxPeerSessions [ ] ={1,3,6,1,4,1,29601,2,61,1,6};
UINT4 FsMsdpMappingComponentId [ ] ={1,3,6,1,4,1,29601,2,61,1,7};
UINT4 FsMsdpListenerPort [ ] ={1,3,6,1,4,1,29601,2,61,1,8};
UINT4 FsMsdpPeerFilter [ ] ={1,3,6,1,4,1,29601,2,61,1,9};
UINT4 FsMsdpPeerCount [ ] ={1,3,6,1,4,1,29601,2,61,1,10};
UINT4 FsMsdpStatEstPeerCount [ ] ={1,3,6,1,4,1,29601,2,61,2,1};
UINT4 FsMsdpPeerAddrType [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,1};
UINT4 FsMsdpPeerRemoteAddress [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,2};
UINT4 FsMsdpPeerState [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,3};
UINT4 FsMsdpPeerRPFFailures [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,4};
UINT4 FsMsdpPeerInSAs [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,5};
UINT4 FsMsdpPeerOutSAs [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,6};
UINT4 FsMsdpPeerInSARequests [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,7};
UINT4 FsMsdpPeerOutSARequests [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,8};
UINT4 FsMsdpPeerInControlMessages [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,9};
UINT4 FsMsdpPeerOutControlMessages [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,10};
UINT4 FsMsdpPeerInDataPackets [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,11};
UINT4 FsMsdpPeerOutDataPackets [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,12};
UINT4 FsMsdpPeerFsmEstablishedTransitions [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,13};
UINT4 FsMsdpPeerFsmEstablishedTime [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,14};
UINT4 FsMsdpPeerInMessageTime [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,15};
UINT4 FsMsdpPeerLocalAddress [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,16};
UINT4 FsMsdpPeerConnectRetryInterval [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,17};
UINT4 FsMsdpPeerHoldTimeConfigured [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,18};
UINT4 FsMsdpPeerKeepAliveConfigured [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,19};
UINT4 FsMsdpPeerDataTtl [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,20};
UINT4 FsMsdpPeerStatus [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,21};
UINT4 FsMsdpPeerRemotePort [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,22};
UINT4 FsMsdpPeerLocalPort [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,23};
UINT4 FsMsdpPeerEncapsulationType [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,24};
UINT4 FsMsdpPeerConnectionAttempts [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,25};
UINT4 FsMsdpPeerDiscontinuityTime [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,26};
UINT4 FsMsdpPeerMD5AuthPassword [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,27};
UINT4 FsMsdpPeerMD5AuthPwdStat [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,28};
UINT4 FsMsdpPeerMD5FailCount [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,29};
UINT4 FsMsdpPeerMD5SuccessCount [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,30};
UINT4 FsMsdpPeerInSAResponses [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,31};
UINT4 FsMsdpPeerOutSAResponses [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,32};
UINT4 FsMsdpPeerUpTime [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,33};
UINT4 FsMsdpPeerInKeepAliveCount [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,34};
UINT4 FsMsdpPeerOutKeepAliveCount [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,35};
UINT4 FsMsdpPeerDataTtlErrorCount [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,36};
UINT4 FsMsdpPeerAdminStatus [ ] ={1,3,6,1,4,1,29601,2,61,1,11,1,37};
UINT4 FsMsdpSACacheAddrType [ ] ={1,3,6,1,4,1,29601,2,61,1,12,1,1};
UINT4 FsMsdpSACacheGroupAddr [ ] ={1,3,6,1,4,1,29601,2,61,1,12,1,2};
UINT4 FsMsdpSACacheSourceAddr [ ] ={1,3,6,1,4,1,29601,2,61,1,12,1,3};
UINT4 FsMsdpSACacheOriginRP [ ] ={1,3,6,1,4,1,29601,2,61,1,12,1,4};
UINT4 FsMsdpSACachePeerLearnedFrom [ ] ={1,3,6,1,4,1,29601,2,61,1,12,1,5};
UINT4 FsMsdpSACacheRPFPeer [ ] ={1,3,6,1,4,1,29601,2,61,1,12,1,6};
UINT4 FsMsdpSACacheInSAs [ ] ={1,3,6,1,4,1,29601,2,61,1,12,1,7};
UINT4 FsMsdpSACacheInDataPackets [ ] ={1,3,6,1,4,1,29601,2,61,1,12,1,8};
UINT4 FsMsdpSACacheUpTime [ ] ={1,3,6,1,4,1,29601,2,61,1,12,1,9};
UINT4 FsMsdpSACacheExpiryTime [ ] ={1,3,6,1,4,1,29601,2,61,1,12,1,10};
UINT4 FsMsdpSACacheStatus [ ] ={1,3,6,1,4,1,29601,2,61,1,12,1,11};
UINT4 FsMsdpMeshGroupName [ ] ={1,3,6,1,4,1,29601,2,61,1,13,1,1};
UINT4 FsMsdpMeshGroupAddrType [ ] ={1,3,6,1,4,1,29601,2,61,1,13,1,2};
UINT4 FsMsdpMeshGroupPeerAddress [ ] ={1,3,6,1,4,1,29601,2,61,1,13,1,3};
UINT4 FsMsdpMeshGroupStatus [ ] ={1,3,6,1,4,1,29601,2,61,1,13,1,4};
UINT4 FsMsdpRPAddrType [ ] ={1,3,6,1,4,1,29601,2,61,1,14,1,1};
UINT4 FsMsdpRPAddress [ ] ={1,3,6,1,4,1,29601,2,61,1,14,1,2};
UINT4 FsMsdpRPOperStatus [ ] ={1,3,6,1,4,1,29601,2,61,1,14,1,3};
UINT4 FsMsdpRPStatus [ ] ={1,3,6,1,4,1,29601,2,61,1,14,1,4};
UINT4 FsMsdpPeerFilterAddrType [ ] ={1,3,6,1,4,1,29601,2,61,1,15,1,1};
UINT4 FsMsdpPeerFilterRouteMap [ ] ={1,3,6,1,4,1,29601,2,61,1,15,1,2};
UINT4 FsMsdpPeerFilterStatus [ ] ={1,3,6,1,4,1,29601,2,61,1,15,1,3};
UINT4 FsMsdpSARedistributionAddrType [ ] ={1,3,6,1,4,1,29601,2,61,1,16,1,1};
UINT4 FsMsdpSARedistributionStatus [ ] ={1,3,6,1,4,1,29601,2,61,1,16,1,2};
UINT4 FsMsdpSARedistributionRouteMap [ ] ={1,3,6,1,4,1,29601,2,61,1,16,1,3};
UINT4 FsMsdpSARedistributionRouteMapStat [ ] ={1,3,6,1,4,1,29601,2,61,1,16,1,4};
UINT4 FsMsdpRtrId [ ] ={1,3,6,1,4,1,29601,2,61,1,17};




tMbDbEntry fsmsdpMibEntry[]= {

{{11,FsMsdpTraceLevel}, NULL, FsMsdpTraceLevelGet, FsMsdpTraceLevelSet, FsMsdpTraceLevelTest, FsMsdpTraceLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsMsdpIPv4AdminStat}, NULL, FsMsdpIPv4AdminStatGet, FsMsdpIPv4AdminStatSet, FsMsdpIPv4AdminStatTest, FsMsdpIPv4AdminStatDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsMsdpIPv6AdminStat}, NULL, FsMsdpIPv6AdminStatGet, FsMsdpIPv6AdminStatSet, FsMsdpIPv6AdminStatTest, FsMsdpIPv6AdminStatDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsMsdpCacheLifetime}, NULL, FsMsdpCacheLifetimeGet, FsMsdpCacheLifetimeSet, FsMsdpCacheLifetimeTest, FsMsdpCacheLifetimeDep, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsMsdpNumSACacheEntries}, NULL, FsMsdpNumSACacheEntriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsMsdpMaxPeerSessions}, NULL, FsMsdpMaxPeerSessionsGet, FsMsdpMaxPeerSessionsSet, FsMsdpMaxPeerSessionsTest, FsMsdpMaxPeerSessionsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "32"},

{{11,FsMsdpMappingComponentId}, NULL, FsMsdpMappingComponentIdGet, FsMsdpMappingComponentIdSet, FsMsdpMappingComponentIdTest, FsMsdpMappingComponentIdDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsMsdpListenerPort}, NULL, FsMsdpListenerPortGet, FsMsdpListenerPortSet, FsMsdpListenerPortTest, FsMsdpListenerPortDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "639"},

{{11,FsMsdpPeerFilter}, NULL, FsMsdpPeerFilterGet, FsMsdpPeerFilterSet, FsMsdpPeerFilterTest, FsMsdpPeerFilterDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsMsdpPeerCount}, NULL, FsMsdpPeerCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,FsMsdpPeerAddrType}, GetNextIndexFsMsdpPeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerRemoteAddress}, GetNextIndexFsMsdpPeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerState}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerRPFFailures}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerRPFFailuresGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerInSAs}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerInSAsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerOutSAs}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerOutSAsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerInSARequests}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerInSARequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerOutSARequests}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerOutSARequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerInControlMessages}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerInControlMessagesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerOutControlMessages}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerOutControlMessagesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerInDataPackets}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerInDataPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerOutDataPackets}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerOutDataPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerFsmEstablishedTransitions}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerFsmEstablishedTransitionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerFsmEstablishedTime}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerFsmEstablishedTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerInMessageTime}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerInMessageTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerLocalAddress}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerLocalAddressGet, FsMsdpPeerLocalAddressSet, FsMsdpPeerLocalAddressTest, FsMsdpPeerTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerConnectRetryInterval}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerConnectRetryIntervalGet, FsMsdpPeerConnectRetryIntervalSet, FsMsdpPeerConnectRetryIntervalTest, FsMsdpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMsdpPeerTableINDEX, 2, 0, 0, "30"},

{{13,FsMsdpPeerHoldTimeConfigured}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerHoldTimeConfiguredGet, FsMsdpPeerHoldTimeConfiguredSet, FsMsdpPeerHoldTimeConfiguredTest, FsMsdpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMsdpPeerTableINDEX, 2, 0, 0, "75"},

{{13,FsMsdpPeerKeepAliveConfigured}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerKeepAliveConfiguredGet, FsMsdpPeerKeepAliveConfiguredSet, FsMsdpPeerKeepAliveConfiguredTest, FsMsdpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMsdpPeerTableINDEX, 2, 0, 0, "60"},

{{13,FsMsdpPeerDataTtl}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerDataTtlGet, FsMsdpPeerDataTtlSet, FsMsdpPeerDataTtlTest, FsMsdpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMsdpPeerTableINDEX, 2, 0, 0, "1"},

{{13,FsMsdpPeerStatus}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerStatusGet, FsMsdpPeerStatusSet, FsMsdpPeerStatusTest, FsMsdpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMsdpPeerTableINDEX, 2, 0, 1, NULL},

{{13,FsMsdpPeerRemotePort}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerRemotePortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, "639"},

{{13,FsMsdpPeerLocalPort}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerLocalPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, "639"},

{{13,FsMsdpPeerEncapsulationType}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerEncapsulationTypeGet, FsMsdpPeerEncapsulationTypeSet, FsMsdpPeerEncapsulationTypeTest, FsMsdpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerConnectionAttempts}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerConnectionAttemptsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerDiscontinuityTime}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerDiscontinuityTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerMD5AuthPassword}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerMD5AuthPasswordGet, FsMsdpPeerMD5AuthPasswordSet, FsMsdpPeerMD5AuthPasswordTest, FsMsdpPeerTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerMD5AuthPwdStat}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerMD5AuthPwdStatGet, FsMsdpPeerMD5AuthPwdStatSet, FsMsdpPeerMD5AuthPwdStatTest, FsMsdpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMsdpPeerTableINDEX, 2, 0, 0, "0"},

{{13,FsMsdpPeerMD5FailCount}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerMD5FailCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerMD5SuccessCount}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerMD5SuccessCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerInSAResponses}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerInSAResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerOutSAResponses}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerOutSAResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerUpTime}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerInKeepAliveCount}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerInKeepAliveCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerOutKeepAliveCount}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerOutKeepAliveCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerDataTtlErrorCount}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerDataTtlErrorCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpPeerAdminStatus}, GetNextIndexFsMsdpPeerTable, FsMsdpPeerAdminStatusGet, FsMsdpPeerAdminStatusSet, FsMsdpPeerAdminStatusTest, FsMsdpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMsdpPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMsdpSACacheAddrType}, GetNextIndexFsMsdpSACacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMsdpSACacheTableINDEX, 4, 0, 0, NULL},

{{13,FsMsdpSACacheGroupAddr}, GetNextIndexFsMsdpSACacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMsdpSACacheTableINDEX, 4, 0, 0, NULL},

{{13,FsMsdpSACacheSourceAddr}, GetNextIndexFsMsdpSACacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMsdpSACacheTableINDEX, 4, 0, 0, NULL},

{{13,FsMsdpSACacheOriginRP}, GetNextIndexFsMsdpSACacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMsdpSACacheTableINDEX, 4, 0, 0, NULL},

{{13,FsMsdpSACachePeerLearnedFrom}, GetNextIndexFsMsdpSACacheTable, FsMsdpSACachePeerLearnedFromGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMsdpSACacheTableINDEX, 4, 0, 0, NULL},

{{13,FsMsdpSACacheRPFPeer}, GetNextIndexFsMsdpSACacheTable, FsMsdpSACacheRPFPeerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMsdpSACacheTableINDEX, 4, 0, 0, NULL},

{{13,FsMsdpSACacheInSAs}, GetNextIndexFsMsdpSACacheTable, FsMsdpSACacheInSAsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMsdpSACacheTableINDEX, 4, 0, 0, NULL},

{{13,FsMsdpSACacheInDataPackets}, GetNextIndexFsMsdpSACacheTable, FsMsdpSACacheInDataPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMsdpSACacheTableINDEX, 4, 0, 0, NULL},

{{13,FsMsdpSACacheUpTime}, GetNextIndexFsMsdpSACacheTable, FsMsdpSACacheUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMsdpSACacheTableINDEX, 4, 0, 0, NULL},

{{13,FsMsdpSACacheExpiryTime}, GetNextIndexFsMsdpSACacheTable, FsMsdpSACacheExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMsdpSACacheTableINDEX, 4, 0, 0, NULL},

{{13,FsMsdpSACacheStatus}, GetNextIndexFsMsdpSACacheTable, FsMsdpSACacheStatusGet, FsMsdpSACacheStatusSet, FsMsdpSACacheStatusTest, FsMsdpSACacheTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMsdpSACacheTableINDEX, 4, 0, 1, NULL},

{{13,FsMsdpMeshGroupName}, GetNextIndexFsMsdpMeshGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMsdpMeshGroupTableINDEX, 3, 0, 0, NULL},

{{13,FsMsdpMeshGroupAddrType}, GetNextIndexFsMsdpMeshGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMsdpMeshGroupTableINDEX, 3, 0, 0, NULL},

{{13,FsMsdpMeshGroupPeerAddress}, GetNextIndexFsMsdpMeshGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMsdpMeshGroupTableINDEX, 3, 0, 0, NULL},

{{13,FsMsdpMeshGroupStatus}, GetNextIndexFsMsdpMeshGroupTable, FsMsdpMeshGroupStatusGet, FsMsdpMeshGroupStatusSet, FsMsdpMeshGroupStatusTest, FsMsdpMeshGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMsdpMeshGroupTableINDEX, 3, 0, 1, NULL},

{{13,FsMsdpRPAddrType}, GetNextIndexFsMsdpRPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMsdpRPTableINDEX, 1, 0, 0, NULL},

{{13,FsMsdpRPAddress}, GetNextIndexFsMsdpRPTable, FsMsdpRPAddressGet, FsMsdpRPAddressSet, FsMsdpRPAddressTest, FsMsdpRPTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMsdpRPTableINDEX, 1, 0, 0, NULL},

{{13,FsMsdpRPOperStatus}, GetNextIndexFsMsdpRPTable, FsMsdpRPOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMsdpRPTableINDEX, 1, 0, 0, NULL},

{{13,FsMsdpRPStatus}, GetNextIndexFsMsdpRPTable, FsMsdpRPStatusGet, FsMsdpRPStatusSet, FsMsdpRPStatusTest, FsMsdpRPTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMsdpRPTableINDEX, 1, 0, 1, NULL},

{{13,FsMsdpPeerFilterAddrType}, GetNextIndexFsMsdpPeerFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMsdpPeerFilterTableINDEX, 1, 0, 0, NULL},

{{13,FsMsdpPeerFilterRouteMap}, GetNextIndexFsMsdpPeerFilterTable, FsMsdpPeerFilterRouteMapGet, FsMsdpPeerFilterRouteMapSet, FsMsdpPeerFilterRouteMapTest, FsMsdpPeerFilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMsdpPeerFilterTableINDEX, 1, 0, 0, NULL},

{{13,FsMsdpPeerFilterStatus}, GetNextIndexFsMsdpPeerFilterTable, FsMsdpPeerFilterStatusGet, FsMsdpPeerFilterStatusSet, FsMsdpPeerFilterStatusTest, FsMsdpPeerFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMsdpPeerFilterTableINDEX, 1, 0, 1, NULL},

{{13,FsMsdpSARedistributionAddrType}, GetNextIndexFsMsdpSARedistributionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMsdpSARedistributionTableINDEX, 1, 0, 0, NULL},

{{13,FsMsdpSARedistributionStatus}, GetNextIndexFsMsdpSARedistributionTable, FsMsdpSARedistributionStatusGet, FsMsdpSARedistributionStatusSet, FsMsdpSARedistributionStatusTest, FsMsdpSARedistributionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMsdpSARedistributionTableINDEX, 1, 0, 1, NULL},

{{13,FsMsdpSARedistributionRouteMap}, GetNextIndexFsMsdpSARedistributionTable, FsMsdpSARedistributionRouteMapGet, FsMsdpSARedistributionRouteMapSet, FsMsdpSARedistributionRouteMapTest, FsMsdpSARedistributionTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMsdpSARedistributionTableINDEX, 1, 0, 0, NULL},

{{13,FsMsdpSARedistributionRouteMapStat}, GetNextIndexFsMsdpSARedistributionTable, FsMsdpSARedistributionRouteMapStatGet, FsMsdpSARedistributionRouteMapStatSet, FsMsdpSARedistributionRouteMapStatTest, FsMsdpSARedistributionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMsdpSARedistributionTableINDEX, 1, 0, 0, "0"},

{{11,FsMsdpRtrId}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FsMsdpStatEstPeerCount}, NULL, FsMsdpStatEstPeerCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData fsmsdpEntry = { 75, fsmsdpMibEntry };

#endif /* _FSMSDPDB_H */

