/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: msdpprotg.h,v 1.2 2011/07/01 06:08:53 siva Exp $
*
* This file contains prototypes for functions defined in Msdp.
*********************************************************************/


PUBLIC INT4 MsdpUtlCreateRBTree PROTO ((VOID));

PUBLIC INT4 MsdpLock (VOID);

PUBLIC INT4 MsdpUnLock (VOID);
PUBLIC INT4 FsMsdpPeerTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 MsdpFsMsdpPeerTableCreate PROTO ((VOID));

tMsdpFsMsdpPeerEntry * MsdpFsMsdpPeerTableCreateApi PROTO ((tMsdpFsMsdpPeerEntry *));

PUBLIC INT4 FsMsdpSACacheTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 MsdpFsMsdpSACacheTableCreate PROTO ((VOID));

tMsdpFsMsdpSACacheEntry * MsdpFsMsdpSACacheTableCreateApi PROTO ((tMsdpFsMsdpSACacheEntry *));

PUBLIC INT4 FsMsdpMeshGroupTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 MsdpFsMsdpMeshGroupTableCreate PROTO ((VOID));

tMsdpFsMsdpMeshGroupEntry * MsdpFsMsdpMeshGroupTableCreateApi PROTO ((tMsdpFsMsdpMeshGroupEntry *));

PUBLIC INT4 FsMsdpRPTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 MsdpFsMsdpRPTableCreate PROTO ((VOID));

tMsdpFsMsdpRPEntry * MsdpFsMsdpRPTableCreateApi PROTO ((tMsdpFsMsdpRPEntry *));

PUBLIC INT4 FsMsdpPeerFilterTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 MsdpFsMsdpPeerFilterTableCreate PROTO ((VOID));

tMsdpFsMsdpPeerFilterEntry * MsdpFsMsdpPeerFilterTableCreateApi PROTO ((tMsdpFsMsdpPeerFilterEntry *));

PUBLIC INT4 FsMsdpSARedistributionTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 MsdpFsMsdpSARedistributionTableCreate PROTO ((VOID));

tMsdpFsMsdpSARedistributionEntry * MsdpFsMsdpSARedistributionTableCreateApi PROTO ((tMsdpFsMsdpSARedistributionEntry *));
INT4 MsdpGetFsMsdpTraceLevel PROTO ((INT4 *));
INT4 MsdpGetFsMsdpIPv4AdminStat PROTO ((INT4 *));
INT4 MsdpGetFsMsdpIPv6AdminStat PROTO ((INT4 *));
INT4 MsdpGetFsMsdpCacheLifetime PROTO ((UINT4 *));
INT4 MsdpGetFsMsdpNumSACacheEntries PROTO ((UINT4 *));
INT4 MsdpGetFsMsdpMaxPeerSessions PROTO ((INT4 *));
INT4 MsdpGetFsMsdpMappingComponentId PROTO ((INT4 *));
INT4 MsdpGetFsMsdpListenerPort PROTO ((INT4 *));
INT4 MsdpGetFsMsdpPeerFilter PROTO ((INT4 *));
INT4 MsdpGetFsMsdpPeerCount PROTO ((INT4 *));
INT4 MsdpGetFsMsdpStatEstPeerCount PROTO ((INT4 *));
INT4 MsdpGetAllFsMsdpPeerTable PROTO ((tMsdpFsMsdpPeerEntry *));
INT4 MsdpGetAllUtlFsMsdpPeerTable PROTO((tMsdpFsMsdpPeerEntry *, tMsdpFsMsdpPeerEntry *));
INT4 MsdpGetAllFsMsdpSACacheTable PROTO ((tMsdpFsMsdpSACacheEntry *));
INT4 MsdpGetAllUtlFsMsdpSACacheTable PROTO((tMsdpFsMsdpSACacheEntry *, tMsdpFsMsdpSACacheEntry *));
INT4 MsdpGetAllFsMsdpMeshGroupTable PROTO ((tMsdpFsMsdpMeshGroupEntry *));
INT4 MsdpGetAllUtlFsMsdpMeshGroupTable PROTO((tMsdpFsMsdpMeshGroupEntry *, tMsdpFsMsdpMeshGroupEntry *));
INT4 MsdpGetAllFsMsdpRPTable PROTO ((tMsdpFsMsdpRPEntry *));
INT4 MsdpGetAllUtlFsMsdpRPTable PROTO((tMsdpFsMsdpRPEntry *, tMsdpFsMsdpRPEntry *));
INT4 MsdpGetAllFsMsdpPeerFilterTable PROTO ((tMsdpFsMsdpPeerFilterEntry *));
INT4 MsdpGetAllUtlFsMsdpPeerFilterTable PROTO((tMsdpFsMsdpPeerFilterEntry *, tMsdpFsMsdpPeerFilterEntry *));
INT4 MsdpGetAllFsMsdpSARedistributionTable PROTO ((tMsdpFsMsdpSARedistributionEntry *));
INT4 MsdpGetAllUtlFsMsdpSARedistributionTable PROTO((tMsdpFsMsdpSARedistributionEntry *, tMsdpFsMsdpSARedistributionEntry *));
INT4 MsdpGetFsMsdpRtrId PROTO ((UINT4 *));
INT4 MsdpSetFsMsdpTraceLevel PROTO ((INT4 ));
INT4 MsdpSetFsMsdpIPv4AdminStat PROTO ((INT4 ));
INT4 MsdpSetFsMsdpIPv6AdminStat PROTO ((INT4 ));
INT4 MsdpSetFsMsdpCacheLifetime PROTO ((UINT4 ));
INT4 MsdpSetFsMsdpMaxPeerSessions PROTO ((INT4 ));
INT4 MsdpSetFsMsdpMappingComponentId PROTO ((INT4 ));
INT4 MsdpSetFsMsdpListenerPort PROTO ((INT4 ));
INT4 MsdpSetFsMsdpPeerFilter PROTO ((INT4 ));
INT4 MsdpSetAllFsMsdpPeerTable PROTO ((tMsdpFsMsdpPeerEntry *, tMsdpIsSetFsMsdpPeerEntry *, INT4 , INT4 ));
INT4 MsdpSetAllFsMsdpSACacheTable PROTO ((tMsdpFsMsdpSACacheEntry *, tMsdpIsSetFsMsdpSACacheEntry *, INT4 , INT4 ));
INT4 MsdpSetAllFsMsdpMeshGroupTable PROTO ((tMsdpFsMsdpMeshGroupEntry *, tMsdpIsSetFsMsdpMeshGroupEntry *, INT4 , INT4 ));
INT4 MsdpSetAllFsMsdpRPTable PROTO ((tMsdpFsMsdpRPEntry *, tMsdpIsSetFsMsdpRPEntry *, INT4 , INT4 ));
INT4 MsdpSetAllFsMsdpPeerFilterTable PROTO ((tMsdpFsMsdpPeerFilterEntry *, tMsdpIsSetFsMsdpPeerFilterEntry *, INT4 , INT4 ));
INT4 MsdpSetAllFsMsdpSARedistributionTable PROTO ((tMsdpFsMsdpSARedistributionEntry *, tMsdpIsSetFsMsdpSARedistributionEntry *, INT4 , INT4 ));

INT4 MsdpInitializeFsMsdpPeerTable PROTO ((tMsdpFsMsdpPeerEntry *));

INT4 MsdpInitializeMibFsMsdpPeerTable PROTO ((tMsdpFsMsdpPeerEntry *));

INT4 MsdpInitializeFsMsdpSACacheTable PROTO ((tMsdpFsMsdpSACacheEntry *));

INT4 MsdpInitializeMibFsMsdpSACacheTable PROTO ((tMsdpFsMsdpSACacheEntry *));

INT4 MsdpInitializeFsMsdpMeshGroupTable PROTO ((tMsdpFsMsdpMeshGroupEntry *));

INT4 MsdpInitializeMibFsMsdpMeshGroupTable PROTO ((tMsdpFsMsdpMeshGroupEntry *));

INT4 MsdpInitializeFsMsdpRPTable PROTO ((tMsdpFsMsdpRPEntry *));

INT4 MsdpInitializeMibFsMsdpRPTable PROTO ((tMsdpFsMsdpRPEntry *));

INT4 MsdpInitializeFsMsdpPeerFilterTable PROTO ((tMsdpFsMsdpPeerFilterEntry *));

INT4 MsdpInitializeMibFsMsdpPeerFilterTable PROTO ((tMsdpFsMsdpPeerFilterEntry *));

INT4 MsdpInitializeFsMsdpSARedistributionTable PROTO ((tMsdpFsMsdpSARedistributionEntry *));

INT4 MsdpInitializeMibFsMsdpSARedistributionTable PROTO ((tMsdpFsMsdpSARedistributionEntry *));
INT4 MsdpTestFsMsdpTraceLevel PROTO ((UINT4 *,INT4 ));
INT4 MsdpTestFsMsdpIPv4AdminStat PROTO ((UINT4 *,INT4 ));
INT4 MsdpTestFsMsdpIPv6AdminStat PROTO ((UINT4 *,INT4 ));
INT4 MsdpTestFsMsdpCacheLifetime PROTO ((UINT4 *,UINT4 ));
INT4 MsdpTestFsMsdpMaxPeerSessions PROTO ((UINT4 *,INT4 ));
INT4 MsdpTestFsMsdpMappingComponentId PROTO ((UINT4 *,INT4 ));
INT4 MsdpTestFsMsdpListenerPort PROTO ((UINT4 *,INT4 ));
INT4 MsdpTestFsMsdpPeerFilter PROTO ((UINT4 *,INT4 ));
INT4 MsdpTestAllFsMsdpPeerTable PROTO ((UINT4 *, tMsdpFsMsdpPeerEntry *, tMsdpIsSetFsMsdpPeerEntry *, INT4 , INT4));
INT4 MsdpTestAllFsMsdpSACacheTable PROTO ((UINT4 *, tMsdpFsMsdpSACacheEntry *, tMsdpIsSetFsMsdpSACacheEntry *, INT4 , INT4));
INT4 MsdpTestAllFsMsdpMeshGroupTable PROTO ((UINT4 *, tMsdpFsMsdpMeshGroupEntry *, tMsdpIsSetFsMsdpMeshGroupEntry *, INT4 , INT4));
INT4 MsdpTestAllFsMsdpRPTable PROTO ((UINT4 *, tMsdpFsMsdpRPEntry *, tMsdpIsSetFsMsdpRPEntry *, INT4 , INT4));
INT4 MsdpTestAllFsMsdpPeerFilterTable PROTO ((UINT4 *, tMsdpFsMsdpPeerFilterEntry *, tMsdpIsSetFsMsdpPeerFilterEntry *, INT4 , INT4));
INT4 MsdpTestAllFsMsdpSARedistributionTable PROTO ((UINT4 *, tMsdpFsMsdpSARedistributionEntry *, tMsdpIsSetFsMsdpSARedistributionEntry *, INT4 , INT4));
tMsdpFsMsdpPeerEntry * MsdpGetFsMsdpPeerTable PROTO ((tMsdpFsMsdpPeerEntry *));
tMsdpFsMsdpSACacheEntry * MsdpGetFsMsdpSACacheTable PROTO ((tMsdpFsMsdpSACacheEntry *));
tMsdpFsMsdpMeshGroupEntry * MsdpGetFsMsdpMeshGroupTable PROTO ((tMsdpFsMsdpMeshGroupEntry *));
tMsdpFsMsdpRPEntry * MsdpGetFsMsdpRPTable PROTO ((tMsdpFsMsdpRPEntry *));
tMsdpFsMsdpPeerFilterEntry * MsdpGetFsMsdpPeerFilterTable PROTO ((tMsdpFsMsdpPeerFilterEntry *));
tMsdpFsMsdpSARedistributionEntry * MsdpGetFsMsdpSARedistributionTable PROTO ((tMsdpFsMsdpSARedistributionEntry *));
tMsdpFsMsdpPeerEntry * MsdpGetFirstFsMsdpPeerTable PROTO ((VOID));
tMsdpFsMsdpSACacheEntry * MsdpGetFirstFsMsdpSACacheTable PROTO ((VOID));
tMsdpFsMsdpMeshGroupEntry * MsdpGetFirstFsMsdpMeshGroupTable PROTO ((VOID));
tMsdpFsMsdpRPEntry * MsdpGetFirstFsMsdpRPTable PROTO ((VOID));
tMsdpFsMsdpPeerFilterEntry * MsdpGetFirstFsMsdpPeerFilterTable PROTO ((VOID));
tMsdpFsMsdpSARedistributionEntry * MsdpGetFirstFsMsdpSARedistributionTable PROTO ((VOID));
tMsdpFsMsdpPeerEntry * MsdpGetNextFsMsdpPeerTable PROTO ((tMsdpFsMsdpPeerEntry *));
tMsdpFsMsdpSACacheEntry * MsdpGetNextFsMsdpSACacheTable PROTO ((tMsdpFsMsdpSACacheEntry *));
tMsdpFsMsdpMeshGroupEntry * MsdpGetNextFsMsdpMeshGroupTable PROTO ((tMsdpFsMsdpMeshGroupEntry *));
tMsdpFsMsdpRPEntry * MsdpGetNextFsMsdpRPTable PROTO ((tMsdpFsMsdpRPEntry *));
tMsdpFsMsdpPeerFilterEntry * MsdpGetNextFsMsdpPeerFilterTable PROTO ((tMsdpFsMsdpPeerFilterEntry *));
tMsdpFsMsdpSARedistributionEntry * MsdpGetNextFsMsdpSARedistributionTable PROTO ((tMsdpFsMsdpSARedistributionEntry *));
INT4 FsMsdpPeerTableFilterInputs PROTO ((tMsdpFsMsdpPeerEntry *, tMsdpFsMsdpPeerEntry *, tMsdpIsSetFsMsdpPeerEntry *));
INT4 FsMsdpSACacheTableFilterInputs PROTO ((tMsdpFsMsdpSACacheEntry *, tMsdpFsMsdpSACacheEntry *, tMsdpIsSetFsMsdpSACacheEntry *));
INT4 FsMsdpMeshGroupTableFilterInputs PROTO ((tMsdpFsMsdpMeshGroupEntry *, tMsdpFsMsdpMeshGroupEntry *, tMsdpIsSetFsMsdpMeshGroupEntry *));
INT4 FsMsdpRPTableFilterInputs PROTO ((tMsdpFsMsdpRPEntry *, tMsdpFsMsdpRPEntry *, tMsdpIsSetFsMsdpRPEntry *));
INT4 FsMsdpPeerFilterTableFilterInputs PROTO ((tMsdpFsMsdpPeerFilterEntry *, tMsdpFsMsdpPeerFilterEntry *, tMsdpIsSetFsMsdpPeerFilterEntry *));
INT4 FsMsdpSARedistributionTableFilterInputs PROTO ((tMsdpFsMsdpSARedistributionEntry *, tMsdpFsMsdpSARedistributionEntry *, tMsdpIsSetFsMsdpSARedistributionEntry *));
INT4 MsdpUtilUpdateFsMsdpPeerTable PROTO ((tMsdpFsMsdpPeerEntry *, tMsdpFsMsdpPeerEntry *, tMsdpIsSetFsMsdpPeerEntry *));
INT4 MsdpUtilUpdateFsMsdpSACacheTable PROTO ((tMsdpFsMsdpSACacheEntry *, tMsdpFsMsdpSACacheEntry *, tMsdpIsSetFsMsdpSACacheEntry *));
INT4 MsdpUtilUpdateFsMsdpMeshGroupTable PROTO ((tMsdpFsMsdpMeshGroupEntry *, tMsdpFsMsdpMeshGroupEntry *, tMsdpIsSetFsMsdpMeshGroupEntry *));
INT4 MsdpUtilUpdateFsMsdpRPTable PROTO ((tMsdpFsMsdpRPEntry *, tMsdpFsMsdpRPEntry *, tMsdpIsSetFsMsdpRPEntry *));
INT4 MsdpUtilUpdateFsMsdpPeerFilterTable PROTO ((tMsdpFsMsdpPeerFilterEntry *, tMsdpFsMsdpPeerFilterEntry *, tMsdpIsSetFsMsdpPeerFilterEntry *));
INT4 MsdpUtilUpdateFsMsdpSARedistributionTable PROTO ((tMsdpFsMsdpSARedistributionEntry *, tMsdpFsMsdpSARedistributionEntry *, tMsdpIsSetFsMsdpSARedistributionEntry *));
INT4 MsdpSetAllFsMsdpPeerTableTrigger PROTO ((tMsdpFsMsdpPeerEntry *, tMsdpIsSetFsMsdpPeerEntry *, INT4));
INT4 MsdpSetAllFsMsdpSACacheTableTrigger PROTO ((tMsdpFsMsdpSACacheEntry *, tMsdpIsSetFsMsdpSACacheEntry *, INT4));
INT4 MsdpSetAllFsMsdpMeshGroupTableTrigger PROTO ((tMsdpFsMsdpMeshGroupEntry *, tMsdpIsSetFsMsdpMeshGroupEntry *, INT4));
INT4 MsdpSetAllFsMsdpRPTableTrigger PROTO ((tMsdpFsMsdpRPEntry *, tMsdpIsSetFsMsdpRPEntry *, INT4));
INT4 MsdpSetAllFsMsdpPeerFilterTableTrigger PROTO ((tMsdpFsMsdpPeerFilterEntry *, tMsdpIsSetFsMsdpPeerFilterEntry *, INT4));
INT4 MsdpSetAllFsMsdpSARedistributionTableTrigger PROTO ((tMsdpFsMsdpSARedistributionEntry *, tMsdpIsSetFsMsdpSARedistributionEntry *, INT4));
