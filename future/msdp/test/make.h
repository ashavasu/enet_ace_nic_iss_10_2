##########################################################################
# Copyright (C) Future Software Limited,2010			         #
#                                                                        #
# $Id: make.h,v 1.1.1.1 2011/02/11 09:19:43 siva Exp $                     #
#								         #
# Description : Contains information fro creating the make file          #
#		for this module      				         #
#								         #
##########################################################################

#include the make.h and make.rule from LR
include ../../LR/make.h
include ../../LR/make.rule

#Compilation switches
COMP_SWITCHES = $(GENERAL_COMPILATION_SWITCHES)\
                $(SYSTEM_COMPILATION_SWITCHES)

#project directories
MSDP_INCL_DIR           = $(BASE_DIR)/msdp/inc

MSDP_TEST_BASE_DIR  = ${BASE_DIR}/msdp/test
MSDP_TEST_SRC_DIR   = ${MSDP_TEST_BASE_DIR}/src
MSDP_TEST_INC_DIR   = ${MSDP_TEST_BASE_DIR}/inc
MSDP_TEST_OBJ_DIR   = ${MSDP_TEST_BASE_DIR}/obj

MSDP_TEST_INCLUDES  = -I$(MSDP_TEST_INC_DIR) \
                     -I$(MSDP_INCL_DIR) \
                     $(COMMON_INCLUDE_DIRS)


##########################################################################
#                    End of file make.h					 #
##########################################################################
