
/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: mststcli.h,v 1.1.1.1 2011/02/11 09:19:43 siva Exp $
 **
 ** Description: MSDP  UT Test cases.
 ** NOTE: This file should not be included in release packaging.
 ** ***********************************************************************/

#include "lr.h"
#include "cli.h"

INT4 cli_process_msdp_test_cmd (tCliHandle CliHandle, UINT4 u4Command,...);

INT4
MsdpUt(INT4 u4GlobalId );


