/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: msdptest.h,v 1.2 2011/02/24 13:14:51 siva Exp $
 **
 ** Description: MSDP  UT Test cases.
 ** NOTE: This file should not be included in release packaging.
 ** ***********************************************************************/

#ifndef _MSDPTEST_H_
#define _MSDPTEST_H_

#include "msdpinc.h"

UINT1 gu1NodeType = 0;


VOID MsdpExecuteUt(UINT4 u4FileNumber, UINT4 u4TestNumber);
VOID MsdpExecuteUtAll (VOID);
VOID MsdpExecuteUtFile (UINT4 u4File);
VOID MsdpExecuteUtInMyOrder (VOID);

typedef struct
{
    UINT1     u1File;
    UINT1     u1Case;
}tTestCase;

tTestCase gTestCase[] = {
    {1,10}, /* msd6port*/
    {2,19}, /* msdcache */
    {3,10}, /*not msdpcli*/
    {4,51}, /*not msdpclig*/
    {5,16}, /* msdpdb*/
    {6,12}, /* msdpdbg*/
    {7,6},  /* msdpdefault*/
    {8,6},  /* msdpdefaultg*/
    {9,36}, /* msdpdsg*/
    {10,16},/* msdppeer */
    {11,7}, /* msdpin */
    {12,6}, /* msdplw*/
    {13,4}, /*not msdplwg*/
    {14,7}, /* msdpmain */
    {15,9}, /* msdpout */
    {16,6}, /* msdppim */
    {17,14},/* msdpport*/
    {18,3}, /* msdpque */
    {19,21},/* msdpsock */
    {20,3}, /* msdpsz*/
    {21,1}, /* msdptask */
    {22,10},/* msdptmr */
    {23,3}, /* msdptrc */ 
    {24,20},/* msdputil */
    {25,12},/* msdputl*/
    {26,10},/*not msdputlg*/
    {27,12} /*not stdmsdlw*/

};
PUBLIC VOID
PimPortHandleSAInfo (tMsdpMrpSaAdvt * pMsdpSaInfo);
/* msdp6port UT cases */
INT4 MsdpUt6Port_1 (VOID);
INT4 MsdpUt6Port_2 (VOID);
INT4 MsdpUt6Port_3 (VOID);
INT4 MsdpUt6Port_4 (VOID);
INT4 MsdpUt6Port_5 (VOID);
INT4 MsdpUt6Port_6 (VOID);
INT4 MsdpUt6Port_7 (VOID);
INT4 MsdpUt6Port_8 (VOID);
INT4 MsdpUt6Port_9 (VOID);
INT4 MsdpUt6Port_10 (VOID);

/* msdpcache UT cases */
INT4 MsdpUtCache_1 (VOID);
INT4 MsdpUtCache_2 (VOID);
INT4 MsdpUtCache_3 (VOID);
INT4 MsdpUtCache_4 (VOID);
INT4 MsdpUtCache_5 (VOID);
INT4 MsdpUtCache_6 (VOID);
INT4 MsdpUtCache_7 (VOID);
INT4 MsdpUtCache_8 (VOID);
INT4 MsdpUtCache_9 (VOID);
INT4 MsdpUtCache_10 (VOID);
INT4 MsdpUtCache_11 (VOID);
INT4 MsdpUtCache_12 (VOID);
INT4 MsdpUtCache_13 (VOID);
INT4 MsdpUtCache_14 (VOID);
INT4 MsdpUtCache_15 (VOID);
INT4 MsdpUtCache_16 (VOID);
INT4 MsdpUtCache_17 (VOID);
INT4 MsdpUtCache_18 (VOID);
INT4 MsdpUtCache_19 (VOID);
INT4 MsdpUtCliCache_1 (VOID);
INT4 MsdpUtCliCache_2 (VOID);
INT4 MsdpUtCliCache_3 (VOID);
INT4 MsdpUtCliCache_4 (VOID);

/* msdpdb UT cases */
INT4 MsdpUtDb_1 (VOID);
INT4 MsdpUtDb_2 (VOID);
INT4 MsdpUtDb_3 (VOID);
INT4 MsdpUtDb_4 (VOID);
INT4 MsdpUtDb_5 (VOID);
INT4 MsdpUtDb_6 (VOID);
INT4 MsdpUtDb_7 (VOID);
INT4 MsdpUtDb_8 (VOID);
INT4 MsdpUtDb_9 (VOID);
INT4 MsdpUtDb_10 (VOID);
INT4 MsdpUtDb_11 (VOID);
INT4 MsdpUtDb_12 (VOID);
INT4 MsdpUtDb_13 (VOID);
INT4 MsdpUtDb_14 (VOID);
INT4 MsdpUtDb_15 (VOID);
INT4 MsdpUtDb_16 (VOID);

/* msdpdbg UT cases */
INT4 MsdpUtDbg_1 (VOID);
INT4 MsdpUtDbg_2 (VOID);
INT4 MsdpUtDbg_3 (VOID);
INT4 MsdpUtDbg_4 (VOID);
INT4 MsdpUtDbg_5 (VOID);
INT4 MsdpUtDbg_6 (VOID);
INT4 MsdpUtDbg_7 (VOID);
INT4 MsdpUtDbg_8 (VOID);
INT4 MsdpUtDbg_9 (VOID);
INT4 MsdpUtDbg_10 (VOID);
INT4 MsdpUtDbg_11 (VOID);
INT4 MsdpUtDbg_12 (VOID);

/* msdpdefault UT cases */
INT4 MsdpUtDefault_1 (VOID);
INT4 MsdpUtDefault_2 (VOID);
INT4 MsdpUtDefault_3 (VOID);
INT4 MsdpUtDefault_4 (VOID);
INT4 MsdpUtDefault_5 (VOID);
INT4 MsdpUtDefault_6 (VOID);


/* msdpdefaultg UT cases */
INT4 MsdpUtDefaultg_1 (VOID);
INT4 MsdpUtDefaultg_2 (VOID);
INT4 MsdpUtDefaultg_3 (VOID);
INT4 MsdpUtDefaultg_4 (VOID);
INT4 MsdpUtDefaultg_5 (VOID);
INT4 MsdpUtDefaultg_6 (VOID);

/* msdpdsg UT cases */
INT4 MsdpUtDsg_1 (VOID);
INT4 MsdpUtDsg_2 (VOID);
INT4 MsdpUtDsg_3 (VOID);
INT4 MsdpUtDsg_4 (VOID);
INT4 MsdpUtDsg_5 (VOID);
INT4 MsdpUtDsg_6 (VOID);
INT4 MsdpUtDsg_7 (VOID);
INT4 MsdpUtDsg_8 (VOID);
INT4 MsdpUtDsg_9 (VOID);
INT4 MsdpUtDsg_10 (VOID);
INT4 MsdpUtDsg_11 (VOID);
INT4 MsdpUtDsg_12 (VOID);
INT4 MsdpUtDsg_13 (VOID);
INT4 MsdpUtDsg_14 (VOID);
INT4 MsdpUtDsg_15 (VOID);
INT4 MsdpUtDsg_16 (VOID);
INT4 MsdpUtDsg_17 (VOID);
INT4 MsdpUtDsg_18 (VOID);
INT4 MsdpUtDsg_19 (VOID);
INT4 MsdpUtDsg_20 (VOID);
INT4 MsdpUtDsg_21 (VOID);
INT4 MsdpUtDsg_22 (VOID);
INT4 MsdpUtDsg_23 (VOID);
INT4 MsdpUtDsg_24 (VOID);
INT4 MsdpUtDsg_25 (VOID);
INT4 MsdpUtDsg_26 (VOID);
INT4 MsdpUtDsg_27 (VOID);
INT4 MsdpUtDsg_28 (VOID);
INT4 MsdpUtDsg_29 (VOID);
INT4 MsdpUtDsg_30 (VOID);
INT4 MsdpUtDsg_31 (VOID);
INT4 MsdpUtDsg_32 (VOID);
INT4 MsdpUtDsg_33 (VOID);
INT4 MsdpUtDsg_34 (VOID);
INT4 MsdpUtDsg_35 (VOID);
INT4 MsdpUtDsg_36 (VOID);


/* msdppeer UT cases */
INT4 MsdpUtPeer_1 (VOID);
INT4 MsdpUtPeer_2 (VOID);
INT4 MsdpUtPeer_3 (VOID);
INT4 MsdpUtPeer_4 (VOID);
INT4 MsdpUtPeer_5 (VOID);
INT4 MsdpUtPeer_6 (VOID);
INT4 MsdpUtPeer_7 (VOID);
INT4 MsdpUtPeer_8 (VOID);
INT4 MsdpUtPeer_9 (VOID);
INT4 MsdpUtPeer_10 (VOID);
INT4 MsdpUtPeer_11 (VOID);
INT4 MsdpUtPeer_12 (VOID);
INT4 MsdpUtPeer_13 (VOID);
INT4 MsdpUtPeer_14 (VOID);
INT4 MsdpUtPeer_15 (VOID);
INT4 MsdpUtPeer_16 (VOID);

/* msdpin UT cases */
INT4 MsdpUtIn_1 (VOID);
INT4 MsdpUtIn_2 (VOID);
INT4 MsdpUtIn_3 (VOID);
INT4 MsdpUtIn_4 (VOID);
INT4 MsdpUtIn_5 (VOID);
INT4 MsdpUtIn_6 (VOID);
INT4 MsdpUtIn_7 (VOID);

/* msdplw UT cases */
INT4 MsdpUtLw_1 (VOID);
INT4 MsdpUtLw_2 (VOID);
INT4 MsdpUtLw_3 (VOID);
INT4 MsdpUtLw_4 (VOID);
INT4 MsdpUtLw_5 (VOID);
INT4 MsdpUtLw_6 (VOID);

/* msdpmain UT cases */
INT4 MsdpUtMain_1 (VOID);
INT4 MsdpUtMain_2 (VOID);
INT4 MsdpUtMain_3 (VOID);
INT4 MsdpUtMain_4 (VOID);
INT4 MsdpUtMain_5 (VOID);
INT4 MsdpUtMain_6 (VOID);
INT4 MsdpUtMain_7 (VOID);

/* msdpout UT cases */
INT4 MsdpUtOut_1 (VOID);
INT4 MsdpUtOut_2 (VOID);
INT4 MsdpUtOut_3 (VOID);
INT4 MsdpUtOut_4 (VOID);
INT4 MsdpUtOut_5 (VOID);
INT4 MsdpUtOut_6 (VOID);
INT4 MsdpUtOut_7 (VOID);
INT4 MsdpUtOut_8 (VOID);
INT4 MsdpUtOut_9 (VOID);

/* msdppim UT cases */
INT4 MsdpUtPim_1 (VOID);
INT4 MsdpUtPim_2 (VOID);
INT4 MsdpUtPim_3 (VOID);
INT4 MsdpUtPim_4 (VOID);
INT4 MsdpUtPim_5 (VOID);
INT4 MsdpUtPim_6 (VOID);

/* msdpport UT cases */
INT4 MsdpUtPort_1 (VOID);
INT4 MsdpUtPort_2 (VOID);
INT4 MsdpUtPort_3 (VOID);
INT4 MsdpUtPort_4 (VOID);
INT4 MsdpUtPort_5 (VOID);
INT4 MsdpUtPort_6 (VOID);
INT4 MsdpUtPort_7 (VOID);
INT4 MsdpUtPort_8 (VOID);
INT4 MsdpUtPort_9 (VOID);
INT4 MsdpUtPort_10 (VOID);
INT4 MsdpUtPort_11 (VOID);
INT4 MsdpUtPort_12 (VOID);
INT4 MsdpUtPort_13 (VOID);
INT4 MsdpUtPort_14 (VOID);

/* msdpque UT cases */
INT4 MsdpUtQue_1 (VOID);
INT4 MsdpUtQue_2 (VOID);
INT4 MsdpUtQue_3 (VOID);

/* msdpsock UT cases */
INT4 MsdpUtSock_1 (VOID);
INT4 MsdpUtSock_2 (VOID);
INT4 MsdpUtSock_3 (VOID);
INT4 MsdpUtSock_4 (VOID);
INT4 MsdpUtSock_5 (VOID);
INT4 MsdpUtSock_6 (VOID);
INT4 MsdpUtSock_7 (VOID);
INT4 MsdpUtSock_8 (VOID);
INT4 MsdpUtSock_9 (VOID);
INT4 MsdpUtSock_10 (VOID);
INT4 MsdpUtSock_11 (VOID);
INT4 MsdpUtSock_12 (VOID);
INT4 MsdpUtSock_13 (VOID);
INT4 MsdpUtSock_14 (VOID);
INT4 MsdpUtSock_15 (VOID);
INT4 MsdpUtSock_16 (VOID);
INT4 MsdpUtSock_17 (VOID);
INT4 MsdpUtSock_18 (VOID);
INT4 MsdpUtSock_19 (VOID);
INT4 MsdpUtSock_20 (VOID);
INT4 MsdpUtSock_21 (VOID);

/* msdpsz UT cases */
INT4 MsdpUtSz_1 (VOID);
INT4 MsdpUtSz_2 (VOID);
INT4 MsdpUtSz_3 (VOID);

/* msdptask UT cases */
INT4 MsdpUtTask_1 (VOID);


/* msdptmr UT cases */
INT4 MsdpUtTmr_1 (VOID);
INT4 MsdpUtTmr_2 (VOID);
INT4 MsdpUtTmr_3 (VOID);
INT4 MsdpUtTmr_4 (VOID);
INT4 MsdpUtTmr_5 (VOID);
INT4 MsdpUtTmr_6 (VOID);
INT4 MsdpUtTmr_7 (VOID);
INT4 MsdpUtTmr_8 (VOID);
INT4 MsdpUtTmr_9 (VOID);
INT4 MsdpUtTmr_10 (VOID);

/* msdptrc UT cases */
INT4 MsdpUtTrc_1 (VOID);
INT4 MsdpUtTrc_2 (VOID);
INT4 MsdpUtTrc_3 (VOID);

/* msdputil UT cases */
INT4 MsdpUtUtil_1 (VOID);
INT4 MsdpUtUtil_2 (VOID);
INT4 MsdpUtUtil_3 (VOID);
INT4 MsdpUtUtil_4 (VOID);
INT4 MsdpUtUtil_5 (VOID);
INT4 MsdpUtUtil_6 (VOID);
INT4 MsdpUtUtil_7 (VOID);
INT4 MsdpUtUtil_8 (VOID);
INT4 MsdpUtUtil_9 (VOID);
INT4 MsdpUtUtil_10 (VOID);
INT4 MsdpUtUtil_11 (VOID);
INT4 MsdpUtUtil_12 (VOID);
INT4 MsdpUtUtil_13 (VOID);
INT4 MsdpUtUtil_14 (VOID);
INT4 MsdpUtUtil_15 (VOID);
INT4 MsdpUtUtil_16 (VOID);
INT4 MsdpUtUtil_17 (VOID);
INT4 MsdpUtUtil_18 (VOID);

/* msdputl UT cases */
INT4 MsdpUtUtl_1 (VOID);
INT4 MsdpUtUtl_2 (VOID);
INT4 MsdpUtUtl_3 (VOID);
INT4 MsdpUtUtl_4 (VOID);
INT4 MsdpUtUtl_5 (VOID);
INT4 MsdpUtUtl_6 (VOID);
INT4 MsdpUtUtl_7 (VOID);
INT4 MsdpUtUtl_8 (VOID);
INT4 MsdpUtUtl_9 (VOID);
INT4 MsdpUtUtl_10 (VOID);
INT4 MsdpUtUtl_11 (VOID);
INT4 MsdpUtUtl_12 (VOID);

/* Cli related */
VOID CliVlanInit (VOID);
VOID CliVlanDeInit (VOID);
#endif /* _MSDPTEST_H_ */ 
