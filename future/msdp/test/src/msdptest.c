/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: msdptest.c,v 1.2 2011/03/01 13:55:08 siva Exp $
 **
 ** Description: MSDP  UT Test cases file.
 ** NOTE: This file should not be include in release packaging.
 ** ***********************************************************************/

#include "msdptest.h"
#include "iss.h"
#include "params.h"

UINT4               gu4MsdpUtTestNumber = 0;
UINT4               gu4MsdpUtStubsReqd = OSIX_FALSE;

PRIVATE INT4
MsdpUtDelEntry ()
{
    tMsdpFsMsdpSACacheEntry SaCache;
    tMsdpFsMsdpSACacheEntry *pCache = NULL;
    tMsdpFsMsdpPeerEntry Peer;
    tMsdpFsMsdpPeerEntry *pPeer = NULL;
    tMsdpFsMsdpRPEntry  Rp;
    tMsdpFsMsdpRPEntry *pRp = NULL;
    tMsdpFsMsdpMeshGroupEntry Mesh;
    tMsdpFsMsdpMeshGroupEntry *pMesh = NULL;
    tMsdpFsMsdpPeerFilterEntry PeerFilt;
    tMsdpFsMsdpPeerFilterEntry *pPeerFilt = NULL;
    tMsdpFsMsdpSARedistributionEntry SARedist;
    tMsdpFsMsdpSARedistributionEntry *pSARedist = NULL;

    MEMSET (&SaCache, NULL, sizeof (tMsdpFsMsdpSACacheEntry));

    while ((NULL != (pCache = MsdpGetNextFsMsdpSACacheTable (&SaCache))))
    {
        MsdpTmrStopTmr (&pCache->CacheSaStateTimer);
        if (OSIX_FAILURE == MsdpUtilRemoveCacheEntry (pCache))
        {
            return OSIX_FAILURE;
        }
        printf ("\r\n Cache Removed\r\n");
        MEMCPY (&SaCache, pCache, sizeof (tMsdpFsMsdpSACacheEntry));
    }

    MEMSET (&Peer, NULL, sizeof (tMsdpFsMsdpPeerEntry));
    while ((NULL != (pPeer = MsdpGetNextFsMsdpPeerTable (&Peer))))
    {
        MsdpTmrStopTmr (&pPeer->MsdpPeerKATimer);
        MsdpTmrStopTmr (&pPeer->MsdpPeerHoldTimer);
        MsdpTmrStopTmr (&pPeer->MsdpPeerConnRetryTimer);
        if (RB_FAILURE == RBTreeRemove
            (gMsdpGlobals.MsdpGlbMib.FsMsdpPeerTable, (tRBElem *) pPeer))
        {
            return OSIX_FAILURE;
        }
        printf ("\r\n Peer Removed\r\n");
        MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID, (UINT1 *) pPeer);
        MEMCPY (&Peer, pPeer, sizeof (tMsdpFsMsdpPeerEntry));
    }

    MEMSET (&Rp, NULL, sizeof (tMsdpFsMsdpRPEntry));
    while ((NULL != (pRp = MsdpGetNextFsMsdpRPTable (&Rp))))
    {
        if (RB_FAILURE == RBTreeRemove (gMsdpGlobals.MsdpGlbMib.FsMsdpRPTable,
                                        (tRBElem *) pRp))
        {
            return OSIX_FAILURE;
        }
        printf ("\r\n RP Removed\r\n");
        MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID, (UINT1 *) pRp);
        MEMCPY (&Rp, pRp, sizeof (tMsdpFsMsdpRPEntry));
    }

    MEMSET (&Mesh, NULL, sizeof (tMsdpFsMsdpMeshGroupEntry));
    while ((NULL != (pMesh = MsdpGetNextFsMsdpMeshGroupTable (&Mesh))))
    {
        if (RB_FAILURE ==
            RBTreeRemove (gMsdpGlobals.MsdpGlbMib.FsMsdpMeshGroupTable,
                          (tRBElem *) pMesh))
        {
            return OSIX_FAILURE;
        }
        printf ("\r\n Mesh Removed\r\n");
        MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID, (UINT1 *) pMesh);
        MEMCPY (&Mesh, pMesh, sizeof (tMsdpFsMsdpMeshGroupEntry));
    }

    MEMSET (&PeerFilt, NULL, sizeof (tMsdpFsMsdpPeerFilterEntry));
    while ((NULL != (pPeerFilt = MsdpGetNextFsMsdpPeerFilterTable (&PeerFilt))))
    {
        if (RB_FAILURE ==
            RBTreeRemove (gMsdpGlobals.MsdpGlbMib.FsMsdpPeerFilterTable,
                          (tRBElem *) pPeerFilt))
        {
            return OSIX_FAILURE;
        }
        printf ("\r\n Peer Filter Removed\r\n");
        MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                            (UINT1 *) pPeerFilt);

        MEMCPY (&PeerFilt, pPeerFilt, sizeof (tMsdpFsMsdpPeerFilterEntry));
    }

    MEMSET (&SARedist, NULL, sizeof (tMsdpFsMsdpSARedistributionEntry));
    while ((NULL != (pSARedist =
                     MsdpGetNextFsMsdpSARedistributionTable (&SARedist))))
    {
        if (RB_FAILURE == RBTreeRemove
            (gMsdpGlobals.MsdpGlbMib.FsMsdpSARedistributionTable,
             (tRBElem *) pSARedist))
        {
            return OSIX_FAILURE;
        }
        printf ("\r\n SA Redistr Entry Removed\r\n");
        MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                            (UINT1 *) pSARedist);

        MEMCPY (&SARedist, pSARedist,
                sizeof (tMsdpFsMsdpSARedistributionEntry));
    }

    return OSIX_SUCCESS;
}

VOID
MsdpExecuteUtInMyOrder (VOID)
{
    MsdpExecuteUt (10, 1);
    MsdpExecuteUt (10, 5);
    MsdpExecuteUt (10, 7);
    MsdpExecuteUt (17, 1);
    MsdpExecuteUt (24, 3);
    MsdpExecuteUt (24, 4);
    MsdpExecuteUt (24, 7);
    MsdpExecuteUt (24, 8);
    MsdpExecuteUt (24, 10);
    MsdpExecuteUt (24, 12);
    MsdpExecuteUt (24, 13);
    MsdpExecuteUt (24, 6);
    MsdpExecuteUt (2, 3);
    MsdpExecuteUt (10, 2);
    MsdpExecuteUt (1, 3);
    MsdpExecuteUt (1, 4);
    MsdpExecuteUt (1, 6);
    MsdpExecuteUt (1, 7);
    MsdpExecuteUt (1, 8);
    MsdpExecuteUt (1, 2);
    MsdpExecuteUt (10, 10);
    MsdpExecuteUt (17, 4);
    MsdpExecuteUt (24, 9);
    MsdpExecuteUt (1, 1);
    MsdpExecuteUt (17, 10);
    MsdpExecuteUt (17, 11);
}

VOID
MsdpExecuteUt (UINT4 u4FileNumber, UINT4 u4TestNumber)
{
    INT4                i4RetVal = -1;

    switch (u4FileNumber)
    {
            /* msd6port */
        case 1:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = MsdpUt6Port_1 ();
                    break;
                case 2:
                    i4RetVal = MsdpUt6Port_2 ();
                    break;
                case 3:
                    i4RetVal = MsdpUt6Port_3 ();
                    break;
                case 4:
                    i4RetVal = MsdpUt6Port_4 ();
                    break;
                case 5:
                    i4RetVal = MsdpUt6Port_5 ();
                    break;
                case 6:
                    i4RetVal = MsdpUt6Port_6 ();
                    break;
                case 7:
                    i4RetVal = MsdpUt6Port_7 ();
                    break;
                case 8:
                    i4RetVal = MsdpUt6Port_8 ();
                    break;
                case 9:
                    i4RetVal = MsdpUt6Port_9 ();
                    break;
                case 10:
                    i4RetVal = MsdpUt6Port_10 ();
                    break;
                default:
                    printf ("Invalid Test for file msdp6port.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of msdp6port.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of msdp6port.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* msdcache */
        case 2:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = MsdpUtCache_1 ();
                    break;
                case 2:
                    i4RetVal = MsdpUtCache_2 ();
                    break;
                case 3:
                    i4RetVal = MsdpUtCache_3 ();
                    break;
                case 4:
                    i4RetVal = MsdpUtCache_4 ();
                    break;
                case 5:
                    i4RetVal = MsdpUtCache_5 ();
                    break;
                case 6:
                    i4RetVal = MsdpUtCache_6 ();
                    break;
                case 7:
                    i4RetVal = MsdpUtCache_7 ();
                    break;
                case 8:
                    i4RetVal = MsdpUtCache_8 ();
                    break;
                case 9:
                    i4RetVal = MsdpUtCache_9 ();
                    break;
                case 10:
                    i4RetVal = MsdpUtCache_10 ();
                    break;
                case 11:
                    i4RetVal = MsdpUtCache_11 ();
                    break;
                case 12:
                    i4RetVal = MsdpUtCache_12 ();
                    break;
                case 13:
                    i4RetVal = MsdpUtCache_13 ();
                    break;
                case 14:
                    i4RetVal = MsdpUtCache_14 ();
                    break;
                case 15:
                    i4RetVal = MsdpUtCache_15 ();
                    break;
                case 16:
                    i4RetVal = MsdpUtCache_16 ();
                    break;
                case 17:
                    i4RetVal = MsdpUtCache_17 ();
                    break;
                case 18:
                    i4RetVal = MsdpUtCache_18 ();
                    break;
                case 19:
                    i4RetVal = MsdpUtCache_19 ();
                    break;
                default:
                    printf ("Invalid Test for file msdpcache.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of msdpcache.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of msdpcache.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* msdpcli */
        case 3:
            /* msdpclig */
        case 4:
            return;
            /* msdpdb */
        case 5:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = MsdpUtDb_1 ();
                    break;
                case 2:
                    i4RetVal = MsdpUtDb_2 ();
                    break;
                case 3:
                    i4RetVal = MsdpUtDb_3 ();
                    break;
                case 4:
                    i4RetVal = MsdpUtDb_4 ();
                    break;
                case 5:
                    i4RetVal = MsdpUtDb_5 ();
                    break;
                case 6:
                    i4RetVal = MsdpUtDb_6 ();
                    break;
                case 7:
                    i4RetVal = MsdpUtDb_7 ();
                    break;
                case 8:
                    i4RetVal = MsdpUtDb_8 ();
                    break;
                case 9:
                    i4RetVal = MsdpUtDb_9 ();
                    break;
                case 10:
                    i4RetVal = MsdpUtDb_10 ();
                    break;
                case 11:
                    i4RetVal = MsdpUtDb_11 ();
                    break;
                case 12:
                    i4RetVal = MsdpUtDb_12 ();
                    break;
                case 13:
                    i4RetVal = MsdpUtDb_13 ();
                    break;
                case 14:
                    i4RetVal = MsdpUtDb_14 ();
                    break;
                case 15:
                    i4RetVal = MsdpUtDb_15 ();
                    break;
                case 16:
                    i4RetVal = MsdpUtDb_16 ();
                    break;
                default:
                    printf ("Invalid Test for file msdpdb.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of msdpdb.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of msdpdb.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* msdpdbg */
        case 6:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = MsdpUtDbg_1 ();
                    break;
                case 2:
                    i4RetVal = MsdpUtDbg_2 ();
                    break;
                case 3:
                    i4RetVal = MsdpUtDbg_3 ();
                    break;
                case 4:
                    i4RetVal = MsdpUtDbg_4 ();
                    break;
                case 5:
                    i4RetVal = MsdpUtDbg_5 ();
                    break;
                case 6:
                    i4RetVal = MsdpUtDbg_6 ();
                    break;
                case 7:
                    i4RetVal = MsdpUtDbg_7 ();
                    break;
                case 8:
                    i4RetVal = MsdpUtDbg_8 ();
                    break;
                case 9:
                    i4RetVal = MsdpUtDbg_9 ();
                    break;
                case 10:
                    i4RetVal = MsdpUtDbg_10 ();
                    break;
                case 11:
                    i4RetVal = MsdpUtDbg_11 ();
                    break;
                case 12:
                    i4RetVal = MsdpUtDbg_12 ();
                    break;
                default:
                    printf ("Invalid Test for file msdpdbg.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of msdpdbg.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of msdpdbg.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* msdpdefault */
        case 7:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = MsdpUtDefault_1 ();
                    break;
                case 2:
                    i4RetVal = MsdpUtDefault_2 ();
                    break;
                case 3:
                    i4RetVal = MsdpUtDefault_3 ();
                    break;
                case 4:
                    i4RetVal = MsdpUtDefault_4 ();
                    break;
                case 5:
                    i4RetVal = MsdpUtDefault_5 ();
                    break;
                case 6:
                    i4RetVal = MsdpUtDefault_6 ();
                    break;
                default:
                    printf ("Invalid Test for file msdpdefault.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of msdpdefault.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of msdpdefault.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* msdpdefaultg */
        case 8:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = MsdpUtDefaultg_1 ();
                    break;
                case 2:
                    i4RetVal = MsdpUtDefaultg_2 ();
                    break;
                case 3:
                    i4RetVal = MsdpUtDefaultg_3 ();
                    break;
                case 4:
                    i4RetVal = MsdpUtDefaultg_4 ();
                    break;
                case 5:
                    i4RetVal = MsdpUtDefaultg_5 ();
                    break;
                case 6:
                    i4RetVal = MsdpUtDefaultg_6 ();
                    break;
                default:
                    printf ("Invalid Test for file msdpdefaultg.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of msdpdefaultg.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of msdpdefaultg.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* msdpdsg */
        case 9:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = MsdpUtDsg_1 ();
                    break;
                case 2:
                    i4RetVal = MsdpUtDsg_2 ();
                    break;
                case 3:
                    i4RetVal = MsdpUtDsg_3 ();
                    break;
                case 4:
                    i4RetVal = MsdpUtDsg_4 ();
                    break;
                case 5:
                    i4RetVal = MsdpUtDsg_5 ();
                    break;
                case 6:
                    i4RetVal = MsdpUtDsg_6 ();
                    break;
                case 7:
                    i4RetVal = MsdpUtDsg_7 ();
                    break;
                case 8:
                    i4RetVal = MsdpUtDsg_8 ();
                    break;
                case 9:
                    i4RetVal = MsdpUtDsg_9 ();
                    break;
                case 10:
                    i4RetVal = MsdpUtDsg_10 ();
                    break;
                case 11:
                    i4RetVal = MsdpUtDsg_11 ();
                    break;
                case 12:
                    i4RetVal = MsdpUtDsg_12 ();
                    break;
                case 13:
                    i4RetVal = MsdpUtDsg_13 ();
                    break;
                case 14:
                    i4RetVal = MsdpUtDsg_14 ();
                    break;
                case 15:
                    i4RetVal = MsdpUtDsg_15 ();
                    break;
                case 16:
                    i4RetVal = MsdpUtDsg_16 ();
                    break;
                case 17:
                    i4RetVal = MsdpUtDsg_17 ();
                    break;
                case 18:
                    i4RetVal = MsdpUtDsg_18 ();
                    break;
                case 19:
                    i4RetVal = MsdpUtDsg_19 ();
                    break;
                case 20:
                    i4RetVal = MsdpUtDsg_20 ();
                    break;
                case 21:
                    i4RetVal = MsdpUtDsg_21 ();
                    break;
                case 22:
                    i4RetVal = MsdpUtDsg_22 ();
                    break;
                case 23:
                    i4RetVal = MsdpUtDsg_23 ();
                    break;
                case 24:
                    i4RetVal = MsdpUtDsg_24 ();
                    break;
                case 25:
                    i4RetVal = MsdpUtDsg_25 ();
                    break;
                case 26:
                    i4RetVal = MsdpUtDsg_26 ();
                    break;
                case 27:
                    i4RetVal = MsdpUtDsg_27 ();
                    break;
                case 28:
                    i4RetVal = MsdpUtDsg_28 ();
                    break;
                case 29:
                    i4RetVal = MsdpUtDsg_29 ();
                    break;
                case 30:
                    i4RetVal = MsdpUtDsg_30 ();
                    break;
                case 31:
                    i4RetVal = MsdpUtDsg_31 ();
                    break;
                case 32:
                    i4RetVal = MsdpUtDsg_32 ();
                    break;
                case 33:
                    i4RetVal = MsdpUtDsg_33 ();
                    break;
                case 34:
                    i4RetVal = MsdpUtDsg_34 ();
                    break;
                case 35:
                    i4RetVal = MsdpUtDsg_35 ();
                    break;
                case 36:
                    i4RetVal = MsdpUtDsg_36 ();
                    break;
                default:
                    printf ("Invalid Test for file msdpdsg.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of msdpdsg.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of msdpdsg.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* msdppeer */
        case 10:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = MsdpUtPeer_1 ();
                    break;
                case 2:
                    i4RetVal = MsdpUtPeer_2 ();
                    break;
                case 3:
                    i4RetVal = MsdpUtPeer_3 ();
                    break;
                case 4:
                    i4RetVal = MsdpUtPeer_4 ();
                    break;
                case 5:
                    i4RetVal = MsdpUtPeer_5 ();
                    break;
                case 6:
                    i4RetVal = MsdpUtPeer_6 ();
                    break;
                case 7:
                    i4RetVal = MsdpUtPeer_7 ();
                    break;
                case 8:
                    i4RetVal = MsdpUtPeer_8 ();
                    break;
                case 9:
                    i4RetVal = MsdpUtPeer_9 ();
                    break;
                case 10:
                    i4RetVal = MsdpUtPeer_10 ();
                    break;
                case 11:
                    i4RetVal = MsdpUtPeer_11 ();
                    break;
                case 12:
                    i4RetVal = MsdpUtPeer_12 ();
                    break;
                case 13:
                    i4RetVal = MsdpUtPeer_13 ();
                    break;
                case 14:
                    i4RetVal = MsdpUtPeer_14 ();
                    break;
                case 15:
                    i4RetVal = MsdpUtPeer_15 ();
                    break;
                case 16:
                    i4RetVal = MsdpUtPeer_16 ();
                    break;
                default:
                    printf ("Invalid Test for file msdppeer.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of msdppeer.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of msdppeer.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* msdpin */
        case 11:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = MsdpUtIn_1 ();
                    break;
                case 2:
                    i4RetVal = MsdpUtIn_2 ();
                    break;
                case 3:
                    i4RetVal = MsdpUtIn_3 ();
                    break;
                case 4:
                    i4RetVal = MsdpUtIn_4 ();
                    break;
                case 5:
                    i4RetVal = MsdpUtIn_5 ();
                    break;
                case 6:
                    i4RetVal = MsdpUtIn_6 ();
                    break;
                case 7:
                    i4RetVal = MsdpUtIn_7 ();
                    break;
                default:
                    printf ("Invalid Test for file msdpin.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of msdpin.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of msdpin.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* msdplw */
        case 12:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = MsdpUtLw_1 ();
                    break;
                case 2:
                    i4RetVal = MsdpUtLw_2 ();
                    break;
                case 3:
                    i4RetVal = MsdpUtLw_3 ();
                    break;
                case 4:
                    i4RetVal = MsdpUtLw_4 ();
                    break;
                case 5:
                    i4RetVal = MsdpUtLw_5 ();
                    break;
                case 6:
                    i4RetVal = MsdpUtLw_6 ();
                    break;
                default:
                    printf ("Invalid Test for file msdplw.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of msdplw.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of msdplw.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* msdplwg */
        case 13:
            return;
            /* msdpmain */
        case 14:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = MsdpUtMain_1 ();
                    break;
                case 2:
                    i4RetVal = MsdpUtMain_2 ();
                    break;
                case 3:
                    i4RetVal = MsdpUtMain_3 ();
                    break;
                case 4:
                    i4RetVal = MsdpUtMain_4 ();
                    break;
                case 5:
                    i4RetVal = MsdpUtMain_5 ();
                    break;
                case 6:
                    i4RetVal = MsdpUtMain_6 ();
                    break;
                case 7:
                    i4RetVal = MsdpUtMain_7 ();
                    break;
                default:
                    printf ("Invalid Test for file msdpmain.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of msdpmain.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of msdpmain.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* msdpout */
        case 15:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = MsdpUtOut_1 ();
                    break;
                case 2:
                    i4RetVal = MsdpUtOut_2 ();
                    break;
                case 3:
                    i4RetVal = MsdpUtOut_3 ();
                    break;
                case 4:
                    i4RetVal = MsdpUtOut_4 ();
                    break;
                case 5:
                    i4RetVal = MsdpUtOut_5 ();
                    break;
                case 6:
                    i4RetVal = MsdpUtOut_6 ();
                    break;
                case 7:
                    i4RetVal = MsdpUtOut_7 ();
                    break;
                case 8:
                    i4RetVal = MsdpUtOut_8 ();
                    break;
                case 9:
                    i4RetVal = MsdpUtOut_9 ();
                    break;
                default:
                    printf ("Invalid Test for file msdpout.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of msdpout.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of msdpout.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* msdppim */
        case 16:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = MsdpUtPim_1 ();
                    break;
                case 2:
                    i4RetVal = MsdpUtPim_2 ();
                    break;
                case 3:
                    i4RetVal = MsdpUtPim_3 ();
                    break;
                case 4:
                    i4RetVal = MsdpUtPim_4 ();
                    break;
                case 5:
                    i4RetVal = MsdpUtPim_5 ();
                    break;
                case 6:
                    i4RetVal = MsdpUtPim_6 ();
                    break;
                default:
                    printf ("Invalid Test for file msdpmain.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of msdppim.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of msdppim.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* msdpport */
        case 17:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = MsdpUtPort_1 ();
                    break;
                case 2:
                    i4RetVal = MsdpUtPort_2 ();
                    break;
                case 3:
                    i4RetVal = MsdpUtPort_3 ();
                    break;
                case 4:
                    i4RetVal = MsdpUtPort_4 ();
                    break;
                case 5:
                    i4RetVal = MsdpUtPort_5 ();
                    break;
                case 6:
                    i4RetVal = MsdpUtPort_6 ();
                    break;
                case 7:
                    i4RetVal = MsdpUtPort_7 ();
                    break;
                case 8:
                    i4RetVal = MsdpUtPort_8 ();
                    break;
                case 9:
                    i4RetVal = MsdpUtPort_9 ();
                    break;
                case 10:
                    i4RetVal = MsdpUtPort_10 ();
                    break;
                case 11:
                    i4RetVal = MsdpUtPort_11 ();
                    break;
                case 12:
                    i4RetVal = MsdpUtPort_12 ();
                    break;
                case 13:
                    i4RetVal = MsdpUtPort_13 ();
                    break;
                case 14:
                    i4RetVal = MsdpUtPort_14 ();
                    break;
                default:
                    printf ("Invalid Test for file msdpport.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of msdpport.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of msdpport.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* msdpque */
        case 18:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = MsdpUtQue_1 ();
                    break;
                case 2:
                    i4RetVal = MsdpUtQue_2 ();
                    break;
                case 3:
                    i4RetVal = MsdpUtQue_3 ();
                    break;
                default:
                    printf ("Invalid Test for file msdpque.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of msdpque.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of msdpque.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* msdpsock */
        case 19:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = MsdpUtSock_1 ();
                    break;
                case 2:
                    i4RetVal = MsdpUtSock_2 ();
                    break;
                case 3:
                    i4RetVal = MsdpUtSock_3 ();
                    break;
                case 4:
                    i4RetVal = MsdpUtSock_4 ();
                    break;
                case 5:
                    i4RetVal = MsdpUtSock_5 ();
                    break;
                case 6:
                    i4RetVal = MsdpUtSock_6 ();
                    break;
                case 7:
                    i4RetVal = MsdpUtSock_7 ();
                    break;
                case 8:
                    i4RetVal = MsdpUtSock_8 ();
                    break;
                case 9:
                    i4RetVal = MsdpUtSock_9 ();
                    break;
                case 10:
                    i4RetVal = MsdpUtSock_10 ();
                    break;
                case 11:
                    i4RetVal = MsdpUtSock_11 ();
                    break;
                case 12:
                    i4RetVal = MsdpUtSock_12 ();
                    break;
                case 13:
                    i4RetVal = MsdpUtSock_13 ();
                    break;
                case 14:
                    i4RetVal = MsdpUtSock_14 ();
                    break;
                case 15:
                    i4RetVal = MsdpUtSock_15 ();
                    break;
                case 16:
                    i4RetVal = MsdpUtSock_16 ();
                    break;
                case 17:
                    i4RetVal = MsdpUtSock_17 ();
                    break;
                case 18:
                    i4RetVal = MsdpUtSock_18 ();
                    break;
                case 19:
                    i4RetVal = MsdpUtSock_19 ();
                    break;
                case 20:
                    i4RetVal = MsdpUtSock_20 ();
                    break;
                case 21:
                    i4RetVal = MsdpUtSock_21 ();
                    break;
                default:
                    printf ("Invalid Test for file msdpsock.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of msdpsock.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of msdpsock.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* msdpsz */
        case 20:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = MsdpUtSz_1 ();
                    break;
                case 2:
                    i4RetVal = MsdpUtSz_2 ();
                    break;
                case 3:
                    i4RetVal = MsdpUtSz_3 ();
                    break;
                default:
                    printf ("Invalid Test for file msdpsz.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of msdpsz.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of msdpsz.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* msdptask */
        case 21:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = MsdpUtTask_1 ();
                    break;
                default:
                    printf ("Invalid Test for file msdptask.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of msdptask.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of msdptask.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* msdptmr */
        case 22:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = MsdpUtTmr_1 ();
                    break;
                case 2:
                    i4RetVal = MsdpUtTmr_2 ();
                    break;
                case 3:
                    i4RetVal = MsdpUtTmr_3 ();
                    break;
                case 4:
                    i4RetVal = MsdpUtTmr_4 ();
                    break;
                case 5:
                    i4RetVal = MsdpUtTmr_5 ();
                    break;
                case 6:
                    i4RetVal = MsdpUtTmr_6 ();
                    break;
                case 7:
                    i4RetVal = MsdpUtTmr_7 ();
                    break;
                case 8:
                    i4RetVal = MsdpUtTmr_8 ();
                    break;
                case 9:
                    i4RetVal = MsdpUtTmr_9 ();
                    break;
                case 10:
                    i4RetVal = MsdpUtTmr_10 ();
                    break;
                default:
                    printf ("Invalid Test for file msdptmr.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of msdptmr.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of msdptmr.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* msdptrc */
        case 23:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = MsdpUtTrc_1 ();
                    break;
                case 2:
                    i4RetVal = MsdpUtTrc_2 ();
                    break;
                case 3:
                    i4RetVal = MsdpUtTrc_3 ();
                    break;
                default:
                    printf ("Invalid Test for file msdptrc.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of msdptrc.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of msdptrc.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* msdputil.c */
        case 24:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = MsdpUtUtil_1 ();
                    break;
                case 2:
                    i4RetVal = MsdpUtUtil_2 ();
                    break;
                case 3:
                    i4RetVal = MsdpUtUtil_3 ();
                    break;
                case 4:
                    i4RetVal = MsdpUtUtil_4 ();
                    break;
                case 5:
                    i4RetVal = MsdpUtUtil_5 ();
                    break;
                case 6:
                    i4RetVal = MsdpUtUtil_6 ();
                    break;
                case 7:
                    i4RetVal = MsdpUtUtil_7 ();
                    break;
                case 8:
                    i4RetVal = MsdpUtUtil_8 ();
                    break;
                case 9:
                    i4RetVal = MsdpUtUtil_9 ();
                    break;
                case 10:
                    i4RetVal = MsdpUtUtil_10 ();
                    break;
                case 11:
                    i4RetVal = MsdpUtUtil_11 ();
                    break;
                case 12:
                    i4RetVal = MsdpUtUtil_12 ();
                    break;
                case 13:
                    i4RetVal = MsdpUtUtil_13 ();
                    break;
                case 14:
                    i4RetVal = MsdpUtUtil_14 ();
                    break;
                case 15:
                    i4RetVal = MsdpUtUtil_15 ();
                    break;
                case 16:
                    i4RetVal = MsdpUtUtil_16 ();
                    break;
                case 17:
                    i4RetVal = MsdpUtCliCache_1 ();
                    break;
                case 18:
                    i4RetVal = MsdpUtCliCache_2 ();
                    break;
                case 19:
                    i4RetVal = MsdpUtCliCache_3 ();
                    break;
                case 20:
                    i4RetVal = MsdpUtCliCache_4 ();
                    break;
                default:
                    printf ("Invalid Test for file msdputil.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of msdputil.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of msdputil.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* msdputl */
        case 25:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = MsdpUtUtl_1 ();
                    break;
                case 2:
                    i4RetVal = MsdpUtUtl_2 ();
                    break;
                case 3:
                    i4RetVal = MsdpUtUtl_3 ();
                    break;
                case 4:
                    i4RetVal = MsdpUtUtl_4 ();
                    break;
                case 5:
                    i4RetVal = MsdpUtUtl_5 ();
                    break;
                case 6:
                    i4RetVal = MsdpUtUtl_6 ();
                    break;
                case 7:
                    i4RetVal = MsdpUtUtl_7 ();
                    break;
                case 8:
                    i4RetVal = MsdpUtUtl_8 ();
                    break;
                case 9:
                    i4RetVal = MsdpUtUtl_9 ();
                    break;
                case 10:
                    i4RetVal = MsdpUtUtl_10 ();
                    break;
                case 11:
                    i4RetVal = MsdpUtUtl_11 ();
                    break;
                case 12:
                    i4RetVal = MsdpUtUtl_12 ();
                    break;
                default:
                    printf ("Invalid Test for file msdputl.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of msdputl.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of msdputl.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* msdputlg */
        case 26:
            /* stdmsdlw */
        case 27:
            return;
    }
}

VOID
MsdpExecuteUtAll (VOID)
{
    UINT1               u1File = 0;

    for (u1File = 1; u1File <= 28; u1File++)
    {
        MsdpExecuteUtFile (u1File);
    }
}

VOID
MsdpExecuteUtFile (UINT4 u4File)
{
    UINT1               u1Case = 0;

    for (u1Case = 1; u1Case <= gTestCase[u4File - 1].u1Case; u1Case++)
    {
        MsdpExecuteUt (u4File, u1Case);

    }
}

/* msdp6port UT cases */
INT4
MsdpUt6Port_1 (VOID)
{
    UINT1               au1Addr[16];
    MEMSET (au1Addr, 0, 16);

    CliVlanInit ();
    sleep (5);
    MsdpUtDelEntry ();
    if (OSIX_FAILURE != Msd6PortGetFirstIfAddr (-1, NULL))
    {
        printf ("Index Invalid and Addr NULL\n");
        CliVlanDeInit ();
        return OSIX_FAILURE;
    }
    if (OSIX_FAILURE != Msd6PortGetFirstIfAddr (0, au1Addr))
    {
        printf ("Index 0 and Addr Valid\n");
        CliVlanDeInit ();
        return OSIX_FAILURE;
    }
    if (OSIX_FAILURE == Msd6PortGetFirstIfAddr (34, au1Addr))
    {
        printf ("Index 34 and Addr valid\n");
        CliVlanDeInit ();
        return OSIX_FAILURE;
    }
    CliVlanDeInit ();
    return OSIX_SUCCESS;
}

INT4
MsdpUt6Port_2 (VOID)
{
    tSNMP_OCTET_STRING_TYPE OctetString;
    UINT1               au1Addr[16];
    UINT2               u2Vlan = 0;
    CHR1               *pc1Addr = "7777::77";

    MEMSET (au1Addr, 0, 16);
    OctetString.pu1_OctetList = au1Addr;

    MsdpUtDelEntry ();
    if (OSIX_SUCCESS == Msd6PortGetVlanIdFromIp6Address (NULL, NULL))
    {
        printf ("addr-NULL and Vlan-NULL\n");
        return OSIX_FAILURE;
    }
    CliVlanInit ();
    MEMCPY (OctetString.pu1_OctetList, pc1Addr, 16);
    OctetString.i4_Length = 16;
    INET_ATON6 (pc1Addr, au1Addr);

    if (OSIX_SUCCESS != Msd6PortGetVlanIdFromIp6Address (&OctetString, &u2Vlan))
    {
        printf ("addr-valid and vlan-passing the addr\n");
        CliVlanDeInit ();
        return OSIX_FAILURE;
    }
    CliVlanDeInit ();
    return OSIX_SUCCESS;
}

INT4
MsdpUt6Port_3 (VOID)
{
    UINT1               au1Addr[16];
    char               *pu1Addr6 = "7777::77";
    MEMSET (au1Addr, 0, 16);

    MsdpUtDelEntry ();
    if (MSDP_INVALID != Msd6PortIsValidLocalAddress (NULL))
    {
        printf ("Addr NULL\n");
        return OSIX_FAILURE;
    }
    if (MSDP_INVALID != Msd6PortIsValidLocalAddress (au1Addr))
    {
        printf ("Invalid IPv6 address\n");
        return OSIX_FAILURE;
    }

    CliVlanInit ();
    INET_ATON6 (pu1Addr6, au1Addr);

    if (MSDP_INVALID == Msd6PortIsValidLocalAddress (au1Addr))
    {
        printf ("Valid IPv6 address\n");
        CliVlanDeInit ();
        return OSIX_FAILURE;
    }
    CliVlanDeInit ();
    return OSIX_SUCCESS;
}

INT4
MsdpUt6Port_4 (VOID)
{
    UINT1               au1Addr[16];
    CHR1               *pIpv6AllZeros = "ff01::5";

    MEMSET (au1Addr, 0, 16);

    MsdpUtDelEntry ();
    INET_ATON6 ((UINT1 *) pIpv6AllZeros, au1Addr);
    if (OSIX_FAILURE != Msd6PortIsAddrMulti (NULL))
    {
        printf ("Addr NULL\n");
        return OSIX_FAILURE;
    }

    if (1 != Msd6PortIsAddrMulti (au1Addr))
    {
        printf ("Multicast address\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
MsdpUt6Port_5 (VOID)
{
    tNetIpv6RtInfoQueryMsg NetIpv6RtQuery;
    tNetIpv6RtInfo      Ip6RtInfo;
    CHR1               *Addr = "7777::7";
    UINT1               au1Addr[16];
    CliVlanInit ();
    sleep (5);
    INET_ATON6 ((UINT1 *) Addr, au1Addr);

    MsdpUtDelEntry ();
    MEMSET (&Ip6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMCPY (Ip6RtInfo.Ip6Dst.u1_addr, au1Addr,
            sizeof (Ip6RtInfo.Ip6Dst.u1_addr));
    Ip6RtInfo.u1Prefixlen = (UINT1) (16 * MSDP_BITS_IN_ONE_BYTE);
    NetIpv6RtQuery.u1QueryFlag = 1;

    Msd6PortIpv6GetRoute (&NetIpv6RtQuery, &Ip6RtInfo);
    CliVlanDeInit ();

    Msd6PortIpv6GetRoute (&NetIpv6RtQuery, &Ip6RtInfo);
    return OSIX_SUCCESS;
}

INT4
MsdpUt6Port_6 (VOID)
{

    MsdpUtDelEntry ();
    if (OSIX_FAILURE == Msd6PortRegiterWithIP6 ())
    {
        printf ("registering MSDP proto id\n");
        return OSIX_FAILURE;
    }
    if (OSIX_FAILURE == Msd6PortRegiterWithIP6 ())
    {
        printf ("registering again MSDP proto id but failed\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
MsdpUt6Port_7 (VOID)
{

    MsdpUtDelEntry ();
    if (OSIX_FAILURE == Msd6PortDeRegiterWithIP6 ())
    {
        printf ("deregistering MSDP proto id\n");
        return OSIX_FAILURE;
    }
    if (OSIX_FAILURE == Msd6PortDeRegiterWithIP6 ())
    {
        printf ("deregistering again MSDP proto id\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
MsdpUt6Port_8 (VOID)
{
    INT4                i4Index = 0;

    tMsdpQueMsg        *pMsdpQueMsg[50];
    tNetIpv6HliParams   Ip6HliParams;
    Ip6HliParams.unIpv6HlCmdType.IfStatChange.u4Index = 33;
    Ip6HliParams.unIpv6HlCmdType.IfStatChange.u4OperStatus = UP;
    Ip6HliParams.unIpv6HlCmdType.IfStatChange.u4IfStat = DOWN;
    Msd6PortHandleIPV6IfChange (&Ip6HliParams);

    MsdpUtDelEntry ();
    MEMSET (pMsdpQueMsg, NULL, sizeof (pMsdpQueMsg));
    for (i4Index = 0; i4Index < MAX_MSDP_QUEUE_MSGS; i4Index++)
    {
        pMsdpQueMsg[i4Index] = MsdpQueAllocQueMsg ();

        if (pMsdpQueMsg[i4Index] == NULL)
        {
            i4Index--;
            break;
        }
    }
    Msd6PortHandleIPV6IfChange (&Ip6HliParams);
    for (; i4Index > 0; i4Index--)
    {
        MemReleaseMemBlock (MSDP_QUE_MEMPOOL_ID,
                            (UINT1 *) pMsdpQueMsg[i4Index]);

    }

    return OSIX_SUCCESS;
}

INT4
MsdpUt6Port_9 (VOID)
{
    /*tNetIpv6AddrChange NetIpv6AddrChange;
       Msd6PortHandleV6AddrChg (&NetIpv6AddrChange); */
    printf ("private function\n");
    return OSIX_SUCCESS;
}

INT4
MsdpUt6Port_10 (VOID)
{
    return -1;
}

PRIVATE tMsdpFsMsdpSACacheEntry *
MsdpUtCacheInit (CHR1 * pu1Addr6)
{
    tMsdpFsMsdpSACacheEntry MsdpInCache;
    tMsdpFsMsdpSACacheEntry *pMsdpInCache = NULL;

    MEMSET (&MsdpInCache, NULL, sizeof (tMsdpFsMsdpSACacheEntry));

    MsdpInCache.MibObject.i4FsMsdpSACacheAddrType = IPVX_ADDR_FMLY_IPV6;
    MsdpInCache.MibObject.i4FsMsdpSACacheGroupAddrLen = IPVX_IPV6_ADDR_LEN;
    MsdpInCache.MibObject.i4FsMsdpSACacheSourceAddrLen = IPVX_IPV6_ADDR_LEN;
    MsdpInCache.MibObject.i4FsMsdpSACacheOriginRPLen = IPVX_IPV6_ADDR_LEN;
    INET_ATON6 (pu1Addr6, MsdpInCache.MibObject.au1FsMsdpSACacheGroupAddr);
    INET_ATON6 (pu1Addr6, MsdpInCache.MibObject.au1FsMsdpSACacheSourceAddr);
    INET_ATON6 (pu1Addr6, MsdpInCache.MibObject.au1FsMsdpSACacheOriginRP);

    pMsdpInCache = MsdpFsMsdpSACacheTableCreateApi (&MsdpInCache);
    return pMsdpInCache;
}

PRIVATE INT4
MsdpUtCacheDeInit (CHR1 * pu1Addr6)
{
    tMsdpFsMsdpSACacheEntry MsdpInCache;
    tMsdpFsMsdpSACacheEntry *pOldSaCache = NULL;

    MEMSET (&MsdpInCache, NULL, sizeof (tMsdpFsMsdpSACacheEntry));

    MsdpInCache.MibObject.i4FsMsdpSACacheAddrType = IPVX_ADDR_FMLY_IPV6;
    MsdpInCache.MibObject.i4FsMsdpSACacheGroupAddrLen = IPVX_IPV6_ADDR_LEN;
    MsdpInCache.MibObject.i4FsMsdpSACacheSourceAddrLen = IPVX_IPV6_ADDR_LEN;
    MsdpInCache.MibObject.i4FsMsdpSACacheOriginRPLen = IPVX_IPV6_ADDR_LEN;
    INET_ATON6 (pu1Addr6, MsdpInCache.MibObject.au1FsMsdpSACacheGroupAddr);
    INET_ATON6 (pu1Addr6, MsdpInCache.MibObject.au1FsMsdpSACacheSourceAddr);
    INET_ATON6 (pu1Addr6, MsdpInCache.MibObject.au1FsMsdpSACacheOriginRP);

    pOldSaCache = MsdpGetFsMsdpSACacheTable (&MsdpInCache);

    if (OSIX_FAILURE == MsdpUtilRemoveCacheEntry (pOldSaCache))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

PRIVATE INT4
MsdpUtCacheMemInit (VOID)
{
    tMsdpFsMsdpSACacheEntry MsdpInCache;
    tMsdpFsMsdpSACacheEntry *pMsdpInCache = NULL;
    UINT1               u1Index = 0;

    for (u1Index = 1; u1Index < MAX_MSDP_FSMSDPSACACHETABLE; u1Index++)
    {
        MEMSET (&MsdpInCache, NULL, sizeof (tMsdpFsMsdpSACacheEntry));
        MsdpInCache.MibObject.i4FsMsdpSACacheAddrType = IPVX_ADDR_FMLY_IPV6;
        MsdpInCache.MibObject.i4FsMsdpSACacheGroupAddrLen = IPVX_IPV6_ADDR_LEN;
        MsdpInCache.MibObject.i4FsMsdpSACacheSourceAddrLen = IPVX_IPV6_ADDR_LEN;
        MsdpInCache.MibObject.i4FsMsdpSACacheOriginRPLen = IPVX_IPV6_ADDR_LEN;
        MsdpInCache.MibObject.au1FsMsdpSACacheGroupAddr[0] = u1Index;
        MsdpInCache.MibObject.au1FsMsdpSACacheSourceAddr[0] = u1Index;
        MsdpInCache.MibObject.au1FsMsdpSACacheOriginRP[0] = u1Index;
        pMsdpInCache = MsdpFsMsdpSACacheTableCreateApi (&MsdpInCache);
        if (NULL == pMsdpInCache)
        {
            /*  printf ("cant make overflow on SACache"); */
            return OSIX_SUCCESS;
        }
    }
    return OSIX_SUCCESS;
}

PRIVATE INT4
MsdpUtCacheMemDeInit (VOID)
{
    tMsdpFsMsdpSACacheEntry MsdpInCache;
    tMsdpFsMsdpSACacheEntry *pOldSaCache = NULL;
    UINT1               u1Index = 0;

    for (u1Index = 1; u1Index < MAX_MSDP_FSMSDPSACACHETABLE; u1Index++)
    {
        MEMSET (&MsdpInCache, NULL, sizeof (tMsdpFsMsdpSACacheEntry));
        MsdpInCache.MibObject.i4FsMsdpSACacheAddrType = IPVX_ADDR_FMLY_IPV6;
        MsdpInCache.MibObject.i4FsMsdpSACacheGroupAddrLen = IPVX_IPV6_ADDR_LEN;
        MsdpInCache.MibObject.i4FsMsdpSACacheSourceAddrLen = IPVX_IPV6_ADDR_LEN;
        MsdpInCache.MibObject.i4FsMsdpSACacheOriginRPLen = IPVX_IPV6_ADDR_LEN;
        MsdpInCache.MibObject.au1FsMsdpSACacheGroupAddr[0] = u1Index;
        MsdpInCache.MibObject.au1FsMsdpSACacheSourceAddr[0] = u1Index;
        MsdpInCache.MibObject.au1FsMsdpSACacheOriginRP[0] = u1Index;
        pOldSaCache = MsdpGetFsMsdpSACacheTable (&MsdpInCache);
/*
        if (OSIX_FAILURE ==  MsdpUtilRemoveCacheEntry (pOldSaCache))
        {
            printf ("cant make discard the memory blks  on SACache");
            return OSIX_FAILURE;
        }*/
    }
    return OSIX_SUCCESS;
}

/* msdpcache UT cases */
INT4
MsdpUtCache_1 (VOID)
{
    tMsdpFsMsdpSACacheEntry MsdpFsMsdpSACacheEntry;
    tMsdpCacheUpdt      MsdpCacheUpdt;
    tMsdpFsMsdpPeerEntry PeerEntry;
/*    CHR1                    *pu1Addr6 = "7777::77";*/
    MEMSET (&MsdpFsMsdpSACacheEntry, NULL, sizeof (tMsdpFsMsdpSACacheEntry));
    MsdpUtDelEntry ();

    MsdpCacheUpdt.i1Flag = MSDP_SA_ADVT_CACHE;
/*    INET_ATON6 (pu1Addr6, &(MsdpCacheUpdt.pLrndFrmPeer->MibObject.au1FsMsdpPeerRemoteAddress[0]));*/
    MsdpCacheUpdt.GrpAddr.u1AddrLen = 16;
    MEMSET (&PeerEntry, NULL, sizeof (tMsdpFsMsdpPeerEntry));

    MsdpCacheUpdt.pLrndFrmPeer = &(PeerEntry);

    MsdCacheFormEntry (0, &MsdpFsMsdpSACacheEntry, &MsdpCacheUpdt);

    return OSIX_SUCCESS;
}

INT4
MsdpUtCache_2 (VOID)
{
    tMsdpCacheUpdt      CacheUpdt;
    tMsdpFsMsdpPeerEntry PeerEntry;
    tMsdpFsMsdpSACacheEntry *pMsdpInCache = NULL;
    CHR1               *pu1Addr6 = "7777::77";
    UINT1               u1Status = 0;

    MEMSET (&CacheUpdt, 0, sizeof (tMsdpCacheUpdt));
    MEMSET (&PeerEntry, NULL, sizeof (tMsdpFsMsdpPeerEntry));

    MsdpUtDelEntry ();
    CacheUpdt.pLrndFrmPeer = &(PeerEntry);

    pMsdpInCache = MsdpUtCacheInit (pu1Addr6);
    if (NULL == pMsdpInCache)
    {
        printf ("Cache Node Init failed\n");
        return OSIX_FAILURE;
    }

    CacheUpdt.u1AddrType = IPVX_ADDR_FMLY_IPV6;
    CacheUpdt.i1Flag = MSDP_PIM_CACHE;
    CacheUpdt.u1Action = MSDP_ADD_CACHE;
    CacheUpdt.GrpAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;

    INET_ATON6 (pu1Addr6, CacheUpdt.OriginatorRP.au1Addr);
    INET_ATON6 (pu1Addr6, CacheUpdt.GrpAddr.au1Addr);
    INET_ATON6 (pu1Addr6, CacheUpdt.SrcAddr.au1Addr);

    if (OSIX_SUCCESS != MsdCacheUpdateCacheTbl (CacheUpdt, 0, &u1Status))
    {
        printf ("Flag:MSDP_PIM_CACHE, Action: MSDP_ADD_CACHE \n");
        MsdpUtCacheDeInit (pu1Addr6);
        return OSIX_FAILURE;
    }

    CacheUpdt.u1AddrType = IPVX_ADDR_FMLY_IPV6;
    CacheUpdt.i1Flag = MSDP_SA_ADVT_CACHE;
    CacheUpdt.u1Action = MSDP_ADD_CACHE;

    if (OSIX_SUCCESS != MsdCacheUpdateCacheTbl (CacheUpdt, 0, &u1Status))
    {
        printf ("Flag:MSDP_SA_ADVT_CACHE, Action: MSDP_ADD_CACHE \n");
        MsdpUtCacheDeInit (pu1Addr6);
        return OSIX_FAILURE;
    }

    CacheUpdt.i1Flag = MSDP_PIM_CACHE;
    CacheUpdt.u1Action = 2;

    if (OSIX_SUCCESS != MsdCacheUpdateCacheTbl (CacheUpdt, 0, &u1Status))
    {
        printf ("Flag:MSDP_PIM_CACHE, Action: 2 \n");
        MsdpUtCacheDeInit (pu1Addr6);
        return OSIX_FAILURE;
    }

    CacheUpdt.u1Action = MSDP_DELETE_CACHE;

    if (OSIX_SUCCESS != MsdCacheUpdateCacheTbl (CacheUpdt, 0, &u1Status))
    {
        printf ("Flag:MSDP_PIM_CACHE, Action: MSDP_DEL_CACHE \n");
        MsdpUtCacheDeInit (pu1Addr6);
        return OSIX_FAILURE;
    }

    CacheUpdt.u1Action = MSDP_ADD_CACHE;

    if (OSIX_SUCCESS != MsdCacheUpdateCacheTbl (CacheUpdt, 0, &u1Status))
    {
        printf ("Flag: MSDP_PIM_CACHE, Action: MSDP_ADD_CACHE \n");
        MsdpUtCacheDeInit (pu1Addr6);
        return OSIX_FAILURE;
    }

/*    INET_ATON6 (pu1Addr6, CacheUpdt.OriginatorRP.au1Addr);
    INET_ATON6 (pu1Addr6, CacheUpdt.GrpAddr.au1Addr);
    INET_ATON6 (pu1Addr6, CacheUpdt.SrcAddr.au1Addr);
*/
    MsdpUtCacheDeInit (pu1Addr6);
    MEMSET (&CacheUpdt, 0, sizeof (tMsdpCacheUpdt));
    CacheUpdt.u1Action = MSDP_ADD_CACHE;

    if (OSIX_FAILURE == MsdpUtCacheMemInit ())
    {
        return OSIX_FAILURE;
    }
    if (OSIX_FAILURE != MsdCacheUpdateCacheTbl (CacheUpdt, 0, &u1Status))
    {
        printf ("Flag: MSDP_PIM_CACHE, Action: 2 \n");
        MsdpUtCacheMemDeInit ();
        return OSIX_FAILURE;
    }
    if (OSIX_FAILURE == MsdpUtCacheMemDeInit ())
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
MsdpUtCache_3 (VOID)
{
    tMsdpCacheUpdt      MsdpCacheUpdt;
    MsdCacheApplySAFilter (&MsdpCacheUpdt);
    return OSIX_SUCCESS;
}

INT4
MsdpUtCache_4 (VOID)
{
    tMsdpCacheUpdt      Cache;
    tCRU_BUF_CHAIN_HEADER Buffer;
    UINT2               u2PktLen = 0;

    MEMSET (&Cache, 0, sizeof (tMsdpCacheUpdt));
    MEMSET (&Buffer, 0, sizeof (tCRU_BUF_CHAIN_HEADER));

    MsdpUtDelEntry ();
    Cache.u1AddrType = IPVX_ADDR_FMLY_IPV4;
    MsdCacheFormOriginSATlv (&Cache, &Buffer, 0, &u2PktLen);

    MEMSET (&Cache, 0, sizeof (tMsdpCacheUpdt));
    MEMSET (&Buffer, 0, sizeof (tCRU_BUF_CHAIN_HEADER));

    Cache.u1AddrType = IPVX_ADDR_FMLY_IPV6;
    MsdCacheFormOriginSATlv (&Cache, &Buffer, 1500, &u2PktLen);

    MsdCacheFormOriginSATlv (&Cache, NULL, 1500, &u2PktLen);

    return OSIX_SUCCESS;
}

INT4
MsdpUtCache_5 (VOID)
{
    tCRU_BUF_CHAIN_HEADER Buffer;
    tMsdpCacheUpdt      Cache;

    MEMSET (&Cache, 0, sizeof (tMsdpCacheUpdt));
    MEMSET (&Buffer, 0, sizeof (tCRU_BUF_CHAIN_HEADER));

    MsdpUtDelEntry ();
    Cache.u1AddrType = IPVX_ADDR_FMLY_IPV4;

    MsdCacheOriginateSAMsg (&Cache, &Buffer, 0);

    return OSIX_SUCCESS;
}

INT4
MsdpUtCache_6 (VOID)
{
    tMsdpFsMsdpRPEntry  RpEntry;
    tMsdpFsMsdpRPEntry *pRpEntry = NULL;
    UINT1               au1RetVal[16];

    MEMSET (au1RetVal, 0, 16);
    MEMSET (&RpEntry, NULL, sizeof (tMsdpFsMsdpRPEntry));

    MsdpUtDelEntry ();
    if (OSIX_FALSE != MsdCacheAmIOriginator (IPVX_ADDR_FMLY_IPV4,
                                             IPVX_IPV4_ADDR_LEN, au1RetVal))
    {
        printf ("Fmly-IPV4, Len-IPV4\n");
        return OSIX_FAILURE;
    }

    RpEntry.MibObject.i4FsMsdpRPAddrType = IPVX_ADDR_FMLY_IPV6;

    if (NULL == MsdpFsMsdpRPTableCreateApi (&RpEntry))
    {
        printf ("RP Table - Node add failed\n");
        return OSIX_FAILURE;
    }

    if (OSIX_FALSE != MsdCacheAmIOriginator (IPVX_ADDR_FMLY_IPV6,
                                             IPVX_IPV6_ADDR_LEN, au1RetVal))
    {
        printf ("Fmly-IPV6, Len-IPV6 Addr-ALL zeros\n");
        return OSIX_FAILURE;
    }

    pRpEntry = MsdpGetFsMsdpRPTable (&RpEntry);
    if (pRpEntry == NULL)
    {
        printf ("Getting RP node failed\n");
        return OSIX_FAILURE;
    }

    pRpEntry->MibObject.au1FsMsdpRPAddress[0] = 7;

    if (OSIX_FALSE != MsdCacheAmIOriginator (IPVX_ADDR_FMLY_IPV6,
                                             IPVX_IPV6_ADDR_LEN, au1RetVal))
    {
        printf ("Fmly-IPV6, Len-IPV6, Valid addr, Oper down\n");
        return OSIX_FAILURE;
    }

    pRpEntry->MibObject.i4FsMsdpRPOperStatus = MSDP_IF_OPERSTATUS_UP;

    if (OSIX_TRUE != MsdCacheAmIOriginator (IPVX_ADDR_FMLY_IPV6,
                                            IPVX_IPV6_ADDR_LEN, au1RetVal))
    {
        printf ("Fmly-IPV6, Len-IPV6, Oper up\n");
        return OSIX_FAILURE;
    }

    if (RB_FAILURE == RBTreeRemove
        (gMsdpGlobals.MsdpGlbMib.FsMsdpRPTable, (tRBElem *) pRpEntry))
    {
        printf ("RP RBtree remove failed\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
MsdpUtCache_7 (VOID)
{
    tMsdpSAReqGrp       ReqGrpAddr;
    CHR1               *pu1Addr6 = "7777::77";

    MsdpUtDelEntry ();
    INET_ATON6 (pu1Addr6, ReqGrpAddr.GrpAddr.au1Addr);
    ReqGrpAddr.GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;

    MsdCacheSendSARequest (&ReqGrpAddr);

    MEMSET (ReqGrpAddr.GrpAddr.au1Addr, 0, 16);
    ReqGrpAddr.GrpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;

    MsdCacheSendSARequest (&ReqGrpAddr);

    return OSIX_SUCCESS;
}

INT4
MsdpUtCache_8 (VOID)
{
    UINT1               au1Addr[4];

    MEMSET (au1Addr, 0, 4);

    MsdpUtDelEntry ();
    MsdCacheFormSAHdr (MSDP_TLV_SA_RESPONSE, 4, 0, au1Addr);

    return OSIX_SUCCESS;
}

INT4
MsdpUtCache_9 (VOID)
{
    tMsdpFsMsdpSACacheEntry SaCache;
    INT4                i4Offset = 0;
    UINT1               u1Count = 0;

    MEMSET (&SaCache, NULL, sizeof (tMsdpFsMsdpSACacheEntry));

    MsdpUtDelEntry ();
    SaCache.MibObject.i4FsMsdpSACacheGroupAddrLen = IPVX_IPV6_ADDR_LEN;

    MsdCacheFormSAPkt (&SaCache, &i4Offset, &u1Count);

    return OSIX_SUCCESS;
}

INT4
MsdpUtCache_10 (VOID)
{
    UINT1               au1MyOrigin[16];
    UINT1               au1CacheOrigin[16];
    UINT1               au1RP[16];

    INT4                i4Offset = 0;
    MEMSET (au1MyOrigin, 1, 16);
    MEMSET (au1CacheOrigin, 2, 16);
    MEMSET (au1RP, 0, 16);

    MsdpUtDelEntry ();
    MsdCacheSetCurrentRPAddr (OSIX_TRUE, IPVX_IPV6_ADDR_LEN, au1MyOrigin,
                              au1CacheOrigin, &i4Offset, au1RP);

    i4Offset = 0;
    MsdCacheSetCurrentRPAddr (OSIX_FALSE, IPVX_IPV6_ADDR_LEN, au1MyOrigin,
                              au1CacheOrigin, &i4Offset, au1RP);

    i4Offset = 12;
    MEMSET (au1RP, 0, 4);
    MsdCacheSetCurrentRPAddr (OSIX_FALSE, IPVX_IPV6_ADDR_LEN, au1MyOrigin,
                              au1CacheOrigin, &i4Offset, au1RP);

    return OSIX_SUCCESS;
}

INT4
MsdpUtCache_11 (VOID)
{
    tMsdpFsMsdpSACacheEntry *pCache1 = NULL;
    tMsdpFsMsdpSACacheEntry *pCache2 = NULL;
    tMsdpFsMsdpSACacheEntry Cache3;
    CHR1               *pu1Addr1 = "7777::7";
    CHR1               *pu1Addr2 = "8888::8";
    UINT1               au1Addr[16];

    MEMSET (au1Addr, 0, 16);

    MsdpUtDelEntry ();
    pCache1 = MsdpUtCacheInit (pu1Addr1);

    if (pCache1 == NULL)
    {
        printf ("Cache1 Init failed\n");
        return OSIX_FAILURE;
    }

    if (NULL != MsdCacheGetNextValidSACache (pCache1, IPVX_ADDR_FMLY_IPV6,
                                             IPVX_IPV6_ADDR_LEN, au1Addr))
    {
        printf ("failure 1\n");
        return OSIX_FAILURE;
    }

    pCache2 = MsdpUtCacheInit (pu1Addr2);

    if (pCache2 == NULL)
    {
        printf ("Cache2 Init failed\n");
        return OSIX_FAILURE;
    }

    MEMSET (&Cache3, NULL, sizeof (Cache3));
    Cache3.MibObject.i4FsMsdpSACacheAddrType = 1;
    Cache3.MibObject.i4FsMsdpSACacheSourceAddrLen = 4;
    Cache3.MibObject.i4FsMsdpSACacheGroupAddrLen = 4;
    Cache3.MibObject.i4FsMsdpSACacheOriginRPLen = 4;
    if (NULL != MsdCacheGetNextValidSACache (&Cache3, IPVX_ADDR_FMLY_IPV4, IPVX_IPV4_ADDR_LEN, au1Addr))    /*Addr Type is different */
    {
        printf ("failure 2\n");
        return OSIX_FAILURE;
    }

    if (NULL != MsdCacheGetNextValidSACache (pCache1, IPVX_ADDR_FMLY_IPV6, IPVX_IPV6_ADDR_LEN, au1Addr))    /*Grp Address is not same */
    {
        printf ("failure 3\n");
        return OSIX_FAILURE;
    }

    INET_ATON6 (pu1Addr2, au1Addr);

    if (NULL == MsdCacheGetNextValidSACache (pCache1, IPVX_ADDR_FMLY_IPV6, IPVX_IPV6_ADDR_LEN, au1Addr))    /*Grp Address is same */
    {
        printf ("failure 4\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
MsdpUtCache_12 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;
    tMsdpFsMsdpSACacheEntry SaCache;
    tMsdpFsMsdpRPEntry  Rp;
    tMsdpSaReqPkt       SaReqPkt;
    UINT1               au1TxBuf[1000];

    MEMSET (&Peer, NULL, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&SaCache, NULL, sizeof (tMsdpFsMsdpSACacheEntry));
    MEMSET (&SaReqPkt, 0, sizeof (tMsdpSaReqPkt));

    MsdpUtDelEntry ();
    Peer.TxBufInfo.pu1Msg = au1TxBuf;
    Peer.TxBufInfo.u2MsgSize = MSDP_PEER_MAX_RX_TX_BUF_LEN - 10;
    MsdCacheSendSAResponseTlv (MSDP_TLV_SA_RESPONSE, &Peer, 1, &SaReqPkt);

    Peer.TxBufInfo.u2MsgSize = MSDP_PEER_MAX_RX_TX_BUF_LEN - 25;
    MEMSET (SaCache.MibObject.au1FsMsdpSACacheOriginRP, 10, 16);
    SaCache.MibObject.au1FsMsdpSACacheGroupAddr[0] = 224;
    SaCache.MibObject.au1FsMsdpSACacheGroupAddr[1] = 10;
    SaCache.MibObject.au1FsMsdpSACacheGroupAddr[2] = 10;
    SaCache.MibObject.au1FsMsdpSACacheGroupAddr[3] = 11;
    MEMSET (SaCache.MibObject.au1FsMsdpSACacheSourceAddr, 10, 16);
    SaCache.MibObject.i4FsMsdpSACacheAddrType = 1;
    SaCache.MibObject.i4FsMsdpSACacheOriginRPLen = 4;
    SaCache.MibObject.i4FsMsdpSACacheGroupAddrLen = 4;
    SaCache.MibObject.i4FsMsdpSACacheSourceAddrLen = 4;
    MsdpFsMsdpSACacheTableCreateApi (&SaCache);

    MEMSET (SaCache.MibObject.au1FsMsdpSACacheSourceAddr, 11, 16);
    MsdpFsMsdpSACacheTableCreateApi (&SaCache);

    MEMSET (SaCache.MibObject.au1FsMsdpSACacheSourceAddr, 12, 16);
    MsdpFsMsdpSACacheTableCreateApi (&SaCache);

    SaReqPkt.au1GrpAddr[0] = 224;
    SaReqPkt.au1GrpAddr[1] = 10;
    SaReqPkt.au1GrpAddr[2] = 10;
    SaReqPkt.au1GrpAddr[3] = 10;
    SaReqPkt.u1Type = 1;
    SaReqPkt.u2Len = 4;
    MsdCacheSendSAResponseTlv (MSDP_TLV_SA_RESPONSE, &Peer, 1, &SaReqPkt);

    SaReqPkt.au1GrpAddr[3] = 11;
    MsdCacheSendSAResponseTlv (MSDP_TLV_SA_RESPONSE, &Peer, 1, &SaReqPkt);

    Peer.TxBufInfo.u2MsgSize = MSDP_PEER_MAX_RX_TX_BUF_LEN - 65;
    MsdCacheSendSAResponseTlv (MSDP_TLV_SA_RESPONSE, &Peer, 1, &SaReqPkt);

    SaCache.MibObject.au1FsMsdpSACacheGroupAddr[0] = 224;
    SaCache.MibObject.au1FsMsdpSACacheGroupAddr[1] = 10;
    SaCache.MibObject.au1FsMsdpSACacheGroupAddr[2] = 10;
    SaCache.MibObject.au1FsMsdpSACacheGroupAddr[3] = 11;

    MEMSET (SaCache.MibObject.au1FsMsdpSACacheOriginRP, 0, 16);
    MEMSET (SaCache.MibObject.au1FsMsdpSACacheSourceAddr, 10, 16);
    SaCache.MibObject.i4FsMsdpSACacheAddrType = 1;
    SaCache.MibObject.i4FsMsdpSACacheOriginRPLen = 4;
    SaCache.MibObject.i4FsMsdpSACacheGroupAddrLen = 4;
    SaCache.MibObject.i4FsMsdpSACacheSourceAddrLen = 4;
    MsdpFsMsdpSACacheTableCreateApi (&SaCache);

    MsdCacheSendSAResponseTlv (MSDP_TLV_SA_RESPONSE, &Peer, 1, &SaReqPkt);

    Rp.MibObject.au1FsMsdpRPAddress[0] = 12;
    Rp.MibObject.au1FsMsdpRPAddress[3] = 12;
    Rp.MibObject.i4FsMsdpRPAddrType = 1;
    Rp.MibObject.i4FsMsdpRPOperStatus = UP;
    MsdpFsMsdpRPTableCreateApi (&Rp);

    MsdCacheSendSAResponseTlv (MSDP_TLV_SA_RESPONSE, &Peer, 1, &SaReqPkt);

    Peer.TxBufInfo.u2MsgSize = MSDP_PEER_MAX_RX_TX_BUF_LEN - 60;
    MsdCacheSendSAResponseTlv (MSDP6_TLV_SA_RESPONSE, &Peer, 2, &SaReqPkt);

    return OSIX_SUCCESS;
}

INT4
MsdpUtCache_13 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;
    tMsdpSaReqPkt       SaReqPkt;
    UINT1               au1TxBuf[1000];

    MsdpUtDelEntry ();
    MEMSET (&Peer, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&SaReqPkt, 0, sizeof (tMsdpSaReqPkt));

    Peer.TxBufInfo.pu1Msg = au1TxBuf;
    MsdCacheHandleSARequest (&Peer, &SaReqPkt, IPVX_ADDR_FMLY_IPV4);

    MsdCacheHandleSARequest (&Peer, &SaReqPkt, IPVX_ADDR_FMLY_IPV6);

    return OSIX_SUCCESS;
}

INT4
MsdpUtCache_14 (VOID)
{
    tMsdpFsMsdpSACacheEntry SaCache;
    tMsdpFsMsdpSACacheEntry *pSaCache = NULL;

    MsdpUtDelEntry ();
    MEMSET (&SaCache, NULL, sizeof (tMsdpFsMsdpSACacheEntry));

    MEMSET (SaCache.MibObject.au1FsMsdpSACacheOriginRP, 10, 16);
    SaCache.MibObject.au1FsMsdpSACacheGroupAddr[0] = 226;
    SaCache.MibObject.au1FsMsdpSACacheGroupAddr[1] = 10;
    SaCache.MibObject.au1FsMsdpSACacheGroupAddr[2] = 11;
    SaCache.MibObject.au1FsMsdpSACacheGroupAddr[3] = 11;
    MEMSET (SaCache.MibObject.au1FsMsdpSACacheSourceAddr, 10, 16);
    SaCache.MibObject.i4FsMsdpSACacheAddrType = 1;
    SaCache.MibObject.i4FsMsdpSACacheOriginRPLen = 4;
    SaCache.MibObject.i4FsMsdpSACacheGroupAddrLen = 4;
    SaCache.MibObject.i4FsMsdpSACacheSourceAddrLen = 4;
    pSaCache = MsdpFsMsdpSACacheTableCreateApi (&SaCache);

    if (pSaCache == NULL)
    {
        printf ("\n UT_14 : Failure in adding Cache - 1\r\n");
        return OSIX_FAILURE;
    }
    MsdpTmrStartTmr (&pSaCache->CacheSaStateTimer, MSDP_CACHE_STATE_TMR, 65);
    MsdCacheDeleteEntry (&SaCache, MSDP_SA_RESP_CACHE);

    return OSIX_SUCCESS;
}

INT4
MsdpUtCache_15 (VOID)
{
    MsdCacheSendSAAdvtEvent ();
    return OSIX_SUCCESS;
}

INT4
MsdpUtCache_16 (VOID)
{
    tMsdpFsMsdpSACacheEntry CurSaCache;
    tMsdpFsMsdpSACacheEntry *pSaCache = NULL;
    UINT1               u1EntryCount = 0;
    UINT1               u1NextAddrType = 0;
    INT4                i4BufSize = 100;
    INT4                i4Offset = 0;

    MsdpUtDelEntry ();
    MEMSET (&CurSaCache, NULL, sizeof (tMsdpFsMsdpSACacheEntry));

    MsdCacheAddValidCacheInSAPkt (1, &i4BufSize, &CurSaCache,
                                  &u1EntryCount, &i4Offset, &u1NextAddrType);
    MEMSET (CurSaCache.MibObject.au1FsMsdpSACacheOriginRP, 10, 16);
    MEMSET (CurSaCache.MibObject.au1FsMsdpSACacheGroupAddr, 10, 16);
    MEMSET (CurSaCache.MibObject.au1FsMsdpSACacheSourceAddr, 10, 16);
    CurSaCache.MibObject.i4FsMsdpSACacheAddrType = 2;
    CurSaCache.MibObject.i4FsMsdpSACacheOriginRPLen = 4;
    CurSaCache.MibObject.i4FsMsdpSACacheGroupAddrLen = 4;
    CurSaCache.MibObject.i4FsMsdpSACacheSourceAddrLen = 4;
    pSaCache = MsdpFsMsdpSACacheTableCreateApi (&CurSaCache);

    CurSaCache.MibObject.i4FsMsdpSACacheAddrType = 1;
    MsdCacheAddValidCacheInSAPkt (1, &i4BufSize, &CurSaCache,
                                  &u1EntryCount, &i4Offset, &u1NextAddrType);
    pSaCache->MibObject.i4FsMsdpSACacheAddrType = 1;
    MEMSET (CurSaCache.MibObject.au1FsMsdpSACacheOriginRP, 0, 16);
    MEMSET (CurSaCache.MibObject.au1FsMsdpSACacheSourceAddr, 10, 16);
    CurSaCache.MibObject.i4FsMsdpSACacheAddrType = 1;
    CurSaCache.MibObject.i4FsMsdpSACacheOriginRPLen = 4;
    CurSaCache.MibObject.i4FsMsdpSACacheGroupAddrLen = 4;
    CurSaCache.MibObject.i4FsMsdpSACacheSourceAddrLen = 4;
    MEMSET (CurSaCache.MibObject.au1FsMsdpSACacheOriginRP, 0, 16);
    pSaCache = MsdpFsMsdpSACacheTableCreateApi (&CurSaCache);

    MEMSET (&CurSaCache, NULL, sizeof (CurSaCache));
    i4BufSize = 10;
    i4Offset = 0;
    MsdCacheAddValidCacheInSAPkt (1, &i4BufSize, &CurSaCache,
                                  &u1EntryCount, &i4Offset, &u1NextAddrType);
    i4BufSize = 100;
    MsdCacheAddValidCacheInSAPkt (1, &i4BufSize, &CurSaCache,
                                  &u1EntryCount, &i4Offset, &u1NextAddrType);

    MsdCacheAddValidCacheInSAPkt (1, &i4BufSize, &CurSaCache,
                                  &u1EntryCount, &i4Offset, &u1NextAddrType);

    return OSIX_SUCCESS;
}

INT4
MsdpUtCache_17 (VOID)
{
    tMsdpCacheAdvtMarker CacheAdvtMarker;
    tMsdpFsMsdpRPEntry  Rp;
    tMsdpFsMsdpSACacheEntry CurSaCache;

    MsdpUtDelEntry ();
    MEMSET (&Rp, NULL, sizeof (tMsdpFsMsdpRPEntry));
    MEMSET (&CurSaCache, NULL, sizeof (tMsdpFsMsdpSACacheEntry));
    MEMSET (&CacheAdvtMarker, 0, sizeof (tMsdpCacheAdvtMarker));

    MsdCacheSAInfoBatchAdvt (NULL);

    CacheAdvtMarker.OrgRpAddr.u1Afi = 1;
    CacheAdvtMarker.OrgRpAddr.u1AddrLen = 4;
    MsdCacheSAInfoBatchAdvt (&CacheAdvtMarker);

    Rp.MibObject.au1FsMsdpRPAddress[0] = 12;
    Rp.MibObject.au1FsMsdpRPAddress[3] = 10;
    Rp.MibObject.i4FsMsdpRPAddressLen = 4;
    Rp.MibObject.i4FsMsdpRPAddrType = 1;
    Rp.MibObject.i4FsMsdpRPOperStatus = MSDP_IF_OPERSTATUS_UP;
    MsdpFsMsdpRPTableCreateApi (&Rp);

    MsdCacheSAInfoBatchAdvt (&CacheAdvtMarker);

    CurSaCache.MibObject.i4FsMsdpSACacheAddrType = 1;
    CurSaCache.MibObject.i4FsMsdpSACacheOriginRPLen = 4;
    CurSaCache.MibObject.i4FsMsdpSACacheGroupAddrLen = 4;
    MEMSET (CurSaCache.MibObject.au1FsMsdpSACacheGroupAddr, 224, 16);
    CurSaCache.MibObject.i4FsMsdpSACacheSourceAddrLen = 4;
    MEMSET (CurSaCache.MibObject.au1FsMsdpSACacheSourceAddr, 13, 16);
    if (NULL == MsdpFsMsdpSACacheTableCreateApi (&CurSaCache))
    {
        printf ("\r\nAdding Cache Entry Failed - 1\r\n");
    }

    MsdCacheSAInfoBatchAdvt (&CacheAdvtMarker);

    CurSaCache.MibObject.i4FsMsdpSACacheAddrType = 2;
    CurSaCache.MibObject.i4FsMsdpSACacheOriginRPLen = 16;
    CurSaCache.MibObject.i4FsMsdpSACacheGroupAddrLen = 16;
    CurSaCache.MibObject.i4FsMsdpSACacheSourceAddrLen = 16;
    if (NULL == MsdpFsMsdpSACacheTableCreateApi (&CurSaCache))
    {
        printf ("\r\nAdding Cache Entry Failed - 2\r\n");
    }

    CacheAdvtMarker.OrgRpAddr.u1Afi = 1;
    CacheAdvtMarker.OrgRpAddr.u1AddrLen = 4;
    MsdCacheSAInfoBatchAdvt (&CacheAdvtMarker);

    MsdpUtDelEntry ();
    CurSaCache.MibObject.i4FsMsdpSACacheAddrType = 2;
    CurSaCache.MibObject.i4FsMsdpSACacheOriginRPLen = 16;
    CurSaCache.MibObject.i4FsMsdpSACacheGroupAddrLen = 16;
    CurSaCache.MibObject.i4FsMsdpSACacheSourceAddrLen = 16;
    if (NULL == MsdpFsMsdpSACacheTableCreateApi (&CurSaCache))
    {
        printf ("\r\nAdding Cache Entry Failed - 3\r\n");
    }

    Rp.MibObject.au1FsMsdpRPAddress[0] = 12;
    Rp.MibObject.au1FsMsdpRPAddress[3] = 10;
    Rp.MibObject.i4FsMsdpRPAddressLen = 4;
    Rp.MibObject.i4FsMsdpRPAddrType = 1;
    Rp.MibObject.i4FsMsdpRPOperStatus = MSDP_IF_OPERSTATUS_UP;
    MsdpFsMsdpRPTableCreateApi (&Rp);

    CacheAdvtMarker.OrgRpAddr.u1Afi = 1;
    CacheAdvtMarker.OrgRpAddr.u1AddrLen = 4;
    MsdCacheSAInfoBatchAdvt (&CacheAdvtMarker);

    CacheAdvtMarker.OrgRpAddr.u1Afi = 2;
    CacheAdvtMarker.OrgRpAddr.u1AddrLen = 4;
    MsdCacheSAInfoBatchAdvt (&CacheAdvtMarker);
    return OSIX_SUCCESS;
}

INT4
MsdpUtCache_18 (VOID)
{
    tMsdpCacheUpdt      MsdpCache;
    UINT2               u2ReadOffset = 0;
    UINT1               au1Msg[100];

    MsdpUtDelEntry ();
    u2ReadOffset = MSDP_PEER_MAX_RX_TX_BUF_LEN - MSDP_V4_SA_ADVT_ONE_ENTRY_SIZE;
    au1Msg[u2ReadOffset + 3] = 31;
    MsdCacheConstructSAEntry (1, &u2ReadOffset, &MsdpCache, au1Msg);

    u2ReadOffset = MSDP_PEER_MAX_RX_TX_BUF_LEN - MSDP_V4_SA_ADVT_ONE_ENTRY_SIZE;
    au1Msg[u2ReadOffset + 3] = 32;
    MsdCacheConstructSAEntry (1, &u2ReadOffset, &MsdpCache, au1Msg);

    u2ReadOffset = MSDP_PEER_MAX_RX_TX_BUF_LEN -
        2 * MSDP_V4_SA_ADVT_ONE_ENTRY_SIZE;
    au1Msg[u2ReadOffset + 3] = 32;
    MsdCacheConstructSAEntry (1, &u2ReadOffset, &MsdpCache, au1Msg);

    u2ReadOffset = MSDP_PEER_MAX_RX_TX_BUF_LEN -
        2 * MSDP_V6_SA_ADVT_ONE_ENTRY_SIZE;
    au1Msg[u2ReadOffset + 3] = 31;
    MsdCacheConstructSAEntry (2, &u2ReadOffset, &MsdpCache, au1Msg);

    u2ReadOffset = MSDP_PEER_MAX_RX_TX_BUF_LEN - MSDP_V6_SA_ADVT_ONE_ENTRY_SIZE;
    au1Msg[u2ReadOffset + 3] = 128;
    MsdCacheConstructSAEntry (2, &u2ReadOffset, &MsdpCache, au1Msg);

    u2ReadOffset = MSDP_PEER_MAX_RX_TX_BUF_LEN -
        2 * MSDP_V6_SA_ADVT_ONE_ENTRY_SIZE;
    au1Msg[u2ReadOffset + 3] = 128;
    MsdCacheConstructSAEntry (2, &u2ReadOffset, &MsdpCache, au1Msg);

    return OSIX_SUCCESS;
}

INT4
MsdpUtCache_19 (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pHead[5000];
    tMsdpMrpSaAdvt      MsdpSAInfo;
    tMsdpFsMsdpPeerEntry Peer;
    tMsdpSaAdvtHdr      SaAdvtHdr;
    tMsdpSaAdvtEntry    SaAdvtEntry;
    INT4                i4Index = 0;
    INT4                i4Offset = 0;
    INT4                grp, src;
    UINT1               au1RxBuf[1500];
    UINT1               au1TxBuf[10];

    MsdpUtDelEntry ();
    Peer.TxBufInfo.pu1Msg = au1TxBuf;
    Peer.RxBufInfo.pu1Msg = au1RxBuf;

    SaAdvtHdr.u1Type = MSDP_TLV_SA;
    SaAdvtHdr.u1EntryCnt = 1;
    SaAdvtEntry.u1Prefix = 31;
    MEMSET (SaAdvtEntry.au1GrpAddr, 223, 4);
    MEMSET (SaAdvtEntry.au1SrcAddr, 12, 4);
    MEMCPY (&au1RxBuf[8], (UINT1 *) &SaAdvtEntry, sizeof (tMsdpSaAdvtEntry));
    MsdCacheProcessSAMsg (&Peer, &SaAdvtHdr, 8, 1);
    MsdpUtDelEntry ();

    SaAdvtHdr.u1Type = MSDP_TLV_SA_RESPONSE;
    SaAdvtHdr.u2Len = 32;
    SaAdvtHdr.u1EntryCnt++;
    MEMSET (&SaAdvtEntry, 0, sizeof (tMsdpSaAdvtEntry));
    SaAdvtEntry.u1Prefix = 32;
    MEMSET (SaAdvtEntry.au1GrpAddr, 224, 4);
    MEMSET (SaAdvtEntry.au1SrcAddr, 13, 4);
    MEMCPY (&au1RxBuf[20], (UINT1 *) &SaAdvtEntry, sizeof (tMsdpSaAdvtEntry));
    MsdCacheProcessSAMsg (&Peer, &SaAdvtHdr, 8, 1);
    MsdpUtDelEntry ();

    for (i4Index = 0; i4Index < 4000; i4Index++)
    {
        MsdpPimIfAllocateBuf (&MsdpSAInfo, 1048, 1);
        if (NULL == (pHead[i4Index] = MsdpSAInfo.pGrpSrcAddrList))
            break;
    }
    MsdCacheProcessSAMsg (&Peer, &SaAdvtHdr, 8, 1);
    for (; i4Index >= 0; i4Index--)
    {

        CRU_BUF_Release_MsgBufChain (pHead[i4Index], OSIX_FALSE);
    }

    SaAdvtHdr.u1Type = MSDP_TLV_SA_RESPONSE;
    SaAdvtHdr.u2Len = 44;
    SaAdvtHdr.u1EntryCnt++;
    MEMSET (&SaAdvtEntry, 0, sizeof (tMsdpSaAdvtEntry));
    SaAdvtEntry.u1Prefix = 32;
    MEMSET (SaAdvtEntry.au1GrpAddr, 224, 4);
    MEMSET (SaAdvtEntry.au1SrcAddr, 13, 4);
    MEMCPY (&au1RxBuf[32], (UINT1 *) &SaAdvtEntry, sizeof (tMsdpSaAdvtEntry));
    MsdCacheProcessSAMsg (&Peer, &SaAdvtHdr, 8, 1);
    MsdpUtDelEntry ();

    SaAdvtHdr.u1Type = MSDP_TLV_SA_RESPONSE;
    SaAdvtHdr.u2Len = 65;
    SaAdvtHdr.u1EntryCnt++;
    MEMSET (&SaAdvtEntry, 0, sizeof (tMsdpSaAdvtEntry));
    SaAdvtEntry.u1Prefix = 32;
    MEMSET (SaAdvtEntry.au1GrpAddr, 224, 4);
    MEMSET (SaAdvtEntry.au1SrcAddr, 13, 4);
    MEMCPY (&au1RxBuf[32], (UINT1 *) &SaAdvtEntry, sizeof (tMsdpSaAdvtEntry));
    MsdCacheProcessSAMsg (&Peer, &SaAdvtHdr, 8, 1);
    MsdpUtDelEntry ();

    i4Index = 8;
    SaAdvtHdr.u2Len = 18;
    grp = 11;
    src = 1;
    for (i4Offset = 0; i4Offset < (MSDP_MAX_GRPSRC_BUF_SIZE + 100);
         i4Offset += (MSDP_TWO * sizeof (tIPvXAddr)))
    {
        MEMSET (&SaAdvtEntry, 0, sizeof (tMsdpSaAdvtEntry));
        SaAdvtHdr.u1EntryCnt++;
        SaAdvtEntry.u1Prefix = 32;
        MEMSET (SaAdvtEntry.au1GrpAddr, grp++, 4);
        MEMSET (SaAdvtEntry.au1SrcAddr, src++, 4);
        MEMCPY (&au1RxBuf[i4Index], (UINT1 *) &SaAdvtEntry,
                sizeof (tMsdpSaAdvtEntry));
        i4Index += sizeof (tMsdpSaAdvtEntry);
        SaAdvtHdr.u2Len += sizeof (tMsdpSaAdvtEntry);

    }

    MsdCacheProcessSAMsg (&Peer, &SaAdvtHdr, 8, 1);
    MsdpUtDelEntry ();

    MsdCacheProcessSAMsg (&Peer, &SaAdvtHdr, 8, 2);
    MsdpUtDelEntry ();
    return OSIX_SUCCESS;
}

#define MSDP_MY_COVERAGE_WANTED 1
#ifdef MSDP_MY_COVERAGE_WANTED
INT4
MsdpUtCliCache_1 (VOID)
{
    tMsdpFsMsdpSACacheEntry Cache;
    tMsdpFsMsdpPeerEntry Peer;
    tMsdpFsMsdpSACacheEntry *pCache;

    MEMSET (&Peer, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&Cache, 0, sizeof (tMsdpFsMsdpSACacheEntry));

    Cache.MibObject.i4FsMsdpSACacheAddrType = 1;
    Cache.MibObject.i4FsMsdpSACacheGroupAddrLen = 4;
    Cache.MibObject.i4FsMsdpSACacheSourceAddrLen = 4;
    Cache.MibObject.i4FsMsdpSACacheOriginRPLen = 4;
    Cache.MibObject.au1FsMsdpSACacheGroupAddr[0] = 10;
    Cache.MibObject.au1FsMsdpSACacheGroupAddr[3] = 10;
    Cache.MibObject.au1FsMsdpSACacheSourceAddr[0] = 10;
    Cache.MibObject.au1FsMsdpSACacheSourceAddr[3] = 5;
    Cache.MibObject.au1FsMsdpSACacheOriginRP[0] = 10;
    Cache.MibObject.au1FsMsdpSACacheOriginRP[3] = 1;
    pCache = MsdpFsMsdpSACacheTableCreateApi (&Cache);

    MsdpOutSendResidualMsgtoPeer (&Peer);
    Peer.TxBufInfo.u2MsgOffset = MSDP_PEER_MAX_RX_TX_BUF_LEN / 2;
    Peer.TxBufInfo.u2MsgSize = MSDP_PEER_MAX_RX_TX_BUF_LEN / 2;
    MsdpOutSendResidualMsgtoPeer (&Peer);

    MsdpUtDelEntry ();

    gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv4AdminStat = MSDP_ENABLED;
    gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv6AdminStat = MSDP_ENABLED;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    MSDP_UNLOCK;
    CliExecuteAppCmd ("show ip msdp count");
    CliExecuteAppCmd ("show ipv6 msdp count");
    CliExecuteAppCmd ("show ip msdp sa-cache 10.0.0.5");
    CliExecuteAppCmd ("show ip msdp sa-cache 5.0.0.10");
    CliExecuteAppCmd ("show ipv6 msdp sa-cache 1212::1212");
    CliExecuteAppCmd ("end;c t");

    CliExecuteAppCmd ("int v 1");
    CliExecuteAppCmd ("ipv6 en");
    CliExecuteAppCmd ("ipv6 address 2222::3333 64");
    CliExecuteAppCmd ("exit");

    sleep (5);
    CliExecuteAppCmd ("ipv6 msdp peer 1212::1212 c v 1");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("show ipv6 msdp peer 1212::1212");
    CliExecuteAppCmd ("show running-config msdp");
    CliExecuteAppCmd ("show running-config msdpv6");
    CliExecuteAppCmd ("c t;ipv6 msdp timer 1212::1212 600;end");
    CliExecuteAppCmd ("c t;ipv6 msdp keepalive 1212::1212 600 75;end");
    CliExecuteAppCmd ("show running-config msdp");
    CliExecuteAppCmd ("show running-config msdpv6");
    CliExecuteAppCmd ("c t;ipv6 msdp keepalive 1212::1212 600 800;end");
    CliExecuteAppCmd ("c t;ipv6 msdp ttl-threshold 1212::1212 25;end");
    CliExecuteAppCmd ("show running-config msdp");
    CliExecuteAppCmd ("show running-config msdpv6");
    /*CliExecuteAppCmd ("c t;ipv6 msd shutdown 1212::1212;end"); */
    CliExecuteAppCmd ("show running-config msdp");
    CliExecuteAppCmd ("show running-config msdpv6");
    CliExecuteAppCmd ("show ip msdp summary");
    CliExecuteAppCmd ("show ipv6 msdp summary");
    CliExecuteAppCmd ("c t;ipv6 msdp peer 1212::1212 c v 1;end");
    MSDP_LOCK;
    MGMT_LOCK ();
    CliGiveAppContext ();
    MsdpUtDelEntry ();

    Cache.MibObject.i4FsMsdpSACacheAddrType = 2;
    Cache.MibObject.i4FsMsdpSACacheGroupAddrLen = 16;
    Cache.MibObject.i4FsMsdpSACacheSourceAddrLen = 16;
    Cache.MibObject.i4FsMsdpSACacheOriginRPLen = 16;
    Cache.MibObject.au1FsMsdpSACacheGroupAddr[0] = 10;
    Cache.MibObject.au1FsMsdpSACacheGroupAddr[15] = 10;
    Cache.MibObject.au1FsMsdpSACacheSourceAddr[0] = 10;
    Cache.MibObject.au1FsMsdpSACacheSourceAddr[15] = 5;
    Cache.MibObject.au1FsMsdpSACacheOriginRP[0] = 10;
    Cache.MibObject.au1FsMsdpSACacheOriginRP[15] = 1;
    pCache = MsdpFsMsdpSACacheTableCreateApi (&Cache);

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    MSDP_UNLOCK;
    CliExecuteAppCmd ("show ip msdp count");
    CliExecuteAppCmd ("show ipv6 msdp count");
    CliExecuteAppCmd ("show ip msdp sa-cache 10.0.0.5");
    CliExecuteAppCmd ("show ip msdp sa-cache 5.0.0.10");
    CliExecuteAppCmd ("show ipv6 msdp sa-cache 1212::1212");
    CliExecuteAppCmd ("end");
    MSDP_LOCK;
    MGMT_LOCK ();
    CliGiveAppContext ();
    MsdpUtDelEntry ();
    return OSIX_SUCCESS;
}

INT4
MsdpUtCliCache_2 (VOID)
{
    UINT1               au1Array[200];
    UINT1              *pu1SrcAddr = NULL;
    UINT1              *pu1GrpAddr = NULL;
    tMsdpFsMsdpSACacheEntry Cache;
    tMsdpFsMsdpSACacheEntry *pCache;
    tMsdpFsMsdpSARedistributionEntry Redist;
    tMsdpFsMsdpPeerEntry Peer;
    tMsdpCacheUpdt      CacheUpd;
    INT4                i4RetVal = 0;

    MEMSET (&Cache, 0, sizeof (tMsdpFsMsdpSACacheEntry));
    MEMSET (&Peer, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&CacheUpd, 0, sizeof (CacheUpd));
    pu1SrcAddr = au1Array;
    pu1GrpAddr = &(au1Array[100]);

    Peer.RxBufInfo.u2MsgSize = MSDP_MIN_IPV6_SA_ADVT_SIZE;
    Peer.RxBufInfo.u2MsgOffset = 10;
    Peer.RxBufInfo.pu1Msg = (UINT1 *) &au1Array;
    MsdpInProcessSaRequestPkt (&Peer, 3);
    ((tMsdpSaAdvtHdr *) Peer.RxBufInfo.pu1Msg)->u1Type = MSDP_TLV_SA_RESPONSE;
    MsdpInHandleSaAdvtOrRespPkt (&Peer, 2);

    Cache.MibObject.i4FsMsdpSACacheAddrType = 1;
    Cache.MibObject.i4FsMsdpSACacheGroupAddrLen = 4;
    Cache.MibObject.i4FsMsdpSACacheSourceAddrLen = 4;
    Cache.MibObject.i4FsMsdpSACacheOriginRPLen = 4;
    Cache.MibObject.au1FsMsdpSACacheGroupAddr[0] = 224;
    Cache.MibObject.au1FsMsdpSACacheGroupAddr[3] = 1;
    Cache.MibObject.au1FsMsdpSACacheSourceAddr[0] = 10;
    Cache.MibObject.au1FsMsdpSACacheSourceAddr[3] = 5;
    Cache.MibObject.au1FsMsdpSACacheOriginRP[0] = 10;
    Cache.MibObject.au1FsMsdpSACacheOriginRP[3] = 1;
    pCache = MsdpFsMsdpSACacheTableCreateApi (&Cache);

    MsdpCliShowSACount ((tCliHandle) au1Array, IPVX_ADDR_FMLY_IPV6);

    Cache.MibObject.i4FsMsdpSACacheAddrType = 1;
    Cache.MibObject.i4FsMsdpSACacheGroupAddrLen = 4;
    Cache.MibObject.i4FsMsdpSACacheSourceAddrLen = 4;
    Cache.MibObject.i4FsMsdpSACacheOriginRPLen = 4;
    Cache.MibObject.au1FsMsdpSACacheGroupAddr[0] = 225;
    Cache.MibObject.au1FsMsdpSACacheGroupAddr[3] = 2;
    Cache.MibObject.au1FsMsdpSACacheSourceAddr[0] = 20;
    Cache.MibObject.au1FsMsdpSACacheSourceAddr[3] = 15;
    Cache.MibObject.au1FsMsdpSACacheOriginRP[0] = 20;
    Cache.MibObject.au1FsMsdpSACacheOriginRP[3] = 10;
    pCache = MsdpFsMsdpSACacheTableCreateApi (&Cache);

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    MSDP_UNLOCK;
    CliExecuteAppCmd ("show ip msdp count");
    CliExecuteAppCmd ("show ipv6 msdp count");
    CliExecuteAppCmd ("show ip msdp sa-cache");
    CliExecuteAppCmd ("show ipv6 msdp sa-cache");
    CliExecuteAppCmd ("show ip msdp sa-cache 225.0.0.2");
    CliExecuteAppCmd ("show ip msdp sa-cache 227.0.0.2");
    CliExecuteAppCmd ("show ip msdp sa-cache 2.0.0.225");
    CliExecuteAppCmd ("show ip msdp sa-cache 10.0.0.5");
    CliExecuteAppCmd ("show ip msdp sa-cache 5.0.0.10");
    CliExecuteAppCmd ("show ipv6 msdp sa-cache 1212::1212");
    CliExecuteAppCmd ("show ipv6 msdp sa-cache ff00::1212");
    MSDP_LOCK;
    MGMT_LOCK ();
    CliGiveAppContext ();
    MEMCPY (pu1SrcAddr, "20.0.0.15", sizeof ("20.0.0.15"));
    MEMCPY (pu1GrpAddr, "225.0.0.2", sizeof ("225.0.0.2"));
    MsdpCliShowCache ((tCliHandle) au1Array, IPVX_ADDR_FMLY_IPV4, pu1SrcAddr,
                      pu1GrpAddr);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    MSDP_UNLOCK;
    CliExecuteAppCmd
        ("c t;clear ip msdp sa-cache;clear ipv6 msdp sa-cache;end");
    CliExecuteAppCmd ("c t;clear ip msdp peer;clear ipv6 msdp peer;end");
    CliExecuteAppCmd ("show ip msdp rpf-peer 12.2");
    MSDP_LOCK;
    MGMT_LOCK ();
    CliGiveAppContext ();

    MsdpUtDelEntry ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    MSDP_UNLOCK;
    CliExecuteAppCmd ("c t;ipv6 ms en");
    CliExecuteAppCmd ("ipv6 ms me mesh 1234::1234;en");
    CliExecuteAppCmd ("show running-config msdp");
    CliExecuteAppCmd ("show running-config msdpv6");
    CliExecuteAppCmd ("end");
    MSDP_LOCK;
    MGMT_LOCK ();
    CliGiveAppContext ();
    i4RetVal = MsdpCliGetOriginatorId (3, (CHR1 *) & i4RetVal);
    i4RetVal =
        MsdpCliConvertIpvxToStr ((UINT1 *) &i4RetVal, 3, (UINT1 *) &i4RetVal);
    MsdpUtDelEntry ();

    MEMSET (&Peer, 0, sizeof (Peer));
    Peer.MibObject.i4FsMsdpPeerAddrType = 3;
    Peer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 10;
    Peer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 3;
    Peer.MibObject.i4FsMsdpPeerRemoteAddressLen = 4;
    Peer.MibObject.i4FsMsdpPeerStatus = ACTIVE;
    Peer.MibObject.i4FsMsdpPeerConnectRetryInterval = 4;
    Peer.MibObject.i4FsMsdpPeerHoldTimeConfigured = 4;
    Peer.MibObject.i4FsMsdpPeerKeepAliveConfigured = 4;
    Peer.MibObject.i4FsMsdpPeerDataTtl = 4;

    MsdpFsMsdpPeerTableCreateApi (&Peer);
    i4RetVal = MsdpShowRunningConfigPeerTable ((tCliHandle) au1Array, 3);
    MsdpUtDelEntry ();
    Peer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 4;
    Peer.MibObject.i4FsMsdpPeerStatus = NOT_IN_SERVICE;
    MsdpFsMsdpPeerTableCreateApi (&Peer);
    i4RetVal = MsdpShowRunningConfigPeerTable ((tCliHandle) au1Array, 3);
    MsdpUtDelEntry ();
    Peer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 5;
    Peer.MibObject.i4FsMsdpPeerStatus = NOT_READY;
    MsdpFsMsdpPeerTableCreateApi (&Peer);
    i4RetVal = MsdpShowRunningConfigPeerTable ((tCliHandle) au1Array, 3);

    MsdpUtDelEntry ();

    Peer.MibObject.i4FsMsdpPeerAddrType = 1;
    Peer.MibObject.i4FsMsdpPeerState = MSDP_PEER_ESTABLISHED;
    MsdpFsMsdpPeerTableCreateApi (&Peer);
    i4RetVal = MsdpCliGetPeerStatistics (IPVX_ADDR_FMLY_IPV4, &i4RetVal,
                                         &i4RetVal, &i4RetVal);
    MsdpUtDelEntry ();

    Redist.MibObject.i4FsMsdpSARedistributionAddrType = IPVX_ADDR_FMLY_IPV4;
    Redist.MibObject.i4FsMsdpSARedistributionStatus = ACTIVE;
    Redist.MibObject.i4FsMsdpSARedistributionRouteMapStat = MSDP_DISABLED;
    Redist.MibObject.i4FsMsdpSARedistributionRouteMapLen = sizeof ("hey");
    MEMCPY (Redist.MibObject.au1FsMsdpSARedistributionRouteMap, "hey",
            sizeof ("hey"));
    MsdpFsMsdpSARedistributionTableCreateApi (&Redist);
    CacheUpd.u1AddrType = IPVX_ADDR_FMLY_IPV4;
    MsdCacheApplySAFilter (&CacheUpd);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    MSDP_UNLOCK;
    CliExecuteAppCmd ("c t;ip ms en;ip msdp peer 10.6 c v 1;end");
    CliExecuteAppCmd ("show ip msdp peer 10.6");
    MSDP_LOCK;
    MGMT_LOCK ();
    CliGiveAppContext ();

    MsdpCliClearPeer ((tCliHandle) & Cache, IPVX_ADDR_FMLY_IPV6, au1Array);
    MsdpUtDelEntry ();

    Redist.MibObject.i4FsMsdpSARedistributionRouteMapStat = MSDP_ENABLED;
    MsdpFsMsdpSARedistributionTableCreateApi (&Redist);
    MsdCacheApplySAFilter (&CacheUpd);
    MsdpUtDelEntry ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    MSDP_UNLOCK;
    CliExecuteAppCmd ("c t;ip ms en;ipv6 ms en;ip ms me mesh 12.1");
    CliExecuteAppCmd ("int v 1;ipv6 en;ipv6 addr 2222::3333 64");
    sleep (5);
    CliExecuteAppCmd ("ex;ipv6 msdp peer 1212::1212 c v 1;en");
    CliExecuteAppCmd ("show ipv6 msdp peer 1212::1212");
    CliExecuteAppCmd ("c t;no ip msdp mesh mesh 12.1");
    CliExecuteAppCmd ("end");
    MSDP_LOCK;
    MGMT_LOCK ();
    CliGiveAppContext ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    MSDP_UNLOCK;
    CliExecuteAppCmd ("c t;ip ms en;ipv6 ms en;ip ms me mesh 12.1");
    CliExecuteAppCmd ("int v 1;ipv6 en;ipv6 addr 2222::3333 64");
    sleep (5);
    CliExecuteAppCmd ("ex;ipv6 msdp peer 1212::1212 c v 1;en");
    CliExecuteAppCmd ("show ipv6 msdp peer 1212::1212");
    CliExecuteAppCmd ("end");
    MSDP_LOCK;
    MGMT_LOCK ();
    CliGiveAppContext ();
    MsdpUtDelEntry ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    MSDP_UNLOCK;
    CliExecuteAppCmd ("c t;ip ms en;ip msdp peer-filter deny-all routemap abd");
    CliExecuteAppCmd ("ipv6 msdp peer-filter deny-all routemap asd;end");
    CliExecuteAppCmd ("show running-config msdp");
    CliExecuteAppCmd ("show running-config msdpv6");
    MSDP_LOCK;
    MGMT_LOCK ();
    CliGiveAppContext ();
    MsdpUtDelEntry ();
    return OSIX_SUCCESS;
}

INT4
MsdpUtCliCache_3 (VOID)
{
    UINT1               au1RtMap[20];
    tMsdpFsMsdpSACacheEntry Cache;
    tMsdpFsMsdpMeshGroupEntry Mesh;
    tMsdpFsMsdpPeerFilterEntry PeerFilt;
    tMsdpFsMsdpSARedistributionEntry SARedist;
    tIPvXAddr           PeerAddr;

    Cache.MibObject.i4FsMsdpSACacheAddrType = 2;
    Cache.MibObject.i4FsMsdpSACacheStatus = CREATE_AND_GO;
    Cache.MibObject.i4FsMsdpSACacheGroupAddrLen = 16;
    Cache.MibObject.i4FsMsdpSACacheSourceAddrLen = 16;
    Cache.MibObject.i4FsMsdpSACacheOriginRPLen = 16;
    Cache.MibObject.au1FsMsdpSACacheGroupAddr[0] = 225;
    Cache.MibObject.au1FsMsdpSACacheGroupAddr[15] = 2;
    Cache.MibObject.au1FsMsdpSACacheSourceAddr[0] = 20;
    Cache.MibObject.au1FsMsdpSACacheSourceAddr[15] = 15;
    Cache.MibObject.au1FsMsdpSACacheOriginRP[0] = 20;
    Cache.MibObject.au1FsMsdpSACacheOriginRP[15] = 10;
    MsdpFsMsdpSACacheTableCreateApi (&Cache);
    MsdpCliShowSACount ((tCliHandle) & Cache, IPVX_ADDR_FMLY_IPV4);
    MsdpCliClearCache (IPVX_ADDR_FMLY_IPV4,
                       Cache.MibObject.au1FsMsdpSACacheGroupAddr);
    MsdpCliClearCache (IPVX_ADDR_FMLY_IPV6,
                       Cache.MibObject.au1FsMsdpSACacheGroupAddr);
    Cache.MibObject.au1FsMsdpSACacheGroupAddr[3] = 2;
    MsdpCliClearCache (IPVX_ADDR_FMLY_IPV4,
                       Cache.MibObject.au1FsMsdpSACacheGroupAddr);
    MsdpUtDelEntry ();

    Mesh.MibObject.i4FsMsdpMeshGroupAddrType = IPVX_ADDR_FMLY_IPV4;
    Mesh.MibObject.i4FsMsdpMeshGroupStatus = DISABLED;
    Mesh.MibObject.i4FsMsdpMeshGroupNameLen = sizeof ("meshy");
    Mesh.MibObject.i4FsMsdpMeshGroupPeerAddressLen = IPVX_IPV4_ADDR_LEN;
    MEMCPY (Mesh.MibObject.au1FsMsdpMeshGroupName, "meshy", sizeof ("meshy"));
    MEMCPY (Mesh.MibObject.au1FsMsdpMeshGroupPeerAddress, "15.0.0.3",
            IPVX_IPV4_ADDR_LEN);
    MsdpFsMsdpMeshGroupTableCreateApi (&Mesh);
    MsdpShowRunningConfigMeshGrpTbl ((tCliHandle) & Mesh, IPVX_ADDR_FMLY_IPV4);
    MsdpUtDelEntry ();

    PeerFilt.MibObject.i4FsMsdpPeerFilterAddrType = IPVX_ADDR_FMLY_IPV4;
    PeerFilt.MibObject.i4FsMsdpPeerFilterStatus = DISABLED;
    PeerFilt.MibObject.i4FsMsdpPeerFilterRouteMapLen = sizeof ("peerfilt");
    MEMCPY (PeerFilt.MibObject.au1FsMsdpPeerFilterRouteMap, "peerfilt",
            sizeof ("peerfilt"));
    MsdpFsMsdpPeerFilterTableCreateApi (&PeerFilt);
    MsdpPeerChkPeerFilterStatus (&PeerAddr, IPVX_ADDR_FMLY_IPV4);
    MsdpShowRunningConfigPeerFilter ((tCliHandle) & PeerFilt,
                                     IPVX_ADDR_FMLY_IPV4);
    MsdpUtDelEntry ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    MSDP_UNLOCK;
    CliExecuteAppCmd ("c t;ip ms en;ipv6 msdp en;ip ms redist;ipv6 ms redist");
    CliExecuteAppCmd ("end");
    MSDP_LOCK;
    MGMT_LOCK ();
    CliGiveAppContext ();
    MsdpShowRunningConfigSAFilter ((tCliHandle) & Mesh, IPVX_ADDR_FMLY_IPV4);
    MsdpShowRunningConfigSAFilter ((tCliHandle) & Mesh, IPVX_ADDR_FMLY_IPV6);
    MsdpUtDelEntry ();

    SARedist.MibObject.i4FsMsdpSARedistributionAddrType = IPVX_ADDR_FMLY_IPV4;
    SARedist.MibObject.i4FsMsdpSARedistributionStatus = DISABLED;
    SARedist.MibObject.i4FsMsdpSARedistributionRouteMapStat = MSDP_DISABLED;
    SARedist.MibObject.i4FsMsdpSARedistributionRouteMapLen = sizeof ("route");
    MEMCPY (SARedist.MibObject.au1FsMsdpSARedistributionRouteMap, "route",
            sizeof ("route"));
    MsdpFsMsdpSARedistributionTableCreateApi (&SARedist);
    MsdpShowRunningConfigSAFilter ((tCliHandle) & Mesh, IPVX_ADDR_FMLY_IPV4);
    MsdpShowRunningConfigSAFilter ((tCliHandle) & Mesh, IPVX_ADDR_FMLY_IPV6);
    MsdpUtDelEntry ();

    SARedist.MibObject.i4FsMsdpSARedistributionStatus = ENABLED;
    MsdpFsMsdpSARedistributionTableCreateApi (&SARedist);
    MsdpShowRunningConfigSAFilter ((tCliHandle) & Mesh, IPVX_ADDR_FMLY_IPV4);
    MsdpShowRunningConfigSAFilter ((tCliHandle) & Mesh, IPVX_ADDR_FMLY_IPV6);
    MsdpUtDelEntry ();

    SARedist.MibObject.i4FsMsdpSARedistributionAddrType = IPVX_ADDR_FMLY_IPV6;
    MsdpFsMsdpSARedistributionTableCreateApi (&SARedist);
    MsdpShowRunningConfigSAFilter ((tCliHandle) & Mesh, IPVX_ADDR_FMLY_IPV4);
    MsdpShowRunningConfigSAFilter ((tCliHandle) & Mesh, IPVX_ADDR_FMLY_IPV6);
    MsdpUtDelEntry ();

    MEMSET (au1RtMap, 0, sizeof (au1RtMap));
    au1RtMap[0] = 10;
    au1RtMap[3] = 10;

    IPVX_ADDR_INIT (PeerAddr, IPVX_ADDR_FMLY_IPV4, au1RtMap);
    MsdpPortPeerRMapRule (&PeerAddr, au1RtMap);

    MsdpPortSARtMapRule (&PeerAddr, &PeerAddr, au1RtMap);
    MsdpUtDelEntry ();

    gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv4AdminStat = MSDP_DISABLED;
    gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv6AdminStat = MSDP_ENABLED;
    MsdpCliShowSummary ((tCliHandle) au1RtMap, IPVX_ADDR_FMLY_IPV4);
    gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv4AdminStat = MSDP_ENABLED;

    return OSIX_SUCCESS;
}

INT4
MsdpUtCliCache_4 (VOID)
{
    tMsdpFsMsdpSACacheEntry Cache;
    tMsdpFsMsdpPeerEntry PeerEntry;
    tMsdpFsMsdpRPEntry  RPEntry;
    tMsdpMrpSaAdvt      PimSaInfo;
    tMsdpSAReqGrp       SaReqGrp;
    tMsdpCacheUpdt      CacheUpd;

    MEMSET (&RPEntry, 0, sizeof (RPEntry));
    MEMSET (&Cache, 0, sizeof (Cache));
    MEMSET (&PeerEntry, 0, sizeof (PeerEntry));
    MEMSET (&PimSaInfo, 0, sizeof (PimSaInfo));
    MEMSET (&SaReqGrp, 0, sizeof (SaReqGrp));
    MEMSET (&CacheUpd, 0, sizeof (CacheUpd));
    RPEntry.MibObject.i4FsMsdpRPAddrType = IPVX_ADDR_FMLY_IPV4;
    RPEntry.MibObject.i4FsMsdpRPStatus = CREATE_AND_WAIT;
    RPEntry.MibObject.i4FsMsdpRPAddrType = IPVX_IPV4_ADDR_LEN;
    RPEntry.MibObject.au1FsMsdpRPAddress[0] = 10;
    RPEntry.MibObject.au1FsMsdpRPAddress[3] = 10;
    MsdpFsMsdpRPTableCreateApi (&RPEntry);
    RPEntry.MibObject.i4FsMsdpRPAddrType = IPVX_ADDR_FMLY_IPV6;
    RPEntry.MibObject.i4FsMsdpRPAddrType = IPVX_IPV6_ADDR_LEN;
    MsdpFsMsdpRPTableCreateApi (&RPEntry);
    MsdpShowRunningConfigRPTable ((tCliHandle) & RPEntry, IPVX_ADDR_FMLY_IPV4);
    MsdpShowRunningConfigRPTable ((tCliHandle) & RPEntry, IPVX_ADDR_FMLY_IPV6);

    PeerEntry.MibObject.i4FsMsdpPeerAddrType = IPVX_ADDR_FMLY_IPV4;
    PeerEntry.MibObject.i4FsMsdpPeerStatus = NOT_IN_SERVICE;
    PeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen = IPVX_IPV4_ADDR_LEN;
    PeerEntry.MibObject.i4FsMsdpPeerLocalAddressLen = IPVX_IPV4_ADDR_LEN;
    PeerEntry.MibObject.au1FsMsdpPeerRemoteAddress[0] = 10;
    PeerEntry.MibObject.au1FsMsdpPeerRemoteAddress[3] = 10;
    PeerEntry.MibObject.au1FsMsdpPeerLocalAddress[0] = 10;
    PeerEntry.MibObject.au1FsMsdpPeerLocalAddress[3] = 10;

    MsdpShowRunningConfigPeerTable ((tCliHandle) & PeerEntry,
                                    IPVX_ADDR_FMLY_IPV4);
    PeerEntry.MibObject.i4FsMsdpPeerAddrType = IPVX_ADDR_FMLY_IPV6;
    PeerEntry.MibObject.i4FsMsdpPeerStatus = NOT_IN_SERVICE;
    PeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen = IPVX_IPV6_ADDR_LEN;
    PeerEntry.MibObject.i4FsMsdpPeerLocalAddressLen = IPVX_IPV6_ADDR_LEN;
    PeerEntry.MibObject.au1FsMsdpPeerRemoteAddress[0] = 10;
    PeerEntry.MibObject.au1FsMsdpPeerRemoteAddress[3] = 10;
    PeerEntry.MibObject.au1FsMsdpPeerLocalAddress[0] = 10;
    PeerEntry.MibObject.au1FsMsdpPeerLocalAddress[3] = 10;

    MsdpFsMsdpPeerTableCreateApi (&PeerEntry);
    MsdpShowRunningConfigPeerTable ((tCliHandle) & PeerEntry,
                                    IPVX_ADDR_FMLY_IPV6);

    PeerEntry.MibObject.i4FsMsdpPeerAddrType = IPVX_ADDR_FMLY_IPV4;
    PeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen = IPVX_IPV4_ADDR_LEN;
    PeerEntry.MibObject.i4FsMsdpPeerLocalAddressLen = IPVX_IPV4_ADDR_LEN;
    MsdpFsMsdpPeerTableCreateApi (&PeerEntry);
    MsdpShowRunningConfigPeerTable ((tCliHandle) & PeerEntry,
                                    IPVX_ADDR_FMLY_IPV4);
    MsdpPeerHandleKeepAliveTmrExpiry (&PeerEntry);
    MsdpCliClearPeer ((tCliHandle) & PeerEntry, IPVX_ADDR_FMLY_IPV4,
                      PeerEntry.MibObject.au1FsMsdpPeerRemoteAddress);
    MsdpCliClearPeer ((tCliHandle) & PeerEntry, IPVX_ADDR_FMLY_IPV4,
                      PeerEntry.MibObject.au1FsMsdpPeerLocalAddress);
    MsdpUtDelEntry ();
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    MSDP_UNLOCK;
    CliExecuteAppCmd ("c t;ip ms dis;ipv6 msdp dis");
    CliExecuteAppCmd ("end;show ip msdp peer 12.6");
    MSDP_LOCK;
    MGMT_LOCK ();
    CliGiveAppContext ();

    gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv4AdminStat = MSDP_DISABLED;
    gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv6AdminStat = MSDP_ENABLED;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    MSDP_UNLOCK;
    CliExecuteAppCmd ("end;show ip msdp peer 12.6");
    CliExecuteAppCmd ("show ip msdp rpf-peer 12.6");
    CliExecuteAppCmd ("show ipv6 msdp rpf-peer 1212::1212");
    CliExecuteAppCmd ("c t;clear ipv6 msdp peer;clear ipv6 msdp sa-cache;end");
    MSDP_LOCK;
    MGMT_LOCK ();
    CliGiveAppContext ();

    gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv4AdminStat = MSDP_ENABLED;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    MSDP_UNLOCK;
    CliExecuteAppCmd ("c t;ip msdp ori 0.0");
    CliExecuteAppCmd ("ipv msdp ori ::;end");
    MSDP_LOCK;
    MGMT_LOCK ();
    CliGiveAppContext ();
    MsdpShowRunningConfigRPTable ((tCliHandle) & PeerEntry,
                                  IPVX_ADDR_FMLY_IPV4);
    MsdpShowRunningConfigRPTable ((tCliHandle) & PeerEntry,
                                  IPVX_ADDR_FMLY_IPV6);

    Cache.MibObject.i4FsMsdpSACacheAddrType = IPVX_ADDR_FMLY_IPV4;
    Cache.MibObject.i4FsMsdpSACacheStatus = CREATE_AND_GO;
    Cache.MibObject.i4FsMsdpSACacheGroupAddrLen = IPVX_IPV4_ADDR_LEN;
    Cache.MibObject.i4FsMsdpSACacheSourceAddrLen = IPVX_IPV4_ADDR_LEN;
    Cache.MibObject.i4FsMsdpSACacheOriginRPLen = IPVX_IPV4_ADDR_LEN;
    Cache.MibObject.au1FsMsdpSACacheGroupAddr[0] = 225;
    Cache.MibObject.au1FsMsdpSACacheGroupAddr[3] = 225;
    Cache.MibObject.au1FsMsdpSACacheSourceAddr[0] = 10;
    Cache.MibObject.au1FsMsdpSACacheSourceAddr[3] = 10;
    Cache.MibObject.au1FsMsdpSACacheOriginRP[0] = 10;
    Cache.MibObject.au1FsMsdpSACacheOriginRP[3] = 10;
    MsdpFsMsdpSACacheTableCreateApi (&Cache);
    Cache.MibObject.au1FsMsdpSACacheGroupAddr[3] = 223;

    MsdpCliClearCache (IPVX_ADDR_FMLY_IPV4,
                       Cache.MibObject.au1FsMsdpSACacheGroupAddr);
    MsdpUtDelEntry ();

    MEMSET (&PeerEntry, 0, sizeof (PeerEntry));
    PeerEntry.RxBufInfo.u2MsgSize = MSDP_KEEP_ALIVE_PKT_SIZE + 2;
    PeerEntry.MibObject.i4FsMsdpPeerHoldTimeConfigured = 0;
    MsdpInProcessKeepAlivePkt (&PeerEntry);

    MsdpOutChkForValidPeer (&PeerEntry, &PeerEntry);
    PeerEntry.MibObject.i4FsMsdpPeerKeepAliveConfigured = 20;
    PeerEntry.MibObject.i4FsMsdpPeerHoldTimeConfigured = 20;

    MsdpPeerFsmHandleEstablished (&PeerEntry);
    MsdpUtDelEntry ();

    MsdpGetFsMsdpMappingComponentId ((INT4 *) (VOID *) &(PimSaInfo.u1CompId));
    PimSaInfo.u1AddrType = IPVX_ADDR_FMLY_IPV4;
    SaReqGrp.u1AddrType = IPVX_ADDR_FMLY_IPV4;
    gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv4AdminStat = MSDP_DISABLED;
    MsdpPimIfHandleSAInfo (&PimSaInfo);
    MsdpPimIfHandleSAReq (&SaReqGrp);
    gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv4AdminStat = MSDP_ENABLED;
    MsdpPimIfHandleSAInfo (&PimSaInfo);
    MsdpPimIfHandleSAReq (&SaReqGrp);
    PimSaInfo.u1AddrType = IPVX_ADDR_FMLY_IPV6;
    SaReqGrp.u1AddrType = IPVX_ADDR_FMLY_IPV6;
    nmhSetFsMsdpIPv6AdminStat (MSDP_DISABLED);
    MsdpPimIfHandleSAInfo (&PimSaInfo);
    MsdpPimIfHandleSAReq (&SaReqGrp);
    gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv6AdminStat = MSDP_ENABLED;
    MsdpPimIfHandleSAInfo (&PimSaInfo);
    MsdpPimIfHandleSAReq (&SaReqGrp);

    MsdpTmrCacheStateTimerExpHdlr (&SaReqGrp);

    return OSIX_SUCCESS;
}
#else
INT4
MsdpUtCliCache_1 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
MsdpUtCliCache_2 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
MsdpUtCliCache_3 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
MsdpUtCliCache_4 (VOID)
{
    return OSIX_SUCCESS;
}
#endif
/* msdpdb UT cases */
INT4
MsdpUtDb_1 (VOID)
{
    return -1;
}

INT4
MsdpUtDb_2 (VOID)
{
    return -1;
}

INT4
MsdpUtDb_3 (VOID)
{
    return -1;
}

INT4
MsdpUtDb_4 (VOID)
{
    return -1;
}

INT4
MsdpUtDb_5 (VOID)
{
    return -1;
}

INT4
MsdpUtDb_6 (VOID)
{
    return -1;
}

INT4
MsdpUtDb_7 (VOID)
{
    return -1;
}

INT4
MsdpUtDb_8 (VOID)
{
    return -1;
}

INT4
MsdpUtDb_9 (VOID)
{
    return -1;
}

INT4
MsdpUtDb_10 (VOID)
{
    return -1;
}

INT4
MsdpUtDb_11 (VOID)
{
    return -1;
}

INT4
MsdpUtDb_12 (VOID)
{
    return -1;
}

INT4
MsdpUtDb_13 (VOID)
{
    return -1;
}

INT4
MsdpUtDb_14 (VOID)
{
    return -1;
}

INT4
MsdpUtDb_15 (VOID)
{
    return -1;
}

INT4
MsdpUtDb_16 (VOID)
{
    return -1;
}

/* msdpdbg UT cases */
INT4
MsdpUtDbg_1 (VOID)
{
    return -1;
}

INT4
MsdpUtDbg_2 (VOID)
{
    return -1;
}

INT4
MsdpUtDbg_3 (VOID)
{
    return -1;
}

INT4
MsdpUtDbg_4 (VOID)
{
    return -1;
}

INT4
MsdpUtDbg_5 (VOID)
{
    return -1;
}

INT4
MsdpUtDbg_6 (VOID)
{
    return -1;
}

INT4
MsdpUtDbg_7 (VOID)
{
    return -1;
}

INT4
MsdpUtDbg_8 (VOID)
{
    return -1;
}

INT4
MsdpUtDbg_9 (VOID)
{
    return -1;
}

INT4
MsdpUtDbg_10 (VOID)
{
    return -1;
}

INT4
MsdpUtDbg_11 (VOID)
{
    return -1;
}

INT4
MsdpUtDbg_12 (VOID)
{
    return -1;
}

/* msdpdefault UT cases */
INT4
MsdpUtDefault_1 (VOID)
{
    return -1;
}

INT4
MsdpUtDefault_2 (VOID)
{
    return -1;
}

INT4
MsdpUtDefault_3 (VOID)
{
    return -1;
}

INT4
MsdpUtDefault_4 (VOID)
{
    return -1;
}

INT4
MsdpUtDefault_5 (VOID)
{
    return -1;
}

INT4
MsdpUtDefault_6 (VOID)
{
    return -1;
}

/* msdpdefaultg UT cases */
INT4
MsdpUtDefaultg_1 (VOID)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, NULL, sizeof (tMsdpFsMsdpPeerEntry));

    if (OSIX_SUCCESS != MsdpInitializeMibFsMsdpPeerTable (&MsdpFsMsdpPeerEntry))
    {
        printf ("Init Peer Table \n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
MsdpUtDefaultg_2 (VOID)
{
    tMsdpFsMsdpSACacheEntry MsdpFsMsdpSACacheEntry;

    MsdpUtDelEntry ();
    MEMSET (&MsdpFsMsdpSACacheEntry, NULL, sizeof (tMsdpFsMsdpSACacheEntry));

    if (OSIX_SUCCESS != MsdpInitializeMibFsMsdpSACacheTable
        (&MsdpFsMsdpSACacheEntry))
    {
        printf ("Init SA Cache Table \n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
MsdpUtDefaultg_3 (VOID)
{
    tMsdpFsMsdpMeshGroupEntry MsdpFsMsdpMeshGroupEntry;

    MEMSET (&MsdpFsMsdpMeshGroupEntry, NULL,
            sizeof (tMsdpFsMsdpMeshGroupEntry));

    MsdpUtDelEntry ();
    if (OSIX_SUCCESS != MsdpInitializeMibFsMsdpMeshGroupTable
        (&MsdpFsMsdpMeshGroupEntry))
    {
        printf ("Init Mesh Group Table \n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
MsdpUtDefaultg_4 (VOID)
{
    tMsdpFsMsdpRPEntry  MsdpFsMsdpRPEntry;

    MEMSET (&MsdpFsMsdpRPEntry, NULL, sizeof (tMsdpFsMsdpRPEntry));

    MsdpUtDelEntry ();
    if (OSIX_SUCCESS != MsdpInitializeMibFsMsdpRPTable (&MsdpFsMsdpRPEntry))
    {
        printf ("Init RP Table \n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
MsdpUtDefaultg_5 (VOID)
{
    tMsdpFsMsdpPeerFilterEntry MsdpFsMsdpPeerFilterEntry;

    MEMSET (&MsdpFsMsdpPeerFilterEntry, 0, sizeof (tMsdpFsMsdpPeerFilterEntry));
    MsdpUtDelEntry ();

    if (OSIX_SUCCESS != MsdpInitializeMibFsMsdpPeerFilterTable
        (&MsdpFsMsdpPeerFilterEntry))
    {
        printf ("Init Peer Filter Table \n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
MsdpUtDefaultg_6 (VOID)
{
    tMsdpFsMsdpSARedistributionEntry MsdpFsMsdpSARedistributionEntry;

    MEMSET (&MsdpFsMsdpSARedistributionEntry, 0,
            sizeof (tMsdpFsMsdpSARedistributionEntry));
    MsdpUtDelEntry ();

    if (OSIX_SUCCESS != MsdpInitializeMibFsMsdpSARedistributionTable
        (&MsdpFsMsdpSARedistributionEntry))
    {
        printf ("Init SA Redistribute Table \n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/* msdpdsg UT cases */
INT4
MsdpUtDsg_1 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_2 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_3 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_4 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_5 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_6 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_7 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_8 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_9 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_10 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_11 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_12 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_13 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_14 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_15 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_16 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_17 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_18 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_19 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_20 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_21 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_22 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_23 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_24 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_25 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_26 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_27 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_28 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_29 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_30 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_31 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_32 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_33 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_34 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_35 (VOID)
{
    return -1;
}

INT4
MsdpUtDsg_36 (VOID)
{
    return -1;
}

/* msdppeer UT cases */
INT4
MsdpUtPeer_1 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;
    tMsdpFsMsdpSACacheEntry Cache;

    Peer.MibObject.i4FsMsdpPeerAddrType = 1;
    Peer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 11;
    Peer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 11;
    Peer.MibObject.i4FsMsdpPeerRemoteAddressLen = 4;
    MsdpPeerDestroyCacheFromPeer (&Peer);

    Cache.MibObject.i4FsMsdpSACacheAddrType = 1;
    Cache.MibObject.i4FsMsdpSACacheGroupAddrLen = 4;
    Cache.MibObject.i4FsMsdpSACacheSourceAddrLen = 4;
    Cache.MibObject.i4FsMsdpSACacheOriginRPLen = 4;
    Cache.MibObject.au1FsMsdpSACachePeerLearnedFrom[0] = 10;
    Cache.MibObject.au1FsMsdpSACachePeerLearnedFrom[3] = 10;
    Cache.MibObject.i4FsMsdpSACachePeerLearnedFromLen = 4;
    MsdpFsMsdpSACacheTableCreateApi (&Cache);

    Peer.MibObject.i4FsMsdpPeerAddrType = 2;
    MsdpPeerDestroyCacheFromPeer (&Peer);

    Peer.MibObject.i4FsMsdpPeerAddrType = 1;
    MsdpPeerDestroyCacheFromPeer (&Peer);

    Peer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 10;
    Peer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 10;
    MsdpPeerDestroyCacheFromPeer (&Peer);

    return OSIX_SUCCESS;
}

INT4
MsdpUtPeer_2 (VOID)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    MEMSET (&MsdpFsMsdpPeerEntry, NULL, sizeof (tMsdpFsMsdpPeerEntry));

    MsdpUtDelEntry ();
    MsdpPeerRunPfsm (NULL, 1);

    MsdpPeerRunPfsm (&MsdpFsMsdpPeerEntry, 1);

    MsdpPeerRunPfsm (&MsdpFsMsdpPeerEntry, MSDP_MAX_PEER_EVENT + 1);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerState = MSDP_MAX_PEER_STATE + 1;

    MsdpPeerRunPfsm (&MsdpFsMsdpPeerEntry, 1);

    MsdpPeerRunPfsm (&MsdpFsMsdpPeerEntry, (MSDP_MAX_PEER_EVENT + 1));

    return OSIX_SUCCESS;
}

INT4
MsdpUtPeer_3 (VOID)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    MEMSET (&MsdpFsMsdpPeerEntry, NULL, sizeof (tMsdpFsMsdpPeerEntry));
    MsdpUtDelEntry ();

    MsdpPeerFsmInvalid (&MsdpFsMsdpPeerEntry);

    return OSIX_SUCCESS;
}

INT4
MsdpUtPeer_4 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;

    Peer.MibObject.i4FsMsdpPeerAddrType = 1;
    Peer.MibObject.au1FsMsdpPeerLocalAddress[0] = 10;
    Peer.MibObject.au1FsMsdpPeerLocalAddress[3] = 10;
    Peer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 9;
    Peer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 9;
    MsdpUtDelEntry ();
    MsdpPeerHdlAdminStatusEnable (&Peer);

    Peer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 11;
    Peer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 11;
    MsdpPeerHdlAdminStatusEnable (&Peer);
    return OSIX_SUCCESS;
}

INT4
MsdpUtPeer_5 (VOID)
{
    tMsdpConfigPeer     ConfigPeer;
    tMsdpFsMsdpPeerEntry Peer;

    ConfigPeer.PeerAddr.au1Addr[0] = 10;
    ConfigPeer.PeerAddr.au1Addr[3] = 10;
    ConfigPeer.PeerAddr.u1Afi = 1;
    ConfigPeer.PeerAddr.u1AddrLen = 4;
    ConfigPeer.u1AddrType = 1;
    MsdpUtDelEntry ();
    MsdpPeerHdlAdminStatusConfig (&ConfigPeer);

    Peer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 10;
    Peer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 10;
    Peer.MibObject.i4FsMsdpPeerAddrType = 1;
    Peer.MibObject.i4FsMsdpPeerRemoteAddressLen = 4;
    MsdpFsMsdpPeerTableCreateApi (&Peer);
    ConfigPeer.u1PeerAdmin = MSDP_PEER_ADMIN_ENABLE;
    MsdpPeerHdlAdminStatusConfig (&ConfigPeer);

    ConfigPeer.u1PeerAdmin = MSDP_PEER_ADMIN_DISABLE;
    MsdpPeerHdlAdminStatusConfig (&ConfigPeer);

    return OSIX_SUCCESS;
}

INT4
MsdpUtPeer_6 (VOID)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    MEMSET (&MsdpFsMsdpPeerEntry, NULL, sizeof (tMsdpFsMsdpPeerEntry));

    MsdpUtDelEntry ();
    MsdpPeerFsmPeerEnable (&MsdpFsMsdpPeerEntry);

    return OSIX_SUCCESS;
}

INT4
MsdpUtPeer_7 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;

    Peer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 10;
    Peer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 10;
    Peer.MibObject.i4FsMsdpPeerAddrType = 1;
    Peer.MibObject.i4FsMsdpPeerRemoteAddressLen = 4;
    MsdpUtDelEntry ();
    MsdpFsMsdpPeerTableCreateApi (&Peer);

    MsdpPeerFsmInitiateConnection (&Peer);
    Peer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 10;
    Peer.MibObject.au1FsMsdpPeerRemoteAddress[15] = 10;
    Peer.MibObject.i4FsMsdpPeerAddrType = 2;
    Peer.MibObject.i4FsMsdpPeerRemoteAddressLen = 16;
    MsdpFsMsdpPeerTableCreateApi (&Peer);

    MsdpPeerFsmInitiateConnection (&Peer);
    return OSIX_SUCCESS;
}

INT4
MsdpUtPeer_8 (VOID)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    MEMSET (&MsdpFsMsdpPeerEntry, NULL, sizeof (tMsdpFsMsdpPeerEntry));

    MsdpUtDelEntry ();
    MsdpPeerFsmPassiveOpen (&MsdpFsMsdpPeerEntry);

    return OSIX_SUCCESS;
}

INT4
MsdpUtPeer_9 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;

    MsdpUtDelEntry ();
    MsdpPeerFsmHandleEstablished (&Peer);
    MsdpTmrStopTmr (&Peer.MsdpPeerHoldTimer);
    MsdpTmrStopTmr (&Peer.MsdpPeerKATimer);
    return OSIX_SUCCESS;
}

INT4
MsdpUtPeer_10 (VOID)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    MsdpUtDelEntry ();
    MsdpPeerFsmHdlConnRetryExpiry (&MsdpFsMsdpPeerEntry);

    return OSIX_SUCCESS;
}

INT4
MsdpUtPeer_11 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;
    MsdpUtDelEntry ();
    MsdpPeerFsmPeerStop (&Peer);
    return OSIX_SUCCESS;
}

INT4
MsdpUtPeer_12 (VOID)
{
    tMsdpFsMsdpPeerEntry *pPeer = NULL;
    tMsdpFsMsdpPeerEntry Peer;

    MsdpUtDelEntry ();
    MsdpPeerUtilFindPeerEntry (0, &pPeer);

    Peer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 10;
    Peer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 10;
    Peer.MibObject.i4FsMsdpPeerAddrType = 1;
    Peer.MibObject.i4FsMsdpPeerRemoteAddressLen = 4;
    Peer.i4SockFd = 0;
    MsdpFsMsdpPeerTableCreateApi (&Peer);
    MsdpPeerUtilFindPeerEntry (0, &pPeer);

    MsdpPeerUtilFindPeerEntry (1, &pPeer);
    return OSIX_SUCCESS;
}

INT4
MsdpUtPeer_13 (VOID)
{

/*MsdpPeerUtilGetPeerEntry (tIPvXAddr * pPeerAddr, UINT1 u1AddrType,
 *                           tMsdpFsMsdpPeerEntry ** ppPeer)
 *                           */
    return OSIX_SUCCESS;
}

INT4
MsdpUtPeer_14 (VOID)
{
    tIPvXAddr           PeerAddr;
    tMsdpFsMsdpPeerEntry Peer;

    MsdpUtDelEntry ();
    MsdpPeerValidateAndAcceptPeer (&PeerAddr, 1, 1);

    PeerAddr.au1Addr[0] = 10;
    PeerAddr.au1Addr[3] = 10;
    PeerAddr.u1Afi = 1;
    PeerAddr.u1AddrLen = 4;
    Peer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 10;
    Peer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 10;
    Peer.MibObject.i4FsMsdpPeerRemoteAddressLen = 4;
    Peer.MibObject.i4FsMsdpPeerAddrType = 1;
    MsdpFsMsdpPeerTableCreateApi (&Peer);
    MsdpPeerValidateAndAcceptPeer (&PeerAddr, 1, 1);
    return OSIX_SUCCESS;
}

INT4
MsdpUtPeer_15 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;

    Peer.MibObject.i4FsMsdpPeerKeepAliveConfigured = 77;
    MsdpUtDelEntry ();
    MsdpPeerHandleKeepAliveTmrExpiry (&Peer);
    MsdpTmrStopTmr (&Peer.MsdpPeerKATimer);
    return OSIX_SUCCESS;
}

INT4
MsdpUtPeer_16 (VOID)
{
    tIPvXAddr           PeerAddr;
    UINT1               u1AddrType = 0;

    MsdpUtDelEntry ();
    MsdpPeerChkPeerFilterStatus (&PeerAddr, u1AddrType);

    return OSIX_SUCCESS;
}

/* msdpin UT cases */
INT4
MsdpUtIn_1 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;
    UINT1               au1TxBuf[1000];
    UINT1               au1RxBuf[1000];

    MEMSET (&Peer, NULL, sizeof (tMsdpFsMsdpPeerEntry));
    /*Peer.MsdpPeerHoldTimer.tmrNode.u2Flags = 0x2; */
    Peer.MibObject.i4FsMsdpPeerAddrType = 1;
    Peer.MibObject.i4FsMsdpPeerHoldTimeConfigured = 75;
    Peer.TxBufInfo.pu1Msg = au1TxBuf;
    Peer.RxBufInfo.pu1Msg = au1RxBuf;
    Peer.RxBufInfo.u2MsgOffset = 0;
    MEMSET (au1RxBuf, 0, 1000);
    MsdpUtDelEntry ();
    MsdpInProcessRxdMsg (&Peer);

    au1RxBuf[0] = 4;
    Peer.RxBufInfo.u2MsgOffset = 0;
    Peer.RxBufInfo.u2MsgSize = 3;
    MsdpInProcessRxdMsg (&Peer);
    MsdpTmrStopTmr (&Peer.MsdpPeerHoldTimer);
    au1RxBuf[999] = 4;
    Peer.RxBufInfo.u2MsgOffset = 999;
    Peer.RxBufInfo.u2MsgSize = 13;
    MsdpInProcessRxdMsg (&Peer);
    MsdpTmrStopTmr (&Peer.MsdpPeerHoldTimer);

    return OSIX_SUCCESS;
}

INT4
MsdpUtIn_2 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;
    UINT1               au1Buf[1000];

    MsdpUtDelEntry ();
    Peer.RxBufInfo.u2MsgSize = MSDP_PEER_MAX_RX_TX_BUF_LEN;
    MsdpInReadPeerMsg (&Peer);

    Peer.RxBufInfo.u2MsgSize = 100;
    Peer.RxBufInfo.pu1Msg = au1Buf;
    Peer.RxBufInfo.u2MsgOffset = 100;
    Peer.i4SockFd = 1;
    MsdpInReadPeerMsg (&Peer);

    Peer.RxBufInfo.u2MsgSize = MSDP_PEER_MAX_RX_TX_BUF_LEN - 10;
    Peer.RxBufInfo.u2MsgOffset = 100;
    Peer.i4SockFd = -1;
    MsdpInReadPeerMsg (&Peer);

    return OSIX_SUCCESS;
}

INT4
MsdpUtIn_3 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;
    UINT1               au1TxBuf[1000];
    UINT1               au1RxBuf[1000];

    MsdpUtDelEntry ();
    Peer.RxBufInfo.pu1Msg = au1RxBuf;
    MsdpInHandleRcvdPacket (&Peer);    /*Invalid Type = 0 */

    Peer.MibObject.i4FsMsdpPeerAddrType = 1;
    Peer.MibObject.i4FsMsdpPeerHoldTimeConfigured = 105;
    Peer.MibObject.i4FsMsdpPeerKeepAliveConfigured = 70;
    Peer.MibObject.i4FsMsdpPeerDataTtl = 15;
    Peer.TxBufInfo.pu1Msg = au1TxBuf;
    Peer.RxBufInfo.pu1Msg = au1RxBuf;
    Peer.RxBufInfo.u2MsgOffset = 0;
    au1RxBuf[0] = MSDP_TLV_SA;
    MsdpInHandleRcvdPacket (&Peer);
    MsdpTmrStopTmr (&Peer.MsdpPeerHoldTimer);

    au1RxBuf[0] = MSDP6_TLV_SA;
    MsdpInHandleRcvdPacket (&Peer);
    MsdpTmrStopTmr (&Peer.MsdpPeerHoldTimer);

    au1RxBuf[0] = MSDP_TLV_SA_RESPONSE;
    MsdpInHandleRcvdPacket (&Peer);
    MsdpTmrStopTmr (&Peer.MsdpPeerHoldTimer);

    au1RxBuf[0] = MSDP6_TLV_SA_RESPONSE;
    MsdpInHandleRcvdPacket (&Peer);
    MsdpTmrStopTmr (&Peer.MsdpPeerHoldTimer);

    au1RxBuf[0] = MSDP_TLV_SA_REQUEST;
    MsdpInHandleRcvdPacket (&Peer);
    MsdpTmrStopTmr (&Peer.MsdpPeerHoldTimer);

    au1RxBuf[0] = MSDP6_TLV_SA_REQUEST;
    MsdpInHandleRcvdPacket (&Peer);
    MsdpTmrStopTmr (&Peer.MsdpPeerHoldTimer);

    au1RxBuf[0] = MSDP_TLV_KEEPALIVE;
    MsdpInHandleRcvdPacket (&Peer);
    MsdpTmrStopTmr (&Peer.MsdpPeerKATimer);
    MsdpTmrStopTmr (&Peer.MsdpPeerHoldTimer);

    return OSIX_SUCCESS;
}

INT4
MsdpUtIn_4 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;
    UINT1               au1TxBuf[1000];
    UINT1               au1RxBuf[1000];
    UINT2               u2Len = 0;
    INT4                i4Offset = 0;

    MEMSET (au1RxBuf, 0, 1000);
    MEMSET (&Peer, NULL, sizeof (tMsdpFsMsdpPeerEntry));

    MsdpUtDelEntry ();
    Peer.MibObject.i4FsMsdpPeerAddrType = 1;
    Peer.MibObject.i4FsMsdpPeerHoldTimeConfigured = 105;
    Peer.MibObject.i4FsMsdpPeerKeepAliveConfigured = 70;
    Peer.MibObject.i4FsMsdpPeerDataTtl = 15;
    Peer.TxBufInfo.pu1Msg = au1TxBuf;
    Peer.RxBufInfo.pu1Msg = au1RxBuf;
    Peer.RxBufInfo.u2MsgOffset = 0;
    Peer.RxBufInfo.u2MsgSize = MSDP_MIN_IPV4_SA_ADVT_SIZE - 1;
    au1RxBuf[0] = MSDP_TLV_SA;

    MsdpInHandleSaAdvtOrRespPkt (&Peer, 1);

    Peer.RxBufInfo.u2MsgSize = MSDP_MIN_IPV4_SA_ADVT_SIZE;
    u2Len = 2 * MSDP_MIN_IPV4_SA_ADVT_SIZE;
    u2Len = OSIX_HTONS (u2Len);
    MEMCPY (&au1RxBuf[1], (UINT1 *) &u2Len, 2);
    MsdpInHandleSaAdvtOrRespPkt (&Peer, 1);
    MsdpTmrStopTmr (&Peer.MsdpPeerHoldTimer);
    MsdpTmrStopTmr (&Peer.MsdpPeerKATimer);

    Peer.RxBufInfo.u2MsgOffset = MSDP_PEER_MAX_RX_TX_BUF_LEN -
        MSDP_IPV4_SA_ADVT_HDR_SIZE + 3;
    i4Offset = Peer.RxBufInfo.u2MsgOffset;
    au1RxBuf[Peer.RxBufInfo.u2MsgOffset] = MSDP_TLV_SA;
    Peer.RxBufInfo.u2MsgSize = 100;
    u2Len = MSDP_MIN_IPV4_SA_ADVT_SIZE;
    u2Len = OSIX_HTONS (u2Len);
    MEMCPY (&au1RxBuf[Peer.RxBufInfo.u2MsgOffset + 1], (UINT1 *) &u2Len, 2);
    au1RxBuf[i4Offset + MSDP_SA_ADVT_RP_OFFSET] = 13;
    au1RxBuf[i4Offset + MSDP_SA_ADVT_RP_OFFSET + 3] = 10;
    au1RxBuf[i4Offset + MSDP_SA_ADVT_RP_OFFSET + 12] = 20;
    au1RxBuf[i4Offset + MSDP_SA_ADVT_RP_OFFSET + 15] = 2;
    Peer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 20;
    Peer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 2;
    Peer.MibObject.i4FsMsdpPeerRemoteAddressLen = 4;

    MsdpInHandleSaAdvtOrRespPkt (&Peer, 1);
    MsdpTmrStopTmr (&Peer.MsdpPeerHoldTimer);
    MsdpTmrStopTmr (&Peer.MsdpPeerKATimer);

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("ip route 13.0 255.0 10.2 10");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("ip route 20.0 255.0 10.2");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    MsdpInHandleSaAdvtOrRespPkt (&Peer, 1);
    MsdpTmrStopTmr (&Peer.MsdpPeerHoldTimer);
    MsdpTmrStopTmr (&Peer.MsdpPeerKATimer);

    /* CliTakeAppContext ();
       MGMT_UNLOCK ();
       CliExecuteAppCmd ("c t");
       CliExecuteAppCmd ("no ip route 13.0 255.0 10.2");
       CliExecuteAppCmd ("ip route 13.0 255.0 30.2 1");
       CliExecuteAppCmd ("end");
       MGMT_LOCK ();
       CliGiveAppContext ();

       Peer.RxBufInfo.u2MsgOffset = MSDP_PEER_MAX_RX_TX_BUF_LEN - 
       MSDP_IPV4_SA_ADVT_HDR_SIZE + 3;
       au1RxBuf[Peer.RxBufInfo.u2MsgOffset] = MSDP_TLV_SA;
       MsdpInHandleSaAdvtOrRespPkt (&Peer, 1);
       MsdpTmrStopTmr (&Peer.MsdpPeerHoldTimer);
     */

    Peer.RxBufInfo.u2MsgOffset = MSDP_PEER_MAX_RX_TX_BUF_LEN -
        MSDP_IPV4_SA_ADVT_HDR_SIZE + 3;
    au1RxBuf[Peer.RxBufInfo.u2MsgOffset] = MSDP_TLV_SA_RESPONSE;
    MsdpInHandleSaAdvtOrRespPkt (&Peer, 1);
    MsdpTmrStopTmr (&Peer.MsdpPeerHoldTimer);
    MsdpTmrStopTmr (&Peer.MsdpPeerKATimer);

    au1RxBuf[0] = MSDP6_TLV_SA;
    MsdpInHandleSaAdvtOrRespPkt (&Peer, 2);
    MsdpTmrStopTmr (&Peer.MsdpPeerHoldTimer);
    MsdpTmrStopTmr (&Peer.MsdpPeerKATimer);

    return OSIX_SUCCESS;
}

INT4
MsdpUtIn_5 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;
    UINT1               au1TxBuf[1000];
    UINT1               au1RxBuf[1000];
    INT4                i4Offset = 0;

    MEMSET (au1RxBuf, 0, 1000);
    MEMSET (&Peer, NULL, sizeof (tMsdpFsMsdpPeerEntry));

    MsdpUtDelEntry ();
    Peer.MibObject.i4FsMsdpPeerAddrType = 1;
    Peer.MibObject.i4FsMsdpPeerHoldTimeConfigured = 105;
    Peer.MibObject.i4FsMsdpPeerKeepAliveConfigured = 70;
    Peer.MibObject.i4FsMsdpPeerDataTtl = 15;
    Peer.TxBufInfo.pu1Msg = au1TxBuf;
    Peer.RxBufInfo.pu1Msg = au1RxBuf;
    Peer.RxBufInfo.u2MsgOffset = 0;
    Peer.RxBufInfo.u2MsgSize = MSDP_IPV4_SA_REQ_PKT_SIZE - 1;
    au1RxBuf[0] = MSDP_TLV_SA_REQUEST;

    MsdpInProcessSaRequestPkt (&Peer, 1);
    MsdpTmrStopTmr (&Peer.MsdpPeerKATimer);
    MsdpTmrStopTmr (&Peer.MsdpPeerHoldTimer);

    au1RxBuf[0] = MSDP_TLV_SA_REQUEST;
    MsdpInProcessSaRequestPkt (&Peer, 2);
    MsdpTmrStopTmr (&Peer.MsdpPeerKATimer);
    MsdpTmrStopTmr (&Peer.MsdpPeerHoldTimer);

    Peer.RxBufInfo.u2MsgSize = MSDP_IPV4_SA_REQ_PKT_SIZE;
    au1RxBuf[0] = MSDP_TLV_SA_REQUEST;

    MsdpInProcessSaRequestPkt (&Peer, 1);
    MsdpTmrStopTmr (&Peer.MsdpPeerKATimer);
    MsdpTmrStopTmr (&Peer.MsdpPeerHoldTimer);

    Peer.RxBufInfo.u2MsgSize = MSDP_IPV4_SA_REQ_PKT_SIZE;
    Peer.RxBufInfo.u2MsgOffset = MSDP_PEER_MAX_RX_TX_BUF_LEN - 5;
    i4Offset = Peer.RxBufInfo.u2MsgOffset;
    au1RxBuf[i4Offset] = MSDP_TLV_SA_REQUEST;

    MsdpInProcessSaRequestPkt (&Peer, 1);
    MsdpTmrStopTmr (&Peer.MsdpPeerKATimer);
    MsdpTmrStopTmr (&Peer.MsdpPeerHoldTimer);

    return OSIX_SUCCESS;
}

INT4
MsdpUtIn_6 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;
    UINT1               au1TxBuf[1000];
    UINT1               au1RxBuf[1000];

    MEMSET (au1RxBuf, 0, 1000);
    MEMSET (&Peer, NULL, sizeof (tMsdpFsMsdpPeerEntry));

    MsdpUtDelEntry ();
    Peer.MibObject.i4FsMsdpPeerAddrType = 1;
    Peer.MibObject.i4FsMsdpPeerHoldTimeConfigured = 105;
    Peer.MibObject.i4FsMsdpPeerKeepAliveConfigured = 70;
    Peer.MibObject.i4FsMsdpPeerDataTtl = 15;
    Peer.TxBufInfo.pu1Msg = au1TxBuf;
    Peer.RxBufInfo.pu1Msg = au1RxBuf;
    Peer.RxBufInfo.u2MsgOffset = 0;
    Peer.RxBufInfo.u2MsgSize = MSDP_KEEP_ALIVE_PKT_SIZE - 1;
    au1RxBuf[0] = MSDP_TLV_KEEPALIVE;

    MsdpInProcessKeepAlivePkt (&Peer);

    Peer.RxBufInfo.u2MsgSize = MSDP_KEEP_ALIVE_PKT_SIZE;
    au1RxBuf[0] = MSDP_TLV_KEEPALIVE;

    MsdpInProcessKeepAlivePkt (&Peer);
    MsdpTmrStopTmr (&Peer.MsdpPeerKATimer);
    MsdpTmrStopTmr (&Peer.MsdpPeerHoldTimer);
    return OSIX_SUCCESS;
}

INT4
MsdpUtIn_7 (VOID)
{
    tMsdpFsMsdpPeerEntry SaAdvtPeer;
    tIPvXAddr           Originator;

    MEMSET (Originator.au1Addr, 7, 16);
    MEMSET (SaAdvtPeer.MibObject.au1FsMsdpPeerRemoteAddress, 1, 16);
    Originator.au1Addr[3] = 8;
    Originator.u1Afi = 1;
    Originator.u1AddrLen = 4;

    MsdpUtDelEntry ();
    MsdpInChkRpfPeer (&SaAdvtPeer, &Originator);

    CliVlanInit ();
    SaAdvtPeer.MibObject.i4FsMsdpPeerAddrType = 1;
    MsdpInChkRpfPeer (&SaAdvtPeer, &Originator);

    MEMSET (SaAdvtPeer.MibObject.au1FsMsdpPeerRemoteAddress, 7, 16);
    SaAdvtPeer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 8;
    MsdpInChkRpfPeer (&SaAdvtPeer, &Originator);

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("ip route 9.0 255.0 7.2");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    MEMSET (Originator.au1Addr, 9, 16);
    MEMSET (SaAdvtPeer.MibObject.au1FsMsdpPeerRemoteAddress, 8, 16);

    MsdpInChkRpfPeer (&SaAdvtPeer, &Originator);

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("ip route 8.0 255.0 7.3");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    sleep (3);
    MsdpInChkRpfPeer (&SaAdvtPeer, &Originator);

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no ip route 8.0 255.0 7.3");
    CliExecuteAppCmd ("ip route 8.0 255.0 7.2");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    MsdpInChkRpfPeer (&SaAdvtPeer, &Originator);

    CliVlanDeInit ();

    return OSIX_SUCCESS;
}

/* msdplw UT cases */
INT4
MsdpUtLw_1 (VOID)
{
    return -1;
}

INT4
MsdpUtLw_2 (VOID)
{
    return -1;
}

INT4
MsdpUtLw_3 (VOID)
{
    return -1;
}

INT4
MsdpUtLw_4 (VOID)
{
    return -1;
}

INT4
MsdpUtLw_5 (VOID)
{
    return -1;
}

INT4
MsdpUtLw_6 (VOID)
{
    return -1;
}

/* msdpmain UT cases */
INT4
MsdpUtMain_1 (VOID)
{
    return -1;
}

INT4
MsdpUtMain_2 (VOID)
{
    return -1;
}

INT4
MsdpUtMain_3 (VOID)
{
    return -1;
}

INT4
MsdpUtMain_4 (VOID)
{
    return -1;
}

INT4
MsdpUtMain_5 (VOID)
{
    return -1;
}

INT4
MsdpUtMain_6 (VOID)
{
    return -1;
}

INT4
MsdpUtMain_7 (VOID)
{
    return -1;
}

/* msdpout UT cases */
INT4
MsdpUtOut_1 (VOID)
{
    tMsdpFsMsdpPeerEntry InPeer;
    tMsdpFsMsdpPeerEntry *pPeer = NULL;
    UINT1               au1LinBuf[500];
    UINT2               u2PktLen = 0;

    MEMSET (&InPeer, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (au1LinBuf, 0, 500);
    MsdpOutValidateAndDecrementTtl (&InPeer, au1LinBuf, &u2PktLen);

    au1LinBuf[0] = MSDP_TLV_SA;
    au1LinBuf[MSDP_THREE] = 1;
    au1LinBuf[MSDP_MIN_IPV4_SA_ADVT_SIZE + MSDP_V4_MDP_TTL_OFFSET] = 15;
    InPeer.MibObject.i4FsMsdpPeerAddrType = 2;

    u2PktLen = MSDP_MIN_IPV4_SA_ADVT_SIZE;
    MsdpOutValidateAndDecrementTtl (&InPeer, au1LinBuf, &u2PktLen);

    InPeer.MibObject.i4FsMsdpPeerAddrType = 1;
    au1LinBuf[0] = MSDP6_TLV_SA;
    MsdpOutValidateAndDecrementTtl (&InPeer, au1LinBuf, &u2PktLen);

    InPeer.MibObject.i4FsMsdpPeerAddrType = 2;
    au1LinBuf[0] = MSDP6_TLV_SA;
    MsdpOutValidateAndDecrementTtl (&InPeer, au1LinBuf, &u2PktLen);

    au1LinBuf[0] = MSDP_TLV_SA;
    u2PktLen = MSDP_MIN_IPV4_SA_ADVT_SIZE;
    InPeer.MibObject.i4FsMsdpPeerAddrType = 1;
    MsdpOutValidateAndDecrementTtl (&InPeer, au1LinBuf, &u2PktLen);

    au1LinBuf[0] = MSDP_TLV_SA;
    InPeer.MibObject.i4FsMsdpPeerAddrType = 1;
    InPeer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 10;
    InPeer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 10;
    InPeer.MibObject.i4FsMsdpPeerDataTtl = 5;
    u2PktLen = 1000;

    pPeer = MsdpFsMsdpPeerTableCreateApi (&InPeer);
    if (OSIX_SUCCESS != MsdpOutValidateAndDecrementTtl
        (&InPeer, au1LinBuf, &u2PktLen))
    {
        printf ("\r\n failure 1");
        return OSIX_FAILURE;
    }

    if (au1LinBuf[MSDP_MIN_IPV4_SA_ADVT_SIZE + MSDP_V4_MDP_TTL_OFFSET] != 10)
    {
        printf ("\r\n failure 2-- ttl updation");
        return OSIX_FAILURE;
    }

    au1LinBuf[MSDP_MIN_IPV4_SA_ADVT_SIZE + MSDP_V4_MDP_TTL_OFFSET] = 15;
    pPeer->MibObject.i4FsMsdpPeerDataTtl = 15;
    if (OSIX_SUCCESS != MsdpOutValidateAndDecrementTtl
        (&InPeer, au1LinBuf, &u2PktLen))
    {
        printf ("\r\n failure 2");
        return OSIX_FAILURE;
    }

    if (au1LinBuf[MSDP_MIN_IPV4_SA_ADVT_SIZE + MSDP_V4_MDP_TTL_OFFSET] != 0)
    {
        printf ("\r\n failure 2 --ttl updation");
        return OSIX_FAILURE;
    }

    au1LinBuf[MSDP_MIN_IPV4_SA_ADVT_SIZE + MSDP_V4_MDP_TTL_OFFSET] = 15;
    pPeer->MibObject.i4FsMsdpPeerDataTtl = 17;
    if (OSIX_SUCCESS == MsdpOutValidateAndDecrementTtl
        (&InPeer, au1LinBuf, &u2PktLen))
    {
        printf ("\r\n failure 3");
        return OSIX_FAILURE;
    }

    if (u2PktLen != MSDP_MIN_IPV4_SA_ADVT_SIZE)
    {
        printf ("\r\n failure 3 --pkt length");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
MsdpUtOut_2 (VOID)
{
    tMsdpFsMsdpPeerEntry RecvPeer;
    tMsdpFsMsdpPeerEntry NextPeer;

    MsdpUtDelEntry ();
    MEMSET (&RecvPeer, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&NextPeer, 0, sizeof (tMsdpFsMsdpPeerEntry));
    if (OSIX_FAILURE == MsdpOutChkForValidPeer (NULL, &NextPeer))
    {
        printf ("Failure 1\r\n");
        return OSIX_FAILURE;
    }

    RecvPeer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 10;
    RecvPeer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 10;
    NextPeer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 10;
    NextPeer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 10;
    if (OSIX_FAILURE != MsdpOutChkForValidPeer (&RecvPeer, &NextPeer))
    {
        printf ("Failure 2\r\n");
        return OSIX_FAILURE;
    }

    RecvPeer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 10;
    RecvPeer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 10;
    NextPeer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 11;
    NextPeer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 11;
    NextPeer.MibObject.i4FsMsdpPeerState = MSDP_PEER_ESTABLISHED;
    if (OSIX_FAILURE == MsdpOutChkForValidPeer (&RecvPeer, &NextPeer))
    {
        printf ("Failure 3\r\n");
        return OSIX_FAILURE;
    }

    NextPeer.MibObject.i4FsMsdpPeerState = ~MSDP_PEER_ESTABLISHED;
    if (OSIX_FAILURE != MsdpOutChkForValidPeer (&RecvPeer, &NextPeer))
    {
        printf ("Failure 3\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
MsdpUtOut_3 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;
    UINT1               au1SrcBuf[500];
    UINT2               u2PktLen = 0;

    MsdpUtDelEntry ();
    MEMSET (&Peer, 0, sizeof (tMsdpFsMsdpPeerEntry));
    Peer.MibObject.i4FsMsdpPeerRemoteAddressLen = 4;
    au1SrcBuf[0] = MSDP_TLV_SA;
    au1SrcBuf[3] = 1;
    u2PktLen = MSDP_MIN_IPV4_SA_ADVT_SIZE;
    MsdpOutUpdatePeerStatistics (&Peer, au1SrcBuf, u2PktLen);

    au1SrcBuf[0] = MSDP_TLV_SA;
    au1SrcBuf[3] = 1;
    u2PktLen = MSDP_MIN_IPV4_SA_ADVT_SIZE + 100;
    MsdpOutUpdatePeerStatistics (&Peer, au1SrcBuf, u2PktLen);

    Peer.MibObject.i4FsMsdpPeerRemoteAddressLen = 16;
    au1SrcBuf[0] = MSDP6_TLV_SA;
    au1SrcBuf[3] = 1;
    u2PktLen = MSDP_MIN_IPV6_SA_ADVT_SIZE;
    MsdpOutUpdatePeerStatistics (&Peer, au1SrcBuf, u2PktLen);

    au1SrcBuf[0] = MSDP6_TLV_SA;
    au1SrcBuf[3] = 1;
    u2PktLen = MSDP_MIN_IPV6_SA_ADVT_SIZE + 100;
    MsdpOutUpdatePeerStatistics (&Peer, au1SrcBuf, u2PktLen);

    au1SrcBuf[0] = MSDP6_TLV_SA_REQUEST;
    MsdpOutUpdatePeerStatistics (&Peer, au1SrcBuf, u2PktLen);

    au1SrcBuf[0] = MSDP_TLV_SA_REQUEST;
    MsdpOutUpdatePeerStatistics (&Peer, au1SrcBuf, u2PktLen);

    au1SrcBuf[0] = MSDP6_TLV_SA_RESPONSE;
    MsdpOutUpdatePeerStatistics (&Peer, au1SrcBuf, u2PktLen);

    au1SrcBuf[0] = MSDP_TLV_SA_RESPONSE;
    MsdpOutUpdatePeerStatistics (&Peer, au1SrcBuf, u2PktLen);

    au1SrcBuf[0] = MSDP_TLV_KEEPALIVE;
    MsdpOutUpdatePeerStatistics (&Peer, au1SrcBuf, u2PktLen);

    au1SrcBuf[0] = 222;
    MsdpOutUpdatePeerStatistics (&Peer, au1SrcBuf, u2PktLen);
    return OSIX_SUCCESS;
}

INT4
MsdpUtOut_4 (VOID)
{
    tMsdpFsMsdpPeerEntry RcvPeer;
    tMsdpFsMsdpMeshGroupEntry Mesh;
    UINT1               au1Buf[1000];

    MsdpUtDelEntry ();
    MEMSET (&RcvPeer, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MsdpOutFwdSAPkt (&RcvPeer, au1Buf, 1, 100, 100, 1000);

    RcvPeer.MibObject.i4FsMsdpPeerAddrType = 1;
    RcvPeer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 10;
    RcvPeer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 10;
    RcvPeer.MibObject.i4FsMsdpPeerRemoteAddressLen = 4;
    RcvPeer.MibObject.i4FsMsdpPeerState = MSDP_PEER_ESTABLISHED;
    RcvPeer.MibObject.i4FsMsdpPeerDataTtl = 15;
    MsdpFsMsdpPeerTableCreateApi (&RcvPeer);
    MsdpOutFwdSAPkt (&RcvPeer, au1Buf, 2, 100, 100, 1000);

    RcvPeer.MibObject.i4FsMsdpPeerAddrType = 1;
    RcvPeer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 11;
    RcvPeer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 11;
    RcvPeer.MibObject.i4FsMsdpPeerRemoteAddressLen = 4;
    RcvPeer.MibObject.i4FsMsdpPeerDataTtl = 15;
    MsdpFsMsdpPeerTableCreateApi (&RcvPeer);
    au1Buf[0] = 1;

    Mesh.MibObject.i4FsMsdpMeshGroupAddrType = 1;
    Mesh.MibObject.au1FsMsdpMeshGroupName[0] = 's';
    Mesh.MibObject.au1FsMsdpMeshGroupName[1] = 's';
    Mesh.MibObject.i4FsMsdpMeshGroupNameLen = 2;
    Mesh.MibObject.au1FsMsdpMeshGroupPeerAddress[0] = 11;
    Mesh.MibObject.au1FsMsdpMeshGroupPeerAddress[3] = 11;
    Mesh.MibObject.i4FsMsdpMeshGroupPeerAddressLen = 4;
    MsdpFsMsdpMeshGroupTableCreateApi (&Mesh);
    au1Buf[MSDP_MIN_IPV6_SA_ADVT_SIZE + MSDP_V4_MDP_TTL_OFFSET] = 20;
    MsdpOutFwdSAPkt (&RcvPeer, au1Buf, 1, 100, 100, 1000);

    Mesh.MibObject.au1FsMsdpMeshGroupPeerAddress[0] = 10;
    Mesh.MibObject.au1FsMsdpMeshGroupPeerAddress[3] = 10;
    MsdpFsMsdpMeshGroupTableCreateApi (&Mesh);

    MsdpOutFwdSAPkt (&RcvPeer, au1Buf, 1, 100, 100, 1000);

    /*MsdpOutFwdSAPkt (NULL, au1Buf, 1, 100, 100, 1000); */
    return OSIX_SUCCESS;
}

INT4
MsdpUtOut_5 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;
    UINT1               au1SrcBuf[100];
    UINT1               au1Buf[MSDP_PEER_MAX_RX_TX_BUF_LEN];

    MsdpUtDelEntry ();
    Peer.TxBufInfo.pu1Msg = au1Buf;
    MEMSET (au1SrcBuf, 0, 100);
    Peer.TxBufInfo.u2MsgSize = 5;
    Peer.TxBufInfo.u2MsgOffset = MSDP_PEER_MAX_RX_TX_BUF_LEN - 10;
    MsdpOutCopyMsgInPeerTxBuf (&Peer, au1Buf, 40, 20, 100);
    Peer.TxBufInfo.u2MsgSize = MSDP_PEER_MAX_RX_TX_BUF_LEN - 50;
    Peer.TxBufInfo.u2MsgOffset = MSDP_PEER_MAX_RX_TX_BUF_LEN - 1;
    MsdpOutCopyMsgInPeerTxBuf (&Peer, au1Buf, 70, 47, 100);
    Peer.TxBufInfo.u2MsgSize = MSDP_PEER_MAX_RX_TX_BUF_LEN - 50;
    Peer.TxBufInfo.u2MsgOffset = MSDP_PEER_MAX_RX_TX_BUF_LEN - 1;
    MsdpOutCopyMsgInPeerTxBuf (&Peer, au1Buf, 50, 70, 100);

    Peer.TxBufInfo.u2MsgSize = 20;
    MsdpOutCopyMsgInPeerTxBuf (&Peer, au1Buf, 50, 60, 100);
    return OSIX_SUCCESS;
}

INT4
MsdpUtOut_6 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;
    UINT1               au1SrcBuf[100];
    UINT1               au1Buf[MSDP_PEER_MAX_RX_TX_BUF_LEN];

    Peer.TxBufInfo.pu1Msg = au1Buf;

    MsdpUtDelEntry ();
    MsdpOutSendMsgtoPeer (&Peer, au1SrcBuf, 50, 70, 100);

    Peer.TxBufInfo.u2MsgSize = MSDP_PEER_MAX_RX_TX_BUF_LEN - 10;
    Peer.TxBufInfo.u2MsgOffset = 100;
    MsdpOutSendMsgtoPeer (&Peer, au1SrcBuf, 40, 50, 100);
    return OSIX_SUCCESS;
}

INT4
MsdpUtOut_7 (VOID)
{
    /*MsdpOutSendResidualMsgtoPeer (tMsdpFsMsdpPeerEntry * pPeer); */
    return -1;
}

INT4
MsdpUtOut_8 (VOID)
{
    tMsdpFsMsdpPeerEntry RcvPeer;
    tMsdpFsMsdpPeerEntry Peer;
    tMsdpFsMsdpMeshGroupEntry Mesh;

    MEMSET (&RcvPeer, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&Peer, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&Mesh, 0, sizeof (tMsdpFsMsdpMeshGroupEntry));

    MsdpUtDelEntry ();
    if (MSDP_PEER_DONT_FORWARD == MsdpOutChkPeerInMeshGrp (NULL, &Peer))
    {
        printf ("\r\nFailure 1");
        return OSIX_FAILURE;
    }

    if (MSDP_PEER_DONT_FORWARD != MsdpOutChkPeerInMeshGrp (&RcvPeer, NULL))
    {
        printf ("\r\nFailure 2");
        return OSIX_FAILURE;
    }

    RcvPeer.MibObject.i4FsMsdpPeerAddrType = 1;
    if (MSDP_PEER_DONT_FORWARD == MsdpOutChkPeerInMeshGrp (&RcvPeer, &Peer))
    {
        printf ("\r\nFailure 3");
        return OSIX_FAILURE;
    }

    Mesh.MibObject.i4FsMsdpMeshGroupAddrType = 1;
    Mesh.MibObject.au1FsMsdpMeshGroupPeerAddress[0] = 10;
    Mesh.MibObject.au1FsMsdpMeshGroupPeerAddress[3] = 10;
    Mesh.MibObject.i4FsMsdpMeshGroupPeerAddressLen = 4;

    Mesh.MibObject.au1FsMsdpMeshGroupName[0] = 's';
    Mesh.MibObject.au1FsMsdpMeshGroupName[1] = 's';
    Mesh.MibObject.au1FsMsdpMeshGroupName[2] = 's';
    Mesh.MibObject.i4FsMsdpMeshGroupNameLen = 3;
    MsdpFsMsdpMeshGroupTableCreateApi (&Mesh);

    RcvPeer.MibObject.i4FsMsdpPeerAddrType = 2;
    RcvPeer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 10;
    RcvPeer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 10;
    RcvPeer.MibObject.i4FsMsdpPeerRemoteAddressLen = 4;

    if (MSDP_PEER_DONT_FORWARD == MsdpOutChkPeerInMeshGrp (&RcvPeer, &Peer))
    {
        printf ("\r\nFailure 4");
        return OSIX_FAILURE;
    }

    RcvPeer.MibObject.i4FsMsdpPeerAddrType = 1;
    RcvPeer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 12;
    RcvPeer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 12;
    RcvPeer.MibObject.i4FsMsdpPeerRemoteAddressLen = 4;
    if (MSDP_PEER_DONT_FORWARD == MsdpOutChkPeerInMeshGrp (&RcvPeer, &Peer))
    {
        printf ("\r\nFailure 5");
        return OSIX_FAILURE;
    }

    RcvPeer.MibObject.i4FsMsdpPeerAddrType = 1;
    RcvPeer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 10;
    RcvPeer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 10;
    if (MSDP_PEER_DONT_FORWARD != MsdpOutChkPeerInMeshGrp (&RcvPeer, &Peer))
    {
        printf ("\r\nFailure 6");
        return OSIX_FAILURE;
    }

    Mesh.MibObject.i4FsMsdpMeshGroupAddrType = 1;
    Mesh.MibObject.au1FsMsdpMeshGroupPeerAddress[0] = 11;
    Mesh.MibObject.au1FsMsdpMeshGroupPeerAddress[3] = 11;
    Mesh.MibObject.au1FsMsdpMeshGroupName[0] = 's';
    Mesh.MibObject.au1FsMsdpMeshGroupName[1] = 's';
    Mesh.MibObject.au1FsMsdpMeshGroupName[2] = 's';
    Mesh.MibObject.i4FsMsdpMeshGroupNameLen = 3;

    Peer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 11;
    Peer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 11;
    Peer.MibObject.i4FsMsdpPeerRemoteAddressLen = 4;
    if (MSDP_PEER_DONT_FORWARD == MsdpOutChkPeerInMeshGrp (&RcvPeer, &Peer))
    {
        printf ("\r\nFailure 7");
        return OSIX_FAILURE;
    }
    MsdpFsMsdpMeshGroupTableCreateApi (&Mesh);

    if (MSDP_PEER_DONT_FORWARD != MsdpOutChkPeerInMeshGrp (&RcvPeer, &Peer))
    {
        printf ("\r\nFailure 8");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
MsdpUtOut_9 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;

    MEMSET (&Peer, 0, sizeof (tMsdpFsMsdpPeerEntry));

    Peer.TxBufInfo.u2MsgSize = 100;
    Peer.TxBufInfo.u2MsgOffset = 101;
    Peer.i4SockFd = 1;
    MsdpUtDelEntry ();
    MsdpOutPeerKeepAlivePkt (&Peer);

    return OSIX_SUCCESS;
}

/* msdppim UT cases */
INT4
MsdpUtPim_1 (VOID)
{
    tMsdpMrpSaAdvt      PimSaInfo;
    tMsdpCacheUpdt      MsdpCache;
    tIPvXAddr           Addr;

    MsdpUtDelEntry ();
    PimSaInfo.pGrpSrcAddrList = CRU_BUF_Allocate_MsgBufChain (500, 0);
    Addr.au1Addr[0] = 224;
    Addr.au1Addr[1] = 10;
    Addr.au1Addr[2] = 10;
    Addr.au1Addr[3] = 10;
    Addr.u1Afi = 1;
    Addr.u1AddrLen = 4;
    CRU_BUF_Copy_OverBufChain
        (PimSaInfo.pGrpSrcAddrList, (UINT1 *) &Addr, 0, 20);
    Addr.au1Addr[0] = 10;
    Addr.au1Addr[3] = 10;
    Addr.u1Afi = 1;
    Addr.u1AddrLen = 4;
    CRU_BUF_Copy_OverBufChain
        (PimSaInfo.pGrpSrcAddrList, (UINT1 *) &Addr, 20, 20);
    MsdpPimIfExtractSGFromBuf (&PimSaInfo, &MsdpCache);

    printf ("\r\nG(%s)\nS(%s)\n", MsdpPrintIPvxAddress (MsdpCache.GrpAddr),
            MsdpPrintIPvxAddress (MsdpCache.SrcAddr));
    CRU_BUF_Release_MsgBufChain (PimSaInfo.pGrpSrcAddrList, OSIX_TRUE);
    return OSIX_SUCCESS;
}

INT4
MsdpUtPim_2 (VOID)
{
    tMsdpMrpSaAdvt      PimSaInfo;
    tMsdpFsMsdpRPEntry  Rp;
    tIPvXAddr           Addr;

    MsdpUtDelEntry ();
    PimSaInfo.pDataPkt = NULL;
    PimSaInfo.pGrpSrcAddrList = CRU_BUF_Allocate_MsgBufChain (500, 0);
    Addr.au1Addr[0] = 224;
    Addr.au1Addr[1] = 10;
    Addr.au1Addr[2] = 10;
    Addr.au1Addr[3] = 10;
    Addr.u1Afi = 1;
    Addr.u1AddrLen = 4;
    CRU_BUF_Copy_OverBufChain
        (PimSaInfo.pGrpSrcAddrList, (UINT1 *) &Addr, 0, 20);
    Addr.au1Addr[0] = 10;
    Addr.au1Addr[3] = 10;
    Addr.u1Afi = 1;
    Addr.u1AddrLen = 4;
    CRU_BUF_Copy_OverBufChain
        (PimSaInfo.pGrpSrcAddrList, (UINT1 *) &Addr, 20, 20);
    PimSaInfo.u1Action = MSDP_ADD_CACHE;
    PimSaInfo.u1AddrType = 1;
    PimSaInfo.u1CompId = 2;
    if (OSIX_FAILURE != MsdpPimIfHandleSAInfo (&PimSaInfo))
    {
        printf ("\r\n failure 1\n");
        CRU_BUF_Release_MsgBufChain (PimSaInfo.pGrpSrcAddrList, OSIX_TRUE);
        return OSIX_FAILURE;
    }

    PimSaInfo.pGrpSrcAddrList = CRU_BUF_Allocate_MsgBufChain (500, 0);
    PimSaInfo.u1CompId = 1;
    if (OSIX_FAILURE == MsdpPimIfHandleSAInfo (&PimSaInfo))
    {
        printf ("\r\n failure 2\n");
        CRU_BUF_Release_MsgBufChain (PimSaInfo.pGrpSrcAddrList, OSIX_TRUE);
        return OSIX_FAILURE;
    }

    PimSaInfo.pGrpSrcAddrList = CRU_BUF_Allocate_MsgBufChain (500, 0);
    Rp.MibObject.au1FsMsdpRPAddress[0] = 12;
    Rp.MibObject.au1FsMsdpRPAddress[3] = 12;
    Rp.MibObject.i4FsMsdpRPAddressLen = 4;
    Rp.MibObject.i4FsMsdpRPAddrType = 1;
    Rp.MibObject.i4FsMsdpRPOperStatus = MSDP_IF_OPERSTATUS_UP;
    MsdpFsMsdpRPTableCreateApi (&Rp);

    Addr.au1Addr[0] = 225;
    Addr.au1Addr[1] = 10;
    Addr.au1Addr[2] = 10;
    Addr.au1Addr[3] = 10;
    Addr.u1Afi = 1;
    Addr.u1AddrLen = 4;
    CRU_BUF_Copy_OverBufChain
        (PimSaInfo.pGrpSrcAddrList, (UINT1 *) &Addr, 0, 20);
    Addr.au1Addr[0] = 11;
    Addr.au1Addr[3] = 10;
    Addr.u1Afi = 1;
    Addr.u1AddrLen = 4;
    CRU_BUF_Copy_OverBufChain
        (PimSaInfo.pGrpSrcAddrList, (UINT1 *) &Addr, 20, 20);

    if (OSIX_FAILURE == MsdpPimIfHandleSAInfo (&PimSaInfo))
    {
        printf ("\r\n failure 3\n");
        CRU_BUF_Release_MsgBufChain (PimSaInfo.pGrpSrcAddrList, OSIX_TRUE);
        return OSIX_FAILURE;
    }

    PimSaInfo.pGrpSrcAddrList = CRU_BUF_Allocate_MsgBufChain (500, 0);
    PimSaInfo.pDataPkt = CRU_BUF_Allocate_MsgBufChain (50, 0);
    if (OSIX_FAILURE == MsdpPimIfHandleSAInfo (&PimSaInfo))
    {
        printf ("\r\n failure 4\n");
        CRU_BUF_Release_MsgBufChain (PimSaInfo.pGrpSrcAddrList, OSIX_TRUE);
        CRU_BUF_Release_MsgBufChain (PimSaInfo.pDataPkt, OSIX_TRUE);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
MsdpUtPim_3 (VOID)
{
    tMsdpMrpSaAdvt      MsdpSaInfo;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    INT4                i4Offset = 0;

    MsdpUtDelEntry ();
    MsdpSaInfo.pGrpSrcAddrList = CRU_BUF_Allocate_MsgBufChain (400, 0);
    GrpAddr.au1Addr[0] = 225;
    GrpAddr.au1Addr[1] = 10;
    GrpAddr.au1Addr[2] = 10;
    GrpAddr.au1Addr[15] = 10;
    GrpAddr.u1Afi = 2;
    GrpAddr.u1AddrLen = 16;

    SrcAddr.au1Addr[0] = 11;
    SrcAddr.au1Addr[15] = 10;
    SrcAddr.u1Afi = 2;
    SrcAddr.u1AddrLen = 16;
    MsdpPimIfFillSAEntry (&MsdpSaInfo, &GrpAddr, &SrcAddr, &i4Offset);
    CRU_BUF_Release_MsgBufChain (MsdpSaInfo.pGrpSrcAddrList, FALSE);
    /*while (i4Offset) 
       if (OSIX_FAILURE == MsdpPimIfFillSAEntry 
       (&MsdpSaInfo, &GrpAddr, &SrcAddr, &i4Offset))
       {
       CRU_BUF_Release_MsgBufChain (MsdpSaInfo.pGrpSrcAddrList, FALSE);
       break;
       } */
    MsdpSaInfo.pGrpSrcAddrList = CRU_BUF_Allocate_MsgBufChain (100, 0);
    MsdpPimIfFillSAEntry (&MsdpSaInfo, &GrpAddr, &SrcAddr, &i4Offset);
    CRU_BUF_Release_MsgBufChain (MsdpSaInfo.pGrpSrcAddrList, FALSE);
    return OSIX_SUCCESS;
}

INT4
MsdpUtPim_4 (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pHead[5000];
    tMsdpMrpSaAdvt      MsdpSAInfo;
    INT4                i4Index = 0;

    MsdpUtDelEntry ();
    MsdpPimIfAllocateBuf (&MsdpSAInfo, 100, 1);
    CRU_BUF_Release_MsgBufChain (MsdpSAInfo.pGrpSrcAddrList, OSIX_FALSE);
    for (i4Index = 0; i4Index < 4000; i4Index++)
    {
        MsdpPimIfAllocateBuf (&MsdpSAInfo, 1048, 1);
        if (NULL == (pHead[i4Index] = MsdpSAInfo.pGrpSrcAddrList))
            break;
    }
    for (; i4Index >= 0; i4Index--)
    {

        CRU_BUF_Release_MsgBufChain (pHead[i4Index], OSIX_FALSE);
    }

    return OSIX_SUCCESS;
}

INT4
MsdpUtPim_5 (VOID)
{
    tMsdpFsMsdpSACacheEntry CurCache;
    tIPvXAddr           GrpAddr;

    MsdpUtDelEntry ();
    MsdpPimGetNextCacheWithReqGrp (&CurCache, &GrpAddr);

    CurCache.MibObject.i4FsMsdpSACacheAddrType = 1;
    CurCache.MibObject.au1FsMsdpSACacheSourceAddr[0] = 10;
    CurCache.MibObject.au1FsMsdpSACacheSourceAddr[3] = 10;
    CurCache.MibObject.au1FsMsdpSACacheGroupAddr[0] = 224;
    CurCache.MibObject.au1FsMsdpSACacheGroupAddr[3] = 10;
    CurCache.MibObject.au1FsMsdpSACacheOriginRP[0] = 24;
    CurCache.MibObject.au1FsMsdpSACacheOriginRP[3] = 24;
    CurCache.MibObject.i4FsMsdpSACacheGroupAddrLen = 4;
    CurCache.MibObject.i4FsMsdpSACacheSourceAddrLen = 4;
    CurCache.MibObject.i4FsMsdpSACacheOriginRPLen = 4;
    MsdpFsMsdpSACacheTableCreateApi (&CurCache);
    GrpAddr.u1AddrLen = 4;

    MEMSET (&CurCache, 0, sizeof (CurCache));
    MsdpPimGetNextCacheWithReqGrp (&CurCache, &GrpAddr);

    CurCache.MibObject.i4FsMsdpSACacheAddrType = 0;
    CurCache.MibObject.au1FsMsdpSACacheSourceAddr[0] = 0;
    CurCache.MibObject.au1FsMsdpSACacheSourceAddr[3] = 0;
    CurCache.MibObject.au1FsMsdpSACacheGroupAddr[0] = 0;
    CurCache.MibObject.au1FsMsdpSACacheGroupAddr[3] = 0;
    CurCache.MibObject.au1FsMsdpSACacheOriginRP[0] = 0;
    CurCache.MibObject.au1FsMsdpSACacheOriginRP[3] = 0;
    GrpAddr.au1Addr[0] = 224;
    GrpAddr.au1Addr[3] = 10;
    GrpAddr.u1Afi = 1;
    MsdpPimGetNextCacheWithReqGrp (&CurCache, &GrpAddr);

    return OSIX_SUCCESS;
}

INT4
MsdpUtPim_6 (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pHead[5000];
    tMsdpFsMsdpSACacheEntry SaCache;
    tMsdpMrpSaAdvt      MsdpSAInfo;
    tMsdpSAReqGrp       ReqGrpAddr;
    INT4                i4Index = 0;

    MsdpUtDelEntry ();
    ReqGrpAddr.u1CompId = 2;
    MsdpPimIfHandleSAReq (&ReqGrpAddr);

    ReqGrpAddr.u1CompId = 1;
    for (i4Index = 0; i4Index < 4000; i4Index++)
    {
        MsdpPimIfAllocateBuf (&MsdpSAInfo, 1048, 1);
        if (NULL == (pHead[i4Index] = MsdpSAInfo.pGrpSrcAddrList))
            break;
    }
    MsdpPimIfHandleSAReq (&ReqGrpAddr);
    for (; i4Index >= 0; i4Index--)
    {

        CRU_BUF_Release_MsgBufChain (pHead[i4Index], OSIX_FALSE);
    }

    ReqGrpAddr.GrpAddr.u1Afi = 1;
    ReqGrpAddr.GrpAddr.u1AddrLen = 4;
    ReqGrpAddr.GrpAddr.au1Addr[0] = 224;
    ReqGrpAddr.GrpAddr.au1Addr[1] = 10;
    ReqGrpAddr.GrpAddr.au1Addr[2] = 10;
    ReqGrpAddr.GrpAddr.au1Addr[3] = 5;

    MsdpPimIfHandleSAReq (&ReqGrpAddr);
    SaCache.MibObject.i4FsMsdpSACacheAddrType = 1;
    SaCache.MibObject.i4FsMsdpSACacheGroupAddrLen = 4;
    SaCache.MibObject.au1FsMsdpSACacheGroupAddr[0] = 224;
    SaCache.MibObject.au1FsMsdpSACacheGroupAddr[1] = 10;
    SaCache.MibObject.au1FsMsdpSACacheGroupAddr[2] = 10;
    SaCache.MibObject.au1FsMsdpSACacheGroupAddr[3] = 5;
    MsdpFsMsdpSACacheTableCreateApi (&SaCache);
    MsdpPimIfHandleSAReq (&ReqGrpAddr);

    MsdpPimIfHandleSAReq (&ReqGrpAddr);
    return OSIX_SUCCESS;
}

/* msdpport UT cases */
INT4
MsdpUtPort_1 (VOID)
{
    UINT4               u4Addr = 0;
    UINT2               u2Vlan = 0;

    MsdpUtDelEntry ();
    if (OSIX_SUCCESS == MsdpPortGetVlanIdFromIpAddress (u4Addr, &u2Vlan))
    {
        printf ("Zero IP address\n");
        return OSIX_FAILURE;
    }

    CliVlanInit ();
    /* hexa decimal value for IP address 7.0.0.7 */
    u4Addr = 117440519;
    if (OSIX_SUCCESS != MsdpPortGetVlanIdFromIpAddress (u4Addr, &u2Vlan))
    {
        printf ("Vlan 2 IP address\n");
        CliVlanDeInit ();
        return OSIX_FAILURE;
    }
    if (2 != u2Vlan)
    {
        printf ("Received Vlan ID is wrong \n");
        CliVlanDeInit ();
        return OSIX_FAILURE;
    }
    CliVlanDeInit ();
    return OSIX_SUCCESS;
}

INT4
MsdpUtPort_2 (VOID)
{
    UINT4               u4Addr;

    MsdpUtDelEntry ();
    if (MSDP_INVALID != MsdpPortIsValidLocalAddress ((UINT1 *) &u4Addr))
    {
        printf ("Zero IP address\n");
        return OSIX_FAILURE;
    }

    CliVlanInit ();
    /* hexa decimal value for IP address 7.0.0.7 */
    u4Addr = 117440519;
    if (MSDP_INVALID == MsdpPortIsValidLocalAddress ((UINT1 *) &u4Addr))
    {
        printf ("Vlan 2 IP address\n");
        CliVlanDeInit ();
        return OSIX_FAILURE;
    }
    CliVlanDeInit ();
    return OSIX_SUCCESS;
}

INT4
MsdpUtPort_3 (VOID)
{
    tMsdpMrpSaAdvt      MsdpSaInfo;
    tIPvXAddr           Addr;

    MsdpUtDelEntry ();
    MsdpSaInfo.u1CompId = 1;
    MsdpSaInfo.u2SrcCount = 1;
    MsdpSaInfo.u2DataPktLen = 0;
    MsdpSaInfo.u1AddrType = 1;
    MsdpSaInfo.pDataPkt = NULL;
    MsdpSaInfo.pGrpSrcAddrList = CRU_BUF_Allocate_MsgBufChain (100, 0);

    Addr.u1Afi = 1;
    Addr.u1AddrLen = 4;
    MEMSET (Addr.au1Addr, 4, 4);

    CRU_BUF_Copy_OverBufChain (MsdpSaInfo.pGrpSrcAddrList,
                               (UINT1 *) &Addr, 0, 20);
    MEMSET (Addr.au1Addr, 6, 4);
    CRU_BUF_Copy_OverBufChain (MsdpSaInfo.pGrpSrcAddrList,
                               (UINT1 *) &Addr, 20, 20);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("set ip pim enable");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    gMsdpGlobals.aMsdpRegister[0].u1ProtId = PIM_ID;
    gMsdpGlobals.aMsdpRegister[0].pUpdateSrcActiveInfo = NULL;
    MsdpPortInformSAToMrp (&MsdpSaInfo);
    gMsdpGlobals.aMsdpRegister[0].u1ProtId = PIM_ID;
    gMsdpGlobals.aMsdpRegister[0].pUpdateSrcActiveInfo = &PimPortHandleSAInfo;
    MsdpPortInformSAToMrp (&MsdpSaInfo);
    return OSIX_SUCCESS;
}

INT4
MsdpUtPort_4 (VOID)
{
    UINT4               u4Port;

    MsdpUtDelEntry ();
    if (OSIX_FAILURE != MsdpPortGetIpPortFromIfIndex (0, &u4Port))
    {
        printf ("Index-0 \n");
        return OSIX_FAILURE;
    }
    if (OSIX_FAILURE != MsdpPortGetIpPortFromIfIndex (-1, &u4Port))
    {
        printf ("Index-(-1) and Port-Valid address passed\n");
        return OSIX_FAILURE;
    }
    if (MSDP_INVALID != (INT4) u4Port)
    {
        printf ("Got Valid Port for Invalid Index\n");
        return OSIX_FAILURE;
    }
    CliVlanInit ();
    if (OSIX_FAILURE == MsdpPortGetIpPortFromIfIndex (34, &u4Port))
    {
        printf ("Index 34 and Port valid adddress\n");
        CliVlanDeInit ();
        return OSIX_FAILURE;
    }
    if (MSDP_INVALID == (INT4) u4Port)
    {
        printf ("Got InValid Port for Valid Index\n");
        CliVlanDeInit ();
        return OSIX_FAILURE;
    }
    CliVlanDeInit ();
    return OSIX_SUCCESS;
}

INT4
MsdpUtPort_5 (VOID)
{
    tRtInfoQueryMsg     RtQueryInfo;
    tNetIpv4RtInfo      RtInfo;
    UINT1               au1Addr[4];

    MEMSET (&RtInfo, NULL, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQueryInfo, NULL, sizeof (tRtInfoQueryMsg));
    MsdpUtDelEntry ();
    CliVlanInit ();
    au1Addr[0] = 7;
    au1Addr[1] = 0;
    au1Addr[2] = 0;
    au1Addr[3] = 7;

    PTR_FETCH4 (RtQueryInfo.u4DestinationIpAddress, au1Addr);
    RtQueryInfo.u4DestinationSubnetMask = MSDP_DEF_SRC_MASK_ADDR;
    RtQueryInfo.u2AppIds = 0;
    RtQueryInfo.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
    RtQueryInfo.u4ContextId = MSDP_DEF_VRF_CTXT_ID;
    MsdpPortIpGetRoute (&RtQueryInfo, &RtInfo);

    CliVlanDeInit ();
    MsdpPortIpGetRoute (&RtQueryInfo, &RtInfo);
    return OSIX_SUCCESS;
}

INT4
MsdpUtPort_6 (VOID)
{
    tMsdpQueMsg        *pMsdpQueMsg[50];
    tMsdpMrpSaAdvt      MsdpSaAdvt;
    tIPvXAddr           Addr;
    INT4                i4Index = 0;

    MsdpUtDelEntry ();
    MEMSET (pMsdpQueMsg, NULL, sizeof (pMsdpQueMsg));
    MsdpSaAdvt.pDataPkt = NULL;
    MsdpSaAdvt.pGrpSrcAddrList = CRU_BUF_Allocate_MsgBufChain (100, 0);
    Addr.u1Afi = 1;
    Addr.u1AddrLen = 4;
    MEMSET (Addr.au1Addr, 4, 4);
    MsdpPortHandleSAInfo (&MsdpSaAdvt);
    for (i4Index = 0; i4Index < MAX_MSDP_QUEUE_MSGS; i4Index++)
    {
        pMsdpQueMsg[i4Index] = MsdpQueAllocQueMsg ();

        if (pMsdpQueMsg[i4Index] == NULL)
        {
            i4Index--;
            break;
        }
    }

    MsdpSaAdvt.pGrpSrcAddrList = CRU_BUF_Allocate_MsgBufChain (100, 0);
    CRU_BUF_Copy_OverBufChain (MsdpSaAdvt.pGrpSrcAddrList,
                               (UINT1 *) &Addr, 0, 20);
    MEMSET (Addr.au1Addr, 6, 4);
    CRU_BUF_Copy_OverBufChain (MsdpSaAdvt.pGrpSrcAddrList,
                               (UINT1 *) &Addr, 20, 20);

    MsdpPortHandleSAInfo (&MsdpSaAdvt);
    for (; i4Index >= 0; i4Index--)
    {
        MemReleaseMemBlock (MSDP_QUE_MEMPOOL_ID,
                            (UINT1 *) pMsdpQueMsg[i4Index]);
    }
    sleep (5);

    return OSIX_SUCCESS;
}

INT4
MsdpUtPort_7 (VOID)
{
    tMsdpSAReqGrp       MsdpSaReqGrp;
    tMsdpQueMsg        *pMsdpQueMsg[MAX_MSDP_QUEUE_MSGS];
    INT4                i4Index = 0;

    MsdpUtDelEntry ();
    MEMSET (pMsdpQueMsg, NULL, sizeof (pMsdpQueMsg));
    MEMSET (MsdpSaReqGrp.GrpAddr.au1Addr, 4, 4);
    MsdpSaReqGrp.u1AddrType = 1;
    MsdpSaReqGrp.u1CompId = 1;
    MsdpPortHandleSARequest (&MsdpSaReqGrp);
    for (i4Index = 0; i4Index < MAX_MSDP_QUEUE_MSGS; i4Index++)
    {
        pMsdpQueMsg[i4Index] = MsdpQueAllocQueMsg ();
        if (pMsdpQueMsg[i4Index] == NULL)
        {
            i4Index--;
            break;
        }
    }

    MsdpPortHandleSARequest (&MsdpSaReqGrp);
    for (; i4Index >= 0; i4Index--)
    {
        MemReleaseMemBlock (MSDP_QUE_MEMPOOL_ID,
                            (UINT1 *) pMsdpQueMsg[i4Index]);
    }

    sleep (5);
    MsdpPortHandleSARequest (&MsdpSaReqGrp);
    return OSIX_SUCCESS;
}

INT4
MsdpUtPort_8 (VOID)
{
    MsdpUtDelEntry ();
    MsdpPortRegisterMrp (139, (VOID *) &MsdpPortHandleSAInfo);
    MsdpPortRegisterMrp (PIM_ID, (VOID *) &MsdpPortHandleSAInfo);
    MsdpPortRegisterMrp (10, (VOID *) &MsdpPortHandleSAInfo);
    MsdpPortRegisterMrp (0, NULL);
    return OSIX_SUCCESS;
}

INT4
MsdpUtPort_9 (VOID)
{

    MsdpUtDelEntry ();

    MsdpPortRegisterMrp (PIM_ID, (VOID *) &MsdpPortHandleSAInfo);
    MsdpPortDeRegisterMrp (PIM_ID);
    MsdpPortRegisterMrp (PIM_ID, (VOID *) &MsdpPortHandleSAInfo);
    MsdpPortRegisterMrp (11, (VOID *) &PimPortHandleSAInfo);
    MsdpPortRegisterMrp (12, (VOID *) &PimPortHandleSAInfo);
    MsdpPortDeRegisterMrp (139);
    MsdpPortDeRegisterMrp (20);

    return OSIX_SUCCESS;
}

INT4
MsdpUtPort_10 (VOID)
{
    MsdpUtDelEntry ();
    if (OSIX_FAILURE == MsdpPortRegiterWithIP ())
    {
        printf ("Registering with IP \n");
        return OSIX_FAILURE;
    }
    if (OSIX_FAILURE != MsdpPortRegiterWithIP ())
    {
        printf ("Again Registering with IP \n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
MsdpUtPort_11 (VOID)
{
    MsdpUtDelEntry ();
    MsdpPortDeRegiterWithIP ();
    return OSIX_SUCCESS;
}

INT4
MsdpUtPort_12 (VOID)
{
    tNetIpv4IfInfo      NetIfInfo;
    UINT4               u4BitMap;

    tMsdpQueMsg        *pMsdpQueMsg[MAX_MSDP_QUEUE_MSGS];
    INT4                i4Index = 0;

    MsdpUtDelEntry ();
    MEMSET (pMsdpQueMsg, NULL, sizeof (pMsdpQueMsg));
    NetIfInfo.u4IfIndex = 33;
    NetIfInfo.u4Oper = 1;
    NetIfInfo.u4Admin = 1;
    u4BitMap = 32;
    MsdpPortHandleIfChg (&NetIfInfo, u4BitMap);
    for (i4Index = 0; i4Index < MAX_MSDP_QUEUE_MSGS; i4Index++)
    {
        pMsdpQueMsg[i4Index] = MsdpQueAllocQueMsg ();
        if (pMsdpQueMsg[i4Index] == NULL)
        {
            i4Index--;
            break;
        }
    }

    MsdpPortHandleIfChg (&NetIfInfo, u4BitMap);

    for (; i4Index >= 0; i4Index--)
    {
        MemReleaseMemBlock (MSDP_QUE_MEMPOOL_ID,
                            (UINT1 *) pMsdpQueMsg[i4Index]);
    }
    sleep (5);
    MsdpPortHandleIfChg (&NetIfInfo, u4BitMap);
    return OSIX_SUCCESS;
}

INT4
MsdpUtPort_13 (VOID)
{
    UINT4               u4IfIndex = 33;
    UINT1               u1OperStatus = 0;
    MsdpUtDelEntry ();
    MsdpPortGetIfInfo (u4IfIndex, &u1OperStatus);
    return OSIX_SUCCESS;
}

INT4
MsdpUtPort_14 (VOID)
{
    UINT4               u4RtrId;

    MsdpUtDelEntry ();
    MsdpPortGetRouterId (0, &u4RtrId);
    return OSIX_SUCCESS;
}

/* msdpque UT cases */
INT4
MsdpUtQue_1 (VOID)
{
    return -1;
}

INT4
MsdpUtQue_2 (VOID)
{
    return -1;
}

INT4
MsdpUtQue_3 (VOID)
{
    return -1;
}

/* msdpsock UT cases */
INT4
MsdpUtSock_1 (VOID)
{
    MsdpSockServerStart (639);
    MsdpSockServerStart (639);
    return OSIX_SUCCESS;
}

INT4
MsdpUtSock_2 (VOID)
{
    MsdpSock6ServerStart (639);
    MsdpSock6ServerStart (639);
    return OSIX_SUCCESS;
}

INT4
MsdpUtSock_3 (VOID)
{
    MsdpSockServerStop (1);
    MsdpSockServerStop (2);
    return OSIX_SUCCESS;
}

INT4
MsdpUtSock_4 (VOID)
{
    INT4                i4TskId = gMsdpGlobals.msdpTaskId;
    MsdpSockV4NewPassiveConnCallBk (1);
    gMsdpGlobals.msdpTaskId = -1;
    MsdpSockV4NewPassiveConnCallBk (1);
    gMsdpGlobals.msdpTaskId = i4TskId;
    return OSIX_SUCCESS;
}

INT4
MsdpUtSock_5 (VOID)
{
    INT4                i4TskId = gMsdpGlobals.msdpTaskId;
    gMsdpGlobals.msdpTaskId = -1;
    MsdpSockV6NewPassiveConnCallBk (1);
    gMsdpGlobals.msdpTaskId = i4TskId;
    MsdpSockV6NewPassiveConnCallBk (1);
    MsdpSockV6NewPassiveConnCallBk (-1);
    return OSIX_SUCCESS;
}

INT4
MsdpUtSock_6 (VOID)
{
    MsdpSockHandleNewPassiveConnEvt (1);
    MsdpSockHandleNewPassiveConnEvt (-1);
    return OSIX_SUCCESS;
}

INT4
MsdpUtSock_7 (VOID)
{
    MsdpSock6HandleNewPassiveConnEvt (1);
    MsdpSock6HandleNewPassiveConnEvt (-1);
    return OSIX_SUCCESS;
}

INT4
MsdpUtSock_8 (VOID)
{
    tIPvXAddr           PeerAddr;
    INT4                i4ClientSockFd = 0;

    MsdpSockAcceptNewConnection (1, &i4ClientSockFd, &PeerAddr);
    return OSIX_SUCCESS;
}

INT4
MsdpUtSock_9 (VOID)
{
    tIPvXAddr           PeerAddr;
    INT4                i4ClientSockFd = 0;
    MsdpSock6AcceptNewConnection (1, &i4ClientSockFd, &PeerAddr);
    return OSIX_SUCCESS;
}

INT4
MsdpUtSock_10 (VOID)
{
    tMsdpFsMsdpPeerEntry PeerEntry;

    PeerEntry.i4SockFd = 1;
    PeerEntry.MibObject.i4FsMsdpPeerAddrType = 1;
    PeerEntry.MibObject.au1FsMsdpPeerRemoteAddress[0] = 10;
    PeerEntry.MibObject.au1FsMsdpPeerRemoteAddress[1] = 0;
    PeerEntry.MibObject.au1FsMsdpPeerRemoteAddress[2] = 0;
    PeerEntry.MibObject.au1FsMsdpPeerRemoteAddress[3] = 10;
    MsdpSockOpenPeerConnection (&PeerEntry);

    PeerEntry.i4SockFd = -1;
    MsdpSockOpenPeerConnection (&PeerEntry);
    MsdpUtDelEntry ();
    return OSIX_SUCCESS;
}

INT4
MsdpUtSock_11 (VOID)
{
    tMsdpFsMsdpPeerEntry PeerEntry;

    CliVlanInit ();
    PeerEntry.i4SockFd = 1;
    PeerEntry.MibObject.i4FsMsdpPeerAddrType = 2;
    PeerEntry.MibObject.au1FsMsdpPeerRemoteAddress[0] = 7;
    PeerEntry.MibObject.au1FsMsdpPeerRemoteAddress[1] = 7;
    PeerEntry.MibObject.au1FsMsdpPeerRemoteAddress[14] = 7;
    PeerEntry.MibObject.au1FsMsdpPeerRemoteAddress[15] = 10;
    MsdpSock6OpenPeerConnection (&PeerEntry);

    PeerEntry.i4SockFd = -1;
    PeerEntry.MibObject.i4FsMsdpPeerAddrType = 2;
    PeerEntry.MibObject.au1FsMsdpPeerRemoteAddress[0] = 7;
    PeerEntry.MibObject.au1FsMsdpPeerRemoteAddress[1] = 7;
    PeerEntry.MibObject.au1FsMsdpPeerRemoteAddress[14] = 7;
    PeerEntry.MibObject.au1FsMsdpPeerRemoteAddress[15] = 9;
    MsdpSock6OpenPeerConnection (&PeerEntry);
    CliVlanDeInit ();

    return OSIX_SUCCESS;
}

INT4
MsdpUtSock_12 (VOID)
{
    tOsixQId            i4QId = gMsdpGlobals.msdpQueId;
    INT4                i4TskId = gMsdpGlobals.msdpTaskId;

    MsdpSockNewActiveConnCallBk (1);

    MsdpSockNewActiveConnCallBk (1);
    gMsdpGlobals.msdpQueId = i4QId;

    gMsdpGlobals.msdpTaskId = -1;
    MsdpSockNewActiveConnCallBk (1);
    gMsdpGlobals.msdpTaskId = i4TskId;
    return OSIX_SUCCESS;
}

INT4
MsdpUtSock_13 (VOID)
{
    MsdpSockHandleNewActiveConnEvt (1);
    return OSIX_SUCCESS;
}

INT4
MsdpUtSock_14 (VOID)
{
    tMsdpFsMsdpPeerEntry PeerEntry;
    MsdpSockClosePeerConnection (&PeerEntry);
    return OSIX_SUCCESS;
}

INT4
MsdpUtSock_15 (VOID)
{
    UINT1               au1Data[100];
    INT4                i4WrBytes = 0;

    MsdpSockSend (0, au1Data, 100, &i4WrBytes);
    MsdpSockSend (-1, au1Data, 100, &i4WrBytes);
    return OSIX_SUCCESS;
}

INT4
MsdpUtSock_16 (VOID)
{
    /*UINT1 au1Data[100];
       INT4 i4RdBytes = 0;

       MsdpSockRcv (0, au1Data, 100, &i4RdBytes);
       MsdpSockRcv (-1, au1Data, 100, &i4RdBytes);
       return  OSIX_SUCCESS; */
    return -1;
}

INT4
MsdpUtSock_17 (VOID)
{
    tOsixQId            i4QId = gMsdpGlobals.msdpQueId;
    INT4                i4TskId = gMsdpGlobals.msdpTaskId;

    MsdpSockIndicateSockReadEvt (-1);
    MsdpSockIndicateSockReadEvt (0);
    MsdpSockIndicateSockReadEvt (2);

    MsdpSockIndicateSockReadEvt (2);
    gMsdpGlobals.msdpQueId = i4QId;

    gMsdpGlobals.msdpTaskId = -1;
    MsdpSockIndicateSockReadEvt (2);
    gMsdpGlobals.msdpTaskId = i4TskId;

    return OSIX_SUCCESS;
}

INT4
MsdpUtSock_18 (VOID)
{
    tOsixQId            i4QId = gMsdpGlobals.msdpQueId;
    INT4                i4TskId = gMsdpGlobals.msdpTaskId;

    MsdpSockIndicateSockWriteEvt (0);
    MsdpSockIndicateSockWriteEvt (2);
    MsdpSockIndicateSockWriteEvt (-1);

    MsdpSockIndicateSockWriteEvt (2);
    gMsdpGlobals.msdpQueId = i4QId;

    gMsdpGlobals.msdpTaskId = -1;
    MsdpSockIndicateSockWriteEvt (2);
    gMsdpGlobals.msdpTaskId = i4TskId;

    return OSIX_SUCCESS;
}

INT4
MsdpUtSock_19 (VOID)
{
    /* tMsdpFsMsdpPeerEntry Peer;

       Peer.i4SockFd = 0;
       Peer.MibObject.i4FsMsdpPeerAddrType = 1;
       Peer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 10;
       Peer.MibObject.au1FsMsdpPeerRemoteAddress[1] = 0;
       Peer.MibObject.au1FsMsdpPeerRemoteAddress[2] = 0;
       Peer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 10;
       MsdpFsMsdpPeerTableCreateApi (&Peer);
       MsdpSockHandleSockReadEvt (0);

       Peer.i4SockFd = 2;
       Peer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 10;
       Peer.MibObject.au1FsMsdpPeerRemoteAddress[1] = 0;
       Peer.MibObject.au1FsMsdpPeerRemoteAddress[2] = 0;
       Peer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 10;
       MsdpSockHandleSockReadEvt (2);

       Peer.i4SockFd = -1;
       Peer.MibObject.au1FsMsdpPeerRemoteAddress[0] = 10;
       Peer.MibObject.au1FsMsdpPeerRemoteAddress[1] = 0;
       Peer.MibObject.au1FsMsdpPeerRemoteAddress[2] = 0;
       Peer.MibObject.au1FsMsdpPeerRemoteAddress[3] = 10;
       MsdpSockHandleSockReadEvt (-1); */
    MsdpUtDelEntry ();
    return OSIX_SUCCESS;
}

INT4
MsdpUtSock_20 (VOID)
{
    MsdpSockHandleSockWriteEvt (0);
    MsdpSockHandleSockWriteEvt (1);
    MsdpSockHandleSockWriteEvt (-1);
    return OSIX_SUCCESS;
}

INT4
MsdpUtSock_21 (VOID)
{
    tMsdpTcpMsg         MsdpTcpMsg;

    MsdpTcpMsg.i4Cmd = MSDP_SKT_READ_READY;
    MsdpTcpMsg.i4SockFd = 1;
    MsdpSockHandleSockReadWriteEvt (&MsdpTcpMsg);

    MsdpTcpMsg.i4Cmd = MSDP_SKT_WRITE_READY;
    MsdpTcpMsg.i4SockFd = 1;
    MsdpSockHandleSockReadWriteEvt (&MsdpTcpMsg);

    MsdpTcpMsg.i4Cmd = MSDP_SKT_READ_READY;
    MsdpTcpMsg.i4SockFd = -1;
    MsdpSockHandleSockReadWriteEvt (&MsdpTcpMsg);

    return OSIX_SUCCESS;
}

/* msdpsz UT cases */
INT4
MsdpUtSz_1 (VOID)
{
    return -1;
}

INT4
MsdpUtSz_2 (VOID)
{
    return -1;
}

INT4
MsdpUtSz_3 (VOID)
{
    return -1;
}

/* msdptask UT cases */
INT4
MsdpUtTask_1 (VOID)
{
    return -1;
}

/* msdptmr UT cases */
INT4
MsdpUtTmr_1 (VOID)
{
    MsdpTmrInitTmrDesc ();
    sleep (2);
    return OSIX_SUCCESS;
}

INT4
MsdpUtTmr_2 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;
    MsdpTmrStartTmr (&Peer.MsdpPeerKATimer, MSDP_KEEP_ALIVE_TMR, (UINT4) 100);
    MsdpTmrStopTmr (&Peer.MsdpPeerKATimer);
    MsdpTmrStartTmr (&Peer.MsdpPeerKATimer, MSDP_KEEP_ALIVE_TMR, (UINT4) 99999);
    MsdpTmrStopTmr (&Peer.MsdpPeerKATimer);
    sleep (2);
    return OSIX_SUCCESS;
}

INT4
MsdpUtTmr_3 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;
    MsdpTmrRestartTmr (&Peer.MsdpPeerKATimer, MSDP_KEEP_ALIVE_TMR, (UINT4) 100);
    MsdpTmrStopTmr (&Peer.MsdpPeerKATimer);
    sleep (2);
    return OSIX_SUCCESS;
}

INT4
MsdpUtTmr_4 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;
    MsdpTmrStopTmr (&Peer.MsdpPeerKATimer);
    sleep (2);
    return OSIX_SUCCESS;
}

INT4
MsdpUtTmr_5 (VOID)
{
    MsdpTmrStartTmr (&gMsdpGlobals.MsdpSaAdvtTimer, MSDP_SA_BATCH_ADVT_EVT,
                     (UINT4) 1);
    MsdpTmrHandleExpiry ();
    sleep (2);

    return OSIX_SUCCESS;
}

INT4
MsdpUtTmr_6 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;
    Peer.i4SockFd = 100;
    MsdpTmrConnRetryExpiryHdlr ((VOID *) &Peer);
    MsdpTmrStopTmr (&Peer.MsdpPeerConnRetryTimer);
    MsdpUtDelEntry ();
    sleep (2);
    return OSIX_SUCCESS;
}

INT4
MsdpUtTmr_7 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;

    Peer.MibObject.i4FsMsdpPeerAddrType = 1;
    MsdpTmrHoldTimerExpiryHdlr ((VOID *) &Peer);
    MsdpTmrStopTmr (&Peer.MsdpPeerHoldTimer);
    MsdpUtDelEntry ();
    sleep (2);
    return OSIX_SUCCESS;
}

INT4
MsdpUtTmr_8 (VOID)
{
    tMsdpFsMsdpPeerEntry Peer;

    Peer.MibObject.i4FsMsdpPeerAddrType = 1;
    MsdpTmrKeepAliveTmrExpiryHdlr ((VOID *) &Peer);
    MsdpTmrStopTmr (&Peer.MsdpPeerKATimer);
    MsdpUtDelEntry ();
    sleep (2);
    return OSIX_SUCCESS;
}

INT4
MsdpUtTmr_9 (VOID)
{

    tMsdpFsMsdpSACacheEntry Cache;
    tMsdpFsMsdpSACacheEntry *pCache = NULL;
    Cache.MibObject.i4FsMsdpSACacheAddrType = 1;
    Cache.MibObject.i4FsMsdpSACacheGroupAddrLen = 4;
    Cache.MibObject.i4FsMsdpSACacheOriginRPLen = 4;
    Cache.MibObject.i4FsMsdpSACacheSourceAddrLen = 4;
    pCache = MsdpFsMsdpSACacheTableCreateApi (&Cache);

    MsdpTmrStartTmr (&pCache->CacheSaStateTimer, MSDP_CACHE_STATE_TMR, 5);
    MsdpTmrStopTmr (&pCache->CacheSaStateTimer);
    MsdpTmrCacheStateTimerExpHdlr ((VOID *) pCache);

    return OSIX_SUCCESS;
}

INT4
MsdpUtTmr_10 (VOID)
{
    MsdpUtDelEntry ();
    return OSIX_SUCCESS;
}

/* msdptrc UT cases */
INT4
MsdpUtTrc_1 (VOID)
{
    return -1;
}

INT4
MsdpUtTrc_2 (VOID)
{
    return -1;
}

INT4
MsdpUtTrc_3 (VOID)
{
    return -1;
}

/* msdputil UT cases */
INT4
MsdpUtUtil_1 (VOID)
{
    tMsdpEventTrap      TrapEvent;
    MEMSET (&TrapEvent, NULL, sizeof (tMsdpEventTrap));

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("as-num 1");
    CliExecuteAppCmd ("router-id 10.0.0.1 ");
    CliExecuteAppCmd ("exit");
    MGMT_LOCK ();
    CliGiveAppContext ();

    TrapEvent.i4AddrType = IPVX_ADDR_FMLY_IPV4;
    TrapEvent.u4EventId = 1;

    MsdpUtilSendEventTrap (&TrapEvent, MSDP_ESTABLISHED_TRAP_ID);

    TrapEvent.i4AddrType = IPVX_ADDR_FMLY_IPV4;
    TrapEvent.u4EventId = 1;

    MsdpUtilSendEventTrap (&TrapEvent, MSDP_BACKWARD_TRANSITION_TRAP_ID);

    TrapEvent.i4AddrType = IPVX_ADDR_FMLY_IPV4;
    TrapEvent.u4EventId = 1;

    MsdpUtilSendEventTrap (&TrapEvent, MSDP_RP_OPERSTATUS_TRAP_ID);

    TrapEvent.i4AddrType = IPVX_ADDR_FMLY_IPV6;
    TrapEvent.u4EventId = 1;

    MsdpUtilSendEventTrap (&TrapEvent, MSDP_ESTABLISHED_TRAP_ID);

    TrapEvent.i4AddrType = IPVX_ADDR_FMLY_IPV6;
    TrapEvent.u4EventId = 1;

    MsdpUtilSendEventTrap (&TrapEvent, MSDP_BACKWARD_TRANSITION_TRAP_ID);

    TrapEvent.i4AddrType = IPVX_ADDR_FMLY_IPV6;
    TrapEvent.u4EventId = 1;

    MsdpUtilSendEventTrap (&TrapEvent, MSDP_RP_OPERSTATUS_TRAP_ID);
    return OSIX_SUCCESS;
}

INT4
MsdpUtUtil_2 (VOID)
{
    tMsdpEventTrap      TrapEvent;
    MEMSET (&TrapEvent, 0, sizeof (tMsdpEventTrap));
    TrapEvent.i4AddrType = IPVX_ADDR_FMLY_IPV4;
    TrapEvent.u4EventId = 1;
    TrapEvent.au1RtrId[0] = 10;
    TrapEvent.au1RtrId[1] = 0;
    TrapEvent.au1RtrId[2] = 0;
    TrapEvent.au1RtrId[3] = 1;

    MsdpUtDelEntry ();
    MsdpUtilSnmpIfSendTrap (4, (VOID *) &TrapEvent);
    MsdpUtilSnmpIfSendTrap (MSDP_ESTABLISHED_TRAP_ID, (VOID *) &TrapEvent);
    MsdpUtilSnmpIfSendTrap (MSDP_BACKWARD_TRANSITION_TRAP_ID,
                            (VOID *) &TrapEvent);
    MsdpUtilSnmpIfSendTrap (MSDP_RP_OPERSTATUS_TRAP_ID, (VOID *) &TrapEvent);

    TrapEvent.i4AddrType = IPVX_ADDR_FMLY_IPV6;

    MsdpUtilSnmpIfSendTrap (MSDP_ESTABLISHED_TRAP_ID, (VOID *) &TrapEvent);

    MsdpUtilSnmpIfSendTrap (MSDP_BACKWARD_TRANSITION_TRAP_ID,
                            (VOID *) &TrapEvent);
    MsdpUtilSnmpIfSendTrap (MSDP_RP_OPERSTATUS_TRAP_ID, (VOID *) &TrapEvent);
    MsdpUtilSnmpIfSendTrap (4, (VOID *) &TrapEvent);
    TrapEvent.i4AddrType = 3;

    MsdpUtilSnmpIfSendTrap (MSDP_ESTABLISHED_TRAP_ID, (VOID *) &TrapEvent);

    return OSIX_SUCCESS;
}

INT4
MsdpUtUtil_3 (VOID)
{
    return -1;
}

INT4
MsdpUtUtil_4 (VOID)
{
    return -1;
}

INT4
MsdpUtUtil_5 (VOID)
{
    MsdpUtilInitGlobals ();
    return OSIX_SUCCESS;
}

INT4
MsdpUtUtil_6 (VOID)
{
    tMsdpFsMsdpSACacheEntry *pSaCache = NULL;
    tMsdpFsMsdpSACacheEntry OldSaCache;

    MEMSET (&OldSaCache, NULL, sizeof (tMsdpFsMsdpSACacheEntry));

    MEMSET (OldSaCache.MibObject.au1FsMsdpSACacheGroupAddr, 223, 16);
    MEMSET (OldSaCache.MibObject.au1FsMsdpSACacheSourceAddr, 23, 16);
    MEMSET (OldSaCache.MibObject.au1FsMsdpSACacheOriginRP, 13, 16);
    OldSaCache.MibObject.i4FsMsdpSACacheGroupAddrLen = 4;
    OldSaCache.MibObject.i4FsMsdpSACacheSourceAddrLen = 4;
    OldSaCache.MibObject.i4FsMsdpSACacheOriginRPLen = 4;

    pSaCache = MsdpFsMsdpSACacheTableCreateApi (&OldSaCache);
    MsdpUtilRemoveCacheEntry (pSaCache);
    tRBTree             RbTree = gMsdpGlobals.MsdpGlbMib.FsMsdpSACacheTable;
    gMsdpGlobals.MsdpGlbMib.FsMsdpSACacheTable = 0;
    MsdpUtilRemoveCacheEntry (pSaCache);
    MsdpUtilRemoveCacheEntry (NULL);
    gMsdpGlobals.MsdpGlbMib.FsMsdpSACacheTable = RbTree;
    return OSIX_SUCCESS;
}

INT4
MsdpUtUtil_7 (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pDataPkt = NULL;
    tCRU_BUF_CHAIN_HEADER *pDataPkt1 = NULL;
    UINT1               au1LinBuf[50000];
    UINT2               u2Len = 50000;

    MsdpUtDelEntry ();
    MEMSET (au1LinBuf, 1, 50000);
    pDataPkt = CRU_BUF_Allocate_MsgBufChain (50000, 0);
    pDataPkt1 = CRU_BUF_Allocate_MsgBufChain (50000, 0);
    MsdpUtilCopyOverBufChain (&pDataPkt, au1LinBuf, 0, &u2Len);
    MsdpUtilCopyOverBufChain (&pDataPkt, au1LinBuf, 50000, &u2Len);
    return OSIX_SUCCESS;
}

INT4
MsdpUtUtil_8 (VOID)
{

    MsdpUtDelEntry ();
    if (OSIX_FALSE != MsdpUtilIsBuffSizeReached (IPVX_ADDR_FMLY_IPV4, 0, 20))
    {
        printf ("IPV4 fmly - offset 0 - Bufsize 20\n");
        return OSIX_FAILURE;
    }
    if (OSIX_FALSE != MsdpUtilIsBuffSizeReached (IPVX_ADDR_FMLY_IPV6, 0, 56))
    {
        printf ("IPV6 fmly - offset 0 - Bufsize 56\n");
        return OSIX_FAILURE;
    }
    if (OSIX_FALSE == MsdpUtilIsBuffSizeReached (IPVX_ADDR_FMLY_IPV4, 0, 12))
    {
        printf ("IPV4 fmly - offset 0 - Bufsize 12\n");
        return OSIX_FAILURE;
    }
    if (OSIX_FALSE == MsdpUtilIsBuffSizeReached (IPVX_ADDR_FMLY_IPV6, 0, 12))
    {
        printf ("IPV6 fmly - offset 0 - Bufsize 12\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
MsdpUtUtil_9 (VOID)
{
    INT4                i4Output = 0;

    MsdpUtDelEntry ();
    if (OSIX_FAILURE ==
        MsdpUtilGetTxPktSize (0, IPVX_ADDR_FMLY_IPV4, &i4Output))
    {
        printf ("msg-0, ipv4 fmly \n");
        return OSIX_FAILURE;
    }
    if (OSIX_FAILURE ==
        MsdpUtilGetTxPktSize (0, IPVX_ADDR_FMLY_IPV6, &i4Output))
    {
        printf ("msg-0, ipv6 fmly \n");
        return OSIX_FAILURE;
    }
    if (OSIX_FAILURE !=
        MsdpUtilGetTxPktSize (9 * 1024, IPVX_ADDR_FMLY_IPV4, &i4Output))
    {
        printf ("msg-max size, ipv4 fmly \n");
        return OSIX_FAILURE;
    }
    if (OSIX_FAILURE !=
        MsdpUtilGetTxPktSize (9 * 1024, IPVX_ADDR_FMLY_IPV6, &i4Output))
    {
        printf ("msg-max size, ipv6 fmly \n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
MsdpUtUtil_10 (VOID)
{
    UINT1               u1Addr = 0;
    MsdpUtDelEntry ();
    if (MSDP_INVALID != MsdpUtilIsValidLocalAddress
        (&u1Addr, IPVX_IPV4_ADDR_LEN))
    {
        printf ("Addr NULL and Len IPV4\n");
        return OSIX_FAILURE;
    }
    if (MSDP_INVALID != MsdpUtilIsValidLocalAddress
        (&u1Addr, IPVX_IPV6_ADDR_LEN))
    {
        printf ("Addr NULL and Len IPV6\n");
        return OSIX_FAILURE;
    }
    if (MSDP_INVALID != MsdpUtilIsValidLocalAddress (&u1Addr, 3))
    {
        printf ("Addr NULL and Len Invalid\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
MsdpUtUtil_11 (VOID)
{

    UINT1               au1Addr[16];

    MEMSET (au1Addr, 0, 16);

    MsdpUtDelEntry ();
    if (OSIX_FALSE == MsdpUtilIsAllZeros (au1Addr, IPVX_ADDR_FMLY_IPV4))
    {
        printf ("Zero address - IPV4 Fmly\n");
        return OSIX_FAILURE;
    }
    MEMSET (au1Addr, 0, 16);
    if (OSIX_FALSE == MsdpUtilIsAllZeros (au1Addr, IPVX_ADDR_FMLY_IPV6))
    {
        printf ("Zero address - IPV6 Fmly\n");
        return OSIX_FAILURE;
    }

    au1Addr[0] = 10;

    if (OSIX_FALSE != MsdpUtilIsAllZeros (au1Addr, IPVX_ADDR_FMLY_IPV4))
    {
        printf ("NonZero address - IPV4 Fmly\n");
        return OSIX_FAILURE;
    }
    if (OSIX_FALSE != MsdpUtilIsAllZeros (au1Addr, IPVX_ADDR_FMLY_IPV6))
    {
        printf ("NonZero address - IPV6 Fmly\n");
        return OSIX_FAILURE;
    }

    if (OSIX_FAILURE != MsdpUtilIsAllZeros (NULL, IPVX_ADDR_FMLY_IPV6))
    {
        printf ("NULL address \n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

INT4
MsdpUtUtil_12 (VOID)
{

    tIPvXAddr           Dest;
    tIPvXAddr           NextHop;
    INT4                i4Metrics = 0;
    UINT4               u4Preference = 0;
    INT4                i4Port = 0;
    CHR1               *Addr = "7777::7";

    MsdpUtDelEntry ();
    MEMSET (&Dest, 0, sizeof (tIPvXAddr));
    MEMSET (&NextHop, 0, sizeof (tIPvXAddr));
    Dest.au1Addr[0] = 10;
    Dest.au1Addr[1] = 0;
    Dest.au1Addr[2] = 0;
    Dest.au1Addr[3] = 10;

    Dest.u1Afi = 1;
    Dest.u1AddrLen = 4;

    MsdpUtilGetUcastRtInfo (&Dest, &NextHop, &i4Metrics, &u4Preference,
                            &i4Port);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("ip route 13.0 255.0 10.2");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    Dest.au1Addr[0] = 13;
    Dest.au1Addr[1] = 0;
    Dest.au1Addr[2] = 0;
    Dest.au1Addr[3] = 11;
    Dest.u1Afi = 1;
    Dest.u1AddrLen = 4;
    MsdpUtilGetUcastRtInfo (&Dest, &NextHop, &i4Metrics, &u4Preference,
                            &i4Port);
    MEMSET (&Dest, 0, sizeof (Dest));
    Dest.au1Addr[0] = 11;
    Dest.au1Addr[1] = 0;
    Dest.au1Addr[2] = 0;
    Dest.au1Addr[14] = 1;
    Dest.au1Addr[15] = 1;

    Dest.u1Afi = 2;
    Dest.u1AddrLen = 16;

    MEMSET (&Dest, 0, sizeof (Dest));
    CliVlanInit ();
    sleep (5);
    INET_ATON6 ((UINT1 *) Addr, Dest.au1Addr);
    MsdpUtilGetUcastRtInfo (&Dest, &NextHop, &i4Metrics, &u4Preference,
                            &i4Port);
    Addr = "8888::8";

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("ipv6 route 8888::0 64 7777::7");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    sleep (5);
    INET_ATON6 ((UINT1 *) Addr, Dest.au1Addr);
    Dest.u1Afi = 2;
    Dest.u1AddrLen = 16;
    MsdpUtilGetUcastRtInfo (&Dest, &NextHop, &i4Metrics, &u4Preference,
                            &i4Port);
    CliVlanDeInit ();

    Dest.u1Afi = 2;
    Dest.u1AddrLen = 16;

    INET_ATON6 ((UINT1 *) Addr, Dest.au1Addr);
    MsdpUtilGetUcastRtInfo (&Dest, &NextHop, &i4Metrics, &u4Preference,
                            &i4Port);

    Dest.u1Afi = 3;
    Dest.u1AddrLen = 16;

    MsdpUtilGetUcastRtInfo (&Dest, &NextHop, &i4Metrics, &u4Preference,
                            &i4Port);

    return OSIX_SUCCESS;
}

INT4
MsdpUtUtil_13 (VOID)
{
    UINT1               u1Val = 0;

    MsdpUtDelEntry ();
    if (MSDP_INVALID != MsdpUtilGetRegisterEntry (-1))
    {
        printf ("Non proto ID exists \n");
        return OSIX_FAILURE;
    }

    u1Val = gMsdpGlobals.aMsdpRegister[1].u1ProtId;
    gMsdpGlobals.aMsdpRegister[1].u1ProtId = 9;

    if (1 != MsdpUtilGetRegisterEntry (9))
    {
        printf ("Given Proto Id not exist \n");
        gMsdpGlobals.aMsdpRegister[1].u1ProtId = u1Val;
        return OSIX_FAILURE;
    }
    gMsdpGlobals.aMsdpRegister[1].u1ProtId = u1Val;
    return OSIX_SUCCESS;
}

INT4
MsdpUtUtil_14 (VOID)
{
    UINT1               u1Val = 0;
    MsdpUtDelEntry ();
    if (MSDP_INVALID != MsdpUtilFindFreeRegisterEntry ())
    {
        printf ("Free Register Entry exists \n");
        return OSIX_FAILURE;
    }

    u1Val = gMsdpGlobals.aMsdpRegister[0].u1Flag;
    gMsdpGlobals.aMsdpRegister[0].u1Flag = DOWN;

    if (MSDP_INVALID == MsdpUtilFindFreeRegisterEntry ())
    {
        printf ("Free Register Entry not exists \n");
        gMsdpGlobals.aMsdpRegister[0].u1Flag = u1Val;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
MsdpUtUtil_15 (VOID)
{
    tIPvXAddr           Dest;
    MEMSET (&Dest, 0, sizeof (tIPvXAddr));
    Dest.au1Addr[0] = 10;
    Dest.au1Addr[1] = 0;
    Dest.au1Addr[2] = 0;
    Dest.au1Addr[3] = 1;

    Dest.u1Afi = 1;
    Dest.u1AddrLen = 4;
    MsdpUtDelEntry ();
    MsdpPrintIPvxAddress (Dest);
    Dest.u1Afi = 2;
    Dest.u1AddrLen = 16;
    MsdpPrintIPvxAddress (Dest);

    return OSIX_SUCCESS;

}

INT4
MsdpUtUtil_16 (VOID)
{
    UINT1               au1Addr[16];
    MEMSET (au1Addr, 0, 16);
    au1Addr[0] = 10;
    au1Addr[1] = 0;
    au1Addr[2] = 0;
    au1Addr[3] = 1;

    MsdpUtDelEntry ();
    MsdpPrintAddress (NULL, 1);
    MsdpPrintAddress (au1Addr, 1);
    MsdpPrintAddress (au1Addr, 2);
    MsdpPrintAddress (au1Addr, 3);
    return OSIX_SUCCESS;
}

/* msdputl UT cases */
INT4
MsdpUtUtl_1 (VOID)
{
    return -1;
}

INT4
MsdpUtUtl_2 (VOID)
{
    return -1;
}

INT4
MsdpUtUtl_3 (VOID)
{
    return -1;
}

INT4
MsdpUtUtl_4 (VOID)
{
    return -1;
}

INT4
MsdpUtUtl_5 (VOID)
{
    return -1;
}

INT4
MsdpUtUtl_6 (VOID)
{
    return -1;
}

INT4
MsdpUtUtl_7 (VOID)
{
    return -1;
}

INT4
MsdpUtUtl_8 (VOID)
{
    return -1;
}

INT4
MsdpUtUtl_9 (VOID)
{
    return -1;
}

INT4
MsdpUtUtl_10 (VOID)
{
    return -1;
}

INT4
MsdpUtUtl_11 (VOID)
{
    return -1;
}

INT4
MsdpUtUtl_12 (VOID)
{
    return -1;
}

VOID
CliVlanInit (VOID)
{
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("int vlan 2");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("ip address 7.7 255.0");
    CliExecuteAppCmd ("ipv6 enable");
    CliExecuteAppCmd ("ipv6  address 7777::77 64");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("vlan 2");
    CliExecuteAppCmd ("ports gig 0/2");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("int gig 0/2");
    CliExecuteAppCmd ("sw pvid 2");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
}

VOID
CliVlanDeInit (VOID)
{
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int gig 0/2");
    CliExecuteAppCmd ("no sw pvid ");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 2");
    CliExecuteAppCmd ("no ipv6 en");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no int vlan 2");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
}
