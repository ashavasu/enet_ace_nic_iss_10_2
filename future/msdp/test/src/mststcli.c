/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: mststcli.c,v 1.1.1.1 2011/02/11 09:19:43 siva Exp $
 **
 ** Description: MSDP  UT Test cases cli file.
 ** NOTE: This file should not be include in release packaging.
 ** ***********************************************************************/

#include "lr.h"
#include "cli.h"
#include "msdpinc.h"
#include "msdpcli.h"
#include "mststcli.h"

#define MAX_ARGS 5

extern VOID         MsdpExecuteUt (UINT4 u4FileNumber, UINT4 u4TestNumber);
extern VOID         MsdpExecuteUtAll (VOID);
extern VOID         MsdpExecuteUtFile (UINT4 u4File);
extern VOID         MsdpExecuteUtInMyOrder (VOID);
extern UINT4        gu4MsdpUtStubsReqd;

/*  Function is called from mststcmd.def file */

INT4
cli_process_msdp_test_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[MAX_ARGS];
    INT4                i4Inst = 0;
    INT1                argno = 0;
    CliRegisterLock (CliHandle, MsdpMainTaskLock, MsdpMainTaskUnLock);
    MSDP_LOCK;

    va_start (ap, u4Command);
    UNUSED_PARAM (CliHandle);
    i4Inst = va_arg (ap, INT4);

    /* Walk through the arguments and store in args array.
     *      * */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == MAX_ARGS)
            break;
    }

    va_end (ap);

    switch (u4Command)

    {
        case 1:

            gu4MsdpUtStubsReqd = OSIX_TRUE;

            if (args[0] == NULL)
            {
                /* order along which the test cases were written */
                if ((args[3] != NULL) && (args[2] == NULL))
                {
                    MsdpExecuteUtInMyOrder ();
                }
                /* specified case on specified file */
                if (args[2] != NULL)
                {
                    MsdpExecuteUt (*(UINT4 *) (args[1]), *(UINT4 *) (args[2]));
                }
                /* all cases on specified file */
                else
                {
                    MsdpExecuteUtFile (*(UINT4 *) (args[1]));
                }
            }
            else
            {
                /* all cases in all the files */
                MsdpExecuteUtAll ();
            }

            gu4MsdpUtStubsReqd = OSIX_FALSE;
            break;
    }

    CliUnRegisterLock (CliHandle);
    MSDP_UNLOCK;

    return CLI_SUCCESS;
}
