/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdmsdwr.c,v 1.1.1.1 2011/01/17 05:57:33 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "stdmsdlw.h"
# include  "stdmsdwr.h"
# include  "stdmsddb.h"
# include  "msdpinc.h"

VOID
RegisterSTDMSD ()
{
    SNMPRegisterMib (&stdmsdOID, &stdmsdEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&stdmsdOID, (const UINT1 *) "stdmsdp");
}

VOID
UnRegisterSTDMSD ()
{
    SNMPUnRegisterMib (&stdmsdOID, &stdmsdEntry);
    SNMPDelSysorEntry (&stdmsdOID, (const UINT1 *) "stdmsdp");
}

INT4
MsdpEnabledGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMsdpEnabled (&(pMultiData->i4_SLongValue)));
}

INT4
MsdpCacheLifetimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMsdpCacheLifetime (&(pMultiData->u4_ULongValue)));
}

INT4
MsdpNumSACacheEntriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMsdpNumSACacheEntries (&(pMultiData->u4_ULongValue)));
}

INT4
MsdpRPAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMsdpRPAddress (&(pMultiData->u4_ULongValue)));
}

INT4
MsdpEnabledSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetMsdpEnabled (pMultiData->i4_SLongValue));
}

INT4
MsdpCacheLifetimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetMsdpCacheLifetime (pMultiData->u4_ULongValue));
}

INT4
MsdpRPAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetMsdpRPAddress (pMultiData->u4_ULongValue));
}

INT4
MsdpEnabledTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2MsdpEnabled (pu4Error, pMultiData->i4_SLongValue));
}

INT4
MsdpCacheLifetimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2MsdpCacheLifetime (pu4Error, pMultiData->u4_ULongValue));
}

INT4
MsdpRPAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2MsdpRPAddress (pu4Error, pMultiData->u4_ULongValue));
}

INT4
MsdpEnabledDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MsdpEnabled (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
MsdpCacheLifetimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MsdpCacheLifetime
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
MsdpRPAddressDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MsdpRPAddress (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexMsdpPeerTable (tSnmpIndex * pFirstMultiIndex,
                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMsdpPeerTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMsdpPeerTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MsdpPeerStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerState (pMultiIndex->pIndex[0].u4_ULongValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
MsdpPeerRPFFailuresGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerRPFFailures (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
MsdpPeerInSAsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerInSAs (pMultiIndex->pIndex[0].u4_ULongValue,
                                 &(pMultiData->u4_ULongValue)));

}

INT4
MsdpPeerOutSAsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerOutSAs (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)));

}

INT4
MsdpPeerInSARequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerInSARequests (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
MsdpPeerOutSARequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerOutSARequests (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
MsdpPeerInControlMessagesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerInControlMessages
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MsdpPeerOutControlMessagesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerOutControlMessages
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MsdpPeerInDataPacketsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerInDataPackets (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
MsdpPeerOutDataPacketsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerOutDataPackets (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
MsdpPeerFsmEstablishedTransitionsGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerFsmEstablishedTransitions
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MsdpPeerFsmEstablishedTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerFsmEstablishedTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MsdpPeerInMessageTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerInMessageTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
MsdpPeerLocalAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerLocalAddress (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
MsdpPeerConnectRetryIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerConnectRetryInterval
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MsdpPeerHoldTimeConfiguredGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerHoldTimeConfigured
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MsdpPeerKeepAliveConfiguredGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerKeepAliveConfigured
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MsdpPeerDataTtlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerDataTtl (pMultiIndex->pIndex[0].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
MsdpPeerStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
MsdpPeerRemotePortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerRemotePort (pMultiIndex->pIndex[0].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
MsdpPeerLocalPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerLocalPort (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
MsdpPeerEncapsulationTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerEncapsulationType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MsdpPeerConnectionAttemptsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerConnectionAttempts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MsdpPeerDiscontinuityTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpPeerDiscontinuityTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MsdpPeerLocalAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMsdpPeerLocalAddress (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
MsdpPeerConnectRetryIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMsdpPeerConnectRetryInterval
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MsdpPeerHoldTimeConfiguredSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMsdpPeerHoldTimeConfigured
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MsdpPeerKeepAliveConfiguredSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMsdpPeerKeepAliveConfigured
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MsdpPeerDataTtlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMsdpPeerDataTtl (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
MsdpPeerStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMsdpPeerStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
MsdpPeerEncapsulationTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMsdpPeerEncapsulationType
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MsdpPeerLocalAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2MsdpPeerLocalAddress (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
MsdpPeerConnectRetryIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2MsdpPeerConnectRetryInterval (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
MsdpPeerHoldTimeConfiguredTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2MsdpPeerHoldTimeConfigured (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
MsdpPeerKeepAliveConfiguredTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2MsdpPeerKeepAliveConfigured (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
MsdpPeerDataTtlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2MsdpPeerDataTtl (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
MsdpPeerStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2MsdpPeerStatus (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
MsdpPeerEncapsulationTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2MsdpPeerEncapsulationType (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
MsdpPeerTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MsdpPeerTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexMsdpSACacheTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMsdpSACacheTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMsdpSACacheTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MsdpSACachePeerLearnedFromGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpSACacheTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpSACachePeerLearnedFrom
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MsdpSACacheRPFPeerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpSACacheTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpSACacheRPFPeer (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
MsdpSACacheInSAsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpSACacheTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpSACacheInSAs (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
MsdpSACacheInDataPacketsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpSACacheTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpSACacheInDataPackets
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MsdpSACacheUpTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpSACacheTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpSACacheUpTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
MsdpSACacheExpiryTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpSACacheTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpSACacheExpiryTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
MsdpSACacheStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpSACacheTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpSACacheStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
MsdpSACacheStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMsdpSACacheStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
MsdpSACacheStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2MsdpSACacheStatus (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
MsdpSACacheTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MsdpSACacheTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexMsdpMeshGroupTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMsdpMeshGroupTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMsdpMeshGroupTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MsdpMeshGroupStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMsdpMeshGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMsdpMeshGroupStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
MsdpMeshGroupStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMsdpMeshGroupStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
MsdpMeshGroupStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2MsdpMeshGroupStatus (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
MsdpMeshGroupTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MsdpMeshGroupTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
