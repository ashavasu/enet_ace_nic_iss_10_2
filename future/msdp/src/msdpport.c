/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: msdpport.c,v 1.7 2012/03/06 11:26:05 siva Exp $
 *
 * Description:This file holds the functions for IPv4 porting fns 
 *             of the MSDP module
 *********************************************************************/

#include "msdpinc.h"

/****************************************************************************
 * FUNCTION NAME : MsdpPortGetVlanIdFromIpAddress
 *
 * DESCRIPTION   : This function obtains the VLAN ID corresponding to a given 
 *                 IP address
 *
 * INPUTS        : u4Addr- The IP Address
 *
 * OUTPUTS       : u2VlanId- VLAN ID for a given IP addr
 *
 * RETURNS       : OSIX_SUCCESS if the VLAN ID is obtained successfully
 *                 OSIX_FAILURE otherwise
 *
 ****************************************************************************/

INT4
MsdpPortGetVlanIdFromIpAddress (UINT4 u4Addr, UINT2 *pu2VlanId)
{
    UINT4               u4IfIndex = 0;
    INT4                i4RetStat = OSIX_FAILURE;

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPortGetVlanIdFromIpAddress"));

    if (CFA_SUCCESS != CfaIpIfGetIfIndexFromIpAddress (u4Addr, &u4IfIndex))
    {
        MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsdpPortGetVlanIdFromIpAddress: "
                        "Unable to get interface index from address"));

        return OSIX_FAILURE;
    }
    else
    {

        MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsdpPortGetVlanIdFromIpAddress: "
                        " interface index from address is retrieved"));

    }

    if (CFA_SUCCESS == CfaGetVlanId (u4IfIndex, pu2VlanId))
    {
        i4RetStat = OSIX_SUCCESS;

        MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsdpPortGetVlanIdFromIpAddress: "
                        "VLAN ID %d obtained successfully", *pu2VlanId));

    }
    else
    {

        MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsdpPortGetVlanIdFromIpAddress: "
                        "Unable to get VLAN Id from interface index"));

        i4RetStat = OSIX_FAILURE;
    }

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPortGetVlanIdFromIpAddress"));

    return i4RetStat;

}

/****************************************************************************
 * FUNCTION NAME : MsdpPortIsValidLocalAddress
 *
 * DESCRIPTION   : This function determines whether the given IP address is a 
 *                 valid local address
 *
 * INPUTS        : u4Addr- The IP Address
 *
 * OUTPUTS       : None
 *
 * RETURNS       : The interface index for  the address, if the address is 
 *                 a valid local address
 *                 MSDP_INVALID otherwise
 *
 ****************************************************************************/
INT4
MsdpPortIsValidLocalAddress (UINT1 *pu1Addr)
{
    UINT4               u4CfaIfIndex = 0;
    UINT4               u4LocalAddr = 0;
    INT4                i4RetVal = MSDP_INVALID;

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPortIsValidLocalAddress"));

    u4LocalAddr = *(UINT4 *) (VOID *) pu1Addr;
    u4LocalAddr = OSIX_HTONL (u4LocalAddr);

    if (CfaIpIfGetIfIndexFromIpAddress (u4LocalAddr, &u4CfaIfIndex)
        == CFA_SUCCESS)
    {
        i4RetVal = u4CfaIfIndex;
    }

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPortIsValidLocalAddress"));

    return i4RetVal;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPortInformSAToMrp
 *
 * DESCRIPTION   : This function informs the SA info (S,G or S,G, data) 
 *                 to all registered MRPs 
 *
 * INPUTS        : pMsdpSaInfo- SA information
 *
 * OUTPUTS       : None
 *
 * RETURNS       : None
 *
 ****************************************************************************/
VOID
MsdpPortInformSAToMrp (tMsdpMrpSaAdvt * pMsdpSaInfo)
{
    INT4                i4Index = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPortInformSAToMrp"));

    for (i4Index = 0; i4Index < MSDP_MAX_REGISTER; i4Index++)
    {
        if ((gMsdpGlobals.aMsdpRegister[i4Index].u1ProtId == PIM_ID) &&
            (gMsdpGlobals.aMsdpRegister[i4Index].pUpdateSrcActiveInfo))
        {
            gMsdpGlobals.aMsdpRegister[i4Index].
                pUpdateSrcActiveInfo (pMsdpSaInfo);
        }
    }

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPortInformSAToMrp"));

}

/****************************************************************************
 * FUNCTION NAME : MsdpPortGetIpPortFromIfIndex
 *
 * DESCRIPTION   : This function obtains the IP port from the interface index
 *
 * INPUTS        : u4IfIndex- The interface index
 *
 * OUTPUTS       : pu4IfIndex- The IP port obtained
 *
 * RETURNS       : OSIX_SUCCESS if the port was obtained successfully
 *                 OSIX_FAILURE otherwise
 *
 ****************************************************************************/

INT4
MsdpPortGetIpPortFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4IfIndex)
{

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPortGetIpPortFromIfIndex"));

    if (NetIpv4GetPortFromIfIndex (u4IfIndex, pu4IfIndex) == NETIPV4_SUCCESS)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_EXIT,
                        "\nEXIT: MsdpPortGetIpPortFromIfIndex"));

        return OSIX_SUCCESS;
    }

    *pu4IfIndex = MSDP_INVALID;

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_EXIT,
                    "\nMsdpPortGetIpPortFromIfIndex:Invalid IF Index :Exit\n"));

    return OSIX_FAILURE;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPortIpGetRoute
 *
 * DESCRIPTION   : This function gets the route information for a route query
 *
 * INPUTS        : pRtQuery- The route query
 *
 * OUTPUTS       : pNetIpRtInfo- Route info
 *
 * RETURNS       : OSIX_SUCCESS if NetIpv4GetRoute succeeds
 *                 OSIX_FAILURE otherwise
 *
 ****************************************************************************/
INT4
MsdpPortIpGetRoute (tRtInfoQueryMsg * pRtQuery, tNetIpv4RtInfo * pNetIpRtInfo)
{
    tIPvXAddr           PrintAddrTraceDestIPAddr;
    INT4                i4RetStat = OSIX_FAILURE;

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPortIpGetRoute"));

    MEMSET (&PrintAddrTraceDestIPAddr, 0, sizeof (tIPvXAddr));

    pRtQuery->u4ContextId = MSDP_DEF_VRF_CTXT_ID;
    i4RetStat = NetIpv4GetRoute (pRtQuery, pNetIpRtInfo);
    if (i4RetStat != NETIPV4_FAILURE)
    {
        IPVX_ADDR_INIT_FROMV4 (PrintAddrTraceDestIPAddr,
                               pRtQuery->u4DestinationIpAddress);

        MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsdpPortIpGetRoute: "
                        "Ipv4 Get Route Success for the Dst %s",
                        MsdpPrintIPvxAddress (PrintAddrTraceDestIPAddr)));

        MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_EXIT,
                        "\nEXIT: MsdpPortIpGetRoute"));

        i4RetStat = OSIX_SUCCESS;
    }
    else
    {
        i4RetStat = OSIX_FAILURE;
        MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsdpPortIpGetRoute: "
                        "NetIpv4GetRoute returns failure"));

    }

    return i4RetStat;

}

/****************************************************************************
 * FUNCTION NAME : MsdpPortHandleSrcInfo
 *
 * DESCRIPTION   : This function handles the source information obtained from
 *                 MRP. This is the API provided by MSDP to MRP.
 *                 This function posts an event to MSDP
 *
 * INPUTS        : pMsdpSaAdvt- The SA advt message
 *
 * OUTPUTS       : None
 *
 * RETURNS       : OSIX_SUCCESS if memory is assigned and OsixSendEvent is 
 *                 successful
 *                 OSIX_FAILURE otherwise
 *
 ****************************************************************************/
INT4
MsdpPortHandleSAInfo (tMsdpMrpSaAdvt * pMsdpSaAdvt)
{
    tMsdpQueMsg        *pMsdpQueMsg = NULL;
    INT4                i4RetVal = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPortHandleSrcInfo"));

    pMsdpQueMsg = MsdpQueAllocQueMsg ();

    if (pMsdpQueMsg == NULL)
    {
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pMsdpSaAdvt->pDataPkt, FALSE);
        CRU_BUF_Release_MsgBufChain (pMsdpSaAdvt->pGrpSrcAddrList, FALSE);

        return OSIX_FAILURE;
    }

    pMsdpQueMsg->eMsgType = MSDP_PIM_UPDATE_SA_EVT;

    MEMCPY (&pMsdpQueMsg->unMsdpMsgIfParam.MsdpMrpSaInfo, pMsdpSaAdvt,
            sizeof (tMsdpMrpSaAdvt));

    i4RetVal = MsdpQuePostQueueMsg (pMsdpQueMsg);

    if (i4RetVal == OSIX_FAILURE)
    {
        MemReleaseMemBlock (MSDP_QUE_MEMPOOL_ID, (UINT1 *) pMsdpQueMsg);
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pMsdpSaAdvt->pDataPkt, FALSE);
        CRU_BUF_Release_MsgBufChain (pMsdpSaAdvt->pGrpSrcAddrList, FALSE);

        MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsdpPortHandleSrcInfo: "
                        "MsdpQuePostQueueMsg returned failure"));

        return OSIX_FAILURE;
    }
    if (OsixSendEvent (SELF, (const UINT1 *) MSDP_TASK_NAME,
                       MSDP_QUEUE_EVENT) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (MSDP_QUE_MEMPOOL_ID, (UINT1 *) pMsdpQueMsg);
        CRU_BUF_Release_MsgBufChain (pMsdpSaAdvt->pDataPkt, FALSE);
        CRU_BUF_Release_MsgBufChain (pMsdpSaAdvt->pGrpSrcAddrList, FALSE);
        MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsdpPortHandleSrcInfo: "
                        "OsixSendEvent returned failure for event "
                        "MSDP_PIM_UPDATE_SA_EVT"));

        return OSIX_FAILURE;
    }

    MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsdpPortHandleSrcInfo: "
                    "OsixSendEvent Success for event "
                    "MSDP_PIM_UPDATE_SA_EVT"));

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPortHandleSrcInfo"));

    return i4RetVal;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPortHandleSARequest
 *
 * DESCRIPTION   : This function handles the SG request from MRP.
 *                 This is the MSDP API provided to MRP. MRP calls this
 *                 API when multicast receiver joined for a Grp or Grp, Src
 *
 * INPUTS        : pMsdpSaReqGrp- SA Req Info from MRP
 *
 * OUTPUTS       : None
 *
 * RETURNS       : OSIX_SUCCESS if memory is allocated and OsixSendEvent is 
 *                 successful
 *                 OSIX_FAILURE otherwise
 *
 ****************************************************************************/
INT4
MsdpPortHandleSARequest (tMsdpSAReqGrp * pMsdpSaReqGrp)
{
    tMsdpQueMsg        *pMsdpQueMsg = NULL;
    INT4                i4RetVal = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPortHandleSgRequest"));

    pMsdpQueMsg = MsdpQueAllocQueMsg ();
    if (pMsdpQueMsg == NULL)
    {
        MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsdpPortHandleSgRequest: "
                        "Memory allocation failed"));

        return OSIX_FAILURE;
    }

    pMsdpQueMsg->eMsgType = MSDP_PIM_REQUEST_SA_EVT;
    MEMCPY (&pMsdpQueMsg->unMsdpMsgIfParam.MsdpSAReqGrpAddr, pMsdpSaReqGrp,
            sizeof (tMsdpSAReqGrp));
    i4RetVal = MsdpQuePostQueueMsg (pMsdpQueMsg);
    if (i4RetVal == OSIX_FAILURE)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsdpPortHandleSgRequest: "
                        "MsdpQuePostQueueMsg returned failure"));

        return OSIX_FAILURE;
    }
    if (OsixSendEvent (SELF, (const UINT1 *) MSDP_TASK_NAME,
                       MSDP_QUEUE_EVENT) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (MSDP_QUE_MEMPOOL_ID, (UINT1 *) pMsdpQueMsg);
        MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsdpPortHandleSgRequest: "
                        "OsixSendEvent returned failure for MSDP_PIM_REQUEST_SA_EVT"));

        return OSIX_FAILURE;
    }

    MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsdpPortHandleSgRequest: "
                    "OsixSendEvent Success for MSDP_PIM_REQUEST_SA_EVT"));

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPortHandleSgRequest"));

    return i4RetVal;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPortRegisterMrp
 *
 * DESCRIPTION   : This function registers MRPs with MSDP. MRP should call 
 *                 this function with Funtion ptr to handle SA message from
 *                 MSDP
 *
 * INPUTS        : u1ProtId- Protocol ID
 *                 pCBUpdateSaInfo- Function pointer
 *
 * OUTPUTS       : gMsdpGlobals.aMsdpRegister[].u1Flag
 *                 gMsdpGlobals.aMsdpRegister[].u1ProtId
 *                 gMsdpGlobals.aMsdpRegister[].pUpdateSrcActiveInfo
 *
 * RETURNS       : OSIX_SUCCESS if free registry entry is found
 *                 OSIX_FAILURE otherwise
 *
 ****************************************************************************/
INT4
MsdpPortRegisterMrp (UINT1 u1ProtoId,
                     VOID (*pCBUpdateSaInfo) (tMsdpMrpSaAdvt *))
{

    INT4                i4Index = MSDP_INVALID;

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPorRegisterMrp"));

    i4Index = MsdpUtilGetRegisterEntry (u1ProtoId);
    if (i4Index == MSDP_INVALID)
    {
        if ((i4Index = MsdpUtilFindFreeRegisterEntry ()) == MSDP_INVALID)
        {

            MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsdpPorRegisterMrp: "
                            "Can't find free reg entry"));

            return OSIX_FAILURE;
        }
    }

    MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsdpPorRegisterMrp: "
                    "Registration with MSDP success for the Proto Id %d",
                    u1ProtoId));

    gMsdpGlobals.aMsdpRegister[i4Index].u1ProtId = u1ProtoId;
    gMsdpGlobals.aMsdpRegister[i4Index].pUpdateSrcActiveInfo = pCBUpdateSaInfo;
    gMsdpGlobals.aMsdpRegister[i4Index].u1Flag = UP;

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPorRegisterMrp"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPortDeRegisterMrp
 *
 * DESCRIPTION   : This function deregisters MRPs with MSDP
 *
 * INPUTS        : u1ProtId- Protocol ID
 *
 * OUTPUTS       : gMsdpGlobals.aMsdpRegister[].u1Flag
 *                 gMsdpGlobals.aMsdpRegister[].u1ProtId
 *                 gMsdpGlobals.aMsdpRegister[].pUpdateSrcActiveInfo
 *
 * RETURNS       : OSIX_SUCCESS if able to obtain the register entry
 *                 OSIX_FAILURE otherwise
 *
 ****************************************************************************/
INT4
MsdpPortDeRegisterMrp (UINT1 u1ProtoId)
{
    INT4                i4Index = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPortDeRegisterMrp"));

    i4Index = MsdpUtilGetRegisterEntry (u1ProtoId);
    if (i4Index != MSDP_INVALID)
    {
        gMsdpGlobals.aMsdpRegister[i4Index].u1Flag = DOWN;
        gMsdpGlobals.aMsdpRegister[i4Index].u1ProtId = 0;
        gMsdpGlobals.aMsdpRegister[i4Index].pUpdateSrcActiveInfo = NULL;

        MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsdpPorRegisterMrp: "
                        "Deregistration with MSDP success for the Proto Id %d",
                        u1ProtoId));

        MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_EXIT,
                        "\nEXIT: MsdpPortDeRegisterMrp"));

        return OSIX_SUCCESS;
    }

    MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsdpPortDeRegisterMrp: "
                    "Couldn't get reg entry"));

    return OSIX_FAILURE;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPortRegiterWithIP
 *
 * DESCRIPTION   : This function registers with IPv4 to get Interface delete
 *                 and shutdown indications with the call back function
 *                 MsdpPortHandleIfChg.
 *
 * INPUTS        : None
 *
 * OUTPUTS       : None
 *
 * RETURNS       : OSIX_SUCCESS if registering is successful
 *                 OSIX_FAILURE otherwise
 *
 ****************************************************************************/
INT4
MsdpPortRegiterWithIP (VOID)
{
    tNetIpRegInfo       RegInfo;

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPortRegiterWithIP"));

    MEMSET (&RegInfo, 0, sizeof (tNetIpRegInfo));

    RegInfo.u4ContextId = MSDP_DEF_VRF_CTXT_ID;
    /* Register for receiving interface change updates */
    RegInfo.u1ProtoId = MSDP_PROTOCOL_ID;
    RegInfo.u2InfoMask |= NETIPV4_IFCHG_REQ;
    RegInfo.pProtoPktRecv = NULL;
    RegInfo.pIfStChng = MsdpPortHandleIfChg;
    RegInfo.pRtChng = NULL;

    if (NetIpv4RegisterHigherLayerProtocol (&RegInfo) == FAILURE)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsdpPortRegiterWithIP: "
                        "IPv4 registration failed"));

        return OSIX_FAILURE;
    }

    MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsdpPorRegisterMrp: "
                    "Registration with IP successful"));

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPortRegiterWithIP"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPortDeRegiterWithIP
 *
 * DESCRIPTION   : This function deregisters with IP
 *
 * INPUTS        : None
 *
 * OUTPUTS       : None
 *
 * RETURNS       : None
 *
 ****************************************************************************/
VOID
MsdpPortDeRegiterWithIP (VOID)
{

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPortDeRegiterWithIP"));

    NetIpv4DeRegisterHigherLayerProtocolInCxt (MSDP_DEF_VRF_CTXT_ID,
                                               MSDP_PROTOCOL_ID);

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPortDeRegiterWithIP"));

    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPortHandleIfChg
 *
 * DESCRIPTION   : This function handles interface changes from IPv4
 *
 * INPUTS        : pNetIfInfo- IPv4 interface information
 *                 u4BitMap- Bitmap for INTERFACE_DOWN, INTERFACE_DELETE
 *
 * OUTPUTS       : None
 *
 * RETURNS       : None
 *
 ****************************************************************************/
VOID
MsdpPortHandleIfChg (tNetIpv4IfInfo * pNetIfInfo, UINT4 u4BitMap)
{
    tMsdpQueMsg        *pMsdpQueMsg = NULL;
    INT4                i4RetVal = 0;

    pMsdpQueMsg = MsdpQueAllocQueMsg ();
    if (pMsdpQueMsg == NULL)
    {
        MemReleaseMemBlock (MSDP_QUE_MEMPOOL_ID, (UINT1 *) pMsdpQueMsg);
        return;
    }

    pMsdpQueMsg->eMsgType = MSDP_IF_CHG_EVT;
    pMsdpQueMsg->unMsdpMsgIfParam.MsdpIfStatus.u4IfIndex =
        pNetIfInfo->u4IfIndex;
    pMsdpQueMsg->unMsdpMsgIfParam.MsdpIfStatus.u1IfOperStatus =
        (UINT1) pNetIfInfo->u4Oper;
    pMsdpQueMsg->unMsdpMsgIfParam.MsdpIfStatus.u1IfAdminStatus =
        (UINT1) pNetIfInfo->u4Admin;
    pMsdpQueMsg->unMsdpMsgIfParam.MsdpIfStatus.u1AddrType = IPVX_ADDR_FMLY_IPV4;

    i4RetVal = MsdpQuePostQueueMsg (pMsdpQueMsg);
    if (i4RetVal == OSIX_FAILURE)
    {
        MemReleaseMemBlock (MSDP_QUE_MEMPOOL_ID, (UINT1 *) pMsdpQueMsg);
        return;
    }

    if (OsixSendEvent (SELF, (const UINT1 *) MSDP_TASK_NAME,
                       MSDP_QUEUE_EVENT) != OSIX_SUCCESS)
    {
        /* Trace */
        return;
    }
    UNUSED_PARAM (u4BitMap);
    return;
}

INT4
MsdpPortGetIfInfo (UINT4 u4IfIndex, UINT1 *pu1OperStatus)
{
    INT4                i4RetVal = 0;

    i4RetVal = CfaGetIfOperStatus (u4IfIndex, pu1OperStatus);
    return i4RetVal;
}

/******************************************************************************
 *
 * FUNCTION NAME      : MsdpPortGetRouterId
 *
 * DESCRIPTION        : This function returns the Router-Id configured for the
 *                      Router
 *
 * INPUT              : i4CtxtId -VRF Context ID
 *
 * OUTPUT             : pu4RtrId : Router-ID for the router
 *
 * RETURNS            : NONE
 *
 ******************************************************************************/
PUBLIC VOID
MsdpPortGetRouterId (INT4 i4CtxtId, UINT4 *pu4RtrId)
{
    RtmApiGetRouterId (i4CtxtId, pu4RtrId);
}

PUBLIC INT4
MsdpPortSARtMapRule (tIPvXAddr * pSrcAddr, tIPvXAddr * pGrpAddr, UINT1 *pRtMap)
{
#ifdef ROUTEMAP_WANTED
    tRtMapInfo          InputRtInfo;
    tRtMapInfo          OutputRtInfo;
    INT4                i4RetVal = 0;
    UINT4               u4TempAddr = 0;
    UINT1               u1AddrLen = 0;

    MEMSET (&InputRtInfo, 0, sizeof (tRtMapInfo));
    MEMSET (&OutputRtInfo, 0, sizeof (tRtMapInfo));

    MEMCPY (&InputRtInfo.SrcXAddr, pSrcAddr, sizeof (tIPvXAddr));
    MEMCPY (&InputRtInfo.DstXAddr, pGrpAddr, sizeof (tIPvXAddr));

    if (*pRtMap == '\0')
    {
        return OSIX_SUCCESS;
    }

    MEMCPY (&InputRtInfo.SrcXAddr, pSrcAddr, sizeof (tIPvXAddr));
    if (pSrcAddr->u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&u4TempAddr, InputRtInfo.SrcXAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
        u4TempAddr = OSIX_NTOHL (u4TempAddr);
        MEMCPY (InputRtInfo.SrcXAddr.au1Addr, &u4TempAddr, IPVX_IPV4_ADDR_LEN);
    }

    MEMCPY (&InputRtInfo.DstXAddr, pGrpAddr, sizeof (tIPvXAddr));
    if (pGrpAddr->u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&u4TempAddr, InputRtInfo.DstXAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
        u4TempAddr = OSIX_NTOHL (u4TempAddr);
        MEMCPY (InputRtInfo.DstXAddr.au1Addr, &u4TempAddr, IPVX_IPV4_ADDR_LEN);
    }

    u1AddrLen = (pGrpAddr->u1Afi == IPVX_ADDR_FMLY_IPV4) ?
        IPVX_IPV4_ADDR_LEN : IPVX_IPV6_ADDR_LEN;

    InputRtInfo.u2DstPrefixLen = u1AddrLen * BYTE_LENGTH;

    i4RetVal = RMapApplyRule (&InputRtInfo, &OutputRtInfo, pRtMap);

    if (i4RetVal != RMAP_ROUTE_PERMIT)
    {
        return OSIX_FAILURE;
    }
    else
    {
        return OSIX_SUCCESS;
    }

#else
    UNUSED_PARAM (pSrcAddr);
    UNUSED_PARAM (pGrpAddr);
    UNUSED_PARAM (pRtMap);
    return OSIX_SUCCESS;
#endif

}

PUBLIC INT4
MsdpPortPeerRMapRule (tIPvXAddr * pPeerAddr, UINT1 *pRtMap)
{
#ifdef ROUTEMAP_WANTED
    tRtMapInfo          InputRtInfo;
    tRtMapInfo          OutputRtInfo;
    INT4                i4PeerFilter = 0;
    INT4                i4GlobalFilter = 0;
    INT4                i4RetVal = 0;
    UINT4               u4TempAddr = 0;

    MEMSET (&InputRtInfo, 0, sizeof (tRtMapInfo));
    MEMSET (&OutputRtInfo, 0, sizeof (tRtMapInfo));

    MEMCPY (&InputRtInfo.SrcXAddr, pPeerAddr, sizeof (tIPvXAddr));
    if (pPeerAddr->u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&u4TempAddr, InputRtInfo.SrcXAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
        u4TempAddr = OSIX_NTOHL (u4TempAddr);
        MEMCPY (InputRtInfo.SrcXAddr.au1Addr, &u4TempAddr, IPVX_IPV4_ADDR_LEN);
    }
    nmhGetFsMsdpPeerFilter (&i4GlobalFilter);

    i4RetVal = RMapApplyRule (&InputRtInfo, &OutputRtInfo, pRtMap);

    if (i4RetVal == RMAP_ENTRY_NOT_MATCHED)
    {
        if (i4GlobalFilter == MSDP_ACCEPT_ALL)
        {
            i4PeerFilter = MSDP_PEER_FILTER_ACCEPT;
        }
        else
        {
            i4PeerFilter = MSDP_PEER_FILTER_DENY;
        }
    }
    else
    {
        if (i4RetVal == RMAP_ROUTE_DENY)
        {
            i4PeerFilter = MSDP_PEER_FILTER_DENY;
        }
        else
        {
            i4PeerFilter = MSDP_PEER_FILTER_ACCEPT;
        }
    }
    return i4PeerFilter;
#else
    UNUSED_PARAM (pPeerAddr);
    UNUSED_PARAM (pRtMap);
    return MSDP_PEER_FILTER_ACCEPT;
#endif
}
