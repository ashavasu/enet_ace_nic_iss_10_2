/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 *$Id: msdpmain.c,v 1.9 2013/12/16 15:22:40 siva Exp $

 * Description: This file contains the msdp task main loop
 *              and the initialization routines.
 *
 *******************************************************************/
#define __MSDPMAIN_C__
#include "msdpinc.h"

/* Proto types of the functions private to this file only */

PRIVATE UINT4 MsdpMainMemInit PROTO ((VOID));
PRIVATE VOID MsdpMainMemClear PROTO ((VOID));

/****************************************************************************
*                                                                           *
* Function     : MsdpMainTask                                               *
*                                                                           *
* Description  : Main function of MSDP.                                     *
*                                                                           *
* Input        : pTaskId                                                    *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : VOID                                                       *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
MsdpMainTask ()
{
    UINT4               u4Event = 0;

    MSDP_TRC_FUNC ((MSDP_MAIN_TRC | MSDP_FN_ENTRY, "\nENTRY: MsdpMainTask"));

    if (TmrCreateTimerList ((CONST UINT1 *) MSDP_TASK_NAME, MSDP_TIMER_EVENT,
                            NULL, (tTimerListId *) & (gMsdpGlobals.msdpTmrLst))
        == TMR_FAILURE)
    {

        MSDP_TRC_FUNC ((MSDP_MAIN_TRC,
                        "MsdpMainTask: MSDP Timer List Creation Failed"));

        return;
    }

    while (OSIX_TRUE == OSIX_TRUE)
    {

        OsixReceiveEvent (MSDP_TIMER_EVENT | MSDP_QUEUE_EVENT |
                          MSDP_V4_PASSIVE_CONN_EVT | MSDP_V6_PASSIVE_CONN_EVT |
                          MSDP_SA_BATCH_ADVT_EVT,
                          (OSIX_WAIT | OSIX_EV_ANY), (UINT4) 0,
                          (UINT4 *) &(u4Event));

        MsdpMainTaskLock ();

        MSDP_TRC ((MSDP_MAIN_TRC, "Rx Event %d\n", u4Event));

        if ((u4Event & MSDP_TIMER_EVENT) == MSDP_TIMER_EVENT)
        {

            MSDP_TRC_FUNC ((MSDP_MAIN_TRC,
                            "\nMsdpMainTask: MSDP_TIMER_EVENT Rcvd"));

            MsdpTmrHandleExpiry ();
        }

        if ((u4Event & MSDP_QUEUE_EVENT) == MSDP_QUEUE_EVENT)
        {
            MSDP_TRC_FUNC ((MSDP_MAIN_TRC,
                            "\nMsdpMainTask: MSDP_QUEUE_EVENT Rcvd"));

            MsdpQueProcessMsgs ();
        }

        if ((u4Event & MSDP_V4_PASSIVE_CONN_EVT) == MSDP_V4_PASSIVE_CONN_EVT)
        {

            MSDP_TRC_FUNC ((MSDP_MAIN_TRC,
                            "\nMsdpMainTask: MSDP_V4_PASSIVE_CONN_EVT Rcvd"));

            MsdpSockHandleNewPassiveConnEvt (gMsdpGlobals.i4MsdpSrvSockId);
        }

        if ((u4Event & MSDP_V6_PASSIVE_CONN_EVT) == MSDP_V6_PASSIVE_CONN_EVT)
        {
            MSDP_TRC_FUNC ((MSDP_MAIN_TRC,
                            "\nMsdpMainTask: MSDP_V6_PASSIVE_CONN_EVT\n"));

            MsdpSock6HandleNewPassiveConnEvt (gMsdpGlobals.i4Msdpv6SrvSockId);
        }

        if ((u4Event & MSDP_SA_BATCH_ADVT_EVT) == MSDP_SA_BATCH_ADVT_EVT)
        {
            MSDP_TRC_FUNC ((MSDP_MAIN_TRC,
                            "\nMsdpMainTask: MSDP_SA_BATCH_ADVT_EVT\n"));

            MsdCacheSAInfoBatchAdvt (&gMsdpGlobals.CacheAdvtMarker);

        }

        /* Mutual exclusion flag OFF */
        MsdpMainTaskUnLock ();
    }

}

/****************************************************************************
*                                                                           *
* Function     : MsdpMainTaskInit                                           *
*                                                                           *
* Description  : MSDP initialization routine.                               *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*                                                                           *
*****************************************************************************/

PUBLIC UINT4
MsdpMainTaskInit (VOID)
{

    MSDP_TRC_FUNC ((MSDP_MAIN_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpMainTaskInit"));

#ifdef MSDP_TRACE_WANTED
    MSDP_TRC_FLAG = ~((UINT4) 0);
#endif

    MEMSET (&gMsdpGlobals, 0, sizeof (gMsdpGlobals));
    MEMCPY (gMsdpGlobals.au1TaskSemName, MSDP_MUT_EXCL_SEM_NAME, OSIX_NAME_LEN);

    if (OsixCreateSem (MSDP_MUT_EXCL_SEM_NAME, MSDP_SEM_CREATE_INIT_CNT, 0,
                       &gMsdpGlobals.msdpTaskSemId) == OSIX_FAILURE)
    {

        MSDP_TRC_FUNC ((MSDP_MAIN_TRC,
                        "\nMsdpMainTaskInit: Semaphore Creation failure for %s ",
                        MSDP_MUT_EXCL_SEM_NAME));

        return OSIX_FAILURE;
    }
    if (MsdpUtlCreateRBTree () == OSIX_FAILURE)
    {

        MSDP_TRC_FUNC ((MSDP_MAIN_TRC,
                        "\nMsdpMainTaskInit: RBTree Creation failure"));

        return OSIX_FAILURE;
    }

    /* Create buffer pools for data structures */

    if (MsdpMainMemInit () == OSIX_FAILURE)
    {

        MSDP_TRC_FUNC ((MSDP_MAIN_TRC, "\nMsdpMainTaskInit: "
                        "Memory Pool Creation Failed"));

        return OSIX_FAILURE;
    }

    if (OsixCreateQ (MSDP_QUEUE_NAME, MSDP_QUEUE_DEPTH, (UINT4) 0,
                     (tOsixQId *) & (gMsdpGlobals.msdpQueId)) == OSIX_FAILURE)
    {

        MSDP_TRC_FUNC ((MSDP_MAIN_TRC, "\nMsdpMainTaskInit: "
                        "IAPQ Creation Failed"));

        return OSIX_FAILURE;
    }

    MsdpTmrInitTmrDesc ();

    /* The following routine initialises the timer descriptor structure */
    MsdpUtilInitGlobals (MSDP_ZERO);

    MSDP_TRC_FUNC ((MSDP_FN_EXIT, "FUNC:MsdpMainTaskInit\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : MsdpMainDeInit                                             */
/*                                                                           */
/* Description  : Deleting the resources when task init fails.               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
MsdpMainDeInit (VOID)
{

    MSDP_TRC_FUNC ((MSDP_MAIN_TRC | MSDP_FN_ENTRY, "\nENTRY: MsdpMainDeInit"));

    MsdpMainMemClear ();

    if (gMsdpGlobals.msdpTaskSemId)
    {
        OsixSemDel (gMsdpGlobals.msdpTaskSemId);
    }
    if (gMsdpGlobals.msdpQueId)
    {
        OsixQueDel (gMsdpGlobals.msdpQueId);
    }
    TmrDeleteTimerList (gMsdpGlobals.msdpTmrLst);

    MSDP_TRC_FUNC ((MSDP_MAIN_TRC | MSDP_FN_EXIT, "\nEXIT: MsdpMainDeInit"));

}

/*****************************************************************************/
/*                                                                           */
/* Function     : MsdpMainTaskLock                                           */
/*                                                                           */
/* Description  : Lock the Msdp Main Task                                    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
MsdpMainTaskLock (VOID)
{

    MSDP_TRC_FUNC ((MSDP_MAIN_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpMainTaskLock"));

    if (OsixSemTake (gMsdpGlobals.msdpTaskSemId) == OSIX_FAILURE)
    {
        MSDP_TRC_FUNC ((MSDP_MAIN_TRC, "MsdpMainTaskLock: TakeSem failure for "
                        "%s \n", MSDP_MUT_EXCL_SEM_NAME));
        return SNMP_FAILURE;
    }

    MSDP_TRC_FUNC ((MSDP_MAIN_TRC | MSDP_FN_EXIT, "\nEXIT: MsdpMainTaskLock"));

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : MsdpMainTaskUnLock                                         */
/*                                                                           */
/* Description  : UnLock the MSDP Task                                       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
MsdpMainTaskUnLock (VOID)
{
    MSDP_TRC_FUNC ((MSDP_MAIN_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpMainTaskUnLock"));

    OsixSemGive (gMsdpGlobals.msdpTaskSemId);

    MSDP_TRC_FUNC ((MSDP_MAIN_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpMainTaskUnLock"));

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : MsdpMainMemInit                                            */
/*                                                                           */
/* Description  : Allocates all the memory that is required for MSDP         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT4
MsdpMainMemInit (VOID)
{

    MSDP_TRC_FUNC ((MSDP_MAIN_TRC | MSDP_FN_ENTRY, "\nENTRY: MsdpMainMemInit"));

    if (MsdpSizingMemCreateMemPools () == OSIX_FAILURE)
    {

        MSDP_TRC_FUNC ((MSDP_MAIN_TRC, "\nMsdpMainMemInit: "
                        "MsdpSizingMemCreateMemPools returns failure"));

        return OSIX_FAILURE;
    }

    MSDP_TRC_FUNC ((MSDP_MAIN_TRC | MSDP_FN_EXIT, "\nEXIT: MsdpMainMemInit"));

    return OSIX_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : MsdpMainMemClear                                           */
/*                                                                           */
/* Description  : Clears all the Memory                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
MsdpMainMemClear (VOID)
{

    MSDP_TRC_FUNC ((MSDP_MAIN_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpMainMemClear"));

    MsdpSizingMemDeleteMemPools ();

    MSDP_TRC_FUNC ((MSDP_MAIN_TRC | MSDP_FN_EXIT, "\nEXIT: MsdpMainMemClear"));

}

/*****************************************************************************/
/* Function     : MsdpMainHdlAdminDown                                       */
/*                                                                           */
/* Description  : Handles the Admin Down                                     */
/*                                                                           */
/* Input        : u1AddrType                                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
MsdpMainHdlAdminDown (UINT1 u1AddrType)
{
    tMsdpFsMsdpPeerEntry Peer;
    tMsdpFsMsdpPeerEntry *pNextPeer = NULL;
    tMsdpFsMsdpSACacheEntry Cache;
    tMsdpFsMsdpSACacheEntry *pNextCache = NULL;

    MSDP_TRC_FUNC ((MSDP_MAIN_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpMainHdlAdminDown"));

    MsdpSockServerStop (u1AddrType);

    MEMSET (&Peer, 0, sizeof (tMsdpFsMsdpPeerEntry));
    Peer.MibObject.i4FsMsdpPeerAddrType = u1AddrType;

    while ((pNextPeer = MsdpGetNextFsMsdpPeerTable (&Peer)) != NULL)
    {
        if (pNextPeer->MibObject.i4FsMsdpPeerAddrType != u1AddrType)
        {
            break;
        }
        MsdpPeerRunPfsm (pNextPeer, MSDP_PEER_STOP_EVT);

        MemReleaseMemBlock (MSDP_FSMSDPPEER_RX_TX_BUF_POOLID,
                            pNextPeer->TxBufInfo.pu1Msg);
        MemReleaseMemBlock (MSDP_FSMSDPPEER_RX_TX_BUF_POOLID,
                            pNextPeer->RxBufInfo.pu1Msg);

        RBTreeRem (gMsdpGlobals.MsdpGlbMib.FsMsdpPeerTable, pNextPeer);

        MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID, (UINT1 *) pNextPeer);

        pNextPeer = NULL;

    }

    MEMSET (&Cache, 0, sizeof (tMsdpFsMsdpSACacheEntry));
    Cache.MibObject.i4FsMsdpSACacheAddrType = u1AddrType;
    while ((pNextCache = MsdpGetNextFsMsdpSACacheTable (&Cache)) != NULL)
    {
        if (pNextCache->MibObject.i4FsMsdpSACacheAddrType != u1AddrType)
        {
            break;
        }
        MsdpTmrStopTmr (&pNextCache->CacheSaStateTimer);
        MsdpUtilRemoveCacheEntry (pNextCache);
        MEMCPY (&Cache, pNextCache, sizeof (Cache));
    }

    MsdpUtilInitGlobals (u1AddrType);

    MSDP_TRC_FUNC ((MSDP_MAIN_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpMainHdlAdminDown"));

    return;
}

/*****************************************************************************/
/* Function     : MsdpMainHdlMsdpAdminStatusChg                              */
/*                                                                           */
/* Description  : Handles the Admin status changes                           */
/*                                                                           */
/* Input        : pMsdpConfigAdmin                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
INT4
MsdpMainHdlMsdpAdminStatusChg (tMsdpConfigAdmin * pMsdpConfigAdmin)
{
    UINT1               u1AddrType = 0;
    INT4                i4AdminStatus = 0;
    INT4                i4RetVal = 0;
    UINT2               u2Port = 0;

    MSDP_TRC_FUNC ((MSDP_MAIN_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpMainHdlMsdpAdminStatusChg"));

    u1AddrType = pMsdpConfigAdmin->u1AddrType;
    i4AdminStatus = pMsdpConfigAdmin->i4MsdpAdminStatus;

    if (i4AdminStatus == MSDP_DISABLED)
    {

        MSDP_TRC_FUNC ((MSDP_MAIN_TRC, "\nMsdpMainHdlMsdpAdminStatusChg: "
                        "MSDP Status Disabled"));

        if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            MsdpPortDeRegiterWithIP ();

            MsdpMainHdlAdminDown (u1AddrType);
            gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv4AdminStat = i4AdminStatus;
            MSDP_TRC_FUNC ((MSDP_MAIN_TRC, "\nMsdpMainHdlMsdpAdminStatusChg: "
                            "MSDP Deregistered with IPv4"));
        }
        else
        {
            Msd6PortDeRegiterWithIP6 ();
            MsdpMainHdlAdminDown (u1AddrType);
            gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv6AdminStat = i4AdminStatus;
            MSDP_TRC_FUNC ((MSDP_MAIN_TRC, "\nMsdpMainHdlMsdpAdminStatusChg: "
                            "MSDP Deregistered with IPv6"));
        }
    }
    else
    {
        u2Port = (UINT2) gMsdpGlobals.MsdpGlbMib.i4FsMsdpListenerPort;
        if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            if (OSIX_FAILURE == MsdpPortRegiterWithIP ())
            {

                MSDP_TRC_FUNC ((MSDP_MAIN_TRC,
                                "\nMsdpMainHdlMsdpAdminStatusChg: "
                                "MsdpPortRegiterWithIP returns failure"));

                return OSIX_FAILURE;
            }

            MSDP_TRC_FUNC ((MSDP_MAIN_TRC, "\nMsdpMainHdlMsdpAdminStatusChg: "
                            "MSDP registered with IPv4"));

            i4RetVal = MsdpSockServerStart (u2Port);
            if (i4RetVal == OSIX_FAILURE)
            {
                MsdpPortDeRegiterWithIP ();
                return OSIX_FAILURE;
            }
            gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv4AdminStat = i4AdminStatus;

            MSDP_TRC_FUNC ((MSDP_MAIN_TRC, "\nMsdpMainHdlMsdpAdminStatusChg: "
                            "MSDP-IPv4 Server socket created successfully"));
        }
        else
        {
            if (OSIX_FAILURE == Msd6PortRegiterWithIP6 ())
            {

                MSDP_TRC_FUNC ((MSDP_MAIN_TRC,
                                "\nMsdpMainHdlMsdpAdminStatusChg: "
                                "Msd6PortRegiterWithIP6 returns failure"));

                return OSIX_FAILURE;
            }

            MSDP_TRC_FUNC ((MSDP_MAIN_TRC, "\nMsdpMainHdlMsdpAdminStatusChg: "
                            "MSDP registered with IPv6"));

            i4RetVal = MsdpSock6ServerStart (u2Port);
            if (i4RetVal == OSIX_FAILURE)
            {
                Msd6PortDeRegiterWithIP6 ();
                return OSIX_FAILURE;
            }

            gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv6AdminStat = i4AdminStatus;
            MSDP_TRC_FUNC ((MSDP_MAIN_TRC, "\nMsdpMainHdlMsdpAdminStatusChg: "
                            "MSDP-IPv6 Server socket created successfully"));
        }
    }

    MSDP_TRC_FUNC ((MSDP_MAIN_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpMainHdlMsdpAdminStatusChg"));

    return i4RetVal;
}

VOID
MsdpMainHandleIfChange (UINT1 u1AddrType, UINT4 u4IfIndex,
                        UINT4 u4AdminStatus, UINT4 u4OperStatus)
{
    tMsdpFsMsdpRPEntry  RpEntry;
    tMsdpFsMsdpRPEntry *pRp = NULL;
    tMsdpEventTrap      MsdpTrapInfo;
    UINT4               u4RetVal = u4IfIndex;

    RpEntry.MibObject.i4FsMsdpRPAddrType = u1AddrType;

    pRp = MsdpGetFsMsdpRPTable (&RpEntry);
    if (NULL == pRp)
    {
        return;
    }
    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MsdpPortGetIpPortFromIfIndex ((UINT4) pRp->i4IfIndex, &u4RetVal);
    }
    if (u4RetVal != u4IfIndex)
    {
        return;
    }

    if (u4OperStatus == MSDP_IF_OPERSTATUS_DOWN ||
        u4AdminStatus == MSDP_IF_OPERSTATUS_DOWN)
    {
        pRp->MibObject.i4FsMsdpRPOperStatus = MSDP_IF_OPERSTATUS_DOWN;
    }
    else
    {
        pRp->MibObject.i4FsMsdpRPOperStatus = MSDP_IF_OPERSTATUS_UP;
    }

    MEMSET (&MsdpTrapInfo, 0, sizeof (tMsdpEventTrap));

    MsdpTrapInfo.i4AddrType = u1AddrType;
    MsdpTrapInfo.u4EventId = pRp->MibObject.i4FsMsdpRPOperStatus;

    MsdpUtilSendEventTrap (&MsdpTrapInfo, MSDP_RP_OPERSTATUS_TRAP_ID);

}

/*-----------------------------------------------------------------------*/
/*                       End of the file  msdpmain.c                     */
/*-----------------------------------------------------------------------*/
