/********************************************************************
 *Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 *$Id: msdptrc.c,v 1.2 2011/01/21 15:04:26 siva Exp $
 *
 *Description:This file holds the functions to handle trace 
 *            messages of MSDP
 *
 *********************************************************/
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "msdpinc.h"

/****************************************************************************
*                                                                           *
* Function     : MsdpTrcPrint                                               *
*                                                                           *
* Description  :  prints the trace - with filename and line no              *
*                                                                           *
* Input        : fname   - File name                                        *
*                u4Line  - Line no                                          *
*                s       - strong to be printed                             *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

VOID
MsdpTrcPrint (const char *fname, UINT4 u4Line, const char *s)
{

    tOsixSysTime        sysTime = 0;
    const char         *pc1Fname = fname;

    if (s == NULL)
    {
        return;
    }
    while (*fname != '\0')
    {
        if (*fname == '/')
            pc1Fname = (fname + 1);
        fname++;
    }
    OsixGetSysTime (&sysTime);
    printf ("MSDP: %d:%s:%d:   %s", sysTime, pc1Fname, u4Line, s);
}

/****************************************************************************
*                                                                           *
* Function     : MsdpTrcWrite                                               *
*                                                                           *
* Description  :  prints the trace - without filename and line no ,         *
*                 Useful for dumping packets                                *
*                                                                           *
* Input        : s - string to be printed                                   *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

VOID
MsdpTrcWrite (CHR1 * s)
{
    if (s != NULL)
    {
        printf ("%s", s);
    }
}

/****************************************************************************
*                                                                           *
* Function     : MsdpTrc                                                    *
*                                                                           *
* Description  : converts variable argument in to string depending on flag  *
*                                                                           *
* Input        : u4Flags  - Trace flag                                      *
*                fmt  - format strong, variable argument
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

CHR1               *
MsdpTrc (UINT4 u4Flags, const char *fmt, ...)
{
    va_list             ap;
#define MSDP_TRC_BUF_SIZE    2000
    static CHR1         buf[MSDP_TRC_BUF_SIZE];

    if ((u4Flags & MSDP_TRC_FLAG) <= 0)
    {
        return (NULL);
    }
    va_start (ap, fmt);
    vsprintf (&buf[0], fmt, ap);
    va_end (ap);

    return (&buf[0]);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  msdptrc.c                      */
/*-----------------------------------------------------------------------*/
