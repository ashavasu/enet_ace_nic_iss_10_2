/********************************************************************
 * * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * *
 * * $Id: msdpeer.c,v 1.8 2012/07/10 05:50:03 siva Exp $
 * *
 * * Description: This file contains the Msdp Peer module related routines
 * *********************************************************************/
#include "msdpinc.h"
#include "msdppfsm.h"
#include "msdptrc.h"
#include "fssocket.h"

VOID
MsdpPeerDestroyCacheFromPeer (tMsdpFsMsdpPeerEntry * pPeer)
{
    tMsdpFsMsdpSACacheEntry *pNextCache = NULL;
    tMsdpFsMsdpSACacheEntry CurCache;
    tMsdpCacheUpdt      MsdpCacheInfo;
    INT4                i4RetVal = 0;
    UINT1               u1Status = 0;

    MEMSET (&CurCache, 0, sizeof (tMsdpFsMsdpSACacheEntry));
    MEMSET (&MsdpCacheInfo, 0, sizeof (tMsdpCacheUpdt));

    while (OSIX_TRUE == OSIX_TRUE)
    {
        pNextCache = MsdpGetNextFsMsdpSACacheTable (&CurCache);

        if (pNextCache == NULL)
        {
            break;
        }

        MEMCPY (&CurCache, pNextCache, sizeof (tMsdpFsMsdpSACacheEntry));

        if (pNextCache->MibObject.i4FsMsdpSACacheAddrType !=
            pPeer->MibObject.i4FsMsdpPeerAddrType)
        {
            continue;
        }

        i4RetVal = MEMCMP
            (pNextCache->MibObject.au1FsMsdpSACachePeerLearnedFrom,
             pPeer->MibObject.au1FsMsdpPeerRemoteAddress,
             pPeer->MibObject.i4FsMsdpPeerRemoteAddressLen);
        if (i4RetVal != 0)
        {
            continue;
        }

        IPVX_ADDR_INIT (MsdpCacheInfo.OriginatorRP,
                        (UINT1) pPeer->MibObject.i4FsMsdpPeerAddrType,
                        pNextCache->MibObject.au1FsMsdpSACacheOriginRP);
        IPVX_ADDR_INIT (MsdpCacheInfo.GrpAddr,
                        (UINT1) pPeer->MibObject.i4FsMsdpPeerAddrType,
                        pNextCache->MibObject.au1FsMsdpSACacheGroupAddr);
        IPVX_ADDR_INIT (MsdpCacheInfo.SrcAddr,
                        (UINT1) pPeer->MibObject.i4FsMsdpPeerAddrType,
                        pNextCache->MibObject.au1FsMsdpSACacheSourceAddr);
        MsdpCacheInfo.u1AddrType =
            (UINT1) pPeer->MibObject.i4FsMsdpPeerAddrType;
        MsdpCacheInfo.u1Action = MSDP_DELETE_CACHE;

        MsdCacheUpdateCacheTbl (MsdpCacheInfo, 0, &u1Status);
    }
}

/****************************************************************************
 * FUNCTION NAME : MsdpPeerRunPfsm
 *
 * DESCRIPTION   : iFSM for MSDP Peer- For different present states and the 
 *                 desired events, the fsm is run and the new state is set
 *
 * INPUTS        : pPeer - The MSDP Peer Entry
 *                 u1PeerEvent - The Peer event
 *
 * OUTPUTS       : pPeer - The MSDP Peer - i4FsMsdpPeerState is modified
 *
 * RETURNS       : None
 ***************************************************************************/
VOID
MsdpPeerRunPfsm (tMsdpFsMsdpPeerEntry * pPeer, UINT1 u1PeerEvent)
{
    tMsdpFsMsdpPeerEntry *pPeerAddr = NULL;
    tIPvXAddr           Peer;
    INT4                i4State = 0;
    INT4                i4RetVal = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_ENTRY, "\nENTRY: MsdpPeerRunPfsm"));

    if (pPeer == NULL)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_PEER, "\nMsdpPeerRunPfsm: pPeer is NULL"));

        return;
    }

    IPVX_ADDR_INIT (Peer, (UINT1) pPeer->MibObject.i4FsMsdpPeerAddrType,
                    pPeer->MibObject.au1FsMsdpPeerRemoteAddress);

    i4RetVal = MsdpPeerUtilGetPeerEntry (&Peer, Peer.u1Afi, &pPeerAddr);

    if (i4RetVal == OSIX_FAILURE)
    {
        /* As the peer doesnot exist, exiting the function */
        return;
    }

    i4State = pPeer->MibObject.i4FsMsdpPeerState;

    if (!
        ((i4State < MSDP_MAX_PEER_STATE)
         && (u1PeerEvent < MSDP_MAX_PEER_EVENT)))
    {
        MSDP_TRC_FUNC ((MSDP_TRC_PEER,
                        "\nMsdpPeerRunPfsm: Peer State or Event is not correct"));

        MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_EXIT,
                        "\nEXIT: MsdpPeerRunPfsm"));

        return;
    }

    /* SEM Function is executed now */
    (*aPfsmFunc[au1PfsmTable[u1PeerEvent][i4State]]) (pPeer);
    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_EXIT, "\nEXIT: MsdpPeerRunPfsm"));

    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPeerFsmInvalid
 * 
 * DESCRIPTION   : This function contains action to be done when the peer 
 *                 moves to Invalid state
 * 
 * INPUTS        : Peer Entry
 * 
 * OUTPUTS       : None
 * 
 * RETURNS       : None
 ***************************************************************************/
VOID
MsdpPeerFsmInvalid (tMsdpFsMsdpPeerEntry * pPeer)
{
    UNUSED_PARAM (pPeer);
    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPeerHdlAdminStatusConfig
 * 
 * DESCRIPTION   : This function contains action to be done when the peer 
 *                 moves to Enable state
 * 
 * INPUTS        : Peer Entry
 * 
 * OUTPUTS       : None
 * 
 * RETURNS       : None
 ***************************************************************************/
VOID
MsdpPeerHdlAdminStatusEnable (tMsdpFsMsdpPeerEntry * pPeer)
{
    tOsixSysTime        SysTime;
    UINT1              *pLocalAddr = NULL;
    UINT1              *pRemoteAddr = NULL;

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPeerHdlAdminStatusEnable"));

    pPeer->MibObject.i4FsMsdpPeerState = MSDP_PEER_INACTIVE;

    pLocalAddr = pPeer->MibObject.au1FsMsdpPeerLocalAddress;
    pRemoteAddr = pPeer->MibObject.au1FsMsdpPeerRemoteAddress;

    pPeer->i4SockFd = MSDP_INVALID_SOCK_ID;

    if (MEMCMP (pLocalAddr, pRemoteAddr, IPVX_MAX_INET_ADDR_LEN) > 0)
    {
        /* This Peer is the server */
        MsdpPeerRunPfsm (pPeer, MSDP_PEER_PASSIVE_OPEN_EVT);
    }
    else
    {
        MsdpPeerRunPfsm (pPeer, MSDP_PEER_ACTIVE_OPEN_EVT);
    }

    OsixGetSysTime (&SysTime);
    pPeer->MibObject.u4FsMsdpPeerUpTime = (UINT4) SysTime;

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPeerHdlAdminStatusEnable"));

    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPeerHdlAdminStatusConfig
 * 
 * DESCRIPTION   : This function contains action to be done when the peer 
 *                 admin status is configured as Enable/Diable
 * 
 * INPUTS        : Peer Entry
 * 
 * OUTPUTS       : None
 * 
 * RETURNS       : None
 ***************************************************************************/
INT4
MsdpPeerHdlAdminStatusConfig (tMsdpConfigPeer * pConfigPeer)
{
    tMsdpFsMsdpPeerEntry *pPeer = NULL;

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPeerHdlAdminStatusConfig"));

    if (MsdpPeerUtilGetPeerEntry (&(pConfigPeer->PeerAddr),
                                  pConfigPeer->u1AddrType,
                                  &pPeer) == OSIX_FAILURE)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_PEER,
                        "\nMsdpPeerHdlAdminStatusConfig: No Peer Entry exists for %s",
                        MsdpPrintIPvxAddress (pConfigPeer->PeerAddr)));

        return OSIX_FAILURE;
    }

    if (pConfigPeer->u1PeerAdmin == MSDP_PEER_ADMIN_ENABLE)
    {
        MSDP_TRC_CRIT ((MSDP_TRC_PEER, "\n MsdpPeerHdlAdminStatusConfig"
                        "Admin Status Set to Enable for the Peer %s",
                        MsdpPrintIPvxAddress (pConfigPeer->PeerAddr)));
        MsdpPeerRunPfsm (pPeer, MSDP_PEER_START_EVT);
        MsdpPeerHdlAdminStatusEnable (pPeer);
        pPeer->MibObject.i4FsMsdpPeerAdminStatus = MSDP_PEER_ADMIN_ENABLE;
    }
    else
    {

        MSDP_TRC_CRIT ((MSDP_TRC_PEER, "\n MsdpPeerHdlAdminStatusConfig"
                        "Admin Status Set to Disable for the Peer %s",
                        MsdpPrintIPvxAddress (pConfigPeer->PeerAddr)));

        MsdpPeerRunPfsm (pPeer, MSDP_PEER_STOP_EVT);
        pPeer->MibObject.i4FsMsdpPeerAdminStatus = MSDP_PEER_ADMIN_DISABLE;
    }

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPeerHdlAdminStatusConfig"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPeerFsmPeerEnable
 * 
 * DESCRIPTION   : This function contains action to be done when the peer 
 *                 moves to Enable state
 * 
 * INPUTS        : Peer Entry
 * 
 * OUTPUTS       : None
 * 
 * RETURNS       : None
 ***************************************************************************/

VOID
MsdpPeerFsmPeerEnable (tMsdpFsMsdpPeerEntry * pPeer)
{

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPeerFsmPeerEnable"));

    pPeer->MibObject.i4FsMsdpPeerState = MSDP_PEER_INACTIVE;

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPeerFsmPeerEnable"));

    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPeerFsmInitiateConnection
 * 
 * DESCRIPTION   : This function describes the procedure for initiating the peer 
 *                 connection
 * 
 * INPUTS        : pPeer- The MSDP Peer Entry
 *
 * OUTPUTS       : pPeer- Its peer state is modified. Further, it is modified 
 *                 in MsdpSockOpenPeerConnection, MsdpSock6OpenPeerConnection
 *                 and MsdpPeerRunPfsm
 *
 * RETURNS       : None
 ***************************************************************************/
VOID
MsdpPeerFsmInitiateConnection (tMsdpFsMsdpPeerEntry * pPeer)
{
    UINT1               u1AddrType =
        (UINT1) pPeer->MibObject.i4FsMsdpPeerAddrType;
    INT4                i4ConnRetryVal = 0;
    INT4                i4Status = MSDP_TCP_OPEN_FAILURE;

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPeerFsmInitiateConnection"));

    pPeer->MibObject.i4FsMsdpPeerState = MSDP_PEER_CONNECT;

    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_PEER,
                        "\nMsdpPeerFsmInitiateConnection: Opening TCP connection"
                        "for the peer %s",
                        pPeer->MibObject.au1FsMsdpPeerRemoteAddress));

        i4Status = MsdpSockOpenPeerConnection (pPeer);
    }
    else
    {
        MSDP_TRC_FUNC ((MSDP_TRC_PEER,
                        "\nMsdpPeerFsmInitiateConnection: Opening TCP connection"
                        "for the peer %s",
                        pPeer->MibObject.au1FsMsdpPeerRemoteAddress));
        i4Status = MsdpSock6OpenPeerConnection (pPeer);
    }

    if (i4Status == MSDP_TCP_OPEN_SUCCESS)
    {

        MsdpPeerRunPfsm (pPeer, MSDP_CONN_ESTAB_EVT);

        MSDP_TRC_CRIT ((MSDP_TRC_PEER,
                        "\nMsdpPeerFsmInitiateConnection: TCP connection"
                        "established for the peer %s",
                        pPeer->MibObject.au1FsMsdpPeerRemoteAddress));
    }
    else if (i4Status == MSDP_TCP_OPEN_INPROGRESS)
    {
        i4ConnRetryVal = pPeer->MibObject.i4FsMsdpPeerConnectRetryInterval;
        MsdpTmrStartTmr (&pPeer->MsdpPeerConnRetryTimer, MSDP_CONN_RETRY_TMR,
                         (UINT4) i4ConnRetryVal);
        pPeer->MibObject.u4FsMsdpPeerConnectionAttempts++;

        MSDP_TRC_CRIT ((MSDP_TRC_PEER,
                        "\nMsdpPeerFsmInitiateConnection: TCP connection"
                        "in progress for the peer %s",
                        pPeer->MibObject.au1FsMsdpPeerRemoteAddress));
    }
    else
    {
        MsdpPeerRunPfsm (pPeer, MSDP_PEER_STOP_EVT);

        MSDP_TRC_CRIT ((MSDP_TRC_PEER,
                        "\nMsdpPeerFsmInitiateConnection: TCP connection"
                        "stopped for the peer %s",
                        pPeer->MibObject.au1FsMsdpPeerRemoteAddress));
    }

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPeerFsmInitiateConnection"));

    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPeerFsmPassiveOpen
 *
 * DESCRIPTION   : In this function, the Peer State is set to MSDP_PEER_LISTEN
 *                 for the passive open connection (TCP client)
 *
 * INPUTS        : pPeer- The MSDP peer
 *
 * OUTPUTS       : pPeer- Its peer state is modified
 *
 * RETURNS       : None
 ***************************************************************************/
VOID
MsdpPeerFsmPassiveOpen (tMsdpFsMsdpPeerEntry * pPeer)
{

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPeerFsmPassiveOpen"));

    pPeer->MibObject.i4FsMsdpPeerState = MSDP_PEER_LISTEN;

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPeerFsmPassiveOpen"));

    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPeerFsmHandleEstablished
 *
 * DESCRIPTION   : The timer is stopped and then started with defined values of
 *                 keep alive time and hold time
 *
 * INPUTS        : pPeer- The MSDP peer
 *
 * OUTPUTS       : pPeer- Its peer state, established transitions and
 *                 established time are modified. It is also modified in 
 *                 MsdpTmrStartTmr and MsdpTmrStopTmr
 *
 * RETURNS       : None
 *
 ***************************************************************************/
VOID
MsdpPeerFsmHandleEstablished (tMsdpFsMsdpPeerEntry * pPeer)
{
    tMsdpEventTrap      MsdpTrapInfo;
    tOsixSysTime        SysTime;
    INT4                i4KeepAliveVal = 0;
    INT4                i4PeerHoldVal = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPeerFsmHandleEstablished"));

    MEMSET (&MsdpTrapInfo, 0, sizeof (tMsdpEventTrap));
    pPeer->MibObject.i4FsMsdpPeerState = MSDP_PEER_ESTABLISHED;
    gMsdpGlobals.MsdpGlbMib.i4FsMsdpStatEstPeerCount++;
    MsdpTmrStopTmr (&pPeer->MsdpPeerConnRetryTimer);

    i4KeepAliveVal = pPeer->MibObject.i4FsMsdpPeerKeepAliveConfigured;
    if (i4KeepAliveVal != 0)
    {
        MsdpOutPeerKeepAlivePkt (pPeer);
    }

    i4PeerHoldVal = pPeer->MibObject.i4FsMsdpPeerHoldTimeConfigured;
    if (i4PeerHoldVal != 0)
    {
        MsdpTmrStartTmr (&pPeer->MsdpPeerHoldTimer, MSDP_PEER_HOLD_TMR,
                         (UINT4) i4PeerHoldVal);
    }

    pPeer->MibObject.u4FsMsdpPeerFsmEstablishedTransitions++;

    OsixGetSysTime (&SysTime);
    pPeer->MibObject.u4FsMsdpPeerFsmEstablishedTime = (UINT4) SysTime;

    MEMSET (&MsdpTrapInfo, 0, sizeof (tMsdpEventTrap));

    MsdpTrapInfo.i4AddrType = pPeer->MibObject.i4FsMsdpPeerAddrType;
    MEMCPY (MsdpTrapInfo.au1PeerAddr,
            pPeer->MibObject.au1FsMsdpPeerRemoteAddress,
            pPeer->MibObject.i4FsMsdpPeerRemoteAddressLen);
    MsdpTrapInfo.u4EventId = pPeer->MibObject.
        u4FsMsdpPeerFsmEstablishedTransitions;

    MsdpUtilSendEventTrap (&MsdpTrapInfo, MSDP_ESTABLISHED_TRAP_ID);

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPeerFsmHandleEstablished"));

    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPeerFsmHdlConnRetryExpiry
 *
 * DESCRIPTION   : This function is called when the connection retry timer 
 *                 is expired. 
 *                 MsdpPeerRunPfsm is called with MSDP_PEER_ACTIVE_OPEN_EVT
 *                 which tries to open a connection with the peer
 *
 * INPUTS        : pPeer- The MSDP peer entry
 *
 * OUTPUTS       : pPeer- Its peer state is modified. MsdpPeerRunPfsm modifies
 *                 it further
 *
 * RETURNS       : None
 ***************************************************************************/
VOID
MsdpPeerFsmHdlConnRetryExpiry (tMsdpFsMsdpPeerEntry * pPeer)
{

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPeerFsmHdlConnRetryExpiry"));

    pPeer->MibObject.i4FsMsdpPeerState = MSDP_PEER_INACTIVE;

    MsdpPeerRunPfsm (pPeer, MSDP_PEER_ACTIVE_OPEN_EVT);

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPeerFsmHdlConnRetryExpiry"));
    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPeerFsmPeerStop
 *
 * DESCRIPTION   : The peer state is set to DISABLED, the timers are stopped 
 *                 and buffer info are reset.
 *
 * INPUTS        : pPeer- The MSDP peer
 *
 * OUTPUTS       : pPeer- Peer state is modified. MsdpTmrStopTmr further 
 *                 modifies it. The buffer info values are reset
 *
 * RETURNS       : None
 *
 ***************************************************************************/
VOID
MsdpPeerFsmPeerStop (tMsdpFsMsdpPeerEntry * pPeer)
{
    tMsdpEventTrap      MsdpTrapInfo;
    tOsixSysTime        SysTime;

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPeerFsmPeerStop"));

    if (pPeer->MibObject.i4FsMsdpPeerState == MSDP_PEER_ESTABLISHED)
    {
        gMsdpGlobals.MsdpGlbMib.i4FsMsdpStatEstPeerCount--;
    }

    pPeer->MibObject.i4FsMsdpPeerState = MSDP_PEER_DISABLED;
    pPeer->MibObject.i4FsMsdpPeerAdminStatus = MSDP_PEER_ADMIN_DISABLE;

    MEMSET (&MsdpTrapInfo, 0, sizeof (tMsdpEventTrap));

    MsdpTrapInfo.i4AddrType = pPeer->MibObject.i4FsMsdpPeerAddrType;
    MEMCPY (MsdpTrapInfo.au1PeerAddr,
            pPeer->MibObject.au1FsMsdpPeerRemoteAddress,
            pPeer->MibObject.i4FsMsdpPeerRemoteAddressLen);
    MsdpTrapInfo.u4EventId = MSDP_PEER_DISABLED;

    MsdpSockClosePeerConnection (pPeer);

    MsdpUtilSendEventTrap (&MsdpTrapInfo, MSDP_BACKWARD_TRANSITION_TRAP_ID);

    OsixGetSysTime (&SysTime);
    pPeer->MibObject.u4FsMsdpPeerUpTime = (UINT4) SysTime;

    MsdpTmrStopTmr (&pPeer->MsdpPeerKATimer);
    MsdpTmrStopTmr (&pPeer->MsdpPeerHoldTimer);
    MsdpTmrStopTmr (&pPeer->MsdpPeerConnRetryTimer);

    pPeer->TxBufInfo.u2MsgSize = 0;
    pPeer->TxBufInfo.u2MsgOffset = 0;

    pPeer->RxBufInfo.u2MsgSize = 0;
    pPeer->RxBufInfo.u2MsgOffset = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPeerFsmPeerStop"));

    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPeerUtilFindPeerEntry
 *
 * DESCRIPTION   : This function gets the peer entry from the socket 
 *                 descriptor provided
 *
 * INPUTS        : i4SockFd- The socket descriptor
 *
 * OUTPUTS       : ppPeer- MSDP peer entry
 *
 * RETURNS       : OSIX_SUCCESS on successfully finding the desired peer entry
 *                 OSIX_FAILURE otherwise
 *
 ***************************************************************************/
INT4
MsdpPeerUtilFindPeerEntry (INT4 i4SockFd, tMsdpFsMsdpPeerEntry ** ppPeer)
{
    tMsdpFsMsdpPeerEntry Peer;
    tMsdpFsMsdpPeerEntry *pNextPeer = NULL;

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPeerUtilFindPeerEntry"));

    MEMSET (&Peer, 0, sizeof (tMsdpFsMsdpPeerEntry));

    while ((pNextPeer = MsdpGetNextFsMsdpPeerTable (&Peer)) != NULL)
    {
        if (pNextPeer->i4SockFd == i4SockFd)
        {
            *ppPeer = pNextPeer;

            MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_EXIT,
                            "\nEXIT: MsdpPeerUtilFindPeerEntry"));

            return OSIX_SUCCESS;
        }
        MEMCPY (&Peer, pNextPeer, sizeof (tMsdpFsMsdpPeerEntry));
    }

    MSDP_TRC_FUNC ((MSDP_TRC_PEER, "\nMsdpPeerUtilFindPeerEntry: "
                    "i4SockFd does not match any peer's SockFd"));

    return OSIX_FAILURE;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPeerUtilGetPeerEntry
 *
 * DESCRIPTION   : This function gets the peer entry from the peer address
 *
 * INPUTS        : pPeerAddr- The peer address
 *                 
 *                 u1AddrType- The peer address type
 *
 * OUTPUTS       : ppPeer- MSDP peer Entry
 *
 * RETURNS       : OSIX_SUCCESS on successfully retrieving the peer
 *                 OSIX_FAILURE otherwise
 *
 ***************************************************************************/
INT4
MsdpPeerUtilGetPeerEntry (tIPvXAddr * pPeerAddr, UINT1 u1AddrType,
                          tMsdpFsMsdpPeerEntry ** ppPeer)
{
    tMsdpFsMsdpPeerEntry Peer;
    tMsdpFsMsdpPeerEntry *pPeer = NULL;
    UINT1               u1AddrLen = IPVX_MAX_INET_ADDR_LEN;

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPeerUtilGetPeerEntry"));

    MEMSET (&Peer, 0, sizeof (tMsdpFsMsdpPeerEntry));

    MEMCPY (Peer.MibObject.au1FsMsdpPeerRemoteAddress, pPeerAddr->au1Addr,
            u1AddrLen);
    Peer.MibObject.i4FsMsdpPeerAddrType = u1AddrType;
    Peer.MibObject.i4FsMsdpPeerRemoteAddressLen = u1AddrLen;

    pPeer = MsdpGetFsMsdpPeerTable (&Peer);
    if (pPeer != NULL)
    {
        *ppPeer = pPeer;

        MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_EXIT,
                        "\nEXIT: MsdpPeerUtilGetPeerEntry"));

        return OSIX_SUCCESS;
    }

    MSDP_TRC_FUNC ((MSDP_TRC_PEER, "\nMsdpPeerUtilGetPeerEntry: "
                    "No Peer Entry exists for %s",
                    MsdpPrintIPvxAddress (*pPeerAddr)));

    return OSIX_FAILURE;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPeerValidateAndAcceptPeer
 * 
 * DESCRIPTION   : The function gets the peer using the peer address and peer 
 *                 address type, makes sure the filter status is ACCEPT and then
 *                 calls MsdpPeerRunPfsm with MSDP_CONN_ESTAB_EVT event.
 *
 * INPUTS        : pPeerAddr- The peer address
 *
 *                 u1AddrType- The address type
 *
 *                 i4ClientSockFd- The client socket descriptor
 *
 * OUTPUTS       : None
 *
 * RETURNS       : Returns OSIX_SUCCESS on getting the peer and verifying the 
 *                 filter status as accept,
 *                 OSIX_FAILURE otherwise
 *
 ***************************************************************************/
INT4
MsdpPeerValidateAndAcceptPeer (tIPvXAddr * pPeerAddr, UINT1 u1AddrType,
                               INT4 i4ClientSockFd)
{
    tMsdpFsMsdpPeerEntry *pPeerEntry = NULL;
    UINT1               u1PeerEvent = MSDP_CONN_ESTAB_EVT;

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPeerValidateAndAcceptPeer"));

    if (MsdpPeerUtilGetPeerEntry (pPeerAddr, u1AddrType,
                                  &pPeerEntry) != OSIX_SUCCESS)
    {
        close (i4ClientSockFd);
        MsdpPeerRunPfsm (pPeerEntry, MSDP_PEER_STOP_EVT);
        MSDP_TRC_FUNC ((MSDP_TRC_PEER, "\nMsdpPeerValidateAndAcceptPeer: "
                        "MsdpPeerUtilGetPeerEntry returns failure "
                        "for %s", MsdpPrintIPvxAddress (*pPeerAddr)));

        return OSIX_FAILURE;
    }
    if (MsdpPeerChkPeerFilterStatus (pPeerAddr, u1AddrType) !=
        MSDP_PEER_FILTER_ACCEPT)
    {
        close (i4ClientSockFd);
        MsdpPeerRunPfsm (pPeerEntry, MSDP_PEER_STOP_EVT);

        MSDP_TRC_FUNC ((MSDP_TRC_PEER, "\nMsdpPeerValidateAndAcceptPeer: "
                        "pPeerAddr's Filter Status not ACCEPT for "
                        "%s", MsdpPrintIPvxAddress (*pPeerAddr)));

        return OSIX_FAILURE;
    }

    /* If the configured peer is connecting again */
    if (pPeerEntry->MibObject.i4FsMsdpPeerState == MSDP_PEER_DISABLED)
    {
        /* the peer will be accepted only if the existing peer connection 
         * is in disabled state.
         * And then MSDP peer state is changed to LISTEN.
         * So that new client will be accepted and gets established*/
        pPeerEntry->MibObject.i4FsMsdpPeerState = MSDP_PEER_LISTEN;
    }
    else if (pPeerEntry->MibObject.i4FsMsdpPeerState == MSDP_PEER_ESTABLISHED)
    {
        /* the peer will be rejected,if the existing peer connection 
         * is in established state, 
         * the connecting peer's socket will be closed*/
        close (i4ClientSockFd);
        MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_EXIT,
                        "\nEXIT: MsdpPeerValidateAndAcceptPeer"));

        return OSIX_FAILURE;
    }

    pPeerEntry->i4SockFd = i4ClientSockFd;

    MsdpPeerRunPfsm (pPeerEntry, u1PeerEvent);

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPeerValidateAndAcceptPeer"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPeerHandleKeepAliveTmrExpiry
 *
 * DESCRIPTION   : This function hanldes keep alive timer expiry
 *                 Sends keep alive packet to the peer and starts
 *                 keep alive timer for the peer.
 *
 * INPUTS        : pPeer- The MSDP peer
 *
 * OUTPUTS       : None
 *
 * RETURNS       : None
 ***************************************************************************/
VOID
MsdpPeerHandleKeepAliveTmrExpiry (tMsdpFsMsdpPeerEntry * pPeer)
{
    tMsdpFsMsdpPeerEntry *pCurPeer = NULL;

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPeerHandleKeepAliveTmrExpiry"));

    pCurPeer = MsdpGetFsMsdpPeerTable (pPeer);
    if (pCurPeer == NULL)
    {
        return;
    }
    MsdpOutPeerKeepAlivePkt (pPeer);

    MSDP_TRC_FUNC ((MSDP_TRC_PEER | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPeerHandleKeepAliveTmrExpiry"));

}

/****************************************************************************
 * FUNCTION NAME : MsdpPeerChkPeerFilterStatus  
 *
 * DESCRIPTION   : This function check the peer filter status for the given peer 
 *                 address
 *
 * INPUTS        : pPeerAddress- The peer address
 *                 u1AddrType- The peer address type
 *
 * OUTPUTS       : None
 *
 * RETURNS       : OSIX_SUCCESS always
 *
 ***************************************************************************/
INT4
MsdpPeerChkPeerFilterStatus (tIPvXAddr * pPeerAddr, UINT1 u1AddrType)
{
    tMsdpFsMsdpPeerFilterEntry PeerFilter;
    tMsdpFsMsdpPeerFilterEntry *pPeerFilter = NULL;
    INT4                i4RetVal = MSDP_PEER_FILTER_ACCEPT;
    INT4             i4GlobalFilter = 0;
    MEMSET (&PeerFilter, 0, sizeof (tMsdpFsMsdpPeerFilterEntry));

    PeerFilter.MibObject.i4FsMsdpPeerFilterAddrType = u1AddrType;
    pPeerFilter = MsdpGetFsMsdpPeerFilterTable (&PeerFilter);

    if (pPeerFilter != NULL)
    {
        i4RetVal = MsdpPortPeerRMapRule
            (pPeerAddr, pPeerFilter->MibObject.au1FsMsdpPeerFilterRouteMap);
    }
    else
    {
        nmhGetFsMsdpPeerFilter (&i4GlobalFilter);
        if (i4GlobalFilter == MSDP_ACCEPT_ALL)
        {   
             i4RetVal = MSDP_PEER_FILTER_ACCEPT;
        }   
        else
        {   
             i4RetVal = MSDP_PEER_FILTER_DENY;
        }   
    }
    return i4RetVal;
}
