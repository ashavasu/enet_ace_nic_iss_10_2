/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *$Id: msdptmr.c,v 1.5 2011/08/09 14:27:20 siva Exp $
 *
 * Description : This file contains procedures containing Msdp Timer
 *               related operations
 ****************************************************************************/
#include "msdpinc.h"

PRIVATE tMsdpTmrDesc gaMsdpTmrDesc[MSDP_MAX_TMRS];

/****************************************************************************
*                                                                           *
* Function     : MsdpTmrInitTmrDesc                                         *
*                                                                           *
* Description  : Initialize Msdp Timer Decriptors                           *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
MsdpTmrInitTmrDesc ()
{

    MSDP_TRC_FUNC ((MSDP_TMR_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpTmrInitTmrDesc"));

    gaMsdpTmrDesc[MSDP_KEEP_ALIVE_TMR].pTmrExpFunc =
        MsdpTmrKeepAliveTmrExpiryHdlr;
    gaMsdpTmrDesc[MSDP_KEEP_ALIVE_TMR].i2Offset =
        (INT2) (MSDP_GET_OFFSET (tMsdpFsMsdpPeerEntry, MsdpPeerKATimer));

    gaMsdpTmrDesc[MSDP_CONN_RETRY_TMR].pTmrExpFunc = MsdpTmrConnRetryExpiryHdlr;
    gaMsdpTmrDesc[MSDP_CONN_RETRY_TMR].i2Offset =
        (INT2) (MSDP_GET_OFFSET (tMsdpFsMsdpPeerEntry, MsdpPeerConnRetryTimer));

    gaMsdpTmrDesc[MSDP_PEER_HOLD_TMR].pTmrExpFunc = MsdpTmrHoldTimerExpiryHdlr;
    gaMsdpTmrDesc[MSDP_PEER_HOLD_TMR].i2Offset =
        (INT2) (MSDP_GET_OFFSET (tMsdpFsMsdpPeerEntry, MsdpPeerHoldTimer));

    gaMsdpTmrDesc[MSDP_CACHE_STATE_TMR].pTmrExpFunc =
        MsdpTmrCacheStateTimerExpHdlr;
    gaMsdpTmrDesc[MSDP_CACHE_STATE_TMR].i2Offset =
        (INT2) (MSDP_GET_OFFSET (tMsdpFsMsdpSACacheEntry, CacheSaStateTimer));

    gaMsdpTmrDesc[MSDP_ACTIVE_SRC_ADVT_TMR].pTmrExpFunc =
        MsdTmrCacheSrcAdvtExpiryHandler;
    gaMsdpTmrDesc[MSDP_ACTIVE_SRC_ADVT_TMR].i2Offset = -1;

    MSDP_TRC_FUNC ((MSDP_TMR_TRC | MSDP_FN_EXIT, "\nEXIT: MsdpTmrInitTmrDesc"));

}

/****************************************************************************
*                                                                           *
* Function     : MsdpTmrStartTmr                                            *
*                                                                           *
* Description  : Starts Msdp Timer                                          *
*                                                                           *
* Input        : pMsdpTmr - pointer to MsdpTmr structure                    *
*                eMsdpTmrId - MSDP timer ID                                 *
*                u4Secs     - ticks in seconds                              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
MsdpTmrStartTmr (tMsdpTmr * pMsdpTmr, enMsdpTmrId eMsdpTmrId, UINT4 u4Secs)
{

    MSDP_TRC_FUNC ((MSDP_TMR_TRC | MSDP_FN_ENTRY, "\nENTRY: MsdpTmrStartTmr"));

    pMsdpTmr->eMsdpTmrId = eMsdpTmrId;

    TmrStopTimer (gMsdpGlobals.msdpTmrLst, &(pMsdpTmr->tmrNode));
    if (TmrStartTimer (gMsdpGlobals.msdpTmrLst, &(pMsdpTmr->tmrNode),
                       (UINT4) NO_OF_TICKS_PER_SEC * u4Secs) == TMR_FAILURE)
    {

        MSDP_TRC_FUNC ((MSDP_TMR_TRC, "\nMsdpTmrStartTmr: "
                        "Tmr Start Failure for the timer %d", eMsdpTmrId));

    }

    MSDP_TRC_FUNC ((MSDP_TMR_TRC | MSDP_FN_EXIT, "\nEXIT: MsdpTmrStartTmr"));

    return;
}

/****************************************************************************
*                                                                           *
* Function     : MsdpTmrRestartTmr                                          *
*                                                                           *
* Description  :  ReStarts Msdp Timer                                       *
*                                                                           *
* Input        : pMsdpTmr - pointer to MsdpTmr structure                    *
*                eMsdpTmrId - MSDP timer ID                                 *
*                u4Secs     - ticks in seconds                              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
MsdpTmrRestartTmr (tMsdpTmr * pMsdpTmr, enMsdpTmrId eMsdpTmrId, UINT4 u4Secs)
{

    MSDP_TRC_FUNC ((MSDP_TMR_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpTmrRestartTmr"));

    TmrStopTimer (gMsdpGlobals.msdpTmrLst, &(pMsdpTmr->tmrNode));
    MsdpTmrStartTmr (pMsdpTmr, eMsdpTmrId, u4Secs);

    MSDP_TRC_FUNC ((MSDP_TMR_TRC | MSDP_FN_EXIT, "\nEXIT: MsdpTmrRestartTmr"));

    return;
}

/****************************************************************************
*                                                                           *
* Function     : MsdpTmrStopTmr                                             *
*                                                                           *
* Description  : Restarts Msdp Timer                                        *
*                                                                           *
* Input        : pMsdpTmr - pointer to MsdpTmr structure                    *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
MsdpTmrStopTmr (tMsdpTmr * pMsdpTmr)
{

    MSDP_TRC_FUNC ((MSDP_TMR_TRC | MSDP_FN_ENTRY, "\nENTRY: MsdpTmrStopTmr"));

    TmrStopTimer (gMsdpGlobals.msdpTmrLst, &(pMsdpTmr->tmrNode));

    MSDP_TRC_FUNC ((MSDP_TMR_TRC | MSDP_FN_EXIT, "\nEXIT: MsdpTmrStopTmr"));

    return;
}

/****************************************************************************
*                                                                           *
* Function     : MsdpTmrHandleExpiry                                        *
*                                                                           *
* Description  : This procedure is invoked when the event indicating a time *
*                expiry occurs. This procedure finds the expired timers and *
*                invokes the corresponding timer routines.                  *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
MsdpTmrHandleExpiry (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    enMsdpTmrId         eMsdpTmrId = MSDP_KEEP_ALIVE_TMR;
    INT2                i2Offset = 0;

    MSDP_TRC_FUNC ((MSDP_TMR_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpTmrHandleExpiry"));

    while ((pExpiredTimers = TmrGetNextExpiredTimer (gMsdpGlobals.msdpTmrLst))
           != NULL)
    {

        eMsdpTmrId = ((tMsdpTmr *) pExpiredTimers)->eMsdpTmrId;
        if (eMsdpTmrId >= MSDP_MAX_TMRS)
        {
            return;
        }
        i2Offset = gaMsdpTmrDesc[eMsdpTmrId].i2Offset;

        if (i2Offset < 0)
        {

            /* The timer function does not take any parameter */

            (*(gaMsdpTmrDesc[eMsdpTmrId].pTmrExpFunc)) (NULL);

        }
        else
        {

            /* The timer function requires a parameter */

            (*(gaMsdpTmrDesc[eMsdpTmrId].pTmrExpFunc))
                ((UINT1 *) pExpiredTimers - i2Offset);
        }
    }

    MSDP_TRC_FUNC ((MSDP_TMR_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpTmrHandleExpiry"));

    return;

}

/****************************************************************************
*                                                                           *
* Function     : MsdpTmrConnRetryExpiryHdlr                                 *
*                                                                           *
* Description  : pPeer is assigned pArg and MsdpPeerRunPfsm is called with  *
*                that and MSDP_CONNECT_RETRY_EXPRY_EVT as arguments         *                                                           
*                                                                           *
* Input        : pArg- Void pointer                                         *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/
VOID
MsdpTmrConnRetryExpiryHdlr (VOID *pArg)
{
    tMsdpFsMsdpPeerEntry *pPeer = pArg;

    MSDP_TRC_FUNC ((MSDP_TMR_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpTmrConnRetryExpiryHdlr"));
    MsdpPeerRunPfsm (pPeer, MSDP_CONNECT_RETRY_EXPRY_EVT);

    MSDP_TRC_FUNC ((MSDP_TMR_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpTmrConnRetryExpiryHdlr"));

}

/****************************************************************************
*                                                                           *
* Function     : MsdpTmrHoldTimerExpiryHdlr                                 *
*                                                                           *
* Description  : This function handles the situation where the hold time has*
*                expired                                                    *
*                                                                           *
* Input        : pArg- A void pointer                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/
VOID
MsdpTmrHoldTimerExpiryHdlr (VOID *pArg)
{
    tMsdpFsMsdpPeerEntry *pPeer = NULL;

    MSDP_TRC_FUNC ((MSDP_TMR_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpTmrHoldTimerExpiryHdlr"));

    pPeer = pArg;
    MsdpPeerRunPfsm (pPeer, MSDP_PEER_HOLD_TMR_EXPRY);

    MSDP_TRC_FUNC ((MSDP_TMR_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpTmrHoldTimerExpiryHdlr"));

}

/****************************************************************************
*                                                                           *
* Function     : MsdpTmrKeepAliveTmrExpiryHdlr                              *
*                                                                           *
* Description  : This function handles the case where the keep alive timer  *
*                expires                                                    *
*                                                                           *
* Input        : pArg- A void pointer                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/
VOID
MsdpTmrKeepAliveTmrExpiryHdlr (VOID *pArg)
{

    tMsdpFsMsdpPeerEntry *pPeer = NULL;

    MSDP_TRC_FUNC ((MSDP_TMR_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpTmrKeepAliveTmrExpiryHdlr"));

    pPeer = pArg;
    MsdpPeerHandleKeepAliveTmrExpiry (pPeer);

    MSDP_TRC_FUNC ((MSDP_TMR_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpTmrKeepAliveTmrExpiryHdlr"));

}

/****************************************************************************
*                                                                           *
* Function     : MsdpTmrCacheStateTimerExpHdlr                              *
*                                                                           *
* Description  : This function handles the case where the cache state timer *
*                expires.
*                Timer for removing SA entries (Cache life time)            *
*                for the SA entries received from the MSDP peer. This timer *
*                value is configurable, should be >90s.                     *                             *
*                                                                           *
* Input        : pArg- A void pointer                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/
VOID
MsdpTmrCacheStateTimerExpHdlr (VOID *pArg)
{
    tMsdpFsMsdpSACacheEntry *pCache = NULL;
    tMsdpFsMsdpSACacheEntry *pCurCache = NULL;

    MSDP_TRC_FUNC ((MSDP_TMR_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpTmrCacheStateTimerExpHdlr"));

    pCache = pArg;
    pCurCache = MsdpGetFsMsdpSACacheTable (pCache);
    if (pCurCache == NULL)
    {
        return;
    }
    MsdCacheDeleteEntry (pCache, MSDP_DELETE_CACHE);

    MSDP_TRC_FUNC ((MSDP_TMR_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpTmrCacheStateTimerExpHdlr"));
}

/****************************************************************************
*                                                                           *
* Function     : MsdTmrCacheSrcAdvtExpiryHandler                            *
*                                                                           *
* Description  :   Timer for sending periodic SA Advertisement for
  SA entries originated by the MSDP router. This timer value is
  60s                             
*                                                                           *
* Input        : pArg- A void pointer                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/
VOID
MsdTmrCacheSrcAdvtExpiryHandler (VOID *pArg)
{
    UNUSED_PARAM (pArg);

    MEMSET (&gMsdpGlobals.CacheAdvtMarker, 0, sizeof (tMsdpCacheAdvtMarker));

    MsdpTmrStartTmr (&gMsdpGlobals.MsdpSaAdvtTimer, MSDP_ACTIVE_SRC_ADVT_TMR,
                     (UINT4) MSDP_SA_ADVT_TIME);

    gMsdpGlobals.CacheAdvtMarker.OrgRpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    gMsdpGlobals.CacheAdvtMarker.OrgRpAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    MsdCacheSAInfoBatchAdvt (&gMsdpGlobals.CacheAdvtMarker);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  msdptmr.c                      */
/*-----------------------------------------------------------------------*/
