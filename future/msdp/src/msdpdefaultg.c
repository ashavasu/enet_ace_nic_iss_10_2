/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: msdpdefaultg.c,v 1.1.1.1 2011/01/17 05:57:32 siva Exp $
*
* Description: This file contains the routines to initialize the
*              mib objects for the module Msdp 
*********************************************************************/

#include "msdpinc.h"

/****************************************************************************
* Function    : MsdpInitializeMibFsMsdpPeerTable
* Input       : pMsdpFsMsdpPeerEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
MsdpInitializeMibFsMsdpPeerTable (tMsdpFsMsdpPeerEntry * pMsdpFsMsdpPeerEntry)
{

    pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerConnectRetryInterval = 30;

    pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerHoldTimeConfigured = 75;

    pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerKeepAliveConfigured = 60;

    pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerDataTtl = 1;

    pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerMD5AuthPwdStat = 0;

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : MsdpInitializeMibFsMsdpSACacheTable
* Input       : pMsdpFsMsdpSACacheEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
MsdpInitializeMibFsMsdpSACacheTable (tMsdpFsMsdpSACacheEntry *
                                     pMsdpFsMsdpSACacheEntry)
{
    UNUSED_PARAM (pMsdpFsMsdpSACacheEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : MsdpInitializeMibFsMsdpMeshGroupTable
* Input       : pMsdpFsMsdpMeshGroupEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
MsdpInitializeMibFsMsdpMeshGroupTable (tMsdpFsMsdpMeshGroupEntry *
                                       pMsdpFsMsdpMeshGroupEntry)
{
    UNUSED_PARAM (pMsdpFsMsdpMeshGroupEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : MsdpInitializeMibFsMsdpRPTable
* Input       : pMsdpFsMsdpRPEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
MsdpInitializeMibFsMsdpRPTable (tMsdpFsMsdpRPEntry * pMsdpFsMsdpRPEntry)
{
    UNUSED_PARAM (pMsdpFsMsdpRPEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : MsdpInitializeMibFsMsdpPeerFilterTable
* Input       : pMsdpFsMsdpPeerFilterEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
MsdpInitializeMibFsMsdpPeerFilterTable (tMsdpFsMsdpPeerFilterEntry *
                                        pMsdpFsMsdpPeerFilterEntry)
{
    UNUSED_PARAM (pMsdpFsMsdpPeerFilterEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : MsdpInitializeMibFsMsdpSARedistributionTable
* Input       : pMsdpFsMsdpSARedistributionEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
MsdpInitializeMibFsMsdpSARedistributionTable (tMsdpFsMsdpSARedistributionEntry *
                                              pMsdpFsMsdpSARedistributionEntry)
{

    pMsdpFsMsdpSARedistributionEntry->MibObject.
        i4FsMsdpSARedistributionRouteMapStat = 0;

    return OSIX_SUCCESS;
}
