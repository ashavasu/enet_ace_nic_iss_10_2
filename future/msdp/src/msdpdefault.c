/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: msdpdefault.c,v 1.1.1.1 2011/01/17 05:57:32 siva Exp $
*
* Description: This file contains the routines to initialize the
*              protocol structure for the module Msdp 
*********************************************************************/

#include "msdpinc.h"

/****************************************************************************
* Function    : MsdpInitializeFsMsdpPeerTable
* Input       : pMsdpFsMsdpPeerEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
MsdpInitializeFsMsdpPeerTable (tMsdpFsMsdpPeerEntry * pMsdpFsMsdpPeerEntry)
{
    if (pMsdpFsMsdpPeerEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    if ((MsdpInitializeMibFsMsdpPeerTable (pMsdpFsMsdpPeerEntry)) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : MsdpInitializeFsMsdpSACacheTable
* Input       : pMsdpFsMsdpSACacheEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
MsdpInitializeFsMsdpSACacheTable (tMsdpFsMsdpSACacheEntry *
                                  pMsdpFsMsdpSACacheEntry)
{
    if (pMsdpFsMsdpSACacheEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((MsdpInitializeMibFsMsdpSACacheTable (pMsdpFsMsdpSACacheEntry)) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : MsdpInitializeFsMsdpMeshGroupTable
* Input       : pMsdpFsMsdpMeshGroupEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
MsdpInitializeFsMsdpMeshGroupTable (tMsdpFsMsdpMeshGroupEntry *
                                    pMsdpFsMsdpMeshGroupEntry)
{
    if (pMsdpFsMsdpMeshGroupEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((MsdpInitializeMibFsMsdpMeshGroupTable (pMsdpFsMsdpMeshGroupEntry)) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : MsdpInitializeFsMsdpRPTable
* Input       : pMsdpFsMsdpRPEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
MsdpInitializeFsMsdpRPTable (tMsdpFsMsdpRPEntry * pMsdpFsMsdpRPEntry)
{
    if (pMsdpFsMsdpRPEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((MsdpInitializeMibFsMsdpRPTable (pMsdpFsMsdpRPEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : MsdpInitializeFsMsdpPeerFilterTable
* Input       : pMsdpFsMsdpPeerFilterEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
MsdpInitializeFsMsdpPeerFilterTable (tMsdpFsMsdpPeerFilterEntry *
                                     pMsdpFsMsdpPeerFilterEntry)
{
    if (pMsdpFsMsdpPeerFilterEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((MsdpInitializeMibFsMsdpPeerFilterTable (pMsdpFsMsdpPeerFilterEntry)) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : MsdpInitializeFsMsdpSARedistributionTable
* Input       : pMsdpFsMsdpSARedistributionEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
MsdpInitializeFsMsdpSARedistributionTable (tMsdpFsMsdpSARedistributionEntry *
                                           pMsdpFsMsdpSARedistributionEntry)
{
    if (pMsdpFsMsdpSARedistributionEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((MsdpInitializeMibFsMsdpSARedistributionTable
         (pMsdpFsMsdpSARedistributionEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
