/******************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *$Id: msdpque.c,v 1.2 2012/03/06 11:26:05 siva Exp $
 * Description : This file contains the processing of 
 *               messages queued
 *****************************************************************************/
#include "msdpinc.h"

/****************************************************************************
*                                                                           *
* Function     : MsdpQueProcessMsgs                                         *
*                                                                           *
* Description  : Deques and Process the msgs                                *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
MsdpQueProcessMsgs ()
{
    tMsdpQueMsg        *pMsdpQueMsg = NULL;
    tMsdpTcpMsg        *pMsdpTcpMsg = NULL;
    tMsdpMrpSaAdvt     *pMsdpMrpSaAdvt = NULL;
    tMsdpSAReqGrp      *pMsdpSaReqGrp = NULL;
    INT4                i4SockFd = MSDP_INVALID_SOCK_ID;
    UINT4               u4IfIndex = 0;
    UINT4               u4AdminStatus = 0;
    UINT4               u4OperStatus = 0;
    UINT1               u1AddrType = 0;

    MSDP_TRC_FUNC ((MSDP_QUE_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpQueProcessMsgs"));

    while (OsixQueRecv (gMsdpGlobals.msdpQueId,
                        (UINT1 *) &pMsdpQueMsg, OSIX_DEF_MSG_LEN,
                        0) == OSIX_SUCCESS)
    {
        if (pMsdpQueMsg == NULL)
        {

            MSDP_TRC_FUNC ((MSDP_QUE_TRC | MSDP_FN_EXIT,
                            "\nEXIT: MsdpQueProcessMsgs"));

            return;
        }
        if ((gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv4AdminStat != MSDP_ENABLED) &&
            (gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv6AdminStat != MSDP_ENABLED))
        {
            MemReleaseMemBlock (MSDP_QUE_MEMPOOL_ID, (UINT1 *) pMsdpQueMsg);
            continue;
        }
        switch (pMsdpQueMsg->eMsgType)
        {

            case MSDP_ACTIVE_CONN_EVT:

                MSDP_TRC_FUNC ((MSDP_QUE_TRC, "\nMsdpQueProcessMsgs: "
                                "MSDP_ACTIVE_CONN_EVT"));

                i4SockFd = pMsdpQueMsg->unMsdpMsgIfParam.MsdpTcpMsg.i4SockFd;
                MsdpSockHandleNewActiveConnEvt (i4SockFd);
                break;

            case MSDP_SOCK_READ_WRITE_EVT:

                MSDP_TRC_FUNC ((MSDP_QUE_TRC, "\nMsdpQueProcessMsgs: "
                                "MSDP_SOCK_READ_WRITE_EVT"));

                pMsdpTcpMsg = &(pMsdpQueMsg->unMsdpMsgIfParam.MsdpTcpMsg);
                MsdpSockHandleSockReadWriteEvt (pMsdpTcpMsg);
                break;

            case MSDP_PIM_UPDATE_SA_EVT:

                MSDP_TRC_FUNC ((MSDP_QUE_TRC, "\nMsdpQueProcessMsgs: "
                                "MSDP_PIM_UPDATE_SA_EVT"));

                pMsdpMrpSaAdvt = &pMsdpQueMsg->unMsdpMsgIfParam.MsdpMrpSaInfo;
                MsdpPimIfHandleSAInfo (pMsdpMrpSaAdvt);
                break;

            case MSDP_PIM_REQUEST_SA_EVT:

                MSDP_TRC_FUNC ((MSDP_QUE_TRC, "\nMsdpQueProcessMsgs: "
                                "MSDP_PIM_REQUEST_SA_EVT"));

                pMsdpSaReqGrp = &pMsdpQueMsg->unMsdpMsgIfParam.MsdpSAReqGrpAddr;
                MsdpPimIfHandleSAReq (pMsdpSaReqGrp);
                break;
            case MSDP_IF_CHG_EVT:
                u4IfIndex = pMsdpQueMsg->unMsdpMsgIfParam.
                    MsdpIfStatus.u4IfIndex;
                u4AdminStatus = pMsdpQueMsg->unMsdpMsgIfParam.
                    MsdpIfStatus.u1IfAdminStatus;
                u4OperStatus = pMsdpQueMsg->unMsdpMsgIfParam.
                    MsdpIfStatus.u1IfOperStatus;
                u1AddrType = pMsdpQueMsg->unMsdpMsgIfParam.
                    MsdpIfStatus.u1AddrType;
                MsdpMainHandleIfChange (u1AddrType, u4IfIndex,
                                        u4AdminStatus, u4OperStatus);
            default:
                break;
        }

        MemReleaseMemBlock (MSDP_QUE_MEMPOOL_ID, (UINT1 *) pMsdpQueMsg);
    }

    MSDP_TRC_FUNC ((MSDP_QUE_TRC | MSDP_FN_EXIT,
                    "\nEXIT: Exit MsdpQueProcessMsgs"));

}

/****************************************************************************
 * FUNCTION NAME : MsdpQueAllocQueMsg
 *
 * DESCRIPTION   : This function allocates memory for the queue and returns a 
 *                 pointer to the allocated block
 *
 * INPUTS        : None
 *
 * OUTPUTS       : None
 *
 * RETURNS       : pAllocQueMsg- Pointer to the memory block allocated
 *
 ***************************************************************************/
tMsdpQueMsg        *
MsdpQueAllocQueMsg (VOID)
{
    tMsdpQueMsg        *pAllocQueMsg = NULL;

    MSDP_TRC_FUNC ((MSDP_QUE_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpQueAllocQueMsg"));

    pAllocQueMsg = (tMsdpQueMsg *) MemAllocMemBlk (MSDP_QUE_MEMPOOL_ID);

    MSDP_TRC_FUNC ((MSDP_QUE_TRC | MSDP_FN_EXIT, "\nEXIT: MsdpQueAllocQueMsg"));

    return pAllocQueMsg;
}

/****************************************************************************
 * FUNCTION NAME : MsdpQuePostQueueMsg
 *
 * DESCRIPTION   : this function posts the given information in MSDP Queue
 *
 * INPUTS        : pMsdpQueMsg- Pointer to queue
 *
 * OUTPUTS       : None
 *
 * RETURNS       : OSIX_SUCCESS if OsixQueSend succeeds
 *                 OSIX_FAILURE otherwise
 *
 ***************************************************************************/
INT4
MsdpQuePostQueueMsg (tMsdpQueMsg * pMsdpQueMsg)
{

    MSDP_TRC_FUNC ((MSDP_QUE_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpQuePostQueueMsg"));

    if (OsixQueSend (gMsdpGlobals.msdpQueId, (UINT1 *) &pMsdpQueMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (MSDP_QUE_MEMPOOL_ID, (UINT1 *) pMsdpQueMsg);

        MSDP_TRC_FUNC ((MSDP_QUE_TRC, "\nMsdpQuePostQueueMsg: "
                        "OsixQueSend returns failure"));

        return OSIX_FAILURE;
    }

    MSDP_TRC_FUNC ((MSDP_QUE_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpQuePostQueueMsg"));

    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  msdpque.c                      */
/*-----------------------------------------------------------------------*/
