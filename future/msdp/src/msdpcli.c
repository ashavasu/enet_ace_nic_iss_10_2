/**********************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved
*
* $Id: msdpcli.c,v 1.14 2014/03/14 12:56:29 siva Exp $
*
*Description:This file holds the functions for the CLI commands of the 
*             MSDP module
**********************************************************/
#ifndef __MSDPCLI_C__
#define __MSDPCLI_C__

#include "msdpinc.h"
#include "msdpcli.h"
#ifdef MSDP_TEST_WANTED
extern VOID         MsdpExecuteUt (UINT4 u4FileNumber, UINT4 u4TestNumber);
extern VOID         MsdpExecuteUtAll (VOID);
extern VOID         MsdpExecuteUtFile (UINT4 u4File);
extern VOID         MsdpExecuteUtInMyOrder (VOID);
#endif
/****************************************************************************
 * Function    :  cli_process_Msdp_cmd
 * Description :  This function is exported to CLI module to handle the
                MSDP cli commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
cli_process_Msdp_Show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[MSDP_CLI_MAX_ARGS];
    UINT1              *pu1SrcAddr = NULL;
    UINT1              *pu1GrpAddr = NULL;
    INT1                i1argno = 1;
    INT4                i4RetStatus = CLI_FAILURE;
    INT4                i4MsdpAddrType = 0;
    INT4                i4Ipv4AdminStat = 0;
    INT4                i4Ipv6AdminStat = 0;
    UINT4               u4CmdType = 0;
    INT4                i4Inst = 0;

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: cli_process_Msdp_Show_cmd"));

    UNUSED_PARAM (u4CmdType);

    CliRegisterLock (CliHandle, MsdpMainTaskLock, MsdpMainTaskUnLock);
    MSDP_LOCK;
    nmhGetFsMsdpIPv4AdminStat (&i4Ipv4AdminStat);
    nmhGetFsMsdpIPv6AdminStat (&i4Ipv6AdminStat);

    if ((i4Ipv4AdminStat != MSDP_ENABLED) && (i4Ipv6AdminStat != MSDP_ENABLED))
    {
        CliPrintf (CliHandle, "MSDP is not enabled\r\n");

        MSDP_UNLOCK;

        CliUnRegisterLock (CliHandle);

        MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                        "\nEXIT: cli_process_Msdp_Show_cmd"));
        return CLI_SUCCESS;
    }

    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);
    UNUSED_PARAM (i4Inst);

    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == MSDP_CLI_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);
    switch (u4Command)
    {
        case CLI_MSDP_SHOW_SUMMARY:
        case CLI_MSDPV6_SHOW_SUMMARY:

            i4MsdpAddrType = (u4Command == CLI_MSDP_SHOW_SUMMARY) ?
                IPVX_ADDR_FMLY_IPV4 : IPVX_ADDR_FMLY_IPV6;
            i4RetStatus = MsdpCliShowSummary (CliHandle, i4MsdpAddrType);
            break;
        case CLI_MSDP_SHOW_CACHE:
        case CLI_MSDPV6_SHOW_CACHE:

            pu1SrcAddr = NULL;
            pu1GrpAddr = NULL;

            if (u4Command == CLI_MSDP_SHOW_CACHE)
            {
                i4MsdpAddrType = IPVX_ADDR_FMLY_IPV4;
                if (args[1] == NULL)
                {
                    i4RetStatus = MsdpCliShowCache (CliHandle, i4MsdpAddrType,
                                                    NULL, NULL);
                    break;
                }

                if (MSDP_START_OF_MCAST_GRP > OSIX_NTOHL (*(UINT4 *) args[1]))
                {
                    pu1SrcAddr = (UINT1 *) args[1];
                }
                else
                {
                    pu1GrpAddr = (UINT1 *) args[1];
                }
            }
            else
            {
                i4MsdpAddrType = IPVX_ADDR_FMLY_IPV6;

                if ((args[1] != NULL) &&
                    (0 == Msd6PortIsAddrMulti ((UINT1 *) args[1])))
                {
                    pu1SrcAddr = (UINT1 *) args[1];
                }
                else
                {
                    pu1GrpAddr = (UINT1 *) args[1];
                }

            }
            i4RetStatus = MsdpCliShowCache (CliHandle, i4MsdpAddrType,
                                            pu1SrcAddr, pu1GrpAddr);
            break;

        case CLI_MSDP_SHOW_COUNT:
        case CLI_MSDPV6_SHOW_COUNT:

            i4MsdpAddrType = (u4Command == CLI_MSDP_SHOW_COUNT) ?
                IPVX_ADDR_FMLY_IPV4 : IPVX_ADDR_FMLY_IPV6;
            i4RetStatus = MsdpCliShowSACount (CliHandle, i4MsdpAddrType);
            break;
        case CLI_MSDP_SHOW_PEER:
        case CLI_MSDPV6_SHOW_PEER:

            i4MsdpAddrType = (u4Command == CLI_MSDP_SHOW_PEER) ?
                IPVX_ADDR_FMLY_IPV4 : IPVX_ADDR_FMLY_IPV6;
            i4RetStatus = MsdpCliShowPeer
                (CliHandle, i4MsdpAddrType, (UINT1 *) args[1]);
            break;
        case CLI_MSDP_SHOW_RPFPEER:
        case CLI_MSDPV6_SHOW_RPFPEER:

            i4MsdpAddrType = (u4Command == CLI_MSDP_SHOW_RPFPEER) ?
                IPVX_ADDR_FMLY_IPV4 : IPVX_ADDR_FMLY_IPV6;
            i4RetStatus = MsdpCliShowRpfPeer
                (CliHandle, i4MsdpAddrType, (UINT1 *) args[1]);
            break;
        case CLI_MSDP_SHOW_MESH:
        case CLI_MSDPV6_SHOW_MESH:

            i4MsdpAddrType = (u4Command == CLI_MSDP_SHOW_MESH) ?
                IPVX_ADDR_FMLY_IPV4 : IPVX_ADDR_FMLY_IPV6;
            i4RetStatus = MsdpCliShowMeshGrp (CliHandle, i4MsdpAddrType,
                                              (UINT1 *) args[1]);
            break;

        case CLI_MSDP_CLEAR_PEER:
        case CLI_MSDPV6_CLEAR_PEER:

            i4MsdpAddrType = (u4Command == CLI_MSDP_CLEAR_PEER) ?
                IPVX_ADDR_FMLY_IPV4 : IPVX_ADDR_FMLY_IPV6;
            i4RetStatus = MsdpCliClearPeer (CliHandle, i4MsdpAddrType,
                                            (UINT1 *) args[1]);
            break;

        case CLI_MSDP_CLEAR_CACHE:
        case CLI_MSDPV6_CLEAR_CACHE:

            i4MsdpAddrType = (u4Command == CLI_MSDP_CLEAR_CACHE) ?
                IPVX_ADDR_FMLY_IPV4 : IPVX_ADDR_FMLY_IPV6;

            i4RetStatus = MsdpCliClearCache (i4MsdpAddrType, (UINT1 *) args[1]);
            break;

#ifdef MSDP_TEST_WANTED
        case CLI_MSDP_UT_TEST:
            if (args[1] == NULL)
            {
                /* order along which the test cases were written */
                if ((args[4] != NULL) && (args[3] == NULL))
                {
                    MsdpExecuteUtInMyOrder ();
                }
                /* specified case on specified file */
                if (args[3] != NULL)
                {
                    MsdpExecuteUt (*(UINT4 *) (args[2]), *(UINT4 *) (args[3]));
                }
                /* all cases on specified file */
                else
                {
                    MsdpExecuteUtFile (*(UINT4 *) (args[2]));
                }
            }
            else
            {
                /* all cases in all the files */
                MsdpExecuteUtAll ();
            }

            break;
#endif
        default:
            break;
    }

    CliUnRegisterLock (CliHandle);

    MSDP_UNLOCK;

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                    "\nEXIT: cli_process_Msdp_Show_cmd"));

    return i4RetStatus;

}

/******************************************************************************
 * Function Name : MsdpCliConvertTimetoString
 * Description   : This routine will display the time string in 00:00:00:00
 *                 format.
 * Input(s)      :  u4Time               - Time
 * Output(s)     :  pi1Time              - Display Time String
 * Return(s)     : True /False
*******************************************************************************/
PRIVATE VOID
MsdpCliConvertTimetoString (UINT4 u4Time, INT1 *pi1Time)
{
    UINT4               u4Hrs = 0;
    UINT4               u4Mins = 0;
    UINT4               u4Secs = 0;

    MEMSET (pi1Time, 0, MSDP_NUMBER_TEN - 1);

    if (u4Time == 0)
    {

        SPRINTF ((CHR1 *) pi1Time, "00:00:00");
        return;
    }

    u4Time = u4Time / 100;
    u4Secs = u4Time % 60;
    u4Time = u4Time / 60;
    u4Mins = u4Time % 60;
    u4Time = u4Time / 60;
    u4Hrs = u4Time % 24;

    SNPRINTF ((CHR1 *) pi1Time, MSDP_NUMBER_TEN - 1,
              "%02d:%02d:%02d", u4Hrs, u4Mins, u4Secs);
    return;
}

/****************************************************************************
 * Function    :  MsdpCliGetOriginatorId
 * Description :  This function obtains the originator ID

 * Input       :  i4AddrType- Address type

 * Output      :  pOriginatorId- Originator ID

 * Returns     :  OSIX_SUCCESS if originator ID could be determined
 *                OSIX_FAILURE otherwise
****************************************************************************/
INT4
MsdpCliGetOriginatorId (INT4 i4AddrType, CHR1 * pOriginatorId)
{
    tSNMP_OCTET_STRING_TYPE RpAddr;
    tUtlIn6Addr         InAddr;
    UINT4               u4Addr = 0;
    UINT1               au1RpAddr[MSDP_MAX_BUFFER];
    CHR1               *pString = NULL;

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpCliGetOriginatorId"));

    MEMSET (&RpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&InAddr, 0, sizeof (tUtlIn6Addr));
    MEMSET (au1RpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (pOriginatorId, 0, IPVX_MAX_INET_ADDR_LEN);

    RpAddr.pu1_OctetList = au1RpAddr;
    nmhGetFsMsdpRPAddress (i4AddrType, &RpAddr);
    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        u4Addr = OSIX_HTONL (*(UINT4 *) (VOID *) RpAddr.pu1_OctetList);
        CLI_CONVERT_IPADDR_TO_STR (pString, u4Addr);
    }
    else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (&InAddr.u1addr, RpAddr.pu1_OctetList, IPVX_IPV6_ADDR_LEN);
        pString = INET_NTOA6 (InAddr);
    }
    else
    {
        return OSIX_FAILURE;
    }

    MEMCPY (pOriginatorId, pString, STRLEN (pString));

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpCliGetOriginatorId"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  MsdpCliGetPeerStatistics
 * Description :  This function gets the peer statistics for an address type

 * Input       :  i4MsdpAddrType- Address type

 * Output      :  pi4ConfiguredPeers- Configured peers
 *                pi4EstablishedPeers- Established peers
 *                pi4ShutdownPeers- Shutdown peers

 * Returns     :  OSIX_SUCCESS always
****************************************************************************/
INT4
MsdpCliGetPeerStatistics (INT4 i4MsdpAddrType,
                          INT4 *pi4ConfiguredPeers,
                          INT4 *pi4EstablishedPeers, INT4 *pi4ShutdownPeers)
{
    typedef struct
    {
        tSNMP_OCTET_STRING_TYPE PeerAddr;
        UINT1               au1PeerAddr[IPVX_MAX_INET_ADDR_LEN];
        INT4                i4AddrType;
    } tPeer;
    tPeer               Current;
    tPeer               Next;
    INT4                i4RetVal = SNMP_SUCCESS;

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpCliGetPeerStatistics"));

    MEMSET (&Current, 0, sizeof (tPeer));
    MEMSET (&Next, 0, sizeof (tPeer));

    Current.PeerAddr.pu1_OctetList = Current.au1PeerAddr;
    Next.PeerAddr.pu1_OctetList = Next.au1PeerAddr;
    Current.i4AddrType = i4MsdpAddrType;
    i4RetVal = nmhGetNextIndexFsMsdpPeerTable
        (Current.i4AddrType, &Next.i4AddrType,
         &Current.PeerAddr, &Next.PeerAddr);
    while ((i4RetVal != SNMP_FAILURE) && (Next.i4AddrType == i4MsdpAddrType))
    {
        *pi4ConfiguredPeers = *pi4ConfiguredPeers + 1;
        nmhGetFsMsdpPeerState (Next.i4AddrType, &Next.PeerAddr, &i4RetVal);
        if (i4RetVal == MSDP_PEER_ESTABLISHED)
        {
            *pi4EstablishedPeers = *pi4EstablishedPeers + 1;
        }
        if (i4RetVal == MSDP_PEER_DISABLED)
        {
            *pi4ShutdownPeers = *pi4ShutdownPeers + 1;
        }

        MEMCPY (&Current, &Next, sizeof (tPeer));
        i4RetVal = nmhGetNextIndexFsMsdpPeerTable
            (Current.i4AddrType, &Next.i4AddrType,
             &Current.PeerAddr, &Next.PeerAddr);
    }

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpCliGetPeerStatistics"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  MsdpCliConvertIpvxToStr
 * Description :  This function converts addresses from IPvx format to string

 * Input       :  u1IpvxAddr- Address in IPvx format
 *                i4AddrType- address type

 * Output      :  pIpStr- IP address string

 * Returns     :  OSIX_SUCCESS always
****************************************************************************/
INT4
MsdpCliConvertIpvxToStr (UINT1 *u1IpvxAddr, INT4 i4AddrType, UINT1 *pIpStr)
{
    tUtlIn6Addr         InAddr;
    CHR1               *pString = NULL;
    UINT4               u4Addr = 0;

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpCliConvertIpvxToStr"));

    MEMSET (&InAddr, 0, sizeof (tUtlIn6Addr));

    if (IPVX_ADDR_FMLY_IPV4 == i4AddrType)
    {
        u4Addr = OSIX_HTONL (*(UINT4 *) (VOID *) u1IpvxAddr);
        CLI_CONVERT_IPADDR_TO_STR (pString, u4Addr);

    }
    else if (IPVX_ADDR_FMLY_IPV6 == i4AddrType)
    {
        MEMCPY (&InAddr.u1addr, u1IpvxAddr, IPVX_IPV6_ADDR_LEN);
        pString = INET_NTOA6 (InAddr);
    }
    else
    {
        return OSIX_FAILURE;
    }

    MEMCPY (pIpStr, pString, STRLEN (pString));

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpCliConvertIpvxToStr"));

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : MsdpCliShowSACount()
 *
 * Description: This function shows the SA count
 *
 * Input      : CliHandle- The CLI handle
 *              i4MsdpAddrType- Address type
 *
 * Output     : NONE
 *
 * Returns    : OSIX_SUCCESS always
 *****************************************************************************/
INT4
MsdpCliShowSACount (tCliHandle CliHandle, INT4 i4MsdpAddrType)
{
    tSNMP_OCTET_STRING_TYPE SrcAddr;
    tSNMP_OCTET_STRING_TYPE GrpAddr;
    tSNMP_OCTET_STRING_TYPE RpAddr;
    tSNMP_OCTET_STRING_TYPE NextSrcAddr;
    tSNMP_OCTET_STRING_TYPE NextGrpAddr;
    tSNMP_OCTET_STRING_TYPE NextRpAddr;
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1RpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextSrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextGrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextRpAddr[IPVX_MAX_INET_ADDR_LEN];
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4MsdpAddrLen = 0;
    INT4                i4GrpCount = 0;
    INT4                i4SrcCount = 0;
    INT4                i4RetVal = OSIX_FAILURE;

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpCliShowSACount"));

    i4MsdpAddrLen = IPVX_IPV6_ADDR_LEN;

    MEMSET (&SrcAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&GrpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextSrcAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextGrpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextRpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1SrcAddr, 0, i4MsdpAddrLen);
    MEMSET (au1GrpAddr, 0, i4MsdpAddrLen);
    MEMSET (au1RpAddr, 0, i4MsdpAddrLen);
    MEMSET (au1NextSrcAddr, 0, i4MsdpAddrLen);
    MEMSET (au1NextGrpAddr, 0, i4MsdpAddrLen);
    MEMSET (au1NextRpAddr, 0, i4MsdpAddrLen);

    SrcAddr.pu1_OctetList = au1SrcAddr;
    GrpAddr.pu1_OctetList = au1GrpAddr;
    RpAddr.pu1_OctetList = au1RpAddr;
    NextSrcAddr.pu1_OctetList = au1NextSrcAddr;
    NextGrpAddr.pu1_OctetList = au1NextGrpAddr;
    NextRpAddr.pu1_OctetList = au1NextRpAddr;
    i4AddrType = i4MsdpAddrType;
    SrcAddr.i4_Length = i4MsdpAddrLen;
    GrpAddr.i4_Length = i4MsdpAddrLen;
    RpAddr.i4_Length = i4MsdpAddrLen;
    i4RetVal = nmhGetNextIndexFsMsdpSACacheTable
        (i4AddrType, &i4NextAddrType, &GrpAddr, &NextGrpAddr,
         &SrcAddr, &NextSrcAddr, &RpAddr, &NextRpAddr);
    if ((i4RetVal == SNMP_FAILURE) || (i4NextAddrType != i4MsdpAddrType))
    {
        CliPrintf (CliHandle, "No Entries Present in Cache\r\n");

        MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                        "\nEXIT: MsdpCliShowSACount"));

        return OSIX_SUCCESS;
    }
    else
    {
        CliPrintf (CliHandle, "(S,G)Count/Group Count :");
    }

    while ((i4RetVal != SNMP_FAILURE) && (i4NextAddrType == i4MsdpAddrType))
    {
        if (0 != MEMCMP (SrcAddr.pu1_OctetList, NextSrcAddr.pu1_OctetList,
                         i4MsdpAddrLen))
        {
            i4SrcCount++;
        }
        if (0 != MEMCMP (GrpAddr.pu1_OctetList, NextGrpAddr.pu1_OctetList,
                         i4MsdpAddrLen))
        {
            i4GrpCount++;
        }

        i4RetVal = nmhGetNextIndexFsMsdpSACacheTable
            (i4AddrType, &i4NextAddrType, &GrpAddr, &NextGrpAddr,
             &SrcAddr, &NextSrcAddr, &RpAddr, &NextRpAddr);

        i4AddrType = i4NextAddrType;
        MEMCPY (GrpAddr.pu1_OctetList, NextGrpAddr.pu1_OctetList,
                i4MsdpAddrLen);
        MEMCPY (SrcAddr.pu1_OctetList, NextSrcAddr.pu1_OctetList,
                i4MsdpAddrLen);
        MEMCPY (RpAddr.pu1_OctetList, NextRpAddr.pu1_OctetList, i4MsdpAddrLen);
        GrpAddr.i4_Length = NextGrpAddr.i4_Length;
        SrcAddr.i4_Length = NextSrcAddr.i4_Length;
        RpAddr.i4_Length = NextRpAddr.i4_Length;
    }

    CliPrintf (CliHandle, "%d/%d \r\n", i4SrcCount, i4GrpCount);

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT, "\nEXIT: MsdpCliShowSACount"));

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : MsdpCliShowPeer
 *
 * Description: This function gives peer information
 *
 * Input      : CliHandle- CLI handle
 *              i4MsdpAddrType- Address type
 *
 * Output     : pu1Peer- MSDP peer entry
 *
 * Returns    : OSIX_SUCCESS always
 *****************************************************************************/
INT4
MsdpCliShowPeer (tCliHandle CliHandle, INT4 i4MsdpAddrType, UINT1 *pu1Peer)
{
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE NextPeerAddr;
    tSNMP_OCTET_STRING_TYPE PeerLocalAddr;
    tSNMP_OCTET_STRING_TYPE MeshName;
    tSNMP_OCTET_STRING_TYPE NextMeshName;
    tSNMP_OCTET_STRING_TYPE RouteMap;
    UINT1               au1PeerState[][MSDP_BUFFER_LEN] =
        { "           ", "Inactive   ", "Listening ",
        "Connected  ", "Established", "Disabled   "
    };
    UINT1               au1PwdState[][MSDP_BUFFER_LEN] =
        { "Password not set", "Password enabled" };
    UINT1               au1Str[MSDP_MAX_BUFFER];
    UINT1               au1NextStr[MSDP_MAX_BUFFER];
    UINT1               au1PeerAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1PeerLocalAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1RpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextAddr[IPVX_MAX_INET_ADDR_LEN];
    INT1                ai1TimeStr[MSDP_NUMBER_TEN];
    UINT4               u4RetVal = 0;
    UINT4               u4SysTime = 0;
    UINT4               u4InSAs = 0;
    UINT4               u4OutSAs = 0;
    UINT4               u4InSARequests = 0;
    UINT4               u4OutSARequests = 0;
    UINT4               u4OutSAResponse = 0;
    UINT4               u4InSAResponse = 0;
    UINT4               u4RpfFailure = 0;
    UINT4               u4DataTtlError = 0;
    UINT4               u4InKeepAliveCnt = 0;
    UINT4               u4OutKeepAliveCnt = 0;
    UINT4               u4ConnectionAttempts = 0;
    INT4                i4MsdpAddrLen = 0;
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4Exist = OSIX_FALSE;
    INT4                i4RetVal = 0;
    INT4                i4PeerStatus = 0;

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_ENTRY, "\nENTRY: MsdpCliShowPeer"));

    i4MsdpAddrLen = IPVX_IPV6_ADDR_LEN;

    MEMSET (&PeerAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextPeerAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PeerLocalAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&MeshName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextMeshName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RouteMap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1PeerAddr, 0, i4MsdpAddrLen);
    MEMSET (&au1PeerLocalAddr, 0, i4MsdpAddrLen);
    MEMSET (&au1NextAddr, 0, i4MsdpAddrLen);
    MEMSET (&au1RpAddr, 0, i4MsdpAddrLen);
    MEMSET (au1Str, 0, MSDP_MAX_BUFFER);
    MEMSET (au1NextStr, '\0', MSDP_MAX_BUFFER);
    MEMSET (ai1TimeStr, 0, MSDP_NUMBER_TEN);

    i4AddrType = i4MsdpAddrType;
    PeerAddr.pu1_OctetList = au1PeerAddr;
    PeerLocalAddr.pu1_OctetList = au1PeerLocalAddr;
    MEMCPY (PeerAddr.pu1_OctetList, pu1Peer, i4MsdpAddrLen);
    PeerAddr.i4_Length = i4MsdpAddrLen;
    i4RetVal = nmhGetFsMsdpPeerStatus (i4AddrType, &PeerAddr, &i4PeerStatus);

    if (i4RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\tPeer does not exist\r\n");

        MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                        "\nEXIT: MsdpCliShowPeer"));

        return OSIX_SUCCESS;
    }

    MsdpCliConvertIpvxToStr (pu1Peer, i4MsdpAddrType, au1Str);
    CliPrintf (CliHandle, "\r\nMSDP Peer %s\r\n", au1Str);

    MEMSET (au1Str, 0, MSDP_MAX_BUFFER);
    nmhGetFsMsdpPeerLocalAddress (i4AddrType, &PeerAddr, &PeerLocalAddr);
    MsdpCliConvertIpvxToStr (PeerLocalAddr.pu1_OctetList,
                             i4MsdpAddrType, au1Str);
    CliPrintf (CliHandle, "\r\nLocal Address         : %s\r\n", au1Str);

    nmhGetFsMsdpPeerState (i4AddrType, &PeerAddr, &i4RetVal);
    CliPrintf (CliHandle, "Connection Status     : %s\r\n",
               au1PeerState[i4RetVal]);

    nmhGetFsMsdpPeerUpTime (i4AddrType, &PeerAddr, &u4RetVal);
    OsixGetSysTime (&u4SysTime);
    MEMSET (ai1TimeStr, 0, MSDP_NUMBER_TEN);
    MsdpCliConvertTimetoString (u4SysTime - u4RetVal, ai1TimeStr);
    CliPrintf (CliHandle, "Uptime(Downtime)      : %s\r\n", ai1TimeStr);

    nmhGetFsMsdpPeerMD5AuthPwdStat (i4AddrType, &PeerAddr, &i4RetVal);
    CliPrintf (CliHandle, "Password              : %s\r\n",
               au1PwdState[i4RetVal]);

    nmhGetFsMsdpPeerKeepAliveConfigured (i4AddrType, &PeerAddr, &i4RetVal);
    CliPrintf (CliHandle, "Keepalive Interval    : %d\r\n", i4RetVal);

    nmhGetFsMsdpPeerHoldTimeConfigured (i4AddrType, &PeerAddr, &i4RetVal);
    CliPrintf (CliHandle, "Keepalive Timeout     : %d\r\n", i4RetVal);

    nmhGetFsMsdpPeerConnectRetryInterval (i4AddrType, &PeerAddr, &i4RetVal);
    CliPrintf (CliHandle, "Reconnection Interval : %d\r\n", i4RetVal);

    nmhGetFsMsdpPeerDataTtl (i4AddrType, &PeerAddr, &i4RetVal);
    CliPrintf (CliHandle, "TTL threshold         : %d\r\n", i4RetVal);

    NextPeerAddr.pu1_OctetList = au1NextAddr;
    MEMCPY (PeerAddr.pu1_OctetList, pu1Peer, i4MsdpAddrLen);
    MEMCPY (NextPeerAddr.pu1_OctetList, pu1Peer, i4MsdpAddrLen);

    MEMSET (au1Str, '\0', MSDP_MAX_BUFFER);
    MeshName.pu1_OctetList = au1Str;
    NextMeshName.pu1_OctetList = au1NextStr;
    i4RetVal = nmhGetFirstIndexFsMsdpMeshGroupTable
        (&MeshName, &i4AddrType, &PeerAddr);

    if (i4RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nMember Of Mesh Group  : None\r\n");
        i4Exist = OSIX_TRUE;
    }
    else
    {
        CliPrintf (CliHandle, "\r\nMember Of Mesh Group(s) ");
    }
    while (i4RetVal != SNMP_FAILURE)
    {
        if ((i4AddrType == i4MsdpAddrType) &&
            (0 == MEMCMP (PeerAddr.pu1_OctetList, NextPeerAddr.pu1_OctetList,
                          i4MsdpAddrLen)))
        {
            CliPrintf (CliHandle, "\r\n                 %s",
                       MeshName.pu1_OctetList);
            i4Exist = OSIX_TRUE;
        }
        i4RetVal = nmhGetNextIndexFsMsdpMeshGroupTable
            (&MeshName, &NextMeshName, i4AddrType,
             &i4NextAddrType, &PeerAddr, &NextPeerAddr);

        MEMCPY (MeshName.pu1_OctetList, NextMeshName.pu1_OctetList,
                NextMeshName.i4_Length);
        MEMCPY (PeerAddr.pu1_OctetList, NextPeerAddr.pu1_OctetList,
                NextPeerAddr.i4_Length);
        PeerAddr.i4_Length = NextPeerAddr.i4_Length;
        MeshName.i4_Length = NextMeshName.i4_Length;
        i4AddrType = i4NextAddrType;

    }
    if (i4Exist == OSIX_FALSE)
    {
        CliPrintf (CliHandle, "None\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\n");
    }
    CliPrintf (CliHandle, "Statistics(IN/OUT)\r\n\r\n");

    MEMCPY (PeerAddr.pu1_OctetList, pu1Peer, i4MsdpAddrLen);
    PeerAddr.i4_Length = i4MsdpAddrLen;
    i4AddrType = i4MsdpAddrType;
    nmhGetFsMsdpPeerInMessageTime (i4AddrType, &PeerAddr, &u4RetVal);
    CliPrintf (CliHandle, "Last Message Received : %d\r\n", u4RetVal);

    nmhGetFsMsdpPeerInSAs (i4AddrType, &PeerAddr, &u4InSAs);
    nmhGetFsMsdpPeerInSARequests (i4AddrType, &PeerAddr, &u4InSARequests);
    nmhGetFsMsdpPeerOutSAs (i4AddrType, &PeerAddr, &u4OutSAs);
    nmhGetFsMsdpPeerOutSARequests (i4AddrType, &PeerAddr, &u4OutSARequests);
    nmhGetFsMsdpPeerInSAResponses (i4AddrType, &PeerAddr, &u4InSAResponse);
    nmhGetFsMsdpPeerOutSAResponses (i4AddrType, &PeerAddr, &u4OutSAResponse);
    nmhGetFsMsdpPeerRPFFailures (i4AddrType, &PeerAddr, &u4RpfFailure);
    nmhGetFsMsdpPeerInKeepAliveCount (i4AddrType, &PeerAddr, &u4InKeepAliveCnt);
    nmhGetFsMsdpPeerOutKeepAliveCount (i4AddrType, &PeerAddr,
                                       &u4OutKeepAliveCnt);
    nmhGetFsMsdpPeerDataTtlErrorCount (i4AddrType, &PeerAddr, &u4DataTtlError);

    CliPrintf (CliHandle, "SAs :%d/%d, SA Requests : %d/%d"
               " SA Responses :%d/%d\r\n",
               u4InSAs, u4OutSAs, u4InSARequests, u4OutSARequests,
               u4InSAResponse, u4OutSAResponse);
    CliPrintf (CliHandle, "KeepAliveCount        : In: %d, Out: %d\r\n",
               u4InKeepAliveCnt, u4OutKeepAliveCnt);
    nmhGetFsMsdpPeerConnectionAttempts (i4AddrType, &PeerAddr,
                                        &u4ConnectionAttempts);
    CliPrintf (CliHandle, "Peer Conn. Attempts   : %d\r\n",
               u4ConnectionAttempts);

    CliPrintf (CliHandle, "RPF Failure Count     : %d\r\n", u4RpfFailure);
    CliPrintf (CliHandle, "Data TTL Error Count  : %d\r\n", u4DataTtlError);
    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT, "\nEXIT: MsdpCliShowPeer"));

    return OSIX_SUCCESS;
}

INT4
MsdpCliClearCache (INT4 i4MsdpAddrType, UINT1 *pGrpAddr)
{
    tMsdpFsMsdpSACacheEntry Cache;
    tMsdpFsMsdpSACacheEntry *pCache = NULL;
    UINT1               u1AddrLen = 0;

    MEMSET (&Cache, 0, sizeof (tMsdpFsMsdpSACacheEntry));

    u1AddrLen = IPVX_IPV6_ADDR_LEN;

    if (pGrpAddr != NULL)
    {
        MEMCPY (Cache.MibObject.au1FsMsdpSACacheGroupAddr, pGrpAddr, u1AddrLen);
        Cache.MibObject.i4FsMsdpSACacheGroupAddrLen = u1AddrLen;
    }
    Cache.MibObject.i4FsMsdpSACacheAddrType = i4MsdpAddrType;

    while (OSIX_TRUE == OSIX_TRUE)
    {
        pCache = MsdpGetNextFsMsdpSACacheTable (&Cache);
        if (pCache == NULL)
        {
            break;
        }

        if ((pGrpAddr == NULL) ||
            (0 == MEMCMP (pCache->MibObject.au1FsMsdpSACacheGroupAddr,
                          pGrpAddr, u1AddrLen)))
        {
            MsdpTmrStopTmr (&pCache->CacheSaStateTimer);
            MsdpUtilRemoveCacheEntry (pCache);
        }
        MEMCPY (&Cache, pCache, sizeof (Cache));
    }
    return OSIX_SUCCESS;
}

INT4
MsdpCliClearPeer (tCliHandle CliHandle, INT4 i4MsdpAddrType, UINT1 *pPeerAddr)
{
    tMsdpFsMsdpPeerEntry Peer;
    tMsdpFsMsdpPeerEntry *pPeer = NULL;
    UINT1               u1AddrLen = 0;

    MEMSET (&Peer, 0, sizeof (tMsdpFsMsdpPeerEntry));

    u1AddrLen = IPVX_IPV6_ADDR_LEN;

    Peer.MibObject.i4FsMsdpPeerAddrType = i4MsdpAddrType;
    if (pPeerAddr != NULL)
    {
        MEMCPY (Peer.MibObject.au1FsMsdpPeerRemoteAddress, pPeerAddr,
                u1AddrLen);
        Peer.MibObject.i4FsMsdpPeerRemoteAddressLen = u1AddrLen;
        pPeer = MsdpGetFsMsdpPeerTable (&Peer);
        if (pPeer == NULL)
        {
            CliPrintf (CliHandle, "Invalid Peer");
            return OSIX_SUCCESS;
        }
        pPeer->MibObject.u4FsMsdpPeerRPFFailures = 0;
        pPeer->MibObject.u4FsMsdpPeerInSAs = 0;
        pPeer->MibObject.u4FsMsdpPeerOutSAs = 0;
        pPeer->MibObject.u4FsMsdpPeerInSARequests = 0;
        pPeer->MibObject.u4FsMsdpPeerOutSARequests = 0;
        pPeer->MibObject.u4FsMsdpPeerInControlMessages = 0;
        pPeer->MibObject.u4FsMsdpPeerOutControlMessages = 0;
        pPeer->MibObject.u4FsMsdpPeerInDataPackets = 0;
        pPeer->MibObject.u4FsMsdpPeerOutDataPackets = 0;
        pPeer->MibObject.u4FsMsdpPeerFsmEstablishedTransitions = 0;
        pPeer->MibObject.u4FsMsdpPeerInMessageTime = 0;
        pPeer->MibObject.u4FsMsdpPeerConnectionAttempts = 0;
        pPeer->MibObject.u4FsMsdpPeerDiscontinuityTime = 0;
        pPeer->MibObject.u4FsMsdpPeerInSAResponses = 0;
        pPeer->MibObject.u4FsMsdpPeerOutSAResponses = 0;
        pPeer->MibObject.u4FsMsdpPeerInKeepAliveCount = 0;
        pPeer->MibObject.u4FsMsdpPeerOutKeepAliveCount = 0;
        pPeer->MibObject.u4FsMsdpPeerDataTtlErrorCount = 0;
        pPeer->MibObject.i4FsMsdpPeerMD5FailCount = 0;
        pPeer->MibObject.i4FsMsdpPeerMD5SuccessCount = 0;
        return OSIX_SUCCESS;
    }

    while (OSIX_TRUE == OSIX_TRUE)
    {
        pPeer = MsdpGetNextFsMsdpPeerTable (&Peer);
        if (pPeer == NULL)
        {
            break;
        }
        pPeer->MibObject.u4FsMsdpPeerRPFFailures = 0;
        pPeer->MibObject.u4FsMsdpPeerInSAs = 0;
        pPeer->MibObject.u4FsMsdpPeerOutSAs = 0;
        pPeer->MibObject.u4FsMsdpPeerInSARequests = 0;
        pPeer->MibObject.u4FsMsdpPeerOutSARequests = 0;
        pPeer->MibObject.u4FsMsdpPeerInControlMessages = 0;
        pPeer->MibObject.u4FsMsdpPeerOutControlMessages = 0;
        pPeer->MibObject.u4FsMsdpPeerInDataPackets = 0;
        pPeer->MibObject.u4FsMsdpPeerOutDataPackets = 0;
        pPeer->MibObject.u4FsMsdpPeerFsmEstablishedTransitions = 0;
        pPeer->MibObject.u4FsMsdpPeerInMessageTime = 0;
        pPeer->MibObject.u4FsMsdpPeerConnectionAttempts = 0;
        pPeer->MibObject.u4FsMsdpPeerDiscontinuityTime = 0;
        pPeer->MibObject.u4FsMsdpPeerInSAResponses = 0;
        pPeer->MibObject.u4FsMsdpPeerOutSAResponses = 0;
        pPeer->MibObject.u4FsMsdpPeerInKeepAliveCount = 0;
        pPeer->MibObject.u4FsMsdpPeerOutKeepAliveCount = 0;
        pPeer->MibObject.u4FsMsdpPeerDataTtlErrorCount = 0;
        pPeer->MibObject.i4FsMsdpPeerMD5FailCount = 0;
        pPeer->MibObject.i4FsMsdpPeerMD5SuccessCount = 0;
        MEMCPY (&Peer, pPeer, sizeof (Peer));
    }
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : MsdpCliShowRpfPeer
 *
 * Description: This function displays the RPF peer for an Originator 
 *              which is configured on other MSDP router
 *
 * Input      : CliHandle- CLI handle
 *              i4MsdpAddrType- Address type
 *              Originator address
 *
 * Output     : 
 *
 * Returns    : OSIX_SUCCESS always
 *****************************************************************************/

INT4
MsdpCliShowRpfPeer (tCliHandle CliHandle, INT4 i4MsdpAddrType,
                    UINT1 *pu1Originator)
{
    tMsdpFsMsdpPeerEntry Peer;
    tIPvXAddr           OrigNextHop;
    tIPvXAddr           Originator;
    UINT1               au1Str[MSDP_MAX_BUFFER];
    UINT1               au1RpfPeer[MSDP_MAX_BUFFER];
    INT4                i4Metrics = 0;
    INT4                i4Port = MSDP_INVALID;
    UINT4               u4Preference = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_IN | MSDP_FN_ENTRY, "\nENTRY: MsdpInChkRpfPeer"));

    MEMSET (&OrigNextHop, 0, sizeof (tIPvXAddr));
    MEMSET (&Originator, 0, sizeof (tIPvXAddr));
    MEMSET (&Peer, 0, sizeof (tMsdpFsMsdpPeerEntry));

    IPVX_ADDR_INIT (Originator, i4MsdpAddrType, pu1Originator);
    MsdpUtilGetUcastRtInfo (&Originator, &OrigNextHop, &i4Metrics,
                            &u4Preference, &i4Port);

    if (i4Port == MSDP_INVALID)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_IN,
                        "\nMsdpInChkRpfPeer: MsdpUtilGetUcastRtInfo gives Error"));

        CliPrintf (CliHandle, "No RPF peer for Originator : %s\r\n",
                   pu1Originator);
        return OSIX_SUCCESS;
    }

    if (IPVX_ADDR_COMPARE (Originator, OrigNextHop) == 0)
    {
        /* originator and the nexthop to reach originator are same */
        Peer.MibObject.i4FsMsdpPeerAddrType = i4MsdpAddrType;
        Peer.MibObject.i4FsMsdpPeerRemoteAddressLen = Originator.u1AddrLen;
        MEMCPY (Peer.MibObject.au1FsMsdpPeerRemoteAddress,
                pu1Originator, Originator.u1AddrLen);

        MsdpCliConvertIpvxToStr (pu1Originator, i4MsdpAddrType, au1Str);
        if (NULL == MsdpGetFsMsdpPeerTable (&Peer))
        {
            CliPrintf (CliHandle, "No RPF peer for Originator : %s\r\n",
                       au1Str);
        }
        else
        {
            CliPrintf (CliHandle, "RPF Peer is %s\r\n", au1Str);
            CliPrintf (CliHandle, "RPF peer is the Originator RP\r\n");
        }
        return OSIX_SUCCESS;
    }
    else
    {
        Peer.MibObject.i4FsMsdpPeerAddrType = i4MsdpAddrType;
        MEMCPY (Peer.MibObject.au1FsMsdpPeerRemoteAddress,
                OrigNextHop.au1Addr, OrigNextHop.u1AddrLen);

        MsdpCliConvertIpvxToStr (pu1Originator, i4MsdpAddrType, au1Str);
        MsdpCliConvertIpvxToStr (OrigNextHop.au1Addr, i4MsdpAddrType,
                                 au1RpfPeer);

        if (NULL != MsdpGetFsMsdpPeerTable (&Peer))
        {
            CliPrintf (CliHandle, "RPF peer for Originator %s: %s\r\n",
                       au1Str, au1RpfPeer);
        }

    }
    return OSIX_SUCCESS;

}

/******************************************************************************
 * Function   : MsdpCliShowMeshGrp
 *
 * Description: This function obtains the mesh groups
 *
 * Input      : CliHandle- CLI handle
 *              i4MsdpAddrType- Address type
 *
 * Output     : pMeshStr- Mesh string
 *
 * Returns    : OSIX_SUCCESS always
 *****************************************************************************/
INT4
MsdpCliShowMeshGrp (tCliHandle CliHandle, INT4 i4MsdpAddrType, UINT1 *pMeshStr)
{

    tMesh              *pCurrent = NULL;
    tMesh              *pNext = NULL;
    UINT1               au1Str[MSDP_MAX_BUFFER];
    INT4                i4RetVal = 0;
    INT4                i4MeshStrLen = STRLEN (pMeshStr);

    pCurrent = (tMesh *) MemAllocMemBlk (MSDP_MESH_MEMPOOL_ID);
    if (pCurrent == NULL)
    {
        MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                        "\nEXIT: Msdp Memory Allocation Fails!!"));
        return OSIX_FAILURE;
    }
    pNext = (tMesh *) MemAllocMemBlk (MSDP_MESH_MEMPOOL_ID);
    if (pNext == NULL)
    {
        MemReleaseMemBlock (MSDP_MESH_MEMPOOL_ID, (UINT1 *) pCurrent);
        MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                        "\nEXIT: Msdp Memory Allocation Fails!!"));
        return OSIX_FAILURE;
    }

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpCliShowMeshGrp"));

    MEMSET (pCurrent, 0, sizeof (tMesh));
    MEMSET (pNext, 0, sizeof (tMesh));
    MEMSET (au1Str, '\0', MSDP_MAX_BUFFER);

    pCurrent->MeshStr.pu1_OctetList = pCurrent->au1MeshStr;
    pCurrent->PeerAddr.pu1_OctetList = pCurrent->au1PeerAddr;
    pCurrent->MeshStr.i4_Length = i4MeshStrLen;
    pCurrent->i4AddrType = i4MsdpAddrType;
    pNext->MeshStr.pu1_OctetList = pNext->au1MeshStr;
    pNext->PeerAddr.pu1_OctetList = pNext->au1PeerAddr;

    MEMCPY (pCurrent->MeshStr.pu1_OctetList, pMeshStr, i4MeshStrLen);

    i4RetVal = nmhGetNextIndexFsMsdpMeshGroupTable
        (&(pCurrent->MeshStr), &(pNext->MeshStr), pCurrent->i4AddrType,
         &(pNext->i4AddrType), &(pCurrent->PeerAddr), &(pNext->PeerAddr));
    CliPrintf (CliHandle, "\r\nMSDP Mesh Group Membership Details\r\n");
    if ((i4RetVal != SNMP_FAILURE) &&
        (i4MsdpAddrType == pNext->i4AddrType) &&
        (0 == MEMCMP (pNext->MeshStr.pu1_OctetList, pMeshStr, i4MeshStrLen)))
    {
        CliPrintf (CliHandle, "Mesh Group Name : %s\r\n",
                   pNext->MeshStr.pu1_OctetList);
    }
    else
    {
        CliPrintf (CliHandle, "Mesh Group Name : %s\r\nNo.Of Members : 0 ",
                   pNext->MeshStr.pu1_OctetList);
    }
    while
        ((i4RetVal != SNMP_FAILURE) &&
         (i4MsdpAddrType == pNext->i4AddrType) &&
         (0 == MEMCMP (pNext->MeshStr.pu1_OctetList, pMeshStr, i4MeshStrLen)))
    {
        MEMCPY (pCurrent, pNext, sizeof (tMesh));
        MsdpCliConvertIpvxToStr
            (pNext->PeerAddr.pu1_OctetList, i4MsdpAddrType, au1Str);

        CliPrintf (CliHandle, "Peer : %s\r\n", au1Str);

        i4RetVal = nmhGetNextIndexFsMsdpMeshGroupTable
            (&(pCurrent->MeshStr), &(pNext->MeshStr), pCurrent->i4AddrType,
             &(pNext->i4AddrType), &(pCurrent->PeerAddr), &(pNext->PeerAddr));
    }
    CliPrintf (CliHandle, "\r\n");

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT, "\nEXIT: MsdpCliShowMeshGrp"));

    MemReleaseMemBlock (MSDP_MESH_MEMPOOL_ID, (UINT1 *) pNext);
    MemReleaseMemBlock (MSDP_MESH_MEMPOOL_ID, (UINT1 *) pCurrent);
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : MsdpCliCountSACache
 *
 * Description: This function counts the SA cache entries 
 *
 * Input      : i4MsdpAddrType- Address type
 *
 * Output     : pu4Count- Count
 *
 * Returns    : OSIX_SUCCESS always
 *****************************************************************************/
PRIVATE INT4
MsdpCliCountSACache (INT4 i4MsdpAddrType, UINT4 *pu4Count)
{
    typedef struct
    {
        tSNMP_OCTET_STRING_TYPE GrpAddr;
        tSNMP_OCTET_STRING_TYPE SrcAddr;
        tSNMP_OCTET_STRING_TYPE RpAddr;
        UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
        UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
        UINT1               au1RpAddr[IPVX_MAX_INET_ADDR_LEN];
        INT4                i4AddrType;
    } tCache;
    tCache              Current, Next;
    INT4                i4RetVal = 0;

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpCliCountSACache"));

    MEMSET (&Current, 0, sizeof (Current));
    MEMSET (&Next, 0, sizeof (Next));

    Current.GrpAddr.pu1_OctetList = Current.au1GrpAddr;
    Current.SrcAddr.pu1_OctetList = Current.au1SrcAddr;
    Current.RpAddr.pu1_OctetList = Current.au1RpAddr;
    Next.GrpAddr.pu1_OctetList = Next.au1GrpAddr;
    Next.SrcAddr.pu1_OctetList = Next.au1SrcAddr;
    Next.RpAddr.pu1_OctetList = Next.au1RpAddr;

    Current.i4AddrType = i4MsdpAddrType;
    i4RetVal = nmhGetNextIndexFsMsdpSACacheTable
        (Current.i4AddrType, &Next.i4AddrType,
         &Current.GrpAddr, &Next.GrpAddr,
         &Current.SrcAddr, &Next.SrcAddr, &Current.RpAddr, &Next.RpAddr);
    while ((i4RetVal != SNMP_FAILURE) && (Next.i4AddrType == i4MsdpAddrType))
    {
        *pu4Count = *pu4Count + 1;
        MEMCPY (&Current, &Next, sizeof (Next));
        i4RetVal = nmhGetNextIndexFsMsdpSACacheTable
            (Current.i4AddrType, &Next.i4AddrType,
             &Current.GrpAddr, &Next.GrpAddr,
             &Current.SrcAddr, &Next.SrcAddr, &Current.RpAddr, &Next.RpAddr);
    }

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpCliCountSACache"));

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : MsdpCliShowCache
 *
 * Description: This function displays the Cache based on the Group/Sorce IP. 
 *
 * Input      : CliHandle- CLI handle
 *              i4MsdpAddrType- Address type
 *
 * Output     : pu1SrcAddr- Source address
 *              pu1GrpAddr- Group address
 *
 * Returns    : OSIX_SUCCESS always
 *****************************************************************************/
INT4
MsdpCliShowCache (tCliHandle CliHandle, INT4 i4MsdpAddrType,
                  UINT1 *pu1SrcAddr, UINT1 *pu1GrpAddr)
{
    typedef struct
    {
        tSNMP_OCTET_STRING_TYPE SrcAddr;
        tSNMP_OCTET_STRING_TYPE GrpAddr;
        tSNMP_OCTET_STRING_TYPE RpAddr;
        UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
        UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
        UINT1               au1RpAddr[IPVX_MAX_INET_ADDR_LEN];
        INT4                i4AddrType;
    } tCache;
    tCache              Current;
    tCache              Next;
    UINT1               au1Str[MSDP_MAX_BUFFER];
    INT1                ai1TimeStr[MSDP_NUMBER_TEN];
    UINT4               u4Count = 0;
    UINT4               u4SysTime = 0;
    UINT4               u4RetVal = 0;
    INT4                i4MsdpAddrLen = 0;
    INT4                i4RetVal = 0;

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_ENTRY, "\nENTRY: MsdpCliShowCache"));

    i4MsdpAddrLen = IPVX_IPV6_ADDR_LEN;

    MEMSET (&Current, 0, sizeof (tCache));
    MEMSET (&Next, 0, sizeof (tCache));
    MEMSET (au1Str, 0, MSDP_MAX_BUFFER);
    MEMSET (ai1TimeStr, 0, MSDP_NUMBER_TEN);

    Current.GrpAddr.pu1_OctetList = Current.au1GrpAddr;
    Current.SrcAddr.pu1_OctetList = Current.au1SrcAddr;
    Current.RpAddr.pu1_OctetList = Current.au1RpAddr;
    Next.GrpAddr.pu1_OctetList = Next.au1GrpAddr;
    Next.SrcAddr.pu1_OctetList = Next.au1SrcAddr;
    Next.RpAddr.pu1_OctetList = Next.au1RpAddr;

    MsdpCliCountSACache (i4MsdpAddrType, &u4Count);
    CliPrintf (CliHandle, " \r\n MSDP SA ROUTE CACHE - %d entries\r\n",
               u4Count);

    if (u4Count == 0)
    {

        MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                        "\nEXIT: MsdpCliShowCache"));

        return OSIX_SUCCESS;
    }

    Current.i4AddrType = i4MsdpAddrType;
    Current.GrpAddr.i4_Length = i4MsdpAddrLen;
    Current.SrcAddr.i4_Length = i4MsdpAddrLen;

    if (pu1GrpAddr != NULL)
    {
        MEMCPY (Current.GrpAddr.pu1_OctetList, pu1GrpAddr, i4MsdpAddrLen);
    }
    if (pu1SrcAddr != NULL)
    {
        MEMCPY (Current.SrcAddr.pu1_OctetList, pu1SrcAddr, i4MsdpAddrLen);
    }
    i4RetVal = nmhGetNextIndexFsMsdpSACacheTable
        (Current.i4AddrType, &Next.i4AddrType, &Current.GrpAddr,
         &Next.GrpAddr, &Current.SrcAddr, &Next.SrcAddr,
         &Current.RpAddr, &Next.RpAddr);

    CliPrintf (CliHandle, "Source\tGroup\tRP\tUptime\r\n");

    while ((i4RetVal != SNMP_FAILURE) && (i4MsdpAddrType == Next.i4AddrType))
    {

        MEMCPY (&Current, &Next, sizeof (tCache));

        if (((NULL == pu1SrcAddr) && (NULL == pu1GrpAddr)) ||
            ((NULL != pu1SrcAddr) &&
             (0 == MEMCMP
              (Next.SrcAddr.pu1_OctetList, pu1SrcAddr, i4MsdpAddrLen))) ||
            ((NULL != pu1GrpAddr) &&
             (0 == MEMCMP
              (Next.GrpAddr.pu1_OctetList, pu1GrpAddr, i4MsdpAddrLen))))
        {
            MEMSET (au1Str, 0, MSDP_MAX_BUFFER);
            MsdpCliConvertIpvxToStr
                (Current.SrcAddr.pu1_OctetList, i4MsdpAddrType, au1Str);
            CliPrintf (CliHandle, " %s\t", au1Str);

            MEMSET (au1Str, 0, MSDP_MAX_BUFFER);
            MsdpCliConvertIpvxToStr
                (Current.GrpAddr.pu1_OctetList, i4MsdpAddrType, au1Str);
            CliPrintf (CliHandle, " %s\t", au1Str);

            MEMSET (au1Str, 0, MSDP_MAX_BUFFER);
            MsdpCliConvertIpvxToStr
                (Current.RpAddr.pu1_OctetList, i4MsdpAddrType, au1Str);
            CliPrintf (CliHandle, " %s\t", au1Str);

            nmhGetFsMsdpSACacheUpTime
                (Current.i4AddrType, &Current.GrpAddr,
                 &Current.SrcAddr, &Current.RpAddr, &u4RetVal);
            OsixGetSysTime (&u4SysTime);
            MEMSET (ai1TimeStr, 0, MSDP_NUMBER_TEN);
            MsdpCliConvertTimetoString (u4SysTime - u4RetVal, ai1TimeStr);
            CliPrintf (CliHandle, " %s\r\n", ai1TimeStr);

        }

        i4RetVal = nmhGetNextIndexFsMsdpSACacheTable
            (Current.i4AddrType, &Next.i4AddrType,
             &Current.GrpAddr, &Next.GrpAddr,
             &Current.SrcAddr, &Next.SrcAddr, &Current.RpAddr, &Next.RpAddr);
    }
    CliPrintf (CliHandle, "\r\n");

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT, "\nEXIT: MsdpCliShowCache"));

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : MsdpCliShowSummary
 *
 * Description: This function displays the statistics related to the peers 
 *              and the SA messages received. 
 *
 * Input      : CliHandle- CLI handle
 *              i4MsdpAddrType- Address type
 *
 * Output     : NONE
 * Returns    : OSIX_SUCCESS always
 *****************************************************************************/
INT4
MsdpCliShowSummary (tCliHandle CliHandle, INT4 i4MsdpAddrType)
{
    typedef struct tSPeer
    {
        tSNMP_OCTET_STRING_TYPE PeerAddr;
        UINT1               au1PeerAddr[IPVX_MAX_INET_ADDR_LEN];
        INT4                i4AddrType;
    } tPeer;
    tPeer               Current;
    tPeer               Next;
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1PeerState[][MSDP_BUFFER_LEN] =
        { "           ", "Inactive   ", "Listen     ",
        "Connecting ", "Established", "Disabled   "
    };
    UINT1               au1Str[MSDP_MAX_BUFFER];
    CHR1                aOriginatorId[MSDP_MAX_BUFFER];
    CHR1                apFilter[][MSDP_BUFFER_LEN] =
        { "deny-all", "accept-all" };
    INT1                ai1TimeStr[MSDP_NUMBER_TEN];
    UINT4               u4RetVal = 0;
    UINT4               u4SysTime = 0;
    INT4                i4RetVal = 0;
    INT4                i4AdminStat = 0;
    INT4                i4MsdpAddrLen = 0;
    INT4                i4ShutdownPeers = 0;
    INT4                i4EstablishedPeers = 0;
    INT4                i4ConfiguredPeers = 0;

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpCliShowSummary"));

    i4MsdpAddrLen = IPVX_IPV6_ADDR_LEN;

    MEMSET (&Current, 0, sizeof (tPeer));
    MEMSET (&RouteMapName, 0, sizeof (RouteMapName));
    MEMSET (&Next, 0, sizeof (tPeer));
    MEMSET (au1Str, 0, MSDP_MAX_BUFFER);
    MEMSET (aOriginatorId, '\0', MSDP_MAX_BUFFER);
    MEMSET (ai1TimeStr, 0, MSDP_NUMBER_TEN);

    CliPrintf (CliHandle, " \r\n\t MSDP Peer Status Summary \r\n");

    if (i4MsdpAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        nmhGetFsMsdpIPv4AdminStat (&i4AdminStat);
    }
    else
    {
        nmhGetFsMsdpIPv6AdminStat (&i4AdminStat);
    }
    CliPrintf (CliHandle, "\r\nMSDP Admin Status\t    : %s \r\n",
               (i4AdminStat == MSDP_ENABLED) ? "Enabled" : "Disabled");

    MsdpCliGetOriginatorId (i4MsdpAddrType, aOriginatorId);
    CliPrintf (CliHandle, "Originator ID\t\t    : %s \r\n", aOriginatorId);

    MsdpCliGetPeerStatistics (i4MsdpAddrType, &i4ConfiguredPeers,
                              &i4EstablishedPeers, &i4ShutdownPeers);
    CliPrintf (CliHandle, "Number Of Configured Peers  : %d \r\n",
               i4ConfiguredPeers);
    CliPrintf (CliHandle, "Number Of Established Peers : %d \r\n",
               i4EstablishedPeers);
    CliPrintf (CliHandle, "Number Of Shutdown Peers    : %d \r\n",
               i4ShutdownPeers);

    nmhGetFsMsdpMaxPeerSessions (&i4RetVal);
    CliPrintf (CliHandle, "Max Peer Sessions Allowed   : %d \r\n", i4RetVal);
    nmhGetFsMsdpPeerFilter (&i4RetVal);
    CliPrintf (CliHandle, "Peer Filter Status          : %s \r\n",
               apFilter[i4RetVal]);

    MEMSET (au1Str, '\0', MSDP_MAX_BUFFER);
    RouteMapName.pu1_OctetList = au1Str;
    if (SNMP_FAILURE != nmhGetFsMsdpPeerFilterRouteMap (i4MsdpAddrType,
                                                        &RouteMapName))
    {
        CliPrintf (CliHandle, "Peer In RouteMap            : %s\r\n",
                   RouteMapName.pu1_OctetList);
    }
    else
    {
        CliPrintf (CliHandle, "Peer In RouteMap            : None\r\n");
    }

    if (SNMP_FAILURE != nmhGetFsMsdpSARedistributionStatus
        (i4MsdpAddrType, &i4RetVal))
    {
        MEMSET (au1Str, '\0', MSDP_MAX_BUFFER);
        CliPrintf (CliHandle, "SA Redistribution           : Enabled\r\n");
        nmhGetFsMsdpSARedistributionRouteMapStat (i4MsdpAddrType, &i4RetVal);
        if (MSDP_ENABLED == i4RetVal)
        {
            nmhGetFsMsdpSARedistributionRouteMap (i4MsdpAddrType,
                                                  &RouteMapName);
            CliPrintf (CliHandle, "SA Out RouteMap             : %s\r\n",
                       RouteMapName.pu1_OctetList);
        }
        else
        {
            CliPrintf (CliHandle, "SA Out RouteMap             : None\r\n");

        }
    }
    else
    {
        CliPrintf (CliHandle, "SA Redistribution           : Disabled\r\n");
    }

    nmhGetFsMsdpCacheLifetime (&u4RetVal);
    CliPrintf (CliHandle, "Cache Life Time             : %d \r\n", u4RetVal);

    nmhGetFsMsdpMappingComponentId (&i4RetVal);
    CliPrintf (CliHandle, "MSDP Mapping Component      : %d \r\n", i4RetVal);

    nmhGetFsMsdpListenerPort (&i4RetVal);
    CliPrintf (CliHandle, "MSDP Listener Port          : %d \r\n", i4RetVal);

    CliPrintf (CliHandle,
               "\r\nPeer \t\tConnection \tUptime\\\t  Last Msg    (S,G)'s\r\n");
    CliPrintf (CliHandle,
               "Address \tState   \tDowntime  Received    Received\r\n");

    Current.i4AddrType = i4MsdpAddrType;
    Current.PeerAddr.pu1_OctetList = Current.au1PeerAddr;
    Next.PeerAddr.pu1_OctetList = Next.au1PeerAddr;
    Current.PeerAddr.i4_Length = i4MsdpAddrLen;

    i4RetVal = nmhGetNextIndexFsMsdpPeerTable
        (Current.i4AddrType, &Next.i4AddrType,
         &Current.PeerAddr, &Next.PeerAddr);

    while ((i4RetVal != SNMP_FAILURE) && (Next.i4AddrType == i4MsdpAddrType))
    {
        MEMCPY (&Current, &Next, sizeof (tPeer));
        MEMSET (au1Str, '\0', MSDP_MAX_BUFFER);
        MsdpCliConvertIpvxToStr
            (Current.PeerAddr.pu1_OctetList, i4MsdpAddrType, au1Str);
        CliPrintf (CliHandle, "%s\t", au1Str);

        nmhGetFsMsdpPeerState (Current.i4AddrType, &Current.PeerAddr,
                               &i4RetVal);
        CliPrintf (CliHandle, "%s\t", au1PeerState[i4RetVal]);

        nmhGetFsMsdpPeerUpTime (Current.i4AddrType, &Current.PeerAddr,
                                &u4RetVal);
        OsixGetSysTime (&u4SysTime);
        MEMSET (ai1TimeStr, 0, MSDP_NUMBER_TEN);
        MsdpCliConvertTimetoString (u4SysTime - u4RetVal, ai1TimeStr);
        CliPrintf (CliHandle, "%s  ", ai1TimeStr);

        nmhGetFsMsdpPeerInMessageTime (Current.i4AddrType, &Current.PeerAddr,
                                       &u4RetVal);
        OsixGetSysTime (&u4SysTime);
        MEMSET (ai1TimeStr, 0, MSDP_NUMBER_TEN);
        MsdpCliConvertTimetoString (u4SysTime - u4RetVal, ai1TimeStr);
        CliPrintf (CliHandle, "%s\t", ai1TimeStr);

        nmhGetFsMsdpPeerInSAs (Current.i4AddrType, &Current.PeerAddr,
                               &u4RetVal);
        CliPrintf (CliHandle, "%d", u4RetVal);

        i4RetVal = nmhGetNextIndexFsMsdpPeerTable
            (Current.i4AddrType, &Next.i4AddrType,
             &Current.PeerAddr, &Next.PeerAddr);

        CliPrintf (CliHandle, " \r\n");
    }
    CliPrintf (CliHandle, " \r\n");

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT, "\nEXIT: MsdpCliShowSummary"));

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MsdpShowRunningConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current configuration   */
/*                        of MSDP Module                                     */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4Module - Specified module (msdp/all), for         */
/*                        configuration                                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS /  CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
MsdpShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module, INT4 i4AddrType)
{

    UNUSED_PARAM (u4Module);

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpShowRunningConfig"));

    {
        CliPrintf (CliHandle, "!\r\n");

        /* MsdpShowRunningConfigScalar will return failure
         * if the msdp module is disabled */
        if (CLI_FAILURE == MsdpShowRunningConfigScalar (CliHandle, i4AddrType))
        {
            CliPrintf (CliHandle, "!\r\n");
            MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                            "\nEXIT: MsdpShowRunningConfig"));
            return CLI_SUCCESS;
        }

        MsdpShowRunningConfigPeerTable (CliHandle, i4AddrType);

        MsdpShowRunningConfigPeerFilter (CliHandle, i4AddrType);

        MsdpShowRunningConfigMeshGrpTbl (CliHandle, i4AddrType);

        MsdpShowRunningConfigSAFilter (CliHandle, i4AddrType);

        MsdpShowRunningConfigRPTable (CliHandle, i4AddrType);

        CliPrintf (CliHandle, "!\r\n");

    }

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpShowRunningConfig"));

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  MsdpShowRunningConfigScalar
 *
 * Description :  This function displays the current configuration of the MSDP
 *                scalars

 * Input       :  CliHandle- CLI handle
 *                i4MsdpAddrType- Address type

 * Output      :  None 

 * Returns     :  CLI_SUCCESS always
****************************************************************************/
INT4
MsdpShowRunningConfigScalar (tCliHandle CliHandle, INT4 i4MsdpAddrType)
{
    INT4                i4RetVal = 0;
    UINT4               u4RetVal = 0;

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpShowRunningConfigScalar"));

    if (i4MsdpAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        nmhGetFsMsdpIPv4AdminStat (&i4RetVal);
        if (i4RetVal != CLI_MSDP_DISABLE)
        {
            CliPrintf (CliHandle, " ip msdp enable \r\n");
        }
        else
        {
            return CLI_FAILURE;
        }
    }
    if (i4MsdpAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        nmhGetFsMsdpIPv6AdminStat (&i4RetVal);
        if (i4RetVal != CLI_MSDP_DISABLE)
        {
            CliPrintf (CliHandle, " ipv6 msdp enable \r\n");
        }
        else
        {
            return CLI_FAILURE;
        }
    }

    nmhGetFsMsdpCacheLifetime (&u4RetVal);
    if (u4RetVal != CLI_MSDP_DISABLE)
    {
        CliPrintf (CliHandle, " ip msdp cache-sa-state %d \r\n", u4RetVal);
    }
    nmhGetFsMsdpMaxPeerSessions (&i4RetVal);
    if (MSDP_MAX_PEER_SESSIONS != i4RetVal)
    {
        CliPrintf (CliHandle, " ip msdp max-peer-sessions  %d \r\n", i4RetVal);
    }

    nmhGetFsMsdpMappingComponentId (&i4RetVal);
    if (MSDP_DEF_COMPONENT != i4RetVal)
    {
        CliPrintf (CliHandle, " ip msdp mapping-component  %d \r\n", i4RetVal);
    }

    nmhGetFsMsdpListenerPort (&i4RetVal);
    if (MSDP_DEF_LISTENER_PORT != i4RetVal)
    {
        CliPrintf (CliHandle, " ip msdp listener-port  %d \r\n", i4RetVal);
    }

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpShowRunningConfigScalar"));

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  MsdpShowRunningConfigPeerFilter
 *
 * Description :  This function shows the current configuration of the peer 
 *                filter

 * Input       :  CliHandle- CLI handle
 *                i4MsdpAddrType- Address type

 * Output      :  None 

 * Returns     : CLI_SUCCESS always
****************************************************************************/
INT4
MsdpShowRunningConfigPeerFilter (tCliHandle CliHandle, INT4 i4MsdpAddrType)
{
    tSNMP_OCTET_STRING_TYPE RouteMap;
    UINT1               au1RouteMap[MSDP_MAX_ROUTEMAP_LEN];
    INT4                i4RetVal = 0;
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = 0;
    CHR1               *pFilter = NULL;

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpShowRunningConfigPeerFilter"));

    MEMSET (&RouteMap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1RouteMap, 0, MSDP_MAX_ROUTEMAP_LEN);
    nmhGetFsMsdpPeerFilter (&i4RetVal);

    if (i4RetVal == MSDP_ACCEPT_ALL)
    {
        pFilter = "accept-all";
    }
    else
    {
        pFilter = "deny-all";
    }
    if (SNMP_SUCCESS != nmhGetFirstIndexFsMsdpPeerFilterTable (&i4AddrType))
    {
        if (MSDP_ACCEPT_ALL == i4RetVal)
        {

            MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                            "\nEXIT: MsdpShowRunningConfigPeerFilter"));

            return CLI_SUCCESS;
        }
        if (i4MsdpAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            CliPrintf (CliHandle, " ip msdp peer-filter deny-all \r\n",
                       i4RetVal);
        }
        if (i4MsdpAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            CliPrintf (CliHandle, " ipv6 msdp peer-filter deny-all \r\n",
                       i4RetVal);
        }

        MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                        "\nEXIT: MsdpShowRunningConfigPeerFilter"));

        return CLI_SUCCESS;
    }
    while (1)
    {
        nmhGetFsMsdpPeerFilterStatus (i4AddrType, &i4RetVal);
        if ((ACTIVE == i4RetVal) && (i4MsdpAddrType == i4AddrType))
        {
            MEMSET (au1RouteMap, '\0', MSDP_MAX_ROUTEMAP_LEN);
            RouteMap.pu1_OctetList = au1RouteMap;
            nmhGetFsMsdpPeerFilterRouteMap (i4AddrType, &RouteMap);
            if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                CliPrintf (CliHandle,
                           " ip msdp peer-filter %s routemap %s \r\n",
                           pFilter, RouteMap.pu1_OctetList);
            }
            if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
            {
                CliPrintf (CliHandle,
                           " ipv6 msdp peer-filter %s routemap %s \r\n",
                           pFilter, RouteMap.pu1_OctetList);
            }
        }

        if (SNMP_SUCCESS != nmhGetNextIndexFsMsdpPeerFilterTable
            (i4AddrType, &i4NextAddrType))
        {
            break;
        }
        i4AddrType = i4NextAddrType;
    }

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpShowRunningConfigPeerFilter"));

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  MsdpShowRunningConfigRPTable
 *
 * Description :  This function shows the current configuration of the RP table

 * Input       :  CliHandle- CLI handle
 *                i4MsdpAddrType- Address type

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
MsdpShowRunningConfigRPTable (tCliHandle CliHandle, INT4 i4MsdpAddrType)
{
    tSNMP_OCTET_STRING_TYPE RpAddr;
    tUtlIn6Addr         InAddr;
    UINT1               au1RpAddr[IPVX_MAX_INET_ADDR_LEN];
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4RetVal = 0;
    UINT4               u4Addr = 0;
    CHR1               *pString = NULL;

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpShowRunningConfigRPTable"));

    MEMSET (&RpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&InAddr, 0, sizeof (tUtlIn6Addr));
    MEMSET (au1RpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    RpAddr.pu1_OctetList = au1RpAddr;

    if (SNMP_SUCCESS != nmhGetFirstIndexFsMsdpRPTable (&i4AddrType))
    {

        MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                        "\nEXIT: MsdpShowRunningConfigRPTable"));

        return CLI_SUCCESS;
    }
    while (1)
    {
        nmhGetFsMsdpRPStatus (i4AddrType, &i4RetVal);
        nmhGetFsMsdpRPAddress (i4AddrType, &RpAddr);
        if ((ACTIVE == i4RetVal) && (i4AddrType == i4MsdpAddrType))
        {

            if ((IPVX_ADDR_FMLY_IPV4 == i4AddrType) &&
                (OSIX_TRUE != MsdpUtilIsAllZeros (RpAddr.pu1_OctetList,
                                                  i4MsdpAddrType)))
            {
                u4Addr = OSIX_HTONL (*(UINT4 *) (VOID *) RpAddr.pu1_OctetList);
                CLI_CONVERT_IPADDR_TO_STR (pString, u4Addr);

                CliPrintf (CliHandle,
                           " ip msdp originator-id %s \r\n", pString);
            }
            if ((IPVX_ADDR_FMLY_IPV6 == i4AddrType) &&
                (OSIX_TRUE != MsdpUtilIsAllZeros (RpAddr.pu1_OctetList,
                                                  i4MsdpAddrType)))
            {
                MEMCPY (&InAddr.u1addr, RpAddr.pu1_OctetList,
                        IPVX_IPV6_ADDR_LEN);
                pString = INET_NTOA6 (InAddr);

                CliPrintf (CliHandle,
                           " ipv6 msdp originator-id %s \r\n", pString);
            }

        }
        if (SNMP_SUCCESS != nmhGetNextIndexFsMsdpRPTable
            (i4AddrType, &i4NextAddrType))
        {
            break;
        }

        i4AddrType = i4NextAddrType;
    }

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpShowRunningConfigRPTable"));

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  MsdpShowRunningConfigMeshGrpTbl
 *
 * Description :  This function shows the current configuration of the Mesh
 *                Group table

 * Input       :  CliHandle- CLI handle
 *                i4MsdpAddrType- Address type

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
MsdpShowRunningConfigMeshGrpTbl (tCliHandle CliHandle, INT4 i4MsdpAddrType)
{
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE NextPeerAddr;
    tSNMP_OCTET_STRING_TYPE GroupName;
    tSNMP_OCTET_STRING_TYPE NextGroupName;
    tUtlIn6Addr         InAddr;
    UINT1               au1PeerAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextPeerAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1GrpName[MSDP_MAX_MESH_LEN];
    UINT1               au1NextGrpName[MSDP_MAX_MESH_LEN];
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4RetVal = 0;
    UINT4               u4Addr = 0;
    CHR1               *pString = NULL;

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpShowRunningConfigMeshGrpTbl"));

    MEMSET (&PeerAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextPeerAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&GroupName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextGroupName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&InAddr, 0, sizeof (tUtlIn6Addr));
    MEMSET (au1PeerAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NextPeerAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1GrpName, '\0', MSDP_MAX_MESH_LEN);
    MEMSET (au1NextGrpName, '\0', MSDP_MAX_MESH_LEN);

    PeerAddr.pu1_OctetList = au1PeerAddr;
    NextPeerAddr.pu1_OctetList = au1NextPeerAddr;
    GroupName.pu1_OctetList = au1GrpName;
    NextGroupName.pu1_OctetList = au1NextGrpName;

    if (SNMP_SUCCESS != nmhGetFirstIndexFsMsdpMeshGroupTable
        (&GroupName, &i4AddrType, &PeerAddr))
    {

        MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                        "\nEXIT: MsdpShowRunningConfigMeshGrpTbl"));

        return CLI_SUCCESS;
    }
    while (1)
    {
        nmhGetFsMsdpMeshGroupStatus (&GroupName, i4AddrType,
                                     &PeerAddr, &i4RetVal);
        if ((ACTIVE == i4RetVal) && (i4AddrType == i4MsdpAddrType))
        {

            if (IPVX_ADDR_FMLY_IPV4 == i4AddrType)
            {
                u4Addr = OSIX_HTONL
                    (*(UINT4 *) (VOID *) PeerAddr.pu1_OctetList);
                CLI_CONVERT_IPADDR_TO_STR (pString, u4Addr);

                CliPrintf (CliHandle,
                           " ip msdp mesh-group %s %s \r\n",
                           GroupName.pu1_OctetList, pString);
            }
            if (IPVX_ADDR_FMLY_IPV6 == i4AddrType)
            {
                MEMCPY (&InAddr.u1addr, PeerAddr.pu1_OctetList,
                        IPVX_IPV6_ADDR_LEN);
                pString = INET_NTOA6 (InAddr);

                CliPrintf (CliHandle,
                           " ipv6 msdp mesh-group %s %s \r\n",
                           GroupName.pu1_OctetList, pString);
            }

        }
        if (SNMP_SUCCESS != nmhGetNextIndexFsMsdpMeshGroupTable
            (&GroupName, &NextGroupName,
             i4AddrType, &i4NextAddrType, &PeerAddr, &NextPeerAddr))
        {
            break;
        }

        MEMCPY (PeerAddr.pu1_OctetList, NextPeerAddr.pu1_OctetList,
                NextPeerAddr.i4_Length);
        MEMCPY (GroupName.pu1_OctetList, NextGroupName.pu1_OctetList,
                NextGroupName.i4_Length);
        PeerAddr.i4_Length = NextPeerAddr.i4_Length;
        MEMSET (au1NextPeerAddr, 0, IPVX_MAX_INET_ADDR_LEN);
        NextPeerAddr.i4_Length = 0;
        GroupName.i4_Length = NextGroupName.i4_Length;
        MEMSET (au1NextGrpName, '\0', MSDP_MAX_MESH_LEN);
        NextGroupName.i4_Length = 0;
        i4AddrType = i4NextAddrType;
        i4NextAddrType = 0;
    }

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpShowRunningConfigMeshGrpTbl"));

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  MsdpShowRunningConfigSAFilter
 *
 * Description :  This function shows the current configuration of the SA filter
 *
 * Input       :  CliHandle- The current CLI's handle
 *                i4MsdpAddrType - The request address type

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
MsdpShowRunningConfigSAFilter (tCliHandle CliHandle, INT4 i4MsdpAddrType)
{
    tSNMP_OCTET_STRING_TYPE RouteMap;
    INT4                i4RetVal = 0;
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = 0;
    UINT1               au1RouteMap[MSDP_MAX_ROUTEMAP_LEN];

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpShowRunningConfigSAFilter"));

    MEMSET (&RouteMap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1RouteMap, 0, MSDP_MAX_ROUTEMAP_LEN);
    if (SNMP_SUCCESS != nmhGetFirstIndexFsMsdpSARedistributionTable
        (&i4AddrType))
    {

        MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                        "\nEXIT: MsdpShowRunningConfigSAFilter"));

        return CLI_SUCCESS;
    }
    while (1)
    {
        if (i4AddrType == i4MsdpAddrType)
        {
            nmhGetFsMsdpSARedistributionStatus (i4AddrType, &i4RetVal);
            if (ACTIVE == i4RetVal)
            {
                MEMSET (au1RouteMap, '\0', MSDP_MAX_ROUTEMAP_LEN);
                RouteMap.pu1_OctetList = au1RouteMap;
                nmhGetFsMsdpSARedistributionRouteMap (i4AddrType, &RouteMap);
                nmhGetFsMsdpSARedistributionRouteMapStat
                    (i4AddrType, &i4RetVal);
                if (*(RouteMap.pu1_OctetList) == '\0')
                {
                    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
                    {
                        CliPrintf (CliHandle, " ip msdp redistribute \r\n");
                    }
                    if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
                    {
                        CliPrintf (CliHandle, " ipv6 msdp redistribute\r\n");
                    }
                }
                else
                {
                    if ((i4AddrType == IPVX_ADDR_FMLY_IPV4) &&
                        (i4RetVal == MSDP_DISABLED))
                    {
                        CliPrintf (CliHandle,
                                   " ip msdp redistribute routemap %s \r\n",
                                   RouteMap.pu1_OctetList);

                        CliPrintf (CliHandle,
                                   " no ip msdp redistribute routemap %s \r\n",
                                   RouteMap.pu1_OctetList);
                    }
                    if ((i4AddrType == IPVX_ADDR_FMLY_IPV4) &&
                        (i4RetVal == MSDP_ENABLED))
                    {
                        CliPrintf (CliHandle,
                                   " ip msdp redistribute routemap %s \r\n",
                                   RouteMap.pu1_OctetList);
                    }
                    if ((i4AddrType == IPVX_ADDR_FMLY_IPV6) &&
                        (i4RetVal == MSDP_DISABLED))
                    {
                        CliPrintf (CliHandle,
                                   " ipv6 msdp redistribute routemap %s \r\n",
                                   RouteMap.pu1_OctetList);

                        CliPrintf (CliHandle,
                                   " no ipv6 msdp redistribute routemap %s \r\n",
                                   RouteMap.pu1_OctetList);
                    }
                    if ((i4AddrType == IPVX_ADDR_FMLY_IPV6) &&
                        (i4RetVal == MSDP_ENABLED))
                    {
                        CliPrintf (CliHandle,
                                   " ipv6 msdp redistribute routemap %s \r\n",
                                   RouteMap.pu1_OctetList);
                    }

                }
            }
        }
        if (SNMP_SUCCESS != nmhGetNextIndexFsMsdpSARedistributionTable
            (i4AddrType, &i4NextAddrType))
        {
            break;
        }
        i4AddrType = i4NextAddrType;
    }

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpShowRunningConfigSAFilter"));

    return CLI_SUCCESS;

}

/****************************************************************************
 * Function    :  MsdpShowRunningConfigPeerTable
 * Description :  This function shows the current configuration of the peer
 *                table
 
 * Input       :  CliHandle- CLI handle
 *                i4MsdpAddrType- Address type
 *
 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
MsdpShowRunningConfigPeerTable (tCliHandle CliHandle, INT4 i4MsdpAddrType)
{
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE LocalAddr;
    tSNMP_OCTET_STRING_TYPE NextPeerAddr;
    tUtlIn6Addr         InAddr;
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4RetVal = 0;
    INT4                i4HoldTime = 0;
    UINT4               u4Addr = 0;
    UINT2               u2VlanId = 0;
    CHR1               *pRemoteAddr = NULL;
    UINT1               au1PeerAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1LocalAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextPeerAddr[IPVX_MAX_INET_ADDR_LEN];

    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpShowRunningConfigPeerTable"));

    MEMSET (&PeerAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&LocalAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextPeerAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1PeerAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1LocalAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1NextPeerAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    PeerAddr.pu1_OctetList = au1PeerAddr;
    LocalAddr.pu1_OctetList = au1LocalAddr;
    NextPeerAddr.pu1_OctetList = au1NextPeerAddr;

    if (SNMP_SUCCESS != nmhGetFirstIndexFsMsdpPeerTable
        (&i4AddrType, &PeerAddr))
    {

        MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                        "\nEXIT: MsdpShowRunningConfigPeerTable"));

        return CLI_SUCCESS;
    }

    while (1)
    {
        if (i4AddrType == i4MsdpAddrType)
        {
            nmhGetFsMsdpPeerStatus (i4AddrType, &PeerAddr, &i4RetVal);
            if ((i4RetVal == ACTIVE) || (i4RetVal == NOT_READY))
            {
                nmhGetFsMsdpPeerLocalAddress
                    (i4AddrType, &PeerAddr, &LocalAddr);

                if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    u4Addr = OSIX_HTONL
                        (*(UINT4 *) (VOID *) LocalAddr.pu1_OctetList);
                    MsdpPortGetVlanIdFromIpAddress (u4Addr, &u2VlanId);
                    u4Addr = OSIX_HTONL
                        (*(UINT4 *) (VOID *) PeerAddr.pu1_OctetList);
                    CLI_CONVERT_IPADDR_TO_STR (pRemoteAddr, u4Addr);

                    CliPrintf (CliHandle,
                               " ip msdp peer %s connect-source vlan %d \r\n",
                               pRemoteAddr, u2VlanId);

                }
                else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
                {
                    Msd6PortGetVlanIdFromIp6Address (&LocalAddr, &u2VlanId);
                    MEMCPY (&InAddr.u1addr, PeerAddr.pu1_OctetList,
                            IPVX_IPV6_ADDR_LEN);
                    pRemoteAddr = INET_NTOA6 (InAddr);
                    CliPrintf (CliHandle,
                               " ipv6 msdp peer %s connect-source vlan %d \r\n",
                               pRemoteAddr, u2VlanId);

                }
            }
            if (i4RetVal == NOT_READY)
            {
                if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    CliPrintf (CliHandle, " ip msdp shutdown %s \r\n",
                               pRemoteAddr);

                }
                else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
                {
                    CliPrintf (CliHandle, " ipv6 msdp shutdown %s \r\n",
                               pRemoteAddr);

                }

            }
            nmhGetFsMsdpPeerConnectRetryInterval
                (i4AddrType, &PeerAddr, &i4RetVal);
            if ((MSDP_CONNECT_RETRY != i4RetVal) &&
                (i4AddrType == IPVX_ADDR_FMLY_IPV4))
            {

                CliPrintf (CliHandle, "  ip msdp timer %s %d \r\n",
                           pRemoteAddr, i4RetVal);

            }
            else if ((MSDP_CONNECT_RETRY != i4RetVal) &&
                     (i4AddrType == IPVX_ADDR_FMLY_IPV6))
            {
                CliPrintf (CliHandle, "  ipv6 msdp timer %s %d \r\n",
                           pRemoteAddr, i4RetVal);
            }
            nmhGetFsMsdpPeerHoldTimeConfigured
                (i4AddrType, &PeerAddr, &i4RetVal);
            nmhGetFsMsdpPeerKeepAliveConfigured
                (i4AddrType, &PeerAddr, &i4HoldTime);

            if (((i4RetVal != MSDP_KAT_VAL) || (i4HoldTime != MSDP_HOLDTIME))
                && (i4AddrType == IPVX_ADDR_FMLY_IPV4))
            {
                CliPrintf (CliHandle,
                           "  ip msdp keepalive %s %d %d \r\n",
                           pRemoteAddr, i4RetVal, i4HoldTime);

            }
            else if (((i4RetVal != MSDP_KAT_VAL) ||
                      (i4HoldTime != MSDP_HOLDTIME)) &&
                     (i4AddrType == IPVX_ADDR_FMLY_IPV6))
            {
                CliPrintf (CliHandle,
                           "  ipv6 msdp keepalive %s %d %d \r\n",
                           pRemoteAddr, i4RetVal, i4HoldTime);

            }

            nmhGetFsMsdpPeerDataTtl (i4AddrType, &PeerAddr, &i4RetVal);
            if ((MSDP_DATA_TTL != i4RetVal) &&
                (i4AddrType == IPVX_ADDR_FMLY_IPV4))
            {
                CliPrintf (CliHandle,
                           "  ip msdp ttl-threshold %s %d \r\n",
                           pRemoteAddr, i4RetVal);

            }
            else if ((MSDP_DATA_TTL != i4RetVal) &&
                     (i4AddrType == IPVX_ADDR_FMLY_IPV6))
            {
                CliPrintf (CliHandle,
                           "  ipv6 msdp ttl-threshold %s %d \r\n",
                           pRemoteAddr, i4RetVal);

            }

        }
        if (SNMP_SUCCESS != nmhGetNextIndexFsMsdpPeerTable
            (i4AddrType, &i4NextAddrType, &PeerAddr, &NextPeerAddr))
        {
            break;
        }
        MEMCPY (PeerAddr.pu1_OctetList, NextPeerAddr.pu1_OctetList,
                NextPeerAddr.i4_Length);
        PeerAddr.i4_Length = NextPeerAddr.i4_Length;
        i4AddrType = i4NextAddrType;
    }
    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpShowRunningConfigPeerTable"));

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function   : MsdpCliGetTraceValue()
 * Description: This function returns the Global trace value 
 * Input      : VOID
 * Output     : NONE
 * Returns    : INT4
 *****************************************************************************/

INT4
MsdpCliGetTraceValue (VOID)
{
    MSDP_TRC_FUNC ((MSDP_CLI_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpCliGetTraceValue"));

    return MSDP_TRC_FLAG;
}

INT4
MsdpCliGetSARedistStatus (INT4 i4AddrType)
{
    INT4                i4Status = 0;
    INT4                i4RetStatus = 0;

    MSDP_LOCK;

    i4RetStatus = nmhGetFsMsdpSARedistributionStatus (i4AddrType, &i4Status);

    MSDP_UNLOCK;

    if (i4RetStatus != SNMP_SUCCESS)
    {
        i4Status = CLI_FAILURE;
    }
    else
    {
        i4Status = CLI_SUCCESS;
    }
    return i4Status;
}

INT4
MsdpCliGetRPStatus (INT4 i4AddrType)
{
    INT4                i4Status = 0;
    INT4                i4RetStatus = 0;

    MSDP_LOCK;

    i4RetStatus = nmhGetFsMsdpRPStatus (i4AddrType, &i4Status);

    MSDP_UNLOCK;

    if (i4RetStatus != SNMP_SUCCESS)
    {
        i4Status = CLI_FAILURE;
    }
    else
    {
        i4Status = CLI_SUCCESS;
    }
    return i4Status;
}
#endif /*End of msdpcli.c file */
