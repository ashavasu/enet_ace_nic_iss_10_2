/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: msdpin.c,v 1.6 2014/03/14 12:56:29 siva Exp $
*
* Description: This file contains the Msdp CLI INPUT module routines 
*********************************************************************/
#include "msdpinc.h"

/****************************************************************************
 * FUNCTION NAME : MsdpInProcessRxdMsg
 *
 * DESCRIPTION   : This function processes the received message (SA, SA response,
 *                 SA request, keep alive messages)
 *
 * INPUTS        : pPeer- MSDP peer entry
 *
 * OUTPUTS       : Peer Rx Buf Info filled and the received data is processed. 
 *
 * RETURNS       : OSIX_SUCCESS if receive buffer is full or message received 
 *                 successfully.
 *                 OSIX_FAILURE otherwise.
 *
 ***************************************************************************/
INT4
MsdpInProcessRxdMsg (tMsdpFsMsdpPeerEntry * pPeer)
{
    INT4                i4ProcessedBytes = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_IN | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpInProcessRxdMsg"));

    while ((i4ProcessedBytes =
            MsdpInHandleRcvdPacket (pPeer)) != MSDP_PKT_ERROR)
    {
        /* reduce the processed bytes from the total msg size */
        if (i4ProcessedBytes == 0)
        {
            MSDP_TRC_FUNC ((MSDP_TRC_IN | MSDP_FN_EXIT,
                            "\nEXIT: MsdpInProcessRxdMsg - more data to read"));
            return OSIX_SUCCESS;
        }

        pPeer->RxBufInfo.u2MsgSize -= (UINT2) i4ProcessedBytes;

        if (pPeer->RxBufInfo.u2MsgSize == 0)
        {
            pPeer->RxBufInfo.u2MsgOffset = 0;

            MSDP_TRC_FUNC ((MSDP_TRC_IN | MSDP_FN_EXIT,
                            "\nEXIT: MsdpInProcessRxdMsg"));
            return OSIX_SUCCESS;
        }
        else
        {
            /* move the offset by the processed bytes */
            pPeer->RxBufInfo.u2MsgOffset += i4ProcessedBytes;
            /* adjust the offset by modulo of the max size */
            pPeer->RxBufInfo.u2MsgOffset %= (UINT2) MSDP_PEER_MAX_RX_TX_BUF_LEN;
        }

        MSDP_TRC_FUNC ((MSDP_TRC_IN, "\nMsdpInProcessRxdMsg: "
                        "MSDP packet processed successfully"));

    }
    /* some error is processin or with socket */
    /*Failure trace */
    MsdpPeerRunPfsm (pPeer, MSDP_PEER_STOP_EVT);

    MSDP_TRC_FUNC ((MSDP_TRC_IN, "\nMsdpInProcessRxdMsg: "
                    "Error in processing or with socket"));

    MSDP_TRC_FUNC ((MSDP_TRC_IN | MSDP_FN_EXIT, "\nEXIT: MsdpInProcessRxdMsg"));

    return OSIX_FAILURE;
}

/****************************************************************************
 * FUNCTION NAME : MsdpInReadPeerMsg
 *
 * DESCRIPTION   : This function receives a message via the socket 
 *
 * INPUTS        : pPeer- MSDP peer entry
 *
 * OUTPUTS       : Peer Rx Buf Info filled with received data. 
 *
 * RETURNS       : OSIX_SUCCESS if buffer is full or message processed
 *                 OSIX_FAILURE otherwise.
 *
 ***************************************************************************/
INT4
MsdpInReadPeerMsg (tMsdpFsMsdpPeerEntry * pPeer)
{
    UINT1              *pu1Data = NULL;
    INT4                i4SockFd = MSDP_INVALID_SOCK_ID;
    INT4                i4RdBytes = 0;
    UINT2               u2LenToRead = 0;
    UINT2               u2MsgOffset = 0;
    UINT2               u2MsgSize = 0;
    UINT2               u2WriteOffset = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_IN | MSDP_FN_ENTRY, "\nENTRY: MsdpInReadPeerMsg"));

    u2MsgSize = pPeer->RxBufInfo.u2MsgSize;
    if (u2MsgSize == MSDP_PEER_MAX_RX_TX_BUF_LEN)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_IN, "\nMsdpInReadPeerMsg: Peer Rcv Buf Full"));

        MSDP_TRC_FUNC ((MSDP_TRC_IN | MSDP_FN_EXIT,
                        "\nEXIT: MsdpInReadPeerMsg"));

        return OSIX_SUCCESS;
    }
    u2MsgOffset = pPeer->RxBufInfo.u2MsgOffset;
    pu1Data = pPeer->RxBufInfo.pu1Msg + u2MsgOffset;
    i4SockFd = pPeer->i4SockFd;

    u2WriteOffset = (UINT2) ((u2MsgOffset + u2MsgSize) %
                             (UINT2) MSDP_PEER_MAX_RX_TX_BUF_LEN);
    if (u2WriteOffset >= u2MsgOffset)
    {
        /* read to the end of the buffer */
        u2LenToRead = (UINT2) ((UINT2) MSDP_PEER_MAX_RX_TX_BUF_LEN -
                               (u2MsgOffset + u2MsgSize));
    }
    else
    {
        /* The buffer is read to the remaining */
        u2LenToRead = (UINT2) (u2MsgOffset - u2WriteOffset);
    }

    if (MsdpSockRcv (i4SockFd, pu1Data, u2LenToRead,
                     &i4RdBytes) == OSIX_SUCCESS)
    {
        pPeer->RxBufInfo.u2MsgSize += (UINT2) i4RdBytes;

        MSDP_TRC_FUNC ((MSDP_TRC_IN,
                        "\nMsdpInReadPeerMsg: Packet Rcvd successfully"));

        MSDP_TRC_FUNC ((MSDP_TRC_IN | MSDP_FN_EXIT,
                        "\nEXIT: MsdpInReadPeerMsg"));

        return OSIX_SUCCESS;
    }
    /* some error is processin or with socket */
    MsdpPeerRunPfsm (pPeer, MSDP_PEER_STOP_EVT);

    MSDP_TRC_FUNC ((MSDP_TRC_IN | MSDP_FN_EXIT, "\nEXIT: MsdpInReadPeerMsg"));
    return OSIX_FAILURE;
}

/****************************************************************************
 * FUNCTION NAME : MsdpInHandleRcvdPacket
 *
 * DESCRIPTION   : This function processes the received packet according to the
 *                 type of message
 *
 * INPUTS        : pPeer- MSDP peer entry
 *
 * OUTPUTS       : None
 *
 * RETURNS       : Returns no.of bytes processed or PKT_ERROR
 *
 ***************************************************************************/
INT4
MsdpInHandleRcvdPacket (tMsdpFsMsdpPeerEntry * pPeer)
{
    tOsixSysTime        SysTime;
    INT4                i4RetVal = MSDP_PKT_ERROR;
    UINT2               u2MsgOffset = 0;
    UINT1               u1Type = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_IN | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpInHandleRcvdPacket"));

    u2MsgOffset = pPeer->RxBufInfo.u2MsgOffset;
    u1Type = *(pPeer->RxBufInfo.pu1Msg + u2MsgOffset);

    OsixGetSysTime (&SysTime);
    pPeer->MibObject.u4FsMsdpPeerInMessageTime = (UINT4) SysTime;

    switch (u1Type)
    {
        case MSDP_TLV_SA:
        case MSDP_TLV_SA_RESPONSE:

            MSDP_TRC_FUNC ((MSDP_TRC_IN, "\nMsdpInHandleRcvdPacket"
                            "IPv4 SA  Rcvd"));

            i4RetVal = MsdpInHandleSaAdvtOrRespPkt (pPeer, IPVX_ADDR_FMLY_IPV4);
            break;
        case MSDP6_TLV_SA:
        case MSDP6_TLV_SA_RESPONSE:

            MSDP_TRC_FUNC ((MSDP_TRC_IN, "\nMsdpInHandleRcvdPacket"
                            "IPv6 SA  Rcvd"));

            i4RetVal = MsdpInHandleSaAdvtOrRespPkt (pPeer, IPVX_ADDR_FMLY_IPV6);
            break;
        case MSDP_TLV_SA_REQUEST:

            MSDP_TRC_FUNC ((MSDP_TRC_IN, "\nMsdpInHandleRcvdPacket"
                            "IPv4 SA Request Rcvd"));

            i4RetVal = MsdpInProcessSaRequestPkt (pPeer, IPVX_ADDR_FMLY_IPV4);
            break;
        case MSDP6_TLV_SA_REQUEST:

            MSDP_TRC_FUNC ((MSDP_TRC_IN, "\nMsdpInHandleRcvdPacket"
                            "IPv4 SA Request Rcvd"));

            i4RetVal = MsdpInProcessSaRequestPkt (pPeer, IPVX_ADDR_FMLY_IPV6);
            break;
        case MSDP_TLV_KEEPALIVE:

            MSDP_TRC_FUNC ((MSDP_TRC_IN, "\nMsdpInHandleRcvdPacket"
                            "Keepalive Pkt Rcvd"));

            i4RetVal = MsdpInProcessKeepAlivePkt (pPeer);
            break;
        default:
            i4RetVal = MSDP_PKT_ERROR;

            MSDP_TRC_FUNC ((MSDP_TRC_IN,
                            "\nMsdpInHandleRcvdPacket: Invalid Pkt Type Rcvd"));

            break;
    }

    MSDP_TRC_FUNC ((MSDP_TRC_IN | MSDP_FN_EXIT,
                    "\nEXIT: MsdpInHandleRcvdPacket"));

    return i4RetVal;
}

/****************************************************************************
 * FUNCTION NAME : MsdpInHandleSaAdvtOrRespPkt
 * 
 * DESCRIPTION   : This function processes the MSDP Source Advertisement (SA)
 *                 and SA Response packet. The SA pkts are fwded to all the 
 *                 MSDP peers. MSDP cache entries are updated with the received
 *                 SA Info. The SA info received in SA Response are informed
 *                 for multicast routing protocol (PIM).
 *
 * INPUTS        : pPeer- MSDP peer entry
 *
 *                 u1AddrType- Peer Address type
 *
 * OUTPUTS       : None
 *
 * RETURNS       : Should be number of bytes processed, if success
 *                 else MSDP_PKT_ERROR 
 ***************************************************************************/
INT4
MsdpInHandleSaAdvtOrRespPkt (tMsdpFsMsdpPeerEntry * pPeer, UINT1 u1AddrType)
{
    tMsdpSaAdvtHdr     *pSaAdvtHdr = NULL;
    tIPvXAddr           SaAdvtOrigRpAddr;
    INT4                i4RetVal = 0;
    INT4                i4HoldTimeVal = 0;
    UINT2               u2PktLen = 0;
    UINT2               u2MsgOffset = 0;
    UINT2               u2ReadOffset = 0;
    UINT2               u2MsgSize = 0;
    UINT2               u2Count = 0;
    UINT1               u1SaAdvtPktMinSize = 0;
    UINT1               u1HdrSize = 0;
    UINT1               au1SaAdvtHdr[sizeof (tMsdpSaAdvtHdr)];

    MSDP_TRC_FUNC ((MSDP_TRC_IN | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpInHandleSaAdvtOrRespPkt"));

    MEMSET (&SaAdvtOrigRpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (au1SaAdvtHdr, 0, sizeof (tMsdpSaAdvtHdr));
    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        u1HdrSize = MSDP_IPV4_SA_ADVT_HDR_SIZE;
        u1SaAdvtPktMinSize = MSDP_MIN_IPV4_SA_ADVT_SIZE;
    }
    else
    {
        u1HdrSize = MSDP_IPV6_SA_ADVT_HDR_SIZE;
        u1SaAdvtPktMinSize = MSDP_MIN_IPV6_SA_ADVT_SIZE;
    }

    u2MsgSize = pPeer->RxBufInfo.u2MsgSize;
    if (u2MsgSize < u1SaAdvtPktMinSize)
    {
        /* pkt hdr is partially read */

        MSDP_TRC_FUNC ((MSDP_TRC_IN,
                        "\nMsdpInHandleSaAdvtOrRespPkt: pkt hdr is partially read"
                        " u2MsgSize(%d)", u2MsgSize));

        return 0;
    }
    u2MsgOffset = pPeer->RxBufInfo.u2MsgOffset;
    u2ReadOffset = u2MsgOffset;

    if ((u2ReadOffset + u1HdrSize) >= MSDP_PEER_MAX_RX_TX_BUF_LEN)
    {
        /* the pkt hdr is wrapped around */
        for (u2Count = 0; u2Count < u1HdrSize; u2Count++)
        {
            au1SaAdvtHdr[u2Count] = *(pPeer->RxBufInfo.pu1Msg + u2ReadOffset);
            u2ReadOffset++;
            if (u2ReadOffset >= MSDP_PEER_MAX_RX_TX_BUF_LEN)
            {
                u2ReadOffset = 0;
            }
        }
        pSaAdvtHdr = (tMsdpSaAdvtHdr *) (VOID *) au1SaAdvtHdr;
    }
    else
    {
        pSaAdvtHdr = (tMsdpSaAdvtHdr *) (VOID *)
            (pPeer->RxBufInfo.pu1Msg + u2MsgOffset);
        u2ReadOffset = (UINT2) (u2MsgOffset + u1HdrSize);
    }

    u2PktLen = pSaAdvtHdr->u2Len;

    u2PktLen = OSIX_NTOHS (u2PktLen);

    if (u2MsgSize >= u2PktLen)
    {
        /* do pkt validation once the full pkt is received */

        /* re-start the peer hold timer */
        i4HoldTimeVal = pPeer->MibObject.i4FsMsdpPeerHoldTimeConfigured;
        if (i4HoldTimeVal != 0)
        {
            MsdpTmrStartTmr (&pPeer->MsdpPeerHoldTimer, MSDP_PEER_HOLD_TMR,
                             (UINT4) i4HoldTimeVal);
        }
        if ((pSaAdvtHdr->u1Type != MSDP_TLV_SA_RESPONSE) &&
            (pSaAdvtHdr->u1Type != MSDP6_TLV_SA_RESPONSE))
        {
            IPVX_ADDR_INIT (SaAdvtOrigRpAddr, u1AddrType,
                            pSaAdvtHdr->au1RpAddr);
            pPeer->MibObject.u4FsMsdpPeerInSAs++;
            if (MsdpInChkRpfPeer (pPeer, &SaAdvtOrigRpAddr) != OSIX_SUCCESS)
            {
                /* The peer is not the RPF peer for the RP Address. Skip it */
                pPeer->MibObject.u4FsMsdpPeerRPFFailures++;

                MSDP_TRC_FUNC ((MSDP_TRC_IN, "\nMsdpInHandleSaAdvtOrRespPkt: "
                                "%s is not RPF peer for RP Addr %s",
                                pPeer->MibObject.au1FsMsdpPeerLocalAddress,
                                MsdpPrintIPvxAddress (SaAdvtOrigRpAddr)));

                return (INT4) u2PktLen;
            }
            /* forward to all the peers */
            MsdpOutFwdSAPkt (pPeer, pPeer->RxBufInfo.pu1Msg, u1AddrType,
                             pPeer->RxBufInfo.u2MsgOffset, u2PktLen,
                             MSDP_PEER_MAX_RX_TX_BUF_LEN);
            MSDP_TRC_FUNC ((MSDP_TRC_IN, "\nMsdpInHandleSaAdvtOrRespPkt: "
                            "Fwded SA Pkt to all Peers"));

        }
        else
        {
            pPeer->MibObject.u4FsMsdpPeerInSAResponses++;
        }

        pPeer->MibObject.u4FsMsdpPeerInControlMessages++;

        i4RetVal = u2PktLen;
        if (MsdCacheProcessSAMsg (pPeer, pSaAdvtHdr, u2ReadOffset,
                                  u1AddrType) != OSIX_SUCCESS)
        {
            /* pkt processing in error */
            MSDP_TRC_FUNC ((MSDP_TRC_IN, "\nMsdpInHandleSaAdvtOrRespPkt: "
                            "Error in Processing SA pkt"));
            i4RetVal = MSDP_PKT_ERROR;
        }
    }

    MSDP_TRC_FUNC ((MSDP_TRC_IN, "\nMsdpInHandleSaAdvtOrRespPkt: "
                    "SA Advt/Response Msg Processed Successfully"));
    /* else pkt is incomplete. more to read */
    MSDP_TRC_FUNC ((MSDP_TRC_IN | MSDP_FN_EXIT,
                    "\nEXIT: MsdpInHandleSaAdvtOrRespPkt"));
    return i4RetVal;
}

/****************************************************************************
 * FUNCTION NAME : MsdpInProcessSaRequestPkt
 *
 * DESCRIPTION   : This function processes the received SA Request packet.
 *                 If the packet is successfully processed the SA REsponse 
 *                 Msg is sent to the requested Peer.
 *
 * INPUTS        : pPeer- MSDP peer entry
 * 
 * OUTPUTS       : None
 *
 * RETURNS       : Should be number of bytes processed, if success
 *                 else MSDP_PKT_ERROR 
 ***************************************************************************/
INT4
MsdpInProcessSaRequestPkt (tMsdpFsMsdpPeerEntry * pPeer, UINT1 u1AddrType)
{
    UINT1               au1SaReqPkt[MSDP_IPV6_SA_REQ_PKT_SIZE];
    tMsdpSaReqPkt      *pSaReqPkt = NULL;
    INT4                i4RetVal = 0;
    INT4                i4HoldTimeVal = 0;
    UINT2               u2MsgOffset = 0;
    UINT2               u2ReadOffset = 0;
    UINT2               u2MsgSize = 0;
    UINT2               u2Count = 0;
    UINT1               u1SaReqPktSize = sizeof (tMsdpSaReqPkt);

    MSDP_TRC_FUNC ((MSDP_TRC_IN | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpInProcessSaRequestPkt"));

    MEMSET (&au1SaReqPkt, 0, MSDP_IPV6_SA_REQ_PKT_SIZE);
    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        u1SaReqPktSize = MSDP_IPV4_SA_REQ_PKT_SIZE;
    }
    else
    {
        u1SaReqPktSize = MSDP_IPV6_SA_REQ_PKT_SIZE;
    }

    u2MsgSize = pPeer->RxBufInfo.u2MsgSize;
    if (u2MsgSize < u1SaReqPktSize)
    {
        /* pkt is partially read */

        MSDP_TRC_FUNC ((MSDP_TRC_IN, "\nMsdpInHandleSaAdvtOrRespPkt: Partially "
                        "read pkt, u2MsgSize %d", u2MsgSize));

        return 0;
    }

    pPeer->MibObject.u4FsMsdpPeerInSARequests++;
    /* re-start the peer hold timer */
    i4HoldTimeVal = pPeer->MibObject.i4FsMsdpPeerHoldTimeConfigured;
    if (i4HoldTimeVal != 0)        /*i4HoldTimeVal == 0 indicates infinite holdtime */
    {
        MsdpTmrStartTmr (&pPeer->MsdpPeerHoldTimer, MSDP_PEER_HOLD_TMR,
                         (UINT4) i4HoldTimeVal);
    }
    u2MsgOffset = pPeer->RxBufInfo.u2MsgOffset;
    u2ReadOffset = u2MsgOffset;

    if ((u2ReadOffset + u1SaReqPktSize) >= MSDP_PEER_MAX_RX_TX_BUF_LEN)
    {
        /* the pkt hdr is wrapped around */
        for (u2Count = 0; u2Count < u1SaReqPktSize; u2Count++)
        {
            au1SaReqPkt[u2Count] = *(pPeer->RxBufInfo.pu1Msg + u2ReadOffset);
            u2ReadOffset++;
            if (u2ReadOffset >= MSDP_PEER_MAX_RX_TX_BUF_LEN)
            {
                u2ReadOffset = 0;
            }
        }
        pSaReqPkt = (tMsdpSaReqPkt *) (VOID *) au1SaReqPkt;
    }
    else
    {
        pSaReqPkt = (tMsdpSaReqPkt *) (VOID *)
            (pPeer->RxBufInfo.pu1Msg + u2MsgOffset);
    }
    pSaReqPkt->u2Len = OSIX_NTOHS (pSaReqPkt->u2Len);

    i4RetVal = pSaReqPkt->u2Len;

    if (MsdCacheHandleSARequest (pPeer, pSaReqPkt, u1AddrType) != OSIX_SUCCESS)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_IN, "\nMsdpInHandleSaAdvtOrRespPkt: "
                        "MsdCacheHandleSARequest returns failure"));

        i4RetVal = MSDP_PKT_ERROR;
    }

    MSDP_TRC_FUNC ((MSDP_TRC_IN, "\nMsdpInHandleSaAdvtOrRespPkt: "
                    "MsdCacheHandleSARequest returns success"));

    MSDP_TRC_FUNC ((MSDP_TRC_IN | MSDP_FN_EXIT,
                    "\nEXIT: MsdpInHandleSaAdvtOrRespPkt"));

    return i4RetVal;
}

/****************************************************************************
 * FUNCTION NAME : MsdpInProcessKeepAlivePkt
 *
 * DESCRIPTION   : This function processes the Keep Alive packet 
 *                 It validates the packet and starts Peer Hold Timer for the
 *                 Hold Time configured 
 *
 * INPUTS        :  pPeer - MSDP Peer Entry
 *
 * OUTPUTS       :  None
 *
 * RETURNS       : MSDP_KEEP_ALIVE_PKT_SIZE for valid packet received
 *                 0 for Invalid packets received 
 ***************************************************************************/
INT4
MsdpInProcessKeepAlivePkt (tMsdpFsMsdpPeerEntry * pPeer)
{
    INT4                i4HoldTimeVal = 0;
    UINT2               u2MsgSize = 0;
    UINT2               u2KatPktSize = MSDP_KEEP_ALIVE_PKT_SIZE;

    MSDP_TRC_FUNC ((MSDP_TRC_IN | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpInProcessKeepAlivePkt"));

    u2MsgSize = pPeer->RxBufInfo.u2MsgSize;
    if (u2MsgSize < u2KatPktSize)
    {
        /* pkt is partially read */

        MSDP_TRC_FUNC ((MSDP_TRC_IN, "\nMsdpInProcessKeepAlivePkt: "
                        "Pkt is partially read, u2MsgSize %d", u2MsgSize));

        return 0;
    }
    /* re-start the peer hold timer */
    i4HoldTimeVal = pPeer->MibObject.i4FsMsdpPeerHoldTimeConfigured;

    MsdpTmrStopTmr (&pPeer->MsdpPeerHoldTimer);
    if (i4HoldTimeVal != 0)        /*i4HoldTimeVal == 0 indicates infinite holdtime */
    {
        MsdpTmrStartTmr (&pPeer->MsdpPeerHoldTimer, MSDP_PEER_HOLD_TMR,
                         (UINT4) i4HoldTimeVal);
    }

    MSDP_TRC_FUNC ((MSDP_TRC_IN, "\nMsdpInProcessKeepAlivePkt: "
                    "Peer Hold timer started for the Peer %s  with the Val  %d",
                    pPeer->MibObject.au1FsMsdpPeerLocalAddress, i4HoldTimeVal));

    MSDP_TRC_FUNC ((MSDP_TRC_IN | MSDP_FN_EXIT,
                    "\nEXIT: MsdpInProcessKeepAlivePkt"));
    pPeer->MibObject.u4FsMsdpPeerInKeepAliveCount++;
    return MSDP_KEEP_ALIVE_PKT_SIZE;
}

/****************************************************************************
 * FUNCTION NAME : MsdpInChkRpfPeer
 * 
 * DESCRIPTION   : This function checks the peer from whom the pkt received
 *                 is a RPF peer for the originator  embedded in the SA msg
 *                 or not 
 * 
 * INPUTS        : pSaAdvtPeer- MSDP peer entry
 *
 *                 pOriginator- Originator ipv4/ipv6 address
 *
 * OUTPUTS       : None
 *
 * RETURNS       : OSIX_SUCCESS is the peer is RPF peer
 *                 OSIX_FAILURE otherwise
 ***************************************************************************/
INT4
MsdpInChkRpfPeer (tMsdpFsMsdpPeerEntry * pSaAdvtPeer, tIPvXAddr * pOriginator)
{
    tIPvXAddr           OrigNextHop;
    tIPvXAddr           PeerNextHop;
    tIPvXAddr           SaAdvtPeerAddr;
    INT4                i4Metrics = 0;
    INT4                i4Port = MSDP_INVALID;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT4               u4Preference = 0;
    UINT1               u1AddrType = IPVX_ADDR_FMLY_IPV4;

    MSDP_TRC_FUNC ((MSDP_TRC_IN | MSDP_FN_ENTRY, "\nENTRY: MsdpInChkRpfPeer"));

    MEMSET (&OrigNextHop, 0, sizeof (tIPvXAddr));
    MEMSET (&PeerNextHop, 0, sizeof (tIPvXAddr));
    MEMSET (&SaAdvtPeerAddr, 0, sizeof (tIPvXAddr));
    MsdpUtilGetUcastRtInfo (pOriginator, &OrigNextHop, &i4Metrics,
                            &u4Preference, &i4Port);

    if (i4Port == MSDP_INVALID)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_IN,
                        "\nMsdpInChkRpfPeer: MsdpUtilGetUcastRtInfo gives Error"));

        return i4RetVal;
    }
    u1AddrType = (UINT1) pSaAdvtPeer->MibObject.i4FsMsdpPeerAddrType;

    IPVX_ADDR_INIT (SaAdvtPeerAddr, u1AddrType,
                    pSaAdvtPeer->MibObject.au1FsMsdpPeerRemoteAddress);
    if (IPVX_ADDR_COMPARE (SaAdvtPeerAddr, OrigNextHop) == 0)
    {
        /* originator is the peer advertised */

        MSDP_TRC_FUNC ((MSDP_TRC_IN,
                        "\nMsdpInChkRpfPeer: Peer Addr %s is the RPF peer for %s",
                        MsdpPrintIPvxAddress (SaAdvtPeerAddr),
                        MsdpPrintIPvxAddress (*pOriginator)));

        MSDP_TRC_FUNC ((MSDP_TRC_IN | MSDP_FN_EXIT,
                        "\nEXIT: MsdpInChkRpfPeer"));

        return OSIX_SUCCESS;
    }

    MsdpUtilGetUcastRtInfo (&SaAdvtPeerAddr, &PeerNextHop, &i4Metrics,
                            &u4Preference, &i4Port);

    if (i4Port == MSDP_INVALID)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_IN,
                        "\nMsdpInChkRpfPeer: Peer Addr %s is not RPF peer for %s",
                        MsdpPrintIPvxAddress (SaAdvtPeerAddr),
                        MsdpPrintIPvxAddress (*pOriginator)));

        MSDP_TRC_FUNC ((MSDP_TRC_IN,
                        "\nMsdpInChkRpfPeer: MsdpUtilGetUcastRtInfo "
                        "gives error"));
        return i4RetVal;
    }

    if (IPVX_ADDR_COMPARE (OrigNextHop, PeerNextHop) == 0)
    {
        /* NH to the originator and the NH to Peer are the same */

        MSDP_TRC_FUNC ((MSDP_TRC_IN,
                        "\nMsdpInChkRpfPeer: Peer Addr %s is the RPF peer for %s",
                        MsdpPrintIPvxAddress (SaAdvtPeerAddr),
                        MsdpPrintIPvxAddress (*pOriginator)));

        MSDP_TRC_FUNC ((MSDP_TRC_IN | MSDP_FN_EXIT,
                        "\nEXIT: MsdpInChkRpfPeer"));

        return OSIX_SUCCESS;
    }

    MSDP_TRC_FUNC ((MSDP_TRC_IN,
                    "\nMsdpInChkRpfPeer: Peer Addr %s is not RPF peer for %s",
                    MsdpPrintIPvxAddress (SaAdvtPeerAddr),
                    MsdpPrintIPvxAddress (*pOriginator)));

    MSDP_TRC_FUNC ((MSDP_TRC_IN | MSDP_FN_EXIT, "\nEXIT: MsdpInChkRpfPeer"));

    return i4RetVal;
}
