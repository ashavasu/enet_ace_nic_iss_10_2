/******************************************************************************
 * Copyright (C) Future Software Limited,2005
 *
 *$Id: msd6port.c,v 1.5 2012/07/11 11:22:04 siva Exp $
 *
 * Description : This file contains the IPv6  porting 
 *               functions of the MSDP module
 *****************************************************************************/
#include "msdpinc.h"
#include "msdpcli.h"

/***************************************************************************
 *                                                                         
 *     Function Name : Msd6PortGetFirstIfAddr 
 *                                                                         
 *     Description   : This function gets the interface address for a given
 *                     interface index                           
 *                                                                         
 *     Input(s)      : u4IfIndex- Interface index           
 *                                                                         
 *     Output(s)     : pu1ip6Addr- Interface Address
 *                                                                         
 *     Returns       : OSIX_SUCCESS on obtaining the interface address
 *                     OSIX_FAILURE otherwise                    
 *                                                                         
***************************************************************************/
INT4
Msd6PortGetFirstIfAddr (UINT4 u4IfIndex, UINT1 *pu1ip6Addr)
{
    tNetIpv6AddrInfo    NetIpv6AddrInfo;
    INT4                i4RetStat = OSIX_FAILURE;

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_ENTRY,
                    "\nENTRY: Msd6PortGetFirstIfAddr"));

    if (pu1ip6Addr == NULL)
    {
        MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_EXIT,
                        "\nEXIT: Msd6PortGetFirstIfAddr: INPUT NULL"));
        return i4RetStat;
    }

    MEMSET (pu1ip6Addr, 0, IPVX_IPV6_ADDR_LEN);

    if (OSIX_SUCCESS == NetIpv6GetFirstIfAddr (u4IfIndex, &NetIpv6AddrInfo))
    {
        MEMCPY (pu1ip6Addr, &(NetIpv6AddrInfo.Ip6Addr.u1_addr),
                IPVX_IPV6_ADDR_LEN);

        MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_EXIT,
                        "\nEXIT: Msd6PortGetFirstIfAddr"));

        i4RetStat = OSIX_SUCCESS;
    }
    else
    {
        MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\n Msd6PortGetFirstIfAddr: "
                        "NetIpv6GetFirstIfAddr returns failure for "
                        "u4IfIndex value %d", u4IfIndex));
    }
    return i4RetStat;

}

/***************************************************************************
 *                                                                         
 *     Function Name : Msd6PortGetVlanIdFromIp6Address                         
 *                                                                         
 *     Description   : This function obtains the vlan id from the ip
 *                     address provided                     
 *                                                                         
 *     Input(s)      : pLocalAddr- Address        
 *                                                                         
 *     Output(s)     : u2VlanId- VLAN ID                                
 *                                                                         
 *     Returns       : OSIX_SUCCESS on obtaining the vlan id corr to address
 *                     OSIX_FAILURE otherwise                         
 *                                                                         
***************************************************************************/
INT4
Msd6PortGetVlanIdFromIp6Address (tSNMP_OCTET_STRING_TYPE * pLocalAddr,
                                 UINT2 *u2VlanId)
{
    UINT4               u4IfIndex = 0;
    INT4                i4RetStat = OSIX_FAILURE;
    tIp6Addr            ip6LocalAddr;

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_ENTRY,
                    "\nENTRY: Msd6PortGetVlanIdFromIp6Address"));

    if (pLocalAddr == NULL)
    {
        MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nInput pLocalAddr is NULL"));
        MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_EXIT,
                        "\nEXIT: Msd6PortGetVlanIdFromIp6Address"));
        return i4RetStat;
    }


    Ip6AddrCopy (&ip6LocalAddr, (tIp6Addr *) (VOID *) pLocalAddr->pu1_OctetList);

    if (NETIPV6_SUCCESS == NetIpv6IsOurAddress ( &ip6LocalAddr, &u4IfIndex))							    
    {
        if (CFA_SUCCESS == CfaGetVlanId (u4IfIndex, u2VlanId))
        {

            MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_EXIT,
                            "\nEXIT: Msd6PortGetVlanIdFromIp6Address"));

            i4RetStat = OSIX_SUCCESS;
        }
        else
        {

            MSDP_TRC_FUNC ((MSDP_TRC_PORT,
                            "\nMsd6PortGetVlanIdFromIp6Address: "
                            "CfaGetVlanId returns failure for "
                            "u2VlanId %d", *u2VlanId));
        }

    }
    else
    {

        MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsd6PortGetVlanIdFromIp6Address: "
                        "NetIpv6GetIfIndexFromIp6Address returns failure"));

    }
    return i4RetStat;

}

/***************************************************************************
 *                                                                         
 *     Function Name : Msd6PortIsValidLocalAddress                          
 *                                                                         
 *     Description   : This function checks whether the given IP address is 
 *                     a valid local address                         
 *                                                                         
 *     Input(s)      : pu1Addr- IP address           
 *                                                                         
 *     Output(s)     : None.                                              
 *                                                                         
 *     Returns       : Interface index of the address provided if valid
 *                     0 otherwise                                   
 *                                                                         
***************************************************************************/
INT4
Msd6PortIsValidLocalAddress (UINT1 *pu1Addr)
{
    tIp6Addr           *pAddr = (tIp6Addr *) (VOID *) pu1Addr;
    UINT4               u4IfIndex = 0;
    INT4                i4RetVal = MSDP_INVALID;

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_ENTRY,
                    "\nENTRY: Msd6PortIsValidLocalAddress"));

    if (CFA_SUCCESS == NetIpv6IsOurAddress (pAddr, &u4IfIndex))
    {
        i4RetVal = u4IfIndex;

        MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_EXIT,
                        "\nEXIT: Msd6PortIsValidLocalAddress"));

    }
    else
    {

        MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsd6PortIsValidLocalAddress: "
                        "NetIpv6IsOurAddress returns failure"));

    }
    return i4RetVal;
}

/***************************************************************************
 *                                                                         
 *     Function Name : Msd6PortIsAddrMulti                          
 *                                                                         
 *     Description   : This function checks whether the given address is a
 *                     multicast address                    
 *                                                                         
 *     Input(s)      : pu1Addr- IP address           
 *                                                                         
 *     Output(s)     : None                                 
 *                                                                         
 *     Returns       : The return value of IS_ADDR_MULTI                                   
 *                                                                         
***************************************************************************/
INT4
Msd6PortIsAddrMulti (UINT1 *pu1Addr)
{
    tIp6Addr            Addr;

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_ENTRY,
                    "\nENTRY: Msd6PortIsAddrMulti"));

    if (pu1Addr == NULL)
    {
        MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nInput Address NULL"));
        MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_EXIT,
                        "\nEXIT: Msd6PortIsAddrMulti"));
        return OSIX_FAILURE;
    }

    MEMSET (&Addr, 0, sizeof (tIp6Addr));

    Addr.u4_addr[0] = ((tIp6Addr *) (VOID *) pu1Addr)->u4_addr[0];

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_EXIT,
                    "\nEXIT: Msd6PortIsAddrMulti"));

    return (IS_ADDR_MULTI (Addr));
}

/***************************************************************************
 *                                                                         
 *     Function Name : Msd6PortIpv6GetRoute                   
 *                                                                         
 *     Description   : This function gets the IPv6 route information                     
 *                                                                         
 *     Input(s)      :  pIp6HliParams : Pointer to tIp6HliParams           
 *                                                                         
 *     Output(s)     :  None.                                              
 *                                                                         
 *     Returns       :  VOIDV.                                   
 *                                                                         
***************************************************************************/
INT4
Msd6PortIpv6GetRoute (tNetIpv6RtInfoQueryMsg * pNetIpv6RtQuery,
                      tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    INT4                i4RetStat = OSIX_FAILURE;

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_ENTRY,
                    "\nENTRY: Msd6PortIpv6GetRoute"));

    pNetIpv6RtQuery->u4ContextId = MSDP_DEF_VRF_CTXT_ID;
    i4RetStat = NetIpv6GetRoute (pNetIpv6RtQuery, pNetIpv6RtInfo);
    if (i4RetStat != NETIPV6_FAILURE)
    {
        MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_EXIT,
                        "\nEXIT: Msd6PortIpv6GetRoute"));
        i4RetStat = OSIX_SUCCESS;
    }
    else
    {
        i4RetStat = OSIX_FAILURE;
        MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsd6PortIpv6GetRoute: "
                        "NetIpv6GetRoute returns failure"));

    }

    return i4RetStat;

}

/***************************************************************************
 *                                                                         
 *     Function Name : Msd6PortRegiterWithIP6                    
 *                                                                         
 *     Description   : This function registers IPv6                           
 *                                                                         
 *     Input(s)      : None          
 *                                                                         
 *     Output(s)     : None.                                              
 *                                                                         
 *     Returns       : OSIX_SUCCESS if NetIpv6RegisterHigherLayerProtocolInCxt
 *                     succeeds
 *                     OSIX_FAILURE otherwise                                   
 *                                                                         
***************************************************************************/
INT4
Msd6PortRegiterWithIP6 (VOID)
{

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_ENTRY,
                    "\nENTRY: Msd6PortRegiterWithIP6"));

    if (NetIpv6RegisterHigherLayerProtocolInCxt
        (MSDP_DEF_VRF_CTXT_ID, MSDP_PROTOCOL_ID,
         NETIPV6_INTERFACE_PARAMETER_CHANGE,
         Msd6PortHandleIPV6IfChange) == NETIPV6_SUCCESS)

    {
        MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_EXIT,
                        "\nEXIT: Msd6PortRegiterWithIP6"));

        return OSIX_SUCCESS;
    }

    MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsd6PortRegiterWithIP6: "
                    "NetIpv6RegisterHigherLayerProtocolInCxt fails"));

    return OSIX_FAILURE;

}

/***************************************************************************
 *                                                                         
 *     Function Name : Msd6PortDeRegiterWithIP6                        
 *                                                                         
 *     Description   : This function deregisters IPv6 
 *                                                                         
 *     Input(s)      : None           
 *                                                                         
 *     Output(s)     : None                                              
 *                                                                         
 *     Returns       :OSIX_SUCCESS if NetIpv6DeRegisterHigherLayerProtocolInCxt
 *                     succeeeds
 *                     OSIX_FAILURE otherwise
 *                                                                         
***************************************************************************/
INT4
Msd6PortDeRegiterWithIP6 (VOID)
{

    MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_ENTRY,
                    "\nENTRY: Msd6PortDeRegiterWithIP6"));

    if (NetIpv6DeRegisterHigherLayerProtocolInCxt
        (MSDP_DEF_VRF_CTXT_ID, MSDP_PROTOCOL_ID) == NETIPV6_SUCCESS)

    {

        MSDP_TRC_FUNC ((MSDP_TRC_PORT | MSDP_FN_EXIT,
                        "\nEXIT: Msd6PortDeRegiterWithIP6"));

        return OSIX_SUCCESS;
    }

    MSDP_TRC_FUNC ((MSDP_TRC_PORT, "\nMsd6PortDeRegiterWithIP6: "
                    "NetIpv6DeRegisterHigherLayerProtocolInCxt fails"));

    return OSIX_FAILURE;

}

/***************************************************************************/
/*                                                                         */
/*     Function Name : Msd6PortHandleIPV6IfChange                          */
/*                                                                         */
/*     Description   : This function is provided to IPv6 as a callback     */
/*                     function. This is called by IPv6 when               */
/*                     Interface status changes.                           */
/*                                                                         */
/*     Input(s)      :  pIp6HliParams : Pointer to tIp6HliParams           */
/*                                                                         */
/*     Output(s)     :  None.                                              */
/*                                                                         */
/*     Returns       :  VOIDV.                                   */
/*                                                                         */
/***************************************************************************/
PUBLIC VOID
Msd6PortHandleIPV6IfChange (tNetIpv6HliParams * pIp6HliParams)
{
    tMsdpQueMsg        *pMsdpQueMsg = NULL;
    INT4                i4RetVal = 0;

    pMsdpQueMsg = MsdpQueAllocQueMsg ();
    if (pMsdpQueMsg == NULL)
    {
        return;
    }

    pMsdpQueMsg->eMsgType = MSDP_IF_CHG_EVT;
    pMsdpQueMsg->unMsdpMsgIfParam.MsdpIfStatus.u4IfIndex =
        pIp6HliParams->unIpv6HlCmdType.IfStatChange.u4Index;
    pMsdpQueMsg->unMsdpMsgIfParam.MsdpIfStatus.u1IfOperStatus =
        (UINT1) pIp6HliParams->unIpv6HlCmdType.IfStatChange.u4OperStatus;
    pMsdpQueMsg->unMsdpMsgIfParam.MsdpIfStatus.u1IfAdminStatus =
        (UINT1) pIp6HliParams->unIpv6HlCmdType.IfStatChange.u4IfStat;
    pMsdpQueMsg->unMsdpMsgIfParam.MsdpIfStatus.u1AddrType = IPVX_ADDR_FMLY_IPV6;
    i4RetVal = MsdpQuePostQueueMsg (pMsdpQueMsg);
    if (i4RetVal == OSIX_FAILURE)
    {
        return;
    }

    if (OsixSendEvent (SELF, (const UINT1 *) MSDP_TASK_NAME,
                       MSDP_QUEUE_EVENT) != OSIX_SUCCESS)
    {
        /* Trace */
        return;
    }
    return;
}
