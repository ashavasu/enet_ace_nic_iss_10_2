/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: msdpdb.c,v 1.6 2012/03/06 11:26:05 siva Exp $
*
* Description: This file contains the routines for the protocol Database Access for the module Msdp 
*********************************************************************/

#include "msdpinc.h"
#include "msdpcli.h"

/****************************************************************************
 Function    :  MsdpTestAllFsMsdpPeerTable
 Input       :  pu4ErrorCode
                pMsdpSetFsMsdpPeerEntry
                pMsdpIsSetFsMsdpPeerEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
MsdpTestAllFsMsdpPeerTable (UINT4 *pu4ErrorCode,
                            tMsdpFsMsdpPeerEntry * pMsdpSetFsMsdpPeerEntry,
                            tMsdpIsSetFsMsdpPeerEntry *
                            pMsdpIsSetFsMsdpPeerEntry, INT4 i4RowStatusLogic,
                            INT4 i4RowCreateOption)
{
    tMsdpFsMsdpPeerEntry *pPeer = NULL;
    INT4                i4IfIndex = 0;
    INT4                i4FsMsdpPeerCount = 0;
    INT4                i4FsMsdpMaxPeerSessions = 0;
    INT4                i4FsMsdpIPv4AdminStat = 0;
    INT4                i4FsMsdpIPv6AdminStat = 0;

    MsdpGetFsMsdpIPv4AdminStat (&i4FsMsdpIPv4AdminStat);
    MsdpGetFsMsdpIPv6AdminStat (&i4FsMsdpIPv6AdminStat);

    if (((pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType ==
          IPVX_ADDR_FMLY_IPV4) && (i4FsMsdpIPv4AdminStat != MSDP_ENABLED)) ||
        ((pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType ==
          IPVX_ADDR_FMLY_IPV6) && (i4FsMsdpIPv6AdminStat != MSDP_ENABLED)))
    {
        CLI_SET_ERR (MSDP_ERROR);
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return OSIX_FAILURE;
    }

    pPeer = MsdpGetFsMsdpPeerTable (pMsdpSetFsMsdpPeerEntry);

    if ((pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerStatus == OSIX_FALSE) &&
        (pPeer == NULL))
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        CLI_SET_ERR (PEER_NOT_EXIST);
        return OSIX_FAILURE;
    }

    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerStatus != OSIX_FALSE)
    {
        switch (pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus)
        {
            case CREATE_AND_WAIT:
            case CREATE_AND_GO:

                if (pPeer != NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
                    CLI_SET_ERR (PEER_ALREADY_EXISTS);
                    return OSIX_FAILURE;
                }

                MsdpGetFsMsdpMaxPeerSessions (&i4FsMsdpMaxPeerSessions);
                MsdpGetFsMsdpPeerCount (&i4FsMsdpPeerCount);
                if ((MemGetFreeUnits (MSDP_FSMSDPPEERTABLE_POOLID) == 0) ||
                    (i4FsMsdpPeerCount >= i4FsMsdpMaxPeerSessions))
                {
                    CLI_SET_ERR (PEERS_COUNT_EXCEEDED);
                    *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
                    return OSIX_FAILURE;
                }
                break;

            case DESTROY:

            case ACTIVE:

            case NOT_IN_SERVICE:

            case NOT_READY:

                if (pPeer == NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
                    CLI_SET_ERR (PEER_NOT_EXIST);
                    return OSIX_FAILURE;
                }

                if (pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus ==
                    pPeer->MibObject.i4FsMsdpPeerStatus)
                {
                    CLI_SET_ERR (PEER_EXISTS);
                    *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
                    return OSIX_FAILURE;
                }
                break;
            default:
                break;

        }

    }

    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerLocalAddress != OSIX_FALSE)
    {
        /*Fill your check condition */
        i4IfIndex = MsdpUtilIsValidLocalAddress
            (pMsdpSetFsMsdpPeerEntry->MibObject.au1FsMsdpPeerLocalAddress,
             pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType);
        if (MSDP_INVALID == i4IfIndex)
        {
            *pu4ErrorCode = SNMP_ERR_BAD_VALUE;
            CLI_SET_ERR (IP_INVALID);
            return OSIX_FAILURE;
        }
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerConnectRetryInterval !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pMsdpSetFsMsdpPeerEntry->MibObject.
             i4FsMsdpPeerConnectRetryInterval < 1) ||
            (pMsdpSetFsMsdpPeerEntry->MibObject.
             i4FsMsdpPeerConnectRetryInterval > MSDP_MAX_CONNECT_RETRY))
        {
            CLI_SET_ERR (CONNECT_RETRY_INVALID);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerHoldTimeConfigured != OSIX_FALSE)
    {
        /*Fill your check condition */
        if (((pMsdpSetFsMsdpPeerEntry->MibObject.
              i4FsMsdpPeerHoldTimeConfigured != 0) &&
             (pMsdpSetFsMsdpPeerEntry->MibObject.
              i4FsMsdpPeerHoldTimeConfigured < 3)) ||
            (pMsdpSetFsMsdpPeerEntry->MibObject.
             i4FsMsdpPeerHoldTimeConfigured > MSDP_MAX_HOLDTIME))
        {
            CLI_SET_ERR (HOLDTIME_RANGE_INVALID);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerKeepAliveConfigured != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pMsdpSetFsMsdpPeerEntry->MibObject.
             i4FsMsdpPeerKeepAliveConfigured < 0) ||
            (pMsdpSetFsMsdpPeerEntry->MibObject.
             i4FsMsdpPeerKeepAliveConfigured > MSDP_MAX_KEEPALIVE))
        {
            CLI_SET_ERR (KEEPALIVE_RANGE_INVALID);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerDataTtl != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerDataTtl < 0) ||
            (pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerDataTtl >
             MSDP_MAX_DATA_TTL))
        {
            CLI_SET_ERR (DATA_TTL_INVALID);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerEncapsulationType != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pMsdpSetFsMsdpPeerEntry->MibObject.
             i4FsMsdpPeerEncapsulationType < 0) ||
            (pMsdpSetFsMsdpPeerEntry->MibObject.
             i4FsMsdpPeerEncapsulationType > 1))
        {
            CLI_SET_ERR (TYPE_INVALID);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerMD5AuthPassword != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerMD5AuthPwdStat != OSIX_FALSE)
    {
        /*Fill your check condition */

    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerAdminStatus != OSIX_FALSE)
    {
        if ((pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAdminStatus !=
             MSDP_PEER_ADMIN_ENABLE) &&
            (pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAdminStatus !=
             MSDP_PEER_ADMIN_DISABLE))
        {
            CLI_SET_ERR (MSDP_INVALID_PEER_STAT);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMsdpSetFsMsdpPeerEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpTestAllFsMsdpSACacheTable
 Input       :  pu4ErrorCode
                pMsdpSetFsMsdpSACacheEntry
                pMsdpIsSetFsMsdpSACacheEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
MsdpTestAllFsMsdpSACacheTable (UINT4 *pu4ErrorCode,
                               tMsdpFsMsdpSACacheEntry *
                               pMsdpSetFsMsdpSACacheEntry,
                               tMsdpIsSetFsMsdpSACacheEntry *
                               pMsdpIsSetFsMsdpSACacheEntry,
                               INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    if (pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMsdpSetFsMsdpSACacheEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpTestAllFsMsdpMeshGroupTable
 Input       :  pu4ErrorCode
                pMsdpSetFsMsdpMeshGroupEntry
                pMsdpIsSetFsMsdpMeshGroupEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
MsdpTestAllFsMsdpMeshGroupTable (UINT4 *pu4ErrorCode,
                                 tMsdpFsMsdpMeshGroupEntry *
                                 pMsdpSetFsMsdpMeshGroupEntry,
                                 tMsdpIsSetFsMsdpMeshGroupEntry *
                                 pMsdpIsSetFsMsdpMeshGroupEntry,
                                 INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tMsdpFsMsdpMeshGroupEntry *pMesh = NULL;
    INT4                i4AddrType = pMsdpSetFsMsdpMeshGroupEntry->MibObject.
        i4FsMsdpMeshGroupAddrType;
    INT4                i4FsMsdpIPv4AdminStat = 0;
    INT4                i4FsMsdpIPv6AdminStat = 0;

    MsdpGetFsMsdpIPv4AdminStat (&i4FsMsdpIPv4AdminStat);
    MsdpGetFsMsdpIPv6AdminStat (&i4FsMsdpIPv6AdminStat);

    if (((i4AddrType == IPVX_ADDR_FMLY_IPV4) &&
         (i4FsMsdpIPv4AdminStat != MSDP_ENABLED)) ||
        ((i4AddrType == IPVX_ADDR_FMLY_IPV6) &&
         (i4FsMsdpIPv6AdminStat != MSDP_ENABLED)))
    {
        CLI_SET_ERR (MSDP_ERROR);
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return OSIX_FAILURE;
    }

    pMesh = MsdpGetFsMsdpMeshGroupTable (pMsdpSetFsMsdpMeshGroupEntry);

    if ((pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupStatus == OSIX_FALSE)
        && (pMesh == NULL))
    {
        CLI_SET_ERR (MESH_NOT_EXIST);
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return OSIX_FAILURE;
    }

    if (pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupStatus != OSIX_FALSE)
    {
        switch (pMsdpSetFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupStatus)
        {
            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                if (pMesh != NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
                    CLI_SET_ERR (MESH_ALREADY_EXISTS);
                    return OSIX_FAILURE;
                }
                break;

            case DESTROY:

            case ACTIVE:

            case NOT_IN_SERVICE:

            case NOT_READY:

                if (pMesh == NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
                    CLI_SET_ERR (MESH_NOT_EXIST);
                    return OSIX_FAILURE;
                }

                if (pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                    i4FsMsdpMeshGroupStatus ==
                    pMesh->MibObject.i4FsMsdpMeshGroupStatus)
                {
                    CLI_SET_ERR (MESH_EXISTS);
                    *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
                    return OSIX_FAILURE;
                }
                break;
            default:
                break;

        }

    }
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpTestAllFsMsdpRPTable
 Input       :  pu4ErrorCode
                pMsdpSetFsMsdpRPEntry
                pMsdpIsSetFsMsdpRPEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
MsdpTestAllFsMsdpRPTable (UINT4 *pu4ErrorCode,
                          tMsdpFsMsdpRPEntry * pMsdpSetFsMsdpRPEntry,
                          tMsdpIsSetFsMsdpRPEntry * pMsdpIsSetFsMsdpRPEntry,
                          INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tMsdpFsMsdpRPEntry *pRp = NULL;
    INT4                i4AddrType = pMsdpSetFsMsdpRPEntry->MibObject.
        i4FsMsdpRPAddrType;
    INT4                i4FsMsdpIPv4AdminStat = 0;
    INT4                i4FsMsdpIPv6AdminStat = 0;
    INT4                i4IfIndex = 0;

    MsdpGetFsMsdpIPv4AdminStat (&i4FsMsdpIPv4AdminStat);
    MsdpGetFsMsdpIPv6AdminStat (&i4FsMsdpIPv6AdminStat);

    if (((i4AddrType == IPVX_ADDR_FMLY_IPV4) &&
         (i4FsMsdpIPv4AdminStat != MSDP_ENABLED)) ||
        ((i4AddrType == IPVX_ADDR_FMLY_IPV6) &&
         (i4FsMsdpIPv6AdminStat != MSDP_ENABLED)))
    {
        CLI_SET_ERR (MSDP_ERROR);
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return OSIX_FAILURE;
    }

    pRp = MsdpGetFsMsdpRPTable (pMsdpSetFsMsdpRPEntry);

    if ((pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPStatus == OSIX_FALSE) &&
        (pRp == NULL))
    {
        CLI_SET_ERR (RP_NOT_EXIST);
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return OSIX_FAILURE;
    }

    if (pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPStatus != OSIX_FALSE)
    {
        switch (pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus)
        {
            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                if (pRp != NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
                    CLI_SET_ERR (RP_ALREADY_EXISTS);
                    return OSIX_FAILURE;
                }
                break;

            case DESTROY:

            case ACTIVE:

            case NOT_IN_SERVICE:

            case NOT_READY:

                if (pRp == NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
                    CLI_SET_ERR (RP_NOT_EXIST);
                    return OSIX_FAILURE;
                }

                if (pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus ==
                    pRp->MibObject.i4FsMsdpRPStatus)
                {
                    CLI_SET_ERR (RP_EXISTS);
                    *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
                    return OSIX_FAILURE;
                }
                break;
            default:
                break;

        }

    }

    if (pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPAddress != OSIX_FALSE)
    {
        i4IfIndex = MsdpUtilIsValidLocalAddress
            (pMsdpSetFsMsdpRPEntry->MibObject.au1FsMsdpRPAddress,
             pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPAddrType);

        if ((MSDP_INVALID == i4IfIndex) &&
            (OSIX_FALSE == MsdpUtilIsAllZeros
             (pMsdpSetFsMsdpRPEntry->MibObject.au1FsMsdpRPAddress,
              pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPAddrType)))
        {
            *pu4ErrorCode = SNMP_ERR_BAD_VALUE;
            CLI_SET_ERR (IP_INVALID);
            return OSIX_FAILURE;
        }

    }

    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpTestAllFsMsdpPeerFilterTable
 Input       :  pu4ErrorCode
                pMsdpSetFsMsdpPeerFilterEntry
                pMsdpIsSetFsMsdpPeerFilterEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
MsdpTestAllFsMsdpPeerFilterTable (UINT4 *pu4ErrorCode,
                                  tMsdpFsMsdpPeerFilterEntry *
                                  pMsdpSetFsMsdpPeerFilterEntry,
                                  tMsdpIsSetFsMsdpPeerFilterEntry *
                                  pMsdpIsSetFsMsdpPeerFilterEntry,
                                  INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tMsdpFsMsdpPeerFilterEntry *pPeerFilter = NULL;
    INT4                i4AddrType = pMsdpSetFsMsdpPeerFilterEntry->MibObject.
        i4FsMsdpPeerFilterAddrType;
    INT4                i4FsMsdpIPv4AdminStat = 0;
    INT4                i4FsMsdpIPv6AdminStat = 0;

    MsdpGetFsMsdpIPv4AdminStat (&i4FsMsdpIPv4AdminStat);
    MsdpGetFsMsdpIPv6AdminStat (&i4FsMsdpIPv6AdminStat);

    if (((i4AddrType == IPVX_ADDR_FMLY_IPV4) &&
         (i4FsMsdpIPv4AdminStat != MSDP_ENABLED)) ||
        ((i4AddrType == IPVX_ADDR_FMLY_IPV6) &&
         (i4FsMsdpIPv6AdminStat != MSDP_ENABLED)))
    {
        CLI_SET_ERR (MSDP_ERROR);
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return OSIX_FAILURE;
    }

    pPeerFilter = MsdpGetFsMsdpPeerFilterTable (pMsdpSetFsMsdpPeerFilterEntry);
    if ((pPeerFilter == NULL) &&
        (pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterStatus ==
         OSIX_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        CLI_SET_ERR (ROW_NOT_EXIST);
        return OSIX_FAILURE;
    }

    if (pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
        switch (pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                i4FsMsdpPeerFilterStatus)
        {
            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                if (pPeerFilter != NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
                    CLI_SET_ERR (ROW_EXIST);
                    return OSIX_FAILURE;
                }
                break;

            case ACTIVE:

            case DESTROY:

            case NOT_IN_SERVICE:

            case NOT_READY:

                if (pPeerFilter == NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
                    CLI_SET_ERR (ROW_NOT_EXIST);
                    return OSIX_FAILURE;
                }

                if (pPeerFilter->MibObject.i4FsMsdpPeerFilterStatus ==
                    pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                    i4FsMsdpPeerFilterStatus)

                {
                    *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
                    CLI_SET_ERR (ROW_EXIST);
                    return OSIX_FAILURE;
                }
                break;
            default:
                break;

        }

    }
    if (pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterRouteMap !=
        OSIX_FALSE)
    {
        if (pMsdpSetFsMsdpPeerFilterEntry->MibObject.
            i4FsMsdpPeerFilterRouteMapLen > MSDP_MAX_ROUTEMAP_LEN)
        {
            *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
            CLI_SET_ERR (MSDP_INVALID_ROUTEMAP_NAME);
            return OSIX_FAILURE;
        }
    }
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpTestAllFsMsdpSARedistributionTable
 Input       :  pu4ErrorCode
                pMsdpSetFsMsdpSARedistributionEntry
                pMsdpIsSetFsMsdpSARedistributionEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
MsdpTestAllFsMsdpSARedistributionTable (UINT4 *pu4ErrorCode,
                                        tMsdpFsMsdpSARedistributionEntry *
                                        pMsdpSetFsMsdpSARedistributionEntry,
                                        tMsdpIsSetFsMsdpSARedistributionEntry *
                                        pMsdpIsSetFsMsdpSARedistributionEntry,
                                        INT4 i4RowStatusLogic,
                                        INT4 i4RowCreateOption)
{
    tMsdpFsMsdpSARedistributionEntry *pSaRedist = NULL;
    INT4                i4AddrType = pMsdpSetFsMsdpSARedistributionEntry->
        MibObject.i4FsMsdpSARedistributionAddrType;

    INT4                i4FsMsdpIPv4AdminStat = 0;
    INT4                i4FsMsdpIPv6AdminStat = 0;

    MsdpGetFsMsdpIPv4AdminStat (&i4FsMsdpIPv4AdminStat);
    MsdpGetFsMsdpIPv6AdminStat (&i4FsMsdpIPv6AdminStat);

    if (((i4AddrType == IPVX_ADDR_FMLY_IPV4) &&
         (i4FsMsdpIPv4AdminStat != MSDP_ENABLED)) ||
        ((i4AddrType == IPVX_ADDR_FMLY_IPV6) &&
         (i4FsMsdpIPv6AdminStat != MSDP_ENABLED)))
    {
        CLI_SET_ERR (MSDP_ERROR);
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return OSIX_FAILURE;
    }

    pSaRedist = MsdpGetFsMsdpSARedistributionTable
        (pMsdpSetFsMsdpSARedistributionEntry);
    if ((pMsdpIsSetFsMsdpSARedistributionEntry->bFsMsdpSARedistributionStatus ==
         OSIX_FALSE) && (pSaRedist == NULL))
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        CLI_SET_ERR (REDIST_NOT_EXIST);
        return OSIX_FAILURE;
    }
    if (pMsdpIsSetFsMsdpSARedistributionEntry->bFsMsdpSARedistributionStatus !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
        switch (pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                i4FsMsdpSARedistributionStatus)
        {
            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                if (pSaRedist != NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
                    CLI_SET_ERR (ROW_EXIST);
                    return OSIX_FAILURE;
                }
                break;

            case ACTIVE:

            case DESTROY:

            case NOT_IN_SERVICE:

            case NOT_READY:

                if (pSaRedist == NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
                    CLI_SET_ERR (ROW_NOT_EXIST);
                    return OSIX_FAILURE;
                }

                if (pSaRedist->MibObject.i4FsMsdpSARedistributionStatus ==
                    pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                    i4FsMsdpSARedistributionStatus)
                {
                    *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
                    CLI_SET_ERR (ROW_EXIST);
                    return OSIX_FAILURE;
                }
                break;
            default:
                break;
        }

    }
    if (pMsdpIsSetFsMsdpSARedistributionEntry->
        bFsMsdpSARedistributionRouteMap != OSIX_FALSE)
    {
        if (pMsdpSetFsMsdpSARedistributionEntry->MibObject.
            i4FsMsdpSARedistributionRouteMapLen > MSDP_MAX_ROUTEMAP_LEN)
        {
            *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
            CLI_SET_ERR (MSDP_INVALID_ROUTEMAP_NAME);
            return OSIX_FAILURE;
        }
    }
    if (pMsdpIsSetFsMsdpSARedistributionEntry->
        bFsMsdpSARedistributionRouteMapStat != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  MsdpTestFsMsdpTraceLevel
 Input       :  pu4ErrorCode
                i4FsMsdpTraceLevel
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpTestFsMsdpTraceLevel (UINT4 *pu4ErrorCode, INT4 i4FsMsdpTraceLevel)
{
    INT4                i4FsMsdpIPv4AdminStat = 0;
    INT4                i4FsMsdpIPv6AdminStat = 0;

    MsdpGetFsMsdpIPv4AdminStat (&i4FsMsdpIPv4AdminStat);
    MsdpGetFsMsdpIPv6AdminStat (&i4FsMsdpIPv6AdminStat);
    if ((i4FsMsdpIPv4AdminStat != MSDP_ENABLED) &&
        (i4FsMsdpIPv6AdminStat != MSDP_ENABLED))
    {
        CLI_SET_ERR (MSDP_ERROR);
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (i4FsMsdpTraceLevel);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  MsdpTestFsMsdpIPv4AdminStat
 Input       :  pu4ErrorCode
                i4FsMsdpIPv4AdminStat
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpTestFsMsdpIPv4AdminStat (UINT4 *pu4ErrorCode, INT4 i4FsMsdpIPv4AdminStat)
{
    INT4                i4MsdpCurAdminStat = 0;

    MsdpGetFsMsdpIPv4AdminStat (&i4MsdpCurAdminStat);

    if ((i4FsMsdpIPv4AdminStat > MSDP_ENABLED) ||
        (i4FsMsdpIPv4AdminStat < MSDP_DISABLED))
    {
        CLI_SET_ERR (MSDP_STATUS_INVALID);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpTestFsMsdpIPv6AdminStat
 Input       :  pu4ErrorCode
                i4FsMsdpIPv6AdminStat
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpTestFsMsdpIPv6AdminStat (UINT4 *pu4ErrorCode, INT4 i4FsMsdpIPv6AdminStat)
{
    INT4                i4MsdpCurAdminStat = 0;

    MsdpGetFsMsdpIPv6AdminStat (&i4MsdpCurAdminStat);

    if ((i4FsMsdpIPv6AdminStat > MSDP_ENABLED) ||
        (i4FsMsdpIPv6AdminStat < MSDP_DISABLED))
    {
        CLI_SET_ERR (MSDP_STATUS_INVALID);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpTestFsMsdpCacheLifetime
 Input       :  pu4ErrorCode
                u4FsMsdpCacheLifetime
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpTestFsMsdpCacheLifetime (UINT4 *pu4ErrorCode, UINT4 u4FsMsdpCacheLifetime)
{
    INT4                i4FsMsdpIPv4AdminStat = 0;
    INT4                i4FsMsdpIPv6AdminStat = 0;

    MsdpGetFsMsdpIPv4AdminStat (&i4FsMsdpIPv4AdminStat);
    MsdpGetFsMsdpIPv6AdminStat (&i4FsMsdpIPv6AdminStat);

    if ((i4FsMsdpIPv4AdminStat != MSDP_ENABLED) &&
        (i4FsMsdpIPv6AdminStat != MSDP_ENABLED))
    {
        CLI_SET_ERR (MSDP_ERROR);
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return OSIX_FAILURE;
    }

    if ((u4FsMsdpCacheLifetime < MSDP_MIN_CACHELIFETIME) &&
        (u4FsMsdpCacheLifetime != 0))
    {
        CLI_SET_ERR (CACHE_LIFETIME_INVALID);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpTestFsMsdpMaxPeerSessions
 Input       :  pu4ErrorCode
                i4FsMsdpMaxPeerSessions
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpTestFsMsdpMaxPeerSessions (UINT4 *pu4ErrorCode,
                               INT4 i4FsMsdpMaxPeerSessions)
{
    INT4                i4FsMsdpIPv4AdminStat = 0;
    INT4                i4FsMsdpIPv6AdminStat = 0;

    MsdpGetFsMsdpIPv4AdminStat (&i4FsMsdpIPv4AdminStat);
    MsdpGetFsMsdpIPv6AdminStat (&i4FsMsdpIPv6AdminStat);

    if ((i4FsMsdpIPv4AdminStat != MSDP_ENABLED) &&
        (i4FsMsdpIPv6AdminStat != MSDP_ENABLED))
    {
        CLI_SET_ERR (MSDP_ERROR);
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return OSIX_FAILURE;
    }
    if ((i4FsMsdpMaxPeerSessions < 1) ||
        (i4FsMsdpMaxPeerSessions > MSDP_MAX_PEER_SESSIONS))
    {
        CLI_SET_ERR (MAX_SESSIONS_INVALID);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpTestFsMsdpMappingComponentId
 Input       :  pu4ErrorCode
                i4FsMsdpMappingComponentId
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpTestFsMsdpMappingComponentId (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMsdpMappingComponentId)
{

    INT4                i4FsMsdpIPv4AdminStat = 0;
    INT4                i4FsMsdpIPv6AdminStat = 0;

    MsdpGetFsMsdpIPv4AdminStat (&i4FsMsdpIPv4AdminStat);
    MsdpGetFsMsdpIPv6AdminStat (&i4FsMsdpIPv6AdminStat);

    if ((i4FsMsdpIPv4AdminStat != MSDP_ENABLED) &&
        (i4FsMsdpIPv6AdminStat != MSDP_ENABLED))
    {
        CLI_SET_ERR (MSDP_ERROR);
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return OSIX_FAILURE;
    }

    if ((i4FsMsdpMappingComponentId < 0) ||
        (i4FsMsdpMappingComponentId > MSDP_MAX_MRP_COMPONENT))
    {
        CLI_SET_ERR (RANGE_INVALID);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpTestFsMsdpListenerPort
 Input       :  pu4ErrorCode
                i4FsMsdpListenerPort
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpTestFsMsdpListenerPort (UINT4 *pu4ErrorCode, INT4 i4FsMsdpListenerPort)
{

    INT4                i4FsMsdpIPv4AdminStat = 0;
    INT4                i4FsMsdpIPv6AdminStat = 0;

    MsdpGetFsMsdpIPv4AdminStat (&i4FsMsdpIPv4AdminStat);
    MsdpGetFsMsdpIPv6AdminStat (&i4FsMsdpIPv6AdminStat);

    if ((i4FsMsdpIPv4AdminStat != MSDP_ENABLED) &&
        (i4FsMsdpIPv6AdminStat != MSDP_ENABLED))
    {
        CLI_SET_ERR (MSDP_ERROR);
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return OSIX_FAILURE;
    }

    if (((i4FsMsdpListenerPort < MSDP_MIN_LISTENER_PORT) &&
         (i4FsMsdpListenerPort != MSDP_DEF_LISTENER_PORT)) ||
        (i4FsMsdpListenerPort > MSDP_MAX_LISTENER_PORT))
    {
        CLI_SET_ERR (RANGE_INVALID);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpTestFsMsdpPeerFilter
 Input       :  pu4ErrorCode
                i4FsMsdpPeerFilter
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpTestFsMsdpPeerFilter (UINT4 *pu4ErrorCode, INT4 i4FsMsdpPeerFilter)
{

    INT4                i4FsMsdpIPv4AdminStat = 0;
    INT4                i4FsMsdpIPv6AdminStat = 0;

    MsdpGetFsMsdpIPv4AdminStat (&i4FsMsdpIPv4AdminStat);
    MsdpGetFsMsdpIPv6AdminStat (&i4FsMsdpIPv6AdminStat);

    if ((i4FsMsdpIPv4AdminStat != MSDP_ENABLED) &&
        (i4FsMsdpIPv6AdminStat != MSDP_ENABLED))
    {
        CLI_SET_ERR (MSDP_ERROR);
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return OSIX_FAILURE;
    }

    if ((i4FsMsdpPeerFilter > MSDP_ACCEPT_ALL) ||
        (i4FsMsdpPeerFilter < MSDP_DENY_ALL))
    {
        CLI_SET_ERR (FILTER_ERROR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (i4FsMsdpPeerFilter);
    return OSIX_SUCCESS;
}
