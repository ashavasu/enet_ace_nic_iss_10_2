/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: msdcache.c,v 1.11 2014/03/14 12:56:29 siva Exp $
*
* Description: This file contains the Msdp CACHE module related routines 
*********************************************************************/
#include "msdpinc.h"

/****************************************************************************
 * FUNCTION NAME : MsdCacheFormEntry 
 *
 * DESCRIPTION   : This function forms the cache Entry to update the MSDP 
 *                 cache Table.
 *
 * INPUTS        : u4DataPktCnt- Data packet count
 *                 pSaCache - Pointer to MSDP Cache Entry
 *                 pMsdpInCache - Input Cache Info
 *
 * OUTPUTS       : pSaCache- SACacheUpTime, SACacheExpiryTime,  
 *                  SACachePeerLearnedFrom, SACacheInSAs and 
 *                  SACacheInDataPackets fields are updated
 *
 *
 * RETURNS       : OSIX_SUCCESS always
 *
 ****************************************************************************/

INT4
MsdCacheFormEntry (UINT4 u4DataPktCnt, tMsdpFsMsdpSACacheEntry * pSaCache,
                   tMsdpCacheUpdt * pMsdpInCache)
{
    tOsixSysTime        SysTime;
    UINT4               u4CacheLifeTime = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_ENTRY,
                    "\nENTRY: MsdCacheFormEntry"));

    OsixGetSysTime (&SysTime);
    pSaCache->MibObject.u4FsMsdpSACacheUpTime = (UINT4) SysTime;
    pSaCache->MibObject.u4FsMsdpSACacheExpiryTime = 0;

    if (pMsdpInCache->i1Flag != MSDP_PIM_CACHE)
    {
        MsdpGetFsMsdpCacheLifetime (&u4CacheLifeTime);
        pSaCache->MibObject.u4FsMsdpSACacheExpiryTime =
            (u4CacheLifeTime + pSaCache->MibObject.u4FsMsdpSACacheUpTime);

        if (pMsdpInCache->i1Flag == MSDP_SA_ADVT_CACHE)
        {
            MEMCPY (&pSaCache->MibObject.au1FsMsdpSACacheRPFPeer,
                    pMsdpInCache->pLrndFrmPeer->MibObject.
                    au1FsMsdpPeerRemoteAddress,
                    pMsdpInCache->GrpAddr.u1AddrLen);
        }

        MEMCPY (&pSaCache->MibObject.au1FsMsdpSACachePeerLearnedFrom,
                pMsdpInCache->pLrndFrmPeer->MibObject.
                au1FsMsdpPeerRemoteAddress, pMsdpInCache->GrpAddr.u1AddrLen);
    }
    pSaCache->MibObject.u4FsMsdpSACacheInSAs++;
    pSaCache->MibObject.u4FsMsdpSACacheInDataPackets += u4DataPktCnt;

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                    "\nEXIT: MsdCacheFormEntry"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdCacheUpdateCacheTbl
 *
 * DESCRIPTION   : This function updates the cache table
 *
 * INPUTS        : MsdpInCache- MSDP Cache Info to be updated th Cache Table
 *                 u4DataPktCnt- Data packet count
 *
 * OUTPUTS       : pu1Status- Cache updation Status it can be DELETED, UPDATED
 *
 * RETURNS       : OSIX_SUCCESS always
 *
 ****************************************************************************/
INT4
MsdCacheUpdateCacheTbl (tMsdpCacheUpdt MsdpInCache, UINT4 u4DataPktCnt,
                        UINT1 *pu1Status)
{
    tMsdpFsMsdpSACacheEntry CurSaCache;
    tMsdpFsMsdpSACacheEntry *pOldSaCache = NULL;
    UINT4               u4CacheLifeTime = 0;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT1               u1AddrLen = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_ENTRY,
                    "\nENTRY: MsdCacheUpdateCacheTbl"));

    MsdpGetFsMsdpCacheLifetime (&u4CacheLifeTime);

    /* If the CacheLiftTime = 0, Caching should not be done, so return from here.
     * But the SA entry can be forwarded, so make the status as MSDP_CACHE_NEW */

    if ((u4CacheLifeTime == 0) && (MsdpInCache.u1Action == MSDP_ADD_CACHE))
    {
        *pu1Status = MSDP_CACHE_NEW;
        return i4RetVal;
    }

    MEMSET (&CurSaCache, 0, sizeof (tMsdpFsMsdpSACacheEntry));

    /* While adding in RB tree, length should be always 16 for both
     * IPV4 and IPv6 addresses.Since the Address MibObject is of fixed
     * length in MIB*/
    MsdpInCache.GrpAddr.u1AddrLen = IPVX_MAX_INET_ADDR_LEN;

    u1AddrLen = MsdpInCache.GrpAddr.u1AddrLen;
    CurSaCache.MibObject.i4FsMsdpSACacheAddrType = MsdpInCache.u1AddrType;

    CurSaCache.MibObject.i4FsMsdpSACacheGroupAddrLen = u1AddrLen;
    CurSaCache.MibObject.i4FsMsdpSACacheSourceAddrLen = u1AddrLen;
    CurSaCache.MibObject.i4FsMsdpSACacheOriginRPLen = u1AddrLen;

    MEMCPY (&CurSaCache.MibObject.au1FsMsdpSACacheGroupAddr,
            MsdpInCache.GrpAddr.au1Addr, u1AddrLen);

    MEMCPY (&CurSaCache.MibObject.au1FsMsdpSACacheSourceAddr,
            MsdpInCache.SrcAddr.au1Addr, u1AddrLen);

    MEMCPY (&CurSaCache.MibObject.au1FsMsdpSACacheOriginRP,
            MsdpInCache.OriginatorRP.au1Addr, u1AddrLen);

    pOldSaCache = MsdpGetFsMsdpSACacheTable (&CurSaCache);
    if (pOldSaCache != NULL)
    {
        if ((MsdpInCache.i1Flag == MSDP_PIM_CACHE) &&
            (MsdpInCache.u1Action == MSDP_ADD_CACHE))
        {
            *pu1Status = MSDP_CACHE_REFRESH;
            MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                            "\nEXIT: MsdCacheUpdateCacheTbl"));
            return OSIX_SUCCESS;
        }
    }

    if (MsdpInCache.u1Action == MSDP_DELETE_CACHE)
    {
        MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheUpdateCacheTbl: "
                        "MsdpInCache.u1Action is MSDP_DELETE_CACHE"));
        *pu1Status = MSDP_CACHE_DELETED;
        i4RetVal = OSIX_FAILURE;
        if (pOldSaCache != NULL)
        {
            MsdpTmrStopTmr (&pOldSaCache->CacheSaStateTimer);
            i4RetVal = MsdpUtilRemoveCacheEntry (pOldSaCache);
        }
        MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                        "\nEXIT : MsdCacheUpdateCacheTbl."));
        return i4RetVal;
    }
    else
    {
        MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheUpdateCacheTbl: "
                        "MsdpInCache.u1Action is MSDP_ADD_CACHE"));
        if (pOldSaCache)
        {
            *pu1Status = MSDP_CACHE_REFRESH;

            MsdCacheFormEntry (u4DataPktCnt, pOldSaCache, &MsdpInCache);
        }
        else
        {
            *pu1Status = MSDP_CACHE_NEW;
            CurSaCache.MibObject.i4FsMsdpSACachePeerLearnedFromLen =
                MsdpInCache.GrpAddr.u1AddrLen;
            CurSaCache.MibObject.i4FsMsdpSACacheRPFPeerLen = u1AddrLen;
            CurSaCache.MibObject.u4FsMsdpSACacheInSAs = 0;
            CurSaCache.MibObject.u4FsMsdpSACacheInDataPackets = 0;

            MsdCacheFormEntry (u4DataPktCnt, &CurSaCache, &MsdpInCache);
            pOldSaCache = MsdpFsMsdpSACacheTableCreateApi (&CurSaCache);
            if (NULL == pOldSaCache)
            {
                MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheUpdateCacheTbl: "
                                "Adding New Cache in Cache Table failed"));
                i4RetVal = OSIX_FAILURE;
            }
            else
            {
                MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                                "\nEXIT: MsdCacheUpdateCacheTbl"));
            }

            gMsdpGlobals.MsdpGlbMib.u4FsMsdpNumSACacheEntries++;
        }

        if ((i4RetVal != OSIX_FAILURE) &&
            (MsdpInCache.i1Flag != MSDP_PIM_CACHE))
        {
            MsdpTmrStartTmr (&pOldSaCache->CacheSaStateTimer,
                             MSDP_CACHE_STATE_TMR, u4CacheLifeTime);
        }
    }
    return i4RetVal;
}

/****************************************************************************
 * FUNCTION NAME : MsdCacheApplySAFilter
 *
 * DESCRIPTION   : This function applies SA filter on given CacheEntry 
 *
 * INPUTS        : pMsdpCacheEntry- MSDP cache entry
 *
 * OUTPUTS       : None
 *
 * RETURNS       : OSIX_SUCCESS or OSIX_FAILURE
 *
 ****************************************************************************/
INT4
MsdCacheApplySAFilter (tMsdpCacheUpdt * pMsdpCacheEntry)
{
    tMsdpFsMsdpSARedistributionEntry SAFilter;
    tMsdpFsMsdpSARedistributionEntry *pSAFilter;
    INT4                i4RetVal = OSIX_SUCCESS;

    MEMSET (&SAFilter, 0, sizeof (tMsdpFsMsdpSARedistributionEntry));
    SAFilter.MibObject.i4FsMsdpSARedistributionAddrType =
        pMsdpCacheEntry->u1AddrType;
    pSAFilter = MsdpGetFsMsdpSARedistributionTable (&SAFilter);

    if ((pSAFilter != NULL) &&
        (pSAFilter->MibObject.i4FsMsdpSARedistributionRouteMapStat ==
         MSDP_ENABLED))
    {
        i4RetVal = MsdpPortSARtMapRule
            (&pMsdpCacheEntry->SrcAddr, &pMsdpCacheEntry->GrpAddr,
             pSAFilter->MibObject.au1FsMsdpSARedistributionRouteMap);
    }
    return i4RetVal;
}

/****************************************************************************
 * FUNCTION NAME : MsdCacheFormOriginSATlv
 *
 * DESCRIPTION   : This function forms SA Advertisement TLV from the Cache 
 *                 information provided
 *
 * INPUTS        : pCache- SA Cache Information to form SA TLV 
 *                 pBuffer- Buffer in which TLV is formed
 *                 u2DataPktLen- Data packet length
 *                   
 * OUTPUTS       : pu2OutPktLen- Out packet length, ie number of bytes
 *                 copied in pBuffer
 *                 gau1LinBuf- Global linear buffer
 *
 * RETURNS       : OSIX_SUCCESS always
 *
 ****************************************************************************/
INT4 
     
     
     
     
     
     
     
    MsdCacheFormOriginSATlv
    (tMsdpCacheUpdt * pCache, tCRU_BUF_CHAIN_HEADER * pBuffer,
     UINT2 u2DataPktLen, UINT2 *pu2OutPktLen)
{
    INT4                i4AddrLen = 0;
    INT4                i4Index = 0;
    UINT2               u2SAPktLen = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_ENTRY,
                    "\nENTRY: MsdCacheFormOriginSATlv"));

    if (pCache->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        i4AddrLen = IPVX_IPV4_ADDR_LEN;
        gau1LinBuf[i4Index] = (UINT1) MSDP_TLV_SA;
    }
    else
    {
        i4AddrLen = IPVX_IPV6_ADDR_LEN;
        gau1LinBuf[i4Index] = (UINT1) MSDP6_TLV_SA;
    }
    /* Type field */
    i4Index += 1;

    /* Length field,  2 byte length to be filled at the end */
    i4Index += MSDP_TWO;

    /* Entry Count field */
    gau1LinBuf[i4Index] = 1;
    i4Index += 1;

    /* Origin RP field */
    MEMCPY (&gau1LinBuf[i4Index], pCache->OriginatorRP.au1Addr, i4AddrLen);
    i4Index += i4AddrLen;

    /* Reserved fields, Move 3 bytes for reserved fields */
    i4Index += MSDP_THREE;

    /* Address Prefix field */
    gau1LinBuf[i4Index] = (UINT1) (i4AddrLen * MSDP_OCTET_IN_BITS);
    i4Index += 1;

    /* Grp Addr field */
    MEMCPY (&gau1LinBuf[i4Index], pCache->GrpAddr.au1Addr, i4AddrLen);
    i4Index += i4AddrLen;

    /* Src Addr field */
    MEMCPY (&gau1LinBuf[i4Index], pCache->SrcAddr.au1Addr, i4AddrLen);
    i4Index += i4AddrLen;

    *pu2OutPktLen = (UINT2) i4Index;

    /* SG Entry Len+ DataLen should be < MSDP_PEER_MAX_TEMP_TX_BUF_LEN */
    if ((pBuffer != NULL) &&
        (MSDP_PEER_MAX_TEMP_TX_BUF_LEN - i4Index) > u2DataPktLen)
    {
        /* Copy the data */
        CRU_BUF_Copy_FromBufChain (pBuffer, &gau1LinBuf[i4Index], 0,
                                   u2DataPktLen);
        *pu2OutPktLen = (UINT2) (i4Index + u2DataPktLen);
    }

    u2SAPktLen = OSIX_HTONS (*pu2OutPktLen);
    MEMCPY (&gau1LinBuf[1], (UINT1 *) &u2SAPktLen, sizeof (UINT2));

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                    "\nEXIT: MsdCacheFormOriginSATlv"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdCacheOriginateSAMsg
 *
 * DESCRIPTION   : This function called to form a SA Message
 *
 * INPUTS        : pCache- Cache infor to form SA message
 *                 u2DataLen- Data length
 *
 * OUTPUTS       : pDataBuf - Buffer filled with SA message
 *
 * RETURNS       : OSIX_SUCCESS when MsdCacheFormOriginSaTlv returns success
 *                 OSIX_FAILURE otherwise
 *
 ****************************************************************************/
INT4
MsdCacheOriginateSAMsg (tMsdpCacheUpdt * pCache,
                        tCRU_BUF_CHAIN_HEADER * pDataBuf, UINT2 u2DataLen)
{
    INT4                i4RetVal = 0;
    UINT2               u2BytesProcessed = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_ENTRY,
                    "\nENTRY: MsdCacheOriginateSAMsg"));

    i4RetVal = MsdCacheFormOriginSATlv (pCache, pDataBuf, u2DataLen,
                                        &u2BytesProcessed);

    MsdpOutFwdSAPkt (NULL, gau1LinBuf, pCache->u1AddrType, 0, u2BytesProcessed,
                     MSDP_PEER_MAX_TEMP_TX_BUF_LEN);

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                    "\nEXIT: MsdCacheOriginateSAMsg"));

    return i4RetVal;
}

/****************************************************************************
 * FUNCTION NAME : MsdCacheAmIOriginator
 *
 * DESCRIPTION   : This function finds whether this MSDP router can fwd the
 *                originated SA messages to the MSDP Peer based on Originator
 *                RP configured. If the Originator RP address is 0.0.0.0, this
 *                function will not allow to fwd the packet
 *
 * INPUTS        : u1AddrType- Address type
 *                 i4AddrLen- Address length
 *                 pu1Rp - Pointer to Originator RP address configured
 *
 * OUTPUTS       : pu1Rp - Orinator RP address configured
 *
 * RETURNS       : OSIX_TRUE if the packet can be fwded
 *                 OSIX_FALSE otherwise
 *
 ****************************************************************************/
INT4
MsdCacheAmIOriginator (UINT1 u1AddrType, INT4 i4AddrLen, UINT1 *pu1Rp)
{
    tMsdpFsMsdpRPEntry *pRpEntry = NULL;
    tMsdpFsMsdpRPEntry  RpEntry;
    INT4                i4RetVal = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_ENTRY,
                    "\nENTRY: MsdCacheAmIOriginator"));

    MEMSET (&RpEntry, 0, sizeof (RpEntry));
    RpEntry.MibObject.i4FsMsdpRPAddrType = u1AddrType;
    pRpEntry = MsdpGetFsMsdpRPTable (&RpEntry);
    if (pRpEntry == NULL)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheAmIOriginator: "
                        "MsdpGetFsMsdpRPTable returns NULL"));

        return OSIX_FALSE;
    }

    i4RetVal = MsdpUtilIsAllZeros (pRpEntry->MibObject.au1FsMsdpRPAddress,
                                   pRpEntry->MibObject.i4FsMsdpRPAddrType);
    if (OSIX_TRUE == i4RetVal)
    {
        /*This router is not an originator.So dont forward the packet */

        MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheAmIOriginator: "
                        "Pkt cannot be fwded, Originator RP not configured"));

        MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                        "\nEXIT: MsdCacheAmIOriginator"));
        return OSIX_FALSE;
    }
    if (pRpEntry->MibObject.i4FsMsdpRPOperStatus != MSDP_IF_OPERSTATUS_UP)
    {
        /** Interface configured as MSDP originator is OPERDOWN.
            So Dont forward the packet***/

        MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheAmIOriginator: "
                        "Pkt cannot be fwded, RPOperStatus is not Up"));

        MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                        "\nEXIT: MsdCacheAmIOriginator"));
        return OSIX_FALSE;
    }
    /*copies the RP Address */
    MEMCPY (pu1Rp, pRpEntry->MibObject.au1FsMsdpRPAddress, i4AddrLen);

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheAmIOriginator: "
                    "Copies the RP Address"));

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                    "\nEXIT: MsdCacheAmIOriginator"));

    return OSIX_TRUE;
}

/****************************************************************************
 * FUNCTION NAME : MsdCacheSendSrcInfoRequest
 *
 * DESCRIPTION   : This function sends the source info request to all MSDP
 *                 Peers
 *
 * INPUTS        : pReqGrpAddr- Requested group address
 *
 * OUTPUTS       : gau1LinBuf- Global linear buffer
 *
 * RETURNS       : OSIX_SUCCESS always
 *
 ****************************************************************************/
INT4
MsdCacheSendSARequest (tMsdpSAReqGrp * pReqGrpAddr)
{
    INT4                i4Index = 0;
    INT4                i4AddrLen = 0;
    UINT2               u2MsdpLen = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_ENTRY,
                    "\nENTRY: MsdCacheSendSARequest"));

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE,
                    "\nMsdCacheSendSARequest: Src Info Req Rcvd for %s",
                    MsdpPrintIPvxAddress (pReqGrpAddr->GrpAddr)));

    if (pReqGrpAddr->GrpAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        i4AddrLen = IPVX_IPV4_ADDR_LEN;
        gau1LinBuf[i4Index] = (UINT1) MSDP_TLV_SA_REQUEST;
        u2MsdpLen = MSDP_SA_REQUEST_TLV_LEN;
    }
    else
    {
        i4AddrLen = IPVX_IPV6_ADDR_LEN;
        gau1LinBuf[i4Index] = (UINT1) MSDP6_TLV_SA_REQUEST;
        u2MsdpLen = MSDP6_SA_REQUEST_TLV_LEN;
    }
    i4Index += sizeof (UINT1);

    u2MsdpLen = OSIX_HTONS (u2MsdpLen);
    MEMCPY (&gau1LinBuf[i4Index], (UINT1 *) &u2MsdpLen, sizeof (UINT2));
    i4Index += sizeof (UINT2);

    gau1LinBuf[i4Index] = 0;
    i4Index += sizeof (UINT1);

    MEMCPY (&gau1LinBuf[i4Index], pReqGrpAddr->GrpAddr.au1Addr, i4AddrLen);
    i4Index += i4AddrLen;

    MsdpOutFwdSAPkt (NULL, gau1LinBuf, pReqGrpAddr->GrpAddr.u1Afi, 0,
                     (UINT2) i4Index, (UINT2) MSDP_PEER_MAX_TEMP_TX_BUF_LEN);

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                    "\nMsdCacheSendSARequest: Fwded SrcInfo Req to all MSDP"
                    " Peers"));

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                    "\nEXIT: MsdCacheSendSARequest"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdCacheFormSAHdr
 *
 * DESCRIPTION   : This function forms the SA header
 *
 * INPUTS        : u1TlvType- Tlv Type
 *                 i4AddrLen- Address length
 *                 u1EntryCount- Entry count
 *                 pu1RpAddr- RP address
 *
 * OUTPUTS       : gau1LinBuf- Global linear buffer
 *
 * RETURNS       : OSIX_SUCCESS always
 *
 ****************************************************************************/
INT4
MsdCacheFormSAHdr (UINT1 u1TlvType, INT4 i4AddrLen,
                   UINT1 u1EntryCount, UINT1 *pu1RpAddr)
{
    INT4                i4Offset = 0;
    UINT2               u2MsdpLen = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_ENTRY,
                    "\nENTRY: MsdCacheFormSAHdr"));

    gau1LinBuf[i4Offset] = u1TlvType;
    i4Offset += 1;

    u2MsdpLen = (UINT2) MSDP_SA_RESPONSE_TLV_LEN (u1EntryCount, i4AddrLen);
    u2MsdpLen = OSIX_HTONS (u2MsdpLen);
    MEMCPY (&gau1LinBuf[i4Offset], (UINT1 *) &u2MsdpLen, sizeof (UINT2));
    i4Offset += MSDP_TWO;

    MEMCPY (&gau1LinBuf[i4Offset], &u1EntryCount, 1);

    i4Offset += MSDP_ONE;
    MEMCPY (&gau1LinBuf[i4Offset], pu1RpAddr, i4AddrLen);

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                    "\nEXIT: MsdCacheFormSAHdr"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdCacheFormSAPkt
 *
 * DESCRIPTION   : This function forms the SA packet
 *
 * INPUTS        : pSaCache- SA cache entry
 *                 pi4Offset- Offset
 *
 * OUTPUTS       : pi4Offset- Offset of gau1LinBuf
 *                 pu1EntryCount- Entry count
 *                 gau1LinBuf- Global linear buffer
 *
 * RETURNS       : OSIX_SUCCESS always
 *
 ****************************************************************************/
INT4
MsdCacheFormSAPkt (tMsdpFsMsdpSACacheEntry * pSaCache,
                   INT4 *pi4Offset, UINT1 *pu1EntryCount)
{
    INT4                i4AddrLen = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_ENTRY,
                    "\nENTRY: MsdCacheFormSAPkt"));

    i4AddrLen = (pSaCache->MibObject.i4FsMsdpSACacheAddrType ==
                 IPVX_ADDR_FMLY_IPV4) ? IPVX_IPV4_ADDR_LEN : IPVX_IPV6_ADDR_LEN;

    *pu1EntryCount += 1;

    MEMSET (&gau1LinBuf[*pi4Offset], 0, MSDP_THREE);
    *pi4Offset += MSDP_THREE;

    gau1LinBuf[*pi4Offset] = (UINT1) (i4AddrLen * MSDP_OCTET_IN_BITS);
    *pi4Offset += MSDP_ONE;

    MEMCPY (&gau1LinBuf[*pi4Offset],
            pSaCache->MibObject.au1FsMsdpSACacheGroupAddr, i4AddrLen);
    *pi4Offset += i4AddrLen;

    MEMCPY (&gau1LinBuf[*pi4Offset],
            pSaCache->MibObject.au1FsMsdpSACacheSourceAddr, i4AddrLen);
    *pi4Offset += i4AddrLen;

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                    "\nEXIT: MsdCacheFormSAPkt"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdCacheSetCurrentRPAddr
 *
 * DESCRIPTION   : This function sets RP address for the current SA cache entry
 *                 This RP address is used to set the SA Hdr to Send out SA 
 *                 Response TLV.
 *
 * INPUTS        : i4SaOriginationStatus- Originator status, Originator
 *                 is configured or not if the oiginator in Cache is 0.0.0.0
 *                 i4AddrLen- Address length
 *                 pu1MyOriginRPAddr- OriginRP address configured
 *                 pCacheOriginRP- Cache originRP present in Cache Entry
 *                 pi4Offset- Offset in SA pkt after Originator address is
 *                 Set
 *
 * OUTPUTS       : pi4Offset- Offset
 *                 pu1RPAddr- RP address
 *
 * RETURNS       : None
 *
 ****************************************************************************/
VOID 
     
     
     
     
     
     
     
    MsdCacheSetCurrentRPAddr
    (INT4 i4SaOriginationStatus, INT4 i4AddrLen,
     UINT1 *pu1MyOriginRPAddr, UINT1 *pCacheOriginRP,
     INT4 *pi4Offset, UINT1 *pu1RPAddr)
{

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_ENTRY,
                    "\nENTRY: MsdCacheSetCurrentRPAddr"));

    if (*pi4Offset == 0)
    {
        if (i4SaOriginationStatus == OSIX_TRUE)
        {
            MEMCPY (pu1RPAddr, pu1MyOriginRPAddr, i4AddrLen);
        }
        else
        {
            MEMCPY (pu1RPAddr, pCacheOriginRP, i4AddrLen);
        }
        *pi4Offset = MSDP_SA_ADVT_RP_OFFSET + i4AddrLen;
    }

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                    "\nEXIT: MsdCacheSetCurrentRPAddr"));

}

/****************************************************************************
 * FUNCTION NAME : MsdCacheGetNextValidSACache
 *
 * DESCRIPTION   : This function gets the next valid SA cache entry
 *
 * INPUTS        : pInSaCache- Current SA cache entry
 *                 u1AddrType- Address type
 *                 i4AddrLen- Address length
 *                 pu1GrpAddr- Group address
 *
 * OUTPUTS       : None
 *
 * RETURNS       : Next SA cache entry if found
 *                 NULL otherwise
 *
 ****************************************************************************/
tMsdpFsMsdpSACacheEntry *
MsdCacheGetNextValidSACache (tMsdpFsMsdpSACacheEntry * pInSaCache,
                             UINT1 u1AddrType, INT4 i4AddrLen,
                             UINT1 *pu1GrpAddr)
{
    tMsdpFsMsdpSACacheEntry *pNextSaCache = NULL;

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_ENTRY,
                    "\nENTRY: MsdCacheGetNextValidSACache"));

    pNextSaCache = MsdpGetNextFsMsdpSACacheTable (pInSaCache);
    if (pNextSaCache == NULL)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheGetNextValidSACache: "
                        "Next SA Cache entry is NULL"));

        return NULL;
    }
    if (pNextSaCache->MibObject.i4FsMsdpSACacheAddrType != u1AddrType)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheGetNextValidSACache: "
                        "Address types don't match"));

        return NULL;
    }
    if (MEMCMP (pNextSaCache->MibObject.au1FsMsdpSACacheGroupAddr,
                pu1GrpAddr, i4AddrLen) != 0)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheGetNextValidSACache: "
                        "SACacheGroupAddresses don't match"));

        return NULL;
    }

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                    "\nEXIT: MsdCacheGetNextValidSACache"));

    return (pNextSaCache);
}

/****************************************************************************
 * FUNCTION NAME : MsdCacheSendSAResponseTlv
 *
 * DESCRIPTION   : This function sends the SA response
 *
 * INPUTS        : u1TlvType- The TLV type
 *                 pPeer- MSDP peer entry
 *                 u1AddrType- Address type
 *                 pSaReqPkt- SA request packet
 *
 * OUTPUTS       : None
 *
 * RETURNS       : OSIX_SUCCESS always
 *
 ****************************************************************************/
INT4
MsdCacheSendSAResponseTlv (UINT1 u1TlvType, tMsdpFsMsdpPeerEntry * pPeer,
                           UINT1 u1AddrType, tMsdpSaReqPkt * pSaReqPkt)
{
    UINT1               au1MyOriginRPAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1CurRpAddr[IPVX_MAX_INET_ADDR_LEN];
    tMsdpFsMsdpSACacheEntry InSaCache;
    tMsdpFsMsdpSACacheEntry *pNextSaCache = NULL;

    INT4                i4AddrLen = 0;
    INT4                i4Offset = 0;
    INT4                i4RetVal = 0;
    INT4                i4BufSize = 0;
    INT4                i4IsRPSame = 0;
    INT4                i4SaOriginationStatus = 0;
    INT4                i4IsOriginator = OSIX_FALSE;
    UINT2               u2PktLen = 0;
    UINT1               u1EntryCount = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_ENTRY,
                    "\nENTRY: MsdCacheSendSAResponseTlv"));

    i4RetVal = MsdpUtilGetTxPktSize (pPeer->TxBufInfo.u2MsgSize,
                                     u1AddrType, &i4BufSize);
    if (i4RetVal == OSIX_FAILURE)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheSendSAResponseTlv: "
                        "MsdpUtilGetTxPktSize returns failure"));

        return OSIX_SUCCESS;
    }
    i4AddrLen = (u1AddrType == IPVX_ADDR_FMLY_IPV4) ?
        IPVX_IPV4_ADDR_LEN : IPVX_IPV6_ADDR_LEN;

    MEMSET (au1MyOriginRPAddr, 0, i4AddrLen);
    MEMSET (au1CurRpAddr, 0, i4AddrLen);
    MEMSET (&InSaCache, 0, sizeof (tMsdpFsMsdpSACacheEntry));

    InSaCache.MibObject.i4FsMsdpSACacheAddrType = u1AddrType;
    InSaCache.MibObject.i4FsMsdpSACacheGroupAddrLen = i4AddrLen;
    MEMCPY (InSaCache.MibObject.au1FsMsdpSACacheGroupAddr,
            pSaReqPkt->au1GrpAddr, i4AddrLen);

    i4IsOriginator = MsdCacheAmIOriginator (u1AddrType, i4AddrLen,
                                            au1MyOriginRPAddr);

    while (NULL != (pNextSaCache = MsdCacheGetNextValidSACache
                    (&InSaCache, u1AddrType, i4AddrLen, pSaReqPkt->au1GrpAddr)))
    {
        i4SaOriginationStatus = OSIX_FALSE;
        if (OSIX_TRUE == MsdpUtilIsAllZeros
            (pNextSaCache->MibObject.au1FsMsdpSACacheOriginRP, u1AddrType))
        {
            if (OSIX_TRUE != i4IsOriginator)
            {
                MEMCPY (&InSaCache, pNextSaCache, sizeof (InSaCache));
                continue;
            }
            else
            {
                i4SaOriginationStatus = OSIX_TRUE;
            }
        }

        /* If suppose buffer size exceeding the available buffer,
         * then packet formation is stopped and the packet is forwarded. */
        i4RetVal = MsdpUtilIsBuffSizeReached (u1AddrType, i4Offset, i4BufSize);
        i4IsRPSame = MEMCMP (pNextSaCache->MibObject.au1FsMsdpSACacheOriginRP,
                             au1CurRpAddr, i4AddrLen);

        if ((i4RetVal == OSIX_FALSE) && ((i4IsRPSame == 0) || (i4Offset == 0)))
        {
            /* If the RP Addr is changed, then the existing buffer 
             * has to be sent out and
             * a new packet has to be formed for the new RP.*/

            MsdCacheSetCurrentRPAddr
                (i4SaOriginationStatus, i4AddrLen, au1MyOriginRPAddr,
                 pNextSaCache->MibObject.au1FsMsdpSACacheOriginRP,
                 &i4Offset, au1CurRpAddr);

            MsdCacheFormSAPkt (pNextSaCache, &i4Offset, &u1EntryCount);

            MEMCPY (&InSaCache, pNextSaCache, sizeof (InSaCache));

        }
        else
        {
            /* Pkt formation over. Reason can be RP address changed or
               Max. Buf reached, so send the pkt to peer */

            MsdCacheFormSAHdr (u1TlvType, i4AddrLen, u1EntryCount,
                               au1CurRpAddr);

            MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheSendSAResponseTlv: "
                            "SA header is formed"));

            /*Forward the pkt to  the peer */
            u2PktLen = (UINT2) MSDP_SA_TLV_LEN (u1EntryCount, i4AddrLen, 0);

            i4RetVal = MsdpOutSendMsgtoPeer (pPeer, gau1LinBuf, 0, u2PktLen,
                                             MSDP_PEER_MAX_TEMP_TX_BUF_LEN);
            if (i4RetVal == OSIX_FAILURE)
            {
                break;
            }

            if (pPeer->MibObject.i4FsMsdpPeerKeepAliveConfigured != 0)
            {
                MsdpTmrStartTmr (&pPeer->MsdpPeerKATimer, MSDP_KEEP_ALIVE_TMR,
                                 (UINT4) pPeer->MibObject.
                                 i4FsMsdpPeerKeepAliveConfigured);
            }
            MsdpOutUpdatePeerStatistics (pPeer, gau1LinBuf, u2PktLen);

            MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheSendSAResponseTlv: "
                            "Message is sent to peer"));

            i4RetVal = MsdpUtilGetTxPktSize (pPeer->TxBufInfo.u2MsgSize,
                                             u1AddrType, &i4BufSize);
            if (i4RetVal == OSIX_FAILURE)
            {

                MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheSendSAResponseTlv: "
                                "MsdpUtilGetTxPktSize returns failure"));

                return OSIX_SUCCESS;
            }

            u1EntryCount = 0;
            i4Offset = 0;
        }
    }
    if (i4Offset != 0)
    {
        MsdCacheFormSAHdr (u1TlvType, i4AddrLen, u1EntryCount, au1CurRpAddr);

        MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheSendSAResponseTlv: "
                        "SA header is formed"));

        u2PktLen = (UINT2) MSDP_SA_TLV_LEN (u1EntryCount, i4AddrLen, 0);
        /* Forward the packet formed. */
        i4RetVal = MsdpOutSendMsgtoPeer (pPeer, gau1LinBuf, 0, u2PktLen,
                                         MSDP_PEER_MAX_TEMP_TX_BUF_LEN);
        if (i4RetVal == OSIX_SUCCESS)
        {
            if (pPeer->MibObject.i4FsMsdpPeerKeepAliveConfigured != 0)
            {
                MsdpTmrStartTmr (&pPeer->MsdpPeerKATimer, MSDP_KEEP_ALIVE_TMR,
                                 (UINT4) pPeer->MibObject.
                                 i4FsMsdpPeerKeepAliveConfigured);
            }
            MsdpOutUpdatePeerStatistics (pPeer, gau1LinBuf, u2PktLen);
            MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheSendSAResponseTlv: "
                            "Message is sent to peer"));
        }

    }

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                    "\nEXIT: MsdCacheSendSAResponseTlv"));

    return OSIX_SUCCESS;

}

/****************************************************************************
 * FUNCTION NAME : MsdCacheHandleSARequest
 *
 * DESCRIPTION   : This function handles SA requests from MSDP Peer
 *
 * INPUTS        : pPeer- MSDP peer entry
 *                 pSaReqPkt- SA request packet
 *                 u1AddrType- Address type
 *
 * OUTPUTS       : None
 *
 * RETURNS       : OSIX_SUCCESS always
 *
 ****************************************************************************/
INT4
MsdCacheHandleSARequest (tMsdpFsMsdpPeerEntry * pPeer,
                         tMsdpSaReqPkt * pSaReqPkt, UINT1 u1AddrType)
{
    UINT1               u1TlvType = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_ENTRY,
                    "\nENTRY: MsdCacheHandleSARequest"));
    if ((u1AddrType != IPVX_ADDR_FMLY_IPV4) &&
        (u1AddrType != IPVX_ADDR_FMLY_IPV6))
    {
        return OSIX_FAILURE;
    }

    u1TlvType = (u1AddrType == IPVX_ADDR_FMLY_IPV4) ?
        MSDP_TLV_SA_RESPONSE : MSDP6_TLV_SA_RESPONSE;

    MsdCacheSendSAResponseTlv (u1TlvType, pPeer, u1AddrType, pSaReqPkt);

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheHandleSARequest: "
                    "SA response is sent"));

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                    "\nEXIT: MsdCacheHandleSARequest"));

    return OSIX_SUCCESS;

}

/****************************************************************************
 * FUNCTION NAME : MsdCacheDeleteEntry
 *
 * DESCRIPTION   : This function deletes an entry from the MSDP Cache
 *
 * INPUTS        : pSaCache- SA cache entry
 *                 i1CacheType- Cache type
 *
 *
 * OUTPUTS       : pSaCache- OriginRP, SourceAddr and GroupAddr fields are 
 *                  modified 
 *
 * RETURNS       : OSIX_SUCCESS always
 *
 ****************************************************************************/
INT4
MsdCacheDeleteEntry (tMsdpFsMsdpSACacheEntry * pSaCache, INT1 i1CacheType)
{
    tMsdpCacheUpdt      MsdpCache;
    UINT1               u1Status = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_ENTRY,
                    "\nENTRY: MsdCacheDeleteEntry"));

    MEMSET (&MsdpCache, 0, sizeof (tMsdpCacheUpdt));
    MsdpCache.u1AddrType = (UINT1) pSaCache->MibObject.i4FsMsdpSACacheAddrType;
    IPVX_ADDR_INIT (MsdpCache.OriginatorRP, MsdpCache.u1AddrType,
                    pSaCache->MibObject.au1FsMsdpSACacheOriginRP);
    IPVX_ADDR_INIT (MsdpCache.SrcAddr, MsdpCache.u1AddrType,
                    pSaCache->MibObject.au1FsMsdpSACacheSourceAddr);
    IPVX_ADDR_INIT (MsdpCache.GrpAddr, MsdpCache.u1AddrType,
                    pSaCache->MibObject.au1FsMsdpSACacheGroupAddr);
    MsdpCache.i1Flag = i1CacheType;
    MsdpCache.u1Action = MSDP_DELETE_CACHE;

    MsdCacheUpdateCacheTbl (MsdpCache, 0, &u1Status);

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheDeleteEntry: "
                    "Cache entry is deleted for CacheOriginRP %s,"
                    "CacheSourceAddr %s, CacheGroupAddr %s ",
                    pSaCache->MibObject.au1FsMsdpSACacheOriginRP,
                    pSaCache->MibObject.au1FsMsdpSACacheSourceAddr,
                    pSaCache->MibObject.au1FsMsdpSACacheGroupAddr));

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                    "\nEXIT: MsdCacheDeleteEntry"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdCacheSendSAAdvtEvent
 *
 * DESCRIPTION   : This function sends the SA iBatch Processing Event
 *
 * INPUTS        : None
 *
 * OUTPUTS       : None
 *
 * RETURNS       : OSIX_SUCCESS if OsixSendEvent succeeds for 
 *                  MSDP_SA_BATCH_ADVT_EVT
 *                 OSIX_FAILURE otherwise 
 *
 ****************************************************************************/
INT4
MsdCacheSendSAAdvtEvent (VOID)
{

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_ENTRY,
                    "\nENTRY: MsdCacheSendSAAdvtEvent"));

    if (OsixSendEvent (SELF, (const UINT1 *) MSDP_TASK_NAME,
                       MSDP_SA_BATCH_ADVT_EVT) != OSIX_SUCCESS)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheSendSAAdvtEvent: "
                        "OsixSendEvent ret failure for MSDP_SA_BATCH_ADVT_EVT"));

        return OSIX_FAILURE;
    }

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                    "\nEXIT: MsdCacheSendSAAdvtEvent"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdCacheAddValidCacheInSAPkt
 *
 * DESCRIPTION   : This function adds valid Cache Entry in SA Packet to send
 *                 to MSDP Peer
 *
 * INPUTS        : u1AddrType- Address type
 *                 pi4BufSize- Buffer size
 *                 pCurSaCache- Current SA cache entry
 *                 pu1EntryCount- Entry count
 *                 pi4Offset- Offset
 *
 *
 * OUTPUTS       : pu1EntryCount- Entry 
 *                 pi4Offset- Offset
 *                 pu1NextAddrType- Next entry's address type
 *
 * RETURNS       : OSIX_SUCCESS if MsdpUtilGetNextFsMsdpSACache doesnt ret NULL
 *                  , address types match, OriginRP is not all zeros and
 *                  MsdpUtilIsBuffSizeReached returns FALSE
 *                 OSIX_FAILURE otherwise 
 *
 ****************************************************************************/
INT4 
     
     
     
     
     
     
     
    MsdCacheAddValidCacheInSAPkt
    (UINT1 u1AddrType, INT4 *pi4BufSize,
     tMsdpFsMsdpSACacheEntry * pCurSaCache, UINT1 *pu1EntryCount,
     INT4 *pi4Offset, UINT1 *pu1NextAddrType)
{
    tMsdpFsMsdpSACacheEntry *pNextSaCache = NULL;
    INT4                i4RetVal = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_ENTRY,
                    "\nENTRY: MsdCacheAddValidCacheInSAPkt"));

    pNextSaCache = MsdpGetNextFsMsdpSACacheTable (pCurSaCache);
    if (pNextSaCache == NULL)
    {

        *pu1NextAddrType = 0;
        MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheAddValidCacheInSAPkt: "
                        "Next SA cache is NULL"));

        return OSIX_FAILURE;
    }

    if (pNextSaCache->MibObject.i4FsMsdpSACacheAddrType != u1AddrType)
    {
        *pu1NextAddrType = (UINT1) pNextSaCache->MibObject.
            i4FsMsdpSACacheAddrType;

        MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheAddValidCacheInSAPkt: "
                        "Address types don't match"));

        return OSIX_FAILURE;
    }
    i4RetVal = MsdpUtilIsAllZeros
        (pNextSaCache->MibObject.au1FsMsdpSACacheOriginRP, u1AddrType);

    if (OSIX_TRUE != i4RetVal)
    {
        MEMCPY (pCurSaCache, pNextSaCache, sizeof (tMsdpFsMsdpSACacheEntry));

        MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                        "\nEXIT: MsdCacheAddValidCacheInSAPkt"));

        return OSIX_SUCCESS;
    }

    i4RetVal = MsdpUtilIsBuffSizeReached (u1AddrType, *pi4Offset, *pi4BufSize);
    if (OSIX_TRUE == i4RetVal)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheAddValidCacheInSAPkt: "
                        "Buffer size is reached"));

        return OSIX_FAILURE;
    }
    MsdCacheFormSAPkt (pNextSaCache, pi4Offset, pu1EntryCount);

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nnMsdCacheAddValidCacheInSAPkt: "
                    "SA packet is formed"));

    MEMCPY (pCurSaCache, pNextSaCache, sizeof (tMsdpFsMsdpSACacheEntry));

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                    "\nEXIT: MsdCacheAddValidCacheInSAPkt"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdCacheSAInfoBatchAdvt
 *
 * DESCRIPTION   : This function does batch advertisement of source active info
 *
 * INPUTS        : pCacheAdvtMarker- Cache advertisement marker
 *
 * OUTPUTS       : pCacheAdvtMarker- OrgRpAddr, GrpAddr and SrcAddr fields 
 *                 are modified
 *                 gau1LinBuf- Global linear buffer
 *
 * RETURNS       : OSIX_SUCCESS if pCacheAdvtMarker is not NULL and 
 *                  MsdCacheAddValidCacheInSAPkt returns success
 *                 OSIX_FAILURE otherwise 
 *
 ****************************************************************************/
INT4
MsdCacheSAInfoBatchAdvt (tMsdpCacheAdvtMarker * pCacheAdvtMarker)
{
    tMsdpFsMsdpSACacheEntry CurSaCache;
    UINT1               au1RpAddr[IPVX_MAX_INET_ADDR_LEN];

    INT4                i4AddrLen = 0;
    INT4                i4Offset = 0;
    INT4                i4RetVal = 0;
    INT4                i4BufSize = 0;
    UINT2               u2PktLen = 0;
    UINT1               u1AddrType = 0;
    UINT1               u1NextAddrType = 0;
    UINT1               u1EntryCount = 0;
    UINT1               u1TlvType = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_ENTRY,
                    "\nENTRY: MsdCacheSAInfoBatchAdvt"));

    MEMSET (&CurSaCache, 0, sizeof (tMsdpFsMsdpSACacheEntry));
    MEMSET (&au1RpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    if (pCacheAdvtMarker == NULL)
    {
        /* As marker node is NULL. We dont process the SA Advt */

        MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheSAInfoBatchAdvt: "
                        "pCacheAdvtMarker is NULL"));

        return OSIX_FAILURE;
    }

    i4AddrLen = pCacheAdvtMarker->OrgRpAddr.u1AddrLen;
    u1AddrType = pCacheAdvtMarker->OrgRpAddr.u1Afi;
    u1TlvType = (u1AddrType == IPVX_ADDR_FMLY_IPV4) ?
        MSDP_TLV_SA : MSDP6_TLV_SA;

    if ((u1AddrType == IPVX_ADDR_FMLY_IPV4) &&
        (OSIX_TRUE != MsdCacheAmIOriginator (u1AddrType, i4AddrLen, au1RpAddr)))
    {
        /*Not an MSDPV4 Originator. SA advt is not allowed for MSDPv4 entries */
        /*Check for MSDPv6 */
        i4AddrLen = IPVX_IPV6_ADDR_LEN;
        u1AddrType = IPVX_ADDR_FMLY_IPV6;
        u1TlvType = MSDP6_TLV_SA;
        MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheSAInfoBatchAdvt: "
                        "MsdCacheAmIOriginator returns false for MSDPv4"));

    }

    if ((u1AddrType == IPVX_ADDR_FMLY_IPV6) &&
        (OSIX_TRUE != MsdCacheAmIOriginator (u1AddrType, i4AddrLen, au1RpAddr)))
    {
        /*Not an MSDPv6 Originator. SA advt is not allowed */

        MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheSAInfoBatchAdvt: "
                        "MsdCacheAmIOriginator returns false for MSDPv6"));

        return OSIX_FAILURE;
    }

    CurSaCache.MibObject.i4FsMsdpSACacheAddrType = u1AddrType;

    CurSaCache.MibObject.i4FsMsdpSACacheOriginRPLen = i4AddrLen;
    MEMCPY (CurSaCache.MibObject.au1FsMsdpSACacheOriginRP,
            pCacheAdvtMarker->OrgRpAddr.au1Addr, i4AddrLen);

    CurSaCache.MibObject.i4FsMsdpSACacheGroupAddrLen = i4AddrLen;
    MEMCPY (CurSaCache.MibObject.au1FsMsdpSACacheGroupAddr,
            pCacheAdvtMarker->GrpAddr.au1Addr, i4AddrLen);

    CurSaCache.MibObject.i4FsMsdpSACacheSourceAddrLen = i4AddrLen;
    MEMCPY (CurSaCache.MibObject.au1FsMsdpSACacheSourceAddr,
            pCacheAdvtMarker->SrcAddr.au1Addr, i4AddrLen);

    i4Offset = MSDP_SA_ADVT_RP_OFFSET + i4AddrLen;
    i4BufSize = MSDP_PEER_MAX_TEMP_TX_BUF_LEN;
    u1NextAddrType = u1AddrType;

    while (OSIX_TRUE == OSIX_TRUE)
    {
        i4RetVal = MsdCacheAddValidCacheInSAPkt (u1AddrType, &i4BufSize,
                                                 &CurSaCache, &u1EntryCount,
                                                 &i4Offset, &u1NextAddrType);
        if (i4RetVal == OSIX_SUCCESS)
        {
            continue;
        }
        else if (u1EntryCount > 0)
        {

            MsdCacheFormSAHdr (u1TlvType, i4AddrLen, u1EntryCount, au1RpAddr);
            MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\n: MsdCacheSAInfoBatchAdvt"
                            "SA header is formed"));

            break;

        }

        MEMSET (&CurSaCache, 0, sizeof (CurSaCache));
        if (u1NextAddrType == 0)
        {
            break;
        }
        else
        {
            i4AddrLen = IPVX_IPV6_ADDR_LEN;
            u1AddrType = IPVX_ADDR_FMLY_IPV6;
            u1TlvType = MSDP6_TLV_SA;

            CurSaCache.MibObject.i4FsMsdpSACacheAddrType = u1AddrType;
            CurSaCache.MibObject.i4FsMsdpSACacheOriginRPLen = i4AddrLen;
            CurSaCache.MibObject.i4FsMsdpSACacheGroupAddrLen = i4AddrLen;
            CurSaCache.MibObject.i4FsMsdpSACacheSourceAddrLen = i4AddrLen;
        }
    }

    /*if AddrType is Zero, there is no cache advt pending.
     *So stop posting the self event.*/
    if (u1NextAddrType != 0)
    {
        pCacheAdvtMarker->OrgRpAddr.u1Afi = u1AddrType;

        pCacheAdvtMarker->OrgRpAddr.u1AddrLen = (UINT1) i4AddrLen;
        MEMCPY (pCacheAdvtMarker->OrgRpAddr.au1Addr,
                CurSaCache.MibObject.au1FsMsdpSACacheOriginRP, i4AddrLen);

        pCacheAdvtMarker->GrpAddr.u1AddrLen = (UINT1) i4AddrLen;
        MEMCPY (pCacheAdvtMarker->GrpAddr.au1Addr,
                CurSaCache.MibObject.au1FsMsdpSACacheGroupAddr, i4AddrLen);

        pCacheAdvtMarker->SrcAddr.u1AddrLen = (UINT1) i4AddrLen;
        MEMCPY (pCacheAdvtMarker->SrcAddr.au1Addr,
                CurSaCache.MibObject.au1FsMsdpSACacheSourceAddr, i4AddrLen);

        MsdCacheSendSAAdvtEvent ();

        MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\n: MsdCacheSAInfoBatchAdvt"
                        "SA advt event is sent"));

    }

    if (u1EntryCount == 0)
    {
        return i4RetVal;
    }

    u2PktLen = MSDP_SA_TLV_LEN (u1EntryCount, i4AddrLen, 0);
    /* Forward the packet to all the peers. */
    MsdpOutFwdSAPkt (NULL, gau1LinBuf, u1AddrType, 0, u2PktLen,
                     (UINT2) MSDP_PEER_MAX_TEMP_TX_BUF_LEN);

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\n: MsdCacheSAInfoBatchAdvt"
                    "SA packet is forwarded"));

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                    "\nEXIT: MsdCacheSAInfoBatchAdvt"));

    return i4RetVal;
}

/****************************************************************************
 * FUNCTION NAME : MsdCacheConstructSAEntry
 *
 * DESCRIPTION   : This function constructs an SA entry from the SA Message
 *                 received. this is used to update the Cache Table
 *
 * INPUTS        : u1AddrType- address type
 *                 pu2ReadOffset- Read offset
 *                 pMsdpCache- Msdp cache entry
 *                 pu1Msg- MSDP SA Msg Rcvd
 *
 *
 * OUTPUTS       : pu2ReadOffset- Read offset
 *
 * RETURNS       : OSIX_SUCCESS always
 *
 ****************************************************************************/
INT4
MsdCacheConstructSAEntry (UINT1 u1AddrType, UINT2 *pu2ReadOffset,
                          tMsdpCacheUpdt * pMsdpCache, UINT1 *pu1Msg)
{
    UINT1               au1SaEntry[MSDP_V6_SA_ADVT_ONE_ENTRY_SIZE];
    tMsdpSaAdvtEntry   *pMsdpSaEntry = NULL;
    tMsdp6SaAdvtEntry  *pMsdp6SaEntry = NULL;
    UINT2               u2Count = 0;
    UINT1               u1SaEntrySize = 0;
    UINT1               u1AddrPrefix = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_ENTRY,
                    "\nENTRY: MsdCacheConstructSAEntry"));

    MEMSET (au1SaEntry, 0, MSDP_V6_SA_ADVT_ONE_ENTRY_SIZE);
    u1SaEntrySize = (u1AddrType == IPVX_ADDR_FMLY_IPV4) ?
        MSDP_V4_SA_ADVT_ONE_ENTRY_SIZE : MSDP_V6_SA_ADVT_ONE_ENTRY_SIZE;
    u1AddrPrefix = (u1AddrType == IPVX_ADDR_FMLY_IPV4) ?
        (BYTE_LENGTH * IPVX_IPV4_ADDR_LEN) : (BYTE_LENGTH * IPVX_IPV6_ADDR_LEN);

    if (*pu2ReadOffset + u1SaEntrySize >= MSDP_PEER_MAX_RX_TX_BUF_LEN)
    {
        /* Extracting the Grp and Src from the received SA pkt byte by byte 
         * as it is wrapped around */
        for (u2Count = 0; u2Count < u1SaEntrySize; u2Count++)
        {
            au1SaEntry[u2Count] = *(pu1Msg + *pu2ReadOffset);
            *pu2ReadOffset += 1;
            if (*pu2ReadOffset >= MSDP_PEER_MAX_RX_TX_BUF_LEN)
            {
                *pu2ReadOffset = 0;
            }
        }

        if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            pMsdpSaEntry = (tMsdpSaAdvtEntry *) au1SaEntry;
        }
        else
        {
            pMsdp6SaEntry = (tMsdp6SaAdvtEntry *) au1SaEntry;
        }
    }
    else
    {
        if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            pMsdpSaEntry = (tMsdpSaAdvtEntry *) (pu1Msg + *pu2ReadOffset);
        }
        else
        {
            pMsdp6SaEntry = (tMsdp6SaAdvtEntry *) (pu1Msg + *pu2ReadOffset);
        }

        *pu2ReadOffset += u1SaEntrySize;
    }

    if (((u1AddrType == IPVX_ADDR_FMLY_IPV4) &&
         (pMsdpSaEntry->u1Prefix != u1AddrPrefix)) ||
        ((u1AddrType == IPVX_ADDR_FMLY_IPV6) &&
         (pMsdp6SaEntry->u1Prefix != u1AddrPrefix)))
    {

        MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\nMsdCacheConstructSAEntry: "
                        "Prefix mismatch"));

        return OSIX_FAILURE;
    }
    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        IPVX_ADDR_INIT (pMsdpCache->GrpAddr, u1AddrType,
                        pMsdpSaEntry->au1GrpAddr);
        IPVX_ADDR_INIT (pMsdpCache->SrcAddr, u1AddrType,
                        pMsdpSaEntry->au1SrcAddr);
    }
    else
    {
        IPVX_ADDR_INIT (pMsdpCache->GrpAddr, u1AddrType,
                        pMsdp6SaEntry->au1GrpAddr);
        IPVX_ADDR_INIT (pMsdpCache->SrcAddr, u1AddrType,
                        pMsdp6SaEntry->au1SrcAddr);
    }

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                    "\nEXIT: MsdCacheConstructSAEntry"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdCacheProcessSAAdvtMsg
 *
 * DESCRIPTION   : This function processes the source active advertisement
 *                 messages. Updates the MSDP Cache Table with the rcvd 
 *                 info and informs to MRP.
 *
 * INPUTS        : pPeer- MSDP peer
 *                 pSaAdvtHdr- Source Active advertisement header
 *                 u2ReadOffset- Read offset
 *                 u1AddrType- Address type
 *
 * OUTPUTS       : pSaAdvtHdr- au1RpAddr field is changed
 *
 * RETURNS       : OSIX_SUCCESS always
 *
 ****************************************************************************/
INT4
MsdCacheProcessSAMsg (tMsdpFsMsdpPeerEntry * pPeer,
                      tMsdpSaAdvtHdr * pSaAdvtHdr,
                      UINT2 u2ReadOffset, UINT1 u1AddrType)
{
    tMsdpCacheUpdt      MsdpCache;
    tMsdpMrpSaAdvt      MsdpSaInfo;
    tMsdpFsMsdpPeerEntry Peer;
    UINT4               u4IsDataPkt = 0;
    INT4                i4RetVal = 0;
    INT4                i4Offset = 0;
    UINT1               u1SaEntryCnt = 0;
    UINT1               u1Status = 0;
    UINT1               u1AddrLen = 0;
    UINT1               u1Len = sizeof (tIPvXAddr);
    UINT1               u1SAFailCount = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_ENTRY,
                    "\nENTRY: MsdCacheProcessSAAdvtMsg"));

    MEMSET (&MsdpCache, 0, sizeof (tMsdpCacheUpdt));
    MEMSET (&MsdpSaInfo, 0, sizeof (tMsdpMrpSaAdvt));
    MEMSET (&Peer, 0, sizeof (tMsdpFsMsdpPeerEntry));

    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        u1AddrLen = IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        u1AddrLen = IPVX_IPV6_ADDR_LEN;
    }

    IPVX_ADDR_INIT (MsdpCache.OriginatorRP, u1AddrType, pSaAdvtHdr->au1RpAddr);

    MsdpCache.i1Flag = ((pSaAdvtHdr->u1Type == MSDP_TLV_SA) ||
                        (pSaAdvtHdr->u1Type == MSDP6_TLV_SA)) ?
        MSDP_SA_ADVT_CACHE : MSDP_SA_RESP_CACHE;
    MsdpCache.u1AddrType = u1AddrType;
    MsdpCache.u1Action = MSDP_ADD_CACHE;

    u4IsDataPkt = 0;

    i4RetVal = MsdpPimIfAllocateBuf (&MsdpSaInfo, MSDP_MAX_GRPSRC_BUF_SIZE,
                                     u1AddrType);
    /* Allocation Fails */
    if (i4RetVal == OSIX_FAILURE)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                        "\nEXIT: MsdCacheProcessSAAdvtMsg"));

        return OSIX_SUCCESS;
    }

    MsdpGetFsMsdpMappingComponentId ((INT4 *) (VOID *) &MsdpSaInfo.u1CompId);

    pSaAdvtHdr->u2Len = OSIX_NTOHS (pSaAdvtHdr->u2Len);

    while (u1SaEntryCnt < pSaAdvtHdr->u1EntryCnt)
    {
        u1SaEntryCnt++;
        i4RetVal = MsdCacheConstructSAEntry (u1AddrType, &u2ReadOffset,
                                             &MsdpCache,
                                             pPeer->RxBufInfo.pu1Msg);
        if (i4RetVal == OSIX_FAILURE)
        {
            u1SAFailCount++;
            MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\n: MsdCacheProcessSAAdvtMsg"
                            "Construction of SA entry not successful"));

            continue;
        }
        /*Check for Data pkt */
        if ((u1SaEntryCnt == pSaAdvtHdr->u1EntryCnt) &&
            (pSaAdvtHdr->u2Len > MSDP_SA_RESPONSE_TLV_LEN (u1SaEntryCnt,
                                                           MsdpCache.GrpAddr.
                                                           u1AddrLen)))
        {
            u4IsDataPkt = 1;    /*Data packet exists */
        }

        MEMCPY (&Peer, pPeer, sizeof (tMsdpFsMsdpPeerEntry));
        MsdpCache.pLrndFrmPeer = &Peer;
        i4RetVal = MsdCacheUpdateCacheTbl (MsdpCache, u4IsDataPkt, &u1Status);
        if ((u1Status == MSDP_CACHE_NEW) || (u4IsDataPkt == 1))
        {
            MsdpPimIfFillSAEntry
                (&MsdpSaInfo, &MsdpCache.GrpAddr,
                 &MsdpCache.SrcAddr, &i4Offset);

        }
        if (u4IsDataPkt == 1)
        {
            /* There is a multicast data packet attached with SA Packet.
             * Also Forward that packet  to PIM*/
            MsdpSaInfo.u2DataPktLen = (UINT2) (pSaAdvtHdr->u2Len -
                                               (UINT2)
                                               MSDP_SA_RESPONSE_TLV_LEN
                                               (u1SaEntryCnt,
                                                MsdpCache.GrpAddr.u1AddrLen));

            MsdpSaInfo.pDataPkt = CRU_BUF_Allocate_MsgBufChain
                (MsdpSaInfo.u2DataPktLen, 0);
            if (MsdpSaInfo.pDataPkt != NULL)
            {
                MsdpUtilCopyOverBufChain (&MsdpSaInfo.pDataPkt,
                                          (pPeer->RxBufInfo.pu1Msg +
                                           u2ReadOffset), 0,
                                          &MsdpSaInfo.u2DataPktLen);
            }
            pPeer->MibObject.u4FsMsdpPeerInDataPackets++;
        }

        if (u1SaEntryCnt == pSaAdvtHdr->u1EntryCnt)
        {
            /* Inform the SA to PIM */
            MsdpPortInformSAToMrp (&MsdpSaInfo);

            MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\n: MsdCacheProcessSAAdvtMsg"
                            "SA informed to MRP"));

            break;
        }

        if ((i4Offset + (MSDP_TWO * u1Len)) > MSDP_MAX_GRPSRC_BUF_SIZE)
        {
            /* Inform the SA to PIM and allocate Buf for remaining
               SAs */
            MsdpPortInformSAToMrp (&MsdpSaInfo);

            MSDP_TRC_FUNC ((MSDP_TRC_CACHE, "\n: MsdCacheProcessSAAdvtMsg"
                            "SA is informed to MRP"));

            MsdpSaInfo.pGrpSrcAddrList = CRU_BUF_Allocate_MsgBufChain
                (MSDP_MAX_GRPSRC_BUF_SIZE, 0);
            /* Allocation Fails */
            if (MsdpSaInfo.pGrpSrcAddrList == NULL)
            {

                MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                                "\nEXIT: MsdCacheProcessSAAdvtMsg"));

                return OSIX_SUCCESS;
            }
            u1SAFailCount = u1SaEntryCnt;
            MsdpSaInfo.u2SrcCount = 0;
            i4Offset = 0;

        }

    }
    if (u1SAFailCount == u1SaEntryCnt)
    {
        CRU_BUF_Release_MsgBufChain (MsdpSaInfo.pGrpSrcAddrList, FALSE);
    }
    MSDP_TRC_FUNC ((MSDP_TRC_CACHE | MSDP_FN_EXIT,
                    "\nEXIT: MsdCacheProcessSAAdvtMsg"));

    KW_FALSEPOSITIVE_FIX (MsdpSaInfo.pGrpSrcAddrList);
    UNUSED_PARAM (u1AddrLen);
    return OSIX_SUCCESS;
}
