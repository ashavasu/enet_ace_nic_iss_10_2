/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdmsdlw.c,v 1.2 2014/03/14 12:56:29 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "stdmsdlw.h"
# include  "msdpinc.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMsdpEnabled
 Input       :  The Indices

                The Object 
                retValMsdpEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpEnabled (INT4 *pi4RetValMsdpEnabled)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsMsdpIPv4AdminStat (pi4RetValMsdpEnabled);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpCacheLifetime
 Input       :  The Indices

                The Object 
                retValMsdpCacheLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpCacheLifetime (UINT4 *pu4RetValMsdpCacheLifetime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsMsdpCacheLifetime (pu4RetValMsdpCacheLifetime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpNumSACacheEntries
 Input       :  The Indices

                The Object 
                retValMsdpNumSACacheEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpNumSACacheEntries (UINT4 *pu4RetValMsdpNumSACacheEntries)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsMsdpNumSACacheEntries (pu4RetValMsdpNumSACacheEntries);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpRPAddress
 Input       :  The Indices

                The Object 
                retValMsdpRPAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpRPAddress (UINT4 *pu4RetValMsdpRPAddress)
{
    tSNMP_OCTET_STRING_TYPE RPAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&RPAddress, 0, sizeof (RPAddress));
    RPAddress.pu1_OctetList = (UINT1 *) pu4RetValMsdpRPAddress;
    RPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;

    i1RetVal = nmhGetFsMsdpRPAddress (IPVX_ADDR_FMLY_IPV4, &RPAddress);
    UNUSED_PARAM (i1RetVal);
    *pu4RetValMsdpRPAddress = OSIX_NTOHL (*pu4RetValMsdpRPAddress);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMsdpEnabled
 Input       :  The Indices

                The Object 
                setValMsdpEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMsdpEnabled (INT4 i4SetValMsdpEnabled)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsMsdpIPv4AdminStat (i4SetValMsdpEnabled);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetMsdpCacheLifetime
 Input       :  The Indices

                The Object 
                setValMsdpCacheLifetime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMsdpCacheLifetime (UINT4 u4SetValMsdpCacheLifetime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsMsdpCacheLifetime (u4SetValMsdpCacheLifetime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetMsdpRPAddress
 Input       :  The Indices

                The Object 
                setValMsdpRPAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMsdpRPAddress (UINT4 u4SetValMsdpRPAddress)
{
    tSNMP_OCTET_STRING_TYPE RPAddress;
    INT4                RPStatus = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&RPAddress, 0, sizeof (RPAddress));
    u4SetValMsdpRPAddress = OSIX_NTOHL (u4SetValMsdpRPAddress);
    RPAddress.pu1_OctetList = (UINT1 *) &u4SetValMsdpRPAddress;
    RPAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpRPStatus (IPVX_ADDR_FMLY_IPV4, &RPStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        i1RetVal = nmhSetFsMsdpRPStatus (IPVX_ADDR_FMLY_IPV4, 4);
    }
    else if (RPStatus != 1)
    {
        i1RetVal = nmhSetFsMsdpRPStatus (IPVX_ADDR_FMLY_IPV4, 1);
    }
    if (i1RetVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetFsMsdpRPAddress (IPVX_ADDR_FMLY_IPV4, &RPAddress);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MsdpEnabled
 Input       :  The Indices

                The Object 
                testValMsdpEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MsdpEnabled (UINT4 *pu4ErrorCode, INT4 i4TestValMsdpEnabled)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhTestv2FsMsdpIPv4AdminStat (pu4ErrorCode, i4TestValMsdpEnabled);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2MsdpCacheLifetime
 Input       :  The Indices

                The Object 
                testValMsdpCacheLifetime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MsdpCacheLifetime (UINT4 *pu4ErrorCode,
                            UINT4 u4TestValMsdpCacheLifetime)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2FsMsdpCacheLifetime (pu4ErrorCode,
                                             u4TestValMsdpCacheLifetime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2MsdpRPAddress
 Input       :  The Indices

                The Object 
                testValMsdpRPAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MsdpRPAddress (UINT4 *pu4ErrorCode, UINT4 u4TestValMsdpRPAddress)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4TestValMsdpRPAddress);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MsdpEnabled
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MsdpEnabled (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2MsdpCacheLifetime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MsdpCacheLifetime (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2MsdpRPAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MsdpRPAddress (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MsdpPeerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMsdpPeerTable
 Input       :  The Indices
                MsdpPeerRemoteAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMsdpPeerTable (UINT4 u4MsdpPeerRemoteAddress)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhValidateIndexInstanceFsMsdpPeerTable (IPVX_ADDR_FMLY_IPV4,
                                                        &PeerRemoteAddress);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMsdpPeerTable
 Input       :  The Indices
                MsdpPeerRemoteAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMsdpPeerTable (UINT4 *pu4MsdpPeerRemoteAddress)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT4                i4FsMsdpPeerAddrType = IPVX_ADDR_FMLY_IPV4;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));

    PeerRemoteAddress.pu1_OctetList = (UINT1 *) pu4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFirstIndexFsMsdpPeerTable (&i4FsMsdpPeerAddrType,
                                                &PeerRemoteAddress);

    *pu4MsdpPeerRemoteAddress = OSIX_NTOHL (*pu4MsdpPeerRemoteAddress);
    if (i4FsMsdpPeerAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        return SNMP_FAILURE;
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMsdpPeerTable
 Input       :  The Indices
                MsdpPeerRemoteAddress
                nextMsdpPeerRemoteAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMsdpPeerTable (UINT4 u4MsdpPeerRemoteAddress,
                              UINT4 *pu4NextMsdpPeerRemoteAddress)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    tSNMP_OCTET_STRING_TYPE NextMsdpPeerRemoteAddress;
    INT4                i4NextFsMsdpPeerAddrType = IPVX_ADDR_FMLY_IPV4;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    MEMSET (&NextMsdpPeerRemoteAddress, 0, sizeof (NextMsdpPeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    NextMsdpPeerRemoteAddress.pu1_OctetList =
        (UINT1 *) pu4NextMsdpPeerRemoteAddress;
    NextMsdpPeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetNextIndexFsMsdpPeerTable (IPVX_ADDR_FMLY_IPV4,
                                               &i4NextFsMsdpPeerAddrType,
                                               &PeerRemoteAddress,
                                               &NextMsdpPeerRemoteAddress);
    if (i4NextFsMsdpPeerAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        return SNMP_FAILURE;
    }
    *pu4NextMsdpPeerRemoteAddress = OSIX_NTOHL (*pu4NextMsdpPeerRemoteAddress);
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMsdpPeerState
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerState (UINT4 u4MsdpPeerRemoteAddress,
                     INT4 *pi4RetValMsdpPeerState)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerState (IPVX_ADDR_FMLY_IPV4,
                                      &PeerRemoteAddress,
                                      pi4RetValMsdpPeerState);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerRPFFailures
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerRPFFailures
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerRPFFailures (UINT4 u4MsdpPeerRemoteAddress,
                           UINT4 *pu4RetValMsdpPeerRPFFailures)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerRPFFailures (IPVX_ADDR_FMLY_IPV4,
                                            &PeerRemoteAddress,
                                            pu4RetValMsdpPeerRPFFailures);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerInSAs
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerInSAs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerInSAs (UINT4 u4MsdpPeerRemoteAddress,
                     UINT4 *pu4RetValMsdpPeerInSAs)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerInSAs (IPVX_ADDR_FMLY_IPV4,
                                      &PeerRemoteAddress,
                                      pu4RetValMsdpPeerInSAs);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerOutSAs
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerOutSAs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerOutSAs (UINT4 u4MsdpPeerRemoteAddress,
                      UINT4 *pu4RetValMsdpPeerOutSAs)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerOutSAs (IPVX_ADDR_FMLY_IPV4,
                                       &PeerRemoteAddress,
                                       pu4RetValMsdpPeerOutSAs);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerInSARequests
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerInSARequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerInSARequests (UINT4 u4MsdpPeerRemoteAddress,
                            UINT4 *pu4RetValMsdpPeerInSARequests)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerInSARequests (IPVX_ADDR_FMLY_IPV4,
                                             &PeerRemoteAddress,
                                             pu4RetValMsdpPeerInSARequests);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerOutSARequests
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerOutSARequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerOutSARequests (UINT4 u4MsdpPeerRemoteAddress,
                             UINT4 *pu4RetValMsdpPeerOutSARequests)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerOutSARequests (IPVX_ADDR_FMLY_IPV4,
                                              &PeerRemoteAddress,
                                              pu4RetValMsdpPeerOutSARequests);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerInControlMessages
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerInControlMessages
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerInControlMessages (UINT4 u4MsdpPeerRemoteAddress,
                                 UINT4 *pu4RetValMsdpPeerInControlMessages)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerInControlMessages (IPVX_ADDR_FMLY_IPV4,
                                                  &PeerRemoteAddress,
                                                  pu4RetValMsdpPeerInControlMessages);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerOutControlMessages
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerOutControlMessages
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerOutControlMessages (UINT4 u4MsdpPeerRemoteAddress,
                                  UINT4 *pu4RetValMsdpPeerOutControlMessages)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerOutControlMessages (IPVX_ADDR_FMLY_IPV4,
                                                   &PeerRemoteAddress,
                                                   pu4RetValMsdpPeerOutControlMessages);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerInDataPackets
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerInDataPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerInDataPackets (UINT4 u4MsdpPeerRemoteAddress,
                             UINT4 *pu4RetValMsdpPeerInDataPackets)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerInDataPackets (IPVX_ADDR_FMLY_IPV4,
                                              &PeerRemoteAddress,
                                              pu4RetValMsdpPeerInDataPackets);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerOutDataPackets
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerOutDataPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerOutDataPackets (UINT4 u4MsdpPeerRemoteAddress,
                              UINT4 *pu4RetValMsdpPeerOutDataPackets)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerOutDataPackets (IPVX_ADDR_FMLY_IPV4,
                                               &PeerRemoteAddress,
                                               pu4RetValMsdpPeerOutDataPackets);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerFsmEstablishedTransitions
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerFsmEstablishedTransitions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerFsmEstablishedTransitions (UINT4 u4MsdpPeerRemoteAddress,
                                         UINT4
                                         *pu4RetValMsdpPeerFsmEstablishedTransitions)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerFsmEstablishedTransitions (IPVX_ADDR_FMLY_IPV4,
                                                          &PeerRemoteAddress,
                                                          pu4RetValMsdpPeerFsmEstablishedTransitions);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerFsmEstablishedTime
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerFsmEstablishedTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerFsmEstablishedTime (UINT4 u4MsdpPeerRemoteAddress,
                                  UINT4 *pu4RetValMsdpPeerFsmEstablishedTime)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerFsmEstablishedTime (IPVX_ADDR_FMLY_IPV4,
                                                   &PeerRemoteAddress,
                                                   pu4RetValMsdpPeerFsmEstablishedTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerInMessageTime
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerInMessageTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerInMessageTime (UINT4 u4MsdpPeerRemoteAddress,
                             UINT4 *pu4RetValMsdpPeerInMessageTime)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerInMessageTime (IPVX_ADDR_FMLY_IPV4,
                                              &PeerRemoteAddress,
                                              pu4RetValMsdpPeerInMessageTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerLocalAddress
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerLocalAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerLocalAddress (UINT4 u4MsdpPeerRemoteAddress,
                            UINT4 *pu4RetValMsdpPeerLocalAddress)
{
    tSNMP_OCTET_STRING_TYPE RetValMsdpPeerLocalAddress;
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    MEMSET (&RetValMsdpPeerLocalAddress, 0,
            sizeof (RetValMsdpPeerLocalAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    RetValMsdpPeerLocalAddress.pu1_OctetList =
        (UINT1 *) pu4RetValMsdpPeerLocalAddress;
    RetValMsdpPeerLocalAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerLocalAddress (IPVX_ADDR_FMLY_IPV4,
                                             &PeerRemoteAddress,
                                             &RetValMsdpPeerLocalAddress);
    *pu4RetValMsdpPeerLocalAddress =
        OSIX_NTOHL (*pu4RetValMsdpPeerLocalAddress);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerConnectRetryInterval
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerConnectRetryInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerConnectRetryInterval (UINT4 u4MsdpPeerRemoteAddress,
                                    INT4 *pi4RetValMsdpPeerConnectRetryInterval)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerConnectRetryInterval (IPVX_ADDR_FMLY_IPV4,
                                                     &PeerRemoteAddress,
                                                     pi4RetValMsdpPeerConnectRetryInterval);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerHoldTimeConfigured
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerHoldTimeConfigured
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerHoldTimeConfigured (UINT4 u4MsdpPeerRemoteAddress,
                                  INT4 *pi4RetValMsdpPeerHoldTimeConfigured)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerHoldTimeConfigured (IPVX_ADDR_FMLY_IPV4,
                                                   &PeerRemoteAddress,
                                                   pi4RetValMsdpPeerHoldTimeConfigured);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerKeepAliveConfigured
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerKeepAliveConfigured
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerKeepAliveConfigured (UINT4 u4MsdpPeerRemoteAddress,
                                   INT4 *pi4RetValMsdpPeerKeepAliveConfigured)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerKeepAliveConfigured (IPVX_ADDR_FMLY_IPV4,
                                                    &PeerRemoteAddress,
                                                    pi4RetValMsdpPeerKeepAliveConfigured);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerDataTtl
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerDataTtl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerDataTtl (UINT4 u4MsdpPeerRemoteAddress,
                       INT4 *pi4RetValMsdpPeerDataTtl)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerDataTtl (IPVX_ADDR_FMLY_IPV4,
                                        &PeerRemoteAddress,
                                        pi4RetValMsdpPeerDataTtl);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerStatus
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerStatus (UINT4 u4MsdpPeerRemoteAddress,
                      INT4 *pi4RetValMsdpPeerStatus)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerStatus (IPVX_ADDR_FMLY_IPV4,
                                       &PeerRemoteAddress,
                                       pi4RetValMsdpPeerStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerRemotePort
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerRemotePort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerRemotePort (UINT4 u4MsdpPeerRemoteAddress,
                          INT4 *pi4RetValMsdpPeerRemotePort)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerRemotePort (IPVX_ADDR_FMLY_IPV4,
                                           &PeerRemoteAddress,
                                           pi4RetValMsdpPeerRemotePort);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerLocalPort
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerLocalPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerLocalPort (UINT4 u4MsdpPeerRemoteAddress,
                         INT4 *pi4RetValMsdpPeerLocalPort)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerLocalPort (IPVX_ADDR_FMLY_IPV4,
                                          &PeerRemoteAddress,
                                          pi4RetValMsdpPeerLocalPort);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerEncapsulationType
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerEncapsulationType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerEncapsulationType (UINT4 u4MsdpPeerRemoteAddress,
                                 INT4 *pi4RetValMsdpPeerEncapsulationType)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerEncapsulationType (IPVX_ADDR_FMLY_IPV4,
                                                  &PeerRemoteAddress,
                                                  pi4RetValMsdpPeerEncapsulationType);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerConnectionAttempts
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerConnectionAttempts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerConnectionAttempts (UINT4 u4MsdpPeerRemoteAddress,
                                  UINT4 *pu4RetValMsdpPeerConnectionAttempts)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerConnectionAttempts (IPVX_ADDR_FMLY_IPV4,
                                                   &PeerRemoteAddress,
                                                   pu4RetValMsdpPeerConnectionAttempts);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpPeerDiscontinuityTime
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                retValMsdpPeerDiscontinuityTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpPeerDiscontinuityTime (UINT4 u4MsdpPeerRemoteAddress,
                                 UINT4 *pu4RetValMsdpPeerDiscontinuityTime)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpPeerDiscontinuityTime (IPVX_ADDR_FMLY_IPV4,
                                                  &PeerRemoteAddress,
                                                  pu4RetValMsdpPeerDiscontinuityTime);
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMsdpPeerLocalAddress
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                setValMsdpPeerLocalAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMsdpPeerLocalAddress (UINT4 u4MsdpPeerRemoteAddress,
                            UINT4 u4SetValMsdpPeerLocalAddress)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    tSNMP_OCTET_STRING_TYPE SetValMsdpPeerLocalAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    MEMSET (&SetValMsdpPeerLocalAddress, 0,
            sizeof (SetValMsdpPeerLocalAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4SetValMsdpPeerLocalAddress = OSIX_NTOHL (u4SetValMsdpPeerLocalAddress);
    SetValMsdpPeerLocalAddress.pu1_OctetList =
        (UINT1 *) &u4SetValMsdpPeerLocalAddress;
    SetValMsdpPeerLocalAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhSetFsMsdpPeerLocalAddress (IPVX_ADDR_FMLY_IPV4,
                                             &PeerRemoteAddress,
                                             &SetValMsdpPeerLocalAddress);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetMsdpPeerConnectRetryInterval
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                setValMsdpPeerConnectRetryInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMsdpPeerConnectRetryInterval (UINT4 u4MsdpPeerRemoteAddress,
                                    INT4 i4SetValMsdpPeerConnectRetryInterval)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhSetFsMsdpPeerConnectRetryInterval (IPVX_ADDR_FMLY_IPV4,
                                                     &PeerRemoteAddress,
                                                     i4SetValMsdpPeerConnectRetryInterval);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetMsdpPeerHoldTimeConfigured
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                setValMsdpPeerHoldTimeConfigured
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMsdpPeerHoldTimeConfigured (UINT4 u4MsdpPeerRemoteAddress,
                                  INT4 i4SetValMsdpPeerHoldTimeConfigured)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhSetFsMsdpPeerHoldTimeConfigured (IPVX_ADDR_FMLY_IPV4,
                                                   &PeerRemoteAddress,
                                                   i4SetValMsdpPeerHoldTimeConfigured);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetMsdpPeerKeepAliveConfigured
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                setValMsdpPeerKeepAliveConfigured
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMsdpPeerKeepAliveConfigured (UINT4 u4MsdpPeerRemoteAddress,
                                   INT4 i4SetValMsdpPeerKeepAliveConfigured)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhSetFsMsdpPeerKeepAliveConfigured (IPVX_ADDR_FMLY_IPV4,
                                                    &PeerRemoteAddress,
                                                    i4SetValMsdpPeerKeepAliveConfigured);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetMsdpPeerDataTtl
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                setValMsdpPeerDataTtl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMsdpPeerDataTtl (UINT4 u4MsdpPeerRemoteAddress,
                       INT4 i4SetValMsdpPeerDataTtl)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhSetFsMsdpPeerDataTtl (IPVX_ADDR_FMLY_IPV4,
                                        &PeerRemoteAddress,
                                        i4SetValMsdpPeerDataTtl);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetMsdpPeerStatus
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                setValMsdpPeerStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMsdpPeerStatus (UINT4 u4MsdpPeerRemoteAddress,
                      INT4 i4SetValMsdpPeerStatus)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhSetFsMsdpPeerStatus (IPVX_ADDR_FMLY_IPV4,
                                       &PeerRemoteAddress,
                                       i4SetValMsdpPeerStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetMsdpPeerEncapsulationType
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                setValMsdpPeerEncapsulationType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMsdpPeerEncapsulationType (UINT4 u4MsdpPeerRemoteAddress,
                                 INT4 i4SetValMsdpPeerEncapsulationType)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhSetFsMsdpPeerEncapsulationType (IPVX_ADDR_FMLY_IPV4,
                                                  &PeerRemoteAddress,
                                                  i4SetValMsdpPeerEncapsulationType);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MsdpPeerLocalAddress
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                testValMsdpPeerLocalAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MsdpPeerLocalAddress (UINT4 *pu4ErrorCode,
                               UINT4 u4MsdpPeerRemoteAddress,
                               UINT4 u4TestValMsdpPeerLocalAddress)
{
    tSNMP_OCTET_STRING_TYPE TestValMsdpPeerLocalAddress;
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    MEMSET (&TestValMsdpPeerLocalAddress, 0,
            sizeof (TestValMsdpPeerLocalAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TestValMsdpPeerLocalAddress = OSIX_NTOHL (u4TestValMsdpPeerLocalAddress);
    TestValMsdpPeerLocalAddress.pu1_OctetList =
        (UINT1 *) &u4TestValMsdpPeerLocalAddress;
    TestValMsdpPeerLocalAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhTestv2FsMsdpPeerLocalAddress (pu4ErrorCode,
                                                IPVX_ADDR_FMLY_IPV4,
                                                &PeerRemoteAddress,
                                                &TestValMsdpPeerLocalAddress);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2MsdpPeerConnectRetryInterval
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                testValMsdpPeerConnectRetryInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MsdpPeerConnectRetryInterval (UINT4 *pu4ErrorCode,
                                       UINT4 u4MsdpPeerRemoteAddress,
                                       INT4
                                       i4TestValMsdpPeerConnectRetryInterval)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhTestv2FsMsdpPeerConnectRetryInterval (pu4ErrorCode,
                                                        IPVX_ADDR_FMLY_IPV4,
                                                        &PeerRemoteAddress,
                                                        i4TestValMsdpPeerConnectRetryInterval);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2MsdpPeerHoldTimeConfigured
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                testValMsdpPeerHoldTimeConfigured
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MsdpPeerHoldTimeConfigured (UINT4 *pu4ErrorCode,
                                     UINT4 u4MsdpPeerRemoteAddress,
                                     INT4 i4TestValMsdpPeerHoldTimeConfigured)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhTestv2FsMsdpPeerHoldTimeConfigured (pu4ErrorCode,
                                                      IPVX_ADDR_FMLY_IPV4,
                                                      &PeerRemoteAddress,
                                                      i4TestValMsdpPeerHoldTimeConfigured);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2MsdpPeerKeepAliveConfigured
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                testValMsdpPeerKeepAliveConfigured
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MsdpPeerKeepAliveConfigured (UINT4 *pu4ErrorCode,
                                      UINT4 u4MsdpPeerRemoteAddress,
                                      INT4 i4TestValMsdpPeerKeepAliveConfigured)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhTestv2FsMsdpPeerKeepAliveConfigured (pu4ErrorCode,
                                                       IPVX_ADDR_FMLY_IPV4,
                                                       &PeerRemoteAddress,
                                                       i4TestValMsdpPeerKeepAliveConfigured);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2MsdpPeerDataTtl
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                testValMsdpPeerDataTtl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MsdpPeerDataTtl (UINT4 *pu4ErrorCode, UINT4 u4MsdpPeerRemoteAddress,
                          INT4 i4TestValMsdpPeerDataTtl)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhTestv2FsMsdpPeerDataTtl (pu4ErrorCode,
                                           IPVX_ADDR_FMLY_IPV4,
                                           &PeerRemoteAddress,
                                           i4TestValMsdpPeerDataTtl);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2MsdpPeerStatus
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                testValMsdpPeerStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MsdpPeerStatus (UINT4 *pu4ErrorCode, UINT4 u4MsdpPeerRemoteAddress,
                         INT4 i4TestValMsdpPeerStatus)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhTestv2FsMsdpPeerStatus (pu4ErrorCode, IPVX_ADDR_FMLY_IPV4,
                                          &PeerRemoteAddress,
                                          i4TestValMsdpPeerStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2MsdpPeerEncapsulationType
 Input       :  The Indices
                MsdpPeerRemoteAddress

                The Object 
                testValMsdpPeerEncapsulationType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MsdpPeerEncapsulationType (UINT4 *pu4ErrorCode,
                                    UINT4 u4MsdpPeerRemoteAddress,
                                    INT4 i4TestValMsdpPeerEncapsulationType)
{
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&PeerRemoteAddress, 0, sizeof (PeerRemoteAddress));
    u4MsdpPeerRemoteAddress = OSIX_NTOHL (u4MsdpPeerRemoteAddress);
    PeerRemoteAddress.pu1_OctetList = (UINT1 *) &u4MsdpPeerRemoteAddress;
    PeerRemoteAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhTestv2FsMsdpPeerEncapsulationType (pu4ErrorCode,
                                                     IPVX_ADDR_FMLY_IPV4,
                                                     &PeerRemoteAddress,
                                                     i4TestValMsdpPeerEncapsulationType);
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MsdpPeerTable
 Input       :  The Indices
                MsdpPeerRemoteAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MsdpPeerTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MsdpSACacheTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMsdpSACacheTable
 Input       :  The Indices
                MsdpSACacheGroupAddr
                MsdpSACacheSourceAddr
                MsdpSACacheOriginRP
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMsdpSACacheTable (UINT4 u4MsdpSACacheGroupAddr,
                                          UINT4 u4MsdpSACacheSourceAddr,
                                          UINT4 u4MsdpSACacheOriginRP)
{
    tSNMP_OCTET_STRING_TYPE SACacheGroupAddr;
    tSNMP_OCTET_STRING_TYPE SACacheSourceAddr;
    tSNMP_OCTET_STRING_TYPE SACacheOriginRP;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&SACacheGroupAddr, 0, sizeof (SACacheGroupAddr));
    MEMSET (&SACacheSourceAddr, 0, sizeof (SACacheSourceAddr));
    MEMSET (&SACacheOriginRP, 0, sizeof (SACacheOriginRP));
    u4MsdpSACacheGroupAddr = OSIX_NTOHL (u4MsdpSACacheGroupAddr);
    u4MsdpSACacheSourceAddr = OSIX_NTOHL (u4MsdpSACacheSourceAddr);
    u4MsdpSACacheOriginRP = OSIX_NTOHL (u4MsdpSACacheOriginRP);
    SACacheGroupAddr.pu1_OctetList = (UINT1 *) &u4MsdpSACacheGroupAddr;
    SACacheGroupAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheSourceAddr.pu1_OctetList = (UINT1 *) &u4MsdpSACacheSourceAddr;
    SACacheSourceAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheOriginRP.pu1_OctetList = (UINT1 *) &u4MsdpSACacheOriginRP;
    SACacheOriginRP.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhValidateIndexInstanceFsMsdpSACacheTable (IPVX_ADDR_FMLY_IPV4,
                                                           &SACacheGroupAddr,
                                                           &SACacheSourceAddr,
                                                           &SACacheOriginRP);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMsdpSACacheTable
 Input       :  The Indices
                MsdpSACacheGroupAddr
                MsdpSACacheSourceAddr
                MsdpSACacheOriginRP
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMsdpSACacheTable (UINT4 *pu4MsdpSACacheGroupAddr,
                                  UINT4 *pu4MsdpSACacheSourceAddr,
                                  UINT4 *pu4MsdpSACacheOriginRP)
{
    tSNMP_OCTET_STRING_TYPE SACacheGroupAddr;
    tSNMP_OCTET_STRING_TYPE SACacheOriginRP;
    tSNMP_OCTET_STRING_TYPE SACacheSourceAddr;
    INT4                i4FsMsdpSACacheAddrType = IPVX_ADDR_FMLY_IPV4;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&SACacheGroupAddr, 0, sizeof (SACacheGroupAddr));
    MEMSET (&SACacheOriginRP, 0, sizeof (SACacheOriginRP));
    MEMSET (&SACacheSourceAddr, 0, sizeof (SACacheSourceAddr));
    SACacheGroupAddr.pu1_OctetList = (UINT1 *) pu4MsdpSACacheGroupAddr;
    SACacheOriginRP.pu1_OctetList = (UINT1 *) pu4MsdpSACacheOriginRP;
    SACacheSourceAddr.pu1_OctetList = (UINT1 *) pu4MsdpSACacheSourceAddr;
    SACacheGroupAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheSourceAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheOriginRP.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFirstIndexFsMsdpSACacheTable (&i4FsMsdpSACacheAddrType,
                                                   &SACacheGroupAddr,
                                                   &SACacheSourceAddr,
                                                   &SACacheOriginRP);
    *pu4MsdpSACacheGroupAddr = OSIX_NTOHL (*pu4MsdpSACacheGroupAddr);
    *pu4MsdpSACacheSourceAddr = OSIX_NTOHL (*pu4MsdpSACacheSourceAddr);
    *pu4MsdpSACacheOriginRP = OSIX_NTOHL (*pu4MsdpSACacheOriginRP);
    if (i4FsMsdpSACacheAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        return SNMP_FAILURE;
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMsdpSACacheTable
 Input       :  The Indices
                MsdpSACacheGroupAddr
                nextMsdpSACacheGroupAddr
                MsdpSACacheSourceAddr
                nextMsdpSACacheSourceAddr
                MsdpSACacheOriginRP
                nextMsdpSACacheOriginRP
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMsdpSACacheTable (UINT4 u4MsdpSACacheGroupAddr,
                                 UINT4 *pu4NextMsdpSACacheGroupAddr,
                                 UINT4 u4MsdpSACacheSourceAddr,
                                 UINT4 *pu4NextMsdpSACacheSourceAddr,
                                 UINT4 u4MsdpSACacheOriginRP,
                                 UINT4 *pu4NextMsdpSACacheOriginRP)
{
    tSNMP_OCTET_STRING_TYPE SACacheGroupAddr;
    tSNMP_OCTET_STRING_TYPE NextMsdpSACacheGroupAddr;
    tSNMP_OCTET_STRING_TYPE SACacheSourceAddr;
    tSNMP_OCTET_STRING_TYPE NextMsdpSACacheSourceAddr;
    tSNMP_OCTET_STRING_TYPE SACacheOriginRP;
    tSNMP_OCTET_STRING_TYPE NextMsdpSACacheOriginRP;
    INT4                i4FsMsdpSACacheAddrType = IPVX_ADDR_FMLY_IPV4;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&SACacheGroupAddr, 0, sizeof (SACacheGroupAddr));
    MEMSET (&SACacheOriginRP, 0, sizeof (SACacheOriginRP));
    MEMSET (&SACacheSourceAddr, 0, sizeof (SACacheSourceAddr));
    MEMSET (&NextMsdpSACacheGroupAddr, 0, sizeof (NextMsdpSACacheGroupAddr));
    MEMSET (&NextMsdpSACacheOriginRP, 0, sizeof (NextMsdpSACacheOriginRP));
    MEMSET (&NextMsdpSACacheSourceAddr, 0, sizeof (NextMsdpSACacheSourceAddr));
    u4MsdpSACacheGroupAddr = OSIX_NTOHL (u4MsdpSACacheGroupAddr);
    u4MsdpSACacheSourceAddr = OSIX_NTOHL (u4MsdpSACacheSourceAddr);
    u4MsdpSACacheOriginRP = OSIX_NTOHL (u4MsdpSACacheOriginRP);
    SACacheGroupAddr.pu1_OctetList = (UINT1 *) &u4MsdpSACacheGroupAddr;
    SACacheGroupAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    NextMsdpSACacheGroupAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheSourceAddr.pu1_OctetList = (UINT1 *) &u4MsdpSACacheSourceAddr;
    SACacheSourceAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    NextMsdpSACacheSourceAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheOriginRP.pu1_OctetList = (UINT1 *) &u4MsdpSACacheOriginRP;
    SACacheOriginRP.i4_Length = IPVX_IPV4_ADDR_LEN;
    NextMsdpSACacheOriginRP.i4_Length = IPVX_IPV4_ADDR_LEN;
    NextMsdpSACacheGroupAddr.pu1_OctetList =
        (UINT1 *) pu4NextMsdpSACacheGroupAddr;
    NextMsdpSACacheOriginRP.pu1_OctetList =
        (UINT1 *) pu4NextMsdpSACacheOriginRP;
    NextMsdpSACacheSourceAddr.pu1_OctetList =
        (UINT1 *) pu4NextMsdpSACacheSourceAddr;
    i1RetVal =
        nmhGetNextIndexFsMsdpSACacheTable (IPVX_IPV4_ADDR_LEN,
                                           &i4FsMsdpSACacheAddrType,
                                           &SACacheGroupAddr,
                                           &NextMsdpSACacheGroupAddr,
                                           &SACacheSourceAddr,
                                           &NextMsdpSACacheSourceAddr,
                                           &SACacheOriginRP,
                                           &NextMsdpSACacheOriginRP);
    *pu4NextMsdpSACacheGroupAddr = OSIX_NTOHL (*pu4NextMsdpSACacheGroupAddr);
    *pu4NextMsdpSACacheSourceAddr = OSIX_NTOHL (*pu4NextMsdpSACacheSourceAddr);
    *pu4NextMsdpSACacheOriginRP = OSIX_NTOHL (*pu4NextMsdpSACacheOriginRP);
    if (i4FsMsdpSACacheAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        return SNMP_FAILURE;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMsdpSACachePeerLearnedFrom
 Input       :  The Indices
                MsdpSACacheGroupAddr
                MsdpSACacheSourceAddr
                MsdpSACacheOriginRP

                The Object 
                retValMsdpSACachePeerLearnedFrom
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpSACachePeerLearnedFrom (UINT4 u4MsdpSACacheGroupAddr,
                                  UINT4 u4MsdpSACacheSourceAddr,
                                  UINT4 u4MsdpSACacheOriginRP,
                                  UINT4 *pu4RetValMsdpSACachePeerLearnedFrom)
{
    tSNMP_OCTET_STRING_TYPE SACacheGroupAddr;
    tSNMP_OCTET_STRING_TYPE SACacheSourceAddr;
    tSNMP_OCTET_STRING_TYPE SACacheOriginRP;
    tSNMP_OCTET_STRING_TYPE RetValMsdpSACachePeerLearnedFrom;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&SACacheGroupAddr, 0, sizeof (SACacheGroupAddr));
    MEMSET (&SACacheOriginRP, 0, sizeof (SACacheOriginRP));
    MEMSET (&SACacheSourceAddr, 0, sizeof (SACacheSourceAddr));
    MEMSET (&RetValMsdpSACachePeerLearnedFrom, 0,
            sizeof (RetValMsdpSACachePeerLearnedFrom));
    u4MsdpSACacheGroupAddr = OSIX_NTOHL (u4MsdpSACacheGroupAddr);
    u4MsdpSACacheSourceAddr = OSIX_NTOHL (u4MsdpSACacheSourceAddr);
    u4MsdpSACacheOriginRP = OSIX_NTOHL (u4MsdpSACacheOriginRP);
    SACacheGroupAddr.pu1_OctetList = (UINT1 *) &u4MsdpSACacheGroupAddr;
    SACacheGroupAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheSourceAddr.pu1_OctetList = (UINT1 *) &u4MsdpSACacheSourceAddr;
    SACacheSourceAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheOriginRP.pu1_OctetList = (UINT1 *) &u4MsdpSACacheOriginRP;
    SACacheOriginRP.i4_Length = IPVX_IPV4_ADDR_LEN;
    RetValMsdpSACachePeerLearnedFrom.pu1_OctetList =
        (UINT1 *) pu4RetValMsdpSACachePeerLearnedFrom;
    RetValMsdpSACachePeerLearnedFrom.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpSACachePeerLearnedFrom (IPVX_ADDR_FMLY_IPV4,
                                                   &SACacheGroupAddr,
                                                   &SACacheSourceAddr,
                                                   &SACacheOriginRP,
                                                   &RetValMsdpSACachePeerLearnedFrom);
    *pu4RetValMsdpSACachePeerLearnedFrom =
        OSIX_NTOHL (*pu4RetValMsdpSACachePeerLearnedFrom);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpSACacheRPFPeer
 Input       :  The Indices
                MsdpSACacheGroupAddr
                MsdpSACacheSourceAddr
                MsdpSACacheOriginRP

                The Object 
                retValMsdpSACacheRPFPeer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpSACacheRPFPeer (UINT4 u4MsdpSACacheGroupAddr,
                          UINT4 u4MsdpSACacheSourceAddr,
                          UINT4 u4MsdpSACacheOriginRP,
                          UINT4 *pu4RetValMsdpSACacheRPFPeer)
{
    tSNMP_OCTET_STRING_TYPE SACacheGroupAddr;
    tSNMP_OCTET_STRING_TYPE SACacheSourceAddr;
    tSNMP_OCTET_STRING_TYPE SACacheOriginRP;
    tSNMP_OCTET_STRING_TYPE RetValMsdpSACacheRPFPeer;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&SACacheGroupAddr, 0, sizeof (SACacheGroupAddr));
    MEMSET (&SACacheOriginRP, 0, sizeof (SACacheOriginRP));
    MEMSET (&SACacheSourceAddr, 0, sizeof (SACacheSourceAddr));
    MEMSET (&RetValMsdpSACacheRPFPeer, 0, sizeof (RetValMsdpSACacheRPFPeer));
    u4MsdpSACacheGroupAddr = OSIX_NTOHL (u4MsdpSACacheGroupAddr);
    u4MsdpSACacheSourceAddr = OSIX_NTOHL (u4MsdpSACacheSourceAddr);
    u4MsdpSACacheOriginRP = OSIX_NTOHL (u4MsdpSACacheOriginRP);
    SACacheGroupAddr.pu1_OctetList = (UINT1 *) &u4MsdpSACacheGroupAddr;
    SACacheGroupAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheSourceAddr.pu1_OctetList = (UINT1 *) &u4MsdpSACacheSourceAddr;
    SACacheSourceAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheOriginRP.pu1_OctetList = (UINT1 *) &u4MsdpSACacheOriginRP;
    SACacheOriginRP.i4_Length = IPVX_IPV4_ADDR_LEN;
    RetValMsdpSACacheRPFPeer.pu1_OctetList =
        (UINT1 *) pu4RetValMsdpSACacheRPFPeer;
    RetValMsdpSACacheRPFPeer.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpSACacheRPFPeer (IPVX_ADDR_FMLY_IPV4,
                                           &SACacheGroupAddr,
                                           &SACacheSourceAddr, &SACacheOriginRP,
                                           &RetValMsdpSACacheRPFPeer);

    *pu4RetValMsdpSACacheRPFPeer = OSIX_NTOHL (*pu4RetValMsdpSACacheRPFPeer);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpSACacheInSAs
 Input       :  The Indices
                MsdpSACacheGroupAddr
                MsdpSACacheSourceAddr
                MsdpSACacheOriginRP

                The Object 
                retValMsdpSACacheInSAs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpSACacheInSAs (UINT4 u4MsdpSACacheGroupAddr,
                        UINT4 u4MsdpSACacheSourceAddr,
                        UINT4 u4MsdpSACacheOriginRP,
                        UINT4 *pu4RetValMsdpSACacheInSAs)
{
    tSNMP_OCTET_STRING_TYPE SACacheGroupAddr;
    tSNMP_OCTET_STRING_TYPE SACacheSourceAddr;
    tSNMP_OCTET_STRING_TYPE SACacheOriginRP;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&SACacheGroupAddr, 0, sizeof (SACacheGroupAddr));
    MEMSET (&SACacheOriginRP, 0, sizeof (SACacheOriginRP));
    MEMSET (&SACacheSourceAddr, 0, sizeof (SACacheSourceAddr));
    u4MsdpSACacheGroupAddr = OSIX_NTOHL (u4MsdpSACacheGroupAddr);
    u4MsdpSACacheSourceAddr = OSIX_NTOHL (u4MsdpSACacheSourceAddr);
    u4MsdpSACacheOriginRP = OSIX_NTOHL (u4MsdpSACacheOriginRP);
    SACacheGroupAddr.pu1_OctetList = (UINT1 *) &u4MsdpSACacheGroupAddr;
    SACacheGroupAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheSourceAddr.pu1_OctetList = (UINT1 *) &u4MsdpSACacheSourceAddr;
    SACacheSourceAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheOriginRP.pu1_OctetList = (UINT1 *) &u4MsdpSACacheOriginRP;
    SACacheOriginRP.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpSACacheInSAs (IPVX_ADDR_FMLY_IPV4,
                                         &SACacheGroupAddr, &SACacheSourceAddr,
                                         &SACacheOriginRP,
                                         pu4RetValMsdpSACacheInSAs);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpSACacheInDataPackets
 Input       :  The Indices
                MsdpSACacheGroupAddr
                MsdpSACacheSourceAddr
                MsdpSACacheOriginRP

                The Object 
                retValMsdpSACacheInDataPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpSACacheInDataPackets (UINT4 u4MsdpSACacheGroupAddr,
                                UINT4 u4MsdpSACacheSourceAddr,
                                UINT4 u4MsdpSACacheOriginRP,
                                UINT4 *pu4RetValMsdpSACacheInDataPackets)
{
    tSNMP_OCTET_STRING_TYPE SACacheGroupAddr;
    tSNMP_OCTET_STRING_TYPE SACacheSourceAddr;
    tSNMP_OCTET_STRING_TYPE SACacheOriginRP;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&SACacheGroupAddr, 0, sizeof (SACacheGroupAddr));
    MEMSET (&SACacheOriginRP, 0, sizeof (SACacheOriginRP));
    MEMSET (&SACacheSourceAddr, 0, sizeof (SACacheSourceAddr));
    u4MsdpSACacheGroupAddr = OSIX_NTOHL (u4MsdpSACacheGroupAddr);
    u4MsdpSACacheSourceAddr = OSIX_NTOHL (u4MsdpSACacheSourceAddr);
    u4MsdpSACacheOriginRP = OSIX_NTOHL (u4MsdpSACacheOriginRP);
    SACacheGroupAddr.pu1_OctetList = (UINT1 *) &u4MsdpSACacheGroupAddr;
    SACacheGroupAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheSourceAddr.pu1_OctetList = (UINT1 *) &u4MsdpSACacheSourceAddr;
    SACacheSourceAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheOriginRP.pu1_OctetList = (UINT1 *) &u4MsdpSACacheOriginRP;
    SACacheOriginRP.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpSACacheInDataPackets (IPVX_ADDR_FMLY_IPV4,
                                                 &SACacheGroupAddr,
                                                 &SACacheSourceAddr,
                                                 &SACacheOriginRP,
                                                 pu4RetValMsdpSACacheInDataPackets);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpSACacheUpTime
 Input       :  The Indices
                MsdpSACacheGroupAddr
                MsdpSACacheSourceAddr
                MsdpSACacheOriginRP

                The Object 
                retValMsdpSACacheUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpSACacheUpTime (UINT4 u4MsdpSACacheGroupAddr,
                         UINT4 u4MsdpSACacheSourceAddr,
                         UINT4 u4MsdpSACacheOriginRP,
                         UINT4 *pu4RetValMsdpSACacheUpTime)
{
    tSNMP_OCTET_STRING_TYPE SACacheGroupAddr;
    tSNMP_OCTET_STRING_TYPE SACacheSourceAddr;
    tSNMP_OCTET_STRING_TYPE SACacheOriginRP;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&SACacheGroupAddr, 0, sizeof (SACacheGroupAddr));
    MEMSET (&SACacheOriginRP, 0, sizeof (SACacheOriginRP));
    MEMSET (&SACacheSourceAddr, 0, sizeof (SACacheSourceAddr));
    u4MsdpSACacheGroupAddr = OSIX_NTOHL (u4MsdpSACacheGroupAddr);
    u4MsdpSACacheSourceAddr = OSIX_NTOHL (u4MsdpSACacheSourceAddr);
    u4MsdpSACacheOriginRP = OSIX_NTOHL (u4MsdpSACacheOriginRP);
    SACacheGroupAddr.pu1_OctetList = (UINT1 *) &u4MsdpSACacheGroupAddr;
    SACacheGroupAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheSourceAddr.pu1_OctetList = (UINT1 *) &u4MsdpSACacheSourceAddr;
    SACacheSourceAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheOriginRP.pu1_OctetList = (UINT1 *) &u4MsdpSACacheOriginRP;
    SACacheOriginRP.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpSACacheUpTime (IPVX_ADDR_FMLY_IPV4,
                                          &SACacheGroupAddr, &SACacheSourceAddr,
                                          &SACacheOriginRP,
                                          pu4RetValMsdpSACacheUpTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpSACacheExpiryTime
 Input       :  The Indices
                MsdpSACacheGroupAddr
                MsdpSACacheSourceAddr
                MsdpSACacheOriginRP

                The Object 
                retValMsdpSACacheExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpSACacheExpiryTime (UINT4 u4MsdpSACacheGroupAddr,
                             UINT4 u4MsdpSACacheSourceAddr,
                             UINT4 u4MsdpSACacheOriginRP,
                             UINT4 *pu4RetValMsdpSACacheExpiryTime)
{
    tSNMP_OCTET_STRING_TYPE SACacheGroupAddr;
    tSNMP_OCTET_STRING_TYPE SACacheSourceAddr;
    tSNMP_OCTET_STRING_TYPE SACacheOriginRP;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&SACacheGroupAddr, 0, sizeof (SACacheGroupAddr));
    MEMSET (&SACacheOriginRP, 0, sizeof (SACacheOriginRP));
    MEMSET (&SACacheSourceAddr, 0, sizeof (SACacheSourceAddr));
    u4MsdpSACacheGroupAddr = OSIX_NTOHL (u4MsdpSACacheGroupAddr);
    u4MsdpSACacheSourceAddr = OSIX_NTOHL (u4MsdpSACacheSourceAddr);
    u4MsdpSACacheOriginRP = OSIX_NTOHL (u4MsdpSACacheOriginRP);
    SACacheGroupAddr.pu1_OctetList = (UINT1 *) &u4MsdpSACacheGroupAddr;
    SACacheGroupAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheSourceAddr.pu1_OctetList = (UINT1 *) &u4MsdpSACacheSourceAddr;
    SACacheSourceAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheOriginRP.pu1_OctetList = (UINT1 *) &u4MsdpSACacheOriginRP;
    SACacheOriginRP.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpSACacheExpiryTime (IPVX_ADDR_FMLY_IPV4,
                                              &SACacheGroupAddr,
                                              &SACacheSourceAddr,
                                              &SACacheOriginRP,
                                              pu4RetValMsdpSACacheExpiryTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMsdpSACacheStatus
 Input       :  The Indices
                MsdpSACacheGroupAddr
                MsdpSACacheSourceAddr
                MsdpSACacheOriginRP

                The Object 
                retValMsdpSACacheStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpSACacheStatus (UINT4 u4MsdpSACacheGroupAddr,
                         UINT4 u4MsdpSACacheSourceAddr,
                         UINT4 u4MsdpSACacheOriginRP,
                         INT4 *pi4RetValMsdpSACacheStatus)
{
    tSNMP_OCTET_STRING_TYPE SACacheGroupAddr;
    tSNMP_OCTET_STRING_TYPE SACacheOriginRP;
    tSNMP_OCTET_STRING_TYPE SACacheSourceAddr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&SACacheGroupAddr, 0, sizeof (SACacheGroupAddr));
    MEMSET (&SACacheOriginRP, 0, sizeof (SACacheOriginRP));
    MEMSET (&SACacheSourceAddr, 0, sizeof (SACacheSourceAddr));
    u4MsdpSACacheGroupAddr = OSIX_NTOHL (u4MsdpSACacheGroupAddr);
    u4MsdpSACacheSourceAddr = OSIX_NTOHL (u4MsdpSACacheSourceAddr);
    u4MsdpSACacheOriginRP = OSIX_NTOHL (u4MsdpSACacheOriginRP);
    SACacheGroupAddr.pu1_OctetList = (UINT1 *) &u4MsdpSACacheGroupAddr;
    SACacheGroupAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheSourceAddr.pu1_OctetList = (UINT1 *) &u4MsdpSACacheSourceAddr;
    SACacheSourceAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheOriginRP.pu1_OctetList = (UINT1 *) &u4MsdpSACacheOriginRP;
    SACacheOriginRP.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpSACacheStatus (IPVX_ADDR_FMLY_IPV4,
                                          &SACacheGroupAddr, &SACacheSourceAddr,
                                          &SACacheOriginRP,
                                          pi4RetValMsdpSACacheStatus);
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMsdpSACacheStatus
 Input       :  The Indices
                MsdpSACacheGroupAddr
                MsdpSACacheSourceAddr
                MsdpSACacheOriginRP

                The Object 
                setValMsdpSACacheStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMsdpSACacheStatus (UINT4 u4MsdpSACacheGroupAddr,
                         UINT4 u4MsdpSACacheSourceAddr,
                         UINT4 u4MsdpSACacheOriginRP,
                         INT4 i4SetValMsdpSACacheStatus)
{
    tSNMP_OCTET_STRING_TYPE SACacheGroupAddr;
    tSNMP_OCTET_STRING_TYPE SACacheOriginRP;
    tSNMP_OCTET_STRING_TYPE SACacheSourceAddr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&SACacheGroupAddr, 0, sizeof (SACacheGroupAddr));
    MEMSET (&SACacheOriginRP, 0, sizeof (SACacheOriginRP));
    MEMSET (&SACacheSourceAddr, 0, sizeof (SACacheSourceAddr));
    u4MsdpSACacheGroupAddr = OSIX_NTOHL (u4MsdpSACacheGroupAddr);
    u4MsdpSACacheSourceAddr = OSIX_NTOHL (u4MsdpSACacheSourceAddr);
    u4MsdpSACacheOriginRP = OSIX_NTOHL (u4MsdpSACacheOriginRP);
    SACacheGroupAddr.pu1_OctetList = (UINT1 *) &u4MsdpSACacheGroupAddr;
    SACacheGroupAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheSourceAddr.pu1_OctetList = (UINT1 *) &u4MsdpSACacheSourceAddr;
    SACacheSourceAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheOriginRP.pu1_OctetList = (UINT1 *) &u4MsdpSACacheOriginRP;
    SACacheOriginRP.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhSetFsMsdpSACacheStatus (IPVX_ADDR_FMLY_IPV4,
                                          &SACacheGroupAddr, &SACacheSourceAddr,
                                          &SACacheOriginRP,
                                          i4SetValMsdpSACacheStatus);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MsdpSACacheStatus
 Input       :  The Indices
                MsdpSACacheGroupAddr
                MsdpSACacheSourceAddr
                MsdpSACacheOriginRP

                The Object 
                testValMsdpSACacheStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MsdpSACacheStatus (UINT4 *pu4ErrorCode, UINT4 u4MsdpSACacheGroupAddr,
                            UINT4 u4MsdpSACacheSourceAddr,
                            UINT4 u4MsdpSACacheOriginRP,
                            INT4 i4TestValMsdpSACacheStatus)
{
    tSNMP_OCTET_STRING_TYPE SACacheGroupAddr;
    tSNMP_OCTET_STRING_TYPE SACacheOriginRP;
    tSNMP_OCTET_STRING_TYPE SACacheSourceAddr;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&SACacheGroupAddr, 0, sizeof (SACacheGroupAddr));
    MEMSET (&SACacheOriginRP, 0, sizeof (SACacheOriginRP));
    MEMSET (&SACacheSourceAddr, 0, sizeof (SACacheSourceAddr));
    u4MsdpSACacheGroupAddr = OSIX_NTOHL (u4MsdpSACacheGroupAddr);
    u4MsdpSACacheSourceAddr = OSIX_NTOHL (u4MsdpSACacheSourceAddr);
    u4MsdpSACacheOriginRP = OSIX_NTOHL (u4MsdpSACacheOriginRP);
    SACacheGroupAddr.pu1_OctetList = (UINT1 *) &u4MsdpSACacheGroupAddr;
    SACacheGroupAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheSourceAddr.pu1_OctetList = (UINT1 *) &u4MsdpSACacheSourceAddr;
    SACacheSourceAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    SACacheOriginRP.pu1_OctetList = (UINT1 *) &u4MsdpSACacheOriginRP;
    SACacheOriginRP.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhTestv2FsMsdpSACacheStatus (pu4ErrorCode,
                                             IPVX_ADDR_FMLY_IPV4,
                                             &SACacheGroupAddr,
                                             &SACacheSourceAddr,
                                             &SACacheOriginRP,
                                             i4TestValMsdpSACacheStatus);
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MsdpSACacheTable
 Input       :  The Indices
                dpSACacheOriginRPsdpSACacheGroupAddr
                MsdpSACacheSourceAddr
                MsdpSACacheOriginRP
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MsdpSACacheTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MsdpMeshGroupTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMsdpMeshGroupTable
 Input       :  The Indices
                MsdpMeshGroupName
                MsdpMeshGroupPeerAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMsdpMeshGroupTable (tSNMP_OCTET_STRING_TYPE *
                                            pMsdpMeshGroupName,
                                            UINT4 u4MsdpMeshGroupPeerAddress)
{
    tSNMP_OCTET_STRING_TYPE MeshGroupPeerAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&MeshGroupPeerAddress, 0, sizeof (MeshGroupPeerAddress));
    u4MsdpMeshGroupPeerAddress = OSIX_NTOHL (u4MsdpMeshGroupPeerAddress);
    MeshGroupPeerAddress.pu1_OctetList = (UINT1 *) &u4MsdpMeshGroupPeerAddress;
    MeshGroupPeerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhValidateIndexInstanceFsMsdpMeshGroupTable (pMsdpMeshGroupName,
                                                             IPVX_ADDR_FMLY_IPV4,
                                                             &MeshGroupPeerAddress);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMsdpMeshGroupTable
 Input       :  The Indices
                MsdpMeshGroupName
                MsdpMeshGroupPeerAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMsdpMeshGroupTable (tSNMP_OCTET_STRING_TYPE *
                                    pMsdpMeshGroupName,
                                    UINT4 *pu4MsdpMeshGroupPeerAddress)
{
    tSNMP_OCTET_STRING_TYPE MeshGroupPeerAddress;
    INT4                i4FsMsdpMeshGroupAddrType = IPVX_ADDR_FMLY_IPV4;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&MeshGroupPeerAddress, 0, sizeof (MeshGroupPeerAddress));
    MeshGroupPeerAddress.pu1_OctetList = (UINT1 *) pu4MsdpMeshGroupPeerAddress;
    MeshGroupPeerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFirstIndexFsMsdpMeshGroupTable (pMsdpMeshGroupName,
                                                     &i4FsMsdpMeshGroupAddrType,
                                                     &MeshGroupPeerAddress);
    *pu4MsdpMeshGroupPeerAddress = OSIX_NTOHL (*pu4MsdpMeshGroupPeerAddress);
    if (i4FsMsdpMeshGroupAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        return SNMP_FAILURE;
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMsdpMeshGroupTable
 Input       :  The Indices
                MsdpMeshGroupName
                nextMsdpMeshGroupName
                MsdpMeshGroupPeerAddress
                nextMsdpMeshGroupPeerAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMsdpMeshGroupTable (tSNMP_OCTET_STRING_TYPE * pMsdpMeshGroupName,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextMsdpMeshGroupName,
                                   UINT4 u4MsdpMeshGroupPeerAddress,
                                   UINT4 *pu4NextMsdpMeshGroupPeerAddress)
{
    tSNMP_OCTET_STRING_TYPE MeshGroupPeerAddress;
    tSNMP_OCTET_STRING_TYPE NextMsdpMeshGroupPeerAddress;
    INT4                i4FsMsdpMeshGroupAddrType = IPVX_ADDR_FMLY_IPV4;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&MeshGroupPeerAddress, 0, sizeof (MeshGroupPeerAddress));
    MEMSET (&NextMsdpMeshGroupPeerAddress, 0,
            sizeof (NextMsdpMeshGroupPeerAddress));
    u4MsdpMeshGroupPeerAddress = OSIX_NTOHL (u4MsdpMeshGroupPeerAddress);
    MeshGroupPeerAddress.pu1_OctetList = (UINT1 *) &u4MsdpMeshGroupPeerAddress;
    MeshGroupPeerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    NextMsdpMeshGroupPeerAddress.pu1_OctetList =
        (UINT1 *) pu4NextMsdpMeshGroupPeerAddress;
    NextMsdpMeshGroupPeerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetNextIndexFsMsdpMeshGroupTable (pMsdpMeshGroupName,
                                                    pNextMsdpMeshGroupName,
                                                    IPVX_ADDR_FMLY_IPV4,
                                                    &i4FsMsdpMeshGroupAddrType,
                                                    &MeshGroupPeerAddress,
                                                    &NextMsdpMeshGroupPeerAddress);
    *pu4NextMsdpMeshGroupPeerAddress =
        OSIX_NTOHL (*pu4NextMsdpMeshGroupPeerAddress);
    if (i4FsMsdpMeshGroupAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        return SNMP_FAILURE;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMsdpMeshGroupStatus
 Input       :  The Indices
                MsdpMeshGroupName
                MsdpMeshGroupPeerAddress

                The Object 
                retValMsdpMeshGroupStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMsdpMeshGroupStatus (tSNMP_OCTET_STRING_TYPE * pMsdpMeshGroupName,
                           UINT4 u4MsdpMeshGroupPeerAddress,
                           INT4 *pi4RetValMsdpMeshGroupStatus)
{
    tSNMP_OCTET_STRING_TYPE MeshGroupPeerAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&MeshGroupPeerAddress, 0, sizeof (MeshGroupPeerAddress));
    u4MsdpMeshGroupPeerAddress = OSIX_NTOHL (u4MsdpMeshGroupPeerAddress);
    MeshGroupPeerAddress.pu1_OctetList = (UINT1 *) &u4MsdpMeshGroupPeerAddress;
    MeshGroupPeerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhGetFsMsdpMeshGroupStatus (pMsdpMeshGroupName,
                                            IPVX_ADDR_FMLY_IPV4,
                                            &MeshGroupPeerAddress,
                                            pi4RetValMsdpMeshGroupStatus);
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMsdpMeshGroupStatus
 Input       :  The Indices
                MsdpMeshGroupName
                MsdpMeshGroupPeerAddress

                The Object 
                setValMsdpMeshGroupStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMsdpMeshGroupStatus (tSNMP_OCTET_STRING_TYPE * pMsdpMeshGroupName,
                           UINT4 u4MsdpMeshGroupPeerAddress,
                           INT4 i4SetValMsdpMeshGroupStatus)
{
    tSNMP_OCTET_STRING_TYPE MeshGroupPeerAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&MeshGroupPeerAddress, 0, sizeof (MeshGroupPeerAddress));
    u4MsdpMeshGroupPeerAddress = OSIX_NTOHL (u4MsdpMeshGroupPeerAddress);
    MeshGroupPeerAddress.pu1_OctetList = (UINT1 *) &u4MsdpMeshGroupPeerAddress;
    MeshGroupPeerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhSetFsMsdpMeshGroupStatus (pMsdpMeshGroupName,
                                            IPVX_ADDR_FMLY_IPV4,
                                            &MeshGroupPeerAddress,
                                            i4SetValMsdpMeshGroupStatus);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MsdpMeshGroupStatus
 Input       :  The Indices
                MsdpMeshGroupName
                MsdpMeshGroupPeerAddress

                The Object 
                testValMsdpMeshGroupStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MsdpMeshGroupStatus (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pMsdpMeshGroupName,
                              UINT4 u4MsdpMeshGroupPeerAddress,
                              INT4 i4TestValMsdpMeshGroupStatus)
{
    tSNMP_OCTET_STRING_TYPE MeshGroupPeerAddress;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&MeshGroupPeerAddress, 0, sizeof (MeshGroupPeerAddress));
    u4MsdpMeshGroupPeerAddress = OSIX_NTOHL (u4MsdpMeshGroupPeerAddress);
    MeshGroupPeerAddress.pu1_OctetList = (UINT1 *) &u4MsdpMeshGroupPeerAddress;
    MeshGroupPeerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    i1RetVal = nmhTestv2FsMsdpMeshGroupStatus (pu4ErrorCode,
                                               pMsdpMeshGroupName,
                                               IPVX_ADDR_FMLY_IPV4,
                                               &MeshGroupPeerAddress,
                                               i4TestValMsdpMeshGroupStatus);
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MsdpMeshGroupTable
 Input       :  The Indices
                MsdpMeshGroupName
                MsdpMeshGroupPeerAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MsdpMeshGroupTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
