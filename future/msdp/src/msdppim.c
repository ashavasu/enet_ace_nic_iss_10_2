/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: msdppim.c,v 1.5 2012/01/27 13:26:32 siva Exp $
 * *
 * * Description: This file contains the Msdp PIM Interface related routines
 * *********************************************************************/
#include "msdpinc.h"

/****************************************************************************
 * FUNCTION NAME : MsdpPimIfExtractSgFromBuf
 *
 * DESCRIPTION   : This function is called when the MRP (PIM) module receives
 *                 multicast data from source. MRP module indicates MSDP about 
 *                 S,G for the multicast data. This S,G info should be copied 
 *                 in the MSDP cache Table
 *
 * INPUTS        : pPimSaInfo- SA info from PIM
 *                 pMsdpCache - Pointer to Cache Table Entry to fill the S,G
 *                 Info
 *
 * OUTPUTS       : None
 *
 * RETURNS       : None
 *
 ****************************************************************************/

VOID
MsdpPimIfExtractSGFromBuf (tMsdpMrpSaAdvt * pPimSaInfo,
                           tMsdpCacheUpdt * pMsdpCache)
{
    UINT1               u1AddrLen = sizeof (tIPvXAddr);

    MSDP_TRC_FUNC ((MSDP_TRC_PIMIF | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPimIfExtractSgFromBuf"));

    pMsdpCache->pLrndFrmPeer = NULL;
    MEMSET (&pMsdpCache->OriginatorRP, 0, u1AddrLen);
    MEMSET (&pMsdpCache->GrpAddr, 0, u1AddrLen);
    MEMSET (&pMsdpCache->SrcAddr, 0, u1AddrLen);

    CRU_BUF_Copy_FromBufChain (pPimSaInfo->pGrpSrcAddrList,
                               (UINT1 *) &pMsdpCache->GrpAddr, 0, u1AddrLen);

    CRU_BUF_Copy_FromBufChain (pPimSaInfo->pGrpSrcAddrList,
                               (UINT1 *) &pMsdpCache->SrcAddr, u1AddrLen,
                               u1AddrLen);

    pMsdpCache->OriginatorRP.u1AddrLen = pMsdpCache->GrpAddr.u1AddrLen;
    pMsdpCache->i1Flag = MSDP_PIM_CACHE;
    pMsdpCache->u1Action = pPimSaInfo->u1Action;
    pMsdpCache->u1AddrType = pPimSaInfo->u1AddrType;

    MSDP_TRC_FUNC ((MSDP_TRC_PIMIF | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPimIfExtractSgFromBuf"));

    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPimIfHandleSrcActiveInfo
 *
 * DESCRIPTION   : This function is called when the MRP (PIM) module receives
 *                 multicast data from source. MRP module indicates MSDP about 
 *                 S,G for the multicast data. This function updates the 
 *                 Cache Table with the received info and send SA Advt Msg to 
 *                 the MSDP Peers
 *
 * OUTPUTS       : pPimSaInfo- S,G Info and data uptained from MRP
 *
 * RETURNS       : OSIX_SUCCESS if the component IDs match
 *                 OSIX_FAILURE otherwise
 *
 ****************************************************************************/
INT4
MsdpPimIfHandleSAInfo (tMsdpMrpSaAdvt * pPimSaInfo)
{
    tMsdpCacheUpdt      MsdpCacheEntry;
    UINT4               u4IsDataPkt = 0;
    INT4                i4RetVal = 0;
    INT4                i4FsMsdpIPv4AdminStat = 0;
    INT4                i4FsMsdpIPv6AdminStat = 0;
    UINT1               u1CompId = 0;
    UINT1               u1Status = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_PIMIF | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPimIfHandleSrcActiveInfo"));

    MSDP_TRC_FUNC ((MSDP_TRC_PIMIF, "\nMsdpPimIfHandleSrcActiveInfo: "
                    "Rcvd Info from MRP (PIM)"));

    MsdpGetFsMsdpIPv4AdminStat (&i4FsMsdpIPv4AdminStat);
    MsdpGetFsMsdpIPv6AdminStat (&i4FsMsdpIPv6AdminStat);

    if (((pPimSaInfo->u1AddrType == IPVX_ADDR_FMLY_IPV4) &&
         (i4FsMsdpIPv4AdminStat == MSDP_DISABLED)) ||
        ((pPimSaInfo->u1AddrType == IPVX_ADDR_FMLY_IPV6) &&
         (i4FsMsdpIPv6AdminStat == MSDP_DISABLED)))
    {
        return OSIX_FAILURE;
    }

    MsdpGetFsMsdpMappingComponentId ((INT4 *) (VOID *) &u1CompId);

    if (u1CompId != pPimSaInfo->u1CompId)
    {
        CRU_BUF_Release_MsgBufChain (pPimSaInfo->pGrpSrcAddrList, FALSE);
        CRU_BUF_Release_MsgBufChain (pPimSaInfo->pDataPkt, FALSE);

        MSDP_TRC_FUNC ((MSDP_TRC_PIMIF, "\nMsdpPimIfHandleSrcActiveInfo: "
                        "The obtained component ID %d does not match with the "
                        "Component ID configured %d", pPimSaInfo->u1CompId,
                        u1CompId));

        return OSIX_FAILURE;
    }

    MsdpPimIfExtractSGFromBuf (pPimSaInfo, &MsdpCacheEntry);

    MSDP_TRC_FUNC ((MSDP_TRC_PIMIF, "\nMsdpPimIfHandleSrcActiveInfo: "
                    "S,G Info extracted from pPimSaInfo"));

    if (pPimSaInfo->pDataPkt != NULL)
    {
        u4IsDataPkt = 1;
    }

    i4RetVal = MsdCacheApplySAFilter (&MsdpCacheEntry);
    if (i4RetVal == OSIX_SUCCESS)
    {
        i4RetVal = MsdCacheUpdateCacheTbl (MsdpCacheEntry, u4IsDataPkt,
                                           &u1Status);

        MSDP_TRC_FUNC ((MSDP_TRC_PIMIF, "\nMsdpPimIfHandleSrcActiveInfo: "
                        "Cache Table updated with Rcvd S,G Info"));

        if (u1Status == MSDP_CACHE_NEW)
        {

            MSDP_TRC_FUNC ((MSDP_TRC_PIMIF, "\nMsdpPimIfHandleSrcActiveInfo: "
                            "New Cache Entry Rcvd"));

            i4RetVal = MsdCacheAmIOriginator (pPimSaInfo->u1AddrType,
                                              MsdpCacheEntry.OriginatorRP.
                                              u1AddrLen,
                                              MsdpCacheEntry.OriginatorRP.
                                              au1Addr);
            if (OSIX_TRUE == i4RetVal)
            {

                MsdCacheOriginateSAMsg (&MsdpCacheEntry, pPimSaInfo->pDataPkt,
                                        pPimSaInfo->u2DataPktLen);

                MSDP_TRC_FUNC ((MSDP_TRC_PIMIF,
                                "\nMsdpPimIfHandleSAInfo: "
                                "Fwded the new Cache Entry to all MSDP Peers"));
            }
            else
            {

                MSDP_TRC_FUNC ((MSDP_TRC_PIMIF,
                                "\nMsdpPimIfHandleSrcActiveInfo: Not fwded"
                                "the new Cache Entry to all MSDP Peers"));
            }
        }
        else
        {
            MSDP_TRC_FUNC ((MSDP_TRC_PIMIF,
                            "\nMsdpPimIfHandleSrcActiveInfo: Not fwded"
                            "the new Cache Entry to all MSDP Peers"));
        }
    }
    else
    {
        MSDP_TRC_FUNC ((MSDP_TRC_PIMIF,
                        "\nMsdpPimIfHandleSrcActiveInfo: Not fwded"
                        "the new Cache Entry to all MSDP Peers"));
    }

    CRU_BUF_Release_MsgBufChain (pPimSaInfo->pGrpSrcAddrList, FALSE);
    CRU_BUF_Release_MsgBufChain (pPimSaInfo->pDataPkt, FALSE);

    MSDP_TRC_FUNC ((MSDP_TRC_PIMIF | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPimIfHandleSrcActiveInfo"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPimIfFillSAEntry
 *
 * DESCRIPTION   : This function fills in the SA Entry
 *
 * INPUTS        : pGrpAddr- Group address
 *                 pSrcAddr- Source address
 *                 pi4Offset- Offset
 *
 * OUTPUTS       : pi4Offset- Offset
 *                 pMsdpSaInfo- u2SrcCount is modified
 *
 * RETURNS       : OSIX_SUCCESS on filling the SA entry successfully
 *                 OSIX_FAILURE otherwise 
 *
 ****************************************************************************/
INT4
MsdpPimIfFillSAEntry (tMsdpMrpSaAdvt * pMsdpSaInfo,
                      tIPvXAddr * pGrpAddr, tIPvXAddr * pSrcAddr,
                      INT4 *pi4Offset)
{

    UINT4               u4Len = sizeof (tIPvXAddr);

    MSDP_TRC_FUNC ((MSDP_TRC_PIMIF | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPimIfHandleSrcActiveInfo"));

    /* Copy the Group address to the CRU buffer */
    if (CRU_BUF_Copy_OverBufChain (pMsdpSaInfo->pGrpSrcAddrList,
                                   (UINT1 *) pGrpAddr,
                                   *pi4Offset, u4Len) == CRU_FAILURE)
    {
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pMsdpSaInfo->pGrpSrcAddrList, FALSE);

        MSDP_TRC_FUNC ((MSDP_TRC_PIMIF, "\nMsdpPimIfHandleSrcActiveInfo: "
                        "CRU_BUF_Copy_OverBufChain returns failure"));

        return OSIX_FAILURE;
    }
    *pi4Offset += u4Len;

    /* Copy the Source address to the CRU buffer */
    if (CRU_BUF_Copy_OverBufChain (pMsdpSaInfo->pGrpSrcAddrList,
                                   (UINT1 *) pSrcAddr,
                                   *pi4Offset, u4Len) == CRU_FAILURE)
    {
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pMsdpSaInfo->pGrpSrcAddrList, FALSE);

        MSDP_TRC_FUNC ((MSDP_TRC_PIMIF, "\nMsdpPimIfHandleSrcActiveInfo: "
                        "CRU_BUF_Copy_OverBufChain returns failure"));

        return OSIX_FAILURE;
    }

    *pi4Offset += u4Len;
    pMsdpSaInfo->u2SrcCount++;

    MSDP_TRC_FUNC ((MSDP_TRC_PIMIF | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPimIfHandleSrcActiveInfo"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPimIfAllocateBuf
 *
 * DESCRIPTION   : This function allocates buffer
 *
 * INPUTS        : pMsdpSAInfo- SA info
 *                 i4BufSize- Buffer size
 *                 u1AddrType- Address type
 *
 * OUTPUTS       : None
 *
 * RETURNS       : OSIX_SUCCESS if msg buf chain is allocated
 *                 OSIX_FAILURE otherwise
 *
 ****************************************************************************/
INT4
MsdpPimIfAllocateBuf (tMsdpMrpSaAdvt * pMsdpSAInfo,
                      INT4 i4BufSize, UINT1 u1AddrType)
{

    MSDP_TRC_FUNC ((MSDP_TRC_PIMIF | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPimIfAllocateBuf"));

    pMsdpSAInfo->pGrpSrcAddrList = CRU_BUF_Allocate_MsgBufChain (i4BufSize, 0);
    /* Allocation Fails */
    if (pMsdpSAInfo->pGrpSrcAddrList == NULL)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_PIMIF, "\nMsdpPimIfAllocateBuf: "
                        "CRU_BUF_Allocate_MsgBufChain returns NULL"));

        return OSIX_FAILURE;
    }
    pMsdpSAInfo->pDataPkt = NULL;
    pMsdpSAInfo->u2DataPktLen = 0;
    pMsdpSAInfo->u1AddrType = u1AddrType;
    pMsdpSAInfo->u2SrcCount = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_PIMIF | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPimIfAllocateBuf"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPimGetNextCacheWithReqGrp
 *
 * DESCRIPTION   : This function obtains the next cache from the required group
 *
 * INPUTS        : pCurCache- The current cache
 *                 pGrpAddr- Group address
 *
 * OUTPUTS       : None
 *
 * RETURNS       : Pointer to the next cache if successfully obtained
 *                 NULL otherwise
 *
 ****************************************************************************/
tMsdpFsMsdpSACacheEntry *
MsdpPimGetNextCacheWithReqGrp (tMsdpFsMsdpSACacheEntry * pCurCache,
                               tIPvXAddr * pGrpAddr)
{
    tMsdpFsMsdpSACacheEntry *pNextCache = NULL;

    MSDP_TRC_FUNC ((MSDP_TRC_PIMIF | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPimGetNextCacheWithReqGrp"));

    pNextCache = MsdpGetNextFsMsdpSACacheTable (pCurCache);
    if (pNextCache == NULL)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_PIMIF, "\nMsdpPimGetNextCacheWithReqGrp: "
                        "No next entry in the SACacheTable"));

        return NULL;
    }

    if (0 != MEMCMP (pNextCache->MibObject.au1FsMsdpSACacheGroupAddr,
                     pGrpAddr->au1Addr, pGrpAddr->u1AddrLen))
    {

        MSDP_TRC_FUNC ((MSDP_TRC_PIMIF, "\nMsdpPimGetNextCacheWithReqGrp: "
                        "The passed address and the address ret by "
                        "MsdpGetNextFsMsdpSACacheTable do not match"));

        return NULL;
    }

    MSDP_TRC_FUNC ((MSDP_TRC_PIMIF | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPimGetNextCacheWithReqGrp"));

    return pNextCache;
}

/****************************************************************************
 * FUNCTION NAME : MsdpPimIfHandleSrcActiveReq
 *
 * DESCRIPTION   : This function handles S,G request info received from
 *                 MRP (PIM). PIM call post msg to MSDP when it identifies 
 *                 a receiver for a Grp.Src or Grp. This function 
 *                 seaches the MSDP Cache Table for the requested Grp,Src
 *                 or Grp Addr. 
 *                 If the Entry available in MSDP Cache table passes this
 *                 information to PIM
 *                 else, Sends MSDP SA request msg to all MSDP Peers
 *
 * INPUTS        : pReqGrpAddr- group address for which a receiver is joined
 *
 * OUTPUTS       : None
 *
 * RETURNS       : OSIX_SUCCESS if component Ids match and buffer allocation 
 *                 is successful
 *                 OSIX_FAILURE otherwise
 *
 ****************************************************************************/
INT4
MsdpPimIfHandleSAReq (tMsdpSAReqGrp * pReqGrpAddr)
{
    tMsdpFsMsdpSACacheEntry CurSaCache;
    tMsdpFsMsdpSACacheEntry *pNextSaCache = NULL;
    tMsdpMrpSaAdvt      MsdpSaInfo;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    INT4                i4RetVal = 0;
    INT4                i4Offset = 0;
    INT4                i4FsMsdpIPv4AdminStat = 0;
    INT4                i4FsMsdpIPv6AdminStat = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_PIMIF | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpPimIfHandleSrcActiveReq"));

    MSDP_TRC_FUNC ((MSDP_TRC_PIMIF, "\nMsdpPimIfHandleSrcActiveReq: "
                    "Rcvd Req from MRP (PIM) for Grp %s",
                    pReqGrpAddr->GrpAddr));

    MsdpGetFsMsdpIPv4AdminStat (&i4FsMsdpIPv4AdminStat);
    MsdpGetFsMsdpIPv6AdminStat (&i4FsMsdpIPv6AdminStat);

    if (((pReqGrpAddr->u1AddrType == IPVX_ADDR_FMLY_IPV4) &&
         (i4FsMsdpIPv4AdminStat == MSDP_DISABLED)) ||
        ((pReqGrpAddr->u1AddrType == IPVX_ADDR_FMLY_IPV6) &&
         (i4FsMsdpIPv6AdminStat == MSDP_DISABLED)))
    {
        return OSIX_FAILURE;
    }

    MEMSET (&CurSaCache, 0, sizeof (tMsdpFsMsdpSACacheEntry));

    MsdpGetFsMsdpMappingComponentId ((INT4 *) (VOID *) &MsdpSaInfo.u1CompId);

    if (MsdpSaInfo.u1CompId != pReqGrpAddr->u1CompId)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_PIMIF, "\nMsdpPimIfHandleSrcActiveReq: "
                        "The passed component ID %d does not match the "
                        "one configured %d", pReqGrpAddr->u1CompId,
                        MsdpSaInfo.u1CompId));

        return OSIX_FAILURE;
    }

    i4RetVal = MsdpPimIfAllocateBuf (&MsdpSaInfo, MSDP_MAX_GRPSRC_BUF_SIZE,
                                     pReqGrpAddr->u1AddrType);
    if (i4RetVal == OSIX_FAILURE)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_PIMIF, "\nMsdpPimIfHandleSrcActiveReq: "
                        "MsdpPimIfAllocateBuf returns failure"));

        return OSIX_FAILURE;
    }

    CurSaCache.MibObject.i4FsMsdpSACacheAddrType = pReqGrpAddr->u1AddrType;
    CurSaCache.MibObject.i4FsMsdpSACacheGroupAddrLen = IPVX_MAX_INET_ADDR_LEN;
    MEMCPY (&CurSaCache.MibObject.au1FsMsdpSACacheGroupAddr,
            pReqGrpAddr->GrpAddr.au1Addr, pReqGrpAddr->GrpAddr.u1AddrLen);

    while (OSIX_TRUE == OSIX_TRUE)
    {
        pNextSaCache = MsdpPimGetNextCacheWithReqGrp (&CurSaCache,
                                                      &pReqGrpAddr->GrpAddr);
        if (pNextSaCache == NULL)
        {
            break;
        }

        MEMCPY (&CurSaCache, pNextSaCache, sizeof (CurSaCache));

        IPVX_ADDR_INIT (GrpAddr, pReqGrpAddr->u1AddrType,
                        pNextSaCache->MibObject.au1FsMsdpSACacheGroupAddr);
        IPVX_ADDR_INIT (SrcAddr, pReqGrpAddr->u1AddrType,
                        pNextSaCache->MibObject.au1FsMsdpSACacheSourceAddr);

        MsdpPimIfFillSAEntry (&MsdpSaInfo, &GrpAddr, &SrcAddr, &i4Offset);

    }
    if (MsdpSaInfo.u2SrcCount)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_PIMIF, "\nMsdpPimIfHandleSrcActiveReq: "
                        "Entry found in the MSDP Cache. Passing the info to PIM"));
        MsdpPortInformSAToMrp (&MsdpSaInfo);
    }
    else
    {
        MSDP_TRC_FUNC ((MSDP_TRC_PIMIF, "\nMsdpPimIfHandleSrcActiveReq: "
                        "Entry Not found in the MSDP Cache. Sending SA Req"));
        MsdCacheSendSARequest (pReqGrpAddr);
        CRU_BUF_Release_MsgBufChain (MsdpSaInfo.pGrpSrcAddrList, FALSE);
    }

    MSDP_TRC_FUNC ((MSDP_TRC_PIMIF | MSDP_FN_EXIT,
                    "\nEXIT: MsdpPimIfHandleSrcActiveReq"));

    return OSIX_SUCCESS;
}
