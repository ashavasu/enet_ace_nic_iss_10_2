/******************************************************************************
 * Copyright (C) Future Software Limited,2005
 *
 *$Id: msdpstub.c,v 1.2 2011/10/13 10:16:09 siva Exp $
 *
 * Description : This file contains the IPv6  porting 
 *               functions of the MSDP module
 *****************************************************************************/
#include "msdpinc.h"
#include "msdpcli.h"
/***************************************************************************
 *                                                                         
 *     Function Name : Msd6PortGetFirstIfAddr 
 *                                                                         
 *     Description   : This function gets the interface address for a given
 *                     interface index                           
 *                                                                         
 *     Input(s)      : u4IfIndex- Interface index           
 *                                                                         
 *     Output(s)     : pu1ip6Addr- Interface Address
 *                                                                         
 *     Returns       : OSIX_SUCCESS on obtaining the interface address
 *                     OSIX_FAILURE otherwise                    
 *                                                                         
***************************************************************************/
INT4
Msd6PortGetFirstIfAddr (UINT4 u4IfIndex, UINT1 *pu1ip6Addr)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1ip6Addr);
    return OSIX_SUCCESS;
}

/***************************************************************************
 *                                                                         
 *     Function Name : Msd6PortGetVlanIdFromIp6Address                         
 *                                                                         
 *     Description   : This function obtains the vlan id from the ip
 *                     address provided                     
 *                                                                         
 *     Input(s)      : pLocalAddr- Address        
 *                                                                         
 *     Output(s)     : u2VlanId- VLAN ID                                
 *                                                                         
 *     Returns       : OSIX_SUCCESS on obtaining the vlan id corr to address
 *                     OSIX_FAILURE otherwise                         
 *                                                                         
***************************************************************************/
INT4
Msd6PortGetVlanIdFromIp6Address (tSNMP_OCTET_STRING_TYPE * pLocalAddr,
                                 UINT2 *u2VlanId)
{
    UNUSED_PARAM (pLocalAddr);
    UNUSED_PARAM (u2VlanId);
    return OSIX_SUCCESS;
}

/***************************************************************************
 *                                                                         
 *     Function Name : Msd6PortIsValidLocalAddress                          
 *                                                                         
 *     Description   : This function checks whether the given IP address is 
 *                     a valid local address                         
 *                                                                         
 *     Input(s)      : pu1Addr- IP address           
 *                                                                         
 *     Output(s)     : None.                                              
 *                                                                         
 *     Returns       : Interface index of the address provided if valid
 *                     -1 otherwise                                   
 *                                                                         
***************************************************************************/
INT4
Msd6PortIsValidLocalAddress (UINT1 *pu1Addr)
{
    UNUSED_PARAM (pu1Addr);
    return OSIX_FAILURE;
}

/***************************************************************************
 *                                                                         
 *     Function Name : Msd6PortIsAddrMulti                          
 *                                                                         
 *     Description   : This function checks whether the given address is a
 *                     multicast address                    
 *                                                                         
 *     Input(s)      : pu1Addr- IP address           
 *                                                                         
 *     Output(s)     : None                                 
 *                                                                         
 *     Returns       : The return value of IS_ADDR_MULTI                                   
 *                                                                         
***************************************************************************/
INT4
Msd6PortIsAddrMulti (UINT1 *pu1Addr)
{
    UNUSED_PARAM (pu1Addr);
    return 0;
}

/***************************************************************************
 *                                                                         
 *     Function Name : Msd6PortIpv6GetRoute                   
 *                                                                         
 *     Description   : This function gets the IPv6 route information                     
 *                                                                         
 *     Input(s)      :  pIp6HliParams : Pointer to tIp6HliParams           
 *                                                                         
 *     Output(s)     :  None.                                              
 *                                                                         
 *     Returns       :  OSIX_SUCCESS or OSIX_FAILURE                                   
 *                                                                         
***************************************************************************/
INT4
Msd6PortIpv6GetRoute (tNetIpv6RtInfoQueryMsg * pNetIpv6RtQuery,
                      tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    UNUSED_PARAM (pNetIpv6RtQuery);
    UNUSED_PARAM (pNetIpv6RtInfo);
    return OSIX_SUCCESS;
}

/***************************************************************************
 *                                                                         
 *     Function Name : Msd6PortRegiterWithIP6                    
 *                                                                         
 *     Description   : This function registers IPv6                           
 *                                                                         
 *     Input(s)      : None          
 *                                                                         
 *     Output(s)     : None.                                              
 *                                                                         
 *     Returns       : OSIX_SUCCESS if NetIpv6RegisterHigherLayerProtocolInCxt
 *                     succeeds
 *                     OSIX_FAILURE otherwise                                   
 *                                                                         
***************************************************************************/
INT4
Msd6PortRegiterWithIP6 (VOID)
{
    return OSIX_SUCCESS;
}

/***************************************************************************
 *                                                                         
 *     Function Name : Msd6PortDeRegiterWithIP6                        
 *                                                                         
 *     Description   : This function deregisters IPv6 
 *                                                                         
 *     Input(s)      : None           
 *                                                                         
 *     Output(s)     : None                                              
 *                                                                         
 *     Returns       : OSIX_SUCCESS if NetIpv6DeRegisterHigherLayerProtocolInCxt
 *                     succeeeds
 *                     OSIX_FAILURE otherwise
 *                                                                         
***************************************************************************/
INT4
Msd6PortDeRegiterWithIP6 (VOID)
{
    return OSIX_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : Msd6PortHandleIPV6IfChange                          */
/*                                                                         */
/*     Description   : This function is provided to IPv6 as a callback     */
/*                     function. This is called by IPv6 when               */
/*                     Interface status changes.                           */
/*                                                                         */
/*     Input(s)      :  pIp6HliParams : Pointer to tIp6HliParams           */
/*                                                                         */
/*     Output(s)     :  None.                                              */
/*                                                                         */
/*     Returns       :  VOIDV.                                   */
/*                                                                         */
/***************************************************************************/
PUBLIC VOID
Msd6PortHandleIPV6IfChange (tNetIpv6HliParams * pIp6HliParams)
{
    UNUSED_PARAM (pIp6HliParams);
    return;
}
