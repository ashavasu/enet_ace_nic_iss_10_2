/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 *$Id: msdplw.c,v 1.1.1.1 2011/01/17 05:57:32 siva Exp $

 * Description: This file contains the low level nmh routines.
 *
*********************************************************************/

#include "msdpinc.h"

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMsdpPeerTable
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMsdpPeerTable (INT4 i4FsMsdpPeerAddrType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMsdpPeerRemoteAddress)
{
    UNUSED_PARAM (i4FsMsdpPeerAddrType);
    UNUSED_PARAM (pFsMsdpPeerRemoteAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMsdpSACacheTable
 Input       :  The Indices
                FsMsdpSACacheAddrType
                FsMsdpSACacheGroupAddr
                FsMsdpSACacheSourceAddr
                FsMsdpSACacheOriginRP
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMsdpSACacheTable (INT4 i4FsMsdpSACacheAddrType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsMsdpSACacheGroupAddr,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsMsdpSACacheSourceAddr,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsMsdpSACacheOriginRP)
{
    UNUSED_PARAM (i4FsMsdpSACacheAddrType);
    UNUSED_PARAM (pFsMsdpSACacheGroupAddr);
    UNUSED_PARAM (pFsMsdpSACacheSourceAddr);
    UNUSED_PARAM (pFsMsdpSACacheOriginRP);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMsdpMeshGroupTable
 Input       :  The Indices
                FsMsdpMeshGroupName
                FsMsdpMeshGroupAddrType
                FsMsdpMeshGroupPeerAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMsdpMeshGroupTable (tSNMP_OCTET_STRING_TYPE *
                                              pFsMsdpMeshGroupName,
                                              INT4 i4FsMsdpMeshGroupAddrType,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsMsdpMeshGroupPeerAddress)
{
    UNUSED_PARAM (pFsMsdpMeshGroupName);
    UNUSED_PARAM (i4FsMsdpMeshGroupAddrType);
    UNUSED_PARAM (pFsMsdpMeshGroupPeerAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMsdpRPTable
 Input       :  The Indices
                FsMsdpRPAddrType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMsdpRPTable (INT4 i4FsMsdpRPAddrType)
{
    UNUSED_PARAM (i4FsMsdpRPAddrType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMsdpPeerFilterTable
 Input       :  The Indices
                FsMsdpPeerFilterAddrType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMsdpPeerFilterTable (INT4 i4FsMsdpPeerFilterAddrType)
{
    UNUSED_PARAM (i4FsMsdpPeerFilterAddrType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMsdpSARedistributionTable
 Input       :  The Indices
                FsMsdpSARedistributionAddrType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMsdpSARedistributionTable (INT4
                                                     i4FsMsdpSARedistributionAddrType)
{
    UNUSED_PARAM (i4FsMsdpSARedistributionAddrType);
    return SNMP_SUCCESS;
}
