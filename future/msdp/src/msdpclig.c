#ifndef __MSDPCLIG_C__
#define __MSDPCLIG_C__
/********************************************************************
* Copyright (C) 2010  Aricent Inc . All Rights Reserved
*
* $Id: msdpclig.c,v 1.4 2014/03/14 12:56:29 siva Exp $
*
* Description: This file contains the Msdp CLI related routines 
*********************************************************************/

#include "msdpinc.h"
#include "msdpcli.h"

/****************************************************************************
 * Function    :  cli_process_Msdp_cmd
 * Description :  This function is exported to CLI module to handle the
                MSDP cli commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
cli_process_Msdp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[MSDP_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT4               u4CmdType = 0;
    INT4                i4Inst;
    tMsdpFsMsdpPeerEntry MsdpSetFsMsdpPeerEntry;
    tMsdpIsSetFsMsdpPeerEntry MsdpIsSetFsMsdpPeerEntry;

    tMsdpFsMsdpSACacheEntry MsdpSetFsMsdpSACacheEntry;
    tMsdpIsSetFsMsdpSACacheEntry MsdpIsSetFsMsdpSACacheEntry;

    tMsdpFsMsdpMeshGroupEntry MsdpSetFsMsdpMeshGroupEntry;
    tMsdpIsSetFsMsdpMeshGroupEntry MsdpIsSetFsMsdpMeshGroupEntry;

    tMsdpFsMsdpRPEntry  MsdpSetFsMsdpRPEntry;
    tMsdpIsSetFsMsdpRPEntry MsdpIsSetFsMsdpRPEntry;

    tMsdpFsMsdpPeerFilterEntry MsdpSetFsMsdpPeerFilterEntry;
    tMsdpIsSetFsMsdpPeerFilterEntry MsdpIsSetFsMsdpPeerFilterEntry;

    tMsdpFsMsdpSARedistributionEntry MsdpSetFsMsdpSARedistributionEntry;
    tMsdpIsSetFsMsdpSARedistributionEntry MsdpIsSetFsMsdpSARedistributionEntry;

    UNUSED_PARAM (u4CmdType);
    CliRegisterLock (CliHandle, MsdpMainTaskLock, MsdpMainTaskUnLock);
    MSDP_LOCK;

    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == MSDP_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);
    switch (u4Command)
    {
        case CLI_MSDP_FSMSDPPEERTABLE:
            MEMSET (&MsdpSetFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
            MEMSET (&MsdpIsSetFsMsdpPeerEntry, 0,
                    sizeof (tMsdpIsSetFsMsdpPeerEntry));

            MSDP_FILL_FSMSDPPEERTABLE_ARGS ((&MsdpSetFsMsdpPeerEntry),
                                            (&MsdpIsSetFsMsdpPeerEntry),
                                            args[0], args[1], args[2], args[3],
                                            args[4], args[5], args[6], args[7],
                                            args[8], args[9], args[10],
                                            args[11], args[12], args[13],
                                            args[14]);

            i4RetStatus =
                MsdpCliSetFsMsdpPeerTable (CliHandle, (&MsdpSetFsMsdpPeerEntry),
                                           (&MsdpIsSetFsMsdpPeerEntry));
            break;

        case CLI_MSDP_FSMSDPSACACHETABLE:
            MEMSET (&MsdpSetFsMsdpSACacheEntry, 0,
                    sizeof (tMsdpFsMsdpSACacheEntry));
            MEMSET (&MsdpIsSetFsMsdpSACacheEntry, 0,
                    sizeof (tMsdpIsSetFsMsdpSACacheEntry));

            MSDP_FILL_FSMSDPSACACHETABLE_ARGS ((&MsdpSetFsMsdpSACacheEntry),
                                               (&MsdpIsSetFsMsdpSACacheEntry),
                                               args[0], args[1], args[2],
                                               args[3], args[4], args[5],
                                               args[6], args[7]);

            i4RetStatus =
                MsdpCliSetFsMsdpSACacheTable (CliHandle,
                                              (&MsdpSetFsMsdpSACacheEntry),
                                              (&MsdpIsSetFsMsdpSACacheEntry));
            break;

        case CLI_MSDP_FSMSDPMESHGROUPTABLE:
            MEMSET (&MsdpSetFsMsdpMeshGroupEntry, 0,
                    sizeof (tMsdpFsMsdpMeshGroupEntry));
            MEMSET (&MsdpIsSetFsMsdpMeshGroupEntry, 0,
                    sizeof (tMsdpIsSetFsMsdpMeshGroupEntry));

            MSDP_FILL_FSMSDPMESHGROUPTABLE_ARGS ((&MsdpSetFsMsdpMeshGroupEntry),
                                                 (&MsdpIsSetFsMsdpMeshGroupEntry),
                                                 args[0], args[1], args[2],
                                                 args[3], args[4], args[5]);

            i4RetStatus =
                MsdpCliSetFsMsdpMeshGroupTable (CliHandle,
                                                (&MsdpSetFsMsdpMeshGroupEntry),
                                                (&MsdpIsSetFsMsdpMeshGroupEntry));
            break;

        case CLI_MSDP_FSMSDPRPTABLE:
            MEMSET (&MsdpSetFsMsdpRPEntry, 0, sizeof (tMsdpFsMsdpRPEntry));
            MEMSET (&MsdpIsSetFsMsdpRPEntry, 0,
                    sizeof (tMsdpIsSetFsMsdpRPEntry));

            MSDP_FILL_FSMSDPRPTABLE_ARGS ((&MsdpSetFsMsdpRPEntry),
                                          (&MsdpIsSetFsMsdpRPEntry), args[0],
                                          args[1], args[2], args[3]);

            i4RetStatus =
                MsdpCliSetFsMsdpRPTable (CliHandle, (&MsdpSetFsMsdpRPEntry),
                                         (&MsdpIsSetFsMsdpRPEntry));
            break;

        case CLI_MSDP_FSMSDPPEERFILTERTABLE:
            MEMSET (&MsdpSetFsMsdpPeerFilterEntry, 0,
                    sizeof (tMsdpFsMsdpPeerFilterEntry));
            MEMSET (&MsdpIsSetFsMsdpPeerFilterEntry, 0,
                    sizeof (tMsdpIsSetFsMsdpPeerFilterEntry));

            MSDP_FILL_FSMSDPPEERFILTERTABLE_ARGS ((&MsdpSetFsMsdpPeerFilterEntry), (&MsdpIsSetFsMsdpPeerFilterEntry), args[0], args[1], args[2], args[3]);

            i4RetStatus =
                MsdpCliSetFsMsdpPeerFilterTable (CliHandle,
                                                 (&MsdpSetFsMsdpPeerFilterEntry),
                                                 (&MsdpIsSetFsMsdpPeerFilterEntry));
            break;

        case CLI_MSDP_FSMSDPSAREDISTRIBUTIONTABLE:
            MEMSET (&MsdpSetFsMsdpSARedistributionEntry, 0,
                    sizeof (tMsdpFsMsdpSARedistributionEntry));
            MEMSET (&MsdpIsSetFsMsdpSARedistributionEntry, 0,
                    sizeof (tMsdpIsSetFsMsdpSARedistributionEntry));

            MSDP_FILL_FSMSDPSAREDISTRIBUTIONTABLE_ARGS ((&MsdpSetFsMsdpSARedistributionEntry), (&MsdpIsSetFsMsdpSARedistributionEntry), args[0], args[1], args[2], args[3], args[4]);

            i4RetStatus =
                MsdpCliSetFsMsdpSARedistributionTable (CliHandle,
                                                       (&MsdpSetFsMsdpSARedistributionEntry),
                                                       (&MsdpIsSetFsMsdpSARedistributionEntry));
            break;

        case CLI_MSDP_FSMSDPTRACELEVEL:

            i4RetStatus = MsdpCliSetFsMsdpTraceLevel (CliHandle, args[0]);
            break;

        case CLI_MSDP_FSMSDPIPV4ADMINSTAT:

            i4RetStatus = MsdpCliSetFsMsdpIPv4AdminStat (CliHandle, args[0]);
            break;

        case CLI_MSDP_FSMSDPIPV6ADMINSTAT:

            i4RetStatus = MsdpCliSetFsMsdpIPv6AdminStat (CliHandle, args[0]);
            break;

        case CLI_MSDP_FSMSDPCACHELIFETIME:

            i4RetStatus = MsdpCliSetFsMsdpCacheLifetime (CliHandle, args[0]);
            break;

        case CLI_MSDP_FSMSDPMAXPEERSESSIONS:

            i4RetStatus = MsdpCliSetFsMsdpMaxPeerSessions (CliHandle, args[0]);
            break;

        case CLI_MSDP_FSMSDPMAPPINGCOMPONENTID:

            i4RetStatus =
                MsdpCliSetFsMsdpMappingComponentId (CliHandle, args[0]);
            break;

        case CLI_MSDP_FSMSDPLISTENERPORT:

            i4RetStatus = MsdpCliSetFsMsdpListenerPort (CliHandle, args[0]);
            break;

        case CLI_MSDP_FSMSDPPEERFILTER:

            i4RetStatus = MsdpCliSetFsMsdpPeerFilter (CliHandle, args[0]);
            break;

    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_MSDP_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", MsdpCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);

    MSDP_UNLOCK;
    UNUSED_PARAM (i4Inst);
    return i4RetStatus;

}

/****************************************************************************
* Function    :  MsdpCliSetFsMsdpPeerTable
* Description :
* Input       :  CliHandle 
*            pMsdpSetFsMsdpPeerEntry
*            pMsdpIsSetFsMsdpPeerEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
MsdpCliSetFsMsdpPeerTable (tCliHandle CliHandle,
                           tMsdpFsMsdpPeerEntry * pMsdpSetFsMsdpPeerEntry,
                           tMsdpIsSetFsMsdpPeerEntry *
                           pMsdpIsSetFsMsdpPeerEntry)
{
    UINT4               u4ErrorCode;

    if (MsdpTestAllFsMsdpPeerTable (&u4ErrorCode, pMsdpSetFsMsdpPeerEntry,
                                    pMsdpIsSetFsMsdpPeerEntry, OSIX_TRUE,
                                    OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (MsdpSetAllFsMsdpPeerTable
        (pMsdpSetFsMsdpPeerEntry, pMsdpIsSetFsMsdpPeerEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  MsdpCliSetFsMsdpSACacheTable
* Description :
* Input       :  CliHandle 
*            pMsdpSetFsMsdpSACacheEntry
*            pMsdpIsSetFsMsdpSACacheEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
MsdpCliSetFsMsdpSACacheTable (tCliHandle CliHandle,
                              tMsdpFsMsdpSACacheEntry *
                              pMsdpSetFsMsdpSACacheEntry,
                              tMsdpIsSetFsMsdpSACacheEntry *
                              pMsdpIsSetFsMsdpSACacheEntry)
{
    UINT4               u4ErrorCode;

    if (MsdpTestAllFsMsdpSACacheTable (&u4ErrorCode, pMsdpSetFsMsdpSACacheEntry,
                                       pMsdpIsSetFsMsdpSACacheEntry, OSIX_TRUE,
                                       OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (MsdpSetAllFsMsdpSACacheTable
        (pMsdpSetFsMsdpSACacheEntry, pMsdpIsSetFsMsdpSACacheEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  MsdpCliSetFsMsdpMeshGroupTable
* Description :
* Input       :  CliHandle 
*            pMsdpSetFsMsdpMeshGroupEntry
*            pMsdpIsSetFsMsdpMeshGroupEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
MsdpCliSetFsMsdpMeshGroupTable (tCliHandle CliHandle,
                                tMsdpFsMsdpMeshGroupEntry *
                                pMsdpSetFsMsdpMeshGroupEntry,
                                tMsdpIsSetFsMsdpMeshGroupEntry *
                                pMsdpIsSetFsMsdpMeshGroupEntry)
{
    UINT4               u4ErrorCode;

    if (MsdpTestAllFsMsdpMeshGroupTable
        (&u4ErrorCode, pMsdpSetFsMsdpMeshGroupEntry,
         pMsdpIsSetFsMsdpMeshGroupEntry, OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (MsdpSetAllFsMsdpMeshGroupTable
        (pMsdpSetFsMsdpMeshGroupEntry, pMsdpIsSetFsMsdpMeshGroupEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  MsdpCliSetFsMsdpRPTable
* Description :
* Input       :  CliHandle 
*            pMsdpSetFsMsdpRPEntry
*            pMsdpIsSetFsMsdpRPEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
MsdpCliSetFsMsdpRPTable (tCliHandle CliHandle,
                         tMsdpFsMsdpRPEntry * pMsdpSetFsMsdpRPEntry,
                         tMsdpIsSetFsMsdpRPEntry * pMsdpIsSetFsMsdpRPEntry)
{
    UINT4               u4ErrorCode;

    if (MsdpTestAllFsMsdpRPTable (&u4ErrorCode, pMsdpSetFsMsdpRPEntry,
                                  pMsdpIsSetFsMsdpRPEntry, OSIX_TRUE,
                                  OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (MsdpSetAllFsMsdpRPTable (pMsdpSetFsMsdpRPEntry, pMsdpIsSetFsMsdpRPEntry,
                                 OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  MsdpCliSetFsMsdpPeerFilterTable
* Description :
* Input       :  CliHandle 
*            pMsdpSetFsMsdpPeerFilterEntry
*            pMsdpIsSetFsMsdpPeerFilterEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
MsdpCliSetFsMsdpPeerFilterTable (tCliHandle CliHandle,
                                 tMsdpFsMsdpPeerFilterEntry *
                                 pMsdpSetFsMsdpPeerFilterEntry,
                                 tMsdpIsSetFsMsdpPeerFilterEntry *
                                 pMsdpIsSetFsMsdpPeerFilterEntry)
{
    UINT4               u4ErrorCode;

    if (MsdpTestAllFsMsdpPeerFilterTable
        (&u4ErrorCode, pMsdpSetFsMsdpPeerFilterEntry,
         pMsdpIsSetFsMsdpPeerFilterEntry, OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (MsdpSetAllFsMsdpPeerFilterTable
        (pMsdpSetFsMsdpPeerFilterEntry, pMsdpIsSetFsMsdpPeerFilterEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  MsdpCliSetFsMsdpSARedistributionTable
* Description :
* Input       :  CliHandle 
*            pMsdpSetFsMsdpSARedistributionEntry
*            pMsdpIsSetFsMsdpSARedistributionEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
MsdpCliSetFsMsdpSARedistributionTable (tCliHandle CliHandle,
                                       tMsdpFsMsdpSARedistributionEntry *
                                       pMsdpSetFsMsdpSARedistributionEntry,
                                       tMsdpIsSetFsMsdpSARedistributionEntry *
                                       pMsdpIsSetFsMsdpSARedistributionEntry)
{
    UINT4               u4ErrorCode;

    if (MsdpTestAllFsMsdpSARedistributionTable
        (&u4ErrorCode, pMsdpSetFsMsdpSARedistributionEntry,
         pMsdpIsSetFsMsdpSARedistributionEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (MsdpSetAllFsMsdpSARedistributionTable
        (pMsdpSetFsMsdpSARedistributionEntry,
         pMsdpIsSetFsMsdpSARedistributionEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  MsdpCliSetFsMsdpTraceLevel
* Description :
* Input       :  CliHandle 
*            pMsdpSetScalar
*            pMsdpIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
MsdpCliSetFsMsdpTraceLevel (tCliHandle CliHandle, UINT4 *pFsMsdpTraceLevel)
{
    UINT4               u4ErrorCode;
    INT4                i4FsMsdpTraceLevel = 0;

    MSDP_FILL_FSMSDPTRACELEVEL (i4FsMsdpTraceLevel, pFsMsdpTraceLevel);

    if (MsdpTestFsMsdpTraceLevel (&u4ErrorCode, i4FsMsdpTraceLevel) !=
        OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (MsdpSetFsMsdpTraceLevel (i4FsMsdpTraceLevel) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  MsdpCliSetFsMsdpIPv4AdminStat
* Description :
* Input       :  CliHandle 
*            pMsdpSetScalar
*            pMsdpIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
MsdpCliSetFsMsdpIPv4AdminStat (tCliHandle CliHandle,
                               UINT4 *pFsMsdpIPv4AdminStat)
{
    UINT4               u4ErrorCode;
    INT4                i4FsMsdpIPv4AdminStat = 0;

    MSDP_FILL_FSMSDPIPV4ADMINSTAT (i4FsMsdpIPv4AdminStat, pFsMsdpIPv4AdminStat);

    if (MsdpTestFsMsdpIPv4AdminStat (&u4ErrorCode, i4FsMsdpIPv4AdminStat) !=
        OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (MsdpSetFsMsdpIPv4AdminStat (i4FsMsdpIPv4AdminStat) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  MsdpCliSetFsMsdpIPv6AdminStat
* Description :
* Input       :  CliHandle 
*            pMsdpSetScalar
*            pMsdpIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
MsdpCliSetFsMsdpIPv6AdminStat (tCliHandle CliHandle,
                               UINT4 *pFsMsdpIPv6AdminStat)
{
    UINT4               u4ErrorCode;
    INT4                i4FsMsdpIPv6AdminStat = 0;

    MSDP_FILL_FSMSDPIPV6ADMINSTAT (i4FsMsdpIPv6AdminStat, pFsMsdpIPv6AdminStat);

    if (MsdpTestFsMsdpIPv6AdminStat (&u4ErrorCode, i4FsMsdpIPv6AdminStat) !=
        OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (MsdpSetFsMsdpIPv6AdminStat (i4FsMsdpIPv6AdminStat) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  MsdpCliSetFsMsdpCacheLifetime
* Description :
* Input       :  CliHandle 
*            pMsdpSetScalar
*            pMsdpIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
MsdpCliSetFsMsdpCacheLifetime (tCliHandle CliHandle,
                               UINT4 *pFsMsdpCacheLifetime)
{
    UINT4               u4ErrorCode;
    UINT4               u4FsMsdpCacheLifetime = 0;

    MSDP_FILL_FSMSDPCACHELIFETIME (u4FsMsdpCacheLifetime, pFsMsdpCacheLifetime);

    if (MsdpTestFsMsdpCacheLifetime (&u4ErrorCode, u4FsMsdpCacheLifetime) !=
        OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (MsdpSetFsMsdpCacheLifetime (u4FsMsdpCacheLifetime) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  MsdpCliSetFsMsdpMaxPeerSessions
* Description :
* Input       :  CliHandle 
*            pMsdpSetScalar
*            pMsdpIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
MsdpCliSetFsMsdpMaxPeerSessions (tCliHandle CliHandle,
                                 UINT4 *pFsMsdpMaxPeerSessions)
{
    UINT4               u4ErrorCode;
    INT4                i4FsMsdpMaxPeerSessions = 0;

    MSDP_FILL_FSMSDPMAXPEERSESSIONS (i4FsMsdpMaxPeerSessions,
                                     pFsMsdpMaxPeerSessions);

    if (MsdpTestFsMsdpMaxPeerSessions (&u4ErrorCode, i4FsMsdpMaxPeerSessions) !=
        OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (MsdpSetFsMsdpMaxPeerSessions (i4FsMsdpMaxPeerSessions) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  MsdpCliSetFsMsdpMappingComponentId
* Description :
* Input       :  CliHandle 
*            pMsdpSetScalar
*            pMsdpIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
MsdpCliSetFsMsdpMappingComponentId (tCliHandle CliHandle,
                                    UINT4 *pFsMsdpMappingComponentId)
{
    UINT4               u4ErrorCode;
    INT4                i4FsMsdpMappingComponentId = 0;

    MSDP_FILL_FSMSDPMAPPINGCOMPONENTID (i4FsMsdpMappingComponentId,
                                        pFsMsdpMappingComponentId);

    if (MsdpTestFsMsdpMappingComponentId
        (&u4ErrorCode, i4FsMsdpMappingComponentId) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (MsdpSetFsMsdpMappingComponentId (i4FsMsdpMappingComponentId) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  MsdpCliSetFsMsdpListenerPort
* Description :
* Input       :  CliHandle 
*            pMsdpSetScalar
*            pMsdpIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
MsdpCliSetFsMsdpListenerPort (tCliHandle CliHandle, UINT4 *pFsMsdpListenerPort)
{
    UINT4               u4ErrorCode;
    INT4                i4FsMsdpListenerPort = 0;

    MSDP_FILL_FSMSDPLISTENERPORT (i4FsMsdpListenerPort, pFsMsdpListenerPort);

    if (MsdpTestFsMsdpListenerPort (&u4ErrorCode, i4FsMsdpListenerPort) !=
        OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (MsdpSetFsMsdpListenerPort (i4FsMsdpListenerPort) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  MsdpCliSetFsMsdpPeerFilter
* Description :
* Input       :  CliHandle 
*            pMsdpSetScalar
*            pMsdpIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
MsdpCliSetFsMsdpPeerFilter (tCliHandle CliHandle, UINT4 *pFsMsdpPeerFilter)
{
    UINT4               u4ErrorCode;
    INT4                i4FsMsdpPeerFilter = 0;

    MSDP_FILL_FSMSDPPEERFILTER (i4FsMsdpPeerFilter, pFsMsdpPeerFilter);

    if (MsdpTestFsMsdpPeerFilter (&u4ErrorCode, i4FsMsdpPeerFilter) !=
        OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (MsdpSetFsMsdpPeerFilter (i4FsMsdpPeerFilter) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssMsdpShowDebugging                               */
/*                                                                           */
/*     DESCRIPTION      : This function prints the MSDP  debug levels        */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
IssMsdpShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DbgLevel = 0;

    MsdpGetFsMsdpTraceLevel (&i4DbgLevel);

    if (i4DbgLevel == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "\rMSDP :");

    if ((i4DbgLevel & CLI_MSDP_PEER_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  MSDP peer debugging is on");
    }
    if ((i4DbgLevel & CLI_MSDP_CACHE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  MSDP cache debugging is on");
    }
    if ((i4DbgLevel & CLI_MSDP_INPUT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  MSDP input debugging is on");
    }
    if ((i4DbgLevel & CLI_MSDP_OUTPUT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  MSDP output debugging is on");
    }

    CliPrintf (CliHandle, "\r\n");

    return;
}

#endif
