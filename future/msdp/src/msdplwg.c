/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
 *$Id: msdplwg.c,v 1.3 2012/04/06 12:36:56 siva Exp $

 * Description:This file contains procedures related to lowlevel nmhGet 
 *  routines
 *******************************************************************/

#include "msdpinc.h"

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMsdpPeerTable
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMsdpPeerTable (INT4 *pi4FsMsdpPeerAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMsdpPeerRemoteAddress)
{

    tMsdpFsMsdpPeerEntry *pMsdpFsMsdpPeerEntry = NULL;

    pMsdpFsMsdpPeerEntry = MsdpGetFirstFsMsdpPeerTable ();

    if (pMsdpFsMsdpPeerEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pi4FsMsdpPeerAddrType =
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType;

    MEMCPY (pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pMsdpFsMsdpPeerEntry->MibObject.au1FsMsdpPeerRemoteAddress,
            pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerRemoteAddressLen);
    pFsMsdpPeerRemoteAddress->i4_Length =
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerRemoteAddressLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMsdpSACacheTable
 Input       :  The Indices
                FsMsdpSACacheAddrType
                FsMsdpSACacheGroupAddr
                FsMsdpSACacheSourceAddr
                FsMsdpSACacheOriginRP
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMsdpSACacheTable (INT4 *pi4FsMsdpSACacheAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMsdpSACacheGroupAddr,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMsdpSACacheSourceAddr,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMsdpSACacheOriginRP)
{

    tMsdpFsMsdpSACacheEntry *pMsdpFsMsdpSACacheEntry = NULL;

    pMsdpFsMsdpSACacheEntry = MsdpGetFirstFsMsdpSACacheTable ();

    if (pMsdpFsMsdpSACacheEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pi4FsMsdpSACacheAddrType =
        pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheAddrType;

    MEMCPY (pFsMsdpSACacheGroupAddr->pu1_OctetList,
            pMsdpFsMsdpSACacheEntry->MibObject.au1FsMsdpSACacheGroupAddr,
            pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheGroupAddrLen);
    pFsMsdpSACacheGroupAddr->i4_Length =
        pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheGroupAddrLen;

    MEMCPY (pFsMsdpSACacheSourceAddr->pu1_OctetList,
            pMsdpFsMsdpSACacheEntry->MibObject.au1FsMsdpSACacheSourceAddr,
            pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheSourceAddrLen);
    pFsMsdpSACacheSourceAddr->i4_Length =
        pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheSourceAddrLen;

    MEMCPY (pFsMsdpSACacheOriginRP->pu1_OctetList,
            pMsdpFsMsdpSACacheEntry->MibObject.au1FsMsdpSACacheOriginRP,
            pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheOriginRPLen);
    pFsMsdpSACacheOriginRP->i4_Length =
        pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheOriginRPLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMsdpMeshGroupTable
 Input       :  The Indices
                FsMsdpMeshGroupName
                FsMsdpMeshGroupAddrType
                FsMsdpMeshGroupPeerAddress
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMsdpMeshGroupTable (tSNMP_OCTET_STRING_TYPE *
                                      pFsMsdpMeshGroupName,
                                      INT4 *pi4FsMsdpMeshGroupAddrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMsdpMeshGroupPeerAddress)
{

    tMsdpFsMsdpMeshGroupEntry *pMsdpFsMsdpMeshGroupEntry = NULL;

    pMsdpFsMsdpMeshGroupEntry = MsdpGetFirstFsMsdpMeshGroupTable ();

    if (pMsdpFsMsdpMeshGroupEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    MEMCPY (pFsMsdpMeshGroupName->pu1_OctetList,
            pMsdpFsMsdpMeshGroupEntry->MibObject.au1FsMsdpMeshGroupName,
            pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupNameLen);
    pFsMsdpMeshGroupName->i4_Length =
        pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupNameLen;

    *pi4FsMsdpMeshGroupAddrType =
        pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupAddrType;

    MEMCPY (pFsMsdpMeshGroupPeerAddress->pu1_OctetList,
            pMsdpFsMsdpMeshGroupEntry->MibObject.au1FsMsdpMeshGroupPeerAddress,
            pMsdpFsMsdpMeshGroupEntry->MibObject.
            i4FsMsdpMeshGroupPeerAddressLen);
    pFsMsdpMeshGroupPeerAddress->i4_Length =
        pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupPeerAddressLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMsdpRPTable
 Input       :  The Indices
                FsMsdpRPAddrType
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMsdpRPTable (INT4 *pi4FsMsdpRPAddrType)
{

    tMsdpFsMsdpRPEntry *pMsdpFsMsdpRPEntry = NULL;

    pMsdpFsMsdpRPEntry = MsdpGetFirstFsMsdpRPTable ();

    if (pMsdpFsMsdpRPEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pi4FsMsdpRPAddrType = pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPAddrType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMsdpPeerFilterTable
 Input       :  The Indices
                FsMsdpPeerFilterAddrType
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMsdpPeerFilterTable (INT4 *pi4FsMsdpPeerFilterAddrType)
{

    tMsdpFsMsdpPeerFilterEntry *pMsdpFsMsdpPeerFilterEntry = NULL;

    pMsdpFsMsdpPeerFilterEntry = MsdpGetFirstFsMsdpPeerFilterTable ();

    if (pMsdpFsMsdpPeerFilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pi4FsMsdpPeerFilterAddrType =
        pMsdpFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterAddrType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMsdpSARedistributionTable
 Input       :  The Indices
                FsMsdpSARedistributionAddrType
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMsdpSARedistributionTable (INT4
                                             *pi4FsMsdpSARedistributionAddrType)
{

    tMsdpFsMsdpSARedistributionEntry *pMsdpFsMsdpSARedistributionEntry = NULL;

    pMsdpFsMsdpSARedistributionEntry =
        MsdpGetFirstFsMsdpSARedistributionTable ();

    if (pMsdpFsMsdpSARedistributionEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pi4FsMsdpSARedistributionAddrType =
        pMsdpFsMsdpSARedistributionEntry->MibObject.
        i4FsMsdpSARedistributionAddrType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMsdpPeerTable
 Input       :  The Indices
                FsMsdpPeerAddrType
                nextFsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress
                nextFsMsdpPeerRemoteAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsMsdpPeerTable (INT4 i4FsMsdpPeerAddrType,
                                INT4 *pi4NextFsMsdpPeerAddrType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMsdpPeerRemoteAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pNextFsMsdpPeerRemoteAddress)
{

    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    tMsdpFsMsdpPeerEntry *pNextMsdpFsMsdpPeerEntry = NULL;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    pNextMsdpFsMsdpPeerEntry =
        MsdpGetNextFsMsdpPeerTable (&MsdpFsMsdpPeerEntry);

    if (pNextMsdpFsMsdpPeerEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pi4NextFsMsdpPeerAddrType =
        pNextMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType;

    MEMCPY (pNextFsMsdpPeerRemoteAddress->pu1_OctetList,
            pNextMsdpFsMsdpPeerEntry->MibObject.au1FsMsdpPeerRemoteAddress,
            pNextMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerRemoteAddressLen);
    pNextFsMsdpPeerRemoteAddress->i4_Length =
        pNextMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerRemoteAddressLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMsdpSACacheTable
 Input       :  The Indices
                FsMsdpSACacheAddrType
                nextFsMsdpSACacheAddrType
                FsMsdpSACacheGroupAddr
                nextFsMsdpSACacheGroupAddr
                FsMsdpSACacheSourceAddr
                nextFsMsdpSACacheSourceAddr
                FsMsdpSACacheOriginRP
                nextFsMsdpSACacheOriginRP
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsMsdpSACacheTable (INT4 i4FsMsdpSACacheAddrType,
                                   INT4 *pi4NextFsMsdpSACacheAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMsdpSACacheGroupAddr,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextFsMsdpSACacheGroupAddr,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMsdpSACacheSourceAddr,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextFsMsdpSACacheSourceAddr,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMsdpSACacheOriginRP,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextFsMsdpSACacheOriginRP)
{

    tMsdpFsMsdpSACacheEntry MsdpFsMsdpSACacheEntry;
    tMsdpFsMsdpSACacheEntry *pNextMsdpFsMsdpSACacheEntry = NULL;

    MEMSET (&MsdpFsMsdpSACacheEntry, 0, sizeof (tMsdpFsMsdpSACacheEntry));

    /* Assign the index */
    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheAddrType =
        i4FsMsdpSACacheAddrType;

    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheGroupAddr,
            pFsMsdpSACacheGroupAddr->pu1_OctetList,
            pFsMsdpSACacheGroupAddr->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheGroupAddrLen =
        pFsMsdpSACacheGroupAddr->i4_Length;
    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheSourceAddr,
            pFsMsdpSACacheSourceAddr->pu1_OctetList,
            pFsMsdpSACacheSourceAddr->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheSourceAddrLen =
        pFsMsdpSACacheSourceAddr->i4_Length;
    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheOriginRP,
            pFsMsdpSACacheOriginRP->pu1_OctetList,
            pFsMsdpSACacheOriginRP->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheOriginRPLen =
        pFsMsdpSACacheOriginRP->i4_Length;

    pNextMsdpFsMsdpSACacheEntry =
        MsdpGetNextFsMsdpSACacheTable (&MsdpFsMsdpSACacheEntry);

    if (pNextMsdpFsMsdpSACacheEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pi4NextFsMsdpSACacheAddrType =
        pNextMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheAddrType;

    MEMCPY (pNextFsMsdpSACacheGroupAddr->pu1_OctetList,
            pNextMsdpFsMsdpSACacheEntry->MibObject.au1FsMsdpSACacheGroupAddr,
            pNextMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheGroupAddrLen);
    pNextFsMsdpSACacheGroupAddr->i4_Length =
        pNextMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheGroupAddrLen;

    MEMCPY (pNextFsMsdpSACacheSourceAddr->pu1_OctetList,
            pNextMsdpFsMsdpSACacheEntry->MibObject.au1FsMsdpSACacheSourceAddr,
            pNextMsdpFsMsdpSACacheEntry->MibObject.
            i4FsMsdpSACacheSourceAddrLen);
    pNextFsMsdpSACacheSourceAddr->i4_Length =
        pNextMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheSourceAddrLen;

    MEMCPY (pNextFsMsdpSACacheOriginRP->pu1_OctetList,
            pNextMsdpFsMsdpSACacheEntry->MibObject.au1FsMsdpSACacheOriginRP,
            pNextMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheOriginRPLen);
    pNextFsMsdpSACacheOriginRP->i4_Length =
        pNextMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheOriginRPLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMsdpMeshGroupTable
 Input       :  The Indices
                FsMsdpMeshGroupName
                nextFsMsdpMeshGroupName
                FsMsdpMeshGroupAddrType
                nextFsMsdpMeshGroupAddrType
                FsMsdpMeshGroupPeerAddress
                nextFsMsdpMeshGroupPeerAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsMsdpMeshGroupTable (tSNMP_OCTET_STRING_TYPE *
                                     pFsMsdpMeshGroupName,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextFsMsdpMeshGroupName,
                                     INT4 i4FsMsdpMeshGroupAddrType,
                                     INT4 *pi4NextFsMsdpMeshGroupAddrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMsdpMeshGroupPeerAddress,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextFsMsdpMeshGroupPeerAddress)
{

    tMsdpFsMsdpMeshGroupEntry MsdpFsMsdpMeshGroupEntry;
    tMsdpFsMsdpMeshGroupEntry *pNextMsdpFsMsdpMeshGroupEntry = NULL;

    MEMSET (&MsdpFsMsdpMeshGroupEntry, 0, sizeof (tMsdpFsMsdpMeshGroupEntry));

    /* Assign the index */
    MEMCPY (MsdpFsMsdpMeshGroupEntry.MibObject.au1FsMsdpMeshGroupName,
            pFsMsdpMeshGroupName->pu1_OctetList,
            pFsMsdpMeshGroupName->i4_Length);

    MsdpFsMsdpMeshGroupEntry.MibObject.i4FsMsdpMeshGroupNameLen =
        pFsMsdpMeshGroupName->i4_Length;
    MsdpFsMsdpMeshGroupEntry.MibObject.i4FsMsdpMeshGroupAddrType =
        i4FsMsdpMeshGroupAddrType;

    MEMCPY (MsdpFsMsdpMeshGroupEntry.MibObject.au1FsMsdpMeshGroupPeerAddress,
            pFsMsdpMeshGroupPeerAddress->pu1_OctetList,
            pFsMsdpMeshGroupPeerAddress->i4_Length);

    MsdpFsMsdpMeshGroupEntry.MibObject.i4FsMsdpMeshGroupPeerAddressLen =
        pFsMsdpMeshGroupPeerAddress->i4_Length;

    pNextMsdpFsMsdpMeshGroupEntry =
        MsdpGetNextFsMsdpMeshGroupTable (&MsdpFsMsdpMeshGroupEntry);

    if (pNextMsdpFsMsdpMeshGroupEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    MEMCPY (pNextFsMsdpMeshGroupName->pu1_OctetList,
            pNextMsdpFsMsdpMeshGroupEntry->MibObject.au1FsMsdpMeshGroupName,
            pNextMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupNameLen);
    pNextFsMsdpMeshGroupName->i4_Length =
        pNextMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupNameLen;

    *pi4NextFsMsdpMeshGroupAddrType =
        pNextMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupAddrType;

    MEMCPY (pNextFsMsdpMeshGroupPeerAddress->pu1_OctetList,
            pNextMsdpFsMsdpMeshGroupEntry->MibObject.
            au1FsMsdpMeshGroupPeerAddress,
            pNextMsdpFsMsdpMeshGroupEntry->MibObject.
            i4FsMsdpMeshGroupPeerAddressLen);
    pNextFsMsdpMeshGroupPeerAddress->i4_Length =
        pNextMsdpFsMsdpMeshGroupEntry->MibObject.
        i4FsMsdpMeshGroupPeerAddressLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMsdpRPTable
 Input       :  The Indices
                FsMsdpRPAddrType
                nextFsMsdpRPAddrType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsMsdpRPTable (INT4 i4FsMsdpRPAddrType,
                              INT4 *pi4NextFsMsdpRPAddrType)
{

    tMsdpFsMsdpRPEntry  MsdpFsMsdpRPEntry;
    tMsdpFsMsdpRPEntry *pNextMsdpFsMsdpRPEntry = NULL;

    MEMSET (&MsdpFsMsdpRPEntry, 0, sizeof (tMsdpFsMsdpRPEntry));

    /* Assign the index */
    MsdpFsMsdpRPEntry.MibObject.i4FsMsdpRPAddrType = i4FsMsdpRPAddrType;

    pNextMsdpFsMsdpRPEntry = MsdpGetNextFsMsdpRPTable (&MsdpFsMsdpRPEntry);

    if (pNextMsdpFsMsdpRPEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pi4NextFsMsdpRPAddrType =
        pNextMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPAddrType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMsdpPeerFilterTable
 Input       :  The Indices
                FsMsdpPeerFilterAddrType
                nextFsMsdpPeerFilterAddrType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsMsdpPeerFilterTable (INT4 i4FsMsdpPeerFilterAddrType,
                                      INT4 *pi4NextFsMsdpPeerFilterAddrType)
{

    tMsdpFsMsdpPeerFilterEntry MsdpFsMsdpPeerFilterEntry;
    tMsdpFsMsdpPeerFilterEntry *pNextMsdpFsMsdpPeerFilterEntry = NULL;

    MEMSET (&MsdpFsMsdpPeerFilterEntry, 0, sizeof (tMsdpFsMsdpPeerFilterEntry));

    /* Assign the index */
    MsdpFsMsdpPeerFilterEntry.MibObject.i4FsMsdpPeerFilterAddrType =
        i4FsMsdpPeerFilterAddrType;

    pNextMsdpFsMsdpPeerFilterEntry =
        MsdpGetNextFsMsdpPeerFilterTable (&MsdpFsMsdpPeerFilterEntry);

    if (pNextMsdpFsMsdpPeerFilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pi4NextFsMsdpPeerFilterAddrType =
        pNextMsdpFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterAddrType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMsdpSARedistributionTable
 Input       :  The Indices
                FsMsdpSARedistributionAddrType
                nextFsMsdpSARedistributionAddrType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsMsdpSARedistributionTable (INT4
                                            i4FsMsdpSARedistributionAddrType,
                                            INT4
                                            *pi4NextFsMsdpSARedistributionAddrType)
{

    tMsdpFsMsdpSARedistributionEntry MsdpFsMsdpSARedistributionEntry;
    tMsdpFsMsdpSARedistributionEntry *pNextMsdpFsMsdpSARedistributionEntry =
        NULL;

    MEMSET (&MsdpFsMsdpSARedistributionEntry, 0,
            sizeof (tMsdpFsMsdpSARedistributionEntry));

    /* Assign the index */
    MsdpFsMsdpSARedistributionEntry.MibObject.i4FsMsdpSARedistributionAddrType =
        i4FsMsdpSARedistributionAddrType;

    pNextMsdpFsMsdpSARedistributionEntry =
        MsdpGetNextFsMsdpSARedistributionTable
        (&MsdpFsMsdpSARedistributionEntry);

    if (pNextMsdpFsMsdpSARedistributionEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pi4NextFsMsdpSARedistributionAddrType =
        pNextMsdpFsMsdpSARedistributionEntry->MibObject.
        i4FsMsdpSARedistributionAddrType;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpTraceLevel
 Input       :  The Indices

                The Object 
                INT4 *pi4RetValFsMsdpTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpTraceLevel (INT4 *pi4RetValFsMsdpTraceLevel)
{
    if (MsdpGetFsMsdpTraceLevel (pi4RetValFsMsdpTraceLevel) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpIPv4AdminStat
 Input       :  The Indices

                The Object 
                INT4 *pi4RetValFsMsdpIPv4AdminStat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpIPv4AdminStat (INT4 *pi4RetValFsMsdpIPv4AdminStat)
{
    if (MsdpGetFsMsdpIPv4AdminStat (pi4RetValFsMsdpIPv4AdminStat) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpIPv6AdminStat
 Input       :  The Indices

                The Object 
                INT4 *pi4RetValFsMsdpIPv6AdminStat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpIPv6AdminStat (INT4 *pi4RetValFsMsdpIPv6AdminStat)
{
    if (MsdpGetFsMsdpIPv6AdminStat (pi4RetValFsMsdpIPv6AdminStat) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpCacheLifetime
 Input       :  The Indices

                The Object 
                UINT4 *pu4RetValFsMsdpCacheLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpCacheLifetime (UINT4 *pu4RetValFsMsdpCacheLifetime)
{
    if (MsdpGetFsMsdpCacheLifetime (pu4RetValFsMsdpCacheLifetime) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpNumSACacheEntries
 Input       :  The Indices

                The Object 
                UINT4 *pu4RetValFsMsdpNumSACacheEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpNumSACacheEntries (UINT4 *pu4RetValFsMsdpNumSACacheEntries)
{
    if (MsdpGetFsMsdpNumSACacheEntries (pu4RetValFsMsdpNumSACacheEntries) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpMaxPeerSessions
 Input       :  The Indices

                The Object 
                INT4 *pi4RetValFsMsdpMaxPeerSessions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpMaxPeerSessions (INT4 *pi4RetValFsMsdpMaxPeerSessions)
{
    if (MsdpGetFsMsdpMaxPeerSessions (pi4RetValFsMsdpMaxPeerSessions) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpMappingComponentId
 Input       :  The Indices

                The Object 
                INT4 *pi4RetValFsMsdpMappingComponentId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpMappingComponentId (INT4 *pi4RetValFsMsdpMappingComponentId)
{
    if (MsdpGetFsMsdpMappingComponentId (pi4RetValFsMsdpMappingComponentId) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpListenerPort
 Input       :  The Indices

                The Object 
                INT4 *pi4RetValFsMsdpListenerPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpListenerPort (INT4 *pi4RetValFsMsdpListenerPort)
{
    if (MsdpGetFsMsdpListenerPort (pi4RetValFsMsdpListenerPort) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerFilter
 Input       :  The Indices

                The Object 
                INT4 *pi4RetValFsMsdpPeerFilter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerFilter (INT4 *pi4RetValFsMsdpPeerFilter)
{
    if (MsdpGetFsMsdpPeerFilter (pi4RetValFsMsdpPeerFilter) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerCount
 Input       :  The Indices

                The Object 
                INT4 *pi4RetValFsMsdpPeerCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerCount (INT4 *pi4RetValFsMsdpPeerCount)
{
    if (MsdpGetFsMsdpPeerCount (pi4RetValFsMsdpPeerCount) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpStatEstPeerCount
 Input       :  The Indices

                The Object 
                INT4 *pi4RetValFsMsdpStatEstPeerCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpStatEstPeerCount (INT4 *pi4RetValFsMsdpStatEstPeerCount)
{
    if (MsdpGetFsMsdpStatEstPeerCount (pi4RetValFsMsdpStatEstPeerCount) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerState
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                INT4 *pi4RetValFsMsdpPeerState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerState (INT4 i4FsMsdpPeerAddrType,
                       tSNMP_OCTET_STRING_TYPE * pFsMsdpPeerRemoteAddress,
                       INT4 *pi4RetValFsMsdpPeerState)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMsdpPeerState = MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerState;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerRPFFailures
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                UINT4 *pu4RetValFsMsdpPeerRPFFailures
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerRPFFailures (INT4 i4FsMsdpPeerAddrType,
                             tSNMP_OCTET_STRING_TYPE * pFsMsdpPeerRemoteAddress,
                             UINT4 *pu4RetValFsMsdpPeerRPFFailures)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpPeerRPFFailures =
        MsdpFsMsdpPeerEntry.MibObject.u4FsMsdpPeerRPFFailures;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerInSAs
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                UINT4 *pu4RetValFsMsdpPeerInSAs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerInSAs (INT4 i4FsMsdpPeerAddrType,
                       tSNMP_OCTET_STRING_TYPE * pFsMsdpPeerRemoteAddress,
                       UINT4 *pu4RetValFsMsdpPeerInSAs)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpPeerInSAs = MsdpFsMsdpPeerEntry.MibObject.u4FsMsdpPeerInSAs;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerOutSAs
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                UINT4 *pu4RetValFsMsdpPeerOutSAs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerOutSAs (INT4 i4FsMsdpPeerAddrType,
                        tSNMP_OCTET_STRING_TYPE * pFsMsdpPeerRemoteAddress,
                        UINT4 *pu4RetValFsMsdpPeerOutSAs)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpPeerOutSAs =
        MsdpFsMsdpPeerEntry.MibObject.u4FsMsdpPeerOutSAs;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerInSARequests
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                UINT4 *pu4RetValFsMsdpPeerInSARequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerInSARequests (INT4 i4FsMsdpPeerAddrType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsMsdpPeerRemoteAddress,
                              UINT4 *pu4RetValFsMsdpPeerInSARequests)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpPeerInSARequests =
        MsdpFsMsdpPeerEntry.MibObject.u4FsMsdpPeerInSARequests;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerOutSARequests
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                UINT4 *pu4RetValFsMsdpPeerOutSARequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerOutSARequests (INT4 i4FsMsdpPeerAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsMsdpPeerRemoteAddress,
                               UINT4 *pu4RetValFsMsdpPeerOutSARequests)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpPeerOutSARequests =
        MsdpFsMsdpPeerEntry.MibObject.u4FsMsdpPeerOutSARequests;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerInControlMessages
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                UINT4 *pu4RetValFsMsdpPeerInControlMessages
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerInControlMessages (INT4 i4FsMsdpPeerAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMsdpPeerRemoteAddress,
                                   UINT4 *pu4RetValFsMsdpPeerInControlMessages)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpPeerInControlMessages =
        MsdpFsMsdpPeerEntry.MibObject.u4FsMsdpPeerInControlMessages;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerOutControlMessages
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                UINT4 *pu4RetValFsMsdpPeerOutControlMessages
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerOutControlMessages (INT4 i4FsMsdpPeerAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMsdpPeerRemoteAddress,
                                    UINT4
                                    *pu4RetValFsMsdpPeerOutControlMessages)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpPeerOutControlMessages =
        MsdpFsMsdpPeerEntry.MibObject.u4FsMsdpPeerOutControlMessages;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerInDataPackets
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                UINT4 *pu4RetValFsMsdpPeerInDataPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerInDataPackets (INT4 i4FsMsdpPeerAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsMsdpPeerRemoteAddress,
                               UINT4 *pu4RetValFsMsdpPeerInDataPackets)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpPeerInDataPackets =
        MsdpFsMsdpPeerEntry.MibObject.u4FsMsdpPeerInDataPackets;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerOutDataPackets
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                UINT4 *pu4RetValFsMsdpPeerOutDataPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerOutDataPackets (INT4 i4FsMsdpPeerAddrType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMsdpPeerRemoteAddress,
                                UINT4 *pu4RetValFsMsdpPeerOutDataPackets)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpPeerOutDataPackets =
        MsdpFsMsdpPeerEntry.MibObject.u4FsMsdpPeerOutDataPackets;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerFsmEstablishedTransitions
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                UINT4 *pu4RetValFsMsdpPeerFsmEstablishedTransitions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerFsmEstablishedTransitions (INT4 i4FsMsdpPeerAddrType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsMsdpPeerRemoteAddress,
                                           UINT4
                                           *pu4RetValFsMsdpPeerFsmEstablishedTransitions)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpPeerFsmEstablishedTransitions =
        MsdpFsMsdpPeerEntry.MibObject.u4FsMsdpPeerFsmEstablishedTransitions;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerFsmEstablishedTime
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                UINT4 *pu4RetValFsMsdpPeerFsmEstablishedTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerFsmEstablishedTime (INT4 i4FsMsdpPeerAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMsdpPeerRemoteAddress,
                                    UINT4
                                    *pu4RetValFsMsdpPeerFsmEstablishedTime)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpPeerFsmEstablishedTime =
        MsdpFsMsdpPeerEntry.MibObject.u4FsMsdpPeerFsmEstablishedTime;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerInMessageTime
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                UINT4 *pu4RetValFsMsdpPeerInMessageTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerInMessageTime (INT4 i4FsMsdpPeerAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsMsdpPeerRemoteAddress,
                               UINT4 *pu4RetValFsMsdpPeerInMessageTime)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpPeerInMessageTime =
        MsdpFsMsdpPeerEntry.MibObject.u4FsMsdpPeerInMessageTime;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerLocalAddress
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsMsdpPeerLocalAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerLocalAddress (INT4 i4FsMsdpPeerAddrType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsMsdpPeerRemoteAddress,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsMsdpPeerLocalAddress)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsMsdpPeerLocalAddress->pu1_OctetList,
            MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerLocalAddress,
            MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerLocalAddressLen);
    pRetValFsMsdpPeerLocalAddress->i4_Length =
        MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerLocalAddressLen;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerConnectRetryInterval
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                INT4 *pi4RetValFsMsdpPeerConnectRetryInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerConnectRetryInterval (INT4 i4FsMsdpPeerAddrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMsdpPeerRemoteAddress,
                                      INT4
                                      *pi4RetValFsMsdpPeerConnectRetryInterval)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMsdpPeerConnectRetryInterval =
        MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerConnectRetryInterval;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerHoldTimeConfigured
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                INT4 *pi4RetValFsMsdpPeerHoldTimeConfigured
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerHoldTimeConfigured (INT4 i4FsMsdpPeerAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMsdpPeerRemoteAddress,
                                    INT4 *pi4RetValFsMsdpPeerHoldTimeConfigured)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMsdpPeerHoldTimeConfigured =
        MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerHoldTimeConfigured;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerKeepAliveConfigured
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                INT4 *pi4RetValFsMsdpPeerKeepAliveConfigured
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerKeepAliveConfigured (INT4 i4FsMsdpPeerAddrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMsdpPeerRemoteAddress,
                                     INT4
                                     *pi4RetValFsMsdpPeerKeepAliveConfigured)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMsdpPeerKeepAliveConfigured =
        MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerKeepAliveConfigured;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerDataTtl
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                INT4 *pi4RetValFsMsdpPeerDataTtl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerDataTtl (INT4 i4FsMsdpPeerAddrType,
                         tSNMP_OCTET_STRING_TYPE * pFsMsdpPeerRemoteAddress,
                         INT4 *pi4RetValFsMsdpPeerDataTtl)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMsdpPeerDataTtl =
        MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerDataTtl;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerStatus
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                INT4 *pi4RetValFsMsdpPeerStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerStatus (INT4 i4FsMsdpPeerAddrType,
                        tSNMP_OCTET_STRING_TYPE * pFsMsdpPeerRemoteAddress,
                        INT4 *pi4RetValFsMsdpPeerStatus)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMsdpPeerStatus =
        MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerStatus;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerRemotePort
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                INT4 *pi4RetValFsMsdpPeerRemotePort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerRemotePort (INT4 i4FsMsdpPeerAddrType,
                            tSNMP_OCTET_STRING_TYPE * pFsMsdpPeerRemoteAddress,
                            INT4 *pi4RetValFsMsdpPeerRemotePort)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMsdpPeerRemotePort =
        MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemotePort;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerLocalPort
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                INT4 *pi4RetValFsMsdpPeerLocalPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerLocalPort (INT4 i4FsMsdpPeerAddrType,
                           tSNMP_OCTET_STRING_TYPE * pFsMsdpPeerRemoteAddress,
                           INT4 *pi4RetValFsMsdpPeerLocalPort)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMsdpPeerLocalPort =
        MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerLocalPort;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerEncapsulationType
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                INT4 *pi4RetValFsMsdpPeerEncapsulationType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerEncapsulationType (INT4 i4FsMsdpPeerAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMsdpPeerRemoteAddress,
                                   INT4 *pi4RetValFsMsdpPeerEncapsulationType)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMsdpPeerEncapsulationType =
        MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerEncapsulationType;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerConnectionAttempts
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                UINT4 *pu4RetValFsMsdpPeerConnectionAttempts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerConnectionAttempts (INT4 i4FsMsdpPeerAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMsdpPeerRemoteAddress,
                                    UINT4
                                    *pu4RetValFsMsdpPeerConnectionAttempts)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpPeerConnectionAttempts =
        MsdpFsMsdpPeerEntry.MibObject.u4FsMsdpPeerConnectionAttempts;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerDiscontinuityTime
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                UINT4 *pu4RetValFsMsdpPeerDiscontinuityTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerDiscontinuityTime (INT4 i4FsMsdpPeerAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMsdpPeerRemoteAddress,
                                   UINT4 *pu4RetValFsMsdpPeerDiscontinuityTime)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpPeerDiscontinuityTime =
        MsdpFsMsdpPeerEntry.MibObject.u4FsMsdpPeerDiscontinuityTime;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerMD5AuthPassword
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsMsdpPeerMD5AuthPassword
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerMD5AuthPassword (INT4 i4FsMsdpPeerAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMsdpPeerRemoteAddress,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsMsdpPeerMD5AuthPassword)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Prevention of plaintext password reading */

    pRetValFsMsdpPeerMD5AuthPassword->i4_Length = 0;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerMD5AuthPwdStat
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                INT4 *pi4RetValFsMsdpPeerMD5AuthPwdStat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerMD5AuthPwdStat (INT4 i4FsMsdpPeerAddrType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMsdpPeerRemoteAddress,
                                INT4 *pi4RetValFsMsdpPeerMD5AuthPwdStat)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMsdpPeerMD5AuthPwdStat =
        MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerMD5AuthPwdStat;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerMD5FailCount
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                INT4 *pi4RetValFsMsdpPeerMD5FailCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerMD5FailCount (INT4 i4FsMsdpPeerAddrType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsMsdpPeerRemoteAddress,
                              INT4 *pi4RetValFsMsdpPeerMD5FailCount)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMsdpPeerMD5FailCount =
        MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerMD5FailCount;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerMD5SuccessCount
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                INT4 *pi4RetValFsMsdpPeerMD5SuccessCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerMD5SuccessCount (INT4 i4FsMsdpPeerAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMsdpPeerRemoteAddress,
                                 INT4 *pi4RetValFsMsdpPeerMD5SuccessCount)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMsdpPeerMD5SuccessCount =
        MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerMD5SuccessCount;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerInSAResponses
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                UINT4 *pu4RetValFsMsdpPeerInSAResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerInSAResponses (INT4 i4FsMsdpPeerAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsMsdpPeerRemoteAddress,
                               UINT4 *pu4RetValFsMsdpPeerInSAResponses)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpPeerInSAResponses =
        MsdpFsMsdpPeerEntry.MibObject.u4FsMsdpPeerInSAResponses;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerOutSAResponses
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                UINT4 *pu4RetValFsMsdpPeerOutSAResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerOutSAResponses (INT4 i4FsMsdpPeerAddrType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMsdpPeerRemoteAddress,
                                UINT4 *pu4RetValFsMsdpPeerOutSAResponses)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpPeerOutSAResponses =
        MsdpFsMsdpPeerEntry.MibObject.u4FsMsdpPeerOutSAResponses;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerUpTime
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                UINT4 *pu4RetValFsMsdpPeerUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerUpTime (INT4 i4FsMsdpPeerAddrType,
                        tSNMP_OCTET_STRING_TYPE * pFsMsdpPeerRemoteAddress,
                        UINT4 *pu4RetValFsMsdpPeerUpTime)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpPeerUpTime =
        MsdpFsMsdpPeerEntry.MibObject.u4FsMsdpPeerUpTime;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerInKeepAliveCount
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                UINT4 *pu4RetValFsMsdpPeerInKeepAliveCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerInKeepAliveCount (INT4 i4FsMsdpPeerAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMsdpPeerRemoteAddress,
                                  UINT4 *pu4RetValFsMsdpPeerInKeepAliveCount)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpPeerInKeepAliveCount =
        MsdpFsMsdpPeerEntry.MibObject.u4FsMsdpPeerInKeepAliveCount;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerOutKeepAliveCount
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                UINT4 *pu4RetValFsMsdpPeerOutKeepAliveCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerOutKeepAliveCount (INT4 i4FsMsdpPeerAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMsdpPeerRemoteAddress,
                                   UINT4 *pu4RetValFsMsdpPeerOutKeepAliveCount)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpPeerOutKeepAliveCount =
        MsdpFsMsdpPeerEntry.MibObject.u4FsMsdpPeerOutKeepAliveCount;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerDataTtlErrorCount
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                UINT4 *pu4RetValFsMsdpPeerDataTtlErrorCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerDataTtlErrorCount (INT4 i4FsMsdpPeerAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMsdpPeerRemoteAddress,
                                   UINT4 *pu4RetValFsMsdpPeerDataTtlErrorCount)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpPeerDataTtlErrorCount =
        MsdpFsMsdpPeerEntry.MibObject.u4FsMsdpPeerDataTtlErrorCount;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerAdminStatus
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                INT4 *pi4RetValFsMsdpPeerAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerAdminStatus (INT4 i4FsMsdpPeerAddrType,
                             tSNMP_OCTET_STRING_TYPE * pFsMsdpPeerRemoteAddress,
                             INT4 *pi4RetValFsMsdpPeerAdminStatus)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;

    if (MsdpGetAllFsMsdpPeerTable (&MsdpFsMsdpPeerEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMsdpPeerAdminStatus =
        MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAdminStatus;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpSACachePeerLearnedFrom
 Input       :  The Indices
                FsMsdpSACacheAddrType
                FsMsdpSACacheGroupAddr
                FsMsdpSACacheSourceAddr
                FsMsdpSACacheOriginRP

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsMsdpSACachePeerLearnedFrom
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpSACachePeerLearnedFrom (INT4 i4FsMsdpSACacheAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMsdpSACacheGroupAddr,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMsdpSACacheSourceAddr,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMsdpSACacheOriginRP,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsMsdpSACachePeerLearnedFrom)
{
    tMsdpFsMsdpSACacheEntry MsdpFsMsdpSACacheEntry;

    MEMSET (&MsdpFsMsdpSACacheEntry, 0, sizeof (tMsdpFsMsdpSACacheEntry));

    /* Assign the index */
    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheAddrType =
        i4FsMsdpSACacheAddrType;

    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheGroupAddr,
            pFsMsdpSACacheGroupAddr->pu1_OctetList,
            pFsMsdpSACacheGroupAddr->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheGroupAddrLen =
        pFsMsdpSACacheGroupAddr->i4_Length;
    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheSourceAddr,
            pFsMsdpSACacheSourceAddr->pu1_OctetList,
            pFsMsdpSACacheSourceAddr->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheSourceAddrLen =
        pFsMsdpSACacheSourceAddr->i4_Length;
    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheOriginRP,
            pFsMsdpSACacheOriginRP->pu1_OctetList,
            pFsMsdpSACacheOriginRP->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheOriginRPLen =
        pFsMsdpSACacheOriginRP->i4_Length;

    if (MsdpGetAllFsMsdpSACacheTable (&MsdpFsMsdpSACacheEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsMsdpSACachePeerLearnedFrom->pu1_OctetList,
            MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACachePeerLearnedFrom,
            MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACachePeerLearnedFromLen);
    pRetValFsMsdpSACachePeerLearnedFrom->i4_Length =
        MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACachePeerLearnedFromLen;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpSACacheRPFPeer
 Input       :  The Indices
                FsMsdpSACacheAddrType
                FsMsdpSACacheGroupAddr
                FsMsdpSACacheSourceAddr
                FsMsdpSACacheOriginRP

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsMsdpSACacheRPFPeer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpSACacheRPFPeer (INT4 i4FsMsdpSACacheAddrType,
                            tSNMP_OCTET_STRING_TYPE * pFsMsdpSACacheGroupAddr,
                            tSNMP_OCTET_STRING_TYPE * pFsMsdpSACacheSourceAddr,
                            tSNMP_OCTET_STRING_TYPE * pFsMsdpSACacheOriginRP,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsMsdpSACacheRPFPeer)
{
    tMsdpFsMsdpSACacheEntry MsdpFsMsdpSACacheEntry;

    MEMSET (&MsdpFsMsdpSACacheEntry, 0, sizeof (tMsdpFsMsdpSACacheEntry));

    /* Assign the index */
    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheAddrType =
        i4FsMsdpSACacheAddrType;

    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheGroupAddr,
            pFsMsdpSACacheGroupAddr->pu1_OctetList,
            pFsMsdpSACacheGroupAddr->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheGroupAddrLen =
        pFsMsdpSACacheGroupAddr->i4_Length;
    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheSourceAddr,
            pFsMsdpSACacheSourceAddr->pu1_OctetList,
            pFsMsdpSACacheSourceAddr->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheSourceAddrLen =
        pFsMsdpSACacheSourceAddr->i4_Length;
    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheOriginRP,
            pFsMsdpSACacheOriginRP->pu1_OctetList,
            pFsMsdpSACacheOriginRP->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheOriginRPLen =
        pFsMsdpSACacheOriginRP->i4_Length;

    if (MsdpGetAllFsMsdpSACacheTable (&MsdpFsMsdpSACacheEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsMsdpSACacheRPFPeer->pu1_OctetList,
            MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheRPFPeer,
            MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheRPFPeerLen);
    pRetValFsMsdpSACacheRPFPeer->i4_Length =
        MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheRPFPeerLen;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpSACacheInSAs
 Input       :  The Indices
                FsMsdpSACacheAddrType
                FsMsdpSACacheGroupAddr
                FsMsdpSACacheSourceAddr
                FsMsdpSACacheOriginRP

                The Object 
                UINT4 *pu4RetValFsMsdpSACacheInSAs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpSACacheInSAs (INT4 i4FsMsdpSACacheAddrType,
                          tSNMP_OCTET_STRING_TYPE * pFsMsdpSACacheGroupAddr,
                          tSNMP_OCTET_STRING_TYPE * pFsMsdpSACacheSourceAddr,
                          tSNMP_OCTET_STRING_TYPE * pFsMsdpSACacheOriginRP,
                          UINT4 *pu4RetValFsMsdpSACacheInSAs)
{
    tMsdpFsMsdpSACacheEntry MsdpFsMsdpSACacheEntry;

    MEMSET (&MsdpFsMsdpSACacheEntry, 0, sizeof (tMsdpFsMsdpSACacheEntry));

    /* Assign the index */
    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheAddrType =
        i4FsMsdpSACacheAddrType;

    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheGroupAddr,
            pFsMsdpSACacheGroupAddr->pu1_OctetList,
            pFsMsdpSACacheGroupAddr->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheGroupAddrLen =
        pFsMsdpSACacheGroupAddr->i4_Length;
    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheSourceAddr,
            pFsMsdpSACacheSourceAddr->pu1_OctetList,
            pFsMsdpSACacheSourceAddr->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheSourceAddrLen =
        pFsMsdpSACacheSourceAddr->i4_Length;
    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheOriginRP,
            pFsMsdpSACacheOriginRP->pu1_OctetList,
            pFsMsdpSACacheOriginRP->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheOriginRPLen =
        pFsMsdpSACacheOriginRP->i4_Length;

    if (MsdpGetAllFsMsdpSACacheTable (&MsdpFsMsdpSACacheEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpSACacheInSAs =
        MsdpFsMsdpSACacheEntry.MibObject.u4FsMsdpSACacheInSAs;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpSACacheInDataPackets
 Input       :  The Indices
                FsMsdpSACacheAddrType
                FsMsdpSACacheGroupAddr
                FsMsdpSACacheSourceAddr
                FsMsdpSACacheOriginRP

                The Object 
                UINT4 *pu4RetValFsMsdpSACacheInDataPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpSACacheInDataPackets (INT4 i4FsMsdpSACacheAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMsdpSACacheGroupAddr,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMsdpSACacheSourceAddr,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMsdpSACacheOriginRP,
                                  UINT4 *pu4RetValFsMsdpSACacheInDataPackets)
{
    tMsdpFsMsdpSACacheEntry MsdpFsMsdpSACacheEntry;

    MEMSET (&MsdpFsMsdpSACacheEntry, 0, sizeof (tMsdpFsMsdpSACacheEntry));

    /* Assign the index */
    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheAddrType =
        i4FsMsdpSACacheAddrType;

    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheGroupAddr,
            pFsMsdpSACacheGroupAddr->pu1_OctetList,
            pFsMsdpSACacheGroupAddr->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheGroupAddrLen =
        pFsMsdpSACacheGroupAddr->i4_Length;
    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheSourceAddr,
            pFsMsdpSACacheSourceAddr->pu1_OctetList,
            pFsMsdpSACacheSourceAddr->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheSourceAddrLen =
        pFsMsdpSACacheSourceAddr->i4_Length;
    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheOriginRP,
            pFsMsdpSACacheOriginRP->pu1_OctetList,
            pFsMsdpSACacheOriginRP->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheOriginRPLen =
        pFsMsdpSACacheOriginRP->i4_Length;

    if (MsdpGetAllFsMsdpSACacheTable (&MsdpFsMsdpSACacheEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpSACacheInDataPackets =
        MsdpFsMsdpSACacheEntry.MibObject.u4FsMsdpSACacheInDataPackets;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpSACacheUpTime
 Input       :  The Indices
                FsMsdpSACacheAddrType
                FsMsdpSACacheGroupAddr
                FsMsdpSACacheSourceAddr
                FsMsdpSACacheOriginRP

                The Object 
                UINT4 *pu4RetValFsMsdpSACacheUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpSACacheUpTime (INT4 i4FsMsdpSACacheAddrType,
                           tSNMP_OCTET_STRING_TYPE * pFsMsdpSACacheGroupAddr,
                           tSNMP_OCTET_STRING_TYPE * pFsMsdpSACacheSourceAddr,
                           tSNMP_OCTET_STRING_TYPE * pFsMsdpSACacheOriginRP,
                           UINT4 *pu4RetValFsMsdpSACacheUpTime)
{
    tMsdpFsMsdpSACacheEntry MsdpFsMsdpSACacheEntry;

    MEMSET (&MsdpFsMsdpSACacheEntry, 0, sizeof (tMsdpFsMsdpSACacheEntry));

    /* Assign the index */
    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheAddrType =
        i4FsMsdpSACacheAddrType;

    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheGroupAddr,
            pFsMsdpSACacheGroupAddr->pu1_OctetList,
            pFsMsdpSACacheGroupAddr->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheGroupAddrLen =
        pFsMsdpSACacheGroupAddr->i4_Length;
    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheSourceAddr,
            pFsMsdpSACacheSourceAddr->pu1_OctetList,
            pFsMsdpSACacheSourceAddr->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheSourceAddrLen =
        pFsMsdpSACacheSourceAddr->i4_Length;
    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheOriginRP,
            pFsMsdpSACacheOriginRP->pu1_OctetList,
            pFsMsdpSACacheOriginRP->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheOriginRPLen =
        pFsMsdpSACacheOriginRP->i4_Length;

    if (MsdpGetAllFsMsdpSACacheTable (&MsdpFsMsdpSACacheEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpSACacheUpTime =
        MsdpFsMsdpSACacheEntry.MibObject.u4FsMsdpSACacheUpTime;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpSACacheExpiryTime
 Input       :  The Indices
                FsMsdpSACacheAddrType
                FsMsdpSACacheGroupAddr
                FsMsdpSACacheSourceAddr
                FsMsdpSACacheOriginRP

                The Object 
                UINT4 *pu4RetValFsMsdpSACacheExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpSACacheExpiryTime (INT4 i4FsMsdpSACacheAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsMsdpSACacheGroupAddr,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsMsdpSACacheSourceAddr,
                               tSNMP_OCTET_STRING_TYPE * pFsMsdpSACacheOriginRP,
                               UINT4 *pu4RetValFsMsdpSACacheExpiryTime)
{
    tMsdpFsMsdpSACacheEntry MsdpFsMsdpSACacheEntry;

    MEMSET (&MsdpFsMsdpSACacheEntry, 0, sizeof (tMsdpFsMsdpSACacheEntry));

    /* Assign the index */
    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheAddrType =
        i4FsMsdpSACacheAddrType;

    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheGroupAddr,
            pFsMsdpSACacheGroupAddr->pu1_OctetList,
            pFsMsdpSACacheGroupAddr->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheGroupAddrLen =
        pFsMsdpSACacheGroupAddr->i4_Length;
    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheSourceAddr,
            pFsMsdpSACacheSourceAddr->pu1_OctetList,
            pFsMsdpSACacheSourceAddr->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheSourceAddrLen =
        pFsMsdpSACacheSourceAddr->i4_Length;
    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheOriginRP,
            pFsMsdpSACacheOriginRP->pu1_OctetList,
            pFsMsdpSACacheOriginRP->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheOriginRPLen =
        pFsMsdpSACacheOriginRP->i4_Length;

    if (MsdpGetAllFsMsdpSACacheTable (&MsdpFsMsdpSACacheEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMsdpSACacheExpiryTime =
        MsdpFsMsdpSACacheEntry.MibObject.u4FsMsdpSACacheExpiryTime;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpSACacheStatus
 Input       :  The Indices
                FsMsdpSACacheAddrType
                FsMsdpSACacheGroupAddr
                FsMsdpSACacheSourceAddr
                FsMsdpSACacheOriginRP

                The Object 
                INT4 *pi4RetValFsMsdpSACacheStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpSACacheStatus (INT4 i4FsMsdpSACacheAddrType,
                           tSNMP_OCTET_STRING_TYPE * pFsMsdpSACacheGroupAddr,
                           tSNMP_OCTET_STRING_TYPE * pFsMsdpSACacheSourceAddr,
                           tSNMP_OCTET_STRING_TYPE * pFsMsdpSACacheOriginRP,
                           INT4 *pi4RetValFsMsdpSACacheStatus)
{
    tMsdpFsMsdpSACacheEntry MsdpFsMsdpSACacheEntry;

    MEMSET (&MsdpFsMsdpSACacheEntry, 0, sizeof (tMsdpFsMsdpSACacheEntry));

    /* Assign the index */
    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheAddrType =
        i4FsMsdpSACacheAddrType;

    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheGroupAddr,
            pFsMsdpSACacheGroupAddr->pu1_OctetList,
            pFsMsdpSACacheGroupAddr->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheGroupAddrLen =
        pFsMsdpSACacheGroupAddr->i4_Length;
    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheSourceAddr,
            pFsMsdpSACacheSourceAddr->pu1_OctetList,
            pFsMsdpSACacheSourceAddr->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheSourceAddrLen =
        pFsMsdpSACacheSourceAddr->i4_Length;
    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheOriginRP,
            pFsMsdpSACacheOriginRP->pu1_OctetList,
            pFsMsdpSACacheOriginRP->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheOriginRPLen =
        pFsMsdpSACacheOriginRP->i4_Length;

    if (MsdpGetAllFsMsdpSACacheTable (&MsdpFsMsdpSACacheEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMsdpSACacheStatus =
        MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheStatus;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpMeshGroupStatus
 Input       :  The Indices
                FsMsdpMeshGroupName
                FsMsdpMeshGroupAddrType
                FsMsdpMeshGroupPeerAddress

                The Object 
                INT4 *pi4RetValFsMsdpMeshGroupStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpMeshGroupStatus (tSNMP_OCTET_STRING_TYPE * pFsMsdpMeshGroupName,
                             INT4 i4FsMsdpMeshGroupAddrType,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsMsdpMeshGroupPeerAddress,
                             INT4 *pi4RetValFsMsdpMeshGroupStatus)
{
    tMsdpFsMsdpMeshGroupEntry MsdpFsMsdpMeshGroupEntry;

    MEMSET (&MsdpFsMsdpMeshGroupEntry, 0, sizeof (tMsdpFsMsdpMeshGroupEntry));

    /* Assign the index */
    MEMCPY (MsdpFsMsdpMeshGroupEntry.MibObject.au1FsMsdpMeshGroupName,
            pFsMsdpMeshGroupName->pu1_OctetList,
            pFsMsdpMeshGroupName->i4_Length);

    MsdpFsMsdpMeshGroupEntry.MibObject.i4FsMsdpMeshGroupNameLen =
        pFsMsdpMeshGroupName->i4_Length;
    MsdpFsMsdpMeshGroupEntry.MibObject.i4FsMsdpMeshGroupAddrType =
        i4FsMsdpMeshGroupAddrType;

    MEMCPY (MsdpFsMsdpMeshGroupEntry.MibObject.au1FsMsdpMeshGroupPeerAddress,
            pFsMsdpMeshGroupPeerAddress->pu1_OctetList,
            pFsMsdpMeshGroupPeerAddress->i4_Length);

    MsdpFsMsdpMeshGroupEntry.MibObject.i4FsMsdpMeshGroupPeerAddressLen =
        pFsMsdpMeshGroupPeerAddress->i4_Length;

    if (MsdpGetAllFsMsdpMeshGroupTable (&MsdpFsMsdpMeshGroupEntry) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMsdpMeshGroupStatus =
        MsdpFsMsdpMeshGroupEntry.MibObject.i4FsMsdpMeshGroupStatus;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpRPAddress
 Input       :  The Indices
                FsMsdpRPAddrType

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsMsdpRPAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpRPAddress (INT4 i4FsMsdpRPAddrType,
                       tSNMP_OCTET_STRING_TYPE * pRetValFsMsdpRPAddress)
{
    tMsdpFsMsdpRPEntry  MsdpFsMsdpRPEntry;

    MEMSET (&MsdpFsMsdpRPEntry, 0, sizeof (tMsdpFsMsdpRPEntry));

    /* Assign the index */
    MsdpFsMsdpRPEntry.MibObject.i4FsMsdpRPAddrType = i4FsMsdpRPAddrType;

    if (MsdpGetAllFsMsdpRPTable (&MsdpFsMsdpRPEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsMsdpRPAddress->pu1_OctetList,
            MsdpFsMsdpRPEntry.MibObject.au1FsMsdpRPAddress,
            MsdpFsMsdpRPEntry.MibObject.i4FsMsdpRPAddressLen);
    pRetValFsMsdpRPAddress->i4_Length =
        MsdpFsMsdpRPEntry.MibObject.i4FsMsdpRPAddressLen;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpRPOperStatus
 Input       :  The Indices
                FsMsdpRPAddrType

                The Object 
                INT4 *pi4RetValFsMsdpRPOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpRPOperStatus (INT4 i4FsMsdpRPAddrType,
                          INT4 *pi4RetValFsMsdpRPOperStatus)
{
    tMsdpFsMsdpRPEntry  MsdpFsMsdpRPEntry;

    MEMSET (&MsdpFsMsdpRPEntry, 0, sizeof (tMsdpFsMsdpRPEntry));

    /* Assign the index */
    MsdpFsMsdpRPEntry.MibObject.i4FsMsdpRPAddrType = i4FsMsdpRPAddrType;

    if (MsdpGetAllFsMsdpRPTable (&MsdpFsMsdpRPEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMsdpRPOperStatus =
        MsdpFsMsdpRPEntry.MibObject.i4FsMsdpRPOperStatus;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpRPStatus
 Input       :  The Indices
                FsMsdpRPAddrType

                The Object 
                INT4 *pi4RetValFsMsdpRPStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpRPStatus (INT4 i4FsMsdpRPAddrType, INT4 *pi4RetValFsMsdpRPStatus)
{
    tMsdpFsMsdpRPEntry  MsdpFsMsdpRPEntry;

    MEMSET (&MsdpFsMsdpRPEntry, 0, sizeof (tMsdpFsMsdpRPEntry));

    /* Assign the index */
    MsdpFsMsdpRPEntry.MibObject.i4FsMsdpRPAddrType = i4FsMsdpRPAddrType;

    if (MsdpGetAllFsMsdpRPTable (&MsdpFsMsdpRPEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMsdpRPStatus = MsdpFsMsdpRPEntry.MibObject.i4FsMsdpRPStatus;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerFilterRouteMap
 Input       :  The Indices
                FsMsdpPeerFilterAddrType

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsMsdpPeerFilterRouteMap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerFilterRouteMap (INT4 i4FsMsdpPeerFilterAddrType,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsMsdpPeerFilterRouteMap)
{
    tMsdpFsMsdpPeerFilterEntry MsdpFsMsdpPeerFilterEntry;

    MEMSET (&MsdpFsMsdpPeerFilterEntry, 0, sizeof (tMsdpFsMsdpPeerFilterEntry));

    /* Assign the index */
    MsdpFsMsdpPeerFilterEntry.MibObject.i4FsMsdpPeerFilterAddrType =
        i4FsMsdpPeerFilterAddrType;

    if (MsdpGetAllFsMsdpPeerFilterTable (&MsdpFsMsdpPeerFilterEntry) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsMsdpPeerFilterRouteMap->pu1_OctetList,
            MsdpFsMsdpPeerFilterEntry.MibObject.au1FsMsdpPeerFilterRouteMap,
            MsdpFsMsdpPeerFilterEntry.MibObject.i4FsMsdpPeerFilterRouteMapLen);
    pRetValFsMsdpPeerFilterRouteMap->i4_Length =
        MsdpFsMsdpPeerFilterEntry.MibObject.i4FsMsdpPeerFilterRouteMapLen;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpPeerFilterStatus
 Input       :  The Indices
                FsMsdpPeerFilterAddrType

                The Object 
                INT4 *pi4RetValFsMsdpPeerFilterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpPeerFilterStatus (INT4 i4FsMsdpPeerFilterAddrType,
                              INT4 *pi4RetValFsMsdpPeerFilterStatus)
{
    tMsdpFsMsdpPeerFilterEntry MsdpFsMsdpPeerFilterEntry;

    MEMSET (&MsdpFsMsdpPeerFilterEntry, 0, sizeof (tMsdpFsMsdpPeerFilterEntry));

    /* Assign the index */
    MsdpFsMsdpPeerFilterEntry.MibObject.i4FsMsdpPeerFilterAddrType =
        i4FsMsdpPeerFilterAddrType;

    if (MsdpGetAllFsMsdpPeerFilterTable (&MsdpFsMsdpPeerFilterEntry) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMsdpPeerFilterStatus =
        MsdpFsMsdpPeerFilterEntry.MibObject.i4FsMsdpPeerFilterStatus;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpSARedistributionStatus
 Input       :  The Indices
                FsMsdpSARedistributionAddrType

                The Object 
                INT4 *pi4RetValFsMsdpSARedistributionStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpSARedistributionStatus (INT4 i4FsMsdpSARedistributionAddrType,
                                    INT4 *pi4RetValFsMsdpSARedistributionStatus)
{
    tMsdpFsMsdpSARedistributionEntry MsdpFsMsdpSARedistributionEntry;

    MEMSET (&MsdpFsMsdpSARedistributionEntry, 0,
            sizeof (tMsdpFsMsdpSARedistributionEntry));

    /* Assign the index */
    MsdpFsMsdpSARedistributionEntry.MibObject.i4FsMsdpSARedistributionAddrType =
        i4FsMsdpSARedistributionAddrType;

    if (MsdpGetAllFsMsdpSARedistributionTable (&MsdpFsMsdpSARedistributionEntry)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMsdpSARedistributionStatus =
        MsdpFsMsdpSARedistributionEntry.MibObject.
        i4FsMsdpSARedistributionStatus;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpSARedistributionRouteMap
 Input       :  The Indices
                FsMsdpSARedistributionAddrType

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsMsdpSARedistributionRouteMap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpSARedistributionRouteMap (INT4 i4FsMsdpSARedistributionAddrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsMsdpSARedistributionRouteMap)
{
    tMsdpFsMsdpSARedistributionEntry MsdpFsMsdpSARedistributionEntry;

    MEMSET (&MsdpFsMsdpSARedistributionEntry, 0,
            sizeof (tMsdpFsMsdpSARedistributionEntry));

    /* Assign the index */
    MsdpFsMsdpSARedistributionEntry.MibObject.i4FsMsdpSARedistributionAddrType =
        i4FsMsdpSARedistributionAddrType;

    if (MsdpGetAllFsMsdpSARedistributionTable (&MsdpFsMsdpSARedistributionEntry)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsMsdpSARedistributionRouteMap->pu1_OctetList,
            MsdpFsMsdpSARedistributionEntry.MibObject.
            au1FsMsdpSARedistributionRouteMap,
            MsdpFsMsdpSARedistributionEntry.MibObject.
            i4FsMsdpSARedistributionRouteMapLen);
    pRetValFsMsdpSARedistributionRouteMap->i4_Length =
        MsdpFsMsdpSARedistributionEntry.MibObject.
        i4FsMsdpSARedistributionRouteMapLen;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsdpSARedistributionRouteMapStat
 Input       :  The Indices
                FsMsdpSARedistributionAddrType

                The Object 
                INT4 *pi4RetValFsMsdpSARedistributionRouteMapStat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsdpSARedistributionRouteMapStat (INT4 i4FsMsdpSARedistributionAddrType,
                                          INT4
                                          *pi4RetValFsMsdpSARedistributionRouteMapStat)
{
    tMsdpFsMsdpSARedistributionEntry MsdpFsMsdpSARedistributionEntry;

    MEMSET (&MsdpFsMsdpSARedistributionEntry, 0,
            sizeof (tMsdpFsMsdpSARedistributionEntry));

    /* Assign the index */
    MsdpFsMsdpSARedistributionEntry.MibObject.i4FsMsdpSARedistributionAddrType =
        i4FsMsdpSARedistributionAddrType;

    if (MsdpGetAllFsMsdpSARedistributionTable (&MsdpFsMsdpSARedistributionEntry)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMsdpSARedistributionRouteMapStat =
        MsdpFsMsdpSARedistributionEntry.MibObject.
        i4FsMsdpSARedistributionRouteMapStat;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpTraceLevel
 Input       :  The Indices

                The Object 
             :  INT4 i4SetValFsMsdpTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpTraceLevel (INT4 i4SetValFsMsdpTraceLevel)
{
    if (MsdpSetFsMsdpTraceLevel (i4SetValFsMsdpTraceLevel) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpIPv4AdminStat
 Input       :  The Indices

                The Object 
             :  INT4 i4SetValFsMsdpIPv4AdminStat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpIPv4AdminStat (INT4 i4SetValFsMsdpIPv4AdminStat)
{
    if (MsdpSetFsMsdpIPv4AdminStat (i4SetValFsMsdpIPv4AdminStat) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpIPv6AdminStat
 Input       :  The Indices

                The Object 
             :  INT4 i4SetValFsMsdpIPv6AdminStat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpIPv6AdminStat (INT4 i4SetValFsMsdpIPv6AdminStat)
{
    if (MsdpSetFsMsdpIPv6AdminStat (i4SetValFsMsdpIPv6AdminStat) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpCacheLifetime
 Input       :  The Indices

                The Object 
             :  UINT4 u4SetValFsMsdpCacheLifetime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpCacheLifetime (UINT4 u4SetValFsMsdpCacheLifetime)
{
    if (MsdpSetFsMsdpCacheLifetime (u4SetValFsMsdpCacheLifetime) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpMaxPeerSessions
 Input       :  The Indices

                The Object 
             :  INT4 i4SetValFsMsdpMaxPeerSessions
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpMaxPeerSessions (INT4 i4SetValFsMsdpMaxPeerSessions)
{
    if (MsdpSetFsMsdpMaxPeerSessions (i4SetValFsMsdpMaxPeerSessions) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpMappingComponentId
 Input       :  The Indices

                The Object 
             :  INT4 i4SetValFsMsdpMappingComponentId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpMappingComponentId (INT4 i4SetValFsMsdpMappingComponentId)
{
    if (MsdpSetFsMsdpMappingComponentId (i4SetValFsMsdpMappingComponentId) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpListenerPort
 Input       :  The Indices

                The Object 
             :  INT4 i4SetValFsMsdpListenerPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpListenerPort (INT4 i4SetValFsMsdpListenerPort)
{
    if (MsdpSetFsMsdpListenerPort (i4SetValFsMsdpListenerPort) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpPeerFilter
 Input       :  The Indices

                The Object 
             :  INT4 i4SetValFsMsdpPeerFilter
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpPeerFilter (INT4 i4SetValFsMsdpPeerFilter)
{
    if (MsdpSetFsMsdpPeerFilter (i4SetValFsMsdpPeerFilter) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpPeerLocalAddress
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsMsdpPeerLocalAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpPeerLocalAddress (INT4 i4FsMsdpPeerAddrType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsMsdpPeerRemoteAddress,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValFsMsdpPeerLocalAddress)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    tMsdpIsSetFsMsdpPeerEntry MsdpIsSetFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerEntry, 0, sizeof (tMsdpIsSetFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerRemoteAddress = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerLocalAddress,
            pSetValFsMsdpPeerLocalAddress->pu1_OctetList,
            pSetValFsMsdpPeerLocalAddress->i4_Length);
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerLocalAddressLen =
        pSetValFsMsdpPeerLocalAddress->i4_Length;

    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerLocalAddress = OSIX_TRUE;

    if (MsdpSetAllFsMsdpPeerTable
        (&MsdpFsMsdpPeerEntry, &MsdpIsSetFsMsdpPeerEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpPeerConnectRetryInterval
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
             :  INT4 i4SetValFsMsdpPeerConnectRetryInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpPeerConnectRetryInterval (INT4 i4FsMsdpPeerAddrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMsdpPeerRemoteAddress,
                                      INT4
                                      i4SetValFsMsdpPeerConnectRetryInterval)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    tMsdpIsSetFsMsdpPeerEntry MsdpIsSetFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerEntry, 0, sizeof (tMsdpIsSetFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerRemoteAddress = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerConnectRetryInterval =
        i4SetValFsMsdpPeerConnectRetryInterval;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerConnectRetryInterval = OSIX_TRUE;

    if (MsdpSetAllFsMsdpPeerTable
        (&MsdpFsMsdpPeerEntry, &MsdpIsSetFsMsdpPeerEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpPeerHoldTimeConfigured
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
             :  INT4 i4SetValFsMsdpPeerHoldTimeConfigured
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpPeerHoldTimeConfigured (INT4 i4FsMsdpPeerAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMsdpPeerRemoteAddress,
                                    INT4 i4SetValFsMsdpPeerHoldTimeConfigured)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    tMsdpIsSetFsMsdpPeerEntry MsdpIsSetFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerEntry, 0, sizeof (tMsdpIsSetFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerRemoteAddress = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerHoldTimeConfigured =
        i4SetValFsMsdpPeerHoldTimeConfigured;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerHoldTimeConfigured = OSIX_TRUE;

    if (MsdpSetAllFsMsdpPeerTable
        (&MsdpFsMsdpPeerEntry, &MsdpIsSetFsMsdpPeerEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpPeerKeepAliveConfigured
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
             :  INT4 i4SetValFsMsdpPeerKeepAliveConfigured
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpPeerKeepAliveConfigured (INT4 i4FsMsdpPeerAddrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMsdpPeerRemoteAddress,
                                     INT4 i4SetValFsMsdpPeerKeepAliveConfigured)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    tMsdpIsSetFsMsdpPeerEntry MsdpIsSetFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerEntry, 0, sizeof (tMsdpIsSetFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerRemoteAddress = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerKeepAliveConfigured =
        i4SetValFsMsdpPeerKeepAliveConfigured;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerKeepAliveConfigured = OSIX_TRUE;

    if (MsdpSetAllFsMsdpPeerTable
        (&MsdpFsMsdpPeerEntry, &MsdpIsSetFsMsdpPeerEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpPeerDataTtl
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
             :  INT4 i4SetValFsMsdpPeerDataTtl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpPeerDataTtl (INT4 i4FsMsdpPeerAddrType,
                         tSNMP_OCTET_STRING_TYPE * pFsMsdpPeerRemoteAddress,
                         INT4 i4SetValFsMsdpPeerDataTtl)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    tMsdpIsSetFsMsdpPeerEntry MsdpIsSetFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerEntry, 0, sizeof (tMsdpIsSetFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerRemoteAddress = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerDataTtl =
        i4SetValFsMsdpPeerDataTtl;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerDataTtl = OSIX_TRUE;

    if (MsdpSetAllFsMsdpPeerTable
        (&MsdpFsMsdpPeerEntry, &MsdpIsSetFsMsdpPeerEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpPeerStatus
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
             :  INT4 i4SetValFsMsdpPeerStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpPeerStatus (INT4 i4FsMsdpPeerAddrType,
                        tSNMP_OCTET_STRING_TYPE * pFsMsdpPeerRemoteAddress,
                        INT4 i4SetValFsMsdpPeerStatus)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    tMsdpIsSetFsMsdpPeerEntry MsdpIsSetFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerEntry, 0, sizeof (tMsdpIsSetFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerRemoteAddress = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerStatus = i4SetValFsMsdpPeerStatus;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerStatus = OSIX_TRUE;

    if (MsdpSetAllFsMsdpPeerTable
        (&MsdpFsMsdpPeerEntry, &MsdpIsSetFsMsdpPeerEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpPeerEncapsulationType
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
             :  INT4 i4SetValFsMsdpPeerEncapsulationType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpPeerEncapsulationType (INT4 i4FsMsdpPeerAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMsdpPeerRemoteAddress,
                                   INT4 i4SetValFsMsdpPeerEncapsulationType)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    tMsdpIsSetFsMsdpPeerEntry MsdpIsSetFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerEntry, 0, sizeof (tMsdpIsSetFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerRemoteAddress = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerEncapsulationType =
        i4SetValFsMsdpPeerEncapsulationType;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerEncapsulationType = OSIX_TRUE;

    if (MsdpSetAllFsMsdpPeerTable
        (&MsdpFsMsdpPeerEntry, &MsdpIsSetFsMsdpPeerEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpPeerMD5AuthPassword
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsMsdpPeerMD5AuthPassword
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpPeerMD5AuthPassword (INT4 i4FsMsdpPeerAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMsdpPeerRemoteAddress,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValFsMsdpPeerMD5AuthPassword)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    tMsdpIsSetFsMsdpPeerEntry MsdpIsSetFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerEntry, 0, sizeof (tMsdpIsSetFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerRemoteAddress = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerMD5AuthPassword,
            pSetValFsMsdpPeerMD5AuthPassword->pu1_OctetList,
            pSetValFsMsdpPeerMD5AuthPassword->i4_Length);
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerMD5AuthPasswordLen =
        pSetValFsMsdpPeerMD5AuthPassword->i4_Length;

    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerMD5AuthPassword = OSIX_TRUE;

    if (MsdpSetAllFsMsdpPeerTable
        (&MsdpFsMsdpPeerEntry, &MsdpIsSetFsMsdpPeerEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpPeerMD5AuthPwdStat
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
             :  INT4 i4SetValFsMsdpPeerMD5AuthPwdStat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpPeerMD5AuthPwdStat (INT4 i4FsMsdpPeerAddrType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMsdpPeerRemoteAddress,
                                INT4 i4SetValFsMsdpPeerMD5AuthPwdStat)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    tMsdpIsSetFsMsdpPeerEntry MsdpIsSetFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerEntry, 0, sizeof (tMsdpIsSetFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerRemoteAddress = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerMD5AuthPwdStat =
        i4SetValFsMsdpPeerMD5AuthPwdStat;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerMD5AuthPwdStat = OSIX_TRUE;

    if (MsdpSetAllFsMsdpPeerTable
        (&MsdpFsMsdpPeerEntry, &MsdpIsSetFsMsdpPeerEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpPeerAdminStatus
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
             :  INT4 i4SetValFsMsdpPeerAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpPeerAdminStatus (INT4 i4FsMsdpPeerAddrType,
                             tSNMP_OCTET_STRING_TYPE * pFsMsdpPeerRemoteAddress,
                             INT4 i4SetValFsMsdpPeerAdminStatus)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    tMsdpIsSetFsMsdpPeerEntry MsdpIsSetFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerEntry, 0, sizeof (tMsdpIsSetFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerRemoteAddress = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAdminStatus =
        i4SetValFsMsdpPeerAdminStatus;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerAdminStatus = OSIX_TRUE;

    if (MsdpSetAllFsMsdpPeerTable
        (&MsdpFsMsdpPeerEntry, &MsdpIsSetFsMsdpPeerEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpSACacheStatus
 Input       :  The Indices
                FsMsdpSACacheAddrType
                FsMsdpSACacheGroupAddr
                FsMsdpSACacheSourceAddr
                FsMsdpSACacheOriginRP

                The Object 
             :  INT4 i4SetValFsMsdpSACacheStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpSACacheStatus (INT4 i4FsMsdpSACacheAddrType,
                           tSNMP_OCTET_STRING_TYPE * pFsMsdpSACacheGroupAddr,
                           tSNMP_OCTET_STRING_TYPE * pFsMsdpSACacheSourceAddr,
                           tSNMP_OCTET_STRING_TYPE * pFsMsdpSACacheOriginRP,
                           INT4 i4SetValFsMsdpSACacheStatus)
{
    tMsdpFsMsdpSACacheEntry MsdpFsMsdpSACacheEntry;
    tMsdpIsSetFsMsdpSACacheEntry MsdpIsSetFsMsdpSACacheEntry;

    MEMSET (&MsdpFsMsdpSACacheEntry, 0, sizeof (tMsdpFsMsdpSACacheEntry));
    MEMSET (&MsdpIsSetFsMsdpSACacheEntry, 0,
            sizeof (tMsdpIsSetFsMsdpSACacheEntry));

    /* Assign the index */
    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheAddrType =
        i4FsMsdpSACacheAddrType;
    MsdpIsSetFsMsdpSACacheEntry.bFsMsdpSACacheAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheGroupAddr,
            pFsMsdpSACacheGroupAddr->pu1_OctetList,
            pFsMsdpSACacheGroupAddr->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheGroupAddrLen =
        pFsMsdpSACacheGroupAddr->i4_Length;
    MsdpIsSetFsMsdpSACacheEntry.bFsMsdpSACacheGroupAddr = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheSourceAddr,
            pFsMsdpSACacheSourceAddr->pu1_OctetList,
            pFsMsdpSACacheSourceAddr->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheSourceAddrLen =
        pFsMsdpSACacheSourceAddr->i4_Length;
    MsdpIsSetFsMsdpSACacheEntry.bFsMsdpSACacheSourceAddr = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheOriginRP,
            pFsMsdpSACacheOriginRP->pu1_OctetList,
            pFsMsdpSACacheOriginRP->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheOriginRPLen =
        pFsMsdpSACacheOriginRP->i4_Length;
    MsdpIsSetFsMsdpSACacheEntry.bFsMsdpSACacheOriginRP = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheStatus =
        i4SetValFsMsdpSACacheStatus;
    MsdpIsSetFsMsdpSACacheEntry.bFsMsdpSACacheStatus = OSIX_TRUE;

    if (MsdpSetAllFsMsdpSACacheTable
        (&MsdpFsMsdpSACacheEntry, &MsdpIsSetFsMsdpSACacheEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpMeshGroupStatus
 Input       :  The Indices
                FsMsdpMeshGroupName
                FsMsdpMeshGroupAddrType
                FsMsdpMeshGroupPeerAddress

                The Object 
             :  INT4 i4SetValFsMsdpMeshGroupStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpMeshGroupStatus (tSNMP_OCTET_STRING_TYPE * pFsMsdpMeshGroupName,
                             INT4 i4FsMsdpMeshGroupAddrType,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsMsdpMeshGroupPeerAddress,
                             INT4 i4SetValFsMsdpMeshGroupStatus)
{
    tMsdpFsMsdpMeshGroupEntry MsdpFsMsdpMeshGroupEntry;
    tMsdpIsSetFsMsdpMeshGroupEntry MsdpIsSetFsMsdpMeshGroupEntry;

    MEMSET (&MsdpFsMsdpMeshGroupEntry, 0, sizeof (tMsdpFsMsdpMeshGroupEntry));
    MEMSET (&MsdpIsSetFsMsdpMeshGroupEntry, 0,
            sizeof (tMsdpIsSetFsMsdpMeshGroupEntry));

    /* Assign the index */
    MEMCPY (MsdpFsMsdpMeshGroupEntry.MibObject.au1FsMsdpMeshGroupName,
            pFsMsdpMeshGroupName->pu1_OctetList,
            pFsMsdpMeshGroupName->i4_Length);

    MsdpFsMsdpMeshGroupEntry.MibObject.i4FsMsdpMeshGroupNameLen =
        pFsMsdpMeshGroupName->i4_Length;
    MsdpIsSetFsMsdpMeshGroupEntry.bFsMsdpMeshGroupName = OSIX_TRUE;

    MsdpFsMsdpMeshGroupEntry.MibObject.i4FsMsdpMeshGroupAddrType =
        i4FsMsdpMeshGroupAddrType;
    MsdpIsSetFsMsdpMeshGroupEntry.bFsMsdpMeshGroupAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpMeshGroupEntry.MibObject.au1FsMsdpMeshGroupPeerAddress,
            pFsMsdpMeshGroupPeerAddress->pu1_OctetList,
            pFsMsdpMeshGroupPeerAddress->i4_Length);

    MsdpFsMsdpMeshGroupEntry.MibObject.i4FsMsdpMeshGroupPeerAddressLen =
        pFsMsdpMeshGroupPeerAddress->i4_Length;
    MsdpIsSetFsMsdpMeshGroupEntry.bFsMsdpMeshGroupPeerAddress = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpMeshGroupEntry.MibObject.i4FsMsdpMeshGroupStatus =
        i4SetValFsMsdpMeshGroupStatus;
    MsdpIsSetFsMsdpMeshGroupEntry.bFsMsdpMeshGroupStatus = OSIX_TRUE;

    if (MsdpSetAllFsMsdpMeshGroupTable
        (&MsdpFsMsdpMeshGroupEntry, &MsdpIsSetFsMsdpMeshGroupEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpRPAddress
 Input       :  The Indices
                FsMsdpRPAddrType

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsMsdpRPAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpRPAddress (INT4 i4FsMsdpRPAddrType,
                       tSNMP_OCTET_STRING_TYPE * pSetValFsMsdpRPAddress)
{
    tMsdpFsMsdpRPEntry  MsdpFsMsdpRPEntry;
    tMsdpIsSetFsMsdpRPEntry MsdpIsSetFsMsdpRPEntry;

    MEMSET (&MsdpFsMsdpRPEntry, 0, sizeof (tMsdpFsMsdpRPEntry));
    MEMSET (&MsdpIsSetFsMsdpRPEntry, 0, sizeof (tMsdpIsSetFsMsdpRPEntry));

    /* Assign the index */
    MsdpFsMsdpRPEntry.MibObject.i4FsMsdpRPAddrType = i4FsMsdpRPAddrType;
    MsdpIsSetFsMsdpRPEntry.bFsMsdpRPAddrType = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (MsdpFsMsdpRPEntry.MibObject.au1FsMsdpRPAddress,
            pSetValFsMsdpRPAddress->pu1_OctetList,
            pSetValFsMsdpRPAddress->i4_Length);
    MsdpFsMsdpRPEntry.MibObject.i4FsMsdpRPAddressLen =
        pSetValFsMsdpRPAddress->i4_Length;

    MsdpIsSetFsMsdpRPEntry.bFsMsdpRPAddress = OSIX_TRUE;

    if (MsdpSetAllFsMsdpRPTable (&MsdpFsMsdpRPEntry, &MsdpIsSetFsMsdpRPEntry,
                                 OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpRPStatus
 Input       :  The Indices
                FsMsdpRPAddrType

                The Object 
             :  INT4 i4SetValFsMsdpRPStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpRPStatus (INT4 i4FsMsdpRPAddrType, INT4 i4SetValFsMsdpRPStatus)
{
    tMsdpFsMsdpRPEntry  MsdpFsMsdpRPEntry;
    tMsdpIsSetFsMsdpRPEntry MsdpIsSetFsMsdpRPEntry;

    MEMSET (&MsdpFsMsdpRPEntry, 0, sizeof (tMsdpFsMsdpRPEntry));
    MEMSET (&MsdpIsSetFsMsdpRPEntry, 0, sizeof (tMsdpIsSetFsMsdpRPEntry));

    /* Assign the index */
    MsdpFsMsdpRPEntry.MibObject.i4FsMsdpRPAddrType = i4FsMsdpRPAddrType;
    MsdpIsSetFsMsdpRPEntry.bFsMsdpRPAddrType = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpRPEntry.MibObject.i4FsMsdpRPStatus = i4SetValFsMsdpRPStatus;
    MsdpIsSetFsMsdpRPEntry.bFsMsdpRPStatus = OSIX_TRUE;

    if (MsdpSetAllFsMsdpRPTable (&MsdpFsMsdpRPEntry, &MsdpIsSetFsMsdpRPEntry,
                                 OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpPeerFilterRouteMap
 Input       :  The Indices
                FsMsdpPeerFilterAddrType

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsMsdpPeerFilterRouteMap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpPeerFilterRouteMap (INT4 i4FsMsdpPeerFilterAddrType,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsMsdpPeerFilterRouteMap)
{
    tMsdpFsMsdpPeerFilterEntry MsdpFsMsdpPeerFilterEntry;
    tMsdpIsSetFsMsdpPeerFilterEntry MsdpIsSetFsMsdpPeerFilterEntry;

    MEMSET (&MsdpFsMsdpPeerFilterEntry, 0, sizeof (tMsdpFsMsdpPeerFilterEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerFilterEntry, 0,
            sizeof (tMsdpIsSetFsMsdpPeerFilterEntry));

    /* Assign the index */
    MsdpFsMsdpPeerFilterEntry.MibObject.i4FsMsdpPeerFilterAddrType =
        i4FsMsdpPeerFilterAddrType;
    MsdpIsSetFsMsdpPeerFilterEntry.bFsMsdpPeerFilterAddrType = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (MsdpFsMsdpPeerFilterEntry.MibObject.au1FsMsdpPeerFilterRouteMap,
            pSetValFsMsdpPeerFilterRouteMap->pu1_OctetList,
            pSetValFsMsdpPeerFilterRouteMap->i4_Length);
    MsdpFsMsdpPeerFilterEntry.MibObject.i4FsMsdpPeerFilterRouteMapLen =
        pSetValFsMsdpPeerFilterRouteMap->i4_Length;

    MsdpIsSetFsMsdpPeerFilterEntry.bFsMsdpPeerFilterRouteMap = OSIX_TRUE;

    if (MsdpSetAllFsMsdpPeerFilterTable
        (&MsdpFsMsdpPeerFilterEntry, &MsdpIsSetFsMsdpPeerFilterEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpPeerFilterStatus
 Input       :  The Indices
                FsMsdpPeerFilterAddrType

                The Object 
             :  INT4 i4SetValFsMsdpPeerFilterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpPeerFilterStatus (INT4 i4FsMsdpPeerFilterAddrType,
                              INT4 i4SetValFsMsdpPeerFilterStatus)
{
    tMsdpFsMsdpPeerFilterEntry MsdpFsMsdpPeerFilterEntry;
    tMsdpIsSetFsMsdpPeerFilterEntry MsdpIsSetFsMsdpPeerFilterEntry;

    MEMSET (&MsdpFsMsdpPeerFilterEntry, 0, sizeof (tMsdpFsMsdpPeerFilterEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerFilterEntry, 0,
            sizeof (tMsdpIsSetFsMsdpPeerFilterEntry));

    /* Assign the index */
    MsdpFsMsdpPeerFilterEntry.MibObject.i4FsMsdpPeerFilterAddrType =
        i4FsMsdpPeerFilterAddrType;
    MsdpIsSetFsMsdpPeerFilterEntry.bFsMsdpPeerFilterAddrType = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpPeerFilterEntry.MibObject.i4FsMsdpPeerFilterStatus =
        i4SetValFsMsdpPeerFilterStatus;
    MsdpIsSetFsMsdpPeerFilterEntry.bFsMsdpPeerFilterStatus = OSIX_TRUE;

    if (MsdpSetAllFsMsdpPeerFilterTable
        (&MsdpFsMsdpPeerFilterEntry, &MsdpIsSetFsMsdpPeerFilterEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpSARedistributionStatus
 Input       :  The Indices
                FsMsdpSARedistributionAddrType

                The Object 
             :  INT4 i4SetValFsMsdpSARedistributionStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpSARedistributionStatus (INT4 i4FsMsdpSARedistributionAddrType,
                                    INT4 i4SetValFsMsdpSARedistributionStatus)
{
    tMsdpFsMsdpSARedistributionEntry MsdpFsMsdpSARedistributionEntry;
    tMsdpIsSetFsMsdpSARedistributionEntry MsdpIsSetFsMsdpSARedistributionEntry;

    MEMSET (&MsdpFsMsdpSARedistributionEntry, 0,
            sizeof (tMsdpFsMsdpSARedistributionEntry));
    MEMSET (&MsdpIsSetFsMsdpSARedistributionEntry, 0,
            sizeof (tMsdpIsSetFsMsdpSARedistributionEntry));

    /* Assign the index */
    MsdpFsMsdpSARedistributionEntry.MibObject.i4FsMsdpSARedistributionAddrType =
        i4FsMsdpSARedistributionAddrType;
    MsdpIsSetFsMsdpSARedistributionEntry.bFsMsdpSARedistributionAddrType =
        OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpSARedistributionEntry.MibObject.i4FsMsdpSARedistributionStatus =
        i4SetValFsMsdpSARedistributionStatus;
    MsdpIsSetFsMsdpSARedistributionEntry.bFsMsdpSARedistributionStatus =
        OSIX_TRUE;

    if (MsdpSetAllFsMsdpSARedistributionTable
        (&MsdpFsMsdpSARedistributionEntry,
         &MsdpIsSetFsMsdpSARedistributionEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpSARedistributionRouteMap
 Input       :  The Indices
                FsMsdpSARedistributionAddrType

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsMsdpSARedistributionRouteMap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpSARedistributionRouteMap (INT4 i4FsMsdpSARedistributionAddrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSetValFsMsdpSARedistributionRouteMap)
{
    tMsdpFsMsdpSARedistributionEntry MsdpFsMsdpSARedistributionEntry;
    tMsdpIsSetFsMsdpSARedistributionEntry MsdpIsSetFsMsdpSARedistributionEntry;

    MEMSET (&MsdpFsMsdpSARedistributionEntry, 0,
            sizeof (tMsdpFsMsdpSARedistributionEntry));
    MEMSET (&MsdpIsSetFsMsdpSARedistributionEntry, 0,
            sizeof (tMsdpIsSetFsMsdpSARedistributionEntry));

    /* Assign the index */
    MsdpFsMsdpSARedistributionEntry.MibObject.i4FsMsdpSARedistributionAddrType =
        i4FsMsdpSARedistributionAddrType;
    MsdpIsSetFsMsdpSARedistributionEntry.bFsMsdpSARedistributionAddrType =
        OSIX_TRUE;

    /* Assign the value */
    MEMCPY (MsdpFsMsdpSARedistributionEntry.MibObject.
            au1FsMsdpSARedistributionRouteMap,
            pSetValFsMsdpSARedistributionRouteMap->pu1_OctetList,
            pSetValFsMsdpSARedistributionRouteMap->i4_Length);
    MsdpFsMsdpSARedistributionEntry.MibObject.
        i4FsMsdpSARedistributionRouteMapLen =
        pSetValFsMsdpSARedistributionRouteMap->i4_Length;

    MsdpIsSetFsMsdpSARedistributionEntry.bFsMsdpSARedistributionRouteMap =
        OSIX_TRUE;

    if (MsdpSetAllFsMsdpSARedistributionTable
        (&MsdpFsMsdpSARedistributionEntry,
         &MsdpIsSetFsMsdpSARedistributionEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsdpSARedistributionRouteMapStat
 Input       :  The Indices
                FsMsdpSARedistributionAddrType

                The Object 
             :  INT4 i4SetValFsMsdpSARedistributionRouteMapStat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsdpSARedistributionRouteMapStat (INT4 i4FsMsdpSARedistributionAddrType,
                                          INT4
                                          i4SetValFsMsdpSARedistributionRouteMapStat)
{
    tMsdpFsMsdpSARedistributionEntry MsdpFsMsdpSARedistributionEntry;
    tMsdpIsSetFsMsdpSARedistributionEntry MsdpIsSetFsMsdpSARedistributionEntry;

    MEMSET (&MsdpFsMsdpSARedistributionEntry, 0,
            sizeof (tMsdpFsMsdpSARedistributionEntry));
    MEMSET (&MsdpIsSetFsMsdpSARedistributionEntry, 0,
            sizeof (tMsdpIsSetFsMsdpSARedistributionEntry));

    /* Assign the index */
    MsdpFsMsdpSARedistributionEntry.MibObject.i4FsMsdpSARedistributionAddrType =
        i4FsMsdpSARedistributionAddrType;
    MsdpIsSetFsMsdpSARedistributionEntry.bFsMsdpSARedistributionAddrType =
        OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpSARedistributionEntry.MibObject.
        i4FsMsdpSARedistributionRouteMapStat =
        i4SetValFsMsdpSARedistributionRouteMapStat;
    MsdpIsSetFsMsdpSARedistributionEntry.bFsMsdpSARedistributionRouteMapStat =
        OSIX_TRUE;

    if (MsdpSetAllFsMsdpSARedistributionTable
        (&MsdpFsMsdpSARedistributionEntry,
         &MsdpIsSetFsMsdpSARedistributionEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpTraceLevel
 Input       :  The Indices

                The Object 
                testValFsMsdpTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpTraceLevel (UINT4 *pu4ErrorCode, INT4 i4TestValFsMsdpTraceLevel)
{
    if (MsdpTestFsMsdpTraceLevel (pu4ErrorCode, i4TestValFsMsdpTraceLevel) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpIPv4AdminStat
 Input       :  The Indices

                The Object 
                testValFsMsdpIPv4AdminStat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpIPv4AdminStat (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsMsdpIPv4AdminStat)
{
    if (MsdpTestFsMsdpIPv4AdminStat (pu4ErrorCode, i4TestValFsMsdpIPv4AdminStat)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpIPv6AdminStat
 Input       :  The Indices

                The Object 
                testValFsMsdpIPv6AdminStat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpIPv6AdminStat (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsMsdpIPv6AdminStat)
{
    if (MsdpTestFsMsdpIPv6AdminStat (pu4ErrorCode, i4TestValFsMsdpIPv6AdminStat)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpCacheLifetime
 Input       :  The Indices

                The Object 
                testValFsMsdpCacheLifetime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpCacheLifetime (UINT4 *pu4ErrorCode,
                              UINT4 u4TestValFsMsdpCacheLifetime)
{
    if (MsdpTestFsMsdpCacheLifetime (pu4ErrorCode, u4TestValFsMsdpCacheLifetime)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpMaxPeerSessions
 Input       :  The Indices

                The Object 
                testValFsMsdpMaxPeerSessions
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpMaxPeerSessions (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsMsdpMaxPeerSessions)
{
    if (MsdpTestFsMsdpMaxPeerSessions
        (pu4ErrorCode, i4TestValFsMsdpMaxPeerSessions) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpMappingComponentId
 Input       :  The Indices

                The Object 
                testValFsMsdpMappingComponentId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpMappingComponentId (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsMsdpMappingComponentId)
{
    if (MsdpTestFsMsdpMappingComponentId
        (pu4ErrorCode, i4TestValFsMsdpMappingComponentId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpListenerPort
 Input       :  The Indices

                The Object 
                testValFsMsdpListenerPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpListenerPort (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsMsdpListenerPort)
{
    if (MsdpTestFsMsdpListenerPort (pu4ErrorCode, i4TestValFsMsdpListenerPort)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpPeerFilter
 Input       :  The Indices

                The Object 
                testValFsMsdpPeerFilter
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpPeerFilter (UINT4 *pu4ErrorCode, INT4 i4TestValFsMsdpPeerFilter)
{
    if (MsdpTestFsMsdpPeerFilter (pu4ErrorCode, i4TestValFsMsdpPeerFilter) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpPeerLocalAddress
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                testValFsMsdpPeerLocalAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpPeerLocalAddress (UINT4 *pu4ErrorCode, INT4 i4FsMsdpPeerAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMsdpPeerRemoteAddress,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValFsMsdpPeerLocalAddress)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    tMsdpIsSetFsMsdpPeerEntry MsdpIsSetFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerEntry, 0, sizeof (tMsdpIsSetFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerRemoteAddress = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerLocalAddress,
            pTestValFsMsdpPeerLocalAddress->pu1_OctetList,
            pTestValFsMsdpPeerLocalAddress->i4_Length);
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerLocalAddressLen =
        pTestValFsMsdpPeerLocalAddress->i4_Length;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerLocalAddress = OSIX_TRUE;

    if (MsdpTestAllFsMsdpPeerTable (pu4ErrorCode, &MsdpFsMsdpPeerEntry,
                                    &MsdpIsSetFsMsdpPeerEntry, OSIX_FALSE,
                                    OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpPeerConnectRetryInterval
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                testValFsMsdpPeerConnectRetryInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpPeerConnectRetryInterval (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMsdpPeerAddrType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMsdpPeerRemoteAddress,
                                         INT4
                                         i4TestValFsMsdpPeerConnectRetryInterval)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    tMsdpIsSetFsMsdpPeerEntry MsdpIsSetFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerEntry, 0, sizeof (tMsdpIsSetFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerRemoteAddress = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerConnectRetryInterval =
        i4TestValFsMsdpPeerConnectRetryInterval;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerConnectRetryInterval = OSIX_TRUE;

    if (MsdpTestAllFsMsdpPeerTable (pu4ErrorCode, &MsdpFsMsdpPeerEntry,
                                    &MsdpIsSetFsMsdpPeerEntry, OSIX_FALSE,
                                    OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpPeerHoldTimeConfigured
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                testValFsMsdpPeerHoldTimeConfigured
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpPeerHoldTimeConfigured (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMsdpPeerAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMsdpPeerRemoteAddress,
                                       INT4
                                       i4TestValFsMsdpPeerHoldTimeConfigured)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    tMsdpIsSetFsMsdpPeerEntry MsdpIsSetFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerEntry, 0, sizeof (tMsdpIsSetFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerRemoteAddress = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerHoldTimeConfigured =
        i4TestValFsMsdpPeerHoldTimeConfigured;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerHoldTimeConfigured = OSIX_TRUE;

    if (MsdpTestAllFsMsdpPeerTable (pu4ErrorCode, &MsdpFsMsdpPeerEntry,
                                    &MsdpIsSetFsMsdpPeerEntry, OSIX_FALSE,
                                    OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpPeerKeepAliveConfigured
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                testValFsMsdpPeerKeepAliveConfigured
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpPeerKeepAliveConfigured (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMsdpPeerAddrType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMsdpPeerRemoteAddress,
                                        INT4
                                        i4TestValFsMsdpPeerKeepAliveConfigured)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    tMsdpIsSetFsMsdpPeerEntry MsdpIsSetFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerEntry, 0, sizeof (tMsdpIsSetFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerRemoteAddress = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerKeepAliveConfigured =
        i4TestValFsMsdpPeerKeepAliveConfigured;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerKeepAliveConfigured = OSIX_TRUE;

    if (MsdpTestAllFsMsdpPeerTable (pu4ErrorCode, &MsdpFsMsdpPeerEntry,
                                    &MsdpIsSetFsMsdpPeerEntry, OSIX_FALSE,
                                    OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpPeerDataTtl
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                testValFsMsdpPeerDataTtl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpPeerDataTtl (UINT4 *pu4ErrorCode, INT4 i4FsMsdpPeerAddrType,
                            tSNMP_OCTET_STRING_TYPE * pFsMsdpPeerRemoteAddress,
                            INT4 i4TestValFsMsdpPeerDataTtl)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    tMsdpIsSetFsMsdpPeerEntry MsdpIsSetFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerEntry, 0, sizeof (tMsdpIsSetFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerRemoteAddress = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerDataTtl =
        i4TestValFsMsdpPeerDataTtl;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerDataTtl = OSIX_TRUE;

    if (MsdpTestAllFsMsdpPeerTable (pu4ErrorCode, &MsdpFsMsdpPeerEntry,
                                    &MsdpIsSetFsMsdpPeerEntry, OSIX_FALSE,
                                    OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpPeerStatus
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                testValFsMsdpPeerStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpPeerStatus (UINT4 *pu4ErrorCode, INT4 i4FsMsdpPeerAddrType,
                           tSNMP_OCTET_STRING_TYPE * pFsMsdpPeerRemoteAddress,
                           INT4 i4TestValFsMsdpPeerStatus)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    tMsdpIsSetFsMsdpPeerEntry MsdpIsSetFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerEntry, 0, sizeof (tMsdpIsSetFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerRemoteAddress = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerStatus =
        i4TestValFsMsdpPeerStatus;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerStatus = OSIX_TRUE;

    if (MsdpTestAllFsMsdpPeerTable (pu4ErrorCode, &MsdpFsMsdpPeerEntry,
                                    &MsdpIsSetFsMsdpPeerEntry, OSIX_FALSE,
                                    OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpPeerEncapsulationType
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                testValFsMsdpPeerEncapsulationType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpPeerEncapsulationType (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMsdpPeerAddrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMsdpPeerRemoteAddress,
                                      INT4 i4TestValFsMsdpPeerEncapsulationType)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    tMsdpIsSetFsMsdpPeerEntry MsdpIsSetFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerEntry, 0, sizeof (tMsdpIsSetFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerRemoteAddress = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerEncapsulationType =
        i4TestValFsMsdpPeerEncapsulationType;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerEncapsulationType = OSIX_TRUE;

    if (MsdpTestAllFsMsdpPeerTable (pu4ErrorCode, &MsdpFsMsdpPeerEntry,
                                    &MsdpIsSetFsMsdpPeerEntry, OSIX_FALSE,
                                    OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpPeerMD5AuthPassword
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                testValFsMsdpPeerMD5AuthPassword
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpPeerMD5AuthPassword (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMsdpPeerAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMsdpPeerRemoteAddress,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValFsMsdpPeerMD5AuthPassword)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    tMsdpIsSetFsMsdpPeerEntry MsdpIsSetFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerEntry, 0, sizeof (tMsdpIsSetFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerRemoteAddress = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerMD5AuthPassword,
            pTestValFsMsdpPeerMD5AuthPassword->pu1_OctetList,
            pTestValFsMsdpPeerMD5AuthPassword->i4_Length);
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerMD5AuthPasswordLen =
        pTestValFsMsdpPeerMD5AuthPassword->i4_Length;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerMD5AuthPassword = OSIX_TRUE;

    if (MsdpTestAllFsMsdpPeerTable (pu4ErrorCode, &MsdpFsMsdpPeerEntry,
                                    &MsdpIsSetFsMsdpPeerEntry, OSIX_FALSE,
                                    OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpPeerMD5AuthPwdStat
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                testValFsMsdpPeerMD5AuthPwdStat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpPeerMD5AuthPwdStat (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMsdpPeerAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMsdpPeerRemoteAddress,
                                   INT4 i4TestValFsMsdpPeerMD5AuthPwdStat)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    tMsdpIsSetFsMsdpPeerEntry MsdpIsSetFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerEntry, 0, sizeof (tMsdpIsSetFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerRemoteAddress = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerMD5AuthPwdStat =
        i4TestValFsMsdpPeerMD5AuthPwdStat;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerMD5AuthPwdStat = OSIX_TRUE;

    if (MsdpTestAllFsMsdpPeerTable (pu4ErrorCode, &MsdpFsMsdpPeerEntry,
                                    &MsdpIsSetFsMsdpPeerEntry, OSIX_FALSE,
                                    OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpPeerAdminStatus
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress

                The Object 
                testValFsMsdpPeerAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpPeerAdminStatus (UINT4 *pu4ErrorCode, INT4 i4FsMsdpPeerAddrType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMsdpPeerRemoteAddress,
                                INT4 i4TestValFsMsdpPeerAdminStatus)
{
    tMsdpFsMsdpPeerEntry MsdpFsMsdpPeerEntry;
    tMsdpIsSetFsMsdpPeerEntry MsdpIsSetFsMsdpPeerEntry;

    MEMSET (&MsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerEntry, 0, sizeof (tMsdpIsSetFsMsdpPeerEntry));

    /* Assign the index */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAddrType = i4FsMsdpPeerAddrType;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpPeerEntry.MibObject.au1FsMsdpPeerRemoteAddress,
            pFsMsdpPeerRemoteAddress->pu1_OctetList,
            pFsMsdpPeerRemoteAddress->i4_Length);

    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerRemoteAddressLen =
        pFsMsdpPeerRemoteAddress->i4_Length;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerRemoteAddress = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpPeerEntry.MibObject.i4FsMsdpPeerAdminStatus =
        i4TestValFsMsdpPeerAdminStatus;
    MsdpIsSetFsMsdpPeerEntry.bFsMsdpPeerAdminStatus = OSIX_TRUE;

    if (MsdpTestAllFsMsdpPeerTable (pu4ErrorCode, &MsdpFsMsdpPeerEntry,
                                    &MsdpIsSetFsMsdpPeerEntry, OSIX_FALSE,
                                    OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpSACacheStatus
 Input       :  The Indices
                FsMsdpSACacheAddrType
                FsMsdpSACacheGroupAddr
                FsMsdpSACacheSourceAddr
                FsMsdpSACacheOriginRP

                The Object 
                testValFsMsdpSACacheStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpSACacheStatus (UINT4 *pu4ErrorCode, INT4 i4FsMsdpSACacheAddrType,
                              tSNMP_OCTET_STRING_TYPE * pFsMsdpSACacheGroupAddr,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsMsdpSACacheSourceAddr,
                              tSNMP_OCTET_STRING_TYPE * pFsMsdpSACacheOriginRP,
                              INT4 i4TestValFsMsdpSACacheStatus)
{
    tMsdpFsMsdpSACacheEntry MsdpFsMsdpSACacheEntry;
    tMsdpIsSetFsMsdpSACacheEntry MsdpIsSetFsMsdpSACacheEntry;

    MEMSET (&MsdpFsMsdpSACacheEntry, 0, sizeof (tMsdpFsMsdpSACacheEntry));
    MEMSET (&MsdpIsSetFsMsdpSACacheEntry, 0,
            sizeof (tMsdpIsSetFsMsdpSACacheEntry));

    /* Assign the index */
    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheAddrType =
        i4FsMsdpSACacheAddrType;
    MsdpIsSetFsMsdpSACacheEntry.bFsMsdpSACacheAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheGroupAddr,
            pFsMsdpSACacheGroupAddr->pu1_OctetList,
            pFsMsdpSACacheGroupAddr->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheGroupAddrLen =
        pFsMsdpSACacheGroupAddr->i4_Length;
    MsdpIsSetFsMsdpSACacheEntry.bFsMsdpSACacheGroupAddr = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheSourceAddr,
            pFsMsdpSACacheSourceAddr->pu1_OctetList,
            pFsMsdpSACacheSourceAddr->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheSourceAddrLen =
        pFsMsdpSACacheSourceAddr->i4_Length;
    MsdpIsSetFsMsdpSACacheEntry.bFsMsdpSACacheSourceAddr = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpSACacheEntry.MibObject.au1FsMsdpSACacheOriginRP,
            pFsMsdpSACacheOriginRP->pu1_OctetList,
            pFsMsdpSACacheOriginRP->i4_Length);

    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheOriginRPLen =
        pFsMsdpSACacheOriginRP->i4_Length;
    MsdpIsSetFsMsdpSACacheEntry.bFsMsdpSACacheOriginRP = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpSACacheEntry.MibObject.i4FsMsdpSACacheStatus =
        i4TestValFsMsdpSACacheStatus;
    MsdpIsSetFsMsdpSACacheEntry.bFsMsdpSACacheStatus = OSIX_TRUE;

    if (MsdpTestAllFsMsdpSACacheTable (pu4ErrorCode, &MsdpFsMsdpSACacheEntry,
                                       &MsdpIsSetFsMsdpSACacheEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpMeshGroupStatus
 Input       :  The Indices
                FsMsdpMeshGroupName
                FsMsdpMeshGroupAddrType
                FsMsdpMeshGroupPeerAddress

                The Object 
                testValFsMsdpMeshGroupStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpMeshGroupStatus (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pFsMsdpMeshGroupName,
                                INT4 i4FsMsdpMeshGroupAddrType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMsdpMeshGroupPeerAddress,
                                INT4 i4TestValFsMsdpMeshGroupStatus)
{
    tMsdpFsMsdpMeshGroupEntry MsdpFsMsdpMeshGroupEntry;
    tMsdpIsSetFsMsdpMeshGroupEntry MsdpIsSetFsMsdpMeshGroupEntry;

    MEMSET (&MsdpFsMsdpMeshGroupEntry, 0, sizeof (tMsdpFsMsdpMeshGroupEntry));
    MEMSET (&MsdpIsSetFsMsdpMeshGroupEntry, 0,
            sizeof (tMsdpIsSetFsMsdpMeshGroupEntry));

    /* Assign the index */
    MEMCPY (MsdpFsMsdpMeshGroupEntry.MibObject.au1FsMsdpMeshGroupName,
            pFsMsdpMeshGroupName->pu1_OctetList,
            pFsMsdpMeshGroupName->i4_Length);

    MsdpFsMsdpMeshGroupEntry.MibObject.i4FsMsdpMeshGroupNameLen =
        pFsMsdpMeshGroupName->i4_Length;
    MsdpIsSetFsMsdpMeshGroupEntry.bFsMsdpMeshGroupName = OSIX_TRUE;

    MsdpFsMsdpMeshGroupEntry.MibObject.i4FsMsdpMeshGroupAddrType =
        i4FsMsdpMeshGroupAddrType;
    MsdpIsSetFsMsdpMeshGroupEntry.bFsMsdpMeshGroupAddrType = OSIX_TRUE;

    MEMCPY (MsdpFsMsdpMeshGroupEntry.MibObject.au1FsMsdpMeshGroupPeerAddress,
            pFsMsdpMeshGroupPeerAddress->pu1_OctetList,
            pFsMsdpMeshGroupPeerAddress->i4_Length);

    MsdpFsMsdpMeshGroupEntry.MibObject.i4FsMsdpMeshGroupPeerAddressLen =
        pFsMsdpMeshGroupPeerAddress->i4_Length;
    MsdpIsSetFsMsdpMeshGroupEntry.bFsMsdpMeshGroupPeerAddress = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpMeshGroupEntry.MibObject.i4FsMsdpMeshGroupStatus =
        i4TestValFsMsdpMeshGroupStatus;
    MsdpIsSetFsMsdpMeshGroupEntry.bFsMsdpMeshGroupStatus = OSIX_TRUE;

    if (MsdpTestAllFsMsdpMeshGroupTable
        (pu4ErrorCode, &MsdpFsMsdpMeshGroupEntry,
         &MsdpIsSetFsMsdpMeshGroupEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpRPAddress
 Input       :  The Indices
                FsMsdpRPAddrType

                The Object 
                testValFsMsdpRPAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpRPAddress (UINT4 *pu4ErrorCode, INT4 i4FsMsdpRPAddrType,
                          tSNMP_OCTET_STRING_TYPE * pTestValFsMsdpRPAddress)
{
    tMsdpFsMsdpRPEntry  MsdpFsMsdpRPEntry;
    tMsdpIsSetFsMsdpRPEntry MsdpIsSetFsMsdpRPEntry;

    MEMSET (&MsdpFsMsdpRPEntry, 0, sizeof (tMsdpFsMsdpRPEntry));
    MEMSET (&MsdpIsSetFsMsdpRPEntry, 0, sizeof (tMsdpIsSetFsMsdpRPEntry));

    /* Assign the index */
    MsdpFsMsdpRPEntry.MibObject.i4FsMsdpRPAddrType = i4FsMsdpRPAddrType;
    MsdpIsSetFsMsdpRPEntry.bFsMsdpRPAddrType = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (MsdpFsMsdpRPEntry.MibObject.au1FsMsdpRPAddress,
            pTestValFsMsdpRPAddress->pu1_OctetList,
            pTestValFsMsdpRPAddress->i4_Length);
    MsdpFsMsdpRPEntry.MibObject.i4FsMsdpRPAddressLen =
        pTestValFsMsdpRPAddress->i4_Length;
    MsdpIsSetFsMsdpRPEntry.bFsMsdpRPAddress = OSIX_TRUE;

    if (MsdpTestAllFsMsdpRPTable (pu4ErrorCode, &MsdpFsMsdpRPEntry,
                                  &MsdpIsSetFsMsdpRPEntry, OSIX_FALSE,
                                  OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpRPStatus
 Input       :  The Indices
                FsMsdpRPAddrType

                The Object 
                testValFsMsdpRPStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpRPStatus (UINT4 *pu4ErrorCode, INT4 i4FsMsdpRPAddrType,
                         INT4 i4TestValFsMsdpRPStatus)
{
    tMsdpFsMsdpRPEntry  MsdpFsMsdpRPEntry;
    tMsdpIsSetFsMsdpRPEntry MsdpIsSetFsMsdpRPEntry;

    MEMSET (&MsdpFsMsdpRPEntry, 0, sizeof (tMsdpFsMsdpRPEntry));
    MEMSET (&MsdpIsSetFsMsdpRPEntry, 0, sizeof (tMsdpIsSetFsMsdpRPEntry));

    /* Assign the index */
    MsdpFsMsdpRPEntry.MibObject.i4FsMsdpRPAddrType = i4FsMsdpRPAddrType;
    MsdpIsSetFsMsdpRPEntry.bFsMsdpRPAddrType = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpRPEntry.MibObject.i4FsMsdpRPStatus = i4TestValFsMsdpRPStatus;
    MsdpIsSetFsMsdpRPEntry.bFsMsdpRPStatus = OSIX_TRUE;

    if (MsdpTestAllFsMsdpRPTable (pu4ErrorCode, &MsdpFsMsdpRPEntry,
                                  &MsdpIsSetFsMsdpRPEntry, OSIX_FALSE,
                                  OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpPeerFilterRouteMap
 Input       :  The Indices
                FsMsdpPeerFilterAddrType

                The Object 
                testValFsMsdpPeerFilterRouteMap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpPeerFilterRouteMap (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMsdpPeerFilterAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsMsdpPeerFilterRouteMap)
{
    tMsdpFsMsdpPeerFilterEntry MsdpFsMsdpPeerFilterEntry;
    tMsdpIsSetFsMsdpPeerFilterEntry MsdpIsSetFsMsdpPeerFilterEntry;

    MEMSET (&MsdpFsMsdpPeerFilterEntry, 0, sizeof (tMsdpFsMsdpPeerFilterEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerFilterEntry, 0,
            sizeof (tMsdpIsSetFsMsdpPeerFilterEntry));

    /* Assign the index */
    MsdpFsMsdpPeerFilterEntry.MibObject.i4FsMsdpPeerFilterAddrType =
        i4FsMsdpPeerFilterAddrType;
    MsdpIsSetFsMsdpPeerFilterEntry.bFsMsdpPeerFilterAddrType = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (MsdpFsMsdpPeerFilterEntry.MibObject.au1FsMsdpPeerFilterRouteMap,
            pTestValFsMsdpPeerFilterRouteMap->pu1_OctetList,
            pTestValFsMsdpPeerFilterRouteMap->i4_Length);
    MsdpFsMsdpPeerFilterEntry.MibObject.i4FsMsdpPeerFilterRouteMapLen =
        pTestValFsMsdpPeerFilterRouteMap->i4_Length;
    MsdpIsSetFsMsdpPeerFilterEntry.bFsMsdpPeerFilterRouteMap = OSIX_TRUE;

    if (MsdpTestAllFsMsdpPeerFilterTable
        (pu4ErrorCode, &MsdpFsMsdpPeerFilterEntry,
         &MsdpIsSetFsMsdpPeerFilterEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpPeerFilterStatus
 Input       :  The Indices
                FsMsdpPeerFilterAddrType

                The Object 
                testValFsMsdpPeerFilterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpPeerFilterStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMsdpPeerFilterAddrType,
                                 INT4 i4TestValFsMsdpPeerFilterStatus)
{
    tMsdpFsMsdpPeerFilterEntry MsdpFsMsdpPeerFilterEntry;
    tMsdpIsSetFsMsdpPeerFilterEntry MsdpIsSetFsMsdpPeerFilterEntry;

    MEMSET (&MsdpFsMsdpPeerFilterEntry, 0, sizeof (tMsdpFsMsdpPeerFilterEntry));
    MEMSET (&MsdpIsSetFsMsdpPeerFilterEntry, 0,
            sizeof (tMsdpIsSetFsMsdpPeerFilterEntry));

    /* Assign the index */
    MsdpFsMsdpPeerFilterEntry.MibObject.i4FsMsdpPeerFilterAddrType =
        i4FsMsdpPeerFilterAddrType;
    MsdpIsSetFsMsdpPeerFilterEntry.bFsMsdpPeerFilterAddrType = OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpPeerFilterEntry.MibObject.i4FsMsdpPeerFilterStatus =
        i4TestValFsMsdpPeerFilterStatus;
    MsdpIsSetFsMsdpPeerFilterEntry.bFsMsdpPeerFilterStatus = OSIX_TRUE;

    if (MsdpTestAllFsMsdpPeerFilterTable
        (pu4ErrorCode, &MsdpFsMsdpPeerFilterEntry,
         &MsdpIsSetFsMsdpPeerFilterEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpSARedistributionStatus
 Input       :  The Indices
                FsMsdpSARedistributionAddrType

                The Object 
                testValFsMsdpSARedistributionStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpSARedistributionStatus (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMsdpSARedistributionAddrType,
                                       INT4
                                       i4TestValFsMsdpSARedistributionStatus)
{
    tMsdpFsMsdpSARedistributionEntry MsdpFsMsdpSARedistributionEntry;
    tMsdpIsSetFsMsdpSARedistributionEntry MsdpIsSetFsMsdpSARedistributionEntry;

    MEMSET (&MsdpFsMsdpSARedistributionEntry, 0,
            sizeof (tMsdpFsMsdpSARedistributionEntry));
    MEMSET (&MsdpIsSetFsMsdpSARedistributionEntry, 0,
            sizeof (tMsdpIsSetFsMsdpSARedistributionEntry));

    /* Assign the index */
    MsdpFsMsdpSARedistributionEntry.MibObject.i4FsMsdpSARedistributionAddrType =
        i4FsMsdpSARedistributionAddrType;
    MsdpIsSetFsMsdpSARedistributionEntry.bFsMsdpSARedistributionAddrType =
        OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpSARedistributionEntry.MibObject.i4FsMsdpSARedistributionStatus =
        i4TestValFsMsdpSARedistributionStatus;
    MsdpIsSetFsMsdpSARedistributionEntry.bFsMsdpSARedistributionStatus =
        OSIX_TRUE;

    if (MsdpTestAllFsMsdpSARedistributionTable
        (pu4ErrorCode, &MsdpFsMsdpSARedistributionEntry,
         &MsdpIsSetFsMsdpSARedistributionEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpSARedistributionRouteMap
 Input       :  The Indices
                FsMsdpSARedistributionAddrType

                The Object 
                testValFsMsdpSARedistributionRouteMap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpSARedistributionRouteMap (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMsdpSARedistributionAddrType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pTestValFsMsdpSARedistributionRouteMap)
{
    tMsdpFsMsdpSARedistributionEntry MsdpFsMsdpSARedistributionEntry;
    tMsdpIsSetFsMsdpSARedistributionEntry MsdpIsSetFsMsdpSARedistributionEntry;

    MEMSET (&MsdpFsMsdpSARedistributionEntry, 0,
            sizeof (tMsdpFsMsdpSARedistributionEntry));
    MEMSET (&MsdpIsSetFsMsdpSARedistributionEntry, 0,
            sizeof (tMsdpIsSetFsMsdpSARedistributionEntry));

    /* Assign the index */
    MsdpFsMsdpSARedistributionEntry.MibObject.i4FsMsdpSARedistributionAddrType =
        i4FsMsdpSARedistributionAddrType;
    MsdpIsSetFsMsdpSARedistributionEntry.bFsMsdpSARedistributionAddrType =
        OSIX_TRUE;

    /* Assign the value */
    MEMCPY (MsdpFsMsdpSARedistributionEntry.MibObject.
            au1FsMsdpSARedistributionRouteMap,
            pTestValFsMsdpSARedistributionRouteMap->pu1_OctetList,
            pTestValFsMsdpSARedistributionRouteMap->i4_Length);
    MsdpFsMsdpSARedistributionEntry.MibObject.
        i4FsMsdpSARedistributionRouteMapLen =
        pTestValFsMsdpSARedistributionRouteMap->i4_Length;
    MsdpIsSetFsMsdpSARedistributionEntry.bFsMsdpSARedistributionRouteMap =
        OSIX_TRUE;

    if (MsdpTestAllFsMsdpSARedistributionTable
        (pu4ErrorCode, &MsdpFsMsdpSARedistributionEntry,
         &MsdpIsSetFsMsdpSARedistributionEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsdpSARedistributionRouteMapStat
 Input       :  The Indices
                FsMsdpSARedistributionAddrType

                The Object 
                testValFsMsdpSARedistributionRouteMapStat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsdpSARedistributionRouteMapStat (UINT4 *pu4ErrorCode,
                                             INT4
                                             i4FsMsdpSARedistributionAddrType,
                                             INT4
                                             i4TestValFsMsdpSARedistributionRouteMapStat)
{
    tMsdpFsMsdpSARedistributionEntry MsdpFsMsdpSARedistributionEntry;
    tMsdpIsSetFsMsdpSARedistributionEntry MsdpIsSetFsMsdpSARedistributionEntry;

    MEMSET (&MsdpFsMsdpSARedistributionEntry, 0,
            sizeof (tMsdpFsMsdpSARedistributionEntry));
    MEMSET (&MsdpIsSetFsMsdpSARedistributionEntry, 0,
            sizeof (tMsdpIsSetFsMsdpSARedistributionEntry));

    /* Assign the index */
    MsdpFsMsdpSARedistributionEntry.MibObject.i4FsMsdpSARedistributionAddrType =
        i4FsMsdpSARedistributionAddrType;
    MsdpIsSetFsMsdpSARedistributionEntry.bFsMsdpSARedistributionAddrType =
        OSIX_TRUE;

    /* Assign the value */
    MsdpFsMsdpSARedistributionEntry.MibObject.
        i4FsMsdpSARedistributionRouteMapStat =
        i4TestValFsMsdpSARedistributionRouteMapStat;
    MsdpIsSetFsMsdpSARedistributionEntry.bFsMsdpSARedistributionRouteMapStat =
        OSIX_TRUE;

    if (MsdpTestAllFsMsdpSARedistributionTable
        (pu4ErrorCode, &MsdpFsMsdpSARedistributionEntry,
         &MsdpIsSetFsMsdpSARedistributionEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhDepv2FsMsdpTraceLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2FsMsdpTraceLevel (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMsdpIPv4AdminStat
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2FsMsdpIPv4AdminStat (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMsdpIPv6AdminStat
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2FsMsdpIPv6AdminStat (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMsdpCacheLifetime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2FsMsdpCacheLifetime (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMsdpMaxPeerSessions
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2FsMsdpMaxPeerSessions (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMsdpMappingComponentId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2FsMsdpMappingComponentId (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMsdpListenerPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2FsMsdpListenerPort (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMsdpPeerFilter
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2FsMsdpPeerFilter (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMsdpPeerTable
 Input       :  The Indices
                FsMsdpPeerAddrType
                FsMsdpPeerRemoteAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMsdpPeerTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMsdpSACacheTable
 Input       :  The Indices
                FsMsdpSACacheAddrType
                FsMsdpSACacheGroupAddr
                FsMsdpSACacheSourceAddr
                FsMsdpSACacheOriginRP
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMsdpSACacheTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMsdpMeshGroupTable
 Input       :  The Indices
                FsMsdpMeshGroupName
                FsMsdpMeshGroupAddrType
                FsMsdpMeshGroupPeerAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMsdpMeshGroupTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMsdpRPTable
 Input       :  The Indices
                FsMsdpRPAddrType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMsdpRPTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMsdpPeerFilterTable
 Input       :  The Indices
                FsMsdpPeerFilterAddrType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMsdpPeerFilterTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMsdpSARedistributionTable
 Input       :  The Indices
                FsMsdpSARedistributionAddrType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMsdpSARedistributionTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
