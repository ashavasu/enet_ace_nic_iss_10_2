/********************************************************************
* Copyright (C) 2012 Aricent Inc . All Rights Reserved
*
* $Id: msdpdbg.c,v 1.4 2012/03/30 13:54:34 siva Exp $
*
* Description: This file contains the routines for the protocol Database Access for the module Msdp 
*********************************************************************/

#include "msdpinc.h"

/****************************************************************************
 Function    :  MsdpGetAllFsMsdpPeerTable
 Input       :  pMsdpGetFsMsdpPeerEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetAllFsMsdpPeerTable (tMsdpFsMsdpPeerEntry * pMsdpGetFsMsdpPeerEntry)
{
    tMsdpFsMsdpPeerEntry *pMsdpFsMsdpPeerEntry = NULL;

    /* Check whether the node is already present */
    pMsdpFsMsdpPeerEntry =
        RBTreeGet (gMsdpGlobals.MsdpGlbMib.FsMsdpPeerTable,
                   (tRBElem *) pMsdpGetFsMsdpPeerEntry);

    if (pMsdpFsMsdpPeerEntry == NULL)
    {
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpGetAllFsMsdpPeerTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pMsdpGetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerState =
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerState;

    pMsdpGetFsMsdpPeerEntry->MibObject.u4FsMsdpPeerRPFFailures =
        pMsdpFsMsdpPeerEntry->MibObject.u4FsMsdpPeerRPFFailures;

    pMsdpGetFsMsdpPeerEntry->MibObject.u4FsMsdpPeerInSAs =
        pMsdpFsMsdpPeerEntry->MibObject.u4FsMsdpPeerInSAs;

    pMsdpGetFsMsdpPeerEntry->MibObject.u4FsMsdpPeerOutSAs =
        pMsdpFsMsdpPeerEntry->MibObject.u4FsMsdpPeerOutSAs;

    pMsdpGetFsMsdpPeerEntry->MibObject.u4FsMsdpPeerInSARequests =
        pMsdpFsMsdpPeerEntry->MibObject.u4FsMsdpPeerInSARequests;

    pMsdpGetFsMsdpPeerEntry->MibObject.u4FsMsdpPeerOutSARequests =
        pMsdpFsMsdpPeerEntry->MibObject.u4FsMsdpPeerOutSARequests;

    pMsdpGetFsMsdpPeerEntry->MibObject.u4FsMsdpPeerInControlMessages =
        pMsdpFsMsdpPeerEntry->MibObject.u4FsMsdpPeerInControlMessages;

    pMsdpGetFsMsdpPeerEntry->MibObject.u4FsMsdpPeerOutControlMessages =
        pMsdpFsMsdpPeerEntry->MibObject.u4FsMsdpPeerOutControlMessages;

    pMsdpGetFsMsdpPeerEntry->MibObject.u4FsMsdpPeerInDataPackets =
        pMsdpFsMsdpPeerEntry->MibObject.u4FsMsdpPeerInDataPackets;

    pMsdpGetFsMsdpPeerEntry->MibObject.u4FsMsdpPeerOutDataPackets =
        pMsdpFsMsdpPeerEntry->MibObject.u4FsMsdpPeerOutDataPackets;

    pMsdpGetFsMsdpPeerEntry->MibObject.u4FsMsdpPeerFsmEstablishedTransitions =
        pMsdpFsMsdpPeerEntry->MibObject.u4FsMsdpPeerFsmEstablishedTransitions;

    pMsdpGetFsMsdpPeerEntry->MibObject.u4FsMsdpPeerFsmEstablishedTime =
        pMsdpFsMsdpPeerEntry->MibObject.u4FsMsdpPeerFsmEstablishedTime;

    pMsdpGetFsMsdpPeerEntry->MibObject.u4FsMsdpPeerInMessageTime =
        pMsdpFsMsdpPeerEntry->MibObject.u4FsMsdpPeerInMessageTime;

    MEMCPY (pMsdpGetFsMsdpPeerEntry->MibObject.au1FsMsdpPeerLocalAddress,
            pMsdpFsMsdpPeerEntry->MibObject.au1FsMsdpPeerLocalAddress,
            pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerLocalAddressLen);

    pMsdpGetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerLocalAddressLen =
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerLocalAddressLen;

    pMsdpGetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerConnectRetryInterval =
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerConnectRetryInterval;

    pMsdpGetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerHoldTimeConfigured =
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerHoldTimeConfigured;

    pMsdpGetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerKeepAliveConfigured =
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerKeepAliveConfigured;

    pMsdpGetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerDataTtl =
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerDataTtl;

    pMsdpGetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus =
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus;

    pMsdpGetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerRemotePort =
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerRemotePort;

    pMsdpGetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerLocalPort =
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerLocalPort;

    pMsdpGetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerEncapsulationType =
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerEncapsulationType;

    pMsdpGetFsMsdpPeerEntry->MibObject.u4FsMsdpPeerConnectionAttempts =
        pMsdpFsMsdpPeerEntry->MibObject.u4FsMsdpPeerConnectionAttempts;

    pMsdpGetFsMsdpPeerEntry->MibObject.u4FsMsdpPeerDiscontinuityTime =
        pMsdpFsMsdpPeerEntry->MibObject.u4FsMsdpPeerDiscontinuityTime;

    MEMCPY (pMsdpGetFsMsdpPeerEntry->MibObject.au1FsMsdpPeerMD5AuthPassword,
            pMsdpFsMsdpPeerEntry->MibObject.au1FsMsdpPeerMD5AuthPassword,
            pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerMD5AuthPasswordLen);

    pMsdpGetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerMD5AuthPasswordLen =
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerMD5AuthPasswordLen;

    pMsdpGetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerMD5AuthPwdStat =
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerMD5AuthPwdStat;

    pMsdpGetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerMD5FailCount =
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerMD5FailCount;

    pMsdpGetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerMD5SuccessCount =
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerMD5SuccessCount;

    pMsdpGetFsMsdpPeerEntry->MibObject.u4FsMsdpPeerInSAResponses =
        pMsdpFsMsdpPeerEntry->MibObject.u4FsMsdpPeerInSAResponses;

    pMsdpGetFsMsdpPeerEntry->MibObject.u4FsMsdpPeerOutSAResponses =
        pMsdpFsMsdpPeerEntry->MibObject.u4FsMsdpPeerOutSAResponses;

    pMsdpGetFsMsdpPeerEntry->MibObject.u4FsMsdpPeerUpTime =
        pMsdpFsMsdpPeerEntry->MibObject.u4FsMsdpPeerUpTime;

    pMsdpGetFsMsdpPeerEntry->MibObject.u4FsMsdpPeerInKeepAliveCount =
        pMsdpFsMsdpPeerEntry->MibObject.u4FsMsdpPeerInKeepAliveCount;

    pMsdpGetFsMsdpPeerEntry->MibObject.u4FsMsdpPeerOutKeepAliveCount =
        pMsdpFsMsdpPeerEntry->MibObject.u4FsMsdpPeerOutKeepAliveCount;

    pMsdpGetFsMsdpPeerEntry->MibObject.u4FsMsdpPeerDataTtlErrorCount =
        pMsdpFsMsdpPeerEntry->MibObject.u4FsMsdpPeerDataTtlErrorCount;

    pMsdpGetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAdminStatus =
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAdminStatus;

    if (MsdpGetAllUtlFsMsdpPeerTable
        (pMsdpGetFsMsdpPeerEntry, pMsdpFsMsdpPeerEntry) == OSIX_FAILURE)

    {
        MSDP_TRC ((MSDP_UTIL_TRC, "MsdpGetAllFsMsdpPeerTable:"
                   "MsdpGetAllUtlFsMsdpPeerTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpGetAllFsMsdpSACacheTable
 Input       :  pMsdpGetFsMsdpSACacheEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetAllFsMsdpSACacheTable (tMsdpFsMsdpSACacheEntry *
                              pMsdpGetFsMsdpSACacheEntry)
{
    tMsdpFsMsdpSACacheEntry *pMsdpFsMsdpSACacheEntry = NULL;

    /* Check whether the node is already present */
    pMsdpFsMsdpSACacheEntry =
        RBTreeGet (gMsdpGlobals.MsdpGlbMib.FsMsdpSACacheTable,
                   (tRBElem *) pMsdpGetFsMsdpSACacheEntry);

    if (pMsdpFsMsdpSACacheEntry == NULL)
    {
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpGetAllFsMsdpSACacheTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pMsdpGetFsMsdpSACacheEntry->MibObject.
            au1FsMsdpSACachePeerLearnedFrom,
            pMsdpFsMsdpSACacheEntry->MibObject.au1FsMsdpSACachePeerLearnedFrom,
            pMsdpFsMsdpSACacheEntry->MibObject.
            i4FsMsdpSACachePeerLearnedFromLen);

    pMsdpGetFsMsdpSACacheEntry->MibObject.i4FsMsdpSACachePeerLearnedFromLen =
        pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACachePeerLearnedFromLen;

    MEMCPY (pMsdpGetFsMsdpSACacheEntry->MibObject.au1FsMsdpSACacheRPFPeer,
            pMsdpFsMsdpSACacheEntry->MibObject.au1FsMsdpSACacheRPFPeer,
            pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheRPFPeerLen);

    pMsdpGetFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheRPFPeerLen =
        pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheRPFPeerLen;

    pMsdpGetFsMsdpSACacheEntry->MibObject.u4FsMsdpSACacheInSAs =
        pMsdpFsMsdpSACacheEntry->MibObject.u4FsMsdpSACacheInSAs;

    pMsdpGetFsMsdpSACacheEntry->MibObject.u4FsMsdpSACacheInDataPackets =
        pMsdpFsMsdpSACacheEntry->MibObject.u4FsMsdpSACacheInDataPackets;

    pMsdpGetFsMsdpSACacheEntry->MibObject.u4FsMsdpSACacheUpTime =
        pMsdpFsMsdpSACacheEntry->MibObject.u4FsMsdpSACacheUpTime;

    pMsdpGetFsMsdpSACacheEntry->MibObject.u4FsMsdpSACacheExpiryTime =
        pMsdpFsMsdpSACacheEntry->MibObject.u4FsMsdpSACacheExpiryTime;

    pMsdpGetFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus =
        pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus;

    if (MsdpGetAllUtlFsMsdpSACacheTable
        (pMsdpGetFsMsdpSACacheEntry, pMsdpFsMsdpSACacheEntry) == OSIX_FAILURE)

    {
        MSDP_TRC ((MSDP_UTIL_TRC, "MsdpGetAllFsMsdpSACacheTable:"
                   "MsdpGetAllUtlFsMsdpSACacheTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpGetAllFsMsdpMeshGroupTable
 Input       :  pMsdpGetFsMsdpMeshGroupEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetAllFsMsdpMeshGroupTable (tMsdpFsMsdpMeshGroupEntry *
                                pMsdpGetFsMsdpMeshGroupEntry)
{
    tMsdpFsMsdpMeshGroupEntry *pMsdpFsMsdpMeshGroupEntry = NULL;

    /* Check whether the node is already present */
    pMsdpFsMsdpMeshGroupEntry =
        RBTreeGet (gMsdpGlobals.MsdpGlbMib.FsMsdpMeshGroupTable,
                   (tRBElem *) pMsdpGetFsMsdpMeshGroupEntry);

    if (pMsdpFsMsdpMeshGroupEntry == NULL)
    {
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpGetAllFsMsdpMeshGroupTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pMsdpGetFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupStatus =
        pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupStatus;

    if (MsdpGetAllUtlFsMsdpMeshGroupTable
        (pMsdpGetFsMsdpMeshGroupEntry,
         pMsdpFsMsdpMeshGroupEntry) == OSIX_FAILURE)

    {
        MSDP_TRC ((MSDP_UTIL_TRC, "MsdpGetAllFsMsdpMeshGroupTable:"
                   "MsdpGetAllUtlFsMsdpMeshGroupTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpGetAllFsMsdpRPTable
 Input       :  pMsdpGetFsMsdpRPEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetAllFsMsdpRPTable (tMsdpFsMsdpRPEntry * pMsdpGetFsMsdpRPEntry)
{
    tMsdpFsMsdpRPEntry *pMsdpFsMsdpRPEntry = NULL;

    /* Check whether the node is already present */
    pMsdpFsMsdpRPEntry =
        RBTreeGet (gMsdpGlobals.MsdpGlbMib.FsMsdpRPTable,
                   (tRBElem *) pMsdpGetFsMsdpRPEntry);

    if (pMsdpFsMsdpRPEntry == NULL)
    {
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpGetAllFsMsdpRPTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pMsdpGetFsMsdpRPEntry->MibObject.au1FsMsdpRPAddress,
            pMsdpFsMsdpRPEntry->MibObject.au1FsMsdpRPAddress,
            pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPAddressLen);

    pMsdpGetFsMsdpRPEntry->MibObject.i4FsMsdpRPAddressLen =
        pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPAddressLen;

    pMsdpGetFsMsdpRPEntry->MibObject.i4FsMsdpRPOperStatus =
        pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPOperStatus;

    pMsdpGetFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus =
        pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus;

    if (MsdpGetAllUtlFsMsdpRPTable (pMsdpGetFsMsdpRPEntry, pMsdpFsMsdpRPEntry)
        == OSIX_FAILURE)

    {
        MSDP_TRC ((MSDP_UTIL_TRC, "MsdpGetAllFsMsdpRPTable:"
                   "MsdpGetAllUtlFsMsdpRPTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpGetAllFsMsdpPeerFilterTable
 Input       :  pMsdpGetFsMsdpPeerFilterEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetAllFsMsdpPeerFilterTable (tMsdpFsMsdpPeerFilterEntry *
                                 pMsdpGetFsMsdpPeerFilterEntry)
{
    tMsdpFsMsdpPeerFilterEntry *pMsdpFsMsdpPeerFilterEntry = NULL;

    /* Check whether the node is already present */
    pMsdpFsMsdpPeerFilterEntry =
        RBTreeGet (gMsdpGlobals.MsdpGlbMib.FsMsdpPeerFilterTable,
                   (tRBElem *) pMsdpGetFsMsdpPeerFilterEntry);

    if (pMsdpFsMsdpPeerFilterEntry == NULL)
    {
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpGetAllFsMsdpPeerFilterTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pMsdpGetFsMsdpPeerFilterEntry->MibObject.
            au1FsMsdpPeerFilterRouteMap,
            pMsdpFsMsdpPeerFilterEntry->MibObject.au1FsMsdpPeerFilterRouteMap,
            pMsdpFsMsdpPeerFilterEntry->MibObject.
            i4FsMsdpPeerFilterRouteMapLen);

    pMsdpGetFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterRouteMapLen =
        pMsdpFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterRouteMapLen;

    pMsdpGetFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterStatus =
        pMsdpFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterStatus;

    if (MsdpGetAllUtlFsMsdpPeerFilterTable
        (pMsdpGetFsMsdpPeerFilterEntry,
         pMsdpFsMsdpPeerFilterEntry) == OSIX_FAILURE)

    {
        MSDP_TRC ((MSDP_UTIL_TRC, "MsdpGetAllFsMsdpPeerFilterTable:"
                   "MsdpGetAllUtlFsMsdpPeerFilterTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpGetAllFsMsdpSARedistributionTable
 Input       :  pMsdpGetFsMsdpSARedistributionEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetAllFsMsdpSARedistributionTable (tMsdpFsMsdpSARedistributionEntry *
                                       pMsdpGetFsMsdpSARedistributionEntry)
{
    tMsdpFsMsdpSARedistributionEntry *pMsdpFsMsdpSARedistributionEntry = NULL;

    /* Check whether the node is already present */
    pMsdpFsMsdpSARedistributionEntry =
        RBTreeGet (gMsdpGlobals.MsdpGlbMib.FsMsdpSARedistributionTable,
                   (tRBElem *) pMsdpGetFsMsdpSARedistributionEntry);

    if (pMsdpFsMsdpSARedistributionEntry == NULL)
    {
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpGetAllFsMsdpSARedistributionTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pMsdpGetFsMsdpSARedistributionEntry->MibObject.
        i4FsMsdpSARedistributionStatus =
        pMsdpFsMsdpSARedistributionEntry->MibObject.
        i4FsMsdpSARedistributionStatus;

    MEMCPY (pMsdpGetFsMsdpSARedistributionEntry->MibObject.
            au1FsMsdpSARedistributionRouteMap,
            pMsdpFsMsdpSARedistributionEntry->MibObject.
            au1FsMsdpSARedistributionRouteMap,
            pMsdpFsMsdpSARedistributionEntry->MibObject.
            i4FsMsdpSARedistributionRouteMapLen);

    pMsdpGetFsMsdpSARedistributionEntry->MibObject.
        i4FsMsdpSARedistributionRouteMapLen =
        pMsdpFsMsdpSARedistributionEntry->MibObject.
        i4FsMsdpSARedistributionRouteMapLen;

    pMsdpGetFsMsdpSARedistributionEntry->MibObject.
        i4FsMsdpSARedistributionRouteMapStat =
        pMsdpFsMsdpSARedistributionEntry->MibObject.
        i4FsMsdpSARedistributionRouteMapStat;

    if (MsdpGetAllUtlFsMsdpSARedistributionTable
        (pMsdpGetFsMsdpSARedistributionEntry,
         pMsdpFsMsdpSARedistributionEntry) == OSIX_FAILURE)

    {
        MSDP_TRC ((MSDP_UTIL_TRC, "MsdpGetAllFsMsdpSARedistributionTable:"
                   "MsdpGetAllUtlFsMsdpSARedistributionTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpSetAllFsMsdpPeerTable
 Input       :  pMsdpSetFsMsdpPeerEntry
                pMsdpIsSetFsMsdpPeerEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpSetAllFsMsdpPeerTable (tMsdpFsMsdpPeerEntry * pMsdpSetFsMsdpPeerEntry,
                           tMsdpIsSetFsMsdpPeerEntry *
                           pMsdpIsSetFsMsdpPeerEntry, INT4 i4RowStatusLogic,
                           INT4 i4RowCreateOption)
{
    tMsdpFsMsdpPeerEntry *pMsdpFsMsdpPeerEntry = NULL;
    tMsdpFsMsdpPeerEntry *pMsdpOldFsMsdpPeerEntry = NULL;
    tMsdpFsMsdpPeerEntry *pMsdpTrgFsMsdpPeerEntry = NULL;
    tMsdpIsSetFsMsdpPeerEntry *pMsdpTrgIsSetFsMsdpPeerEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pMsdpOldFsMsdpPeerEntry =
        (tMsdpFsMsdpPeerEntry *) MemAllocMemBlk (MSDP_FSMSDPPEERTABLE_POOLID);
    if (pMsdpOldFsMsdpPeerEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pMsdpTrgFsMsdpPeerEntry =
        (tMsdpFsMsdpPeerEntry *) MemAllocMemBlk (MSDP_FSMSDPPEERTABLE_POOLID);
    if (pMsdpTrgFsMsdpPeerEntry == NULL)
    {
        MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpPeerEntry);
        return OSIX_FAILURE;
    }
    pMsdpTrgIsSetFsMsdpPeerEntry =
        (tMsdpIsSetFsMsdpPeerEntry *)
        MemAllocMemBlk (MSDP_FSMSDPPEERTABLE_ISSET_POOLID);
    if (pMsdpTrgIsSetFsMsdpPeerEntry == NULL)
    {
        MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpPeerEntry);
        MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpPeerEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pMsdpOldFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (pMsdpTrgFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
    MEMSET (pMsdpTrgIsSetFsMsdpPeerEntry, 0,
            sizeof (tMsdpIsSetFsMsdpPeerEntry));

    /* Check whether the node is already present */
    pMsdpFsMsdpPeerEntry =
        RBTreeGet (gMsdpGlobals.MsdpGlbMib.FsMsdpPeerTable,
                   (tRBElem *) pMsdpSetFsMsdpPeerEntry);

    if (pMsdpFsMsdpPeerEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus ==
             CREATE_AND_WAIT)
            || (pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus ==
                CREATE_AND_GO)
            ||
            ((pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pMsdpFsMsdpPeerEntry =
                (tMsdpFsMsdpPeerEntry *)
                MemAllocMemBlk (MSDP_FSMSDPPEERTABLE_POOLID);
            if (pMsdpFsMsdpPeerEntry == NULL)
            {
                if (MsdpSetAllFsMsdpPeerTableTrigger (pMsdpSetFsMsdpPeerEntry,
                                                      pMsdpIsSetFsMsdpPeerEntry,
                                                      SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpPeerTable:MsdpSetAllFsMsdpPeerTableTrigger function fails\r\n"));

                }
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpPeerTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                    (UINT1 *) pMsdpOldFsMsdpPeerEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                    (UINT1 *) pMsdpTrgFsMsdpPeerEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_ISSET_POOLID,
                                    (UINT1 *) pMsdpTrgIsSetFsMsdpPeerEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pMsdpFsMsdpPeerEntry, 0, sizeof (tMsdpFsMsdpPeerEntry));
            if ((MsdpInitializeFsMsdpPeerTable (pMsdpFsMsdpPeerEntry)) ==
                OSIX_FAILURE)
            {
                if (MsdpSetAllFsMsdpPeerTableTrigger (pMsdpSetFsMsdpPeerEntry,
                                                      pMsdpIsSetFsMsdpPeerEntry,
                                                      SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpPeerTable:MsdpSetAllFsMsdpPeerTableTrigger function fails\r\n"));

                }
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpPeerTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                    (UINT1 *) pMsdpFsMsdpPeerEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                    (UINT1 *) pMsdpOldFsMsdpPeerEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                    (UINT1 *) pMsdpTrgFsMsdpPeerEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_ISSET_POOLID,
                                    (UINT1 *) pMsdpTrgIsSetFsMsdpPeerEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerAddrType != OSIX_FALSE)
            {
                pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType =
                    pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType;
            }

            if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerRemoteAddress !=
                OSIX_FALSE)
            {
                MEMCPY (pMsdpFsMsdpPeerEntry->MibObject.
                        au1FsMsdpPeerRemoteAddress,
                        pMsdpSetFsMsdpPeerEntry->MibObject.
                        au1FsMsdpPeerRemoteAddress,
                        pMsdpSetFsMsdpPeerEntry->MibObject.
                        i4FsMsdpPeerRemoteAddressLen);

                pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerRemoteAddressLen =
                    pMsdpSetFsMsdpPeerEntry->MibObject.
                    i4FsMsdpPeerRemoteAddressLen;
            }

            if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerLocalAddress !=
                OSIX_FALSE)
            {
                MEMCPY (pMsdpFsMsdpPeerEntry->MibObject.
                        au1FsMsdpPeerLocalAddress,
                        pMsdpSetFsMsdpPeerEntry->MibObject.
                        au1FsMsdpPeerLocalAddress,
                        pMsdpSetFsMsdpPeerEntry->MibObject.
                        i4FsMsdpPeerLocalAddressLen);

                pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerLocalAddressLen =
                    pMsdpSetFsMsdpPeerEntry->MibObject.
                    i4FsMsdpPeerLocalAddressLen;
            }

            if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerConnectRetryInterval !=
                OSIX_FALSE)
            {
                pMsdpFsMsdpPeerEntry->MibObject.
                    i4FsMsdpPeerConnectRetryInterval =
                    pMsdpSetFsMsdpPeerEntry->MibObject.
                    i4FsMsdpPeerConnectRetryInterval;
            }

            if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerHoldTimeConfigured !=
                OSIX_FALSE)
            {
                pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerHoldTimeConfigured =
                    pMsdpSetFsMsdpPeerEntry->MibObject.
                    i4FsMsdpPeerHoldTimeConfigured;
            }

            if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerKeepAliveConfigured !=
                OSIX_FALSE)
            {
                pMsdpFsMsdpPeerEntry->MibObject.
                    i4FsMsdpPeerKeepAliveConfigured =
                    pMsdpSetFsMsdpPeerEntry->MibObject.
                    i4FsMsdpPeerKeepAliveConfigured;
            }

            if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerDataTtl != OSIX_FALSE)
            {
                pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerDataTtl =
                    pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerDataTtl;
            }

            if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerStatus != OSIX_FALSE)
            {
                pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus =
                    pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus;
            }

            if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerEncapsulationType !=
                OSIX_FALSE)
            {
                pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerEncapsulationType =
                    pMsdpSetFsMsdpPeerEntry->MibObject.
                    i4FsMsdpPeerEncapsulationType;
            }

            if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerMD5AuthPassword !=
                OSIX_FALSE)
            {
                MEMCPY (pMsdpFsMsdpPeerEntry->MibObject.
                        au1FsMsdpPeerMD5AuthPassword,
                        pMsdpSetFsMsdpPeerEntry->MibObject.
                        au1FsMsdpPeerMD5AuthPassword,
                        pMsdpSetFsMsdpPeerEntry->MibObject.
                        i4FsMsdpPeerMD5AuthPasswordLen);

                pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerMD5AuthPasswordLen =
                    pMsdpSetFsMsdpPeerEntry->MibObject.
                    i4FsMsdpPeerMD5AuthPasswordLen;
            }

            if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerMD5AuthPwdStat !=
                OSIX_FALSE)
            {
                pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerMD5AuthPwdStat =
                    pMsdpSetFsMsdpPeerEntry->MibObject.
                    i4FsMsdpPeerMD5AuthPwdStat;
            }

            if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerAdminStatus != OSIX_FALSE)
            {
                pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAdminStatus =
                    pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAdminStatus;
            }

            if ((pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == 1)
                                    && (pMsdpSetFsMsdpPeerEntry->MibObject.
                                        i4FsMsdpPeerStatus == ACTIVE)))
            {
                pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus = ACTIVE;
            }
            else if (pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus ==
                     CREATE_AND_WAIT)
            {
                pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gMsdpGlobals.MsdpGlbMib.FsMsdpPeerTable,
                 (tRBElem *) pMsdpFsMsdpPeerEntry) != RB_SUCCESS)
            {
                if (MsdpSetAllFsMsdpPeerTableTrigger (pMsdpSetFsMsdpPeerEntry,
                                                      pMsdpIsSetFsMsdpPeerEntry,
                                                      SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpPeerTable: MsdpSetAllFsMsdpPeerTableTrigger function returns failure.\r\n"));
                }
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpPeerTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                    (UINT1 *) pMsdpFsMsdpPeerEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                    (UINT1 *) pMsdpOldFsMsdpPeerEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                    (UINT1 *) pMsdpTrgFsMsdpPeerEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_ISSET_POOLID,
                                    (UINT1 *) pMsdpTrgIsSetFsMsdpPeerEntry);
                return OSIX_FAILURE;
            }
            if (MsdpUtilUpdateFsMsdpPeerTable
                (NULL, pMsdpFsMsdpPeerEntry,
                 pMsdpIsSetFsMsdpPeerEntry) != OSIX_SUCCESS)
            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpPeerTable: MsdpUtilUpdateFsMsdpPeerTable function returns failure.\r\n"));

                if (MsdpSetAllFsMsdpPeerTableTrigger (pMsdpSetFsMsdpPeerEntry,
                                                      pMsdpIsSetFsMsdpPeerEntry,
                                                      SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpPeerTable: MsdpSetAllFsMsdpPeerTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gMsdpGlobals.MsdpGlbMib.FsMsdpPeerTable,
                           pMsdpFsMsdpPeerEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                    (UINT1 *) pMsdpFsMsdpPeerEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                    (UINT1 *) pMsdpOldFsMsdpPeerEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                    (UINT1 *) pMsdpTrgFsMsdpPeerEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_ISSET_POOLID,
                                    (UINT1 *) pMsdpTrgIsSetFsMsdpPeerEntry);
                return OSIX_FAILURE;
            }

            if ((pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == 1)
                                    && (pMsdpSetFsMsdpPeerEntry->MibObject.
                                        i4FsMsdpPeerStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pMsdpTrgFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType =
                    pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType;
                MEMCPY (pMsdpTrgFsMsdpPeerEntry->MibObject.
                        au1FsMsdpPeerRemoteAddress,
                        pMsdpSetFsMsdpPeerEntry->MibObject.
                        au1FsMsdpPeerRemoteAddress,
                        pMsdpSetFsMsdpPeerEntry->MibObject.
                        i4FsMsdpPeerRemoteAddressLen);

                pMsdpTrgFsMsdpPeerEntry->MibObject.
                    i4FsMsdpPeerRemoteAddressLen =
                    pMsdpSetFsMsdpPeerEntry->MibObject.
                    i4FsMsdpPeerRemoteAddressLen;
                pMsdpTrgFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus =
                    CREATE_AND_WAIT;
                pMsdpTrgIsSetFsMsdpPeerEntry->bFsMsdpPeerStatus = OSIX_TRUE;

                if (MsdpSetAllFsMsdpPeerTableTrigger (pMsdpTrgFsMsdpPeerEntry,
                                                      pMsdpTrgIsSetFsMsdpPeerEntry,
                                                      SNMP_FAILURE) !=
                    OSIX_SUCCESS)
                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpPeerTable: MsdpSetAllFsMsdpPeerTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                        (UINT1 *) pMsdpFsMsdpPeerEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                        (UINT1 *) pMsdpOldFsMsdpPeerEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                        (UINT1 *) pMsdpTrgFsMsdpPeerEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_ISSET_POOLID,
                                        (UINT1 *) pMsdpTrgIsSetFsMsdpPeerEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus ==
                     CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pMsdpTrgFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus =
                    CREATE_AND_WAIT;
                pMsdpTrgIsSetFsMsdpPeerEntry->bFsMsdpPeerStatus = OSIX_TRUE;

                if (MsdpSetAllFsMsdpPeerTableTrigger (pMsdpTrgFsMsdpPeerEntry,
                                                      pMsdpTrgIsSetFsMsdpPeerEntry,
                                                      SNMP_FAILURE) !=
                    OSIX_SUCCESS)
                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpPeerTable: MsdpSetAllFsMsdpPeerTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                        (UINT1 *) pMsdpFsMsdpPeerEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                        (UINT1 *) pMsdpOldFsMsdpPeerEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                        (UINT1 *) pMsdpTrgFsMsdpPeerEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_ISSET_POOLID,
                                        (UINT1 *) pMsdpTrgIsSetFsMsdpPeerEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus ==
                CREATE_AND_GO)
            {
                pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus = ACTIVE;
            }

            if (MsdpSetAllFsMsdpPeerTableTrigger (pMsdpSetFsMsdpPeerEntry,
                                                  pMsdpIsSetFsMsdpPeerEntry,
                                                  SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpPeerTable:  MsdpSetAllFsMsdpPeerTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpPeerEntry);
            MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpPeerEntry);
            MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_ISSET_POOLID,
                                (UINT1 *) pMsdpTrgIsSetFsMsdpPeerEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (MsdpSetAllFsMsdpPeerTableTrigger (pMsdpSetFsMsdpPeerEntry,
                                                  pMsdpIsSetFsMsdpPeerEntry,
                                                  SNMP_FAILURE) != OSIX_SUCCESS)

            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpPeerTable: MsdpSetAllFsMsdpPeerTableTrigger function returns failure.\r\n"));
            }
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpPeerTable: Failure.\r\n"));
            MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpPeerEntry);
            MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpPeerEntry);
            MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_ISSET_POOLID,
                                (UINT1 *) pMsdpTrgIsSetFsMsdpPeerEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus ==
              CREATE_AND_WAIT)
             || (pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus ==
                 CREATE_AND_GO))
    {
        if (MsdpSetAllFsMsdpPeerTableTrigger (pMsdpSetFsMsdpPeerEntry,
                                              pMsdpIsSetFsMsdpPeerEntry,
                                              SNMP_FAILURE) != OSIX_SUCCESS)

        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpPeerTable: MsdpSetAllFsMsdpPeerTableTrigger function returns failure.\r\n"));
        }
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpSetAllFsMsdpPeerTable: The row is already present.\r\n"));
        MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpPeerEntry);
        MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpPeerEntry);
        MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpPeerEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pMsdpOldFsMsdpPeerEntry, pMsdpFsMsdpPeerEntry,
            sizeof (tMsdpFsMsdpPeerEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus == DESTROY)
    {
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus = DESTROY;

        if (MsdpUtilUpdateFsMsdpPeerTable (pMsdpOldFsMsdpPeerEntry,
                                           pMsdpFsMsdpPeerEntry,
                                           pMsdpIsSetFsMsdpPeerEntry) !=
            OSIX_SUCCESS)
        {

            if (MsdpSetAllFsMsdpPeerTableTrigger (pMsdpSetFsMsdpPeerEntry,
                                                  pMsdpIsSetFsMsdpPeerEntry,
                                                  SNMP_FAILURE) != OSIX_SUCCESS)

            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpPeerTable: MsdpSetAllFsMsdpPeerTableTrigger function returns failure.\r\n"));
            }
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpPeerTable: MsdpUtilUpdateFsMsdpPeerTable function returns failure.\r\n"));
        }
        RBTreeRem (gMsdpGlobals.MsdpGlbMib.FsMsdpPeerTable,
                   pMsdpFsMsdpPeerEntry);
        if (MsdpSetAllFsMsdpPeerTableTrigger
            (pMsdpSetFsMsdpPeerEntry, pMsdpIsSetFsMsdpPeerEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpPeerTable: MsdpSetAllFsMsdpPeerTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpPeerEntry);
            MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpPeerEntry);
            MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_ISSET_POOLID,
                                (UINT1 *) pMsdpTrgIsSetFsMsdpPeerEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                            (UINT1 *) pMsdpFsMsdpPeerEntry);
        MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpPeerEntry);
        MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpPeerEntry);
        MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpPeerEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsMsdpPeerTableFilterInputs
        (pMsdpFsMsdpPeerEntry, pMsdpSetFsMsdpPeerEntry,
         pMsdpIsSetFsMsdpPeerEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpPeerEntry);
        MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpPeerEntry);
        MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpPeerEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus == ACTIVE) &&
        (pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus !=
         NOT_IN_SERVICE))
    {
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pMsdpTrgFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus = NOT_IN_SERVICE;
        pMsdpTrgIsSetFsMsdpPeerEntry->bFsMsdpPeerStatus = OSIX_TRUE;

        if (MsdpUtilUpdateFsMsdpPeerTable (pMsdpOldFsMsdpPeerEntry,
                                           pMsdpFsMsdpPeerEntry,
                                           pMsdpIsSetFsMsdpPeerEntry) !=
            OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pMsdpFsMsdpPeerEntry, pMsdpOldFsMsdpPeerEntry,
                    sizeof (tMsdpFsMsdpPeerEntry));
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpPeerTable:                 MsdpUtilUpdateFsMsdpPeerTable Function returns failure.\r\n"));

            if (MsdpSetAllFsMsdpPeerTableTrigger (pMsdpSetFsMsdpPeerEntry,
                                                  pMsdpIsSetFsMsdpPeerEntry,
                                                  SNMP_FAILURE) != OSIX_SUCCESS)

            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpPeerTable: MsdpSetAllFsMsdpPeerTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpPeerEntry);
            MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpPeerEntry);
            MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_ISSET_POOLID,
                                (UINT1 *) pMsdpTrgIsSetFsMsdpPeerEntry);
            return OSIX_FAILURE;
        }

        if (MsdpSetAllFsMsdpPeerTableTrigger (pMsdpTrgFsMsdpPeerEntry,
                                              pMsdpTrgIsSetFsMsdpPeerEntry,
                                              SNMP_FAILURE) != OSIX_SUCCESS)
        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpPeerTable: MsdpSetAllFsMsdpPeerTableTrigger function returns failure.\r\n"));
        }
    }

    if (pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerLocalAddress != OSIX_FALSE)
    {
        MEMCPY (pMsdpFsMsdpPeerEntry->MibObject.au1FsMsdpPeerLocalAddress,
                pMsdpSetFsMsdpPeerEntry->MibObject.au1FsMsdpPeerLocalAddress,
                pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerLocalAddressLen);

        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerLocalAddressLen =
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerLocalAddressLen;
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerConnectRetryInterval !=
        OSIX_FALSE)
    {
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerConnectRetryInterval =
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerConnectRetryInterval;
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerHoldTimeConfigured != OSIX_FALSE)
    {
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerHoldTimeConfigured =
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerHoldTimeConfigured;
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerKeepAliveConfigured != OSIX_FALSE)
    {
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerKeepAliveConfigured =
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerKeepAliveConfigured;
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerDataTtl != OSIX_FALSE)
    {
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerDataTtl =
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerDataTtl;
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerStatus != OSIX_FALSE)
    {
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus =
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus;
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerEncapsulationType != OSIX_FALSE)
    {
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerEncapsulationType =
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerEncapsulationType;
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerMD5AuthPassword != OSIX_FALSE)
    {
        MEMCPY (pMsdpFsMsdpPeerEntry->MibObject.au1FsMsdpPeerMD5AuthPassword,
                pMsdpSetFsMsdpPeerEntry->MibObject.au1FsMsdpPeerMD5AuthPassword,
                pMsdpSetFsMsdpPeerEntry->MibObject.
                i4FsMsdpPeerMD5AuthPasswordLen);

        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerMD5AuthPasswordLen =
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerMD5AuthPasswordLen;
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerMD5AuthPwdStat != OSIX_FALSE)
    {
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerMD5AuthPwdStat =
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerMD5AuthPwdStat;
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerAdminStatus != OSIX_FALSE)
    {
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAdminStatus =
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAdminStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus = ACTIVE;
    }

    if (MsdpUtilUpdateFsMsdpPeerTable (pMsdpOldFsMsdpPeerEntry,
                                       pMsdpFsMsdpPeerEntry,
                                       pMsdpIsSetFsMsdpPeerEntry) !=
        OSIX_SUCCESS)
    {

        if (MsdpSetAllFsMsdpPeerTableTrigger (pMsdpSetFsMsdpPeerEntry,
                                              pMsdpIsSetFsMsdpPeerEntry,
                                              SNMP_FAILURE) != OSIX_SUCCESS)

        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpPeerTable: MsdpSetAllFsMsdpPeerTableTrigger function returns failure.\r\n"));

        }
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpSetAllFsMsdpPeerTable: MsdpUtilUpdateFsMsdpPeerTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pMsdpFsMsdpPeerEntry, pMsdpOldFsMsdpPeerEntry,
                sizeof (tMsdpFsMsdpPeerEntry));
        MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpPeerEntry);
        MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpPeerEntry);
        MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpPeerEntry);
        return OSIX_FAILURE;

    }
    if (MsdpSetAllFsMsdpPeerTableTrigger (pMsdpSetFsMsdpPeerEntry,
                                          pMsdpIsSetFsMsdpPeerEntry,
                                          SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpSetAllFsMsdpPeerTable: MsdpSetAllFsMsdpPeerTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                        (UINT1 *) pMsdpOldFsMsdpPeerEntry);
    MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                        (UINT1 *) pMsdpTrgFsMsdpPeerEntry);
    MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_ISSET_POOLID,
                        (UINT1 *) pMsdpTrgIsSetFsMsdpPeerEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  MsdpSetAllFsMsdpSACacheTable
 Input       :  pMsdpSetFsMsdpSACacheEntry
                pMsdpIsSetFsMsdpSACacheEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpSetAllFsMsdpSACacheTable (tMsdpFsMsdpSACacheEntry *
                              pMsdpSetFsMsdpSACacheEntry,
                              tMsdpIsSetFsMsdpSACacheEntry *
                              pMsdpIsSetFsMsdpSACacheEntry,
                              INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tMsdpFsMsdpSACacheEntry *pMsdpFsMsdpSACacheEntry = NULL;
    tMsdpFsMsdpSACacheEntry *pMsdpOldFsMsdpSACacheEntry = NULL;
    tMsdpFsMsdpSACacheEntry *pMsdpTrgFsMsdpSACacheEntry = NULL;
    tMsdpIsSetFsMsdpSACacheEntry *pMsdpTrgIsSetFsMsdpSACacheEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pMsdpOldFsMsdpSACacheEntry =
        (tMsdpFsMsdpSACacheEntry *)
        MemAllocMemBlk (MSDP_FSMSDPSACACHETABLE_POOLID);
    if (pMsdpOldFsMsdpSACacheEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pMsdpTrgFsMsdpSACacheEntry =
        (tMsdpFsMsdpSACacheEntry *)
        MemAllocMemBlk (MSDP_FSMSDPSACACHETABLE_POOLID);
    if (pMsdpTrgFsMsdpSACacheEntry == NULL)
    {
        MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpSACacheEntry);
        return OSIX_FAILURE;
    }
    pMsdpTrgIsSetFsMsdpSACacheEntry =
        (tMsdpIsSetFsMsdpSACacheEntry *)
        MemAllocMemBlk (MSDP_FSMSDPSACACHETABLE_ISSET_POOLID);
    if (pMsdpTrgIsSetFsMsdpSACacheEntry == NULL)
    {
        MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpSACacheEntry);
        MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpSACacheEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pMsdpOldFsMsdpSACacheEntry, 0, sizeof (tMsdpFsMsdpSACacheEntry));
    MEMSET (pMsdpTrgFsMsdpSACacheEntry, 0, sizeof (tMsdpFsMsdpSACacheEntry));
    MEMSET (pMsdpTrgIsSetFsMsdpSACacheEntry, 0,
            sizeof (tMsdpIsSetFsMsdpSACacheEntry));

    /* Check whether the node is already present */
    pMsdpFsMsdpSACacheEntry =
        RBTreeGet (gMsdpGlobals.MsdpGlbMib.FsMsdpSACacheTable,
                   (tRBElem *) pMsdpSetFsMsdpSACacheEntry);

    if (pMsdpFsMsdpSACacheEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pMsdpSetFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus ==
             CREATE_AND_WAIT)
            || (pMsdpSetFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus ==
                CREATE_AND_GO)
            ||
            ((pMsdpSetFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus ==
              ACTIVE) && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pMsdpFsMsdpSACacheEntry =
                (tMsdpFsMsdpSACacheEntry *)
                MemAllocMemBlk (MSDP_FSMSDPSACACHETABLE_POOLID);
            if (pMsdpFsMsdpSACacheEntry == NULL)
            {
                if (MsdpSetAllFsMsdpSACacheTableTrigger
                    (pMsdpSetFsMsdpSACacheEntry, pMsdpIsSetFsMsdpSACacheEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpSACacheTable:MsdpSetAllFsMsdpSACacheTableTrigger function fails\r\n"));

                }
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpSACacheTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                    (UINT1 *) pMsdpOldFsMsdpSACacheEntry);
                MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                    (UINT1 *) pMsdpTrgFsMsdpSACacheEntry);
                MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_ISSET_POOLID,
                                    (UINT1 *) pMsdpTrgIsSetFsMsdpSACacheEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pMsdpFsMsdpSACacheEntry, 0,
                    sizeof (tMsdpFsMsdpSACacheEntry));
            if ((MsdpInitializeFsMsdpSACacheTable (pMsdpFsMsdpSACacheEntry)) ==
                OSIX_FAILURE)
            {
                if (MsdpSetAllFsMsdpSACacheTableTrigger
                    (pMsdpSetFsMsdpSACacheEntry, pMsdpIsSetFsMsdpSACacheEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpSACacheTable:MsdpSetAllFsMsdpSACacheTableTrigger function fails\r\n"));

                }
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpSACacheTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                    (UINT1 *) pMsdpFsMsdpSACacheEntry);
                MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                    (UINT1 *) pMsdpOldFsMsdpSACacheEntry);
                MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                    (UINT1 *) pMsdpTrgFsMsdpSACacheEntry);
                MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_ISSET_POOLID,
                                    (UINT1 *) pMsdpTrgIsSetFsMsdpSACacheEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheAddrType !=
                OSIX_FALSE)
            {
                pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheAddrType =
                    pMsdpSetFsMsdpSACacheEntry->MibObject.
                    i4FsMsdpSACacheAddrType;
            }

            if (pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheGroupAddr !=
                OSIX_FALSE)
            {
                MEMCPY (pMsdpFsMsdpSACacheEntry->MibObject.
                        au1FsMsdpSACacheGroupAddr,
                        pMsdpSetFsMsdpSACacheEntry->MibObject.
                        au1FsMsdpSACacheGroupAddr,
                        pMsdpSetFsMsdpSACacheEntry->MibObject.
                        i4FsMsdpSACacheGroupAddrLen);

                pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheGroupAddrLen =
                    pMsdpSetFsMsdpSACacheEntry->MibObject.
                    i4FsMsdpSACacheGroupAddrLen;
            }

            if (pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheSourceAddr !=
                OSIX_FALSE)
            {
                MEMCPY (pMsdpFsMsdpSACacheEntry->MibObject.
                        au1FsMsdpSACacheSourceAddr,
                        pMsdpSetFsMsdpSACacheEntry->MibObject.
                        au1FsMsdpSACacheSourceAddr,
                        pMsdpSetFsMsdpSACacheEntry->MibObject.
                        i4FsMsdpSACacheSourceAddrLen);

                pMsdpFsMsdpSACacheEntry->MibObject.
                    i4FsMsdpSACacheSourceAddrLen =
                    pMsdpSetFsMsdpSACacheEntry->MibObject.
                    i4FsMsdpSACacheSourceAddrLen;
            }

            if (pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheOriginRP !=
                OSIX_FALSE)
            {
                MEMCPY (pMsdpFsMsdpSACacheEntry->MibObject.
                        au1FsMsdpSACacheOriginRP,
                        pMsdpSetFsMsdpSACacheEntry->MibObject.
                        au1FsMsdpSACacheOriginRP,
                        pMsdpSetFsMsdpSACacheEntry->MibObject.
                        i4FsMsdpSACacheOriginRPLen);

                pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheOriginRPLen =
                    pMsdpSetFsMsdpSACacheEntry->MibObject.
                    i4FsMsdpSACacheOriginRPLen;
            }

            if (pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheStatus !=
                OSIX_FALSE)
            {
                pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus =
                    pMsdpSetFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus;
            }

            if ((pMsdpSetFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == 1)
                                    && (pMsdpSetFsMsdpSACacheEntry->MibObject.
                                        i4FsMsdpSACacheStatus == ACTIVE)))
            {
                pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus =
                    ACTIVE;
            }
            else if (pMsdpSetFsMsdpSACacheEntry->MibObject.
                     i4FsMsdpSACacheStatus == CREATE_AND_WAIT)
            {
                pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus =
                    NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gMsdpGlobals.MsdpGlbMib.FsMsdpSACacheTable,
                 (tRBElem *) pMsdpFsMsdpSACacheEntry) != RB_SUCCESS)
            {
                if (MsdpSetAllFsMsdpSACacheTableTrigger
                    (pMsdpSetFsMsdpSACacheEntry, pMsdpIsSetFsMsdpSACacheEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpSACacheTable: MsdpSetAllFsMsdpSACacheTableTrigger function returns failure.\r\n"));
                }
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpSACacheTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                    (UINT1 *) pMsdpFsMsdpSACacheEntry);
                MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                    (UINT1 *) pMsdpOldFsMsdpSACacheEntry);
                MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                    (UINT1 *) pMsdpTrgFsMsdpSACacheEntry);
                MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_ISSET_POOLID,
                                    (UINT1 *) pMsdpTrgIsSetFsMsdpSACacheEntry);
                return OSIX_FAILURE;
            }
            if (MsdpUtilUpdateFsMsdpSACacheTable
                (NULL, pMsdpFsMsdpSACacheEntry,
                 pMsdpIsSetFsMsdpSACacheEntry) != OSIX_SUCCESS)
            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpSACacheTable: MsdpUtilUpdateFsMsdpSACacheTable function returns failure.\r\n"));

                if (MsdpSetAllFsMsdpSACacheTableTrigger
                    (pMsdpSetFsMsdpSACacheEntry, pMsdpIsSetFsMsdpSACacheEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpSACacheTable: MsdpSetAllFsMsdpSACacheTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gMsdpGlobals.MsdpGlbMib.FsMsdpSACacheTable,
                           pMsdpFsMsdpSACacheEntry);
                MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                    (UINT1 *) pMsdpFsMsdpSACacheEntry);
                MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                    (UINT1 *) pMsdpOldFsMsdpSACacheEntry);
                MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                    (UINT1 *) pMsdpTrgFsMsdpSACacheEntry);
                MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_ISSET_POOLID,
                                    (UINT1 *) pMsdpTrgIsSetFsMsdpSACacheEntry);
                return OSIX_FAILURE;
            }

            if ((pMsdpSetFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == 1)
                                    && (pMsdpSetFsMsdpSACacheEntry->MibObject.
                                        i4FsMsdpSACacheStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pMsdpTrgFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheAddrType =
                    pMsdpSetFsMsdpSACacheEntry->MibObject.
                    i4FsMsdpSACacheAddrType;
                MEMCPY (pMsdpTrgFsMsdpSACacheEntry->MibObject.
                        au1FsMsdpSACacheGroupAddr,
                        pMsdpSetFsMsdpSACacheEntry->MibObject.
                        au1FsMsdpSACacheGroupAddr,
                        pMsdpSetFsMsdpSACacheEntry->MibObject.
                        i4FsMsdpSACacheGroupAddrLen);

                pMsdpTrgFsMsdpSACacheEntry->MibObject.
                    i4FsMsdpSACacheGroupAddrLen =
                    pMsdpSetFsMsdpSACacheEntry->MibObject.
                    i4FsMsdpSACacheGroupAddrLen;
                MEMCPY (pMsdpTrgFsMsdpSACacheEntry->MibObject.
                        au1FsMsdpSACacheSourceAddr,
                        pMsdpSetFsMsdpSACacheEntry->MibObject.
                        au1FsMsdpSACacheSourceAddr,
                        pMsdpSetFsMsdpSACacheEntry->MibObject.
                        i4FsMsdpSACacheSourceAddrLen);

                pMsdpTrgFsMsdpSACacheEntry->MibObject.
                    i4FsMsdpSACacheSourceAddrLen =
                    pMsdpSetFsMsdpSACacheEntry->MibObject.
                    i4FsMsdpSACacheSourceAddrLen;
                MEMCPY (pMsdpTrgFsMsdpSACacheEntry->MibObject.
                        au1FsMsdpSACacheOriginRP,
                        pMsdpSetFsMsdpSACacheEntry->MibObject.
                        au1FsMsdpSACacheOriginRP,
                        pMsdpSetFsMsdpSACacheEntry->MibObject.
                        i4FsMsdpSACacheOriginRPLen);

                pMsdpTrgFsMsdpSACacheEntry->MibObject.
                    i4FsMsdpSACacheOriginRPLen =
                    pMsdpSetFsMsdpSACacheEntry->MibObject.
                    i4FsMsdpSACacheOriginRPLen;
                pMsdpTrgFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus =
                    CREATE_AND_WAIT;
                pMsdpTrgIsSetFsMsdpSACacheEntry->bFsMsdpSACacheStatus =
                    OSIX_TRUE;

                if (MsdpSetAllFsMsdpSACacheTableTrigger
                    (pMsdpTrgFsMsdpSACacheEntry,
                     pMsdpTrgIsSetFsMsdpSACacheEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpSACacheTable: MsdpSetAllFsMsdpSACacheTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                        (UINT1 *) pMsdpFsMsdpSACacheEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                        (UINT1 *) pMsdpOldFsMsdpSACacheEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                        (UINT1 *) pMsdpTrgFsMsdpSACacheEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pMsdpTrgIsSetFsMsdpSACacheEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pMsdpSetFsMsdpSACacheEntry->MibObject.
                     i4FsMsdpSACacheStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pMsdpTrgFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus =
                    CREATE_AND_WAIT;
                pMsdpTrgIsSetFsMsdpSACacheEntry->bFsMsdpSACacheStatus =
                    OSIX_TRUE;

                if (MsdpSetAllFsMsdpSACacheTableTrigger
                    (pMsdpTrgFsMsdpSACacheEntry,
                     pMsdpTrgIsSetFsMsdpSACacheEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpSACacheTable: MsdpSetAllFsMsdpSACacheTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                        (UINT1 *) pMsdpFsMsdpSACacheEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                        (UINT1 *) pMsdpOldFsMsdpSACacheEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                        (UINT1 *) pMsdpTrgFsMsdpSACacheEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pMsdpTrgIsSetFsMsdpSACacheEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pMsdpSetFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus ==
                CREATE_AND_GO)
            {
                pMsdpSetFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus =
                    ACTIVE;
            }

            if (MsdpSetAllFsMsdpSACacheTableTrigger (pMsdpSetFsMsdpSACacheEntry,
                                                     pMsdpIsSetFsMsdpSACacheEntry,
                                                     SNMP_SUCCESS) !=
                OSIX_SUCCESS)
            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpSACacheTable:  MsdpSetAllFsMsdpSACacheTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpSACacheEntry);
            MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpSACacheEntry);
            MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_ISSET_POOLID,
                                (UINT1 *) pMsdpTrgIsSetFsMsdpSACacheEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (MsdpSetAllFsMsdpSACacheTableTrigger (pMsdpSetFsMsdpSACacheEntry,
                                                     pMsdpIsSetFsMsdpSACacheEntry,
                                                     SNMP_FAILURE) !=
                OSIX_SUCCESS)

            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpSACacheTable: MsdpSetAllFsMsdpSACacheTableTrigger function returns failure.\r\n"));
            }
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpSACacheTable: Failure.\r\n"));
            MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpSACacheEntry);
            MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpSACacheEntry);
            MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_ISSET_POOLID,
                                (UINT1 *) pMsdpTrgIsSetFsMsdpSACacheEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pMsdpSetFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus ==
              CREATE_AND_WAIT)
             || (pMsdpSetFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus ==
                 CREATE_AND_GO))
    {
        if (MsdpSetAllFsMsdpSACacheTableTrigger (pMsdpSetFsMsdpSACacheEntry,
                                                 pMsdpIsSetFsMsdpSACacheEntry,
                                                 SNMP_FAILURE) != OSIX_SUCCESS)

        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpSACacheTable: MsdpSetAllFsMsdpSACacheTableTrigger function returns failure.\r\n"));
        }
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpSetAllFsMsdpSACacheTable: The row is already present.\r\n"));
        MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpSACacheEntry);
        MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpSACacheEntry);
        MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpSACacheEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pMsdpOldFsMsdpSACacheEntry, pMsdpFsMsdpSACacheEntry,
            sizeof (tMsdpFsMsdpSACacheEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pMsdpSetFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus == DESTROY)
    {
        pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus = DESTROY;

        if (MsdpUtilUpdateFsMsdpSACacheTable (pMsdpOldFsMsdpSACacheEntry,
                                              pMsdpFsMsdpSACacheEntry,
                                              pMsdpIsSetFsMsdpSACacheEntry) !=
            OSIX_SUCCESS)
        {

            if (MsdpSetAllFsMsdpSACacheTableTrigger (pMsdpSetFsMsdpSACacheEntry,
                                                     pMsdpIsSetFsMsdpSACacheEntry,
                                                     SNMP_FAILURE) !=
                OSIX_SUCCESS)

            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpSACacheTable: MsdpSetAllFsMsdpSACacheTableTrigger function returns failure.\r\n"));
            }
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpSACacheTable: MsdpUtilUpdateFsMsdpSACacheTable function returns failure.\r\n"));
        }
        RBTreeRem (gMsdpGlobals.MsdpGlbMib.FsMsdpSACacheTable,
                   pMsdpFsMsdpSACacheEntry);
        if (MsdpSetAllFsMsdpSACacheTableTrigger
            (pMsdpSetFsMsdpSACacheEntry, pMsdpIsSetFsMsdpSACacheEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpSACacheTable: MsdpSetAllFsMsdpSACacheTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpSACacheEntry);
            MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpSACacheEntry);
            MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_ISSET_POOLID,
                                (UINT1 *) pMsdpTrgIsSetFsMsdpSACacheEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                            (UINT1 *) pMsdpFsMsdpSACacheEntry);
        MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpSACacheEntry);
        MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpSACacheEntry);
        MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpSACacheEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsMsdpSACacheTableFilterInputs
        (pMsdpFsMsdpSACacheEntry, pMsdpSetFsMsdpSACacheEntry,
         pMsdpIsSetFsMsdpSACacheEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpSACacheEntry);
        MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpSACacheEntry);
        MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpSACacheEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus == ACTIVE) &&
        (pMsdpSetFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus !=
         NOT_IN_SERVICE))
    {
        pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pMsdpTrgFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus =
            NOT_IN_SERVICE;
        pMsdpTrgIsSetFsMsdpSACacheEntry->bFsMsdpSACacheStatus = OSIX_TRUE;

        if (MsdpUtilUpdateFsMsdpSACacheTable (pMsdpOldFsMsdpSACacheEntry,
                                              pMsdpFsMsdpSACacheEntry,
                                              pMsdpIsSetFsMsdpSACacheEntry) !=
            OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pMsdpFsMsdpSACacheEntry, pMsdpOldFsMsdpSACacheEntry,
                    sizeof (tMsdpFsMsdpSACacheEntry));
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpSACacheTable:                 MsdpUtilUpdateFsMsdpSACacheTable Function returns failure.\r\n"));

            if (MsdpSetAllFsMsdpSACacheTableTrigger (pMsdpSetFsMsdpSACacheEntry,
                                                     pMsdpIsSetFsMsdpSACacheEntry,
                                                     SNMP_FAILURE) !=
                OSIX_SUCCESS)

            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpSACacheTable: MsdpSetAllFsMsdpSACacheTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpSACacheEntry);
            MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpSACacheEntry);
            MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_ISSET_POOLID,
                                (UINT1 *) pMsdpTrgIsSetFsMsdpSACacheEntry);
            return OSIX_FAILURE;
        }

        if (MsdpSetAllFsMsdpSACacheTableTrigger (pMsdpTrgFsMsdpSACacheEntry,
                                                 pMsdpTrgIsSetFsMsdpSACacheEntry,
                                                 SNMP_FAILURE) != OSIX_SUCCESS)
        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpSACacheTable: MsdpSetAllFsMsdpSACacheTableTrigger function returns failure.\r\n"));
        }
    }

    if (pMsdpSetFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheStatus != OSIX_FALSE)
    {
        pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus =
            pMsdpSetFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus = ACTIVE;
    }

    if (MsdpUtilUpdateFsMsdpSACacheTable (pMsdpOldFsMsdpSACacheEntry,
                                          pMsdpFsMsdpSACacheEntry,
                                          pMsdpIsSetFsMsdpSACacheEntry) !=
        OSIX_SUCCESS)
    {

        if (MsdpSetAllFsMsdpSACacheTableTrigger (pMsdpSetFsMsdpSACacheEntry,
                                                 pMsdpIsSetFsMsdpSACacheEntry,
                                                 SNMP_FAILURE) != OSIX_SUCCESS)

        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpSACacheTable: MsdpSetAllFsMsdpSACacheTableTrigger function returns failure.\r\n"));

        }
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpSetAllFsMsdpSACacheTable: MsdpUtilUpdateFsMsdpSACacheTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pMsdpFsMsdpSACacheEntry, pMsdpOldFsMsdpSACacheEntry,
                sizeof (tMsdpFsMsdpSACacheEntry));
        MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpSACacheEntry);
        MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpSACacheEntry);
        MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpSACacheEntry);
        return OSIX_FAILURE;

    }
    if (MsdpSetAllFsMsdpSACacheTableTrigger (pMsdpSetFsMsdpSACacheEntry,
                                             pMsdpIsSetFsMsdpSACacheEntry,
                                             SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpSetAllFsMsdpSACacheTable: MsdpSetAllFsMsdpSACacheTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                        (UINT1 *) pMsdpOldFsMsdpSACacheEntry);
    MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                        (UINT1 *) pMsdpTrgFsMsdpSACacheEntry);
    MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_ISSET_POOLID,
                        (UINT1 *) pMsdpTrgIsSetFsMsdpSACacheEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  MsdpSetAllFsMsdpMeshGroupTable
 Input       :  pMsdpSetFsMsdpMeshGroupEntry
                pMsdpIsSetFsMsdpMeshGroupEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpSetAllFsMsdpMeshGroupTable (tMsdpFsMsdpMeshGroupEntry *
                                pMsdpSetFsMsdpMeshGroupEntry,
                                tMsdpIsSetFsMsdpMeshGroupEntry *
                                pMsdpIsSetFsMsdpMeshGroupEntry,
                                INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tMsdpFsMsdpMeshGroupEntry *pMsdpFsMsdpMeshGroupEntry = NULL;
    tMsdpFsMsdpMeshGroupEntry *pMsdpOldFsMsdpMeshGroupEntry = NULL;
    tMsdpFsMsdpMeshGroupEntry *pMsdpTrgFsMsdpMeshGroupEntry = NULL;
    tMsdpIsSetFsMsdpMeshGroupEntry *pMsdpTrgIsSetFsMsdpMeshGroupEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pMsdpOldFsMsdpMeshGroupEntry =
        (tMsdpFsMsdpMeshGroupEntry *)
        MemAllocMemBlk (MSDP_FSMSDPMESHGROUPTABLE_POOLID);
    if (pMsdpOldFsMsdpMeshGroupEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pMsdpTrgFsMsdpMeshGroupEntry =
        (tMsdpFsMsdpMeshGroupEntry *)
        MemAllocMemBlk (MSDP_FSMSDPMESHGROUPTABLE_POOLID);
    if (pMsdpTrgFsMsdpMeshGroupEntry == NULL)
    {
        MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpMeshGroupEntry);
        return OSIX_FAILURE;
    }
    pMsdpTrgIsSetFsMsdpMeshGroupEntry =
        (tMsdpIsSetFsMsdpMeshGroupEntry *)
        MemAllocMemBlk (MSDP_FSMSDPMESHGROUPTABLE_ISSET_POOLID);
    if (pMsdpTrgIsSetFsMsdpMeshGroupEntry == NULL)
    {
        MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpMeshGroupEntry);
        MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpMeshGroupEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pMsdpOldFsMsdpMeshGroupEntry, 0,
            sizeof (tMsdpFsMsdpMeshGroupEntry));
    MEMSET (pMsdpTrgFsMsdpMeshGroupEntry, 0,
            sizeof (tMsdpFsMsdpMeshGroupEntry));
    MEMSET (pMsdpTrgIsSetFsMsdpMeshGroupEntry, 0,
            sizeof (tMsdpIsSetFsMsdpMeshGroupEntry));

    /* Check whether the node is already present */
    pMsdpFsMsdpMeshGroupEntry =
        RBTreeGet (gMsdpGlobals.MsdpGlbMib.FsMsdpMeshGroupTable,
                   (tRBElem *) pMsdpSetFsMsdpMeshGroupEntry);

    if (pMsdpFsMsdpMeshGroupEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pMsdpSetFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupStatus ==
             CREATE_AND_WAIT)
            || (pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                i4FsMsdpMeshGroupStatus == CREATE_AND_GO)
            ||
            ((pMsdpSetFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupStatus ==
              ACTIVE) && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pMsdpFsMsdpMeshGroupEntry =
                (tMsdpFsMsdpMeshGroupEntry *)
                MemAllocMemBlk (MSDP_FSMSDPMESHGROUPTABLE_POOLID);
            if (pMsdpFsMsdpMeshGroupEntry == NULL)
            {
                if (MsdpSetAllFsMsdpMeshGroupTableTrigger
                    (pMsdpSetFsMsdpMeshGroupEntry,
                     pMsdpIsSetFsMsdpMeshGroupEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpMeshGroupTable:MsdpSetAllFsMsdpMeshGroupTableTrigger function fails\r\n"));

                }
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpMeshGroupTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                    (UINT1 *) pMsdpOldFsMsdpMeshGroupEntry);
                MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                    (UINT1 *) pMsdpTrgFsMsdpMeshGroupEntry);
                MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pMsdpTrgIsSetFsMsdpMeshGroupEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pMsdpFsMsdpMeshGroupEntry, 0,
                    sizeof (tMsdpFsMsdpMeshGroupEntry));
            if ((MsdpInitializeFsMsdpMeshGroupTable (pMsdpFsMsdpMeshGroupEntry))
                == OSIX_FAILURE)
            {
                if (MsdpSetAllFsMsdpMeshGroupTableTrigger
                    (pMsdpSetFsMsdpMeshGroupEntry,
                     pMsdpIsSetFsMsdpMeshGroupEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpMeshGroupTable:MsdpSetAllFsMsdpMeshGroupTableTrigger function fails\r\n"));

                }
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpMeshGroupTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                    (UINT1 *) pMsdpFsMsdpMeshGroupEntry);
                MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                    (UINT1 *) pMsdpOldFsMsdpMeshGroupEntry);
                MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                    (UINT1 *) pMsdpTrgFsMsdpMeshGroupEntry);
                MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pMsdpTrgIsSetFsMsdpMeshGroupEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupName !=
                OSIX_FALSE)
            {
                MEMCPY (pMsdpFsMsdpMeshGroupEntry->MibObject.
                        au1FsMsdpMeshGroupName,
                        pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                        au1FsMsdpMeshGroupName,
                        pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                        i4FsMsdpMeshGroupNameLen);

                pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupNameLen =
                    pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                    i4FsMsdpMeshGroupNameLen;
            }

            if (pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupAddrType !=
                OSIX_FALSE)
            {
                pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupAddrType =
                    pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                    i4FsMsdpMeshGroupAddrType;
            }

            if (pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupPeerAddress !=
                OSIX_FALSE)
            {
                MEMCPY (pMsdpFsMsdpMeshGroupEntry->MibObject.
                        au1FsMsdpMeshGroupPeerAddress,
                        pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                        au1FsMsdpMeshGroupPeerAddress,
                        pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                        i4FsMsdpMeshGroupPeerAddressLen);

                pMsdpFsMsdpMeshGroupEntry->MibObject.
                    i4FsMsdpMeshGroupPeerAddressLen =
                    pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                    i4FsMsdpMeshGroupPeerAddressLen;
            }

            if (pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupStatus !=
                OSIX_FALSE)
            {
                pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupStatus =
                    pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                    i4FsMsdpMeshGroupStatus;
            }

            if ((pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                 i4FsMsdpMeshGroupStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                        i4FsMsdpMeshGroupStatus == ACTIVE)))
            {
                pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupStatus =
                    ACTIVE;
            }
            else if (pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                     i4FsMsdpMeshGroupStatus == CREATE_AND_WAIT)
            {
                pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupStatus =
                    NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gMsdpGlobals.MsdpGlbMib.FsMsdpMeshGroupTable,
                 (tRBElem *) pMsdpFsMsdpMeshGroupEntry) != RB_SUCCESS)
            {
                if (MsdpSetAllFsMsdpMeshGroupTableTrigger
                    (pMsdpSetFsMsdpMeshGroupEntry,
                     pMsdpIsSetFsMsdpMeshGroupEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpMeshGroupTable: MsdpSetAllFsMsdpMeshGroupTableTrigger function returns failure.\r\n"));
                }
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpMeshGroupTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                    (UINT1 *) pMsdpFsMsdpMeshGroupEntry);
                MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                    (UINT1 *) pMsdpOldFsMsdpMeshGroupEntry);
                MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                    (UINT1 *) pMsdpTrgFsMsdpMeshGroupEntry);
                MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pMsdpTrgIsSetFsMsdpMeshGroupEntry);
                return OSIX_FAILURE;
            }
            if (MsdpUtilUpdateFsMsdpMeshGroupTable
                (NULL, pMsdpFsMsdpMeshGroupEntry,
                 pMsdpIsSetFsMsdpMeshGroupEntry) != OSIX_SUCCESS)
            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpMeshGroupTable: MsdpUtilUpdateFsMsdpMeshGroupTable function returns failure.\r\n"));

                if (MsdpSetAllFsMsdpMeshGroupTableTrigger
                    (pMsdpSetFsMsdpMeshGroupEntry,
                     pMsdpIsSetFsMsdpMeshGroupEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpMeshGroupTable: MsdpSetAllFsMsdpMeshGroupTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gMsdpGlobals.MsdpGlbMib.FsMsdpMeshGroupTable,
                           pMsdpFsMsdpMeshGroupEntry);
                MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                    (UINT1 *) pMsdpFsMsdpMeshGroupEntry);
                MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                    (UINT1 *) pMsdpOldFsMsdpMeshGroupEntry);
                MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                    (UINT1 *) pMsdpTrgFsMsdpMeshGroupEntry);
                MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pMsdpTrgIsSetFsMsdpMeshGroupEntry);
                return OSIX_FAILURE;
            }

            if ((pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                 i4FsMsdpMeshGroupStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                        i4FsMsdpMeshGroupStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                MEMCPY (pMsdpTrgFsMsdpMeshGroupEntry->MibObject.
                        au1FsMsdpMeshGroupName,
                        pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                        au1FsMsdpMeshGroupName,
                        pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                        i4FsMsdpMeshGroupNameLen);

                pMsdpTrgFsMsdpMeshGroupEntry->MibObject.
                    i4FsMsdpMeshGroupNameLen =
                    pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                    i4FsMsdpMeshGroupNameLen;
                pMsdpTrgFsMsdpMeshGroupEntry->MibObject.
                    i4FsMsdpMeshGroupAddrType =
                    pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                    i4FsMsdpMeshGroupAddrType;
                MEMCPY (pMsdpTrgFsMsdpMeshGroupEntry->MibObject.
                        au1FsMsdpMeshGroupPeerAddress,
                        pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                        au1FsMsdpMeshGroupPeerAddress,
                        pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                        i4FsMsdpMeshGroupPeerAddressLen);

                pMsdpTrgFsMsdpMeshGroupEntry->MibObject.
                    i4FsMsdpMeshGroupPeerAddressLen =
                    pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                    i4FsMsdpMeshGroupPeerAddressLen;
                pMsdpTrgFsMsdpMeshGroupEntry->MibObject.
                    i4FsMsdpMeshGroupStatus = CREATE_AND_WAIT;
                pMsdpTrgIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupStatus =
                    OSIX_TRUE;

                if (MsdpSetAllFsMsdpMeshGroupTableTrigger
                    (pMsdpTrgFsMsdpMeshGroupEntry,
                     pMsdpTrgIsSetFsMsdpMeshGroupEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpMeshGroupTable: MsdpSetAllFsMsdpMeshGroupTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                        (UINT1 *) pMsdpFsMsdpMeshGroupEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                        (UINT1 *) pMsdpOldFsMsdpMeshGroupEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                        (UINT1 *) pMsdpTrgFsMsdpMeshGroupEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pMsdpTrgIsSetFsMsdpMeshGroupEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                     i4FsMsdpMeshGroupStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pMsdpTrgFsMsdpMeshGroupEntry->MibObject.
                    i4FsMsdpMeshGroupStatus = CREATE_AND_WAIT;
                pMsdpTrgIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupStatus =
                    OSIX_TRUE;

                if (MsdpSetAllFsMsdpMeshGroupTableTrigger
                    (pMsdpTrgFsMsdpMeshGroupEntry,
                     pMsdpTrgIsSetFsMsdpMeshGroupEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpMeshGroupTable: MsdpSetAllFsMsdpMeshGroupTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                        (UINT1 *) pMsdpFsMsdpMeshGroupEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                        (UINT1 *) pMsdpOldFsMsdpMeshGroupEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                        (UINT1 *) pMsdpTrgFsMsdpMeshGroupEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pMsdpTrgIsSetFsMsdpMeshGroupEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                i4FsMsdpMeshGroupStatus == CREATE_AND_GO)
            {
                pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                    i4FsMsdpMeshGroupStatus = ACTIVE;
            }

            if (MsdpSetAllFsMsdpMeshGroupTableTrigger
                (pMsdpSetFsMsdpMeshGroupEntry, pMsdpIsSetFsMsdpMeshGroupEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpMeshGroupTable:  MsdpSetAllFsMsdpMeshGroupTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpMeshGroupEntry);
            MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpMeshGroupEntry);
            MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_ISSET_POOLID,
                                (UINT1 *) pMsdpTrgIsSetFsMsdpMeshGroupEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (MsdpSetAllFsMsdpMeshGroupTableTrigger
                (pMsdpSetFsMsdpMeshGroupEntry, pMsdpIsSetFsMsdpMeshGroupEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpMeshGroupTable: MsdpSetAllFsMsdpMeshGroupTableTrigger function returns failure.\r\n"));
            }
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpMeshGroupTable: Failure.\r\n"));
            MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpMeshGroupEntry);
            MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpMeshGroupEntry);
            MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_ISSET_POOLID,
                                (UINT1 *) pMsdpTrgIsSetFsMsdpMeshGroupEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pMsdpSetFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupStatus ==
              CREATE_AND_WAIT)
             || (pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                 i4FsMsdpMeshGroupStatus == CREATE_AND_GO))
    {
        if (MsdpSetAllFsMsdpMeshGroupTableTrigger (pMsdpSetFsMsdpMeshGroupEntry,
                                                   pMsdpIsSetFsMsdpMeshGroupEntry,
                                                   SNMP_FAILURE) !=
            OSIX_SUCCESS)

        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpMeshGroupTable: MsdpSetAllFsMsdpMeshGroupTableTrigger function returns failure.\r\n"));
        }
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpSetAllFsMsdpMeshGroupTable: The row is already present.\r\n"));
        MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpMeshGroupEntry);
        MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpMeshGroupEntry);
        MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpMeshGroupEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pMsdpOldFsMsdpMeshGroupEntry, pMsdpFsMsdpMeshGroupEntry,
            sizeof (tMsdpFsMsdpMeshGroupEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pMsdpSetFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupStatus ==
        DESTROY)
    {
        pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupStatus = DESTROY;

        if (MsdpUtilUpdateFsMsdpMeshGroupTable (pMsdpOldFsMsdpMeshGroupEntry,
                                                pMsdpFsMsdpMeshGroupEntry,
                                                pMsdpIsSetFsMsdpMeshGroupEntry)
            != OSIX_SUCCESS)
        {

            if (MsdpSetAllFsMsdpMeshGroupTableTrigger
                (pMsdpSetFsMsdpMeshGroupEntry, pMsdpIsSetFsMsdpMeshGroupEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpMeshGroupTable: MsdpSetAllFsMsdpMeshGroupTableTrigger function returns failure.\r\n"));
            }
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpMeshGroupTable: MsdpUtilUpdateFsMsdpMeshGroupTable function returns failure.\r\n"));
        }
        RBTreeRem (gMsdpGlobals.MsdpGlbMib.FsMsdpMeshGroupTable,
                   pMsdpFsMsdpMeshGroupEntry);
        if (MsdpSetAllFsMsdpMeshGroupTableTrigger
            (pMsdpSetFsMsdpMeshGroupEntry, pMsdpIsSetFsMsdpMeshGroupEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpMeshGroupTable: MsdpSetAllFsMsdpMeshGroupTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpMeshGroupEntry);
            MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpMeshGroupEntry);
            MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_ISSET_POOLID,
                                (UINT1 *) pMsdpTrgIsSetFsMsdpMeshGroupEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                            (UINT1 *) pMsdpFsMsdpMeshGroupEntry);
        MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpMeshGroupEntry);
        MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpMeshGroupEntry);
        MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpMeshGroupEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsMsdpMeshGroupTableFilterInputs
        (pMsdpFsMsdpMeshGroupEntry, pMsdpSetFsMsdpMeshGroupEntry,
         pMsdpIsSetFsMsdpMeshGroupEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpMeshGroupEntry);
        MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpMeshGroupEntry);
        MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpMeshGroupEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupStatus == ACTIVE)
        && (pMsdpSetFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupStatus !=
            NOT_IN_SERVICE))
    {
        pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pMsdpTrgFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupStatus =
            NOT_IN_SERVICE;
        pMsdpTrgIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupStatus = OSIX_TRUE;

        if (MsdpUtilUpdateFsMsdpMeshGroupTable (pMsdpOldFsMsdpMeshGroupEntry,
                                                pMsdpFsMsdpMeshGroupEntry,
                                                pMsdpIsSetFsMsdpMeshGroupEntry)
            != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pMsdpFsMsdpMeshGroupEntry, pMsdpOldFsMsdpMeshGroupEntry,
                    sizeof (tMsdpFsMsdpMeshGroupEntry));
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpMeshGroupTable:                 MsdpUtilUpdateFsMsdpMeshGroupTable Function returns failure.\r\n"));

            if (MsdpSetAllFsMsdpMeshGroupTableTrigger
                (pMsdpSetFsMsdpMeshGroupEntry, pMsdpIsSetFsMsdpMeshGroupEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpMeshGroupTable: MsdpSetAllFsMsdpMeshGroupTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpMeshGroupEntry);
            MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpMeshGroupEntry);
            MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_ISSET_POOLID,
                                (UINT1 *) pMsdpTrgIsSetFsMsdpMeshGroupEntry);
            return OSIX_FAILURE;
        }

        if (MsdpSetAllFsMsdpMeshGroupTableTrigger (pMsdpTrgFsMsdpMeshGroupEntry,
                                                   pMsdpTrgIsSetFsMsdpMeshGroupEntry,
                                                   SNMP_FAILURE) !=
            OSIX_SUCCESS)
        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpMeshGroupTable: MsdpSetAllFsMsdpMeshGroupTableTrigger function returns failure.\r\n"));
        }
    }

    if (pMsdpSetFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupStatus ==
        ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupStatus != OSIX_FALSE)
    {
        pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupStatus =
            pMsdpSetFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupStatus = ACTIVE;
    }

    if (MsdpUtilUpdateFsMsdpMeshGroupTable (pMsdpOldFsMsdpMeshGroupEntry,
                                            pMsdpFsMsdpMeshGroupEntry,
                                            pMsdpIsSetFsMsdpMeshGroupEntry) !=
        OSIX_SUCCESS)
    {

        if (MsdpSetAllFsMsdpMeshGroupTableTrigger (pMsdpSetFsMsdpMeshGroupEntry,
                                                   pMsdpIsSetFsMsdpMeshGroupEntry,
                                                   SNMP_FAILURE) !=
            OSIX_SUCCESS)

        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpMeshGroupTable: MsdpSetAllFsMsdpMeshGroupTableTrigger function returns failure.\r\n"));

        }
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpSetAllFsMsdpMeshGroupTable: MsdpUtilUpdateFsMsdpMeshGroupTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pMsdpFsMsdpMeshGroupEntry, pMsdpOldFsMsdpMeshGroupEntry,
                sizeof (tMsdpFsMsdpMeshGroupEntry));
        MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpMeshGroupEntry);
        MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpMeshGroupEntry);
        MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpMeshGroupEntry);
        return OSIX_FAILURE;

    }
    if (MsdpSetAllFsMsdpMeshGroupTableTrigger (pMsdpSetFsMsdpMeshGroupEntry,
                                               pMsdpIsSetFsMsdpMeshGroupEntry,
                                               SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpSetAllFsMsdpMeshGroupTable: MsdpSetAllFsMsdpMeshGroupTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                        (UINT1 *) pMsdpOldFsMsdpMeshGroupEntry);
    MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                        (UINT1 *) pMsdpTrgFsMsdpMeshGroupEntry);
    MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_ISSET_POOLID,
                        (UINT1 *) pMsdpTrgIsSetFsMsdpMeshGroupEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  MsdpSetAllFsMsdpRPTable
 Input       :  pMsdpSetFsMsdpRPEntry
                pMsdpIsSetFsMsdpRPEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpSetAllFsMsdpRPTable (tMsdpFsMsdpRPEntry * pMsdpSetFsMsdpRPEntry,
                         tMsdpIsSetFsMsdpRPEntry * pMsdpIsSetFsMsdpRPEntry,
                         INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tMsdpFsMsdpRPEntry *pMsdpFsMsdpRPEntry = NULL;
    tMsdpFsMsdpRPEntry *pMsdpOldFsMsdpRPEntry = NULL;
    tMsdpFsMsdpRPEntry *pMsdpTrgFsMsdpRPEntry = NULL;
    tMsdpIsSetFsMsdpRPEntry *pMsdpTrgIsSetFsMsdpRPEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pMsdpOldFsMsdpRPEntry =
        (tMsdpFsMsdpRPEntry *) MemAllocMemBlk (MSDP_FSMSDPRPTABLE_POOLID);
    if (pMsdpOldFsMsdpRPEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pMsdpTrgFsMsdpRPEntry =
        (tMsdpFsMsdpRPEntry *) MemAllocMemBlk (MSDP_FSMSDPRPTABLE_POOLID);
    if (pMsdpTrgFsMsdpRPEntry == NULL)
    {
        MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpRPEntry);
        return OSIX_FAILURE;
    }
    pMsdpTrgIsSetFsMsdpRPEntry =
        (tMsdpIsSetFsMsdpRPEntry *)
        MemAllocMemBlk (MSDP_FSMSDPRPTABLE_ISSET_POOLID);
    if (pMsdpTrgIsSetFsMsdpRPEntry == NULL)
    {
        MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpRPEntry);
        MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpRPEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pMsdpOldFsMsdpRPEntry, 0, sizeof (tMsdpFsMsdpRPEntry));
    MEMSET (pMsdpTrgFsMsdpRPEntry, 0, sizeof (tMsdpFsMsdpRPEntry));
    MEMSET (pMsdpTrgIsSetFsMsdpRPEntry, 0, sizeof (tMsdpIsSetFsMsdpRPEntry));

    /* Check whether the node is already present */
    pMsdpFsMsdpRPEntry =
        RBTreeGet (gMsdpGlobals.MsdpGlbMib.FsMsdpRPTable,
                   (tRBElem *) pMsdpSetFsMsdpRPEntry);

    if (pMsdpFsMsdpRPEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus ==
             CREATE_AND_WAIT)
            || (pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus ==
                CREATE_AND_GO)
            || ((pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus == ACTIVE)
                && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pMsdpFsMsdpRPEntry =
                (tMsdpFsMsdpRPEntry *)
                MemAllocMemBlk (MSDP_FSMSDPRPTABLE_POOLID);
            if (pMsdpFsMsdpRPEntry == NULL)
            {
                if (MsdpSetAllFsMsdpRPTableTrigger (pMsdpSetFsMsdpRPEntry,
                                                    pMsdpIsSetFsMsdpRPEntry,
                                                    SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpRPTable:MsdpSetAllFsMsdpRPTableTrigger function fails\r\n"));

                }
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpRPTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                    (UINT1 *) pMsdpOldFsMsdpRPEntry);
                MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                    (UINT1 *) pMsdpTrgFsMsdpRPEntry);
                MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_ISSET_POOLID,
                                    (UINT1 *) pMsdpTrgIsSetFsMsdpRPEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pMsdpFsMsdpRPEntry, 0, sizeof (tMsdpFsMsdpRPEntry));
            if ((MsdpInitializeFsMsdpRPTable (pMsdpFsMsdpRPEntry)) ==
                OSIX_FAILURE)
            {
                if (MsdpSetAllFsMsdpRPTableTrigger (pMsdpSetFsMsdpRPEntry,
                                                    pMsdpIsSetFsMsdpRPEntry,
                                                    SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpRPTable:MsdpSetAllFsMsdpRPTableTrigger function fails\r\n"));

                }
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpRPTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                    (UINT1 *) pMsdpFsMsdpRPEntry);
                MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                    (UINT1 *) pMsdpOldFsMsdpRPEntry);
                MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                    (UINT1 *) pMsdpTrgFsMsdpRPEntry);
                MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_ISSET_POOLID,
                                    (UINT1 *) pMsdpTrgIsSetFsMsdpRPEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPAddrType != OSIX_FALSE)
            {
                pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPAddrType =
                    pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPAddrType;
            }

            if (pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPAddress != OSIX_FALSE)
            {
                MEMCPY (pMsdpFsMsdpRPEntry->MibObject.au1FsMsdpRPAddress,
                        pMsdpSetFsMsdpRPEntry->MibObject.au1FsMsdpRPAddress,
                        pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPAddressLen);

                pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPAddressLen =
                    pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPAddressLen;
            }

            if (pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPStatus != OSIX_FALSE)
            {
                pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus =
                    pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus;
            }

            if ((pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == 1)
                                    && (pMsdpSetFsMsdpRPEntry->MibObject.
                                        i4FsMsdpRPStatus == ACTIVE)))
            {
                pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus = ACTIVE;
            }
            else if (pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus ==
                     CREATE_AND_WAIT)
            {
                pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gMsdpGlobals.MsdpGlbMib.FsMsdpRPTable,
                 (tRBElem *) pMsdpFsMsdpRPEntry) != RB_SUCCESS)
            {
                if (MsdpSetAllFsMsdpRPTableTrigger (pMsdpSetFsMsdpRPEntry,
                                                    pMsdpIsSetFsMsdpRPEntry,
                                                    SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpRPTable: MsdpSetAllFsMsdpRPTableTrigger function returns failure.\r\n"));
                }
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpRPTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                    (UINT1 *) pMsdpFsMsdpRPEntry);
                MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                    (UINT1 *) pMsdpOldFsMsdpRPEntry);
                MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                    (UINT1 *) pMsdpTrgFsMsdpRPEntry);
                MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_ISSET_POOLID,
                                    (UINT1 *) pMsdpTrgIsSetFsMsdpRPEntry);
                return OSIX_FAILURE;
            }
            if (MsdpUtilUpdateFsMsdpRPTable
                (NULL, pMsdpFsMsdpRPEntry,
                 pMsdpIsSetFsMsdpRPEntry) != OSIX_SUCCESS)
            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpRPTable: MsdpUtilUpdateFsMsdpRPTable function returns failure.\r\n"));

                if (MsdpSetAllFsMsdpRPTableTrigger (pMsdpSetFsMsdpRPEntry,
                                                    pMsdpIsSetFsMsdpRPEntry,
                                                    SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpRPTable: MsdpSetAllFsMsdpRPTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gMsdpGlobals.MsdpGlbMib.FsMsdpRPTable,
                           pMsdpFsMsdpRPEntry);
                MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                    (UINT1 *) pMsdpFsMsdpRPEntry);
                MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                    (UINT1 *) pMsdpOldFsMsdpRPEntry);
                MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                    (UINT1 *) pMsdpTrgFsMsdpRPEntry);
                MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_ISSET_POOLID,
                                    (UINT1 *) pMsdpTrgIsSetFsMsdpRPEntry);
                return OSIX_FAILURE;
            }

            if ((pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == 1)
                                    && (pMsdpSetFsMsdpRPEntry->MibObject.
                                        i4FsMsdpRPStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pMsdpTrgFsMsdpRPEntry->MibObject.i4FsMsdpRPAddrType =
                    pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPAddrType;
                pMsdpTrgFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus =
                    CREATE_AND_WAIT;
                pMsdpTrgIsSetFsMsdpRPEntry->bFsMsdpRPStatus = OSIX_TRUE;

                if (MsdpSetAllFsMsdpRPTableTrigger (pMsdpTrgFsMsdpRPEntry,
                                                    pMsdpTrgIsSetFsMsdpRPEntry,
                                                    SNMP_FAILURE) !=
                    OSIX_SUCCESS)
                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpRPTable: MsdpSetAllFsMsdpRPTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                        (UINT1 *) pMsdpFsMsdpRPEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                        (UINT1 *) pMsdpOldFsMsdpRPEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                        (UINT1 *) pMsdpTrgFsMsdpRPEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_ISSET_POOLID,
                                        (UINT1 *) pMsdpTrgIsSetFsMsdpRPEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus ==
                     CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pMsdpTrgFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus =
                    CREATE_AND_WAIT;
                pMsdpTrgIsSetFsMsdpRPEntry->bFsMsdpRPStatus = OSIX_TRUE;

                if (MsdpSetAllFsMsdpRPTableTrigger (pMsdpTrgFsMsdpRPEntry,
                                                    pMsdpTrgIsSetFsMsdpRPEntry,
                                                    SNMP_FAILURE) !=
                    OSIX_SUCCESS)
                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpRPTable: MsdpSetAllFsMsdpRPTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                        (UINT1 *) pMsdpFsMsdpRPEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                        (UINT1 *) pMsdpOldFsMsdpRPEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                        (UINT1 *) pMsdpTrgFsMsdpRPEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_ISSET_POOLID,
                                        (UINT1 *) pMsdpTrgIsSetFsMsdpRPEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus ==
                CREATE_AND_GO)
            {
                pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus = ACTIVE;
            }

            if (MsdpSetAllFsMsdpRPTableTrigger (pMsdpSetFsMsdpRPEntry,
                                                pMsdpIsSetFsMsdpRPEntry,
                                                SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpRPTable:  MsdpSetAllFsMsdpRPTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpRPEntry);
            MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpRPEntry);
            MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_ISSET_POOLID,
                                (UINT1 *) pMsdpTrgIsSetFsMsdpRPEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (MsdpSetAllFsMsdpRPTableTrigger (pMsdpSetFsMsdpRPEntry,
                                                pMsdpIsSetFsMsdpRPEntry,
                                                SNMP_FAILURE) != OSIX_SUCCESS)

            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpRPTable: MsdpSetAllFsMsdpRPTableTrigger function returns failure.\r\n"));
            }
            MSDP_TRC ((MSDP_UTIL_TRC, "MsdpSetAllFsMsdpRPTable: Failure.\r\n"));
            MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpRPEntry);
            MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpRPEntry);
            MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_ISSET_POOLID,
                                (UINT1 *) pMsdpTrgIsSetFsMsdpRPEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus ==
              CREATE_AND_WAIT)
             || (pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus ==
                 CREATE_AND_GO))
    {
        if (MsdpSetAllFsMsdpRPTableTrigger (pMsdpSetFsMsdpRPEntry,
                                            pMsdpIsSetFsMsdpRPEntry,
                                            SNMP_FAILURE) != OSIX_SUCCESS)

        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpRPTable: MsdpSetAllFsMsdpRPTableTrigger function returns failure.\r\n"));
        }
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpSetAllFsMsdpRPTable: The row is already present.\r\n"));
        MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpRPEntry);
        MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpRPEntry);
        MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpRPEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pMsdpOldFsMsdpRPEntry, pMsdpFsMsdpRPEntry,
            sizeof (tMsdpFsMsdpRPEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus == DESTROY)
    {
        pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus = DESTROY;

        if (MsdpUtilUpdateFsMsdpRPTable (pMsdpOldFsMsdpRPEntry,
                                         pMsdpFsMsdpRPEntry,
                                         pMsdpIsSetFsMsdpRPEntry) !=
            OSIX_SUCCESS)
        {

            if (MsdpSetAllFsMsdpRPTableTrigger (pMsdpSetFsMsdpRPEntry,
                                                pMsdpIsSetFsMsdpRPEntry,
                                                SNMP_FAILURE) != OSIX_SUCCESS)

            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpRPTable: MsdpSetAllFsMsdpRPTableTrigger function returns failure.\r\n"));
            }
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpRPTable: MsdpUtilUpdateFsMsdpRPTable function returns failure.\r\n"));
        }
        RBTreeRem (gMsdpGlobals.MsdpGlbMib.FsMsdpRPTable, pMsdpFsMsdpRPEntry);
        if (MsdpSetAllFsMsdpRPTableTrigger (pMsdpSetFsMsdpRPEntry,
                                            pMsdpIsSetFsMsdpRPEntry,
                                            SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpRPTable: MsdpSetAllFsMsdpRPTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpRPEntry);
            MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpRPEntry);
            MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_ISSET_POOLID,
                                (UINT1 *) pMsdpTrgIsSetFsMsdpRPEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                            (UINT1 *) pMsdpFsMsdpRPEntry);
        MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpRPEntry);
        MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpRPEntry);
        MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpRPEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsMsdpRPTableFilterInputs (pMsdpFsMsdpRPEntry, pMsdpSetFsMsdpRPEntry,
                                   pMsdpIsSetFsMsdpRPEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpRPEntry);
        MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpRPEntry);
        MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpRPEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus == ACTIVE) &&
        (pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus != NOT_IN_SERVICE))
    {
        pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pMsdpTrgFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus = NOT_IN_SERVICE;
        pMsdpTrgIsSetFsMsdpRPEntry->bFsMsdpRPStatus = OSIX_TRUE;

        if (MsdpUtilUpdateFsMsdpRPTable (pMsdpOldFsMsdpRPEntry,
                                         pMsdpFsMsdpRPEntry,
                                         pMsdpIsSetFsMsdpRPEntry) !=
            OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pMsdpFsMsdpRPEntry, pMsdpOldFsMsdpRPEntry,
                    sizeof (tMsdpFsMsdpRPEntry));
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpRPTable:                 MsdpUtilUpdateFsMsdpRPTable Function returns failure.\r\n"));

            if (MsdpSetAllFsMsdpRPTableTrigger (pMsdpSetFsMsdpRPEntry,
                                                pMsdpIsSetFsMsdpRPEntry,
                                                SNMP_FAILURE) != OSIX_SUCCESS)

            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpRPTable: MsdpSetAllFsMsdpRPTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpRPEntry);
            MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpRPEntry);
            MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_ISSET_POOLID,
                                (UINT1 *) pMsdpTrgIsSetFsMsdpRPEntry);
            return OSIX_FAILURE;
        }

        if (MsdpSetAllFsMsdpRPTableTrigger (pMsdpTrgFsMsdpRPEntry,
                                            pMsdpTrgIsSetFsMsdpRPEntry,
                                            SNMP_FAILURE) != OSIX_SUCCESS)
        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpRPTable: MsdpSetAllFsMsdpRPTableTrigger function returns failure.\r\n"));
        }
    }

    if (pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPAddress != OSIX_FALSE)
    {
        MEMCPY (pMsdpFsMsdpRPEntry->MibObject.au1FsMsdpRPAddress,
                pMsdpSetFsMsdpRPEntry->MibObject.au1FsMsdpRPAddress,
                pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPAddressLen);

        pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPAddressLen =
            pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPAddressLen;
    }
    if (pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPStatus != OSIX_FALSE)
    {
        pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus =
            pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus = ACTIVE;
    }

    if (MsdpUtilUpdateFsMsdpRPTable (pMsdpOldFsMsdpRPEntry,
                                     pMsdpFsMsdpRPEntry,
                                     pMsdpIsSetFsMsdpRPEntry) != OSIX_SUCCESS)
    {

        if (MsdpSetAllFsMsdpRPTableTrigger (pMsdpSetFsMsdpRPEntry,
                                            pMsdpIsSetFsMsdpRPEntry,
                                            SNMP_FAILURE) != OSIX_SUCCESS)

        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpRPTable: MsdpSetAllFsMsdpRPTableTrigger function returns failure.\r\n"));

        }
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpSetAllFsMsdpRPTable: MsdpUtilUpdateFsMsdpRPTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pMsdpFsMsdpRPEntry, pMsdpOldFsMsdpRPEntry,
                sizeof (tMsdpFsMsdpRPEntry));
        MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpRPEntry);
        MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpRPEntry);
        MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpRPEntry);
        return OSIX_FAILURE;

    }
    if (MsdpSetAllFsMsdpRPTableTrigger (pMsdpSetFsMsdpRPEntry,
                                        pMsdpIsSetFsMsdpRPEntry,
                                        SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpSetAllFsMsdpRPTable: MsdpSetAllFsMsdpRPTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                        (UINT1 *) pMsdpOldFsMsdpRPEntry);
    MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                        (UINT1 *) pMsdpTrgFsMsdpRPEntry);
    MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_ISSET_POOLID,
                        (UINT1 *) pMsdpTrgIsSetFsMsdpRPEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  MsdpSetAllFsMsdpPeerFilterTable
 Input       :  pMsdpSetFsMsdpPeerFilterEntry
                pMsdpIsSetFsMsdpPeerFilterEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpSetAllFsMsdpPeerFilterTable (tMsdpFsMsdpPeerFilterEntry *
                                 pMsdpSetFsMsdpPeerFilterEntry,
                                 tMsdpIsSetFsMsdpPeerFilterEntry *
                                 pMsdpIsSetFsMsdpPeerFilterEntry,
                                 INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tMsdpFsMsdpPeerFilterEntry *pMsdpFsMsdpPeerFilterEntry = NULL;
    tMsdpFsMsdpPeerFilterEntry *pMsdpOldFsMsdpPeerFilterEntry = NULL;
    tMsdpFsMsdpPeerFilterEntry *pMsdpTrgFsMsdpPeerFilterEntry = NULL;
    tMsdpIsSetFsMsdpPeerFilterEntry *pMsdpTrgIsSetFsMsdpPeerFilterEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pMsdpOldFsMsdpPeerFilterEntry =
        (tMsdpFsMsdpPeerFilterEntry *)
        MemAllocMemBlk (MSDP_FSMSDPPEERFILTERTABLE_POOLID);
    if (pMsdpOldFsMsdpPeerFilterEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pMsdpTrgFsMsdpPeerFilterEntry =
        (tMsdpFsMsdpPeerFilterEntry *)
        MemAllocMemBlk (MSDP_FSMSDPPEERFILTERTABLE_POOLID);
    if (pMsdpTrgFsMsdpPeerFilterEntry == NULL)
    {
        MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpPeerFilterEntry);
        return OSIX_FAILURE;
    }
    pMsdpTrgIsSetFsMsdpPeerFilterEntry =
        (tMsdpIsSetFsMsdpPeerFilterEntry *)
        MemAllocMemBlk (MSDP_FSMSDPPEERFILTERTABLE_ISSET_POOLID);
    if (pMsdpTrgIsSetFsMsdpPeerFilterEntry == NULL)
    {
        MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpPeerFilterEntry);
        MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpPeerFilterEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pMsdpOldFsMsdpPeerFilterEntry, 0,
            sizeof (tMsdpFsMsdpPeerFilterEntry));
    MEMSET (pMsdpTrgFsMsdpPeerFilterEntry, 0,
            sizeof (tMsdpFsMsdpPeerFilterEntry));
    MEMSET (pMsdpTrgIsSetFsMsdpPeerFilterEntry, 0,
            sizeof (tMsdpIsSetFsMsdpPeerFilterEntry));

    /* Check whether the node is already present */
    pMsdpFsMsdpPeerFilterEntry =
        RBTreeGet (gMsdpGlobals.MsdpGlbMib.FsMsdpPeerFilterTable,
                   (tRBElem *) pMsdpSetFsMsdpPeerFilterEntry);

    if (pMsdpFsMsdpPeerFilterEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pMsdpSetFsMsdpPeerFilterEntry->MibObject.
             i4FsMsdpPeerFilterStatus == CREATE_AND_WAIT)
            || (pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                i4FsMsdpPeerFilterStatus == CREATE_AND_GO)
            ||
            ((pMsdpSetFsMsdpPeerFilterEntry->MibObject.
              i4FsMsdpPeerFilterStatus == ACTIVE) && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pMsdpFsMsdpPeerFilterEntry =
                (tMsdpFsMsdpPeerFilterEntry *)
                MemAllocMemBlk (MSDP_FSMSDPPEERFILTERTABLE_POOLID);
            if (pMsdpFsMsdpPeerFilterEntry == NULL)
            {
                if (MsdpSetAllFsMsdpPeerFilterTableTrigger
                    (pMsdpSetFsMsdpPeerFilterEntry,
                     pMsdpIsSetFsMsdpPeerFilterEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpPeerFilterTable:MsdpSetAllFsMsdpPeerFilterTableTrigger function fails\r\n"));

                }
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpPeerFilterTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                    (UINT1 *) pMsdpOldFsMsdpPeerFilterEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                    (UINT1 *) pMsdpTrgFsMsdpPeerFilterEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pMsdpTrgIsSetFsMsdpPeerFilterEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pMsdpFsMsdpPeerFilterEntry, 0,
                    sizeof (tMsdpFsMsdpPeerFilterEntry));
            if ((MsdpInitializeFsMsdpPeerFilterTable
                 (pMsdpFsMsdpPeerFilterEntry)) == OSIX_FAILURE)
            {
                if (MsdpSetAllFsMsdpPeerFilterTableTrigger
                    (pMsdpSetFsMsdpPeerFilterEntry,
                     pMsdpIsSetFsMsdpPeerFilterEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpPeerFilterTable:MsdpSetAllFsMsdpPeerFilterTableTrigger function fails\r\n"));

                }
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpPeerFilterTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                    (UINT1 *) pMsdpFsMsdpPeerFilterEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                    (UINT1 *) pMsdpOldFsMsdpPeerFilterEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                    (UINT1 *) pMsdpTrgFsMsdpPeerFilterEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pMsdpTrgIsSetFsMsdpPeerFilterEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterAddrType !=
                OSIX_FALSE)
            {
                pMsdpFsMsdpPeerFilterEntry->MibObject.
                    i4FsMsdpPeerFilterAddrType =
                    pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                    i4FsMsdpPeerFilterAddrType;
            }

            if (pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterRouteMap !=
                OSIX_FALSE)
            {
                MEMCPY (pMsdpFsMsdpPeerFilterEntry->MibObject.
                        au1FsMsdpPeerFilterRouteMap,
                        pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                        au1FsMsdpPeerFilterRouteMap,
                        pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                        i4FsMsdpPeerFilterRouteMapLen);

                pMsdpFsMsdpPeerFilterEntry->MibObject.
                    i4FsMsdpPeerFilterRouteMapLen =
                    pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                    i4FsMsdpPeerFilterRouteMapLen;
            }

            if (pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterStatus !=
                OSIX_FALSE)
            {
                pMsdpFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterStatus =
                    pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                    i4FsMsdpPeerFilterStatus;
            }

            if ((pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                 i4FsMsdpPeerFilterStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                        i4FsMsdpPeerFilterStatus == ACTIVE)))
            {
                pMsdpFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterStatus =
                    ACTIVE;
            }
            else if (pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                     i4FsMsdpPeerFilterStatus == CREATE_AND_WAIT)
            {
                pMsdpFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterStatus =
                    NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gMsdpGlobals.MsdpGlbMib.FsMsdpPeerFilterTable,
                 (tRBElem *) pMsdpFsMsdpPeerFilterEntry) != RB_SUCCESS)
            {
                if (MsdpSetAllFsMsdpPeerFilterTableTrigger
                    (pMsdpSetFsMsdpPeerFilterEntry,
                     pMsdpIsSetFsMsdpPeerFilterEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpPeerFilterTable: MsdpSetAllFsMsdpPeerFilterTableTrigger function returns failure.\r\n"));
                }
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpPeerFilterTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                    (UINT1 *) pMsdpFsMsdpPeerFilterEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                    (UINT1 *) pMsdpOldFsMsdpPeerFilterEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                    (UINT1 *) pMsdpTrgFsMsdpPeerFilterEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pMsdpTrgIsSetFsMsdpPeerFilterEntry);
                return OSIX_FAILURE;
            }
            if (MsdpUtilUpdateFsMsdpPeerFilterTable
                (NULL, pMsdpFsMsdpPeerFilterEntry,
                 pMsdpIsSetFsMsdpPeerFilterEntry) != OSIX_SUCCESS)
            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpPeerFilterTable: MsdpUtilUpdateFsMsdpPeerFilterTable function returns failure.\r\n"));

                if (MsdpSetAllFsMsdpPeerFilterTableTrigger
                    (pMsdpSetFsMsdpPeerFilterEntry,
                     pMsdpIsSetFsMsdpPeerFilterEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpPeerFilterTable: MsdpSetAllFsMsdpPeerFilterTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gMsdpGlobals.MsdpGlbMib.FsMsdpPeerFilterTable,
                           pMsdpFsMsdpPeerFilterEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                    (UINT1 *) pMsdpFsMsdpPeerFilterEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                    (UINT1 *) pMsdpOldFsMsdpPeerFilterEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                    (UINT1 *) pMsdpTrgFsMsdpPeerFilterEntry);
                MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pMsdpTrgIsSetFsMsdpPeerFilterEntry);
                return OSIX_FAILURE;
            }

            if ((pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                 i4FsMsdpPeerFilterStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                        i4FsMsdpPeerFilterStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pMsdpTrgFsMsdpPeerFilterEntry->MibObject.
                    i4FsMsdpPeerFilterAddrType =
                    pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                    i4FsMsdpPeerFilterAddrType;
                pMsdpTrgFsMsdpPeerFilterEntry->MibObject.
                    i4FsMsdpPeerFilterStatus = CREATE_AND_WAIT;
                pMsdpTrgIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterStatus =
                    OSIX_TRUE;

                if (MsdpSetAllFsMsdpPeerFilterTableTrigger
                    (pMsdpTrgFsMsdpPeerFilterEntry,
                     pMsdpTrgIsSetFsMsdpPeerFilterEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpPeerFilterTable: MsdpSetAllFsMsdpPeerFilterTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                        (UINT1 *) pMsdpFsMsdpPeerFilterEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                        (UINT1 *)
                                        pMsdpOldFsMsdpPeerFilterEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                        (UINT1 *)
                                        pMsdpTrgFsMsdpPeerFilterEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pMsdpTrgIsSetFsMsdpPeerFilterEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                     i4FsMsdpPeerFilterStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pMsdpTrgFsMsdpPeerFilterEntry->MibObject.
                    i4FsMsdpPeerFilterStatus = CREATE_AND_WAIT;
                pMsdpTrgIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterStatus =
                    OSIX_TRUE;

                if (MsdpSetAllFsMsdpPeerFilterTableTrigger
                    (pMsdpTrgFsMsdpPeerFilterEntry,
                     pMsdpTrgIsSetFsMsdpPeerFilterEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpPeerFilterTable: MsdpSetAllFsMsdpPeerFilterTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                        (UINT1 *) pMsdpFsMsdpPeerFilterEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                        (UINT1 *)
                                        pMsdpOldFsMsdpPeerFilterEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                        (UINT1 *)
                                        pMsdpTrgFsMsdpPeerFilterEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pMsdpTrgIsSetFsMsdpPeerFilterEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                i4FsMsdpPeerFilterStatus == CREATE_AND_GO)
            {
                pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                    i4FsMsdpPeerFilterStatus = ACTIVE;
            }

            if (MsdpSetAllFsMsdpPeerFilterTableTrigger
                (pMsdpSetFsMsdpPeerFilterEntry, pMsdpIsSetFsMsdpPeerFilterEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpPeerFilterTable:  MsdpSetAllFsMsdpPeerFilterTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpPeerFilterEntry);
            MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpPeerFilterEntry);
            MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_ISSET_POOLID,
                                (UINT1 *) pMsdpTrgIsSetFsMsdpPeerFilterEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (MsdpSetAllFsMsdpPeerFilterTableTrigger
                (pMsdpSetFsMsdpPeerFilterEntry, pMsdpIsSetFsMsdpPeerFilterEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpPeerFilterTable: MsdpSetAllFsMsdpPeerFilterTableTrigger function returns failure.\r\n"));
            }
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpPeerFilterTable: Failure.\r\n"));
            MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpPeerFilterEntry);
            MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpPeerFilterEntry);
            MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_ISSET_POOLID,
                                (UINT1 *) pMsdpTrgIsSetFsMsdpPeerFilterEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pMsdpSetFsMsdpPeerFilterEntry->MibObject.
              i4FsMsdpPeerFilterStatus == CREATE_AND_WAIT)
             || (pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                 i4FsMsdpPeerFilterStatus == CREATE_AND_GO))
    {
        if (MsdpSetAllFsMsdpPeerFilterTableTrigger
            (pMsdpSetFsMsdpPeerFilterEntry, pMsdpIsSetFsMsdpPeerFilterEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpPeerFilterTable: MsdpSetAllFsMsdpPeerFilterTableTrigger function returns failure.\r\n"));
        }
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpSetAllFsMsdpPeerFilterTable: The row is already present.\r\n"));
        MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpPeerFilterEntry);
        MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpPeerFilterEntry);
        MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpPeerFilterEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pMsdpOldFsMsdpPeerFilterEntry, pMsdpFsMsdpPeerFilterEntry,
            sizeof (tMsdpFsMsdpPeerFilterEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pMsdpSetFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterStatus ==
        DESTROY)
    {
        pMsdpFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterStatus =
            DESTROY;

        if (MsdpUtilUpdateFsMsdpPeerFilterTable (pMsdpOldFsMsdpPeerFilterEntry,
                                                 pMsdpFsMsdpPeerFilterEntry,
                                                 pMsdpIsSetFsMsdpPeerFilterEntry)
            != OSIX_SUCCESS)
        {

            if (MsdpSetAllFsMsdpPeerFilterTableTrigger
                (pMsdpSetFsMsdpPeerFilterEntry, pMsdpIsSetFsMsdpPeerFilterEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpPeerFilterTable: MsdpSetAllFsMsdpPeerFilterTableTrigger function returns failure.\r\n"));
            }
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpPeerFilterTable: MsdpUtilUpdateFsMsdpPeerFilterTable function returns failure.\r\n"));
        }
        RBTreeRem (gMsdpGlobals.MsdpGlbMib.FsMsdpPeerFilterTable,
                   pMsdpFsMsdpPeerFilterEntry);
        if (MsdpSetAllFsMsdpPeerFilterTableTrigger
            (pMsdpSetFsMsdpPeerFilterEntry, pMsdpIsSetFsMsdpPeerFilterEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpPeerFilterTable: MsdpSetAllFsMsdpPeerFilterTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpPeerFilterEntry);
            MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpPeerFilterEntry);
            MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_ISSET_POOLID,
                                (UINT1 *) pMsdpTrgIsSetFsMsdpPeerFilterEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                            (UINT1 *) pMsdpFsMsdpPeerFilterEntry);
        MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpPeerFilterEntry);
        MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpPeerFilterEntry);
        MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpPeerFilterEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsMsdpPeerFilterTableFilterInputs
        (pMsdpFsMsdpPeerFilterEntry, pMsdpSetFsMsdpPeerFilterEntry,
         pMsdpIsSetFsMsdpPeerFilterEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpPeerFilterEntry);
        MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpPeerFilterEntry);
        MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpPeerFilterEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pMsdpFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterStatus ==
         ACTIVE)
        && (pMsdpSetFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterStatus !=
            NOT_IN_SERVICE))
    {
        pMsdpFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pMsdpTrgFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterStatus =
            NOT_IN_SERVICE;
        pMsdpTrgIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterStatus = OSIX_TRUE;

        if (MsdpUtilUpdateFsMsdpPeerFilterTable (pMsdpOldFsMsdpPeerFilterEntry,
                                                 pMsdpFsMsdpPeerFilterEntry,
                                                 pMsdpIsSetFsMsdpPeerFilterEntry)
            != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pMsdpFsMsdpPeerFilterEntry, pMsdpOldFsMsdpPeerFilterEntry,
                    sizeof (tMsdpFsMsdpPeerFilterEntry));
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpPeerFilterTable:                 MsdpUtilUpdateFsMsdpPeerFilterTable Function returns failure.\r\n"));

            if (MsdpSetAllFsMsdpPeerFilterTableTrigger
                (pMsdpSetFsMsdpPeerFilterEntry, pMsdpIsSetFsMsdpPeerFilterEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpPeerFilterTable: MsdpSetAllFsMsdpPeerFilterTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpPeerFilterEntry);
            MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpPeerFilterEntry);
            MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_ISSET_POOLID,
                                (UINT1 *) pMsdpTrgIsSetFsMsdpPeerFilterEntry);
            return OSIX_FAILURE;
        }

        if (MsdpSetAllFsMsdpPeerFilterTableTrigger
            (pMsdpTrgFsMsdpPeerFilterEntry, pMsdpTrgIsSetFsMsdpPeerFilterEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpPeerFilterTable: MsdpSetAllFsMsdpPeerFilterTableTrigger function returns failure.\r\n"));
        }
    }

    if (pMsdpSetFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterStatus ==
        ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterRouteMap !=
        OSIX_FALSE)
    {
        MEMCPY (pMsdpFsMsdpPeerFilterEntry->MibObject.
                au1FsMsdpPeerFilterRouteMap,
                pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                au1FsMsdpPeerFilterRouteMap,
                pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                i4FsMsdpPeerFilterRouteMapLen);

        pMsdpFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterRouteMapLen =
            pMsdpSetFsMsdpPeerFilterEntry->MibObject.
            i4FsMsdpPeerFilterRouteMapLen;
    }
    if (pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterStatus != OSIX_FALSE)
    {
        pMsdpFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterStatus =
            pMsdpSetFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pMsdpFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterStatus = ACTIVE;
    }

    if (MsdpUtilUpdateFsMsdpPeerFilterTable (pMsdpOldFsMsdpPeerFilterEntry,
                                             pMsdpFsMsdpPeerFilterEntry,
                                             pMsdpIsSetFsMsdpPeerFilterEntry) !=
        OSIX_SUCCESS)
    {

        if (MsdpSetAllFsMsdpPeerFilterTableTrigger
            (pMsdpSetFsMsdpPeerFilterEntry, pMsdpIsSetFsMsdpPeerFilterEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpPeerFilterTable: MsdpSetAllFsMsdpPeerFilterTableTrigger function returns failure.\r\n"));

        }
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpSetAllFsMsdpPeerFilterTable: MsdpUtilUpdateFsMsdpPeerFilterTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pMsdpFsMsdpPeerFilterEntry, pMsdpOldFsMsdpPeerFilterEntry,
                sizeof (tMsdpFsMsdpPeerFilterEntry));
        MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpPeerFilterEntry);
        MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpPeerFilterEntry);
        MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpPeerFilterEntry);
        return OSIX_FAILURE;

    }
    if (MsdpSetAllFsMsdpPeerFilterTableTrigger (pMsdpSetFsMsdpPeerFilterEntry,
                                                pMsdpIsSetFsMsdpPeerFilterEntry,
                                                SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpSetAllFsMsdpPeerFilterTable: MsdpSetAllFsMsdpPeerFilterTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                        (UINT1 *) pMsdpOldFsMsdpPeerFilterEntry);
    MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                        (UINT1 *) pMsdpTrgFsMsdpPeerFilterEntry);
    MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_ISSET_POOLID,
                        (UINT1 *) pMsdpTrgIsSetFsMsdpPeerFilterEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  MsdpSetAllFsMsdpSARedistributionTable
 Input       :  pMsdpSetFsMsdpSARedistributionEntry
                pMsdpIsSetFsMsdpSARedistributionEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpSetAllFsMsdpSARedistributionTable (tMsdpFsMsdpSARedistributionEntry *
                                       pMsdpSetFsMsdpSARedistributionEntry,
                                       tMsdpIsSetFsMsdpSARedistributionEntry *
                                       pMsdpIsSetFsMsdpSARedistributionEntry,
                                       INT4 i4RowStatusLogic,
                                       INT4 i4RowCreateOption)
{
    tMsdpFsMsdpSARedistributionEntry *pMsdpFsMsdpSARedistributionEntry = NULL;
    tMsdpFsMsdpSARedistributionEntry *pMsdpOldFsMsdpSARedistributionEntry =
        NULL;
    tMsdpFsMsdpSARedistributionEntry *pMsdpTrgFsMsdpSARedistributionEntry =
        NULL;
    tMsdpIsSetFsMsdpSARedistributionEntry
        * pMsdpTrgIsSetFsMsdpSARedistributionEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pMsdpOldFsMsdpSARedistributionEntry =
        (tMsdpFsMsdpSARedistributionEntry *)
        MemAllocMemBlk (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID);
    if (pMsdpOldFsMsdpSARedistributionEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pMsdpTrgFsMsdpSARedistributionEntry =
        (tMsdpFsMsdpSARedistributionEntry *)
        MemAllocMemBlk (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID);
    if (pMsdpTrgFsMsdpSARedistributionEntry == NULL)
    {
        MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpSARedistributionEntry);
        return OSIX_FAILURE;
    }
    pMsdpTrgIsSetFsMsdpSARedistributionEntry =
        (tMsdpIsSetFsMsdpSARedistributionEntry *)
        MemAllocMemBlk (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_ISSET_POOLID);
    if (pMsdpTrgIsSetFsMsdpSARedistributionEntry == NULL)
    {
        MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpSARedistributionEntry);
        MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpSARedistributionEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pMsdpOldFsMsdpSARedistributionEntry, 0,
            sizeof (tMsdpFsMsdpSARedistributionEntry));
    MEMSET (pMsdpTrgFsMsdpSARedistributionEntry, 0,
            sizeof (tMsdpFsMsdpSARedistributionEntry));
    MEMSET (pMsdpTrgIsSetFsMsdpSARedistributionEntry, 0,
            sizeof (tMsdpIsSetFsMsdpSARedistributionEntry));

    /* Check whether the node is already present */
    pMsdpFsMsdpSARedistributionEntry =
        RBTreeGet (gMsdpGlobals.MsdpGlbMib.FsMsdpSARedistributionTable,
                   (tRBElem *) pMsdpSetFsMsdpSARedistributionEntry);

    if (pMsdpFsMsdpSARedistributionEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pMsdpSetFsMsdpSARedistributionEntry->MibObject.
             i4FsMsdpSARedistributionStatus == CREATE_AND_WAIT)
            || (pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                i4FsMsdpSARedistributionStatus == CREATE_AND_GO)
            ||
            ((pMsdpSetFsMsdpSARedistributionEntry->MibObject.
              i4FsMsdpSARedistributionStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pMsdpFsMsdpSARedistributionEntry =
                (tMsdpFsMsdpSARedistributionEntry *)
                MemAllocMemBlk (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID);
            if (pMsdpFsMsdpSARedistributionEntry == NULL)
            {
                if (MsdpSetAllFsMsdpSARedistributionTableTrigger
                    (pMsdpSetFsMsdpSARedistributionEntry,
                     pMsdpIsSetFsMsdpSARedistributionEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpSARedistributionTable:MsdpSetAllFsMsdpSARedistributionTableTrigger function fails\r\n"));

                }
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpSARedistributionTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                    (UINT1 *)
                                    pMsdpOldFsMsdpSARedistributionEntry);
                MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                    (UINT1 *)
                                    pMsdpTrgFsMsdpSARedistributionEntry);
                MemReleaseMemBlock
                    (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_ISSET_POOLID,
                     (UINT1 *) pMsdpTrgIsSetFsMsdpSARedistributionEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pMsdpFsMsdpSARedistributionEntry, 0,
                    sizeof (tMsdpFsMsdpSARedistributionEntry));
            if ((MsdpInitializeFsMsdpSARedistributionTable
                 (pMsdpFsMsdpSARedistributionEntry)) == OSIX_FAILURE)
            {
                if (MsdpSetAllFsMsdpSARedistributionTableTrigger
                    (pMsdpSetFsMsdpSARedistributionEntry,
                     pMsdpIsSetFsMsdpSARedistributionEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpSARedistributionTable:MsdpSetAllFsMsdpSARedistributionTableTrigger function fails\r\n"));

                }
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpSARedistributionTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                    (UINT1 *) pMsdpFsMsdpSARedistributionEntry);
                MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                    (UINT1 *)
                                    pMsdpOldFsMsdpSARedistributionEntry);
                MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                    (UINT1 *)
                                    pMsdpTrgFsMsdpSARedistributionEntry);
                MemReleaseMemBlock
                    (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_ISSET_POOLID,
                     (UINT1 *) pMsdpTrgIsSetFsMsdpSARedistributionEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pMsdpIsSetFsMsdpSARedistributionEntry->
                bFsMsdpSARedistributionAddrType != OSIX_FALSE)
            {
                pMsdpFsMsdpSARedistributionEntry->MibObject.
                    i4FsMsdpSARedistributionAddrType =
                    pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                    i4FsMsdpSARedistributionAddrType;
            }

            if (pMsdpIsSetFsMsdpSARedistributionEntry->
                bFsMsdpSARedistributionStatus != OSIX_FALSE)
            {
                pMsdpFsMsdpSARedistributionEntry->MibObject.
                    i4FsMsdpSARedistributionStatus =
                    pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                    i4FsMsdpSARedistributionStatus;
            }

            if (pMsdpIsSetFsMsdpSARedistributionEntry->
                bFsMsdpSARedistributionRouteMap != OSIX_FALSE)
            {
                MEMCPY (pMsdpFsMsdpSARedistributionEntry->MibObject.
                        au1FsMsdpSARedistributionRouteMap,
                        pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                        au1FsMsdpSARedistributionRouteMap,
                        pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                        i4FsMsdpSARedistributionRouteMapLen);

                pMsdpFsMsdpSARedistributionEntry->MibObject.
                    i4FsMsdpSARedistributionRouteMapLen =
                    pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                    i4FsMsdpSARedistributionRouteMapLen;
            }

            if (pMsdpIsSetFsMsdpSARedistributionEntry->
                bFsMsdpSARedistributionRouteMapStat != OSIX_FALSE)
            {
                pMsdpFsMsdpSARedistributionEntry->MibObject.
                    i4FsMsdpSARedistributionRouteMapStat =
                    pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                    i4FsMsdpSARedistributionRouteMapStat;
            }

            if ((pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                 i4FsMsdpSARedistributionStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                        i4FsMsdpSARedistributionStatus == ACTIVE)))
            {
                pMsdpFsMsdpSARedistributionEntry->MibObject.
                    i4FsMsdpSARedistributionStatus = ACTIVE;
            }
            else if (pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                     i4FsMsdpSARedistributionStatus == CREATE_AND_WAIT)
            {
                pMsdpFsMsdpSARedistributionEntry->MibObject.
                    i4FsMsdpSARedistributionStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gMsdpGlobals.MsdpGlbMib.FsMsdpSARedistributionTable,
                 (tRBElem *) pMsdpFsMsdpSARedistributionEntry) != RB_SUCCESS)
            {
                if (MsdpSetAllFsMsdpSARedistributionTableTrigger
                    (pMsdpSetFsMsdpSARedistributionEntry,
                     pMsdpIsSetFsMsdpSARedistributionEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpSARedistributionTable: MsdpSetAllFsMsdpSARedistributionTableTrigger function returns failure.\r\n"));
                }
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpSARedistributionTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                    (UINT1 *) pMsdpFsMsdpSARedistributionEntry);
                MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                    (UINT1 *)
                                    pMsdpOldFsMsdpSARedistributionEntry);
                MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                    (UINT1 *)
                                    pMsdpTrgFsMsdpSARedistributionEntry);
                MemReleaseMemBlock
                    (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_ISSET_POOLID,
                     (UINT1 *) pMsdpTrgIsSetFsMsdpSARedistributionEntry);
                return OSIX_FAILURE;
            }
            if (MsdpUtilUpdateFsMsdpSARedistributionTable
                (NULL, pMsdpFsMsdpSARedistributionEntry,
                 pMsdpIsSetFsMsdpSARedistributionEntry) != OSIX_SUCCESS)
            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpSARedistributionTable: MsdpUtilUpdateFsMsdpSARedistributionTable function returns failure.\r\n"));

                if (MsdpSetAllFsMsdpSARedistributionTableTrigger
                    (pMsdpSetFsMsdpSARedistributionEntry,
                     pMsdpIsSetFsMsdpSARedistributionEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpSARedistributionTable: MsdpSetAllFsMsdpSARedistributionTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gMsdpGlobals.MsdpGlbMib.FsMsdpSARedistributionTable,
                           pMsdpFsMsdpSARedistributionEntry);
                MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                    (UINT1 *) pMsdpFsMsdpSARedistributionEntry);
                MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                    (UINT1 *)
                                    pMsdpOldFsMsdpSARedistributionEntry);
                MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                    (UINT1 *)
                                    pMsdpTrgFsMsdpSARedistributionEntry);
                MemReleaseMemBlock
                    (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_ISSET_POOLID,
                     (UINT1 *) pMsdpTrgIsSetFsMsdpSARedistributionEntry);
                return OSIX_FAILURE;
            }

            if ((pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                 i4FsMsdpSARedistributionStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                        i4FsMsdpSARedistributionStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pMsdpTrgFsMsdpSARedistributionEntry->MibObject.
                    i4FsMsdpSARedistributionAddrType =
                    pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                    i4FsMsdpSARedistributionAddrType;
                pMsdpTrgFsMsdpSARedistributionEntry->MibObject.
                    i4FsMsdpSARedistributionStatus = CREATE_AND_WAIT;
                pMsdpTrgIsSetFsMsdpSARedistributionEntry->
                    bFsMsdpSARedistributionStatus = OSIX_TRUE;

                if (MsdpSetAllFsMsdpSARedistributionTableTrigger
                    (pMsdpTrgFsMsdpSARedistributionEntry,
                     pMsdpTrgIsSetFsMsdpSARedistributionEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpSARedistributionTable: MsdpSetAllFsMsdpSARedistributionTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                        (UINT1 *)
                                        pMsdpFsMsdpSARedistributionEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                        (UINT1 *)
                                        pMsdpOldFsMsdpSARedistributionEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                        (UINT1 *)
                                        pMsdpTrgFsMsdpSARedistributionEntry);
                    MemReleaseMemBlock
                        (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_ISSET_POOLID,
                         (UINT1 *) pMsdpTrgIsSetFsMsdpSARedistributionEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                     i4FsMsdpSARedistributionStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pMsdpTrgFsMsdpSARedistributionEntry->MibObject.
                    i4FsMsdpSARedistributionStatus = CREATE_AND_WAIT;
                pMsdpTrgIsSetFsMsdpSARedistributionEntry->
                    bFsMsdpSARedistributionStatus = OSIX_TRUE;

                if (MsdpSetAllFsMsdpSARedistributionTableTrigger
                    (pMsdpTrgFsMsdpSARedistributionEntry,
                     pMsdpTrgIsSetFsMsdpSARedistributionEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    MSDP_TRC ((MSDP_UTIL_TRC,
                               "MsdpSetAllFsMsdpSARedistributionTable: MsdpSetAllFsMsdpSARedistributionTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                        (UINT1 *)
                                        pMsdpFsMsdpSARedistributionEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                        (UINT1 *)
                                        pMsdpOldFsMsdpSARedistributionEntry);
                    MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                        (UINT1 *)
                                        pMsdpTrgFsMsdpSARedistributionEntry);
                    MemReleaseMemBlock
                        (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_ISSET_POOLID,
                         (UINT1 *) pMsdpTrgIsSetFsMsdpSARedistributionEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                i4FsMsdpSARedistributionStatus == CREATE_AND_GO)
            {
                pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                    i4FsMsdpSARedistributionStatus = ACTIVE;
            }

            if (MsdpSetAllFsMsdpSARedistributionTableTrigger
                (pMsdpSetFsMsdpSARedistributionEntry,
                 pMsdpIsSetFsMsdpSARedistributionEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpSARedistributionTable:  MsdpSetAllFsMsdpSARedistributionTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpSARedistributionEntry);
            MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpSARedistributionEntry);
            MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pMsdpTrgIsSetFsMsdpSARedistributionEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (MsdpSetAllFsMsdpSARedistributionTableTrigger
                (pMsdpSetFsMsdpSARedistributionEntry,
                 pMsdpIsSetFsMsdpSARedistributionEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpSARedistributionTable: MsdpSetAllFsMsdpSARedistributionTableTrigger function returns failure.\r\n"));
            }
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpSARedistributionTable: Failure.\r\n"));
            MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpSARedistributionEntry);
            MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpSARedistributionEntry);
            MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pMsdpTrgIsSetFsMsdpSARedistributionEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pMsdpSetFsMsdpSARedistributionEntry->MibObject.
              i4FsMsdpSARedistributionStatus == CREATE_AND_WAIT)
             || (pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                 i4FsMsdpSARedistributionStatus == CREATE_AND_GO))
    {
        if (MsdpSetAllFsMsdpSARedistributionTableTrigger
            (pMsdpSetFsMsdpSARedistributionEntry,
             pMsdpIsSetFsMsdpSARedistributionEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpSARedistributionTable: MsdpSetAllFsMsdpSARedistributionTableTrigger function returns failure.\r\n"));
        }
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpSetAllFsMsdpSARedistributionTable: The row is already present.\r\n"));
        MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpSARedistributionEntry);
        MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpSARedistributionEntry);
        MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpSARedistributionEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pMsdpOldFsMsdpSARedistributionEntry,
            pMsdpFsMsdpSARedistributionEntry,
            sizeof (tMsdpFsMsdpSARedistributionEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pMsdpSetFsMsdpSARedistributionEntry->MibObject.
        i4FsMsdpSARedistributionStatus == DESTROY)
    {
        pMsdpFsMsdpSARedistributionEntry->MibObject.
            i4FsMsdpSARedistributionStatus = DESTROY;

        if (MsdpUtilUpdateFsMsdpSARedistributionTable
            (pMsdpOldFsMsdpSARedistributionEntry,
             pMsdpFsMsdpSARedistributionEntry,
             pMsdpIsSetFsMsdpSARedistributionEntry) != OSIX_SUCCESS)
        {

            if (MsdpSetAllFsMsdpSARedistributionTableTrigger
                (pMsdpSetFsMsdpSARedistributionEntry,
                 pMsdpIsSetFsMsdpSARedistributionEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpSARedistributionTable: MsdpSetAllFsMsdpSARedistributionTableTrigger function returns failure.\r\n"));
            }
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpSARedistributionTable: MsdpUtilUpdateFsMsdpSARedistributionTable function returns failure.\r\n"));
        }
        RBTreeRem (gMsdpGlobals.MsdpGlbMib.FsMsdpSARedistributionTable,
                   pMsdpFsMsdpSARedistributionEntry);
        if (MsdpSetAllFsMsdpSARedistributionTableTrigger
            (pMsdpSetFsMsdpSARedistributionEntry,
             pMsdpIsSetFsMsdpSARedistributionEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpSARedistributionTable: MsdpSetAllFsMsdpSARedistributionTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpSARedistributionEntry);
            MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpSARedistributionEntry);
            MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pMsdpTrgIsSetFsMsdpSARedistributionEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                            (UINT1 *) pMsdpFsMsdpSARedistributionEntry);
        MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpSARedistributionEntry);
        MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpSARedistributionEntry);
        MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpSARedistributionEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsMsdpSARedistributionTableFilterInputs
        (pMsdpFsMsdpSARedistributionEntry, pMsdpSetFsMsdpSARedistributionEntry,
         pMsdpIsSetFsMsdpSARedistributionEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpSARedistributionEntry);
        MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpSARedistributionEntry);
        MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpSARedistributionEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pMsdpFsMsdpSARedistributionEntry->MibObject.
         i4FsMsdpSARedistributionStatus == ACTIVE)
        && (pMsdpSetFsMsdpSARedistributionEntry->MibObject.
            i4FsMsdpSARedistributionStatus != NOT_IN_SERVICE))
    {
        pMsdpFsMsdpSARedistributionEntry->MibObject.
            i4FsMsdpSARedistributionStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pMsdpTrgFsMsdpSARedistributionEntry->MibObject.
            i4FsMsdpSARedistributionStatus = NOT_IN_SERVICE;
        pMsdpTrgIsSetFsMsdpSARedistributionEntry->
            bFsMsdpSARedistributionStatus = OSIX_TRUE;

        if (MsdpUtilUpdateFsMsdpSARedistributionTable
            (pMsdpOldFsMsdpSARedistributionEntry,
             pMsdpFsMsdpSARedistributionEntry,
             pMsdpIsSetFsMsdpSARedistributionEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pMsdpFsMsdpSARedistributionEntry,
                    pMsdpOldFsMsdpSARedistributionEntry,
                    sizeof (tMsdpFsMsdpSARedistributionEntry));
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpSARedistributionTable:                 MsdpUtilUpdateFsMsdpSARedistributionTable Function returns failure.\r\n"));

            if (MsdpSetAllFsMsdpSARedistributionTableTrigger
                (pMsdpSetFsMsdpSARedistributionEntry,
                 pMsdpIsSetFsMsdpSARedistributionEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                MSDP_TRC ((MSDP_UTIL_TRC,
                           "MsdpSetAllFsMsdpSARedistributionTable: MsdpSetAllFsMsdpSARedistributionTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                (UINT1 *) pMsdpOldFsMsdpSARedistributionEntry);
            MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                (UINT1 *) pMsdpTrgFsMsdpSARedistributionEntry);
            MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_ISSET_POOLID,
                                (UINT1 *)
                                pMsdpTrgIsSetFsMsdpSARedistributionEntry);
            return OSIX_FAILURE;
        }

        if (MsdpSetAllFsMsdpSARedistributionTableTrigger
            (pMsdpTrgFsMsdpSARedistributionEntry,
             pMsdpTrgIsSetFsMsdpSARedistributionEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpSARedistributionTable: MsdpSetAllFsMsdpSARedistributionTableTrigger function returns failure.\r\n"));
        }
    }

    if (pMsdpSetFsMsdpSARedistributionEntry->MibObject.
        i4FsMsdpSARedistributionStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pMsdpIsSetFsMsdpSARedistributionEntry->bFsMsdpSARedistributionStatus !=
        OSIX_FALSE)
    {
        pMsdpFsMsdpSARedistributionEntry->MibObject.
            i4FsMsdpSARedistributionStatus =
            pMsdpSetFsMsdpSARedistributionEntry->MibObject.
            i4FsMsdpSARedistributionStatus;
    }
    if (pMsdpIsSetFsMsdpSARedistributionEntry->
        bFsMsdpSARedistributionRouteMap != OSIX_FALSE)
    {
        MEMCPY (pMsdpFsMsdpSARedistributionEntry->MibObject.
                au1FsMsdpSARedistributionRouteMap,
                pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                au1FsMsdpSARedistributionRouteMap,
                pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                i4FsMsdpSARedistributionRouteMapLen);

        pMsdpFsMsdpSARedistributionEntry->MibObject.
            i4FsMsdpSARedistributionRouteMapLen =
            pMsdpSetFsMsdpSARedistributionEntry->MibObject.
            i4FsMsdpSARedistributionRouteMapLen;
    }
    if (pMsdpIsSetFsMsdpSARedistributionEntry->
        bFsMsdpSARedistributionRouteMapStat != OSIX_FALSE)
    {
        pMsdpFsMsdpSARedistributionEntry->MibObject.
            i4FsMsdpSARedistributionRouteMapStat =
            pMsdpSetFsMsdpSARedistributionEntry->MibObject.
            i4FsMsdpSARedistributionRouteMapStat;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pMsdpFsMsdpSARedistributionEntry->MibObject.
            i4FsMsdpSARedistributionStatus = ACTIVE;
    }

    if (MsdpUtilUpdateFsMsdpSARedistributionTable
        (pMsdpOldFsMsdpSARedistributionEntry, pMsdpFsMsdpSARedistributionEntry,
         pMsdpIsSetFsMsdpSARedistributionEntry) != OSIX_SUCCESS)
    {

        if (MsdpSetAllFsMsdpSARedistributionTableTrigger
            (pMsdpSetFsMsdpSARedistributionEntry,
             pMsdpIsSetFsMsdpSARedistributionEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpSetAllFsMsdpSARedistributionTable: MsdpSetAllFsMsdpSARedistributionTableTrigger function returns failure.\r\n"));

        }
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpSetAllFsMsdpSARedistributionTable: MsdpUtilUpdateFsMsdpSARedistributionTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pMsdpFsMsdpSARedistributionEntry,
                pMsdpOldFsMsdpSARedistributionEntry,
                sizeof (tMsdpFsMsdpSARedistributionEntry));
        MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                            (UINT1 *) pMsdpOldFsMsdpSARedistributionEntry);
        MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                            (UINT1 *) pMsdpTrgFsMsdpSARedistributionEntry);
        MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_ISSET_POOLID,
                            (UINT1 *) pMsdpTrgIsSetFsMsdpSARedistributionEntry);
        return OSIX_FAILURE;

    }
    if (MsdpSetAllFsMsdpSARedistributionTableTrigger
        (pMsdpSetFsMsdpSARedistributionEntry,
         pMsdpIsSetFsMsdpSARedistributionEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpSetAllFsMsdpSARedistributionTable: MsdpSetAllFsMsdpSARedistributionTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                        (UINT1 *) pMsdpOldFsMsdpSARedistributionEntry);
    MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                        (UINT1 *) pMsdpTrgFsMsdpSARedistributionEntry);
    MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_ISSET_POOLID,
                        (UINT1 *) pMsdpTrgIsSetFsMsdpSARedistributionEntry);
    return OSIX_SUCCESS;

}
