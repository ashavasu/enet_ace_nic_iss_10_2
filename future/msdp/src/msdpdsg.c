/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: msdpdsg.c,v 1.4 2012/03/30 13:54:34 siva Exp $
*
* Description: This file contains the routines for DataStructure access for the module Msdp 
*********************************************************************/

#include "msdpinc.h"

/****************************************************************************
 Function    :  MsdpGetFsMsdpTraceLevel
 Input       :  The Indices
 Input       :  pi4FsMsdpTraceLevel
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetFsMsdpTraceLevel (INT4 *pi4FsMsdpTraceLevel)
{
    *pi4FsMsdpTraceLevel = gMsdpGlobals.MsdpGlbMib.i4FsMsdpTraceLevel;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpSetFsMsdpTraceLevel
 Input       :  i4FsMsdpTraceLevel
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
MsdpSetFsMsdpTraceLevel (INT4 i4FsMsdpTraceLevel)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gMsdpGlobals.MsdpGlbMib.i4FsMsdpTraceLevel = i4FsMsdpTraceLevel;

    nmhSetCmnNew (FsMsdpTraceLevel, 11, MsdpMainTaskLock, MsdpMainTaskUnLock, 0,
                  0, 0, i4SetOption, "%i",
                  gMsdpGlobals.MsdpGlbMib.i4FsMsdpTraceLevel);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpGetFsMsdpIPv4AdminStat
 Input       :  The Indices
 Input       :  pi4FsMsdpIPv4AdminStat
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetFsMsdpIPv4AdminStat (INT4 *pi4FsMsdpIPv4AdminStat)
{
    *pi4FsMsdpIPv4AdminStat = gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv4AdminStat;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpSetFsMsdpIPv4AdminStat
 Input       :  i4FsMsdpIPv4AdminStat
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
MsdpSetFsMsdpIPv4AdminStat (INT4 i4FsMsdpIPv4AdminStat)
{
    tMsdpConfigAdmin    MsdpConfigAdmin;
    INT4                i4MsdpCurAdminStat = 0;
    INT4                i4FsMsdpIPv6AdminStat = 0;
    INT4                i4RetVal = 0;
    INT4                i4SetOption = SNMP_SUCCESS;

    MEMSET (&MsdpConfigAdmin, 0, sizeof (tMsdpConfigAdmin));

    MsdpGetFsMsdpIPv4AdminStat (&i4MsdpCurAdminStat);
    MsdpGetFsMsdpIPv6AdminStat (&i4FsMsdpIPv6AdminStat);

    if (i4FsMsdpIPv4AdminStat == i4MsdpCurAdminStat)
    {
        return OSIX_SUCCESS;
    }
    MsdpConfigAdmin.i4MsdpAdminStatus = i4FsMsdpIPv4AdminStat;
    MsdpConfigAdmin.u1AddrType = IPVX_ADDR_FMLY_IPV4;

    i4RetVal = MsdpMainHdlMsdpAdminStatusChg (&MsdpConfigAdmin);

    if ((i4RetVal == OSIX_SUCCESS) && (i4FsMsdpIPv6AdminStat != MSDP_ENABLED))
    {
        MsdpTmrStartTmr (&gMsdpGlobals.MsdpSaAdvtTimer,
                         MSDP_ACTIVE_SRC_ADVT_TMR, MSDP_SA_ADVT_TIME);
    }

    nmhSetCmnNew (FsMsdpIPv4AdminStat, 11, MsdpMainTaskLock, MsdpMainTaskUnLock,
                  0, 0, 0, i4SetOption, "%i",
                  gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv4AdminStat);

    return i4RetVal;
}

/****************************************************************************
 Function    :  MsdpGetFsMsdpIPv6AdminStat
 Input       :  The Indices
 Input       :  pi4FsMsdpIPv6AdminStat
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetFsMsdpIPv6AdminStat (INT4 *pi4FsMsdpIPv6AdminStat)
{
    *pi4FsMsdpIPv6AdminStat = gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv6AdminStat;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpSetFsMsdpIPv6AdminStat
 Input       :  i4FsMsdpIPv6AdminStat
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
MsdpSetFsMsdpIPv6AdminStat (INT4 i4FsMsdpIPv6AdminStat)
{
    tMsdpConfigAdmin    MsdpConfigAdmin;
    INT4                i4RetVal = 0;
    INT4                i4MsdpCurAdminStat = 0;
    INT4                i4FsMsdpIPv4AdminStat = 0;
    INT4                i4SetOption = SNMP_SUCCESS;

    MEMSET (&MsdpConfigAdmin, 0, sizeof (tMsdpConfigAdmin));

    MsdpGetFsMsdpIPv4AdminStat (&i4FsMsdpIPv4AdminStat);
    MsdpGetFsMsdpIPv6AdminStat (&i4MsdpCurAdminStat);
    if (i4FsMsdpIPv6AdminStat == i4MsdpCurAdminStat)
    {
        return OSIX_SUCCESS;
    }
    MsdpConfigAdmin.i4MsdpAdminStatus = i4FsMsdpIPv6AdminStat;
    MsdpConfigAdmin.u1AddrType = IPVX_ADDR_FMLY_IPV6;

    i4RetVal = MsdpMainHdlMsdpAdminStatusChg (&MsdpConfigAdmin);

    if ((i4RetVal == OSIX_SUCCESS) && (i4FsMsdpIPv4AdminStat != MSDP_ENABLED))
    {
        MsdpTmrStartTmr (&gMsdpGlobals.MsdpSaAdvtTimer,
                         MSDP_ACTIVE_SRC_ADVT_TMR, MSDP_SA_ADVT_TIME);
    }

    nmhSetCmnNew (FsMsdpIPv6AdminStat, 11, MsdpMainTaskLock, MsdpMainTaskUnLock,
                  0, 0, 0, i4SetOption, "%i",
                  gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv6AdminStat);

    return i4RetVal;
}

/****************************************************************************
 Function    :  MsdpGetFsMsdpCacheLifetime
 Input       :  The Indices
 Input       :  pu4FsMsdpCacheLifetime
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetFsMsdpCacheLifetime (UINT4 *pu4FsMsdpCacheLifetime)
{
    *pu4FsMsdpCacheLifetime = gMsdpGlobals.MsdpGlbMib.u4FsMsdpCacheLifetime;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpSetFsMsdpCacheLifetime
 Input       :  u4FsMsdpCacheLifetime
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
MsdpSetFsMsdpCacheLifetime (UINT4 u4FsMsdpCacheLifetime)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gMsdpGlobals.MsdpGlbMib.u4FsMsdpCacheLifetime = u4FsMsdpCacheLifetime;

    nmhSetCmnNew (FsMsdpCacheLifetime, 11, MsdpMainTaskLock, MsdpMainTaskUnLock,
                  0, 0, 0, i4SetOption, "%t",
                  gMsdpGlobals.MsdpGlbMib.u4FsMsdpCacheLifetime);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpGetFsMsdpNumSACacheEntries
 Input       :  The Indices
 Input       :  pu4FsMsdpNumSACacheEntries
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetFsMsdpNumSACacheEntries (UINT4 *pu4FsMsdpNumSACacheEntries)
{
    *pu4FsMsdpNumSACacheEntries =
        gMsdpGlobals.MsdpGlbMib.u4FsMsdpNumSACacheEntries;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpGetFsMsdpMaxPeerSessions
 Input       :  The Indices
 Input       :  pi4FsMsdpMaxPeerSessions
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetFsMsdpMaxPeerSessions (INT4 *pi4FsMsdpMaxPeerSessions)
{
    *pi4FsMsdpMaxPeerSessions = gMsdpGlobals.MsdpGlbMib.i4FsMsdpMaxPeerSessions;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpSetFsMsdpMaxPeerSessions
 Input       :  i4FsMsdpMaxPeerSessions
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
MsdpSetFsMsdpMaxPeerSessions (INT4 i4FsMsdpMaxPeerSessions)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gMsdpGlobals.MsdpGlbMib.i4FsMsdpMaxPeerSessions = i4FsMsdpMaxPeerSessions;

    nmhSetCmnNew (FsMsdpMaxPeerSessions, 11, MsdpMainTaskLock,
                  MsdpMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gMsdpGlobals.MsdpGlbMib.i4FsMsdpMaxPeerSessions);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpGetFsMsdpMappingComponentId
 Input       :  The Indices
 Input       :  pi4FsMsdpMappingComponentId
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetFsMsdpMappingComponentId (INT4 *pi4FsMsdpMappingComponentId)
{
    *pi4FsMsdpMappingComponentId =
        gMsdpGlobals.MsdpGlbMib.i4FsMsdpMappingComponentId;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpSetFsMsdpMappingComponentId
 Input       :  i4FsMsdpMappingComponentId
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
MsdpSetFsMsdpMappingComponentId (INT4 i4FsMsdpMappingComponentId)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gMsdpGlobals.MsdpGlbMib.i4FsMsdpMappingComponentId =
        i4FsMsdpMappingComponentId;

    nmhSetCmnNew (FsMsdpMappingComponentId, 11, MsdpMainTaskLock,
                  MsdpMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gMsdpGlobals.MsdpGlbMib.i4FsMsdpMappingComponentId);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpGetFsMsdpListenerPort
 Input       :  The Indices
 Input       :  pi4FsMsdpListenerPort
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetFsMsdpListenerPort (INT4 *pi4FsMsdpListenerPort)
{
    *pi4FsMsdpListenerPort = gMsdpGlobals.MsdpGlbMib.i4FsMsdpListenerPort;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpSetFsMsdpListenerPort
 Input       :  i4FsMsdpListenerPort
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
MsdpSetFsMsdpListenerPort (INT4 i4FsMsdpListenerPort)
{
    INT4                i4FsMsdpIPv4AdminStat = 0;
    INT4                i4FsMsdpIPv6AdminStat = 0;
    INT4                i4SetOption = SNMP_SUCCESS;

    MsdpGetFsMsdpIPv4AdminStat (&i4FsMsdpIPv4AdminStat);
    MsdpGetFsMsdpIPv6AdminStat (&i4FsMsdpIPv6AdminStat);

    if ((i4FsMsdpIPv4AdminStat == MSDP_ENABLED) ||
        (i4FsMsdpIPv6AdminStat == MSDP_ENABLED))
    {
        gMsdpGlobals.MsdpGlbMib.i4FsMsdpListenerPort = i4FsMsdpListenerPort;
    }

    if (i4FsMsdpIPv4AdminStat == MSDP_ENABLED)
    {
        MsdpSockServerStop (IPVX_ADDR_FMLY_IPV4);
        MsdpSockServerStart ((UINT2) i4FsMsdpListenerPort);
    }

    if (i4FsMsdpIPv6AdminStat == MSDP_ENABLED)
    {
        MsdpSockServerStop (IPVX_ADDR_FMLY_IPV6);
        MsdpSock6ServerStart ((UINT2) i4FsMsdpListenerPort);
    }

    nmhSetCmnNew (FsMsdpListenerPort, 11, MsdpMainTaskLock, MsdpMainTaskUnLock,
                  0, 0, 0, i4SetOption, "%i",
                  gMsdpGlobals.MsdpGlbMib.i4FsMsdpListenerPort);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpGetFsMsdpPeerFilter
 Input       :  The Indices
 Input       :  pi4FsMsdpPeerFilter
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetFsMsdpPeerFilter (INT4 *pi4FsMsdpPeerFilter)
{
    *pi4FsMsdpPeerFilter = gMsdpGlobals.MsdpGlbMib.i4FsMsdpPeerFilter;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpSetFsMsdpPeerFilter
 Input       :  i4FsMsdpPeerFilter
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
MsdpSetFsMsdpPeerFilter (INT4 i4FsMsdpPeerFilter)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gMsdpGlobals.MsdpGlbMib.i4FsMsdpPeerFilter = i4FsMsdpPeerFilter;

    nmhSetCmnNew (FsMsdpPeerFilter, 11, MsdpMainTaskLock, MsdpMainTaskUnLock, 0,
                  0, 0, i4SetOption, "%i",
                  gMsdpGlobals.MsdpGlbMib.i4FsMsdpPeerFilter);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpGetFsMsdpPeerCount
 Input       :  The Indices
 Input       :  pi4FsMsdpPeerCount
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetFsMsdpPeerCount (INT4 *pi4FsMsdpPeerCount)
{
    *pi4FsMsdpPeerCount = gMsdpGlobals.MsdpGlbMib.i4FsMsdpPeerCount;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpGetFsMsdpStatEstPeerCount
 Input       :  The Indices
 Input       :  pi4FsMsdpStatEstPeerCount
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetFsMsdpStatEstPeerCount (INT4 *pi4FsMsdpStatEstPeerCount)
{
    *pi4FsMsdpStatEstPeerCount =
        gMsdpGlobals.MsdpGlbMib.i4FsMsdpStatEstPeerCount;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpGetFirstFsMsdpPeerTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pMsdpFsMsdpPeerEntry or NULL
****************************************************************************/
tMsdpFsMsdpPeerEntry *
MsdpGetFirstFsMsdpPeerTable ()
{
    tMsdpFsMsdpPeerEntry *pMsdpFsMsdpPeerEntry = NULL;

    pMsdpFsMsdpPeerEntry =
        (tMsdpFsMsdpPeerEntry *) RBTreeGetFirst (gMsdpGlobals.MsdpGlbMib.
                                                 FsMsdpPeerTable);

    return pMsdpFsMsdpPeerEntry;
}

/****************************************************************************
 Function    :  MsdpGetNextFsMsdpPeerTable
 Input       :  pCurrentMsdpFsMsdpPeerEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextMsdpFsMsdpPeerEntry or NULL
****************************************************************************/
tMsdpFsMsdpPeerEntry *
MsdpGetNextFsMsdpPeerTable (tMsdpFsMsdpPeerEntry * pCurrentMsdpFsMsdpPeerEntry)
{
    tMsdpFsMsdpPeerEntry *pNextMsdpFsMsdpPeerEntry = NULL;

    pNextMsdpFsMsdpPeerEntry =
        (tMsdpFsMsdpPeerEntry *) RBTreeGetNext (gMsdpGlobals.MsdpGlbMib.
                                                FsMsdpPeerTable,
                                                (tRBElem *)
                                                pCurrentMsdpFsMsdpPeerEntry,
                                                NULL);

    return pNextMsdpFsMsdpPeerEntry;
}

/****************************************************************************
 Function    :  MsdpGetFsMsdpPeerTable
 Input       :  pMsdpFsMsdpPeerEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetMsdpFsMsdpPeerEntry or NULL
****************************************************************************/
tMsdpFsMsdpPeerEntry *
MsdpGetFsMsdpPeerTable (tMsdpFsMsdpPeerEntry * pMsdpFsMsdpPeerEntry)
{
    tMsdpFsMsdpPeerEntry *pGetMsdpFsMsdpPeerEntry = NULL;

    pGetMsdpFsMsdpPeerEntry =
        (tMsdpFsMsdpPeerEntry *) RBTreeGet (gMsdpGlobals.MsdpGlbMib.
                                            FsMsdpPeerTable,
                                            (tRBElem *) pMsdpFsMsdpPeerEntry);

    return pGetMsdpFsMsdpPeerEntry;
}

/****************************************************************************
 Function    :  MsdpGetFirstFsMsdpSACacheTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pMsdpFsMsdpSACacheEntry or NULL
****************************************************************************/
tMsdpFsMsdpSACacheEntry *
MsdpGetFirstFsMsdpSACacheTable ()
{
    tMsdpFsMsdpSACacheEntry *pMsdpFsMsdpSACacheEntry = NULL;

    pMsdpFsMsdpSACacheEntry =
        (tMsdpFsMsdpSACacheEntry *) RBTreeGetFirst (gMsdpGlobals.MsdpGlbMib.
                                                    FsMsdpSACacheTable);

    return pMsdpFsMsdpSACacheEntry;
}

/****************************************************************************
 Function    :  MsdpGetNextFsMsdpSACacheTable
 Input       :  pCurrentMsdpFsMsdpSACacheEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextMsdpFsMsdpSACacheEntry or NULL
****************************************************************************/
tMsdpFsMsdpSACacheEntry *
MsdpGetNextFsMsdpSACacheTable (tMsdpFsMsdpSACacheEntry *
                               pCurrentMsdpFsMsdpSACacheEntry)
{
    tMsdpFsMsdpSACacheEntry *pNextMsdpFsMsdpSACacheEntry = NULL;

    pNextMsdpFsMsdpSACacheEntry =
        (tMsdpFsMsdpSACacheEntry *) RBTreeGetNext (gMsdpGlobals.MsdpGlbMib.
                                                   FsMsdpSACacheTable,
                                                   (tRBElem *)
                                                   pCurrentMsdpFsMsdpSACacheEntry,
                                                   NULL);

    return pNextMsdpFsMsdpSACacheEntry;
}

/****************************************************************************
 Function    :  MsdpGetFsMsdpSACacheTable
 Input       :  pMsdpFsMsdpSACacheEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetMsdpFsMsdpSACacheEntry or NULL
****************************************************************************/
tMsdpFsMsdpSACacheEntry *
MsdpGetFsMsdpSACacheTable (tMsdpFsMsdpSACacheEntry * pMsdpFsMsdpSACacheEntry)
{
    tMsdpFsMsdpSACacheEntry *pGetMsdpFsMsdpSACacheEntry = NULL;

    pGetMsdpFsMsdpSACacheEntry =
        (tMsdpFsMsdpSACacheEntry *) RBTreeGet (gMsdpGlobals.MsdpGlbMib.
                                               FsMsdpSACacheTable,
                                               (tRBElem *)
                                               pMsdpFsMsdpSACacheEntry);

    return pGetMsdpFsMsdpSACacheEntry;
}

/****************************************************************************
 Function    :  MsdpGetFirstFsMsdpMeshGroupTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pMsdpFsMsdpMeshGroupEntry or NULL
****************************************************************************/
tMsdpFsMsdpMeshGroupEntry *
MsdpGetFirstFsMsdpMeshGroupTable ()
{
    tMsdpFsMsdpMeshGroupEntry *pMsdpFsMsdpMeshGroupEntry = NULL;

    pMsdpFsMsdpMeshGroupEntry =
        (tMsdpFsMsdpMeshGroupEntry *) RBTreeGetFirst (gMsdpGlobals.MsdpGlbMib.
                                                      FsMsdpMeshGroupTable);

    return pMsdpFsMsdpMeshGroupEntry;
}

/****************************************************************************
 Function    :  MsdpGetNextFsMsdpMeshGroupTable
 Input       :  pCurrentMsdpFsMsdpMeshGroupEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextMsdpFsMsdpMeshGroupEntry or NULL
****************************************************************************/
tMsdpFsMsdpMeshGroupEntry *
MsdpGetNextFsMsdpMeshGroupTable (tMsdpFsMsdpMeshGroupEntry *
                                 pCurrentMsdpFsMsdpMeshGroupEntry)
{
    tMsdpFsMsdpMeshGroupEntry *pNextMsdpFsMsdpMeshGroupEntry = NULL;

    pNextMsdpFsMsdpMeshGroupEntry =
        (tMsdpFsMsdpMeshGroupEntry *) RBTreeGetNext (gMsdpGlobals.MsdpGlbMib.
                                                     FsMsdpMeshGroupTable,
                                                     (tRBElem *)
                                                     pCurrentMsdpFsMsdpMeshGroupEntry,
                                                     NULL);

    return pNextMsdpFsMsdpMeshGroupEntry;
}

/****************************************************************************
 Function    :  MsdpGetFsMsdpMeshGroupTable
 Input       :  pMsdpFsMsdpMeshGroupEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetMsdpFsMsdpMeshGroupEntry or NULL
****************************************************************************/
tMsdpFsMsdpMeshGroupEntry *
MsdpGetFsMsdpMeshGroupTable (tMsdpFsMsdpMeshGroupEntry *
                             pMsdpFsMsdpMeshGroupEntry)
{
    tMsdpFsMsdpMeshGroupEntry *pGetMsdpFsMsdpMeshGroupEntry = NULL;

    pGetMsdpFsMsdpMeshGroupEntry =
        (tMsdpFsMsdpMeshGroupEntry *) RBTreeGet (gMsdpGlobals.MsdpGlbMib.
                                                 FsMsdpMeshGroupTable,
                                                 (tRBElem *)
                                                 pMsdpFsMsdpMeshGroupEntry);

    return pGetMsdpFsMsdpMeshGroupEntry;
}

/****************************************************************************
 Function    :  MsdpGetFirstFsMsdpRPTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pMsdpFsMsdpRPEntry or NULL
****************************************************************************/
tMsdpFsMsdpRPEntry *
MsdpGetFirstFsMsdpRPTable ()
{
    tMsdpFsMsdpRPEntry *pMsdpFsMsdpRPEntry = NULL;

    pMsdpFsMsdpRPEntry =
        (tMsdpFsMsdpRPEntry *) RBTreeGetFirst (gMsdpGlobals.MsdpGlbMib.
                                               FsMsdpRPTable);

    return pMsdpFsMsdpRPEntry;
}

/****************************************************************************
 Function    :  MsdpGetNextFsMsdpRPTable
 Input       :  pCurrentMsdpFsMsdpRPEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextMsdpFsMsdpRPEntry or NULL
****************************************************************************/
tMsdpFsMsdpRPEntry *
MsdpGetNextFsMsdpRPTable (tMsdpFsMsdpRPEntry * pCurrentMsdpFsMsdpRPEntry)
{
    tMsdpFsMsdpRPEntry *pNextMsdpFsMsdpRPEntry = NULL;

    pNextMsdpFsMsdpRPEntry =
        (tMsdpFsMsdpRPEntry *) RBTreeGetNext (gMsdpGlobals.MsdpGlbMib.
                                              FsMsdpRPTable,
                                              (tRBElem *)
                                              pCurrentMsdpFsMsdpRPEntry, NULL);

    return pNextMsdpFsMsdpRPEntry;
}

/****************************************************************************
 Function    :  MsdpGetFsMsdpRPTable
 Input       :  pMsdpFsMsdpRPEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetMsdpFsMsdpRPEntry or NULL
****************************************************************************/
tMsdpFsMsdpRPEntry *
MsdpGetFsMsdpRPTable (tMsdpFsMsdpRPEntry * pMsdpFsMsdpRPEntry)
{
    tMsdpFsMsdpRPEntry *pGetMsdpFsMsdpRPEntry = NULL;

    pGetMsdpFsMsdpRPEntry =
        (tMsdpFsMsdpRPEntry *) RBTreeGet (gMsdpGlobals.MsdpGlbMib.FsMsdpRPTable,
                                          (tRBElem *) pMsdpFsMsdpRPEntry);

    return pGetMsdpFsMsdpRPEntry;
}

/****************************************************************************
 Function    :  MsdpGetFirstFsMsdpPeerFilterTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pMsdpFsMsdpPeerFilterEntry or NULL
****************************************************************************/
tMsdpFsMsdpPeerFilterEntry *
MsdpGetFirstFsMsdpPeerFilterTable ()
{
    tMsdpFsMsdpPeerFilterEntry *pMsdpFsMsdpPeerFilterEntry = NULL;

    pMsdpFsMsdpPeerFilterEntry =
        (tMsdpFsMsdpPeerFilterEntry *) RBTreeGetFirst (gMsdpGlobals.MsdpGlbMib.
                                                       FsMsdpPeerFilterTable);

    return pMsdpFsMsdpPeerFilterEntry;
}

/****************************************************************************
 Function    :  MsdpGetNextFsMsdpPeerFilterTable
 Input       :  pCurrentMsdpFsMsdpPeerFilterEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextMsdpFsMsdpPeerFilterEntry or NULL
****************************************************************************/
tMsdpFsMsdpPeerFilterEntry *
MsdpGetNextFsMsdpPeerFilterTable (tMsdpFsMsdpPeerFilterEntry *
                                  pCurrentMsdpFsMsdpPeerFilterEntry)
{
    tMsdpFsMsdpPeerFilterEntry *pNextMsdpFsMsdpPeerFilterEntry = NULL;

    pNextMsdpFsMsdpPeerFilterEntry =
        (tMsdpFsMsdpPeerFilterEntry *) RBTreeGetNext (gMsdpGlobals.MsdpGlbMib.
                                                      FsMsdpPeerFilterTable,
                                                      (tRBElem *)
                                                      pCurrentMsdpFsMsdpPeerFilterEntry,
                                                      NULL);

    return pNextMsdpFsMsdpPeerFilterEntry;
}

/****************************************************************************
 Function    :  MsdpGetFsMsdpPeerFilterTable
 Input       :  pMsdpFsMsdpPeerFilterEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetMsdpFsMsdpPeerFilterEntry or NULL
****************************************************************************/
tMsdpFsMsdpPeerFilterEntry *
MsdpGetFsMsdpPeerFilterTable (tMsdpFsMsdpPeerFilterEntry *
                              pMsdpFsMsdpPeerFilterEntry)
{
    tMsdpFsMsdpPeerFilterEntry *pGetMsdpFsMsdpPeerFilterEntry = NULL;

    pGetMsdpFsMsdpPeerFilterEntry =
        (tMsdpFsMsdpPeerFilterEntry *) RBTreeGet (gMsdpGlobals.MsdpGlbMib.
                                                  FsMsdpPeerFilterTable,
                                                  (tRBElem *)
                                                  pMsdpFsMsdpPeerFilterEntry);

    return pGetMsdpFsMsdpPeerFilterEntry;
}

/****************************************************************************
 Function    :  MsdpGetFirstFsMsdpSARedistributionTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pMsdpFsMsdpSARedistributionEntry or NULL
****************************************************************************/
tMsdpFsMsdpSARedistributionEntry *
MsdpGetFirstFsMsdpSARedistributionTable ()
{
    tMsdpFsMsdpSARedistributionEntry *pMsdpFsMsdpSARedistributionEntry = NULL;

    pMsdpFsMsdpSARedistributionEntry =
        (tMsdpFsMsdpSARedistributionEntry *) RBTreeGetFirst (gMsdpGlobals.
                                                             MsdpGlbMib.
                                                             FsMsdpSARedistributionTable);

    return pMsdpFsMsdpSARedistributionEntry;
}

/****************************************************************************
 Function    :  MsdpGetNextFsMsdpSARedistributionTable
 Input       :  pCurrentMsdpFsMsdpSARedistributionEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextMsdpFsMsdpSARedistributionEntry or NULL
****************************************************************************/
tMsdpFsMsdpSARedistributionEntry *
MsdpGetNextFsMsdpSARedistributionTable (tMsdpFsMsdpSARedistributionEntry *
                                        pCurrentMsdpFsMsdpSARedistributionEntry)
{
    tMsdpFsMsdpSARedistributionEntry *pNextMsdpFsMsdpSARedistributionEntry =
        NULL;

    pNextMsdpFsMsdpSARedistributionEntry =
        (tMsdpFsMsdpSARedistributionEntry *) RBTreeGetNext (gMsdpGlobals.
                                                            MsdpGlbMib.
                                                            FsMsdpSARedistributionTable,
                                                            (tRBElem *)
                                                            pCurrentMsdpFsMsdpSARedistributionEntry,
                                                            NULL);

    return pNextMsdpFsMsdpSARedistributionEntry;
}

/****************************************************************************
 Function    :  MsdpGetFsMsdpSARedistributionTable
 Input       :  pMsdpFsMsdpSARedistributionEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetMsdpFsMsdpSARedistributionEntry or NULL
****************************************************************************/
tMsdpFsMsdpSARedistributionEntry *
MsdpGetFsMsdpSARedistributionTable (tMsdpFsMsdpSARedistributionEntry *
                                    pMsdpFsMsdpSARedistributionEntry)
{
    tMsdpFsMsdpSARedistributionEntry *pGetMsdpFsMsdpSARedistributionEntry =
        NULL;

    pGetMsdpFsMsdpSARedistributionEntry =
        (tMsdpFsMsdpSARedistributionEntry *) RBTreeGet (gMsdpGlobals.MsdpGlbMib.
                                                        FsMsdpSARedistributionTable,
                                                        (tRBElem *)
                                                        pMsdpFsMsdpSARedistributionEntry);

    return pGetMsdpFsMsdpSARedistributionEntry;
}

/****************************************************************************
 Function    :  MsdpGetFsMsdpRtrId
 Input       :  The Indices
 Input       :  pu4FsMsdpRtrId
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetFsMsdpRtrId (UINT4 *pu4FsMsdpRtrId)
{
    *pu4FsMsdpRtrId = gMsdpGlobals.MsdpGlbMib.u4FsMsdpRtrId;

    return OSIX_SUCCESS;
}
