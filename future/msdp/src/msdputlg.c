/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: msdputlg.c,v 1.4 2013/12/09 11:35:02 siva Exp $
*
* Description: This file contains utility functions used by protocol Msdp
*********************************************************************/

#include "msdpinc.h"

/****************************************************************************
 Function    :  MsdpUtlCreateRBTree
 Input       :  None
 Description :  This function creates all 
                the RBTree required.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
MsdpUtlCreateRBTree ()
{

    if (MsdpFsMsdpPeerTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (MsdpFsMsdpSACacheTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (MsdpFsMsdpMeshGroupTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (MsdpFsMsdpRPTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (MsdpFsMsdpPeerFilterTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (MsdpFsMsdpSARedistributionTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpFsMsdpPeerTableCreate
 Input       :  None
 Description :  This function creates the FsMsdpPeerTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
MsdpFsMsdpPeerTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tMsdpFsMsdpPeerEntry, MibObject.FsMsdpPeerTableNode);

    if ((gMsdpGlobals.MsdpGlbMib.FsMsdpPeerTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsMsdpPeerTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpFsMsdpSACacheTableCreate
 Input       :  None
 Description :  This function creates the FsMsdpSACacheTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
MsdpFsMsdpSACacheTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tMsdpFsMsdpSACacheEntry,
                       MibObject.FsMsdpSACacheTableNode);

    if ((gMsdpGlobals.MsdpGlbMib.FsMsdpSACacheTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsMsdpSACacheTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpFsMsdpMeshGroupTableCreate
 Input       :  None
 Description :  This function creates the FsMsdpMeshGroupTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
MsdpFsMsdpMeshGroupTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tMsdpFsMsdpMeshGroupEntry,
                       MibObject.FsMsdpMeshGroupTableNode);

    if ((gMsdpGlobals.MsdpGlbMib.FsMsdpMeshGroupTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsMsdpMeshGroupTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpFsMsdpRPTableCreate
 Input       :  None
 Description :  This function creates the FsMsdpRPTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
MsdpFsMsdpRPTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tMsdpFsMsdpRPEntry, MibObject.FsMsdpRPTableNode);

    if ((gMsdpGlobals.MsdpGlbMib.FsMsdpRPTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsMsdpRPTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpFsMsdpPeerFilterTableCreate
 Input       :  None
 Description :  This function creates the FsMsdpPeerFilterTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
MsdpFsMsdpPeerFilterTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tMsdpFsMsdpPeerFilterEntry,
                       MibObject.FsMsdpPeerFilterTableNode);

    if ((gMsdpGlobals.MsdpGlbMib.FsMsdpPeerFilterTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsMsdpPeerFilterTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpFsMsdpSARedistributionTableCreate
 Input       :  None
 Description :  This function creates the FsMsdpSARedistributionTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
MsdpFsMsdpSARedistributionTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tMsdpFsMsdpSARedistributionEntry,
                       MibObject.FsMsdpSARedistributionTableNode);

    if ((gMsdpGlobals.MsdpGlbMib.FsMsdpSARedistributionTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsMsdpSARedistributionTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FsMsdpPeerTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsMsdpPeerTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsMsdpPeerTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tMsdpFsMsdpPeerEntry *pFsMsdpPeerEntry1 = (tMsdpFsMsdpPeerEntry *) pRBElem1;
    tMsdpFsMsdpPeerEntry *pFsMsdpPeerEntry2 = (tMsdpFsMsdpPeerEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsMsdpPeerEntry1->MibObject.i4FsMsdpPeerAddrType >
        pFsMsdpPeerEntry2->MibObject.i4FsMsdpPeerAddrType)
    {
        return 1;
    }
    else if (pFsMsdpPeerEntry1->MibObject.i4FsMsdpPeerAddrType <
             pFsMsdpPeerEntry2->MibObject.i4FsMsdpPeerAddrType)
    {
        return -1;
    }

    i4MaxLength = 0;
    if (pFsMsdpPeerEntry1->MibObject.i4FsMsdpPeerRemoteAddressLen >
        pFsMsdpPeerEntry2->MibObject.i4FsMsdpPeerRemoteAddressLen)
    {
        i4MaxLength = pFsMsdpPeerEntry1->MibObject.i4FsMsdpPeerRemoteAddressLen;
    }
    else
    {
        i4MaxLength = pFsMsdpPeerEntry2->MibObject.i4FsMsdpPeerRemoteAddressLen;
    }

    if (MEMCMP
        (pFsMsdpPeerEntry1->MibObject.au1FsMsdpPeerRemoteAddress,
         pFsMsdpPeerEntry2->MibObject.au1FsMsdpPeerRemoteAddress,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsMsdpPeerEntry1->MibObject.au1FsMsdpPeerRemoteAddress,
              pFsMsdpPeerEntry2->MibObject.au1FsMsdpPeerRemoteAddress,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pFsMsdpPeerEntry1->MibObject.i4FsMsdpPeerRemoteAddressLen >
        pFsMsdpPeerEntry2->MibObject.i4FsMsdpPeerRemoteAddressLen)
    {
        return 1;
    }
    else if (pFsMsdpPeerEntry1->MibObject.i4FsMsdpPeerRemoteAddressLen <
             pFsMsdpPeerEntry2->MibObject.i4FsMsdpPeerRemoteAddressLen)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  FsMsdpSACacheTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsMsdpSACacheTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsMsdpSACacheTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tMsdpFsMsdpSACacheEntry *pFsMsdpSACacheEntry1 =
        (tMsdpFsMsdpSACacheEntry *) pRBElem1;
    tMsdpFsMsdpSACacheEntry *pFsMsdpSACacheEntry2 =
        (tMsdpFsMsdpSACacheEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsMsdpSACacheEntry1->MibObject.i4FsMsdpSACacheAddrType >
        pFsMsdpSACacheEntry2->MibObject.i4FsMsdpSACacheAddrType)
    {
        return 1;
    }
    else if (pFsMsdpSACacheEntry1->MibObject.i4FsMsdpSACacheAddrType <
             pFsMsdpSACacheEntry2->MibObject.i4FsMsdpSACacheAddrType)
    {
        return -1;
    }

    i4MaxLength = 0;
    if (pFsMsdpSACacheEntry1->MibObject.i4FsMsdpSACacheGroupAddrLen >
        pFsMsdpSACacheEntry2->MibObject.i4FsMsdpSACacheGroupAddrLen)
    {
        i4MaxLength =
            pFsMsdpSACacheEntry1->MibObject.i4FsMsdpSACacheGroupAddrLen;
    }
    else
    {
        i4MaxLength =
            pFsMsdpSACacheEntry2->MibObject.i4FsMsdpSACacheGroupAddrLen;
    }

    if (MEMCMP
        (pFsMsdpSACacheEntry1->MibObject.au1FsMsdpSACacheGroupAddr,
         pFsMsdpSACacheEntry2->MibObject.au1FsMsdpSACacheGroupAddr,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsMsdpSACacheEntry1->MibObject.au1FsMsdpSACacheGroupAddr,
              pFsMsdpSACacheEntry2->MibObject.au1FsMsdpSACacheGroupAddr,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pFsMsdpSACacheEntry1->MibObject.i4FsMsdpSACacheGroupAddrLen >
        pFsMsdpSACacheEntry2->MibObject.i4FsMsdpSACacheGroupAddrLen)
    {
        return 1;
    }
    else if (pFsMsdpSACacheEntry1->MibObject.i4FsMsdpSACacheGroupAddrLen <
             pFsMsdpSACacheEntry2->MibObject.i4FsMsdpSACacheGroupAddrLen)
    {
        return -1;
    }
    i4MaxLength = 0;
    if (pFsMsdpSACacheEntry1->MibObject.i4FsMsdpSACacheSourceAddrLen >
        pFsMsdpSACacheEntry2->MibObject.i4FsMsdpSACacheSourceAddrLen)
    {
        i4MaxLength =
            pFsMsdpSACacheEntry1->MibObject.i4FsMsdpSACacheSourceAddrLen;
    }
    else
    {
        i4MaxLength =
            pFsMsdpSACacheEntry2->MibObject.i4FsMsdpSACacheSourceAddrLen;
    }

    if (MEMCMP
        (pFsMsdpSACacheEntry1->MibObject.au1FsMsdpSACacheSourceAddr,
         pFsMsdpSACacheEntry2->MibObject.au1FsMsdpSACacheSourceAddr,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsMsdpSACacheEntry1->MibObject.au1FsMsdpSACacheSourceAddr,
              pFsMsdpSACacheEntry2->MibObject.au1FsMsdpSACacheSourceAddr,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pFsMsdpSACacheEntry1->MibObject.i4FsMsdpSACacheSourceAddrLen >
        pFsMsdpSACacheEntry2->MibObject.i4FsMsdpSACacheSourceAddrLen)
    {
        return 1;
    }
    else if (pFsMsdpSACacheEntry1->MibObject.i4FsMsdpSACacheSourceAddrLen <
             pFsMsdpSACacheEntry2->MibObject.i4FsMsdpSACacheSourceAddrLen)
    {
        return -1;
    }
    i4MaxLength = 0;
    if (pFsMsdpSACacheEntry1->MibObject.i4FsMsdpSACacheOriginRPLen >
        pFsMsdpSACacheEntry2->MibObject.i4FsMsdpSACacheOriginRPLen)
    {
        i4MaxLength =
            pFsMsdpSACacheEntry1->MibObject.i4FsMsdpSACacheOriginRPLen;
    }
    else
    {
        i4MaxLength =
            pFsMsdpSACacheEntry2->MibObject.i4FsMsdpSACacheOriginRPLen;
    }

    if (MEMCMP
        (pFsMsdpSACacheEntry1->MibObject.au1FsMsdpSACacheOriginRP,
         pFsMsdpSACacheEntry2->MibObject.au1FsMsdpSACacheOriginRP,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsMsdpSACacheEntry1->MibObject.au1FsMsdpSACacheOriginRP,
              pFsMsdpSACacheEntry2->MibObject.au1FsMsdpSACacheOriginRP,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pFsMsdpSACacheEntry1->MibObject.i4FsMsdpSACacheOriginRPLen >
        pFsMsdpSACacheEntry2->MibObject.i4FsMsdpSACacheOriginRPLen)
    {
        return 1;
    }
    else if (pFsMsdpSACacheEntry1->MibObject.i4FsMsdpSACacheOriginRPLen <
             pFsMsdpSACacheEntry2->MibObject.i4FsMsdpSACacheOriginRPLen)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  FsMsdpMeshGroupTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsMsdpMeshGroupTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsMsdpMeshGroupTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tMsdpFsMsdpMeshGroupEntry *pFsMsdpMeshGroupEntry1 =
        (tMsdpFsMsdpMeshGroupEntry *) pRBElem1;
    tMsdpFsMsdpMeshGroupEntry *pFsMsdpMeshGroupEntry2 =
        (tMsdpFsMsdpMeshGroupEntry *) pRBElem2;
    INT4                i4MaxLength = 0;
    i4MaxLength = 0;
    if (pFsMsdpMeshGroupEntry1->MibObject.i4FsMsdpMeshGroupNameLen >
        pFsMsdpMeshGroupEntry2->MibObject.i4FsMsdpMeshGroupNameLen)
    {
        i4MaxLength =
            pFsMsdpMeshGroupEntry1->MibObject.i4FsMsdpMeshGroupNameLen;
    }
    else
    {
        i4MaxLength =
            pFsMsdpMeshGroupEntry2->MibObject.i4FsMsdpMeshGroupNameLen;
    }

    if (MEMCMP
        (pFsMsdpMeshGroupEntry1->MibObject.au1FsMsdpMeshGroupName,
         pFsMsdpMeshGroupEntry2->MibObject.au1FsMsdpMeshGroupName,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsMsdpMeshGroupEntry1->MibObject.au1FsMsdpMeshGroupName,
              pFsMsdpMeshGroupEntry2->MibObject.au1FsMsdpMeshGroupName,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pFsMsdpMeshGroupEntry1->MibObject.i4FsMsdpMeshGroupNameLen >
        pFsMsdpMeshGroupEntry2->MibObject.i4FsMsdpMeshGroupNameLen)
    {
        return 1;
    }
    else if (pFsMsdpMeshGroupEntry1->MibObject.i4FsMsdpMeshGroupNameLen <
             pFsMsdpMeshGroupEntry2->MibObject.i4FsMsdpMeshGroupNameLen)
    {
        return -1;
    }

    if (pFsMsdpMeshGroupEntry1->MibObject.i4FsMsdpMeshGroupAddrType >
        pFsMsdpMeshGroupEntry2->MibObject.i4FsMsdpMeshGroupAddrType)
    {
        return 1;
    }
    else if (pFsMsdpMeshGroupEntry1->MibObject.i4FsMsdpMeshGroupAddrType <
             pFsMsdpMeshGroupEntry2->MibObject.i4FsMsdpMeshGroupAddrType)
    {
        return -1;
    }

    i4MaxLength = 0;
    if (pFsMsdpMeshGroupEntry1->MibObject.i4FsMsdpMeshGroupPeerAddressLen >
        pFsMsdpMeshGroupEntry2->MibObject.i4FsMsdpMeshGroupPeerAddressLen)
    {
        i4MaxLength =
            pFsMsdpMeshGroupEntry1->MibObject.i4FsMsdpMeshGroupPeerAddressLen;
    }
    else
    {
        i4MaxLength =
            pFsMsdpMeshGroupEntry2->MibObject.i4FsMsdpMeshGroupPeerAddressLen;
    }

    if (MEMCMP
        (pFsMsdpMeshGroupEntry1->MibObject.au1FsMsdpMeshGroupPeerAddress,
         pFsMsdpMeshGroupEntry2->MibObject.au1FsMsdpMeshGroupPeerAddress,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsMsdpMeshGroupEntry1->MibObject.au1FsMsdpMeshGroupPeerAddress,
              pFsMsdpMeshGroupEntry2->MibObject.au1FsMsdpMeshGroupPeerAddress,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pFsMsdpMeshGroupEntry1->MibObject.i4FsMsdpMeshGroupPeerAddressLen >
        pFsMsdpMeshGroupEntry2->MibObject.i4FsMsdpMeshGroupPeerAddressLen)
    {
        return 1;
    }
    else if (pFsMsdpMeshGroupEntry1->MibObject.i4FsMsdpMeshGroupPeerAddressLen <
             pFsMsdpMeshGroupEntry2->MibObject.i4FsMsdpMeshGroupPeerAddressLen)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  FsMsdpRPTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsMsdpRPTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsMsdpRPTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tMsdpFsMsdpRPEntry *pFsMsdpRPEntry1 = (tMsdpFsMsdpRPEntry *) pRBElem1;
    tMsdpFsMsdpRPEntry *pFsMsdpRPEntry2 = (tMsdpFsMsdpRPEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsMsdpRPEntry1->MibObject.i4FsMsdpRPAddrType >
        pFsMsdpRPEntry2->MibObject.i4FsMsdpRPAddrType)
    {
        return 1;
    }
    else if (pFsMsdpRPEntry1->MibObject.i4FsMsdpRPAddrType <
             pFsMsdpRPEntry2->MibObject.i4FsMsdpRPAddrType)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsMsdpPeerFilterTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsMsdpPeerFilterTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsMsdpPeerFilterTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tMsdpFsMsdpPeerFilterEntry *pFsMsdpPeerFilterEntry1 =
        (tMsdpFsMsdpPeerFilterEntry *) pRBElem1;
    tMsdpFsMsdpPeerFilterEntry *pFsMsdpPeerFilterEntry2 =
        (tMsdpFsMsdpPeerFilterEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsMsdpPeerFilterEntry1->MibObject.i4FsMsdpPeerFilterAddrType >
        pFsMsdpPeerFilterEntry2->MibObject.i4FsMsdpPeerFilterAddrType)
    {
        return 1;
    }
    else if (pFsMsdpPeerFilterEntry1->MibObject.i4FsMsdpPeerFilterAddrType <
             pFsMsdpPeerFilterEntry2->MibObject.i4FsMsdpPeerFilterAddrType)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsMsdpSARedistributionTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsMsdpSARedistributionTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsMsdpSARedistributionTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tMsdpFsMsdpSARedistributionEntry *pFsMsdpSARedistributionEntry1 =
        (tMsdpFsMsdpSARedistributionEntry *) pRBElem1;
    tMsdpFsMsdpSARedistributionEntry *pFsMsdpSARedistributionEntry2 =
        (tMsdpFsMsdpSARedistributionEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsMsdpSARedistributionEntry1->MibObject.
        i4FsMsdpSARedistributionAddrType >
        pFsMsdpSARedistributionEntry2->MibObject.
        i4FsMsdpSARedistributionAddrType)
    {
        return 1;
    }
    else if (pFsMsdpSARedistributionEntry1->MibObject.
             i4FsMsdpSARedistributionAddrType <
             pFsMsdpSARedistributionEntry2->MibObject.
             i4FsMsdpSARedistributionAddrType)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsMsdpPeerTableFilterInputs
 Input       :  The Indices
                pMsdpFsMsdpPeerEntry
                pMsdpSetFsMsdpPeerEntry
                pMsdpIsSetFsMsdpPeerEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsMsdpPeerTableFilterInputs (tMsdpFsMsdpPeerEntry * pMsdpFsMsdpPeerEntry,
                             tMsdpFsMsdpPeerEntry * pMsdpSetFsMsdpPeerEntry,
                             tMsdpIsSetFsMsdpPeerEntry *
                             pMsdpIsSetFsMsdpPeerEntry)
{
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerAddrType == OSIX_TRUE)
    {
        if (pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType ==
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType)
            pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerAddrType = OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerRemoteAddress == OSIX_TRUE)
    {
        if ((MEMCMP (pMsdpFsMsdpPeerEntry->MibObject.au1FsMsdpPeerRemoteAddress,
                     pMsdpSetFsMsdpPeerEntry->MibObject.
                     au1FsMsdpPeerRemoteAddress,
                     pMsdpSetFsMsdpPeerEntry->MibObject.
                     i4FsMsdpPeerRemoteAddressLen) == 0)
            && (pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerRemoteAddressLen ==
                pMsdpSetFsMsdpPeerEntry->MibObject.
                i4FsMsdpPeerRemoteAddressLen))
            pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerRemoteAddress = OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerLocalAddress == OSIX_TRUE)
    {
        if ((MEMCMP (pMsdpFsMsdpPeerEntry->MibObject.au1FsMsdpPeerLocalAddress,
                     pMsdpSetFsMsdpPeerEntry->MibObject.
                     au1FsMsdpPeerLocalAddress,
                     pMsdpSetFsMsdpPeerEntry->MibObject.
                     i4FsMsdpPeerLocalAddressLen) == 0)
            && (pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerLocalAddressLen ==
                pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerLocalAddressLen))
            pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerLocalAddress = OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerConnectRetryInterval == OSIX_TRUE)
    {
        if (pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerConnectRetryInterval ==
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerConnectRetryInterval)
            pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerConnectRetryInterval =
                OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerHoldTimeConfigured == OSIX_TRUE)
    {
        if (pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerHoldTimeConfigured ==
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerHoldTimeConfigured)
            pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerHoldTimeConfigured =
                OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerKeepAliveConfigured == OSIX_TRUE)
    {
        if (pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerKeepAliveConfigured ==
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerKeepAliveConfigured)
            pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerKeepAliveConfigured =
                OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerDataTtl == OSIX_TRUE)
    {
        if (pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerDataTtl ==
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerDataTtl)
            pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerDataTtl = OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerStatus == OSIX_TRUE)
    {
        if (pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus ==
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus)
            pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerStatus = OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerEncapsulationType == OSIX_TRUE)
    {
        if (pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerEncapsulationType ==
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerEncapsulationType)
            pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerEncapsulationType =
                OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerMD5AuthPassword == OSIX_TRUE)
    {
        if ((MEMCMP
             (pMsdpFsMsdpPeerEntry->MibObject.au1FsMsdpPeerMD5AuthPassword,
              pMsdpSetFsMsdpPeerEntry->MibObject.au1FsMsdpPeerMD5AuthPassword,
              pMsdpSetFsMsdpPeerEntry->MibObject.
              i4FsMsdpPeerMD5AuthPasswordLen) == 0)
            && (pMsdpFsMsdpPeerEntry->MibObject.
                i4FsMsdpPeerMD5AuthPasswordLen ==
                pMsdpSetFsMsdpPeerEntry->MibObject.
                i4FsMsdpPeerMD5AuthPasswordLen))
            pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerMD5AuthPassword = OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerMD5AuthPwdStat == OSIX_TRUE)
    {
        if (pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerMD5AuthPwdStat ==
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerMD5AuthPwdStat)
            pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerMD5AuthPwdStat = OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerAdminStatus == OSIX_TRUE)
    {
        if (pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAdminStatus ==
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAdminStatus)
            pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerAdminStatus = OSIX_FALSE;
    }
    if ((pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerAddrType == OSIX_FALSE)
        && (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerRemoteAddress == OSIX_FALSE)
        && (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerLocalAddress == OSIX_FALSE)
        && (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerConnectRetryInterval ==
            OSIX_FALSE)
        && (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerHoldTimeConfigured ==
            OSIX_FALSE)
        && (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerKeepAliveConfigured ==
            OSIX_FALSE)
        && (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerDataTtl == OSIX_FALSE)
        && (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerStatus == OSIX_FALSE)
        && (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerEncapsulationType ==
            OSIX_FALSE)
        && (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerMD5AuthPassword == OSIX_FALSE)
        && (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerMD5AuthPwdStat == OSIX_FALSE)
        && (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerAdminStatus == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsMsdpSACacheTableFilterInputs
 Input       :  The Indices
                pMsdpFsMsdpSACacheEntry
                pMsdpSetFsMsdpSACacheEntry
                pMsdpIsSetFsMsdpSACacheEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsMsdpSACacheTableFilterInputs (tMsdpFsMsdpSACacheEntry *
                                pMsdpFsMsdpSACacheEntry,
                                tMsdpFsMsdpSACacheEntry *
                                pMsdpSetFsMsdpSACacheEntry,
                                tMsdpIsSetFsMsdpSACacheEntry *
                                pMsdpIsSetFsMsdpSACacheEntry)
{
    if (pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheAddrType == OSIX_TRUE)
    {
        if (pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheAddrType ==
            pMsdpSetFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheAddrType)
            pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheAddrType = OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheGroupAddr == OSIX_TRUE)
    {
        if ((MEMCMP
             (pMsdpFsMsdpSACacheEntry->MibObject.au1FsMsdpSACacheGroupAddr,
              pMsdpSetFsMsdpSACacheEntry->MibObject.au1FsMsdpSACacheGroupAddr,
              pMsdpSetFsMsdpSACacheEntry->MibObject.
              i4FsMsdpSACacheGroupAddrLen) == 0)
            && (pMsdpFsMsdpSACacheEntry->MibObject.
                i4FsMsdpSACacheGroupAddrLen ==
                pMsdpSetFsMsdpSACacheEntry->MibObject.
                i4FsMsdpSACacheGroupAddrLen))
            pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheGroupAddr = OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheSourceAddr == OSIX_TRUE)
    {
        if ((MEMCMP
             (pMsdpFsMsdpSACacheEntry->MibObject.au1FsMsdpSACacheSourceAddr,
              pMsdpSetFsMsdpSACacheEntry->MibObject.au1FsMsdpSACacheSourceAddr,
              pMsdpSetFsMsdpSACacheEntry->MibObject.
              i4FsMsdpSACacheSourceAddrLen) == 0)
            && (pMsdpFsMsdpSACacheEntry->MibObject.
                i4FsMsdpSACacheSourceAddrLen ==
                pMsdpSetFsMsdpSACacheEntry->MibObject.
                i4FsMsdpSACacheSourceAddrLen))
            pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheSourceAddr = OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheOriginRP == OSIX_TRUE)
    {
        if ((MEMCMP
             (pMsdpFsMsdpSACacheEntry->MibObject.au1FsMsdpSACacheOriginRP,
              pMsdpSetFsMsdpSACacheEntry->MibObject.au1FsMsdpSACacheOriginRP,
              pMsdpSetFsMsdpSACacheEntry->MibObject.
              i4FsMsdpSACacheOriginRPLen) == 0)
            && (pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheOriginRPLen ==
                pMsdpSetFsMsdpSACacheEntry->MibObject.
                i4FsMsdpSACacheOriginRPLen))
            pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheOriginRP = OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheStatus == OSIX_TRUE)
    {
        if (pMsdpFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus ==
            pMsdpSetFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheStatus)
            pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheStatus = OSIX_FALSE;
    }
    if ((pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheAddrType == OSIX_FALSE)
        && (pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheGroupAddr == OSIX_FALSE)
        && (pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheSourceAddr ==
            OSIX_FALSE)
        && (pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheOriginRP == OSIX_FALSE)
        && (pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheStatus == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsMsdpMeshGroupTableFilterInputs
 Input       :  The Indices
                pMsdpFsMsdpMeshGroupEntry
                pMsdpSetFsMsdpMeshGroupEntry
                pMsdpIsSetFsMsdpMeshGroupEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsMsdpMeshGroupTableFilterInputs (tMsdpFsMsdpMeshGroupEntry *
                                  pMsdpFsMsdpMeshGroupEntry,
                                  tMsdpFsMsdpMeshGroupEntry *
                                  pMsdpSetFsMsdpMeshGroupEntry,
                                  tMsdpIsSetFsMsdpMeshGroupEntry *
                                  pMsdpIsSetFsMsdpMeshGroupEntry)
{
    if (pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupName == OSIX_TRUE)
    {
        if ((MEMCMP
             (pMsdpFsMsdpMeshGroupEntry->MibObject.au1FsMsdpMeshGroupName,
              pMsdpSetFsMsdpMeshGroupEntry->MibObject.au1FsMsdpMeshGroupName,
              pMsdpSetFsMsdpMeshGroupEntry->MibObject.
              i4FsMsdpMeshGroupNameLen) == 0)
            && (pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupNameLen ==
                pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                i4FsMsdpMeshGroupNameLen))
            pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupName = OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupAddrType == OSIX_TRUE)
    {
        if (pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupAddrType ==
            pMsdpSetFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupAddrType)
            pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupAddrType =
                OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupPeerAddress ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pMsdpFsMsdpMeshGroupEntry->MibObject.
              au1FsMsdpMeshGroupPeerAddress,
              pMsdpSetFsMsdpMeshGroupEntry->MibObject.
              au1FsMsdpMeshGroupPeerAddress,
              pMsdpSetFsMsdpMeshGroupEntry->MibObject.
              i4FsMsdpMeshGroupPeerAddressLen) == 0)
            && (pMsdpFsMsdpMeshGroupEntry->MibObject.
                i4FsMsdpMeshGroupPeerAddressLen ==
                pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                i4FsMsdpMeshGroupPeerAddressLen))
            pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupPeerAddress =
                OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupStatus == OSIX_TRUE)
    {
        if (pMsdpFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupStatus ==
            pMsdpSetFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupStatus)
            pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupStatus = OSIX_FALSE;
    }
    if ((pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupName == OSIX_FALSE)
        && (pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupAddrType ==
            OSIX_FALSE)
        && (pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupPeerAddress ==
            OSIX_FALSE)
        && (pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupStatus ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsMsdpRPTableFilterInputs
 Input       :  The Indices
                pMsdpFsMsdpRPEntry
                pMsdpSetFsMsdpRPEntry
                pMsdpIsSetFsMsdpRPEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsMsdpRPTableFilterInputs (tMsdpFsMsdpRPEntry * pMsdpFsMsdpRPEntry,
                           tMsdpFsMsdpRPEntry * pMsdpSetFsMsdpRPEntry,
                           tMsdpIsSetFsMsdpRPEntry * pMsdpIsSetFsMsdpRPEntry)
{
    if (pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPAddrType == OSIX_TRUE)
    {
        if (pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPAddrType ==
            pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPAddrType)
            pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPAddrType = OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPAddress == OSIX_TRUE)
    {
        if ((MEMCMP (pMsdpFsMsdpRPEntry->MibObject.au1FsMsdpRPAddress,
                     pMsdpSetFsMsdpRPEntry->MibObject.au1FsMsdpRPAddress,
                     pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPAddressLen) ==
             0)
            && (pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPAddressLen ==
                pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPAddressLen))
            pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPAddress = OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPStatus == OSIX_TRUE)
    {
        if (pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus ==
            pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus)
            pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPStatus = OSIX_FALSE;
    }
    if ((pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPAddrType == OSIX_FALSE)
        && (pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPAddress == OSIX_FALSE)
        && (pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPStatus == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsMsdpPeerFilterTableFilterInputs
 Input       :  The Indices
                pMsdpFsMsdpPeerFilterEntry
                pMsdpSetFsMsdpPeerFilterEntry
                pMsdpIsSetFsMsdpPeerFilterEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsMsdpPeerFilterTableFilterInputs (tMsdpFsMsdpPeerFilterEntry *
                                   pMsdpFsMsdpPeerFilterEntry,
                                   tMsdpFsMsdpPeerFilterEntry *
                                   pMsdpSetFsMsdpPeerFilterEntry,
                                   tMsdpIsSetFsMsdpPeerFilterEntry *
                                   pMsdpIsSetFsMsdpPeerFilterEntry)
{
    if (pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterAddrType == OSIX_TRUE)
    {
        if (pMsdpFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterAddrType ==
            pMsdpSetFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterAddrType)
            pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterAddrType =
                OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterRouteMap == OSIX_TRUE)
    {
        if ((MEMCMP
             (pMsdpFsMsdpPeerFilterEntry->MibObject.au1FsMsdpPeerFilterRouteMap,
              pMsdpSetFsMsdpPeerFilterEntry->MibObject.
              au1FsMsdpPeerFilterRouteMap,
              pMsdpSetFsMsdpPeerFilterEntry->MibObject.
              i4FsMsdpPeerFilterRouteMapLen) == 0)
            && (pMsdpFsMsdpPeerFilterEntry->MibObject.
                i4FsMsdpPeerFilterRouteMapLen ==
                pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                i4FsMsdpPeerFilterRouteMapLen))
            pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterRouteMap =
                OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterStatus == OSIX_TRUE)
    {
        if (pMsdpFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterStatus ==
            pMsdpSetFsMsdpPeerFilterEntry->MibObject.i4FsMsdpPeerFilterStatus)
            pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterStatus =
                OSIX_FALSE;
    }
    if ((pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterAddrType ==
         OSIX_FALSE)
        && (pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterRouteMap ==
            OSIX_FALSE)
        && (pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterStatus ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsMsdpSARedistributionTableFilterInputs
 Input       :  The Indices
                pMsdpFsMsdpSARedistributionEntry
                pMsdpSetFsMsdpSARedistributionEntry
                pMsdpIsSetFsMsdpSARedistributionEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsMsdpSARedistributionTableFilterInputs (tMsdpFsMsdpSARedistributionEntry *
                                         pMsdpFsMsdpSARedistributionEntry,
                                         tMsdpFsMsdpSARedistributionEntry *
                                         pMsdpSetFsMsdpSARedistributionEntry,
                                         tMsdpIsSetFsMsdpSARedistributionEntry *
                                         pMsdpIsSetFsMsdpSARedistributionEntry)
{
    if (pMsdpIsSetFsMsdpSARedistributionEntry->
        bFsMsdpSARedistributionAddrType == OSIX_TRUE)
    {
        if (pMsdpFsMsdpSARedistributionEntry->MibObject.
            i4FsMsdpSARedistributionAddrType ==
            pMsdpSetFsMsdpSARedistributionEntry->MibObject.
            i4FsMsdpSARedistributionAddrType)
            pMsdpIsSetFsMsdpSARedistributionEntry->
                bFsMsdpSARedistributionAddrType = OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpSARedistributionEntry->bFsMsdpSARedistributionStatus ==
        OSIX_TRUE)
    {
        if (pMsdpFsMsdpSARedistributionEntry->MibObject.
            i4FsMsdpSARedistributionStatus ==
            pMsdpSetFsMsdpSARedistributionEntry->MibObject.
            i4FsMsdpSARedistributionStatus)
            pMsdpIsSetFsMsdpSARedistributionEntry->
                bFsMsdpSARedistributionStatus = OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpSARedistributionEntry->
        bFsMsdpSARedistributionRouteMap == OSIX_TRUE)
    {
        if ((MEMCMP
             (pMsdpFsMsdpSARedistributionEntry->MibObject.
              au1FsMsdpSARedistributionRouteMap,
              pMsdpSetFsMsdpSARedistributionEntry->MibObject.
              au1FsMsdpSARedistributionRouteMap,
              pMsdpSetFsMsdpSARedistributionEntry->MibObject.
              i4FsMsdpSARedistributionRouteMapLen) == 0)
            && (pMsdpFsMsdpSARedistributionEntry->MibObject.
                i4FsMsdpSARedistributionRouteMapLen ==
                pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                i4FsMsdpSARedistributionRouteMapLen))
            pMsdpIsSetFsMsdpSARedistributionEntry->
                bFsMsdpSARedistributionRouteMap = OSIX_FALSE;
    }
    if (pMsdpIsSetFsMsdpSARedistributionEntry->
        bFsMsdpSARedistributionRouteMapStat == OSIX_TRUE)
    {
        if (pMsdpFsMsdpSARedistributionEntry->MibObject.
            i4FsMsdpSARedistributionRouteMapStat ==
            pMsdpSetFsMsdpSARedistributionEntry->MibObject.
            i4FsMsdpSARedistributionRouteMapStat)
            pMsdpIsSetFsMsdpSARedistributionEntry->
                bFsMsdpSARedistributionRouteMapStat = OSIX_FALSE;
    }
    if ((pMsdpIsSetFsMsdpSARedistributionEntry->
         bFsMsdpSARedistributionAddrType == OSIX_FALSE)
        && (pMsdpIsSetFsMsdpSARedistributionEntry->
            bFsMsdpSARedistributionStatus == OSIX_FALSE)
        && (pMsdpIsSetFsMsdpSARedistributionEntry->
            bFsMsdpSARedistributionRouteMap == OSIX_FALSE)
        && (pMsdpIsSetFsMsdpSARedistributionEntry->
            bFsMsdpSARedistributionRouteMapStat == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  MsdpSetAllFsMsdpPeerTableTrigger
 Input       :  The Indices
                pMsdpSetFsMsdpPeerEntry
                pMsdpIsSetFsMsdpPeerEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
MsdpSetAllFsMsdpPeerTableTrigger (tMsdpFsMsdpPeerEntry *
                                  pMsdpSetFsMsdpPeerEntry,
                                  tMsdpIsSetFsMsdpPeerEntry *
                                  pMsdpIsSetFsMsdpPeerEntry, INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsMsdpPeerRemoteAddressVal;
    tSNMP_OCTET_STRING_TYPE FsMsdpPeerLocalAddressVal;
    UINT1               au1FsMsdpPeerLocalAddressVal[256];
    tSNMP_OCTET_STRING_TYPE FsMsdpPeerMD5AuthPasswordVal;
    UINT1               au1FsMsdpPeerMD5AuthPasswordVal[256];

    FsMsdpPeerRemoteAddressVal.pu1_OctetList =
        pMsdpSetFsMsdpPeerEntry->MibObject.au1FsMsdpPeerRemoteAddress;
    FsMsdpPeerRemoteAddressVal.i4_Length =
        pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerRemoteAddressLen;

    MEMSET (au1FsMsdpPeerLocalAddressVal, 0,
            sizeof (au1FsMsdpPeerLocalAddressVal));
    FsMsdpPeerLocalAddressVal.pu1_OctetList = au1FsMsdpPeerLocalAddressVal;
    FsMsdpPeerLocalAddressVal.i4_Length = 0;

    MEMSET (au1FsMsdpPeerMD5AuthPasswordVal, 0,
            sizeof (au1FsMsdpPeerMD5AuthPasswordVal));
    FsMsdpPeerMD5AuthPasswordVal.pu1_OctetList =
        au1FsMsdpPeerMD5AuthPasswordVal;
    FsMsdpPeerMD5AuthPasswordVal.i4_Length = 0;

    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerAddrType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMsdpPeerAddrType, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 2, i4SetOption, "%i %s %i",
                      pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType,
                      &FsMsdpPeerRemoteAddressVal,
                      pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType);
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerRemoteAddress == OSIX_TRUE)
    {
        MEMCPY (FsMsdpPeerRemoteAddressVal.pu1_OctetList,
                pMsdpSetFsMsdpPeerEntry->MibObject.au1FsMsdpPeerRemoteAddress,
                pMsdpSetFsMsdpPeerEntry->MibObject.
                i4FsMsdpPeerRemoteAddressLen);
        FsMsdpPeerRemoteAddressVal.i4_Length =
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerRemoteAddressLen;

        nmhSetCmnNew (FsMsdpPeerRemoteAddress, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 2, i4SetOption, "%i %s %s",
                      pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType,
                      &FsMsdpPeerRemoteAddressVal, &FsMsdpPeerRemoteAddressVal);
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerLocalAddress == OSIX_TRUE)
    {
        MEMCPY (FsMsdpPeerLocalAddressVal.pu1_OctetList,
                pMsdpSetFsMsdpPeerEntry->MibObject.au1FsMsdpPeerLocalAddress,
                pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerLocalAddressLen);
        FsMsdpPeerLocalAddressVal.i4_Length =
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerLocalAddressLen;

        nmhSetCmnNew (FsMsdpPeerLocalAddress, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 2, i4SetOption, "%i %s %s",
                      pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType,
                      &FsMsdpPeerRemoteAddressVal, &FsMsdpPeerLocalAddressVal);
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerConnectRetryInterval == OSIX_TRUE)
    {
        MEMCPY (FsMsdpPeerRemoteAddressVal.pu1_OctetList,
                pMsdpSetFsMsdpPeerEntry->MibObject.au1FsMsdpPeerRemoteAddress,
                pMsdpSetFsMsdpPeerEntry->MibObject.
                i4FsMsdpPeerRemoteAddressLen);
        FsMsdpPeerRemoteAddressVal.i4_Length =
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerRemoteAddressLen;

        nmhSetCmnNew (FsMsdpPeerConnectRetryInterval, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 2, i4SetOption, "%i %s %i",
                      pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType,
                      &FsMsdpPeerRemoteAddressVal,
                      pMsdpSetFsMsdpPeerEntry->MibObject.
                      i4FsMsdpPeerConnectRetryInterval);
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerHoldTimeConfigured == OSIX_TRUE)
    {

        MEMCPY (FsMsdpPeerRemoteAddressVal.pu1_OctetList,
                pMsdpSetFsMsdpPeerEntry->MibObject.au1FsMsdpPeerRemoteAddress,
                pMsdpSetFsMsdpPeerEntry->MibObject.
                i4FsMsdpPeerRemoteAddressLen);
        FsMsdpPeerRemoteAddressVal.i4_Length =
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerRemoteAddressLen;

        nmhSetCmnNew (FsMsdpPeerHoldTimeConfigured, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 2, i4SetOption, "%i %s %i",
                      pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType,
                      &FsMsdpPeerRemoteAddressVal,
                      pMsdpSetFsMsdpPeerEntry->MibObject.
                      i4FsMsdpPeerHoldTimeConfigured);
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerKeepAliveConfigured == OSIX_TRUE)
    {

        MEMCPY (FsMsdpPeerRemoteAddressVal.pu1_OctetList,
                pMsdpSetFsMsdpPeerEntry->MibObject.au1FsMsdpPeerRemoteAddress,
                pMsdpSetFsMsdpPeerEntry->MibObject.
                i4FsMsdpPeerRemoteAddressLen);
        FsMsdpPeerRemoteAddressVal.i4_Length =
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerRemoteAddressLen;

        nmhSetCmnNew (FsMsdpPeerKeepAliveConfigured, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 2, i4SetOption, "%i %s %i",
                      pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType,
                      &FsMsdpPeerRemoteAddressVal,
                      pMsdpSetFsMsdpPeerEntry->MibObject.
                      i4FsMsdpPeerKeepAliveConfigured);
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerDataTtl == OSIX_TRUE)
    {

        MEMCPY (FsMsdpPeerRemoteAddressVal.pu1_OctetList,
                pMsdpSetFsMsdpPeerEntry->MibObject.au1FsMsdpPeerRemoteAddress,
                pMsdpSetFsMsdpPeerEntry->MibObject.
                i4FsMsdpPeerRemoteAddressLen);
        FsMsdpPeerRemoteAddressVal.i4_Length =
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerRemoteAddressLen;

        nmhSetCmnNew (FsMsdpPeerDataTtl, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 2, i4SetOption, "%i %s %i",
                      pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType,
                      &FsMsdpPeerRemoteAddressVal,
                      pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerDataTtl);
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMsdpPeerStatus, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 1, 2, i4SetOption, "%i %s %i",
                      pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType,
                      &FsMsdpPeerRemoteAddressVal,
                      pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus);
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerEncapsulationType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMsdpPeerEncapsulationType, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 2, i4SetOption, "%i %s %i",
                      pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType,
                      &FsMsdpPeerRemoteAddressVal,
                      pMsdpSetFsMsdpPeerEntry->MibObject.
                      i4FsMsdpPeerEncapsulationType);
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerMD5AuthPassword == OSIX_TRUE)
    {
        MEMCPY (FsMsdpPeerMD5AuthPasswordVal.pu1_OctetList,
                pMsdpSetFsMsdpPeerEntry->MibObject.au1FsMsdpPeerMD5AuthPassword,
                pMsdpSetFsMsdpPeerEntry->MibObject.
                i4FsMsdpPeerMD5AuthPasswordLen);
        FsMsdpPeerMD5AuthPasswordVal.i4_Length =
            pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerMD5AuthPasswordLen;

        nmhSetCmnNew (FsMsdpPeerMD5AuthPassword, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 2, i4SetOption, "%i %s %s",
                      pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType,
                      &FsMsdpPeerRemoteAddressVal,
                      &FsMsdpPeerMD5AuthPasswordVal);
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerMD5AuthPwdStat == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMsdpPeerMD5AuthPwdStat, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 2, i4SetOption, "%i %s %i",
                      pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType,
                      &FsMsdpPeerRemoteAddressVal,
                      pMsdpSetFsMsdpPeerEntry->MibObject.
                      i4FsMsdpPeerMD5AuthPwdStat);
    }
    if (pMsdpIsSetFsMsdpPeerEntry->bFsMsdpPeerAdminStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMsdpPeerAdminStatus, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 2,
                      i4SetOption, "%i %s %i",
                      pMsdpSetFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType,
                      &FsMsdpPeerRemoteAddressVal,
                      pMsdpSetFsMsdpPeerEntry->MibObject.
                      i4FsMsdpPeerAdminStatus);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpSetAllFsMsdpSACacheTableTrigger
 Input       :  The Indices
                pMsdpSetFsMsdpSACacheEntry
                pMsdpIsSetFsMsdpSACacheEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
MsdpSetAllFsMsdpSACacheTableTrigger (tMsdpFsMsdpSACacheEntry *
                                     pMsdpSetFsMsdpSACacheEntry,
                                     tMsdpIsSetFsMsdpSACacheEntry *
                                     pMsdpIsSetFsMsdpSACacheEntry,
                                     INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsMsdpSACacheGroupAddrVal;
    UINT1               au1FsMsdpSACacheGroupAddrVal[256];
    tSNMP_OCTET_STRING_TYPE FsMsdpSACacheSourceAddrVal;
    UINT1               au1FsMsdpSACacheSourceAddrVal[256];
    tSNMP_OCTET_STRING_TYPE FsMsdpSACacheOriginRPVal;
    UINT1               au1FsMsdpSACacheOriginRPVal[256];

    MEMSET (au1FsMsdpSACacheGroupAddrVal, 0,
            sizeof (au1FsMsdpSACacheGroupAddrVal));
    FsMsdpSACacheGroupAddrVal.pu1_OctetList = au1FsMsdpSACacheGroupAddrVal;
    FsMsdpSACacheGroupAddrVal.i4_Length = 0;

    MEMSET (au1FsMsdpSACacheSourceAddrVal, 0,
            sizeof (au1FsMsdpSACacheSourceAddrVal));
    FsMsdpSACacheSourceAddrVal.pu1_OctetList = au1FsMsdpSACacheSourceAddrVal;
    FsMsdpSACacheSourceAddrVal.i4_Length = 0;

    MEMSET (au1FsMsdpSACacheOriginRPVal, 0,
            sizeof (au1FsMsdpSACacheOriginRPVal));
    FsMsdpSACacheOriginRPVal.pu1_OctetList = au1FsMsdpSACacheOriginRPVal;
    FsMsdpSACacheOriginRPVal.i4_Length = 0;

    if (pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheAddrType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMsdpSACacheAddrType, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 4, i4SetOption,
                      "%i %s %s %s %i",
                      pMsdpSetFsMsdpSACacheEntry->MibObject.
                      i4FsMsdpSACacheAddrType, &FsMsdpSACacheGroupAddrVal,
                      &FsMsdpSACacheSourceAddrVal, &FsMsdpSACacheOriginRPVal,
                      pMsdpSetFsMsdpSACacheEntry->MibObject.
                      i4FsMsdpSACacheAddrType);
    }
    if (pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheGroupAddr == OSIX_TRUE)
    {
        MEMCPY (FsMsdpSACacheGroupAddrVal.pu1_OctetList,
                pMsdpSetFsMsdpSACacheEntry->MibObject.au1FsMsdpSACacheGroupAddr,
                pMsdpSetFsMsdpSACacheEntry->MibObject.
                i4FsMsdpSACacheGroupAddrLen);
        FsMsdpSACacheGroupAddrVal.i4_Length =
            pMsdpSetFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheGroupAddrLen;

        nmhSetCmnNew (FsMsdpSACacheGroupAddr, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 4, i4SetOption,
                      "%i %s %s %s %s",
                      pMsdpSetFsMsdpSACacheEntry->MibObject.
                      i4FsMsdpSACacheAddrType, &FsMsdpSACacheGroupAddrVal,
                      &FsMsdpSACacheSourceAddrVal, &FsMsdpSACacheOriginRPVal,
                      &FsMsdpSACacheGroupAddrVal);
    }
    if (pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheSourceAddr == OSIX_TRUE)
    {
        MEMCPY (FsMsdpSACacheSourceAddrVal.pu1_OctetList,
                pMsdpSetFsMsdpSACacheEntry->MibObject.
                au1FsMsdpSACacheSourceAddr,
                pMsdpSetFsMsdpSACacheEntry->MibObject.
                i4FsMsdpSACacheSourceAddrLen);
        FsMsdpSACacheSourceAddrVal.i4_Length =
            pMsdpSetFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheSourceAddrLen;

        nmhSetCmnNew (FsMsdpSACacheSourceAddr, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 4, i4SetOption,
                      "%i %s %s %s %s",
                      pMsdpSetFsMsdpSACacheEntry->MibObject.
                      i4FsMsdpSACacheAddrType, &FsMsdpSACacheGroupAddrVal,
                      &FsMsdpSACacheSourceAddrVal, &FsMsdpSACacheOriginRPVal,
                      &FsMsdpSACacheSourceAddrVal);
    }
    if (pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheOriginRP == OSIX_TRUE)
    {
        MEMCPY (FsMsdpSACacheOriginRPVal.pu1_OctetList,
                pMsdpSetFsMsdpSACacheEntry->MibObject.au1FsMsdpSACacheOriginRP,
                pMsdpSetFsMsdpSACacheEntry->MibObject.
                i4FsMsdpSACacheOriginRPLen);
        FsMsdpSACacheOriginRPVal.i4_Length =
            pMsdpSetFsMsdpSACacheEntry->MibObject.i4FsMsdpSACacheOriginRPLen;

        nmhSetCmnNew (FsMsdpSACacheOriginRP, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 4, i4SetOption,
                      "%i %s %s %s %s",
                      pMsdpSetFsMsdpSACacheEntry->MibObject.
                      i4FsMsdpSACacheAddrType, &FsMsdpSACacheGroupAddrVal,
                      &FsMsdpSACacheSourceAddrVal, &FsMsdpSACacheOriginRPVal,
                      &FsMsdpSACacheOriginRPVal);
    }
    if (pMsdpIsSetFsMsdpSACacheEntry->bFsMsdpSACacheStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMsdpSACacheStatus, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 1, 4, i4SetOption,
                      "%i %s %s %s %i",
                      pMsdpSetFsMsdpSACacheEntry->MibObject.
                      i4FsMsdpSACacheAddrType, &FsMsdpSACacheGroupAddrVal,
                      &FsMsdpSACacheSourceAddrVal, &FsMsdpSACacheOriginRPVal,
                      pMsdpSetFsMsdpSACacheEntry->MibObject.
                      i4FsMsdpSACacheStatus);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpSetAllFsMsdpMeshGroupTableTrigger
 Input       :  The Indices
                pMsdpSetFsMsdpMeshGroupEntry
                pMsdpIsSetFsMsdpMeshGroupEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
MsdpSetAllFsMsdpMeshGroupTableTrigger (tMsdpFsMsdpMeshGroupEntry *
                                       pMsdpSetFsMsdpMeshGroupEntry,
                                       tMsdpIsSetFsMsdpMeshGroupEntry *
                                       pMsdpIsSetFsMsdpMeshGroupEntry,
                                       INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsMsdpMeshGroupNameVal;
    UINT1               au1FsMsdpMeshGroupNameVal[256];
    tSNMP_OCTET_STRING_TYPE FsMsdpMeshGroupPeerAddressVal;
    UINT1               au1FsMsdpMeshGroupPeerAddressVal[256];

    MEMSET (au1FsMsdpMeshGroupNameVal, 0, sizeof (au1FsMsdpMeshGroupNameVal));
    FsMsdpMeshGroupNameVal.pu1_OctetList = au1FsMsdpMeshGroupNameVal;
    FsMsdpMeshGroupNameVal.i4_Length = 0;

    MEMSET (au1FsMsdpMeshGroupPeerAddressVal, 0,
            sizeof (au1FsMsdpMeshGroupPeerAddressVal));
    FsMsdpMeshGroupPeerAddressVal.pu1_OctetList =
        au1FsMsdpMeshGroupPeerAddressVal;
    FsMsdpMeshGroupPeerAddressVal.i4_Length = 0;

    if (pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupName == OSIX_TRUE)
    {
        MEMCPY (FsMsdpMeshGroupNameVal.pu1_OctetList,
                pMsdpSetFsMsdpMeshGroupEntry->MibObject.au1FsMsdpMeshGroupName,
                pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                i4FsMsdpMeshGroupNameLen);
        FsMsdpMeshGroupNameVal.i4_Length =
            pMsdpSetFsMsdpMeshGroupEntry->MibObject.i4FsMsdpMeshGroupNameLen;

        nmhSetCmnNew (FsMsdpMeshGroupName, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 3, i4SetOption, "%s %i %s %s",
                      &FsMsdpMeshGroupNameVal,
                      pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                      i4FsMsdpMeshGroupAddrType, &FsMsdpMeshGroupPeerAddressVal,
                      &FsMsdpMeshGroupNameVal);
    }
    if (pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupAddrType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMsdpMeshGroupAddrType, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 3, i4SetOption, "%s %i %s %i",
                      &FsMsdpMeshGroupNameVal,
                      pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                      i4FsMsdpMeshGroupAddrType, &FsMsdpMeshGroupPeerAddressVal,
                      pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                      i4FsMsdpMeshGroupAddrType);
    }
    if (pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupPeerAddress ==
        OSIX_TRUE)
    {
        MEMCPY (FsMsdpMeshGroupPeerAddressVal.pu1_OctetList,
                pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                au1FsMsdpMeshGroupPeerAddress,
                pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                i4FsMsdpMeshGroupPeerAddressLen);
        FsMsdpMeshGroupPeerAddressVal.i4_Length =
            pMsdpSetFsMsdpMeshGroupEntry->MibObject.
            i4FsMsdpMeshGroupPeerAddressLen;

        nmhSetCmnNew (FsMsdpMeshGroupPeerAddress, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 3, i4SetOption, "%s %i %s %s",
                      &FsMsdpMeshGroupNameVal,
                      pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                      i4FsMsdpMeshGroupAddrType, &FsMsdpMeshGroupPeerAddressVal,
                      &FsMsdpMeshGroupPeerAddressVal);
    }
    if (pMsdpIsSetFsMsdpMeshGroupEntry->bFsMsdpMeshGroupStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMsdpMeshGroupStatus, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 1, 3, i4SetOption, "%s %i %s %i",
                      &FsMsdpMeshGroupNameVal,
                      pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                      i4FsMsdpMeshGroupAddrType, &FsMsdpMeshGroupPeerAddressVal,
                      pMsdpSetFsMsdpMeshGroupEntry->MibObject.
                      i4FsMsdpMeshGroupStatus);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpSetAllFsMsdpRPTableTrigger
 Input       :  The Indices
                pMsdpSetFsMsdpRPEntry
                pMsdpIsSetFsMsdpRPEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
MsdpSetAllFsMsdpRPTableTrigger (tMsdpFsMsdpRPEntry * pMsdpSetFsMsdpRPEntry,
                                tMsdpIsSetFsMsdpRPEntry *
                                pMsdpIsSetFsMsdpRPEntry, INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsMsdpRPAddressVal;
    UINT1               au1FsMsdpRPAddressVal[256];

    MEMSET (au1FsMsdpRPAddressVal, 0, sizeof (au1FsMsdpRPAddressVal));
    FsMsdpRPAddressVal.pu1_OctetList = au1FsMsdpRPAddressVal;
    FsMsdpRPAddressVal.i4_Length = 0;

    if (pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPAddrType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMsdpRPAddrType, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 1, i4SetOption, "%i %i",
                      pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPAddrType,
                      pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPAddrType);
    }
    if (pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPAddress == OSIX_TRUE)
    {
        MEMCPY (FsMsdpRPAddressVal.pu1_OctetList,
                pMsdpSetFsMsdpRPEntry->MibObject.au1FsMsdpRPAddress,
                pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPAddressLen);
        FsMsdpRPAddressVal.i4_Length =
            pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPAddressLen;

        nmhSetCmnNew (FsMsdpRPAddress, 13, MsdpMainTaskLock, MsdpMainTaskUnLock,
                      0, 0, 1, i4SetOption, "%i %s",
                      pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPAddrType,
                      &FsMsdpRPAddressVal);
    }
    if (pMsdpIsSetFsMsdpRPEntry->bFsMsdpRPStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMsdpRPStatus, 13, MsdpMainTaskLock, MsdpMainTaskUnLock,
                      0, 1, 1, i4SetOption, "%i %i",
                      pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPAddrType,
                      pMsdpSetFsMsdpRPEntry->MibObject.i4FsMsdpRPStatus);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpSetAllFsMsdpPeerFilterTableTrigger
 Input       :  The Indices
                pMsdpSetFsMsdpPeerFilterEntry
                pMsdpIsSetFsMsdpPeerFilterEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
MsdpSetAllFsMsdpPeerFilterTableTrigger (tMsdpFsMsdpPeerFilterEntry *
                                        pMsdpSetFsMsdpPeerFilterEntry,
                                        tMsdpIsSetFsMsdpPeerFilterEntry *
                                        pMsdpIsSetFsMsdpPeerFilterEntry,
                                        INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsMsdpPeerFilterRouteMapVal;
    UINT1               au1FsMsdpPeerFilterRouteMapVal[256];

    MEMSET (au1FsMsdpPeerFilterRouteMapVal, 0,
            sizeof (au1FsMsdpPeerFilterRouteMapVal));
    FsMsdpPeerFilterRouteMapVal.pu1_OctetList = au1FsMsdpPeerFilterRouteMapVal;
    FsMsdpPeerFilterRouteMapVal.i4_Length = 0;

    if (pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterAddrType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMsdpPeerFilterAddrType, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 1, i4SetOption, "%i %i",
                      pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                      i4FsMsdpPeerFilterAddrType,
                      pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                      i4FsMsdpPeerFilterAddrType);
    }
    if (pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterRouteMap == OSIX_TRUE)
    {
        MEMCPY (FsMsdpPeerFilterRouteMapVal.pu1_OctetList,
                pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                au1FsMsdpPeerFilterRouteMap,
                pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                i4FsMsdpPeerFilterRouteMapLen);
        FsMsdpPeerFilterRouteMapVal.i4_Length =
            pMsdpSetFsMsdpPeerFilterEntry->MibObject.
            i4FsMsdpPeerFilterRouteMapLen;

        nmhSetCmnNew (FsMsdpPeerFilterRouteMap, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 1, i4SetOption, "%i %s",
                      pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                      i4FsMsdpPeerFilterAddrType, &FsMsdpPeerFilterRouteMapVal);
    }
    if (pMsdpIsSetFsMsdpPeerFilterEntry->bFsMsdpPeerFilterStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMsdpPeerFilterStatus, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 1, 1, i4SetOption, "%i %i",
                      pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                      i4FsMsdpPeerFilterAddrType,
                      pMsdpSetFsMsdpPeerFilterEntry->MibObject.
                      i4FsMsdpPeerFilterStatus);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpSetAllFsMsdpSARedistributionTableTrigger
 Input       :  The Indices
                pMsdpSetFsMsdpSARedistributionEntry
                pMsdpIsSetFsMsdpSARedistributionEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
MsdpSetAllFsMsdpSARedistributionTableTrigger (tMsdpFsMsdpSARedistributionEntry *
                                              pMsdpSetFsMsdpSARedistributionEntry,
                                              tMsdpIsSetFsMsdpSARedistributionEntry
                                              *
                                              pMsdpIsSetFsMsdpSARedistributionEntry,
                                              INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsMsdpSARedistributionRouteMapVal;
    UINT1               au1FsMsdpSARedistributionRouteMapVal[256];

    MEMSET (au1FsMsdpSARedistributionRouteMapVal, 0,
            sizeof (au1FsMsdpSARedistributionRouteMapVal));
    FsMsdpSARedistributionRouteMapVal.pu1_OctetList =
        au1FsMsdpSARedistributionRouteMapVal;
    FsMsdpSARedistributionRouteMapVal.i4_Length = 0;

    if (pMsdpIsSetFsMsdpSARedistributionEntry->
        bFsMsdpSARedistributionAddrType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMsdpSARedistributionAddrType, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 1, i4SetOption, "%i %i",
                      pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                      i4FsMsdpSARedistributionAddrType,
                      pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                      i4FsMsdpSARedistributionAddrType);
    }
    if (pMsdpIsSetFsMsdpSARedistributionEntry->bFsMsdpSARedistributionStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsMsdpSARedistributionStatus, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 1, 1, i4SetOption, "%i %i",
                      pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                      i4FsMsdpSARedistributionAddrType,
                      pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                      i4FsMsdpSARedistributionStatus);
    }
    if (pMsdpIsSetFsMsdpSARedistributionEntry->
        bFsMsdpSARedistributionRouteMap == OSIX_TRUE)
    {
        MEMCPY (FsMsdpSARedistributionRouteMapVal.pu1_OctetList,
                pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                au1FsMsdpSARedistributionRouteMap,
                pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                i4FsMsdpSARedistributionRouteMapLen);
        FsMsdpSARedistributionRouteMapVal.i4_Length =
            pMsdpSetFsMsdpSARedistributionEntry->MibObject.
            i4FsMsdpSARedistributionRouteMapLen;

        nmhSetCmnNew (FsMsdpSARedistributionRouteMap, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 1, i4SetOption, "%i %s",
                      pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                      i4FsMsdpSARedistributionAddrType,
                      &FsMsdpSARedistributionRouteMapVal);
    }
    if (pMsdpIsSetFsMsdpSARedistributionEntry->
        bFsMsdpSARedistributionRouteMapStat == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMsdpSARedistributionRouteMapStat, 13, MsdpMainTaskLock,
                      MsdpMainTaskUnLock, 0, 0, 1, i4SetOption, "%i %i",
                      pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                      i4FsMsdpSARedistributionAddrType,
                      pMsdpSetFsMsdpSARedistributionEntry->MibObject.
                      i4FsMsdpSARedistributionRouteMapStat);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpFsMsdpPeerTableCreateApi
 Input       :  pMsdpFsMsdpPeerEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tMsdpFsMsdpPeerEntry *
MsdpFsMsdpPeerTableCreateApi (tMsdpFsMsdpPeerEntry * pSetMsdpFsMsdpPeerEntry)
{
    tMsdpFsMsdpPeerEntry *pMsdpFsMsdpPeerEntry = NULL;

    if (pSetMsdpFsMsdpPeerEntry == NULL)
    {
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpFsMsdpPeerTableCreatApi: pSetMsdpFsMsdpPeerEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pMsdpFsMsdpPeerEntry =
        (tMsdpFsMsdpPeerEntry *) MemAllocMemBlk (MSDP_FSMSDPPEERTABLE_POOLID);
    if (pMsdpFsMsdpPeerEntry == NULL)
    {
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpFsMsdpPeerTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pMsdpFsMsdpPeerEntry, pSetMsdpFsMsdpPeerEntry,
                sizeof (tMsdpFsMsdpPeerEntry));
        if (RBTreeAdd
            (gMsdpGlobals.MsdpGlbMib.FsMsdpPeerTable,
             (tRBElem *) pMsdpFsMsdpPeerEntry) != RB_SUCCESS)
        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpFsMsdpPeerTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (MSDP_FSMSDPPEERTABLE_POOLID,
                                (UINT1 *) pMsdpFsMsdpPeerEntry);
            return NULL;
        }
        return pMsdpFsMsdpPeerEntry;
    }
}

/****************************************************************************
 Function    :  MsdpFsMsdpSACacheTableCreateApi
 Input       :  pMsdpFsMsdpSACacheEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tMsdpFsMsdpSACacheEntry *
MsdpFsMsdpSACacheTableCreateApi (tMsdpFsMsdpSACacheEntry *
                                 pSetMsdpFsMsdpSACacheEntry)
{
    tMsdpFsMsdpSACacheEntry *pMsdpFsMsdpSACacheEntry = NULL;

    if (pSetMsdpFsMsdpSACacheEntry == NULL)
    {
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpFsMsdpSACacheTableCreatApi: pSetMsdpFsMsdpSACacheEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pMsdpFsMsdpSACacheEntry =
        (tMsdpFsMsdpSACacheEntry *)
        MemAllocMemBlk (MSDP_FSMSDPSACACHETABLE_POOLID);
    if (pMsdpFsMsdpSACacheEntry == NULL)
    {
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpFsMsdpSACacheTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pMsdpFsMsdpSACacheEntry, pSetMsdpFsMsdpSACacheEntry,
                sizeof (tMsdpFsMsdpSACacheEntry));
        if (RBTreeAdd
            (gMsdpGlobals.MsdpGlbMib.FsMsdpSACacheTable,
             (tRBElem *) pMsdpFsMsdpSACacheEntry) != RB_SUCCESS)
        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpFsMsdpSACacheTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                                (UINT1 *) pMsdpFsMsdpSACacheEntry);
            return NULL;
        }
        return pMsdpFsMsdpSACacheEntry;
    }
}

/****************************************************************************
 Function    :  MsdpFsMsdpMeshGroupTableCreateApi
 Input       :  pMsdpFsMsdpMeshGroupEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tMsdpFsMsdpMeshGroupEntry *
MsdpFsMsdpMeshGroupTableCreateApi (tMsdpFsMsdpMeshGroupEntry *
                                   pSetMsdpFsMsdpMeshGroupEntry)
{
    tMsdpFsMsdpMeshGroupEntry *pMsdpFsMsdpMeshGroupEntry = NULL;

    if (pSetMsdpFsMsdpMeshGroupEntry == NULL)
    {
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpFsMsdpMeshGroupTableCreatApi: pSetMsdpFsMsdpMeshGroupEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pMsdpFsMsdpMeshGroupEntry =
        (tMsdpFsMsdpMeshGroupEntry *)
        MemAllocMemBlk (MSDP_FSMSDPMESHGROUPTABLE_POOLID);
    if (pMsdpFsMsdpMeshGroupEntry == NULL)
    {
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpFsMsdpMeshGroupTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pMsdpFsMsdpMeshGroupEntry, pSetMsdpFsMsdpMeshGroupEntry,
                sizeof (tMsdpFsMsdpMeshGroupEntry));
        if (RBTreeAdd
            (gMsdpGlobals.MsdpGlbMib.FsMsdpMeshGroupTable,
             (tRBElem *) pMsdpFsMsdpMeshGroupEntry) != RB_SUCCESS)
        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpFsMsdpMeshGroupTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (MSDP_FSMSDPMESHGROUPTABLE_POOLID,
                                (UINT1 *) pMsdpFsMsdpMeshGroupEntry);
            return NULL;
        }
        return pMsdpFsMsdpMeshGroupEntry;
    }
}

/****************************************************************************
 Function    :  MsdpFsMsdpRPTableCreateApi
 Input       :  pMsdpFsMsdpRPEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tMsdpFsMsdpRPEntry *
MsdpFsMsdpRPTableCreateApi (tMsdpFsMsdpRPEntry * pSetMsdpFsMsdpRPEntry)
{
    tMsdpFsMsdpRPEntry *pMsdpFsMsdpRPEntry = NULL;

    if (pSetMsdpFsMsdpRPEntry == NULL)
    {
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpFsMsdpRPTableCreatApi: pSetMsdpFsMsdpRPEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pMsdpFsMsdpRPEntry =
        (tMsdpFsMsdpRPEntry *) MemAllocMemBlk (MSDP_FSMSDPRPTABLE_POOLID);
    if (pMsdpFsMsdpRPEntry == NULL)
    {
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpFsMsdpRPTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pMsdpFsMsdpRPEntry, pSetMsdpFsMsdpRPEntry,
                sizeof (tMsdpFsMsdpRPEntry));
        if (RBTreeAdd
            (gMsdpGlobals.MsdpGlbMib.FsMsdpRPTable,
             (tRBElem *) pMsdpFsMsdpRPEntry) != RB_SUCCESS)
        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpFsMsdpRPTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (MSDP_FSMSDPRPTABLE_POOLID,
                                (UINT1 *) pMsdpFsMsdpRPEntry);
            return NULL;
        }
        return pMsdpFsMsdpRPEntry;
    }
}

/****************************************************************************
 Function    :  MsdpFsMsdpPeerFilterTableCreateApi
 Input       :  pMsdpFsMsdpPeerFilterEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tMsdpFsMsdpPeerFilterEntry *
MsdpFsMsdpPeerFilterTableCreateApi (tMsdpFsMsdpPeerFilterEntry *
                                    pSetMsdpFsMsdpPeerFilterEntry)
{
    tMsdpFsMsdpPeerFilterEntry *pMsdpFsMsdpPeerFilterEntry = NULL;

    if (pSetMsdpFsMsdpPeerFilterEntry == NULL)
    {
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpFsMsdpPeerFilterTableCreatApi: pSetMsdpFsMsdpPeerFilterEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pMsdpFsMsdpPeerFilterEntry =
        (tMsdpFsMsdpPeerFilterEntry *)
        MemAllocMemBlk (MSDP_FSMSDPPEERFILTERTABLE_POOLID);
    if (pMsdpFsMsdpPeerFilterEntry == NULL)
    {
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpFsMsdpPeerFilterTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pMsdpFsMsdpPeerFilterEntry, pSetMsdpFsMsdpPeerFilterEntry,
                sizeof (tMsdpFsMsdpPeerFilterEntry));
        if (RBTreeAdd
            (gMsdpGlobals.MsdpGlbMib.FsMsdpPeerFilterTable,
             (tRBElem *) pMsdpFsMsdpPeerFilterEntry) != RB_SUCCESS)
        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpFsMsdpPeerFilterTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (MSDP_FSMSDPPEERFILTERTABLE_POOLID,
                                (UINT1 *) pMsdpFsMsdpPeerFilterEntry);
            return NULL;
        }
        return pMsdpFsMsdpPeerFilterEntry;
    }
}

/****************************************************************************
 Function    :  MsdpFsMsdpSARedistributionTableCreateApi
 Input       :  pMsdpFsMsdpSARedistributionEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tMsdpFsMsdpSARedistributionEntry *
MsdpFsMsdpSARedistributionTableCreateApi (tMsdpFsMsdpSARedistributionEntry *
                                          pSetMsdpFsMsdpSARedistributionEntry)
{
    tMsdpFsMsdpSARedistributionEntry *pMsdpFsMsdpSARedistributionEntry = NULL;

    if (pSetMsdpFsMsdpSARedistributionEntry == NULL)
    {
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpFsMsdpSARedistributionTableCreatApi: pSetMsdpFsMsdpSARedistributionEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pMsdpFsMsdpSARedistributionEntry =
        (tMsdpFsMsdpSARedistributionEntry *)
        MemAllocMemBlk (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID);
    if (pMsdpFsMsdpSARedistributionEntry == NULL)
    {
        MSDP_TRC ((MSDP_UTIL_TRC,
                   "MsdpFsMsdpSARedistributionTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pMsdpFsMsdpSARedistributionEntry,
                pSetMsdpFsMsdpSARedistributionEntry,
                sizeof (tMsdpFsMsdpSARedistributionEntry));
        if (RBTreeAdd
            (gMsdpGlobals.MsdpGlbMib.FsMsdpSARedistributionTable,
             (tRBElem *) pMsdpFsMsdpSARedistributionEntry) != RB_SUCCESS)
        {
            MSDP_TRC ((MSDP_UTIL_TRC,
                       "MsdpFsMsdpSARedistributionTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (MSDP_FSMSDPSAREDISTRIBUTIONTABLE_POOLID,
                                (UINT1 *) pMsdpFsMsdpSARedistributionEntry);
            return NULL;
        }
        return pMsdpFsMsdpSARedistributionEntry;
    }
}
