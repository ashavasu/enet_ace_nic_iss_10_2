/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: msdputl.c,v 1.4 2013/09/17 11:38:59 siva Exp $
*
* Description: This file contains utility functions used by protocol Msdp
*********************************************************************/

#include "msdpinc.h"
#include "msdpcli.h"
/****************************************************************************
 Function    :  MsdpGetAllUtlFsMsdpPeerTable
 Input       :  pMsdpGetFsMsdpPeerEntry
                pMsdpdsFsMsdpPeerEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetAllUtlFsMsdpPeerTable (tMsdpFsMsdpPeerEntry * pMsdpGetFsMsdpPeerEntry,
                              tMsdpFsMsdpPeerEntry * pMsdpdsFsMsdpPeerEntry)
{
    UNUSED_PARAM (pMsdpGetFsMsdpPeerEntry);
    UNUSED_PARAM (pMsdpdsFsMsdpPeerEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpGetAllUtlFsMsdpSACacheTable
 Input       :  pMsdpGetFsMsdpSACacheEntry
                pMsdpdsFsMsdpSACacheEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetAllUtlFsMsdpSACacheTable (tMsdpFsMsdpSACacheEntry *
                                 pMsdpGetFsMsdpSACacheEntry,
                                 tMsdpFsMsdpSACacheEntry *
                                 pMsdpdsFsMsdpSACacheEntry)
{
    UNUSED_PARAM (pMsdpGetFsMsdpSACacheEntry);
    UNUSED_PARAM (pMsdpdsFsMsdpSACacheEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpGetAllUtlFsMsdpMeshGroupTable
 Input       :  pMsdpGetFsMsdpMeshGroupEntry
                pMsdpdsFsMsdpMeshGroupEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetAllUtlFsMsdpMeshGroupTable (tMsdpFsMsdpMeshGroupEntry *
                                   pMsdpGetFsMsdpMeshGroupEntry,
                                   tMsdpFsMsdpMeshGroupEntry *
                                   pMsdpdsFsMsdpMeshGroupEntry)
{
    UNUSED_PARAM (pMsdpGetFsMsdpMeshGroupEntry);
    UNUSED_PARAM (pMsdpdsFsMsdpMeshGroupEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpGetAllUtlFsMsdpRPTable
 Input       :  pMsdpGetFsMsdpRPEntry
                pMsdpdsFsMsdpRPEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetAllUtlFsMsdpRPTable (tMsdpFsMsdpRPEntry * pMsdpGetFsMsdpRPEntry,
                            tMsdpFsMsdpRPEntry * pMsdpdsFsMsdpRPEntry)
{
    UNUSED_PARAM (pMsdpGetFsMsdpRPEntry);
    UNUSED_PARAM (pMsdpdsFsMsdpRPEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpGetAllUtlFsMsdpPeerFilterTable
 Input       :  pMsdpGetFsMsdpPeerFilterEntry
                pMsdpdsFsMsdpPeerFilterEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetAllUtlFsMsdpPeerFilterTable (tMsdpFsMsdpPeerFilterEntry *
                                    pMsdpGetFsMsdpPeerFilterEntry,
                                    tMsdpFsMsdpPeerFilterEntry *
                                    pMsdpdsFsMsdpPeerFilterEntry)
{
    UNUSED_PARAM (pMsdpGetFsMsdpPeerFilterEntry);
    UNUSED_PARAM (pMsdpdsFsMsdpPeerFilterEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MsdpGetAllUtlFsMsdpSARedistributionTable
 Input       :  pMsdpGetFsMsdpSARedistributionEntry
                pMsdpdsFsMsdpSARedistributionEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpGetAllUtlFsMsdpSARedistributionTable (tMsdpFsMsdpSARedistributionEntry *
                                          pMsdpGetFsMsdpSARedistributionEntry,
                                          tMsdpFsMsdpSARedistributionEntry *
                                          pMsdpdsFsMsdpSARedistributionEntry)
{
    UNUSED_PARAM (pMsdpGetFsMsdpSARedistributionEntry);
    UNUSED_PARAM (pMsdpdsFsMsdpSARedistributionEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  MsdpUtilUpdateFsMsdpPeerTable
 * Input       :   pMsdpOldFsMsdpPeerEntry
                   pMsdpFsMsdpPeerEntry
                   pMsdpIsSetFsMsdpPeerEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpUtilUpdateFsMsdpPeerTable (tMsdpFsMsdpPeerEntry * pMsdpOldFsMsdpPeerEntry,
                               tMsdpFsMsdpPeerEntry * pMsdpFsMsdpPeerEntry,
                               tMsdpIsSetFsMsdpPeerEntry *
                               pMsdpIsSetFsMsdpPeerEntry)
{
    tMsdpConfigPeer     ConfigPeer;
    INT4                i4RetVal = 0;
    INT4                i4IfIndex = 0;

    MEMSET (&ConfigPeer, 0, sizeof (tMsdpConfigPeer));

    if (pMsdpOldFsMsdpPeerEntry == NULL)
    {
        i4IfIndex = MsdpUtilIsValidLocalAddress
            (pMsdpFsMsdpPeerEntry->MibObject.au1FsMsdpPeerLocalAddress,
             pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType);

        pMsdpFsMsdpPeerEntry->i4IfIndex = i4IfIndex;

        pMsdpFsMsdpPeerEntry->TxBufInfo.pu1Msg = (UINT1 *)
            MemAllocMemBlk (MSDP_FSMSDPPEER_RX_TX_BUF_POOLID);
        if (pMsdpFsMsdpPeerEntry->TxBufInfo.pu1Msg == NULL)
        {
            return OSIX_FAILURE;
        }

        pMsdpFsMsdpPeerEntry->RxBufInfo.pu1Msg = (UINT1 *)
            MemAllocMemBlk (MSDP_FSMSDPPEER_RX_TX_BUF_POOLID);
        if (pMsdpFsMsdpPeerEntry->RxBufInfo.pu1Msg == NULL)
        {
            MemReleaseMemBlock (MSDP_FSMSDPPEER_RX_TX_BUF_POOLID,
                                pMsdpFsMsdpPeerEntry->TxBufInfo.pu1Msg);
            return OSIX_FAILURE;
        }

    }
    else
    {
        if ((pMsdpOldFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus ==
             pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus) &&
            (pMsdpOldFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAdminStatus ==
             pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAdminStatus))
        {
            return OSIX_SUCCESS;
        }
    }
    IPVX_ADDR_INIT (ConfigPeer.PeerAddr,
                    pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType,
                    pMsdpFsMsdpPeerEntry->MibObject.au1FsMsdpPeerRemoteAddress);
    ConfigPeer.PeerAddr.u1AddrLen =
        (UINT1) pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerRemoteAddressLen;
    ConfigPeer.u1AddrType =
        (UINT1) pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAddrType;
    if (pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus == ACTIVE)
    {
        if (pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAdminStatus ==
            MSDP_PEER_ADMIN_DISABLE)
        {
            ConfigPeer.u1PeerAdmin = MSDP_PEER_ADMIN_DISABLE;

            i4RetVal = MsdpPeerHdlAdminStatusConfig (&ConfigPeer);
        }
        else
        {
            ConfigPeer.u1PeerAdmin = MSDP_PEER_ADMIN_ENABLE;

            i4RetVal = MsdpPeerHdlAdminStatusConfig (&ConfigPeer);
            if ((i4RetVal == OSIX_SUCCESS) && (pMsdpOldFsMsdpPeerEntry == NULL))
            {
                gMsdpGlobals.MsdpGlbMib.i4FsMsdpPeerCount++;
            }
            else if ((i4RetVal == OSIX_FAILURE) &&
                     (pMsdpOldFsMsdpPeerEntry == NULL))
            {
                MemReleaseMemBlock (MSDP_FSMSDPPEER_RX_TX_BUF_POOLID,
                                    pMsdpFsMsdpPeerEntry->TxBufInfo.pu1Msg);
                MemReleaseMemBlock (MSDP_FSMSDPPEER_RX_TX_BUF_POOLID,
                                    pMsdpFsMsdpPeerEntry->TxBufInfo.pu1Msg);
            }
        }
    }

    else if (pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus == DESTROY)
    {
        ConfigPeer.u1PeerAdmin = MSDP_PEER_ADMIN_DISABLE;

        i4RetVal = MsdpPeerHdlAdminStatusConfig (&ConfigPeer);

        MsdpPeerDestroyCacheFromPeer (pMsdpFsMsdpPeerEntry);
        MemReleaseMemBlock (MSDP_FSMSDPPEER_RX_TX_BUF_POOLID,
                            pMsdpFsMsdpPeerEntry->TxBufInfo.pu1Msg);
        MemReleaseMemBlock (MSDP_FSMSDPPEER_RX_TX_BUF_POOLID,
                            pMsdpFsMsdpPeerEntry->RxBufInfo.pu1Msg);
        if (i4RetVal == OSIX_SUCCESS)
        {
            gMsdpGlobals.MsdpGlbMib.i4FsMsdpPeerCount--;
        }
    }

    if ((pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerStatus == NOT_IN_SERVICE)
        && (pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerAdminStatus ==
            MSDP_PEER_ADMIN_DISABLE))
    {
        pMsdpFsMsdpPeerEntry->MibObject.i4FsMsdpPeerState = MSDP_PEER_DISABLED;
    }

    if (i4RetVal == OSIX_FAILURE)
    {
        CLI_SET_ERR (MSDP_PEER_ADMIN_ERROR);
    }

    UNUSED_PARAM (pMsdpIsSetFsMsdpPeerEntry);
    return i4RetVal;
}

/****************************************************************************
 * Function    :  MsdpUtilUpdateFsMsdpSACacheTable
 * Input       :   pMsdpOldFsMsdpSACacheEntry
                   pMsdpFsMsdpSACacheEntry
                   pMsdpIsSetFsMsdpSACacheEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpUtilUpdateFsMsdpSACacheTable (tMsdpFsMsdpSACacheEntry *
                                  pMsdpOldFsMsdpSACacheEntry,
                                  tMsdpFsMsdpSACacheEntry *
                                  pMsdpFsMsdpSACacheEntry,
                                  tMsdpIsSetFsMsdpSACacheEntry *
                                  pMsdpIsSetFsMsdpSACacheEntry)
{

    UNUSED_PARAM (pMsdpOldFsMsdpSACacheEntry);
    UNUSED_PARAM (pMsdpFsMsdpSACacheEntry);
    UNUSED_PARAM (pMsdpIsSetFsMsdpSACacheEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  MsdpUtilUpdateFsMsdpMeshGroupTable
 * Input       :   pMsdpOldFsMsdpMeshGroupEntry
                   pMsdpFsMsdpMeshGroupEntry
                   pMsdpIsSetFsMsdpMeshGroupEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpUtilUpdateFsMsdpMeshGroupTable (tMsdpFsMsdpMeshGroupEntry *
                                    pMsdpOldFsMsdpMeshGroupEntry,
                                    tMsdpFsMsdpMeshGroupEntry *
                                    pMsdpFsMsdpMeshGroupEntry,
                                    tMsdpIsSetFsMsdpMeshGroupEntry *
                                    pMsdpIsSetFsMsdpMeshGroupEntry)
{

    UNUSED_PARAM (pMsdpOldFsMsdpMeshGroupEntry);
    UNUSED_PARAM (pMsdpFsMsdpMeshGroupEntry);
    UNUSED_PARAM (pMsdpIsSetFsMsdpMeshGroupEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  MsdpUtilUpdateFsMsdpRPTable
 * Input       :   pMsdpOldFsMsdpRPEntry
                   pMsdpFsMsdpRPEntry
                   pMsdpIsSetFsMsdpRPEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpUtilUpdateFsMsdpRPTable (tMsdpFsMsdpRPEntry * pMsdpOldFsMsdpRPEntry,
                             tMsdpFsMsdpRPEntry * pMsdpFsMsdpRPEntry,
                             tMsdpIsSetFsMsdpRPEntry * pMsdpIsSetFsMsdpRPEntry)
{
    INT4                i4IfIndex = 0;
    INT4                i4RetVal = 0;
    UINT1               u1OperStatus = 0;

    i4IfIndex = MsdpUtilIsValidLocalAddress
        (pMsdpFsMsdpRPEntry->MibObject.au1FsMsdpRPAddress,
         pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPAddrType);

    pMsdpFsMsdpRPEntry->i4IfIndex = i4IfIndex;
    i4RetVal = MsdpPortGetIfInfo (i4IfIndex, &u1OperStatus);

    if (i4RetVal == CFA_SUCCESS)
    {
        pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPOperStatus = u1OperStatus;
    }
    else
    {
        pMsdpFsMsdpRPEntry->MibObject.i4FsMsdpRPOperStatus =
            MSDP_IF_OPERSTATUS_DOWN;
    }
    UNUSED_PARAM (pMsdpOldFsMsdpRPEntry);
    UNUSED_PARAM (pMsdpIsSetFsMsdpRPEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  MsdpUtilUpdateFsMsdpPeerFilterTable
 * Input       :   pMsdpOldFsMsdpPeerFilterEntry
                   pMsdpFsMsdpPeerFilterEntry
                   pMsdpIsSetFsMsdpPeerFilterEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpUtilUpdateFsMsdpPeerFilterTable (tMsdpFsMsdpPeerFilterEntry *
                                     pMsdpOldFsMsdpPeerFilterEntry,
                                     tMsdpFsMsdpPeerFilterEntry *
                                     pMsdpFsMsdpPeerFilterEntry,
                                     tMsdpIsSetFsMsdpPeerFilterEntry *
                                     pMsdpIsSetFsMsdpPeerFilterEntry)
{

    UNUSED_PARAM (pMsdpOldFsMsdpPeerFilterEntry);
    UNUSED_PARAM (pMsdpFsMsdpPeerFilterEntry);
    UNUSED_PARAM (pMsdpIsSetFsMsdpPeerFilterEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  MsdpUtilUpdateFsMsdpSARedistributionTable
 * Input       :   pMsdpOldFsMsdpSARedistributionEntry
                   pMsdpFsMsdpSARedistributionEntry
                   pMsdpIsSetFsMsdpSARedistributionEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
MsdpUtilUpdateFsMsdpSARedistributionTable (tMsdpFsMsdpSARedistributionEntry *
                                           pMsdpOldFsMsdpSARedistributionEntry,
                                           tMsdpFsMsdpSARedistributionEntry *
                                           pMsdpFsMsdpSARedistributionEntry,
                                           tMsdpIsSetFsMsdpSARedistributionEntry
                                           *
                                           pMsdpIsSetFsMsdpSARedistributionEntry)
{

    UNUSED_PARAM (pMsdpOldFsMsdpSARedistributionEntry);
    UNUSED_PARAM (pMsdpFsMsdpSARedistributionEntry);
    UNUSED_PARAM (pMsdpIsSetFsMsdpSARedistributionEntry);
    return OSIX_SUCCESS;
}
