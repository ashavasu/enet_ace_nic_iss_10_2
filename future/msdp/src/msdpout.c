/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: msdpout.c,v 1.4 2011/01/27 14:13:55 siva Exp $
*
* Description: This file contains the Msdp OUTPUT module related routines 
*********************************************************************/
#include "msdpinc.h"
#include "fssocket.h"

INT4
MsdpOutValidateAndDecrementTtl (tMsdpFsMsdpPeerEntry * pInPeer,
                                UINT1 *pu1SrcBuf, UINT2 *pu2PktLen)
{
    tMsdpFsMsdpPeerEntry *pPeer = NULL;
    INT4                i4RetVal = 0;
    INT4                i4DataTtl = 0;
    UINT4               u4TTLOffset = 0;
    UINT1               u1AddrType = 0;
    UINT1               u1AddrLen = 0;
    UINT1               u1EntryCount = 0;
    UINT1               u1SAEntrySize = 0;

    if ((pu1SrcBuf[0] != MSDP_TLV_SA) && (pu1SrcBuf[0] != MSDP6_TLV_SA))
    {
        return OSIX_SUCCESS;
    }
    u1AddrType = (UINT1) pInPeer->MibObject.i4FsMsdpPeerAddrType;
    u1AddrLen = (u1AddrType == IPVX_ADDR_FMLY_IPV4) ?
        IPVX_IPV4_ADDR_LEN : IPVX_IPV6_ADDR_LEN;
    u1EntryCount = pu1SrcBuf[MSDP_THREE];
    u1SAEntrySize = (UINT1) MSDP_SA_TLV_LEN (u1EntryCount, u1AddrLen, 0);

    if (u1SAEntrySize == *pu2PktLen)
    {
        return OSIX_SUCCESS;
    }
    u4TTLOffset = (u1AddrType == IPVX_ADDR_FMLY_IPV4) ?
        MSDP_V4_MDP_TTL_OFFSET : MSDP_V6_MDP_HOPLIMIT_OFFSET;

    pPeer = MsdpGetFsMsdpPeerTable (pInPeer);

    if (pPeer == NULL)
    {
        return OSIX_SUCCESS;
    }

    i4DataTtl = pu1SrcBuf[u1SAEntrySize + u4TTLOffset] -
        pPeer->MibObject.i4FsMsdpPeerDataTtl;

    if (i4DataTtl >= 0)
    {
        pu1SrcBuf[u1SAEntrySize + u4TTLOffset] = (UINT1) i4DataTtl;
        i4RetVal = OSIX_SUCCESS;
    }
    else
    {
        *pu2PktLen = u1SAEntrySize;
        i4RetVal = OSIX_FAILURE;
    }
    return i4RetVal;
}

/****************************************************************************
 * FUNCTION NAME : MsdpOutChkForValidPeer
 *
 * DESCRIPTION   : This function checks the received peer address
 *                 to peer address passed (pNextPeer) or same or not
 *
 * INPUTS        : pRecvPeer- MSDP received peer
 *                 pNextPeer- Next peer entry
 *                 
 * OUTPUTS       : None
 *
 * RETURNS       : OSIX_SUCCESS if rcvd Peer address and passed peer
 *                 address are same and  Peer state of the passed peer is
 *                 in ESTABLISHED state 
 *                 OSIX_FAILURE otherwise
 *
 ***************************************************************************/

INT4
MsdpOutChkForValidPeer (tMsdpFsMsdpPeerEntry * pRecvPeer,
                        tMsdpFsMsdpPeerEntry * pNextPeer)
{
    INT4                i4RetVal = 1;

    MSDP_TRC_FUNC ((MSDP_TRC_OUT | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpOutChkForValidPeer"));

    if (pRecvPeer == NULL)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_OUT, "\nMsdpOutChkForValidPeer: EXIT"
                        "Received peer is NULL."));
    }
    else
    {
        i4RetVal = MEMCMP (pRecvPeer, pNextPeer, sizeof (tMsdpFsMsdpPeerEntry));
        if (i4RetVal == 0)
        {

            MSDP_TRC_FUNC ((MSDP_TRC_OUT, "\nMsdpOutChkForValidPeer: EXIT"
                            "Received peer %s and next peer %s are the same one",
                            pRecvPeer->MibObject.au1FsMsdpPeerLocalAddress,
                            pNextPeer->MibObject.au1FsMsdpPeerLocalAddress));

            return OSIX_FAILURE;
        }

    }

    if (pNextPeer->MibObject.i4FsMsdpPeerState != MSDP_PEER_ESTABLISHED)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_OUT, "\nMsdpOutChkForValidPeer: EXIT"
                        "Peer state is not ESTABLISHED"));

        return OSIX_FAILURE;
    }

    MSDP_TRC_FUNC ((MSDP_TRC_OUT, "\nMsdpOutChkForValidPeer: "
                    "The peer  is valid one for forwarding the pkt"));

    MSDP_TRC_FUNC ((MSDP_TRC_OUT | MSDP_FN_EXIT,
                    "\nEXIT: MsdpOutChkForValidPeer"));

    return OSIX_SUCCESS;
}

VOID
MsdpOutUpdatePeerStatistics (tMsdpFsMsdpPeerEntry * pPeer, UINT1 *pu1SrcBuf,
                             UINT2 u2PktLen)
{
    INT4                i4SAEntrySize = 0;
    switch (*pu1SrcBuf)
    {
        case MSDP_TLV_SA:
        case MSDP6_TLV_SA:
            i4SAEntrySize = MSDP_SA_TLV_LEN (pu1SrcBuf[MSDP_THREE],
                                             pPeer->MibObject.
                                             i4FsMsdpPeerRemoteAddressLen, 0);
            if (i4SAEntrySize < u2PktLen)
            {
                pPeer->MibObject.u4FsMsdpPeerOutDataPackets++;
            }
            pPeer->MibObject.u4FsMsdpPeerOutSAs++;
            break;

        case MSDP_TLV_SA_REQUEST:
        case MSDP6_TLV_SA_REQUEST:
            pPeer->MibObject.u4FsMsdpPeerOutSARequests++;
            break;

        case MSDP_TLV_SA_RESPONSE:
        case MSDP6_TLV_SA_RESPONSE:
            pPeer->MibObject.u4FsMsdpPeerOutSAResponses++;
            break;

        case MSDP_TLV_KEEPALIVE:
            pPeer->MibObject.u4FsMsdpPeerOutKeepAliveCount++;
            break;
        default:
            break;

    }
    pPeer->MibObject.u4FsMsdpPeerOutControlMessages++;
}

/****************************************************************************
 * FUNCTION NAME : MsdpOutFwdSAPkt
 *
 * DESCRIPTION   : This function forwards the received SA Advt packet to all
 *                 MSDP Peers
 *
 * INPUTS        : pRcvPeer- MSDP received peer
 *                 pu1SrcBuf- Source buffer
 *                 i4AddrType- Peer Address type
 *                 u2SrcMsgOffset- Source message offset
 *                 u2PktLen- Packet length
 *                 u2BufSize- Buffer size
 *                 
 * OUTPUTS       : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/
VOID
MsdpOutFwdSAPkt (tMsdpFsMsdpPeerEntry * pRcvPeer, UINT1 *pu1SrcBuf,
                 INT4 i4AddrType, UINT2 u2SrcMsgOffset, UINT2 u2PktLen,
                 UINT2 u2BufSize)
{
    tMsdpFsMsdpPeerEntry Peer;
    tMsdpFsMsdpPeerEntry *pNextPeer = NULL;
    INT4                i4RetVal = MSDP_PEER_DONT_FORWARD;

    MSDP_TRC_FUNC ((MSDP_TRC_OUT | MSDP_FN_ENTRY, "\nENTRY: MsdpOutFwdSAPkt"));

    MEMSET (&Peer, 0, sizeof (tMsdpFsMsdpPeerEntry));

    while ((pNextPeer = MsdpGetNextFsMsdpPeerTable (&Peer)) != NULL)
    {
        if (i4AddrType != pNextPeer->MibObject.i4FsMsdpPeerAddrType)
        {
            break;
        }
        MEMCPY (&Peer, pNextPeer, sizeof (tMsdpFsMsdpPeerEntry));
        /* Donot forward the pkt to peer from whom its rcvd */

        i4RetVal = MsdpOutChkForValidPeer (pRcvPeer, pNextPeer);

        if (i4RetVal == OSIX_FAILURE)
        {
            MSDP_TRC_FUNC ((MSDP_TRC_OUT,
                            "\nMsdpOutChkForValidPeer returns error"));
            continue;
        }

        i4RetVal = MsdpOutChkPeerInMeshGrp (pRcvPeer, pNextPeer);

        if (i4RetVal == MSDP_PEER_DONT_FORWARD)
        {
            MSDP_TRC_FUNC ((MSDP_TRC_OUT,
                            "\nMsdpOutChkForValidPeer Mesh Grp returns Dont FORWARD"));

            continue;
        }

        i4RetVal = MsdpOutValidateAndDecrementTtl
            (pNextPeer, pu1SrcBuf, &u2PktLen);
        if (i4RetVal == OSIX_FAILURE)
        {
            /*TTL Error in the multicast data pkt.
             *So dropping the mcast pkt alone.*/
            pNextPeer->MibObject.u4FsMsdpPeerDataTtlErrorCount++;
            continue;
        }

        i4RetVal = MsdpOutSendMsgtoPeer (pNextPeer, pu1SrcBuf,
                                         u2SrcMsgOffset, u2PktLen, u2BufSize);
        if (i4RetVal == OSIX_SUCCESS)
        {
            if (pNextPeer->MibObject.i4FsMsdpPeerKeepAliveConfigured != 0)
            {
                MsdpTmrStartTmr
                    (&pNextPeer->MsdpPeerKATimer, MSDP_KEEP_ALIVE_TMR,
                     (UINT4) pNextPeer->MibObject.
                     i4FsMsdpPeerKeepAliveConfigured);
            }
            MsdpOutUpdatePeerStatistics (pNextPeer, pu1SrcBuf, u2PktLen);
        }

    }

    MSDP_TRC_FUNC ((MSDP_TRC_OUT | MSDP_FN_EXIT, "\nEXIT: MsdpOutFwdSAPkt"));
    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpOutCopyMsgInPeerTxBuf
 *
 * DESCRIPTION   : This function copies the message to the peer transmit buffer
 *
 * INPUTS        : pPeer- MSDP peer entry
 *                 pu1SrcBuf- Source buffer
 *                 u2SrcMsgOffset- Message offset
 *                 u2SrcMsgLen- Source message length
 *                 u2SrcBufSize- Source buffer size 
 *
 * OUTPUTS       : None
 *
 * RETURNS       : None
 ***************************************************************************/
VOID
MsdpOutCopyMsgInPeerTxBuf (tMsdpFsMsdpPeerEntry * pPeer, UINT1 *pu1SrcBuf,
                           UINT2 u2SrcMsgOffset, UINT2 u2SrcMsgLen,
                           UINT2 u2SrcBufSize)
{
    UINT1              *pu1ResDataBuf = pPeer->TxBufInfo.pu1Msg;
    UINT2               u2PeerTxResMsgSize = pPeer->TxBufInfo.u2MsgSize;
    UINT2               u2PeerTxBufWriteOffset = 0;
    UINT2               u2BytesToCopy = 0;
    UINT2               u2TxContiBytes = 0;
    UINT2               u2SrcContiBytes = 0;
    UINT2               u2Count = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_OUT | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpOutCopyMsgInPeerTxBuf"));

    u2PeerTxBufWriteOffset = (UINT2) ((pPeer->TxBufInfo.u2MsgOffset +
                                       u2PeerTxResMsgSize) %
                                      (UINT2) MSDP_PEER_MAX_RX_TX_BUF_LEN);

    if ((u2PeerTxBufWriteOffset + u2SrcMsgLen) >= MSDP_PEER_MAX_RX_TX_BUF_LEN)
    {
        /* Tx buffer doesnt have contiguous free memory */
        if ((u2SrcMsgOffset + u2SrcMsgLen) < u2SrcBufSize)
        {
            /* Src Msg in the buffer is contiguous */
            /* copy the Msg till the end of the Tx buffer */
            u2BytesToCopy = (UINT2) (MSDP_PEER_MAX_RX_TX_BUF_LEN -
                                     u2PeerTxBufWriteOffset);
            MEMCPY ((pu1ResDataBuf + u2PeerTxBufWriteOffset),
                    (pu1SrcBuf + u2SrcMsgOffset), u2BytesToCopy);

            /* copy the remaining Src Msg from the start of the Tx buf */
            u2SrcMsgOffset += u2BytesToCopy;
            u2BytesToCopy = (UINT2) (u2SrcMsgLen - u2BytesToCopy);
            MEMCPY (pu1ResDataBuf, (pu1SrcBuf + u2SrcMsgOffset), u2BytesToCopy);
        }
        else
        {
            /* both Tx and Src Buffer Msgs are not contiguous */
            u2TxContiBytes = (UINT2) (MSDP_PEER_MAX_RX_TX_BUF_LEN -
                                      u2PeerTxBufWriteOffset);
            u2SrcContiBytes = (UINT2) (u2SrcBufSize - u2SrcMsgOffset);

            /* Find the smallest linear bytes */
            u2BytesToCopy = ((u2SrcContiBytes < u2TxContiBytes) ?
                             u2SrcContiBytes : u2TxContiBytes);
            MEMCPY ((pu1ResDataBuf + u2PeerTxBufWriteOffset),
                    (pu1SrcBuf + u2SrcMsgOffset), u2BytesToCopy);

            u2PeerTxBufWriteOffset += u2BytesToCopy;
            u2SrcMsgOffset += u2BytesToCopy;

            /* copy the rest */
            u2BytesToCopy = (UINT2) (u2SrcMsgLen - u2BytesToCopy);
            for (u2Count = 0; u2Count < u2BytesToCopy; u2Count++)
            {
                if (u2PeerTxBufWriteOffset >= MSDP_PEER_MAX_RX_TX_BUF_LEN)
                {
                    u2PeerTxBufWriteOffset %=
                        (UINT2) MSDP_PEER_MAX_RX_TX_BUF_LEN;
                }
                if (u2SrcMsgOffset >= u2SrcBufSize)
                {
                    u2SrcMsgOffset %= u2SrcBufSize;
                }
                *(pu1ResDataBuf + u2PeerTxBufWriteOffset) =
                    *(pu1SrcBuf + u2SrcMsgOffset);
            }
        }
    }
    else
    {
        /* Tx buffer has contiguous free memory */

        if ((u2SrcMsgOffset + u2SrcMsgLen) >= u2SrcBufSize)
        {
            /* Src Msg in buffer is not contiguous */
            u2BytesToCopy = (UINT2) (u2SrcBufSize - u2SrcMsgOffset);
            MEMCPY ((pu1ResDataBuf + u2PeerTxBufWriteOffset),
                    (pu1SrcBuf + u2SrcMsgOffset), u2BytesToCopy);

            u2PeerTxBufWriteOffset += u2BytesToCopy;
            u2BytesToCopy = (UINT2) (u2SrcMsgLen - u2BytesToCopy);
            MEMCPY ((pu1ResDataBuf + u2PeerTxBufWriteOffset), pu1SrcBuf,
                    u2BytesToCopy);
        }
        else
        {
            /* both Tx and Src Buffer Msgs are contiguous */
            MEMCPY ((pu1ResDataBuf + u2PeerTxBufWriteOffset),
                    (pu1SrcBuf + u2SrcMsgOffset), u2SrcMsgLen);
        }

    }
    pPeer->TxBufInfo.u2MsgSize += u2SrcMsgLen;

    MSDP_TRC_FUNC ((MSDP_TRC_OUT | MSDP_FN_EXIT,
                    "\nEXIT: MsdpOutCopyMsgInPeerTxBuf"));

    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpOutSendMsgtoPeer
 *
 * DESCRIPTION   : This function sends message to the given peer
 *
 * INPUTS        : pPeer- MSDP peer entry
 *                 pu1Buf- Buffer that contains data to send
 *                 u2MsgOffset- Message offset of pu1Buf from which data to be
 *                 send
 *                 u2MsgLen- Message length to be send
 *                 u2BufSize- Free Buffer size of Peer Tx Buf
 *
 * OUTPUTS       : None
 *
 * RETURNS       : OSIX_FAILURE if MsdpSockSend fails.
 *                 OSIX_SUCCESS otherwise
 ***************************************************************************/
INT4
MsdpOutSendMsgtoPeer (tMsdpFsMsdpPeerEntry * pPeer, UINT1 *pu1Buf,
                      UINT2 u2MsgOffset, UINT2 u2MsgLen, UINT2 u2BufSize)
{
    INT4                i4SockFd = MSDP_INVALID_SOCK_ID;
    INT4                i4WrBytes = 0;
    UINT2               u2BytesToWrite = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_OUT | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpOutSendMsgtoPeer"));

    i4SockFd = pPeer->i4SockFd;

    if ((MSDP_PEER_MAX_RX_TX_BUF_LEN - pPeer->TxBufInfo.u2MsgSize) < u2MsgLen)
    {
        /* no buffer to hold if the send fails. dont try to send */

        MSDP_TRC_FUNC ((MSDP_TRC_OUT, "\nMsdpOutSendMsgtoPeer: No buffer to"
                        " hold the info, Free Buf size %d, Msg Len %d",
                        (MSDP_PEER_MAX_RX_TX_BUF_LEN -
                         pPeer->TxBufInfo.u2MsgSize), u2MsgLen));

        MSDP_TRC_FUNC ((MSDP_TRC_OUT | MSDP_FN_EXIT,
                        "\nEXIT: MsdpOutSendMsgtoPeer"));

        return OSIX_FAILURE;
    }

    if (u2MsgOffset + u2MsgLen >= u2BufSize)
    {
        /* the msg is wrapped around */
        /* As the message is wrapped around in the buffer,
         * finding the number of bytes present in the last hakf of the 
         * buffer.*/
        /* (ie) bytes from the u2MsgOffset  to the end of buffer */
        /* Sending #u2BytesToWrite bytes in the socket. */

        u2BytesToWrite = (UINT2) (u2BufSize - u2MsgOffset);
        if (MsdpSockSend (i4SockFd, (pu1Buf + u2MsgOffset), u2BytesToWrite,
                          &i4WrBytes) != OSIX_SUCCESS)
        {
            MsdpPeerRunPfsm (pPeer, MSDP_PEER_STOP_EVT);
            /*Send failed */
            MSDP_TRC_FUNC ((MSDP_TRC_OUT, "\nMsdpOutSendMsgtoPeer, "
                            "Msg wrapped around case: MsdpSockSend returns failure"));

            return OSIX_FAILURE;
        }

        MSDP_TRC_FUNC ((MSDP_TRC_OUT,
                        "\nMsdpOutSendMsgtoPeer: Sock Send Success "
                        "for the Peer %s, Sent  Bytes %d",
                        pPeer->MibObject.au1FsMsdpPeerLocalAddress, i4WrBytes));

        u2MsgOffset += i4WrBytes;
        u2MsgLen -= i4WrBytes;
        if (u2BytesToWrite > i4WrBytes)
        {

            MSDP_TRC_FUNC ((MSDP_TRC_OUT, "\nMsdpOutSendMsgtoPeer: Bytes Sent "
                            "%d, Bytes to send %d, Copied rest of the bytes "
                            "in Peer Tx Buf", i4WrBytes, u2BytesToWrite));

            MsdpOutCopyMsgInPeerTxBuf (pPeer, pu1Buf, u2MsgOffset,
                                       u2MsgLen, u2BufSize);
        }
        else
        {
            /* Here the remaining data in  the buffer, from buffer 
             * starting point to the given length u2MsgLen is to be sent*/

            u2MsgOffset = 0;    /* from the beginning */
            u2BytesToWrite = (UINT2) (u2MsgLen - (UINT2) i4WrBytes);
            u2MsgLen -= i4WrBytes;

            if (MsdpSockSend (i4SockFd, (pu1Buf + u2MsgOffset), u2BytesToWrite,
                              &i4WrBytes) != OSIX_SUCCESS)
            {
                MsdpPeerRunPfsm (pPeer, MSDP_PEER_STOP_EVT);

                MSDP_TRC_FUNC ((MSDP_TRC_OUT, "\nMsdpOutSendMsgtoPeer: "
                                "MsdpSockSend returns failure"));

                return OSIX_FAILURE;
            }

            u2MsgOffset += i4WrBytes;
            u2MsgLen -= i4WrBytes;

            if (u2BytesToWrite > i4WrBytes)
            {

                MSDP_TRC_FUNC ((MSDP_TRC_OUT,
                                "\nMsdpOutSendMsgtoPeer: Bytes Sent %d, Bytes "
                                "to send %d, Copied rest of the bytes in "
                                "Peer Tx Buf", i4WrBytes, u2BytesToWrite));

                MsdpOutCopyMsgInPeerTxBuf (pPeer, pu1Buf, u2MsgOffset,
                                           u2MsgLen, u2BufSize);
            }

            MSDP_TRC_FUNC ((MSDP_TRC_OUT, "\nMsdpOutSendMsgtoPeer: Sock Send "
                            "Success. Bytes Sent %d, Bytes to send 0,",
                            i4WrBytes));
        }
    }
    else
    {
        /* the msg is contiguous */
        u2BytesToWrite = u2MsgLen;
        if (MsdpSockSend (i4SockFd, (pu1Buf + u2MsgOffset), u2BytesToWrite,
                          &i4WrBytes) != OSIX_SUCCESS)
        {
            MsdpPeerRunPfsm (pPeer, MSDP_PEER_STOP_EVT);

            MSDP_TRC_FUNC ((MSDP_TRC_OUT, "\nMsdpOutSendMsgtoPeer: Msg is "
                            "contiguous case: MsdpSockSend returns failure"));

            return OSIX_FAILURE;
        }

        u2MsgOffset += i4WrBytes;
        u2MsgLen -= i4WrBytes;

        if (u2BytesToWrite > i4WrBytes)
        {

            MSDP_TRC_FUNC ((MSDP_TRC_OUT, "\nMsdpOutSendMsgtoPeer: Bytes Sent "
                            "%d, Bytes to send %d, Copied rest of the bytes in "
                            "Peer Tx Buf", i4WrBytes, u2BytesToWrite));

            MsdpOutCopyMsgInPeerTxBuf (pPeer, pu1Buf, u2MsgOffset,
                                       u2MsgLen, u2BufSize);
        }

    }

    pPeer->TxBufInfo.u2MsgOffset = u2MsgOffset;
    pPeer->TxBufInfo.u2MsgSize = u2MsgLen;

    if (u2MsgLen != 0)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_OUT, "\nMsdpOutSendMsgtoPeer: More Bytes to "
                        "Send %d invoking SelAddWrFd", u2MsgLen));

        if (SelAddWrFd (i4SockFd, MsdpSockIndicateSockWriteEvt) < 0)
        {
            MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockHandleNewPassiveConnEvt: "
                            "SelAddFd failure for writing to a new "
                            "IPv4 client connection"));
        }

    }
    else
    {

        MSDP_TRC_FUNC ((MSDP_TRC_OUT,
                        "\nMsdpOutSendMsgtoPeer: Sock Send Success"
                        ". Bytes Sent %d, Bytes to send 0,", i4WrBytes));
    }

    MSDP_TRC_FUNC ((MSDP_TRC_OUT | MSDP_FN_EXIT,
                    "\nEXIT: MsdpOutSendMsgtoPeer"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdpOutSendResidualMsgtoPeer
 *
 * DESCRIPTION   : This function sends a residual message to the peer
 *
 * INPUTS        : pPeer- MSDP peer entry
 *
 * OUTPUTS       : pPeer- Message offset and message length are modified. 
 *                 MsdpPeerRunPfsm further modifies pPeer
 *
 * RETURNS       : OSIX_FAILURE if MsdpSockSend fails
 *                 OSIX_SUCCESS otherwise
 *
 ***************************************************************************/
INT4
MsdpOutSendResidualMsgtoPeer (tMsdpFsMsdpPeerEntry * pPeer)
{
    UINT1              *pu1Buf = pPeer->TxBufInfo.pu1Msg;
    INT4                i4SockFd = MSDP_INVALID_SOCK_ID;
    INT4                i4WrBytes = 0;
    UINT2               u2MsgOffset = pPeer->TxBufInfo.u2MsgOffset;
    UINT2               u2MsgLen = pPeer->TxBufInfo.u2MsgSize;
    UINT2               u2BufSize = MSDP_PEER_MAX_RX_TX_BUF_LEN;
    UINT2               u2BytesToWrite = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_OUT | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpOutSendResidualMsgtoPeer"));

    i4SockFd = pPeer->i4SockFd;
    if (u2MsgOffset + u2MsgLen >= u2BufSize)
    {
        /* the msg is wrapped around */

        u2BytesToWrite = (UINT2) (u2BufSize - u2MsgOffset);
        if (MsdpSockSend (i4SockFd, (pu1Buf + u2MsgOffset), u2BytesToWrite,
                          &i4WrBytes) != OSIX_SUCCESS)
        {
            MsdpPeerRunPfsm (pPeer, MSDP_PEER_STOP_EVT);

            MSDP_TRC_FUNC ((MSDP_TRC_OUT, "\nMsdpOutSendResidualMsgtoPeer, Msg "
                            "wrapped arnd case: MsdpSockSend returns failure"));

            return OSIX_FAILURE;
        }

        MSDP_TRC_FUNC ((MSDP_TRC_OUT,
                        "\nMsdpOutSendResidualMsgtoPeer: Sock Send Success "
                        "for the Peer %s, Sent  Bytes %d",
                        pPeer->MibObject.au1FsMsdpPeerLocalAddress, i4WrBytes));

        u2MsgOffset += i4WrBytes;
        u2MsgLen -= i4WrBytes;

        if (u2BytesToWrite > i4WrBytes)
        {

            MSDP_TRC_FUNC ((MSDP_TRC_OUT,
                            "\nMsdpOutSendResidualMsgtoPeer: Bytes Sent "
                            "%d, Bytes to send %d, Copied rest of the bytes in "
                            "Peer Tx Buf", i4WrBytes, u2BytesToWrite));

        }
        else
        {
            /* bytes from offset to the end of buffer has been sent */
            u2MsgOffset = 0;    /* from the beginning */
            u2BytesToWrite = (UINT2) (u2MsgLen - (UINT2) i4WrBytes);
            u2MsgLen -= i4WrBytes;

            if (MsdpSockSend (i4SockFd, (pu1Buf + u2MsgOffset), u2BytesToWrite,
                              &i4WrBytes) != OSIX_SUCCESS)
            {
                MsdpPeerRunPfsm (pPeer, MSDP_PEER_STOP_EVT);

                MSDP_TRC_FUNC ((MSDP_TRC_OUT, "\nMsdpOutSendResidualMsgtoPeer, "
                                "MsdpSockSend returns failure"));

                return OSIX_FAILURE;
            }

            u2MsgOffset += i4WrBytes;
            u2MsgLen -= i4WrBytes;

            if (u2BytesToWrite > i4WrBytes)
            {

                MSDP_TRC_FUNC ((MSDP_TRC_OUT,
                                "\nMsdpOutSendResidualMsgtoPeer: Bytes Sent "
                                "%d, Bytes to send %d, Copied rest of the bytes"
                                " in Peer Tx Buf", i4WrBytes, u2BytesToWrite));

                MsdpOutCopyMsgInPeerTxBuf (pPeer, pu1Buf, u2MsgOffset,
                                           u2MsgLen, u2BufSize);
            }

            MSDP_TRC_FUNC ((MSDP_TRC_OUT,
                            "\nMsdpOutSendResidualMsgtoPeer: Sock Send "
                            "Success. Bytes Sent %d, Bytes to send 0,",
                            i4WrBytes));

        }
    }
    else
    {
        u2BytesToWrite = u2MsgLen;
        /* the msg is contiguous */
        if (MsdpSockSend (i4SockFd, (pu1Buf + u2MsgOffset), u2BytesToWrite,
                          &i4WrBytes) != OSIX_SUCCESS)
        {
            MsdpPeerRunPfsm (pPeer, MSDP_PEER_STOP_EVT);

            MSDP_TRC_FUNC ((MSDP_TRC_OUT, "\nMsdpOutSendResidualMsgtoPeer, "
                            "Msg contiguous case: MsdpSockSend returns failure"));

            return OSIX_FAILURE;
        }

        u2MsgOffset += i4WrBytes;
        u2MsgLen -= i4WrBytes;

        if (u2BytesToWrite > i4WrBytes)
        {

            MSDP_TRC_FUNC ((MSDP_TRC_OUT,
                            "\nMsdpOutSendResidualMsgtoPeer: Sock Send "
                            "Success. Bytes Sent %d, Bytes to send %d,"
                            "Copied rest of the bytes in Peer Tx Buf",
                            i4WrBytes, u2BytesToWrite));

            MsdpOutCopyMsgInPeerTxBuf (pPeer, pu1Buf, u2MsgOffset,
                                       u2MsgLen, u2BufSize);

        }

        MSDP_TRC_FUNC ((MSDP_TRC_OUT,
                        "\nMsdpOutSendResidualMsgtoPeer: Sock Send Success"
                        ". Bytes Sent %d, Bytes to send 0,", i4WrBytes));

    }

    pPeer->TxBufInfo.u2MsgOffset = u2MsgOffset;
    pPeer->TxBufInfo.u2MsgSize = u2MsgLen;

    if (u2MsgLen != 0)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_OUT,
                        "\nMsdpOutSendResidualMsgtoPeer: More Bytes to "
                        "Send %d invoking SelAddWrFd", u2MsgLen));

        if (SelAddWrFd (i4SockFd, MsdpSockIndicateSockWriteEvt) < 0)
        {
            MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nnMsdpOutSendResidualMsgtoPeer: "
                            "SelAddFd failure for writing to a new "
                            "IPv4 client connection"));
        }

    }

    MSDP_TRC_FUNC ((MSDP_TRC_OUT | MSDP_FN_EXIT,
                    "\nEXIT: MsdpOutSendResidualMsgtoPeer"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdpOutChkPeerInMeshGrp
 *
 * DESCRIPTION   : This function checks whether the peer is in a mesh group
 *
 * INPUTS        : pPeer- MSDP peer entry
 *                 pRcvPeer- Received peer entry
 *
 * OUTPUTS       : None
 *
 * RETURNS       : MSDP_PEER_DONT_FORWARD always
 *
 ***************************************************************************/
INT4
MsdpOutChkPeerInMeshGrp (tMsdpFsMsdpPeerEntry * pRcvPeer,
                         tMsdpFsMsdpPeerEntry * pPeer)
{
    tMsdpFsMsdpMeshGroupEntry curMesh;
    tMsdpFsMsdpMeshGroupEntry *pNextMesh = NULL;
    INT4                i4RetVal = 0;

    if (pRcvPeer == NULL)
    {
        return MSDP_PEER_FORWARD;
    }
    if (pPeer == NULL)
    {
        return MSDP_PEER_DONT_FORWARD;
    }

    MEMSET (&curMesh, 0, sizeof (curMesh));
    curMesh.MibObject.i4FsMsdpMeshGroupAddrType = pRcvPeer->MibObject.
        i4FsMsdpPeerAddrType;

    while (OSIX_TRUE == OSIX_TRUE)
    {
        pNextMesh = MsdpGetNextFsMsdpMeshGroupTable (&curMesh);
        if (pNextMesh == NULL)
        {
            /* Mesh group table returns NULL,
             * which indicates  no entry configured.So forward the pkt*/
            i4RetVal = MSDP_PEER_FORWARD;
            break;
        }

        MEMCPY (&curMesh, pNextMesh, sizeof (curMesh));

        if (pRcvPeer->MibObject.i4FsMsdpPeerAddrType !=
            pNextMesh->MibObject.i4FsMsdpMeshGroupAddrType)
        {
            /* Address type of the peer from which received 
             * is not matching.So get next entry till the type matches*/
            continue;
        }
        i4RetVal = MEMCMP (pNextMesh->MibObject.au1FsMsdpMeshGroupPeerAddress,
                           pRcvPeer->MibObject.au1FsMsdpPeerRemoteAddress,
                           pRcvPeer->MibObject.i4FsMsdpPeerRemoteAddressLen);
        if (i4RetVal != 0)
        {
            /*Address of the peer from which received doesnot match.
             *So get next entry till the address matches*/
            continue;
        }

        /** As the address type and address of the peer from which received
         * matches with mesh group address type and mesh group peer address,
         *  this peer belongs to this mesh group.
         *  Now search for the peer to be forwarded in the mesh group table 
         *  with the mesh group name that the received peer belongs to.*/
        i4RetVal = MSDP_PEER_DONT_FORWARD;
        MEMCPY (curMesh.MibObject.au1FsMsdpMeshGroupPeerAddress,
                pPeer->MibObject.au1FsMsdpPeerRemoteAddress,
                pPeer->MibObject.i4FsMsdpPeerRemoteAddressLen);

        pNextMesh = MsdpGetFsMsdpMeshGroupTable (&curMesh);

        if (pNextMesh == NULL)
        {
            /* Peer to be forwarded is not exist in the
             * Mesh group of the Peer from which received*/
            i4RetVal = MSDP_PEER_FORWARD;
        }

        break;
    }

    return i4RetVal;
}

/****************************************************************************
 * FUNCTION NAME : MsdpOutPeerKeepAlivePkt
 *
 * DESCRIPTION   : This function forms the KeepAlive TLV and forwards it
 *                 to the peer for which keepalive period expired.
 *
 * INPUTS        : pPeer- MSDP peer entry
 *
 * OUTPUTS       : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/
VOID
MsdpOutPeerKeepAlivePkt (tMsdpFsMsdpPeerEntry * pPeer)
{
    INT4                i4RetVal = 0;
    UINT2               u2Length = MSDP_KEEP_ALIVE_PKT_SIZE;

    /* Assigning Keepalive TLV type */
    gau1LinBuf[0] = MSDP_TLV_KEEPALIVE;
    u2Length = OSIX_HTONS (u2Length);
    /* Assigning KeepAlive TLV length value */
    MEMCPY (&gau1LinBuf[1], (UINT1 *) &u2Length, sizeof (UINT2));

    i4RetVal = MsdpOutSendMsgtoPeer (pPeer, gau1LinBuf, 0,
                                     (UINT2) MSDP_KEEP_ALIVE_PKT_SIZE,
                                     MSDP_PEER_MAX_TEMP_TX_BUF_LEN);
    if (i4RetVal == OSIX_SUCCESS)
    {
        if (pPeer->MibObject.i4FsMsdpPeerKeepAliveConfigured != 0)
        {
            MsdpTmrStartTmr (&pPeer->MsdpPeerKATimer, MSDP_KEEP_ALIVE_TMR,
                             (UINT4) pPeer->MibObject.
                             i4FsMsdpPeerKeepAliveConfigured);
        }
        MsdpOutUpdatePeerStatistics (pPeer, gau1LinBuf, u2Length);
    }
}
