/********************************************************************
 * * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * *
 * * $Id: msdpsock.c,v 1.6 2012/03/06 11:26:05 siva Exp $
 * *
 * * Description: This file contains the Msdp TCP related routines
 * *********************************************************************/
#include "msdpinc.h"
#include "fssocket.h"

/****************************************************************************
 * FUNCTION NAME : MsdpSockServerStart
 *
 * DESCRIPTION   : This function creates IPv4 server socket. This function is
 *                 called when the MSDP-IPv4 is enabled 
 *
 * INPUTS        : u2Port- Port number
 *
 * OUTPUTS       :  gMsdpGlobals.i4MsdpSrvSockId updated with created socket ID
 *
 * RETURNS       : OSIX_SUCCESS if successfully created and set up
 *                 OSIX_FAILURE otherwise
 *
 ***************************************************************************/
INT4
MsdpSockServerStart (UINT2 u2Port)
{
    INT4                i4SockFd = MSDP_INVALID_SOCK_ID;
    INT4                i4Flags = MSDP_INVALID;
    INT4                i4RetVal = 0;
    struct sockaddr_in  ServAddr;

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpSockServerStart"));

    /* Open the tcp socket */
    i4SockFd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4SockFd < 0)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockServerStart: "
                        "Server socket creation failed"));

        return OSIX_FAILURE;
    }
    MEMSET (&ServAddr, 0, sizeof (ServAddr));
    ServAddr.sin_family = AF_INET;
    ServAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
    ServAddr.sin_port = OSIX_HTONS (u2Port);

    i4RetVal = bind (i4SockFd, (struct sockaddr *) &ServAddr,
                     sizeof (ServAddr));
    if (i4RetVal < 0)
    {
        close (i4SockFd);

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockServerStart: "
                        "Server socket bind failure"));

        return OSIX_FAILURE;
    }
    /* Get current socket flags */
    if ((i4Flags = fcntl (i4SockFd, F_GETFL, 0)) < 0)
    {
        close (i4SockFd);

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockServerStart: "
                        "Server socket get flags returns failure"));

        return OSIX_FAILURE;
    }

    /* Set the socket in non-blocking mode */
    i4Flags |= O_NONBLOCK;
    if (fcntl (i4SockFd, F_SETFL, i4Flags) < 0)
    {
        close (i4SockFd);

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockServerStart: "
                        "Error in setting Server socket non blocking "));

        return OSIX_FAILURE;
    }
    /* Initializing the port and Binding the socket */
    i4RetVal = listen (i4SockFd, MSDP_MAX_LISTEN);
    if (i4RetVal < 0)
    {
        close (i4SockFd);

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockServerStart: "
                        "Server listen returns failure"));

        return OSIX_FAILURE;
    }
    gMsdpGlobals.i4MsdpSrvSockId = i4SockFd;

    /* Add this SockFd to SELECT library */
    SelAddFd (i4SockFd, MsdpSockV4NewPassiveConnCallBk);

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockServerStart: "
                    "Server socket created successfully Socket ID %d",
                    i4SockFd));

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT,
                    "\nEXIT: MsdpSockServerStart"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdpSock6ServerStart
 *
 * DESCRIPTION   : This function starts the IPv6 server socket
 *
 * INPUTS        : u2Port- Port number
 *
 * OUTPUTS       : None
 *
 * RETURNS       : OSIX_SUCCESS if port is created and set up successfully
 *                 OSIX_FAILURE otherwise
 *
 ***************************************************************************/
INT4
MsdpSock6ServerStart (UINT2 u2Port)
{
    INT4                i4SockFd = MSDP_INVALID_SOCK_ID;
    INT4                i4Flags = MSDP_INVALID;
    INT4                i4RetVal = 0;
    INT4                i4OptVal = 1;
    struct sockaddr_in6 ServAddr;

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpSock6ServerStart"));

    /* Open the tcp socket with INET6 addr family */
    i4SockFd = socket (AF_INET6, SOCK_STREAM, IPPROTO_TCP);
    if (i4SockFd < 0)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSock6ServerStart: "
                        "Server socket creation failed"));

        return OSIX_FAILURE;
    }

    if (OSIX_SUCCESS != setsockopt (i4SockFd, IPPROTO_IPV6, IPV6_V6ONLY,
                                    &i4OptVal, sizeof (i4OptVal)))
    {
        close (i4SockFd);

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSock6ServerStart: "
                        "setsockopt ret true"));

        return OSIX_FAILURE;
    }

    /* Get current socket flags */
    if ((i4Flags = fcntl (i4SockFd, F_GETFL, 0)) < 0)
    {
        close (i4SockFd);

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSock6ServerStart: "
                        "Server socket get flags returns failure"));

        return OSIX_FAILURE;
    }

    /* Set the socket in non-blocking mode */
    i4Flags |= O_NONBLOCK;
    if (fcntl (i4SockFd, F_SETFL, i4Flags) < 0)
    {
        close (i4SockFd);

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSock6ServerStart: "
                        "Error in setting Server socket non blocking"));

        return OSIX_FAILURE;
    }

    /* Initializing and binding the port with the socket */
    MEMSET (&ServAddr, 0, sizeof (ServAddr));
    ServAddr.sin6_family = AF_INET6;
    /*  ServAddr.sin6_addr.s6_addr = OSIX_HTONL (INADDR_ANY); */
    inet_pton (AF_INET6, (const CHR1 *) "0::0", &ServAddr.sin6_addr.s6_addr);
    ServAddr.sin6_port = OSIX_HTONS (u2Port);

    i4RetVal = bind (i4SockFd, (struct sockaddr *) &ServAddr,
                     sizeof (ServAddr));
    if (i4RetVal < 0)
    {
        close (i4SockFd);

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSock6ServerStart: "
                        "Server socket bind failure"));

        return OSIX_FAILURE;
    }

    i4RetVal = listen (i4SockFd, MSDP_MAX_LISTEN);
    if (i4RetVal < 0)
    {
        close (i4SockFd);

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSock6ServerStart: "
                        "Server listen returns failure"));

        return OSIX_FAILURE;
    }

    gMsdpGlobals.i4Msdpv6SrvSockId = i4SockFd;

    /* Add this SockFd to SELECT library */
    SelAddFd (i4SockFd, MsdpSockV6NewPassiveConnCallBk);

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSock6ServerStart: "
                    "Server socket creation success Socket Id = %d", i4SockFd));

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT,
                    "\nEXIT: MsdpSock6ServerStart"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdpSockServerStop
 *
 * DESCRIPTION   : This function terminates the established socket connection
 *
 * INPUTS        : u1AddrType0- Address type
 *
 * OUTPUTS       : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/
VOID
MsdpSockServerStop (UINT1 u1AddrType)
{
    INT4                i4SockFd = MSDP_INVALID_SOCK_ID;

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpSockServerStop"));

    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        i4SockFd = gMsdpGlobals.i4MsdpSrvSockId;
    }
    else
    {
        i4SockFd = gMsdpGlobals.i4Msdpv6SrvSockId;
    }
    SelRemoveFd (i4SockFd);
    close (i4SockFd);

    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        gMsdpGlobals.i4MsdpSrvSockId = MSDP_INVALID_SOCK_ID;
    }
    else
    {
        gMsdpGlobals.i4Msdpv6SrvSockId = MSDP_INVALID_SOCK_ID;
    }

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSock6ServerStart: "
                    "Server socket Socket Id = %d closed", i4SockFd));

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT,
                    "\nEXIT: MsdpSock6ServerStart"));

    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpSockV4NewPassiveConnCallBk
 *
 * DESCRIPTION   : This function is the call back funtion for the SLI to call
 *                 when the new TCP connection established. This function 
 *                 posts an event to MSDP
 *
 * INPUTS        : i4SockFd- Socket descriptor
 *
 * OUTPUTS       : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/
VOID
MsdpSockV4NewPassiveConnCallBk (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpSockV4NewPassiveConnCallBk"));

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK,
                    "\nMsdpSockV4NewPassiveConnCallBk: New TCP Client connec "
                    "Rcvd "));

    if (OsixSendEvent (SELF, (const UINT1 *) MSDP_TASK_NAME,
                       MSDP_V4_PASSIVE_CONN_EVT) != OSIX_SUCCESS)
    {
        MSDP_TRC_FUNC ((MSDP_TRC_SOCK,
                        "OsixSendEvent for MSDP_V4_PASSIVE_CONN_EVT fails"));

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT,
                        "\nEXIT: MsdpSockV4NewPassiveConnCallBk"
                        "OsixSendEvent, event MSDP_V4_PASSIVE_CONN_EVT fails"));

    }
    else
    {
        MSDP_TRC_FUNC ((MSDP_TRC_SOCK,
                        "OsixSendEvent for MSDP_V4_PASSIVE_CONN_EVT Success"));
    }

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT,
                    "\nEXIT: MsdpSockV4NewPassiveConnCallBk"));

    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpSockV6NewPassiveConnCallBk
 *
 * DESCRIPTION   : This function is the call back funtion for the SLI to call
 *                 when the new TCP connection established. This function 
 *                 posts an event to MSDP
 *
 * INPUTS        : i4SockFd- Socket descriptor
 *
 * OUTPUTS       : None
 *
 * RETURNS       : None
 * 
 ***************************************************************************/
VOID
MsdpSockV6NewPassiveConnCallBk (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpSockV6NewPassiveConnCallBk"));

    if (OsixSendEvent (SELF, (const UINT1 *) MSDP_TASK_NAME,
                       MSDP_V6_PASSIVE_CONN_EVT) != OSIX_SUCCESS)
    {
        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockV6NewPassiveConnCallBk: "
                        "OsixSendEvent, event MSDP_V6_PASSIVE_CONN_EVT fails"));
    }
    else
    {
        MSDP_TRC_FUNC ((MSDP_TRC_SOCK,
                        "OsixSendEvent for MSDP_V6_PASSIVE_CONN_EVT Success"));
    }

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT,
                    "\nEXIT: MsdpSockV6NewPassiveConnCallBk"));
    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpSockHandleNewPassiveConnEvt
 *
 * DESCRIPTION   : This function handles a new IPv4 passive connection. This 
 *                 function is called when the MSDP taks receives 
 *                 MSDP_V4_PASSIVE_CONN_EVT. This function accepts the new 
 *                 Client connection and SelAddFd to receive packets from
 *                 new client and SelAddFd for accepting durthur client 
 *                 connection
 *
 * INPUTS        : i4SrvSockFd- Socket descriptor
 *
 * OUTPUTS       : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/

VOID
MsdpSockHandleNewPassiveConnEvt (INT4 i4SrvSockFd)
{
    tIPvXAddr           PeerAddr;
    INT4                i4ClientSockFd = MSDP_INVALID_SOCK_ID;

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpSockHandleNewPassiveConnEvt"));

    MEMSET (&PeerAddr, 0, sizeof (tIPvXAddr));
    if ((MsdpSockAcceptNewConnection
         (i4SrvSockFd, &i4ClientSockFd, &PeerAddr) == OSIX_SUCCESS))
    {
        if (MsdpPeerValidateAndAcceptPeer (&PeerAddr, IPVX_ADDR_FMLY_IPV4,
                                           i4ClientSockFd) != OSIX_FAILURE)
        {
            if (SelAddFd (i4ClientSockFd, MsdpSockIndicateSockReadEvt)
                == OSIX_FAILURE)
            {

                MSDP_TRC_FUNC ((MSDP_TRC_SOCK,
                                "\nMsdpSockHandleNewPassiveConnEvt: "
                                "SelAddFd failure to get pkt from "
                                "IPv4 client socket id %d", i4ClientSockFd));

            }
            else
            {
                MSDP_TRC_FUNC ((MSDP_TRC_SOCK,
                                "\nMsdpSockHandleNewPassiveConnEvt: "
                                "SelAddFd Success to get pkt from "
                                "IPv4 client socket id %d", i4ClientSockFd));
            }
        }
    }
    if (SelAddFd (i4SrvSockFd, MsdpSockV4NewPassiveConnCallBk) == OSIX_FAILURE)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockHandleNewPassiveConnEvt: "
                        "SelAddFd failure for getting a new "
                        "IPv4 client connection"));

    }
    else
    {
        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockHandleNewPassiveConnEvt: "
                        "SelAddFd Successfor getting a new "
                        "IPv4 client connection"));
    }

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT,
                    "\nEXIT: MsdpSockHandleNewPassiveConnEvt"));

    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpSock6HandleNewPassiveConnEvt
 *
 * DESCRIPTION   : This function handles a new IPv4 passive connection. This 
 *                 function is called when the MSDP taks receives 
 *                 MSDP_V6_PASSIVE_CONN_EVT. This function accepts the new 
 *                 Client connection and SelAddFd to receive packets from
 *                 new client and SelAddFd for accepting durthur client 
 *                 connection
 *
 * INPUTS        : i4SrvSockFd- Socket descriptor
 *
 * OUTPUTS       : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/
VOID
MsdpSock6HandleNewPassiveConnEvt (INT4 i4SrvSockFd)
{
    tIPvXAddr           PeerAddr;
    INT4                i4ClientSockFd = MSDP_INVALID_SOCK_ID;

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpSock6HandleNewPassiveConnEvt"));

    MEMSET (&PeerAddr, 0, sizeof (tIPvXAddr));
    if ((MsdpSock6AcceptNewConnection
         (i4SrvSockFd, &i4ClientSockFd, &PeerAddr) == OSIX_SUCCESS))
    {
        if (MsdpPeerValidateAndAcceptPeer (&PeerAddr, IPVX_ADDR_FMLY_IPV6,
                                           i4ClientSockFd) != OSIX_FAILURE)
        {
            if (SelAddFd (i4ClientSockFd, MsdpSockIndicateSockReadEvt)
                == OSIX_FAILURE)
            {

                MSDP_TRC_FUNC ((MSDP_TRC_SOCK,
                                "\nMsdpSockHandleNewPassiveConnEvt: "
                                "SelAddFd failure to get pkt from "
                                "IPv6 client socket"));

            }
        }
        else
        {

            MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockHandleNewPassiveConnEvt: "
                            "Peer not validated and accepted: %s",
                            MsdpPrintIPvxAddress (PeerAddr)));

        }
    }
    if (SelAddFd (i4SrvSockFd, MsdpSockV6NewPassiveConnCallBk) == OSIX_FAILURE)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockHandleNewPassiveConnEvt: "
                        "SelAddFd failure for getting a new "
                        "IPv6 client connection"));

    }

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT,
                    "\nEXIT: MsdpSock6HandleNewPassiveConnEvt"));

    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpSockAcceptNewConnection
 *
 * DESCRIPTION   : This function accepts a new IPv4 connection
 *
 * INPUTS        : i4SrvSockFd- Socket descriptor
 *                 
 * OUTPUTS       : pi4ClientSockFd- Client socket descriptor
 *                 pPeerAddr- Peer address
 *
 * RETURNS       : OSIX_SUCCESS upon successfully accepting the connection
 *                 OSIX_FAILURE otherwise
 *
 ***************************************************************************/
INT4
MsdpSockAcceptNewConnection (INT4 i4SrvSockFd, INT4 *pi4ClientSockFd,
                             tIPvXAddr * pPeerAddr)
{
    struct sockaddr_in  ClientSockAddr;
    INT4                i4Val = 0;
    INT4                i4CliSockFd = MSDP_INVALID_SOCK_ID;
    INT4                i4Flags = 0;
    UINT4               u4SockAddrLen = sizeof (struct sockaddr_in);
    UINT4               u4IpAddr = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpSockAcceptNewConnection"));

    if (i4SrvSockFd == -1)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockAcceptNewConnection: "
                        "Invalid Server Socket Id"));

        return OSIX_FAILURE;
    }

    MEMSET (&ClientSockAddr, 0, u4SockAddrLen);

    i4CliSockFd = accept (i4SrvSockFd, (struct sockaddr *) &ClientSockAddr,
                          &u4SockAddrLen);
    if (i4CliSockFd < 0)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockAcceptNewConnection: "
                        "Invalid client Socket id"));

        return OSIX_FAILURE;
    }

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockAcceptNewConnection: "
                    "Accepted new client Socket, id %d", i4CliSockFd));
    /* Set the socket is non-blocking mode */
    i4Flags = O_NONBLOCK;
    i4Val = fcntl (i4CliSockFd, F_SETFL, i4Flags);
    if (i4Val < 0)
    {
        close (i4CliSockFd);

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockAcceptNewConnection: "
                        "Error in setting Client socket non blocking"));

        return OSIX_FAILURE;
    }

    *pi4ClientSockFd = i4CliSockFd;

    u4IpAddr = ClientSockAddr.sin_addr.s_addr;
    IPVX_ADDR_INIT_FROMV4 ((*pPeerAddr), u4IpAddr);

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT,
                    "\nEXIT: MsdpSockAcceptNewConnection"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdpSock6AcceptNewConnection
 *
 * DESCRIPTION   : This function accepts a new IPv6 connection
 *
 * INPUTS        : i4SrvSockFd- Socket descriptor
 *
 * OUTPUTS       : pi4ClientSockFd- Client's socket descriptor
 *                 pPeerAddr- Peer address
 *
 * RETURNS       : OSIX_SUCCESS on successfully accepting the connection
 *                 OSIX_FAILURE otherwise
 *
 ***************************************************************************/
INT4
MsdpSock6AcceptNewConnection (INT4 i4SrvSockFd, INT4 *pi4ClientSockFd,
                              tIPvXAddr * pPeerAddr)
{
    struct sockaddr_in6 ClientSockAddr;
    UINT4               u4SockAddrLen = sizeof (struct sockaddr_in6);
    INT4                i4Val = 0;
    INT4                i4CliSockFd = MSDP_INVALID_SOCK_ID;
    INT4                i4Flags = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpSock6AcceptNewConnection"));

    if (i4SrvSockFd == -1)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSock6AcceptNewConnection: "
                        "i4SrvSockFd is -1"));

        return OSIX_FAILURE;
    }
    MEMSET (&ClientSockAddr, 0, u4SockAddrLen);

    i4CliSockFd = accept (i4SrvSockFd, (struct sockaddr *) &ClientSockAddr,
                          &u4SockAddrLen);
    if (i4CliSockFd < 0)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSock6AcceptNewConnection: "
                        "Invalid client socket id"));

        return OSIX_FAILURE;
    }

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSock6AcceptNewConnection: "
                    "Accepted new client Socket, id %d", i4CliSockFd));
    /* Set the socket is non-blocking mode */
    i4Flags = O_NONBLOCK;
    i4Val = fcntl (i4CliSockFd, F_SETFL, i4Flags);
    if (i4Val < 0)
    {
        close (i4CliSockFd);

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSock6AcceptNewConnection: "
                        "Error in setting Client socket non blocking"));

        return OSIX_FAILURE;
    }

    *pi4ClientSockFd = i4CliSockFd;
    IPVX_ADDR_INIT_FROMV6 ((*pPeerAddr), (&(ClientSockAddr.sin6_addr.s6_addr)));

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT,
                    "\nEXIT: MsdpSock6AcceptNewConnection"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdpSockOpenPeerConnection
 *
 * DESCRIPTION   : This function opens the IPv4 peer connection (MSDP Client)
 *
 * INPUTS        : pPeerEntry- MSDP peer entry
 *
 * OUTPUTS       : pPeerEntry- Updated with new peer sock Id
 *
 * RETURNS       : OSIX_SUCCESS on successfully opening the peer connection
 *                 OSIX_FAILURE otherwise
 *
 ***************************************************************************/
INT4
MsdpSockOpenPeerConnection (tMsdpFsMsdpPeerEntry * pPeerEntry)
{
    struct sockaddr_in  DestAddr;
    struct sockaddr_in  LocalAddr;
    INT4                i4SockFd = MSDP_INVALID_SOCK_ID;
    INT4                i4Flags = MSDP_INVALID;
    INT4                i4RetVal = 0;
    UINT4               u4Addr = 0;
    INT1                i1OptVal = 1;
    INT4                i4OptLen = sizeof (i1OptVal);
    INT4                i4SktOpenStats = MSDP_TCP_OPEN_FAILURE;
    INT4                i4Val = MSDP_INVALID;
    UINT2               u2Port = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpSockOpenPeerConnection"));

    MEMSET (&DestAddr, 0, sizeof (DestAddr));
    MEMSET (&LocalAddr, 0, sizeof (LocalAddr));

    u2Port = (UINT2) gMsdpGlobals.MsdpGlbMib.i4FsMsdpListenerPort;

    PTR_FETCH4 (u4Addr, (pPeerEntry->MibObject.au1FsMsdpPeerRemoteAddress));
    DestAddr.sin_family = AF_INET;
    DestAddr.sin_addr.s_addr = OSIX_NTOHL (u4Addr);    /*u4Addr in Net order */
    DestAddr.sin_port = OSIX_HTONS (u2Port);

    i4SockFd = pPeerEntry->i4SockFd;
    if (i4SockFd == MSDP_INVALID_SOCK_ID)
    {
        i4SockFd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
        if (i4SockFd < 0)
        {

            MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockOpenPeerConnection: "
                            "Server socket creation failed"));

            return i4SktOpenStats;
        }

        PTR_FETCH4 (u4Addr, (pPeerEntry->MibObject.au1FsMsdpPeerLocalAddress));
        LocalAddr.sin_family = AF_INET;
        LocalAddr.sin_addr.s_addr = OSIX_HTONL (u4Addr);    /*u4Addr in Net order */
        LocalAddr.sin_port = OSIX_HTONS (INADDR_ANY);

        /* Set the socket is non-blocking mode */
        i4Flags = O_NONBLOCK;
        i4Val = fcntl (i4SockFd, F_SETFL, i4Flags);
        if (i4Val < 0)
        {
            close (i4SockFd);

            MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockOpenPeerConnection: "
                            "Error in setting Server socket non blocking"));

            return i4SktOpenStats;
        }

        i4Val = setsockopt (i4SockFd, SOL_SOCKET, SO_REUSEADDR,
                            &i1OptVal, sizeof (i1OptVal));
        if (i4Val < 0)
        {
            close (i4SockFd);

            MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockOpenPeerConnection: "
                            "setsockopt returns failure"));

            return i4SktOpenStats;
        }

        i4RetVal = bind (i4SockFd, (struct sockaddr *) &LocalAddr,
                         sizeof (LocalAddr));
        if (i4RetVal < 0)
        {
            close (i4SockFd);

            MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockOpenPeerConnection: "
                            "Server socket bind failure"));

            return i4SktOpenStats;
        }
        pPeerEntry->i4SockFd = i4SockFd;
    }
    if ((i4RetVal = connect (i4SockFd, (struct sockaddr *) &DestAddr,
                             sizeof (struct sockaddr_in))) < 0)
    {
        getsockopt (i4SockFd, SOL_SOCKET, SO_ERROR, &i1OptVal, &i4OptLen);

        if ((i1OptVal == EINPROGRESS) || (i1OptVal == EALREADY) ||
            (errno == EINPROGRESS) || (errno == EALREADY))
        {
            i4SktOpenStats = MSDP_TCP_OPEN_INPROGRESS;
        }
        else if ((i1OptVal == EISCONN) || (errno == EISCONN))
        {
            /* Already connected */
            i4SktOpenStats = MSDP_TCP_OPEN_SUCCESS;
        }
    }
    else
    {
        i4SktOpenStats = MSDP_TCP_OPEN_SUCCESS;
    }

    if (i4SktOpenStats == MSDP_TCP_OPEN_SUCCESS)
    {
        if (SelAddFd (i4SockFd, MsdpSockIndicateSockReadEvt) == OSIX_FAILURE)
        {

            MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nnMsdpSockOpenPeerConnection: "
                            "SelAddFd failure to get pkt from "
                            "IPv4 client socket"));
        }
    }

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK,
                    "\nMsdpSockOpenPeerConnection: Successfully opened new peer "
                    "connection Sock Id - %d", i4SockFd));

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT,
                    "\nEXIT: MsdpSockOpenPeerConnection"));

    return i4SktOpenStats;
}

/****************************************************************************
 * FUNCTION NAME : MsdpSock6OpenPeerConnection
 *
 * DESCRIPTION   : This function opens an IPv6 peer connection (MSDP Client)
 *
 * INPUTS        : pPeerEntry- MSDP peer entry
 *
 * OUTPUTS       : pPeerEntry- Updated with socket Id for new IPv6  connection
 *
 * RETURNS       : OSIX_SUCCESS if the peer connection is successfully opened
 *                 OSIX_FAILURE otherwise
 *
 ***************************************************************************/
INT4
MsdpSock6OpenPeerConnection (tMsdpFsMsdpPeerEntry * pPeerEntry)
{
    INT4                i4SktOpenStats = MSDP_TCP_OPEN_FAILURE;
    struct sockaddr_in6 DestAddr;
    struct sockaddr_in6 LocalAddr;
    INT4                i4SockFd = MSDP_INVALID_SOCK_ID;
    INT4                i4Flags = MSDP_INVALID;
    INT4                i4RetVal = 0;
    INT1                i1OptVal = 1;
    INT4                i4OptLen = sizeof (i1OptVal);
    INT4                i4Val = MSDP_INVALID;
    UINT2               u2Port = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpSock6OpenPeerConnection"));

    MEMSET (&DestAddr, 0, sizeof (DestAddr));
    MEMSET (&LocalAddr, 0, sizeof (LocalAddr));

    u2Port = (UINT2) gMsdpGlobals.MsdpGlbMib.i4FsMsdpListenerPort;

    DestAddr.sin6_family = AF_INET6;
    DestAddr.sin6_port = OSIX_HTONS (u2Port);
    MEMCPY (DestAddr.sin6_addr.s6_addr,
            pPeerEntry->MibObject.au1FsMsdpPeerRemoteAddress,
            IPVX_IPV6_ADDR_LEN);

    i4SockFd = pPeerEntry->i4SockFd;
    if (i4SockFd == MSDP_INVALID_SOCK_ID)
    {
        i4SockFd = socket (AF_INET6, SOCK_STREAM, IPPROTO_TCP);
        if (i4SockFd < 0)
        {

            MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSock6OpenPeerConnection: "
                            "Server socket creation failed"));

            return i4SktOpenStats;
        }

        LocalAddr.sin6_family = AF_INET6;
        LocalAddr.sin6_port = OSIX_HTONS (INADDR_ANY);
        MEMCPY (LocalAddr.sin6_addr.s6_addr,
                pPeerEntry->MibObject.au1FsMsdpPeerLocalAddress,
                IPVX_IPV6_ADDR_LEN);

        /* Set the socket is non-blocking mode */
        i4Flags = O_NONBLOCK;
        i4Val = fcntl (i4SockFd, F_SETFL, i4Flags);
        if (i4Val < 0)
        {
            close (i4SockFd);

            MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSock6OpenPeerConnection: "
                            "Error in setting Server socket non blocking"));

            return i4SktOpenStats;
        }

        i4Val = setsockopt (i4SockFd, SOL_SOCKET, SO_REUSEADDR,
                            &i1OptVal, sizeof (i1OptVal));
        if (i4Val < 0)
        {
            close (i4SockFd);

            MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSock6OpenPeerConnection: "
                            "setsockopt returns failure"));

            return i4SktOpenStats;
        }

        i4RetVal = bind (i4SockFd, (struct sockaddr *) &LocalAddr,
                         sizeof (LocalAddr));
        if (i4RetVal < 0)
        {
            close (i4SockFd);

            MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSock6OpenPeerConnection: "
                            "Server socket bind failed"));

            return i4SktOpenStats;
        }
        pPeerEntry->i4SockFd = i4SockFd;
    }
    if ((i4RetVal = connect (i4SockFd, (struct sockaddr *) &DestAddr,
                             sizeof (struct sockaddr_in6))) < 0)
    {
        getsockopt (i4SockFd, SOL_SOCKET, SO_ERROR, &i1OptVal, &i4OptLen);
        if ((i1OptVal == EINPROGRESS) || (i1OptVal == EALREADY) ||
            (errno == EINPROGRESS) || (errno == EALREADY))
        {
            i4SktOpenStats = MSDP_TCP_OPEN_INPROGRESS;
        }
        else if ((i1OptVal == EISCONN) || (errno == EISCONN))
        {
            /* Already connected */
            i4SktOpenStats = MSDP_TCP_OPEN_SUCCESS;
        }
    }
    else
    {
        i4SktOpenStats = MSDP_TCP_OPEN_SUCCESS;
    }

    if (i4SktOpenStats == MSDP_TCP_OPEN_SUCCESS)
    {
        if (SelAddFd (i4SockFd, MsdpSockIndicateSockReadEvt) == OSIX_FAILURE)
        {
            MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nnMsdpSock6OpenPeerConnection: "
                            "SelAddFd failure to get pkt from "
                            "IPv6 client socket"));
        }
    }

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK,
                    "\nMsdpSock6OpenPeerConnection: Opened new IPv6 connection "
                    "Sock Id %d", i4SockFd));

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT,
                    "\nEXIT: MsdpSock6OpenPeerConnection"));
    return i4SktOpenStats;
}

/****************************************************************************
 * FUNCTION NAME : MsdpSockNewActiveConnCallBk
 *
 * DESCRIPTION   : This is the call back function registered with SLI when 
 *                 connect call retured IN PROGRESS while trying to connect
 *                 the peer 
 *
 * INPUTS        : i4SockFd- Socket descriptor
 *
 * OUTPUTS       : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/
VOID
MsdpSockNewActiveConnCallBk (INT4 i4SockFd)
{
    tMsdpQueMsg        *pQueMsg = NULL;

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpSockNewActiveConnCallBk"));

    if ((pQueMsg = MsdpQueAllocQueMsg ()) == NULL)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockNewActiveConnCallBk: "
                        "Memory not allocated"));

        return;
    }
    pQueMsg->eMsgType = MSDP_ACTIVE_CONN_EVT;
    pQueMsg->unMsdpMsgIfParam.MsdpTcpMsg.i4Cmd = MSDP_SKT_WRITE_READY;
    pQueMsg->unMsdpMsgIfParam.MsdpTcpMsg.i4SockFd = i4SockFd;

    if (MsdpQuePostQueueMsg (pQueMsg) == OSIX_FAILURE)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockNewActiveConnCallBk: "
                        "MsdpQuePostQueueMsg returns failure"));

        return;
    }
    if (OsixSendEvent (SELF, (const UINT1 *) MSDP_TASK_NAME,
                       MSDP_QUEUE_EVENT) != OSIX_SUCCESS)
    {
        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockNewActiveConnCallBk: "
                        "OsixSendEvent, event MSDP_QUEUE_EVENT fails"));
    }
    else
    {
        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockNewActiveConnCallBk: "
                        "OsixSendEvent success for the event "
                        "MSDP_ACTIVE_CONN_EVT"));
    }

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT,
                    "\nEXIT: MsdpSockNewActiveConnCallBk"));

    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpSockHandleNewActiveConnEvt
 *
 * DESCRIPTION   : This is called when the MSDP_ACTIVE_CONN_EVT is processed
 *                 This function searches for the Peer Addr and tries to 
 *                 establish the connection again
 *
 * INPUTS        : i4SockFd- Socket descriptor
 *
 * OUTPUTS       : None
 * 
 * RETURNS       : None
 *
 ***************************************************************************/
VOID
MsdpSockHandleNewActiveConnEvt (INT4 i4SockFd)
{
    tMsdpFsMsdpPeerEntry *pPeerEntry = NULL;
    tIPvXAddr           PeerAddr;
    UINT1               u1PeerEvent = MSDP_CONN_ESTAB_EVT;
    UINT1               u1AddrType = IPVX_ADDR_FMLY_IPV4;

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpSockHandleNewActiveConnEvt"));

    MEMSET (&PeerAddr, 0, sizeof (tIPvXAddr));

    if (MsdpPeerUtilFindPeerEntry (i4SockFd, &pPeerEntry) != OSIX_SUCCESS)
    {
        close (i4SockFd);

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockHandleNewActiveConnEvt: "
                        "MsdpPeerUtilFindPeerEntry failure for the socket id %d",
                        i4SockFd));

        return;
    }
    u1AddrType = (UINT1) pPeerEntry->MibObject.i4FsMsdpPeerAddrType;
    IPVX_ADDR_INIT (PeerAddr, u1AddrType,
                    pPeerEntry->MibObject.au1FsMsdpPeerRemoteAddress);
    if (MsdpPeerChkPeerFilterStatus (&PeerAddr, u1AddrType) !=
        MSDP_PEER_FILTER_ACCEPT)
    {
        MsdpPeerRunPfsm (pPeerEntry, MSDP_PEER_STOP_EVT);

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockHandleNewActiveConnEvt: "
                        "MsdpPeerChkPeerFilterStatus is in deny mode %s",
                        MsdpPrintIPvxAddress (PeerAddr)));

        return;
    }
    MsdpPeerRunPfsm (pPeerEntry, u1PeerEvent);

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT,
                    "\nEXIT: MsdpSockHandleNewActiveConnEvt"));

    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpSockClosePeerConnection
 *
 * DESCRIPTION   : This function closes the IPv4 peer connection
 *
 * INPUTS        : pPeerEntry- MSDP peer entry
 *
 * OUTPUTS       : pPeerEntry- Its socket descriptor field is modified
 *
 * RETURNS       : OSIX_SUCCESS always
 *
 ***************************************************************************/
INT4
MsdpSockClosePeerConnection (tMsdpFsMsdpPeerEntry * pPeerEntry)
{
    INT4                i4SockFd = MSDP_INVALID_SOCK_ID;

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpSockClosePeerConnection"));

    i4SockFd = pPeerEntry->i4SockFd;

    SelRemoveWrFd (i4SockFd);
    SelRemoveFd (i4SockFd);

    close (i4SockFd);
    pPeerEntry->i4SockFd = MSDP_INVALID_SOCK_ID;

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT,
                    "\nEXIT: MsdpSockClosePeerConnection"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdpSockSend
 *
 * DESCRIPTION   : This function sends the data via the socket
 *
 * INPUTS        : i4SockFd- Socket descriptor
 *                 pu1Data- Data to send
 *                 u2PktLen- Packet length
 *
 * OUTPUTS       : pi4WrBytes- Number of bytes written
 *
 * RETURNS       : OSIX_SUCCESS if the data was successfully sent
 *                 OSIX_FAILURE otherwise
 *
 ***************************************************************************/
INT4
MsdpSockSend (INT4 i4SockFd, UINT1 *pu1Data, UINT2 u2PktLen, INT4 *pi4WrBytes)
{
    INT4                i4WrBytes = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_ENTRY, "\nENTRY: MsdpSockSend"));

    if (i4SockFd == MSDP_INVALID_SOCK_ID)
    {
        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockSend: "
                        "i4SockFd is INVALID"));

        return OSIX_FAILURE;
    }

    i4WrBytes = send (i4SockFd, pu1Data, u2PktLen, MSG_NOSIGNAL);
    if (i4WrBytes < 0)
    {
        /* Other than EWOULDBLOCK OR EAGAIN cases,
         * critical error occured in the socket*/
        if ((errno != EWOULDBLOCK) && (errno != EAGAIN))
        {

            MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockSend: "
                            "Sock send failed for the socket Id %d", i4SockFd));

            return OSIX_FAILURE;
        }
        i4WrBytes = 0;
    }
    *pi4WrBytes = i4WrBytes;

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockSend: "
                    "Successfully sent %d bytes", i4WrBytes));

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT, "\nEXIT: MsdpSockSend"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : MsdpSockRcv
 *
 * DESCRIPTION   : This function receives data via the socket
 *
 * INPUTS        : i4SockFd- Socket descriptor
 *                 u2Len- Length
 *
 * OUTPUTS       : pu1Data- Data
 *                 pi4RdBytes- Number of bytes received
 *
 * RETURNS       : OSIX_SUCCESS if the data was received successfully
 *                 OSIX_FAILURE otherwise
 *
 ***************************************************************************/
INT4
MsdpSockRcv (INT4 i4SockFd, UINT1 *pu1Data, UINT2 u2Len, INT4 *pi4RdBytes)
{

    INT4                i4RdBytes = 0;

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_ENTRY, "\nENTRY: MsdpSockRcv"));

    if (i4SockFd == MSDP_INVALID_SOCK_ID)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockRcv: "
                        "i4SockFd is invalid"));

        return OSIX_FAILURE;
    }

    i4RdBytes = recv (i4SockFd, pu1Data, u2Len, 0);

    if (i4RdBytes < 0)
    {
        /* Other than EWOULDBLOCK OR EAGAIN cases,
         * critical error occured in the socket*/
        if ((errno == EWOULDBLOCK) || (errno == EAGAIN))
        {
            *pi4RdBytes = i4RdBytes;

            MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT,
                            "\nEXIT: MsdpSockRcv"));

            return OSIX_SUCCESS;
        }

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockRcv: "
                        "Sock recv failed %d", i4SockFd));

        return OSIX_FAILURE;
    }
    else if (i4RdBytes == 0)
    {
        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockRcv: "
                        "Sock Connection Closed %d", i4SockFd));
        return OSIX_FAILURE;
    }

    *pi4RdBytes = i4RdBytes;

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockSend: "
                    "Successfully Rcvd %d bytes from Sock Id %d", i4RdBytes,
                    i4SockFd));

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT, "\nEXIT: MsdpSockRcv"));

    return OSIX_SUCCESS;

}

/****************************************************************************
 * FUNCTION NAME : MsdpSockIndicateSockReadEvt
 *
 * DESCRIPTION   : This function posts an event to MSDP when the data is 
 *                 available in the socket to read. 
 *
 * INPUTS        : i4SockFd- Socket descriptor
 *
 * OUTPUTS       : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/
VOID
MsdpSockIndicateSockReadEvt (INT4 i4SockFd)
{
    tMsdpQueMsg        *pQueMsg = NULL;

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpSockIndicateSockReadEvt"));

    if ((pQueMsg = MsdpQueAllocQueMsg ()) == NULL)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockIndicateSockReadEvt: "
                        "Memory not allocated"));

        return;
    }
    pQueMsg->eMsgType = MSDP_SOCK_READ_WRITE_EVT;
    pQueMsg->unMsdpMsgIfParam.MsdpTcpMsg.i4Cmd = MSDP_SKT_READ_READY;
    pQueMsg->unMsdpMsgIfParam.MsdpTcpMsg.i4SockFd = i4SockFd;

    if (MsdpQuePostQueueMsg (pQueMsg) == OSIX_FAILURE)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockIndicateSockReadEvt: "
                        "MsdpQuePostQueueMsg returns failure"));

        return;
    }
    if (OsixSendEvent (SELF, (const UINT1 *) MSDP_TASK_NAME,
                       MSDP_QUEUE_EVENT) != OSIX_SUCCESS)
    {
        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockIndicateSockReadEvt: "
                        "OsixSendEvent, event MSDP_QUEUE_EVENT fails"));
    }
    else
    {
        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockIndicateSockReadEvt: "
                        "OsixSendEvent success for MSDP_SOCK_READ_WRITE_EVT, "
                        "Command MSDP_SKT_READ_READY"));
    }

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT,
                    "\nEXIT: MsdpSockIndicateSockReadEvt"));

    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpSockIndicateSockWriteEvt
 *
 * DESCRIPTION   : This function is called when the socket is ready for 
 *                 writing
 *
 * INPUTS        : i4SockFd- Socket descriptor
 *
 * OUTPUTS       : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/
VOID
MsdpSockIndicateSockWriteEvt (INT4 i4SockFd)
{
    tMsdpQueMsg        *pQueMsg = NULL;

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpSockIndicateSockWriteEvt"));

    if ((pQueMsg = MsdpQueAllocQueMsg ()) == NULL)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockIndicateSockWriteEvt: "
                        "Memory not allocated"));

        return;
    }
    pQueMsg->eMsgType = MSDP_SOCK_READ_WRITE_EVT;
    pQueMsg->unMsdpMsgIfParam.MsdpTcpMsg.i4Cmd = MSDP_SKT_WRITE_READY;
    pQueMsg->unMsdpMsgIfParam.MsdpTcpMsg.i4SockFd = i4SockFd;

    if (MsdpQuePostQueueMsg (pQueMsg) == OSIX_FAILURE)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockIndicateSockWriteEvt: "
                        "MsdpQuePostQueueMsg returns failure"));

        return;
    }
    if (OsixSendEvent (SELF, (const UINT1 *) MSDP_TASK_NAME,
                       MSDP_QUEUE_EVENT) != OSIX_SUCCESS)
    {
        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockIndicateSockWriteEvt: "
                        "OsixSendEvent, event MSDP_QUEUE_EVENT fails"));
    }
    else
    {
        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockIndicateSockReadEvt: "
                        "OsixSendEvent success for MSDP_SOCK_READ_WRITE_EVT, "
                        "Command MSDP_SKT_WRITE_READY"));
    }

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT,
                    "\nEXIT: MsdpSockIndicateSockWriteEvt"));

    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpSockHandleSockReadEvt
 *
 * DESCRIPTION   : This function handles the socket read. This function is 
 *                 called when MSDP processes 
 *                 MSDP_SKT_READ_READY command in MSDP_SOCK_READ_WRITE_EVT
 *
 * INPUTS        : i4SockFd- Socket descriptor
 *
 * OUTPUTS       : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/
VOID
MsdpSockHandleSockReadEvt (INT4 i4SockFd)
{
    tMsdpFsMsdpPeerEntry *pPeerEntry = NULL;

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpSockHandleSockReadEvt"));

    if (MsdpPeerUtilFindPeerEntry (i4SockFd, &pPeerEntry) != OSIX_SUCCESS)
    {
        close (i4SockFd);

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockHandleSockReadEvt: "
                        "MsdpPeerUtilFindPeerEntry returns failure"
                        "for i4SockFd %d", i4SockFd));

        return;
    }
    if (MsdpInReadPeerMsg (pPeerEntry) == OSIX_FAILURE)
    {

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockHandleSockReadEvt: "
                        "MsdpInReadPeerMsg returns failure"));

        return;
    }
    if (MsdpInProcessRxdMsg (pPeerEntry) == OSIX_FAILURE)
    {
        return;
    }

    if (SelAddFd (i4SockFd, MsdpSockIndicateSockReadEvt) == OSIX_FAILURE)
    {
        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockHandleNewPassiveConnEvt: "
                        "SelAddFd failure to get pkt from "
                        "IPv4 client socket"));
    }

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT,
                    "\nEXIT: MsdpSockHandleSockReadEvt"));

    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpSockHandleSockWriteEvt
 *
 * DESCRIPTION   : This function handles the socket read. This function is 
 *                 called when MSDP processes 
 *                 MSDP_SKT_WRITE_READY command in MSDP_SOCK_READ_WRITE_EVT
 *
 * INPUTS        : i4SockFd- Socket descriptor
 *
 * OUTPUTS       : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/
VOID
MsdpSockHandleSockWriteEvt (INT4 i4SockFd)
{
    tMsdpFsMsdpPeerEntry *pPeerEntry = NULL;

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpSockHandleSockWriteEvt"));

    if (MsdpPeerUtilFindPeerEntry (i4SockFd, &pPeerEntry) != OSIX_SUCCESS)
    {
        close (i4SockFd);

        MSDP_TRC_FUNC ((MSDP_TRC_SOCK, "\nMsdpSockHandleSockWriteEvt: "
                        "MsdpPeerUtilFindPeerEntry returns failure"
                        " for i4SockFd %d", i4SockFd));

        return;
    }
    MsdpOutSendResidualMsgtoPeer (pPeerEntry);

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT,
                    "\nEXIT: MsdpSockHandleSockWriteEvt"));

    return;
}

/****************************************************************************
 * FUNCTION NAME : MsdpSockHandleSockReadWriteEvt
 *
 * DESCRIPTION   : This function handles the socket read write event
 *
 * INPUTS        : pMsdpTcpMsg- TCP message
 *
 * OUTPUTS       : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/
VOID
MsdpSockHandleSockReadWriteEvt (tMsdpTcpMsg * pMsdpTcpMsg)
{

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpSockHandleSockReadWriteEvt"));

    if (pMsdpTcpMsg->i4Cmd == MSDP_SKT_READ_READY)
    {
        MsdpSockHandleSockReadEvt (pMsdpTcpMsg->i4SockFd);
    }
    else
    {
        MsdpSockHandleSockWriteEvt (pMsdpTcpMsg->i4SockFd);
    }

    MSDP_TRC_FUNC ((MSDP_TRC_SOCK | MSDP_FN_EXIT,
                    "\nEXIT: MsdpSockHandleSockReadWriteEvt"));

    return;
}
