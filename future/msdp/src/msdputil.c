/******************************************************************************
 * Copyright (C) Future Software Limited,2005
 *
 *$Id: msdputil.c,v 1.12 2014/03/14 12:56:29 siva Exp $
 *
 * Description : This file contains the utility 
 *               functions of the MSDP module
 *****************************************************************************/

#include "msdpinc.h"
#include "fsmsdp.h"
#include "snmputil.h"

/* Proto types of the functions private to this file only */

UINT4               gaMSDP_TRAP_OID[] =
    { 1, 3, 6, 1, 4, 1, 29601, 2, 61, 1, 0 };
UINT4               gaMSDP_SNMP_TRAP_OID[] =
    { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };

/****************************************************************************
 *    FUNCTION NAME    : MsdpUtilSendEventTrap 
 *    DESCRIPTION      : This fun calls MsdpUtilSnmpIfSendTrap to send the trap
 *                       for the events
 *    INPUT            : TrapEvent - the Msdp event 
 *    OUTPUT           : NONE
 *    RETURNS          : NONE 
 ****************************************************************************/
VOID
MsdpUtilSendEventTrap (tMsdpEventTrap * pTrapEvent, UINT4 u4TrapId)
{
    UINT4               u4Value = 0;
    MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_ENTRY,
                    "MsdpUtilSendEventTrap: Entry \r\n"));

    MsdpPortGetRouterId (MSDP_DEF_VRF_CTXT_ID, &u4Value);
    PTR_ASSIGN4 (pTrapEvent->au1RtrId, u4Value);

    MsdpUtilSnmpIfSendTrap ((UINT1) u4TrapId, pTrapEvent);

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC,
                    "MsdpUtilSendEventTrap: sent Msdp trap Event %d\r\n",
                    pTrapEvent->u4EventId));

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_EXIT,
                    "MsdpUtilSendEventTrap: Exit \r\n"));
    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : MsdpUtilSnmpIfSendTrap                                  */
/*                                                                            */
/*  Description     : Generates trap and sends to SNMP manager.               */
/*                                                                            */
/*  Input(s)        : u1TrapId:  Trap ID representing the type of trap.       */
/*                    pTrapInfo: structure containing the trap information.   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Returns         : VOID                                                    */
/*                                                                            */
/******************************************************************************/
VOID
MsdpUtilSnmpIfSendTrap (UINT1 u1TrapId, VOID *pTrapInfo)
{
#if defined(FUTURE_SNMP_WANTED) ||defined (SNMP_3_WANTED) || \
    defined (SNMPV3_WANTED)

    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_COUNTER64_TYPE u8CounterVal = { MSDP_ZERO, MSDP_ZERO };
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tMsdpEventTrap     *pMsdpEventTrap = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT1               au1Buf[MSDP_MAX_BUFFER];

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_ENTRY,
                    "MsdpUtilSnmpIfSendTrap : Entry\r\n"));

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC, "Trap to be generated: %d\n", u1TrapId));

    MEMSET (&au1Buf, 0, MSDP_MAX_BUFFER);

    pEnterpriseOid = alloc_oid ((sizeof (gaMSDP_TRAP_OID) /
                                 sizeof (UINT4)) + MSDP_TWO);
    if (pEnterpriseOid == NULL)
    {
        MSDP_TRC_FUNC ((MSDP_UTIL_TRC,
                        "Enterprise Id Could not be allocated.\r\n"));
        return;
    }

    MEMCPY (pEnterpriseOid->pu4_OidList, gaMSDP_TRAP_OID,
            sizeof (gaMSDP_TRAP_OID));

    pEnterpriseOid->u4_Length = sizeof (gaMSDP_TRAP_OID) / sizeof (UINT4);
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u1TrapId;

    /*  pEnterpriseOid->u4_Length = pEnterpriseOid->u4_Length - MSDPSM_TWO;
       pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = MSDPSM_ZERO;
       pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u1TrapId; */

    pSnmpTrapOid = alloc_oid (SNMP_V2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        MSDP_TRC_FUNC ((MSDP_UTIL_TRC, " Error Freeing pEnterpriseOid.\r\n"));
        return;
    }
    /* MEMCPY (pSnmpTrapOid->pu4_OidList, gaMSDP_SNMP_TRAP_OID,
       SNMP_V2_TRAP_OID_LEN * sizeof (UINT4)); */
    MEMCPY (pSnmpTrapOid->pu4_OidList, gaMSDP_SNMP_TRAP_OID,
            sizeof (gaMSDP_SNMP_TRAP_OID));

    pSnmpTrapOid->u4_Length = sizeof (gaMSDP_SNMP_TRAP_OID) / sizeof (UINT4);

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0L, 0, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        MSDP_TRC_FUNC ((MSDP_UTIL_TRC,
                        " Error Freeing pEnterpriseOid and pSnmpTrapOid.\r\n"));
        return;
    }

    pStartVb = pVbList;

    pMsdpEventTrap = (tMsdpEventTrap *) pTrapInfo;
    SPRINTF ((CHR1 *) au1Buf, "fsMsdpRtrId");
    if ((pOid = MsdpUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
    {
        MSDP_TRC_FUNC ((MSDP_UTIL_TRC,
                        " IpAddrtype Error Freeing Varlist.\r\n"));
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }
    pOstring = SNMP_AGT_FormOctetString (pMsdpEventTrap->au1RtrId,
                                         IPVX_IPV4_ADDR_LEN);
    if (pOstring == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
        (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0L, 0, pOstring, NULL, u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        free_octetstring (pOstring);
        SNMP_free_snmp_vb_list (pStartVb);
        MSDP_TRC_FUNC ((MSDP_UTIL_TRC,
                        " IpAddrtype Error Freeing Varlist & pOID.\r\n"));
        return;
    }

    switch (u1TrapId)
    {

        case MSDP_ESTABLISHED_TRAP_ID:
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            SPRINTF ((CHR1 *) au1Buf, "fsMsdpPeerAddrType");
            if ((pOid = MsdpUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                MSDP_TRC_FUNC ((MSDP_UTIL_TRC,
                                " Event Error Freeing Varlist \r\n"));
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_INTEGER32, MSDP_ZERO,
                 pMsdpEventTrap->i4AddrType, NULL, NULL, u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                MSDP_TRC_FUNC ((MSDP_UTIL_TRC,
                                " Event Error Freeing Varlist & pOID \r\n"));
                return;
            }

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            SPRINTF ((CHR1 *) au1Buf, "fsMsdpPeerRemoteAddress");
            if ((pOid = MsdpUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                MSDP_TRC_FUNC ((MSDP_UTIL_TRC,
                                " Event Error Freeing Varlist \r\n"));
                return;
            }

            pOstring = SNMP_AGT_FormOctetString (pMsdpEventTrap->au1PeerAddr,
                                                 IPVX_MAX_INET_ADDR_LEN);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_OCTET_PRIM,
                 0L, 0, pOstring, NULL, u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                MSDP_TRC_FUNC ((MSDP_UTIL_TRC,
                                " Event Error Freeing Varlist & pOID \r\n"));
                return;
            }

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            SPRINTF ((CHR1 *) au1Buf, "fsMsdpPeerFsmEstablishedTransitions");
            if ((pOid = MsdpUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                MSDP_TRC_FUNC ((MSDP_UTIL_TRC,
                                " Event Error Freeing Varlist \r\n"));
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_INTEGER32, MSDP_ZERO,
                 pMsdpEventTrap->u4EventId, NULL, NULL, u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                MSDP_TRC_FUNC ((MSDP_UTIL_TRC,
                                " Event Error Freeing Varlist & pOID \r\n"));
                return;
            }

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            break;

        case MSDP_BACKWARD_TRANSITION_TRAP_ID:

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            SPRINTF ((CHR1 *) au1Buf, "fsMsdpPeerAddrType");
            if ((pOid = MsdpUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                MSDP_TRC_FUNC ((MSDP_UTIL_TRC,
                                " Event Error Freeing Varlist \r\n"));
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_INTEGER32, MSDP_ZERO,
                 pMsdpEventTrap->i4AddrType, NULL, NULL, u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                MSDP_TRC_FUNC ((MSDP_UTIL_TRC,
                                " Event Error Freeing Varlist & pOID \r\n"));
                return;
            }

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            SPRINTF ((CHR1 *) au1Buf, "fsMsdpPeerRemoteAddress");
            if ((pOid = MsdpUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                MSDP_TRC_FUNC ((MSDP_UTIL_TRC,
                                " Event Error Freeing Varlist \r\n"));
                return;
            }

            pOstring = SNMP_AGT_FormOctetString (pMsdpEventTrap->au1PeerAddr,
                                                 IPVX_MAX_INET_ADDR_LEN);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_OCTET_PRIM,
                 0L, 0, pOstring, NULL, u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                MSDP_TRC_FUNC ((MSDP_UTIL_TRC,
                                " Event Error Freeing Varlist & pOID \r\n"));
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "fsMsdpPeerState");
            if ((pOid = MsdpUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                MSDP_TRC_FUNC ((MSDP_UTIL_TRC,
                                " Event Error Freeing Varlist \r\n"));
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_INTEGER32, MSDP_ZERO,
                 pMsdpEventTrap->u4EventId, NULL, NULL, u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);

                MSDP_TRC_FUNC ((MSDP_UTIL_TRC,
                                " Event Error Freeing Varlist & pOID \r\n"));
                return;
            }

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            break;

        case MSDP_RP_OPERSTATUS_TRAP_ID:

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "fsMsdpPeerAddrType");
            if ((pOid = MsdpUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                MSDP_TRC_FUNC ((MSDP_UTIL_TRC,
                                " Event Error Freeing Varlist \r\n"));
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_INTEGER32, MSDP_ZERO,
                 pMsdpEventTrap->i4AddrType, NULL, NULL, u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                MSDP_TRC_FUNC ((MSDP_UTIL_TRC,
                                " Event Error Freeing Varlist & pOID \r\n"));
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "fsMsdpRPOperStatus");
            if ((pOid = MsdpUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);

                MSDP_TRC_FUNC ((MSDP_UTIL_TRC,
                                " Event Error Freeing Varlist \r\n"));
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_INTEGER32, MSDP_ZERO,
                 pMsdpEventTrap->u4EventId, NULL, NULL, u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);

                MSDP_TRC_FUNC ((MSDP_UTIL_TRC,
                                " Event Error Freeing Varlist & pOID \r\n"));
                return;
            }

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            break;

        default:
            MSDP_TRC_FUNC ((MSDP_UTIL_TRC, "Invalid Trap Id Rxd.\r\n"));

            SNMP_free_snmp_vb_list (pStartVb);
            return;
    }
    /* The following API sends the Trap info to the FutureSNMP Agent */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC, " Trap info given to SNMP Agent\r\n"));

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_EXIT,
                    "MsdpUtilSnmpIfSendTrap : Entry\r\n"));
#else
    UNUSED_PARAM (pTrapInfo);
    UNUSED_PARAM (u1TrapId);
#endif
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : MsdpUtilParseSubIdNew                                   */
/*                                                                            */
/*  Description     : returns the numeric value of ppu1TempPtr                */
/*                                                                            */
/*  Input(s)        :  **ppu1TempPtr                                          */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Returns         : value of ppu1TempPtr or MSDP_INVALID                  */
/*                                                                            */
/******************************************************************************/
INT4
MsdpUtilParseSubIdNew (UINT1 **ppu1TempPtr)
{
    INT4                i4Value = MSDP_ZERO;
    UINT1              *pu1Tmp = NULL;

    for (pu1Tmp = *ppu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                                 ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                                 ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        i4Value = (i4Value * MSDP_NUMBER_TEN) +
            (*pu1Tmp & MSDP_MAX_HEX_SINGLE_DIGIT);
    }

    if (*ppu1TempPtr == pu1Tmp)
    {
        i4Value = MSDP_INVALID;
    }
    *ppu1TempPtr = pu1Tmp;
    return (i4Value);
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : MsdpUtilMakeObjIdFromDotNew                             */
/*                                                                            */
/*  Description     : Gives Oid from the input string.                        */
/*                                                                            */
/*  Input(s)        :  pi1TxtStr                                              */
/*                                                                            */
/*  Output(s)       :  None                                                   */
/*                                                                            */
/*  Returns         :  pOidPtr or NULL                                        */
/*                                                                            */
/******************************************************************************/
#define   MSDP_SNMP_TEMP_BUFF   280
INT1                gai4TempBuffer[MSDP_SNMP_TEMP_BUFF];
tSNMP_OID_TYPE     *
MsdpUtilMakeObjIdFromDotNew (INT1 *pi1TxtStr)
{
    tSNMP_OID_TYPE     *pOidPtr = NULL;
    INT1               *pi1TempPtr = NULL, *pi1DotPtr = NULL;
    UINT2               u2Cnt = MSDP_ZERO;
    UINT2               u2DotCount = MSDP_ZERO;
    UINT1              *pu1TmpPtr = NULL;

    /* see if there is an alpha descriptor at begining */
    if (ISALPHA (*pi1TxtStr) != MSDP_ZERO)
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TxtStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TxtStr + STRLEN ((INT1 *) pi1TxtStr);
        }
        pi1TempPtr = pi1TxtStr;

        for (u2Cnt = MSDP_ZERO;
             ((pi1TempPtr < pi1DotPtr) &&
              (u2Cnt < MSDP_SNMP_TEMP_BUFF)); u2Cnt++)
        {
            gai4TempBuffer[u2Cnt] = *pi1TempPtr++;
        }
        if (u2Cnt != MSDP_SNMP_TEMP_BUFF)
            gai4TempBuffer[u2Cnt] = '\0';
        else
        {
            MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_EXIT,
                            "MsdpUtilMakeObjIdFromDotNew : EXIT\r\n"));
            return (NULL);
        }

        for (u2Cnt = MSDP_ZERO;
             (u2Cnt < (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID))
              && (orig_mib_oid_table[u2Cnt].pName != NULL)); u2Cnt++)
        {
            if ((STRCMP (orig_mib_oid_table[u2Cnt].pName,
                         (INT1 *) gai4TempBuffer) == MSDP_ZERO)
                && (STRLEN ((INT1 *) gai4TempBuffer) ==
                    STRLEN (orig_mib_oid_table[u2Cnt].pName)))
            {
                STRNCPY ((INT1 *) gai4TempBuffer,
                         orig_mib_oid_table[u2Cnt].pNumber,
                         MSDP_SNMP_TEMP_BUFF);
                gai4TempBuffer[MSDP_SNMP_TEMP_BUFF - 1] = '\0';
                break;
            }
        }
        if ((u2Cnt < (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID)))
            && (orig_mib_oid_table[u2Cnt].pName == NULL))
        {
            return (NULL);
        }

        if (u2Cnt == (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID)))
        {
            return (NULL);
        }

        /* now concatenate the non-alpha part to the begining */
        STRNCAT ((INT1 *) gai4TempBuffer, (INT1 *) pi1DotPtr,
                 STRLEN (pi1DotPtr));
    }
    else
    {                            /* is not alpha, so just copy into gai4TempBuffer */
        STRCPY ((INT1 *) gai4TempBuffer, (INT1 *) pi1TxtStr);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = MSDP_ZERO;
    for (u2Cnt = MSDP_ZERO;
         ((u2Cnt < MSDP_SNMP_TEMP_BUFF) && (gai4TempBuffer[u2Cnt] != '\0'));
         u2Cnt++)
    {
        if (gai4TempBuffer[u2Cnt] == '.')
        {
            u2DotCount++;
        }
    }
    if ((pOidPtr = alloc_oid ((INT4) (u2DotCount + MSDP_ONE))) == NULL)
    {
        return (NULL);
    }

    /* now we convert number.number.... strings */
    pu1TmpPtr = (UINT1 *) gai4TempBuffer;
    for (u2Cnt = MSDP_ZERO; u2Cnt < u2DotCount + MSDP_ONE; u2Cnt++)
    {
        if ((pOidPtr->pu4_OidList[u2Cnt] =
             ((UINT4) (MsdpUtilParseSubIdNew (&pu1TmpPtr)))) ==
            (UINT4) MSDP_INVALID)
        {
            free_oid (pOidPtr);
            return (NULL);
        }
        if (*pu1TmpPtr == '.')
        {
            pu1TmpPtr++;        /* to skip over dot */
        }
        else if (*pu1TmpPtr != '\0')
        {
            free_oid (pOidPtr);
            return (NULL);
        }
    }                            /* end of for loop */
    pOidPtr->u4_Length = u2DotCount + MSDP_ONE;

    return (pOidPtr);
}

VOID
MsdpUtilInitGlobals (UINT1 u1AddrType)
{
    INT4                i4Index = 0;

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_ENTRY,
                    "MsdpUtilInitGlobals : Entry\r\n"));
    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv4AdminStat = MSDP_DISABLED;
    }
    else if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv6AdminStat = MSDP_DISABLED;
    }
    else
    {
        gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv4AdminStat = MSDP_DISABLED;
        gMsdpGlobals.MsdpGlbMib.i4FsMsdpIPv6AdminStat = MSDP_DISABLED;
    }
    gMsdpGlobals.MsdpGlbMib.i4FsMsdpPeerFilter = MSDP_ACCEPT_ALL;
    gMsdpGlobals.MsdpGlbMib.i4FsMsdpMappingComponentId = MSDP_DEF_COMPONENT;
    gMsdpGlobals.MsdpGlbMib.i4FsMsdpMaxPeerSessions = MSDP_MAX_PEER_SESSIONS;
    gMsdpGlobals.MsdpGlbMib.i4FsMsdpListenerPort = MSDP_DEF_LISTENER_PORT;
    gMsdpGlobals.MsdpGlbMib.i4FsMsdpTraceLevel = 0;
    gMsdpGlobals.MsdpGlbMib.u4FsMsdpCacheLifetime = 0;
    gMsdpGlobals.MsdpGlbMib.i4FsMsdpPeerCount = 0;
    gMsdpGlobals.MsdpGlbMib.i4FsMsdpStatEstPeerCount = 0;
    gMsdpGlobals.MsdpGlbMib.u4FsMsdpNumSACacheEntries = 0;

    for (i4Index = 0; i4Index < MSDP_MAX_REGISTER; i4Index++)
    {
        gMsdpGlobals.aMsdpRegister[i4Index].u1ProtId = 0;
        gMsdpGlobals.aMsdpRegister[i4Index].u1Flag = DOWN;
        gMsdpGlobals.aMsdpRegister[i4Index].pUpdateSrcActiveInfo = NULL;
    }

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_EXIT,
                    "MsdpUtilInitGlobals : Entry\r\n"));

}

/******************************************************************************
 * Function   : MsdpUtilRemoveCacheEntry 
 *
 * Description: This function removes a cache entry
 *
 * Input      : pOldSaCache- Old SA cache entry
 *              pCurSaCache- Current SA cache entry
 *
 * Output     : None
 *
 * Returns    : OSIX_SUCCESS if removal successful
 *              OSIX_FAILURE otherwise
 *              
 *****************************************************************************/

INT4
MsdpUtilRemoveCacheEntry (tMsdpFsMsdpSACacheEntry * pOldSaCache)
{
    INT4                i4RetVal = OSIX_FAILURE;

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpUtilRemoveCacheEntry"));

    if (pOldSaCache == NULL)
    {
        return OSIX_FAILURE;
    }

    if (RB_SUCCESS == RBTreeRemove (gMsdpGlobals.MsdpGlbMib.FsMsdpSACacheTable,
                                    (tRBElem *) pOldSaCache))
    {

        MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_EXIT,
                        "\nEXIT: MsdpUtilRemoveCacheEntry"));
        gMsdpGlobals.MsdpGlbMib.u4FsMsdpNumSACacheEntries--;
        MemReleaseMemBlock (MSDP_FSMSDPSACACHETABLE_POOLID,
                            (UINT1 *) pOldSaCache);

        i4RetVal = OSIX_SUCCESS;
    }
    else
    {

        MSDP_TRC_FUNC ((MSDP_UTIL_TRC, "\nMsdpUtilRemoveCacheEntry: "
                        "RBTreeRemove failed or old SA cache is empty"));

    }

    return i4RetVal;
}

/******************************************************************************
 * Function   : MsdpUtilCopyOverBufChain
 *
 * Description: This function copies over the buffer chain
 *
 * Input      : pu1LinBuf- Linear buffer
 *              u2Offset- Offset
 *              pu2Len- Length
 *
 * Output     : ppDataPkt- Data packet
 *
 * Returns    : OSIX_SUCCESS if CRU_BUF_Copy_OverBufChain returns success
 *              OSIX_FAILURE otherwise
 *              
 *****************************************************************************/
INT4
MsdpUtilCopyOverBufChain (tCRU_BUF_CHAIN_HEADER ** ppDataPkt,
                          UINT1 *pu1LinBuf, UINT2 u2Offset, UINT2 *pu2Len)
{

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpUtilCopyOverBufChain"));

    if (CRU_BUF_Copy_OverBufChain (*ppDataPkt, pu1LinBuf, u2Offset, *pu2Len)
        == CRU_FAILURE)
    {
        *pu2Len = 0;
        CRU_BUF_Release_MsgBufChain (*ppDataPkt, FALSE);
        *ppDataPkt = NULL;

        MSDP_TRC_FUNC ((MSDP_UTIL_TRC, "\nMsdpUtilCopyOverBufChain: "
                        "CRU_BUF_Copy_OverBufChain returns failure"));

        return OSIX_FAILURE;
    }

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_ENTRY,
                    "\nEXIT: MsdpUtilCopyOverBufChain"));

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : MsdpUtilIsBuffSizeReached
 *
 * Description: This function determines whether the buffer size has been 
 *              reached
 *
 * Input      : u1AddrType- Address type
 *              i4Offset- Offset
 *              i4BufSize- Buffer size
 *
 * Output     : None
 *
 * Returns    : OSIX_TRUE if buffer size is reached
 *              OSIX_FALSE otherwise
 *              
 *****************************************************************************/
INT4
MsdpUtilIsBuffSizeReached (UINT1 u1AddrType, INT4 i4Offset, INT4 i4BufSize)
{

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpUtilIsBuffSizeReached"));

    if ((u1AddrType == IPVX_ADDR_FMLY_IPV4) &&
        ((i4Offset + (INT4) MSDP_MIN_IPV4_SA_ADVT_SIZE) > i4BufSize))
    {

        MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_EXIT,
                        "\nEXIT: MsdpUtilIsBuffSizeReached"));

        return OSIX_TRUE;
    }
    if ((u1AddrType == IPVX_ADDR_FMLY_IPV6) &&
        ((INT4) (i4Offset + MSDP_MIN_IPV6_SA_ADVT_SIZE) > i4BufSize))
    {

        MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_EXIT,
                        "\nEXIT: MsdpUtilIsBuffSizeReached"));

        return OSIX_TRUE;
    }

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC, "\nMsdpUtilIsBuffSizeReached: "
                    "Buffer size is not reached"));

    return OSIX_FALSE;
}

/******************************************************************************
 * Function   : MsdpUtilGetTxPktSize
 * 
 * Description: This function obtains the transmit packet size
 *
 * Input      : u2MsgInBuffer- Message in the buffer
 *              u1AddrType- Address type
 *
 * Output     : pi4BufSize- Buffer size
 *
 * Returns    : OSIX_SUCCESS if the packet size is successfully obtained
 *              OSIX_FAILURE otherwise
 *              
 *****************************************************************************/
INT4
MsdpUtilGetTxPktSize (UINT2 u2MsgInBuffer, UINT1 u1AddrType, INT4 *pi4BufSize)
{

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpUtilGetTxPktSize"));

    *pi4BufSize = MSDP_PEER_MAX_RX_TX_BUF_LEN - u2MsgInBuffer;

    if (*pi4BufSize > MSDP_PEER_MAX_TEMP_TX_BUF_LEN)
    {
        *pi4BufSize = MSDP_PEER_MAX_TEMP_TX_BUF_LEN;
    }

    if ((u1AddrType == IPVX_ADDR_FMLY_IPV4) &&
        (*pi4BufSize < (INT4) MSDP_MIN_IPV4_SA_ADVT_SIZE))
    {

        MSDP_TRC_FUNC ((MSDP_UTIL_TRC, "\nMsdpUtilGetTxPktSize: "
                        "Buffer size less than min for IPv4"));

        return OSIX_FAILURE;
    }

    if ((u1AddrType == IPVX_ADDR_FMLY_IPV6) &&
        (*pi4BufSize < (INT4) MSDP_MIN_IPV6_SA_ADVT_SIZE))
    {

        MSDP_TRC_FUNC ((MSDP_UTIL_TRC, "\nMsdpUtilGetTxPktSize: "
                        "Buffer size less than min for IPv6"));

        return OSIX_FAILURE;
    }

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpUtilGetTxPktSize"));

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   :  MsdpUtilIsValidLocalAddress
 *
 * Description: This function checks if the given address is a valid local 
 *              address
 *
 * Input      : pu1Addr- An address
 *              i4AddrLen- Address length
 *
 * Output     : None
 *
 * Returns    : returns I/f Index if Address is valid
 *                else -1 
 *              
 *****************************************************************************/
INT4
MsdpUtilIsValidLocalAddress (UINT1 *pu1Addr, INT4 i4AddrType)
{
    INT4                i4RetVal = MSDP_INVALID;

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpUtilIsValidLocalAddress"));

    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        i4RetVal = MsdpPortIsValidLocalAddress (pu1Addr);
    }
    else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        i4RetVal = Msd6PortIsValidLocalAddress (pu1Addr);
    }

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpUtilIsValidLocalAddress"));

    return i4RetVal;
}

/******************************************************************************
 * Function   :  MsdpUtilIsAllZeros
 *
 * Description: This function checks whether the given address is composed
 *              entirely of 0s
 *
 * Input      : pu1Addr- Address value
 *              i4MsdpAddrType- Address type
 *
 * Output     : None
 *
 * Returns    : OSIX_FAILURE if the address is not all zeros
 *              OSIX_SUCCESS otherwise
 *              
 *****************************************************************************/

INT4
MsdpUtilIsAllZeros (UINT1 *pu1Addr, INT4 i4MsdpAddrType)
{
    UINT1               au1Ipv6AllZeros[IPVX_IPV6_ADDR_LEN];
    INT4                i4RetVal = OSIX_TRUE;
    UINT4               u4Addr = 0;

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpUtilIsAllZeros"));

    if (pu1Addr == NULL)
    {

        MSDP_TRC_FUNC ((MSDP_UTIL_TRC, "\nInput Address is NULL"));

        MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_EXIT,
                        "\nEXIT: MsdpUtilIsAllZeros"));
        return OSIX_FAILURE;
    }

    MEMSET (au1Ipv6AllZeros, 0, IPVX_IPV6_ADDR_LEN);

    if (i4MsdpAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (0 != MEMCMP (pu1Addr, &u4Addr, IPVX_IPV4_ADDR_LEN))
        {
            i4RetVal = OSIX_FALSE;
        }
        else
        {

            MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_EXIT,
                            "\nEXIT: MsdpUtilIsAllZeros"));

        }
    }
    else
    {
        if (0 != MEMCMP (pu1Addr, au1Ipv6AllZeros, IPVX_IPV6_ADDR_LEN))
        {
            i4RetVal = OSIX_FALSE;
        }
        else
        {

            MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_EXIT,
                            "\nEXIT: MsdpUtilIsAllZeros"));
        }
    }

    return i4RetVal;
}

/***************************************************************************
 * Function Name    :  MsdpUtilGetUcastRtInfo
 *
 * Description      :  This function is used to get the details of Unicast 
 *                     route 
 *
 * Input (s)        : pDest - Unicast Destination IP address 
 *                    
 * Output (s)       : pNextHop - Next Hop Ip Address to reach Dest
 *                    pi4Metrics - Metric to reach Dest
 *                    pu4Preference - The route's preference value
 *                    pi4Port - The IP port to be used to reach Dest
 *
 * Returns          : None
 ****************************************************************************/

VOID
MsdpUtilGetUcastRtInfo (tIPvXAddr * pDest, tIPvXAddr * pNextHop,
                        INT4 *pi4Metrics, UINT4 *pu4Preference, INT4 *pi4Port)
{
    tRtInfoQueryMsg     RtQueryInfo;
    tNetIpv4RtInfo      RtInfo;
    tNetIpv6RtInfo      Ip6RtInfo;
    tNetIpv6RtInfoQueryMsg QryMsg;
    INT4                i4RetStat = OSIX_FAILURE;
    tIPvXAddr           TmpAddr;
    UINT4               u4Addr = 0;
    UINT1               au1NullAddr[IPVX_MAX_INET_ADDR_LEN];

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpUtilGetUcastRtInfo"));

    MEMSET (&TmpAddr, 0, sizeof (tIPvXAddr));
    if (pDest->u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        MEMSET (&RtInfo, 0, sizeof (tNetIpv4RtInfo));
        MEMSET (&RtQueryInfo, 0, sizeof (tRtInfoQueryMsg));
        PTR_FETCH4 (RtQueryInfo.u4DestinationIpAddress, pDest->au1Addr);
        RtQueryInfo.u4DestinationSubnetMask = MSDP_DEF_SRC_MASK_ADDR;
        RtQueryInfo.u2AppIds = 0;
        RtQueryInfo.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
        RtQueryInfo.u4ContextId = MSDP_DEF_VRF_CTXT_ID;

        i4RetStat = MsdpPortIpGetRoute (&RtQueryInfo, &RtInfo);

        if (i4RetStat == OSIX_FAILURE)
        {
            *pi4Port = (INT4) MSDP_INVALID;
        }
        else
        {
            *pi4Metrics = RtInfo.i4Metric1;
            *pu4Preference = (UINT4) RtInfo.u1Preference;
            if (RtInfo.u4NextHop == 0)
            {
                IPVX_ADDR_COPY (pNextHop, pDest);
            }
            else
            {
                u4Addr = OSIX_NTOHL (RtInfo.u4NextHop);
                IPVX_ADDR_INIT_IPV4 (TmpAddr, IPVX_ADDR_FMLY_IPV4,
                                     (UINT1 *) &(u4Addr));
                IPVX_ADDR_COPY (pNextHop, &TmpAddr);
            }
            *pi4Port = (INT4) RtInfo.u4RtIfIndx;
        }
    }
    else if (pDest->u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        MEMSET (&Ip6RtInfo, 0, sizeof (tNetIpv6RtInfo));
        MEMSET (&QryMsg, 0, sizeof (tNetIpv6RtInfoQueryMsg));

        MEMCPY (Ip6RtInfo.Ip6Dst.u1_addr, pDest->au1Addr,
                sizeof (Ip6RtInfo.Ip6Dst.u1_addr));
        MEMSET (au1NullAddr, 0, sizeof (au1NullAddr));
        Ip6RtInfo.u1Prefixlen = (UINT1) (pDest->u1AddrLen *
                                         MSDP_BITS_IN_ONE_BYTE);
        QryMsg.u1QueryFlag = 1;
        i4RetStat = Msd6PortIpv6GetRoute (&QryMsg, &Ip6RtInfo);
        if (i4RetStat == OSIX_FAILURE)
        {
            *pi4Port = (INT4) MSDP_INVALID;
        }
        else
        {
            *pi4Metrics = Ip6RtInfo.u4Metric;
            *pu4Preference = Ip6RtInfo.u1Preference;
            if (MEMCMP (Ip6RtInfo.NextHop.u1_addr, au1NullAddr,
                        IPVX_IPV6_ADDR_LEN) == 0)
            {
                IPVX_ADDR_COPY (pNextHop, pDest);
                if (MsdpPortGetIpPortFromIfIndex (Ip6RtInfo.u4Index,
                                                  (UINT4 *) pi4Port) !=
                    OSIX_SUCCESS)
                {
                    *pi4Port = (INT4) MSDP_INVALID;
                }
            }
            if (IS_ADDR_UNSPECIFIED (Ip6RtInfo.NextHop))
            {
                IPVX_ADDR_COPY (pNextHop, pDest);
            }
            else
            {
                IPVX_ADDR_INIT_IPV6 (TmpAddr, IPVX_ADDR_FMLY_IPV6,
                                     Ip6RtInfo.NextHop.u1_addr);
                IPVX_ADDR_COPY (pNextHop, &TmpAddr);
                if (MsdpPortGetIpPortFromIfIndex (Ip6RtInfo.u4Index,
                                                  (UINT4 *) pi4Port) !=
                    OSIX_SUCCESS)
                {
                    *pi4Port = (INT4) MSDP_INVALID;
                }
            }
        }
    }

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpUtilGetUcastRtInfo"));
}

/***************************************************************************
 * Function Name    :  MsdpUtilGetRegisterEntry
 *
 * Description      :  This function gets that registry entry for which the 
 *                     prot id matches the given prot id
 *
 * Input (s)        : u1ProtId- prot id 
 *                    
 * Output (s)       : None
 *
 * Returns          : The index value of the registry entry that matches. 
 *                    MSDP_INVALID if no match found.
 *
 ****************************************************************************/
INT4
MsdpUtilGetRegisterEntry (UINT1 u1ProtId)
{
    INT4                i4Index = 0;

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpUtilGetRegisterEntry"));

    for (i4Index = 0; i4Index < MSDP_MAX_REGISTER; i4Index++)
    {
        if (gMsdpGlobals.aMsdpRegister[i4Index].u1ProtId == u1ProtId)
        {

            MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_EXIT,
                            "\nEXIT: MsdpUtilGetRegisterEntry"));

            return i4Index;
        }
    }

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC, "\nMsdpUtilGetRegisterEntry: "
                    "Reg entries don't match passed prot id %d", u1ProtId));

    return MSDP_INVALID;
}

/***************************************************************************
 * Function Name    :  MsdpUtilFindFreeRegisterEntry
 *
 * Description      :  This function finds a free registry entry and returns
 *                     the index
 *
 * Input (s)        : None
 *                    
 * Output (s)       : None
 *
 * Returns          : The index of the first free registry entry
 *                    MSDP_INVALID if no free entries available
 *
 ****************************************************************************/
INT4
MsdpUtilFindFreeRegisterEntry (VOID)
{
    INT4                i4Index = 0;

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpUtilFindFreeRegisterEntry"));

    for (i4Index = 0; i4Index < MSDP_MAX_REGISTER; i4Index++)
    {
        if (gMsdpGlobals.aMsdpRegister[i4Index].u1Flag == DOWN)
        {

            MSDP_TRC_FUNC ((MSDP_UTIL_TRC | MSDP_FN_EXIT,
                            "\nEXIT: MsdpUtilFindFreeRegisterEntry"));

            return i4Index;
        }
    }

    MSDP_TRC_FUNC ((MSDP_UTIL_TRC, "\nMsdpUtilFindFreeRegisterEntry: "
                    "No Free Register Entry available"));

    return MSDP_INVALID;
}

/****************************************************************************
 * Function Name         :   MsdpPrintIPvxAddress
 *
 * Description           :   Converts the Input IpAddress into String and
 *                           Prints the IPAddress in the standard format
 *
 * Input (s)             :   Addr: Ip Address
 *
 * Output (s)            :   None
 *
 * Returns             : string containing the IpAddress in printable format
 ****************************************************************************/
UINT1              *
MsdpPrintIPvxAddress (tIPvXAddr Addr)
{
    return (MsdpPrintAddress (Addr.au1Addr, Addr.u1Afi));
}

/****************************************************************************
 * Function Name         :   MsdpPrintAddress
 *
 * Description           :   Converts the Input IpAddress into String and
 *                           Prints the IPAddress in the standard format
 *
 * Input (s)             :   pu1Addr: Ip Address
 *                           u1Afi  : Ip Address Family
 *
 * Output (s)            :   None
 *
 * Returns             : string containing the IpAddress in printable format
 ****************************************************************************/
UINT1              *
MsdpPrintAddress (UINT1 *pu1Addr, UINT1 u1Afi)
{
    tIp6Addr            Ip6Addr;

    if (pu1Addr == NULL)
    {
        return NULL;
    }
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    if (u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&Ip6Addr.u1_addr[12], pu1Addr, IPVX_IPV4_ADDR_LEN);
    }
    else if (u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (&Ip6Addr.u1_addr, pu1Addr, IPVX_IPV6_ADDR_LEN);
    }
    else
    {
        return NULL;
    }
    return (Ip6PrintAddr (&Ip6Addr));
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  msdputil.c                     */
/*-----------------------------------------------------------------------*/
