/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *$Id: msdptask.c,v 1.2 2011/01/21 15:04:26 siva Exp $

 * Description:This file contains procedures related to
 *             MSDP - Task Initialization
 *******************************************************************/
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *

 * Description: This file contains the low level nmh routines.
 *
 *******************************************************************/

#include "msdpinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : MsdpTaskSpawnMsdpTask()                                    */
/*                                                                           */
/* Description  : This procedure is provided to Spawn Msdp Task              */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS, if MSDP Task is successfully spawned         */
/*                OSIX_FAILURE, otherwise                                    */
/*                                                                           */
/*****************************************************************************/

VOID
MsdpTaskSpawnMsdpTask (INT4 *pi4Args)
{
    UNUSED_PARAM (pi4Args);

    MSDP_TRC_FUNC ((MSDP_TASK_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpTaskSpawnMsdpTask"));

    /* task initializations */
    if (MsdpMainTaskInit () == OSIX_FAILURE)
    {
        MSDP_TRC ((MSDP_TASK_TRC, " !!!!! MSDP TASK INIT FAILURE  !!!!! \n"));
        MsdpMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* spawn msdp task */

    /*if (OsixCreateTask ((const UINT1 *) MSDP_TASK_NAME, MSDP_TASK_PRIORITY,
       OSIX_DEFAULT_STACK_SIZE,
       MsdpMainTask, NULL, OSIX_DEFAULT_TASK_MODE,
       (tOsixTaskId *) & (gMsdpGlobals.msdpTaskId))
       == OSIX_FAILURE)
       {
       return OSIX_FAILURE;
       } */

    MsdpTaskRegisterMsdpMibs ();
    lrInitComplete (OSIX_SUCCESS);
    MsdpMainTask ();

    MSDP_TRC_FUNC ((MSDP_TASK_TRC, "\nMsdpTaskSpawnMsdpTask: "
                    "MsdpMainTaskInit returns success"));

    MSDP_TRC_FUNC ((MSDP_TASK_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpTaskSpawnMsdpTask"));
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : MsdpTaskRegisterMsdpMibs                                   */
/*                                                                           */
/* Description  : This function registers the standard and proprietary MIBs  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS always                                        */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
MsdpTaskRegisterMsdpMibs ()
{
    MSDP_TRC_FUNC ((MSDP_TASK_TRC | MSDP_FN_ENTRY,
                    "\nENTRY: MsdpTaskRegisterMsdpMibs"));

    RegisterSTDMSD ();
    RegisterFSMSDP ();

    MSDP_TRC_FUNC ((MSDP_TASK_TRC | MSDP_FN_EXIT,
                    "\nEXIT: MsdpTaskRegisterMsdpMibs"));

    return OSIX_SUCCESS;
}

/*------------------------------------------------------------------------*/
/*                        End of the file  msdptask.c                     */
/*------------------------------------------------------------------------*/
