/***************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                    */
/* $Id: vlnmpbnp.c,v 1.15 2015/04/25 12:25:33 siva Exp $                    */
/*  FILE NAME             : vlmpbnpx.c                                     */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*  SUBSYSTEM NAME        : VLAN Module                                    */
/*  MODULE NAME           : VLAN                                           */
/*  LANGUAGE              : C                                              */
/*  TARGET ENVIRONMENT    : Any                                            */
/*  DATE OF FIRST RELEASE : 07 DEC 2006                                    */
/*  AUTHOR                : Aricent Inc.                                   */
/*  DESCRIPTION           : This file contains VLAN NP Routine             */
/*                                                                         */
/***************************************************************************/
#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#ifdef PBB_WANTED
#include "pbb.h"
#endif

#include "l2iwf.h"
#include "npapi.h"
#include "npvlnmpb.h"

/*****************************************************************************/
/* Function Name      : FsMiVlanHwProviderBridgePortType.                    */
/*                                                                           */
/* Description        : This function is called when the port type is        */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier             */
/*                      u4IfIndex - Port Index.                              */
/*                      u4PortType - CUSTOMER_NETWORK_PORT/CUSTOMER_EDGE_PORT*/
/*                      PROVIDER_NETWORK_PORT                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetProviderBridgePortType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                     UINT4 u4PortType)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PortType);
    return FNP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPortSVlanTranslationStatus.             */
/*                                                                           */
/* Description        : This function is called when the SVLAN Translation   */
/*                      status onfigured for a port.                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortSVlanTranslationStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                         UINT1 u1Status)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwAddSVlanTranslationEntry.                  */
/*                                                                           */
/* Description        : This function is called when an entry is configured  */
/*                      for an VID translation table                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2LocalSVlan - SVlan received in the packet ranges
 *                                     < 1 - 4094 >                          */
/*                      u2RelaySVlan - SVlan used in the bridge  ranges
 *                                     < 1 - 4094 >                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwAddSVlanTranslationEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                    UINT2 u2LocalSVlan, UINT2 u2RelaySVlan)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalSVlan);
    UNUSED_PARAM (u2RelaySVlan);
    /*
     * The following should be taken care or considered before calling 
     * the HW-API.
     * This API can be called with the following inputs for local and 
     * relay VID.
     -Local and relay VID - valid values (Non-zero values)
     -Local VID:0,        relay VID:nonzero
     -Local VID:non-zero, relay VID:0

     * If the HW maintains ingress and egress VID translation tables,
     * entry must be added in ingress VLAN translation table when the 
     * local VID is not 0. 
     * Entry must be added in Egress VLAN translation table when relay 
     * VLAN-ID is not 0.
     * When both local and relay VLANIDs are not zero, entry must be 
     * added in Ingress and Egress VLAN translation tables.
     */

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwDelSVlanTranslationEntry.                  */
/*                                                                           */
/* Description        : This function is called when an entry is deleted     */
/*                      from an VID translation table                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2LocalSVlan - SVlan received in the packet  ranges
 *                      < 1 - 4094 >                                         */
/*                      u2RelaySVlan - SVlan used in the bridge  ranges
 *                      < 1 - 4094 >                                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwDelSVlanTranslationEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                    UINT2 u2LocalSVlan, UINT2 u2RelaySVlan)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalSVlan);
    UNUSED_PARAM (u2RelaySVlan);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPortEtherTypeSwapStatus.                */
/*                                                                           */
/* Description        : This function is called when the SVLAN Ether type    */
/*                      status onfigured for a port.                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortEtherTypeSwapStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      UINT1 u1Status)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    return FNP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwAddEtherTypeSwapEntry.                     */
/*                                                                           */
/* Description        : This function is called when an entry is added       */
/*                      to the ether type swap table which is applied in     */
/*                      ingress of a packet.                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2LocalEtherType- Ether type received in the packet 
 *                      ranges < 1 - 65535 >                                 */
/*                      u2RelayEtherType- Ether type understand by the bridge
 *                      ranges < 1 - 65535 >                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwAddEtherTypeSwapEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                 UINT2 u2LocalEtherType, UINT2 u2RelayEtherType)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalEtherType);
    UNUSED_PARAM (u2RelayEtherType);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwDelEtherTypeSwapEntry.                     */
/*                                                                           */
/* Description        : This function is called when an entry is deleted     */
/*                      to the ether type swap table which is applied in     */
/*                      ingress of a packet.                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2LocalEtherType- Ether type received in the packet 
 *                      ranges < 1 - 65535 >                                 */
/*                      u2RelayEtherType- Ether type understand by the bridge
 *                      ranges < 1 - 65535 >                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwDelEtherTypeSwapEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                 UINT2 u2LocalEtherType, UINT2 u2RelayEtherType)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalEtherType);
    UNUSED_PARAM (u2RelayEtherType);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwAddSVlanMap.                               */
/*                                                                           */
/* Description        : This function is called when an entry is added for   */
/*                      Port in the SVLAN classification tables which is     */
/*                      assigned for that port                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      VlanSVlanMap-Structure containing info for the       */
/*                      Configured SVlan classification entry.               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwAddSVlanMap (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanSVlanMap);
    return FNP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwDelSVlanMap.                               */
/*                                                                           */
/* Description        : This function is called when an entry is deleted for */
/*                      Port in the SVLAN classification tables which is     */
/*                      assigned for that port                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      VlanSVlanMap-Structure containing info for the       */
/*                      deleted SVlan classification entry.                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwDeleteSVlanMap (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanSVlanMap);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPortSVlanClassifyMethod                 */
/*                                                                           */
/* Description        : This function is called when a port is configured    */
/*                      for a type of SVLAN classification tables which is   */
/*                      used in assigning SVLAN for untagged packets         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index                               */
/*                      u1TableType - Type of table assigned for that port 
 *                      can take values from the tVlanSVlanTableType enum.   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortSVlanClassifyMethod (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      UINT1 u1TableType)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1TableType);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwPortMacLearningLimit.                      */
/*                                                                           */
/* Description        : This function is called for configuring the Unicast  */
/*                      mac limit for a Port                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex  - Port identifier.                        */
/*                      u4MacLimit - Mac Limit of the Unicast Table ranges 
 *                      < 0 - 4294967295 >                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwPortMacLearningLimit (UINT4 u4ContextId, UINT4 u4IfIndex,
                                UINT4 u4MacLimit)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4MacLimit);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwStaticMulticastMacTableLimit .             */
/*                                                                           */
/* Description        : This function is called for configuring the Multicast*/
/*                      mac limit  for the bridge                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4MacLimit - Mac Limit of the Multicast Table ranges 
 *                      < 0 - 4294967295 >                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwMulticastMacTableLimit (UINT4 u4ContextId, UINT4 u4MacLimit)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4MacLimit);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPortCustomerVlan.                       */
/*                                                                           */
/* Description        : This function is called for setting the port         */
/*                      configured as customer vlan                          */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Interface Index                          */
/*                      CVlanId - Configured Customer VLAN ID 
 *                      ranges < 1 - 4094 >                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortCustomerVlan (UINT4 u4ContextId, UINT4 u4IfIndex,
                               tVlanId CVlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (CVlanId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwResetPortCustomerVlan.                     */
/*                                                                           */
/* Description        : This function is called for resetting the ports      */
/*                      configured customer vlan                             */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Interface Index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwResetPortCustomerVlan (UINT4 u4ContextId, UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwCreateProviderEdgePort                     */
/*                                                                           */
/* Description        : This function creates a Provider Edge Port (logical  */
/*                      port) in hardware.                                   */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.
 *                      u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId   - Service Vlan Id ranges < 1 - 4094 >.     */
/*                      PepConfig - Contains values to be used for PVID,     */
/*                                  Default user priority, Acceptable        */
/*                                  frame types, enable ingress filtering    */
/*                                  on port creation.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwCreateProviderEdgePort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  tVlanId SVlanId, tHwVlanPbPepInfo PepConfig)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (PepConfig);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwDelProviderEdgePort                        */
/*                                                                           */
/* Description        : This function deletes the Provider Edge Port from    */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.
 *                      u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id ranges < 1 - 4094 >.    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwDelProviderEdgePort (UINT4 u4ContextId, UINT4 u4IfIndex,
                               tVlanId SVlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPepPvid                                 */
/*                                                                           */
/* Description        : This function sets the PVID for the given Provider   */
/*                      Edge Port in hardware.                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.
 *                      u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id < 1 - 4094 >.           */
/*                      Pvid       - PVID to be set < 1 - 4094 >.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPepPvid (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVlanId,
                      tVlanId Pvid)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (Pvid);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPepAccFrameType                         */
/*                                                                           */
/* Description        : This function sets the Acceptable frame type for the */
/*                      given Provider Edge Port in hardware.                */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.
 *                      u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id ranges < 1 - 4094 >.    */
/*                      u1AccepFrameType - Acceptable frame type to be set   */
/*                                         for this PEP.Frame types are
 *                      VLAN_ADMIT_ALL_FRAMES,
 *                      VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES,
 *                     VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES
 *                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPepAccFrameType (UINT4 u4ContextId, UINT4 u4IfIndex,
                              tVlanId SVlanId, UINT1 u1AccepFrameType)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (u1AccepFrameType);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPepDefUserPriority                      */
/*                                                                           */
/* Description        : This function sets the Default User priority for the */
/*                      given Provider Edge Port in hardware.                */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.
 *                      u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id ranges <1-4094>.        */
/*                      i4DefUsrPri  - Default user priority for this 
 *                      PEP ranges <0-7>.                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPepDefUserPriority (UINT4 u4ContextId, UINT4 u4IfIndex,
                                 tVlanId SVlanId, INT4 i4DefUsrPri)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (i4DefUsrPri);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPepIngFiltering                         */
/*                                                                           */
/* Description        : This function sets the Enable Ingress Filetering     */
/*                      parameter for the given Provider Edge Port in        */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.
 *                      u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id ranges <1-4094>.        */
/*                      u1IngFilterEnable - Enable / Disable Ingress         */
/*                                          filtering can take macro 
 *                                          VLAN_ENABLED,VLAN_DISABLED.      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPepIngFiltering (UINT4 u4ContextId, UINT4 u4IfIndex,
                              tVlanId SVlanId, UINT1 u1IngFilterEnable)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (u1IngFilterEnable);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetCvidUntagPep                            */
/*                                                                           */
/* Description        : This function sets the UntagPep value to true/false  */
/*                      in CVID registration table.                          */
/*                                                                           */
/* Input(s)           : u4ContextId  - Context Id.                           */
/*                      VlanSVlanMap - Structure filled with C-VID reg       */
/*                      table parameters.                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetCvidUntagPep (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanSVlanMap);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetCvidUntagCep                            */
/*                                                                           */
/* Description        : This function sets the UntagCep value to true/false  */
/*                      in CVID registration table.                          */
/*                                                                           */
/* Input(s)           : u4ContextId  - Context Id.                           */
/*                      VlanSVlanMap - Structure filled with C-VID reg       */
/*                      table parameters.                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetCvidUntagCep (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanSVlanMap);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPcpEncodTbl.                            */
/*                                                                           */
/* Description        : This function sets/modifies PCP encoding table entry */
/*                      for the given port, given PCP selection row, given   */
/*                      priority, given drop_eligible value.                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.
 *                      u4IfIndex - Port Index.                              */
/*                      NpPbVlanPcpInfo - Entry containing the following 
 *                                                                   fields: */
/*                          u2PcpSelRow - PCP selection row where            */
/*                                        modification will be done.Row values
 *                                        are VLAN_8P0D_SEL_ROW,
 *                                        VLAN_7P1D_SEL_ROW,
 *                                        VLAN_6P2D_SEL_ROW,
 *                                        VLAN_5P3D_SEL_ROW                  */
/*                          u2Priority  - Priority value ranges <0-7>.       */
/*                          u1DropEligible - Drop Eligible value             
 *                          VLAN_SNMP_TRUE,VLAN_SNMP_FALSE.                  */
/*                          u2PcpVlaue  - PCP value to be set for the above  */
/*                                        input value <0-7>.                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPcpEncodTbl (UINT4 u4ContextId, UINT4 u4IfIndex,
                          tHwVlanPbPcpInfo NpPbVlanPcpInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (NpPbVlanPcpInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPcpDecodTbl.                            */
/*                                                                           */
/* Description        : This function sets/modifies PCP decoding table entry */
/*                      for the given port, given PCP selection row, given   */
/*                      PCP value.                                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.
 *                      u4IfIndex - Port Index.                              */
/*                      PcpTblEntry - Entry containing the following fields: */
/*                          u2PcpSelRow - PCP selection row where            */
/*                                        modification will be done.Row 
 *                                        values are VLAN_8P0D_SEL_ROW,
 *                                        VLAN_7P1D_SEL_ROW,
 *                                        VLAN_6P2D_SEL_ROW,
 *                                        VLAN_5P3D_SEL_ROW                  */
/*                          u2PcpVlaue  - PCP value < 0-7 >                  */
/*                          u2Priority  - Priority value to be set for the   */
/*                                        about input (port, PcpSel, pcp).   */
/*                                          can take <0-7>                   */
/*                          u1DropEligible - Drop Eligible value to be set   */
/*                                           for the above input             */
/*                                           (port, PcpSel, Pcp).can take
 *                                           VLAN_SNMP_TRUE,VLAN_SNMP_FALSE  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPcpDecodTbl (UINT4 u4ContextId, UINT4 u4IfIndex,
                          tHwVlanPbPcpInfo NpPbVlanPcpInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (NpPbVlanPcpInfo);
    return FNP_SUCCESS;
}

/* Use-DEI parameter. */
/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPortUseDei.                             */
/*                                                                           */
/* Description        : This function sets the Use_DEI parameter for the     */
/*                      given port.                                          */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.
 *                      u4IfIndex - Port Index.                              */
/*                      u1UseDei  - True/False for Use_DEI parameter for     */
/*                                  this port.VLAN_SNMP_TRUE,VLAN_SNMP_FALSE */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortUseDei (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1UseDei)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1UseDei);
    return FNP_SUCCESS;
}

/* Require Drop Encoding parameter. */
/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPortReqDropEncoding.                    */
/*                                                                           */
/* Description        : This function sets the Require Drop Encoding         */
/*                      parameter for the given port.                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.
 *                      u4IfIndex - Port Index.                              */
/*                      u1ReqDropEncoding  - True/False value for Require    */
/*                                           Drop encoding parameter for     */
/*                                           this port.can take
 *                                           VLAN_SNMP_FALSE,VLAN_SNMP_TRUE  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortReqDropEncoding (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  UINT1 u1ReqDrpEncoding)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1ReqDrpEncoding);
    return FNP_SUCCESS;
}

/* Port Priority Code Point Selection Row. */
/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPortPcpSelRow.                          */
/*                                                                           */
/* Description        : This function sets the Port Priority Code Point      */
/*                      Selection for the given port.                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.
 *                      u4IfIndex       - Port Index.                        */
/*                      u2PcpSelection  - It can take the following values: 
 *                      VLAN_8P0D_SEL_ROW,VLAN_7P1D_SEL_ROW,
 *                      VLAN_6P2D_SEL_ROW,VLAN_5P3D_SEL_ROW                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortPcpSelection (UINT4 u4ContextId, UINT4 u4IfIndex,
                               UINT2 u2PcpSelection)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2PcpSelection);
    return FNP_SUCCESS;
}

/* Internal CNP - Service Priority Regeneration Table NPAPIs */
/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetServicePriRegenEntry                    */
/*                                                                           */
/* Description        : This function sets the service priority              */
/*                      regeneration table entry.                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.
 *                      u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id can take < 1-4094 >.    */
/*                      i4RecvPriority - Receive Priority can take < 0-7 >   */
/*                      i4RegenPriority - Regenerated Priority can 
 *                                                take < 0-7 >               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetServicePriRegenEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                   tVlanId SVlanId, INT4 i4RecvPriority,
                                   INT4 i4RegenPriority)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (i4RecvPriority);
    UNUSED_PARAM (i4RegenPriority);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetTunnelMacAddress                        */
/*                                                                           */
/* Description        : This function creates filters for receiving the      */
/*                      packets destined to proprietary tunnel MAC           */
/*                      address configured for different L2 protocols.       */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      MacAddr - MAC address used for protocol tunneling.   */
/*                      u2Protocol - L2 protocol for which the tunnel MAC    */
/*                                   address is configured for tunneling     */
/*                                   can take any of the following.          */
/*                                   VLAN_NP_DOT1X_PROTO_ID,                 */
/*                                   VLAN_NP_LACP_PROTO_ID,                  */
/*                                   VLAN_NP_STP_PROTO_ID,                   */
/*                                   VLAN_NP_GVRP_PROTO_ID,                  */
/*                                   VLAN_NP_GMRP_PROTO_ID,                  */
/*                                   VLAN_NP_IGMP_PROTO_ID                   */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetTunnelMacAddress (UINT4 u4ContextId, tMacAddr MacAddr,
                               UINT2 u2Protocol)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u2Protocol);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwGetNextSVlanMap                            */
/*                                                                           */
/* Description        : This function is called to get Cvid Entry for CVlan  */
/*                      for Port in the SVLAN classification tables          */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.
 *                      VlanSVlanMap-Structure containing info for the       */
/*                      deleted SVlan classification entry.                  */
/*                      CvlanId - CVlan Id can take < 1-4094 >               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsMiVlanHwGetNextSVlanMap (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap,
                           tVlanSVlanMap * pRetVlanSVlanMap)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanSVlanMap);
    UNUSED_PARAM (pRetVlanSVlanMap);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwGetNextSVlanTranslationEntry               */
/*                                                                           */
/* Description        : This function is called to get entry                 */
/*                      from a  VID translation table                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2LocalSVlan - SVlan received in the packet          */
/*                      pVidTransEntryInfo - Vid Translation Entry           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On VLAN_SUCCESS                       */
/*                      VLAN_FAILURE - On failure                            */
/*****************************************************************************/
INT4
FsMiVlanHwGetNextSVlanTranslationEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                        tVlanId u2LocalSVlan,
                                        tVidTransEntryInfo * pVidTransEntryInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalSVlan);
    UNUSED_PARAM (pVidTransEntryInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetEvcAttribute                            */
/*                                                                           */
/* Description        : This function is called to get entry                 */
/*                      from a  Evc Info table                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      i4Action - Attribute action.                         */
/*                      pIssEvcInfo - Evc Info Entry                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On VLAN_SUCCESS                       */
/*                      FNP_FAILURE - On failure                            */
/*****************************************************************************/
INT4
FsMiVlanHwSetEvcAttribute (UINT4 u4ContextId, INT4 i4Action,
                           tEvcInfo * pIssEvcInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (i4Action);
    UNUSED_PARAM (pIssEvcInfo);

    return FNP_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetCVlanStat                               */
/*                                                                           */
/* Description        : This function is called to set the customer vlan     */
/*                                                         statistics        */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      tHwVlanCVlanStat - structure containing the          */
/*                      cvlan counter stat                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On VLAN_SUCCESS                        */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4 FsMiVlanHwSetCVlanStat ( tHwVlanCVlanStat VlanSVlanMap)
{

		UNUSED_PARAM (VlanSVlanMap);
		return FNP_SUCCESS;
}


/*****************************************************************************/
/* Function Name      : FsMiVlanHwGetCVlanStat                               */
/*                                                                           */
/* Description        : This function is called to get the customer-vlan     */
/*                      stat                                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u2Port      - Interface ID.                          */
/*                      u2CVlanId   - Customer Vlan ID.                      */
/*                      pu4VlanStatsValue - Total Frames.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On VLAN_SUCCESS                        */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4 FsMiVlanHwGetCVlanStat ( UINT2 u2Port,
                             UINT2 u2CVlanId,UINT1 u1StatsType,
                             UINT4 *pu4VlanStatsValue)
{
		UNUSED_PARAM (u2Port);
		UNUSED_PARAM (u2CVlanId);
		UNUSED_PARAM (u1StatsType);
		UNUSED_PARAM (pu4VlanStatsValue);
		return FNP_SUCCESS;

}


/*****************************************************************************/
/* Function Name      : FsMiVlanHwClearCVlanStat                             */
/*                                                                           */
/* Description        : This function is called to Clear the customer-vlan   */
/*                      stat                                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u2Port      - Interface ID.                          */
/*                      u2CVlanId   - Customer Vlan ID.                      */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On VLAN_SUCCESS                        */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4 FsMiVlanHwClearCVlanStat ( UINT2 u2Port,
                               UINT2 u2CVlanId)
{

		UNUSED_PARAM (u2Port);
		UNUSED_PARAM (u2CVlanId);
		return FNP_SUCCESS;

}

