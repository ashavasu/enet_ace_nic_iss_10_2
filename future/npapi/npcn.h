/****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved                     */
/*  $Id: npcn.h,v 1.1 2010/07/05 06:38:32 prabuc Exp $                                                                  */
/*  FILE NAME             : npcn.h                                          */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : CN                                              */
/*  MODULE NAME           : CN-NPAPI_STUBS                                  */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This file contains the NPAPI stub functions     */
/*                          macros and includes for Aricent CN Module.      */
/****************************************************************************/

#ifndef __NP_CN_H__
#define __NP_CN_H__

#include "cnnp.h"

#endif  /* __NP_CN_H__ */

