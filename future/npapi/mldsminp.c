/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldsminp.c,v 1.2 2007/02/01 14:59:31 iss Exp $
 *
 * Description: All Network Processor API functions for MI MLD 
 *              done here
 *******************************************************************/

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "snp.h"
#include "npapi.h"
#include "npmimlds.h"

/*****************************************************************************/
/* Function Name      : FsMiMldsHwEnableMldSnooping                            */
/*                                                                           */
/* Description        : This function enables MLD snooping feature in the    */
/*                      hardware. This creates and installs filter needed    */
/*                      for snooping feature                                 */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiMldsHwEnableMldSnooping (UINT4 u4Instance)
{
    UNUSED_PARAM (u4Instance);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiMldsHwDisableMldSnooping                           */
/*                                                                           */
/* Description        : This function disables MLD snooping feature in the  */
/*                      hardware. This removes & destroys filter needed      */
/*                      for snooping feature                                 */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiMldsHwDisableMldSnooping (UINT4 u4Instance)
{
    UNUSED_PARAM (u4Instance);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiNpUpdateIp6mcFwdEntries                             */
/*                                                                           */
/* Description        : This function Updates IP Multicast forwarding entries*/
/*                      in the hardwarei based on the EventType.             */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiNpUpdateIp6mcFwdEntries (UINT4 u4Instance, tSnoopVlanId VlanId,
                             UINT1 *pGrpAddr, UINT1 *pSrcAddr,
                             tPortList PortList, tPortList UntagPortList,
                             UINT1 u1EventType)
{
    UNUSED_PARAM (u4Instance);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pGrpAddr);
    UNUSED_PARAM (pSrcAddr);
    UNUSED_PARAM (PortList);
    UNUSED_PARAM (UntagPortList);
    UNUSED_PARAM (u1EventType);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiNpGetIp6FwdEntryHitBitStatus                          */
/*                                                                           */
/* Description        : This function gets how many times this forwarding    */
/*                      entry is used for sending data traffic               */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Id                             */
/*                      VlanId - vlan id                                     */
/*                      pGrpAddr - Group Address                            */
/*                      pSrcAddr - Source Address                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiNpGetIp6FwdEntryHitBitStatus (UINT4 u4Instance, tSnoopVlanId VlanId,
                                  UINT1 *pGrpAddr, UINT1 *pSrcAddr)
{
    UNUSED_PARAM (u4Instance);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pGrpAddr);
    UNUSED_PARAM (pSrcAddr);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiMldsHwEnableIpMldSnooping                          */
/*                                                                           */
/* Description        : This function is called when IP  MLD is enabled      */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiMldsHwEnableIpMldSnooping (UINT4 u4Instance)
{
    UNUSED_PARAM (u4Instance);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiMldsHwDisableIpMldSnooping                         */
/*                                                                           */
/* Description        : This function is called when IP MLD is disabled.     */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiMldsHwDisableIpMldSnooping (UINT4 u4Instance)
{
    UNUSED_PARAM (u4Instance);
    return FNP_SUCCESS;
}
