/***************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                    */
/* $Id: vlmpbnpx.c,v 1.5 2012/07/26 09:42:25 siva Exp $                    */                                                    */
/*  FILE NAME             : vlmpbnpx.c                                     */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*  SUBSYSTEM NAME        : VLAN Module                                    */
/*  MODULE NAME           : VLAN                                           */
/*  LANGUAGE              : C                                              */
/*  TARGET ENVIRONMENT    : Any                                            */
/*  DATE OF FIRST RELEASE : 07 DEC 2006                                    */
/*  AUTHOR                : Aricent Inc.                                   */
/*  DESCRIPTION           : This file contains VLAN NP Routine             */
/*                                                                         */
/***************************************************************************/

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "npapi.h"
#include "npvlnmpb.h"

/****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwProviderBridgePortType.               */
/*                                                                          */
/* Description        : This function is called when the port type is       */
/*                      configured for a port.                              */
/*                                                                          */
/* Input(s)           : u4ContextId - Virtual Context Identifier            */
/*                      u4IfIndex - Port Index.                             */
/*                     u4PortType - CUSTOMER_NETWORK_PORT/CUSTOMER_EDGE_PORT*/
/*                      PROVIDER_NETWORK_PORT                
 *                      pSlotInfo - Slot Information                        */
/*                                                                          */
/* Output(s)          : None                                                */
/*                                                                          */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                        */
/*                      FNP_FAILURE - On failure                            */
/****************************************************************************/
INT4
FsMiVlanMbsmHwSetProviderBridgePortType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                         UINT4 u4PortType,
                                         tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PortType);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPortIngressEtherType.               */
/*                                                                           */
/* Description        : This function is called when the ingress ether type  */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2EtherType - Configured Ingress Ether type          */
/*                      pSlotInfo - Slot Information                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortIngressEtherType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                       UINT2 u2EtherType,
                                       tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2EtherType);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPortEgressEtherType.                */
/*                                                                           */
/* Description        : This function is called when the Egress ether type   */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2EtherType - Configured Egress Ether type           */
/*                      pSlotInfo - Slot Information                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortEgressEtherType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      UINT2 u2EtherType,
                                      tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2EtherType);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPortSVlanTranslationStatus.         */
/*                                                                           */
/* Description        : This function is called when the SVLAN Translation   */
/*                      status onfigured for a port.                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                      pSlotInfo - Slot Information                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortSVlanTranslationStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                             UINT1 u1Status,
                                             tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwAddSVlanTranslationEntry.              */
/*                                                                           */
/* Description        : This function is called when an entry is configured  */
/*                      for an VID translation table                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2LocalSVlan - SVlan received in the packet          */
/*                      u2RelaySVlan - SVlan used in the bridge              */
/*                      pSlotInfo - Slot Information                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwAddSVlanTranslationEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                        UINT2 u2LocalSVlan, UINT2 u2RelaySVlan,
                                        tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalSVlan);
    UNUSED_PARAM (u2RelaySVlan);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPortEtherTypeSwapStatus.            */
/*                                                                           */
/* Description        : This function is called when the SVLAN Ether type    */
/*                      status onfigured for a port.                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                      pSlotInfo - Slot Information                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortEtherTypeSwapStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                          UINT1 u1Status,
                                          tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwAddEtherTypeSwapEntry.                 */
/*                                                                           */
/* Description        : This function is called when an entry is added       */
/*                      to the ether type swap table which is applied in     */
/*                      ingress of a packet.                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2LocalEtherType- Ether type received in the packet  */
/*                      u2RelayEtherType- Ether type understand by the bridge*/
/*                      pSlotInfo - Slot Information                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwAddEtherTypeSwapEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                     UINT2 u2LocalEtherType,
                                     UINT2 u2RelayEtherType,
                                     tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalEtherType);
    UNUSED_PARAM (u2RelayEtherType);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwAddSVlanMap.                           */
/*                                                                           */
/* Description        : This function is called when an entry is added for   */
/*                      Port in the SVLAN classification tables which is     */
/*                      assigned for that port                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      VlanSVlanMap-Structure containing info for the       */
/*                      Configured SVlan classification entry.               */
/*                      pSlotInfo - Slot Information                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwAddSVlanMap (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap,
                           tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanSVlanMap);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPortSVlanClassifyMethod             */
/*                                                                           */
/* Description        : This function is called when a port is configured    */
/*                      for a type of SVLAN classification tables which is   */
/*                      used in assigning SVLAN for untagged packets         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index                               */
/*                      u1TableType - Type of table assigned for that port
 *                       can take value from the enum tVlanSVlanTableType    */
/*                      pSlotInfo - Slot Information                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortSVlanClassifyMethod (UINT4 u4ContextId, UINT4 u4IfIndex,
                                          UINT1 u1TableType,
                                          tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1TableType);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}



/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwPortMacLearningLimit.                  */
/*                                                                           */
/* Description        : This function is called for configuring the Unicast  */
/*                      mac limit for a Port                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex  - Port identifier.                        */
/*                      u4MacLimit - Mac Limit of the Unicast Table ranges
 *                      < 0 - 4294967295 >                                   */
/*                      pSlotInfo - Slot Information                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwPortMacLearningLimit (UINT4 u4ContextId, UINT4 u4IfIndex,
                                    UINT4 u4MacLimit, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4MacLimit);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPortCustomerVlan.                   */
/*                                                                           */
/* Description        : This function is called for setting the port         */
/*                      configured customer vlan                             */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port IfIndex                             */
/*                      CVlanId - Configured Customer VLAN ID 
 *                                ranges < 1 - 4094>                         */
/*                      pSlotInfo - Slot Information                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortCustomerVlan (UINT4 u4ContextId, UINT4 u4IfIndex,
                                   tVlanId CVlanId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (CVlanId);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwCreateProviderEdgePort                 */
/*                                                                           */
/* Description        : This function creates a Provider Edge Port (logical  */
/*                      port) in hardware.                                   */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId   - Service Vlan Id ranges < 1 - 4094 >.     */
/*                      PepConfig - Contains values to be used for PVID,     */
/*                                  Default user priority, Acceptable        */
/*                                  frame types, enable ingress filtering    */
/*                                  on port creation.                        */
/*                      pSlotInfo - Slot Information                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwCreateProviderEdgePort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      tVlanId SVlanId,
                                      tHwVlanPbPepInfo PepConfig,
                                      tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (PepConfig);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPcpEncodTbl.                        */
/*                                                                           */
/* Description        : This function sets/modifies PCP encoding table entry */
/*                      for the given port, given PCP selection row, given   */
/*                      priority, given drop_eligible value.                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                    : u4IfIndex - Port IfIndex.                            */
/*                      NpPbVlanPcpInfo - Entry containing the following 
 *                                        fields:                            */
/*                          u2PcpSelRow - PCP selection row where            */
/*                                        modification will be done.Can be
 *                                        VLAN_8P0D_SEL_ROW,
 *                                        VLAN_7P1D_SEL_ROW,
 *                                        VLAN_6P2D_SEL_ROW,
 *                                        VLAN_5P3D_SEL_ROW.
 *                          u2Priority  - Priority value ranges < 0 - 7 >    */
/*                          u1DropEligible - Drop Eligible 
 *                                          value VLAN_SNMP_TRUE,
 *                                          VLAN_SNMP_FALSE.                 */
/*                          u2PcpVlaue  - PCP value to be set for the above  */
/*                                        input ranges < 0 - 7 >.            */
/*                      pSlotInfo - Slot Information                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPcpEncodTbl (UINT4 u4ContextId, UINT4 u4IfIndex,
                              tHwVlanPbPcpInfo NpPbVlanPcpInfo,
                              tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (NpPbVlanPcpInfo);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPcpDecodTbl.                        */
/*                                                                           */
/* Description        : This function sets/modifies PCP decoding table entry */
/*                      for the given port, given PCP selection row, given   */
/*                      PCP value.                                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                    : u4IfIndex - Port IfIndex.                            */
/*                      PcpTblEntry - Entry containing the following fields: */
/*                          u2PcpSelRow - PCP selection row where            */
/*                                        modification will be done.can take */
/*                                        VLAN_8P0D_SEL_ROW,                 */
/*                                        VLAN_7P1D_SEL_ROW,                 */
/*                                        VLAN_6P2D_SEL_ROW,                 */
/*                                        VLAN_5P3D_SEL_ROW.                 */
/*                          u2PcpVlaue  - PCP value < 0 -7 >                 */
/*                          u2Priority  - Priority value to be set for the   */
/*                                        about input (port, PcpSel, pcp).
 *                                        can take <0-7>  */
/*                          u1DropEligible - Drop Eligible value to be set   */
/*                                           for the above input             */
/*                                           (port, PcpSel, Pcp) can take
 *                                           VLAN_SNMP_FALSE,VLAN_SNMP_TRUE  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPcpDecodTbl (UINT4 u4ContextId, UINT4 u4IfIndex,
                              tHwVlanPbPcpInfo NpPbVlanPcpInfo,
                              tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (NpPbVlanPcpInfo);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/* Use-DEI parameter. */
/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPortUseDei.                         */
/*                                                                           */
/* Description        : This function sets the Use_DEI parameter for the     */
/*                      given port.                                          */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                    : u4IfIndex - Port IfIndex.                            */
/*                      u1UseDei  - True/False for Use_DEI parameter for     */
/*                                  this port.can take VLAN_SNMP_FALSE,
 *                                  VLAN_SNMP_TRUE                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortUseDei (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1UseDei,
                             tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1UseDei);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/* Require Drop Encoding parameter. */
/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPortReqDropEncoding.                */
/*                                                                           */
/* Description        : This function sets the Require Drop Encoding         */
/*                      parameter for the given port.                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Current Context Id.
 *                      u4IfIndex - Port Index.                              */
/*                      u1ReqDropEncoding  - True/False value for Require    */
/*                                           Drop encoding parameter for     */
/*                                           this port.can take 
 *                                           VLAN_SNMP_FALSE,
 *                                           VLAN_SNMP_TRUE
 *                      pSlotInfo - Pointer to the Slot information Structure*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortReqDropEncoding (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      UINT1 u1ReqDrpEncoding,
                                      tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1ReqDrpEncoding);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/* Port Priority Code Point Selection Row. */
/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPortPcpSelRow.                      */
/*                                                                           */
/* Description        : This function sets the Port Priority Code Point      */
/*                      Selection for the given port.                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                    : u4IfIndex - Port Index.                              */
/*                      u2PcpSelection  - It can take the following values:  */
/*                                        VLAN_8P0D_SEL_ROW,                 */
/*                                        VLAN_7P1D_SEL_ROW,                 */
/*                                        VLAN_6P2D_SEL_ROW,                 */
/*                                        VLAN_5P3D_SEL_ROW.                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortPcpSelection (UINT4 u4ContextId, UINT4 u4IfIndex,
                                   UINT2 u2PcpSelection,
                                   tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2PcpSelection);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/* Internal CNP - Service Priority Regeneration Table NPAPIs */
/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetServicePriRegenEntry                */
/*                                                                           */
/* Description        : This function sets the service priority              */
/*                      regeneration table entry.                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                    : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id < 1 - 4094 >.           */
/*                      i4RecvPriority - Receive Priority < 0 - 7 >          */
/*                      i4RegenPriority - Regenerated Priority can take 
 *                                                        <0-7>              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetServicePriRegenEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                       tVlanId SVlanId, INT4 i4RecvPriority,
                                       INT4 i4RegenPriority,
                                       tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (i4RecvPriority);
    UNUSED_PARAM (i4RegenPriority);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwMulticastMacTableLimit                     */
/*                                                                           */
/* Description        : This function is called for configuring the Multicast*/
/*                      mac limit  for the bridge                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4MacLimit - Mac Limit of the Multicast Table        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwMulticastMacTableLimit (UINT4 u4ContextId, UINT4 u4MacLimit,
                                      tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4MacLimit);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}
