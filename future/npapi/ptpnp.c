/*****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpnp.c,v 1.8 2014/01/03 12:38:48 siva Exp $
 *
 * Description: This file contains the PTP NPAPIs that will be used for
 *              programming the hardware chipsets.
 *****************************************************************************/

#ifndef _PTPNP_C_
#define _PTPNP_C_

#include "lr.h"
#include "cfa.h"
#include "ptp.h"
#include "npapi.h"
#include "ptpnp.h"

/***************************************************************************
 * Function Name             : FsNpHwConfigPtpInfo
 *
 * Description               : This function is invoked to get or set
 *                             PTP information in hardware
 *
 * Input                     : u1InfoTye - type of information needs to be 
 *                             get or set in from hardware.
 *                             pPtpHwInfo - pointer to hardware information
 *                             structure
 *
 * Output                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * Returns                   : FNP_SUCCESS/FNP_FAILURE
 *
 **************************************************************************/
INT4
FsNpHwConfigPtpInfo (tPtpHwInfo * pPtpHwPtpInfo)
{
    switch (pPtpHwPtpInfo->u4InfoType)
    {
        case PTP_HW_SET_STATUS:
            return (FNP_SUCCESS);

        case PTP_HW_SET_TRANSMIT_TS_OPTION:
            /* This NPAPI will be invoked by the PTP module to configure the
             * following options.

             1.  Domain matching enable/disable
             2.  Alternate Master Time stamp enable/disable
             3.  Layer 2 PTP detection enable/disable
             4.  IPV6 PTP detection enable/disable
             5.  IPV4 PTP detection enable/disable
             6.  Receive Time Stamping enable/disable
             7.  Appending Time Stamping to Packet Enable/Disable
             8.  Inserting Time stamp Enable/Disable
             9.  Time stamp seconds Enable/Disable
             10. Enabling Timestamp generation for IANA assigned IP 
             destination address

             This NPAPI will be invoked even if any one of the parameters are
             configured. If there is no change of the values than that of the
             previous values, than for those parameters, OSIX_INVALID_VALUE
             will be passed as inputs.

             This API will contain a valid pSlotInfo field only when the API is
             invoked to program the particular slot. Otherwise this value will
             be NULL.

             This NPAPI will be invoked whenever there is a change in these
             configured parameters through the configuration handler sub
             module. Whenever PTP is enabled in the software, this NPAPI 
             will be invoked to configure trapping of PTP packets to CPU and
             the clock type  (One step or Two Step).
             */
            return (FNP_SUCCESS);

        case PTP_HW_SET_RECEIVE_TS_OPTION:
            /* This NPAPI will be invoked by the PTP module to configure
             * the following options.
             * 1.  Domain matching enable/disable
             * 2.  Alternate Master Time stamp enable/disable
             * 3.  Layer 2 PTP detection enable/disable
             * 4.  IPV6 PTP detection enable/disable
             * 5.  IPV4 PTP detection enable/disable
             * 6.  Receive Time Stamping enable/disable
             * 7.  Appending Time Stamping to Packet Enable/Disable
             * 8.  Inserting Time stamp Enable/Disable
             * 9.  Time stamp seconds Enable/Disable
             * 10. Enabling Timestamp generation for IANA assigned IP
             *     destination address
             * This NPAPI will be invoked even if any one of the parameters are
             * configured.
             * This API will contain a valid "pSlotInfo" field only when the 
             * API is invoked to program the particular slot. Otherwise this
             * value will be NULL.
             * */
            return (FNP_SUCCESS);

        case PTP_HW_SET_DOMAIN_MODE:
            /* This NPAPI will be invoked by PTP module to program the 
             * domain mode into the chipset. The value of the domain mode can
             * take any one of the following values:
             * Boundary (1) - Boundary clock
             * Ordinary (2) - Ordinary Clock
             * Transparent (3) - Transparent Clock
             * Forward (4) - Forward Mode. PTP does not operate in the 
             *               software the PTP packets are switched at wire rate.
             * */
            return (FNP_SUCCESS);
        case PTP_HW_SET_CLOCK_ADJUSTMENT_FACTOR:
            /* This NPAPI will be invoked to set rate adjustment. This NPAPI
             * will be invoked whenever the rate adjustment is estimated by
             * the PTP module. This NPAPI should be invoked only when there 
             * is a change from the previous value and that happens only in 
             * primary context and primary domain. This API will contain a 
             * valid "pSlotInfo" field only when the API is invoked to program
             * the particular slot. Otherwise this value will be NULL
             * */
            return (FNP_SUCCESS);

        case PTP_HW_SET_CLOCK:
            /* This NPAPI will be invoked to set the system time. This NPAPI
             * will be invoked whenever the node detects a drift in the system
             * time from the master. The programming will happen only when the
             * observed drift is present in primary context and primay domain
             * This API will contain a valid "pSlotInfo" field only when the
             * API is invoked to program the particular slot. Otherwise this 
             * value will be NULL.
             * */
            return (FNP_SUCCESS);

        case PTP_HW_SET_SRC_ID_HASH:
            /* Source Identity hash value is one of the filter parameter used
             * in the receive packet parser/timestamp unit of the chip. This 
             * hash value is computed based on the source portIdentity field 
             * (octet 20 - 29) of the received ptp message. If this filter 
             * option is enabled in the chipset, the incoming packets are
             * matched to the source identity hash value against the programmed
             * hash value in the chipset. If the value is not matched, then the
             * message will not be timestamped. In order to support this,
             * this NPAPI is being programmed.
             * */
            return (FNP_SUCCESS);

        case PTP_HW_INIT:
            /* Add the PTP filter for receiving the L2 PTP packets and UDP
             * packets
             */

            return (FNP_SUCCESS);
        case PTP_HW_DEINIT:
            /* Remove the PTP filter for receiving the L2 PTP packets and UDP
             * packets
             */
            return (FNP_SUCCESS);

        case PTP_HW_GET_PKT_LOG_TIMESTAMP:
            /* This is invoked to get the logged time stamp of the packet 
             * sent or received from PTP */
            return (FNP_SUCCESS);

        case PTP_HW_CONFIG_PEER_DELAY:
            /* This is to program the peer delay for the given interface */
            return (FNP_SUCCESS);
        case PTP_HW_GET_CLOCK:
            /* This is invoked to get the time from Hardware */
            return (FNP_SUCCESS);

        default:
            return FNP_FAILURE;

    }
}

/***************************************************************************
 * FUNCTION NAME             : FsNpHwSetPtpStatus
 *
 * DESCRIPTION               : This API is obselete.
 *                             The hardware chipset needs to enable/disable
 *                             status of the PTP over this virtual interface 
 *                             once this API is triggered. 
 *
 * INPUT                     : pPtpHwPtpStatusInfo
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : FNP_SUCCESS/FNP_FAILURE
 *
 **************************************************************************/
INT4
FsNpHwSetPtpStatus (tPtpHwPtpStatusInfo * pPtpHwPtpStatusInfo)
{
    UNUSED_PARAM (pPtpHwPtpStatusInfo);

    return FNP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME             : FsNpHwSetPtpTransmitTSOption
 *
 * DESCRIPTION               : This API is obselete. 
 *                             This NPAPI will be invoked by the PTP module to
 *                             configure various Transmit options for the 
 *                             chipsets.
 *
 * INPUT                     : pPtpHwTransmitTsOpt
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : FNP_SUCCESS/FNP_FAILURE
 *
 **************************************************************************/
INT4
FsNpHwSetPtpTransmitTSOption (tPtpHwTransmitTsOpt * pPtpHwTransmitTsOpt)
{
    /* This NPAPI will be invoked by the PTP module to configure the 
     * following options.

     1.  Domain matching enable/disable
     2.  Alternate Master Time stamp enable/disable
     3.  Layer 2 PTP detection enable/disable
     4.  IPV6 PTP detection enable/disable
     5.  IPV4 PTP detection enable/disable
     6.  Receive Time Stamping enable/disable
     7.  Appending Time Stamping to Packet Enable/Disable
     8.  Inserting Time stamp Enable/Disable
     9.  Time stamp seconds Enable/Disable
     10. Enabling Timestamp generation for IANA assigned IP destination address

     This NPAPI will be invoked even if any one of the parameters are 
     configured. If there is no change of the values than that of the previous 
     values, than for those parameters, OSIX_INVALID_VALUE will be passed as 
     inputs.

     This API will contain a valid pSlotInfo field only when the API is 
     invoked to program the particular slot. Otherwise this value will be NULL.

     This NPAPI will be invoked whenever there is a change in these configured 
     parameters through the configuration handler sub module. Whenever PTP is
     enabled in the software, this NPAPI will be invoked to configure trapping 
     of PTP packets to CPU and the clock type  (One step or Two Step).
     */

    UNUSED_PARAM (pPtpHwTransmitTsOpt);
    return FNP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME             : FsNpHwSetPtpReceiveTSOption
 *
 * DESCRIPTION               : This API is obselete.
 *                             This NPAPI will be invoked by the PTP module to
 *                             configure the receive Time stamp options for the 
 *                             given interface.
 *
 * INPUT                     : pPtpHwRecvTsOpt
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : FNP_SUCCESS/FNP_FAILURE
 *
 **************************************************************************/
INT4
FsNpHwSetPtpReceiveTSOption (tPtpHwRecvTsOpt * pPtpHwRecvTsOpt)
{
    /* This NPAPI will be invoked by the PTP module to configure the following
     * options.
     * 1.  Domain matching enable/disable
     * 2.  Alternate Master Time stamp enable/disable
     * 3.  Layer 2 PTP detection enable/disable
     * 4.  IPV6 PTP detection enable/disable
     * 5.  IPV4 PTP detection enable/disable
     * 6.  Receive Time Stamping enable/disable
     * 7.  Appending Time Stamping to Packet Enable/Disable
     * 8.  Inserting Time stamp Enable/Disable
     * 9.  Time stamp seconds Enable/Disable
     * 10. Enabling Timestamp generation for IANA assigned IP destination 
     * address
     * This NPAPI will be invoked even if any one of the parameters are 
     * configured. 
     * This API will contain a valid "pSlotInfo" field only when the API is 
     * invoked to program the particular slot. Otherwise this value will be
     * NULL.
     * */

    UNUSED_PARAM (pPtpHwRecvTsOpt);
    return FNP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME             : FsNpPtpHwSetClk
 *
 * DESCRIPTION               : This API is obselete.
 *                             This NPAPI will be invoked by the PTP module to
 *                             configure the clock of the port.
 *
 * INPUT                     : pPtpHwPtpClk
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : FNP_SUCCESS/FNP_FAILURE
 *
 **************************************************************************/
INT4
FsNpPtpHwSetClk (tPtpHwPtpClk * pPtpHwPtpClk)
{
    /* This NPAPI will be invoked to set the system time. This NPAPI will be
     * invoked whenever the node detects a drift in the system time from the 
     * master. The programming will happen only when the observed drift is
     * present in primary context and primay domain
     *
     * This API will contain a valid "pSlotInfo" field only when the API is 
     * invoked to program the particular slot. Otherwise this value will be
     * NULL.
     * */
    UNUSED_PARAM (pPtpHwPtpClk);
    return FNP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME             : FsNpPtpHwSetClkAdj
 *
 * DESCRIPTION               : This API is obselete.
 *                             This NPAPI will be invoked by the PTP module to 
 *                             configure rate adjustments in the chipsets. The
 *                             rate adjustment value corresponds to the
 *                             syntonization ratio configured.
 *
 * INPUT                     : pPtpHwPtpClkRateAdj
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : FNP_SUCCESS/FNP_FAILURE
 *
 **************************************************************************/
INT4
FsNpPtpHwSetClkAdj (tPtpHwPtpClkRateAdj * pPtpHwPtpClkRateAdj)
{
    /* This NPAPI will be invoked to set rate adjustment. This NPAPI will be
     * invoked whenever the rate adjustment is estimated by the PTP module. 
     * This NPAPI should be invoked only when there is a change from the 
     * previous value and that happens only in primary context and primary
     * domain.
     * This API will contain a valid "pSlotInfo" field only when the API is
     * invoked to program the particular slot. Otherwise this value will be NULL
     * */
    UNUSED_PARAM (pPtpHwPtpClkRateAdj);
    return FNP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME             : FsNpPtpHwTimeStampCallBack
 *
 * DESCRIPTION               : This Call back will be invoked by the PTP module
 *                             to get the time stamp for the packet.
 *
 * INPUT                     : pPtpHwPtpCbParams
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : FNP_SUCCESS/FNP_FAILURE
 *
 **************************************************************************/
INT4
FsNpPtpHwTimeStampCallBack (tPtpHwPtpCbParams * pPtpHwPtpCbParams)
{

    /* This is a call back interface provided by the PTP module to the hardware
     * If the clock is operating as a two step clock, then the chipset needs to 
     * provide the transmitted time stamp which will be used by the PTP for
     * follow up messages.
     * To indicate the follow up message, chipset should use this callback API. 
     * This will just post the event to the PTP module and return from there on
     * so that driver thread is not prolonged.
     * */
    if (PtpApiTriggerPtpFollowUp
        (pPtpHwPtpCbParams->u4ContextId,
         pPtpHwPtpCbParams->u4IfIndex,
         &(pPtpHwPtpCbParams->FsClkTimeVal),
         pPtpHwPtpCbParams->u1DomainId,
         pPtpHwPtpCbParams->u1MsgType) == OSIX_FAILURE)
    {
        return FNP_FAILURE;
    }

    return FNP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME             : FsNpPtpSetDomainMode
 *
 * DESCRIPTION               : This API is obselete.
 *                             This NPAPI will be invoked by the PTP module to 
 *                             configure the domain mode of the clock.
 *
 * INPUT                     : pPtpHwDmnModeParams
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : FNP_SUCCESS/FNP_FAILURE
 *
 **************************************************************************/
INT4
FsNpPtpSetDomainMode (tPtpHwDmnModeParams * pPtpHwDmnModeParams)
{
    /* This NPAPI will be invoked by PTP module to program the domain mode
     * into the chipset. The value of the domain mode can take any one of the
     * following values
     * Boundary (1) - Boundary clock
     * Ordinary (2) - Ordinary Clock
     * Transparent (3) - Transparent Clock
     * Forward (4) - Forward Mode. PTP does not operate in the software the PTP
     *               packets are switched at wire rate.
     * */
    UNUSED_PARAM (pPtpHwDmnModeParams);
    return FNP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME             : FsNpPtpHwSetSrcPortIdHash 
 *
 * DESCRIPTION               : This API is obselete.
 *                             This NPAPI will be invoked by the PTP module to 
 *                             program the Source Port Identity hash. 
 *
 * INPUT                     : pPtpHwSrcIdHash - Pointer to the Src Id hash
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : FNP_SUCCESS/FNP_FAILURE
 *
 **************************************************************************/
INT4
FsNpPtpHwSetSrcPortIdHash (tPtpHwSrcIdHash * pPtpHwSrcIdHash)
{
    /* Source Identity hash value is one of the filter parameter used in the 
     * receive packet parser/timestamp unit of the chip. This hash value is
     * computed based on the source portIdentity field (octet 20 - 29) of the
     * received ptp message. If this filter option is enabled in the chipset,
     * the incoming packets are matched to the source identity hash value 
     * against the programmed hash value in the chipset. If the value is not 
     * matched, then the message will not be timestamped. In order to support
     * this, this NPAPI is being programmed.
     * */
    UNUSED_PARAM (pPtpHwSrcIdHash);
    return FNP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME             : FsNpPtpHwInit
 *
 * DESCRIPTION               : This API is obselete.
 *                             This NPAPI will be add the PTP filters in the
 *                             hardware.
 *
 * INPUT                     : None
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : FNP_SUCCESS/FNP_FAILURE
 *
 **************************************************************************/

INT4
FsNpPtpHwInit (tPtpHwLog * pPtpHwLog)
{
    /* Add the PTP filter for receiving the L2 PTP packets and UDP
     * packets
     */
    UNUSED_PARAM (pPtpHwLog);
    return FNP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME             : FsNpPtpHwDeInit
 *
 * DESCRIPTION               : This API is obselete.
 *                             This NPAPI will be remove the PTP filters
 *                             from the hardware.
 *
 * INPUT                     : None
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : FNP_SUCCESS/FNP_FAILURE
 *
 **************************************************************************/

INT4
FsNpPtpHwDeInit ()
{
    /* Remove the PTP filter for receiving the L2 PTP packets and UDP
     * packets
     */
    return FNP_SUCCESS;
}
#endif /* _PTPNP_C_ */
