/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: poenp.c,v 1.4 2007/02/01 14:59:31 iss Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/
#include "lr.h"
#include "poe.h"
#include "nppoe.h"
#include "npapi.h"
/*****************************************************************************/
/*    Function Name       : FsPoeHwInit                                      */
/*                                                                           */
/*    Description         : This function takes care of initialising the     */
/*                          hardware related parameters.                     */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
INT4
FsPoeHwInit (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsPoeHwDeInit                                    */
/*                                                                           */
/*    Description         : This function takes care of deinitialising the   */
/*                          hardware related parameters.                     */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
INT4
FsPoeHwDeInit (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsPoeGetPseStatus                                */
/*                                                                           */
/*    Description         : This function gets the status of the power source*/
/*                          equipment.                                       */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
INT4
FsPoeGetPseStatus (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsPoeDetectAndClassifyPD                         */
/*                                                                           */
/*    Description         : This function applies detects and classifies     */
/*                          Powered Device                                   */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
INT4
FsPoeDetectAndClassifyPD (tPoePsePortEntry * pPoePsePortEntry, INT4 i4ErrStatus)
{
    UNUSED_PARAM (i4ErrStatus);
    UNUSED_PARAM (pPoePsePortEntry);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsPoeApplyPowerOnPI                              */
/*                                                                           */
/*    Description         : This function applies power on the given         */
/*                          powered Interface                                */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
INT4
FsPoeApplyPowerOnPI (tPoePsePortEntry * pPoePsePortEntry, INT4 i4ErrStatus)
{
    UNUSED_PARAM (i4ErrStatus);
    UNUSED_PARAM (pPoePsePortEntry);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsPoeRemovePowerFromPI                           */
/*                                                                           */
/*    Description         : This function removes power from the given       */
/*                          powered Interface                                */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
INT4
FsPoeRemovePowerFromPI (tPoePsePortEntry * pPoePsePortEntry, INT4 i4ErrStatus)
{
    UNUSED_PARAM (i4ErrStatus);
    UNUSED_PARAM (pPoePsePortEntry);
    return FNP_SUCCESS;
}
