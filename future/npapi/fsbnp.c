/*************************************************************************
* Copyright (C) 2007-2012 Aricent Group . All Rights Reserved
*
* $Id: fsbnp.c,v 1.3 2017/10/09 13:14:11 siva Exp $
*
* Description: This file contains the function implementations of FSB NP-API.
*
*************************************************************************/
#ifndef _FSBNP_C_
#define _FSBNP_C_

#include "lr.h"
#include "npapi.h"
#include "fsb.h"
#include "fsbnp.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsMiNpFsbHwInit                                  */
/*                                                                           */
/*    Description         : The NPAPI installs the Default VLAN ACL          */
/*                          required for switching & copying the             */
/*                          FIP VLAN Discovery frames to the FIP Snooping    */
/*                          module, executing in the control plane CPU.      */
/*                          This is called while FSB module is enabled.      */
/*                                                                           */
/*    Input(s)            : pFsbHwFilterEntry - Inputs to install filter     */
/*                                                                           */
/*    Output(s)           : pFsbLocRemFilterId - Pointer to                  */
/*                          Filter ID structure                              */
/*                                                                           */
/*    Returns             : FNP_SUCCESS / FNP_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsMiNpFsbHwInit (tFsbHwFilterEntry * pFsbHwFilterEntry,
                 tFsbLocRemFilterId * pFsbLocRemFilterId)
{
    UNUSED_PARAM (pFsbHwFilterEntry);
    UNUSED_PARAM (pFsbLocRemFilterId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsFsbHwCreateFilter                              */
/*                                                                           */
/*    Description         : This NP call installs filters for default ACLS   */
/*                          (for the FIP Snooping Enabled FCOE Vlans)        */
/*                          and ACL's required for the FIP sessions          */
/*                                                                           */
/*    Input(s)            : pFsbHwFilterEntry - Inputs to install filter     */
/*                                                                           */
/*    Output(s)           : pu4FilterId - Filter ID                          */
/*                                                                           */
/*    Returns             : FNP_SUCCESS / FNP_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsFsbHwCreateFilter (tFsbHwFilterEntry * pFsbHwFilterEntry, UINT4 *pu4FilterId)
{
    UNUSED_PARAM (pFsbHwFilterEntry);
    UNUSED_PARAM (pu4FilterId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsFsbHwDeleteFilter                              */
/*                                                                           */
/*    Description         : This NP call deletes the filter installed        */
/*                                                                           */
/*    Input(s)            : pFsbHwFilterEntry - Inputs to install filter     */
/*                          u4FilterId - Filter id to be removed             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FNP_SUCCESS / FNP_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsFsbHwDeleteFilter (tFsbHwFilterEntry * pFsbHwFilterEntry, UINT4 u4FilterId)
{
    UNUSED_PARAM (pFsbHwFilterEntry);
    UNUSED_PARAM (u4FilterId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsMiNpFsbHwDeInit                                */
/*                                                                           */
/*    Description         : This NP call deletes the filter installed        */
/*                          when FSB module is disabled                      */
/*                                                                           */
/*    Input(s)            : pFsbHwFilterEntry - Inputs to install filter     */
/*                        : FsbLocRemFilterId - Filter Id structure          */
/*                                                                           */
/*    Output(s)           : NONE                                             */
/*                                                                           */
/*    Returns             : FNP_SUCCESS / FNP_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsMiNpFsbHwDeInit (tFsbHwFilterEntry * pFsbHwFilterEntry,
                   tFsbLocRemFilterId FsbLocRemFilterId)
{
    UNUSED_PARAM (pFsbHwFilterEntry);
    UNUSED_PARAM (FsbLocRemFilterId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsbFsFsbHwCreateSChannelFilter                   */
/*                                                                           */
/*    Description         : This NP call installs filters in the lookup      */
/*                          stage based on the outer vlan and physical       */
/*                          port with the action as class id set             */
/*                                                                           */
/*    Input(s)            : pFsbHwSChannelFilterEntry -Inputs to             */
/*                          install filter                                   */
/*                                                                           */
/*    Output(s)           : pu4FilterId - Filter ID                          */
/*                                                                           */
/*    Returns             : FNP_SUCCESS / FNP_FAILURE                        */
/*                                                                           */
/*****************************************************************************/

INT4
FsFsbHwCreateSChannelFilter (tFsbHwSChannelFilterEntry *
                             pFsbHwSChannelFilterEntry, UINT4 *pu4FilterId)
{
    UNUSED_PARAM (pFsbHwSChannelFilterEntry);
    UNUSED_PARAM (pu4FilterId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsFsbHwDeleteSChannelFilter                      */
/*                                                                           */
/*    Description         : This NP call deletes the filter installed        */
/*                                                                           */
/*    Input(s)            : pFsbHwSChannelFilterEntry - Pointer to filter    */
/*                          entry structure                                  */
/*                          u4FilterId - Filter id to be removed             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FNP_SUCCESS / FNP_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsFsbHwDeleteSChannelFilter (tFsbHwSChannelFilterEntry *
                             pFsbHwSChannelFilterEntry, UINT4 u4FilterId)
{
    UNUSED_PARAM (pFsbHwSChannelFilterEntry);
    UNUSED_PARAM (u4FilterId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsFsbHwSetFCoEParams                             */
/*                                                                           */
/*    Description         : This NP call remove or add the stacking port from*/
/*                          FCoE VLAN, if the FCoE VLAN status is enabled or */
/*                          disabled respectively.                           */
/*                                                                           */
/*    Input(s)            : pFsbHwVlanEntry - VLAN Id, VLAN enabled Status   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FNP_SUCCESS / FNP_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsFsbHwSetFCoEParams (tFsbHwVlanEntry * pFsbHwVlanEntry)
{
    UNUSED_PARAM (pFsbHwVlanEntry);
    return FNP_SUCCESS;
}
#endif
