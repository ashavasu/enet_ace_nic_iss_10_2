/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rstnp.c,v 1.14 2007/07/17 13:29:53 iss Exp $
 *
 * Description: All network processor function  given here
 *******************************************************************/

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "rstp.h"
#include "npapi.h"
#include "nprstp.h"

/***********************************************************************  
 * FUNCTION NAME :  FsStpNpHwInit                                      *
 *                                                                     *
 * DESCRIPTION   :  Enables/Disables the Spanning tree support in the  *
 *                  hardware.                                          *
 *                                                                     *
 * INPUTS        :  None                                               *
 *                                                                     *
 * OUTPUTS       :  None                                               *
 *                                                                     *
 * RETURNS       :  FNP_SUCCESS - success                              *  
 *                  FNP_FAILURE - Error during setting                 *
 *                                                                     *
 * COMMENTS      :  None                                               *
 ***********************************************************************/
INT1
FsStpNpHwInit (VOID)
{

    return FNP_SUCCESS;

}

/***********************************************************************
 * FUNCTION NAME : FsRstpNpInitHw                                      *
 *                                                                     *
 * DESCRIPTION   : This function performs any necessary RSTP related   * 
 *                 initialisation in the Hardware                      *
 *                                                                     *
 * INPUTS        : None                                                *
 *                                                                     *
 * OUTPUTS       : None                                                *
 *                                                                     *
 * RETURNS       : None                                                *
 *                                                                     *
 ***********************************************************************/
VOID
FsRstpNpInitHw (VOID)
{
    return;
}

/**********************************************************************/
/* Function Name      : FsRstpNpSetPortState                          */
/*                                                                    */
/* Description        : Sets the RSTP Port State in the Hardware.     */
/*                      When RSTP is working in RSTP mode             */
/*                      or RSTP in STP compatible                     */
/*                      mode, porting should be done for this API.    */
/*                                                                    */
/*                                                                    */
/* Input(s)           : u4IfIndex - Interface Index.                  */
/*                      u1Status  - Status to be set.                 */
/*                           Values can be AST_PORT_STATE_DISCARDING  */
/*                                  or AST_PORT_STATE_LEARNING        */
/*                                  or AST_PORT_STATE_FORWARDING      */
/*                                                                    */
/* Output(s)          : None                                          */
/*                                                                    */
/* Global Variables                                                   */
/* Referred           : None                                          */
/*                                                                    */
/* Global Variables                                                   */
/* Modified           : None                                          */
/*                                                                    */
/* Return Value(s)    : FNP_SUCCESS - On successful set (or)          */
/*                      FNP_FAILURE - Error during setting            */
/**********************************************************************/
INT1
FsRstpNpSetPortState (UINT4 u4IfIndex, UINT1 u1Status)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    return FNP_SUCCESS;
}

/***********************************************************************/
/* Function Name      : FsRstNpGetPortState                            */
/*                                                                     */
/* Description        : Gets the RSTP Port State in the Hardware.      */
/*                      When RSTP is working in RSTP mode              */
/*                      or RSTP in STP compatible                      */
/*                      mode, porting should be done for this API.     */
/*                                                                     */
/*                                                                     */
/* Input(s)           : u4IfIndex - Port Number.                       */
/*                      pu1Status  - Status returned from Hardware.    */
/*                            Values can be AST_PORT_STATE_DISCARDING  */
/*                                  or AST_PORT_STATE_LEARNING         */
/*                                  or AST_PORT_STATE_FORWARDING       */
/*                                                                     */
/* Output(s)          : None                                           */
/*                                                                     */
/* Global Variables                                                    */
/* Referred           : None                                           */
/*                                                                     */
/* Global Variables                                                    */
/* Modified           : None                                           */
/*                                                                     */
/* Return Value(s)    : FNP_SUCCESS - On successful set (or)           */
/*                      FNP_FAILURE - Error during setting             */
/***********************************************************************/

INT1
FsRstNpGetPortState (UINT4 u4IfIndex, UINT1 *pu1Status)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1Status);
    return FNP_SUCCESS;
}
