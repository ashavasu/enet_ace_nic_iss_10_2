/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpnpx.c,v 1.2 2007/02/01 14:59:31 iss Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/
#ifndef _IGMPNPX_C_
#define _IGMPNPX_C_

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "npapi.h"
#include "igmpnp.h"

/*****************************************************************************/
/* Function Name      : FsIgmpMbsmHwEnableIgmp                               */
/*                                                                           */
/* Description        : This function is called when IGMP is enabled. It     */
/*                      sets up a filter in the hardware so that IGMP packets*/
/*                      are received in the CPU.                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsIgmpMbsmHwEnableIgmp (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

#endif
