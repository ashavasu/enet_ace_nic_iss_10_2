/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: npcfa.h,v 1.6 2012/05/10 13:20:16 siva Exp $
 *
 * Description: Exported file for CFA NP-API.
 *
 *******************************************************************/
#ifndef __NPCFA_H
#define __NPCFA_H
#include "cfanp.h"

#define CFA_NP_KNOWN_UCAST_PKT       1
#define CFA_NP_UNKNOWN_UCAST_PKT     2
#define CFA_NP_BCAST_PKT             3
#define CFA_NP_MCAST_PKT             4

#define CFA_NP_UNTAGGED              5
#define CFA_NP_TAGGED                6

#define CFA_NP_IS_PORT_CHANNEL(u2Port) \
        (((u2Port > BRG_MAX_PHY_PORTS) && (u2Port <= BRG_MAX_PHY_PLUS_LOG_PORTS)) \
        ? FNP_TRUE : FNP_FALSE)


#define CFA_NP_API             0x00000001

#ifdef MBSM_WANTED
PUBLIC INT4 CfaMbsmNpEnablePort PROTO ((UINT4 , INT4 ));
INT4 CfaMbsmNpRegisterProtocol PROTO ((UINT1 u1Protocol,
                                       tMbsmSlotInfo *pSlotInfo));
#endif

#endif /* __NPCFA_H */
