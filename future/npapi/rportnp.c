/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rportnp.c,v 1.6 2015/10/29 09:28:56 siva Exp $
 *
 * Description: This file contains the function  Routed Port NP-API.
 ****************************************************************************/
#include "lr.h"
#include "rportnp.h"

/******************************************************************************/
/*   Function Name             : FsNpL3Ipv4ArpAdd                             */
/*   Description               : Adds the Specified IP address and MAC address*/
/*                               to the H/W L3 IP Table for CFA_ENET type     */
/*                               interface Returns Full if no                 */
/*                               free entry exist in the L3 Ip Table          */
/*   Input(s)                  : u4VrId - The virtual router identifier      */
/*                               u4IfIndex - CFA L3 Vlan If Index             */
/*                               u4IpAddr  - IP address for which ARP is added*/
/*                               pMacAddr  -  MAC Address                     */
/*                               pu1IfName - Interface name                   */
/*                               i1State   - State of the ARP entry           */
/*   Output(s)                 : pbu1TblFull - Hw Table Full/not              */
/*                                            (FNP_FALSE/FNP_TRUE)            */
/*   Global Variables Referred : None                                         */
/*   Global variables Modified : None                                         */
/*   Exceptions                : None                                         */
/*   Use of Recursion          : None                                         */
/*   Returns                   : FNP_SUCCESS/FNP_FAILURE                      */
/******************************************************************************/

UINT4
FsNpL3Ipv4ArpAdd (UINT4 u4IfIndex, UINT4 u4IpAddr,
                  UINT1 *pMacAddr, UINT1 *pu1IfName,
                  INT1 i1State, UINT1 *pbu1TblFull)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (pMacAddr);
    UNUSED_PARAM (pu1IfName);
    UNUSED_PARAM (i1State);
    UNUSED_PARAM (pbu1TblFull);

    return FNP_SUCCESS;
}

/******************************************************************************/
/*   Function Name             : FsNpL3Ipv4ArpModify                          */
/*   Description               : Modify the ARP entry over the router port    */
/*   Input(s)                  : u4VrId - The virtual router identifier      */
/*                               u4IfIndex - CFA L3 Vlan If Index             */
/*                               u4IpAddr  - IP address for which ARP is added*/
/*                               pMacAddr  -  MAC Address                     */
/*                               pu1IfName - Interface name                   */
/*                               i1State   - State of the ARP entry           */
/*   Output(s)                 : None                                         */
/*   Global Variables Referred : None                                         */
/*   Global variables Modified : None                                         */
/*   Exceptions                : None                                         */
/*   Use of Recursion          : None                                         */
/*   Returns                   : FNP_SUCCESS/FNP_FAILURE                      */
/******************************************************************************/

UINT4
FsNpL3Ipv4ArpModify (UINT4 u4IfIndex, UINT4 u4IpAddr,
                     UINT1 *pMacAddr, UINT1 *pu1IfName, INT1 i1State)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (pMacAddr);
    UNUSED_PARAM (pu1IfName);
    UNUSED_PARAM (i1State);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name             :  FsNpIpv4L3IpInterface                       */
/*  Description               :  Create L3 IP Interface                      */
/*  Input(s)                  :  L3Action  --Action to be performed          */
/*                            :  i)Create                                    */
/*                            :  ii)Modify                                   */
/*                            :  i)Delete                                    */
/*                            :  pFsNpL3IfInfo -- Intrface Info              */
/*  Output(s)                 :  None                                        */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions                :  None                                        */
/*  Use of Recursion          :  None                                        */
/*  Returns                   :  FNP_FAILURE(0) on failure                   */
/*                               FNP_SUCCESS(1) on success                   */
/*****************************************************************************/
UINT4
FsNpIpv4L3IpInterface (tL3Action L3Action, tFsNpL3IfInfo * pFsNpL3IfInfo)
{

    UNUSED_PARAM (L3Action);
    UNUSED_PARAM (pFsNpL3IfInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name             :  FsNpIpv4GetEcmpGroups                       */
/*  Description               :  Get Max ECMP Groups Supported               */
/*  Input(s)                  :  NONE                                        */
/*  Output(s)                 :  pu4EcmpGroup                                */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions                :  None                                        */
/*  Use of Recursion          :  None                                        */
/*  Returns                   :  FNP_FAILURE(0) on failure                   */
/*                               FNP_SUCCESS(1) on success                   */
/*****************************************************************************/
UINT4
FsNpIpv4GetEcmpGroups (UINT4 *pu4EcmpGroups)
{
    UNUSED_PARAM (pu4EcmpGroups);
    return FNP_SUCCESS;
}
