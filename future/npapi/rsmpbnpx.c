
#ifndef _RSMIPBNPX_C_
#define _RSMIPBNPX_C_

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "rstp.h"
#include "npapi.h"
#include "nprstpmi.h"
#include "npcfa.h"
#include "rstmpbnp.h"
/***************************************************************************/
/* Function Name      : FsMiPbRstpMbsmNpSetPortState                       */
/*                                                                         */
/* Description        : Stub provided for backward compatibility with      */
/*                      MI-unaware hardware.                               */
/*                                                                         */
/* Input(s)           : u4ContextId - Virtual Switch ID                    */
/*                      u2PortNum - CEP Port Number.                       */
/*                      SVid      - Service VLAN Identifier ranges <1-4094>*/
/*                      u1Status  - Status to be set.                      */
/*                                  Values can be AST_PORT_STATE_DISCARDING*/
/*                                  or AST_PORT_STATE_LEARNING             */
/*                                  or AST_PORT_STATE_FORWARDING           */
/*                      pSlotInfo - Info of the slot inserted.             */
/*                                                                         */
/* Output(s)          : None                                               */
/*                                                                         */
/* Global Variables                                                        */
/* Referred           : None                                               */
/*                                                                         */
/* Global Variables                                                        */
/* Modified           : None                                               */
/*                                                                         */
/* Return Value(s)    : FNP_SUCCESS - On successful set (or)               */
/*                      FNP_FAILURE - Error during setting                 */
/***************************************************************************/
INT1
FsMiPbRstpMbsmNpSetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId Svid,
                              UINT1 u1Status, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (Svid);
    UNUSED_PARAM (u1Status);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}
#endif
