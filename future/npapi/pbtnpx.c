#ifdef PBBTE_WANTED

#ifndef _PBTNPX_C_
#define _PBTNPX_C_
#include "lr.h"
#include "npapi.h"
#include "pbtnp.h"

/*****************************************************************************/
/* Function Name      : FsMiPbbTeHwSetEspVid                                 */
/*                                                                           */
/* Description        : This function is called to configure a Vlan as       */
/*                      ESP-VLAN in the newly inserted card.                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      tVlanId     - Esp VLAN Identifier
 *                      pSlotInfo   - Slot Information                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiPbbTeMbsmHwSetEspVid (UINT4 u4ContextId, tVlanId EspVlanId,
                          tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (EspVlanId);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}
#endif
#endif
