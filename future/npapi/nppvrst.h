/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: nppvrst.h,v 1.2 2008/01/07 14:42:27 iss Exp $ 
 *
 * Description: All prototypes for Network Processor API functions for PVRST 
 * 
 *
 *******************************************************************/
#ifndef _NPPVRST_H
#define _NPPVRST_H

#include "pvrstnp.h"

INT4 FsPvrstCreateFilter (VOID);
INT4 FsPvrstHwCreateFilter (UINT1 *pFilterAddr, UINT4 u4PktType);
INT4 FsPvrstDestroyFilter (VOID);
INT4 FsPvrstHwDestroyFilter (UINT4 u4PktType);
INT4 FsPvrstCreateFP (VOID);
INT4 FsPvrstDestroyFP (VOID);
INT4 FsPvrstHwCreateFP (UINT1 *pFilterAddr, UINT4 u4PktType);
INT4 FsPvrstHwDestroyFP (UINT4 u4PktType);

#endif /* _NPPVRST_H */
