/*****************************************************************************
 *  * Description: This file contains the function implementations  of
 *   *              TAC NP-API.
 *    ****************************************************************************/
/* $Id: tacnp.c,v 1.1 2011/06/10 11:05:38 siva Exp $*/
#ifndef _TACNP_C_
#define _TACNP_C_
#include "npapi.h"
#include "tacnp.h"
/*****************************************************************************/
/* Function Name      : FsTacHwUpdateStatus                                  */
/*                                                                           */
/* Description        : This function is called when a status change is      */
/*                        triggered                                            */
/*                                                                           */
/* Input(s)           : i4Status                                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsTacHwUpdateStatus (INT4 i4Status)
{
    UNUSED_PARAM (i4Status);
    return FNP_SUCCESS;
}
#endif
