/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: fwlnp.c,v 1.3 2015/07/22 09:41:08 siva Exp $
 * 
 * Description: This file contains the function implementations  of FWL NP-API.
 * ****************************************************************************/

#ifndef _FWLNP_C_
#define _FWLNP_C_
#include "lr.h"
#include "npapi.h"
#include "firewall.h"
#include "fwlnp.h"

/*****************************************************************************
 *    Function Name      : FwlNpEnableDosAttack 
 *    Description        : This NPAPI call is to invoke the hardware function
 *                           calls which will identify the DOS attack packets and
 *                           drop those.
 *    Input(s)           : None 
 *    Output(s)          : None
 *    Returns            : None 
 *****************************************************************************/
VOID
FwlNpEnableDosAttack (VOID)
{
    return;
}

/*****************************************************************************
 *    Function Name      : FwlNpEnableIpHeaderValidation
 *    Description        : This NPAPI call is to create filter entries in the
 *                           hardware to drop the attack packets and the packets
 *                           with invalid L3 header.
 *    Input(s)           : None
 *    Output(s)          : None
 *    Returns            : None 
 *****************************************************************************/
VOID
FwlNpEnableIpHeaderValidation (VOID)
{
    return;
}

/*****************************************************************************
 *    Function Name      : FwlNpDisableIpHeaderValidation
 *    Description        : This function is to destroy all the filters created 
 *                         to perform DOS attack prevention and IP header
 *                         validation
 *    Input(s)           : None 
 *    Output(s)          : None
 *    Returns            : None 
 *****************************************************************************/
VOID
FwlNpDisableIpHeaderValidation (VOID)
{
    return;
}

/*****************************************************************************
 *    Function Name      : FwlNpDisableDosAttack
 *    Description        : This function is to disable all the DOS attack checks
 *                           in the hardware during de-Init.
 *    Input(s)           : None 
 *    Output(s)          : None
 *    Returns            : None 
 *****************************************************************************/
VOID
FwlNpDisableDosAttack (VOID)
{
    return;
}

/*****************************************************************************
 *    Function Name      : FwlNpGetHwFilterCapabilities
 *    Description        : This function outputs the hardware filters
 *                         supported for detecting the attack packets.
 *    Input(s)           : None
 *    Output(s)          : pFwlFilter - Hardware filters supported
 *    Returns            : None
 *****************************************************************************/

VOID
FwlNpGetHwFilterCapabilities (UINT1 *pFwlFilter)
{
    UNUSED_PARAM (pFwlFilter);
    return;
}

/*****************************************************************************
 *  *    Function Name      : FwlNpRateLimitEnable
 *   *    Description        : This function outputs the hardware filters
 *    *                         supported for detecting the attack packets.
 *     *    Input(s)           : None
 *      *    Output(s)          : pFwlFilter - Hardware filters supported
 *       *    Returns            : None
 *        *****************************************************************************/

INT4
FwlNpRateLimitEnable (tFwlRateLimit *pFwlRateLimit)
{
      UNUSED_PARAM(pFwlRateLimit);
        return SNMP_SUCCESS;
}
/*****************************************************************************
 *  *    Function Name      : FwlNpRateLimitDisable
 *   *    Description        : This function outputs the hardware filters
 *    *                         supported for detecting the attack packets.
 *     *    Input(s)           : None
 *      *    Output(s)          : pFwlFilter - Hardware filters supported
 *       *    Returns            : None
 *        *****************************************************************************/

INT4
FwlNpRateLimitDisable (tFwlRateLimit *pFwlRateLimit)
{
      UNUSED_PARAM(pFwlRateLimit);
        return SNMP_SUCCESS;
}

#endif
