
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rstmipbnp.c,v 1.6 2007/07/17 13:29:53 iss Exp $
 *
 * Description: All network processor function  given here
 *******************************************************************/

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "rstp.h"
#include "npapi.h"
#include "npcfa.h"
#include "rstmpbnp.h"

/***************************************************************************/
/* Function Name      : FsMiPbRstNpSetPortState                            */
/*                                                                         */
/* Description        : This function is used to program the PEP Port 
 *                     State.PEP is Part of the CVLAN component.This NPAPI 
 *                     is used only when the bridge is operation in 
 *                     provider bridge Mode.                               */
/*                                                                         */
/*                                                                         */
/* Input(s)           : u4ContextId - Virtual Switch ID                    */
/*                      u4IfIndex   - CEP Port Number.                     */
/*                      SVlanId     - Service VLAN id can take <1-4094>    */
/*                      pu1Status  - Status returned from Hardware.        */
/*                                  Values can be AST_PORT_STATE_DISCARDING*/
/*                                  or AST_PORT_STATE_LEARNING             */
/*                                  or AST_PORT_STATE_FORWARDING           */
/*                                                                         */
/* Output(s)          : None                                               */
/*                                                                         */
/* Global Variables                                                        */
/* Referred           : None                                               */
/*                                                                         */
/* Global Variables                                                        */
/* Modified           : None                                               */
/*                                                                         */
/* Return Value(s)    : FNP_SUCCESS - On successful set (or)               */
/*                      FNP_FAILURE - Error during setting                 */
/***************************************************************************/

INT1
FsMiPbRstpNpSetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVlanId,
                          UINT1 u1Status)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (u1Status);
    return FNP_SUCCESS;
}

/***************************************************************************/
/* Function Name      : FsMiPbRstNpGetPortState                            */
/*                                                                         */
/* Description        : This function is used to get the PEP Port State.   */
/*                      PEP is Part of the CVLAN component.This NPAPI is 
 *                      used only when the bridge is operation in provider 
 *                      bridge                                             */
/*                      Mode.                                              */
/*                                                                         */
/* Input(s)           : u4ContextId - Virtual Switch ID                    */
/*                      u4IfIndex   - CEP Port IfIndex.                    */
/*                      SVlanId     - Service VLAN id ranges <1-4094>      */
/*                                                                         */
/* Output(s)          : pu1Status  - Status returned from Hardware.        */
/*                                  Values can be AST_PORT_STATE_DISCARDING*/
/*                                  or AST_PORT_STATE_LEARNING             */
/*                                  or AST_PORT_STATE_FORWARDING           */
/*                                                                         */
/* Global Variables                                                        */
/* Referred           : None                                               */
/*                                                                         */
/* Global Variables                                                        */
/* Modified           : None                                               */
/*                                                                         */
/* Return Value(s)    : FNP_SUCCESS - On successful get (or)               */
/*                      FNP_FAILURE - Error during getting                 */
/***************************************************************************/
INT1
FsMiPbRstNpGetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVlanId,
                         UINT1 *pu1Status)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (pu1Status);
    return FNP_SUCCESS;
}
