/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mplsnpx.c,v 1.11 2015/02/16 12:21:37 siva Exp $
 *
 * Description: contains function implementations of MPLS MBSM FS NP-API
 ****************************************************************************/
#ifdef MPLS_WANTED
#ifndef _MPLSNPX_C_
#define _MPLSNPX_C_
/* Currently BCM is not supporting both L2 and L3 switching. 
 * So change to L2_MPLS_TESTING incase of L2VPN Testing*/

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "npapi.h"
#include "cfanp.h"
#include "npcfa.h"
#include "la.h"
#include "npla.h"
#include "fsvlan.h"
#include "mpls.h"
#include "mplsnp.h"

/*****************************************************************************/
/* Function Name      : FsMplsMbsmHwEnableMpls                               */
/*                                                                           */
/* Description        : This function enables MPLS feature in the            */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : pSlotInfo - Slot Information                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMplsMbsmHwEnableMpls (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsMbsmHwDisableMpls                              */
/*                                                                           */
/* Description        : This function disables MPLS  feature in the          */
/*                      hardware units.                                      */
/*                                                                           */
/* Input(s)           : pSlotInfo - Slot Information                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsMbsmHwDisableMpls (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsMbsmHwGetPwCtrlChnlCapabilities                */
/*                                                                           */
/* Description        : This function gets the pseudowire control channel    */
/*                      capabilities supported by hardware.                  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1PwCCTypeCapabs: bitmap that contains the values   */
/*                      bit 0: PW-ACH                                        */
/*                      bit 1: Router Alert Label                            */
/*                      bit 2: TTL Expiry (PW label with TTL=1)              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsMbsmHwGetPwCtrlChnlCapabilities (UINT1 *pu1PwCCTypeCapabs)
{
    UNUSED_PARAM (pu1PwCCTypeCapabs);
    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : FsMplsMbsmHwCreatePwVc                               */
/*                                                                           */
/* Description        : This function configures a VLL (virtual leased line) */
/*                      at the hardware.                                     */
/*                      Note: This function catters only for VPWS LERs.      */
/*                                                                           */
/* Input(s)           : u4VpnId       : VPN instance identifier              */
/*                                                                           */
/*                      pMplsHwVcInfo : Vll info for PW initiation as well as*/
/*                                      PW termination                       */
/*                                                                           */
/*                      u4Action      : ACTION to be performed on this       */
/*                                      on the labels supplied               */
/*                      pSlotInfo     : Slot Information                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMplsMbsmHwCreatePwVc (UINT4 u4VpnId, tMplsHwVcTnlInfo * pMplsHwVcInfo,
                        UINT4 u4Action, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (pMplsHwVcInfo);
    UNUSED_PARAM (u4Action);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsMbsmHwDeletePwVc                               */
/*                                                                           */
/* Description        : This function Deletes the VC(Virtual Circuit).       */
/*                      This is for VPWS so delete the VPN instance itself   */
/*                                                                           */
/* Input(s)           : u4VpnId       : VPN instance identifier              */
/*                                                                           */
/*                      pMplsHwDelVcInfo  : H/W Delete VC Info.              */
/*                                                                           */
/*                      u4Action      : ACTION to be performed on this       */
/*                                      on the labels supplied               */
/*                      pSlotInfo     : Slot Information                     */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsMbsmHwDeletePwVc (UINT4 u4VpnId, tMplsHwVcTnlInfo * pMplsHwDelVcInfo,
                        UINT4 u4Action, tMbsmSlotInfo * pSlotInfo)
{

    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (pMplsHwDelVcInfo);
    UNUSED_PARAM (u4Action);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsMbsmHwCreateILM                                */
/*                                                                           */
/* Description        : This function creates ILM database in the H/W.       */
/*                      This function addresses the LSR configurations,both  */
/*                      for L2/L3 VPN scenario.                              */
/*                                                                           */
/* Input(s)           : tMplsHwIlmInfo: VPN instance identifier              */
/*                                                                           */
/*                      MplsHwIlmInfo : ILM info                             */
/*                                                                           */
/*                      u4Action      : ACTION to be performed on this       */
/*                                      on the labels supplied.              */
/*                      pSlotInfo     : Slot Information                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMplsMbsmHwCreateILM (tMplsHwIlmInfo * pMplsHwIlmInfo, UINT4 u4Action,
                       tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pMplsHwIlmInfo);
    UNUSED_PARAM (u4Action);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsMbsmHwDeleteILM                                    */
/*                                                                           */
/* Description        : This function deletes ILM database in the H/W.       */
/*                                                                           */
/* Input(s)           : tMplsHwIlmInfo : ILM Information                     */
/*                                                                           */
/*                      u4Action      : ACTION to be performed.              */
/*                      pSlotInfo     : Slot Information                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMplsMbsmHwDeleteILM (tMplsHwIlmInfo * pMplsHwDelIlmParams, UINT4 u4Action,
                       tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pMplsHwDelIlmParams);
    UNUSED_PARAM (u4Action);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsMbsmHwCreateL3FTN                              */
/*                                                                           */
/* Description        : This function configures a L3 FEC to NHLFE           */
/*                      at the hardware.                                     */
/*                                                                           */
/* Input(s)           : u4VpnId       : VPN instance identifier              */
/*                                                                           */
/*                      tMplsHwL3FTNInfo:Info for L3 Mpls Tnl Initiatiion    */
/*                      pSlotInfo     : Slot Information                     */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMplsMbsmHwCreateL3FTN (UINT4 u4VpnId, tMplsHwL3FTNInfo * pMplsHwL3FTNInfo,
                         tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (pMplsHwL3FTNInfo);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsMbsmHwDeleteL3FTN                              */
/*                                                                           */
/* Description        : This function deletes a L3 FEC to NHLFE Entry        */
/*                      at the hardware.                                     */
/*                                                                           */
/* Input(s)           : u4L3EgrIntf                                          */
/*                      pSlotInfo     : Slot Information                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsMbsmHwDeleteL3FTN (UINT4 u4L3EgrIntf, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4L3EgrIntf);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsMbsmHwCreateVplsVpn                            */
/*                                                                           */
/* Description        : Instantiates a VPLS VPN at the H/W                   */
/* Input(s)           : u4VplsInstance - Forwarding Instance Id              */
/*                      u4VpnId        - Vpn Id                              */
/*                                                                           */
/*                      pMplsHwVplsVpnInfo - Vpls info                       */
/*                      pSlotInfo     : Slot Information                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsMbsmHwCreateVplsVpn (UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo
                           * pMplsHwVplsVpnInfo, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4VplsInstance);
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (pMplsHwVplsVpnInfo);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : FsMplsMbsmHwDeleteVplsVpn                                */
/*                                                                           */
/* Description        : Deletes a VPLS VPN at the H/W                        */
/* Input(s)           : u4VplsInstance - Forwarding Instance Id              */
/*                      u4VpnId        - Vpn Id                              */
/*                                                                           */
/*                      pMplsHwVplsVpnInfo - Vpls info                       */
/*                      pSlotInfo     : Slot Information                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsMbsmHwDeleteVplsVpn (UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo
                           * pMplsHwVplsVpnInfo, tMbsmSlotInfo * pSlotInfo)
{

    UNUSED_PARAM (u4VplsInstance);
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (pMplsHwVplsVpnInfo);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : FsMplsMbsmHwVplsAddPwVc                                  */
/*                                                                           */
/* Description        : Associates PWVC to a VPLS VPN at the H/W             */
/* Input(s)           : u4VplsInstance - Forwarding Instance Id              */
/*                      u4VpnId        - Vpn Id                              */
/*                      pMplsHwVplsVpnInfo - Vpls info                       */
/*                      u4Action           - Action to be done               */
/*                      pSlotInfo     : Slot Information                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsMbsmHwVplsAddPwVc (UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo
                         * pMplsHwVplsVcInfo, UINT4 u4Action,
                         tMbsmSlotInfo * pSlotInfo)
{

    UNUSED_PARAM (u4VplsInstance);
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (pMplsHwVplsVcInfo);
    UNUSED_PARAM (u4Action);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : FsMplsMbsmHwVplsDeletePwVc                               */
/*                                                                           */
/* Description        : Dis-associates PWVC from a VPLS VPN at the H/W       */
/* Input(s)           : u4VplsInstance - Forwarding Instance Id              */
/*                      u4VpnId        - Vpn Id                              */
/*                      pMplsHwVplsVpnInfo - Vpls info                       */
/*                      u4Action       - Action flag                         */
/*                      pSlotInfo     : Slot Information                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsMbsmHwVplsDeletePwVc (UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo
                            * pMplsHwVplsVcInfo, UINT4 u4Action,
                            tMbsmSlotInfo * pSlotInfo)
{

    UNUSED_PARAM (u4VplsInstance);
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (pMplsHwVplsVcInfo);
    UNUSED_PARAM (u4Action);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : FsMplsMbsmHwAddVpnAc                                     */
/*                                                                           */
/* Description        : Adds an Attachment circuit to the VPN instance.      */
/* Input(s)           : u4VplsInstance - Forwarding Instance Id              */
/*                      u4VpnId        - Vpn Id                              */
/*                      pMplsHwVplsVpnInfo - AC info                         */
/*                      u4Action       - Action flag                         */
/*                      pSlotInfo     : Slot Information                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsMbsmHwAddVpnAc (UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo
                      * pMplsHwVplsVcInfo, UINT4 u4Action,
                      tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4VplsInstance);
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (pMplsHwVplsVcInfo);
    UNUSED_PARAM (u4Action);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : FsMplsMbsmHwDeleteVpnAc                              */
/*                                                                           */
/* Description        : Deletes an Attachment circuit from an VPN instance.  */
/* Input(s)           : u4VplsInstance - Forwarding Instance Id              */
/*                      u4VpnId        - Vpn Id                              */
/*                      pMplsHwVplsVpnInfo - AC info                         */
/*                      u4Action       - Action flag                         */
/*                      pSlotInfo     : Slot Information                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsMbsmHwDeleteVpnAc (UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo
                         * pMplsHwVplsVcInfo, UINT4 u4Action,
                         tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4VplsInstance);
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (pMplsHwVplsVcInfo);
    UNUSED_PARAM (u4Action);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;

}
/******************************************************************************/
/*  Function Name             : NpMplsMbsmCreateMplsInterfaceWr               */
/*  Description               : calls routine npapi                           */
/*                              NpMplsMbsmCreateMplsInterface for Mbsm        */
/*  Input(s)                  : pMplsHwMplsIntInfo-contains all parameter     */
/*                              pSlotInfo - Slot Information                  */
/*  Output(s)                 : None                                          */
/*  Global Variables Referred : None                                          */
/*  Global variables Modified : None                                          */
/*  Exceptions                : None                                          */
/*  Use of Recursion          : None                                          */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE                       */
/******************************************************************************/
UINT4
NpMplsMbsmCreateMplsInterfaceWr (tMplsHwMplsIntInfo *  pMplsHwMplsIntInfo ,tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM(pMplsHwMplsIntInfo->u4L3Intf);
    NpMplsMbsmCreateMplsInterface ( pMplsHwMplsIntInfo->u4CfaIfIndex, pMplsHwMplsIntInfo->u2VlanId,
                                    pMplsHwMplsIntInfo->au1MacAddress, pSlotInfo);
    return FNP_SUCCESS;
}


/******************************************************************************/
/*  Function Name             : NpMplsMbsmCreateMplsInterface                 */
/*  Description               : Creates an L3 interface in the specified      */
/*                              unit by getting the VLAN Id of specified      */
/*                              CFA If Index. The L3 Interface will be created*/
/*                              only if the VLAN is already configured in the */
/*                              specified unit                                */
/*  Input(s)                  : u4CfaIfIndex - CFA Vlan If Index              */
/*                              VlanId - Vlan Identifier                      */
/*                              au1MacAddr - Mac address of this interface    */
/*                              pSlotInfo - Slot Information                  */
/*  Output(s)                 : None                                          */
/*  Global Variables Referred : None                                          */
/*  Global variables Modified : None                                          */
/*  Exceptions                : None                                          */
/*  Use of Recursion          : None                                          */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE                       */
/******************************************************************************/

UINT4
NpMplsMbsmCreateMplsInterface (UINT4 u4CfaIfIndex, UINT2 u2VlanId,
                               UINT1 *au1MacAddr, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4CfaIfIndex);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (au1MacAddr);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/* MPLS_P2MP_LSP_CHANGES - S */
/*****************************************************************************/
/* Function Name      : FsMplsMbsmHwP2mpAddILM                               */
/*                                                                           */
/* Description        : This function creates the ILM database in the H/W    */
/*                      for P2MP tunnels. This function addresses the        */
/*                                                                           */
/*                      head/branch configurations of P2MP destinations.     */
/* Input(s)           : u4P2mpId          - P2MP Identifier                  */
/*                      MplsHwP2mpIlmInfo - ILM info                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsMbsmHwP2mpAddILM (UINT4 u4P2mpId,
                        tMplsHwIlmInfo * pMplsHwP2mpIlmInfo,
                        tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4P2mpId);
    UNUSED_PARAM (pMplsHwP2mpIlmInfo);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsHwP2mpRemoveILM                                */
/*                                                                           */
/* Description        : This function deletes the ILM database in the H/W    */
/*                      for P2MP tunnels.                                    */
/*                                                                           */
/* Input(s)           : u4P2mpId          - P2MP Identifier                  */
/*                      MplsHwP2mpIlmInfo - ILM info                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsMbsmHwP2mpRemoveILM (UINT4 u4P2mpId,
                           tMplsHwIlmInfo * pMplsHwP2mpIlmInfo,
                           tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4P2mpId);
    UNUSED_PARAM (pMplsHwP2mpIlmInfo);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : FsMplsMbsmHwGetVpnAc                                 */
/*                                                                           */
/* Description        : Deletes an Attachment circuit from an VPN instance.  */
/* Input(s)           : pMplsMbsmHwVplsVpnInfo - AC info                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsMbsmHwGetVpnAc (tMplsHwVplsInfo * pMplsHwVplsVcInfo,
                        tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pMplsHwVplsVcInfo);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsMbsmHwGetPwVc                                  */
/*                                                                           */
/* Description        : This function gives the next pw entry                */
/* Input(s)           : MplsHwVllInfo   : Vll info                           */
/*                                                                           */
/* Output(s)          : MplsHwVllInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsMbsmHwGetPwVc (tMplsHwVcTnlInfo * pMplsHwVcInfo,
                      tMplsHwVcTnlInfo * pNextMplsHwVcInfo,
                      tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pMplsHwVcInfo);
    UNUSED_PARAM (pNextMplsHwVcInfo);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsMbsmHwGetILM                                   */
/*                                                                           */
/* Description        : This function gives the Next entry of the            */
/*                    : of incoming entry                                    */
/*                                                                           */
/* Input(s)           : pMplsHwIlmInfo: ILM info                             */
/*                                                                           */
/* Output(s)          : pNextMplsHwIlmInfo: ILM info                         */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMplsMbsmHwGetILM (tMplsHwIlmInfo * pMplsHwIlmInfo,
                     tMplsHwIlmInfo * pNextMplsHwIlmInfo,
                     tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pMplsHwIlmInfo);
    UNUSED_PARAM (pNextMplsHwIlmInfo);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsMbsmHwGetL3FTN                                 */
/*                                                                           */
/* Description        : This function is to get L3FTN                        */
/*                                                                           */
/* Input(s)           : pMplsHwIlmInfo: ILM info                             */
/*                                                                           */
/* Output(s)          : pNextMplsHwIlmInfo: ILM info                         */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsMbsmHwGetL3FTN (UINT4 u4VpnId, UINT4 u4IfIndex,
                        tMplsHwL3FTNInfo * pMplsHwL3FTNInfo,
                        tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pMplsHwL3FTNInfo);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/* MPLS_P2MP_LSP_CHANGES - E */
#ifdef MPLS_L3VPN_WANTED
INT4
FsMplsMbsmHwL3vpnIngressMap (tMplsHwL3vpnIgressInfo * pMplsHwL3vpnIgressInfo,
                             tMbsmSlotInfo * pSlotInfo)
{

    UNUSED_PARAM (pMplsHwL3vpnIgressInfo);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

INT4
FsMplsMbsmHwL3vpnEgressMap (tMplsHwL3vpnEgressInfo * pMplsHwL3vpnEgressInfo,
                            tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pMplsHwL3vpnEgressInfo);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}
#endif
#endif
#endif /* MPLS_WANTED */
