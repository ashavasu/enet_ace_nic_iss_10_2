/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipmcnp.c,v 1.16 2014/09/09 10:05:25 siva Exp $
 *
 * Description: All prototypes for Network Processor API functions
 *              done here
 *******************************************************************/
#ifdef FS_NPAPI
#include "lr.h"

#ifdef MBSM_WANTED
#include "mbsm.h"
#endif

#include "npapi.h"
#include "npipmc.h"

UINT1               gu1IpmcInitCnt = 0;

/*****************************************************************************
 *  Function Name   : FsNpIpv4McInit
 *  Description     : This function initialises the IP Multicasting in the 
 *                    driver. This function enables the IPMC Module, enforces
 *                    source port check, and defines the search rule for IPMC
 *                    entry
 *  Input(s)        : None
 *  Output(s)       : None.
 *  Returns         : FNP_SUCCESS/FNP_FAILURE
 ***************************************************************/

PUBLIC UINT4
FsNpIpv4McInit ()
{
    gu1IpmcInitCnt++;
    if (gu1IpmcInitCnt > 1)
    {
        /* Either PIM/DVMRP did the initialisation */
        return FNP_SUCCESS;
    }
    /* Enable IPMC in the hardware */
    return FNP_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : FsNpIpv4McDeInit
 *  Description     : This function deinitialises the IP Multicasting in the 
 *                    driver. This function disables the IPMC Module
 *  Input(s)        : None
 *  Output(s)       : None.
 *  Returns         : FNP_SUCCESS/FNP_FAILURE
 ***************************************************************/

PUBLIC UINT4
FsNpIpv4McDeInit ()
{
    /* This de-initialisation routine is called by both PIM and DVMRP.
     * But either PIM or DVMRP only should do the de-initialization. 
     */
    gu1IpmcInitCnt--;
    if (gu1IpmcInitCnt != 0)
    {
        /* Either PIM/DVMRP did the de-initialisation */
        return FNP_SUCCESS;
    }
    /* Disable IPMC in the hardware */
    return FNP_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : FsNpIpv4VlanMcInit
 *  Description     : This function initialises the IP Multicasting in the
 *                    Given Vlan  This function enables the IPMC Module, enforces
 *                    source port check, and defines the search rule for IPMC
 *                    entry
 *  Input(s)        : u4VlanId ->Vlan ID
 *  Output(s)       : None.
 *  Returns         : FNP_SUCCESS/FNP_FAILURE
 ***************************************************************/

PUBLIC UINT4
FsNpIpv4VlanMcInit (UINT4 u4VlanId)
{
    UNUSED_PARAM (u4VlanId);
    return FNP_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : FsNpIpv4VlanMcDeInit
 *  Description     : This function Deinitialises the IP Multicasting in the 
 *                    Given Vlan  This function disables the IPMC Module.
 *  Input(s)        : None
 *  Output(s)       : None.
 *  Returns         : FNP_SUCCESS/FNP_FAILURE
 ***************************************************************/

PUBLIC UINT4
FsNpIpv4VlanMcDeInit (UINT4 u4Index)
{
    UNUSED_PARAM (u4Index);
    return FNP_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : FsNpIpv4RportMcInit
 *  Description     : This function initialises the IP Multicasting in the
 *                    Given Physical port or Port channel index.
 *                    This function enables the IPMC Module, enforces
 *                    source port check, and defines the search rule for IPMC
 *                    entry
 *  Input(s)        :tRPortInfo
 *  Output(s)       : None.
 *  Returns         : FNP_SUCCESS/FNP_FAILURE
 ***************************************************************/

PUBLIC UINT4
FsNpIpv4RportMcInit (tRPortInfo PortInfo)
{
    UNUSED_PARAM (PortInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : FsNpIpv4RportMcDeInit
 *  Description     : This function Deinitialises the IP Multicasting in the
 *                    Given Physical port or Port Channel.
 *                    This function disables the IPMC Module.
 *  Input(s)        : tRPortInfo
 *  Output(s)       : None.
 *  Returns         : FNP_SUCCESS/FNP_FAILURE
 ***************************************************************/

PUBLIC UINT4
FsNpIpv4RportMcDeInit (tRPortInfo PortInfo)
{
    UNUSED_PARAM (PortInfo);
    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FsNpIpv4McAddRouteEntry                  */
/*  Description     : This function adds Multicast Route Entry */
/*                    in IPMC Table. It also add a downstream  */
/*                    interface to the existing route entry.   */
/*  Input(s)        : u4VrId        :  Virtual Router ID       */
/*                                     ZERO when not supported.*/
/*                    u4GrpAddr     :  Group Address           */
/*                    u4GrpPrefix   :  Group Prefix            */
/*                    u4SrcIpAddr   :  Source Address          */
/*                    u4SrcIpPrefix :  Source Prefix           */
/*                    rtEntry       :  Have the route entry    */
/*                                     parameters like command */
/*                                     RPF interface and flags.*/
/*                    u2NoOfDownStreamIf:  No of associated    */
/*                                     downstream interfaces.  */
/*                    NOTE: Currently only one interface is    */
/*                          added at a time                    */
/*                    pDownStreamIf :  downstream interface    */
/*                                     to be added.            */
/*  Output(s)       : None.                                    */
/*  Returns         : FNP_SUCCESS/FNP_FAILURE                  */
/***************************************************************/

PUBLIC UINT4
FsNpIpv4McAddRouteEntry (UINT4 u4VrId,
                         UINT4 u4GrpAddr,
                         UINT4 u4GrpPrefix,
                         UINT4 u4SrcIpAddr,
                         UINT4 u4SrcIpPrefix,
                         UINT1 u1CallerId,
                         tMcRtEntry rtEntry,
                         UINT2 u2NoOfDownStreamIf,
                         tMcDownStreamIf * pDownStreamIf)
{
    UINT4               u4Iif = 0;
    tMacAddr            au1MacAddr;
    if (pDownStreamIf != NULL)
    {
        pDownStreamIf->u2TtlThreshold = 1;
    }
    if (rtEntry.pUpStreamIf != NULL)
    {
        rtEntry.pUpStreamIf->u2TtlThreshold = 1;
    }

    u4Iif = rtEntry.u2RpfIf;

    if ((u4GrpAddr == 0) || (u4SrcIpAddr == 0) || (u4Iif == 0))
    {
        return FNP_FAILURE;
    }

    /* Program hardware for this multicast group */

    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4GrpPrefix);
    UNUSED_PARAM (u4SrcIpPrefix);
    UNUSED_PARAM (u2NoOfDownStreamIf);
    UNUSED_PARAM (au1MacAddr);
    UNUSED_PARAM (u1CallerId);
    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FsNpIpv4McDelRouteEntry                  */
/*  Description     : Deletes multicast route entry if         */
/*                    (u2NoOfDownStreamIf = 0) or else deletes */
/*                    the associated outgoing interface from   */
/*                    the corresponding route entry.           */
/*  Input(s)        : u4VrId        :  Virtual Router ID       */
/*                                     ZERO when not supported.*/
/*                    u4GrpAddr     :  Group Address           */
/*                    u4GrpPrefix   :  Group Prefix            */
/*                    u4SrcIpAddr   :  Source Address          */
/*                    u4SrcIpPrefix :  Source Prefix           */
/*                    u2NoOfDownStreamIf:  No of associated    */
/*                                     downstream interfaces.  */
/*                    pDownStreamIf: Pointer to list of        */
/*                                     downstream interfaces   */
/*                                     to be deleted.          */
/*  Output(s)       : None.                                    */
/*  Returns         : FNP_SUCCESS/FNP_FAILURE                  */
/***************************************************************/

PUBLIC UINT4
FsNpIpv4McDelRouteEntry (UINT4 u4VrId,
                         UINT4 u4GrpAddr,
                         UINT4 u4GrpPrefix,
                         UINT4 u4SrcIpAddr,
                         UINT4 u4SrcIpPrefix,
                         tMcRtEntry rtEntry,
                         UINT2 u2NoOfDownStreamIf,
                         tMcDownStreamIf * pDownStreamIf)
{
    UINT4               u4Iif;
    tMacAddr            au1MacAddr;

    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4GrpPrefix);
    UNUSED_PARAM (u4SrcIpPrefix);
    UNUSED_PARAM (rtEntry);
    UNUSED_PARAM (u2NoOfDownStreamIf);
    UNUSED_PARAM (pDownStreamIf);

    MEMSET (au1MacAddr, 0, sizeof (au1MacAddr));
    u4Iif = rtEntry.u2RpfIf;

    if ((u4GrpAddr == 0) || (u4SrcIpAddr == 0) || (u4Iif == 0))
    {
        return FNP_FAILURE;
    }

    /* Remove this multicast group from the hardware */

    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FsNpIpv4McClearAllRoutes                 */
/*  Description     : Clears the multicast route entries       */
/*                    for the specified Vr ID.                 */
/*  Input(s)        : u4VrId        :  Virtual Router ID       */
/*                                     ZERO when not supported.*/
/*  Output(s)       : None.                                    */
/*  Returns         : None.                                    */
/***************************************************************/

PUBLIC VOID
FsNpIpv4McClearAllRoutes (UINT4 u4VrId)
{
    UNUSED_PARAM (u4VrId);
    return;
}

/***************************************************************/
/*  Function Name   : FsNpIpv4McAddCpuPort                     */
/*  Description     : Adds the Cpu Port to the Multicast Group */
/*              Port list to enable reception of unknown       */
/*              multicast data packets                         */
/*                    for the specified Vr ID.                 */
/*  Input(s)        : u1GenRtrId        :  Router ID           */
/*                    u4GrpAddr     :  Group Address           */
/*                    u4SrcIpAddr   :  Source IP address       */
/*              rtEntry       :  Have the route entry          */
/*                                     parameters like  RPF    */
/*  Output(s)       : None.                                    */
/*  Returns         : None.                                    */
/***************************************************************/

UINT4
FsNpIpv4McAddCpuPort (UINT2 u2GenRtrId, UINT4 u4GrpAddr,
                      UINT4 u4SrcAddr, tMcRtEntry rtEntry)
{
    UNUSED_PARAM (u2GenRtrId);
    UNUSED_PARAM (u4GrpAddr);
    UNUSED_PARAM (u4SrcAddr);
    UNUSED_PARAM (rtEntry);

    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FsNpIpv4McDelCpuPort                     */
/*  Description     : Removes the Cpu Port from the Multicast  */
/*              Group Port list to disable reception of        */
/*              unknown multicast data packets on the CPU      */
/*                    Port for the specified Vr ID.            */
/*  Input(s)        : u1GenRtrId    :  Router ID               */
/*                    u4GrpAddr     :  Group Address           */
/*                    u4SrcIpAddr   :  Source IP address       */
/*              rtEntry       :  Have the route entry          */
/*                                     parameters like  RPF    */
/*  Output(s)       : None.                                    */
/*  Returns         : None.                                    */
/***************************************************************/

UINT4
FsNpIpv4McDelCpuPort (UINT2 u2GenRtrId, UINT4 u4GrpAddr,
                      UINT4 u4SrcAddr, tMcRtEntry rtEntry)
{
    UNUSED_PARAM (u2GenRtrId);
    UNUSED_PARAM (u4GrpAddr);
    UNUSED_PARAM (u4SrcAddr);
    UNUSED_PARAM (rtEntry);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : FsNpIpv4McGetHitStatus                                 */
/*                                                                           */
/*  Description     : This function gets the hit status for a particular     */
/*                    multicast group from the hardware.                     */
/*                                                                           */
/*  Input(s)        : u4GrpAddr     :  Multicast group address               */
/*                    u4SrcIpAddr   :  Source IP                             */
/*                    u4Iif         :  Incoming interface                    */
/*                    u2VlanId      :  Vlan Identifier                       */
/*                                                                           */
/*  Output(s)       : pu4HitStatus - Hit bit status                          */
/*                                                                           */
/*  Returns         : None                                                   */
/*****************************************************************************/

VOID
FsNpIpv4McGetHitStatus (UINT4 u4SrcIpAddr, UINT4 u4GrpAddr,
                        UINT4 u4Iif, UINT2 u2VlanId, UINT4 *pu4HitStatus)
{
    UNUSED_PARAM (u4SrcIpAddr);
    UNUSED_PARAM (u4GrpAddr);
    UNUSED_PARAM (u4Iif);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (pu4HitStatus);
}

/***************************************************************/
/*  Function Name   : FsNpIpv4McUpdateOifVlanEntry             */
/*  Description     : This function updates the L3 bitmap      */
/*                   a route entry in the IPMC table           */
/*                                                             */
/*                                                             */
/*  Input(s)        : u4VrId        :  Virtual Router ID       */
/*                                     ZERO when not supported.*/
/*                    u4GrpAddr     :  Group Address           */
/*                    u4SrcIpAddr   :  Source IP address       */
/*                    rtEntry       :  Have the route entry    */
/*                                     parameters like  RPF    */
/*                    pDownStreamIf :  downstream interface    */
/*                                     to be updated           */
/*  Output(s)       : None.                                    */
/*  Returns         : None                                     */
/***************************************************************/

VOID
FsNpIpv4McUpdateOifVlanEntry (UINT4 u4VrId,
                              UINT4 u4GrpAddr,
                              UINT4 u4SrcIpAddr,
                              tMcRtEntry rtEntry, tMcDownStreamIf downStreamIf)
{

    UINT4               u4Iif;
    tMacAddr            au1MacAddr;

    UNUSED_PARAM (u4VrId);

    MEMSET (au1MacAddr, 0, sizeof (au1MacAddr));
    u4Iif = rtEntry.u2RpfIf;
    downStreamIf.u2TtlThreshold = 1;

    if ((u4GrpAddr == 0) || (u4SrcIpAddr == 0) || (u4Iif == 0))
    {
        return;
    }

    /* Update hardware IPMC table L3 bitmap for the VLAN associated with this outgoing 
     * interface */
    return;

}

/***************************************************************/
/*  Function Name   : FsNpIpv4McUpdateIifVlanEntry             */
/*  Description     : This function updates the L3 bitmap      */
/*                   a route entry in the IPMC table           */
/*                                                             */
/*                                                             */
/*  Input(s)        : u4VrId        :  Virtual Router ID       */
/*                                     ZERO when not supported.*/
/*                    u4GrpAddr     :  Group Address           */
/*                    u4RtAddr      :  Source IP address       */
/*                    rtEntry       :  Have the route entry    */
/*                                     parameters like  RPF    */
/*  Output(s)       : None.                                    */
/*  Returns         : None                                     */
/***************************************************************/

VOID
FsNpIpv4McUpdateIifVlanEntry (UINT4 u4VrId,
                              UINT4 u4GrpAddr,
                              UINT4 u4SrcIpAddr, tMcRtEntry rtEntry)
{
    UINT4               u4Iif;
    tMacAddr            au1MacAddr;

    UNUSED_PARAM (u4SrcIpAddr);
    UNUSED_PARAM (u4GrpAddr);
    UNUSED_PARAM (u4VrId);

    MEMSET (au1MacAddr, 0, sizeof (au1MacAddr));
    u4Iif = rtEntry.u2RpfIf;

    /* Update hardware IPMC L2 bitmap for the VLAN associated with this incoming interface
     */
    return;
}

/***************************************************************/
/*  Function Name   : FsNpIpv4GetMRouteStats                   */
/*  Description     : This function get the muticast route     */
/*                    statistics from NP depending upon the    */
/*                    stats Type.                              */
/*  Input(s)        : u4VrId- Virtual Router ID                */
/*                    ZERO when not supported.                 */
/*                    u4GrpAddr - Group Address                */
/*                    u4SrcAddr - Source Address               */
/*                    i4StatType -Statistic type               */
/*  Output(s)       : pu4RetValue                              */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : FNP_SUCCESS/FNP_FAILURE                  */
/***************************************************************/
INT4
FsNpIpv4GetMRouteStats (UINT4 u4VrId, UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                        INT4 i4StatType, UINT4 *pu4RetValue)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4GrpAddr);
    UNUSED_PARAM (u4SrcAddr);
    UNUSED_PARAM (i4StatType);
    *pu4RetValue = 0;

    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FsNpIpv4GetMRouteHCStats                 */
/*  Description     : This function get the muticast route     */
/*                    64-bitstatistics from NP depending upon  */
/*                    stats Type.                              */
/*  Input(s)        : u4VrId- Virtual Router ID                */
/*                    ZERO when not supported.                 */
/*                    u4GrpAddr - Group Address                */
/*                    u4SrcAddr - Source Address               */
/*                    i4StatType -Statistic type               */
/*  Output(s)       : pu8RetValue                              */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : FNP_SUCCESS/FNP_FAILURE                  */
/***************************************************************/
INT4
FsNpIpv4GetMRouteHCStats (UINT4 u4VrId, UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                          INT4 i4StatType, tSNMP_COUNTER64_TYPE * pu8RetValue)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4GrpAddr);
    UNUSED_PARAM (u4SrcAddr);
    UNUSED_PARAM (i4StatType);
    pu8RetValue->lsn = 0;
    pu8RetValue->msn = 0;

    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FsNpIpv4GetMNextHopStats                 */
/*  Description     : The number of packets which have been    */
/*                    forwarded on this out going interface    */
/*  Input(s)        : u4VrId- Virtual Router ID                */
/*                    ZERO when not supported.                 */
/*                    u4GrpAddr - Group Address                */
/*                    u4SrcAddr - Source Address               */
/*                    i4OutIfIndex - Out going interface index */
/*                    i4StatType - Statistic type              */
/*  Output(s)       : pu4RetValue                              */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : FNP_SUCCESS/FNP_FAILURE                  */
/***************************************************************/
INT4
FsNpIpv4GetMNextHopStats (UINT4 u4VrId, UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                          INT4 i4OutIfIndex, INT4 i4StatType,
                          UINT4 *pu4RetValue)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4GrpAddr);
    UNUSED_PARAM (u4SrcAddr);
    UNUSED_PARAM (i4OutIfIndex);
    UNUSED_PARAM (i4StatType);
    *pu4RetValue = 0;

    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FsNpIpv4GetMIfaceStats                   */
/*  Description     : The number of octets of multicast packets*/
/*                    that have arrived or forwarded  on the   */
/*                    interface depending upon the stats type  */
/*  Input(s)        : u4VrId- Virtual Router ID                */
/*                    ZERO when not supported.                 */
/*                    i4IfIndex - Interface Index              */
/*                    i4StatType - Statistics type             */
/*  Output(s)       : pu4RetValue                              */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : FNP_SUCCESS/FNP_FAILURE                  */
/***************************************************************/
INT4
FsNpIpv4GetMIfaceStats (UINT4 u4VrId, INT4 i4IfIndex, INT4 i4StatType,
                        UINT4 *pu4RetValue)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4StatType);
    *pu4RetValue = 0;

    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FsNpIpv4GetMIfaceHCStats                 */
/*  Description     : The number of 64-bit octets of multicast */
/*                    packets that have arrived or forwarded   */
/*                    on the interface depending upon the stats*/
/*                    type                                     */
/*  Input(s)        : u4VrId- Virtual Router ID                */
/*                    ZERO when not supported.                 */
/*                    i4IfIndex - Interface Index              */
/*                    i4StatType - Statistics type             */
/*  Output(s)       : pu8RetValue                              */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : FNP_SUCCESS/FNP_FAILURE                  */
/***************************************************************/
INT4
FsNpIpv4GetMIfaceHCStats (UINT4 u4VrId, INT4 i4IfIndex, INT4 i4StatType,
                          tSNMP_COUNTER64_TYPE * pu8RetValue)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4StatType);
    pu8RetValue->lsn = 0;
    pu8RetValue->msn = 0;

    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FsNpIpv4SetMIfaceTtlTreshold             */
/*  Description     : Sets the The datagram TTL threshold for  */
/*                    the interface                            */
/*  Input(s)        : u4VrId- Virtual Router ID                */
/*                    ZERO when not supported.                 */
/*                    i4IfIndex - Interface Index              */
/*                    i4TtlTreshold - Ttl-threshold            */
/*  Output(s)       :                                          */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : FNP_SUCCESS/FNP_FAILURE                  */
/***************************************************************/
INT4
FsNpIpv4SetMIfaceTtlTreshold (UINT4 u4VrId, INT4 i4IfIndex, INT4 i4TtlTreshold)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TtlTreshold);
    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FsNpIpv4SetMIfaceRateLimit               */
/*  Description     : Sets the The datagram Rate limit for     */
/*                    the interface                            */
/*  Input(s)        : u4VrId- Virtual Router ID                */
/*                    ZERO when not supported.                 */
/*                    i4IfIndex - Interface Index              */
/*  Output(s)       : i4RateLimit - rate limit value           */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : FNP_SUCCESS/FNP_FAILURE                  */
/***************************************************************/
INT4
FsNpIpv4SetMIfaceRateLimit (UINT4 u4VrId, INT4 i4IfIndex, INT4 i4RateLimit)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4RateLimit);
    return FNP_FAILURE;
}

/***************************************************************/
/*  Function Name   : FsNpIpvXHwGetMcastEntry                  */
/*  Description     : Gets the multicast route entry from NP.  */
/*                                                             */
/*  Input(s)        :  pNpL3McastEntry :VrId, GrpAddr, SrcAddr */
/*                     and Afi are given as input to NP.       */
/*                                                             */
/*  Output(s)       :  pNpL3McastEntry: RpfIf, OifCnt.         */
/*                     OifList(containing CfaIndex of OIFs) ,  */
/*                     CpuPortExists( indicating whether       */
/*                     CPUport  is added or not).              */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : FNP_SUCCESS/FNP_FAILURE                  */
/***************************************************************/

INT4
FsNpIpvXHwGetMcastEntry (tNpL3McastEntry * pNpL3McastEntry)
{
    UNUSED_PARAM (pNpL3McastEntry);
    return FNP_SUCCESS;
}

#ifdef PIM_WANTED

/*****************************************************************************/
/* Function Name      : FsPimNpInitHw                                        */
/*                                                                           */
/* Description        : This function enables PIM & enables IPMC in the      */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

PUBLIC UINT4
FsPimNpInitHw (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsPimNpDeInitHw                                      */
/*                                                                           */
/* Description        : This function disables PIM & disables IPMC in the    */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

PUBLIC UINT4
FsPimNpDeInitHw (VOID)
{
    return FNP_SUCCESS;
}

#endif /* PIM_WANTED */

#ifdef DVMRP_WANTED

/*****************************************************************************/
/* Function Name      : FsDvmrNpInitHw                                       */
/*                                                                           */
/* Description        : This function enables DVMRP & enables IPMC in the    */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

PUBLIC UINT4
FsDvmrpNpInitHw (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsDvmrpNpDeInitHw                                    */
/*                                                                           */
/* Description        : This function disables DVMRP & disables IPMC in the  */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

PUBLIC UINT4
FsDvmrpNpDeInitHw (VOID)
{
    return FNP_SUCCESS;
}

#endif /* DVMRP_WANTED */
#ifdef IGMPPRXY_WANTED
/*****************************************************************************/
/* Function Name      : FsNpIgmpProxyInit                                    */
/*                                                                           */
/* Description        : This function enables enables IPMC in the            */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

PUBLIC UINT4
FsNpIgmpProxyInit (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsNpIgmpProxyDeInit                                  */
/*                                                                           */
/* Description        : This function disables disables IPMC in the          */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

PUBLIC UINT4
FsNpIgmpProxyDeInit (VOID)
{
    return FNP_SUCCESS;
}
#endif /* IGMPPRXY_WANTED */
#ifdef PIM_WANTED
/*****************************************************************************/
/* Function Name      : FsNpIpv4McClearHitBit                                */
/*                                                                           */
/* Description        : This function Clears the Hit Bit of the multicast    */
/*                      route   .                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

UINT4
FsNpIpv4McClearHitBit (UINT2 u2VlanId, UINT4 u4GrpAddr, UINT4 u4SrcAddr)
{
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (u4GrpAddr);
    UNUSED_PARAM (u4SrcAddr);
    return FNP_SUCCESS;
}
#endif

/****************************************************************/
/*  Function Name   : FsNpIpv4McRpfDFInfo                  */
/*  Description     : This function adds Multicast RPF Info */
/*                    in IPMC Table.  */
/*  Input(s)        : */
/*                  tMcRpfDFInfo *pMcRpfDFInfo     */
 /**/
/****************************************************************/
    PUBLIC UINT4
FsNpIpv4McRpfDFInfo (tMcRpfDFInfo * pMcRpfDFInfo)
{
    UNUSED_PARAM (pMcRpfDFInfo);
    return FNP_SUCCESS;
}

#endif
