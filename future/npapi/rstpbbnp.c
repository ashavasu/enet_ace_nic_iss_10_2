/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rstpbbnp.c,v 1.1 2008/11/12 10:08:42 premap-iss Exp $
 *
 * Description: All network processor function  given here
 *******************************************************************/

#include "lr.h"
#include "npapi.h"
#include "rstmpbbnp.h"
/*****************************************************************************
 * FUNCTION:
 * FsMiPbbRstHwServiceInstancePtToPtStatus 
 *
 * DESCRIPTION:
 * This function sets the OperPointToPoint status of the VIP index or ISID
 * in the Hardware.
 *
 * INPUTS:
 * u4ContextId - Context Identifier
 * u4VipIndex - VIP Identifier
 * u4Isid - Backbone Service Instance Identifier
 * u1PointToPointStatus - Oper Point to Point Status of ISID
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * FNP_SUCCESS/FNP_FAILURE
 *
 *****************************************************************************/
INT4
FsMiPbbRstHwServiceInstancePtToPtStatus (UINT4 u4ContextId,
                                         UINT4 u4VipIndex,
                                         UINT4 u4Isid,
                                         UINT1 u1PointToPointStatus)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4VipIndex);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (u1PointToPointStatus);
    return FNP_SUCCESS;
}
