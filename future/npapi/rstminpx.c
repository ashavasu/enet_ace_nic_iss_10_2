/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rstminpx.c,v 1.4 2007/07/17 13:29:53 iss Exp $
 *
 * Description: All network processor functions for RSTP-NP
 *
 *******************************************************************/
#ifndef _RSTMINPX_C_
#define _RSTMINPX_C_

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "rstp.h"
#include "npapi.h"
#include "nprstpmi.h"
#include "npcfa.h"

/************************************************************************/
/* Function Name      : FsMiRstpMbsmNpSetPortState                      */
/*                                                                      */
/* Description        : Sets the RSTP Port State in the Hardware.       */
/*                      When RSTP is working in RSTP mode               */
/*                      or RSTP in STP compatible                       */
/*                      mode, porting should be done for this API.      */
/*                                                                      */
/*                                                                      */
/* Input(s)           : u4ContextId - Context Id
 *                      u4IfIndex - Port Number.                        */
/*                      u1Status  - Status to be set.                   */
/*                               Values can be AST_PORT_STATE_DISCARDING*/
/*                                  or AST_PORT_STATE_LEARNING          */
/*                                  or AST_PORT_STATE_FORWARDING   
 *                       pSlotInfo - pointer to the Slot information 
 *                                   structure                          */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Global Variables                                                     */
/* Referred           : None                                            */
/*                                                                      */
/* Global Variables                                                     */
/* Modified           : None                                            */
/*                                                                      */
/* Return Value(s)    : FNP_SUCCESS - On successful set (or)            */
/*                      FNP_FAILURE - Error during setting              */
/************************************************************************/
INT1
FsMiRstpMbsmNpSetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1Status,
                            tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/************************************************************************
 * Function Name                  :FsMiRstpMbsmNpInitHw                 *
 *                                                                      *
 * DESCRIPTION                    :This function performs any necessary * 
 *                                 RSTP related initialisation in the   *
 *                                 Hardware                             *
 *                                                                      *
 * INPUTS                         : u4ContextId - Context Id.
 *                                   pSlotInfo - Slot Information       *
 *                                                                      * 
 * OUTPUTS                        : None                                *
 *                                                                      *
 * RETURNS                        : FNP_SUCCESS - On Success            *
 *                                  FNP_FAILURE - On Failure            *
 *                                                                      *
 ************************************************************************/

INT1
FsMiRstpMbsmNpInitHw (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pSlotInfo);
    return (FNP_SUCCESS);
}
#endif
