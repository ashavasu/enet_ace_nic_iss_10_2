#ifndef __NPIPMC_H
#define __NP_IPMC_H

#include "ipmcnp.h"

#ifdef MBSM_WANTED
PUBLIC UINT4  FsNpIpv4MbsmMcInit PROTO ((tMbsmSlotInfo * pSlotInfo));
#endif

#endif /* __NPIPMC_H */
