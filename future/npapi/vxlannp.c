/*****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: vxlannp.c,v 1.1 2014/11/17 08:08:52 siva Exp $
 *
 * Description: This file contains the VXLAN NPAPIs that will be used for
 *              programming the hardware chipsets.
 *****************************************************************************/

#ifndef _VXLANNP_C_
#define _VXLANNP_C_

#include "vxlannp.h"

/***************************************************************************
 * Function Name             : FsNpHwVxlanInfo
 *
 * Description               : This function is invoked to configure/clear
 *                             VXLAN information in hardware
 *
 * Input                     : u4InfoTye - type of information needs to be
 *                             get or set in from hardware.
 *                             pVxlanHwInfo - pointer to hardware information
 *                             structure
 *                             u1Flag - Flag to configure or clear the VXLAN
 *                              information in hardware
 *
 * Output                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * Returns                   : FNP_SUCCESS/FNP_FAILURE
 *
 **************************************************************************/
INT4
FsNpHwVxlanInfo (tVxlanHwInfo * pVxlanHwInfo,
                       UINT1          u1Flag)
{
    switch (pVxlanHwInfo->u4InfoType)
    {
        case VXLAN_HW_UDP_PORT:
            /* This will set the VXLAN UDP port number */
            if(u1Flag == VXLAN_HW_CONFIGURE)
            {
                /* Configure the VXLAN UDP port number */
            }
            else if(VXLAN_HW_CLEAR)
            {
                /* Clear the VXLAN UDP port number */
            }
            return (FNP_SUCCESS);

        case VXLAN_HW_NVE_DATABASE:
            /* This database contains the following information
             * 1. VNI Number
             * 2. Destionation VM mac
             * 3. Source VTEP address type (IPV4/IPV6)
             * 4. Source VTEP address
             * 5. Remote VTEP address type IPV4/IPV6)
             * 6. Remote VTEP address
             * */
            if(u1Flag == VXLAN_HW_CONFIGURE)
            {
                /* Add an entry in VXLAN NVE database */
            }
            else if(VXLAN_HW_CLEAR)
            {
                /* Remove an entry in VXLAN NVE database */
            }
            return (FNP_SUCCESS);

        case VXLAN_HW_MCAST_DATABASE:
            /* This database contains the following information
             *  1. VNI Number
             *  2. Source VTEP address type (IPV4/IPV6)
             *  3. Source VTEP address
             *  4. Multicast group address type IPV4/IPV6)
             *  5. Multicast group address
             *  */
            if(u1Flag == VXLAN_HW_CONFIGURE)
            {
                /* Add an entry in VXLAN Mcast database */
            }
            else if(VXLAN_HW_CLEAR)
            {
                /* Remove an entry in VXLAN Mcast database */
            }
            return (FNP_SUCCESS);

        case VXLAN_HW_VNI_VLAN_MAPPING:
            /* This database contains the following information
             * 1. Vlan id
             * 2. VNI Number
             * */
            if(u1Flag == VXLAN_HW_CONFIGURE)
            {
                /* Add an entry in VXLAN VNI-VLAN database */
            }
            else if(VXLAN_HW_CLEAR)
            {
                /* Remove an entry in VXLAN VNI-VLAN database */
            }
            return (FNP_SUCCESS);

        default:
            return FNP_FAILURE;

    }
}
#endif
