/*****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: rmonv2np.c,v 1.5 2015/09/13 10:37:04 siva Exp $
 *
 * Description: This file contains the function implementations  of
 *              RMON NP-API.
 ****************************************************************************/

#ifndef _RMONV2NP_C_
#define _RMONV2NP_C_

#include "lr.h"
#include "npapi.h"
#include "cfa.h"
#include "npcfa.h"
#include "rmon2.h"
#include "nprmonv2.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : FsRMONv2NPInit                                             */
/*                                                                           */
/* Description  : This function initializes all the memory requirements,     */
/*                  RBtree, FP Group creation for RMONv2                     */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : FNP_SUCCESS / FNP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT4
FsRMONv2NPInit (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FsRMONv2NPDeInit                                           */
/*                                                                           */
/* Description  : This function frees all the memory,                        */
/*                 RBtrees, and FP Groups used RMONv2                        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : FNP_SUCCESS / FNP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

VOID
FsRMONv2NPDeInit (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FsRMONv2HwSetStatus                                        */
/*                                                                           */
/* Description  : Updates the RMONv2 status                                  */
/*                                                                           */
/* Input        : RMON2_ENABLE/RMON2_DISABLE                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : FNP_SUCCESS / FNP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
FsRMONv2HwSetStatus (UINT4 u4RMONv2Status)
{
    UNUSED_PARAM (u4RMONv2Status);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FsRMONv2HwGetStatus                                        */
/*                                                                           */
/* Description  : Retrieves the RMONv2 status                                */
/*                                                                           */
/* Input        : NONE                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : FNP_SUCCESS / FNP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
UINT4
FsRMONv2HwGetStatus (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FsRMONv2EnableProtocol                                     */
/*                                                                           */
/* Description  : Enables the Specified Protocol for this probe              */
/*                                                                           */
/* Input        : pointer to ProtocolIdfr structure                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : FNP_SUCCESS / FNP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
FsRMONv2EnableProtocol (tRmon2ProtocolIdfr Rmon2ProtoIdfr)
{
    UNUSED_PARAM (Rmon2ProtoIdfr);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FsRMONv2DisableProtocol                                    */
/*                                                                           */
/* Description  : Disables the Specified Protocol for this probe             */
/*                                                                           */
/* Input        : ProtocolIdfr Local Index                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : FNP_SUCCESS / FNP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
FsRMONv2DisableProtocol (UINT4 u4ProtocolLclIdx)
{
    UNUSED_PARAM (u4ProtocolLclIdx);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FsRMONv2EnableProbe                                        */
/*                                                                           */
/* Description  : Enables an Interface to be Monitored by Probe              */
/*                                                                           */
/* Input        : Interface Index, Vlan Id, Member Ports to be monitored     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : FNP_SUCCESS / FNP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
FsRMONv2EnableProbe (UINT4 u4InterfaceIdx, UINT2 u2VlanId, tPortList PortList)
{
    UNUSED_PARAM (u4InterfaceIdx);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (PortList);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FsRMONv2DisableProbe                                       */
/*                                                                           */
/* Description  : Disables an Interface from Monitoring                      */
/*                                                                           */
/* Input        : Interface Index, Vlan Id, Member Ports                     */
/*                to be stopped from monitoring                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : FNP_SUCCESS / FNP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
FsRMONv2DisableProbe (UINT4 u4InterfaceIdx, UINT2 u2VlanId, tPortList PortList)
{
    UNUSED_PARAM (u4InterfaceIdx);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (PortList);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FsRMONv2AddFlowStats                                       */
/*                                                                           */
/* Description  : Updates the Flow Stats tree                                */
/*                                                                           */
/* Input        : tPktHeader - Packet Info                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : FNP_SUCCESS / FNP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
FsRMONv2AddFlowStats (tPktHeader FlowStatsTuple)
{
    UNUSED_PARAM (FlowStatsTuple);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FsRMONv2RemoveFlowStats                                    */
/*                                                                           */
/* Description  : Updates the Flow Stats tree                                */
/*                                                                           */
/* Input        : tPktHeader - Packet Info                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : FNP_SUCCESS / FNP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
FsRMONv2RemoveFlowStats (tPktHeader FlowStatsTuple)
{
    UNUSED_PARAM (FlowStatsTuple);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FsRMONv2CollectStats                                       */
/*                                                                           */
/* Description  : Updates the Flow Stats tree                                */
/*                                                                           */
/* Input        : tPktHeader - Packet Info                                   */
/*                                                                           */
/* Output       : tRmon2Stats - Packet Count                                 */
/*                                                                           */
/* Returns      : FNP_SUCCESS / FNP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
FsRMONv2CollectStats (tPktHeader FlowStatsTuple, tRmon2Stats * pStatsCnt)
{
    UNUSED_PARAM (FlowStatsTuple);
    UNUSED_PARAM (pStatsCnt);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/* Function     : Rmonv2FsRMONv2GetPortID                                   */
/*                                                                          */
/* Description  : Get the port Id from hardware                             */
/*                                                                          */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : FNP_SUCCESS / FNP_FAILURE                                 */
/*                                                                          */
/****************************************************************************/
UINT1
FsRMONv2GetPortID (UINT1 *au1Mac,UINT4 u4VlanIndex,INT4 *i4PktPortNum)
{
    UNUSED_PARAM(au1Mac);   
    UNUSED_PARAM(u4VlanIndex);   
    UNUSED_PARAM(i4PktPortNum);   
    return FNP_SUCCESS;
}
#endif
