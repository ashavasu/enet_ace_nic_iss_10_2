/*****************************************************************************
 * $Id: vcmnp.c,v 1.6 2017/11/14 07:31:17 siva Exp $
 *
 * Description: This file contains the function implementations  of
 *              VCM NP-API.
 ****************************************************************************/
#include "lr.h"
#include "npapi.h"
#include "npvcm.h"

/*****************************************************************************/
/* Function Name      : FsVcmHwCreateContext                                 */
/*                                                                           */
/* Description        : This function is called when a context is created.   */
/*                                                                           */
/* Input(s)           : Context Id.                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVcmHwCreateContext (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVcmHwDeleteContext                                 */
/*                                                                           */
/* Description        : This function is called when a context is deleted.   */
/*                                                                           */
/* Input(s)           : Context Id.                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVcmHwDeleteContext (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVcmHwMapPortToContext                              */
/*                                                                           */
/* Description        : This function is invoked when a port is mapped to    */
/*                      a context.                                           */
/*                                                                           */
/* Input(s)           : Context Id, Interface Index.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVcmHwMapPortToContext (UINT4 u4ContextId, UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVcmHwUnMapPortFromContext                          */
/*                                                                           */
/* Description        : This function is invoked when a port is un-mapped    */
/*                      from a context.                                      */
/*                                                                           */
/* Input(s)           : Context Id, Interface Index.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVcmHwUnMapPortFromContext (UINT4 u4ContextId, UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVcmHwMapIfIndexToBrgPort                           */
/*                                                                           */
/* Description        : This function is invoked when a port is mapped to    */
/*                      a context.                                           */
/*                                                                           */
/* Input(s)           : Context Id, Interface Index, Local port.             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVcmHwMapIfIndexToBrgPort (UINT4 u4ContextId, UINT4 u4IfIndex,
                            tContextMapInfo ContextInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (ContextInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVcmSispHwSetPortVlanMapping                        */
/*                                                                           */
/* Description        : This NPAPI will program the hardware regarding the   */
/*                      Port Vlan Context Information. SISP enabled          */
/*                      interfaces will be part of unique vlan across        */
/*                      contexts. This holds key for packet processing and   */
/*                      sisp related processing. This NPAPI programs this    */
/*                      association into the hardware.                       */
/*                                                                           */
/* Input(s)           : u4IfIndex          - Port Interface Index            */
/*                      tVlan Id           - Vlan Identifier                 */
/*                      u4SecondaryContext - Secondary context to which port */
/*                                           should be mapped.               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVcmSispHwSetPortVlanMapping (UINT4 u4IfIndex, tVlanId VlanId,
                               UINT4 u4ContextId, UINT1 u1Status)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1Status);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVcmHwMapVirtualRouter                              */
/*                                                                           */
/* Description        : This function is called when an L3 interfaces is     */
/*                      mapped to or unmapped from, L3 Virtual Router        */
/*                      This function also creates and deletes virtual router*/
/*                      in the NP                                            */
/*                                                                           */
/* Input(s)           : VrMapAction -   Takes the following actions,         */
/*                       NP_HW_CREATE_VIRTUAL_ROUTER - Creates virtual router*/
/*                       NP_HW_DELETE_VIRTUAL_ROUTER - Deletes virtual router*/
/*                       NP_HW_MAP_INTF_TO_VR - IVR interface is mapped to   */
/*                                               virtual router              */
/*                       NP_HW_UNMAP_INTF_TO_VR - IVR Interface is unmapped  */
/*                                                from virtual router        */
/*                       NP_HW_MAP_RPORT_TO_VR - Router port is mapped to    */
/*                                               virtual router              */
/*                       NP_HW_UNMAP_RPORT_TO_VR - Router port is unmapped   */
/*                                                 from virtual router       */
/*                    : pVcmVrMapInfo - Virutal router   mapping info        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVcmHwMapVirtualRouter (tVrMapAction VrMapAction,
                         tFsNpVcmVrMapInfo * pVcmVrMapInfo)
{
    UNUSED_PARAM (VrMapAction);
    UNUSED_PARAM (pVcmVrMapInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVcmSispHwSetPortCtrlStatus                         */
/*                                                                           */
/* Description        : This NPAPI will be used to update SISP enable/disable*/
/*                      status on a physical or port channel port.           */
/*                                                                           */
/* Input(s)           : Interface Index, SISP status(Enable/Disable)         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVcmSispHwSetPortCtrlStatus (UINT4 u4IfIndex, UINT1 u1Status)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVcmHwHandleVrfCounter                              */
/*                                                                           */
/* Description        : This NPAPI will program the hardware regarding the   */
/*                      VRF Counters                                         */
/* Input(s)           : VrfCounter          - VRF Statistics                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On Failure                             */
/*****************************************************************************/

INT4
FsVcmHwHandleVrfCounter (tVrfCounter VrfCounter)
{
    UNUSED_PARAM (VrfCounter);
    return FNP_SUCCESS;
}
