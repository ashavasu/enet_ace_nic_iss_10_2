/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mstnp.c,v 1.15 2007/07/17 13:29:53 iss Exp $ fsbrgnp.c
 *
 * Description: All network processor function  given here
 *
 *******************************************************************/

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fssnmp.h"
#include "fsvlan.h"
#include "mstp.h"
#include "npapi.h"
#include "npmstp.h"

/***************************************************************************/
/* FUNCTION NAME : FsMstpNpCreateInstance                                  */
/*                                                                         */
/* DESCRIPTION   : Creates a Spanning Tree instance in the Hardware.       */
/* INPUTS        : u2InstId - Instance id of the Spanning tree to be       */
/*                 created                                                 */
/*                 ranges <1-64>.                                          */
/*                                                                         */
/* OUTPUTS       : None                                                    */
/*                                                                         */
/* RETURNS       : FNP_SUCCESS - success                                   */
/*                 FNP_FAILURE - Error during creating                     */
/* COMMENTS      : None                                                    */
/***************************************************************************/

PUBLIC INT1
FsMstpNpCreateInstance (UINT2 u2InstId)
{
    UNUSED_PARAM (u2InstId);
    return FNP_SUCCESS;
}

/***************************************************************************/
/* FUNCTION NAME : FsMstpNpAddVlanInstMapping                              */
/*                                                                         */
/* DESCRIPTION   : Map a VLAN to the Spanning Tree Instance                */
/*                 in the hardware.                                        */
/*                                                                         */
/* INPUTS        : u2VlanId - VLAN Id that has to be mapped.               */
/*                 u2InstId - Spanning tree instance Id ranges <1-64>.     */
/*                                                                         */
/* OUTPUTS       : None                                                    */
/*                                                                         */
/* RETURNS       : FNP_SUCCESS - success                                   */
/*                 FNP_FAILURE - Error during mapping                      */
/*                                                                         */
/* COMMENTS      : None                                                    */
/*                                                                         */
/***************************************************************************/

PUBLIC INT1
FsMstpNpAddVlanInstMapping (tVlanId VlanId, UINT2 u2InstId)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2InstId);
    return FNP_SUCCESS;
}

/***************************************************************************/
/* FUNCTION NAME : FsMstpNpAddVlanListInstMapping                          */
/*                                                                         */
/* DESCRIPTION   : Map a set of VLANs to the Spanning Tree Instance in     */
/*                 the hardware.                                           */
/*                                                                         */
/* INPUTS        : pu1VlanList - List of VLAN Ids to be mapped.            */
/*                 u2InstId    - Spanning tree instance Id ranges <1-64>.  */
/*                 u2NumVlans  - Number of VLANs to be mapped.             */
/*                 pu2LastVlan - VLAN ID of the last VLAN mapped           */
/*                 successfully.                                           */
/*                                                                         */
/* OUTPUTS       : None                                                    */
/*                                                                         */
/* RETURNS       : FNP_SUCCESS - success                                   */
/*                 FNP_FAILURE - Error during mapping                      */
/*                                                                         */
/* COMMENTS      : None                                                    */
/*                                                                         */
/***************************************************************************/

PUBLIC INT1
FsMstpNpAddVlanListInstMapping (UINT1 *pu1VlanList, UINT2 u2InstId,
                                UINT2 u2NumVlans, UINT2 *pu2LastVlan)
{
    UNUSED_PARAM (pu1VlanList);
    UNUSED_PARAM (u2InstId);
    UNUSED_PARAM (u2NumVlans);
    UNUSED_PARAM (pu2LastVlan);
    return FNP_SUCCESS;
}

/***************************************************************************/
/* FUNCTION NAME : FsMstpNpDeleteInstance                                  */
/*                                                                         */
/* DESCRIPTION   : Delete the Spanning Tree instance in the hardware.      */
/*                 Hardware should take of UnMapping the Vlan to to        */
/*                 Instance                                                */
/*                 Mapping and should delete the Instance.                 */
/*                                                                         */
/* INPUTS        : u2InstId - Instance id of the Spanning tree to          */
/*                be deleted ranges <1-64>                                 */
/*                                                                         */
/* OUTPUTS       : None                                                    */
/*                                                                         */
/* RETURNS       : FNP_SUCCESS - success                                   */
/*                 FNP_FAILURE - Error during deleting                     */
/*                                                                         */
/* COMMENTS      : None                                                    */
/*                                                                         */
/***************************************************************************/

PUBLIC INT1
FsMstpNpDeleteInstance (UINT2 u2InstId)
{
    UNUSED_PARAM (u2InstId);
    return FNP_SUCCESS;
}

/***************************************************************************/
/* FUNCTION NAME : FsMstpNpDelVlanInstanceMapping                          */
/*                                                                         */
/* DESCRIPTION   : Delete the VLAN-Spanning Tree instance mapping in the   */
/*                 hardware.                                               */
/*                                                                         */
/* INPUTS        : u2VlanId - VLAN Id that has to be unmapped              */
/*                    ranges<1-4094>.                                      */
/*                 u2InstId - Spanning tree instance Id ranges <1-64>.     */
/* OUTPUTS       : None                                                    */
/*                                                                         */
/* RETURNS       : FNP_SUCCESS - success                                   */
/*                 FNP_FAILURE - Error during deleting the mapping         */
/*                                                                         */
/* COMMENTS      : None                                                    */
/*                                                                         */
/***************************************************************************/

PUBLIC INT1
FsMstpNpDelVlanInstMapping (tVlanId VlanId, UINT2 u2InstId)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2InstId);
    return FNP_SUCCESS;
}

/***************************************************************************/
/* FUNCTION NAME : FsMstpNpDelVlanListInstanceMapping                      */
/* DESCRIPTION   : Delete the VLAN-Spanning Tree instance mapping in the   */
/*                 hardware for a set of VLANs                             */
/* INPUTS        : pu1VlanList - List of VLAN Ids to be unmapped.          */
/*                 u2InstId - Spanning tree instance Id ranges <1-64>.     */
/*                 u2NumVlans - Number of VLANs to be unmapped.            */
/*                 pu2LastVlan - VLAN ID of the last vlan unmapped         */
/*                 successfully                                            */
/*                                                                         */
/* OUTPUTS       : None                                                    */
/*                                                                         */
/* RETURNS       : FNP_SUCCESS - success                                   */
/*                 FNP_FAILURE - Error during deleting the mapping         */
/* COMMENTS      : None                                                    */
/*                                                                         */
/***************************************************************************/

PUBLIC INT1
FsMstpNpDelVlanListInstMapping (UINT1 *pu1VlanList, UINT2 u2InstId,
                                UINT2 u2NumVlans, UINT2 *pu2LastVlan)
{
    UNUSED_PARAM (pu1VlanList);
    UNUSED_PARAM (u2InstId);
    UNUSED_PARAM (u2NumVlans);
    UNUSED_PARAM (pu2LastVlan);
    return FNP_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME : FsMstpNpSetInstancePortState                          */
/* DESCRIPTION   : Sets the MSTP Port State in the Hardware for given    */
/*                 INSTANCE. When MSTP is operating in MSTP mode or MSTP */
/*                 in RSTP compatible mode or MSTP in STP compatible 
 *                 mode.                                                 */
/* INPUTS        : u4IfIndex   - Interface index of whose STP state is to*/
/*                 be updated                                            */
/*                 u2InstanceId- Instance ID ranges <1-64>.              */
/*                 u1PortState - Value of Port State can take any of the */
/*                 following AST_PORT_STATE_FORWARDING,                  */
/*                 AST_PORT_STATE_LEARNING,                              */
/*                 AST_PORT_STATE_DISCARDING,                            */
/*                 AST_PORT_STATE_DISABLED.                              */
/*  OUTPUTS       : None                                                 */
/* RETURNS       : FNP_SUCCESS - success                                 */
/*                 FNP_FAILURE - Error during setting                    */
/* COMMENTS      : None                                                  */
/*************************************************************************/
INT4
FsMstpNpSetInstancePortState (UINT4 u4IfIndex, UINT2 u2InstanceId,
                              UINT1 u1PortState)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2InstanceId);
    UNUSED_PARAM (u1PortState);
    return FNP_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME : FsMstpNpInitHw                                        */
/* DESCRIPTION   : This function performs any necessary MSTP related     */
/*                 initialisation in the Hardware                        */
/* INPUTS        : None                                                  */
/* OUTPUTS       : None                                                  */
/* RETURNS       : None                                                  */
/*************************************************************************/

VOID
FsMstpNpInitHw (VOID)
{
    return;
}

/*************************************************************************/
/* FUNCTION NAME : FsMstpNpDeInitHw                                      */
/* DESCRIPTION   : Sets the MSTP Disable Option  in the Hardware.        */
/*                 This function removes MSTP related initialisation     */
/*                 (done in FsMstpNpInitHw) from the Hardware            */
/* INPUTS        : None                                                  */
/* OUTPUTS       : None                                                  */
/* RETURNS       : None                                                  */
/*************************************************************************/

VOID
FsMstpNpDeInitHw (VOID)
{
    return;                        /* stub */
}

/*************************************************************************/
/* Function Name      : FsMstNpGetPortState                              */
/*                                                                       */
/* Description        : Gets the MSTP Port State in the Hardware.        */
/*                                                                       */
/*                                                                       */
/* Input(s)           : u2InstanceId - ranges <1-64>
 *                      u4IfIndex - Port Number.                         */
/*                      pu1Status  - Status returned from Hardware.      */
/*                                  Values can be 
 *                                  AST_PORT_STATE_DISCARDING            */
/*                                  or AST_PORT_STATE_LEARNING           */
/*                                  or AST_PORT_STATE_FORWARDING         */
/*                                                                       */
/* Output(s)          : None                                             */
/*                                                                       */
/* Global Variables                                                      */
/* Referred           : None                                             */
/*                                                                       */
/* Global Variables                                                      */
/* Modified           : None                                             */
/*                                                                       */
/* Return Value(s)    : FNP_SUCCESS - On successful set (or)             */
/*                      FNP_FAILURE - Error during setting               */
/*************************************************************************/
INT1
FsMstNpGetPortState (UINT2 u2InstanceId, UINT4 u4IfIndex, UINT1 *pu1Status)
{
    UNUSED_PARAM (u2InstanceId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1Status);
    return FNP_SUCCESS;
}
