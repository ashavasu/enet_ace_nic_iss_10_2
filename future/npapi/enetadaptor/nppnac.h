/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/
#ifndef _NPPNAC_H
#define _NPPNAC_H

#include "pnacnp.h"

#ifdef MBSM_WANTED
INT4 PnacMbsmHwCreatePnacFilters PROTO ((tMbsmSlotInfo *));
#endif
#endif /* _NPPNAC_H */
