/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mstminpx.c,v 1.4 2007/07/17 13:29:53 iss Exp $ fsbrgnp.c
 *
 * Description: All network processor function  given here
 *
 *******************************************************************/
#ifdef  MSTP_WANTED

#ifndef _MSTPMINPX_C_
#define _MSTPMINPX_C_

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "rstp.h"
#include "mstp.h"
#include "npapi.h"
#include "npmstpmi.h"
#include "npcfa.h"

/***************************************************************************
 * FUNCTION NAME : FsMiMstpMbsmNpCreateInstance                            *
 *                                                                         *
 * DESCRIPTION   : Creates a Spanning Tree instance in the Hardware.       * 
 *                                                                         *
 * INPUTS        : u2InstId - Instance id of the Spanning tree to be 
 *                 created.                                                *
 *                 ranges <1-64>                                           *
 *                                                                         *
 * OUTPUTS       : None                                                    *
 *                                                                         *
 * RETURNS       : FNP_SUCCESS - success                                   *
 *                 FNP_FAILURE - Error during creating                     * 
 *                                                                         * 
 * COMMENTS      : None                                                    *
 *                                                                         *
 ***************************************************************************/

PUBLIC INT1
FsMiMstpMbsmNpCreateInstance (UINT4 u4ContextId, UINT2 u2InstId,
                              tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2InstId);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME : FsMiMstpMbsmNpAddVlanInstMapping                        *
 *                                                                         *
 * DESCRIPTION   : Map a VLAN to the Spanning Tree Instance in the 
 *                     hardware.                                           *
 *                                                                         *
 * INPUTS        : u4ContextId - context Id.
 *                 VlanId - VLAN Id that has to be mapped ranges <1-4094>. *
 *                 u2InstId - Spanning tree instance Id ranges <1-64>.
 *                 pSlotInfo - Pointer to the slot information structure   *
 *                                                                         *
 * OUTPUTS       : None                                                    *
 *                                                                         *
 * RETURNS       : FNP_SUCCESS - success                                   *
 *                 FNP_FAILURE - Error during mapping                      *
 *                                                                         *
 * COMMENTS      : None                                                    * 
 *                                                                         *
 ***************************************************************************/

PUBLIC INT1
FsMiMstpMbsmNpAddVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId,
                                  UINT2 u2InstId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2InstId);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/* Program port state for instance */

/***************************************************************************
 * FUNCTION NAME : FsMiMstpMbsmNpSetInstancePortState                      * 
 *                                                                         *
 * DESCRIPTION   : Sets the MSTP Port State in the Hardware for given 
 *                  INSTANCE.                                              *  
 *                 When MSTP is operating in MSTP mode or MSTP in RSTP     *
 *                 compatible mode or MSTP in STP compatible mode.         *
 *                                                                         *
 * INPUTS        : pSlotInfo - Slot Information                            *
 *                 u4ContextId - Context ID                                *  
 *                 u4IfIndex   - Interface index of whose STP state is to  *  
 *                 be updated                                              *
 *                 u2InstanceId- Instance ID ranges <1-64>.                *
 *                 u1PortState - Value of Port State can take any of       *
 *                 the following.                                          *
 *                 AST_PORT_STATE_DISABLED,                                * 
 *                 AST_PORT_STATE_DISCARDING,                              *
 *                 AST_PORT_STATE_LEARNING,                                *
 *                 AST_PORT_STATE_FORWARDING                               *
 * 
 *  OUTPUTS       : None                                                         *
 *
 * RETURNS       : FNP_SUCCESS - success                                   *
 *                 FNP_FAILURE - Error during setting                      *
 *
 *  COMMENTS      : None                                                   *
 *                                                                         *
 ***************************************************************************/
INT1
FsMiMstpMbsmNpSetInstancePortState (UINT4 u4ContextId, UINT4 u4IfIndex,
                                    UINT2 u2InstanceId, UINT1 u1PortState,
                                    tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2InstanceId);
    UNUSED_PARAM (u1PortState);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/***************************************************************************
 * Function Name                  :FsMiMstpMbsmNpInitHw                    *
 *                                                                         *
 * DESCRIPTION                    :This function performs any necessary    * 
 *                                 MSTP related initialisation in the      *
 *                                 Hardware                                *
 *                                                                         *
 * INPUTS                         : pSlotInfo - Slot Information           *
 *                                : u4ContextId - Context ID               *  
 *                                                                         * 
 * OUTPUTS                        : None                                   *
 *                                                                         *
 * RETURNS                        : FNP_SUCCESS - On Success               *
 *                                  FNP_FAILURE - On Failure               *
 *                                                                         *
 ***************************************************************************/

INT1
FsMiMstpMbsmNpInitHw (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

#endif /* MSTP_WANTED */
#endif
