/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: npcfa.h,v 1.6 2012/05/10 13:20:16 siva Exp $
 *
 * Description: Exported file for CFA NP-API.
 *
 *******************************************************************/
#ifndef __NPCFA_H
#define __NPCFA_H
#include "cfanp.h"

#define CFA_NP_KNOWN_UCAST_PKT       1
#define CFA_NP_UNKNOWN_UCAST_PKT     2
#define CFA_NP_BCAST_PKT             3
#define CFA_NP_MCAST_PKT             4

#define CFA_NP_UNTAGGED              5
#define CFA_NP_TAGGED                6

#define CFA_NP_IS_PORT_CHANNEL(u2Port) \
        (((u2Port > BRG_MAX_PHY_PORTS) && (u2Port <= BRG_MAX_PHY_PLUS_LOG_PORTS)) \
        ? FNP_TRUE : FNP_FALSE)



#define NP_MAX_PACKET_SIZE 1600

#define CFA_NP_API             0x00000001

#define   CFA_GDD_INTERRUPT_EVENT     0x00010000

#ifdef MBSM_WANTED
PUBLIC INT4 CfaMbsmNpEnablePort PROTO ((UINT4 , INT4 ));
INT4 CfaMbsmNpRegisterProtocol PROTO ((UINT1 u1Protocol,
                                       tMbsmSlotInfo *pSlotInfo));
#endif


#define CFA_GET_LPORT_FROM_IFIDX(ifidx) CustNpGetLportFromIfIndex(ifidx)  /*for customized portmapping */

#define CFA_GET_IFIDX_FROM_LPORT(lport) CustNpGeIfIndexFromLport(lport)  /*for customized portmapping  */

/********************************************************************
      NP CFA util API's used in _CFANP_C scope
*********************************************************************/

INT4
NpCfaUtilPortRead (tCRU_BUF_CHAIN_HEADER  **pCruBufdt);


INT4 run_packet_task(void);
INT4 CfaHwL2VlanIntfRead (UINT1 src_dev, UINT4 lport, BOOL1 WithTag,UINT2 vid,UINT1 priority, BOOL1 IsTrunk,void *gtBuf,UINT4 len);
    
INT4 CfaNpControlCPURateLimit PROTO ((VOID *gtBuf));


PUBLIC UINT4 CfaGddSendIntrEvent PROTO ((VOID));

#endif /* __NPCFA_H */
