/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mstnpx.c,v 1.4 2007/07/17 13:29:53 iss Exp $ fsbrgnp.c
 *
 * Description: All network processor function  given here
 *
 *******************************************************************/
#ifdef  MSTP_WANTED

#ifndef _MSTPNPX_C_
#define _MSTPNPX_C_

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fssnmp.h"
#include "fsvlan.h"
#include "mstp.h"
#include "npapi.h"
#include "npmstp.h"

/*************************************************************************
 * FUNCTION NAME : FsMstpMbsmNpCreateInstance                            *
 *                                                                       *
 * DESCRIPTION   : Creates a Spanning Tree instance in the Hardware.     *
 *                                                                       *
 * INPUTS        : u2InstId - Instance id of the Spanning tree to be     *
 *                               created.                                *
 *                 ranges <1-64>                                         *
 *                                                                       *
 * OUTPUTS       : None                                                  *
 *                                                                       *
 * RETURNS       : FNP_SUCCESS - success                                 *
 *                 FNP_FAILURE - Error during creating                   *
 *                                                                       *
 * COMMENTS      : None                                                  *
 *                                                                       *
 *************************************************************************/

PUBLIC INT1
FsMstpMbsmNpCreateInstance (UINT2 u2InstId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u2InstId);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*************************************************************************
 * FUNCTION NAME : FsMstpMbsmNpAddVlanInstMapping                        *  
 *                                                                       *
 * DESCRIPTION   : Map a VLAN to the Spanning Tree Instance in the       *
 *                                       hardware.                       *
 *                                                                       * 
 * INPUTS        : VlanId - VLAN Id that has to be mapped ranges         * 
 *                                                            <1-4094>.  *
 *                 u2InstId - Spanning tree instance Id ranges <1-64>.   *
 *                                                                       *
 * OUTPUTS       : None                                                  *
 *                                                                       *
 * RETURNS       : FNP_SUCCESS - success                                 *
 *                 FNP_FAILURE - Error during mapping                    *
 *                                                                       *
 * COMMENTS      : None                                                  *
 *                                                                       *
 *************************************************************************/

PUBLIC INT1
FsMstpMbsmNpAddVlanInstMapping (tVlanId VlanId, UINT2 u2InstId,
                                tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2InstId);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/* Program port state for instance */

/*************************************************************************
 * FUNCTION NAME : FsMstpMbsmNpSetInstancePortState                      *
 *
 * DESCRIPTION   : Sets the MSTP Port State in the Hardware for given 
 *                        INSTANCE.                                      * 
 *                 When MSTP is operating in MSTP mode or MSTP in RSTP   *
 *                 compatible mode or MSTP in STP compatible mode.       *
 *
 * INPUTS        : u4IfIndex   - Interface index of whose STP state 
 *                is to be    updated                                    *
 *                 u2InstanceId- Instance ID ranges <1-64>.              *
 *                 u1PortState - Value of Port State 
 *                 AST_PORT_STATE_DISABLED,                              *
 *                 AST_PORT_STATE_DISCARDING,AST_PORT_STATE_LEARNING,    * 
 *                 AST_PORT_STATE_FORWARDING.                            *
 * 
 * OUTPUTS       : None                                                  * 
 *
 * RETURNS       : FNP_SUCCESS - success                                 *
 *                 FNP_FAILURE - Error during setting                    *
 *
 * COMMENTS      : None                                                  *
 *
 *************************************************************************/
INT1
FsMstpMbsmNpSetInstancePortState (UINT4 u4IfIndex, UINT2 u2InstanceId,
                                  UINT1 u1PortState, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2InstanceId);
    UNUSED_PARAM (u1PortState);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*************************************************************************
 * Function Name                  :FsMstpMbsmNpInitHw                    *
 *                                                                       *
 * DESCRIPTION                    :This function performs any necessary  * 
 *                                 MSTP related initialisation in the    *
 *                                 Hardware                              *
 *                                                                       *
 * INPUTS                         : pSlotInfo - Slot Information         *
 *                                                                       *
 *                                                                       *
 * OUTPUTS                        : None                                 *
 *                                                                       *
 * RETURNS                        : FNP_SUCCESS - On Success             *
 *                                  FNP_FAILURE - On Failure             *
 *                                                                       *
 *************************************************************************/

INT1
FsMstpMbsmNpInitHw (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

#endif /* MSTP_WANTED */
#endif
