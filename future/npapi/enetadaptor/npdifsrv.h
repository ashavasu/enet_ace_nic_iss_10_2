/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: npdifsrv.h,v 1.3 2007/02/01 14:59:31 iss Exp $
 *
 * Description: This file contains the function implementations  of DiffSrv 
 *              System related NP-API.
 ****************************************************************************/

#ifndef _NPDS_H
#define _NPDS_H

#include "difsrvnp.h"

#endif  /* _NPDS_H */
