/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: eoamnpx.c,v 1.2 2007/02/01 14:59:31 iss Exp $
 * 
 * Description: All prototypes for Network Processor API functions
 *  *           done here.
 *****************************************************************************/
#ifdef EOAM_WANTED

#include "lr.h"
#include "mbsm.h"
#include "npapi.h"
#include "eoamnp.h"

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMbsmHwInit
 *                                                                          
 *    DESCRIPTION      : This function is invoked inorder to register the 
 *                       call back funtion for posting the Link
 *                       monitoring threshold crossing
 *                       event details to the Ethernet OAM task once 
 *                       the card gets inserted.This is applicable only 
 *                       when Link Monitoring is done in HW.
 *
 *    INPUT            : pSlotInfo - Pointer to the SlotInfo structure
 *                       through which the slot number is identified.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : FNP_SUCCESS/FNP_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
EoamMbsmHwInit (tMbsmSlotInfo * pSlotInfo)
{
    /* The registration to the call back function is applicable only
     * if polling is done in HW */
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}
#endif /* EOAM_WANTED */
/*-----------------------------------------------------------------------*/
/*                       End of the file  eoamnpx.c                      */
/*-----------------------------------------------------------------------*/
