/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pvrstnp.c,v 1.2 2008/01/07 14:42:27 iss Exp $ fsbrgnp.c
 *
 * Description: All network processor function  given here
 *
 *******************************************************************/

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fssnmp.h"
#include "fsvlan.h"
#include "pvrst.h"
#include "npapi.h"
#include "nppvrst.h"

/*******************************************************************************
 * FsPvrstNpCreateVlanSpanningTree
 *
 * DESCRIPTION:
 * Creates a Spanning Tree for the given VLAN. In case hardware supports automatic 
 * spanning tree creation for Vlan then this routine should not be ported
 *
 * INPUTS:
 * VlanId - Vlan id of the Spanning tree to be created.
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * FNP_SUCCESS - success
 * FNP_FAILURE - Error during creating
 *
 * COMMENTS:
 *  None
 *
 *******************************************************************************/

PUBLIC INT1
FsPvrstNpCreateVlanSpanningTree (tVlanId VlanId)
{
    UNUSED_PARAM (VlanId);
    return FNP_SUCCESS;
}

/*******************************************************************************
 * FsPvrstNpDeleteVlanSpanningTree
 *
 * DESCRIPTION:
 * Delete the Spanning Tree instance in the hardware for the given Vlan.
 *
 * INPUTS:
 * VlanId - Vlan id of the Spanning tree to be deleted
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * FNP_SUCCESS - success
 * FNP_FAILURE - Error during deleting
 *
 * COMMENTS:
 *  None
 *
 *******************************************************************************/

PUBLIC INT1
FsPvrstNpDeleteVlanSpanningTree (tVlanId VlanId)
{
    UNUSED_PARAM (VlanId);
    return FNP_SUCCESS;
}

/*****************************************************************************
 * FsPvrstNpInitHw 
 *
 * DESCRIPTION:
 * This function performs any necessary PVRST related initialisation in 
 * the Hardware
 *
 * INPUTS:
 * None 
 *
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * None
 *
 *****************************************************************************/

INT4
FsPvrstNpInitHw (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************
 * FsPvrstNpDeInitHw 
 *
 * DESCRIPTION:
 * Sets the PVRST Disable Option  in the Hardware.
 * This function removes PVRST related initialisation (done in FsPvrstNpInitHw) 
 * from the Hardware
 *
 * INPUTS:
 * None
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * None
 *
 *****************************************************************************/

VOID
FsPvrstNpDeInitHw (VOID)
{
    return;
}

/*******************************************************************************
 * FsPvrstNpSetVlanPortState
 *
 * DESCRIPTION:
 * Sets the PVRST Port State in the Hardware for given Vlan. 
 *
 * INPUTS:
 * None
 * u4IfIndex   - Interface index of whose Vlan state is to be updated
 * VlanId - Vlan ID.     
 * u1PortState - Value of Port State.
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * FNP_SUCCESS - success
 * FNP_FAILURE - Error during setting
 *
 * COMMENTS:
 *  None
 *
 *******************************************************************************/
INT4
FsPvrstNpSetVlanPortState (UINT4 u4IfIndex, tVlanId VlanId, UINT1 u1PortState)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1PortState);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsPvrstNpGetVlanPortState                          */
/*                                                                           */
/* Description        : Gets the Vlan Port State in the Hardware.            */
/*                                                                           */
/*                                                                           */
/* Input(s)           : VlanId      - Vlan ID.                               */
/*                      u4IfIndex - Port Number.                             */
/*                      pu1Status  - Status returned from Hardware.          */
/*                                  Values can be AST_PORT_STATE_DISCARDING  */
/*                                  or AST_PORT_STATE_LEARNING               */
/*                                  or AST_PORT_STATE_FORWARDING             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On successful set (or)                 */
/*                      FNP_FAILURE - Error during setting                   */
/*****************************************************************************/
INT1
FsPvrstNpGetVlanPortState (tVlanId VlanId, UINT4 u4IfIndex, UINT1 *pu1Status)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1Status);
    return FNP_SUCCESS;
}
