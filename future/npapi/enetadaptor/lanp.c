/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lanp.c,v 1.10 2014/06/13 12:55:53 siva Exp $
 *
 * Description: Exported file for FS NP-API.
 *
 *******************************************************************/

/*****************************************************************************/
/*                           INCLUDE FILES                                   */
/*****************************************************************************/
#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "la.h"
#include "npapi.h"

#include "brgnp.h"
#include "npla.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "EnetHal_L2_Api.h"

/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : FsLaHwCreateAggGroup.                                */
/*                                                                           */
/* Description        : This function is called when an aggregator's Oper    */
/*                      Status becomes UP i.e. when the first port is        */
/*                      attached to the aggregator.                          */
/*                                                                           */
/* Input(s)           : Aggregator Index.                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                              */
/*                      FNP_FAILURE - On failure                              */
/*****************************************************************************/
INT4
FsLaHwCreateAggGroup (UINT2 u2AggIndex, UINT2 *pu2HwAggId)
{
    UINT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
    ret = EnetHal_FsLaHwCreateAggGroup (u2AggIndex, pu2HwAggId);
#endif //USER_HW_API

    return ret;
}

/*****************************************************************************/
/* Function Name      : FsLaHwAddLinkToAggGroup.                             */
/*                                                                           */
/* Description        : This function is called to attach a port to its      */
/*                      aggregator.                                          */
/*                                                                           */
/* Input(s)           : Aggregator Index, Port Number.                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                              */
/*                      FNP_FAILURE - On failure                              */
/*****************************************************************************/
INT4
FsLaHwAddLinkToAggGroup (UINT2 u2AggIndex, UINT2 u2PortNumber,
                         UINT2 *pu2HwAggId)
{
    /* This API needs to be ported in such a way that the STAP states for
     * the secondary indices are as of same as the aggregated index depends
     * upon the STP running [it can be one of the following: STP/RSTP/MSTP]
     */
    UINT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
    ret = EnetHal_FsLaHwAddLinkToAggGroup (u2AggIndex, u2PortNumber, pu2HwAggId);
#endif //USER_HW_API

    return ret;
}

/*****************************************************************************/
/* Function Name      : FsLaHwDlagAddLinkToAggGroup.                         */
/*                                                                           */
/* Description        : This function is called to attach a port to its      */
/*                      aggregator.                                          */
/*                                                                           */
/* Input(s)           : Aggregator Index, Port Number.                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsLaHwDlagAddLinkToAggGroup (UINT2 u2AggIndex, UINT2 u2PortNumber,
                             UINT2 *pu2HwAggId)
{

    UNUSED_PARAM (u2AggIndex);
    UNUSED_PARAM (u2PortNumber);
    UNUSED_PARAM (pu2HwAggId);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      :  FsLaHwSetSelectionPolicy.                           */
/*                                                                           */
/* Description        : This function is called to set the selection policy. */
/*                                                                           */
/* Input(s)           : Aggregation Index                                    */
/*                      Link Selection Policy                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                              */
/*                      FNP_FAILURE - On failure                              */
/*****************************************************************************/
INT4
FsLaHwSetSelectionPolicy (UINT2 u2AggIndex, UINT1 u1SelectionPolicy)
{
    UINT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
    ret = EnetHal_FsLaHwSetSelectionPolicy (u2AggIndex, u1SelectionPolicy);
#endif //USER_HW_API

    return ret;
}

/*****************************************************************************/
/* Function Name      :  FsLaHwSetSelectionPolicyBitList                     */
/*                                                                           */
/* Description        : This function is called to set the selection policy. */
/*                                                                           */
/* Input(s)           : Aggregation Index                                    */
/*                      Link Selection Policy                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                              */
/*****************************************************************************/
INT4
FsLaHwSetSelectionPolicyBitList (UINT2 u2AggIndex,
                                 UINT4 u4SelectionPolicyBitList)
{
    UINT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
    ret = EnetHal_FsLaHwSetSelectionPolicyBitList (u2AggIndex, u4SelectionPolicyBitList);
#endif //USER_HW_API

    return ret;
}

/*****************************************************************************/
/* Function Name      : FsLaHwRemoveLinkFromAggGroup.                        */
/*                                                                           */
/* Description        : This function is called when a attached port is      */
/*                      removed from the aggregation group.                  */
/*                                                                           */
/* Input(s)           : Aggregator Index, Port Number.                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                              */
/*                      FNP_FAILURE - On failure                              */
/*****************************************************************************/
INT4
FsLaHwRemoveLinkFromAggGroup (UINT2 u2AggIndex, UINT2 u2PortNumber)
{
    UNUSED_PARAM (u2AggIndex);
    UNUSED_PARAM (u2PortNumber);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsLaHwDlagRemoveLinkFromAggGroup.                    */
/*                                                                           */
/* Description        : This function is called when a attached port is      */
/*                      removed from the aggregation group.                  */
/*                                                                           */
/* Input(s)           : Aggregator Index, Port Number.                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsLaHwDlagRemoveLinkFromAggGroup (UINT2 u2AggIndex, UINT2 u2PortNumber)
{
    UNUSED_PARAM (u2AggIndex);
    UNUSED_PARAM (u2PortNumber);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsLaHwDeleteAggregator.                              */
/*                                                                           */
/* Description        : This function is called when the last port attached  */
/*                      to the aggregator goes down. This function will be   */
/*                      called after LaHwRemoveLinkFromAggGroup() for the    */
/*                      last port in the aggregator.                         */
/*                                                                           */
/* Input(s)           : Aggregator Index.                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                              */
/*                      FNP_FAILURE - On failure                              */
/*****************************************************************************/
INT4
FsLaHwDeleteAggregator (UINT2 u2AggIndex)
{
    UNUSED_PARAM (u2AggIndex);
    return FNP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : FsLaHwInit.                                          */
/*                                                                           */
/* Description        : This function is called when LA is initialised.      */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                              */
/*                      FNP_FAILURE - On failure                              */
/*****************************************************************************/
INT4
FsLaHwInit (VOID)
{
    UINT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
    ret = EnetHal_FsLaHwInit ();
#endif //USER_HW_API

    return ret;
}

/*****************************************************************************/
/* Function Name      : FsLaHwDeInit.                                        */
/*                                                                           */
/* Description        : This function is called when LA is shut down.        */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                              */
/*                      FNP_FAILURE - On failure                              */
/*****************************************************************************/
INT4
FsLaHwDeInit (VOID)
{
    return FNP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : FsLaHwEnableCollection.                              */
/*                                                                           */
/* Description        : This function is called when collection is to be     */
/*                      enabled on the port.                                 */
/*                                                                           */
/* Input(s)           : Port number.                                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                              */
/*                      FNP_FAILURE - On failure                              */
/*****************************************************************************/
INT4
FsLaHwEnableCollection (UINT2 u2AggIndex, UINT2 u2PortNumber)
{
    UINT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
    ret = EnetHal_FsLaHwEnableCollection (u2AggIndex, u2PortNumber);
#endif //USER_HW_API

    return ret;
}

/*****************************************************************************/
/* Function Name      : FsLaHwEnableCollection.                              */
/*                                                                           */
/* Description        : This function is called when distribution is to be   */
/*                      enabled on the port.                                 */
/*                                                                           */
/* Input(s)           : Port number.                                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                              */
/*                      FNP_FAILURE - On failure                              */
/*****************************************************************************/
INT4
FsLaHwEnableDistribution (UINT2 u2AggIndex, UINT2 u2PortNumber)
{
    UNUSED_PARAM (u2PortNumber);
    UNUSED_PARAM (u2AggIndex);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsLaHwDisableCollection.                             */
/*                                                                           */
/* Description        : This function is called when collection is to be     */
/*                      disabled on the port.                                */
/*                                                                           */
/* Input(s)           : Port number.                                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                              */
/*                      FNP_FAILURE - On failure                              */
/*****************************************************************************/
INT4
FsLaHwDisableCollection (UINT2 u2AggIndex, UINT2 u2PortNumber)
{
    UNUSED_PARAM (u2PortNumber);
    UNUSED_PARAM (u2AggIndex);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsLaHwSetPortChannelStatus                           */
/*                                                                           */
/* Description        : This function is called from LA after getting the    */
/*                      actual port-channel status from L2IWF.               */
/*                      Port status of all ports in the AggEntry should      */
/*                      be programmed based on the module status of          */
/*                      RSTP/MSTP.                                           */
/*                                                                           */
/* Input(s)           : u2AggIndex - AggIndex                                */
/*                      u2Inst     - Instance of the Port channel interface  */
/*                      u1StpState - Stp state of the port channel in the    */
/*                                   given instance. If MSTP is not enabled  */
/*                                   the u2Inst will be the default instance.*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsLaHwSetPortChannelStatus (UINT2 u2AggIndex, UINT2 u2Inst, UINT1 u1StpState)
{
    UINT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
    ret = EnetHal_FsLaHwSetPortChannelStatus (u2AggIndex, u2Inst, u1StpState);
#endif //USER_HW_API

    return ret;
}

/*****************************************************************************/
/* Function Name      : FsLaHwAddPortToConfAggGroup                          */
/*                                                                           */
/* Description        : This function is called to add a port to the         */
/*                      configured port list of the NP Agg entry whenever a  */
/*                      port is added to a port-channel interface.           */
/*                                                                           */
/* Input(s)           : u2AggIndex - AggIndex.                               */
/*                      u2PortNumber - Port to be added to port-channel.     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsLaHwAddPortToConfAggGroup (UINT2 u2AggIndex, UINT2 u2PortNumber)
{
	UINT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
    ret = EnetHal_FsLaHwAddPortToConfAggGroup (u2AggIndex, u2PortNumber);
#endif //USER_HW_API

    return ret;
}

/*****************************************************************************/
/* Function Name      : FsLaHwRemovePortFromConfAggGroup                     */
/*                                                                           */
/* Description        : This function is called whenever a port is removed   */
/*                      from the configured list of a port-channel.          */
/*                                                                           */
/* Input(s)           : u2AggIndex - AggIndex.                               */
/*                      u2PortNumber - Port to be removed from port-channel. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsLaHwRemovePortFromConfAggGroup (UINT2 u2AggIndex, UINT2 u2PortNumber)
{
	UINT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
    ret = EnetHal_FsLaHwRemovePortFromConfAggGroup (u2AggIndex, u2PortNumber);
#endif //USER_HW_API

    return ret;
}

/*****************************************************************************/
/* Function Name      : FsLaHwDlagStatus                                     */
/*                                                                           */
/* Description        : This function is called to enable DLAG in Hw.        */
/*                                                                           */
/* Input(s)           : u1DlagStatus - DLAG status.                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsLaHwDlagStatus (UINT1 u1DlagStatus)
{

    UNUSED_PARAM (u1DlagStatus);
    return FNP_SUCCESS;
}

#ifdef L2RED_WANTED
/*****************************************************************************/
/* Function Name      : FsLaHwCleanAndDeleteAggregator.                      */
/*                                                                           */
/* Description        : This function is called when the last port attached  */
/*                      to the aggregator goes down. This function will be   */
/*                      called after LaHwRemoveLinkFromAggGroup() for the    */
/*                      last port in the aggregator.                         */
/*                                                                           */
/* Input(s)           : Aggregator Index.                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsLaHwCleanAndDeleteAggregator (UINT2 u2HwAggIndex)
{
    UNUSED_PARAM (u2HwAggIndex);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsLaGetNextAggregator.                               */
/*                                                                           */
/* Description        : This function gets the aggregator that is having the */
/*                      hw agg id greater than that of the given hwagg id.   */
/*                                                                           */
/* Input(s)           : i4HwAggIndex - Aggregator Index used by hw.          */
/*                      pHwInfo - Information about the next aggregator.     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsLaGetNextAggregator (INT4 i4HwAggIndex, tLaHwInfo * pHwInfo)
{
    UNUSED_PARAM (i4HwAggIndex);
    UNUSED_PARAM (pHwInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsLaRedHwInit                                        */
/*                                                                           */
/* Description        : This function is initilizes the datastructures       */
/*                      aLaHwAggEntry                                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : aLaHwAggEntry                                        */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsLaRedHwInit (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsLaRedHwNpDeleteAggregator.                         */
/*                                                                           */
/* Description        : This function is called to delete the NP information */
/*                      for an aggregator.                                   */
/*                                                                           */
/* Input(s)           : Aggregator Index.                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsLaRedHwNpDeleteAggregator (UINT2 u2AggIndex)
{
    UNUSED_PARAM (u2AggIndex);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsLaRedHwUpdateDBForAggregator                       */
/*                                                                           */
/* Description        : This function is used to synchornize the NPAPI       */
/*                      datastructures for an aggregator during switchover   */
/*                      from Standby to active.                              */
/*                                                                           */
/* Input(s)           : u2AggIndex - Aggreagator index                       */
/*                      ConfigPorts - Ports configured  for this aggregator  */
/*                      ActivePorts - Attached ports to this aggregator      */
/*                      u1SelectionPolicy - Selection policy                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : aLaHwAggEntry,aPortToAggIdMap                        */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsLaRedHwUpdateDBForAggregator (UINT2 u2AggIndex,
                                tPortList ConfigPorts,
                                tPortList ActivePorts, UINT1 u1SelectionPolicy)
{
    UNUSED_PARAM (u2AggIndex);
    UNUSED_PARAM (ConfigPorts);
    UNUSED_PARAM (ActivePorts);
    UNUSED_PARAM (u1SelectionPolicy);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsLaRedHwNpUpdateDlfMcIpmcPort                       */
/*                                                                           */
/* Description        : This function is used to synchornize the DLF,MC,     */
/*                      IPMC port in NPAPI datastructures for an aggregator  */
/*                                                                           */
/* Input(s)           : u2AggIndex - Aggreagator index                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : aLaHwAggEntry                                        */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsLaRedHwNpUpdateDlfMcIpmcPort (UINT2 u2AggIndex)
{
    UNUSED_PARAM (u2AggIndex);
    return FNP_SUCCESS;
}
#endif
/* end of file */
