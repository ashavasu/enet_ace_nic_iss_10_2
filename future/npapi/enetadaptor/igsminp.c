/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igsminp.c,v 1.4 2010/05/13 11:53:38 prabuc Exp $
 *
 * Description: All prototypes for Multiple Instance Network Processor 
 *              API functions done here
 *******************************************************************/

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "snp.h"
#include "npapi.h"
#include "npigsmi.h"


#include "adap_types.h"
#include "adap_logger.h"
#include "EnetHal_L3_Api.h"

/*****************************************************************************/
/* Function Name      : FsMiIgsHwEnableIgmpSnooping                          */
/*                                                                           */
/* Description        : This function enables IGMP snooping feature in the   */
/*                      hardware for a given instance. This creates and      */
/*                      installs filter needed for snooping feature          */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiIgsHwEnableIgmpSnooping (UINT4 u4Instance)
{
    UNUSED_PARAM (u4Instance);
#ifdef USER_HW_API
	 EnetHal_FsMiIgsHwEnableIgmpSnooping (u4Instance);
#endif
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiIgsHwDisableIpIgmpSnooping                       */
/*                                                                           */
/* Description        : This function disables IGMP snooping feature in the  */
/*                      hardware for given instance. This removes & destroys */
/*                      filter needed for snooping feature                   */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiIgsHwDisableIgmpSnooping (UINT4 u4Instance)
{
    UNUSED_PARAM (u4Instance);
#ifdef USER_HW_API
	 EnetHal_FsMiIgsHwDisableIgmpSnooping (u4Instance);
#endif
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiIgsHwSparseMode                                  */
/*                                                                           */
/* Description        : This function enables/disables IGMP sparse mode in   */
/*                      the hardware.                                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      u1SparseStatus - Sparse Mode Status                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiIgsHwSparseMode (UINT4 u4ContextId, UINT1 u1SparseStatus)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1SparseStatus);
	printf("FsMiIgsHwSparseMode\r\n");

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiIgsHwEnhancedMode                                */
/*                                                                           */
/* Description        : This function enables IGMP Enhanced mode in the      */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      u1EnhStatus - Enhanced Mode Status                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiIgsHwEnhancedMode (UINT4 u4ContextId, UINT1 u1EnhStatus)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1EnhStatus);
	printf("FsMiIgsHwEnhancedMode\r\n");

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiIgsHwPortRateLimit                               */
/*                                                                           */
/* Description        : This function configures IGMP Rate Limit for a       */
/*                      downstream interface in hardware. In default mode,   */
/*                      the downstream interface corresponds to only port and*/
/*                      inner-vlan will be 0. When enhanced mode is       */
/*                      enabled, downstream interface corresponds to port and*/
/*                      inner-vlan.In this mode inner-vlan may be within the */
/*                      valid vlan range or 0.When rate limit is          */
/*                      configured for an interface, this API will be invoked*/
/*                      with action as .ADD. and u4Rate as the required rate.*/
/*                      When rate limit is configured as zero (rate limit all*/
/*                      packets on this interface) this API will be invoked  */
/*                      with action as .ADD. and u4Rate as zero. When rate   */
/*                      limit is configured to default value ( no rate       */
/*                      limiting to be applied on this interface) this API   */
/*                      will be invoked with action as .REMOVE. and the      */
/*                      u4Rate can be ignored.                               */
/*                                                                           */
/* Input(s)           : UINT4 u4Instance  - Instance Id                      */
/*                                                                           */
/*                    : Struct tIgsHwRateLmt - It contains                   */
/*                                                                           */
/*                      UINT4 u4Port            :  Port Number               */
/*                      tVlanId InnerVlanId     :  Inner-Vlan                */
/*                      UINT4 u4Rate            :  No of packets per second  */
/*                                                                           */
/*                    : UINT1 u1Action - Type of action to be performed.     */
/*                                        ( Add/Remove Rate-limit for port ) */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiIgsHwPortRateLimit (UINT4 u4Instance, tIgsHwRateLmt * pIgsHwRateLmt,
                        UINT1 u1Action)
{
    UNUSED_PARAM (u4Instance);
    UNUSED_PARAM (pIgsHwRateLmt);
    UNUSED_PARAM (u1Action);

	printf("FsMiIgsHwPortRateLimit\r\n");

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiNpUpdateIpmcFwdEntries                           */
/*                                                                           */
/* Description        : This function Updates IP Multicast forwarding entries*/
/*                      in the hardware for a given instance based on the    */
/*                      EventType.                                           */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiNpUpdateIpmcFwdEntries (UINT4 u4Instance, tSnoopVlanId VlanId,
                            UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                            tPortList PortList, tPortList UntagPortList,
                            UINT1 u1EventType)
{
    UNUSED_PARAM (u4Instance);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4GrpAddr);
    UNUSED_PARAM (u4SrcAddr);
    UNUSED_PARAM (PortList);
    UNUSED_PARAM (UntagPortList);
    UNUSED_PARAM (u1EventType);
	printf("FsMiNpUpdateIpmcFwdEntries\r\n");

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiNpGetFwdEntryHitBitStatus                        */
/*                                                                           */
/* Description        : This function gets how many times this forwarding    */
/*                      entry for a given instance is used for sending data  */
/*                      traffic                                              */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                      VlanId - vlan id                                     */
/*                      u4GrpAddr - Group Address                            */
/*                      u4SrcAddr - Source Address                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiNpGetFwdEntryHitBitStatus (UINT4 u4Instance, tSnoopVlanId VlanId,
                               UINT4 u4GrpAddr, UINT4 u4SrcAddr)
{
    UNUSED_PARAM (u4Instance);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4GrpAddr);
    UNUSED_PARAM (u4SrcAddr);
	printf("FsMiNpGetFwdEntryHitBitStatus\r\n");

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiIgsHwAddRtrPort                                      */
/*                                                                           */
/* Description        : This function adds router Port to router Portlsit    */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                      VlanId - Vlan ID to which the MAC address should be  */
/*                               associated. This will be zero if no VLAN    */
/*                      u2Port - Port number to be added                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiIgsHwAddRtrPort (UINT4 u4Instance, tSnoopVlanId VlanId, UINT2 u2Port)
{
    UNUSED_PARAM (u4Instance);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2Port);
	printf("FsMiIgsHwAddRtrPort\r\n");

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiIgsHwDelRtrPort                                      */
/*                                                                           */
/* Description        : This function deletes Port from router Portlsit      */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                      VlanId - Vlan ID to which the MAC address should be  */
/*                               associated. This will be zero if no VLAN    */
/*                      u2Port - Port number to be deleted                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiIgsHwDelRtrPort (UINT4 u4Instance, tSnoopVlanId VlanId, UINT2 u2Port)
{
    UNUSED_PARAM (u4Instance);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2Port);
	printf("FsMiIgsHwDelRtrPort\r\n");

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiIgsHwEnableIpIgmpSnooping                          */
/*                                                                           */
/* Description        : This function is called when IP  IGMP is enabled for */
/*                      a given instance                                     */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiIgsHwEnableIpIgmpSnooping (UINT4 u4Instance)
{
    UNUSED_PARAM (u4Instance);
#ifdef USER_HW_API
	 EnetHal_FsMiIgsHwEnableIpIgmpSnooping(u4Instance);
#endif
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiIgsHwDisableIpIgmpSnooping                       */
/*                                                                           */
/* Description        : This function is called when IP IGMP is disabled for */
/*                      a given instance                                    */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiIgsHwDisableIpIgmpSnooping (UINT4 u4Instance)
{
    UNUSED_PARAM (u4Instance);
#ifdef USER_HW_API
	 EnetHal_FsMiIgsHwDisableIpIgmpSnooping(u4Instance);
#endif
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiIgsHwUpdateIpmcEntry                             */
/*                                                                           */
/* Description        : This function Updates IP Multicast forwarding entries*/
/*                      in the hardware for a given instance based on the    */
/*                      EventType.                                           */
/*                                                                           */
/* Input(s)           : u4Instance      - Insatnce Id                        */
/*                      pIgsHwIpFwdInfo - IP Fwd information                 */
/*                      u1Action        - SNOOP_HW_CREATE_ENTRY /            */
/*                                        SNOOP_HW_DELETE_ENTRY /            */
/*                                        SNOOP_HW_ADD_PORT /                */
/*                                        SNOOP_HW_DEL_PORT /                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiIgsHwUpdateIpmcEntry (UINT4 u4Instance, tIgsHwIpFwdInfo * pIgsHwIpFwdInfo,
                          UINT1 u1Action)
{
    UNUSED_PARAM (u4Instance);
    UNUSED_PARAM (pIgsHwIpFwdInfo);
    UNUSED_PARAM (u1Action);
#ifdef USER_HW_API
	//tEnetHal_IgsHwIpFwdInfo IgsHwIpFwdInfo;
//	IgsHwIpFwdInfo.InnerVlanId = pIgsHwIpFwdInfo->InnerVlanId;
//	IgsHwIpFwdInfo.OuterVlanId = pIgsHwIpFwdInfo->OuterVlanId;
//	IgsHwIpFwdInfo.u4GrpAddr = pIgsHwIpFwdInfo->u4GrpAddr;
//	IgsHwIpFwdInfo.u4SrcAddr = pIgsHwIpFwdInfo->u4SrcAddr;
//	memcpy(&IgsHwIpFwdInfo.pPortList, pIgsHwIpFwdInfo->PortList, sizeof(pIgsHwIpFwdInfo->PortList));
//	memcpy(&IgsHwIpFwdInfo.pUntagPortList, pIgsHwIpFwdInfo->UntagPortList, sizeof(pIgsHwIpFwdInfo->UntagPortList));
//	EnetHal_FsMiIgsHwUpdateIpmcEntry (u4Instance, &IgsHwIpFwdInfo, u1Action);
#endif
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiIgsHwGetIpFwdEntryHitBitStatus                   */
/*                                                                           */
/* Description        : This function is used to check whether the forwarding*/
/*                      entry for a given instance is used for sending data  */
/*                      traffic                                              */
/*                                                                           */
/* Input(s)           : u4Instance      - Insatnce Id                        */
/*                      pIgsHwIpFwdInfo - IP FWD Information                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiIgsHwGetIpFwdEntryHitBitStatus (UINT4 u4Instance,
                                    tIgsHwIpFwdInfo * pIgsHwIpFwdInfo)
{
    UNUSED_PARAM (u4Instance);
    UNUSED_PARAM (pIgsHwIpFwdInfo);
	printf("FsMiIgsHwGetIpFwdEntryHitBitStatus\r\n");

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiIgsHwClearIpmcFwdEntries                     */
/*                                                                           */
/*    Description         : This function deletes all the multicast          */
/*                          entries for a context form the hardware. (IPMC   */
/*                          table).                                          */
/*                                                                           */
/*    Input(s)            : u4Instance - Insatnce Id                         */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*****************************************************************************/
VOID
FsMiIgsHwClearIpmcFwdEntries (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
	printf("FsMiIgsHwClearIpmcFwdEntries\r\n");

    return;
}
