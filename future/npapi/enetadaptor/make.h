#
# $Id
# Description: Auxillary file for NP makefile.
#

# Top level make include files
# ----------------------------

include ../../LR/make.h
include ../../LR/make.rule

# Compilation switches
# --------------------

NPAPI_COMPILATION_SWITCHES =
ifeq (yes,$(MEA_ENV_S1_K7))	
NPAPI_COMPILATION_SWITCHES += -DMEA_ENV_S1_K7
endif

ifeq (yes,$(MEA_OS_EPC_ZYNQ7045))	
NPAPI_COMPILATION_SWITCHES += -MEA_OS_EPC_ZYNQ7045
endif

NPAPI_FINAL_COMPILATION_SWITCHES = ${NPAPI_COMPILATION_SWITCHES}   \
                                   ${GENERAL_COMPILATION_SWITCHES} \
                                   ${SYSTEM_COMPILATION_SWITCHES}

# Library archive utility
# -----------------------

AR            = ar
ARFLAGS       = rsv


# Directories
# -----------

NPAPI_BASE_DIR    = ${BASE_DIR}/npapi/enetadaptor
NPAPI_INC_DIR     = ${NPAPI_BASE_DIR}
NPAPI_SRC_DIR     = ${NPAPI_BASE_DIR}
NPAPI_OBJ_DIR     = ${NPAPI_BASE_DIR}
NPAPI_LIB_DIR     = ${NPAPI_BASE_DIR}
RMON_INC_DIR      = ${BASE_DIR}/rmon/inc
CFA_INC_DIR       = $(BASE_DIR)/cfa2/inc
ISS_COMMON_SYS_DIR   = $(BASE_DIR)/ISS/common/system/inc
ISS_ENET_DIR   		 = $(BASE_DIR)/ISS/enetadaptor/inc
VLAN_DIR   			 = $(BASE_DIR)/vlangarp/vlan/inc

COMN_NPAPI_INC_DIR  = ${NPAPI_INCLUDE_DIR}

# Include files
# -------------

NPAPI_INCLUDE_FILES = $(NPAPI_INC_DIR)
                
NPAPI_FINAL_INCLUDE_FILES = ${NPAPI_INCLUDE_FILES}


# Include directories
# -------------------

NPAPI_INCLUDE_DIRS = -I${NPAPI_INC_DIR} -I$(BASE_DIR)/inc -I$(RMON_INC_DIR) -I$(CFA_INC_DIR) -I$(ISS_COMMON_SYS_DIR) -I$(ISS_ENET_DIR) -I$(VLAN_DIR) -I${BASE_DIR}/vlangarp/garp/inc

NPAPI_FINAL_INCLUDE_DIRS = ${NPAPI_INCLUDE_DIRS} \
                           ${COMMON_INCLUDE_DIRS}

# Project dependencies
# --------------------

NPAPI_DEPENDENCIES = $(COMMON_DEPENDENCIES)       \
                     $(NPAPI_FINAL_INCLUDE_FILES) \
                     $(NPAPI_BASE_DIR)/Makefile   \
                     $(NPAPI_BASE_DIR)/make.h

# -----------------------------  END OF FILE  -------------------------------
