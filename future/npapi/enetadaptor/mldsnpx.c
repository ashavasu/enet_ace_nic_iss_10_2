/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldsnpx.c,v 1.2 2007/02/01 14:59:31 iss Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/
#ifndef _MLDSNPX_C_
#define _MLDSNPX_C_

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "snp.h"
#include "npapi.h"
#include "npmlds.h"

/*****************************************************************************/
/* Function Name      : FsMldsMbsmHwEnableMldSnooping                        */
/*                                                                           */
/* Description        : This function enables MLD snooping feature in the    */
/*                      hardware. Filters are added for receiving MLD        */
/*                      L2MC table is initialized for building MAC based     */
/*                      Multicast data forwarding                            */
/*                                                                           */
/* Input(s)           : pSlotInfo - Pointer to Slot information structure    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMldsMbsmHwEnableMldSnooping (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMldsMbsmHwEnableIpMldSnooping                      */
/*                                                                           */
/* Description        : This function enables MLD snooping feature in the    */
/*                      hardware. Filters are added for receiving MLD        */
/*                      IPMC table is initialized for building IP based      */
/*                      Multicast data forwarding                            */
/*                                                                           */
/* Input(s)           : pSlotInfo - Pointer to Slot information structure    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMldsMbsmHwEnableIpMldSnooping (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

#endif
