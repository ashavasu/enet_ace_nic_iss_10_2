/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: npbrg.h,v 1.3 2007/02/01 14:59:31 iss Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 * 
 *
 *******************************************************************/
#ifndef _NPBRG_H
#define _NPBRG_H

#include "brgnp.h"

#endif /* _NPBRG_H */
