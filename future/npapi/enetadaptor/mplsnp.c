/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mplsnp.c,v 1.18 2015/11/04 11:26:04 siva Exp $
 *
 * Description: This file contains function implementations of MPLS FS NP-API.
 ****************************************************************************/
#ifdef MPLS_WANTED
#ifndef _MPLSNP_C_
#define _MPLSNP_C_

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "npapi.h"
#include "cfanp.h"
#include "npcfa.h"
#include "la.h"
#include "npla.h"
#include "fsvlan.h"
#include "mpls.h"
#include "mplsnp.h"

/*****************************************************************************/
/* Function Name      : FsMplsHwEnableMpls                                   */
/*                                                                           */
/* Description        : This function enables MPLS feature in the            */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMplsHwEnableMpls (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsHwDisableMpls                                  */
/*                                                                           */
/* Description        : This function disables MPLS  feature in the          */
/*                      hardware units.                                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMplsHwDisableMpls (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsHwGetPwCtrlChnlCapabilities                    */
/*                                                                           */
/* Description        : This function gets the pseudowire control channel    */
/*                      capabilities supported by hardware.                  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1PwCCTypeCapabs: bitmap that contains the values   */
/*                      bit 0: PW-ACH                                        */
/*                      bit 1: Router Alert Label                            */
/*                      bit 2: TTL Expiry (PW label with TTL=1)              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsHwGetPwCtrlChnlCapabilities (UINT1 *pu1PwCCTypeCapabs)
{
    UNUSED_PARAM (pu1PwCCTypeCapabs);
    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : FsMplsHwCreatePwVc                                   */
/*                                                                           */
/* Description        : This function configures a PwVc(virtual leased line) */
/*                      at the hardware.                                     */
/*                      Note: This function catters only for VPWS LERs.      */
/*                                                                           */
/* Input(s)           : u4VpnId       : VPN instance identifier              */
/*                                                                           */
/*                      tMplsHwVcTnlInfo : Vll info for PW initiation        */
/*                                                                           */
/*                                                                           */
/*                      u4Action      : ACTION to be performed on this       */
/*                                      on the labels supplied               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMplsHwCreatePwVc (UINT4 u4VpnId, tMplsHwVcTnlInfo * pMplsHwVcInfo,
                    UINT4 u4Action)
{
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (pMplsHwVcInfo);
    UNUSED_PARAM (u4Action);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsHwGetPwVc                                 */
/*                                                                           */
/* Description        : This function gives the next pw entry                */
/* Input(s)           : MplsHwVllInfo   : Vll info                           */
/*                                                                           */
/* Output(s)          : MplsHwVllInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMplsHwGetPwVc (tMplsHwVcTnlInfo * pMplsHwVcInfo,
                 tMplsHwVcTnlInfo * pNextMplsHwVcInfo)
{
    UNUSED_PARAM (pMplsHwVcInfo);
    UNUSED_PARAM (pNextMplsHwVcInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsHwTraversePwVc                                 */
/*                                                                           */
/* Description        : This function gives the next pw entry                */
/* Input(s)           : MplsHwVllInfo   : Vll info                           */
/*                                                                           */
/* Output(s)          : MplsHwVllInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMplsHwTraversePwVc (tMplsHwVcTnlInfo * pMplsHwVcInfo,
                      tMplsHwVcTnlInfo * pNextMplsHwVcInfo)
{
    UNUSED_PARAM (pMplsHwVcInfo);
    UNUSED_PARAM (pNextMplsHwVcInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsHwdDeletePwVc                                  */
/*                                                                           */
/* Description        : This function Deletes the VC(Virtual Circuit).       */
/*                      This is for VPWS so delete the VPN instance itself   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMplsHwDeletePwVc (UINT4 u4VpnId, tMplsHwVcTnlInfo * pMplsHwDelVcInfo,
                    UINT4 u4Action)
{

    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (u4Action);
    UNUSED_PARAM (pMplsHwDelVcInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsHwCreateILM                                    */
/*                                                                           */
/* Description        : This function creates ILM database in the H/W.       */
/*                      This function addresses the LSR configurations,both  */
/*                      for L2/L3 VPN scenario.                              */
/*                                                                           */
/* Input(s)           : tMplsHwIlmInfo: VPN instance identifier              */
/*                                                                           */
/*                      MplsHwIlmInfo : ILM info                             */
/*                                                                           */
/*                      u4Action      : ACTION to be performed on this       */
/*                                      on the labels supplied.              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMplsHwCreateILM (tMplsHwIlmInfo * pMplsHwIlmInfo, UINT4 u4Action)
{
    UNUSED_PARAM (pMplsHwIlmInfo);
    UNUSED_PARAM (u4Action);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsHwGetILM                                  */
/*                                                                           */
/* Description        : This function gives the Next entry of the            */
/*                    : of incoming entry                                    */
/*                                                                           */
/* Input(s)           : pMplsHwIlmInfo: ILM info                             */
/*                                                                           */
/* Output(s)          : pNextMplsHwIlmInfo: ILM info                         */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMplsHwGetILM (tMplsHwIlmInfo * pMplsHwIlmInfo,
                tMplsHwIlmInfo * pNextMplsHwIlmInfo)
{
    UNUSED_PARAM (pMplsHwIlmInfo);
    UNUSED_PARAM (pNextMplsHwIlmInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsHwTraverseILM                                  */
/*                                                                           */
/* Description        : This function gives the Next entry of the            */
/*                    : of incoming entry                                    */
/*                                                                           */
/* Input(s)           : pMplsHwIlmInfo: ILM info                             */
/*                                                                           */
/* Output(s)          : pNextMplsHwIlmInfo: ILM info                         */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMplsHwTraverseILM (tMplsHwIlmInfo * pMplsHwIlmInfo,
                     tMplsHwIlmInfo * pNextMplsHwIlmInfo)
{
    UNUSED_PARAM (pMplsHwIlmInfo);
    UNUSED_PARAM (pNextMplsHwIlmInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsHwDeleteILM                                    */
/*                                                                           */
/* Description        : This function deletes ILM database in the H/W.       */
/*                                                                           */
/* Input(s)           : tMplsHwIlmInfo : ILM Information                     */
/*                                                                           */
/*                      u4Action      : ACTION to be performed.              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMplsHwDeleteILM (tMplsHwIlmInfo * pMplsHwDelIlmParams, UINT4 u4Action)
{
    UNUSED_PARAM (pMplsHwDelIlmParams);
    UNUSED_PARAM (u4Action);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsHwCreateL3FTN                                  */
/*                                                                           */
/* Description        : This function configures a L3 FEC to NHLFE           */
/*                      at the hardware.                                     */
/*                                                                           */
/* Input(s)           : u4VpnId       : VPN instance identifier              */
/*                                                                           */
/*                      tMplsHwL3FTNInfo:Info for L3 Mpls Tnl Initiatiion    */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMplsHwCreateL3FTN (UINT4 u4VpnId, tMplsHwL3FTNInfo * pMplsHwL3FTNInfo)
{
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (pMplsHwL3FTNInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsHwTraverseL3FTN                                */
/*                                                                           */
/* Description        : This traverse the FTN table and give the next entry  */
/*                      of incoming entry.                                   */
/*                                                                           */
/* Input(s)           : tMplsHwL3FTNInfo                                     */
/*                                                                           */
/* Output(s)          : tNextMplsHwL3FTNInfo                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMplsHwTraverseL3FTN (tMplsHwL3FTNInfo * pMplsHwL3FTNInfo,
                       tMplsHwL3FTNInfo * pNextMplsHwL3FTNInfo)
{
    UNUSED_PARAM (pMplsHwL3FTNInfo);
    UNUSED_PARAM (pNextMplsHwL3FTNInfo);
    return FNP_SUCCESS;
}

INT4
FsMplsHwGetL3FTN (UINT4 u4VpnId, UINT4 u4IfIndex,
                  tMplsHwL3FTNInfo * pMplsHwL3FTNInfo)
{
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pMplsHwL3FTNInfo);
    return FNP_SUCCESS;
}

#ifdef NP_BACKWD_COMPATIBILITY
/*****************************************************************************/
/* Function Name      : FsMplsHwDeleteL3FTN                                  */
/*                                                                           */
/* Description        : This function deletes a L3 FEC to NHLFE Entry        */
/*                      at the hardware.                                     */
/*                                                                           */
/* Input(s)           : u4L3EgrIntf                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsHwDeleteL3FTN (UINT4 u4L3EgrIntf)
{
    UNUSED_PARAM (u4L3EgrIntf);

    return FNP_SUCCESS;
}
#else
/*****************************************************************************/
/* Function Name      : FsMplsHwDeleteL3FTN                                  */
/*                                                                           */
/* Description        : This function deletes a L3 FEC to NHLFE Entry        */
/*                      at the hardware.                                     */
/*                                                                           */
/* Input(s)           : u4VpnId - VRF Identifier                             */
/*                      pMplsHwL3FTNInfo - pointer to the structure          */
/*                      containing the tunnel and next hop information.      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsHwDeleteL3FTN (UINT4 u4VpnId, tMplsHwL3FTNInfo * pMplsHwL3FTNInfo)
{
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (pMplsHwL3FTNInfo);

    return FNP_SUCCESS;
}
#endif
/*****************************************************************************/
/* Function Name      : FsMplsWpHwDeleteL3FTN                                */
/*                                                                           */
/* Description        : This function deletes a L3 FEC to NHLFE Entry        */
/*                      at the hardware based on NP Backward compatability   */
/*                                                                           */
/* Input(s)           : u4VpnId - VRF Identifier                             */
/*                      pMplsHwL3FTNInfo - pointer to the structure          */
/*                      containing the tunnel and next hop information.      */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMplsWpHwDeleteL3FTN (UINT4 u4VpnId, tMplsHwL3FTNInfo * pMplsHwL3FTNInfo)
{
    INT4                i4RetVal = FNP_FAILURE;
#ifdef NP_BACKWD_COMPATIBILITY
    i4RetVal = FsMplsHwDeleteL3FTN (pMplsHwL3FTNInfo->u4EgrL3Intf);
#else
    i4RetVal = FsMplsHwDeleteL3FTN (u4VpnId, pMplsHwL3FTNInfo);
#endif
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : FsMplsHwEnableMplsIf                                 */
/*                                                                           */
/* Description        : This function Enables the MPLS interface with the    */
/*                      interface related params passed                      */
/*                                                                           */
/* Input(s)           : u4Intf - Mpls Interface Index                        */
/*                      pMplsIntfParamsSet - MPLS Intf Params                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsHwEnableMplsIf (UINT4 u4Intf, tMplsIntfParamsSet * pMplsIntfParamsSet)
{
    UNUSED_PARAM (u4Intf);
    UNUSED_PARAM (pMplsIntfParamsSet);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsHwDisableMplsIf                                */
/*                                                                           */
/* Description        : This function Disables MPLS on the interface         */
/*                                                                           */
/* Input(s)           : u4Intf - Mpls Interface Index                        */
/*                      pMplsIntfParamsSet - MPLS Intf Params                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsHwDisableMplsIf (UINT4 u4Intf)
{
    UNUSED_PARAM (u4Intf);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsHwCreateVplsVpn                                */
/*                                                                           */
/* Description        : Instantiates a VPLS VPN at the H/W                   */
/* Input(s)           : u4VplsInstance - Forwarding Instance Id              */
/*                      u4VpnId        - Vpn Id                              */
/*                                                                           */
/*                      pMplsHwVplsVpnInfo - Vpls info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsHwCreateVplsVpn (UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo
                       * pMplsHwVplsVpnInfo)
{
    UNUSED_PARAM (u4VplsInstance);
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (pMplsHwVplsVpnInfo);

    return FNP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : FsMplsHwDeleteVplsVpn                                */
/*                                                                           */
/* Description        : Deletes a VPLS VPN at the H/W                        */
/* Input(s)           : u4VplsInstance - Forwarding Instance Id              */
/*                      u4VpnId        - Vpn Id                              */
/*                                                                           */
/*                      pMplsHwVplsVpnInfo - Vpls info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsHwDeleteVplsVpn (UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo
                       * pMplsHwVplsVpnInfo)
{

    UNUSED_PARAM (u4VplsInstance);
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (pMplsHwVplsVpnInfo);

    return FNP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : FsMplsHwVplsAddPwVc                                  */
/*                                                                           */
/* Description        : Associates PWVC to a VPLS VPN at the H/W             */
/* Input(s)           : u4VplsInstance - Forwarding Instance Id              */
/*                      u4VpnId        - Vpn Id                              */
/*                      pMplsHwVplsVpnInfo - Vpls info                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsHwVplsAddPwVc (UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo
                     * pMplsHwVplsVcInfo, UINT4 u4Action)
{

    UNUSED_PARAM (u4VplsInstance);
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (pMplsHwVplsVcInfo);
    UNUSED_PARAM (u4Action);

    return FNP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : FsMplsHwVplsDeletePwVc                               */
/*                                                                           */
/* Description        : Dis-associates PWVC from a VPLS VPN at the H/W       */
/* Input(s)           : u4VplsInstance - Forwarding Instance Id              */
/*                      u4VpnId        - Vpn Id                              */
/*                      pMplsHwVplsVpnInfo - Vpls info                       */
/*                      u4Action       - Action flag                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsHwVplsDeletePwVc (UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo
                        * pMplsHwVplsVcInfo, UINT4 u4Action)
{

    UNUSED_PARAM (u4VplsInstance);
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (pMplsHwVplsVcInfo);
    UNUSED_PARAM (u4Action);

    return FNP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : FsMplsHwVplsFlushMac                                 */
/*                                                                           */
/* Description        : Removes a learnt MAC address on a VPLS VPN instance  */
/* Input(s)           : u4VplsInstance - Forwarding Instance Id              */
/*                      u4VpnId        - Vpn Id                              */
/*                      pMplsHwVplsVpnInfo - Vpls info havind MAC address to */
/*                      flushed                                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsHwVplsFlushMac (UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo
                      * pMplsHwVplsVpnInfo)
{

    UNUSED_PARAM (u4VplsInstance);
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (pMplsHwVplsVpnInfo);

    return FNP_SUCCESS;

}
#ifdef HVPLS_WANTED
/*****************************************************************************/
/* Function Name      : FsMplsRegisterFwdAlarm                               */
/*                                                                           */
/* Description        : Register alarm values for FDB                        */
/* Input(s)           : u4VpnId -  Vpn Id                                    */
/*                      pMplsHwVplsVpnInfo -Vpls info having water           */
/*                      Mark value                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4 FsMplsRegisterFwdAlarm (UINT4 u4VpnId,tMplsHwVplsInfo *pMplsHwVplsVpnInfo)
{
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (pMplsHwVplsVpnInfo);
    return FNP_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : FsMplsDeRegisterFwdAlarm                               */
/*                                                                           */
/* Description        : Register alarm values for FDB                        */
/* Input(s)           : u4VpnId -  Vpn Id                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu4MplsNpGlobalInit                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4 FsMplsDeRegisterFwdAlarm (UINT4 u4VpnId)
{
    UNUSED_PARAM (u4VpnId);
    return FNP_SUCCESS;
}
#endif
/*****************************************************************************/
/* Function Name      : NpMplsCreateMplsInterfaceWr                          */
/*                                                                           */
/* Description        : This function calls a routine NpMplsCreateMplsInterface
                         in the H/W.                                         */
/*                                                                           */
/* Input(s)           : pMplsHwMplsIntInfo : Mpls Create Interface Info      */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/


UINT4
NpMplsCreateMplsInterfaceWr (tMplsHwMplsIntInfo *  pMplsHwMplsIntInfo)
{

   UNUSED_PARAM (pMplsHwMplsIntInfo->u4L3Intf);
   NpMplsCreateMplsInterface (pMplsHwMplsIntInfo->u4CfaIfIndex,  pMplsHwMplsIntInfo->u2VlanId,
                              pMplsHwMplsIntInfo->au1MacAddress);
   return FNP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : NpMplsCreateMplsInterface                            */
/*                                                                           */
/* Description        : This function creates MPLS interface in the H/W.     */
/*                                                                           */
/* Input(s)           : u4CfaIfIndex  : MPLS interface number                */
/*                      u2VlanId      : Underlayer vlan id                   */
/*                      au1MacAddress : L3 Vlan mac address corr to vlan id  */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

UINT4
NpMplsCreateMplsInterface (UINT4 u4CfaIfIndex, UINT2 u2VlanId,
                           UINT1 *au1MacAddress)
{
    UNUSED_PARAM (u4CfaIfIndex);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (au1MacAddress);

    return FNP_SUCCESS;
}

/***********************************************************************/
/*  Function Name             : FsMplsHwAddMplsRoute                   */
/*  Description               : This function adds an MPLS Route       */
/*                              entry to ip unicast route table        */
/*                              of Network Processort(Fast path)       */
/*  Input(s)                  : u4VrId   - The virtual router          */
/*                                         identifier.                 */
/*                              u4IpDestAddr - Destination IP          */
/*                                             Address                 */
/*                              u4IpSubNetMask-Destination             */
/*                                             Subnet Mask             */
/*                              routeEntry    -Unicast Route           */
/*                                             Entry containing        */
/*                                             Gateway IP              */
/*                                             Address.                */
/*                              u4RouteType   -Unicast Route           */
/*                                             Type - static/dy        */
/*  Output(s)                 : None                                   */
/*  Global Variables Referred : None                                   */
/*  Global variables Modified : None                                   */
/*  Exceptions                : None                                   */
/*  Use of Recursion          : None                                   */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE                */
/***********************************************************************/
UINT4
FsMplsHwAddMplsRoute (UINT4 u4VrId, UINT4 u4IpDestAddr, UINT4 u4IpSubNetMask,
                      tFsNpNextHopInfo routeEntry, UINT1 *pbu1TblFull)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4IpDestAddr);
    UNUSED_PARAM (u4IpSubNetMask);
    UNUSED_PARAM (routeEntry);
    UNUSED_PARAM (pbu1TblFull);

    return FNP_SUCCESS;
}
/********************************************************************************/
/*  Function    :  FsMplsHwAddMplsIpv6Route                                     */
/*  Description :  This function is used to add route entries into the FIB table*/  
/*                 associated with the Virtual Router Id (u4VrId). If no route  */
/*                 entry exists for the given Prefix (pu1Ip6Prefix) /           */ 
/*                 Prefix Length (u1PrefixLen) / Next-Hop combination, then an  */ 
/*                 entry should be added in the FIB table. If an entry already  */
/*                 exists, then the interface index and next-hop type           */
/*                 information should be updated.                               */
/*                                                                              */                   
/* input       :  u4VrId       - Virtual Router Identifier.                     */
/*                  pu1Ip6Prefix - Ipv6 Route Prefix                            */
/*                  u1PrefixLen  - Ipv6 Route Prefix Length.                    */
/*                  pu1NextHop   - Next-Hop address.                            */
/*                  u4NHType     - Describes the action to be performed when    */
/*                               this route entry is chosen for forwarding.     */
/*                               The value are:                                 */
/*                                 NH_DIRECT (1) - Destination Address belongs  */
/*                                                 to Directly Attached Subnet. */
/*                                 NH_REMOTE (2) - Destination Address belongs  */
/*                                                 to different subnet and packet*/
/*                                                 needs to be forwarded to the */
/*                                                 next-hop.                    */
/*                                 NH_SENDTO_CP (3) - Send the packet to the    */
/*                                                    control plane.            */
/*                                 NH_DISCARD (4)- Drop the packet.             */
/*                                 NH_TUNNEL (5) - The next-hop address is the  */
/*                                                 address of the next-hop in the*/
/*                                                 tunnel. The tunnel exit node */
/*                                                 address, to which the        */
/*                                                 encapsulated packets are sent,*/
/*                                                 is either derived from this  */
/*                                                 next-hop or from the tunnel  */
/*                                                 interface structure.         */       
/*                 pFsNpIntInfo  - Interface information through next hop is reachable.*/
/*   Output      :  None.                                                       */
/*   Returns     :  FNP_SUCCESS or FNP_FAILURE                                  */
/********************************************************************************/
UINT4
FsMplsHwAddMplsIpv6Route(UINT4 u4VrId, UINT1 *pu1Ip6Prefix, UINT1 u1PrefixLen,
                         UINT1 *pu1NextHop, UINT4 u4NHType, tFsNpIntInfo *pIntInfo)
{
    UNUSED_PARAM(u4VrId);
    UNUSED_PARAM(pu1Ip6Prefix);
    UNUSED_PARAM(u1PrefixLen);
    UNUSED_PARAM(pu1NextHop);
    UNUSED_PARAM(u4NHType);
    UNUSED_PARAM(pIntInfo);

    return FNP_SUCCESS;
}
/*************************************************************************************/
/*   Function    :  FsMplsHwDeleteMplsIpv6Route                                      */
/*   Description :  This function is used to delete route entries from the FIB       */
/*                table associated with the Virtual Router Id (u4VrId).              */
/*   Input       :  u4VrId       - Virtual Router Identifier.                        */
/*                pu1Ip6Prefix - Ipv6 Route Prefix                                   */
/*                u1PrefixLen  - Ipv6 Route Prefix Length.                           */
/*                pu1NextHop   - Next-Hop address.                                   */
/*                u4IfIndex    - Interface through with the next-hop is              */
/*                               reachable.                                          */
/*   Output      :  None.                                                            */
/*   Returns     :  FNP_SUCCESS or FNP_FAILURE                                       */
/***************************************************************************************/
UINT4
FsMplsHwDeleteMplsIpv6Route (UINT4 u4VrId, UINT1 *pu1Ip6Prefix, UINT1 u1PrefixLen,
                                   UINT1 *pu1NextHop, UINT4 u4IfIndex, tFsNpRouteInfo *pRouteInfo)
{
    UNUSED_PARAM(u4VrId);
    UNUSED_PARAM(pu1Ip6Prefix);
    UNUSED_PARAM(u1PrefixLen);
    UNUSED_PARAM(pu1NextHop);
    UNUSED_PARAM(u4IfIndex);
    UNUSED_PARAM(pRouteInfo);


    return FNP_SUCCESS;
}

/***********************************************************************/
/*  Function Name             : FsMplsHwGetMplsStats                   */
/*  Description               : This function is used to get the       */
/*                              mpls statistics from the hardware      */
/*                              based on the input params passed       */
/*  Input(s)                  : pInputParams - gives information on    */
/*                                             index to be used to get */
/*                                             the stats               */
/*                              StatType  - what stats to be retrieved */
/*  Output(s)                 : pStatInfo - used for returning the     */
/*                                          retrieved Statistics       */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE                */
/***********************************************************************/
INT4
FsMplsHwGetMplsStats (tMplsInputParams * pInputParams,
                      tStatsType StatsType, tStatsInfo * pStatsInfo)
{
    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (StatsType);
    UNUSED_PARAM (pStatsInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsHwAddVpnAc                                     */
/*                                                                           */
/* Description        : Adds an Attachment circuit to the VPN instance.      */
/* Input(s)           : u4VplsInstance - Forwarding Instance Id              */
/*                      u4VpnId        - Vpn Id                              */
/*                      pMplsHwVplsVpnInfo - AC info                         */
/*                      u4Action       - Action flag                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsHwAddVpnAc (UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo
                  * pMplsHwVplsVcInfo, UINT4 u4Action)
{
    UNUSED_PARAM (u4VplsInstance);
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (pMplsHwVplsVcInfo);
    UNUSED_PARAM (u4Action);
    return FNP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : FsMplsHwGetVpnAc                                     */
/*                                                                           */
/* Description        : Deletes an Attachment circuit from an VPN instance.  */
/* Input(s)           : pMplsHwVplsVpnInfo - AC info                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsHwGetVpnAc (tMplsHwVplsInfo * pMplsHwVplsVcInfo)
{
    UNUSED_PARAM (pMplsHwVplsVcInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsHwDeleteVpnAc                                  */
/*                                                                           */
/* Description        : Deletes an Attachment circuit from an VPN instance.  */
/* Input(s)           : u4VplsInstance - Forwarding Instance Id              */
/*                      u4VpnId        - Vpn Id                              */
/*                      pMplsHwVplsVpnInfo - AC info                         */
/*                      u4Action       - Action flag                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsHwDeleteVpnAc (UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo
                     * pMplsHwVplsVcInfo, UINT4 u4Action)
{
    UNUSED_PARAM (u4VplsInstance);
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (pMplsHwVplsVcInfo);
    UNUSED_PARAM (u4Action);
    return FNP_SUCCESS;

}

/* MPLS_P2MP_LSP_CHANGES - S */
/*****************************************************************************/
/* Function Name      : FsMplsHwP2mpAddILM                                   */
/*                                                                           */
/* Description        : This function creates the ILM database in the H/W    */
/*                      for P2MP tunnels. This function addresses the        */
/*                                                                           */
/*                      head/branch configurations of P2MP destinations.     */
/* Input(s)           : u4P2mpId          - P2MP Identifier                  */
/*                      MplsHwP2mpIlmInfo - ILM info                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsHwP2mpAddILM (UINT4 u4P2mpId, tMplsHwIlmInfo * pMplsHwP2mpIlmInfo)
{
    UNUSED_PARAM (u4P2mpId);
    UNUSED_PARAM (pMplsHwP2mpIlmInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsHwP2mpRemoveILM                                */
/*                                                                           */
/* Description        : This function deletes the ILM database in the H/W    */
/*                      for P2MP tunnels.                                    */
/*                                                                           */
/* Input(s)           : u4P2mpId          - P2MP Identifier                  */
/*                      MplsHwP2mpIlmInfo - ILM info                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsHwP2mpRemoveILM (UINT4 u4P2mpId, tMplsHwIlmInfo * pMplsHwP2mpIlmInfo)
{
    UNUSED_PARAM (u4P2mpId);
    UNUSED_PARAM (pMplsHwP2mpIlmInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsHwP2mpTraverseILM                              */
/*                                                                           */
/* Description        : This function gives the Next entry of the            */
/*                      of incoming entry                                    */
/*                                                                           */
/* Input(s)           : u4P2mpId               - P2MP Identifier             */
/*             pMplsHwP2mpIlmInfo     - ILM info                    */
/*                      pNextMplsHwP2mpIlmInfo - ILM info                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsHwP2mpTraverseILM (UINT4 u4P2mpId, tMplsHwIlmInfo * pMplsHwP2mpIlmInfo,
                         tMplsHwIlmInfo * pNextMplsHwP2mpIlmInfo)
{
    UNUSED_PARAM (u4P2mpId);
    UNUSED_PARAM (pMplsHwP2mpIlmInfo);
    UNUSED_PARAM (pNextMplsHwP2mpIlmInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsHwP2mpAddBud                                   */
/*                                                                           */
/* Description        : This function creates the bud configuration in H/W   */
/*                      for P2MP tunnel                                      */
/*                                                                           */
/* Input(s)           : u4P2mpId          - P2MP Identifier                  */
/*                      u4Action          - Action for bud node              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsHwP2mpAddBud (UINT4 u4P2mpId, UINT4 u4Action)
{
    UNUSED_PARAM (u4P2mpId);
    UNUSED_PARAM (u4Action);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsHwP2mpRemoveBud                                */
/*                                                                           */
/* Description        : This function deletes the bud configuration in H/W   */
/*                      for P2MP tunnel.                                     */
/*                                                                           */
/* Input(s)           : u4P2mpId          - P2MP Identifier                  */
/*                      u4Action          - Action for bud node              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMplsHwP2mpRemoveBud (UINT4 u4P2mpId, UINT4 u4Action)
{
    UNUSED_PARAM (u4P2mpId);
    UNUSED_PARAM (u4Action);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMplsHwModifyPwVc                                   */
/*                                                                           */
/* Description        : This Wrapper function modifies the Pseudowire        */
/*                      information in the hardware                          */
/*                                                                           */
/* Input(s)           : u4VpnId        : VPN hardware identifier             */
/*                      u4OperActVpnId : Oper active VPN ID                  */
/*                      pMplsHwVcInfo  : Information required to modify the  */
/*                                       PW created in the hardware          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMplsHwModifyPwVc (UINT4 u4VpnId, UINT4 u4OperActVpnId, tMplsHwVcTnlInfo
                    * pMplsHwVcInfo)
{
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (u4OperActVpnId);
    UNUSED_PARAM (pMplsHwVcInfo);
    return FNP_SUCCESS;
}

/* MPLS_P2MP_LSP_CHANGES - E */
#ifdef MPLS_L3VPN_WANTED
INT4
FsMplsHwL3vpnIngressMap (tMplsHwL3vpnIgressInfo * pMplsHwL3vpnIgressInfo)
{

    UNUSED_PARAM (pMplsHwL3vpnIgressInfo);
    return FNP_SUCCESS;
}

INT4
FsMplsHwL3vpnEgressMap (tMplsHwL3vpnEgressInfo * pMplsHwL3vpnEgressInfo)
{
    UNUSED_PARAM (pMplsHwL3vpnEgressInfo);
    return FNP_SUCCESS;
}
#endif
#endif
#endif /* MPLS_WANTED */
