/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: npla.h,v 1.4 2007/02/01 14:59:31 iss Exp $
 *
 * Description: Prototypes for Link Aggregation module's NP-API.
 *
 *******************************************************************/

#ifndef _NPLA_H
#define _NPLA_H

#include "lanp.h"

#ifdef MBSM_WANTED
INT4 FsLaMbsmHwRemoveLinkFromOtherUnits PROTO ((UINT2 u2AggIndex,
                                                UINT2 u2PortNumber));
#endif

#endif /* _NPLA_H */
