/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnminpx.c,v 1.17 2015/09/29 10:25:57 siva Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/
#ifdef VLAN_WANTED

#ifndef _VLANNPX_C_
#define _VLANNPX_C_

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "l2iwf.h"
#include "npapi.h"
#include "npvlanmi.h"

/*****************************************************************************/
/* Function Name       : FsMiVlanMbsmHwInit                                  */
/*                                                                           */
/* Description         : This function takes care of initialising the        */
/*                       hardware related parameters.                        */
/*                                                                           */
/* Input(s)            : u4ContextId - Context Identifier                    */
/*                                                                           */
/* Output(s)           : None                                                */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Exceptions or Operating                                                   */
/* System Error Handling    : None.                                          */
/*                                                                           */
/* Use of Recursion        : None.                                           */
/*                                                                           */
/* Returns            : FNP_SUCCESS OR FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwInit (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwSetBrgMode                         */
/*                                                                           */
/*    Description         : This function sets the bridge mode in hardware.  */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4BridgeMode - VLAN_CUSTOMER_BRIDGE_MODE /       */
/*                          VLAN_PROVIDER_BRIDGE_MODE /                      */
/*                          VLAN_PROVIDER_EDGE_BRIDGE_MODE /                 */
/*                          VLAN_PROVIDER_CORE_BRIDGE_MODE                   */
/*                          pSlotInfo - Slot Information                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns                 : FNP_SUCCESS/FNP_FAILURE                      */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetBrgMode (UINT4 u4ContextId, UINT4 u4BridgeMode,
                          tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4BridgeMode);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwSetDefaultVlanId                   */
/*                                                                           */
/*    Description         : This function sets the given VlanId as the       */
/*                          default Vlan Id.                                 */
/*                                                                           */
/*    Input               :  VlanId     - Default Vlan Identfier             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetDefaultVlanId (UINT4 u4ContextId, tVlanId VlanId,
                                tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name       : FsMiVlanMbsmHwAddStaticUcastEntry                   */
/*                                                                           */
/* Description         : This function adds the egress ports  into hardware  */
/*                       Static Unicast table .                              */
/*                                                                           */
/* Input(s)            : u4ContextId         - Context Identifier            */
/*                       u4FdbId             - Fid                           */
/*                       MacAddr             - Mac Address.                  */
/*                       u4RcvPort           - Received Interface Index      */
/*                       pHwAllowedToGoPorts - Pointer to HwPortArray        */
/*                       u1Status            -  Status of entry takes values */
/*                       like VLAN_PERMANENT,VLAN_DELETE_ON_RESET,
 *                       VLAN_DELETE_ON_TIMEOUT                              */
/*                                                                           */
/* Output(s)           : None                                                */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Exceptions or Operating                                                   */
/* System Error Handling    : None.                                          */
/*                                                                           */
/* Use of Recursion        : None.                                           */
/*                                                                           */
/* Returns            : FNP_SUCCESS/FNP_FAILURE                              */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwAddStaticUcastEntry (UINT4 u4ContextId, UINT4 u4FdbId,
                                   tMacAddr MacAddr, UINT4 u4RcvPort,
                                   tHwPortArray * pHwAllowedToGoPorts,
                                   UINT1 u1Status, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4FdbId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4RcvPort);
    UNUSED_PARAM (pHwAllowedToGoPorts);
    UNUSED_PARAM (u1Status);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name       : FsMiVlanMbsmHwSetUcastPort                          */
/*                                                                           */
/* Description         : This function adds the egress port in existing      */
/*                       Unicast entry or if the Unicast entry is not present*/
/*                       it will create it and update it's portlist          */
/*                                                                           */
/* Input(s)            : u4ContextId         - Context Identifier            */
/*                       u4FdbId             - Fid                           */
/*                       MacAddr             - Mac Address.                  */
/*                       u4RcvPort           - Received Interface Index      */
/*                       u2AllowedToGoPort   - Pointer to HwPortArray        */
/*                       u1Status            -  Status of entry takes values */
/*                       like VLAN_PERMANENT,VLAN_DELETE_ON_RESET,
 *                       VLAN_DELETE_ON_TIMEOUT                              */
/*                                                                           */
/* Output(s)           : None                                                */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Exceptions or Operating                                                   */
/* System Error Handling    : None.                                          */
/*                                                                           */
/* Use of Recursion        : None.                                           */
/*                                                                           */
/* Returns            : FNP_SUCCESS/FNP_FAILURE                              */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetUcastPort (UINT4 u4ContextId, UINT4 u4FdbId,
                            tMacAddr MacAddr, UINT4 u4RcvPort,
                            UINT4 u4AllowedToGoPort,
                            UINT1 u1Status, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4FdbId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4RcvPort);
    UNUSED_PARAM (u4AllowedToGoPort);
    UNUSED_PARAM (u1Status);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/* Function Name       : FsMiVlanMbsmHwResetUcastPort                       */
/*                                                                          */
/* Description         : This function removes the egress port in existing  */
/*                       Unicast entry                                      */
/*                                                                          */
/* Input(s)            : u4ContextId         - Context Identifie            */
/*                       u4FdbId             - Fid                          */
/*                       MacAddr             - Mac Address                  */
/*                       u4RcvPort           - Received Interface Index     */
/*                       u2AllowedToGoPort   - Port to be removed           */
/*                                                                          */
/* Output(s)           : None                                               */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None.                                        */
/*                                                                          */
/* Exceptions or Operating                                                  */
/* System Error Handling    : None.                                         */
/*                                                                          */
/* Use of Recursion        : None.                                          */
/*                                                                          */
/* Returns            : FNP_SUCCESS/FNP_FAILURE                             */
/****************************************************************************/
INT4
FsMiVlanMbsmHwResetUcastPort (UINT4 u4ContextId, UINT4 u4FdbId,
                              tMacAddr MacAddr, UINT4 u4RcvPort,
                              UINT4 u4AllowedToGoPort,
                              tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4FdbId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4RcvPort);
    UNUSED_PARAM (u4AllowedToGoPort);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwDelStaticUcastEntry                */
/*                                                                           */
/*    Description         : This function deletes the entry from  hardware   */
/*                          Static Unicast table.                            */
/*                                                                           */
/*    Input(s)            : u4ContextId      - Context Identifier            */
/*                          u4Fid            - Fid                           */
/*                          MacAddr          - Mac Address.                  */
/*                          u4RcvPort        - Received port                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwDelStaticUcastEntry (UINT4 u4ContextId, UINT4 u4Fid,
                                   tMacAddr MacAddr, UINT4 u4RcvPort,
                                   tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4RcvPort);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwAddMcastEntry                      */
/*                                                                           */
/*    Description         : This function adds an entry to the hardware      */
/*                          Multicast table.                                 */
/*                          This function can be called, when the Multicast  */
/*                          entry is already present in the Hardware. In     */
/*                          which case the Old Multicast member ports        */
/*                          must be removed and the new ports ('PortBmp')    */
/*                          must be added in the Hardware.                   */
/*                                                                           */
/*    Input(s)            : u4ContextId   - Context Identifier               */
/*                          VlanId        - VlanId                           */
/*                          MacAddr       - Mac Address.                     */
/*                          pHwMcastPorts - Pointer to HwPortArray           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwAddMcastEntry (UINT4 u4ContextId, tVlanId VlanId,
                             tMacAddr MacAddr, tHwPortArray * pHwMcastPorts,
                             tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (pHwMcastPorts);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsMiVlanMbsmHwSetMcastPort                       */
/*                                                                           */
/*    Description         : This function adds a member port in              */
/*                          existing multicast entry. Application has to     */
/*                          ensure the existence of multicast entry          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId      - VlanId                             */
/*                          MacAddr     - Mac Address.                       */
/*                          u4Port      - Interface Index of member port to  */
/*                                        be added                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetMcastPort (UINT4 u4ContextId, tVlanId VlanId,
                            tMacAddr MacAddr, UINT4 u4Port,
                            tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsMiVlanMbsmHwResetMcastPort                     */
/*                                                                           */
/*    Description         : This function resets a member port from the      */
/*                          existing multicast entry. Application has to     */
/*                          ensure the existence of multicast entry          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId      - VlanId                             */
/*                          MacAddr     - Mac Address.                       */
/*                          u4Port      - Interface Index of member port to  */
/*                                        be removed                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwResetMcastPort (UINT4 u4ContextId, tVlanId VlanId,
                              tMacAddr MacAddr, UINT4 u4Port,
                              tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwAddVlanEntry                       */
/*                                                                           */
/*    Description         : This function adds an entry to the hardware      */
/*                          Vlan table.                                      */
/*                                                                           */
/*    Input(s)            : u4ContextId    - Context Identifier              */
/*                          VlanId         - VlanId                          */
/*                          pHwEgressPorts - Pointer to HwPortArray          */
/*                          pHwUnTagPorts  - Pointer to HwPortArray          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwAddVlanEntry (UINT4 u4ContextId, tVlanId VlanId,
                            tHwPortArray * pHwEgressPorts,
                            tHwPortArray * pHwUnTagPorts,
                            tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pHwEgressPorts);
    UNUSED_PARAM (pHwUnTagPorts);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwSetVlanMemberPort                  */
/*                                                                           */
/*    Description         : This function adds the port as the member of     */
/*                          given Vlan.                                      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId      - Vlan to which the port is going to */
/*                                        be an member                       */
/*                          u4Port      - The port number                    */
/*                          u1IsTagged  - If value is VLAN_TRUE, then port   */
/*                                        is tagged port &                   */
/*                                        If value is VLAN_FALSE, then       */
/*                                        port is an untagged port           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetVlanMemberPort (UINT4 u4ContextId, tVlanId VlanId,
                                 UINT4 u4Port, UINT1 u1IsTagged,
                                 tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u1IsTagged);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwResetVlanMemberPort                */
/*                                                                           */
/*    Description         : This function Reset the port as the member of    */
/*                          given Vlan.                                      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId      - Vlan to which the port is going to */
/*                                        be an member                       */
/*                          u4Port      - The port number                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwResetVlanMemberPort (UINT4 u4ContextId, tVlanId VlanId,
                                   UINT4 u4Port, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwSetPortPvid                        */
/*                                                                           */
/*    Description         : This function sets the given VlanId as PortPvid  */
/*                          for the Port.                                    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u4Port      - The port number                    */
/*                          VlanId      - Vlan to which the port is going to */
/*                                        be an member                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortPvid (UINT4 u4ContextId, UINT4 u4Port, tVlanId VlanId,
                           tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwSetPortAccFrameType                */
/*                                                                           */
/*    Description         : This function sets the Acceptable frame types    */
/*                          parameter for the Given Port.                    */
/*                                                                           */
/*    Input(s)            : u4ContextId    - Context Identifier              */
/*                          u4Port         - The port number                 */
/*                         u1AccFrameType - Acceptable Frame types           */
/*                        parameter value.Frame types are                    */
/*                       VLAN_ADMIT_ALL_FRAMES,                              */
/*                       VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES,                 */
/*                       VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/

INT4
FsMiVlanMbsmHwSetPortAccFrameType (UINT4 u4ContextId, UINT4 u4Port,
                                   UINT1 u1AccFrameType,
                                   tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u1AccFrameType);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwSetPortIngFiltering                */
/*                                                                           */
/*    Description         : This function sets the Ingress Filtering         */
/*                          parameter for the Given Port.                    */
/*                                                                           */
/*    Input(s)            : u4ContextId       - Context Identifier           */
/*                          u4Port            - The port number              */
/*                          u1IngFilterEnable - FNP_TRUE if Ingress          */
/*                                              filtering enabled, otherwise */
/*                                              FNP_FALSE                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortIngFiltering (UINT4 u4ContextId, UINT4 u4Port,
                                   UINT1 u1IngFilterEnable,
                                   tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u1IngFilterEnable);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsMiVlanMbsmHwSetDefUserPriority                 */
/*                                                                           */
/*    Description         : This function set the default user priority to   */
/*                          port                                             */
/*                                                                           */
/*    Input(s)            : u4ContextId   - Context Identifier               */
/*                          u4Port        - The port number                  */
/*                          i4DefPriority - default user priority < 0 - 7 >  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetDefUserPriority (UINT4 u4ContextId, UINT4 u4Port,
                                  INT4 i4DefPriority, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (i4DefPriority);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsMiVlanMbsmHwTraffClassMapInit                  */
/*                                                                           */
/*    Description         : This function sets which cosq a given priority   */
/*                          should fall into in the given unit.              */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u1Priority  - Cosq Priority can take <0-7>       */
/*                          i4CosqValue - Cosq Value can take value from the
 *                          array gau1PriTrfClassMap.                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*                                                                           */
/*****************************************************************************/

INT4
FsMiVlanMbsmHwTraffClassMapInit (UINT4 u4ContextId, UINT1 u1Priority,
                                 INT4 i4CosqValue, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1Priority);
    UNUSED_PARAM (i4CosqValue);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwSetTraffClassMap                   */
/*                                                                           */
/*    Description         : This function set the traffic class for which    */
/*                          user priority of the port should map into        */
/*                                                                           */
/*    Input(s)            : u4ContextId    - Context Identifier              */
/*                          u4Port         - logical port number             */
/*                          i4UserPriority - user priority < 0 - 7 >         */
/*                          i4TraffClass   - traffic class to be mapped 
 *                                            < 0 - 7 >                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/

INT4
FsMiVlanMbsmHwSetTraffClassMap (UINT4 u4ContextId, UINT4 u4Port,
                                INT4 i4UserPriority, INT4 i4TraffClass,
                                tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (i4UserPriority);
    UNUSED_PARAM (i4TraffClass);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwAddStMcastEntry                    */
/*                                                                           */
/*    Description         : This API deletes the static entry from the       */
/*                          hardware multicast table.                        */
/*                                                                           */
/*    Input(s)            : u4ContextId    - Context Identifier              */
/*                          VlanId         - Vlan Identifier                 */
/*                          McastAddr      - Mac Address                     */
/*                          i4RcvPort      - Received port                   */
/*                          pHwMcastPorts  - Pointer to HwPortArray          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwAddStMcastEntry (UINT4 u4ContextId, tVlanId VlanId,
                               tMacAddr McastAddr, INT4 i4RcvPort,
                               tHwPortArray * pHwMcastPorts,
                               tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (McastAddr);
    UNUSED_PARAM (i4RcvPort);
    UNUSED_PARAM (pHwMcastPorts);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwDelStMcastEntry                    */
/*                                                                           */
/*    Description         : This function deletes a static mcast entry from  */
/*                          the hardware Multicast table.                    */
/*                                                                           */
/*    Input(s)            : u4ContextId   - Context Identifier               */
/*                          VlanId        - VlanId                           */
/*                          MacAddr       - Mac Address.                     */
/*                          u4RcvPort     - Receiver port.                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwDelStMcastEntry (UINT4 u4ContextId, tVlanId VlanId,
                               tMacAddr MacAddr, UINT4 u4RcvPort,
                               tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4RcvPort);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwSetPortNumTrafClasses              */
/*                                                                           */
/*    Description         : This function set the number of traffic classes  */
/*                          for the port.                                    */
/*                                                                           */
/*    Input(s)            : u4ContextId     - Context Identifier             */
/*                          u4Port          - logical port number            */
/*                          i4NumTraffClass - number of traffic classes 
 *                                             < 1 - 8 >*/
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortNumTrafClasses (UINT4 u4ContextId, UINT4 u4Port,
                                     INT4 i4NumTraffClass,
                                     tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (i4NumTraffClass);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwSetRegenUserPriority               */
/*                                                                           */
/*    Description         : This function overides the priority information  */
/*                          in the priority regeneration table maintained by */
/*                          each port for the user priority                  */
/*                                                                           */
/*    Input(s)            : u4ContextId     - Context Identifier             */
/*                          u4Port          - logical port number            */
/*                          i4UserPriority  - user priority information      */
/*                                             ranges < 0 - 7 >              */
/*                          i4RegenPriority - regenerated user priority can  */
/*                                            take <0-7>                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/

INT4
FsMiVlanMbsmHwSetRegenUserPriority (UINT4 u4ContextId, UINT4 u4Port,
                                    INT4 i4UserPriority,
                                    INT4 i4RegenPriority,
                                    tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (i4UserPriority);
    UNUSED_PARAM (i4RegenPriority);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwSetSubnetBasedStatusOnPort         */
/*                                                                           */
/*    Description         : This function enable or disable the Subnet based */
/*                          vlan classification on the port                  */
/*                                                                           */
/*    Input(s)            : u4ContextId       - Context Identifier           */
/*                          u4Port            - The Interface Index          */
/*                          u1VlanProtoEnable - FNP_TRUE/FNP_FALSE           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetSubnetBasedStatusOnPort (UINT4 u4ContextId, UINT4 u4Port,
                                          UINT1 u1VlanSubnetEnable,
                                          tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u1VlanSubnetEnable);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwEnableProtoVlanOnPort              */
/*                                                                           */
/*    Description         : This function enable or disable the  Port        */
/*                          Protocol based type on the port                  */
/*                                                                           */
/*    Input(s)            : u4ContextId       - Context Identifier           */
/*                          u4Port            - The Interface Index          */
/*                          u1VlanProtoEnable - FNP_TRUE/FNP_FALSE           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/

INT4
FsMiVlanMbsmHwEnableProtoVlanOnPort (UINT4 u4ContextId, UINT4 u4Port,
                                     UINT1 u1VlanProtoEnable,
                                     tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u1VlanProtoEnable);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwSetAllGroupsPorts                  */
/*                                                                           */
/*    Description         : This function takes care of updating the         */
/*                          hardware table for  Forward All Groups behavior  */
/*                          for the specified VLAN ID.                       */
/*                                                                           */
/*    Input(s)            : u4ContextId      - Context Identifier            */
/*                          VlanId           - VlanId ranges < 1 - 4094 >    */
/*                          pHwAllGroupPorts - Pointer to HwPortArray        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetAllGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                                 tHwPortArray * pHwAllGroupPorts,
                                 tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pHwAllGroupPorts);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwSetUnRegGroupsPorts                */
/*                                                                           */
/*    Description         : This function takes care of updating the         */
/*                          hardware table for  Forward All unreg groups     */
/*                          for the specified VLAN ID.                       */
/*                                                                           */
/*    Input(s)            : u4ContextId   - Context Identifier               */
/*                          VlanId        - VlanId                           */
/*                          pHwUnRegPorts - Pointer to HwPortArray           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetUnRegGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                                   tHwPortArray * pHwUnRegPorts,
                                   tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pHwUnRegPorts);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwSetTunnelFilter                    */
/*                                                                           */
/*    Description         : This function creates/deletes h/w filters for    */
/*                          Provider Gvrp/Gmrp/STP Macaddress. This enables  */
/*                          the tunnel PDUs to be switched by software.      */
/*                                                                           */
/*    Input(s)            : u4ContextId  - Context Identifier                */
/*                          i4BridgeMode - Bridge Mode                       */
/*                          VLAN_CUSTOMER_BRIDGE_MODE,                       */
/*                          VLAN_PROVIDER_BRIDGE_MODE,                       */
/*                          VLAN_PROVIDER_EDGE_BRIDGE_MODE,                  */
/*                          VLAN_PROVIDER_CORE_BRIDGE_MODE                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/

INT4
FsMiVlanMbsmHwSetTunnelFilter (UINT4 u4ContextId, INT4 i4BridgeMode,
                               tMacAddr MacAddr, UINT2 u2ProtocolId,
                               tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (i4BridgeMode);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u2ProtocolId);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwAddPortSubnetVlanEntry             */
/*                                                                           */
/*    Description         : This function is used to add a port based        */
/*                          subnet vlan mapping entry in hardware            */
/*                                                                           */
/*    Input(s)            : u4ContextId  - Context Identifier                */
/*                          u4Port       - The Interface Index               */
/*                          u4SubnetAddr - Subnet Address                    */
/*                          VlanId       - Vlan Id                           */
/*                          u1ArpOption  - ARP traffic option(allow/suppress)*/
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwAddPortSubnetVlanEntry (UINT4 u4ContextId, UINT4 u4Port,
                                      UINT4 u4SubnetAddr, tVlanId VlanId,
                                      UINT1 u1ArpOption,
                                      tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u4SubnetAddr);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1ArpOption);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

#ifndef BRIDGE_WANTED
/*****************************************************************************/
/*    Function Name       : FsMiBrgMbsmSetAgingTime                          */
/*                                                                           */
/*    Description         : Set the FDB aging time value for the given       */
/*                          context. This will be called either during the   */
/*                          STAP operation during reconfiguration to time-   */
/*                          out entries learned during this period. Also     */
/*                          can be used to set aging time other than default */
/*                          value 300secs supported by the 802.1D/802.1Q     */
/*                          standard.                                        */
/*                                                                           */
/*    Input(s)            : u4ContextId  - Context Identifier                */
/*                          i4AgingTime  - Time in Seconds
 *                          ranges < 10 - 1000000 >                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/

INT1
FsMiBrgMbsmSetAgingTime (UINT4 u4ContextId, INT4 i4AgingTime,
                         tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (i4AgingTime);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}
#endif /* BRIDGE_WANTED */
/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetFidPortLearningStatus               */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning status for a Port in the given Fid.         */
/*                                                                           */
/*                                                                           */
/* Input (s)          : u4ContextId - Context Id                             */
/*                    : u4Fid       - Fdb Identifier                         */
/*                      should be applied                                    */
/*                      u4Port - Port where mac lreaning status will applied */
/*                               it will be used only if pHwPortList is null.*/
/*                      u1Action - learning enable/disable                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetFidPortLearningStatus (UINT4 u4ContextId, UINT4 u4Fid,
                                        UINT4 u4Port, UINT1 u1Action,
                                        tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u1Action);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetProtocolTunnelStatusOnPort          */
/*                                                                           */
/* Description        : This function is called for configuring the action   */
/*                      to be taken on receiving control protocol PDUs on    */
/*                      the given port in the given virtual context.         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual context Id                     */
/*                      u4IfIndex   - Interface index                        */
/*                      ProtocolId  - Control Protocol for which action      */
/*                                    is being configured.                   */
/*                      u4TunnelStatus - Tunnel status                       */
/*                         VLAN_TUNNEL_PROTOCOL_PEER    - Normal             */
/*                         VLAN_TUNNEL_PROTOCOL_TUNNEL  - Encapsulate        */
/*                         VLAN_TUNNEL_PROTOCOL_DISCARD - Discard            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetProtocolTunnelStatusOnPort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                             tVlanHwTunnelFilters ProtocolId,
                                             UINT4 u4TunnelStatus,
                                             tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (ProtocolId);
    UNUSED_PARAM (u4TunnelStatus);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSetPortProtectedStatus                 */
/*                                                                           */
/* Description        : This function is called for configuring the          */
/*                      Protected/Split Horizon Status on port.              */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual context Id                     */
/*                      u4IfIndex - Interface index.                         */
/*                      u4ProtectedStatus - Port's protected status          */
/*                          VLAN_SNMP_TRUE - protected status enabled.       */
/*                          VLAN_SNMP_FALSE - protected status disabled.     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSetPortProtectedStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      INT4 u4ProtectedStatus,
                                      tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4ProtectedStatus);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwSwitchMacLearningLimit                 */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning limit for a switch.                         */
/*                                                                           */
/* Input(s)           : u4ContextId - contextId                              */
/*                      u4MacLimit - MAC learning limit.                     */
/*                      pSlotInfo  - Slot Information                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwSwitchMacLearningLimit (UINT4 u4ContextId, UINT4 u4MacLimit,
                                      tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4MacLimit);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwPortMacLearningStatus.                 */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning status for a Port                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                      pSlotInfo - Slot Information                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwPortMacLearningStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                     UINT1 u1Status, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanMbsmHwMacLearningLimit                       */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning limit for a VLAN in the given context.      */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      VlanId  - VLAN identifier.                           */
/*                      u2FdbId - Fdb Identifier                             */
/*                      u4MacLimit - MAC learning limit.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwMacLearningLimit (UINT4 u4ContextId, tVlanId VlanId,
                                UINT2 u2FdbId, UINT4 u4MacLimit,
                                tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2FdbId);
    UNUSED_PARAM (u4MacLimit);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name       : FsMiVlanMbsmHwAddStaticUcastEntryEx                 */
/*                                                                           */
/* Description         : This function adds the egress ports  into hardware  */
/*                       Static Unicast table .                              */
/*                                                                           */
/* Input(s)            : u4ContextId         - Context Identifier            */
/*                       u4FdbId             - Fid                           */
/*                       MacAddr             - Mac Address.                  */
/*                       u4RcvPort           - Received Interface Index      */
/*                       pHwAllowedToGoPorts - Pointer to HwPortArray        */
/*                       u1Status            -  Status of entry takes values */
/*                       like  permanent, deleteOnReset and                  */
/*                       deleteOnTimeout
 *                       ConnectionId - Back Bone Mac Address                */
/*                                                                           */
/* Output(s)           : None                                                */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Exceptions or Operating                                                   */
/* System Error Handling    : None.                                          */
/*                                                                           */
/* Use of Recursion        : None.                                           */
/*                                                                           */
/* Returns            : FNP_SUCCESS/FNP_FAILURE                              */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwAddStaticUcastEntryEx (UINT4 u4ContextId, UINT4 u4FdbId,
                                     tMacAddr MacAddr, UINT4 u4RcvPort,
                                     tHwPortArray * pHwAllowedToGoPorts,
                                     UINT1 u1Status, tMacAddr ConnectionId,
                                     tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4FdbId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4RcvPort);
    UNUSED_PARAM (pHwAllowedToGoPorts);
    UNUSED_PARAM (u1Status);
    UNUSED_PARAM (ConnectionId);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}
/*****************************************************************************/
/*    Function Name       : FsMiVlanMbsmHwEvbConfigSChIface                  */
/*                                                                           */
/*    Description         : This function provides suppport to create/delete */
/*                          EVB (Edge Virtual Bridging) S-Channel interface  */
/*                          (virtual port) in the hardware                   */
/*                                                                           */
/*    Input(s)            : pVlanEvbHwConfigInfo - parameters to be          */
/*                          programmed in hardware.                          */
/*                          u1OpCode -indicates about the S-Channel interface*/
/*                          creation or S-Channel interface deletion         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanMbsmHwEvbConfigSChIface (tVlanEvbHwConfigInfo * pVlanEvbHwConfigInfo,
                                      tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pVlanEvbHwConfigInfo);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

#endif
#endif
