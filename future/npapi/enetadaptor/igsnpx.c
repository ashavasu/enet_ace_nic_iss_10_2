/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igsnpx.c,v 1.3 2008/06/04 15:28:22 iss Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/
#ifndef _IGSNPX_C_
#define _IGSNPX_C_

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "snp.h"
#include "igs.h"
#include "npapi.h"
#include "npigs.h"

/*****************************************************************************/
/* Function Name      : FsIgsMbsmHwEnableIgmpSnooping                        */
/*                                                                           */
/* Description        : This function enables IGMP snooping feature in the   */
/*                      hardware. Filters are added for receiving IGMP       */
/*                      L2MC table is initialized for building MAC based     */
/*                      Multicast data forwarding                            */
/*                                                                           */
/* Input(s)           : pSlotInfo - Pointer to Slot information structure    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsIgsMbsmHwEnableIgmpSnooping (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsIgsMbsmHwEnableIpIgmpSnooping                      */
/*                                                                           */
/* Description        : This function enables IGMP snooping feature in the   */
/*                      hardware. Filters are added for receiving IGMP       */
/*                      IPMC table is initialized for building IP based      */
/*                      Multicast data forwarding                            */
/*                                                                           */
/* Input(s)           : pSlotInfo - Pointer to Slot information structure    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsIgsMbsmHwEnableIpIgmpSnooping (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsIgsMbsmHwEnhancedMode                              */
/*                                                                           */
/* Description        : This function enables IGMP Enhanced mode in the      */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : u1EnhStatus - Enhanced Mode Status                   */
/*                      pSlotInfo   - Pointer to Slot information structure  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsIgsMbsmHwEnhancedMode (UINT1 u1EnhStatus, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u1EnhStatus);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsIgsMbsmHwPortRateLimit                             */
/*                                                                           */
/* Description        : This function configures IGMP Rate Limit for a       */
/*                      downstream interface in hardware. In default mode,   */
/*                      the downstream interface corresponds to only port and*/
/*                      inner-vlan will be 0. When enhanced mode is          */
/*                      enabled, downstream interface corresponds to port and*/
/*                      inner-vlan.In this mode inner-vlan may be within the */
/*                      valid vlan range or 0.When rate limit is             */
/*                      configured for an interface, this API will be invoked*/
/*                      with action as .ADD. and u4Rate as the required rate.*/
/*                      When rate limit is configured as zero (rate limit all*/
/*                      packets on this interface) this API will be invoked  */
/*                      with action as .ADD. and u4Rate as zero. When rate   */
/*                      limit is configured to default value ( no rate       */
/*                      limiting to be applied on this interface) this API   */
/*                      will be invoked with action as .REMOVE. and the      */
/*                      u4Rate can be ignored.                               */
/*                                                                           */
/* Input(s)           : Struct tIgsHwRateLmt - It contains                   */
/*                      UINT4 u4Port            :  Port Number               */
/*                      tVlanId InnerVlanId     :  Inner-Vlan                */
/*                      UINT4 u4Rate            :  No of packets per second  */
/*                    : UINT1 u1Action - Type of action to be performed.     */
/*                                        ( Add/Remove Rate-limit for port ) */
/*                    : pSlotInfo - Pointer to Slot information structure    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsIgsMbsmHwPortRateLimit (tIgsHwRateLmt * pIgsHwRateLmt, UINT1 u1Action,
                          tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pIgsHwRateLmt);
    UNUSED_PARAM (u1Action);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMbsmNpUpdateMcFwdEntries                           */
/*                                                                           */
/* Description        : This function Updates Multicast forwarding entries   */
/*                      in the hardwarei based on the EventType.             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMbsmNpUpdateIpmcFwdEntries (tSnoopVlanId VlanId, UINT4 u4GrpAddr,
                              UINT4 u4SrcAddr, tPortList PortList,
                              tPortList UntagPortList,
                              tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4GrpAddr);
    UNUSED_PARAM (u4SrcAddr);
    UNUSED_PARAM (PortList);
    UNUSED_PARAM (UntagPortList);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}
#endif
