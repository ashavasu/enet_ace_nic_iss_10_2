/********************************************************************
 * Copyright (C) Future Software Limited, 1997-98, 2001-2003
 *
 * $Id: pppnp.c,v 1.2 2014/03/11 14:02:44 siva Exp $
 *
 * Description: All network processor function  given here
 *******************************************************************/
#ifndef _PPPNP_C_
#define _PPPNP_C_
#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "npapi.h"
#include "pppnp.h"

#include "adap_types.h"
#include "adap_logger.h"
#include "EnetHal_L2_Api.h"

/*****************************************************************************/
/*    Function Name       : FsNpPppLCPUp                                     */
/*                                                                           */
/*    Description         : This function is called when the LCP protocol    */
/*                          is UP                                            */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
UINT4
FsNpPppLCPUp (UINT4 IfIndex, UINT4 u4PeerMRU, UINT4 u4LocalMRU,
              UINT1 u1LocToRemPFC, UINT1 u1RemToLocPFC,
              UINT1 u1LocToRemACFC, UINT1 u1RemToLocACFC)
{
    UNUSED_PARAM (IfIndex);
    UNUSED_PARAM (u4PeerMRU);
    UNUSED_PARAM (u4LocalMRU);
    UNUSED_PARAM (u1LocToRemPFC);
    UNUSED_PARAM (u1RemToLocPFC);
    UNUSED_PARAM (u1LocToRemACFC);
    UNUSED_PARAM (u1RemToLocACFC);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsNpPppGetLinkStatus                             */
/*                                                                           */
/*    Description         : This function checks if the flows for the PPP    */
/*                          channels are active                              */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
UINT4
FsNpPppGetLinkStatus (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsNpPppIPCPUp                                    */
/*                                                                           */
/*    Description         : This function is called when the PPP Control     */
/*                          protocol establishes  IP address with Server    */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
UINT4
FsNpPppIPCPUp (UINT4 IfIndex, UINT4 u4AckPeerIPAddr, UINT4 u4AckLocalIPAddr)
{
#ifdef USER_HW_API
    INT1 ret=FNP_SUCCESS;
    ret = EnetHal_FsNpPppIPCPUp(IfIndex, u4AckPeerIPAddr, u4AckLocalIPAddr);
#endif //USER_HW_API
    UNUSED_PARAM (IfIndex);
    UNUSED_PARAM (u4AckPeerIPAddr);
    UNUSED_PARAM (u4AckLocalIPAddr);
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsNpPPPoEUpdateNat                               */
/*                                                                           */
/*    Description         : This function Updates the NAT Protocol with the  */
/*                          negotiated IP address                            */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
UINT4
FsNpPPPoEUpdateNat (UINT4 IfIndex, UINT4 u4AckPeerIPAddr)
{
    UNUSED_PARAM (IfIndex);
    UNUSED_PARAM (u4AckPeerIPAddr);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsNpPppCPDown                                    */
/*                                                                           */
/*    Description         : This function is called when the PPP session is  */
/*                          made down                                        */
/*                                                                           */
/*    Input(s)            : Interface Index                                  */
/*                          CP type can be LCP/IPCP                          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
UINT4
FsNpPppCPDown (UINT4 u4IfIndex, UINT4 u4CPType)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4CPType);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsNpCreatePppIf                                  */
/*                                                                           */
/*    Description         : This function creates the PPP Interface in the   */
/*                          hardware                                         */
/*                                                                           */
/*    Input(s)            : Interface Index of the PPP Interface             */
/*                          Physical Index on which the PPP is layered       */
/*                          Interface type can be PPP/MLPPP/MCPPP Interface  */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
UINT4
FsNpCreatePppIf (UINT4 u4IfIndex, UINT4 u4PhysIfIndex, UINT4 u4IfType)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PhysIfIndex);
    UNUSED_PARAM (u4IfType);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsNpUpdPppIf                                     */
/*                                                                           */
/*    Description         : This function updates the PPP/MLPPP/MCPPP If     */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : Interface Index                                  */
/*                          Interface Type                                   */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
UINT4
FsNpUpdPppIf (UINT4 u4IfIndex, UINT4 u4IfType, UINT4 u4Reserved)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4IfType);
    UNUSED_PARAM (u4Reserved);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsNpDelPppIf                                     */
/*                                                                           */
/*    Description         : This function Deletes the PPP/MLPPP/MCPPP If     */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : Interface Index                                  */
/*                          Interface Type                                   */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
UINT4
FsNpDelPppIf (UINT4 u4IfIndex, UINT4 u4IfType, UINT4 u4Reserved)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4IfType);
    UNUSED_PARAM (u4Reserved);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsNpPPPoEAddNewSession                           */
/*                                                                           */
/*    Description         : This function  adds a new PPPoE session in NP    */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : Interface Index                                  */
/*                          Destination Mac Address                          */
/*                          Port Number                                      */
/*                          Session Identifier                               */
/*                          Cookie Identifier                                */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
UINT4
FsNpPPPoESession (ePppNpwFsPPPoESession pNpType,
                  unPppNpwFsPPPoESession * pUnion)
{
    switch (pNpType)
    {
        case PPPoE_ADD_SESSION:
            UNUSED_PARAM (pUnion->FsAddNewSession);
            break;
        case PPPoE_DELETE_SESSION:
            UNUSED_PARAM (pUnion->FsDeleteSession);
            break;
        case PPPoE_TRAP_UPDATE:
            UNUSED_PARAM (pUnion->FsFilter);
            break;
        case PPPoE_UPDATE_NAT:
            UNUSED_PARAM (pUnion->FsNATEntry);
            break;
        default:
            break;
    }
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsNpPPPoEAddNewSession                           */
/*                                                                           */
/*    Description         : This function  adds a new PPPoE session in NP    */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : Interface Index                                  */
/*                          Destination Mac Address                          */
/*                          Port Number                                      */
/*                          Session Identifier                               */
/*                          Cookie Identifier                                */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
UINT4
FsNpPPPoEAddNewSession (UINT4 u4IfIndex, tMacAddr DestMACAddr,
                        tMacAddr SrcMACAddr, UINT2 u2Port, UINT2 u2SessionId,
                        UINT4 ACCookieId, tVlanTag *pVlanInfo)
{
    INT1 ret=FNP_SUCCESS;
#ifdef USER_HW_API
    tEnetHal_MacAddr dest_mac;
    tEnetHal_MacAddr src_mac;
    tEnetHal_VlanTag VlanTag;
    adap_mac_from_arr(&dest_mac, DestMACAddr);
    adap_mac_from_arr(&src_mac, SrcMACAddr);
    memcpy (&VlanTag, pVlanInfo, sizeof(tEnetHal_VlanTag));
    ret = EnetHal_FsNpPPPoEAddNewSession(u4IfIndex, &dest_mac, &src_mac, u2Port, 
		u2SessionId, ACCookieId, &VlanTag);
#endif //USER_HW_API
    return ret;
}

/******************************************************************/
/*  Function Name             : FlPPPoEDelSession                 */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
UINT4
FsNpPPPoEDelSession (UINT4 u4IfIndex, UINT2 u2Port, UINT2 u2SessionId)
{
#ifdef USER_HW_API
    INT1 ret=FNP_SUCCESS;
    ret = EnetHal_FsNpPPPoEDelSession(u4IfIndex, u2Port, u2SessionId);
#endif //USER_HW_API
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u2SessionId);
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsNpPppInit                                      */
/*                                                                           */
/*    Description         : This function is called To Initialise the PPP    */
/*                          NP Specifics                                     */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
UINT4
FsNpPppInit (VOID)
{
#ifdef USER_HW_API
    EnetHal_FsNpPppInit();
#endif //USER_HW_API
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : PppNpTinygramCompressSupported                   */
/*                                                                           */
/*    Description         : This function is called To Initialise the PPP    */
/*                          NP Specifics                                     */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
INT4
PppNpTinygramCompressSupported ()
{
    return FNP_FALSE;
}

/*****************************************************************************/
/*    Function Name       : PppNpNonEthernetMacTypesSupported                */
/*                                                                           */
/*    Description         : This function is called To Initialise the PPP    */
/*                          NP Specifics                                     */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
INT4
PppNpNonEthernetMacTypesSupported ()
{
    return FNP_FALSE;
}

/*****************************************************************************/
/*    Function Name       : PppNpSRouteBridgeSupported                       */
/*                                                                           */
/*    Description         : This function is called To Initialise the PPP    */
/*                          NP Specifics                                     */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
INT4
PppNpSRouteBridgeSupported ()
{
    return FNP_FALSE;
}

/*****************************************************************************/
/*    Function Name       : FsNpDeleteMPBundle                               */
/*                                                                           */
/*    Description         : This function is called to delete specified      */
/*                          MultiLink Bundle.                                */
/*                                                                           */
/*    Input(s)            : u4IfIndex                                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
UINT4
FsNpDeleteMPBundle (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsNpDisableLinkInMPBundle                        */
/*                                                                           */
/*    Description         : This function is called to disable a specified   */
/*                          link in MultiLink bundle.                        */
/*                                                                           */
/*    Input(s)            : u4MemberIfIndex, u4BundleIfIndex                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
UINT4
FsNpDisableLinkInMPBundle (UINT4 u4MemberIfIndex, UINT4 u4BundleIfIndex)
{
    UNUSED_PARAM (u4MemberIfIndex);
    UNUSED_PARAM (u4BundleIfIndex);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsNpRemoveLinkFromMPBundle                       */
/*                                                                           */
/*    Description         : This function is called to remove the spcified   */
/*                          link from the MultiLink bundle.                  */
/*                                                                           */
/*    Input(s)            : u4MemberIfIndex, u4BundleIfIndex                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
UINT4
FsNpRemoveLinkFromMPBundle (UINT4 u4MemberIfIndex, UINT4 u4BundleIfIndex)
{
    UNUSED_PARAM (u4MemberIfIndex);
    UNUSED_PARAM (u4BundleIfIndex);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsNpAddLinkToMPBundle                            */
/*                                                                           */
/*    Description         : This function is called to add a link in         */
/*                          MultiLink Bundle.                                */
/*                                                                           */
/*    Input(s)            : u4MemberIfIndex, u4BundleIfIndex                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
UINT4
FsNpAddLinkToMPBundle (UINT4 u4MemberIfIndex, UINT4 u4BundleIfIndex)
{
    UNUSED_PARAM (u4MemberIfIndex);
    UNUSED_PARAM (u4BundleIfIndex);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsNpEnableLinkInMPBundle                         */
/*                                                                           */
/*    Description         : This function is called to enable a link in      */
/*                          MultiLink bundle in hardware.                    */
/*                                                                           */
/*    Input(s)            : u4MemberIfIndex, u4BundleIfIndex                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
UINT4
FsNpEnableLinkInMPBundle (UINT4 u4MemberIfIndex, UINT4 u4BundleIfIndex)
{
    UNUSED_PARAM (u4MemberIfIndex);
    UNUSED_PARAM (u4BundleIfIndex);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsNpCreateMPBundle                               */
/*                                                                           */
/*    Description         : Function used to create a multilink bundle in the*/
/*                          Hardware.                                        */
/*                                                                           */
/*    Input(s)            : u4IfIndex                                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
UINT4
FsNpCreateMPBundle (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return FNP_SUCCESS;
}


/*****************************************************************************/
/*    Function Name       : FsNpPPPoEAddVlanInfo                             */
/*                                                                           */
/*    Description         : This function adds VLAN Info for PPPoE session in NP*/
/*                                                                           */
/*                                                                           */
/*    Input(s)            : Interface Index                                  */
/*                          Port Number                                      */
/*                          VLAN Info                                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
UINT4
FsNpPPPoEAddVlanInfo (UINT4 u4IfIndex, UINT4 u4PhysIfIndex, tVlanTag *pVlanInfo)
{
    INT1 ret=FNP_SUCCESS;
#ifdef USER_HW_API
    tEnetHal_VlanTag VlanTag;

    memcpy (&VlanTag, pVlanInfo, sizeof(tEnetHal_VlanTag));
    ret = EnetHal_FsNpPPPoEAssociateVlan(u4IfIndex, u4PhysIfIndex, &VlanTag);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsNpPPPoEDelVlanInfo                             */
/*                                                                           */
/*    Description         : This function deletes VLAN Info for PPPoE session in NP*/
/*                                                                           */
/*                                                                           */
/*    Input(s)            : Interface Index                                  */
/*                          Port Number                                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
UINT4
FsNpPPPoEDelVlanInfo (UINT4 u4IfIndex, UINT4 u4PhysIfIndex)
{
    INT1 ret=FNP_SUCCESS;
#ifdef USER_HW_API
#endif //USER_HW_API
    return ret;
}



#endif /* _PPPNP_C_ */
