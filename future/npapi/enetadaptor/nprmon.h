/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: nprmon.h,v 1.3 2007/02/01 14:59:31 iss Exp $
 *
 * Description: This file contains the function implementations  of 
 *              RMON NP-API.
 ****************************************************************************/

#ifndef _NPRMON_H
#define _NPRMON_H

#include "rmonnp.h"

#endif /* _NPRMON_H */
