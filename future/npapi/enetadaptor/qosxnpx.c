/*$Id: qosxnpx.c,v 1.4 2014/11/19 11:03:55 siva Exp $*/
/*****************************************************************************
 ** Copyright (C) 2006 Aricent Inc . All Rights Reserved
 **
 ** $Id: qosxnpx.c,v 1.4 2014/11/19 11:03:55 siva Exp $
 **
 ** Description: This file contains the function implementations  of DiffServ
 **              System related NP-API.
 *****************************************************************************/
#ifndef _QOSXNPX_C
#define _QOSXNPX_C

#include "npqosx.h"

/*****************************************************************************/
/* Function Name      : QoSMbsmHwUpdatePolicyMapForClass                     */
/*                                                                           */
/* Description        : This function used to update the Policy Parameters   */
/*                      for a CLASS of traffic.                              */
/*                                                                           */
/* Input(s)           : pClsMapEntry -  Ptr for a ClassMapEntry              */
/*                    : pPlyMapEntry -  Ptr for a PolicyMapEntry             */
/*                    : pInProActEntry -Ptr for a In Profile Action Entry    */
/*                    : pOutProActEntry-Ptr for a Out Profile Action Entry   */
/*                    : pMeterEntry    -Ptr for a Meter entry                */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSMbsmHwUpdatePolicyMapForClass (tQoSClassMapEntry * pClsMapEntry,
                                  tQoSPolicyMapEntry * pPlyMapEntry,
                                  tQoSInProfileActionEntry * pInProActEntry,
                                  tQoSOutProfileActionEntry * pOutProActEntry,
                                  tQoSMeterEntry * pMeterEntry,
                                  tMbsmSlotInfo * pSlotInfo)
{

    UNUSED_PARAM (pClsMapEntry);
    UNUSED_PARAM (pPlyMapEntry);
    UNUSED_PARAM (pInProActEntry);
    UNUSED_PARAM (pOutProActEntry);
    UNUSED_PARAM (pMeterEntry);
    UNUSED_PARAM (pSlotInfo);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSMbsmHwQueueCreate                                     */
/*                                                                           */
/* Description        : This function is used to Create a Queue in the HW    */
/*                                                                           */
/* Input(s)           : i4IfIndex   - Scheduler and Q Interface.             */
/*                    : u4QId       - Q id                                   */
/*                    : pQEntry     - Ptr to 'tQoSQEntry'                    */
/*                    : pQTypeEntry - Ptr to 'tQoSQtypeEntry'                */
/*                    : papRDCfgEntry - Ptr to  array of ptr type            */
/*                                     'tQoSREDCfgEntry'                     */
/*                                     3 Prts papRDCfgEntry[0-2] for DP      */
/*                                     Low(0), Medium(1), high (2)           */
/*                    : i2HL        - Scheduler Hierarchy level              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSMbsmHwQueueCreate (INT4 i4IfIndex, UINT4 u4QId, tQoSQEntry * pQEntry,
                      tQoSQtypeEntry * pQTypeEntry,
                      tQoSREDCfgEntry * papRDCfgEntry[], INT2 i2HL,
                      tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4QId);
    UNUSED_PARAM (pQEntry);
    UNUSED_PARAM (pQTypeEntry);
    UNUSED_PARAM (papRDCfgEntry);
    UNUSED_PARAM (i2HL);
    UNUSED_PARAM (pSlotInfo);

    return (FNP_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : QoSMbsmHwMapClassToQueue                             */
/*                                                                           */
/* Description        : This function is used to Map or UnMap a Q and a      */
/*                      Scheduler in the Hardware device.                    */
/*                                                                           */
/* Input(s)           : i4IfIndex   - Scheduler and Q Interface.             */
/*                    : i4ClsOrPriType - Map Type of the 'u4ClsOrPri'        */
/*                               NONE  means CLASS  otherwise                */
/*                               VLAN_PRI/IP_TOS/IP_DSCP/MPLS_EXP/VLAN_DEI   */
/*                    : u4ClsOrPri  - Map Type Value                         */
/*                    : u4QId       - Q id                                   */
/*                    : u1Flag      - Map / Unmap                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSMbsmHwMapClassToQueue (INT4 i4IfIndex, INT4 i4ClsOrPriType, UINT4 u4ClsOrPri,
                          UINT4 u4QId, UINT1 u1Flag, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4ClsOrPriType);
    UNUSED_PARAM (u4ClsOrPri);
    UNUSED_PARAM (u4QId);
    UNUSED_PARAM (u1Flag);
    UNUSED_PARAM (pSlotInfo);
    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSMbsmHwMapClassToQueue                             */
/*                                                                           */
/* Description        : This function is used to Map or UnMap a Q and a      */
/*                      Scheduler in the Hardware device.This function is the*/
/*                      replication of the function QoSMbsmHwMapClassToQueue */
/*                      with additional paramter pClsMapEntry to get the     */
/*                      hardware handle infromation                           */
/*                                                                           */
/* Input(s)           : i4IfIndex   - Scheduler and Q Interface.             */
/*                    : i4ClsOrPriType - Map Type of the 'u4ClsOrPri'        */
/*                               NONE  means CLASS  otherwise                */
/*                               VLAN_PRI/IP_TOS/IP_DSCP/MPLS_EXP/VLAN_DEI   */
/*                    : u4ClsOrPri  - Map Type Value                         */
/*                    : u4QId       - Q id                                   */
/*                    : u1Flag      - Map / Unmap                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSMbsmHwMapClassToQueueId (tQoSClassMapEntry * pClsMapEntry,
                            INT4 i4IfIndex, INT4 i4ClsOrPriType,
                            UINT4 u4ClsOrPri, UINT4 u4QId, UINT1 u1Flag,
                            tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pClsMapEntry);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4ClsOrPriType);
    UNUSED_PARAM (u4ClsOrPri);
    UNUSED_PARAM (u4QId);
    UNUSED_PARAM (u1Flag);
    UNUSED_PARAM (pSlotInfo);
    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : FsQosMbsmHwConfigPfc                                 */
/*                                                                           */
/* Description        : This function is used to configure the PFC in HW     */
/*                                                                           */
/* Input(s)           : pQosPfcHwEntry - PFC Hw Entry                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsQosMbsmHwConfigPfc (tQosPfcHwEntry * pQosPfcHwEntry,
                      tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pQosPfcHwEntry);
    UNUSED_PARAM (pSlotInfo);
    return (FNP_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : QoSMbsmHwSchedulerUpdateParams                       */
/*                                                                           */
/* Description        : This function is used to Update a Scheduler in the   */
/*                      Hardware device.                                     */
/*                                                                           */
/* Input(s)           : pSchedEntry   - Ptr to 'tQoSSchedulerEntry'          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSMbsmHwSchedulerUpdateParams (tQoSSchedulerEntry * pSchedEntry,
                                tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSchedEntry);
    UNUSED_PARAM (pSlotInfo);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSMbsmHwSchedulerHierarchyMap                           */
/*                                                                           */
/* Description        : This function is used to Map Hierarchy Scheduler in  */
/*                      Hardware device.                                     */
/*                                                                           */
/* Input(s)           : i4IfIndex   - Interface Index                        */
/*                    : i4SchedId   - Scheduler ID                           */
/*                    : u2Sweight   - Weight of the Scheduler Hierarchy      */
/*                    : u1Spriority - Priority of the Scheduler Hierarchy    */
/*                    : i4NextSchedId - Next Level Scheduler Id              */
/*                    : i4NextQId   - Next Level Q Id                        */
/*                    : i2HL        - Scheduler Hierarchy level              */
/*                    : u1Flag      - Map / Unmap                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSMbsmHwSchedulerHierarchyMap (INT4 i4IfIndex, UINT4 u4SchedId,
                                UINT2 u2Sweight, UINT1 u1Spriority,
                                UINT4 u4NextSchedId, UINT4 u4NextQId, INT2 i2HL,
                                UINT1 u1Flag, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SchedId);
    UNUSED_PARAM (u2Sweight);
    UNUSED_PARAM (u1Spriority);
    UNUSED_PARAM (u4NextSchedId);
    UNUSED_PARAM (u4NextQId);
    UNUSED_PARAM (i2HL);
    UNUSED_PARAM (u1Flag);
    UNUSED_PARAM (pSlotInfo);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSMbsmHwSetDefUserPriority                              */
/*                                                                           */
/* Description        : This function is used set Port Default User Priority */
/*                      in Hardware device.                                  */
/*                                                                           */
/* Input(s)           : i4Port        - Interface Index                      */
/*                    : i4DefPriority - Default Priority for the given Port  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSMbsmHwSetDefUserPriority (INT4 i4Port, INT4 i4DefPriority,
                             tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (i4Port);
    UNUSED_PARAM (i4DefPriority);
    UNUSED_PARAM (pSlotInfo);
    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSMbsmHwSetPbitPreferenceOverDscp                       */
/*                                                                           */
/* Description        : This function sets the pbit preference over DSCP if  */
/*                      the frame contains both pbit and DSCP                */
/*                                                                           */
/* Input(s)           : i4Port                                               */
/*                    : i4PbitPref                                           */
/*                                                                           */
/* Output             : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
QoSMbsmHwSetPbitPreferenceOverDscp (INT4 i4Port, INT4 i4PbitPref,
                                    tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (i4Port);
    UNUSED_PARAM (i4PbitPref);
    UNUSED_PARAM (pSlotInfo);
    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSMbsmHwSetCpuRateLimit                                 */
/*                                                                           */
/* Description        : This function is used set the CPU rate limiting      */
/*                      in Hardware device.                                  */
/*                                                                           */
/* Input(s)           : i4CpuQueueId   - CPU Queue Id                        */
/*                    : u4MinRate      - Mininum transmission rate           */
/*                    : u4MaxRate      - Maximum transmission rate           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSMbsmHwSetCpuRateLimit (INT4 i4CpuQueueId, UINT4 u4MinRate, UINT4 u4MaxRate,
                          tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (i4CpuQueueId);
    UNUSED_PARAM (u4MinRate);
    UNUSED_PARAM (u4MaxRate);
    UNUSED_PARAM (pSlotInfo);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSMbsmHwMapClasstoPriMap                                */
/*                                                                           */
/* Description        : This function map vlanpri/dscp/exp values to         */
/*                        internal priority                                  */
/*                                                                           */
/* Input(s)           : pQosClassToPriMapEntry                               */
/*                                                                           */
/* Output             : Field processor entry id if prio type is exp         */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSMbsmHwMapClasstoPriMap (tQosClassToPriMapEntry * pQosClassToPriMapEntry,
                           tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pQosClassToPriMapEntry);
    UNUSED_PARAM (pSlotInfo);
    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosMbsmHwMapClassToIntPriority                       */
/*                                                                           */
/* Description        : This function associatess L2/L3 filter classified    */
/*                          traffic to internal priority                     */
/*                                                                           */
/* Input(s)           : pQosClassToIntPriEntryi                              */
/*                                                                           */
/* Output             : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QosMbsmHwMapClassToIntPriority (tQosClassToIntPriEntry * pQosClassToIntPriEntry,
                                tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pQosClassToIntPriEntry);
    UNUSED_PARAM (pSlotInfo);
    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSMbsmHwInit                                        */
/*                                                                           */
/* Description        : This function intialises the QoS Engine in the       */
/*                       Hardware device                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSMbsmHwInit (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSMbsmHwSchedulerAdd                                */
/*                                                                           */
/* Description        : This function is to configure the scheduler in HW    */
/*                                                                           */
/* Input(s)           : tQoSSchedulerEntry - Scheduler Hw Entry              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSMbsmHwSchedulerAdd (tQoSSchedulerEntry * pSchedEntry,
                       tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSchedEntry);
    UNUSED_PARAM (pSlotInfo);
    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSMbsmHwMeterCreate                                 */
/*                                                                           */
/* Description        : This function is used to Create Meter in the device  */
/*                                                                           */
/* Input(s)           : pMeterEntry    -Ptr for a Meter entry                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSMbsmHwMeterCreate (tQoSMeterEntry * pMeterEntry, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pMeterEntry);
    UNUSED_PARAM (pSlotInfo);
    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSMbsmHwMapClassToPolicy                            */
/*                                                                           */
/* Description        : This function used to configure the set traffic into */
/*                       a CLASS and Policy for it.                          */
/*                                                                           */
/* Input(s)           : pClsMapEntry -  Ptr for a ClassMapEntry              */
/*                    : pPlyMapEntry -  Ptr for a PolicyMapEntry             */
/*                    : pInProActEntry -Ptr for a In Profile Action Entry    */
/*                    : pOutProActEntry-Ptr for a Out Profile Action Entry   */
/*                    : pMeterEntry    -Ptr for a Meter entry                */
/*                    : u1Flag         -  QOS_PLY_ADD / QOS_PLY_MAP          */
/*                      QOS_PLY_ADD - Policy needs to be created and map     */
/*                                    the Class and Policy                   */
/*                      QOS_PLY_MAP - Map the Class with already created     */
/*                                    Policy.                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSMbsmHwMapClassToPolicy (tQoSClassMapEntry * pClsMapEntry,
                           tQoSPolicyMapEntry * pPlyMapEntry,
                           tQoSInProfileActionEntry * pInProActEntry,
                           tQoSOutProfileActionEntry * pOutProActEntry,
                           tQoSMeterEntry * pMeterEntry, UINT1 u1Flag,
                           tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pClsMapEntry);
    UNUSED_PARAM (pPlyMapEntry);
    UNUSED_PARAM (pInProActEntry);
    UNUSED_PARAM (pOutProActEntry);
    UNUSED_PARAM (pMeterEntry);
    UNUSED_PARAM (u1Flag);
    UNUSED_PARAM (pSlotInfo);

    return (FNP_SUCCESS);
}

#endif /* __QOSX_NPX_C__ */
