/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: brgnpx.c,v 1.2 2007/02/01 14:59:31 iss Exp $
 *
 * Description: All network processor function  given here
 *
 *******************************************************************/

#include "lr.h"
#include "cfa.h"
#include "fsvlan.h"
#include "bridge.h"
#include "npapi.h"
#include "npbrg.h"
#include "npcfa.h"

/*******************************************************************************
* FsBrgMbsmSetAgingTime
*
* DESCRIPTION:
* Set the FDB aging time value. This will be called either during the STAP
* operation during reconfiguration to time out entries learned during this 
* period. Also can be used to set aging time other than default value 300secs 
* supported by the 802.1D/802.1Q standard. 
*
* INPUTS:
* i4Aging - Aging Time
* OUTPUTS:
* None.
*
* RETURNS:
* FNP_SUCCESS - on success
* FNP_FAILURE - on error
*
*
* COMMENTS:
* None 

*
*******************************************************************************/
INT1
FsBrgMbsmSetAgingTime (INT4 i4AgingTime, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (i4AgingTime);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}
