/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: npiss.h,v 1.8 2014/11/05 06:47:38 siva Exp $
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/

#ifndef _NPISS_H
#define _NPISS_H

#include "issnp.h"
#include "cli.h"
VOID  NpSetTrace PROTO((UINT1 u1TraceModule, UINT1 u1TraceLevel ));
VOID  NpGetTrace PROTO((UINT1 u1TraceModule, UINT1 *pu1TraceLevel));
VOID  NpSetTraceLevel PROTO ((UINT2 u2TraceLevel));
VOID  NpGetTraceLevel PROTO ((UINT2 *pu2TraceLevel));

VOID
IssNpapiShowDebugging PROTO((tCliHandle));

#endif /* _NPISS_H */
