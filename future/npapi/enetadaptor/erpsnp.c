/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                      *
 *                                                                           *
 * $Id: erpsnp.c                                                             *
 *                                                                           *
 * Description: All prototypes for Network Processor API functions           *
 *              done here                                                    *
 *******************************************************************i*********/

#include "lr.h"
#include "npapi.h"
#include "ecfm.h"
#include "erps.h"
#include "erpsnp.h"

/*****************************************************************************/
/*                                                                           */
/* Function Name      : FsErpsHwRingConfig                                   */
/*                                                                           */
/* Description        : This function will program/update ring information   */
/*                      into the hardware. This function will be called      */
/*                      whenever a new ring is made active/move to           */
/*                      notInService/Deleted and whenever any dynamic or     */
/*                      configuration change causes a ring port to move to   */
/*                      block or unblocked state.                            */
/*                                                                           */
/* Input(s)           : pErpsHwRingInfo  . This variable will contain ring    */
/*                                        information that needs to be       */
/*                                        updated in the hardware.           */
/*                                                                           */
/*                      Following Ring Action Ids can be passed to the h/w   */
/*                      o ERPS_HW_RING_CREATE - to create a ring in the h/w  */
/*                      o ERPS_HW_RING_MODIFY - to modify the ring           */
/*                                              information in h/w           */
/*                      o ERPS_HW_RING_DELETE - to delete a ring entry       */
/*                      o ERPS_HW_ADD_GROUP_AND_VLAN_MAPPING - create the    */
/*                                   vlan group in the h/w and add the       */
/*                                   vlan to it.                             */
/*                      o ERPS_HW_DEL_GROUP_AND_VLAN_MAPPING - Unmap the     */
/*                                   vlan from the vlan group and then       */
/*                                   delete the vlan group from the h/w      */
/*                      o ERPS_HW_ADD_VLAN_MAPPING - add vlan to vlan        */
/*                                   group in h/w                            */
/*                      o ERPS_HW_DEL_VLAN_MAPPING - delete/unmap vlan       */
/*                                   from the vlan group in h/w              */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS . When hardware successfully configured  */
/*                      FNP_FAILURE . When hardware configuration failed     */
/*                                                                           */
/*****************************************************************************/
INT4
FsErpsHwRingConfig (tErpsHwRingInfo * pErpsHwRingInfo)
{
    UNUSED_PARAM (pErpsHwRingInfo);

    return FNP_SUCCESS;
}
