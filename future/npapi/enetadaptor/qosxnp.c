/*$Id: qosxnp.c,v 1.11 2015/02/28 12:17:05 siva Exp $*/
/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qosxnp.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-NPAPI_STUBS                                 */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This file contains the NPAPI stub functions     */
/*                          implementations for Aricent QoS Module.         */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/

#ifndef __QOSX_NP_C__
#define __QOSX_NP_C__
#include "npqosx.h"

//#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "EnetHal_L2_Api.h"

/*****************************************************************************
 * private function
******************************************************************************/
#ifdef USER_HW_API	
static void convertMeterEntry(tQoSMeterEntry *pMeterEntry,tEnetHal_QoSMeterEntry *pEnetMeterEntry)
{
	pEnetMeterEntry->i4QoSMeterId = pMeterEntry->i4QoSMeterId;
	pEnetMeterEntry->u1QoSInterval = pMeterEntry->u1QoSInterval;
	pEnetMeterEntry->u1QoSMeterColorMode = pMeterEntry->u1QoSMeterColorMode;
	pEnetMeterEntry->u1QoSMeterPacketBased = pMeterEntry->u1QoSMeterPacketBased;
	pEnetMeterEntry->u1QoSMeterStatus = pMeterEntry->u1QoSMeterStatus;
	pEnetMeterEntry->u1QoSMeterType = pMeterEntry->u1QoSMeterType;
	pEnetMeterEntry->u4NextMeter = pMeterEntry->u4NextMeter;
	pEnetMeterEntry->u4QoSCBS = pMeterEntry->u4QoSCBS;
	pEnetMeterEntry->u4QoSCIR = pMeterEntry->u4QoSCIR;
	pEnetMeterEntry->u4QoSEBS = pMeterEntry->u4QoSEBS;
	pEnetMeterEntry->u4QoSEIR = pMeterEntry->u4QoSEIR;
	pEnetMeterEntry->u4QoSMeterFlag = pMeterEntry->u4QoSMeterFlag;
}
static void convertClassMapEntry(tQoSClassMapEntry * pClsMapEntry,tEnetHal_QoSClassMapEntry *pEnetClsMapEntry)
{

    UINT4 IssPort;
    BOOLEAN    IssPortExists;

	pEnetClsMapEntry->PreColor = pClsMapEntry->u1PreColor;
	pEnetClsMapEntry->QoSMFCStatus = pClsMapEntry->u4QoSMFClass;
	pEnetClsMapEntry->QoSMFClass = pClsMapEntry->u4QoSMFClass;
	pEnetClsMapEntry->TrafficClass = pClsMapEntry->u4TrafficClass;
	pEnetClsMapEntry->u2PPPSessId = pClsMapEntry->u2PPPSessId;

	if(pClsMapEntry->pInVlanMapPtr!= NULL)
	{
		tEnetHal_InVlanMapEntry tInVlanMapEntry;
		memset(&tInVlanMapEntry,0,sizeof(tEnetHal_InVlanMapEntry));
		pEnetClsMapEntry->pInVlanMapPtr = &tInVlanMapEntry;

		tInVlanMapEntry.u1Status = pClsMapEntry->pInVlanMapPtr->u1Status;
		tInVlanMapEntry.u2VlanId = pClsMapEntry->pInVlanMapPtr->u2VlanId;
		tInVlanMapEntry.u4Id = pClsMapEntry->pInVlanMapPtr->u4Id;
		tInVlanMapEntry.u4IfIndex = pClsMapEntry->pInVlanMapPtr->u4IfIndex;
		tInVlanMapEntry.u4VlanMapHwId = pClsMapEntry->pInVlanMapPtr->u4VlanMapHwId;
	}

	if(pClsMapEntry->pPriorityMapPtr!= NULL)
	{
		tEnetHal_PriorityMapEntry	tPriorityMapEntry;
		memset(&tPriorityMapEntry,0,sizeof(tEnetHal_PriorityMapEntry));
		pEnetClsMapEntry->pPriorityMapPtr = &tPriorityMapEntry;

		tPriorityMapEntry.i1InDEI = pClsMapEntry->pPriorityMapPtr->i1InDEI;
		tPriorityMapEntry.u1Color = pClsMapEntry->pPriorityMapPtr->u1Color;
		tPriorityMapEntry.u1InPriType = pClsMapEntry->pPriorityMapPtr->u1InPriType;
		tPriorityMapEntry.u1InPriority = pClsMapEntry->pPriorityMapPtr->u1InPriority;
		tPriorityMapEntry.u1PriorityMapStatus = pClsMapEntry->pPriorityMapPtr->u1PriorityMapStatus;
		tPriorityMapEntry.u1RegenInnerPri = pClsMapEntry->pPriorityMapPtr->u1RegenInnerPri;
		tPriorityMapEntry.u1RegenPri = pClsMapEntry->pPriorityMapPtr->u1RegenPri;
		tPriorityMapEntry.u2VlanId = pClsMapEntry->pPriorityMapPtr->u2VlanId;
		tPriorityMapEntry.u4HwFilterId = pClsMapEntry->pPriorityMapPtr->u4HwFilterId;
		tPriorityMapEntry.u4IfIndex = pClsMapEntry->pPriorityMapPtr->u4IfIndex;
		tPriorityMapEntry.u4QoSPriorityMapId = pClsMapEntry->pPriorityMapPtr->u4QoSPriorityMapId;
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"pClsMapEntry->pL2FilterPtr\r\n");
	if(pClsMapEntry->pL2FilterPtr!= NULL)
	{
		tEnetHal_L2FilterEntry	tL2FilterEntry;
		memset(&tL2FilterEntry,0,sizeof(tEnetHal_L2FilterEntry));
		pEnetClsMapEntry->pL2FilterPtr = &tL2FilterEntry;

		tL2FilterEntry.filterNoId = pClsMapEntry->pL2FilterPtr->i4IssL2FilterNo;
		tL2FilterEntry.filterPriority = pClsMapEntry->pL2FilterPtr->i4IssL2FilterPriority;
		tL2FilterEntry.filterProtocolType = pClsMapEntry->pL2FilterPtr->u4IssL2FilterProtocolType;
		tL2FilterEntry.filterCustomerVlanId = pClsMapEntry->pL2FilterPtr->u4IssL2FilterCustomerVlanId;
		tL2FilterEntry.filterServiceVlanId = pClsMapEntry->pL2FilterPtr->u4IssL2FilterServiceVlanId;
		tL2FilterEntry.filterMatchCount = pClsMapEntry->pL2FilterPtr->u4IssL2FilterMatchCount;
		tL2FilterEntry.filterRedirectPort = pClsMapEntry->pL2FilterPtr->u4IssL2FilterRedirectPort;
		tL2FilterEntry.u4StatsTransitFlag = pClsMapEntry->pL2FilterPtr->u4StatsTransitFlag;

		tL2FilterEntry.FilterAction = pClsMapEntry->pL2FilterPtr->IssL2FilterAction;
		adap_mac_from_arr(&tL2FilterEntry.L2FilterDstMacAddr, pClsMapEntry->pL2FilterPtr->IssL2FilterDstMacAddr);
		adap_mac_from_arr(&tL2FilterEntry.L2FilterSrcMacAddr, pClsMapEntry->pL2FilterPtr->IssL2FilterSrcMacAddr);
		tL2FilterEntry.u2InnerEtherType = pClsMapEntry->pL2FilterPtr->u2InnerEtherType;
		tL2FilterEntry.u2OuterEtherType = pClsMapEntry->pL2FilterPtr->u2OuterEtherType;
		tL2FilterEntry.FilterCVlanPriority = pClsMapEntry->pL2FilterPtr->i1IssL2FilterCVlanPriority;
		tL2FilterEntry.FilterSVlanPriority = pClsMapEntry->pL2FilterPtr->i1IssL2FilterSVlanPriority;
		tL2FilterEntry.FilterTagType = pClsMapEntry->pL2FilterPtr->u1IssL2FilterTagType;
		tL2FilterEntry.L2FilterStatus = pClsMapEntry->pL2FilterPtr->u1IssL2FilterStatus;
		tL2FilterEntry.CfiDei = pClsMapEntry->pL2FilterPtr->i1CfiDei;
		tL2FilterEntry.DropPrecedence = pClsMapEntry->pL2FilterPtr->i1DropPrecedence;
		tL2FilterEntry.FilterUserPriority = pClsMapEntry->pL2FilterPtr->u1IssL2FilterUserPriority;
		tL2FilterEntry.L2SubAction = pClsMapEntry->pL2FilterPtr->u1IssL2SubAction;
		tL2FilterEntry.L2SubActionId = pClsMapEntry->pL2FilterPtr->u2IssL2SubActionId;
		tL2FilterEntry.RedirEgressIfIndex = pClsMapEntry->pL2FilterPtr->u4IssL2RedirectId;


		for(IssPort = 1; IssPort <= ISS_MAX_PORTS; IssPort++)
		{
			ISS_IS_MEMBER_OF_PORTLIST (pClsMapEntry->pL2FilterPtr->IssL2FilterInPortList, IssPort, IssPortExists);
			if (IssPortExists == ISS_TRUE)
			{
				tL2FilterEntry.InPortList[IssPort] = 1;
			}
			else
			{
				tL2FilterEntry.InPortList[IssPort] = 0;
			}
		}
		for(IssPort = 1; IssPort <= ISS_MAX_PORTS; IssPort++)
		{
			ISS_IS_MEMBER_OF_PORTLIST (pClsMapEntry->pL2FilterPtr->IssL2FilterOutPortList, IssPort, IssPortExists);
			if (IssPortExists == ISS_TRUE)
			{
				tL2FilterEntry.OutPortList[IssPort] = 1;
			}
			else
			{
				tL2FilterEntry.OutPortList[IssPort] = 0;
			}
		}
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"pClsMapEntry->pL3FilterPtr:%p\r\n", pClsMapEntry->pL3FilterPtr);
	if(pClsMapEntry->pL3FilterPtr!= NULL)
	{
		tEnetHal_L3FilterEntry	tL3FilterEntry;
		memset(&tL3FilterEntry,0,sizeof(tEnetHal_L3FilterEntry));
		pEnetClsMapEntry->pL3FilterPtr = &tL3FilterEntry;

		tL3FilterEntry.filterNoId = pClsMapEntry->pL3FilterPtr->i4IssL3FilterNo;
		tL3FilterEntry.filterPriority = pClsMapEntry->pL3FilterPtr->i4IssL3FilterPriority;
		tL3FilterEntry.L3FilterProtocol = pClsMapEntry->pL3FilterPtr->IssL3FilterProtocol;
		tL3FilterEntry.FilterMessageType = pClsMapEntry->pL3FilterPtr->i4IssL3FilterMessageType;
		tL3FilterEntry.FilterMessageCode = pClsMapEntry->pL3FilterPtr->i4IssL3FilterMessageCode;
		tL3FilterEntry.CVlanId = pClsMapEntry->pL3FilterPtr->u4CVlanId;
		tL3FilterEntry.SVlanId = pClsMapEntry->pL3FilterPtr->u4SVlanId;
		tL3FilterEntry.FilterDstIpAddr = pClsMapEntry->pL3FilterPtr->u4IssL3FilterDstIpAddr;
		tL3FilterEntry.FilterSrcIpAddr = pClsMapEntry->pL3FilterPtr->u4IssL3FilterSrcIpAddr;
		tL3FilterEntry.FilterDstIpAddrMask = pClsMapEntry->pL3FilterPtr->u4IssL3FilterDstIpAddrMask;
		tL3FilterEntry.FilterSrcIpAddrMask = pClsMapEntry->pL3FilterPtr->u4IssL3FilterSrcIpAddrMask;
		tL3FilterEntry.FilterMinDstProtPort = pClsMapEntry->pL3FilterPtr->u4IssL3FilterMinDstProtPort;
		tL3FilterEntry.FilterMaxDstProtPort = pClsMapEntry->pL3FilterPtr->u4IssL3FilterMaxDstProtPort;
		tL3FilterEntry.FilterMinSrcProtPort = pClsMapEntry->pL3FilterPtr->u4IssL3FilterMinSrcProtPort;
		tL3FilterEntry.FilterMaxSrcProtPort = pClsMapEntry->pL3FilterPtr->u4IssL3FilterMaxSrcProtPort;
		tL3FilterEntry.FilterRedirectPort = pClsMapEntry->pL3FilterPtr->u4IssL3FilterRedirectPort;
		tL3FilterEntry.FilterTos = pClsMapEntry->pL3FilterPtr->IssL3FilterTos;
		tL3FilterEntry.FilterDscp = pClsMapEntry->pL3FilterPtr->i4IssL3FilterDscp;
		tL3FilterEntry.FilterAction = pClsMapEntry->pL3FilterPtr->IssL3FilterAction;

		tL3FilterEntry.CVlanPriority = pClsMapEntry->pL3FilterPtr->i1CVlanPriority;
		tL3FilterEntry.SVlanPriority = pClsMapEntry->pL3FilterPtr->i1SVlanPriority;
		tL3FilterEntry.FilterTagType = pClsMapEntry->pL3FilterPtr->u1IssL3FilterTagType;
		tL3FilterEntry.L3FilterStatus = pClsMapEntry->pL3FilterPtr->u1IssL3FilterStatus;
		tL3FilterEntry.L3SubAction = pClsMapEntry->pL3FilterPtr->u1IssL3SubAction;
		tL3FilterEntry.L3SubActionId = pClsMapEntry->pL3FilterPtr->u2IssL3SubActionId;
		tL3FilterEntry.RedirEgressIfIndex = pClsMapEntry->pL3FilterPtr->u4IssL3RedirectId;
		//tL3FilterEntry.ipv6DstIpAddress = pClsMapEntry->pL3FilterPtr->ipv6DstIpAddress;
		//tL3FilterEntry.ipv6SrcIpAddress = pClsMapEntry->pL3FilterPtr->ipv6SrcIpAddress;

		for(IssPort = 1; IssPort <= ISS_MAX_PORTS; IssPort++)
		{
			ISS_IS_MEMBER_OF_PORTLIST (pClsMapEntry->pL3FilterPtr->IssL3FilterInPortList, IssPort, IssPortExists);
			if (IssPortExists == ISS_TRUE)
			{
				tL3FilterEntry.InPortList[IssPort] = 1;
			}
			else
			{
				tL3FilterEntry.InPortList[IssPort] = 0;
			}
		}
		for(IssPort = 1; IssPort <= ISS_MAX_PORTS; IssPort++)
		{
			ISS_IS_MEMBER_OF_PORTLIST (pClsMapEntry->pL3FilterPtr->IssL3FilterOutPortList, IssPort, IssPortExists);
			if (IssPortExists == ISS_TRUE)
			{
				tL3FilterEntry.OutPortList[IssPort] = 1;
			}
			else
			{
				tL3FilterEntry.OutPortList[IssPort] = 0;
			}
		}
	}

}
static void convertPolicyMapEntry(tQoSPolicyMapEntry * pPlyMapEntry,tEnetHal_QoSPolicyMapEntry *pEnetPlyMapEntry)
{
	pEnetPlyMapEntry->DefaultPHB = pPlyMapEntry->u1DefaultPHB;
	pEnetPlyMapEntry->HwExpMapId = pPlyMapEntry->i4HwExpMapId;
	pEnetPlyMapEntry->IfIndex = pPlyMapEntry->i4IfIndex;
	pEnetPlyMapEntry->PHBType = pPlyMapEntry->u1PHBType;
	pEnetPlyMapEntry->PolicyMapStatus = pPlyMapEntry->u1PolicyMapStatus;
	pEnetPlyMapEntry->QoSClass = pPlyMapEntry->u4QoSClass;
	pEnetPlyMapEntry->QoSMeterId = pPlyMapEntry->i4QoSMeterId;
	pEnetPlyMapEntry->QoSPolicyMapId = pPlyMapEntry->i4QoSPolicyMapId;
}
static void convertInProfileActionEntry(tQoSInProfileActionEntry * pInProActEntry,tEnetHal_QoSInProfileActionEntry *pEnetInProActEntry)
{
	pEnetInProActEntry->QoSInProfConfActFlag = pInProActEntry->u4QoSInProfConfActFlag;
	pEnetInProActEntry->QoSInProfConfNewClass = pInProActEntry->u4QoSInProfConfNewClass;
	pEnetInProActEntry->QoSInProfConfTrafficClass = pInProActEntry->u4QoSInProfConfTrafficClass;
	pEnetInProActEntry->QoSInProfExcActFlag = pInProActEntry->u4QoSInProfExcActFlag;
	pEnetInProActEntry->QoSInProfExcNewClass = pInProActEntry->u4QoSInProfExcNewClass;
	pEnetInProActEntry->QoSInProfExcTrafficClass = pInProActEntry->u4QoSInProfExcTrafficClass;
	pEnetInProActEntry->QoSInProfileActionDscp = pInProActEntry->u4QoSInProfileActionDscp;
	pEnetInProActEntry->QoSInProfileActionInnerVlanDE = pInProActEntry->u4QoSInProfileActionInnerVlanDE;
	pEnetInProActEntry->QoSInProfileActionInnerVlanPrio = pInProActEntry->u4QoSInProfileActionInnerVlanPrio;
	pEnetInProActEntry->QoSInProfileActionIpTOS = pInProActEntry->u4QoSInProfileActionIpTOS;
	pEnetInProActEntry->QoSInProfileActionMplsExp = pInProActEntry->u4QoSInProfileActionMplsExp;
	pEnetInProActEntry->QoSInProfileActionPort = pInProActEntry->u4QoSInProfileActionPort;
	pEnetInProActEntry->QoSInProfileActionVlanDE = pInProActEntry->u4QoSInProfileActionVlanDE;
	pEnetInProActEntry->QoSInProfileExceedActionInnerVlanPrio = pInProActEntry->u4QoSInProfileActionVlanPrio;
	pEnetInProActEntry->QoSInProfileExceedActionIpTOS = pInProActEntry->u4QoSInProfileExceedActionIpTOS;
	pEnetInProActEntry->QoSInProfileExceedActionMplsExp = pInProActEntry->u4QoSInProfileExceedActionMplsExp;
	pEnetInProActEntry->QoSInProfileExceedActionVlanDE = pInProActEntry->u4QoSInProfileExceedActionVlanDE;
	pEnetInProActEntry->QoSInProfileExceedActionVlanPrio = pInProActEntry->u4QoSInProfileExceedActionVlanPrio;
}
static void convertOutProfileActionEntry(tQoSOutProfileActionEntry * pOutProActEntry,tEnetHal_QoSOutProfileActionEntry *pEnetOutProActEntry)
{
	pEnetOutProActEntry->QoSOutProfileActionDscp = pOutProActEntry->u4QoSOutProfileActionDscp;
	pEnetOutProActEntry->QoSOutProfileActionFlag = pOutProActEntry->u4QoSOutProfileActionFlag;
	pEnetOutProActEntry->QoSOutProfileActionInnerVlanDE = pOutProActEntry->u4QoSOutProfileActionInnerVlanDE;
	pEnetOutProActEntry->QoSOutProfileActionInnerVlanPrio = pOutProActEntry->u4QoSOutProfileActionInnerVlanPrio;
	pEnetOutProActEntry->QoSOutProfileActionIpTOS = pOutProActEntry->u4QoSOutProfileActionIpTOS;
	pEnetOutProActEntry->QoSOutProfileActionMplsExp = pOutProActEntry->u4QoSOutProfileActionMplsExp;
	pEnetOutProActEntry->QoSOutProfileActionVlanDE = pOutProActEntry->u4QoSOutProfileActionVlanDE;
	pEnetOutProActEntry->QoSOutProfileActionVlanPrio = pOutProActEntry->u4QoSOutProfileActionVlanPrio;
	pEnetOutProActEntry->QoSOutProfileNewClass = pOutProActEntry->u4QoSOutProfileNewClass;
	pEnetOutProActEntry->QoSOutProfileTrafficClass = pOutProActEntry->u4QoSOutProfileTrafficClass;
}

static void convertSchedulerEntry(tQoSSchedulerEntry *pSchedEntry,tEnetHal_QoSSchedulerEntry *pEnetSchedEntry)
{
	if(pSchedEntry->pShapePtr != NULL)
	{
		(pEnetSchedEntry->pShapePtr)->u2ShaperId = (pSchedEntry->pShapePtr)->u2ShaperId;
		(pEnetSchedEntry->pShapePtr)->u4QosCBS = (pSchedEntry->pShapePtr)->u4QosCBS;
		(pEnetSchedEntry->pShapePtr)->u4QosCIR = (pSchedEntry->pShapePtr)->u4QosCIR;
		(pEnetSchedEntry->pShapePtr)->u4QosEBS = (pSchedEntry->pShapePtr)->u4QosEBS;
		(pEnetSchedEntry->pShapePtr)->u4QosEIR = (pSchedEntry->pShapePtr)->u4QosEIR;
	}

	pEnetSchedEntry->QosSchedulerStatus = pSchedEntry->u1QosSchedulerStatus;
	pEnetSchedEntry->QosSchedulerId = pSchedEntry->i4QosSchedulerId;
	pEnetSchedEntry->QosSchedHwId = pSchedEntry->u4QosSchedHwId;
	pEnetSchedEntry->QosSchedChildren = pSchedEntry->u4QosSchedChildren;
	pEnetSchedEntry->QosSchedAlgo = pSchedEntry->u1QosSchedAlgo;
	pEnetSchedEntry->QosIfIndex = pSchedEntry->i4QosIfIndex;
	pEnetSchedEntry->HL = pSchedEntry->u1HL;
	pEnetSchedEntry->Flag = pSchedEntry->u1Flag;

}

static void convertQTypeEntry(tQoSQtypeEntry *pQTypeEntry,tEnetHal_QoSQtypeEntry *pEnetQTypeEntry)
{
	pEnetQTypeEntry->u4QueueTypeId = pQTypeEntry->u4QueueTypeId;
	pEnetQTypeEntry->u4QueueSize = pQTypeEntry->u4QueueSize;
	pEnetQTypeEntry->u1DropAlgo = pQTypeEntry->u1DropAlgo;
	pEnetQTypeEntry->u1DropAlgoEnableFlag = pQTypeEntry->u1DropAlgoEnableFlag;
}

static void convertQEntry(tQoSQEntry *pQEntry,tEnetHal_QoSQEntry *pEnetQEntry)
{
	pEnetQEntry->u4QueueType = pQEntry->u4QueueType;
	pEnetQEntry->u2QosQWeight = pQEntry->u2QosQWeight;
	pEnetQEntry->u1QosQStatus = pQEntry->u1QosQStatus;
	pEnetQEntry->u1QosQPriority = pQEntry->u1QosQPriority;
	pEnetQEntry->i4QosSchedulerId = pQEntry->i4QosSchedulerId;
	pEnetQEntry->i4QosQueueHwId = pQEntry->i4QosQueueHwId;
	pEnetQEntry->i4QosQId = pQEntry->i4QosQId;

	if(pQEntry->pSchedPtr != NULL)
	{
		pEnetQEntry->pSchedPtr->QosIfIndex = pQEntry->pSchedPtr->i4QosIfIndex;
		pEnetQEntry->pSchedPtr->QosSchedulerId = pQEntry->pSchedPtr->i4QosSchedulerId;
		pEnetQEntry->pSchedPtr->Flag = pQEntry->pSchedPtr->u1Flag;
		pEnetQEntry->pSchedPtr->HL = pQEntry->pSchedPtr->u1HL;
		pEnetQEntry->pSchedPtr->QosSchedAlgo = pQEntry->pSchedPtr->u1QosSchedAlgo;
		pEnetQEntry->pSchedPtr->QosSchedulerStatus = pQEntry->pSchedPtr->u1QosSchedulerStatus;
		pEnetQEntry->pSchedPtr->QosSchedChildren = pQEntry->pSchedPtr->u4QosSchedChildren;
		pEnetQEntry->pSchedPtr->QosSchedHwId = pQEntry->pSchedPtr->u4QosSchedHwId;
		if(pQEntry->pSchedPtr->pShapePtr != NULL)
		{
			pEnetQEntry->pSchedPtr->pShapePtr->u2ShaperId = pQEntry->pSchedPtr->pShapePtr->u2ShaperId;
			pEnetQEntry->pSchedPtr->pShapePtr->u4QosCBS = pQEntry->pSchedPtr->pShapePtr->u4QosCBS;
			pEnetQEntry->pSchedPtr->pShapePtr->u4QosCIR = pQEntry->pSchedPtr->pShapePtr->u4QosCIR;
			pEnetQEntry->pSchedPtr->pShapePtr->u4QosEBS = pQEntry->pSchedPtr->pShapePtr->u4QosEBS;
			pEnetQEntry->pSchedPtr->pShapePtr->u4QosEIR = pQEntry->pSchedPtr->pShapePtr->u4QosEIR;
		}
	}

	if(pQEntry->pShapePtr != NULL)
	{
		pEnetQEntry->pShapePtr->u2ShaperId = pQEntry->pShapePtr->u2ShaperId;
		pEnetQEntry->pShapePtr->u4QosCBS = pQEntry->pShapePtr->u4QosCBS;
		pEnetQEntry->pShapePtr->u4QosCIR = pQEntry->pShapePtr->u4QosCIR;
		pEnetQEntry->pShapePtr->u4QosEBS = pQEntry->pShapePtr->u4QosEBS;
		pEnetQEntry->pShapePtr->u4QosEIR = pQEntry->pShapePtr->u4QosEIR;

	}
}

static void convertQoSREDEntry(UINT2 wredIndex, tQoSREDCfgEntry *papRDCfgEntry[],tEnetHal_QoSREDCfgEntry *pApRDCfgEntry)
{
	if(papRDCfgEntry[wredIndex]!= NULL)
	{
		pApRDCfgEntry[wredIndex].u1DropPrecedence = papRDCfgEntry[wredIndex]->u1DropPrecedence;
		pApRDCfgEntry[wredIndex].u1DropThreshType = papRDCfgEntry[wredIndex]->u1DropThreshType;
		pApRDCfgEntry[wredIndex].u1ExpWeight = papRDCfgEntry[wredIndex]->u1ExpWeight;
		pApRDCfgEntry[wredIndex].u1MaxProbability = papRDCfgEntry[wredIndex]->u1MaxProbability;
		pApRDCfgEntry[wredIndex].u1RDActionFlag = papRDCfgEntry[wredIndex]->u1RDActionFlag;
		pApRDCfgEntry[wredIndex].u4ECNThresh = papRDCfgEntry[wredIndex]->u4ECNThresh;
		pApRDCfgEntry[wredIndex].u4Gain = papRDCfgEntry[wredIndex]->u4Gain;
		pApRDCfgEntry[wredIndex].u4MaxAvgThresh = papRDCfgEntry[wredIndex]->u4MaxAvgThresh;
		pApRDCfgEntry[wredIndex].u4MaxPktSize = papRDCfgEntry[wredIndex]->u4MaxPktSize;
		pApRDCfgEntry[wredIndex].u4MinAvgThresh = papRDCfgEntry[wredIndex]->u4MinAvgThresh;

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"convertQoSREDEntry - wredIndex: %d MinThresh:%d Max Thresh:%d Prob:%d ECN_Enable:%d ECN_Thresh:%d \n",
                wredIndex,
				pApRDCfgEntry[wredIndex].u4MinAvgThresh,
				pApRDCfgEntry[wredIndex].u4MaxAvgThresh,
				pApRDCfgEntry[wredIndex].u1MaxProbability,
				pApRDCfgEntry[wredIndex].u1RDActionFlag,
				pApRDCfgEntry[wredIndex].u4ECNThresh);
	}
}

static void convertQosHwMapClassToPriority(tQosClassToPriMapEntry *pQosClassToPriMapEntry,tEnetHal_QosClassToPriMapEntry *pEnetHal_QoSClassToIntPriEntry)
{
	pEnetHal_QoSClassToIntPriEntry->i4IfIndex = pQosClassToPriMapEntry->i4IfIndex;
	pEnetHal_QoSClassToIntPriEntry->i4HwExpMapId = pQosClassToPriMapEntry->i4HwExpMapId;
	pEnetHal_QoSClassToIntPriEntry->u4HwFilterId = pQosClassToPriMapEntry->u4HwFilterId;
	pEnetHal_QoSClassToIntPriEntry->u2VlanId = pQosClassToPriMapEntry->u2VlanId;
	pEnetHal_QoSClassToIntPriEntry->u1PriType = pQosClassToPriMapEntry->u1PriType;
	pEnetHal_QoSClassToIntPriEntry->u1Priority = pQosClassToPriMapEntry->u1Priority;
	pEnetHal_QoSClassToIntPriEntry->u1RegenPriority = pQosClassToPriMapEntry->u1RegenPriority;
	pEnetHal_QoSClassToIntPriEntry->u1Flag = pQosClassToPriMapEntry->u1Flag;
	pEnetHal_QoSClassToIntPriEntry->i1DEI = pQosClassToPriMapEntry->i1DEI;
	pEnetHal_QoSClassToIntPriEntry->i1Color = pQosClassToPriMapEntry->i1Color;
}

#endif
/*****************************************************************************/
/* Function Name      : QoSHwInit                                            */
/*                                                                           */
/* Description        : This function intialises the QoS Engine in the       */
/*                       Hardware device                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwInit (VOID)
{
	INT4 ret = FNP_SUCCESS;
#ifdef USER_HW_API		
	ret = EnetHal_QoSHwInit ();
#endif	
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwMapClassToPolicy                                */
/*                                                                           */
/* Description        : This function used to configure the set traffic into */
/*                       a CLASS and Policy for it.                          */
/*                                                                           */
/* Input(s)           : pClsMapEntry -  Ptr for a ClassMapEntry              */
/*                    : pPlyMapEntry -  Ptr for a PolicyMapEntry             */
/*                    : pInProActEntry -Ptr for a In Profile Action Entry    */
/*                    : pOutProActEntry-Ptr for a Out Profile Action Entry   */
/*                    : pMeterEntry    -Ptr for a Meter entry                */
/*                    : u1Flag         -  QOS_PLY_ADD / QOS_PLY_MAP          */
/*                      QOS_PLY_ADD - Policy needs to be created and map     */
/*                                    the Class and Policy                   */
/*                      QOS_PLY_MAP - Map the Class with already created     */
/*                                    Policy.                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwMapClassToPolicy (tQoSClassMapEntry * pClsMapEntry,
                       tQoSPolicyMapEntry * pPlyMapEntry,
                       tQoSInProfileActionEntry * pInProActEntry,
                       tQoSOutProfileActionEntry * pOutProActEntry,
                       tQoSMeterEntry * pMeterEntry, UINT1 u1Flag)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (pClsMapEntry);
    UNUSED_PARAM (pPlyMapEntry);
    UNUSED_PARAM (pInProActEntry);
    UNUSED_PARAM (pOutProActEntry);
    UNUSED_PARAM (pMeterEntry);
    UNUSED_PARAM (u1Flag);
    if(pClsMapEntry != NULL)
    {
    	printf("QoSHwMapClassToPolicy CLASS params - u4QoSMFClass:%d PPP session id %d\n",
    			pClsMapEntry->u4QoSMFClass, pClsMapEntry->u2PPPSessId);
    }
	if(pPlyMapEntry != NULL)
	{
		printf("QoSHwMapClassToPolicy POLICY params - i4IfIndex:%d i4QoSMeterId:%d i4QoSPolicyMapId:%d u4QoSClass:%d \n",
				pPlyMapEntry->i4IfIndex, pPlyMapEntry->i4QoSMeterId, pPlyMapEntry->i4QoSPolicyMapId, pPlyMapEntry->u4QoSClass);
	}
	if(pMeterEntry != NULL)
	{
		printf("QoSHwMapClassToPolicy METER params - i4QoSMeterId:%d u1QoSMeterType:%d u4QoSCBS:%d u4QoSCIR:%d u4QoSEBS:%d u4QoSEIR:%d u4QoSMeterFlag:%d\n",
				pMeterEntry->i4QoSMeterId, pMeterEntry->u1QoSMeterType, pMeterEntry->u4QoSCBS, pMeterEntry->u4QoSCIR, pMeterEntry->u4QoSEBS, pMeterEntry->u4QoSEIR, pMeterEntry->u4QoSMeterFlag);

	}

#ifdef USER_HW_API		
	tEnetHal_QoSClassMapEntry tClsMapEntry;
	tEnetHal_QoSPolicyMapEntry tPlyMapEntry;
	tEnetHal_QoSInProfileActionEntry tInProActEntry;
	tEnetHal_QoSOutProfileActionEntry tOutProActEntry;
	tEnetHal_QoSMeterEntry tMeterEntry;

	memset(&tClsMapEntry,0,sizeof(tClsMapEntry));
	memset(&tPlyMapEntry,0,sizeof(tPlyMapEntry));
	memset(&tInProActEntry,0,sizeof(tInProActEntry));
	memset(&tOutProActEntry,0,sizeof(tOutProActEntry));
	memset(&tMeterEntry,0,sizeof(tMeterEntry));

	if(pClsMapEntry != NULL)
	{
		convertClassMapEntry(pClsMapEntry,&tClsMapEntry);
	}
	if(pPlyMapEntry != NULL)
	{
		convertPolicyMapEntry(pPlyMapEntry,&tPlyMapEntry);
	}
	if(pInProActEntry != NULL)
	{
		convertInProfileActionEntry(pInProActEntry,&tInProActEntry);
	}
	if(pOutProActEntry != NULL)
	{
		convertOutProfileActionEntry(pOutProActEntry,&tOutProActEntry);
	}
	if(pMeterEntry != NULL)
	{
		convertMeterEntry(pMeterEntry,&tMeterEntry);
	}

	ret = EnetHal_QoSHwMapClassToPolicy ( (pClsMapEntry == NULL) ? NULL : &tClsMapEntry,
			(pPlyMapEntry == NULL) ? NULL : &tPlyMapEntry,
			(pInProActEntry == NULL) ? NULL : &tInProActEntry,
			(pOutProActEntry == NULL) ? NULL : &tOutProActEntry,
			(pMeterEntry == NULL) ? NULL : &tMeterEntry,
			u1Flag);
#endif
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwUpdatePolicyMapForClass                         */
/*                                                                           */
/* Description        : This function used to update the Policy Parameters   */
/*                      for a CLASS of traffic.                              */
/*                                                                           */
/* Input(s)           : pClsMapEntry -  Ptr for a ClassMapEntry              */
/*                    : pPlyMapEntry -  Ptr for a PolicyMapEntry             */
/*                    : pInProActEntry -Ptr for a In Profile Action Entry    */
/*                    : pOutProActEntry-Ptr for a Out Profile Action Entry   */
/*                    : pMeterEntry    -Ptr for a Meter entry                */
/*                    : u1Flag         -Updated Field in the In pPlyMapEntry,*/
/*                                      pInProActEntry, pOutProActEntry      */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwUpdatePolicyMapForClass (tQoSClassMapEntry * pClsMapEntry,
                              tQoSPolicyMapEntry * pPlyMapEntry,
                              tQoSInProfileActionEntry * pInProActEntry,
                              tQoSOutProfileActionEntry * pOutProActEntry,
                              tQoSMeterEntry * pMeterEntry, UINT1 u1Flag)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (pInProActEntry);
    UNUSED_PARAM (pOutProActEntry);
    UNUSED_PARAM (u1Flag);
    if(pClsMapEntry != NULL)
    {
    	printf("QoSHwUpdatePolicyMapForClass CLASS params - u4QoSMFClass:%d, pPriorityMapPtr:%p, pL2FilterPtr%p, pL3FilterPtr:%p\n",
    			pClsMapEntry->u4QoSMFClass, pClsMapEntry->pPriorityMapPtr, pClsMapEntry->pL2FilterPtr, pClsMapEntry->pL3FilterPtr);
    }
	if(pPlyMapEntry != NULL)
	{
		printf("QoSHwUpdatePolicyMapForClass POLICY params - i4IfIndex:%d i4QoSMeterId:%d i4QoSPolicyMapId:%d u4QoSClass:%d \n",
				pPlyMapEntry->i4IfIndex, pPlyMapEntry->i4QoSMeterId, pPlyMapEntry->i4QoSPolicyMapId, pPlyMapEntry->u4QoSClass);
	}
	if(pMeterEntry != NULL)
	{
		printf("QoSHwUpdatePolicyMapForClass METER params - i4QoSMeterId:%d u1QoSMeterType:%d u4QoSCBS:%d u4QoSCIR:%d u4QoSEBS:%d u4QoSEIR:%d u4QoSMeterFlag:%d\n",
				pMeterEntry->i4QoSMeterId, pMeterEntry->u1QoSMeterType, pMeterEntry->u4QoSCBS, pMeterEntry->u4QoSCIR, pMeterEntry->u4QoSEBS, pMeterEntry->u4QoSEIR, pMeterEntry->u4QoSMeterFlag);

	}
#ifdef USER_HW_API		
	tEnetHal_QoSClassMapEntry tClsMapEntry;
	tEnetHal_QoSPolicyMapEntry tPlyMapEntry;
	tEnetHal_QoSInProfileActionEntry tInProActEntry;
	tEnetHal_QoSOutProfileActionEntry tOutProActEntry;
	tEnetHal_QoSMeterEntry tMeterEntry;

	memset(&tClsMapEntry,0,sizeof(tClsMapEntry));
	memset(&tPlyMapEntry,0,sizeof(tPlyMapEntry));
	memset(&tInProActEntry,0,sizeof(tInProActEntry));
	memset(&tOutProActEntry,0,sizeof(tOutProActEntry));
	memset(&tMeterEntry,0,sizeof(tMeterEntry));

	if(pClsMapEntry != NULL)
	{
		convertClassMapEntry(pClsMapEntry,&tClsMapEntry);
	}
	if(pPlyMapEntry != NULL)
	{
		convertPolicyMapEntry(pPlyMapEntry,&tPlyMapEntry);
	}
	if(pInProActEntry != NULL)
	{
		convertInProfileActionEntry(pInProActEntry,&tInProActEntry);
	}
	if(pOutProActEntry != NULL)
	{
		convertOutProfileActionEntry(pOutProActEntry,&tOutProActEntry);
	}
	if(pMeterEntry != NULL)
	{
		convertMeterEntry(pMeterEntry,&tMeterEntry);
	}

	ret = EnetHal_QoSHwUpdatePolicyMapForClass ((pClsMapEntry == NULL) ? NULL : &tClsMapEntry,
			(pPlyMapEntry == NULL) ? NULL : &tPlyMapEntry,
			(pInProActEntry == NULL) ? NULL : &tInProActEntry,
			(pOutProActEntry == NULL) ? NULL : &tOutProActEntry,
			(pMeterEntry == NULL) ? NULL : &tMeterEntry,
			u1Flag);
#endif	
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwUnmapClassFromPolicy                            */
/*                                                                           */
/* Description        : This function is used to Remove the Policy for a     */
/*                      CLASS of traffic.                                    */
/*                                                                           */
/* Input(s)           : pClsMapEntry -  Ptr for a ClassMapEntry              */
/*                    : pPlyMapEntry -  Ptr for a PolicyMapEntry             */
/*                    : pMeterEntry    -Ptr for a Meter entry                */
/*                    : u1Flag     -  QOS_PLY_UNMAP / QOS_PLY_DEL            */
/*                      QOS_PLY_DEL  -  Unmap Class from Policy and delete   */
/*                                      the Policy                           */
/*                      QOS_PLY_UNMAP - Unmap the Class from Policy.         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwUnmapClassFromPolicy (tQoSClassMapEntry * pClsMapEntry,
                           tQoSPolicyMapEntry * pPlyMapEntry,
                           tQoSMeterEntry * pMeterEntry, UINT1 u1Flag)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (pClsMapEntry);
    UNUSED_PARAM (pPlyMapEntry);
    UNUSED_PARAM (pMeterEntry);
    UNUSED_PARAM (u1Flag);	
    if(pClsMapEntry != NULL)
    {
    	printf("QoSHwUnmapClassFromPolicy CLASS params - u4QoSMFClass:%d pPriorityMapPtr:%p, pL2FilterPtr%p, pL3FilterPtr:%p\n",
    			pClsMapEntry->u4QoSMFClass, pClsMapEntry->pPriorityMapPtr, pClsMapEntry->pL2FilterPtr, pClsMapEntry->pL3FilterPtr);
    }
	if(pPlyMapEntry != NULL)
	{
		printf("QoSHwUnmapClassFromPolicy POLICY params - i4IfIndex:%d i4QoSMeterId:%d i4QoSPolicyMapId:%d u4QoSClass:%d \n",
				pPlyMapEntry->i4IfIndex, pPlyMapEntry->i4QoSMeterId, pPlyMapEntry->i4QoSPolicyMapId, pPlyMapEntry->u4QoSClass);
	}
	if(pMeterEntry != NULL)
	{
		printf("QoSHwUnmapClassFromPolicy METER params - i4QoSMeterId:%d u1QoSMeterType:%d u4QoSCBS:%d u4QoSCIR:%d u4QoSEBS:%d u4QoSEIR:%d u4QoSMeterFlag:%d\n",
				pMeterEntry->i4QoSMeterId, pMeterEntry->u1QoSMeterType, pMeterEntry->u4QoSCBS, pMeterEntry->u4QoSCIR, pMeterEntry->u4QoSEBS, pMeterEntry->u4QoSEIR, pMeterEntry->u4QoSMeterFlag);

	}
	
#ifdef USER_HW_API	
	tEnetHal_QoSClassMapEntry tClsMapEntry;
	tEnetHal_QoSPolicyMapEntry tPlyMapEntry;
	tEnetHal_QoSMeterEntry tMeterEntry;

	memset(&tClsMapEntry,0,sizeof(tClsMapEntry));
	memset(&tPlyMapEntry,0,sizeof(tPlyMapEntry));
	memset(&tMeterEntry,0,sizeof(tMeterEntry));

	if(pClsMapEntry != NULL)
	{
		convertClassMapEntry(pClsMapEntry,&tClsMapEntry);
	}

	if(pPlyMapEntry != NULL)
	{
		convertPolicyMapEntry(pPlyMapEntry,&tPlyMapEntry);
	}
	if(pMeterEntry != NULL)
	{
		convertMeterEntry(pMeterEntry,&tMeterEntry);
	}

	ret =  EnetHal_QoSHwUnmapClassFromPolicy ((pClsMapEntry == NULL) ? NULL : &tClsMapEntry,
			(pPlyMapEntry == NULL) ? NULL : &tPlyMapEntry,
			(pMeterEntry == NULL) ? NULL : &tMeterEntry,
			u1Flag);
#endif
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwDeleteClassMapEntry                             */
/*                                                                           */
/* Description        : This function Delete the Filter From a CLASS         */
/*                                                                           */
/* Input(s)           : pClsMapEntry -  Ptr for a ClassMapEntry              */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwDeleteClassMapEntry (tQoSClassMapEntry * pClsMapEntry)
{
	
	INT4 ret=FNP_SUCCESS;

    UNUSED_PARAM (pClsMapEntry);
	if(pClsMapEntry != NULL)
	{
		printf("QoSHwDeleteClassMapEntry u4QoSMFClass:%d pClsMapEntry:%p\n", pClsMapEntry->u4QoSMFClass, pClsMapEntry);
	}
#ifdef USER_HW_API
	tEnetHal_QoSClassMapEntry tClsMapEntry;
	
	memset (&tClsMapEntry, 0, sizeof(tEnetHal_QoSClassMapEntry));
	if(pClsMapEntry != NULL)
	{
		convertClassMapEntry(pClsMapEntry,&tClsMapEntry);
	}

	ret = EnetHal_QoSHwDeleteClassMapEntry ((pClsMapEntry == NULL) ? NULL : &tClsMapEntry);
#endif
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwMeterCreate                                     */
/*                                                                           */
/* Description        : This function is used to Create Meter in the device  */
/*                                                                           */
/* Input(s)           : pMeterEntry    -Ptr for a Meter entry                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwMeterCreate (tQoSMeterEntry * pMeterEntry)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (pMeterEntry);
	if(pMeterEntry)
	{
		printf("QoSHwMeterCreate for MeterId: %d u1QoSMeterType: %d u4QoSCBS: %d u4QoSCIR: %d u4QoSEBS: %d u4QoSEIR:%d\n",
				pMeterEntry->i4QoSMeterId, pMeterEntry->u1QoSMeterType,	pMeterEntry->u4QoSCBS, pMeterEntry->u4QoSCIR, pMeterEntry->u4QoSEBS, pMeterEntry->u4QoSEIR);
	}
#ifdef USER_HW_API
	tEnetHal_QoSMeterEntry tMeterEntry;

	memset(&tMeterEntry,0,sizeof(tMeterEntry));
	if(pMeterEntry)
	{
		convertMeterEntry(pMeterEntry,&tMeterEntry);
	}

	ret = EnetHal_QoSHwMeterCreate ((pMeterEntry == NULL) ? NULL : &tMeterEntry);
#endif
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwMeterDelete                                     */
/*                                                                           */
/* Description        : This function is used to Delate a  Meter form the    */
/*                      Hardware device.                                     */
/*                                                                           */
/* Input(s)           : i4MeterId  - Meter Id                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwMeterDelete (INT4 i4MeterId)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (i4MeterId);
#ifdef USER_HW_API
	ret = EnetHal_QoSHwMeterDelete (i4MeterId);
#endif
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwSchedulerAdd                                    */
/*                                                                           */
/* Description        : This function is used to create a Scheduler in the   */
/*                      Hardware device.                                     */
/*                                                                           */
/* Input(s)           : pSchedEntry - Ptr to 'tQoSSchedulerEntry'            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwSchedulerAdd (tQoSSchedulerEntry * pSchedEntry)
{
	INT4 ret = FNP_SUCCESS;
	
    UNUSED_PARAM (pSchedEntry);
	if(pSchedEntry != NULL)
	{
		printf("QoSHwSchedulerAdd for Port: %d, SchedId: %d HL:%d flag:%d\n", pSchedEntry->i4QosIfIndex, pSchedEntry->i4QosSchedulerId, pSchedEntry->u1HL, pSchedEntry->u1Flag);
	}
#ifdef USER_HW_API
	tEnetHal_QoSSchedulerEntry tSchedEntry;
	tEnetHal_QoSShapeCfgEntry  tShapePtr;

	memset(&tSchedEntry,0,sizeof(tSchedEntry));
	memset(&tShapePtr,0,sizeof(tShapePtr));
	if(pSchedEntry != NULL)
	{
		tSchedEntry.pShapePtr = &tShapePtr;
		convertSchedulerEntry(pSchedEntry,&tSchedEntry);
	}

	ret = EnetHal_QoSHwSchedulerAdd ((pSchedEntry == NULL) ? NULL : &tSchedEntry);
#endif
	/* ISS Sched data update */
    pSchedEntry->u4QosSchedHwId = pSchedEntry->i4QosSchedulerId;
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwSchedulerUpdateParams                           */
/*                                                                           */
/* Description        : This function is used to Update a Scheduler in the   */
/*                      Hardware device.                                     */
/*                                                                           */
/* Input(s)           : pSchedEntry   - Ptr to 'tQoSSchedulerEntry'          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwSchedulerUpdateParams (tQoSSchedulerEntry * pSchedEntry)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (pSchedEntry);
	if(pSchedEntry != NULL)
	{
	    printf("QoSHwSchedulerUpdateParams i4QosSchedulerId:%d\n", pSchedEntry->i4QosSchedulerId);
	}
#ifdef USER_HW_API
	tEnetHal_QoSSchedulerEntry tSchedEntry;
	tEnetHal_QoSShapeCfgEntry  tShapePtr;

	memset(&tSchedEntry,0,sizeof(tSchedEntry));
	memset(&tShapePtr,0,sizeof(tShapePtr));
	if(pSchedEntry != NULL)
	{
		tSchedEntry.pShapePtr = &tShapePtr;
		convertSchedulerEntry(pSchedEntry,&tSchedEntry);
	}

	ret = EnetHal_QoSHwSchedulerUpdateParams ((pSchedEntry == NULL) ? NULL : &tSchedEntry);
#endif
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwSchedulerDelete                                 */
/*                                                                           */
/* Description        : This function is used to Delete a Scheduler in the   */
/*                      Hardware device.                                     */
/*                                                                           */
/* Input(s)           : i4IfIndex - Interface Index                          */
/*                    : u4SchedId - Scheduler Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwSchedulerDelete (INT4 i4IfIndex, UINT4 u4SchedId)
{
	INT4 ret = FNP_SUCCESS;
#ifdef USER_HW_API
	ret = EnetHal_QoSHwSchedulerDelete (i4IfIndex,u4SchedId);
#endif
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwQueueCreate                                     */
/*                                                                           */
/* Description        : This function is used to Create a Queue in the HW    */
/*                                                                           */
/* Input(s)           : i4IfIndex   - Scheduler and Q Interface.             */
/*                    : u4QId       - Q id                                   */
/*                    : pQEntry     - Ptr to 'tQoSQEntry'                    */
/*                    : pQTypeEntry - Ptr to 'tQoSQtypeEntry'                */
/*                    : papRDCfgEntry - Ptr to  array of ptr type            */
/*                                     'tQoSREDCfgEntry'                     */
/*                                     3 Prts papRDCfgEntry[0-2] for DP      */
/*                                     Low(0), Medium(1), high (2)           */
/*                    : i2HL        - Scheduler Hierarchy level              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwQueueCreate (INT4 i4IfIndex, UINT4 u4QId, tQoSQEntry * pQEntry,
                  tQoSQtypeEntry * pQTypeEntry,
                  tQoSREDCfgEntry * papRDCfgEntry[], INT2 i2HL)
{
	INT4 ret=FNP_SUCCESS;

	if(pQTypeEntry != NULL)
	{
		printf("QoSHwQueueCreate u4QueueTypeId %d, u4QueueSize:%d\n", pQTypeEntry->u4QueueTypeId, pQTypeEntry->u4QueueSize);
	}

	if(pQEntry != NULL)
	{
		printf("QoSHwQueueCreate i4QosQId %d, i4QosSchedulerId:%d, u1QosQPriority:%d, u2QosQWeight:%d, u4QueueType:%d\n",
				pQEntry->i4QosQId, pQEntry->i4QosSchedulerId, pQEntry->u1QosQPriority, pQEntry->u2QosQWeight, pQEntry->u4QueueType);
	}
#ifdef USER_HW_API
	tEnetHal_QoSQEntry tQEntry;
	tEnetHal_QoSQtypeEntry tQTypeEntry;
	tEnetHal_QoSREDCfgEntry *tApRDCfgEntry[3];
	tEnetHal_QoSSchedulerEntry tSchedEntry;
	tEnetHal_QoSShapeCfgEntry  tShapePtr;
	tEnetHal_QoSShapeCfgEntry  tSchedShapePtr;
	UINT2 wredIndex;

	memset(&tQEntry,0,sizeof(tQEntry));
	memset(&tQTypeEntry,0,sizeof(tQTypeEntry));
	memset(&tSchedEntry,0,sizeof(tSchedEntry));
	memset(&tShapePtr,0,sizeof(tShapePtr));
	memset(&tSchedShapePtr,0,sizeof(tSchedShapePtr));

	if(pQTypeEntry != NULL)
	{
		convertQTypeEntry(pQTypeEntry,&tQTypeEntry);
	}

	if(pQEntry != NULL)
	{
		tQEntry.pSchedPtr = &tSchedEntry;
		tQEntry.pShapePtr = &tShapePtr;
		tQEntry.pSchedPtr->pShapePtr = &tSchedShapePtr;
		convertQEntry(pQEntry,&tQEntry);
	}

    for(wredIndex = 0; wredIndex < 3; wredIndex++)
    {
    	tApRDCfgEntry[wredIndex] = NULL;
        if(papRDCfgEntry[wredIndex] != NULL)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"adap_createQueueEntry - wredIndex: %d MinThresh:%d Max Thresh:%d Prob:%d ECN_Enable:%d ECN_Thresh:%d \n",
                    wredIndex,
                    papRDCfgEntry[wredIndex]->u4MinAvgThresh,
                    papRDCfgEntry[wredIndex]->u4MaxAvgThresh,
                    papRDCfgEntry[wredIndex]->u1MaxProbability,
                    papRDCfgEntry[wredIndex]->u1RDActionFlag,
                    papRDCfgEntry[wredIndex]->u4ECNThresh);
        	convertQoSREDEntry(wredIndex, &papRDCfgEntry[wredIndex],tApRDCfgEntry[wredIndex]);
        }
    }

	ret = EnetHal_QoSHwQueueCreate (i4IfIndex,u4QId,(pQEntry == NULL) ? NULL : &tQEntry,
									(pQTypeEntry == NULL) ? NULL : &tQTypeEntry,
									tApRDCfgEntry,i2HL);
#endif	
	pQEntry->i4QosQueueHwId = pQEntry->i4QosQId;
    return ret;

}

/*****************************************************************************/
/* Function Name      : QoSHwQueueDelete                                     */
/*                                                                           */
/* Description        : This function is used to Delete a Queue in the HW    */
/*                                                                           */
/* Input(s)           : i4IfIndex   - Scheduler and Q Interface.             */
/*                    : u4QId       - Q id                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwQueueDelete (INT4 i4IfIndex, UINT4 u4Id)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4Id);
#ifdef USER_HW_API
	ret = EnetHal_QoSHwQueueDelete (i4IfIndex,u4Id);
#endif
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwMapClassToQueue                                 */
/*                                                                           */
/* Description        : This function is used to Map or UnMap a Q and a      */
/*                      Scheduler in the Hardware device.                    */
/*                                                                           */
/* Input(s)           : i4IfIndex   - Scheduler and Q Interface.             */
/*                    : i4ClsOrPriType - Map Type of the 'u4ClsOrPri'        */
/*                               NONE  means CLASS  otherwise                */
/*                               VLAN_PRI/IP_TOS/IP_DSCP/MPLS_EXP/VLAN_DEI   */
/*                    : u4ClsOrPri  - Map Type Value                         */
/*                    : u4QId       - Q id                                   */
/*                    : u1Flag      - Map / Unmap                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwMapClassToQueue (INT4 i4IfIndex, INT4 i4ClsOrPriType, UINT4 u4ClsOrPri,
                      UINT4 u4QId, UINT1 u1Flag)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4ClsOrPriType);
    UNUSED_PARAM (u4ClsOrPri);
    UNUSED_PARAM (u4QId);
    UNUSED_PARAM (u1Flag);
#ifdef USER_HW_API
#endif
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwMapClassToQueueId                               */
/*                                                                           */
/* Description        : This function is used to Map or UnMap a Q and a      */
/*                      Scheduler in the Hardware device.This is the          */
/*                      replication of the function QoSHwMapClassToQueue with*/
/*                      additional paramter pClsMapEntry to get the          */
/*                      hardware handle infromation                          */
/*                                                                           */
/* Input(s)           : i4IfIndex   - Scheduler and Q Interface.             */
/*                    : i4ClsOrPriType - Map Type of the 'u4ClsOrPri'        */
/*                               NONE  means CLASS  otherwise                */
/*                               VLAN_PRI/IP_TOS/IP_DSCP/MPLS_EXP/VLAN_DEI   */
/*                    : u4ClsOrPri  - Map Type Value                         */
/*                    : u4QId       - Q id                                   */
/*                    : u1Flag      - Map / Unmap                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwMapClassToQueueId (tQoSClassMapEntry * pClsMapEntry, INT4 i4IfIndex,
                        INT4 i4ClsOrPriType, UINT4 u4ClsOrPri,
                        UINT4 u4QId, UINT1 u1Flag)
{
	INT4 ret = FNP_SUCCESS;
	
    UNUSED_PARAM (pClsMapEntry);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4ClsOrPriType);
    UNUSED_PARAM (u4ClsOrPri);
    UNUSED_PARAM (u4QId);
    UNUSED_PARAM (u1Flag);
	if(pClsMapEntry != NULL)
	{
		printf("QoSHwMapClassToQueueId - ClassId:%d PriMap:%p L2Filter:%p L3Filter:%p\n",
	                pClsMapEntry->u4QoSMFClass, pClsMapEntry->pPriorityMapPtr, pClsMapEntry->pL2FilterPtr, pClsMapEntry->pL3FilterPtr);
	}
#ifdef USER_HW_API
	tEnetHal_QoSClassMapEntry tEnetHal_ClsMapEntry;
	memset(&tEnetHal_ClsMapEntry,0,sizeof(tEnetHal_QoSClassMapEntry));
	if(pClsMapEntry != NULL)
	{
	       ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"QoSHwMapClassToQueueId - ClassId:%d PriMap:%p L2Filter:%p L3Filter:%p\n",
	                pClsMapEntry->u4QoSMFClass, pClsMapEntry->pPriorityMapPtr, pClsMapEntry->pL2FilterPtr, pClsMapEntry->pL3FilterPtr);

		convertClassMapEntry(pClsMapEntry,&tEnetHal_ClsMapEntry);
	}

	ret = EnetHal_QoSHwMapClassToQueueId ((pClsMapEntry == NULL) ? NULL : &tEnetHal_ClsMapEntry,i4IfIndex,i4ClsOrPriType,u4ClsOrPri,u4QId,u1Flag);
#endif
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwSchedulerHierarchyMap                           */
/*                                                                           */
/* Description        : This function is used to Map Hierarchy Scheduler in  */
/*                      Hardware device.                                     */
/*                                                                           */
/* Input(s)           : i4IfIndex   - Interface Index                        */
/*                    : i4SchedId   - Scheduler ID                           */
/*                    : u2Sweight   - Weight of the Scheduler Hierarchy      */
/*                    : u1Spriority - Priority of the Scheduler Hierarchy    */
/*                    : i4NextSchedId - Next Level Scheduler Id              */
/*                    : i4NextQId   - Next Level Q Id                        */
/*                    : i2HL        - Scheduler Hierarchy level              */
/*                    : u1Flag      - Map / Unmap                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwSchedulerHierarchyMap (INT4 i4IfIndex, UINT4 u4SchedId, UINT2 u2Sweight,
                            UINT1 u1Spriority, UINT4 u4NextSchedId,
                            UINT4 u4NextQId, INT2 i2HL, UINT1 u1Flag)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u1Spriority);
    UNUSED_PARAM (u4NextQId);
#ifdef USER_HW_API
	ret = EnetHal_QoSHwSchedulerHierarchyMap (i4IfIndex,u4SchedId,u2Sweight,u1Spriority,u4NextSchedId,u4NextQId,i2HL,u1Flag);
#endif
	printf("===Exit QoSHwSchedulerHierarchyMap  ret:%d\n", ret);
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwSetDefUserPriority                              */
/*                                                                           */
/* Description        : This function is used set Port Default User Priority */
/*                      in Hardware device.                                  */
/*                                                                           */
/* Input(s)           : i4Port        - Interface Index                      */
/*                    : i4DefPriority - Default Priority for the given Port  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwSetDefUserPriority (INT4 i4Port, INT4 i4DefPriority)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (i4Port);
    UNUSED_PARAM (i4DefPriority);
#ifdef USER_HW_API
#endif	
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwGetMeterStats                                   */
/*                                                                           */
/* Description        : This function is used to Get the Meter Statistics    */
/*                     from the Hardware device.                             */
/*                                                                           */
/* Input(s)           : u4MeterId - Meter Id                                 */
/*                    : u4StatsType - Statistics Type                        */
/*                    : pu8MeterStatsCounter - Ptr to the Stats Value        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwGetMeterStats (UINT4 u4MeterId, UINT4 u4StatsType,
                    tSNMP_COUNTER64_TYPE * pu8MeterStatsCounter)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4MeterId);
    UNUSED_PARAM (u4StatsType);
    UNUSED_PARAM (pu8MeterStatsCounter);
#ifdef USER_HW_API
	ret = EnetHal_QoSHwGetMeterStats (u4MeterId,u4StatsType,(tEnetHal_COUNTER64_TYPE *)pu8MeterStatsCounter);
#endif
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwGetCoSQStats                                    */
/*                                                                           */
/* Description        : This function is used to Get the Queue Statistics    */
/*                      from the Hardware device.                            */
/*                                                                           */
/* Input(s)           : i4IfIndex - Interface Index                          */
/*                    : u4QId       - Q Id                                   */
/*                    : u4StatsType - Statistics Type                        */
/*                    : pu8CoSQStatsCounter  - Ptr to the Stats Value        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwGetCoSQStats (INT4 i4IfIndex, UINT4 u4QId, UINT4 u4StatsType,
                   tSNMP_COUNTER64_TYPE * pu8CoSQStatsCounter)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4QId);
    UNUSED_PARAM (u4StatsType);
    UNUSED_PARAM (pu8CoSQStatsCounter);
#ifdef USER_HW_API
	ret = EnetHal_QoSHwGetCoSQStats (i4IfIndex,u4QId,u4StatsType,(tEnetHal_COUNTER64_TYPE *)pu8CoSQStatsCounter);
#endif
    return ret;
}

/*****************************************************************************/
/* Function Name      : FsQosHwConfigPfc                                     */
/*                                                                           */
/* Description        : This function is used to configure the PFC in HW     */
/*                                                                           */
/* Input(s)           : pQosPfcHwEntry - PFC Hw Entry                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsQosHwConfigPfc (tQosPfcHwEntry * pQosPfcHwEntry)
{
	INT4 ret = FNP_SUCCESS;

    UNUSED_PARAM (pQosPfcHwEntry);
#ifdef USER_HW_API
#endif	
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwGetCountActStats                                */
/*                                                                           */
/* Description        : This function gets the CountAct statistics from the  */
/*                      Hardware device                                      */
/*                                                                           */
/* Input(s)           : u4DiffServId                                         */
/*                    : u4StatsType                                          */
/*                                                                           */
/* Output             : pu8RetValDiffServCounter                             */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwGetCountActStats (UINT4 u4DiffServId,
                       UINT4 u4StatsType,
                       tSNMP_COUNTER64_TYPE * pu8RetValDiffServCounter)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4DiffServId);
    UNUSED_PARAM (u4StatsType);
    UNUSED_PARAM (pu8RetValDiffServCounter);
#ifdef USER_HW_API
#endif	
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwGetAlgDropStats                                 */
/*                                                                           */
/* Description        : This function gets the AlgDrop  statistics from the  */
/*                      Hardware device                                      */
/*                                                                           */
/* Input(s)           : u4DiffServId                                         */
/*                    : u4StatsType                                          */
/*                                                                           */
/* Output             : pu8RetValDiffServCounter                             */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwGetAlgDropStats (UINT4 u4DiffServId,
                      UINT4 u4StatsType,
                      tSNMP_COUNTER64_TYPE * pu8RetValDiffServCounter)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4DiffServId);
    UNUSED_PARAM (u4StatsType);
    UNUSED_PARAM (pu8RetValDiffServCounter);
#ifdef USER_HW_API
#endif	
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwGetRandomDropStats                              */
/*                                                                           */
/* Description        : This function gets the RandomDrop statistics from the*/
/*                      Hardware device                                      */
/*                                                                           */
/* Input(s)           : u4DiffServId                                         */
/*                    : u4StatsType                                          */
/*                                                                           */
/* Output             : pu8RetValDiffServCounter                             */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwGetRandomDropStats (UINT4 u4DiffServId,
                         UINT4 u4StatsType,
                         tSNMP_COUNTER64_TYPE * pu8RetValDiffServCounter)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4DiffServId);
    UNUSED_PARAM (u4StatsType);
    UNUSED_PARAM (pu8RetValDiffServCounter);
#ifdef USER_HW_API
#endif
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwSetPbitPreferenceOverDscp                       */
/*                                                                           */
/* Description        : This function sets the pbit preference over DSCP if  */
/*                      the frame contains both pbit and DSCP                */
/*                                                                           */
/* Input(s)           : i4Port                                               */
/*                    : i4PbitPref                                           */
/*                                                                           */
/* Output             : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
QoSHwSetPbitPreferenceOverDscp (INT4 i4Port, INT4 i4PbitPref)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (i4Port);
    UNUSED_PARAM (i4PbitPref);
#ifdef USER_HW_API
	ret = EnetHal_QoSHwSetPbitPreferenceOverDscp (i4Port,i4PbitPref);
#endif	
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwSetCpuRateLimit                                 */
/*                                                                           */
/* Description        : This function is used set the CPU rate limiting      */
/*                      in Hardware device.                                  */
/*                                                                           */
/* Input(s)           : i4CpuQueueId   - CPU Queue Id                        */
/*                    : u4MinRate      - Mininum transmission rate           */
/*                    : u4MaxRate      - Maximum transmission rate           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwSetCpuRateLimit (INT4 i4CpuQueueId, UINT4 u4MinRate, UINT4 u4MaxRate)
{
	INT4 ret =FNP_SUCCESS;
    UNUSED_PARAM (i4CpuQueueId);
    UNUSED_PARAM (u4MinRate);
    UNUSED_PARAM (u4MaxRate);
#ifdef USER_HW_API
	ret = EnetHal_QoSHwSetCpuRateLimit (i4CpuQueueId,u4MinRate,u4MaxRate);
#endif	
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwMapClasstoPriMap                                */
/*                                                                           */
/* Description        : This function map vlanpri/dscp/exp values to         */
/*                        internal priority                                  */
/*                                                                           */
/* Input(s)           : pQosClassToPriMapEntry                               */
/*                                                                           */
/* Output             : Field processor entry id if prio type is exp         */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwMapClasstoPriMap (tQosClassToPriMapEntry * pQosClassToPriMapEntry)
{
	INT4 ret = FNP_SUCCESS;
	
	if(pQosClassToPriMapEntry != NULL)
	{
		printf("QoSHwMapClasstoPriMap i4IfIndex:%d \n",pQosClassToPriMapEntry->i4IfIndex);
		printf("QoSHwMapClasstoPriMap u1Flag:%d \n",pQosClassToPriMapEntry->u1Flag);
		printf("QoSHwMapClasstoPriMap u1PriType:%d \n",pQosClassToPriMapEntry->u1PriType);
		printf("QoSHwMapClasstoPriMap u1Priority:%d \n",pQosClassToPriMapEntry->u1Priority);
		printf("QoSHwMapClasstoPriMap u1RegenPriority:%d \n",pQosClassToPriMapEntry->u1RegenPriority);
		printf("QoSHwMapClasstoPriMap u2VlanId:%d \n",pQosClassToPriMapEntry->u2VlanId);
		printf("QoSHwMapClasstoPriMap u4HwFilterId:%d \n",pQosClassToPriMapEntry->u4HwFilterId);
	}
#ifdef USER_HW_API
	tEnetHal_QosClassToPriMapEntry tQosClassToPriEntry;
	memset(&tQosClassToPriEntry,0,sizeof(tEnetHal_QosClassToPriMapEntry));
	if(pQosClassToPriMapEntry != NULL)
	{
		convertQosHwMapClassToPriority(pQosClassToPriMapEntry,&tQosClassToPriEntry);
	}

	ret = EnetHal_QoSHwMapClasstoPriMap ((pQosClassToPriMapEntry == NULL) ? NULL : &tQosClassToPriEntry);
#endif	
    return ret;
}

/*****************************************************************************/
/* Function Name      : QosHwMapClassToIntPriority                           */
/*                                                                           */
/* Description        : This function associatess L2/L3 filter classified    */
/*                          traffic to internal priority                     */
/*                                                                           */
/* Input(s)           : pQosClassToIntPriEntryi                              */
/*                                                                           */
/* Output             : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QosHwMapClassToIntPriority (tQosClassToIntPriEntry * pQosClassToIntPriEntry)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (pQosClassToIntPriEntry);
#ifdef USER_HW_API
#endif	
    return ret;
}
/*****************************************************************************/
/* Function Name      : QosHwGetDscpQueueMap                                 */
/*                                                                           */
/* Description        : This function used to get the DSCP to Queue mapping  */
/*                       information configured in the hardware              */
/*                                                                           */
/* Input(s)           : i4QosDscpVal - DscpValue                             */
/*                    : i4QosQueID   - Queue ID                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT1
QosHwGetDscpQueueMap (INT4  i4QosDscpVal, INT4 * i4QosQueID)
{
	INT1 ret=FNP_SUCCESS;
    UNUSED_PARAM(i4QosDscpVal);
    UNUSED_PARAM(i4QosQueID);
#ifdef USER_HW_API
#endif
    return ret;
}
/*****************************************************************************/
/* Function Name      : QoSHwSetTrafficClassToPcp                            */
/*                                                                           */
/* Description        : This function sets the Traffic class to Pcp in h/w   */
/*                                                                           */
/*                                                                           */
/* Input(s)           : i4IfIndex                                            */
/*                    : i4UserPriority                                       */
/*                    : i4TrafficClass                                       */
/* Output             : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT1
QoSHwSetTrafficClassToPcp (INT4 i4IfIndex, INT4 i4Priority, INT4 i4TrafficClass)
{
	INT1 ret = FNP_SUCCESS;
    UNUSED_PARAM(i4IfIndex);
    UNUSED_PARAM(i4Priority);
    UNUSED_PARAM(i4TrafficClass);
#ifdef USER_HW_API
#endif
    return ret;
}

/*****************************************************************************/
/* Function Name      : QoSHwMeterStatsUpdate                                */
/*                                                                           */
/* Description        : This function is used to Create Meter in the device  */
/*                                                                           */
/* Input(s)           : pMeterEntry    -Ptr for a Meter entry                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwMeterStatsUpdate (UINT4 u4HwFilterId,tQoSMeterEntry * pMeterEntry)
{
    UNUSED_PARAM (u4HwFilterId);
    UNUSED_PARAM (pMeterEntry);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSHwMeterStatsClear                                 */
/*                                                                           */
/* Description        : This function is used to Create Meter in the device  */
/*                                                                           */
/* Input(s)           : u4MererId    -Meter ID                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwMeterStatsClear (UINT4 u4MeterId)
{
    UNUSED_PARAM (u4MeterId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSHwSetVlanQueuingStatus                            */
/*                                                                           */
/* Description        : This function Creates the Hierarchical Scheduling    */
/*                      Schema to perform VLAN based Subscriber Queue-Mapping*/
/*                                                                           */
/*                                                                           */
/* Input(s)           : i4IfIndex                                            */
/*                    : i4UserPriority                                       */
/*                    : i4TrafficClass                                       */
/* Output             : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT1
QoSHwSetVlanQueuingStatus (INT4 i4IfIndex, tQoSQEntry  *pQEntrySubscriber)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pQEntrySubscriber);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsQosHwGetPfcStats                                   */
/*                                                                           */
/* Description        : This function is used to Get PFC Statistics          */
/*                                                                           */
/* Input(s)           : QosPfcHwStats                                        */
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
UINT1 FsQosHwGetPfcStats (tQosPfcHwStats *pPfcHwStats) 
{
    UNUSED_PARAM(pPfcHwStats);
    return FNP_SUCCESS;
}
#endif /* __QOSX_NP_C__ */
