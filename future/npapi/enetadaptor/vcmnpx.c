/*****************************************************************************
 * Description: This file contains the function implementations  of
 *              VCM NP-API.
 * $Id: vcmnpx.c,v 1.7 2010/10/20 13:19:12 prabuc Exp $
 ****************************************************************************/
#include "lr.h"
#include "cfa.h"
#include "npapi.h"
#include "npvcm.h"

/*****************************************************************************/
/* Function Name      : FsVcmMbsmHwCreateContext                             */
/*                                                                           */
/* Description        : This function is called when a context is created.   */
/*                                                                           */
/* Input(s)           : Context Id.                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVcmMbsmHwCreateContext (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVcmMbsmHwMapPortToContext                          */
/*                                                                           */
/* Description        : This function is invoked when a port is mapped to    */
/*                      a context.                                           */
/*                                                                           */
/* Input(s)           : Context Id, Interface Index.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVcmMbsmHwMapPortToContext (UINT4 u4ContextId, UINT4 u4IfIndex,
                             tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVcmMbsmHwMapIfIndexToBrgPort                       */
/*                                                                           */
/* Description        : This function is invoked when a Slot is attached and */
/*                      the mapping of ports in that Slot to                 */
/*                      a context to map the ifindex to local port id.       */
/*                                                                           */
/* Input(s)           : Context Id, Interface Index, Context Info,pSlotInfo. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVcmMbsmHwMapIfIndexToBrgPort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                tContextMapInfo ContextInfo,
                                tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    UNUSED_PARAM (ContextInfo);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4ContextId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVcmMbsmHwMapVirtualRouter                          */
/*                                                                           */
/* Description        : This function is called when an L3 interfaces is     */
/*                      mapped to or unmapped from, L3 Virtual Router        */
/*                      This function also creates and deletes virtual router*/
/*                      in the NP                                            */
/*                                                                           */
/* Input(s)           : VrMapAction -   Takes the following actions,         */
/*                       NP_HW_CREATE_VIRTUAL_ROUTER - Creates virtual router*/
/*                       NP_HW_DELETE_VIRTUAL_ROUTER - Deletes virtual router*/
/*                       NP_HW_MAP_INTF_TO_VR - IVR interface is mapped to   */
/*                                               virtual router              */
/*                       NP_HW_UNMAP_INTF_TO_VR - IVR Interface is unmapped  */
/*                                                from virtual router        */
/*                       NP_HW_MAP_RPORT_TO_VR - Router port is mapped to    */
/*                                               virtual router              */
/*                       NP_HW_UNMAP_RPORT_TO_VR - Router port is unmapped   */
/*                                                 from virtual router       */
/*                    : pVcmVrMapInfo - Virutal router   mapping info        */
/*                    : pSlotInfo - SLot Info                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVcmMbsmHwMapVirtualRouter (tVrMapAction VrMapAction,
                             tFsNpVcmVrMapInfo * pVcmVrMapInfo,
                             tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (VrMapAction);
    UNUSED_PARAM (pVcmVrMapInfo);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVcmSispMbsmHwSetPortVlanMapping                    */
/*                                                                           */
/* Description        : This function is invoked when a Slot is attached and */
/*                      the mapping of port-vlan-context for the ports       */
/*                      present in that slot will be programmed.             */
/*                                                                           */
/* Input(s)           : u4IfIndex          - Port Interface Index            */
/*                      tVlan Id           - Vlan Identifier                 */
/*                      u4SecondaryContext - Secondary context to which port */
/*                                           should be mapped.               */
/*                      pSlotInfo          - Slot Information.               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVcmSispMbsmHwSetPortVlanMapping (UINT4 u4IfIndex, tVlanId VlanId,
                                   UINT4 u4ContextId, UINT1 u1Status,
                                   tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    UNUSED_PARAM (VlanId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVcmSispMbsmHwSetPortCtrlStatus                     */
/*                                                                           */
/* Description        : This NPAPI will be used to update SISP enable/disable*/
/*                      status on a physical or port channel port.           */
/*                                                                           */
/* Input(s)           : Interface Index, SISP status(Enable/Disable)         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVcmSispMbsmHwSetPortCtrlStatus (UINT4 u4IfIndex, UINT1 u1Status,
                                  tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}
