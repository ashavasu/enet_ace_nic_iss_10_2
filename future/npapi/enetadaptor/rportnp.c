/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rportnp.c,v 1.6 2015/10/29 09:28:56 siva Exp $
 *
 * Description: This file contains the function  Routed Port NP-API.
 ****************************************************************************/
#include "lr.h"
#include "rportnp.h"

#include "adap_types.h"
#include "adap_logger.h"
#include "EnetHal_L3_Api.h"

/******************************************************************************/
/*   Function Name             : FsNpL3Ipv4ArpAdd                             */
/*   Description               : Adds the Specified IP address and MAC address*/
/*                               to the H/W L3 IP Table for CFA_ENET type     */
/*                               interface Returns Full if no                 */
/*                               free entry exist in the L3 Ip Table          */
/*   Input(s)                  : u4VrId - The virtual router identifier      */
/*                               u4IfIndex - CFA L3 Vlan If Index             */
/*                               u4IpAddr  - IP address for which ARP is added*/
/*                               pMacAddr  -  MAC Address                     */
/*                               pu1IfName - Interface name                   */
/*                               i1State   - State of the ARP entry           */
/*   Output(s)                 : pbu1TblFull - Hw Table Full/not              */
/*                                            (FNP_FALSE/FNP_TRUE)            */
/*   Global Variables Referred : None                                         */
/*   Global variables Modified : None                                         */
/*   Exceptions                : None                                         */
/*   Use of Recursion          : None                                         */
/*   Returns                   : FNP_SUCCESS/FNP_FAILURE                      */
/******************************************************************************/

UINT4
FsNpL3Ipv4ArpAdd (UINT4 u4IfIndex, UINT4 u4IpAddr,
                  UINT1 *pMacAddr, UINT1 *pu1IfName,
                  INT1 i1State, UINT1 *pbu1TblFull)
{
	UINT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
    tEnetHal_MacAddr mac_addr;
    adap_mac_from_arr(&mac_addr, pMacAddr);
    ret = EnetHal_FsNpL3Ipv4ArpAdd (u4IfIndex,u4IpAddr,&mac_addr,pu1IfName,i1State,pbu1TblFull);
    (*pbu1TblFull) = FNP_FALSE;
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (pMacAddr);
    UNUSED_PARAM (pu1IfName);
    UNUSED_PARAM (i1State);
    UNUSED_PARAM (pbu1TblFull);
#endif
    return ret;
}

/******************************************************************************/
/*   Function Name             : FsNpL3Ipv4ArpModify                          */
/*   Description               : Modify the ARP entry over the router port    */
/*   Input(s)                  : u4VrId - The virtual router identifier      */
/*                               u4IfIndex - CFA L3 Vlan If Index             */
/*                               u4IpAddr  - IP address for which ARP is added*/
/*                               pMacAddr  -  MAC Address                     */
/*                               pu1IfName - Interface name                   */
/*                               i1State   - State of the ARP entry           */
/*   Output(s)                 : None                                         */
/*   Global Variables Referred : None                                         */
/*   Global variables Modified : None                                         */
/*   Exceptions                : None                                         */
/*   Use of Recursion          : None                                         */
/*   Returns                   : FNP_SUCCESS/FNP_FAILURE                      */
/******************************************************************************/

UINT4
FsNpL3Ipv4ArpModify (UINT4 u4IfIndex, UINT4 u4IpAddr,
                     UINT1 *pMacAddr, UINT1 *pu1IfName, INT1 i1State)
{
    UINT4 ret = FNP_SUCCESS;
#ifdef USER_HW_API
    tEnetHal_MacAddr mac_addr;
    adap_mac_from_arr(&mac_addr, pMacAddr);
    ret = EnetHal_FsNpL3Ipv4ArpModify(u4IfIndex, u4IpAddr,&mac_addr,pu1IfName,i1State);
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (pMacAddr);
    UNUSED_PARAM (pu1IfName);
    UNUSED_PARAM (i1State);
#endif
    return ret;
}

/*****************************************************************************/
/*  Function Name             :  FsNpIpv4L3IpInterface                       */
/*  Description               :  Create L3 IP Interface                      */
/*  Input(s)                  :  L3Action  --Action to be performed          */
/*                            :  i)Create                                    */
/*                            :  ii)Modify                                   */
/*                            :  i)Delete                                    */
/*                            :  pFsNpL3IfInfo -- Intrface Info              */
/*  Output(s)                 :  None                                        */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions                :  None                                        */
/*  Use of Recursion          :  None                                        */
/*  Returns                   :  FNP_FAILURE(0) on failure                   */
/*                               FNP_SUCCESS(1) on success                   */
/*****************************************************************************/
UINT4
FsNpIpv4L3IpInterface (tL3Action L3Action, tFsNpL3IfInfo * pFsNpL3IfInfo)
{

    UNUSED_PARAM (L3Action);
    UNUSED_PARAM (pFsNpL3IfInfo);
    UINT4 ret = FNP_SUCCESS;
    tEnetHal_FsNpL3IfInfo EnetNpL3IfInfo;

#ifdef USER_HW_API
    EnetNpL3IfInfo.u4IpAddr=pFsNpL3IfInfo->u4IpAddr;
    EnetNpL3IfInfo.u4IpSubnet=pFsNpL3IfInfo->u4IpSubnet;
    EnetNpL3IfInfo.u4IfIndex=pFsNpL3IfInfo->u4IfIndex;
    EnetNpL3IfInfo.u2ErrCode=pFsNpL3IfInfo->u2ErrCode;
    EnetNpL3IfInfo.u4VrId=pFsNpL3IfInfo->u4VrId;
    EnetNpL3IfInfo.IfType= pFsNpL3IfInfo->au1Dummy[0];
    EnetNpL3IfInfo.u1Port= pFsNpL3IfInfo->au1Dummy[1];

    ret = EnetHal_FsNpIpv4L3IpInterface ((eEnetHal_L3Action)L3Action, &EnetNpL3IfInfo);
#endif
    return ret;
}

/*****************************************************************************/
/*  Function Name             :  FsNpIpv4GetEcmpGroups                       */
/*  Description               :  Get Max ECMP Groups Supported               */
/*  Input(s)                  :  NONE                                        */
/*  Output(s)                 :  pu4EcmpGroup                                */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions                :  None                                        */
/*  Use of Recursion          :  None                                        */
/*  Returns                   :  FNP_FAILURE(0) on failure                   */
/*                               FNP_SUCCESS(1) on success                   */
/*****************************************************************************/
UINT4
FsNpIpv4GetEcmpGroups (UINT4 *pu4EcmpGroups)
{
    UNUSED_PARAM (pu4EcmpGroups);
    UINT4 ret = FNP_SUCCESS;

    return ret;
}
