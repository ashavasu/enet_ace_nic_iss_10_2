/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipmcnpx.c,v 1.3 2014/12/18 12:17:05 siva Exp $
 *
 * Description: This file contains the function implementations  of IPMC NP-API.
 ****************************************************************************/
#ifndef _IPMCNPX_C_
#define _IPMCNPX_C_

#include "lr.h"
#include "cfa.h"
#include "npapi.h"
#include "ipmcnp.h"
#include "ip6mcnp.h"

/*****************************************************************************
 *  Function Name   : FsNpIpv4MbsmMcInit
 *  Description     : This function initialises the IP Multicasting in the 
 *                    driver. This function enables the IPMC Module, enforces
 *                    source port check, and defines the search rule for IPMC
 *                    entry
 *  Input(s)        : pSlotInfo - Slot on which the IPMC bit should be set
 *  Output(s)       : None.
 *  Returns         : FNP_SUCCESS/FNP_FAILURE
 ***************************************************************/

PUBLIC UINT4
FsNpIpv4MbsmMcInit (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FsNpIpv4MbsmMcAddRouteEntry              */
/*  Description     : This function adds Multicast Route Entry */
/*                    in IPMC Table. It also add a downstream  */
/*                    interface to the existing route entry.   */
/*  Input(s)        : u4VrId        :  Virtual Router ID       */
/*                                     ZERO when not supported.*/
/*                    u4GrpAddr     :  Group Address           */
/*                    u4GrpPrefix   :  Group Prefix            */
/*                    u4SrcIpAddr   :  Source Address          */
/*                    u4SrcIpPrefix :  Source Prefix           */
/*                    rtEntry       :  Have the route entry    */
/*                                     parameters like command */
/*                                     RPF interface and flags.*/
/*                    u2NoOfDownStreamIf:  No of associated    */
/*                                     downstream interfaces.  */
/*                    NOTE: Currently only one interface is    */
/*                          added at a time                    */
/*                    pDownStreamIf :  downstream interface    */
/*                                     to be added.            */
/*                    pSlotInfo     :  Slot Information        */
/*  Output(s)       : None.                                    */
/*  Returns         : FNP_SUCCESS/FNP_FAILURE                  */
/***************************************************************/

PUBLIC UINT4
FsNpIpv4MbsmMcAddRouteEntry (UINT4 u4VrId,
                             UINT4 u4GrpAddr,
                             UINT4 u4GrpPrefix,
                             UINT4 u4SrcIpAddr,
                             UINT4 u4SrcIpPrefix,
                             UINT1 u1CallerId,
                             tMcRtEntry rtEntry,
                             UINT2 u2NoOfDownStreamIf,
                             tMcDownStreamIf * pDownStreamIf,
                             tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4GrpAddr);
    UNUSED_PARAM (u4GrpPrefix);
    UNUSED_PARAM (u4SrcIpAddr);
    UNUSED_PARAM (u4SrcIpPrefix);
    UNUSED_PARAM (u1CallerId);
    UNUSED_PARAM (rtEntry);
    UNUSED_PARAM (u2NoOfDownStreamIf);
    UNUSED_PARAM (pDownStreamIf);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

#ifdef PIM_WANTED
/*****************************************************************************/
/* Function Name      : FsPimMbsmNpInitHw                                    */
/*                                                                           */
/* Description        : This function enables PIM & enables IPMC in the      */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : pSlotInfo - Slot on which PIM should be enabled      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

PUBLIC UINT4
FsPimMbsmNpInitHw (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}
#endif /* PIM_WANTED */

#ifdef DVMRP_WANTED
/*****************************************************************************/
/* Function Name      : FsDvmrpMbsmNpInitHw                                  */
/*                                                                           */
/* Description        : This function enables DVMRP & enables IPMC in the    */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : pSlotInfo - Slot on which DVMRP should be enabled    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

PUBLIC UINT4
FsDvmrpMbsmNpInitHw (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}
#endif /* DVMRP_WANTED */

/*****************************************************************************
 *  Function Name   : FsNpIpv6MbsmMcInit
 *  Description     : This function initialises the IP Multicasting in the 
 *                    driver. This function enables the IP6MC Module, enforces
 *                    source port check, and defines the search rule for IP6MC
 *                    entry
 *  Input(s)        : pSlotInfo - Slot on which the IPMC bit should be set
 *  Output(s)       : None.
 *  Returns         : FNP_SUCCESS/FNP_FAILURE
 ***************************************************************/

PUBLIC UINT4
FsNpIpv6MbsmMcInit (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FsNpIpv6MbsmMcAddRouteEntry              */
/*  Description     : This function adds Multicast Route Entry */
/*                    in IPMC Table. It also add a downstream  */
/*                    interface to the existing route entry.   */
/*  Input(s)        : u4VrId        :  Virtual Router ID       */
/*                                     ZERO when not supported.*/
/*                    u4GrpAddr     :  Group Address           */
/*                    u4GrpPrefix   :  Group Prefix            */
/*                    u4SrcIpAddr   :  Source Address          */
/*                    u4SrcIpPrefix :  Source Prefix           */
/*                    rtEntry       :  Have the route entry    */
/*                                     parameters like command */
/*                                     RPF interface and flags.*/
/*                    u2NoOfDownStreamIf:  No of associated    */
/*                                     downstream interfaces.  */
/*                    NOTE: Currently only one interface is    */
/*                          added at a time                    */
/*                    pDownStreamIf :  downstream interface    */
/*                                     to be added.            */
/*                    pSlotInfo     :  Slot Information        */
/*  Output(s)       : None.                                    */
/*  Returns         : FNP_SUCCESS/FNP_FAILURE                  */
/***************************************************************/

PUBLIC UINT4
FsNpIpv6MbsmMcAddRouteEntry (UINT4 u4VrId,
                             UINT1 *pGrpAddr,
                             UINT4 u4GrpPrefix,
                             UINT1 *pSrcIpAddr,
                             UINT4 u4SrcIpPrefix,
                             UINT1 u1CallerId,
                             tMc6RtEntry rtEntry,
                             UINT2 u2NoOfDownStreamIf,
                             tMc6DownStreamIf * pDownStreamIf,
                             tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pGrpAddr);
    UNUSED_PARAM (u4GrpPrefix);
    UNUSED_PARAM (pSrcIpAddr);
    UNUSED_PARAM (u4SrcIpPrefix);
    UNUSED_PARAM (u1CallerId);
    UNUSED_PARAM (rtEntry);
    UNUSED_PARAM (u2NoOfDownStreamIf);
    UNUSED_PARAM (pDownStreamIf);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

#ifdef PIMV6_WANTED
/*****************************************************************************/
/* Function Name      : FsPimv6MbsmNpInitHw                                  */
/*                                                                           */
/* Description        : This function enables PIMv6 & enables IP6MC in the   */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : pSlotInfo - Slot on which PIM should be enabled      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

PUBLIC UINT4
FsPimv6MbsmNpInitHw (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}
#endif /* PIM_WANTED */

#endif
