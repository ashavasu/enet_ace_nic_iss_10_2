/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ecfmnp.c,v 1.2 2007/10/18 11:27:41 iss Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/
#include "lr.h"
#include "cfa.h"
#include "rmgr.h"
#include "ecfm.h"
#include "npapi.h"
#include "npecfm.h"

/***********************************************************************
 * Function Name      : FsEcfmHwInit                                       
 *                                                                       
 * Description        : This function takes care of initialising the     
 *                      hardware related parameters.
 *                                                                       
 * Input(s)           : None                                             
 *                                                                       
 * Output(s)          : None.                                            
 *                                                                       
 * Return Value(s)    : FNP_SUCCESS/FNP_FAILURE                        
 ***********************************************************************/
INT4
FsEcfmHwInit (VOID)
{
    return FNP_SUCCESS;
}

/***********************************************************************
 * Function Name      : FsEcfmHwDeInit                                    
 *                                                                     
 * Description        : This function takes care of de-initialising the   
 *                       hardware related parameters.
 *                                                                     
 * Input(s)           : None                                           
 *                                                                     
 * Output(s)          : None.                                          
 *                                                                     
 * Return Value(s)    : FNP_SUCCESS/FNP_FAILURE                      
 ***********************************************************************/
INT4
FsEcfmHwDeInit (VOID)
{
    return FNP_SUCCESS;
}

/******************************************************************************/
/*                           End  of file ecfmnp.c                            */
/******************************************************************************/
