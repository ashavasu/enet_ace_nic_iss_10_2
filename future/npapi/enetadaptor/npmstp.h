/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: npmstp.h,v 1.3 2007/02/01 14:59:31 iss Exp $ 
 *
 * Description: All prototypes for Network Processor API functions for MSTP 
 * 
 *
 *******************************************************************/
#ifndef _NPMSTP_H
#define _NPMSTP_H

#include "mstnp.h"

#define MSTP_NP_BCM_INST(u2InstId) (u2InstId + 1)

#endif /* _NPMSTP_H */
