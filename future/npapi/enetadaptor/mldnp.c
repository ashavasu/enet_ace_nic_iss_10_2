/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldnp.c,v 1.4 2014/12/10 12:53:49 siva Exp $
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/
#ifndef _MLDNP_C_
#define _MLDNP_C_
#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "npapi.h"
#include "npmld.h"

/*****************************************************************************/
/* Function Name      : FsMldHwEnableMld                                     */
/*                                                                           */
/* Description        : This function is called when MLD is enabled. It      */
/*                      sets up a filter in the hardware so that MLD  packets*/
/*                      are received in the cPU.                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                                    */
/*****************************************************************************/

INT4
FsMldHwEnableMld (VOID)
{
    EnetHal_FsNpMLDEnable();
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMldHwDisableMld                                    */
/*                                                                           */
/* Description        : This function disables MLD snooping feature in the   */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMldHwDisableMld (VOID)
{
    EnetHal_FsNpMLDDisable();
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMldHwDestroyMLDFilter                             */
/*                                                                           */
/* Description        : This function destroys the MLD filter on all units  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMldHwDestroyMLDFilter (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMldHwDestroyMLDFP                                 */
/*                                                                           */
/* Description        : This function destroys the MLD Field Processor      */
/*                      group and entry on all units.                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMldHwDestroyMLDFP (VOID)
{
    return FNP_SUCCESS;
}


/*****************************************************************************/
/* Function Name      : FsNpIpv6SetMldIfaceJoinRate                          */
/*                                                                           */
/* Description        : This function is called when the rate for MLD join   */
/*                      packets are configured in the interface.             */
/*                      It sets the rate at which the join packets must be   */
/*                      accepted in an MLD interface                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsNpIpv6SetMldIfaceJoinRate (INT4 i4IfIndex, INT4 i4RateLimit)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4RateLimit);
    return FNP_SUCCESS;
}


#endif
