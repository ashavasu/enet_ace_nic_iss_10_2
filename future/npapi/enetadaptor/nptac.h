/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: nptac.h,v 1.1 2011/06/20 12:27:02 siva Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 * 
 *
 *******************************************************************/
#ifndef _NPTAC_H
#define _NPTAC_H

#include "tac.h"

#endif /* _NPTAC__H */
