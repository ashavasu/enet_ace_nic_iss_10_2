/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlanminp.c,v 1.37 2015/09/29 10:31:45 siva Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/
#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#ifdef PBB_WANTED
#include "pbb.h"
#endif

#include "l2iwf.h"
#include "npapi.h"
#include "npvlanmi.h"

#include "adap_types.h"
#include "adap_logger.h"
#include "EnetHal_L2_Api.h"

/* VLAN FDB TABLE is currently supported for the Option NPAPI_WANTED.
 * Which means FDB table is present either in hardware or software.
 * If the table should be duplicated then whenever software is updated
 * then the hardware should be intimated */

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwInit                                   */
/*                                                                           */
/*    Description         : This function takes care of initialising the     */
/*                          hardware related parameters.                     */
/*                                                                           */
/*    Input(s)            : u4ContextId - current Context Id                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
INT4
FsMiVlanHwInit (UINT4 u4ContextId)
{
	INT4 ret=FNP_SUCCESS;
    /* Set Egress filtering to enable in the hardware */
    /* gegrSetUnkUcEgressFilterCmd () - Egress filtering for Unknown Unicast */
    /* gegrSetUnregMcEgressFilterCmd ()  - Egress filtering for unregistered 
     * multicast*/
    /* gegrVlanEgressFilteringEnable () - Egress filtering for known unicast */

	UNUSED_PARAM (u4ContextId);
#ifdef USER_HW_API
	ret = EnetHal_FsMiVlanHwInit (u4ContextId);
#endif //USER_HW_API

    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwDeInit                                 */
/*                                                                           */
/*    Description         : This function deinitialises all the hardware     */
/*                          related tables and parameters.                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Current Context Id                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns                 : FNP_SUCCESS/FNP_FAILURE                      */
/*****************************************************************************/
INT4
FsMiVlanHwDeInit (UINT4 u4ContextId)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwDeInit (u4ContextId);
#endif //USER_HW_API	
    /* Disable VLAN in hardware */
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetAllGroupsPorts                      */
/*                                                                           */
/*    Description         : This function takes care of updating the         */
/*                          hardware table for  Forward All Groups behavior  */
/*                          for the specified VLAN ID.                       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId  - VlanId can take <1-4094>               */
/*                          pHwAllGroupPorts - Pointer to array of ports     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetAllGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                             tHwPortArray * pHwAllGroupPorts)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pHwAllGroupPorts);
#ifdef USER_HW_API
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwEvbConfigSChIface                      */
/*                                                                           */
/*    Description         : This function provides suppport to create/delete */
/*                          EVB (Edge Virtual Bridging) S-Channel interface  */
/*                          (virtual port) in the                            */
/*                          hardware                                         */
/*                                                                           */
/*    Input(s)            : pVlanEvbHwConfigInfo - parameters to be          */
/*                                                 programmed in hardware.   */
/*                          u1OpCode -indicates about the S-Channel interface*/
/*                                   creation or S-Channel interface deletion*/
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwEvbConfigSChIface (tVlanEvbHwConfigInfo *pVlanEvbHwConfigInfo)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (pVlanEvbHwConfigInfo);
#ifdef USER_HW_API
#endif //USER_HW_API
    return ret;
}


/*****************************************************************************/
/*    Function Name       : FsMiVlanHwResetAllGroupsPorts                    */
/*                                                                           */
/*    Description         : This function removes the given Ports from       */
/*                          Forward All Group Port list for the specified    */
/*                          Vlan.                                            */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId  - VlanId can take <1-4094>               */
/*                          puHwResetAllGroupPorts - Pointer to array of     */
/*                          ports                                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwResetAllGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                               tHwPortArray * pHwResetAllGroupPorts)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pHwResetAllGroupPorts);
#ifdef USER_HW_API
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwResetUnRegGroupsPorts                  */
/*                                                                           */
/*    Description         : This function removes the given Ports from       */
/*                          Forward UnReg Group Port list for the specified  */
/*                          Vlan.                                            */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId  - VlanId can takes <1-4094>              */
/*                          pHwResetUnRegGroupPorts - Pointer to array       */
/*                                 of ports                                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwResetUnRegGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                                 tHwPortArray * pHwResetUnRegGroupPorts)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pHwResetUnRegGroupPorts);
#ifdef USER_HW_API
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetUnRegGroupsPorts                    */
/*                                                                           */
/*    Description         : This function takes care of updating the         */
/*                          hardware table for  Forward All unreg groups     */
/*                          for the specified VLAN ID.                       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId  - VlanId  can take <1-4094>              */
/*                          pHwUnRegPorts - Pointer to array of ports        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetUnRegGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                               tHwPortArray * pHwUnRegPorts)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pHwUnRegPorts);
#ifdef USER_HW_API
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwAddStaticUcastEntry                    */
/*                                                                           */
/*    Description         : This function adds the egress ports  into        */
/*                          hardware Static Unicast table                    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4Fid            - Fid                           */
/*                          pMacAddr         - Pointer to the Mac Address.   */
/*                          u4Port           - Received Interface Index      */
/*                          pHwAllowedToGoPorts - pointer to array of        */
/*                          allowed to go ports                              */
/*                          u1Status         -  Status of entry takes values */
/*                          like  VLAN_PERMANENT, VLAN_DELETE_ON_RESET,      */
/*                          VLAN_DELETE_ON_TIMEOUT                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwAddStaticUcastEntry (UINT4 u4ContextId, UINT4 u4Fid, tMacAddr MacAddr,
                               UINT4 u4Port, tHwPortArray * pHwAllowedToGoPorts,
                               UINT1 u1Status)
{
	INT4 ret=FNP_SUCCESS;

#ifdef USER_HW_API
	tEnetHal_MacAddr mac_addr;
	adap_mac_from_arr(&mac_addr, MacAddr);
    ret = EnetHal_FsMiVlanHwAddStaticUcastEntry (u4ContextId,u4Fid,&mac_addr,u4Port,(tEnetHal_HwPortArray *)pHwAllowedToGoPorts,u1Status);
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (pHwAllowedToGoPorts);
    UNUSED_PARAM (u1Status);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwDelStaticUcastEntry                    */
/*                                                                           */
/*    Description         : This function delets the entry from  hardware    */
/*                          Static Unicast table.                            */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                    u4Fid            -  fdb id                             */
/*                          pMacAddr         - Pointer to the Mac Address.   */
/*                          u4Port           - Received Interface Index      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwDelStaticUcastEntry (UINT4 u4ContextId, UINT4 u4Fid, tMacAddr MacAddr,
                               UINT4 u4Port)
{
	INT4 ret=FNP_SUCCESS;

#ifdef USER_HW_API
    tEnetHal_MacAddr mac_addr;
    adap_mac_from_arr(&mac_addr, MacAddr);
    ret = EnetHal_FsMiVlanHwDelStaticUcastEntry (u4ContextId,u4Fid,&mac_addr,u4Port);
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (MacAddr);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGetFdbEntry                            */
/*                                                                           */
/*    Description         : This function gets the entry from  hardware      */
/*                           Unicast Mac table.                              */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4FdbId  - FdbId                                 */
/*                          pMacAddr - Pointer to the Mac Address.           */
/*                                                                           */
/*    Output(s)           : pEntry  - Pointer to Unicast Mac Entry           */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwGetFdbEntry (UINT4 u4ContextId, UINT4 u4FdbId, tMacAddr MacAddr,
                       tHwUnicastMacEntry * pEntry)
{
	INT4 ret=FNP_SUCCESS;
	tEnetHal_HwUnicastMacEntry tEnetHal_pEntry;

#ifdef USER_HW_API
	tEnetHal_MacAddr mac_addr;
    adap_mac_from_arr(&mac_addr, MacAddr);
    ret = EnetHal_FsMiVlanHwGetFdbEntry (u4ContextId,u4FdbId,&mac_addr,&tEnetHal_pEntry);

#ifdef MPLS_WANTED
    pEntry->u4PwVcIndex = (UINT4)tEnetHal_pEntry.u4PwVcIndex;
#endif
    pEntry->u4Port = (UINT4)tEnetHal_pEntry.u4Port;
    adap_mac_to_arr(&tEnetHal_pEntry.ConnectionId, pEntry->ConnectionId);
    pEntry->u1EntryType = (UINT1)tEnetHal_pEntry.u1EntryType;
    pEntry->u1HitStatus = (UINT1)tEnetHal_pEntry.u1HitStatus;
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4FdbId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (pEntry);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGetPortFromFdb                         */
/*                                                                           */
/*    Description         : This function gets the port from the FDB table   */
/*                          for the given vlanId and mac                     */
/*                                                                           */
/*    Input(s)            : u2VlanId - Vlan Id                               */
/*                          MacAddr  - MacAddr                               */
/*                                                                           */
/*    Output(s)           : pu4Port  - Physical port number                  */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
                                                                                                 /*    Returns            : FNP_SUCCESS/FNP_FAILURE                           *//*****************************************************************************/
INT4
FsMiVlanHwGetPortFromFdb (UINT2 u2VlanId, UINT1 *MacAddr, UINT4 *pu4Port)
{
	INT4 ret=FNP_SUCCESS;

    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (pu4Port);
#ifdef USER_HW_API
#endif //USER_HW_API	
    return ret;
}

#ifndef SW_LEARNING
/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGetFdbCount                            */
/*                                                                           */
/*    Description         : This function gets the Dynamic Fdb Count for the */
/*                          given FdbId                                      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4FdbId  - FdbId.                                */
/*                                                                           */
/*    Output(s)           : pu4Count - Count of the given FdbId.             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwGetFdbCount (UINT4 u4ContextId, UINT4 u4FdbId, UINT4 *pu4Count)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4FdbId);
    UNUSED_PARAM (pu4Count);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwGetFdbCount (u4ContextId,u4FdbId,pu4Count);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGetFirstTpFdbEntry                     */
/*                                                                           */
/*    Description         : This function gets the first Fdb entry in the    */
/*                          lexicographic order.                             */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          pu4FdbId - Pointer to the Fdb id                 */
/*                          pMacAddr - Pointer to the Mac Address.           */
/*                                                                           */
/*    Output(s)           : The first FdbId and Mac address.                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwGetFirstTpFdbEntry (UINT4 u4ContextId, UINT4 *pu4FdbId,
                              tMacAddr MacAddr)
{
	INT4 ret=FNP_SUCCESS;

    /* gfdbGetFdbEntryFirst () */

    /* 
     * First get the VlanId depending on the Vlan learning type and
     * then invoke the Hw api.
     */
#ifdef USER_HW_API
    tEnetHal_MacAddr mac_addr;
    adap_mac_from_arr(&mac_addr, MacAddr);
    ret = EnetHal_FsMiVlanHwGetFirstTpFdbEntry (u4ContextId,pu4FdbId,&mac_addr);
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pu4FdbId);
    UNUSED_PARAM (MacAddr);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGetNextTpFdbEntry                      */
/*                                                                           */
/*    Description         : This function gets the next Fdb entry in the     */
/*                          lexicographic order.                             */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4FdbId - The current FdbId.                     */
/*                          pMacAddr - Pointer to the current Mac addr.      */
/*    Input(s)            : pu4NextContextId - Pointer to next Context Id    */
/*                          pu4NextFdbId - Pointer to the next Fdb Id.       */
/*                          pNextMacAddr - Pointer to the next mac addr.     */
/*                                                                           */
/*    Output(s)           : The next FdbId and Mac address.                  */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwGetNextTpFdbEntry (UINT4 u4ContextId, UINT4 u4FdbId, tMacAddr MacAddr,
                             UINT4 *pu4NextContextId, UINT4 *pu4NextFdbId,
                             tMacAddr NextMacAddr)
{
	INT4 ret=FNP_SUCCESS;

    /* 
     * First get the next VlanId depending on the Vlan learning type and
     * then invoke the Hw api.
     */
#ifdef USER_HW_API
	tEnetHal_MacAddr mac_addr;
	tEnetHal_MacAddr next_mac_addr;
    adap_mac_from_arr(&mac_addr, MacAddr);
    ret = EnetHal_FsMiVlanHwGetNextTpFdbEntry (u4ContextId,u4FdbId,&mac_addr,pu4NextContextId,pu4NextFdbId,&next_mac_addr);
    adap_mac_to_arr(&next_mac_addr, NextMacAddr);
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4FdbId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (pu4NextContextId);
    UNUSED_PARAM (pu4NextFdbId);
    UNUSED_PARAM (NextMacAddr);
#endif //USER_HW_API
    return ret;
}

#endif
/*****************************************************************************/
/*    Function Name       : FsMiVlanHwAddMcastEntry                          */
/*                                                                           */
/*    Description         : This function adds an entry to the hardware      */
/*                          Multicast table.                                 */
/*                          This function can be called, when the Multicast  */
/*                          entry is already present in the Hardware. In     */
/*                          which case the Old Multicast member ports        */
/*                          must be removed and the new ports ('PortBmp')    */
/*                          must be added in the Hardware.                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId   - VlanId can take <1-4094>              */
/*                          pMacAddr - Pointer to the Mac Address.           */
/*                          pHwMcastPorts - Pointer to array of ports        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwAddMcastEntry (UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr,
                         tHwPortArray * pHwMcastPorts)
{
	INT4 ret=FNP_SUCCESS;

#ifdef USER_HW_API
    tEnetHal_MacAddr mac_addr;
    adap_mac_from_arr(&mac_addr, MacAddr);
    ret = EnetHal_FsMiVlanHwAddMcastEntry (u4ContextId,VlanId,&mac_addr,(tEnetHal_HwPortArray *)pHwMcastPorts);
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (pHwMcastPorts);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwAddStMcastEntry                        */
/*                                                                           */
/*    Description         : This function adds a static entry to the         */
/*                          hardware Multicast table                         */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId    - VlanId can take <1-4094>             */
/*                          pMacAddr  - Pointer to the Mac Address.          */
/*                          i4RcvPort - Received port index                  */
/*                          pHwMcastPorts - Pointer to array of ports        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwAddStMcastEntry (UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr,
                           INT4 i4RcvPort, tHwPortArray * pHwMcastPorts)
{
	INT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
	tEnetHal_MacAddr mac_addr;
    adap_mac_from_arr(&mac_addr, MacAddr);
    ret = EnetHal_FsMiVlanHwAddStMcastEntry (u4ContextId,VlanId,&mac_addr,i4RcvPort, (tEnetHal_HwPortArray *) pHwMcastPorts);
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (i4RcvPort);
    UNUSED_PARAM (pHwMcastPorts);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetMcastPort                           */
/*                                                                           */
/*    Description         : This function adds a member port to the existing */
/*                          Multicast entry. The Application has to ensure   */
/*                          that the existence of Mcast Entry for the port.  */
/*                          This API will be called only under dynamic       */
/*                          updation of the Multicast Entry by the S/W.      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId   - VlanId can take <1-4094>              */
/*                          pMacAddr - Pointer to the Mac Address.           */
/*                          u4IfIndex - Interface Index of New member port   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwSetMcastPort (UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr,
                        UINT4 u4IfIndex)
{
	INT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
	tEnetHal_MacAddr mac_addr;
    adap_mac_from_arr(&mac_addr, MacAddr);
    ret = EnetHal_FsMiVlanHwSetMcastPort (u4ContextId,VlanId,&mac_addr,u4IfIndex);
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4IfIndex);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwResetMcastPort                         */
/*                                                                           */
/*    Description         : This function resets a member port from the      */
/*                          existing multicast entry. Application has to     */
/*                          ensure the existence of multicast entry          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId   - VlanId can take <1-4094>              */
/*                          pMacAddr - Pointer to the Mac Address.           */
/*                          u4IfIndex - Interface Index of member port to be */
/*                                      removed                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwResetMcastPort (UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr,
                          UINT4 u4IfIndex)
{
	INT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
	tEnetHal_MacAddr mac_addr;
    adap_mac_from_arr(&mac_addr, MacAddr);
    ret = EnetHal_FsMiVlanHwResetMcastPort (u4ContextId,VlanId,&mac_addr,u4IfIndex);
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4IfIndex);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwDelMcastEntry                          */
/*                                                                           */
/*    Description         : This function deletes an entry from the hardware */
/*                          Multicast table.                                 */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId   - VlanId can take <1-4094>              */
/*                          pMacAddr - Pointer to the Mac Address.           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwDelMcastEntry (UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr)
{
	INT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
    tEnetHal_MacAddr mac_addr;
    adap_mac_from_arr(&mac_addr, MacAddr);
    ret = EnetHal_FsMiVlanHwDelMcastEntry(u4ContextId,VlanId,&mac_addr);
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwDelStMcastEntry                        */
/*                                                                           */
/*    Description         : This function deletes a static entry from the    */
/*                          hardware Multicast tale                          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId    - VlanId can take <1-4094>             */
/*                          pMacAddr  - Pointer to the Mac Address.          */
/*                          i4RcvPort - Received port index                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwDelStMcastEntry (UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr,
                           INT4 i4RcvPort)
{
	INT4 ret=FNP_SUCCESS;

#ifdef USER_HW_API
	tEnetHal_MacAddr mac_addr;
    adap_mac_from_arr(&mac_addr, MacAddr);
    ret = EnetHal_FsMiVlanHwDelStMcastEntry(u4ContextId,VlanId,&mac_addr,i4RcvPort);
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (i4RcvPort);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwAddVlanEntry                           */
/*                                                                           */
/*    Description         : This function adds an entry to the hardware      */
/*                          Vlan table.                                      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId     - VlanId can take <1-4094>            */
/*                          pHwEgressPorts- pointer to array of egress ports */
/*                          pHwUnTagPorts - pointer to array of Untag Ports. */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwAddVlanEntry (UINT4 u4ContextId, tVlanId VlanId,
                        tHwPortArray * pHwEgressPorts,
                        tHwPortArray * pHwUnTagPorts)
{
	INT4 ret =FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pHwEgressPorts);
    UNUSED_PARAM (pHwUnTagPorts);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwAddVlanEntry (u4ContextId,VlanId,(tEnetHal_HwPortArray *)pHwEgressPorts,(tEnetHal_HwPortArray *)pHwUnTagPorts);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwDelVlanEntry                           */
/*                                                                           */
/*    Description         : This function deletes an entry from the hardware */
/*                          Vlan table.                                      */
/*                          In case of Hybrid learning switches, this fn.    */
/*                          should disassociate the VLAN to FID mappings and */
/*                          and shud delete the FDB table if the number of   */
/*                          VLANS mapped to the FDB is 0.                    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                    VlanId     - VlanId cqan take <1-4094>                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwDelVlanEntry (UINT4 u4ContextId, tVlanId VlanId)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwDelVlanEntry (u4ContextId,VlanId);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetVlanMemberPort                      */
/*                                                                           */
/*    Description         : This function set the port as the member of      */
/*                          given Vlan                                       */
/*                                                                           */
/*    Input(s)            :u4ContextId - Context Id                          */
/*                   VlanId     - Vlan to which the port is going to         */
/*                                        be an member                       */
/*                          u4IfIndex  - The Interface Index                 */
/*                          u1IsTagged - Flag to indicate whether this port  */
/*                                       will be tagged or untagged          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetVlanMemberPort (UINT4 u4ContextId, tVlanId VlanId, UINT4 u4IfIndex,
                             UINT1 u1IsTagged)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1IsTagged);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwSetVlanMemberPort (u4ContextId,VlanId,u4IfIndex,u1IsTagged);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwResetVlanMemberPort                    */
/*                                                                           */
/*    Description         : This function Reset the port as the member of    */
/*                          given Vlan.                                      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                   VlanId     - Vlan to which the port is going to         */
/*                                        be an member                       */
/*                          u4IfIndex  - The Interface Index                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwResetVlanMemberPort (UINT4 u4ContextId, tVlanId VlanId,
                               UINT4 u4IfIndex)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4IfIndex);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwResetVlanMemberPort (u4ContextId,VlanId,u4IfIndex);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetPortPvid                            */
/*                                                                           */
/*    Description         : This function sets the given VlanId as PortPvid  */
/*                          for the Port.                                    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                    VlanId     - Vlan to which the port is going to        */
/*                                        be an member                       */
/*                          u4IfIndex  - The Interface Index                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortPvid (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId VlanId)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4IfIndex);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwSetPortPvid (u4ContextId,u4IfIndex,VlanId);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetDefaultVlanId                       */
/*                                                                           */
/*    Description         : This function sets the given VlanId as Default   */
/*                          Vlan Id.                                         */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId     -  Default Vlan Id                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwSetDefaultVlanId (UINT4 u4ContextId, tVlanId VlanId)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwSetDefaultVlanId (u4ContextId,VlanId);
#endif //USER_HW_API	
    return ret;
}

#ifdef L2RED_WANTED
/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSyncDefaultVlanId                      */
/*                                                                           */
/*    Description         : This function gets the default Vlan Id configured*/
/*                          in hardware and compares that with SwVlanId.     */
/*                          If there is a mismatch then it updates the hw    */
/*                          accordingly.                                     */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                        : SwVlanId - Default vlan id present in sw.        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwSyncDefaultVlanId (UINT4 u4ContextId, tVlanId SwVlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (SwVlanId);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanRedHwUpdateDBForDefaultVlanId            */
/*                                                                           */
/*    Description         : This function updates the Np with the vlan id    */
/*                          configured as the default vlan id                */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId     - Vlan Id to be configured as default */
/*                                       vlan id                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanRedHwUpdateDBForDefaultVlanId (UINT4 u4ContextId, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    return FNP_SUCCESS;
}
#endif

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetPortAccFrameType                    */
/*                                                                           */
/*    Description         : This function sets the Acceptable frame types    */
/*                          parameter for the Given Port.                    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4IfIndex   - The Interface Index                */
/*                          u1AccFrameType - Acceptable Frame types          */
/*                          parameter value can take                         */
/*                           VLAN_ADMIT_ALL_FRAMES,                          */
/*                           VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES,             */
/*                      VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortAccFrameType (UINT4 u4ContextId, UINT4 u4IfIndex,
                               INT1 u1AccFrameType)
{
	INT4 ret =FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1AccFrameType);
    UNUSED_PARAM (u4IfIndex);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwSetPortAccFrameType (u4ContextId,u4IfIndex,u1AccFrameType);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetPortIngFiltering                    */
/*                                                                           */
/*    Description         : This function sets the Ingress Filtering         */
/*                          parameter for the Given Port.                    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                    u4IfIndex      - The Interface Index                   */
/*                          u1IngFilterEnable - FNP_TRUE if Ingress          */
/*                                              filtering enabled, otherwise */
/*                                              FNP_FALSE                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortIngFiltering (UINT4 u4ContextId, UINT4 u4IfIndex,
                               UINT1 u1IngFilterEnable)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1IngFilterEnable);
    UNUSED_PARAM (u4IfIndex);
#ifdef USER_HW_API
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwVlanEnable                             */
/*                                                                           */
/*    Description         : This function enables hardware.                  */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwVlanEnable (UINT4 u4ContextId)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
#ifdef USER_HW_API
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwVlanDisable                            */
/*                                                                           */
/*    Description         : This function disables the hardware.             */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            :                                                   */
/*                                FNP_SUCCESS: Disabled and entries flushed  */
/*                                FNP_FAILURE: Disabling not supported       */
/*                                FNP_VLAN_DISABLED_BUT_NOT_DELETED:         */
/*                                 Hardware supports disabling but the       */
/*                                 entries were not flushed                  */
/*****************************************************************************/
INT4
FsMiVlanHwVlanDisable (UINT4 u4ContextId)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
#ifdef USER_HW_API
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetVlanLearningType                    */
/*                                                                           */
/*    Description         : This function programs the Vlan Learning Type    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u1LearningType - can take                        */
/*                          VLAN_INDEP_LEARNING,                             */
/*                          VLAN_SHARED_LEARNING,                            */
/*                          VLAN_HYBRID_LEARNING                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetVlanLearningType (UINT4 u4ContextId, UINT1 u1LearningType)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1LearningType);
    /* 
     * If the Hardware does not support the given Vlan Learning Type, then
     * return failure.
     */
#ifdef USER_HW_API	 
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetMacBasedStatusOnPort                */
/*                                                                           */
/*    Description         : This function enable or disable the Mac based    */
/*                        Vlan Type on the port                              */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                    u4IfIndex               - The Interface Index          */
/*                          u1MacBasedVlanEnable - FNP_TRUE/FNP_FALSE        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetMacBasedStatusOnPort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                   UINT1 u1MacBasedVlanEnable)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1MacBasedVlanEnable);
    UNUSED_PARAM (u4IfIndex);
#ifdef USER_HW_API
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetSubnetBasedStatusOnPort             */
/*                                                                           */
/*    Description         : This function enable or disable the Subnet based */
/*                          Vlan classification on the port                  */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4IfIndex   - Interface Index                    */
/*                          u1SubnetBasedVlanEnable - FNP_TRUE/FNP_FALSE     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetSubnetBasedStatusOnPort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      UINT1 u1SubnetBasedVlanEnable)
{
	INT4 ret =FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1SubnetBasedVlanEnable);
    UNUSED_PARAM (u4IfIndex);
#ifdef USER_HW_API
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwEnableProtoVlanOnPort                  */
/*                                                                           */
/*    Description         : This function enable or disable the  Port        */
/*                          Protocol based type on the port                  */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                    u4IfIndex               - The Interface Index          */
/*                           u1VlanProtoEnable - FNP_TRUE/FNP_FALSE          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwEnableProtoVlanOnPort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                 UINT1 u1VlanProtoEnable)
{
	INT4 ret =FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1VlanProtoEnable);
    UNUSED_PARAM (u4IfIndex);
#ifdef USER_HW_API
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetDefUserPriority                     */
/*                                                                           */
/*    Description         : This function set the default user priority to   */
/*                          port                                             */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                    u4IfIndex               - The Interface Index          */
/*                          i4DefPriority - default user priority can take 
 *                                            <0-7>                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetDefUserPriority (UINT4 u4ContextId, UINT4 u4IfIndex,
                              INT4 i4DefPriority)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4DefPriority);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwSetDefUserPriority (u4ContextId,u4IfIndex,i4DefPriority);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetPortNumTrafClasses                  */
/*                                                                           */
/*    Description         : This function set the number of traffic classes  */
/*                          for the port.                                    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4IfIndex               - The Interface Index    */
/*                          i4NumTraffClass - number of traffic classes can  */
/*                          take <1-8>                                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortNumTrafClasses (UINT4 u4ContextId, UINT4 u4IfIndex,
                                 INT4 i4NumTraffClass)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4NumTraffClass);

#ifdef USER_HW_API
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetRegenUserPriority                   */
/*                                                                           */
/*    Description         : This function overides the priority information  */
/*                          in the priority regeneration table maintained by */
/*                          each port for the user priority                  */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4IfIndex               - The Interface Index    */
/*                         i4UserPriority  - user priority information  can  */
/*                         take <0-7>                                        */
/*                         i4RegenPriority - regenerated user priority can   */
/*                         take <0-7>                                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetRegenUserPriority (UINT4 u4ContextId, UINT4 u4IfIndex,
                                INT4 i4UserPriority, INT4 i4RegenPriority)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4UserPriority);
    UNUSED_PARAM (i4RegenPriority);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwSetRegenUserPriority (u4ContextId,u4IfIndex,i4UserPriority,i4RegenPriority);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetTraffClassMap                       */
/*                                                                           */
/*    Description         : This function set the traffic class for which    */
/*                          user priority of the port should map into        */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4IfIndex  - The Interface Index                 */
/*                          i4UserPriority  - user priority can take <0-7>   */
/*                          i4TraffClass - traffic class to be mapped can    */
/*                          take <0-7>                                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetTraffClassMap (UINT4 u4ContextId, UINT4 u4IfIndex,
                            INT4 i4UserPriority, INT4 i4TraffClass)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4UserPriority);
    UNUSED_PARAM (i4TraffClass);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwSetTraffClassMap (u4ContextId,u4IfIndex,i4UserPriority,i4TraffClass);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGmrpEnable                             */
/*                                                                           */
/*    Description         : This function will be used to enable GMRP in H/W */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwGmrpEnable (UINT4 u4ContextId)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwGmrpEnable (u4ContextId);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGmrpDisable                            */
/*                                                                           */
/*    Description         : This function will be used to disable GMRP in    */
/*                          H/W                                              */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwGmrpDisable (UINT4 u4ContextId)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwGmrpDisable (u4ContextId);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGvrpEnable                             */
/*                                                                           */
/*    Description         : This function will be used to enable GVRP in H/W */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwGvrpEnable (UINT4 u4ContextId)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
#ifdef USER_HW_API
    ret =  EnetHal_FsMiVlanHwGvrpEnable (u4ContextId);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGvrpDisable                            */
/*                                                                           */
/*    Description         : This function will be used to disable GVRP in    */
/*                          H/W                                              */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwGvrpDisable (UINT4 u4ContextId)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
#ifdef USER_HW_API
    ret =  EnetHal_FsMiVlanHwGvrpDisable (u4ContextId);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiNpDeleteAllFdbEntries                        */
/*                                                                           */
/*    Description         : This function flushes all entres in FDB table    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiNpDeleteAllFdbEntries (UINT4 u4ContextId)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
#ifdef USER_HW_API
    ret = EnetHal_FsMiNpDeleteAllFdbEntries (u4ContextId);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwAddVlanProtocolMap                     */
/*                                                                           */
/*    Description         : This function maps the valid and existence group */
/*                          ID to Vlan ID                                    */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                    u4IfIndex - Interface Index                            */
/*                          u4GroupId  - Group Id can take <0-2147483647>    */
/*                          pProtoTemplate - pointer to protocol template    */
/*                          tVlanId - VLAN Id can take <1-4094>              */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwAddVlanProtocolMap (UINT4 u4ContextId, UINT4 u4IfIndex,
                              UINT4 u4GroupId,
                              tVlanProtoTemplate * pProtoTemplate,
                              tVlanId VlanId)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4GroupId);
    UNUSED_PARAM (pProtoTemplate);
    UNUSED_PARAM (VlanId);
#ifdef USER_HW_API
    ret =  EnetHal_FsMiVlanHwAddVlanProtocolMap (u4ContextId,u4IfIndex,u4GroupId,
    												(tEnetHal_VlanProtoTemplate *)pProtoTemplate,VlanId);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwDelVlanProtocolMap                     */
/*                                                                           */
/*    Description         : This function deletes the mapped VlanId  from    */
/*                          protocol group                                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                    u4IfIndex - Interface Index                            */
/*                          pProtoTemplate - pointer to protocol template    */
/*                          u4GroupId  - Group Id can take <0-2147483647>    */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwDelVlanProtocolMap (UINT4 u4ContextId, UINT4 u4IfIndex,
                              UINT4 u4GroupId,
                              tVlanProtoTemplate * pProtoTemplate)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4GroupId);
    UNUSED_PARAM (pProtoTemplate);
#ifdef USER_HW_API
    ret =  EnetHal_FsMiVlanHwDelVlanProtocolMap (u4ContextId,u4IfIndex,u4GroupId,(tEnetHal_VlanProtoTemplate *) pProtoTemplate);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsMiVlanHwGetPortStats                           */
/*                                                                           */
/*    Description         : This function gets the statistics value for the  */
/*                          given statistics type for a port on a vlan       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4Port     - The Port Number                     */
/*                          VlanId     - Vlan Id                             */
/*                          StatsType  - Statistics type can take any of     */
/*                                       the following.                      */
/*                                 VLAN_STAT_DOT1D_TP_PORT_IN_DISCARDS,      */
/*                                 VLAN_STAT_VLAN_PORT_IN_FRAMES,            */
/*                                 VLAN_STAT_VLAN_PORT_OUT_FRAMES,           */
/*                                 VLAN_STAT_VLAN_PORT_IN_DISCARDS,          */
/*                                 VLAN_STAT_VLAN_PORT_IN_OVERFLOW,          */
/*                                 VLAN_STAT_VLAN_PORT_OUT_OVERFLOW,         */
/*                                 VLAN_STAT_VLAN_PORT_IN_DISCARDS_OVERFLOW  */
/*                                                                           */
/*    Output(s)           : pu4PortStatsValue - Pointer to the               */
/*                          Statistics value                                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS                                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
FsMiVlanHwGetPortStats (UINT4 u4ContextId, UINT4 u4Port, tVlanId VlanId,
                        UINT1 u1StatsType, UINT4 *pu4PortStatsValue)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1StatsType);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwGetPortStats (u4ContextId,u4Port,VlanId,u1StatsType,pu4PortStatsValue);
#endif //USER_HW_API	
    /* 
     * Based on the StatsType obtain the statistics value on the 
     * u4Port/VlanId
     */
    return ret;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsMiVlanHwGetPortStats64                         */
/*                                                                           */
/*    Description         : This function gets the statistics value for the  */
/*                          given statistics type for a port on a vlan       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4Port     - The Port Number                     */
/*                          VlanId     - Vlan Id can take <1-4094>           */
/*                          StatsType  - Statistics type can take any of     */
/*                                       the following.                      */
/*                                 VLAN_STAT_DOT1D_TP_PORT_IN_DISCARDS,      */
/*                                 VLAN_STAT_VLAN_PORT_IN_FRAMES,            */
/*                                 VLAN_STAT_VLAN_PORT_OUT_FRAMES,           */
/*                                 VLAN_STAT_VLAN_PORT_IN_DISCARDS,          */
/*                                 VLAN_STAT_VLAN_PORT_IN_OVERFLOW,          */
/*                                 VLAN_STAT_VLAN_PORT_OUT_OVERFLOW,         */
/*                                 VLAN_STAT_VLAN_PORT_IN_DISCARDS_OVERFLOW  */
/*                                                                           */
/*    Output(s)           : pValue - Pointer to the Statistics value         */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS                                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
FsMiVlanHwGetPortStats64 (UINT4 u4ContextId, UINT4 u4Port, tVlanId VlanId,
                          UINT1 u1StatsType, tSNMP_COUNTER64_TYPE * pValue)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1StatsType);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwGetPortStats64 (u4ContextId,u4Port,VlanId,u1StatsType,(tEnetHal_COUNTER64_TYPE *) pValue);

    /* 
     * Based on the StatsType obtain the statistics value on the 
     * u4Port/VlanId
     */
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGetVlanStats                           */
/*                                                                           */
/*    Description         : This function scans the hardware VLAN statistics */
/*                          table and returns the required value.            */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4VlanId    - Vlan Id can take <1-4094>          */
/*                          StatsType  - Statistics type can take any of     */
/*                                       the following.                      */
/*                                 VLAN_STAT_DOT1D_TP_PORT_IN_DISCARDS,      */
/*                                 VLAN_STAT_VLAN_PORT_IN_FRAMES,            */
/*                                 VLAN_STAT_VLAN_PORT_OUT_FRAMES,           */
/*                                 VLAN_STAT_VLAN_PORT_IN_DISCARDS,          */
/*                                 VLAN_STAT_VLAN_PORT_IN_OVERFLOW,          */
/*                                 VLAN_STAT_VLAN_PORT_OUT_OVERFLOW,         */
/*                                 VLAN_STAT_VLAN_PORT_IN_DISCARDS_OVERFLOW  */
/*                                                                           */
/*    Output(s)           : pu4VlanStatsValue - returned statistics value    */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/

INT4
FsMiVlanHwGetVlanStats (UINT4 u4ContextId, tVlanId VlanId,
                        UINT1 u1StatsType, UINT4 *pu4VlanStatsValue)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1StatsType);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwGetVlanStats (u4ContextId,VlanId,u1StatsType,pu4VlanStatsValue);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwResetVlanStats                         */
/*                                                                           */
/*    Description         : This function resets harrdware VLAN statistics   */
/*                          to zero                                          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4VlanId    - Vlan Id can take <1-4094>          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwResetVlanStats (UINT4 u4ContextId, tVlanId VlanId)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
#ifdef USER_HW_API
    ret =  EnetHal_FsMiVlanHwResetVlanStats (u4ContextId,VlanId);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetPortTunnelMode                      */
/*                                                                           */
/*    Description         : This function sets the tunnel mode of a port.    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4IfIndex - Interface Index                      */
/*                          u4Mode    - Tunnel mode of a port it can take    */
/*                          VLAN_ACCESS_PORT,                                */
/*                          VLAN_TRUNK_PORT,                                 */
/*                          VLAN_HYBRID_PORT                                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortTunnelMode (UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 u4Mode)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4Mode);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwSetPortTunnelMode (u4ContextId,u4IfIndex, u4Mode);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetTunnelFilter                        */
/*                                                                           */
/*    Description         : This function creates/deletes h/w filters for    */
/*                          Provider Gvrp/Gmrp/STP Macaddress. This enables  */
/*                          the tunnel PDUs to be switched by software.      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          i4BridgeMode - Bridge Mode can take              */
/*                            VLAN_CUSTOMER_BRIDGE_MODE,                     */
/*                             VLAN_PROVIDER_BRIDGE_MODE,                    */
/*                              VLAN_PROVIDER_EDGE_BRIDGE_MODE,              */
/*                               VLAN_PROVIDER_CORE_BRIDGE_MODE              */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gVlanProviderGmrpAddr, gVlanProviderStpAddr*/
/*                                gVlanProviderGvrpAddr                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetTunnelFilter (UINT4 u4ContextId, INT4 i4BridgeMode)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (i4BridgeMode);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwSetTunnelFilter (u4ContextId,i4BridgeMode);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwCheckTagAtEgress                       */
/*                                                                           */
/*    Description         : This function specifies whether the underlying   */
/*                          H/W supports the exlusion of the provider VLAN   */
/*                          Tag by itself.                                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                                                                           */
/*    Output(s)           : pu1TagSet - VLAN_TRUE/ VLAN_FALSE                */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwCheckTagAtEgress (UINT4 u4ContextId, UINT1 *pu1TagSet)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (*pu1TagSet);
#ifdef USER_HW_API
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwCheckTagAtIngress                         */
/*                                                                           */
/*    Description         : This function specifies whether the underlying   */
/*                          H/W supports the inclusion of the provider VLAN  */
/*                          Tag by itself.                                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                                                                           */
/*    Output(s)           : pu1TagSet - VLAN_TRUE/ VLAN_FALSE                */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwCheckTagAtIngress (UINT4 u4ContextId, UINT1 *pu1TagSet)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (*pu1TagSet);
#ifdef USER_HW_API
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwCreateFdbId                            */
/*                                                                           */
/*    Description         : This function will be used to create an Fid      */
/*                          entry in H/W.                                    */
/*                          If the FID is already created in the hardware,   */
/*                          the function should return success.              */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                    u4Fid - Fdb Id                                         */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwCreateFdbId (UINT4 u4ContextId, UINT4 u4Fid)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Fid);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwCreateFdbId (u4ContextId,u4Fid);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwDeleteFdbId                            */
/*                                                                           */
/*    Description         : This function will be used to delete an Fid      */
/*                          entry in H/W.                                    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                    u4Fid - FDB Id                                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwDeleteFdbId (UINT4 u4ContextId, UINT4 u4Fid)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Fid);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwDeleteFdbId (u4ContextId,u4Fid);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwAssociateVlanFdb                       */
/*                                                                           */
/*    Description         : This function will be used to associate a VlanId */
/*                          with an Fid in H/w.                              */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                    u4Fid - FDB Id.                                        */
/*                          VlanId - Vlan Identifier can take <1-4094>.      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwAssociateVlanFdb (UINT4 u4ContextId, UINT4 u4Fid, tVlanId VlanId)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (VlanId);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwAssociateVlanFdb (u4ContextId,u4Fid,VlanId);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwDisassociateVlanFdb                    */
/*                                                                           */
/*    Description         : This function will be used to disassociate a     */
/*                          VlanId from a Fid in H/W                         */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                    u4Fid - FDB ID.                                        */
/*                          VlanId - VlanIdentifier can take <1-4094>.       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwDisassociateVlanFdb (UINT4 u4ContextId, UINT4 u4Fid, tVlanId VlanId)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (VlanId);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwDisassociateVlanFdb (u4ContextId,u4Fid,VlanId);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwFlushPortFdbId                         */
/*                                                                           */
/*    Description         : This function flushes the Fdb entires learnt on  */
/*                          a specific port in a specific Fdb table          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4Port       - The Interface Index               */
/*                          u4Fid      - Fdb Id in which the Entry has to be */
/*                                        flushed.                           */
/*                           i4OptimizeFlag - Indicates whether this call can*/
/*                                            be grouped with the flush call */
/*                                            for other ports as a single    */
/*                                            flush call can take            */
/*                                            VLAN_NO_OPTIMIZE,              */
/*                                            VLAN_OPTIMIZE                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwFlushPortFdbId (UINT4 u4ContextId, UINT4 u4Port, UINT4 u4Fid,
                          INT4 i4OptimizeFlag)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (i4OptimizeFlag);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwFlushPortFdbId (u4ContextId,u4Port,u4Fid,i4OptimizeFlag);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwFlushPortFdbList                       */
/*                                                                           */
/*    Description         : This function flushes the Fdb entires learnt on  */
/*                          a specific port in a specific Fdb table          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2Port               - The Interface Index       */
/*                          u4Fid   - Fdb Id in which the Entry has to be    */
/*                                   flushed.                                */
/*                           i4OptimizeFlag - Indicates whether this call can*/
/*                                            be grouped with the flush call */
/*                                            for other ports as a single    */
/*                                            flush call                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwFlushPortFdbList (UINT4 u4ContextId, tVlanFlushInfo * pVlanFlushInfo)
{
	INT4 ret=FNP_SUCCESS;
   UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pVlanFlushInfo);
#ifdef USER_HW_API
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwFlushPort                              */
/*                                                                           */
/*    Description         : This function flushes the FDB entries            */
/*                          learnt on this port. Flushing                    */
/*                          is done based on port in all  FDB Table          */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4IfIndex               - The Interface Index    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwFlushPort (UINT4 u4ContextId, UINT4 u4IfIndex, INT4 i4OptimizeFlag)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4OptimizeFlag);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwFlushPort (u4ContextId,u4IfIndex,i4OptimizeFlag);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwFlushFdbId                             */
/*                                                                           */
/*    Description         : This function flushes all the learnt FDB entries */
/*                          in the FDB table                                 */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4Fid               - The FDB ID                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwFlushFdbId (UINT4 u4ContextId, UINT4 u4Fid)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Fid);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwFlushFdbId (u4ContextId,u4Fid);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name   : FsMiVlanHwSetShortAgeout                             */
/*                                                                           */
/*    Description     : This function sets the short age out time for        */
/*                      this port.                                           */
/*                                                                           */
/*    Input(s)        : u4ContextId - Context Id                             */
/*                      u4Port - port number                                 */
/*                      i4AgingTime - time in seconds can 
 *                      take  <10 - 1000000>                                 */
/*                                                                           */
/*    Output(s)       : None                                                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
FsMiVlanHwSetShortAgeout (UINT4 u4ContextId, UINT4 u4Port, INT4 i4AgingTime)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (i4AgingTime);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwSetShortAgeout (u4ContextId,u4Port,i4AgingTime);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name   : FsMiVlanHwResetShortAgeout                           */
/*                                                                           */
/*    Description     : This function resets the short age out time and      */
/*                      reverts back to the long ageout for this port.       */
/*                                                                           */
/*    Input(s)        : u4ContextId - Context Id                             */
/*                u4Port - port number                                       */
/*                      i4LongAgeout - Long ageout time to be reverted to    */
/*                                                                           */
/*    Output(s)       : None                                                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
FsMiVlanHwResetShortAgeout (UINT4 u4ContextId, UINT4 u4Port, INT4 i4LongAgeout)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (i4LongAgeout);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwResetShortAgeout (u4ContextId,u4Port,i4LongAgeout);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGetVlanInfo                            */
/*                                                                           */
/*    Description         : This function returns the VLAN membership of the */
/*                          given VLAN in the hardware if it is present.     */
/*                          The VLAN membership returned should not include  */
/*                          the physical ports that are part of aggregation  */
/*                          group. i.e. if P1, P2 and P3 are members of      */
/*                          agg. group P26, then the port list returned      */
/*                          should contain P26 and not P1, P2 and P3.        */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                    VlanId - VLAN id for which the info needs to be        */
/*                          retrieved.                                       */
/*                                                                           */
/*    Output(s)           : pHwEntry - VLAN info pointer.                    */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwGetVlanInfo (UINT4 u4ContextId, tVlanId VlanId,
                       tHwVlanPortArray * pHwEntry)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pHwEntry);
#ifdef USER_HW_API
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGetMcastEntry                          */
/*                                                                           */
/*    Description         : This function gets the given multicast entry     */
/*                          present in the hardware.                         */
/*                          The MCAST membership returned should not include */
/*                          the physical ports that are part of aggregation  */
/*                          group. i.e. if P1, P2 and P3 are members of      */
/*                          agg. group P26, then the port list returned      */
/*                          should contain P26 and not P1, P2 and P3.        */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                    VlanId - VLAN Id of the mcast entry.                   */
/*                          MacAdd - Mcast mac address.                      */
/*                                                                           */
/*    Output(s)           : pHwMcastPorts- pointer to Mcast Ports in Hardware*/
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwGetMcastEntry (UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr,
                         tHwPortArray * pHwMcastPorts)
{
	INT4 ret=FNP_SUCCESS;

#ifdef USER_HW_API
	tEnetHal_MacAddr mac_addr;
    adap_mac_from_arr(&mac_addr, MacAddr);
    ret = EnetHal_FsMiVlanHwGetMcastEntry (u4ContextId,VlanId,&mac_addr,(tEnetHal_HwPortArray *) pHwMcastPorts);
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (pHwMcastPorts);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsMiVlanHwTraffClassMapInit                      */
/*                                                                           */
/*    Description         : This function sets which cosq a given priority   */
/*                          should fall into in all units.                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u1Priority - Cosq Priority                       */
/*                          i4CosqValue - Cosq Value can take value from the 
 *                           gau1PriTrfClassMap array                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*                                                                           */
/*****************************************************************************/

INT4
FsMiVlanHwTraffClassMapInit (UINT4 u4ContextId, UINT1 u1Priority,
                             INT4 i4CosqValue)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1Priority);
    UNUSED_PARAM (i4CosqValue);
#ifdef USER_HW_API
#endif //USER_HW_API
    return ret;
}

/***************************************************************************/
/* FUNCTION NAME :  FsMiBrgSetAgingTime                                    */
/*                                                                         */
/* DESCRIPTION   :  Set the FDB aging time value for the given context.    */
/*                  This will be called either during the STAP operation   */
/*                  during reconfiguration to time out entries learned     */
/*                  during this period. Also can be used to set aging      */
/*                  time other than default value 300secs supported by     */
/*                  the 802.1D/802.1Q standard.                            */
/*                                                                         */
/* INPUTS        :  u4ContextId - context Identifier                       */
/*                  i4Aging - time in seconds ranges <10-1000000>          */
/* OUTPUTS       :  None.                                                  */
/*                                                                         */
/* RETURNS       :  FNP_SUCCESS - on success                               */
/*                  FNP_FAILURE - on error                                 */
/*                                                                         */
/* COMMENTS      :  None                                                   */
/***************************************************************************/
INT1
FsMiBrgSetAgingTime (UINT4 u4ContextId, INT4 i4AgingTime)
{
	INT1 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (i4AgingTime);
#ifdef USER_HW_API
    ret = EnetHal_FsBrgSetAgingTime (i4AgingTime);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetBrgMode                             */
/*                                                                           */
/*    Description         : This function sets the bridge mode in hardware.  */
/*                                                                           */
/*                          For chipset that requires offset for filter      */
/*                          installation, all the filters needs to be        */
/*                          reinstalled with the new offset appropriate to   */
/*                          the bridge-mode.                                 */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4BridgeMode - VLAN_CUSTOMER_BRIDGE_MODE /       */
/*                          VLAN_PROVIDER_BRIDGE_MODE /                      */
/*                          VLAN_PROVIDER_EDGE_BRIDGE_MODE /                 */
/*                          VLAN_PROVIDER_CORE_BRIDGE_MODE                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns                 : FNP_SUCCESS/FNP_FAILURE                      */
/*****************************************************************************/
INT4
FsMiVlanHwSetBrgMode (UINT4 u4ContextId, UINT4 u4BridgeMode)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4BridgeMode);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwSetBrgMode (u4ContextId,u4BridgeMode);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetBaseBridgeMode                     */
/*                                                                           */
/*    Description         : This function sets the 802.1D Mode in hardware   */
/*                                                                           */
/*    Input(s)            : u4Mode    - Enable / Diable 802.1D Mode          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwSetBaseBridgeMode (UINT4 u4IfIndex, UINT4 u4Mode)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4Mode);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwSetBaseBridgeMode (u4IfIndex,u4Mode);
#endif //USER_HW_API	
    return ret;
}

#ifdef L2RED_WANTED
/*****************************************************************************/
/*    Function Name       : FsMiVlanHwScanProtocolVlanTbl                    */
/*                                                                           */
/*    Description         : This function scans the protocol VLAN table and  */
/*                          calls the callback provided by the application.  */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                    ProtoVlanCallBack - Protocol VLAN call back            */
/*                          function pointer.                                */
/*                          u4Port - Port for which the scan needs to be     */
/*                          performed.                                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwScanProtocolVlanTbl (UINT4 u4ContextId, UINT4 u4Port,
                               FsMiVlanHwProtoCb ProtoVlanCallBack)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (ProtoVlanCallBack);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGetVlanProtocolMap                     */
/*                                                                           */
/*    Description         : This function returns the VLAN id for which the  */
/*                          protocol group is mapped.                        */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4IfIndex - Interface Index                      */
/*                          u4GroupId  - Group Id can take <0-2147483647>    */
/*                          pProtoTemplate - pointer to protocol template    */
/*                                                                           */
/*    Output(s)           : pVlanId - Pointer to the VLAN ID.                */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwGetVlanProtocolMap (UINT4 u4ContextId, UINT4 u4IfIndex,
                              UINT4 u4GroupId,
                              tVlanProtoTemplate * pProtoTemplate,
                              tVlanId * pVlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4GroupId);
    UNUSED_PARAM (pProtoTemplate);
    UNUSED_PARAM (pVlanId);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwScanMulticastTbl                       */
/*                                                                           */
/*    Description         : This function scans the VLAN multicast table and */
/*                          calls the callback provided by the application.  */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          McastCallBack - Multicast call back function     */
/*                          pointer. Call back function's last argument is a */
/*                          port array. So if the hw api for getting         */
/*                          multicast entry information gives port bitmap,   */
/*                          then convert that port bit map to port array and */
/*                          call the call back function.                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwScanMulticastTbl (UINT4 u4ContextId, FsMiVlanHwMcastCb McastCallBack)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (McastCallBack);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGetStMcastEntry                        */
/*                                                                           */
/*    Description         : This function gets the given static mcast entry  */
/*                          present in the hardware.                         */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId - VLAN Id of the mcast entry.             */
/*                          MacAdd - Mcast mac address.                      */
/*                          u4RcvPort - Receive port .                       */
/*                          pHwMcastPorts - pointer to get Mcast Ports in    */
/*                                  Hardware                                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwGetStMcastEntry (UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr,
                           UINT4 u4RcvPort, tHwPortArray * pHwMcastPorts)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4RcvPort);
    UNUSED_PARAM (pHwMcastPorts);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwScanMulticastTbl                       */
/*                                                                           */
/*    Description         : This function scans the VLAN multicast table and */
/*                          calls the callback provided by the application.  */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          McastCallBack - Multicast call back function     */
/*                          pointer.                                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiVlanHwScanUnicastTbl (UINT4 u4ContextId, FsMiVlanHwUcastCb UcastCallBack)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (UcastCallBack);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGetStaticUcastEntry                    */
/*                                                                           */
/*    Description         : This function gets the static unicast entry      */
/*                          for the given FDB Id and mac address.            */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                    u4Fid            - Fid                                 */
/*                          pMacAddr         - Pointer to the Mac Address.   */
/*                          u4Port           - Received Interface Index      */
/*                                                                           */
/*    Output(s)           : pAllowedToGoPorts - pointer to AllowedToGoPorts  */
/*                          pu1Status        -  Status of entry takes values */
/*                          like  VLAN_PERMANENT, VLAN_DELETE_ON_RESET,      */
/*                          VLAN_DELETE_ON_TIMEOUT                           */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwGetStaticUcastEntry (UINT4 u4ContextId, UINT4 u4Fid, tMacAddr MacAddr,
                               UINT4 u4Port, tHwPortArray * pAllowedToGoPorts,
                               UINT1 *pu1Status)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (pAllowedToGoPorts);
    UNUSED_PARAM (pu1Status);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwGetSyncedTnlProtocolMacAddr                */
/*                                                                           */
/* Description        : This function validates filter mac address with sync */
/*                      mac address.                                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      MacAddr - MAC address used for protocol tunneling.   */
/*                      u2Protocol - L2 protocol for which the tunnel MAC    */
/*                                   address is configured for tunneling.    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On success                            */
/*                      VLAN_FAILURE - On failure                            */
/*****************************************************************************/
INT4
FsMiVlanHwGetSyncedTnlProtocolMacAddr (UINT4 u4ContextId, UINT2 u2Protocol,
                                       tMacAddr MacAddr)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Protocol);
    UNUSED_PARAM (MacAddr);

    return FNP_SUCCESS;
}
#endif /* L2RED_WANTED */

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwAddPortMacVlanEntry                    */
/*                                                                           */
/*    Description         : This function adds the MAC based VLAN entry in   */
/*                          the H/W table.                                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4IfIndex   - Port number                        */
/*                          MacAddr     - Mac Address.                       */
/*                          VlanId      - Vlan Identifier can take <1-4094>  */
/*                          bSuppressOption - FNP_TRUE/FNP_FALSE             */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwAddPortMacVlanEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                               tMacAddr MacAddr, tVlanId VlanId,
                               BOOL1 bSuppressOption)
{
	INT4 ret=FNP_SUCCESS;

#ifdef USER_HW_API
	tEnetHal_MacAddr mac_addr;
    adap_mac_from_arr(&mac_addr, MacAddr);
    ret = EnetHal_FsMiVlanHwAddPortMacVlanEntry (u4ContextId,u4IfIndex,&mac_addr,VlanId,bSuppressOption);
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (bSuppressOption);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (VlanId);
#endif //USER_HW_API

    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwDeletePortMacVlanEntry                 */
/*                                                                           */
/*    Description         : This function deletes the MAC based VLAN entry in*/
/*                          the H/W table.                                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4IfIndex   - Port number                        */
/*                          MacAddr     - Mac Address.                       */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwDeletePortMacVlanEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  tMacAddr MacAddr)
{
	INT4 ret=FNP_SUCCESS;

#ifdef USER_HW_API
	tEnetHal_MacAddr mac_addr;
    adap_mac_from_arr(&mac_addr, MacAddr);
    ret = EnetHal_FsMiVlanHwDeletePortMacVlanEntry(u4ContextId,u4IfIndex, &mac_addr);
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (MacAddr);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwAddPortSubnetVlanEntry                 */
/*                                                                           */
/*    Description         : This function adds the Subnet based VLAN entry   */
/*                          in the H/W table.                                */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4IfIndex   - Port number                        */
/*                          SubnetAddr  - Subnet Address.                    */
/*                          VlanId      - Vlan Identifier can take <1-4094>  */
/*                          bArpOption  - FNP_TRUE/FNP_FALSE                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwAddPortSubnetVlanEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  UINT4 SubnetAddr, tVlanId VlanId,
                                  BOOL1 bARPOption)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (bARPOption);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SubnetAddr);
    UNUSED_PARAM (VlanId);
#ifdef USER_HW_API
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwDeletePortSubnetVlanEntry              */
/*                                                                           */
/*    Description         : This function deletes the Subnet based VLAN entry*/
/*                          in the H/W table.                                */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4IfIndex   - Port number                        */
/*                          SubnetAddr  - Subnet Address                     */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwDeletePortSubnetVlanEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                     UINT4 SubnetAddr)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SubnetAddr);
#ifdef USER_HW_API
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwUpdatePortSubnetVlanEntry              */
/*                                                                           */
/*    Description         : This function adds the Subnet based VLAN entry   */
/*                          in the H/W table.                                */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4IfIndex   - Port number                        */
/*                          u4SubnetAddr  - Subnet Address.                  */
/*                          u4SubnetAddr  - Subnet Mask   .                  */
/*                          VlanId      - Vlan Identifier can take <1-4094>  */
/*                          bArpOption  - FNP_TRUE/FNP_FALSE                 */
/*                          u1Action    - VLAN_ADD/VLAN_DELETE               */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwUpdatePortSubnetVlanEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                     UINT4 u4SubnetAddr, UINT4 u4SubnetMask,
                                     tVlanId VlanId, BOOL1 bARPOption,
                                     UINT1 u1Action)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4SubnetAddr);
    UNUSED_PARAM (u4SubnetMask);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1Action);
    UNUSED_PARAM (bARPOption);
#ifdef USER_HW_API
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanNpHwRunMacAgeing                         */
/*                                                                           */
/*    Description         : The function initiates the "hardware FDB entries"*/
/*                          ageing process when a timer expiry event is      */
/*                          received for a given context ID                  */
/*                                                                           */
/*    Input(s)            : u4ContextId      - Virtual Switch ID             */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanNpHwRunMacAgeing (UINT4 u4ContextId)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
#ifdef USER_HW_API
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwMacLearningLimit                           */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning limit for a VLAN in the given context.      */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      VlanId  - VLAN identifier can take <1-4094>.         */
/*                      u2FdbId - Fdb Identifier                             */
/*                      u4MacLimit - MAC learning limit can take 
 *                                       <0-4294967295>.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwMacLearningLimit (UINT4 u4ContextId, tVlanId VlanId,
                            UINT2 u2FdbId, UINT4 u4MacLimit)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2FdbId);
    UNUSED_PARAM (u4MacLimit);
#ifdef USER_HW_API
    if (EnetHal_VlanHwMacLearningLimit(u4ContextId, VlanId, u2FdbId, u4MacLimit) != ENET_SUCCESS)
    {
        return FNP_FAILURE;
    }
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSwitchMacLearningLimit                     */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning limit for the given context.                */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4MacLimit - MAC learning limit.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSwitchMacLearningLimit (UINT4 u4ContextId, UINT4 u4MacLimit)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4MacLimit);
#ifdef USER_HW_API
    if (EnetHal_VlanHwSwitchMacLearningLimit(u4ContextId, u4MacLimit) != ENET_SUCCESS)
    {
        return FNP_FAILURE;
    }
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwMacLearningStatus                          */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning status for a VLAN in the given context.     */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      VlanId  - VLAN identifier.                           */
/*                      pHwEgressPorts - pointer to array of egress ports.   */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwMacLearningStatus (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2FdbId,
                             tHwPortArray * pHwEgressPorts, UINT1 u1Status)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2FdbId);
    UNUSED_PARAM (pHwEgressPorts);
    UNUSED_PARAM (u1Status);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwMacLearningStatus (u4ContextId,VlanId,u2FdbId,(tEnetHal_HwPortArray *)pHwEgressPorts,u1Status);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetFidPortLearningStatus                   */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning status for a Port in the given Fid and      */
/*                      in give  context.                                    */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4Fid       - Fdb Identifier                         */
/*                      HwPortList - Port list where MAC learning status    */
/*                      should be applied                                    */
/*                      u4Port - Port where mac lreaning status will applied */
/*                               it will be used only if HwPortList is null.*/
/*                      u1Action - learning enable/disable                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsMiVlanHwSetFidPortLearningStatus (UINT4 u4ContextId, UINT4 u4Fid,
                                    tHwPortArray HwPortList, UINT4 u4Port,
                                    UINT1 u1Action)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (HwPortList);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u1Action);
#ifdef USER_HW_API
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetProtocolTunnelStatusOnPort              */
/*                                                                           */
/* Description        : This function is called for configuring the action   */
/*                      to be taken on receiving control protocol PDUs on    */
/*                      the given port in the given virtual context.         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual context Id                     */
/*                      u4IfIndex   - Interface index                        */
/*                      ProtocolId  - Control Protocol for which action      */
/*                                    is being configured.                   */
/*                      u4TunnelStatus - Tunnel status                       */
/*                         VLAN_TUNNEL_PROTOCOL_PEER    - Normal             */
/*                         VLAN_TUNNEL_PROTOCOL_TUNNEL  - Encapsulate        */
/*                         VLAN_TUNNEL_PROTOCOL_DISCARD - Discard            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetProtocolTunnelStatusOnPort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                         tVlanHwTunnelFilters ProtocolId,
                                         UINT4 u4TunnelStatus)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (ProtocolId);
    UNUSED_PARAM (u4TunnelStatus);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwSetProtocolTunnelStatusOnPort (u4ContextId,u4IfIndex,(eEnetHal_VlanHwTunnelFilters)ProtocolId,u4TunnelStatus);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPortProtectedStatus                     */
/*                                                                           */
/* Description        : This function is called for configuring the          */
/*                      Protected/Split Horizon Status on port.              */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual context Id                     */
/*                      u4IfIndex - Interface index.                         */
/*                      i4ProtectedStatus - Port's protected status          */
/*                          VLAN_SNMP_TRUE - protected status enabled.       */
/*                          VLAN_SNMP_FALSE - protected status disabled.     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortProtectedStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  INT4 i4ProtectedStatus)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4ProtectedStatus);
#ifdef USER_HW_API
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwAddStaticUcastEntryEx                  */
/*                                                                           */
/*    Description         : This function adds the egress ports  into        */
/*                          hardware Static Unicast table                    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4Fid            - Fid                           */
/*                          pMacAddr         - Pointer to the Mac Address.   */
/*                          u4Port           - Received Interface Index      */
/*                          pHwAllowedToGoPorts - pointer to array of        */
/*                                                allowed to go ports        */
/*                          u1Status         -  Status of entry takes values */
/*                          like  permanent, deleteOnReset and               */
/*                           deleteOnTimeout                                 */
/*                          ConnectionId     - BackBone MacAddress           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwAddStaticUcastEntryEx (UINT4 u4ContextId, UINT4 u4Fid,
                                 tMacAddr MacAddr, UINT4 u4Port,
                                 tHwPortArray * pHwAllowedToGoPorts,
                                 UINT1 u1Status, tMacAddr ConnectionId)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (pHwAllowedToGoPorts);
    UNUSED_PARAM (u1Status);
    UNUSED_PARAM (ConnectionId);
#ifdef USER_HW_API
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsMiVlanHwGetStaticUcastEntryEx                  */
/*                                                                           */
/*    Description         : This function gets the static unicast entry      */
/*                          for the given FDB Id and mac address.            */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4Fid            - Fid                           */
/*                          pMacAddr         - Pointer to the Mac Address.   */
/*                          u4Port           - Received Interface Index      */
/*                                                                           */
/*    Output(s)           : pAllowedToGoPorts - pointer to AllowedToGoPorts  */
/*                          pu1Status        -  Status of entry takes values */
/*                          like  permanent, deleteOnReset and               */
/*                           deleteOnTimeout
                            ConnectioId - BackBone Mac Address               */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiVlanHwGetStaticUcastEntryEx (UINT4 u4ContextId, UINT4 u4Fid,
                                 tMacAddr MacAddr, UINT4 u4Port,
                                 tHwPortArray * pAllowedToGoPorts,
                                 UINT1 *pu1Status, tMacAddr ConnectionId)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (pAllowedToGoPorts);
    UNUSED_PARAM (pu1Status);
    UNUSED_PARAM (ConnectionId);
#ifdef USER_HW_API
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwForwardPktOnPorts                        */
/*    Description         : This function transmits the provided packet to   */
/*                          the given ports. This function should also       */
/*                          apply the egress rules for the ports and         */
/*                          add/update/remove VLAN tag of the packet based on*/
/*                          the VLAN configuration before transmitting the   */
/*                          packet.                                          */
/*    Input (s)           : pu1Packet - Pointer to packet buffer             */
/*                          u2PacketLen - Length of the packet               */
/*                          pVlanFwdInfo - Pointer to VLAN information.      */
/*    Output(s)           : None                                             */
/*    Returns             : FNP_SUCCESS/FNP_FAILURE                          */
/*****************************************************************************/
INT4
FsVlanHwForwardPktOnPorts (UINT1 *pu1Packet, UINT2 u2PacketLen,
                           tHwVlanFwdInfo * pVlanFwdInfo)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (pu1Packet);
    UNUSED_PARAM (u2PacketLen);
    UNUSED_PARAM (pVlanFwdInfo);
#ifdef USER_HW_API
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwPortMacLearningStatus.                     */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning status for a Port                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwPortMacLearningStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                 UINT1 u1Status)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwPortMacLearningStatus (u4ContextId,u4IfIndex,u1Status);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwGetMacLearningMode                       */
/*    Description         : This function sets the hardware mac learning     */
/*                          method adopted for the platform. It will be      */
/*                          invoked from Vlan hardware initialization        */
/*                          routine during system startup                    */
/*                                                                           */
/*                          The different types of mac learning methods      */
/*                          supported are:                                   */
/*                                                                           */
/*                          1) HW_UPDATED_CPU_LEARNING - Hardware updated    */
/*                                CPU learning . equivalent to SW_LEARNING   */
/*                          2) HW_INDICATED_CPU_PROGRAMMING - Hardware       */
/*                                indicated CPU programming . equivalent to  */
/*                                SW_LEARNING + updating hardware table      */
/*                          3) CPU_DIRECTED_LEARNING - CPU directed learning */
/*                                no learning in control plane (database)    */
/*    Input (s)           : None                                             */
/*    Output(s)           : pu4LearningMode - returns the h/w mac learning   */
/*                                            mode used in the system        */
/*    Returns             : FNP_SUCCESS/FNP_FAILURE                          */
/*****************************************************************************/
INT4
FsVlanHwGetMacLearningMode (UINT4 *pu4LearningMode)
{
	INT4 ret = FNP_SUCCESS;

    /* In case HW_UPDATED_CPU_LEARNING, then the flow is
       HW add/delete entry to MAC table --> Notify control plane -->
       --> NPAPI call back function --> VLAN API --> VLAN Add/Delete
       to control plane data base */

    /* In case HW_INDICATED_CPU_PROGRAMMING, then the flow is
       HW learns/ageout entry of MAC table --> Notify control plane --> NPAPI
       call back function --> VLAN API --> VLAN Add/Delete to control plane
       data base --> Call NPAPI to program MAC table */

    /* In case CPU_DIRECTED_LEARNING, then the flow is HW forward packet to CPU
       in ase of no MAC --> VLAN module process packet --> VLAN Add/Delete to
       control data base --> Call NPAPI to program MAC table */

#ifdef USER_HW_API
    ret = EnetHal_FsVlanHwGetMacLearningMode (pu4LearningMode);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FsMiVlanHwSetMcastIndex                                    */
/*                                                                           */
/* Description  : This function returns the Hw Multicast Index to be used    */
/*                for programming Multicast MAC Entry.                       */
/*                The Opcode passed to the function can take below values    */
/*                                                                           */
/*                VLAN_OPCODE_GET_MCAST_INDEX - A new Multicast Index will   */
/*                be generated and returned provided there is no entry for   */
/*                given MAC and VLAN. If the entry is available,corresponding*/
/*                Index will be returned                                     */
/*                                                                           */
/*                VLAN_OPCODE_SET_MCAST_INDEX - Multicast Index for the      */
/*                corresponding MAC and VLAN will be returned                */
/*                                                                           */
/* Input        : HwMcastIndexInfo - Entry which contains all required       */
/*                Information like opcode, VLAN and MAC                      */
/*                                                                           */
/* Output       : pu4McastIndex - Multicast Index to be programmed in Hw.    */
/*                             - only for opcode VLAN_OPCODE_GET_MCAST_INDEX */
/*                                                                           */
/* Returns      : FNP_SUCCESS / FNP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
FsMiVlanHwSetMcastIndex (tHwMcastIndexInfo HwMcastIndexInfo,
                         UINT4 *pu4McastIndex)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (HwMcastIndexInfo);
    UNUSED_PARAM (pu4McastIndex);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwSetMcastIndex ((tEnetHal_HwMcastIndexInfo *)&HwMcastIndexInfo,pu4McastIndex);
#endif //USER_HW_API	
    return ret;

}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPortIngressEtherType.                   */
/*                                                                           */
/* Description        : This function is called when the ingress ether type  */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2EtherType - Configured Ingress Ether type          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortIngressEtherType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                   UINT2 u2EtherType)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2EtherType);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwSetPortIngressEtherType (u4ContextId,u4IfIndex,u2EtherType);
#endif //USER_HW_API	
    return ret;

}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPortEgressEtherType.                    */
/*                                                                           */
/* Description        : This function is called when the Egress ether type   */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2EtherType - Configured Egress Ether type           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortEgressEtherType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  UINT2 u2EtherType)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2EtherType);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwSetPortEgressEtherType (u4ContextId,u4IfIndex,u2EtherType);
#endif //USER_HW_API
    return ret;

}

/* Require VlanPortProperty parameter. */
/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetPortProperty                            */
/*                                                                           */
/* Description        : This function sets the Vlan Port Properties          */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      VlanPortProperty Port Property Structure             */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetPortProperty (UINT4 u4ContextId, UINT4 u4IfIndex,
                           tHwVlanPortProperty VlanPortProperty)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (VlanPortProperty);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwSetPortProperty (u4ContextId,u4IfIndex,(tEnetHal_HwVlanPortProperty *)&VlanPortProperty);
#endif //USER_HW_API	
    return ret;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwSetVlanLoopbackStatus                      */
/*                                                                           */
/* Description        : This function sets the Vlan Port Properties          */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      VlanId - Vlan Identifier                             */
/*                      i4LoopbackStatus = Enabled/Disabled Status           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwSetVlanLoopbackStatus (UINT4 u4ContextId, tVlanId VlanId,
                                 INT4 i4LoopbackStatus)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (i4LoopbackStatus);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwSetVlanLoopbackStatus (u4ContextId,VlanId,i4LoopbackStatus);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/* Function Name      : FsMiVlanHwPortUnicastMacSecType                      */
/*                                                                           */
/* Description        : This function is called for configuring the Unicast  */
/*                      mac security type for a Port                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex  - Port identifier.                        */
/*                      u4MacSecLevel - Mac Limit of the Unicast Table ranges*/
/*                      < 0 - 10 >                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiVlanHwPortUnicastMacSecType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                INT4 i4MacSecType)
{
	INT4 ret = FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (i4MacSecType);
    UNUSED_PARAM (u4IfIndex);
#ifdef USER_HW_API
    ret = EnetHal_FsMiVlanHwPortUnicastMacSecType (u4ContextId,u4IfIndex,i4MacSecType);
#endif //USER_HW_API	
    return ret;
}
/*****************************************************************************/
/*    Function Name       : FsMiVlanHwSetBridgePortType                      */
/*                                                                           */
/*    Description         : This function provides support to set the        */
/*                         properties of an Uplink Acces Port (UAP)          */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : pVlanHwPortInfo  -     parameters to be          */
/*                                                 programmed in hardware.   */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/

INT4 FsMiVlanHwSetBridgePortType  (tVlanHwPortInfo *pVlanHwPortInfo)
{

        UNUSED_PARAM (pVlanHwPortInfo);
        return FNP_SUCCESS;
}
/***************************************************************************
 *
 *    Function Name       : FsMiVlanHwPortPktReflectStatus
 *
 *    Description         : This function is called for handling packet reflection
 *                          feature.
 *
 *    Input(s)            : pPortReflectEntry -pointer to structure
 *                          tFsNpVlanPortReflectEntry
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/


UINT1
FsMiVlanHwPortPktReflectStatus (tFsNpVlanPortReflectEntry *
                                    pPortReflectEntry)
{
    UNUSED_PARAM (pPortReflectEntry);
    return FNP_SUCCESS;
}
