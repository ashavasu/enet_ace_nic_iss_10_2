/*****************************************************************************/
/* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*******************************************************************************
**    FILE  NAME             : ofcnp.c
**    PRINCIPAL AUTHOR       : Aricent Inc.
**    SUBSYSTEM NAME         : OFC
**    MODULE NAME            : OFC-NPAPI MODULE
**    LANGUAGE               : ANSI-C
**    DATE OF FIRST RELEASE  :
**    DESCRIPTION            : This file contains routines for Hardware flow table
**                             implementations for both TCAM and NON-TCAM based 
**                               chipsets
* * $Id: ofcnp.c,v 1.1
**---------------------------------------------------------------------------*/
#ifndef _OFCNP_C_
#define _OFCNP_C_

#include "npapi.h"
#include "ofcnp.h"
#include "npcfa.h"
#include "nputil.h"

#include "adap_types.h"
#include "adap_logger.h"
#include "adap_openFlow.h"

/*********************************************************************************************
*    Function Name      : FsOfcHwOpenflowInitAclFilters
*    Description        : This function is used to Reserve the HW ACL Filters for Openflow
*                         Client
*    Input(s)           : u4Entries -> HW Number of Param Values
*    Output(s)          : None.
*    Returns            : FNP_SUCCESS/FNP_FAILURE
*********************************************************************************************/

INT4
FsOfcHwOpenflowInitAclFilters (UINT4 u4Entries)
{
    UNUSED_PARAM (u4Entries);
    INT4 ret = FNP_SUCCESS;
#ifdef USER_HW_API
	ret = EnetHal_FsOfcHwOpenflowInitAclFilters (u4Entries);
#endif
    return ret;
}

/*********************************************************************************************
*    Function Name      : FsOfcHwOpenflowInitMeterEntries
*    Description        : This function is used to Reserve the HW Meter Entries for Openflow
*                         Client
*    Input(s)           : u4Entries -> HW Number of Param Values
*    Output(s)          : None.
*    Returns            : FNP_SUCCESS/FNP_FAILURE
*********************************************************************************************/

INT4
FsOfcHwOpenflowInitMeterEntries (UINT4 u4Entries)
{
    UNUSED_PARAM (u4Entries);
    INT4 ret = FNP_SUCCESS;
#ifdef USER_HW_API
	ret = EnetHal_FsOfcHwOpenflowInitMeterEntries (u4Entries);
#endif
    return ret;
}

/*********************************************************************************************
*    Function Name      : FsOfcHwOpenflowClientCfgParamsUpdate
*    Description        : This function is used to update the Openflow Client parameter
*                         Configurations
*    Input(s)           : OfcCfgParam -> Configuration Paramater Identified
*                         u4CfgParamValue -> Cfg Param Value
*    Output(s)          : None.
*    Returns            : FNP_SUCCESS/FNP_FAILURE
*********************************************************************************************/

INT4
FsOfcHwOpenflowClientCfgParamsUpdate (eOfcCfgParam OfcCfgParam,
                                      UINT4 u4CfgParamValue)
{
    UNUSED_PARAM (OfcCfgParam);
    UNUSED_PARAM (u4CfgParamValue);
    INT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
	ret = EnetHal_FsOfcHwOpenflowClientCfgParamsUpdate ((enetOfcCfgParam)OfcCfgParam,u4CfgParamValue);
#endif
    return ret;
}

/***********************************************************************************************
*    Function Name       : FsOfcHwUpdateGroupEntry                        
*    Description         : This function is used to update the Group Entries to HW Group Table             
*    Input(s)            : pOfcGroupEntry->Pointer to the Group Entry fields                                 
*                          OfcCmd ->Action to be performed                                                    
*    Output(s)           : None.
*    Returns             : FNP_SUCCESS/FNP_FAILURE                                  
************************************************************************************************/
INT4
FsOfcHwUpdateGroupEntry (tOfcHwGroupInfo * pOfcGroupEntry, eOfcCmd OfcCmd)
{
    UNUSED_PARAM (OfcCmd);
    UNUSED_PARAM (pOfcGroupEntry);

    INT4 ret=FNP_SUCCESS;

#ifdef USER_HW_API
    tEnetOfcHwGroupInfo tEnetOfcGroupEntry;

	memset(&tEnetOfcGroupEntry,0,sizeof(tEnetOfcHwGroupInfo));

	if(pOfcGroupEntry != NULL)
	{
		tEnetOfcGroupEntry.ofcGroupDurationSec = pOfcGroupEntry->u4FsofcGroupDurationSec;
		tEnetOfcGroupEntry.ofcGroupIndex = pOfcGroupEntry->u4FsofcGroupIndex;
	}
	ret = EnetHal_FsOfcHwUpdateGroupEntry (&tEnetOfcGroupEntry,(enetOfcCmd)OfcCmd);
#endif
    return ret;
}

/**********************************************************************************************
*    Function Name       : FsOfcHwUpdateMeterEntry 
*    Description         : This function is used to update the Meter Entries to HW Meter Table
*    Input(s)            : pOfcMeterEntry->Pointer to the Meter Entry fields
*                          OfcCmd ->Action to be performed
*    Output(s)           : None.
*    Returns             : FNP_SUCCESS/FNP_FAILURE
**********************************************************************************************/
INT4
FsOfcHwUpdateMeterEntry (tOfcHwMeterInfo * pOfcMeterEntry, eOfcCmd OfcCmd)
{
    UNUSED_PARAM (OfcCmd);
    UNUSED_PARAM (pOfcMeterEntry);
    INT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
    tEnetOfcHwMeterInfo tOfcMeterEntry;

	memset(&tOfcMeterEntry,0,sizeof(tEnetOfcHwMeterInfo));

	if(pOfcMeterEntry != NULL)
	{
		tOfcMeterEntry.u2Flags = pOfcMeterEntry->u2Flags;
	}

	ret = EnetHal_FsOfcHwUpdateMeterEntry (&tOfcMeterEntry,(enetOfcCmd)OfcCmd);
#endif
    return ret;
}

/***********************************************************************************************
 *   Function Name      : FsOfcHwGetGroupStats
*    Description        : This function is used to Get  the  Group Entry Statistics
*    Input(s)           : pOfcGroupEntry->Pointer to the Group Entry fields
*                         pOfcGroupStats->pinter to the Group statistics Object
*    Output(s)          : None.
*    Returns            : FNP_SUCCESS/FNP_FAILURE
 **********************************************************************************************/
INT4
FsOfcHwGetGroupStats (tOfcHwGroupInfo * pOfcGroupEntry,
                      tOfcGroupStats * pOfcGroupStats)
{
    UNUSED_PARAM (pOfcGroupEntry);
    UNUSED_PARAM (pOfcGroupStats);
    INT4 ret = FNP_SUCCESS;
#ifdef USER_HW_API
    tEnetOfcHwGroupInfo tOfcGroupEntry;
    tEnetOfcGroupStats tOfcGroupStats;

    memset(&tOfcGroupEntry,0,sizeof(tEnetOfcHwGroupInfo));
    memset(&tOfcGroupStats,0,sizeof(tEnetOfcGroupStats));

	if(pOfcGroupEntry != NULL)
	{
		tOfcGroupEntry.ofcGroupDurationSec = pOfcGroupEntry->u4FsofcGroupDurationSec;
	}

	ret = EnetHal_FsOfcHwGetGroupStats (&tOfcGroupEntry,&tOfcGroupStats);
#endif
    return ret;
}

/***********************************************************************************************
 *   Function Name      : FsOfcHwGetMeterStats
*    Description        : This function is used to Get  the  Meter Entry Statistics
*    Input(s)           : pOfcMeterEntry->Pointer to the Meter Entry fields
*                         pOfcMeterStats->pinter to the Meter statistics Object
*    Output(s)          : None.
*    Returns            : FNP_SUCCESS/FNP_FAILURE
 **********************************************************************************************/
INT4
FsOfcHwGetMeterStats (tOfcHwMeterInfo * pOfcMeterEntry,
                      tOfcHwMeterStats * pOfcMeterStats)
{
    UNUSED_PARAM (pOfcMeterEntry);
    UNUSED_PARAM (pOfcMeterStats);
    INT4 ret = FNP_SUCCESS;


#ifdef USER_HW_API

    tEnetOfcHwMeterInfo tOfcMeterEntry;
    tEnetOfcHwMeterStats tOfcMeterStats;

	memset(&tOfcMeterEntry,0,sizeof(tEnetOfcHwMeterInfo));
	memset(&tOfcMeterStats,0,sizeof(tEnetOfcHwMeterStats));

	if(pOfcMeterEntry != NULL)
	{
		tOfcMeterEntry.u2Flags = pOfcMeterEntry->u2Flags;
	}

	ret = EnetHal_FsOfcHwGetMeterStats (&tOfcMeterEntry,&tOfcMeterStats);
#endif
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsOfcHwAddVlanEntry                              */
/*                                                                           */
/*    Description         : This function adds an entry to the hardware      */
/*                          Vlan table.                                      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId     - VlanId                              */
/*                          pHwEgressPorts- pointer to array of egress ports */
/*                          pHwUnTagPorts - pointer to array of Untag Ports. */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsOfcHwAddVlanEntry (UINT4 u4ContextId, UINT4 u4VlanId,
                     UINT1 *pu1EgressPorts, UINT1 *pu1UntagPorts)
{

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4VlanId);
    UNUSED_PARAM (pu1EgressPorts);
    UNUSED_PARAM (pu1UntagPorts);
    INT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
	ret = EnetHal_FsOfcHwAddVlanEntry (u4ContextId,u4VlanId,pu1EgressPorts,pu1UntagPorts);
#endif
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsOfcHwDelVlanEntry                              */
/*                                                                           */
/*    Description         : This function deletes an entry from the hardware */
/*                          Vlan table.                                      */
/*                          In case of Hybrid learning switches, this fn.    */
/*                          should disassociate the VLAN to FID mappings and */
/*                          and shud delete the FDB table if the number of   */
/*                          VLANS mapped to the FDB is 0.                    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId     - VlanId                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsOfcHwDelVlanEntry (UINT4 u4ContextId, UINT4 u4VlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4VlanId);
    INT4 ret = FNP_SUCCESS;
#ifdef USER_HW_API
	ret = EnetHal_FsOfcHwDelVlanEntry (u4ContextId,u4VlanId);
#endif
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsOfcHwSetVlanMemberPorts                        */
/*                                                                           */
/*    Description         : This function set the port as the member of      */
/*                          given Vlan                                       */
/*                                                                           */
/*    Input(s)            :u4ContextId - Context Id                          */
/*                         VlanId     - Vlan to which the port is going to   */
/*                                        be an member                       */
/*                          u4IfIndex  - The Interface Index                 */
/*                          u1IsTagged - Flag to indicate whether this port  */
/*                                       will be tagged or untagged          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsOfcHwSetVlanMemberPorts (UINT4 u4ContextId, UINT4 u4VlanId,
                           UINT1 *pu1EgressPorts, UINT1 *pu1UntagPorts)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4VlanId);
    UNUSED_PARAM (pu1EgressPorts);
    UNUSED_PARAM (pu1UntagPorts);
    INT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
	ret = EnetHal_FsOfcHwSetVlanMemberPorts (u4ContextId,u4VlanId,pu1EgressPorts,pu1UntagPorts);
#endif
    return ret;
}

/*****************************************************************************/
/*    Function Name       : FsOfcHwResetVlanMemberPorts                      */
/*                                                                           */
/*    Description         : This function Reset the port as the member of    */
/*                          given Vlan.                                      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId     - Vlan to which the port is going to  */
/*                                        be an member                       */
/*                          u4IfIndex  - The Interface Index                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsOfcHwResetVlanMemberPorts (UINT4 u4ContextId, UINT4 u4VlanId,
                             UINT1 *pu1EgressPorts, UINT1 *pu1UntagPorts)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4VlanId);
    UNUSED_PARAM (pu1EgressPorts);
    UNUSED_PARAM (pu1UntagPorts);
    INT4 ret = FNP_SUCCESS;
#ifdef USER_HW_API
	ret = EnetHal_FsOfcHwResetVlanMemberPorts (u4ContextId,u4VlanId,pu1EgressPorts,pu1UntagPorts);
#endif
    return ret;
}

/***********************************************************************************************
*    Function Name      : FsOfcHwInitFilter
*    Description        : This function is used to update the Default Flow
*    Input(s)           : u4Index->portNo to perform the operation
*                         PFlow - Flow to be updated
*    Output(s)          : None.
*    Returns            : None.
***********************************************************************************************/

VOID
FsOfcHwInitFilter (UINT4 u4Index, tOfcHwFlowInfo * pFlow)
{
    UNUSED_PARAM (u4Index);
    UNUSED_PARAM (pFlow);

#ifdef USER_HW_API
    tEnetOfcHwFlowInfo tFlow;




	memset(&tFlow,0,sizeof(tEnetOfcHwFlowInfo));

	if(pFlow != NULL)
	{
		//tFlow.u1TableId = pFlow->u1TableId;
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u4InPort %d\r\n",pFlow->ExactKey.u4InPort);

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"au1DstMacAddr %d-%d-%d-%d-%d-%d\r\n",
													pFlow->ExactKey.au1DstMacAddr[0],
													pFlow->ExactKey.au1DstMacAddr[1],
													pFlow->ExactKey.au1DstMacAddr[2],
													pFlow->ExactKey.au1DstMacAddr[3],
													pFlow->ExactKey.au1DstMacAddr[4],
													pFlow->ExactKey.au1DstMacAddr[5]);

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"au1SrcMacAddr %d-%d-%d-%d-%d-%d\r\n",
													pFlow->ExactKey.au1SrcMacAddr[0],
													pFlow->ExactKey.au1SrcMacAddr[1],
													pFlow->ExactKey.au1SrcMacAddr[2],
													pFlow->ExactKey.au1SrcMacAddr[3],
													pFlow->ExactKey.au1SrcMacAddr[4],
													pFlow->ExactKey.au1SrcMacAddr[5]);

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u2EthType %d\r\n",pFlow->ExactKey.u2EthType);

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u4Ip4Src %d.%d.%d.%d\r\n",
												(pFlow->ExactKey.u4Ip4Src & 0xFF000000) >> 24,
												(pFlow->ExactKey.u4Ip4Src & 0x00FF0000) >> 16,
												(pFlow->ExactKey.u4Ip4Src & 0x0000FF00) >> 8,
												(pFlow->ExactKey.u4Ip4Src & 0x000000FF) );


		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u4Ip4Dst %d.%d.%d.%d\r\n",
												(pFlow->ExactKey.u4Ip4Dst & 0xFF000000) >> 24,
												(pFlow->ExactKey.u4Ip4Dst & 0x00FF0000) >> 16,
												(pFlow->ExactKey.u4Ip4Dst & 0x0000FF00) >> 8,
												(pFlow->ExactKey.u4Ip4Dst & 0x000000FF) );


		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u1IpDscp %d\r\n",pFlow->ExactKey.u1IpDscp);

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u1IpTos %d\r\n",pFlow->ExactKey.u1IpTos);

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u1IpProto %d\r\n",pFlow->ExactKey.u1IpProto);

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u2TpDst %d\r\n",pFlow->ExactKey.u2TpDst);

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u2TpSrc %d\r\n",pFlow->ExactKey.u2TpSrc);




		EnetHal_FsOfcHwInitFilter(u4Index,&tFlow);
	}
#endif
}

/***********************************************************************************************
*    Function Name      : FsOfcHwOpenflowPortUpdate                                     
*    Description        : This function is used to update the port status              
*    Input(s)           : u4Ifindex->portNo to perform the operation                  
*                         OfcCmd ->Action to be perfomed             
*    Output(s)          : None.                                    
*    Returns            : FNP_SUCCESS/FNP_FAILURE                                 
***********************************************************************************************/

INT4
FsOfcHwOpenflowPortUpdate (UINT4 u4IfIndex, eOfcCmd OfcCmd)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (OfcCmd);
    INT4 ret = FNP_SUCCESS;
#ifdef USER_HW_API
	ret = EnetHal_FsOfcHwOpenflowPortUpdate (u4IfIndex,(enetOfcCmd)OfcCmd);
#endif
    return ret;
}

#ifdef OPENFLOW_TCAM
/********************************************************************************************
*    Function Name       : FsOfcHwUpdateWCFlowEntry - Define only TCAM based chipset       
*    Description         : This function is used to update the wildcard Flows                            
*    Input(s)            : u4TableId->Table ID in which operations is to be performe       
*                          pOfcFlowMatch->Pointer to the flowmatch fields                       
*                          OfcCmd ->Action to be performed
*                          pActlist->pointer to the actions set                               
*    Output(s)           : None.                                                               
*    Returns             : FNP_SUCCESS/FNP_FAILURE                                       
*********************************************************************************************/
INT4
FsOfcHwUpdateWCFlowEntry (tOfcHwFlowInfo * pFlow, eOfcCmd OfcCmd)
{
    INT4                i4RetVal = OFC_NP_ZERO;
    i4RetVal = FsOfcHwUpdateExactFlowEntry (pFlow, OfcCmd);
    return FNP_SUCCESS;
}

/***********************************************************************************************
*    Function Name      : FsOfcHwGetWCFlowStats - Define only for TCAM based chipset      
*    Description        : This function is used to Get  the FlowStatistics                       
*    Input(s)           : u4TableId->Particular tableid in which operations is to be performe       
*                         pOfcFlowMatch->Pointer to the flowmatch fields                      
*                         pOfcFlowStats->pinter to the flow statistics                            
*    Output(s)          : None.                                                               
*    Returns            : FNP_SUCCESS/FNP_FAILURE                                            
***********************************************************************************************/
INT4
FsOfcHwGetWCFlowStats (UINT4 u4TableId, tOfcHwWcFlowMatch * pOfcFlowMatch,
                       tOfcFlowStats * pOfcFlowStats)
{
    UNUSED_PARAM (u4TableId);
    UNUSED_PARAM (pOfcFlowMatch);
    UNUSED_PARAM (pOfcFlowStats);
    return FNP_SUCCESS;
}
#endif

/*****************************************************************************************
*    Function Name      :FsOfcHwUpdateExactFlowEntry - Define only for NON TCAM                      
*                        based chipset                                                 
*    Description        :This function is used to update the Exact fields                    
*    Input(s)           :pFlow -> Flow Entry                     
*                        OfcCmd -> Action to be performed
*    Output(s)          :None.                                                               
*    Returns            :FNP_SUCCESS/FNP_FAILURE                                             
*****************************************************************************************/
INT4
FsOfcHwUpdateExactFlowEntry (tOfcHwFlowInfo * pFlow, eOfcCmd OfcCmd)
{
    UNUSED_PARAM (pFlow);
    UNUSED_PARAM (OfcCmd);

#ifdef USER_HW_API
#endif
    return FNP_SUCCESS;
}

/*****************************************************************************************
*    Function Name      :FsOfcHwGetExactFlowStats - Define only for NON TCAM                                    
*              based chipset                                                
*    Description        :This function is used to Get the flow  fields                            
*    Input(s)           :pOfcFlowStats ->pointer to the flow statistics structure                     
*                        pOfcFlowMatch->Pointer to the flowmatch fields                       
*    Output(s)          :None.                                                               
*    Returns            :FNP_SUCCESS/FNP_FAILURE                                             
*****************************************************************************************/
INT4
FsOfcHwGetExactFlowStats (tOfcHwExactFlowMatch * pOfcFlowMatch,
                          tOfcFlowStats * pOfcFlowStats)
{
    UNUSED_PARAM (pOfcFlowMatch);
    UNUSED_PARAM (pOfcFlowStats);

#ifdef USER_HW_API
#endif
    return FNP_SUCCESS;
}

#endif
/***********************************************************************************************
                    END OF FILE ofcnp.c 
***********************************************************************************************/
