/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : isspinpx.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Port Isolation                                 */
/*    MODULE NAME           : ISS                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE : 23 July 2010                                   */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains definitions for  ISS Port   */
/*                            isolation MBSM related NP-API.                 */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    23 July 2010           Initial Creation                        */
/*---------------------------------------------------------------------------*/

#ifndef ISSPINPX_C
#define ISSPINPX_C

#include "lr.h"
#include "isspinp.h"
#include "npapi.h"

/********************************************************************/
/*                                                                  */
/* Function Name : IssPIMbsmHwConfigPortIsolationEntry              */
/*                                                                  */
/* Description   : This function configures the Port Isolation      */
/*                 entries in the Port Isolation table when the line*/
/*                 card is inserted or deleted.                     */
/*                                                                  */
/* Input(s)      : pHwUpdPortIsolation - pointer to the Port        */
/*                 Isolation entry with ingress, egress, add/delete */
/*                 information, number of egress ports populated.   */
/*                 pIssMbsmInfo  - pointer to the Mbsm info struct  */
/*                 with the card insert/delete information and slot */
/*                 information populated.                           */
/*                                                                  */
/* Output(s)     : None.                                            */
/*                                                                  */
/* Returns       : FNP_SUCCESS / FNP_FAILURE.                       */
/********************************************************************/
INT4
IssPIMbsmHwConfigPortIsolationEntry (tIssHwUpdtPortIsolation *
                                     pHwUpdPortIsolation,
                                     tIssMbsmInfo * pIssMbsmInfo)
{
    UNUSED_PARAM (pIssMbsmInfo);
    UNUSED_PARAM (pHwUpdPortIsolation);
    return FNP_FAILURE;
}
#endif /*ISSPINPX_C */
