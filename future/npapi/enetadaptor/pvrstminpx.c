/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pvrstminpx.c,v 1.1 2007/10/24 09:55:37 iss Exp $ 
 *
 * Description: All network processor function  given here
 *
 *******************************************************************/
#ifdef  PVRST_WANTED

#ifndef _PVRSTMINPX_C_
#define _PVRSTMINPX_C_

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "rstp.h"
#include "pvrst.h"
#include "npapi.h"
#include "nppvrstmi.h"
#include "npcfa.h"

/* Program port state for Vlan */

/*******************************************************************************
 * FsMiPvrstMbsmNpSetVlanPortState
 *
 * DESCRIPTION:
 * Sets the PVRST Port State in the Hardware for given Vlan. When PVRST is
 * operating in PVRST mode or PVRST in RSTP compatible mode or PVRST in STP
 * compatible mode.
 *
 * INPUTS:
 * u4IfIndex   - Interface index of whose STP state is to be updated
 * VlanId- Vlan ID.     
 * u1PortState - Value of Port State.
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * FNP_SUCCESS - success
 * FNP_FAILURE - Error during setting
 *
 * COMMENTS:
 *  None
 *
 *******************************************************************************/
INT1
FsMiPvrstMbsmNpSetVlanPortState (UINT4 u4ContextId, UINT4 u4IfIndex,
                                 tVlanId VlanId, UINT1 u1PortState,
                                 tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1PortState);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************
 * Function Name                  :FsMiPvrstMbsmNpInitHw                     *
 *                                                                           *
 * DESCRIPTION                    :This function performs any necessary      * 
 *                                 PVRST related initialisation in the       *
 *                                 Hardware                                  *
 *                                                                           *
 * INPUTS                         : pSlotInfo - Slot Information             *
 *                                : u4ContextId - Context ID                 *  
 *                                                                           * 
 * OUTPUTS                        : None                                     *
 *                                                                           *
 * RETURNS                        : FNP_SUCCESS - On Success                 *
 *                                  FNP_FAILURE - On Failure                 *
 *                                                                           *
 *****************************************************************************/

INT1
FsMiPvrstMbsmNpInitHw (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

#endif
#endif
