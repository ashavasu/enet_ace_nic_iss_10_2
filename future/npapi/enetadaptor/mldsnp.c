/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldsnp.c,v 1.2 2007/02/01 14:59:31 iss Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "snp.h"
#include "npapi.h"
#include "npmlds.h"

/*****************************************************************************/
/* Function Name      : FsMldsHwEnableMldSnooping                            */
/*                                                                           */
/* Description        : This function enables MLD snooping feature in the    */
/*                      hardware. This creates and installs filter needed    */
/*                      for snooping feature                                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMldsHwEnableMldSnooping (VOID)
{

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMldsHwDisableMldSnooping                           */
/*                                                                           */
/* Description        : This function disables MLD snooping feature in the  */
/*                      hardware. This removes & destroys filter needed      */
/*                      for snooping feature                                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMldsHwDisableMldSnooping (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsNpUpdateIp6mcFwdEntries                             */
/*                                                                           */
/* Description        : This function Updates IP Multicast forwarding entries*/
/*                      in the hardwarei based on the EventType.             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsNpUpdateIp6mcFwdEntries (tSnoopVlanId VlanId, UINT1 *pGrpAddr,
                           UINT1 *pSrcAddr, tPortList PortList,
                           tPortList UntagPortList, UINT1 u1EventType)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pGrpAddr);
    UNUSED_PARAM (pSrcAddr);
    UNUSED_PARAM (PortList);
    UNUSED_PARAM (UntagPortList);
    UNUSED_PARAM (u1EventType);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsNpGetIp6FwdEntryHitBitStatus                          */
/*                                                                           */
/* Description        : This function gets how many times this forwarding    */
/*                      entry is used for sending data traffic               */
/*                                                                           */
/* Input(s)           : VlanId - vlan id                                     */
/*                      pGrpAddr - Group Address                            */
/*                      pSrcAddr - Source Address                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsNpGetIp6FwdEntryHitBitStatus (tSnoopVlanId VlanId, UINT1 *pGrpAddr,
                                UINT1 *pSrcAddr)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pGrpAddr);
    UNUSED_PARAM (pSrcAddr);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMldsHwEnableIpMldSnooping                          */
/*                                                                           */
/* Description        : This function is called when IP  MLD is enabled      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMldsHwEnableIpMldSnooping (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMldsHwDisableIpMldSnooping                         */
/*                                                                           */
/* Description        : This function is called when IP MLD is disabled.     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMldsHwDisableIpMldSnooping (VOID)
{
    return FNP_SUCCESS;
}
