/****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved                     */
/* $Id: cnnp.c,v 1.1 2010/07/05 06:38:32 prabuc Exp $                                                                   */
/*                                                                          */
/*  FILE NAME             : cnnp.c                                          */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : CN                                              */
/*  MODULE NAME           : CN-NPAPI_STUBS                                  */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This file contains the NPAPI stub functions     */
/*                          implementations for Aricent CN Module.          */
/****************************************************************************/

#ifndef __CN_NP_C__
#define __CN_NP_C__
#include "npcn.h"

/*****************************************************************************/
/* Function Name      :  FsMiCnHwSetDefenseMode                              */
/*                                                                           */
/* Description        : This function set defense mode  in the               */
/*                      Hardware device                                      */
/*                                                                           */
/* Input(s)           : u4CompId - component ID                              */
/*                      u4PortId - Port ID                                   */
/*                      u1CNPV   - CNPV                                      */
/*                      u1DefenseMode - Defense to set                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiCnHwSetDefenseMode (UINT4 u4CompId, UINT4 u4PortId, UINT1 u1CNPV,
                        UINT1 u1DefenseMode)
{
    UNUSED_PARAM (u4CompId);
    UNUSED_PARAM (u4PortId);
    UNUSED_PARAM (u1CNPV);
    UNUSED_PARAM (u1DefenseMode);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      :   FsMiCnHwSetCpParams                                */
/*                                                                           */
/* Description        : This function set CP parameters  in the              */
/*                      Hardware device                                      */
/*                                                                           */
/* Input(s)           : u4CompId - component ID                              */
/*                      u4PortId - Port ID                                   */
/*                      u4TrafficClass - Queue to which this CP needs to be  */
/*                                       configured.                         */
/*                      *pCpParams - Pointer CP params table                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiCnHwSetCpParams (UINT4 u4CompId, UINT4 u4PortId,
                     UINT4 u4TrafficClass, tCnHwCpParams * pCpParams)
{
    UNUSED_PARAM (u4CompId);
    UNUSED_PARAM (u4PortId);
    UNUSED_PARAM (u4TrafficClass);
    UNUSED_PARAM (pCpParams);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      :  FsMiCnHwSetCnmPriority                              */
/*                                                                           */
/* Description        : This function set defense mode  in the               */
/*                      Hardware device                                      */
/*                                                                           */
/* Input(s)           : u4CompId - component ID                              */
/*                      u4CnmPri - CNM Priority                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiCnHwSetCnmPriority (UINT4 u4CompId, UINT1 u1CnmPri)
{
    UNUSED_PARAM (u4CompId);
    UNUSED_PARAM (u1CnmPri);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      :  FsMiCnHwReSetCounters                               */
/*                                                                           */
/* Description        : This function Reset counter(for a component)         */
/*                      in the Hardware device                               */
/*                                                                           */
/* Input(s)           : u4CompId - Component Id                              */
/*                      u4PortId - Port ID                                   */
/*                      u1CNPV - CNPV                                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiCnHwReSetCounters (UINT4 u4CompId, UINT4 u4PortId, UINT1 u1CNPV)
{
    UNUSED_PARAM (u4CompId);
    UNUSED_PARAM (u4PortId);
    UNUSED_PARAM (u1CNPV);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      :   FsMiCnHwGetCounters                                */
/*                                                                           */
/* Description        : This function get counters  in the                   */
/*                      Hardware device                                      */
/*                                                                           */
/* Input(s)           : u4CompId - component ID                              */
/*                      u4PortId - Port ID                                   */
/*                      u1CNPV   - CNPV                                      */
/*                                                                           */
/* Output(s)          : *pDisFrames - Pointer to discarded frames            */
/*                      *pTransFrames - Pointer to Tansmitted frames         */
/*                      *PTransCnms - Pointer to  Tansmitted CNMs            */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiCnHwGetCounters (UINT4 u4CompId, UINT4 u4PortId, UINT1 u1CNPV,
                     FS_UINT8 * pDisFrames,
                     FS_UINT8 * pTransFrames, FS_UINT8 * pTransCnms)
{
    UNUSED_PARAM (u4CompId);
    UNUSED_PARAM (u4PortId);
    UNUSED_PARAM (u1CNPV);
    FSAP_U8_CLR (pDisFrames);
    FSAP_U8_CLR (pTransFrames);
    FSAP_U8_CLR (pTransCnms);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : FsMiCnHwCallBkFunc                                   */
/*                                                                           */
/* Description        : This function is a call back function which will get */
/*                      called when CNM generated in hardware to send trap   */
/*                      It should call the CnApiNpapiNotifyCnm function to   */
/*                      post the event in CN queue                           */
/*                                                                           */
/* Input(s)           : pu1CpId - pointer to CPID                            */
/*                      i4CnmQOffset - CNM offset                            */
/*                      i4CnmQDelta   - CNM delta                            */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiCnHwCallBkFunc (UINT1 *pu1CpId, INT4 i4CnmQOffset, INT4 i4CnmQDelta)
{
    UNUSED_PARAM (pu1CpId);
    UNUSED_PARAM (i4CnmQOffset);
    UNUSED_PARAM (i4CnmQDelta);

    return (FNP_SUCCESS);
}
#endif
/*-----------------------------------------------------------------------*/
/*                       End of the file  cnnp.c                         */
/*-----------------------------------------------------------------------*/
