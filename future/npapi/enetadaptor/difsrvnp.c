/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: difsrvnp.c,v 1.8 2007/10/25 10:16:31 iss Exp $
 *
 * Description: This file contains the function implementations  of DiffServ
 *              System related NP-API.
 ****************************************************************************/

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "npapi.h"
#ifdef SWC
#include "dscxe.h"
#else
#include "diffsrv.h"
#endif
#include "npdifsrv.h"

#ifndef SWC
/*****************************************************************************/
/* Function Name      : DsHwInit                                             */
/*                                                                           */
/* Description        : This function intialises the DiffServ in the device  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
DsHwInit (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsHwClassifierAdd                                    */
/*                                                                           */
/* Description        : This function intialises the DiffServ in the device  */
/*                                                                           */
/* Input(s)           : tDiffServClfrEntry *pDsClfrEntry                     */
/*                      tDiffServMultiFieldClfrEntry *pDsMultiFieldClfrEntry */
/*                                                                           */
/*                      tDiffServInProfileActionEntry *                      */
/*                      pDsInProfileActionEntry                              */
/*                                                                           */
/*                      tDiffServOutProfileActionEntry *                     */
/*                      pDsOutProfileActionEntry                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
DsHwClassifierAdd (tDiffServClfrEntry * pDsClfrEntry,
                   tDiffServMultiFieldClfrEntry * pDsMultiFieldClfrEntry,
                   tDiffServInProfileActionEntry * pDsInProfileActionEntry,
                   tDiffServOutProfileActionEntry * pDsOutProfileActionEntry,
                   tDiffServMeterEntry * pDsMeterEntry)
{

    UNUSED_PARAM (pDsClfrEntry);
    UNUSED_PARAM (pDsMultiFieldClfrEntry);
    UNUSED_PARAM (pDsInProfileActionEntry);
    UNUSED_PARAM (pDsOutProfileActionEntry);
    UNUSED_PARAM (pDsMeterEntry);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsHwClassifierDelete                                 */
/*                                                                           */
/* Description        : This function intialises the DiffServ in the device  */
/*                                                                           */
/* Input(s)           : tDiffServClfrEntry *pDsClfrEntry                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
DsHwClassifierDelete (tDiffServClfrEntry * pDsClfrEntry,
                      tDiffServMultiFieldClfrEntry * pDsMultiFieldClfrEntry,
                      tDiffServMeterEntry * pDsMeterEntry)
{

    UNUSED_PARAM (pDsClfrEntry);
    UNUSED_PARAM (pDsMultiFieldClfrEntry);
    UNUSED_PARAM (pDsMeterEntry);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsHwClassifierUpdate                                 */
/*                                                                           */
/* Description        : This function intialises the DiffServ in the device  */
/*                                                                           */
/* Input(s)           : tDiffServClfrEntry *pDsClfrEntry                     */
/*                                                                           */
/*                      tDiffServInProfileActionEntry *                      */
/*                      pDsInProfileActionEntry                              */
/*                                                                           */
/*                      tDiffServOutProfileActionEntry *                     */
/*                      pDsOutProfileActionEntry                             */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
DsHwClassifierUpdate (tDiffServClfrEntry * pDsClfrEntry,
                      tDiffServMultiFieldClfrEntry * pDsMultiFieldClfrEntry,
                      tDiffServInProfileActionEntry * pDsInProfileActionEntry,
                      tDiffServOutProfileActionEntry * pDsOutProfileActionEntry,
                      tDiffServMeterEntry * pDsMeterEntry)
{

    UNUSED_PARAM (pDsClfrEntry);
    UNUSED_PARAM (pDsMultiFieldClfrEntry);
    UNUSED_PARAM (pDsInProfileActionEntry);
    UNUSED_PARAM (pDsOutProfileActionEntry);
    UNUSED_PARAM (pDsMeterEntry);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsHwSchedulerAdd                                     */
/*                                                                           */
/* Description        : This function intialises the DiffServ in the device  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
DsHwSchedulerAdd (tDiffServSchedulerEntry * pDsSchedulerEntry)
{

    UNUSED_PARAM (pDsSchedulerEntry);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsGetCounters                                        */
/*                                                                           */
/* Description        : This function gets the counters in the device        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
DsHwGetCounters (INT4 i4DataPid, INT4 i4ClfrId, tDiffServCounters * DsCounters)
{

    UNUSED_PARAM (i4DataPid);
    UNUSED_PARAM (i4ClfrId);
    UNUSED_PARAM (DsCounters);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsHwChangeCosqScheduleAlgo                                 */
/*                                                                           */
/* Description  : This function will change the cosq scheduling algorithm    */
/*                                                                           */
/* Input        : u4PortId      - Port Number                                */
/*                u4CosqAlgo    - New Algorithm to set                       */
/*                pu4Weight     - Weight of cosqs to be set                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : FNP_SUCCESS /FNP_FAILURE                                   */
/*                                                                           */
/*****************************************************************************/
INT4
DsHwChangeCosqScheduleAlgo (UINT4 u4PortId, UINT4 u4CosqAlgo, UINT4 *pu4Weight,
                            UINT4 *pu4MinBw, UINT4 *pu4MaxBw, UINT4 *pu4BwFlag)
{
    UNUSED_PARAM (u4PortId);
    UNUSED_PARAM (u4CosqAlgo);
    UNUSED_PARAM (pu4Weight);
    UNUSED_PARAM (pu4MinBw);
    UNUSED_PARAM (pu4MaxBw);
    UNUSED_PARAM (pu4BwFlag);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsHwGetCosqBandwidth                                       */
/*                                                                           */
/* Description  : This function is called to get the cosq bandwidths         */
/*                                                                           */
/* Input        : u4PortId      - Port Number                                */
/*                pu4MinBw      - Minimum Bandwidth                          */
/*                pu4MaxBw      - Maximum Bandwidth                          */
/*                pu4BwFlag     - Bandwidth Flag                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : FNP_SUCCESS /FNP_FAILURE                                   */
/*                                                                           */
/*****************************************************************************/
INT4
DsHwGetCosqBandwidth (UINT4 u4PortId, UINT4 *pu4MinBw, UINT4 *pu4MaxBw,
                      UINT4 *pu4BwFlag)
{
    UNUSED_PARAM (u4PortId);
    UNUSED_PARAM (pu4MinBw);
    UNUSED_PARAM (pu4MaxBw);
    UNUSED_PARAM (pu4BwFlag);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsGetEasyriderStaus                                        */
/*                                                                           */
/* Description  : This function is called to get the target chipset if it is */
/*                Easyrider                                                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : FNP_SUCCESS /FNP_FAILURE                                   */
/*                                                                           */
/*****************************************************************************/

INT4
DsGetEasyriderStaus ()
{
    return FNP_SUCCESS;
}

#else

/*****************************************************************************/
/* Function Name      : DsHwResetClassifierAction                            */
/*                                                                           */
/* Description        : This function intialises the DiffServ in the device  */
/*                                                                           */
/* Input(s)           : tDiffServClfrEntry *pDsClfrEntry                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
DsHwResetClassifierAction (tDiffServClfrEntry * pDsClfrEntry,
                           tDiffServMultiFieldClfrEntry *
                           pDsMultiFieldClfrEntry)
{
    UNUSED_PARAM (pDsClfrEntry);
    UNUSED_PARAM (pDsMultiFieldClfrEntry);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsHwSetClassifierAction                              */
/*                                                                           */
/* Description        : This function intialises the DiffServ in the device  */
/*                                                                           */
/* Input(s)           : pDsClfrEntry, pDsMultiFieldClfrEntry,                */
/*                      pDsInProfileActionEntry,                             */
/*                      ClsfrTCFlag - Whether map, unmap or don't care about */
/*                                    the traffic class in the Classifier    */
/*                                    entry.                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
DsHwSetClassifierAction (tDiffServClfrEntry * pDsClfrEntry,
                         tDiffServMultiFieldClfrEntry * pDsMultiFieldClfrEntry,
                         tDiffServInProfileActionEntry
                         * pDsInProfileActionEntry, tDsNpClfrTCFlag ClsfrTcFlag)
{
    UNUSED_PARAM (pDsClfrEntry);
    UNUSED_PARAM (pDsMultiFieldClfrEntry);
    UNUSED_PARAM (pDsInProfileActionEntry);
    UNUSED_PARAM (ClsfrTcFlag);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsHwSetIPTOSQPrecedence                              */
/*                                                                           */
/* Description        : This function sets the precedence of the IP TOS for  */
/*                      selecting the QPriotrity of any switched or routed   */
/*                      packet.                                              */
/*                                                                           */
/* Input(s)           : IPTOSQPrec - The precedence value                    */
/*                      PrecType   - Indicates whether the precedence should */
/*                                   be applied for switched or routed       */
/*                                   packets.                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
DsHwSetIPTOSQPrecedence (UINT4 u4IPTOSQPrec, tDsNpPrecType u4PrecType)
{
    UNUSED_PARAM (u4IPTOSQPrec);
    UNUSED_PARAM (u4PrecType);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsHwSetDot1PQPrecedence                              */
/*                                                                           */
/* Description        : This function sets the precedence of the Dot1P       */
/*                      priority for selecting the QPriotrity of any         */
/*                      switched or routed packet.                           */
/*                                                                           */
/* Input(s)           : Dot1PQPrec - The precedence value                    */
/*                      PrecType   - Indicates whether the precedence should */
/*                                   be applied for switched or routed       */
/*                                   packets.                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
DsHwSetDot1PQPrecedence (UINT4 u4Dot1PQPrec, tDsNpPrecType u4PrecType)
{
    UNUSED_PARAM (u4Dot1PQPrec);
    UNUSED_PARAM (u4PrecType);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsHwSetDSCPQPrioMap                                  */
/*                                                                           */
/* Description        : This function maps a DSCP value to a Queue priority  */
/*                      value.                                               */
/*                                                                           */
/* Input(s)           : u4DSCPValue                                          */
/*                      u4Qpriority                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
DsHwSetDSCPQPrioMap (UINT4 u4DSCPValue, UINT4 u4QPriority)
{
    UNUSED_PARAM (u4DSCPValue);
    UNUSED_PARAM (u4QPriority);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsHwSetPortParams                                    */
/*                                                                           */
/* Description        : This function configures the port based QOS          */
/*                      parameters in the hardware.                          */
/*                                                                           */
/* Input(s)           : u4PortNum    - Port index                            */
/*                      pDsPortEntry - Pointer to memory containing the      */
/*                                     values for the parameters             */
/*                    : u4ParamFlag  - Flag indicating the particular        */
/*                                     parameter that needs to be set.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
DsHwSetPortParams (UINT4 u4PortNum, tDiffServPortEntry * pDsPortEntry,
                   tDsNpPortParams u4ParamFlag)
{
    UNUSED_PARAM (u4PortNum);
    UNUSED_PARAM (pDsPortEntry);
    UNUSED_PARAM (u4ParamFlag);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsHwSetTCParams                                      */
/*                                                                           */
/* Description        : This function configures the Traffic Class based QOS */
/*                      parameters in the hardware.                          */
/*                                                                           */
/* Input(s)           : u4TrafficClassId - Traffic class index               */
/*                      pDsTcEntry -   Pointer to memory containing the      */
/*                                     values for the parameters             */
/*                    : u4ParamFlag  - Flag indicating the particular        */
/*                                     parameter that needs to be set.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
DsHwSetTCParams (UINT4 u4TrafficClassId, tDiffServTCEntry * pDsTcEntry,
                 tDsNpTCParams u4ParamFlag)
{
    UNUSED_PARAM (u4TrafficClassId);
    UNUSED_PARAM (pDsTcEntry);
    UNUSED_PARAM (u4ParamFlag);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsHwSetREDParams                                     */
/*                                                                           */
/* Description        : This function configures the RED Curve parameters    */
/*                      in the hardware.                                     */
/*                                                                           */
/* Input(s)           : u4REDCurveId - RED Curve number                      */
/*                      pDsREDEntry  - Pointer to memory containing the      */
/*                                     values for the parameters             */
/*                    : u4CreateFlag - Flag indicating whether this Curve    */
/*                                     should be enabled or disabled.        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
DsHwSetREDParams (UINT4 u4REDCurveId, tDiffServREDEntry * pDsREDEntry,
                  tDsNpREDParams u4EnableFlag)
{
    UNUSED_PARAM (u4REDCurveId);
    UNUSED_PARAM (pDsREDEntry);
    UNUSED_PARAM (u4EnableFlag);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsHwGetWFHBDCounter                                  */
/*                                                                           */
/* Description        : This function gets the specified counter from the    */
/*                      device                                               */
/*                                                                           */
/* Input(s)           : CounterType - Flag indicating the type of counter    */
/*                                    to be read                             */
/*                                                                           */
/* Output(s)          : u4Count                                              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
DsHwGetWFHBDCounter (tDsNpCounterType CounterType, UINT4 *pu4Count)
{
    UNUSED_PARAM (CounterType);
    UNUSED_PARAM (pu4Count);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsHwInitQoS                                          */
/*                                                                           */
/* Description        : This function is called when the switch starts up.   */
/*                      It sets the default values in the Priotity and       */
/*                      WFHBD registers.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
DsHwInitQoS (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsHwInitWfhbd                                        */
/*                                                                           */
/* Description        : This function sets the default values for the port   */
/*                      WFHBD parameters, the BackLoggLimit, and the         */
/*                      BackLogg counter                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
DsHwInitWfhbd (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsHwInitPriorityScheme                               */
/*                                                                           */
/* Description        : This function sets the default precedence when       */
/*                      the priorities from the classifier or the packet to  */
/*                      the queue priority. It also initialises the IPTOS to */
/*                      queue priority/ dot1p priority mapping memory.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
DsHwInitPriorityScheme (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsHwInitDefaultTrfClass                              */
/*                                                                           */
/* Description        : This function initialises the Traffic class          */
/*                      Remapping memory to set up mappings for the four     */
/*                      default traffic classes on all ports.                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
DsHwInitDefaultTrfClasses (VOID)
{
    return FNP_SUCCESS;
}

#endif /* SWC */
