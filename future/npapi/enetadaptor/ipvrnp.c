/* $Id: ipvrnp.c,v 1.3 2014/10/16 13:04:31 siva Exp $ */
#include "lr.h"
#include "cfa.h"
#include "ipnp.h"
#include "cfanp.h"
#include "rportnp.h"
/******************************************************************************/
/*   Function Name             : FsNpIpv4VrfArpAdd                               */
/*   Description               : Adds the Specified IP address and MAC address*/
/*                               to the H/W L3 IP Table Returns Full if no    */
/*                               free entry exist in the L3 Ip Table          */
/*   Input(s)                  : u4VrId - The virtual router identifier      */
/*                               u2VlanId  - L2 Vlan Id                       */
/*                               u4IfIndex - CFA L3 Vlan If Index             */
/*                               u4IpAddr  - IP address for which ARP is added*/
/*                               pMacAddr  -  MAC Address                     */
/*                               pu1IfName - Interface name                   */
/*                               i1State   - ARP_STATIC / ARP_DYNAMIC         */
/*   Output(s)                 : pbu1TblFull - Hw Table Full/not              */
/*                                            (FNP_FALSE/FNP_TRUE)            */
/*   Global Variables Referred : None                                         */
/*   Global variables Modified : None                                         */
/*   Exceptions                : None                                         */
/*   Use of Recursion          : None                                         */
/*   Returns                   : FNP_SUCCESS/FNP_FAILURE                      */
/******************************************************************************/
UINT4
FsNpIpv4VrfArpAdd (UINT4 u4VrId, tNpVlanId u2VlanId, UINT4 u4IfIndex,
                   UINT4 u4IpAddr, UINT1 *pMacAddr, UINT1 *pu1IfName,
                   INT1 i1State, UINT4 *pu4TblFull)
{
    UNUSED_PARAM (u4VrId);
    return (FsNpIpv4ArpAdd (u2VlanId, u4IfIndex,
                            u4IpAddr, pMacAddr, pu1IfName,
                            i1State, pu4TblFull));

}

/******************************************************************************/
/*   Function Name             : FsNpIpv4VrfArpModify                            */
/*   Description               : Modifies the ARP entry corresponding to the  */
/*                               given IP address                             */
/*   Input(s)                  : u4VrId - The virtual router identifier      */
/*                               u2VlanId  - L2 Vlan Id                       */
/*                               u4IfIndex - CFA L3 Vlan If Index             */
/*                               u4IpAddr  - IP address for which ARP is added*/
/*                               pMacAddr  -  MAC Address                     */
/*                               pu1IfName - Interface name                   */
/*                               i1State   - State of the ARP entry           */
/*   Output(s)                 : None                                         */
/*   Global Variables Referred : None                                         */
/*   Global variables Modified : None                                         */
/*   Exceptions                : None                                         */
/*   Use of Recursion          : None                                         */
/*   Returns                   : FNP_SUCCESS/FNP_FAILURE                      */
/******************************************************************************/
UINT4
FsNpIpv4VrfArpModify (UINT4 u4VrId, tNpVlanId u2VlanId, UINT4 u4IfIndex,
                      UINT4 u4PhyIfIndex, UINT4 u4IpAddr, UINT1 *pMacAddr,
                      UINT1 *pu1IfName, INT1 i1State)
{
    UNUSED_PARAM (u4VrId);
    return (FsNpIpv4ArpModify
            (u2VlanId, u4IfIndex, u4PhyIfIndex, u4IpAddr, pMacAddr, pu1IfName,
             i1State));
}

/*****************************************************************************/
/*  Function Name             :  FsNpIpv4VrfArpDel                              */
/*  Description               :  Deletes ARP entry from the Fast Path Table  */
/*  Input(s)                  :  u4VrId - The virtual router identifier      */
/*                               u4IpAddr   -- IpAddress                     */
/*                               pu1IfName  -- Pointer to interface name.    */
/*                               i1State    -- ARP_STATIC / ARP_DYNAMIC      */
/*  Output(s)                 :  None                                        */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions                :  None                                        */
/*  Use of Recursion          :  None                                        */
/*  Returns                   :  FNP_FAILURE(0) on failure                   */
/*                               FNP_SUCCESS(1) on success                   */
/*****************************************************************************/
UINT4
FsNpIpv4VrfArpDel (UINT4 u4VrId, UINT4 u4IpAddr, UINT1 *pu1IfName, INT1 i1State)
{
    UNUSED_PARAM (u4VrId);
    return (FsNpIpv4ArpDel (u4IpAddr, pu1IfName, i1State));
}

/******************************************************************************/
/*   Function Name             : FsNpIpv4VrfCheckHitOnArpEntry                   */
/*   Description               : Checks the HIT bit information of the ARP    */
/*                               ARP Entry                                    */
/*   Input(s)                  : u4VrId - The virtual router identifier      */
/*                               u4IpAddress - IP address                     */
/*                               u1NextHopFlag - Indicates the IP address is  */
/*                                               nexthop address or not       */
/*   Output(s)                 : Hit Status of L3 Entry                       */
/*   Global Variables Referred : None                                         */
/*   Global variables Modified : None                                         */
/*   Exceptions                : None                                         */
/*   Use of Recursion          : None                                         */
/*   Returns                   : FNP_TRUE/FNP_FALSE                           */
/******************************************************************************/
UINT1
FsNpIpv4VrfCheckHitOnArpEntry (UINT4 u4VrId, UINT4 u4IpAddress,
                               UINT1 u1NextHopFlag)
{
    UNUSED_PARAM (u4VrId);
    return (FsNpIpv4CheckHitOnArpEntry (u4IpAddress, u1NextHopFlag));
}

/*****************************************************************************/
/*   Function Name             : FsNpIpv4VrfClearArpTable                       */
/*   Description               : Deletes all entries from H/W L3 IP Table    */
/*   Input(s)                  : u4VrId - The virtual router identifier      */
/*   Output(s)                 : None.                                       */
/*   Global Variables Referred : None                                        */
/*   Global variables Modified : None                                        */
/*   Exceptions                : None                                        */
/*   Use of Recursion          : None                                        */
/*   Returns                   : FNP_SUCCESS - If supported                  */
/*                               FNP_NOT_SUPPORTED - If driver API is not    */
/*                                                   supported.              */
/*****************************************************************************/

INT4
FsNpIpv4VrfClearArpTable (UINT4 u4VrId)
{
    UNUSED_PARAM (u4VrId);
    return (FsNpIpv4ClearArpTable ());
}

/******************************************************************************/
/*   Function Name             :  FsNpIpv4VrfGetSrcMovedIpAddr                   */
/*   Description               :  Get the IP address for which source is      */
/*                                moved                                       */
/*   Input(s)                  :  None                                        */
/*   Output(s)                 : Virtual Router Id and Source Moved IP address*/
/*   Global Variables Referred : None                                         */
/*   Global variables Modified : None                                         */
/*   Exceptions                : None                                         */
/*   Use of Recursion          : None                                         */
/*   Returns                   : FNP_SUCCESS/FNP_FAILURE                      */
/******************************************************************************/
INT4
FsNpIpv4VrfGetSrcMovedIpAddr (UINT4 *pu4VrId, UINT4 *pu4IpAddress)
{
    *pu4VrId = 0;
    return (FsNpIpv4GetSrcMovedIpAddr (pu4IpAddress));
}

/*****************************************************************************
 *    Function Name             : FsNpCfaVrfSetDlfStatus
 *    Description               : This function registers/de-registers to get
 *                                the L3 the DLF packets to CPU
 *    Input(s)                  : Virutal routerid and dlf status - True/False
 *    Output(s)                 : None.
 *    Global Variables Referred : None
 *    Global Variables Modified : None
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *    Use of Recursion          : None.
 *    Returns                   : None.
 *
 *****************************************************************************/
PUBLIC INT4
FsNpCfaVrfSetDlfStatus (UINT4 u4VrfId, UINT1 u1Status)
{
    UNUSED_PARAM (u4VrfId);
    return (FsNpCfaSetDlfStatus (u1Status));
}

/******************************************************************************/
/*   Function Name             : FsNpL3Ipv4VrfArpAdd                             */
/*   Description               : Adds the Specified IP address and MAC address*/
/*                               to the H/W L3 IP Table for CFA_ENET type     */
/*                               interface Returns Full if no                 */
/*                               free entry exist in the L3 Ip Table          */
/*   Input(s)                  : u4IfIndex - CFA L3 If Index                  */
/*                                            (port or trunk - but not vlan)  */
/*                               u4IpAddr  - IP address for which ARP is added*/
/*                               pMacAddr  -  MAC Address                     */
/*                               pu1IfName - Interface name                   */
/*                               i1State   - State of the ARP entry           */
/*   Output(s)                 : pbu1TblFull - Hw Table Full/not              */
/*                                            (FNP_FALSE/FNP_TRUE)            */
/*   Global Variables Referred : None                                         */
/*   Global variables Modified : None                                         */
/*   Exceptions                : None                                         */
/*   Use of Recursion          : None                                         */
/*   Returns                   : FNP_SUCCESS/FNP_FAILURE                      */
/******************************************************************************/
UINT4
FsNpL3Ipv4VrfArpAdd (UINT4 u4VrfId, UINT4 u4IfIndex, UINT4 u4IpAddr,
                     UINT1 *pMacAddr, UINT1 *pu1IfName,
                     INT1 i1State, UINT1 *pbu1TblFull)
{
    UNUSED_PARAM (u4VrfId);
    return (FsNpL3Ipv4ArpAdd (u4IfIndex, u4IpAddr,
                              pMacAddr, pu1IfName, i1State, pbu1TblFull));
}

/******************************************************************************/
/*   Function Name             : FsNpL3Ipv4VrfArpModify                          */
/*   Description               : Modify the ARP entry over the router port    */
/*   Input(s)                  : u4IfIndex - CFA L3 Vlan If Index             */
/*                               u4IpAddr  - IP address for which ARP is added*/
/*                               pMacAddr  -  MAC Address                     */
/*                               pu1IfName - Interface name                   */
/*                               i1State   - State of the ARP entry           */
/*   Output(s)                 : None                                         */
/*   Global Variables Referred : None                                         */
/*   Global variables Modified : None                                         */
/*   Exceptions                : None                                         */
/*   Use of Recursion          : None                                         */
/*   Returns                   : FNP_SUCCESS/FNP_FAILURE                      */
/******************************************************************************/

UINT4
FsNpL3Ipv4VrfArpModify (UINT4 u4VrfId, UINT4 u4IfIndex, UINT4 u4IpAddr,
                        UINT1 *pMacAddr, UINT1 *pu1IfName, INT1 i1State)
{
    UNUSED_PARAM (u4VrfId);
    return (FsNpL3Ipv4ArpModify (u4IfIndex, u4IpAddr,
                                 pMacAddr, pu1IfName, i1State));
}

/*****************************************************************************/
/*  Function Name             :  FsNpIpv4VrfArpGet                           */
/*  Description               :  Gets ARP entry from the Fast Path Table     */
/*  Input(s)                  :  u4VrId - The virtual router identifier      */
/*                               u4IpAddr   -- IpAddress                     */
/*  Output(s)                 :  pHwAddr - MAC address                       */
/*                               pu4CfaIfIndex -- Pointer to IfIndex         */
/*                               pi1State    -- ARP_STATIC / ARP_DYNAMIC     */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions                :  None                                        */
/*  Use of Recursion          :  None                                        */
/*  Returns                   :  FNP_FAILURE(0) on failure                   */
/*                               FNP_SUCCESS(1) on success                   */
/*****************************************************************************/
INT4
FsNpIpv4VrfArpGet (tNpArpInput ArpNpInParam, tNpArpOutput * pArpNpOutParam)
{
    UNUSED_PARAM (ArpNpInParam);
    UNUSED_PARAM (pArpNpOutParam);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name             :  FsNpIpv4VrfArpGetNext                       */
/*  Description               :  Gets  the next ARP entry from the FPT       */
/*  Input(s)                  :  u4VrId - The virtual router identifier      */
/*                               u4IpAddr   -- IpAddress                     */
/*  Output(s)                 :  pHwAddr - MAC address                       */
/*                               pu4CfaIfIndex -- Pointer to IfIndex         */
/*                               pi1State    -- ARP_STATIC / ARP_DYNAMIC     */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions                :  None                                        */
/*  Use of Recursion          :  None                                        */
/*  Returns                   :  FNP_FAILURE(0) on failure                   */
/*                               FNP_SUCCESS(1) on success                   */
/*****************************************************************************/
INT4
FsNpIpv4VrfArpGetNext (tNpArpInput ArpNpInParam, tNpArpOutput * pArpNpOutParam)
{
    UNUSED_PARAM (ArpNpInParam);
    UNUSED_PARAM (pArpNpOutParam);
    return FNP_SUCCESS;
}
