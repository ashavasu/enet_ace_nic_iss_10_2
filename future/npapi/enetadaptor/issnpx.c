/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: issnpx.c,v 1.2 2007/02/01 14:59:31 iss Exp $
 *
 * Description: This file contains the function implementations  of ISS
 *              System related NP-API.
 ****************************************************************************/
#ifndef _ISSNPX_C_
#define _ISSNPX_C_

#include "lr.h"
#include "iss.h"
#include "npapi.h"
#include "npiss.h"

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssMbsmHwSetPortEgressStatus                     */
/*                                                                          */
/*    Description        : This function sets the egress status of the      */
/*                         given port. In case if the IfIndex is 0, then    */
/*                         it will be applied for all ports.                */
/*                                                                          */
/*    Input(s)           : u4IfIndex - Port interface index.                */
/*                         u1Status - Egress enabled / disabled.            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssMbsmHwSetPortEgressStatus (UINT4 u4IfIndex, UINT1 u1Status,
                              tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssMbsmHwSetPortStatsCollection                  */
/*                                                                          */
/*    Description        : This function enables/disables the port stats    */
/*                         collection of the given port. In case if the     */
/*                         IfIndex is 0, then it will be applied for all    */
/*                         ports.                                           */
/*                                                                          */
/*    Input(s)           : u4IfIndex - Port interface index.                */
/*                         u1Status - Status collection enabled / disabled. */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssMbsmHwSetPortStatsCollection (UINT4 u4IfIndex, UINT1 u1Status,
                                 tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssMbsmHwSetPortMirroringStatus                  */
/*                                                                          */
/*    Description        : This function enables/disables the port          */
/*                         mirroring in hardware.                           */
/*                                                                          */
/*    Input(s)           : i4MirrorStatus - Port mirroring status.          */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssMbsmHwSetPortMirroringStatus (INT4 i4MirrorStatus, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (i4MirrorStatus);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssMbsmHwSetMirrorToPort                         */
/*                                                                          */
/*    Description        : This function sets the port on which the         */
/*                         mirrored packets has to be sent in hardware.     */
/*                                                                          */
/*    Input(s)           : u4MirrorPort - Mirror port.                      */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssMbsmHwSetMirrorToPort (UINT4 u4MirrorPort, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4MirrorPort);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssMbsmHwUpdateL2Filter                          */
/*                                                                          */
/*    Description        : This function is used to update the hardware L2  */
/*                         filter.                                          */
/*                                                                          */
/*    Input(s)           : pIssL2FilterEntry - L2Filter Entry to be updated.*/
/*                         i4Value - Flag for the operation                 */
/*                                  (ISS_L2FILTER_ADD                       */
/*                                   or ISS_L2FILTER_DELETE).               */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
INT4
IssMbsmHwUpdateL2Filter (tIssL2FilterEntry * pIssL2FilterEntry, INT4 i4Value,
                         tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pIssL2FilterEntry);
    UNUSED_PARAM (i4Value);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssMbsmHwUpdateL3Filter                          */
/*                                                                          */
/*    Description        : This function is used to update the hardware L3  */
/*                         filter.                                          */
/*                                                                          */
/*    Input(s)           : pIssL3FilterEntry - L3Filter Entry to be updated.*/
/*                         i4Value - Flag for the operation                 */
/*                                  (ISS_L3FILTER_ADD                       */
/*                                   or ISS_L3FILTER_DELETE).               */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssMbsmHwUpdateL3Filter (tIssL3FilterEntry * pIssL3FilterEntry, INT4 i4Value,
                         tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pIssL3FilterEntry);
    UNUSED_PARAM (i4Value);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}
#endif
