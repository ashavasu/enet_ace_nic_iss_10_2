/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: npmimlds.h,v 1.2 2007/02/01 14:59:31 iss Exp $
 *
 * Description: Prototypes for IGMP Snooping module's NP-API.
 *
 *******************************************************************/

#ifndef _NPMIMLDS_H
#define _NMIPMLDS_H
#include "mldsminp.h"

#endif
