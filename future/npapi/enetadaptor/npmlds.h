/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: npmlds.h,v 1.2 2007/02/01 14:59:31 iss Exp $
 *
 * Description: Prototypes for IGMP Snooping module's NP-API.
 *
 *******************************************************************/

#ifndef _NPMLDS_H
#define _NPMLDS_H
#include "mldsnp.h"

#endif
