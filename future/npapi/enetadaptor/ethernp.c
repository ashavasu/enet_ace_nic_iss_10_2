/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ethernp.c,v 1.7 2008/12/31 06:01:43 premap-iss Exp $
 *
 * Description: This file contains the function implementations  of ether 
 *              MIB NP-API.
 ****************************************************************************/

#include "lr.h"
#include "cfa.h"
#include "npapi.h"
#include "npether.h"

/****************************************************************************
 Function    : FsEtherHwGetStats
 Input       : u4IfIndex : the index of the table (= IfIndex).
               pEthStats        : the pointer to a structure to store stats
 Output      : The hardware routine takes the index &
               stores the statistics requested in the structure pointed to by
               pEthStats.
 Returns     : FNP_SUCCESS or FNP_FAILURE
****************************************************************************/

INT4
FsEtherHwGetStats (UINT4 u4IfIndex, tEthStats * pEthStats)
{
    INT4                i4RetVal = FNP_SUCCESS;

    UNUSED_PARAM (u4IfIndex);

    pEthStats->u4AlignErrors = 0;
    pEthStats->u4FCSErrors = 0;
    pEthStats->u4SingleColFrames = 0;
    pEthStats->u4MultipleColFrames = 0;
    pEthStats->u4SQETestErrors = 0;
    pEthStats->u4DeferredTrans = 0;
    pEthStats->u4LateCollisions = 0;
    pEthStats->u4ExcessiveCols = 0;
    pEthStats->u4IntMacTxErrors = 0;
    pEthStats->u4CarrierSenseErrors = 0;
    pEthStats->u4FrameTooLongs = 0;
    pEthStats->u4IntMacRxErrors = 0;
    pEthStats->u4SymbolErrors = 0;
    /* NOTE: Get the Port Duplexity from Hardware using driver APIs. */
    pEthStats->u4DuplexStatus = ETH_STATS_DUP_ST_UNKNOWN;
    pEthStats->u2RateControlStatus = 0;
    pEthStats->bRateControlAbility = FALSE;

    MEMSET (&(pEthStats->EthHCStats), 0, sizeof (pEthStats->EthHCStats));

    return (i4RetVal);
}

/****************************************************************************
 Function    : FsEtherHwGetCollFreq
 Input       : u4IfIndex       : the index of the table (= IfIndex).
               i4dot3CollCount : the number of collisions suffered
               pElement        : the number of MAC frames
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCCESS or FNP_FAILURE
****************************************************************************/

INT4
FsEtherHwGetCollFreq (UINT4 u4IfIndex, INT4 i4dot3CollCount, UINT4 *pElement)
{
    INT4                i4RetVal = FNP_SUCCESS;

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4dot3CollCount);
    UNUSED_PARAM (pElement);

    return (i4RetVal);
}

/****************************************************************************
 Function    : FsEtherHwGetCntrl
 Input       : u4IfIndex : the index of the table (= IfIndex).
               pElement         : the control information needed
               i4Code           : type of information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCCESS or FNP_FAILURE
****************************************************************************/

INT4
FsEtherHwGetCntrl (UINT4 u4IfIndex, INT4 *pElement, INT4 i4Code)
{
    INT4                i4RetVal = FNP_SUCCESS;

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pElement);

    switch (i4Code)
    {
        case ETH_CNTRL_FNS_SUPPORTED:
            break;
        case ETH_CNTRL_IN_UNK_OPCODES:
            break;
        case ETH_CNTRL_HC_IN_UNK_OPCODES:
            break;
        default:
            break;
    }

    return (i4RetVal);
}

/****************************************************************************
 Function    : FsEtherHwSetPauseAdminMode
 Input       : u4IfIndex : the index of the table (= IfIndex).
               pElement         : the value to be set
 Output      : The hardware routine takes the indicies & sets the value of the
               element in hardware.
 Returns     : FNP_SUCCESS or FNP_FAILURE
****************************************************************************/

INT4
FsEtherHwSetPauseAdminMode (UINT4 u4IfIndex, INT4 *pElement)
{
    INT4                i4RetVal = FNP_SUCCESS;

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pElement);

    return (i4RetVal);
}

/****************************************************************************
 Function    : FsEtherHwGetPauseMode
 Input       : u4IfIndex : the index of the table (= IfIndex).
               pElement         : pointer to the information needed
               i4Code           : type of information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCCESS or FNP_FAILURE
****************************************************************************/

INT4
FsEtherHwGetPauseMode (UINT4 u4IfIndex, INT4 *pElement, INT4 i4Code)
{
    INT4                i4RetVal = FNP_SUCCESS;

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pElement);

    switch (i4Code)
    {
        case ETH_PAUSE_ADMIN_MODE:
            break;
        case ETH_PAUSE_OPER_MODE:
            break;
        default:
            break;
    }
    return (i4RetVal);
}

/****************************************************************************
 Function    : FsEtherHwGetPauseFrames
 Input       : u4IfIndex : the index of the table (= IfIndex).
               pElement         : pointer to the information needed
               i4Code           : type of information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCCESS or FNP_FAILURE
****************************************************************************/

INT4
FsEtherHwGetPauseFrames (UINT4 u4IfIndex, UINT4 *pElement, INT4 i4Code)
{
    INT4                i4RetVal = FNP_SUCCESS;

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pElement);

    switch (i4Code)
    {
        case ETH_PAUSE_IN_FRAMES:
            break;
        case ETH_PAUSE_OUT_FRAMES:
            break;
        case ETH_HC_PAUSE_IN_FRAMES:
            break;
        case ETH_HC_PAUSE_OUT_FRAMES:
            break;
        default:
            break;
    }

    return (i4RetVal);
}
