/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdnp.c,v 1.4 2011/01/21 11:26:28 siva Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/

#include "lr.h"
#include "npapi.h"
#include "mplsapi.h"
#include "lspp.h"
#include "tcp.h"
#include "bfd.h"
#include "bfdnp.h"

/****************************************************************************
 *
 * Function Name      : FsMiBfdHwStartBfd
 *                                                                           
 * Description        : The routine handles the porting of BFD NPAPI to Offload
 *                      a session to NP or Modify a already offloaded session
 *                      in the NP or to delete a session from NP.
 *                                                                           
 * Input(s)           : u4ContextId - Context Identifier                   
 *                      
 *                      pBfdSessHandle - Pointer to structure tBfdSessionHandle
 *                          This structure contains the following:
 *                             u4BfdSessId - Contains the session identifier 
 *                                   that uniquely identifies the BFD session.
 *                             u4BfdSessLocalDiscr -Local discriminator value
 *                                   assigned for this BFD session.
 *                             LabelList - Structure variable of type 
 *                                   tMplsHdrParams.
 *                      
 *                      pBfdSessPathInfo - Pointer to the tBfdSessionPathInfo 
 *                          which provides the path/interface details to be
 *                          used to transmit or receive the BFD control packets
 *                      
 *                      pBfdParams - Pointer to the structure tBfdParams. 
 *                          This structure contains all the parameters to be 
 *                          used by the hardware to construct and transmit BFD 
 *                          control packets at negotiated interval and use the 
 *                          calculated detection time interval to detect faults
 *                          .This structure additionally contains values 
 *                          expected to be present in the received BFD control 
 *                          packet which can be used to perform validation
 *
 *                      pBfdModifiedParams - Pointer to the structure 
 *                          tBfdModifiedParams. This structure is used to 
 *                          indicate modified parameters.
 *
 *                      pBfdTxCtrlPkt - Pointer to the buffer containing 
 *                          the entire BFD packet (including the encapsulation 
 *                          and labels) to be transmitted on the interface by 
 *                          the BFD Offload module.
 *                          
 *                      pBfdRxCtrlPkt - Pointer to the buffer containing the 
 *                          entire BFD packet (including the encapsulation 
 *                          and labels) expected to be received on the 
 *                          interface by the BFD Offload module.
 *
 *                      eBfdSessHwStartBfdCallFlag - variable of enum 
 *                          tBfdSessHwStartBfdCallFlag indicating the reason 
 *                          for the call to the BFD Offload module. 
 *
 * Output(s)          : pBfdSessHwHandle - pointer to the handle returned 
 *                          by the hardware for the BFD session  
 *                                                                           
 * Return Value(s)    : FNP_SUCCESS - On Success                             
 *                      FNP_FAILURE - On failure                             
 *****************************************************************************/

INT4                FsMiBfdHwStartBfd
    (UINT4 u4ContextId,
     tBfdSessionHandle * pBfdSessHandle,
     tBfdSessionPathInfo * pBfdSessPathInfo,
     tBfdParams * pBfdParams,
     tBfdOffPktBuf * pBfdTxCtrlPkt,
     tBfdOffPktBuf * pBfdRxCtrlPkt,
     tBfdSessHwStartBfdCallFlag eBfdSessHwStartBfdCallFlag,
     tBfdHwhandle * pBfdSessHwHandle)
{

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pBfdSessHandle);
    UNUSED_PARAM (pBfdSessPathInfo);
    UNUSED_PARAM (pBfdParams);
    UNUSED_PARAM (pBfdTxCtrlPkt);
    UNUSED_PARAM (pBfdRxCtrlPkt);
    UNUSED_PARAM (eBfdSessHwStartBfdCallFlag);
    UNUSED_PARAM (pBfdSessHwHandle);
    return FNP_SUCCESS;
}

/****************************************************************************
 *
 * Function Name      : FsMiBfdHwStopBfd
 *                                                                           
 * Description        : The routine handles the porting of BFD NPAPI to delete i
 *                      the offloaded session from the NP. This is done when 
 *                      the session is administratively down or some other 
 *                      internal down event occurs.
 *                                                                           
 * Input(s)           : u4ContextId - Context Identifier                   
 *                      
 *                      pBfdSessHandle - Pointer to structure tBfdSessionHandle
 *                          This structure contains the following:
 *                             u4BfdSessId - Contains the session identifier 
 *                                   that uniquely identifies the BFD session.
 *                             u4BfdSessLocalDiscr -Local discriminator value
 *                                   assigned for this BFD session.
 *                             LabelList - Structure variable of type 
 *                                   tMplsHdrParams.
 *                      pBfdSessHwHandle - pointer to the handle that was 
 *                          returned by the hardware for the BFD session.
 *
 *                      BfdSessHwStopBfdCallFlag . variable of enum 
 *                          tBfdSessHwStopBfdCallFlag indicating the reason 
 *                          for the call to the BFD Offload module. Since this 
 *                          API call is made during deletion or disable of a 
 *                          BFD session in control plane, this flag is used to
 *                          specify the reason for the call so as to provide 
 *                          flexibility to the BFD Offload module. 
 *
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : FNP_SUCCESS - On Success                             
 *                      FNP_FAILURE - On failure                             
 *****************************************************************************/

INT4                FsMiBfdHwStopBfd
    (UINT4 u4ContextId,
     tBfdSessionHandle * pBfdSessHandle,
     tBfdHwhandle * pBfdSessHwHandle,
     tBfdSessHwStopBfdCallFlag eBfdSessHwStopBfdCallFlag)
{

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pBfdSessHandle);
    UNUSED_PARAM (pBfdSessHwHandle);
    UNUSED_PARAM (eBfdSessHwStopBfdCallFlag);
    return FNP_SUCCESS;
}

/****************************************************************************
 *
 * Function Name      : FsMiBfdHwRegisterSessionCb
 *                                                                           
 * Description        : This routine is called by BFD to register a function 
 *                      with the BFD Offload module. This registered function 
 *                      is used by the BFD Offload module to call back upon 
 *                      the occurrence of specific events.
 *                                                                           
 * Input(s)           : u4ContextId - Context Identifier                   
 *                      
 *                      BfdEventType - Enum of type tBfdEventType. It can 
 *                          take following values:
 *          
 *                      BfdOffCallBack - Pointer to the callback 
 *                          function to be registered. This function is 
 *                          used by the BFD Offload module to notify the
 *                          BFD module in the control plane when any of
 *                          the above events occur
 *
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : FNP_SUCCESS - On Success                             
 *                      FNP_FAILURE - On failure                             
 *****************************************************************************/

INT4                FsMiBfdHwRegisterSessionCb
    (UINT4 u4ContextId,
     tBfdEventType eBfdEventType, FsMiBfdHwSessionCb BfdOffCallBack)
{

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (eBfdEventType);
    UNUSED_PARAM (BfdOffCallBack);
    return FNP_SUCCESS;
}

/****************************************************************************
 *
 * Function Name      : FsMiBfdGetHwBfdParameters
 *                                                                           
 * Description        : This routine is called by BFD to get the BFD session 
 *                      parameters from the BFD Offload module. Used by the 
 *                      Control card to query the Session parameters available
 *                      in the line card and to update the internal data 
 *                      maintained at the control card. This can help the 
 *                      control card to asynchronously fetch the data 
 *                      whenever needed eg when hardware audit is implemented.
 *                                                                           
 * Input(s)           : u4ContextId - Context Identifier                   
 *                      
 *                      pBfdSessHandle - Pointer to structure tBfdSessionHandle
 *                          This structure contains the following:
 *                             u4BfdSessId - Contains the session identifier 
 *                                   that uniquely identifies the BFD session.
 *                             u4BfdSessLocalDiscr -Local discriminator value
 *                                   assigned for this BFD session.
 *                             LabelList - Structure variable of type 
 *                                   tMplsHdrParams.
 *
 *                      pBfdSessHwHandle - pointer to the handle that was 
 *                          returned by the hardware for the BFD session.
 *
 * Output(s)          : pBfdParams - Pointer to the structure tBfdParams. 
 *                          This structure contains all the parameters to be 
 *                          used by the hardware to construct and transmit BFD 
 *                          control packets at negotiated interval and use the 
 *                          calculated detection time interval to detect faults
 *                          .This structure additionally contains values 
 *                          expected to be present in the received BFD control 
 *                          packet which can be used to perform validation
 *                                                                           
 * Return Value(s)    : FNP_SUCCESS - On Success                             
 *                      FNP_FAILURE - On failure                             
 *****************************************************************************/

INT4                FsMiBfdGetHwBfdParameters
    (UINT4 u4ContextId,
     tBfdSessionHandle * pBfdSessHandle,
     tBfdHwhandle * pBfdSessHwHandle, tBfdParams * pBfdParams)
{

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pBfdSessHandle);
    UNUSED_PARAM (pBfdSessHwHandle);
    UNUSED_PARAM (pBfdParams);
    return FNP_SUCCESS;
}

/****************************************************************************
 *
 * Function Name      : FsMiBfdGetHwBfdParameters
 *                                                                           
 * Description        : This routine is called by BFD to get the BFD session 
 *                      statistics from the BFD Offload module. Used by the 
 *                      Control card to query the Session statistics available
 *                      in the line card and to update the internal data 
 *                      maintained at the control card.
 *                                                                           
 * Input(s)           : u4ContextId - Context Identifier                   
 *                      
 *                      pBfdSessHandle - Pointer to structure tBfdSessionHandle
 *                          This structure contains the following:
 *                             u4BfdSessId - Contains the session identifier 
 *                                   that uniquely identifies the BFD session.
 *                             u4BfdSessLocalDiscr -Local discriminator value
 *                                   assigned for this BFD session.
 *                             LabelList - Structure variable of type 
 *                                   tMplsHdrParams.
 *
 *                      pBfdSessHwHandle - pointer to the handle that was 
 *                          returned by the hardware for the BFD session.
 *
 * Output(s)          : pSessionStats - Pointer to the Session statistics 
 *                          being returned by the BFD Offload module.  
 *                                                                           
 * Return Value(s)    : FNP_SUCCESS - On Success                             
 *                      FNP_FAILURE - On failure                             
 *****************************************************************************/
INT4                FsMiBfdGetHwBfdSessionStats
    (UINT4 u4ContextId,
     tBfdSessionHandle * pBfdSessHandle,
     tBfdHwhandle * pBfdSessHwHandle, tBfdSessStats * pSessionStats)
{

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pBfdSessHandle);
    UNUSED_PARAM (pBfdSessHwHandle);
    UNUSED_PARAM (pSessionStats);
    return FNP_SUCCESS;
}

/****************************************************************************
 *
 * Function Name      : FsMiBfdHwClearBfdStats
 *                                                                           
 * Description        : This API is called by BFD module to clear the 
 *                      statistics for the BFD session from the BFD Offload
 *                      module. 
 *                                                                           
 * Input(s)           : u4ContextId - Context Identifier                   
 *                      
 *                      pBfdSessHandle - Pointer to structure tBfdSessionHandle
 *                          This structure contains the following:
 *                             u4BfdSessId - Contains the session identifier 
 *                                   that uniquely identifies the BFD session.
 *                             u4BfdSessLocalDiscr -Local discriminator value
 *                                   assigned for this BFD session.
 *                             LabelList - Structure variable of type 
 *                                   tMplsHdrParams.
 *
 *                      pBfdSessHwHandle - pointer to the handle that was 
 *                          returned by the hardware for the BFD session.
 *
 *                      bClearAllSessStats . This flag indicates if all the 
 *                          statistics of all the BFD sessions in the 
 *                          particular context have to be cleared. When this 
 *                          is true, pBfdSessHandle and pBfdSessHwHandle will
 *                          be NULL. When false only the particular session 
 *                          statistics needs to be cleared.     
 *
 * Output(s)          : None 
 *                                                                           
 * Return Value(s)    : FNP_SUCCESS - On Success                             
 *                      FNP_FAILURE - On failure                             
 *****************************************************************************/

INT4                FsMiBfdHwClearBfdStats
    (UINT4 u4ContextId,
     tBfdSessionHandle * pBfdSessHandle,
     tBfdHwhandle * pBfdSessHwHandle, BOOL1 bClearAllSessStats)
{

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pBfdSessHandle);
    UNUSED_PARAM (pBfdSessHwHandle);
    UNUSED_PARAM (bClearAllSessStats);
    return FNP_SUCCESS;
}
