/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikenp.c,v 1.1 2011/02/16 10:02:52 siva Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *******************************************************************/

#ifndef _IKENP_C_
#define _IKENP_C_

#include "lr.h"
#include "cfa.h"
#include "npapi.h"
#include "ikenp.h"

/******************************************************************/
/*  Function Name             : FsNpIkeInit                       */
/*  Description               : This is the function to be        */
/*                              called for initialising           */
/*                              IKE filters                       */
/*  Input(s)                  : None.                             */
/*  Output(s)                 : None                              */
/*  Global Variables Referred : None                              */
/*  Global variables Modified : None                              */
/*  Exceptions                : None                              */
/*  Use of Recursion          : None                              */
/*  Returns                   : VOID                              */
/******************************************************************/

VOID
FsNpIkeInit ()
{
    return;
}

#endif
/* end of file */
