/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmnp.c,v 1.3 2011/08/18 12:24:16 siva Exp $
 *
 * Description: This file contains the function implementations  of NP
 *              redundancy.
 ****************************************************************************/

#ifndef _RMNP_C_
#define _RMNP_C_

#include "lr.h"
#include "npapi.h"
#include "rmnp.h"
/*****************************************************************************
 * Function Name        : RmNpUpdateNodeState
 *
 * Description          : This function will be invoked by the RM task to
 *                        pass events like GO_ACTIVE, GO_STANDBY etc. to NP
 *                        module.
 *
 * Input(s)             : u1Event - Node Status given by RM module
 *
 * Output(s)            : None.
 *
 * Returns              : FNP_SUCCESS / FNP_FAILURE
 *****************************************************************************/
INT4
RmNpUpdateNodeState (UINT1 u1Event)
{
    UNUSED_PARAM (u1Event);
    return FNP_SUCCESS;
}

/* HITLESS RESTART */
/*****************************************************************************/
/*    Function Name       : FsNpHRSteadyStatePkt                             */
/*                                                                           */
/*    Description         : This function does the any of the following.     */
/*                          1) stores the steady state pkt in NPSIM.         */
/*                          2) indicates NPSIM to start sending steady state */
/*                             pkts when ISS node goes down.                 */
/*                          3) indicates NPSIM to stop sending steady state  */
/*                             pkts when ISS node comes up again.            */
/*                                                                           */
/*    Input(s)            : eAction      - NP_RM_HR_STDY_ST_TRIG_START or    */
/*                                         NP_RM_HR_STDY_ST_TRIG_STOP or     */
/*                                         NP_RM_HR_STORE_STDY_ST_PKT.       */
/*                          pRmHRPktInfo - steady state pkt info structure.  */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : FNP_SUCCESS/FNP_FAILURE                          */
/*****************************************************************************/
INT4
FsNpHRSetStdyStInfo (tRmHRAction eAction, tRmHRPktInfo * pRmHRPktInfo)
{
    UNUSED_PARAM (eAction);
    UNUSED_PARAM (pRmHRPktInfo);
    return FNP_SUCCESS;
}
#endif /* _RMNP_C_ */
