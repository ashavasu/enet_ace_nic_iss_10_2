/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpsnpx.c
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/

#include "lr.h"
#include "npapi.h"
#include "ecfm.h"
#include "elps.h"
#include "elpsnp.h"

/*****************************************************************************/
/* Function Name      : FsMiElpsMbsmHwPgSwitchDataPath                       */
/*                                                                           */
/* Description        : The routine handles the porting of ELPS NPAPI to     */
/*                      switch the data path and revert the data path in     */
/*                      respective scenarios. It calls the Svc API to create */
/*                      and delete the Hardware entries to do same           */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      u4PgId      - Protection Group Identifier            */
/*                      pPgHwInfo   - Structure containing all the inputs    */
/*                                    such as  Working Port, Working         */
/*                                    Service, Protection Port, Protection   */
/*                                    Service for a Protection Group         */
/*                      o PgProtType - Structure containing the Protection   */
/*                                     architecture for the protection group */
/*                         o u1ArchType                                      */
/*                             The Protection Group architecture - 1:1 or 1+1*/
/*                         o u1CommChannel                                   */
/*                             The Protection Group Channel - APS Channel or */
/*                             No APS Channel                                */
/*                         o u1Direction                                     */
/*                             The Protection Group Switching Type -         */
/*                             Unidirectional or Bi-directional switching    */
/*                      o u4PgIngressPortId:                                 */
/*                          The Protection point on which the protection     */
/*                          group is originating. This object will carry the */
/*                          value of an IfIndex. This object can have a      */
/*                          value of 0 incase no ingress port is configured  */
/*                          configured for the protection group.             */
/*                      o u4PgWorkingPortId:                                 */
/*                          Working port IfIndex.                            */
/*                      o u4PgProtectionPortId:                              */
/*                          Protection port IfIndex.                         */
/*                      o u4PgWorkingServiceValue:                           */
/*                          Working service id which is carrying the normal  */
/*                          traffic in the absence of any failure or fault.  */
/*                          This value can be a VLAN id.                     */
/*                      o u4PgProtectionServiceValue:                        */
/*                          Protection service id which is carrying the      */
/*                          normal traffic in the absence of working         */
/*                          transport service. This value can be a VLAN id.  */
/*                      o u1PgIngressBrgPortType:                            */
/*                          Bridge Port type of Ingress Port. Based on this  */
/*                          bridge port type the required parameters can be  */
/*                          changed in the hardware tables to perform the    */
/*                          below mentioned actions on the data path.        */
/*                      o u1PgAction                                         */
/*                          ELPS_PG_CREATE / ELPS_PG_DELETE /                */
/*                          ELPS_PG_SWITCH_TO_WORKING /                      */
/*                          ELPS_PG_SWITCH_TO_PROTECTION                     */
/*                          ELPS_PG_CREATE - In case of Port disjoint        */
/*                            scenario, indicates that the Port isolation    */
/*                            entry needs to be created upon creation of     */
/*                            Protection group in the control plane.         */
/*                          ELPS_PG_DELETE - Indicates that all the entries  */
/*                            created in the hardware for this protection    */
/*                            group are to be deleted from the hardware.     */
/*                            This NPAPI will be invoked with this option    */
/*                            upon deletion of the protection group through  */
/*                            configuration in the control plane.            */
/*                          ELPS_PG_SWITCH_TO_PROTECTION - Indicates that    */
/*                            the data path needs to be switched to the      */
/*                            protection path upon a switch from working to  */
/*                            protection entity. The failed working path is  */
/*                            to be blocked from accepting data traffic.     */
/*                            This is applicable to the port disjoint,       */
/*                            service disjoint as well as the port-and-      */
/*                            service disjoint scenarios.                    */
/*                          ELPS_PG_SWITCH_TO_WORKING - Indicates that the   */
/*                            data path needs to be reverted to the working  */
/*                            path in hardware upon revertion from the       */
/*                            Protection to Working entity.                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiElpsMbsmHwPgSwitchDataPath (UINT4 u4ContextId, UINT4 u4PgId,
                                tElpsHwPgSwitchInfo * pPgHwInfo,
                                tMbsmSlotInfo * pSlotInfo)
{
    /* This NPAPI can be ported to perform the following actions based on 
     * the operational deployment scenario (port-and-service disjoint, 
     * service disjoint, port disjoint) and based on the requirement:
     *
     * - Block the traffic received on the failed/faulty working port upon 
     *   a protection switch event.
     * - Install a "switching entry" at the ingress and the egress on the 
     *   ingress port such that the working service identifier is modified to 
     *   the protection service identifier before the forwarding and MAC 
     *   switching decisions are made.
     * - Install a "switching entry" at the egress and ingress on the working 
     *   and/or protection port, in the absence of a valid configured ingress 
     *   port, such that the working service identifier is modified to the 
     *   protection service identifier after the forwarding and MAC switching 
     *   decisions are made.
     * - Install a set of "isolated" or "protected" ports among the set of the 
     *   service member ports such that traffic is isolated only to the ingress
     *   and working ports under normal circumstances and isolated to the 
     *   ingress and protection ports upon failure of the working port.
     * - Modify and tweak any Quality of Service parameters such as priority or
     *   traffic class queues for the failed working service or the newly 
     *   active protection service or to handle any extra traffic that may have
     *   been previously carried on the protection port and/or service before 
     *   the protection switch event. */
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4PgId);
    UNUSED_PARAM (pPgHwInfo);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}
