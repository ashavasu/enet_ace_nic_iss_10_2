/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pnacnpx.c,v 1.3 2007/07/17 13:29:53 iss Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/
#ifndef _PNACNPX_C_
#define _PNACNPX_C_

#include "lr.h"
#include "cfa.h"
#include "pnac.h"
#include "npapi.h"
#include "nppnac.h"

/***************************************************************************/
/* Function Name      : PnacMbsmHwEnable                                   */
/*                                                                         */
/* Description        : This function is called when PNAC module is enabled*/
/*                      through Manager routines. It does the following:   */
/*                      1. Flushes all existing address entries in h/w 
 *                      table                                              */
/*                      2. Registers for PAE group address in h/w          */
/*                                                                         */
/* Input(s)           : pSlotInfo - Pointer to the Slot information 
 *                                  structure                              */
/*                                                                         */
/* Output(s)          : None.                                              */
/*                                                                         */
/* Global Variables                                                        */
/* Referred           : None.                                              */
/*                                                                         */
/* Global Variables                                                        */
/* Modified           : None.                                              */
/*                                                                         */
/* Return Value(s)    : FNP_SUCCESS or FNP_FAILURE.                        */
/***************************************************************************/
INT4
PnacMbsmHwEnable (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/***************************************************************************/
/* Function Name      : PnacMbsmHwCreatePnacFilters                        */
/*                                                                         */
/* Description        : This function is called from CFA to create filters */
/*                      (FFP & FP) for reception of EAPOL packets and      */
/*                      for Blocking incoming traffic when a port is       */
/*                      un-authorised for incoming direction.              */
/*                                                                         */
/* Input(s)           : pSlotInfo -pointer to the Slot information structur*/
/*                                                                         */
/* Output(s)          : None.                                              */
/*                                                                         */
/* Global Variables                                                        */
/* Referred           : None.                                              */
/*                                                                         */
/* Global Variables                                                        */
/* Modified           : None.                                              */
/*                                                                         */
/* Return Value(s)    : FNP_SUCCESS or FNP_FAILURE.                        */
/***************************************************************************/

INT4
PnacMbsmHwCreatePnacFilters (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/***************************************************************************/
/* Function Name      : PnacMbsmHwSetAuthStatus                            */
/*                                                                         */
/* Description        : This function is called when hardware module is    */
/*                      present and the port authorization status changes. */
/*                      Called to set the authorization status in the      */
/*                      Hardware for the Port or MAC concerned. This 
 *                      routine configures the hardware to control the 
 *                      traffic of                                         */
/*                      packets.                                           */
/*                                                                         */
/* Input(s)           : u2Port - Port Number                               */
/*                      pu1SuppAddr - Supplicant MAC address (in port-based*/
/*                              authentication case, this argument is not  */
/*                              valid)                                     */
/*                      u1AuthMode - Authentication mode (Values can be    */
/*                               PNAC_PORT_AUTHMODE_PORTBASED or           */
/*                               PNAC_PORT_AUTHMODE_MACBASED)              */
/*                      u1AuthStatus - Authorization Status                */
/*                           (Values can be PNAC_PORTSTATUS_AUTHORIZED or  */
/*                             PNAC_PORTSTATUS_UNAUTHORIZED)               */
/*                      u1CtrlDir - Oper Controlled directions             */
/*                   ( PNAC_CNTRLD_DIR_BOTH or PNAC_CNTRLD_DIR_IN )        */
/*                      pSlotInfo - Pointer to the Slot information 
 *                      structure                                          */
/*                                                                         */
/* Output(s)          : None.                                              */
/*                                                                         */
/* Global Variables                                                        */
/* Referred           : None.                                              */
/*                                                                         */
/* Global Variables                                                        */
/* Modified           : None.                                              */
/*                                                                         */
/* Return Value(s)    : FNP_SUCCESS or FNP_FAILURE.                        */
/***************************************************************************/
INT4
PnacMbsmHwSetAuthStatus (UINT2 u2Port, UINT1 *pu1SuppAddr,
                         UINT1 u1AuthMode, UINT1 u1AuthStatus, UINT1 u1CtrlDir,
                         tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (pu1SuppAddr);
    UNUSED_PARAM (u1AuthMode);
    UNUSED_PARAM (u1AuthStatus);
    UNUSED_PARAM (u1CtrlDir);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}
#endif
