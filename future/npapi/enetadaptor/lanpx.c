/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lanpx.c,v 1.3 2007/08/01 09:12:45 iss Exp $
 *
 * Description: Exported file for FS NP-API.
 *
 *******************************************************************/
#ifndef _LANPX_C_
#define _LANPX_C_

/*****************************************************************************/
/*                           INCLUDE FILES                                   */
/*****************************************************************************/
#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "la.h"
#include "npapi.h"

#include "brgnp.h"
#include "npla.h"

/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : FsLaMbsmHwCreateAggGroup.                            */
/*                                                                           */
/* Description        : This function is called when an aggregator's Oper    */
/*                      Status becomes UP i.e. when the first port is        */
/*                      attached to the aggregator.                          */
/*                                                                           */
/* Input(s)           : Aggregator Index.                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsLaMbsmHwCreateAggGroup (UINT2 u2AggIndex, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u2AggIndex);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsLaMbsmHwAddPortToConfAggGroup                      */
/*                                                                           */
/* Description        : This function is called to add a port to the         */
/*                      configured port list of the NP Agg entry.            */
/*                                                                           */
/* Input(s)           : Aggregator Index, Port Number.                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsLaMbsmHwAddPortToConfAggGroup (UINT2 u2AggIndex, UINT2 u2PortNumber,
                                 tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u2AggIndex);
    UNUSED_PARAM (u2PortNumber);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsLaMbsmHwAddLinkToAggGroup.                         */
/*                                                                           */
/* Description        : This function is called to attach a port to its      */
/*                      aggregator.                                          */
/*                                                                           */
/* Input(s)           : Aggregator Index, Port Number.                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsLaMbsmHwAddLinkToAggGroup (UINT2 u2AggIndex, UINT2 u2PortNumber,
                             tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u2AggIndex);
    UNUSED_PARAM (u2PortNumber);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsLaMbsmHwRemoveLinkFromOtherUnits                   */
/*                                                                           */
/* Description        : This function is called when a attached port is      */
/*                      removed from the aggregation group as a result of    */
/*                      the Card on which this port resides, being           */
/*                      removed from the system i.e. the port is no longer   */
/*                      present in the system.                               */
/*                                                                           */
/* Input(s)           : Aggregator Index, Port Number.                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsLaMbsmHwRemoveLinkFromOtherUnits (UINT2 u2AggIndex, UINT2 u2PortNumber)
{
    UNUSED_PARAM (u2AggIndex);
    UNUSED_PARAM (u2PortNumber);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      :  FsLaHwSetSelectionPolicy.                           */
/*                                                                           */
/* Description        : This function is called to set the selection policy. */
/*                                                                           */
/* Input(s)           : Aggregation Index                                    */
/*                      Link Selection Policy                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsLaMbsmHwSetSelectionPolicy (UINT2 u2AggIndex, UINT1 u1SelectionPolicy,
                              tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u2AggIndex);
    UNUSED_PARAM (u1SelectionPolicy);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsLaMbsmHwInit.                                      */
/*                                                                           */
/* Description        : This function is called when LA is initialised.      */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : aLaHwAggEntry                                        */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                              */
/*                      FNP_FAILURE - On failure                              */
/*****************************************************************************/

INT4
FsLaMbsmHwInit (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}
#endif
/* end of file */
