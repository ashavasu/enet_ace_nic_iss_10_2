/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlannp.c,v 1.36 2012/07/26 09:42:24 siva Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/
#ifdef VLAN_WANTED

#ifndef _VLANNP_C_
#define _VLANNP_C_

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "npapi.h"
#include "npvlan.h"

/* VLAN FDB TABLE is currently supported for the Option NPAPI_WANTED.
 * Which means FDB table is present either in hardware or software.
 * If the table should be duplicated then whenever software is updated
 * then the hardware should be intimated */

/*****************************************************************************/
/*    Function Name       : FsVlanHwInit                                     */
/*                                                                           */
/*    Description         : This function takes care of initialising the     */
/*                          hardware related parameters.                     */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*****************************************************************************/
INT4
FsVlanHwInit (VOID)
{
    /* Set Egress filtering to enable in the hardware */
    /* gegrSetUnkUcEgressFilterCmd () - Egress filtering for Unknown Unicast */
    /* gegrSetUnregMcEgressFilterCmd ()  - Egress filtering for unregistered 
     * multicast*/
    /* gegrVlanEgressFilteringEnable () - Egress filtering for known unicast */

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwDeInit                                   */
/*                                                                           */
/*    Description         : This function deinitialises all the hardware     */
/*                          related tables and parameters.                   */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns                 : FNP_SUCCESS/FNP_FAILURE                      */
/*****************************************************************************/
INT4
FsVlanHwDeInit (VOID)
{

    /* Disable VLAN in hardware */
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwSetBrgMode                               */
/*                                                                           */
/*    Description         : This function sets the bridge mode in hardware.  */
/*                                                                           */
/*                          For chipset that requires offset for filter      */
/*                          installation, all the filters needs to be        */
/*                          reinstalled with the new offset appropriate to   */
/*                          the bridge-mode.                                 */
/*                                                                           */
/*    Input(s)            : u4BridgeMode - Bridge Mode to set.It should take */
/*                          any one of the following.
 *                          VLAN_CUSTOMER_BRIDGE_MODE,
 *                          VLAN_PROVIDER_BRIDGE_MODE,
 *                          VLAN_PROVIDER_EDGE_BRIDGE_MODE, 
 *                          VLAN_PROVIDER_CORE_BRIDGE_MODE
 *                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns                 : FNP_SUCCESS/FNP_FAILURE                      */
/*****************************************************************************/
INT4
FsVlanHwSetBrgMode (UINT4 u4BridgeMode)
{
    UNUSED_PARAM (u4BridgeMode);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwSetAllGroupsPorts                        */
/*                                                                           */
/*    Description         : This function takes care of updating the         */
/*                          hardware table for  Forward All Groups behavior  */
/*                          for the specified VLAN ID.                       */
/*                                                                           */
/*    Input(s)            : VlanId  - VlanId for which the forward all 
 *                          groups behaviour needs to be updated. 
 *                                                                           */
/*                          PortBmp - PortList ( Port needed to be set )     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwSetAllGroupsPorts (tVlanId VlanId, tPortList PortBmp)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (PortBmp);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwResetAllGroupsPorts                      */
/*                                                                           */
/*    Description         : This function removes the given Ports from       */
/*                          Forward All Group Port list for the specified    */
/*                          Vlan.                                            */
/*                                                                           */
/*    Input(s)            : VlanId  - VlanId for which the forward all 
 *                          groups behaviour needs to be updated. 
 *                                                                           */
/*                          PortBmp - PortList ( Ports to be removed from 
 *                          the forward AllGroups egress port list )         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwResetAllGroupsPorts (tVlanId VlanId, tPortList PortBmp)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (PortBmp);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwSetUnRegGroupsPorts                      */
/*                                                                           */
/*    Description         : This function takes care of updating the         */
/*                          hardware table for  Forward All unreg groups     */
/*                          for the specified VLAN ID.                       */
/*                                                                           */
/*    Input(s)            : VlanId - VlanId for which the forward 
 *                          unregistered groups behaviour needs to be 
 *                          updated.           
 *                                                                           */
/*                          PortBmp  - PortList ( Ports to be set ).         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwSetUnRegGroupsPorts (tVlanId VlanId, tPortList PortBmp)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (PortBmp);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwResetUnRegGroupsPorts                    */
/*                                                                           */
/*    Description         : This function removes the given Ports from       */
/*                          Forward UnReg Group Port list for the specified  */
/*                          Vlan.                                            */
/*                                                                           */
/*    Input(s)            : VlanId  - VlanId for which the forward 
 *                          unregistered groups behaviour needs to be 
 *                          updated.             
 *                                                                           */
/*                          PortBmp - PortList ( Ports to be removed )       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwResetUnRegGroupsPorts (tVlanId VlanId, tPortList PortBmp)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (PortBmp);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwAddStaticUcastEntry                      */
/*                                                                           */
/*    Description         : This function adds the egress ports  into        */
/*                          hardware Static Unicast table                    */
/*                                                                           */
/*    Input(s)            : u4Fid            - Fid ( FDB Id )                */
/*                          pMacAddr         - Pointer to the Mac Address    */
/*                                             to be added.                  */
/*                          u4Port           - Received Interface Index      */
/*                          AllowedToGoPortBmp - PortList which has the      */
/*                                               list of outgoing ports      */
/*                          u1Status         -  Status of entry takes values */
/*                          like  VLAN_PERMANENT,VLAN_DELETE_ON_RESET,       */
/*                          VLAN_DELETE_ON_TIMEOUT                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwAddStaticUcastEntry (UINT4 u4Fid, tMacAddr MacAddr,
                             UINT4 u4Port, tPortList AllowedToGoPortBmp,
                             UINT1 u1Status)
{
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (AllowedToGoPortBmp);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u1Status);

    /* If number of ports in the Port list is more than 1, then return failure */
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwDelStaticUcastEntry                      */
/*                                                                           */
/*    Description         : This function delets the entry from  hardware    */
/*                          Static Unicast table.                            */
/*                                                                           */
/*    Input(s)            : u4Fid            - Fid ( FDB Id )                */
/*                          pMacAddr         - Pointer to the Mac Address.   */
/*                          u4Port           - Received Interface Index      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwDelStaticUcastEntry (UINT4 u4Fid, tMacAddr MacAddr, UINT4 u4Port)
{
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (MacAddr);

    return FNP_SUCCESS;
}

#ifndef SW_LEARNING
/*****************************************************************************/
/*    Function Name       : FsVlanHwGetFdbCount                              */
/*                                                                           */
/*    Description         : This function gets the Dynamic Fdb Count for the */
/*                          given FdbId                                      */
/*                                                                           */
/*    Input(s)            : u4FdbId  - FdbId.                                */
/*                                                                           */
/*    Output(s)           : pu4Count - Count of the given FdbId.             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwGetFdbCount (UINT4 u4FdbId, UINT4 *pu4Count)
{
    UNUSED_PARAM (u4FdbId);
    UNUSED_PARAM (pu4Count);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwGetFdbEntry                              */
/*                                                                           */
/*    Description         : This function gets the entry from  hardware      */
/*                           Unicast Mac table.                              */
/*                                                                           */
/*    Input(s)            : u4FdbId  - FdbId                                 */
/*                          pMacAddr - Pointer to the Mac Address.           */
/*                                                                           */
/*    Output(s)           : pEntry  - Pointer to Unicast Mac Entry           */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwGetFdbEntry (UINT4 u4FdbId,
                     tMacAddr MacAddr, tHwUnicastMacEntry * pEntry)
{
    UNUSED_PARAM (u4FdbId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (pEntry);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwGetFirstTpFdbEntry                       */
/*                                                                           */
/*    Description         : This function gets the first Fdb entry in the    */
/*                          lexicographic order.                             */
/*                                                                           */
/*    Input(s)            : pu4FdbId - Pointer to first Fdb id               */
/*                          pMacAddr - Pointer to the Mac Address.           */
/*                                                                           */
/*    Output(s)           : The first FdbId and Mac address.                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwGetFirstTpFdbEntry (UINT4 *pu4FdbId, tMacAddr MacAddr)
{
    UNUSED_PARAM (pu4FdbId);
    UNUSED_PARAM (MacAddr);
    /* gfdbGetFdbEntryFirst () */

    /* 
     * First get the VlanId depending on the Vlan learning type and
     * then invoke the Hw api.
     */

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwGetNextTpFdbEntry                        */
/*                                                                           */
/*    Description         : This function gets the next Fdb entry in the     */
/*                          lexicographic order.                             */
/*                                                                           */
/*    Input(s)            : u4FdbId - The current FdbId.                     */
/*                          pMacAddr - Pointer to the current Mac addr.      */
/*    Input(s)            : pu4NextFdbId - Pointer to the next Fdb Id.       */
/*                          pNextMacAddr - Pointer to the next mac addr.     */
/*                                                                           */
/*    Output(s)           : The next FdbId and Mac address.                  */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwGetNextTpFdbEntry (UINT4 u4FdbId, tMacAddr MacAddr,
                           UINT4 *pu4NextFdbId, tMacAddr NextMacAddr)
{
    UNUSED_PARAM (u4FdbId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (pu4NextFdbId);
    UNUSED_PARAM (NextMacAddr);

    /* 
     * First get the next VlanId depending on the Vlan learning type and
     * then invoke the Hw api.
     */

    return FNP_SUCCESS;
}

#endif
/*****************************************************************************/
/*    Function Name       : FsVlanHwAddMcastEntry                            */
/*                                                                           */
/*    Description         : This function adds an entry to the hardware      */
/*                          Multicast table.                                 */
/*                          This function can be called, when the Multicast  */
/*                          entry is already present in the Hardware. In     */
/*                          which case the Old Multicast member ports        */
/*                          must be removed and the new ports ('PortBmp')    */
/*                          must be added in the Hardware.                   */
/*                                                                           */
/*    Input(s)            : VlanId   - VlanId                                */
/*                          pMacAddr - Pointer to the Mac Address.           */
/*                          PortBmp - List of Ports on which the packet for 
 *                          the specified multicast address and VLAN ID 
 *                          has to be forwarded .                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwAddMcastEntry (tVlanId VlanId, tMacAddr MacAddr, tPortList PortBmp)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (PortBmp);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwAddStMcastEntry                          */
/*                                                                           */
/*    Description         : This function adds a static entry to the         */
/*                          hardware Multicast table                         */
/*                                                                           */
/*    Input(s)            : VlanId    - VlanId of the multicast entry.       */
/*                          pMacAddr  - Pointer to the Mac Address.          */
/*                          i4RcvPort - Received port                        */
/*                          PortBmp   - List of Ports.                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwAddStMcastEntry (tVlanId VlanId, tMacAddr MacAddr,
                         INT4 i4RcvPort, tPortList PortBmp)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (i4RcvPort);
    UNUSED_PARAM (PortBmp);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwSetMcastPort                             */
/*                                                                           */
/*    Description         : This function adds a member port to the existing */
/*                          Multicast entry. The Application has to ensure   */
/*                          that the existence of Mcast Entry for the port.  */
/*                          This API will be called only under dynamic       */
/*                          updation of the Multicast Entry by the S/W.      */
/*                                                                           */
/*    Input(s)            : VlanId   - VlanId                                */
/*                          pMacAddr - Pointer to the Mac Address.           */
/*                          u4IfIndex - Interface Index of New member port   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwSetMcastPort (tVlanId VlanId, tMacAddr MacAddr, UINT4 u4IfIndex)
{

    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4IfIndex);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwResetMcastPort                           */
/*                                                                           */
/*    Description         : This function resets a member port from the      */
/*                          existing multicast entry. Application has to     */
/*                          ensure the existence of multicast entry          */
/*                                                                           */
/*    Input(s)            : VlanId   - VlanId                                */
/*                          pMacAddr - Pointer to the Mac Address.           */
/*                          u4IfIndex - Interface Index of member port to be */
/*                                      removed                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwResetMcastPort (tVlanId VlanId, tMacAddr MacAddr, UINT4 u4IfIndex)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4IfIndex);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwDelMcastEntry                            */
/*                                                                           */
/*    Description         : This function deletes an entry from the hardware */
/*                          Multicast table.                                 */
/*                                                                           */
/*    Input(s)            : VlanId   - VlanId                                */
/*                          pMacAddr - Pointer to the Mac Address 
 *                          to be deleted.                                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwDelMcastEntry (tVlanId VlanId, tMacAddr MacAddr)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwDelStMcastEntry                          */
/*                                                                           */
/*    Description         : This function deletes a static entry from the    */
/*                          hardware Multicast tale                          */
/*                                                                           */
/*    Input(s)            : VlanId    - VlanId                               */
/*                          pMacAddr  - Pointer to the Mac Address.          */
/*                          i4RcvPort - Received port                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwDelStMcastEntry (tVlanId VlanId, tMacAddr MacAddr, INT4 i4RcvPort)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (i4RcvPort);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwAddVlanEntry                             */
/*                                                                           */
/*    Description         : This function adds an entry to the hardware      */
/*                          Vlan table.                                      */
/*                                                                           */
/*    Input(s)            : VlanId     - VlanId                              */
/*                          PortBmp - List of Ports to be added as member 
 *                                    ports.                                 */
/*                          UnTagPortBmp - List of Untagged Ports.           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwAddVlanEntry (tVlanId VlanId, tPortList PortBmp, tPortList UnTagPortBmp)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (PortBmp);
    UNUSED_PARAM (UnTagPortBmp);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwDelVlanEntry                             */
/*                                                                           */
/*    Description         : This function deletes an entry from the hardware */
/*                          Vlan table.                                      */
/*                          In case of Hybrid learning switches, this fn.    */
/*                          should disassociate the VLAN to FID mappings and */
/*                          and shud delete the FDB table if the number of   */
/*                          VLANS mapped to the FDB is 0.                    */
/*                                                                           */
/*    Input(s)            : VlanId     - VlanId to be deleted.               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwDelVlanEntry (tVlanId VlanId)
{
    UNUSED_PARAM (VlanId);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwSetVlanMemberPort                        */
/*                                                                           */
/*    Description         : This function set the port as the member of      */
/*                          given Vlan                                       */
/*                                                                           */
/*    Input(s)            : VlanId     - Vlan to which the port is going to  */
/*                                        be an member                       */
/*                          u4IfIndex  - The Interface Index                 */
/*                          u1IsTagged - Flag to indicate whether this port  */
/*                                       will be tagged or untagged can take */
/*                                       VLAN_TRUE/VLAN_FALSE                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwSetVlanMemberPort (tVlanId VlanId, UINT4 u4IfIndex, UINT1 u1IsTagged)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1IsTagged);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwResetVlanMemberPort                      */
/*                                                                           */
/*    Description         : This function deletes a member port of the 
 *                          specified VLAN entry from the hardware table     */
/*                          given Vlan.                                      */
/*                                                                           */
/*    Input(s)            : VlanId     - Vlan id of the entry to be updated  */
/*                                                                           */
/*                          u4IfIndex  - The Interface Index that is to be 
 *                                       deleted from the member list of the 
 *                                       specified VLAN                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwResetVlanMemberPort (tVlanId VlanId, UINT4 u4IfIndex)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4IfIndex);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwSetPortPvid                              */
/*                                                                           */
/*    Description         : This function sets the given VlanId as PortPvid  */
/*                          for the Port.                                    */
/*                                                                           */
/*    Input(s)            : VlanId     - Vlan to which the port is going to  */
/*                                        be an member                       */
/*                          u4IfIndex  - The Interface Index for which 
 *                                       PVID is to be set                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwSetPortPvid (UINT4 u4IfIndex, tVlanId VlanId)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4IfIndex);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsVlanHwSetDefaultVlanId                         */
/*                                                                           */
/*    Description         : This function sets the given VlanId as default   */
/*                          Vlan Id                                          */
/*                                                                           */
/*    Input(s)            : DefaultVlanId - Vlan Identifier of the default   */
/*                          Vlan                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS                                      */
/*****************************************************************************/
INT4
FsVlanHwSetDefaultVlanId (tVlanId DefaultVlanId)
{
    /* Need to update the Default Vlan Id */
    UNUSED_PARAM (DefaultVlanId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwSetBaseBridgeMode                        */
/*                                                                           */
/*    Description         : This function sets the 802.1D Mode in hardware   */
/*                                                                           */
/*    Input(s)            : u4Mode    - Enable / Diable 802.1D Mode          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwSetBaseBridgeMode (UINT4 u4Mode)
{
    UNUSED_PARAM (u4Mode);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwSetPortAccFrameType                      */
/*                                                                           */
/*    Description         : This function sets the Acceptable frame types    */
/*                          parameter for the Given Port.                    */
/*                                                                           */
/*    Input(s)            : u4IfIndex      - The Interface Index             */
/*                          u1AccFrameType - Acceptable Frame types          */
/*                                           parameter value can take        */
/*                          VLAN_ADMIT_ALL_FRAMES,                           */
/*                           VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES,             */
/*                  VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES.     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwSetPortAccFrameType (UINT4 u4IfIndex, INT1 u1AccFrameType)
{
    UNUSED_PARAM (u1AccFrameType);
    UNUSED_PARAM (u4IfIndex);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwSetPortIngFiltering                      */
/*                                                                           */
/*    Description         : This function sets the Ingress Filtering         */
/*                          parameter for the Given Port.                    */
/*                                                                           */
/*    Input(s)            : u4IfIndex      - The Interface Index             */
/*                          u1IngFilterEnable - FNP_TRUE if Ingress          */
/*                                              filtering enabled, otherwise */
/*                                              FNP_FALSE                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwSetPortIngFiltering (UINT4 u4IfIndex, UINT1 u1IngFilterEnable)
{
    UNUSED_PARAM (u1IngFilterEnable);
    UNUSED_PARAM (u4IfIndex);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwVlanEnable                               */
/*                                                                           */
/*    Description         : This function enables VLAN in the hardware 
 *                          table.                                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwVlanEnable (VOID)
{

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwVlanDisable                              */
/*                                                                           */
/*    Description         : This function disables VLAN in the hardware 
 *                          table.                                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            :                                                   */
/*                                FNP_SUCCESS: Disabled and entries flushed  */
/*                                FNP_FAILURE: Disabling not supported       */
/*                                FNP_VLAN_DISABLED_BUT_NOT_DELETED:         */
/*                                 Hardware supports disabling but the       */
/*                                 entries were not flushed                  */
/*****************************************************************************/
INT4
FsVlanHwVlanDisable (VOID)
{

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwSetVlanLearningType                      */
/*                                                                           */
/*    Description         : This function programs the Vlan Learning Type    */
/*                                                                           */
/*    Input(s)            : u1LearningType - can take                        */
/*                          VLAN_INDEP_LEARNING,                             */
/*                          VLAN_SHARED_LEARNING,                            */
/*                          VLAN_HYBRID_LEARNING                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwSetVlanLearningType (UINT1 u1LearningType)
{
    UNUSED_PARAM (u1LearningType);
    /* 
     * If the Hardware does not support the given Vlan Learning Type, then
     * return failure.
     */
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwSetMacBasedStatusOnPort                  */
/*                                                                           */
/*    Description         : This function enable or disable the Mac based    */
/*                        Vlan Type on the port                              */
/*    Input(s)            : u4IfIndex            - The Interface Index       */
/*                          u1MacBasedVlanEnable - FNP_TRUE/FNP_FALSE        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwSetMacBasedStatusOnPort (UINT4 u4IfIndex, UINT1 u1MacBasedVlanEnable)
{

    UNUSED_PARAM (u1MacBasedVlanEnable);
    UNUSED_PARAM (u4IfIndex);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwSetSubnetBasedStatusOnPort               */
/*                                                                           */
/*    Description         : This function enable or disable the Subnet based */
/*                          Vlan classification on the port                  */
/*                                                                           */
/*    Input(s)            : u4IfIndex   - Interface Index                    */
/*                          u1SubnetBasedVlanEnable - FNP_TRUE/FNP_FALSE     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwSetSubnetBasedStatusOnPort (UINT4 u4IfIndex,
                                    UINT1 u1SubnetBasedVlanEnable)
{
    UNUSED_PARAM (u1SubnetBasedVlanEnable);
    UNUSED_PARAM (u4IfIndex);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwEnableProtoVlanOnPort                    */
/*                                                                           */
/*    Description         : This function enable or disable the  Port        */
/*                          Protocol based type on the port                  */
/*    Input(s)            : u4IfIndex               - The Interface Index    */
/*                           u1VlanProtoEnable - FNP_TRUE/FNP_FALSE          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwEnableProtoVlanOnPort (UINT4 u4IfIndex, UINT1 u1VlanProtoEnable)
{
    UNUSED_PARAM (u1VlanProtoEnable);
    UNUSED_PARAM (u4IfIndex);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwSetDefUserPriority                       */
/*                                                                           */
/*    Description         : This function set the default user priority to   */
/*                          port                                             */
/*                                                                           */
/*    Input(s)            : u4IfIndex               - The Interface Index    */
/*                          i4DefPriority - default user priority 
 *                          ranges <0-7>                                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwSetDefUserPriority (UINT4 u4IfIndex, INT4 i4DefPriority)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4DefPriority);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwSetPortNumTrafClasses                    */
/*                                                                           */
/*    Description         : This function set the number of traffic classes  */
/*                          for the port.                                    */
/*                                                                           */
/*    Input(s)            : u4IfIndex               - The Interface Index    */
/*                          i4NumTraffClass - number of traffic classesi 
 *                          ranges from < 1 - 8 >     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwSetPortNumTrafClasses (UINT4 u4IfIndex, INT4 i4NumTraffClass)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4NumTraffClass);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwSetRegenUserPriority                     */
/*                                                                           */
/*    Description         : This function overides the priority information  */
/*                          in the priority regeneration table maintained by */
/*                          each port for the user priority                  */
/*                                                                           */
/*    Input(s)            : u4IfIndex               - The Interface Index    */
/*                         i4UserPriority  - user priority information can 
 *                         take value <0-7>                                  */
/*                         i4RegenPriority - regenerated user priority     
 *                         can take value <0-7>                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwSetRegenUserPriority (UINT4 u4IfIndex, INT4 i4UserPriority,
                              INT4 i4RegenPriority)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4UserPriority);
    UNUSED_PARAM (i4RegenPriority);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwSetTraffClassMap                         */
/*                                                                           */
/*    Description         : This function set the traffic class for which    */
/*                          user priority of the port should map into        */
/*    Input(s)            : u4IfIndex               - The Interface Index    */
/*                          i4UserPriority  - user priority ranges < 0 - 7 > */
/*                          i4TraffClass - traffic class to be mapped 
 *                          ranges < 0 - 7 >                                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwSetTraffClassMap (UINT4 u4IfIndex, INT4 i4UserPriority,
                          INT4 i4TraffClass)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4UserPriority);
    UNUSED_PARAM (i4TraffClass);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwGmrpEnable                               */
/*                                                                           */
/*    Description         : This function will be used to enable GMRP in H/W */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwGmrpEnable (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwGmrpDisable                              */
/*                                                                           */
/*    Description         : This function will be used to disable GMRP in    */
/*                          H/W                                              */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwGmrpDisable (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwGvrpEnable                               */
/*                                                                           */
/*    Description         : This function will be used to enable GVRP in H/W */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwGvrpEnable (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwGvrpDisable                              */
/*                                                                           */
/*    Description         : This function will be used to disable GVRP in    */
/*                          H/W                                              */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwGvrpDisable (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsNpDeleteAllFdbEntries                          */
/*                                                                           */
/*    Description         : This function flushes all entres in FDB table    */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
VOID
FsNpDeleteAllFdbEntries (VOID)
{
    return;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwAddVlanProtocolMap                       */
/*                                                                           */
/*    Description         : This function maps the valid and existence group */
/*                          ID to Vlan ID                                    */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                          u4GroupId  - Group Id ranges < 0 -2147483647 >   */
/*                          pProtoTemplate - pointer to protocol template    */
/*                          tVlanId - VLAN Id                                */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwAddVlanProtocolMap (UINT4 u4IfIndex, UINT4 u4GroupId,
                            tVlanProtoTemplate * pProtoTemplate, tVlanId VlanId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4GroupId);
    UNUSED_PARAM (pProtoTemplate);
    UNUSED_PARAM (VlanId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwDelVlanProtocolMap                       */
/*                                                                           */
/*    Description         : This function deletes the mapped VlanId  from    */
/*                          protocol group                                   */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                          pProtoTemplate - pointer to protocol template    */
/*                          u4GroupId  - Group Id < 0 - 2147483647 >         */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwDelVlanProtocolMap (UINT4 u4IfIndex, UINT4 u4GroupId,
                            tVlanProtoTemplate * pProtoTemplate)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4GroupId);
    UNUSED_PARAM (pProtoTemplate);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsVlanHwGetPortStats                             */
/*                                                                           */
/*    Description         : This function gets the statistics value for the  */
/*                          given statistics type for a port on a vlan       */
/*                                                                           */
/*    Input(s)            : u4Port     - The Port Number                     */
/*                          VlanId     - Vlan Id                             */
/*                          StatsType  - Statistics type can take any of the 
 *                          following.
 *                          VLAN_STAT_DOT1D_TP_PORT_IN_DISCARDS,
 *                          VLAN_STAT_VLAN_PORT_IN_FRAMES,
 *                          VLAN_STAT_VLAN_PORT_OUT_FRAMES,
 *                          VLAN_STAT_VLAN_PORT_IN_DISCARDS,
 *                          VLAN_STAT_VLAN_PORT_IN_OVERFLOW,
 *                          VLAN_STAT_VLAN_PORT_OUT_OVERFLOW,
 *                          VLAN_STAT_VLAN_PORT_IN_DISCARDS_OVERFLOW         */
/*                                                                           */
/*    Output(s)           : pu4PortStatsValue - Pointer to the               */
/*                          Statistics value                                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS                                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
FsVlanHwGetPortStats (UINT4 u4Port, tVlanId VlanId, UINT1 u1StatsType,
                      UINT4 *pu4PortStatsValue)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1StatsType);

    pu4PortStatsValue = 0;
    /* 
     * Based on the StatsType obtain the statistics value on the 
     * u4Port/VlanId
     */
    return (FNP_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsVlanHwGetPortStats64                           */
/*                                                                           */
/*    Description         : This function gets the statistics value for the  */
/*                          given statistics type for a port on a vlan       */
/*                                                                           */
/*    Input(s)            : u4Port     - The Port Number                     */
/*                          VlanId     - Vlan Id                             */
/*                          StatsType  - Statistics type can take any of the 
 *                          following.
 *                          VLAN_STAT_DOT1D_TP_PORT_IN_DISCARDS,
 *                          VLAN_STAT_VLAN_PORT_IN_FRAMES,
 *                          VLAN_STAT_VLAN_PORT_OUT_FRAMES,
 *                          VLAN_STAT_VLAN_PORT_IN_DISCARDS,
 *                          VLAN_STAT_VLAN_PORT_IN_OVERFLOW,
 *                          VLAN_STAT_VLAN_PORT_OUT_OVERFLOW,
 *                          VLAN_STAT_VLAN_PORT_IN_DISCARDS_OVERFLOW         */
/*                                                                           */
/*    Output(s)           : pValue - Pointer to the Statistics value         */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS                                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
FsVlanHwGetPortStats64 (UINT4 u4Port, tVlanId VlanId, UINT1 u1StatsType,
                        tSNMP_COUNTER64_TYPE * pValue)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1StatsType);

    MEMSET (pValue, 0, sizeof (tSNMP_COUNTER64_TYPE));
    /* 
     * Based on the StatsType obtain the statistics value on the 
     * u4Port/VlanId
     */

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwSetPortTunnelMode                        */
/*                                                                           */
/*    Description         : This function sets the tunnel mode of a port.    */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                          u4Mode    - Tunnel mode of a port it can take    */
/*                          VLAN_ACCESS_PORT,                                */
/*                          VLAN_TRUNK_PORT,                                 */
/*                          VLAN_HYBRID_PORT                                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwSetPortTunnelMode (UINT4 u4IfIndex, UINT4 u4Mode)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4Mode);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwSetTunnelFilter                          */
/*                                                                           */
/*    Description         : This function creates/deletes h/w filters for    */
/*                          Provider Gvrp/Gmrp/STP Macaddress. This enables  */
/*                          the tunnel PDUs to be switched by software.      */
/*                                                                           */
/*    Input(s)            : u4BridgeMode - Bridge Mode to set.It should take */
/*                          any one of the following.
 *                          VLAN_CUSTOMER_BRIDGE_MODE,
 *                          VLAN_PROVIDER_BRIDGE_MODE,
 *                          VLAN_PROVIDER_EDGE_BRIDGE_MODE, 
 *                          VLAN_PROVIDER_CORE_BRIDGE_MODE
 *                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gVlanProviderGmrpAddr, gVlanProviderStpAddr*/
/*                                gVlanProviderGvrpAddr                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwSetTunnelFilter (INT4 i4BridgeMode)
{
    UNUSED_PARAM (i4BridgeMode);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwCheckTagAtEgress                         */
/*                                                                           */
/*    Description         : This function specifies whether the underlying   */
/*                          H/W supports the exlusion of the provider VLAN   */
/*                          Tag by itself.                                   */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : pu1TagSet - VLAN_TRUE/ VLAN_FALSE                */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwCheckTagAtEgress (UINT1 *pu1TagSet)
{
    UNUSED_PARAM (*pu1TagSet);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwCheckTagAtIngress                        */
/*                                                                           */
/*    Description         : This function specifies whether the underlying   */
/*                          H/W supports the inclusion of the provider VLAN  */
/*                          Tag by itself.                                   */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : pu1TagSet - VLAN_TRUE/ VLAN_FALSE                */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwCheckTagAtIngress (UINT1 *pu1TagSet)
{
    UNUSED_PARAM (*pu1TagSet);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwCreateFdbId                              */
/*                                                                           */
/*    Description         : This function will be used to create an Fid      */
/*                          entry in H/W.                                    */
/*                          If the FID is already created in the hardware,   */
/*                          the function should return success.              */
/*                                                                           */
/*    Input(s)            : u4Fid - Fdb Id                                   */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwCreateFdbId (UINT4 u4Fid)
{
    UNUSED_PARAM (u4Fid);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwDeleteFdbId                              */
/*                                                                           */
/*    Description         : This function will be used to delete an Fid      */
/*                          entry in H/W.                                    */
/*                                                                           */
/*    Input(s)            : u4Fid - FDB Id                                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwDeleteFdbId (UINT4 u4Fid)
{
    UNUSED_PARAM (u4Fid);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwAssociateVlanFdb                         */
/*                                                                           */
/*    Description         : This function will be used to associate a VlanId */
/*                          with an Fid in H/w.                              */
/*                                                                           */
/*    Input(s)            : u4Fid - FDB Id.                                  */
/*                          VlanId - Vlan Identifier.                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwAssociateVlanFdb (UINT4 u4Fid, tVlanId VlanId)
{
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (VlanId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwDisassociateVlanFdb                      */
/*                                                                           */
/*    Description         : This function will be used to disassociate a     */
/*                          VlanId from a Fid in H/W                         */
/*                                                                           */
/*    Input(s)            : u4Fid - FDB ID.                                  */
/*                          VlanId - VlanIdentifier.                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwDisassociateVlanFdb (UINT4 u4Fid, tVlanId VlanId)
{
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (VlanId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwFlushPortFdbId                           */
/*                                                                           */
/*    Description         : This function flushes the Fdb entires learnt on  */
/*                          a specific port in a specific Fdb table          */
/*                                                                           */
/*    Input(s)            : u4Port               - The Interface Index       */
/*                          u4Fid   - Fdb Id in which the Entry has to be    */
/*                                   flushed.                                */
/*                           i4OptimizeFlag - Indicates whether this call can*/
/*                                            be grouped with the flush call */
/*                                            for other ports as a single    */
/*                                            flush call can take 
 *                                            VLAN_OPTIMIZE,
 *                                            VLAN_NO_OPTIMIZE               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwFlushPortFdbId (UINT4 u4Port, UINT4 u4Fid, INT4 i4OptimizeFlag)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (i4OptimizeFlag);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwFlushPort                                */
/*                                                                           */
/*    Description         : This function flushes the FDB entries            */
/*                          learnt on this port. Flushing                    */
/*                          is done based on port in all  FDB Table          */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : u4IfIndex               - The Interface Index
 *                          i4OptimizeFlag - can take VLAN_OPTIMIZE,
 *                          VLAN_NO_OPTIMIZE                                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwFlushPort (UINT4 u4IfIndex, INT4 i4OptimizeFlag)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4OptimizeFlag);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwFlushFdbId                               */
/*                                                                           */
/*    Description         : This function flushes all the learnt FDB entries */
/*                          in the FDB table                                 */
/*                                                                           */
/*    Input(s)            : u4Fid               - The FDB ID                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwFlushFdbId (UINT4 u4Fid)
{
    UNUSED_PARAM (u4Fid);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name   : FsVlanHwSetShortAgeout                               */
/*                                                                           */
/*    Description     : This function sets the short age out time for        */
/*                      this port.                                           */
/*                                                                           */
/*    Input(s)        : u4Port - port number                              */
/*                      i4AgingTime - time in seconds to be set.
 *                      ranges < 10 - 1000000 >                              */
/*                                                                           */
/*    Output(s)       : None                                                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
FsVlanHwSetShortAgeout (UINT4 u4Port, INT4 i4AgingTime)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (i4AgingTime);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name   : FsVlanHwResetShortAgeout                             */
/*                                                                           */
/*    Description     : This function resets the short age out time and      */
/*                      reverts back to the long ageout for this port.       */
/*                                                                           */
/*    Input(s)        : u4Port - port number                              */
/*                      i4LongAgeout - Long ageout time to be reverted to 
 *                       NORMAL_AGEOUT_TIME, get from the API 
 *                       VlanGetAgeoutInt()                                  */
/*                                                                           */
/*    Output(s)       : None                                                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
FsVlanHwResetShortAgeout (UINT4 u4Port, INT4 i4LongAgeout)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (i4LongAgeout);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwGetVlanInfo                              */
/*                                                                           */
/*    Description         : This function returns the VLAN membership of the */
/*                          given VLAN in the hardware if it is present.     */
/*                          The VLAN membership returned should not include  */
/*                          the physical ports that are part of aggregation  */
/*                          group. i.e. if P1, P2 and P3 are members of      */
/*                          agg. group P26, then the port list returned      */
/*                          should contain P26 and not P1, P2 and P3.        */
/*                                                                           */
/*    Input(s)            : VlanId - VLAN id for which the info needs to be  */
/*                          retrieved.                                       */
/*                                                                           */
/*    Output(s)           : pHwEntry - VLAN info pointer.                    */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwGetVlanInfo (tVlanId VlanId, tHwVlanEntry * pHwEntry)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pHwEntry);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwGetMcastEntry                            */
/*                                                                           */
/*    Description         : This function gets the given multicast entry     */
/*                          present in the hardware.                         */
/*                          The MCAST membership returned should not include */
/*                          the physical ports that are part of aggregation  */
/*                          group. i.e. if P1, P2 and P3 are members of      */
/*                          agg. group P26, then the port list returned      */
/*                          should contain P26 and not P1, P2 and P3.        */
/*                                                                           */
/*    Input(s)            : VlanId - VLAN Id of the mcast entry.             */
/*                          MacAdd - Mcast mac address.                      */
/*                                                                           */
/*    Output(s)           : HwPortList - Hardware port list.                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwGetMcastEntry (tVlanId VlanId, tMacAddr MacAddr, tPortList HwPortList)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (HwPortList);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsVlanHwTraffClassMapInit                        */
/*                                                                           */
/*    Description         : This function sets which cosq a given priority   */
/*                          should fall into in all units.                   */
/*                                                                           */
/*    Input(s)            : u1Priority - Cosq Priority ranges <0-7>          */
/*                          i4CosqValue - Cosq Value can take value 
 *                          from the array gau1PriTrfClassMap                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*                                                                           */
/*****************************************************************************/

INT4
FsVlanHwTraffClassMapInit (UINT1 u1Priority, INT4 i4CosqValue)
{
    UNUSED_PARAM (u1Priority);
    UNUSED_PARAM (i4CosqValue);

    return FNP_SUCCESS;
}

#ifndef BRIDGE_WANTED
/****************************************************************************/
/* FUNCTION NAME : FsBrgSetAgingTime                                        */
/*                                                                          */
/* DESCRIPTION   : Set the FDB aging time value for the given context.      */
/*                 This will be called either during the STAP operation       
 *                 this period. Also can be used to set aging time other 
 *                 than default value 300secs supported by the 
 *                 802.1D/802.1Q  standard.                                  */
/*                                                                           */
/* INPUTS        : i4Aging - time in seconds ranges < 10 - 1000000 >         */
/* OUTPUTS       : None.                                                     */
/*                                                                           */
/* RETURNS       : FNP_SUCCESS - on success                                  */
/*                 FNP_FAILURE - on error                                    */
/*                                                                           */
/*                                                                           */
/* COMMENTS      : None                                                      */
/*                                                                           */
/*****************************************************************************/
INT1
FsBrgSetAgingTime (INT4 i4AgingTime)
{
    UNUSED_PARAM (i4AgingTime);
    return FNP_SUCCESS;
}
#endif

/****************************************************************************/
/* Function    Function    Function    :  FsVlanHwGetVlanStats              */
/*                                                                          */
/* Description :  Retrieves the VLAN statistics from the hardware           */
/*                                                                          */
/* Input       :  VlanId - Vlan Id                                          */
/*                         StatsType  - Statistics type can take any of     */
/*                                      the following.                      */
/*                                VLAN_STAT_DOT1D_TP_PORT_IN_DISCARDS,      */
/*                                VLAN_STAT_VLAN_PORT_IN_FRAMES,            */
/*                                VLAN_STAT_VLAN_PORT_OUT_FRAMES,           */
/*                                VLAN_STAT_VLAN_PORT_IN_DISCARDS,          */
/*                                VLAN_STAT_VLAN_PORT_IN_OVERFLOW,          */
/*                                VLAN_STAT_VLAN_PORT_OUT_OVERFLOW,         */
/*                                VLAN_STAT_VLAN_PORT_IN_DISCARDS_OVERFLOW  */
/*                                                                          */
/* Output      :  pu4VlanStatsValue - returned statistics value updated in  */
/*                the hardware for any of the stats type mentioned in       */
/*                u1StatsType.                                              */
/*                                                                          */
/* Returns     :  FNP_SUCCESS or FNP_FAILURE                                */
/****************************************************************************/
INT4
FsVlanHwGetVlanStats (tVlanId VlanId,
                      UINT1 u1StatsType, UINT4 *pu4VlanStatsValue)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1StatsType);
    UNUSED_PARAM (pu4VlanStatsValue);

    return FNP_SUCCESS;
}

/****************************************************************************/
/* Function    :  FsVlanHwResetVlanStats                                    */
/* Description :  Clears the VLAN statistics in the hardware                */
/* Input       :  VlanId - Vlan Id                                          */
/* Output      :  None                                                      */
/* Returns     :  FNP_SUCCESS or FNP_FAILURE
 ****************************************************************************/
INT4
FsVlanHwResetVlanStats (tVlanId VlanId)
{
    UNUSED_PARAM (VlanId);

    return FNP_SUCCESS;
}

/****************************************************************************/
/* Function Name      : FsVlanHwMacLearningStatus                           */
/*                                                                          */
/* Description        : This function is called for configuring the Mac     */
/*                      learning status for a VLAN.                         */
/*                                                                          */
/* Input(s)           : VlanId  - VLAN identifier.                          */
/*                      u2FdbId - Fdb Identifier                            */
/*                      HwPortList - Port list of VLAN member ports         */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED               */
/*                                                                          */
/* Output(s)          : None                                                */
/*                                                                          */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                        */
/*                      FNP_FAILURE - On failure                            */
/****************************************************************************/
INT4
FsVlanHwMacLearningStatus (tVlanId VlanId, UINT4 u2FdbId, tPortList HwPortList,
                           UINT1 u1Status)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2FdbId);
    UNUSED_PARAM (HwPortList);
    UNUSED_PARAM (u1Status);
    return FNP_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : FsVlanHwPortMacLearningStatus.                       */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning status for a Port                           */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanHwPortMacLearningStatus (UINT4 u4IfIndex, UINT1 u1Status)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    return FNP_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : FsVlanHwMacLearningLimit                             */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning limit for a VLAN.                           */
/*                                                                           */
/* Input(s)           : VlanId  - VLAN identifier.                           */
/*                      u2FdbId - Fdb Identifier                             */
/*                      u4MacLimit - MAC learning limit 
 *                      ranges < 0 - 4294967295 >.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanHwMacLearningLimit (tVlanId VlanId, UINT4 u2FdbId, UINT4 u4MacLimit)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4MacLimit);
    UNUSED_PARAM (u2FdbId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwSwitchMacLearningLimit                       */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning limit for a switch.                         */
/*                                                                           */
/* Input(s)           : u4MacLimit - MAC learning limit.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanHwSwitchMacLearningLimit (UINT4 u4MacLimit)
{
    UNUSED_PARAM (u4MacLimit);
    return FNP_SUCCESS;
}

#ifdef L2RED_WANTED
/*****************************************************************************/
/*    Function Name       : FsVlanHwScanProtocolVlanTbl                      */
/*                                                                           */
/*    Description         : This function scans the protocol VLAN table and  */
/*                          calls the callback provided by the application.  */
/*                                                                           */
/*    Input(s)            : ProtoVlanCallBack - Protocol VLAN call back      */
/*                          function pointer.                                */
/*                          u4Port - Port for which the scan needs to be     */
/*                          performed.                                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwScanProtocolVlanTbl (UINT4 u4Port, FsVlanHwProtoCb ProtoVlanCallBack)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (ProtoVlanCallBack);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwGetVlanProtocolMap                       */
/*                                                                           */
/*    Description         : This function returns the VLAN id for which the  */
/*                          protocol group is mapped.                        */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                          u4GroupId  - Group Id ranges < 0 - 2147483647 >  */
/*                          pProtoTemplate - pointer to protocol template    */
/*                                                                           */
/*    Output(s)           : pVlanId - Pointer to the VLAN ID.                */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwGetVlanProtocolMap (UINT4 u4IfIndex, UINT4 u4GroupId,
                            tVlanProtoTemplate * pProtoTemplate,
                            tVlanId * pVlanId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4GroupId);
    UNUSED_PARAM (pProtoTemplate);
    UNUSED_PARAM (pVlanId);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwScanMulticastTbl                         */
/*                                                                           */
/*    Description         : This function scans the VLAN multicast table and */
/*                          calls the callback provided by the application.  */
/*                                                                           */
/*    Input(s)            : McastCallBack - Multicast call back function     */
/*                          pointer.                                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwScanMulticastTbl (FsVlanHwMcastCb McastCallBack)
{
    UNUSED_PARAM (McastCallBack);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwGetStMcastEntry                          */
/*                                                                           */
/*    Description         : This function gets the given static mcast entry  */
/*                          present in the hardware.                         */
/*                                                                           */
/*    Input(s)            : VlanId - VLAN Id of the mcast entry.             */
/*                          MacAdd - Mcast mac address.                      */
/*                          u4RcvPort - Receive port.                        */
/*                          HwPortList - port list present in the hardware.  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwGetStMcastEntry (tVlanId VlanId, tMacAddr MacAddr,
                         UINT4 u4RcvPort, tPortList HwPortList)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4RcvPort);
    UNUSED_PARAM (HwPortList);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwScanUnicastTbl                           */
/*                                                                           */
/*    Description         : This function scans the VLAN unicast table and   */
/*                          calls the callback provided by the application.  */
/*                                                                           */
/*    Input(s)            : UcastCallBack - Unicast call back function       */
/*                          pointer.                                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsVlanHwScanUnicastTbl (FsVlanHwUcastCb UcastCallBack)
{
    UNUSED_PARAM (UcastCallBack);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwGetStaticUcastEntry                      */
/*                                                                           */
/*    Description         : This function gets the static unicast entry      */
/*                          for the given FDB Id and mac address.            */
/*                                                                           */
/*    Input(s)            : u4Fid            - Fid                           */
/*                          pMacAddr         - Pointer to the Mac Address.   */
/*                          u4Port           - Received Interface Index      */
/*                                                                           */
/*    Output(s)           : AllowedToGoPortBmp - PortList                    */
/*                          pu1Status        -  Status of entry give values */
/*                          as VLAN_PERMANENT,VLAN_DELETE_ON_RESET,          */
/*                          VLAN_DELETE_ON_TIMEOUT                           */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwGetStaticUcastEntry (UINT4 u4Fid, tMacAddr MacAddr,
                             UINT4 u4Port, tPortList AllowedToGoPortBmp,
                             UINT1 *pu1Status)
{
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (AllowedToGoPortBmp);
    UNUSED_PARAM (pu1Status);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwGetSyncedTnlProtocolMacAddr                  */
/*                                                                           */
/* Description        : Validate sync Protocol mac address in                */
/*                                                                           */
/* Input(s)           : u2Protocol - Protocol id                             */
/*                      pMacAddr    - synced Protocol mac address            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - if synced mac address is same as input */
/*                      FNP_FAILURE - if synced mac address is not same      */
/*****************************************************************************/
INT4
FsVlanHwGetSyncedTnlProtocolMacAddr (UINT4 u2Protocol, tMacAddr MacAddr)
{
    UNUSED_PARAM (u2Protocol);
    UNUSED_PARAM (MacAddr);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwSyncDefaultVlanId                        */
/*                                                                           */
/*    Description         : This function gets the default Vlan Id configured*/
/*                          in hardware and compares that with SwVlanId.     */
/*                          If there is a mismatch then it updates the hw    */
/*                          accordingly.                                     */
/*                                                                           */
/*    Input(s)            : SwVlanId - Default vlan id present in sw.        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwSyncDefaultVlanId (tVlanId SwVlanId)
{
    UNUSED_PARAM (SwVlanId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanRedHwUpdateDBForDefaultVlanId              */
/*                                                                           */
/*    Description         : This function updates the Np with the vlan id    */
/*                          configured as the default vlan id                */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId     - Vlan Id to be configured as default */
/*                                       vlan id                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanRedHwUpdateDBForDefaultVlanId (tVlanId VlanId)
{
    UNUSED_PARAM (VlanId);
    return FNP_SUCCESS;
}
#endif /* L2RED_WANTED */

/*****************************************************************************/
/*    Function Name       : FsVlanHwAddPortMacVlanEntry                      */
/*                                                                           */
/*    Description         : This function is responsible for adding a new    */
/*                          mac-based vlan entry corresponding to a port in  */
/*                          the hardware table.                              */
/*                                                                           */
/*    Input(s)            : MacAddr          - Mac Address of the Entry to   */
/*                                             be added                      */
/*                          u4VlanId         - VlanId of the entry to be     */
/*                                             added                         */
/*                          u4Port           - Interface Index of the entry  */
/*                                             to be added                   */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwAddPortMacVlanEntry (UINT4 u4Port, tMacAddr MacAddr, UINT4 u4VlanId)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4VlanId);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwDeletePortMacVlanEntry                   */
/*                                                                           */
/*    Description         : This function is responsible for deleting an     */
/*                          existing mac-based vlan entry corresponding to a */
/*                          port in the hardware table.                      */
/*                                                                           */
/*    Input(s)            : MacAddr          - Mac Address of the Entry to   */
/*                                             be deleted                    */
/*                          u4Port           - Interface Index of the entry  */
/*                                             to be deleted                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwDeletePortMacVlanEntry (UINT4 u4Port, tMacAddr MacAddr)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (MacAddr);
    return FNP_SUCCESS;

}

/*****************************************************************************/
/*    Function Name       : FsVlanHwAddPortSubnetVlanEntry                   */
/*                                                                           */
/*    Description         : This function adds the Subnet based VLAN entry   */
/*                          in the H/W table.                                */
/*                                                                           */
/*    Input(s)            : u4IfIndex   - Port number                        */
/*                          SubnetAddr  - Subnet Address.                    */
/*                          VlanId      - Vlan Identifier can take <1-4094>  */
/*                          bArpOption  - FNP_TRUE/FNP_FALSE                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwAddPortSubnetVlanEntry (UINT4 u4IfIndex, UINT4 SubnetAddr,
                                tVlanId VlanId, BOOL1 bARPOption)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SubnetAddr);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (bARPOption);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwDeletePortSubnetVlanEntry                */
/*                                                                           */
/*    Description         : This function deletes the Subnet based VLAN entry*/
/*                          in the H/W table.                                */
/*                                                                           */
/*    Input(s)            : u4IfIndex   - Port number                        */
/*                          SubnetAddr  - Subnet Address                     */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwDeletePortSubnetVlanEntry (UINT4 u4IfIndex, UINT4 SubnetAddr)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SubnetAddr);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanNpHwRunMacAgeing                           */
/*                                                                           */
/*    Description         : The function initiates the "hardware FDB entries"*/
/*                          ageing process when a timer expiry event is      */
/*                          received                                         */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanNpHwRunMacAgeing (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwSetFidPortLearningStatus                     */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning status for a Port in the given Fid.         */
/*                                                                           */
/*                                                                           */
/* Input (s)          : u4Fid       - Fdb Identifier                         */
/*                      pHwPortList - Port list where MAC learning status    */
/*                      should be applied                                    */
/*                      u4Port - Port where mac lreaning status will applied */
/*                               it will be used only if pHwPortList is null.*/
/*                      u1Action - learning enable/disable                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanHwSetFidPortLearningStatus (UINT4 u4Fid, tPortList HwPortList,
                                  UINT4 u4Port, UINT1 u1Action)
{
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (HwPortList);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u1Action);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwSetProtocolTunnelStatusOnPort                */
/*                                                                           */
/* Description        : This function is called for configuring the action   */
/*                      to be taken on receiving control protocol PDUs on    */
/*                      the given port.                                      */
/*                                                                           */
/* Input(s)           : u4IfIndex   - Interface index                        */
/*                      ProtocolId  - Control Protocol for which action      */
/*                                    is being configured.                   */
/*                      u4TunnelStatus - Tunnel status                       */
/*                         VLAN_TUNNEL_PROTOCOL_PEER    - Normal             */
/*                         VLAN_TUNNEL_PROTOCOL_TUNNEL  - Encapsulate        */
/*                         VLAN_TUNNEL_PROTOCOL_DISCARD - Discard            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanHwSetProtocolTunnelStatusOnPort (UINT4 u4IfIndex,
                                       tVlanHwTunnelFilters ProtocolId,
                                       UINT4 u4TunnelStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (ProtocolId);
    UNUSED_PARAM (u4TunnelStatus);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwSetPortProtectedStatus                       */
/*                                                                           */
/* Description        : This function is called for configuring the          */
/*                      Protected/Split Horizon Status on port.              */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index.                         */
/*                      i4ProtectedStatus - Port's protected status          */
/*                          VLAN_SNMP_TRUE - protected status enabled.       */
/*                          VLAN_SNMP_FALSE - protected status disabled.     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanHwSetPortProtectedStatus (UINT4 u4IfIndex, INT4 i4ProtectedStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4ProtectedStatus);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsVlanHwAddStaticUcastEntryEx                    */
/*                                                                           */
/*    Description         : This function adds the egress ports  into 
 *                          hardware  Static Unicast table .                 */
/*                                                                           */
/*    Input(s)            : u4Fid            - Fid                           */
/*                          pMacAddr         - Pointer to the Mac Address.   */
/*                          u2RcvPort        - Received port                 */
/*                          AllowedToGoPorts - PortList                      */
/*                          u1Status         -  Status of entry takes values */
/*                          like  permanent, deleteOnReset and               */
/*                          ConnectionId     - BackBone MacAddress           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS                                       */
/*                         FNP_FAILURE                                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
FsVlanHwAddStaticUcastEntryEx (UINT4 u4FdbId,
                               tMacAddr MacAddr,
                               UINT4 u2RcvPort, tPortList AllowedToGoPorts,
                               UINT1 u1Status, tMacAddr ConnectionId)
{
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (pAllowedToGoPorts);
    UNUSED_PARAM (pu1Status);
    UNUSED_PARAM (ConnectionId);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsVlanHwGetStaticUcastEntryEx                    */
/*                                                                           */
/*    Description         : This function gets the static unicast entry      */
/*                          for the given FDB Id and mac address.            */
/*                                                                           */
/*    Input(s)            : u4Fid            - Fid                           */
/*                          pMacAddr         - Pointer to the Mac Address.   */
/*                          u4Port           - Received Interface Index      */
/*                                                                           */
/*    Output(s)           : AllowedToGoPortBmp - PortList                    */
/*                          pu1Status        -  Status of entry takes values */
/*                          like  permanent, deleteOnReset and               */
/*                           deleteOnTimeout        
 *                          ConnectionId - Back Bone Mac address             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsVlanHwGetStaticUcastEntryEx (UINT4 u4Fid, tMacAddr MacAddr,
                               UINT4 u4Port, tPortList AllowedToGoPortBmp,
                               UINT1 *pu1Status, tMacAddr ConnectionId)
{
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (pAllowedToGoPorts);
    UNUSED_PARAM (pu1Status);
    UNUSED_PARAM (ConnectionId);

    return FNP_SUCCESS;
}
#endif
#endif
