/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldnpx.c,v 1.2 2007/02/01 14:59:31 iss Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/
#ifndef _MLDNPX_C_
#define _MLDNPX_C_

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "npapi.h"
#include "npmld.h"

/*****************************************************************************/
/* Function Name      : FsMldMbsmHwEnableMld                                 */
/*                                                                           */
/* Description        : This function enables MLD snooping feature in the    */
/*                      hardware. Filters are added for receiving MLD pkts   */
/*                                                                           */
/* Input(s)           : pSlotInfo - Pointer to Slot information structure    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMldMbsmHwEnableMld (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

#endif
