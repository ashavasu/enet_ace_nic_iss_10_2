/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipnp.c,v 1.41 2016/10/06 12:19:29 siva Exp $
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/
#include "lr.h"
#include "cfa.h"
#include "npip.h"

#include "adap_types.h"
#include "adap_logger.h"
#include "EnetHal_L3_Api.h"

UINT4               gu4DhcpSrvRlyFilterCnt = 0;
/******************************************************************/
/*  Function Name             : FsNpIpInit                        */
/*  Description               : This is the first function to be  */
/*                              called for initialising           */
/*                              any IP specific operation         */
/*  Input(s)                  : None.                             */
/*  Output(s)                 : None                              */
/*  Global Variables Referred : None                              */
/*  Global variables Modified : None                              */
/*  Exceptions                : None                              */
/*  Use of Recursion          : None                              */
/*  Returns                   : VOID                              */
/******************************************************************/

VOID
FsNpIpInit ()
{
#ifdef USER_HW_API
    EnetHal_FsNpIpInit();
#endif //USER_HW_API
    return;
}

/*****************************************************************/
/*  Function Name             : FsNpOspfInit                     */
/*  Description               : This fucntion does the initializa*/
/*                              -ation in hardware to receive    */
/*                              OSPF packets                     */
/*  Input(s)                  : None.                            */
/*  Output(s)                 : None                             */
/*  Global Variables Referred : None                             */
/*  Global variables Modified : None                             */
/*  Exceptions                : None                             */
/*  Use of Recursion          : None                             */
/*  Returns                   : VOID                             */
/*****************************************************************/
INT1
FsNpOspfInit ()
{
	INT1 ret=FNP_SUCCESS;
#ifdef USER_HW_API
    ret = EnetHal_FsNpOspfInit();
#endif //USER_HW_API
    return ret;
}

/*****************************************************************/
/*  Function Name             : FsNpOspfDeInit                   */
/*  Description               : This fucntion does the de-init   */
/*                              in hardware                      */
/*  Input(s)                  : None.                            */
/*  Output(s)                 : None                             */
/*  Global Variables Referred : None                             */
/*  Global variables Modified : None                             */
/*  Exceptions                : None                             */
/*  Use of Recursion          : None                             */
/*  Returns                   : VOID                             */
/*****************************************************************/
VOID
FsNpOspfDeInit ()
{
#ifdef USER_HW_API
#endif //USER_HW_API
    return;
}

/*****************************************************************/
/*  Function Name             : FsNpDhcpSrvInit                  */
/*  Description               : This fucntion does the initializa*/
/*                              -ation in hardware to receive    */
/*                              DHCP server packets              */
/*  Input(s)                  : None.                            */
/*  Output(s)                 : None                             */
/*  Global Variables Referred : None                             */
/*  Global variables Modified : None                             */
/*  Exceptions                : None                             */
/*  Use of Recursion          : None                             */
/*  Returns                   : VOID                             */
/*****************************************************************/
INT1
FsNpDhcpSrvInit ()
{
    /* DHCP server and relay uses the same filters,
     * so ensure that the filter is installed only once
     */
    if (gu4DhcpSrvRlyFilterCnt == 0)
    {
        /* Register DHCP server with hardware */
    }

    gu4DhcpSrvRlyFilterCnt++;
#ifdef USER_HW_API
    EnetHal_FsNpDhcpSrvInit();
#endif //USER_HW_API
    return FNP_SUCCESS;
}

/*****************************************************************/
/*  Function Name             : FsNpDhcpSrvDeInit                */
/*  Description               : This fucntion does the de-init   */
/*                              in hardware                      */
/*  Input(s)                  : None.                            */
/*  Output(s)                 : None                             */
/*  Global Variables Referred : None                             */
/*  Global variables Modified : None                             */
/*  Exceptions                : None                             */
/*  Use of Recursion          : None                             */
/*  Returns                   : VOID                             */
/*****************************************************************/
VOID
FsNpDhcpSrvDeInit ()
{
    gu4DhcpSrvRlyFilterCnt--;

    /* DHCP server and relay uses the same filters,
     * so ensure that the filter is deleted only once
     */
    if (gu4DhcpSrvRlyFilterCnt == 0)
    {
        /* De-register DHCP server from hardware */
    }
#ifdef USER_HW_API
    EnetHal_CtrlProto_Device_DhcpSrv_DeInit();
#endif //USER_HW_API
    return;
}

/*****************************************************************/
/*  Function Name             : FsNpDhcpRlyInit                  */
/*  Description               : This fucntion does the initializa*/
/*                              -ation in hardware to receive    */
/*                              DHCP relay packets               */
/*  Input(s)                  : None.                            */
/*  Output(s)                 : None                             */
/*  Global Variables Referred : None                             */
/*  Global variables Modified : None                             */
/*  Exceptions                : None                             */
/*  Use of Recursion          : None                             */
/*  Returns                   : VOID                             */
/*****************************************************************/
INT1
FsNpDhcpRlyInit ()
{
    /* DHCP server and relay uses the same filters,
     * so ensure that the filter is installed only once
     */
    if (gu4DhcpSrvRlyFilterCnt == 0)
    {
        /* Register DHCP relay with hardware */
    }

    gu4DhcpSrvRlyFilterCnt++;

#ifdef USER_HW_API
    EnetHal_FsNpDhcpRlyInit ();
#endif //USER_HW_API

    return FNP_SUCCESS;
}

/*****************************************************************/
/*  Function Name             : FsNpDhcpRlyDeInit                */
/*  Description               : This fucntion does the de-init   */
/*                              in hardware                      */
/*  Input(s)                  : None.                            */
/*  Output(s)                 : None                             */
/*  Global Variables Referred : None                             */
/*  Global variables Modified : None                             */
/*  Exceptions                : None                             */
/*  Use of Recursion          : None                             */
/*  Returns                   : VOID                             */
/*****************************************************************/
VOID
FsNpDhcpRlyDeInit ()
{
    gu4DhcpSrvRlyFilterCnt--;

    /* DHCP server and relay uses the same filters,
     * so ensure that the filter is deleted only once
     */
    if (gu4DhcpSrvRlyFilterCnt == 0)
    {
        /* De-register DHCP relay from hardware */
    }

#ifdef USER_HW_API
    EnetHal_CtrlProto_Device_DhcpRly_DeInit();
#endif //USER_HW_API
    return;
}

/*****************************************************************/
/*  Function Name             : FsNpRipInit                      */
/*  Description               : This function initializes filter */
/*                              to receive rip packets           */
/*  Input(s)                  : None.                            */
/*  Output(s)                 : None                             */
/*  Global Variables Referred : None                             */
/*  Global variables Modified : None                             */
/*  Exceptions                : None                             */
/*  Use of Recursion          : None                             */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE          */
/*****************************************************************/
INT1
FsNpRipInit ()
{
#ifdef USER_HW_API
    EnetHal_FsNpRipInit();
#endif //USER_HW_API
    return FNP_SUCCESS;
}

/*****************************************************************/
/*  Function Name             : FsNpRipDeInit                   */
/*  Description               : This function removes the filter */
/*                              to skip receiving rip packets    */
/*  Input(s)                  : None.                            */
/*  Output(s)                 : None                             */
/*  Global Variables Referred : None                             */
/*  Global variables Modified : None                             */
/*  Exceptions                : None                             */
/*  Use of Recursion          : None                             */
/*  Returns                   : VOID                             */
/*****************************************************************/
VOID
FsNpRipDeInit ()
{
#ifdef USER_HW_API
#endif //USER_HW_API
    return;
}

/*****************************************************************/
/*  Function Name             : FsNpIpv4CreateIpInterface        */
/*  Description               : Create a local IP Interface for  */
/*                              the specified prefix.            */
/*  Input(s)                  : u4VrId   - The virtual router    */
/*                                         identifier.           */
/*                              pu1IfName -Interface name.       */
/*                              u4IfIndex -Interface Index       */
/*                              u4IpAddr  -Ip address to be      */
/*                                         configured for the    */
/*                                         interface             */
/*                              u4IpSubnetMask -Network Mask     */
/*                                              for this         */
/*                                              interface        */
/*                              u2VlanId   - Vlan Identifier     */
/*                              au1MacAddr - Mac address of this */
/*                                           interface           */
/*  Output(s)                 : None                             */
/*  Global Variables Referred : None                             */
/*  Global variables Modified : None                             */
/*  Exceptions                : None                             */
/*  Use of Recursion          : None                             */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE          */
/*****************************************************************/

UINT4
FsNpIpv4CreateIpInterface (UINT4 u4VrId, UINT1 *pu1IfName,
                           UINT4 u4CfaIfIndex,
                           UINT4 u4IpAddr, UINT4 u4IpSubNetMask,
                           UINT2 u2VlanId, UINT1 *au1MacAddr)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pu1IfName);
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (u4IpSubNetMask);
    UNUSED_PARAM (u4CfaIfIndex);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (au1MacAddr);

    UINT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
    ret = EnetHal_FsNpIpv4CreateIpInterface (u4VrId,pu1IfName,u4CfaIfIndex,u4IpAddr,u4IpSubNetMask,u2VlanId,au1MacAddr);
#endif //USER_HW_API

    return ret;
}

/*****************************************************************/
/*  Function Name             : FsNpIpv4ModifyIpInterface        */
/*  Description               : Modify the specified IP          */
/*                              interface. This function will be */
/*                              invoked whenever any IP          */
/*                              parameters of the interface is   */
/*                              changed.                         */
/*  Input(s)                  : u4VrId   - The virtual router    */
/*                                         identifier.           */
/*                              pu1IfName -Interface name.       */
/*                              u4IfIndex -Interface Index       */
/*                              u4IpAddr  -Ip address to be      */
/*                                         configured for the    */
/*                                         interface             */
/*                              u4IpSubnetMask -Network Mask     */
/*                                              for this         */
/*                                              interface        */
/*                              u2VlanId   - Vlan Identifier     */
/*                              au1MacAddr - Mac address of this */
/*                                           interface           */
/*  Output(s)                 : None                             */
/*  Global Variables Referred : None                             */
/*  Global variables Modified : None                             */
/*  Exceptions                : None                             */
/*  Use of Recursion          : None                             */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE          */
/*****************************************************************/

UINT4
FsNpIpv4ModifyIpInterface (UINT4 u4VrId, UINT1 *pu1IfName,
                           UINT4 u4CfaIfIndex,
                           UINT4 u4IpAddr, UINT4 u4IpSubNetMask,
                           UINT2 u2VlanId, UINT1 *au1MacAddr)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pu1IfName);
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (u4IpSubNetMask);
    UNUSED_PARAM (u4CfaIfIndex);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (au1MacAddr);

    UINT4 ret=FNP_SUCCESS;

#ifdef USER_HW_API
    ret = EnetHal_FsNpIpv4ModifyIpInterface (u4VrId,pu1IfName,u4CfaIfIndex,u4IpAddr,u4IpSubNetMask,u2VlanId, au1MacAddr);
#endif //USER_HW_API

    return ret;
}

/******************************************************************************
 *  Function Name             : FsNpIpv4DeleteIpInterface
 *  Description               : Delete a local IP Interface for
 *                              the specified prefix.
 *  Input(s)                  : u4VrId   - The virtual router
 *                                         identifier.
 *                              pu1IfName - Interface name.
 *                              u4IfIndex -Interface Index
 *                              u2VlanId  - Vlan Identifier
 *  Output(s)                 : None
 *  Global Variables Referred : None
 *  Global variables Modified : None
 *  Exceptions                : None
 *  Use of Recursion          : None
 *  Returns                   : FNP_SUCCESS/FNP_FAILURE
 *****************************************************************************/
UINT4
FsNpIpv4DeleteIpInterface (UINT4 u4VrId, UINT1 *pu1IfName,
                           UINT4 u4IfIndex, UINT2 u2VlanId)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pu1IfName);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2VlanId);
    UINT4 ret=FNP_SUCCESS;

#ifdef USER_HW_API
    ret = EnetHal_FsNpIpv4DeleteIpInterface (u4VrId,pu1IfName,u4IfIndex,u2VlanId);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************/
/*  Function Name             : FsNpIpv4UpdateIpInterfaceStatus  */
/*  Description               : Modify the specified IP intf.    */
/*                              oper. status. This function will */
/*                              invoked whenever the oper status */
/*                              of the IP interface becomes up/  */
/*                              down.                            */
/*                                                               */
/*  Input(s)                  : u4VrId   - The virtual router    */
/*                                         identifier.           */
/*                              pu1IfName -Interface name.       */
/*                              u4IfIndex -Interface Index       */
/*                              u4IpAddr  -Ip address to be      */
/*                                         configured for the    */
/*                                         interface             */
/*                              u4IpSubnetMask -Network Mask     */
/*                                              for this         */
/*                                              interface        */
/*                              u2VlanId   - Vlan Identifier     */
/*                              au1MacAddr - Mac address of this */
/*                                           interface           */
/*                              u4Status - CFA_IF_UP/CFA_IF_DOWN */
/*  Output(s)                 : None                             */
/*  Global Variables Referred : None                             */
/*  Global variables Modified : None                             */
/*  Exceptions                : None                             */
/*  Use of Recursion          : None                             */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE          */
/*****************************************************************/
UINT4
FsNpIpv4UpdateIpInterfaceStatus (UINT4 u4VrId, UINT1 *pu1IfName,
                                 UINT4 u4CfaIfIndex, UINT4 u4IpAddr,
                                 UINT4 u4IpSubNetMask, UINT2 u2VlanId,
                                 UINT1 *au1MacAddr, UINT4 u4Status)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pu1IfName);
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (u4IpSubNetMask);
    UNUSED_PARAM (u4CfaIfIndex);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (au1MacAddr);
    UNUSED_PARAM (u4Status);

    UINT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
    ret = EnetHal_FsNpIpv4ModifyIpInterface (u4VrId,pu1IfName,u4CfaIfIndex,u4IpAddr,u4IpSubNetMask,u2VlanId, au1MacAddr);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*   Function Name             : FsNpIpv4SetForwardingStatus                 */
/*   Description               : Enables or disables IP forwarding in        */
/*                               hardware.                                   */
/*   Input(s)                  : u4VrId - Virtual router identifier.         */
/*                               u1Status - FNP_FORW_ENABLE/FNP_FORW_DISABLE */
/*   Output(s)                 : None.                                       */
/*   Global Variables Referred : None                                        */
/*   Global variables Modified : None                                        */
/*   Exceptions                : None                                        */
/*   Use of Recursion          : None                                        */
/*   Returns                   : FNP_SUCCESS / FNP_FAILURE                   */
/*****************************************************************************/
INT4
FsNpIpv4SetForwardingStatus (UINT4 u4VrId, UINT1 u1Status)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u1Status);
#ifdef USER_HW_API
#endif //USER_HW_API
    return FNP_SUCCESS;
}

/***************************************************************************/
/*   Function Name             : FsNpIpv4ClearRouteTable                   */
/*   Description               : Deletes all entries from H/W DEF IP Table */
/*   Input(s)                  : None.                                     */
/*   Output(s)                 : None.                                     */
/*   Global Variables Referred : None                                      */
/*   Global variables Modified : None                                      */
/*   Exceptions                : None                                      */
/*   Use of Recursion          : None                                      */
/*   Returns                   : FNP_SUCCESS - If supported                  */
/*                               FNP_NOT_SUPPORTED - If driver API is not    */
/*                                                   supported.              */
/***************************************************************************/

INT4
FsNpIpv4ClearRouteTable ()
{
#ifdef USER_HW_API
#endif //USER_HW_API
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*   Function Name             : FsNpIpv4ClearArpTable                       */
/*   Description               : Deletes all entries from H/W L3 IP Table    */
/*   Input(s)                  : NONE.                                       */
/*   Output(s)                 : None.                                       */
/*   Global Variables Referred : None                                        */
/*   Global variables Modified : None                                        */
/*   Exceptions                : None                                        */
/*   Use of Recursion          : None                                        */
/*   Returns                   : FNP_SUCCESS - If supported                  */
/*                               FNP_NOT_SUPPORTED - If driver API is not    */
/*                                                   supported.              */
/*****************************************************************************/

INT4
FsNpIpv4ClearArpTable (VOID)
{
	INT4 ret = FNP_SUCCESS;
#ifdef USER_HW_API
   EnetHal_FsNpIpv4ClearArpTable();
#endif //USER_HW_API
    return ret;
}

/***********************************************************************/
/*  Function Name             : FsNpIpv4UcDelRoute                     */
/*  Description               : This function deletes a route          */
/*                              entry from ip unicast route table      */
/*                              of Network Processort(Fast path)       */
/*  Input(s)                  : u4VrId   - The virtual router          */
/*                                         identifier.                 */
/*                              u4IpDestAddr - Destination IP          */
/*                                             Address                 */
/*                              u4IpSubNetMask-Destination             */
/*                                             Subnet Mask             */
/*                              routeEntry    -Unicast Route           */
/*                                             Entry containing        */
/*                                             Gateway IP              */
/*                                             Address.                */
/*  Output(s)                 : None                                   */
/*  Global Variables Referred : None                                   */
/*  Global variables Modified : None                                   */
/*  Exceptions                : None                                   */
/*  Use of Recursion          : None                                   */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE                */
/***************************************************************/

UINT4
FsNpIpv4UcDelRoute (UINT4 u4VrId, UINT4 u4IpDestAddr,
                    UINT4 u4IpSubNetMask, tFsNpNextHopInfo routeEntry,
                    int *pi4FreeDefIpB4Del)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4IpDestAddr);
    UNUSED_PARAM (u4IpSubNetMask);
    UNUSED_PARAM (routeEntry);
    UNUSED_PARAM (*pi4FreeDefIpB4Del);
    UINT4 ret=FNP_SUCCESS;


#ifdef USER_HW_API
    tEnetHal_NpNextHopInfo enetrouteEntry;

    enetrouteEntry.u2VlanId = routeEntry.u2VlanId;
    enetrouteEntry.u4NextHopGt = routeEntry.u4NextHopGt;
    enetrouteEntry.u4IfIndex = routeEntry.u4IfIndex;

    ret = EnetHal_FsNpIpv4UcDelRoute (u4VrId,u4IpDestAddr,u4IpSubNetMask,enetrouteEntry,pi4FreeDefIpB4Del);
#endif //USER_HW_API
    return ret;
}

/******************************************************************************/
/*   Function Name             : FsNpIpv4ArpAdd                               */
/*   Description               : Adds the Specified IP address and MAC address*/
/*                               to the H/W L3 IP Table Returns Full if no    */
/*                               free entry exist in the L3 Ip Table          */
/*   Input(s)                  : u4VrId - The virtual router identifier      */
/*                               u2VlanId  - L2 Vlan Id                       */
/*                               u4IfIndex - CFA L3 Vlan If Index             */
/*                               u4IpAddr  - IP address for which ARP is added*/
/*                               pMacAddr  -  MAC Address                     */
/*                               pu1IfName - Interface name                   */
/*                               i1State   - ARP_STATIC / ARP_DYNAMIC         */
/*   Output(s)                 : pbu1TblFull - Hw Table Full/not              */
/*                                            (FNP_FALSE/FNP_TRUE)            */
/*   Global Variables Referred : None                                         */
/*   Global variables Modified : None                                         */
/*   Exceptions                : None                                         */
/*   Use of Recursion          : None                                         */
/*   Returns                   : FNP_SUCCESS/FNP_FAILURE                      */
/******************************************************************************/
UINT4
FsNpIpv4ArpAdd (tNpVlanId u2VlanId, UINT4 u4IfIndex,
                UINT4 u4IpAddr, UINT1 *pMacAddr, UINT1 *pu1IfName,
                INT1 i1State, UINT4 *pu4TblFull)
{
    UINT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
    tEnetHal_MacAddr mac_addr;
    adap_mac_from_arr(&mac_addr, pMacAddr);
    ret = EnetHal_FsNpIpv4ArpAdd (u2VlanId,u4IfIndex,u4IpAddr,&mac_addr,pu1IfName,i1State,pu4TblFull);
    (*pu4TblFull) = FNP_FALSE;
#else
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (*pMacAddr);
    UNUSED_PARAM (*pu4TblFull);
    UNUSED_PARAM (i1State);
    UNUSED_PARAM (pu1IfName);
#endif //USER_HW_API
    return ret;
}

/******************************************************************************/
/*   Function Name             : FsNpIpv4ArpModify                            */
/*   Description               : Modifies the ARP entry corresponding to the  */
/*                               given IP address                             */
/*   Input(s)                  : u4VrId - The virtual router identifier      */
/*                               u2VlanId  - L2 Vlan Id                       */
/*                               u4IfIndex - CFA L3 Vlan If Index             */
/*                               u4IpAddr  - IP address for which ARP is added*/
/*                               pMacAddr  -  MAC Address                     */
/*                               pu1IfName - Interface name                   */
/*                               i1State   - State of the ARP entry           */
/*   Output(s)                 : pbu1TblFull - Hw Table Full/not              */
/*                                            (FNP_FALSE/FNP_TRUE)            */
/*   Global Variables Referred : None                                         */
/*   Global variables Modified : None                                         */
/*   Exceptions                : None                                         */
/*   Use of Recursion          : None                                         */
/*   Returns                   : FNP_SUCCESS/FNP_FAILURE                      */
/******************************************************************************/
UINT4
FsNpIpv4ArpModify (tNpVlanId u2VlanId, UINT4 u4IfIndex, UINT4 u4PhyIfIndex,
                   UINT4 u4IpAddr, UINT1 *pMacAddr, UINT1 *pu1IfName,
                   INT1 i1State)
{
    UINT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
    tEnetHal_MacAddr mac_addr;
    adap_mac_from_arr(&mac_addr, pMacAddr);
    ret = EnetHal_FsNpIpv4ArpModify(u2VlanId,u4IfIndex, u4PhyIfIndex,u4IpAddr,&mac_addr,pu1IfName,i1State);
#else
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PhyIfIndex);
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (*pMacAddr);
    UNUSED_PARAM (i1State);
    UNUSED_PARAM (pu1IfName);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*  Function Name             :  FsNpIpv4ArpDel                              */
/*  Description               :  Deletes ARP entry from the Fast Path Table  */
/*  Input(s)                  :  u4VrId - The virtual router identifier      */
/*                               u4IpAddr   -- IpAddress                     */
/*                               pu1IfName  -- Pointer to interface name.    */
/*                               i1State    -- ARP_STATIC / ARP_DYNAMIC      */
/*  Output(s)                 :  None                                        */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions                :  None                                        */
/*  Use of Recursion          :  None                                        */
/*  Returns                   :  FNP_FAILURE(0) on failure                   */
/*                               FNP_SUCCESS(1) on success                   */
/*****************************************************************************/
UINT4
FsNpIpv4ArpDel (UINT4 u4IpAddr, UINT1 *pu1IfName, INT1 i1State)
{
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (i1State);
    UNUSED_PARAM (pu1IfName);
    UINT4 ret = FNP_SUCCESS;
#ifdef USER_HW_API
    ret = EnetHal_FsNpIpv4ArpDel (u4IpAddr,pu1IfName,i1State);
#endif //USER_HW_API
    return ret;
}

/******************************************************************************/
/*   Function Name             : FsNpIpv4CheckHitOnArpEntry                   */
/*   Description               : Checks the HIT bit information of the ARP    */
/*                               ARP Entry                                    */
/*   Input(s)                  : u4VrId - The virtual router identifier      */
/*                               u4IpAddress - IP address                     */
/*                               u1NextHopFlag - Indicates the IP address is  */
/*                                               nexthop address or not       */
/*   Output(s)                 : Hit Status of L3 Entry                       */
/*   Global Variables Referred : None                                         */
/*   Global variables Modified : None                                         */
/*   Exceptions                : None                                         */
/*   Use of Recursion          : None                                         */
/*   Returns                   : FNP_TRUE/FNP_FALSE                           */
/******************************************************************************/
UINT1
FsNpIpv4CheckHitOnArpEntry (UINT4 u4IpAddress, UINT1 u1NextHopFlag)
{
    UNUSED_PARAM (u4IpAddress);
    UNUSED_PARAM (u1NextHopFlag);
    UINT1 ret = FNP_FALSE;
#ifdef USER_HW_API
    ret = EnetHal_FsNpIpv4CheckHitOnArpEntry (u4IpAddress,u1NextHopFlag);
#endif //USER_HW_API
    return ret;
}

/*****************************************************************************/
/*   Function Name             : FsNpIpv4SyncVlanAndL3Info                   */
/*   Description               : Syncronises VLAN and L3 Info for Untagging  */
/*   Input(s)                  : None.                                       */
/*   Output(s)                 : None.                                       */
/*   Global Variables Referred : None                                        */
/*   Global variables Modified : None                                        */
/*   Exceptions                : None                                        */
/*   Use of Recursion          : None                                        */
/*   Returns                   : VOID                                        */
/*****************************************************************************/

VOID
FsNpIpv4SyncVlanAndL3Info ()
{
#ifdef USER_HW_API
#endif //USER_HW_API
    return;
}

/***********************************************************************/
/*  Function Name             : NpIpv4UcAddRoute                       */
/*  Description               : This function adds an route            */
/*                              entry to ip unicast route table        */
/*                              of Network Processort(Fast path)       */
/*  Input(s)                  : u4VrId   - The virtual router          */
/*                                         identifier.                 */
/*                              u4IpDestAddr - Destination IP          */
/*                                             Address                 */
/*                              u4IpSubNetMask-Destination             */
/*                                             Subnet Mask             */
/*                              routeEntry    -Unicast Route           */
/*                                             Entry containing        */
/*                                             Gateway IP              */
/*                                             Address.                */
/*                              u4RouteType   -Unicast Route           */
/*                                             Type - static/dy        */
/*  Output(s)                 : None                                   */
/*  Global Variables Referred : None                                   */
/*  Global variables Modified : None                                   */
/*  Exceptions                : None                                   */
/*  Use of Recursion          : None                                   */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE                */
/***********************************************************************/
UINT4
FsNpIpv4UcAddRoute (UINT4 u4VrId, UINT4 u4IpDestAddr, UINT4 u4IpSubNetMask,
                    tFsNpNextHopInfo * pRouteEntry, UINT1 *pbu1TblFull)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4IpDestAddr);
    UNUSED_PARAM (u4IpSubNetMask);
    UNUSED_PARAM (*pRouteEntry);
    UNUSED_PARAM (*pbu1TblFull);

    UINT4 ret = FNP_SUCCESS;

#ifdef USER_HW_API
    tEnetHal_NpNextHopInfo tRouteEntry;

    tRouteEntry.u2VlanId = pRouteEntry->u2VlanId;
    tRouteEntry.u4IfIndex = pRouteEntry->u4IfIndex;
    tRouteEntry.u4NextHopGt = pRouteEntry->u4NextHopGt;

    ret = EnetHal_FsNpIpv4UcAddRoute (u4VrId,u4IpDestAddr, u4IpSubNetMask,&tRouteEntry,pbu1TblFull);

    (*pbu1TblFull) = FNP_FALSE;
#endif //USER_HW_API
    return ret;
}

#ifdef VRRP_WANTED
/***************************************************************/
/*  Function Name             : FsNpIpv4CreateVrrpInterface    */
/*  Description               : Create Vrrp Interface          */
/*  Input(s)                  : u4IfIndex -Interface Index     */
/*                              u4IpAddr  - Master Ip address  */
/*                              u1MacAddr - Virtual Mac Address*/
/*  Output(s)                 : None                           */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions                : None                           */
/*  Use of Recursion          : None                           */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE        */
/***************************************************************/

UINT4
FsNpIpv4VrrpIntfCreateWr (tNpVlanId u2VlanId, UINT4 u4IfIndex,
                          UINT4 u4IpAddr, UINT1 *au1MacAddr)
{
    UNUSED_PARAM (u4IfIndex);
    return (FsNpIpv4CreateVrrpInterface (u2VlanId, u4IpAddr, au1MacAddr));
}

UINT4
FsNpIpv4CreateVrrpInterface (tNpVlanId u2VlanId, UINT4 u4IpAddr,
                             UINT1 *au1MacAddr)
{
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (*au1MacAddr);
    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name             : FsNpIpv4DeleteVrrpInterface    */
/*  Description               : Deletes Vrrp Interface         */
/*  Input(s)                  : u4IfIndex -Interface Index     */
/*                              u4IpAddr  - Master Ip address  */
/*                              u1MacAddr - Virtual Mac Address*/
/*  Output(s)                 : None                           */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions                : None                           */
/*  Use of Recursion          : None                           */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE        */
/***************************************************************/

UINT4
FsNpIpv4VrrpIntfDeleteWr (tNpVlanId u2VlanId, UINT4 u4IfIndex,
                          UINT4 u4IpAddr, UINT1 *au1MacAddr)
{
    UNUSED_PARAM (u4IfIndex);
    return (FsNpIpv4DeleteVrrpInterface (u2VlanId, u4IpAddr, au1MacAddr));
}

UINT4
FsNpIpv4DeleteVrrpInterface (tNpVlanId u2VlanId, UINT4 u4IpAddr,
                             UINT1 *au1MacAddr)
{
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (*au1MacAddr);

    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name             : FsNpIpv4GetVrrpInterface       */
/*  Description               : Checks whether vrrp instance   */
/*                              is installed in the interface  */
/*  Input(s)                  : i4IfIndex -Interface Index     */
/*                              i4VrId - VRRP instance Id      */
/*  Output(s)                 : None                           */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions                : None                           */
/*  Use of Recursion          : None                           */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE        */
/***************************************************************/

UINT4
FsNpIpv4GetVrrpInterface (INT4 i4IfIndex, INT4 i4VrId)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4VrId);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsNpv4VrrpInstallFilter                          */
/*                                                                          */
/*    Description        : This function is used to install the filters     */
/*                         in the hardware.                                 */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
FsNpIpv4VrrpInstallFilter (VOID)
{
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsNpv4VrrpRemoveFilter                           */
/*                                                                          */
/*    Description        : This function is used to remove the filters      */
/*                         in the hardware.                                 */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
FsNpIpv4VrrpRemoveFilter (VOID)
{
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsNpVrrpHwProgram                                */
/*                                                                          */
/*    Description        : This function is common to create,delete or Get  */
/*                         Vrrp interface                                   */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/

INT4
FsNpVrrpHwProgram (UINT1 u1NpAction, tVrrpNwIntf * pVrrpNwIntf)
{
    UNUSED_PARAM (u1NpAction);
    UNUSED_PARAM (pVrrpNwIntf);
    return FNP_SUCCESS;
}

#endif /* VRRP_WANTED */

#ifdef ISIS_WANTED
/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsNpIsisHwProgram                                */
/*                                                                          */
/*    Description        : This function is used to Enable or Disable ISIS  */
/*                         in Hardware.                                     */
/*                                                                          */
/*    Input(s)           : u1Status (Enable / Disable) for ISIS.            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/

UINT1
FsNpIsisHwProgram (UINT1 u1Status)
{
    UNUSED_PARAM (u1Status);

    return FNP_SUCCESS;
}

#endif /* ISIS_WANTED */

/******************************************************************************/
/*  Function Name             : FsNpIpv4IsRtPresentInFastPath                 */
/*  Input(s)                  :u4DestAddr - Route Entries destination Address */
/*                             u4Mask - Route Entries Mask                    */
/*  Output(s)                 : None                                          */
/*  Global Variables Referred : None                                          */
/*  Global variables Modified : None                                          */
/*  Exceptions                : None                                          */
/*  Use of Recursion          : None                                          */
/*  Returns                   : FNP_FALSE/FNP_TRUE                            */
/******************************************************************************/

UINT4
FsNpIpv4IsRtPresentInFastPath (UINT4 u4DestAddr, UINT4 u4Mask)
{
    UNUSED_PARAM (u4DestAddr);
    UNUSED_PARAM (u4Mask);

#ifdef USER_HW_API
#endif //USER_HW_API
    return FNP_TRUE;
}

/*****************************************************************************/
/*  Function Name            : FsNpIpv4GetStats                              */
/*  Description               : Get the IP statistics                        */
/*  Input(s)                  : None                                         */
/*  Global Variables Referred : None                                         */
/*  Global variables Modified : None                                         */
/*  Exceptions                : None                                         */
/*  Use of Recursion          : None                                         */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE                      */
/*****************************************************************************/

UINT4
FsNpIpv4GetStats (INT4 i4StatType, UINT4 *pu4RetVal)
{
    UNUSED_PARAM (*pu4RetVal);
    if (i4StatType == NP_STAT_UNSUPPORTED)
    {
        PRINTF ("Stats not supported for %d \n", i4StatType);
        return FNP_FAILURE;
    }
    /* Call the hardware API to get the stats */

#ifdef USER_HW_API
#endif //USER_HW_API
    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name             : FsNpIpv4VrmEnableVr            */
/*  Description               : Enable/Disable the specified   */
/*                              Virtual Router in the IPv4     */
/*                              Routing engine.                */
/*  Input(s)                  : u4VrId   - The virtual router  */
/*                                         identifier.         */
/*                              u1Status - FNP_TRUE, enable the*/
/*                                         specified Virtual   */
/*                                         Router              */
/*                                         FNP_FALSE, disable  */
/*                                         the specified       */
/*                                         Virtual Router      */
/*  Output(s)                 : None                           */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions                : None                           */
/*  Use of Recursion          : None                           */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE        */
/***************************************************************/

UINT4
FsNpIpv4VrmEnableVr (UINT4 u4VrId, UINT1 u1Status)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u1Status);
#ifdef USER_HW_API
#endif //USER_HW_API
    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name             : FsNpIpv4BindIfToVrId           */
/*  Description               : Bind an interface to a Virtual */
/*                              Router                         */
/*  Input(s)                  : u4VrId   - The virtual router  */
/*                                         identifier.         */
/*                              u4IfIndex -Interface Index     */
/*  Output(s)                 : None                           */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions                : None                           */
/*  Use of Recursion          : None                           */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE        */
/***************************************************************/

UINT4
FsNpIpv4BindIfToVrId (UINT4 u4IfIndex, UINT4 u4VrId)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4VrId);
#ifdef USER_HW_API
#endif //USER_HW_API
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name             :  FsNpIpv4GetNextHopInfo                      */
/*  Description               :  Finds the Data Plane Route for the params   */
/*  Input(s)                  :  u4VrId     -- Virtual Router Id             */
/*                               u4Dest     -- Destination Network           */
/*                               u4DestMask -- Destination Mask              */
/*  Output(s)                 :  bestHopInfo-- NextHop Information           */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions                :  None                                        */
/*  Use of Recursion          :  None                                        */
/*  Returns                   :  FNP_FAILURE(0) on failure                   */
/*                               FNP_SUCCESS(1) on success                   */
/*****************************************************************************/
UINT4
FsNpIpv4GetNextHopInfo (UINT4 u4VrId, UINT4 u4Dest, UINT4 u4DestMask,
                        tFsNpNextHopInfo * pNextHopInfo)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4Dest);
    UNUSED_PARAM (u4DestMask);
    UNUSED_PARAM (pNextHopInfo);
#ifdef USER_HW_API
#endif //USER_HW_API
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name             :  FsNpIpv4UcAddTrap                           */
/*  Description               :  Creates a Trap entry in the Fast Path Table */
/*  Input(s)                  :  u4VrId     -- Virtual Router Id             */
/*                               u4Dest     -- Destination Network           */
/*                               u4DestMask -- Destination Mask              */
/*                               nextHopInfo-- Next Hop Information          */
/*  Output(s)                 :  None                                        */
/*  Output(s)                 :  None                                        */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions                :  None                                        */
/*  Use of Recursion          :  None                                        */
/*  Returns                   :  FNP_FAILURE(0) on failure                   */
/*                               FNP_SUCCESS(1) on success                   */
/*****************************************************************************/
UINT4
FsNpIpv4UcAddTrap (UINT4 u4VrId, UINT4 u4Dest, UINT4 u4DestMask,
                   tFsNpNextHopInfo nextHopInfo)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4Dest);
    UNUSED_PARAM (u4DestMask);
    UNUSED_PARAM (nextHopInfo);
#ifdef USER_HW_API
#endif //USER_HW_API
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name             :  FsNpIpv4ClearFowardingTbl                   */
/*  Description               :  Clears Forwarding Table                     */
/*  Input(s)                  :  u4VrId     -- Virtual Router Id             */
/*  Output(s)                 :  None                                        */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions                :  None                                        */
/*  Use of Recursion          :  None                                        */
/*  Returns                   :  FNP_FAILURE(0) on failure                   */
/*                               FNP_SUCCESS(1) on success                   */
/*****************************************************************************/
UINT4
FsNpIpv4ClearFowardingTbl (UINT4 u4VrId)
{
    UNUSED_PARAM (u4VrId);
#ifdef USER_HW_API
#endif //USER_HW_API
    return FNP_SUCCESS;
}

/******************************************************************************/
/*   Function Name             :  FsNpIpv4GetSrcMovedIpAddr                   */
/*   Description               :  Get the IP address for which source is      */
/*                                moved                                       */
/*   Input(s)                  :  None                                        */
/*   Output(s)                 : Source Moved IP address                      */
/*   Global Variables Referred : None                                         */
/*   Global variables Modified : None                                         */
/*   Exceptions                : None                                         */
/*   Use of Recursion          : None                                         */
/*   Returns                   : FNP_SUCCESS/FNP_FAILURE                      */
/******************************************************************************/
INT4
FsNpIpv4GetSrcMovedIpAddr (UINT4 *pu4IpAddress)
{
    UNUSED_PARAM (pu4IpAddress);
#ifdef USER_HW_API
#endif //USER_HW_API
    return FNP_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name   : FsNpIpv4MapVlansToIpInterface
 *
 *    Description     : This function will add/delete mapping between VLAN 
 *                      interface index to VLAN in H/W
 *    Output(s)       : None                                                 
 *
 *    Returns         : FNP_SUCCESS or FNP_FAILURE
 *
 *****************************************************************************/

INT4
FsNpIpv4MapVlansToIpInterface (tNpIpVlanMappingInfo * pPvlanMappingInfo)
{
    UNUSED_PARAM (pPvlanMappingInfo);
#ifdef USER_HW_API
#endif //USER_HW_API
    return FNP_SUCCESS;
}

#ifdef NAT_WANTED
/******************************************************************************/
/*  Function Name              : FsNpNatDisableOnIntf                         */
/*  Description                : Disable NAT on a given IP interface          */
/*  Input(s)                   : i4Intf - Ip interface                        */
/*  Output(s)                  : None                                         */
/*  Global Variables Referred  : None                                         */
/*  Global variables Modified  : None                                         */
/*  Exceptions                 : None                                         */
/*  Use of Recursion           : None                                         */
/*  Returns                    : None                                         */
/******************************************************************************/
VOID
FsNpNatDisableOnIntf (INT4 i4Intf)
{
    UNUSED_PARAM (i4Intf);
    /* This Function is Obsoleted and is replaced by Function 
     * FsNatDisableOnIntf in the control plane.
     * It is retained for backward compatibility */
    return;
}

/******************************************************************************/
/*  Function Name              : FsNpNatEnableOnIntf                          */
/*  Description                : Enables Nat on the given IP Interface        */
/*  Input(s)                   : i4Intf - IP interface                        */
/*  Output(s)                  : None                                         */
/*  Global Variables Referred  : None                                         */
/*  Global variables Modified  : None                                         */
/*  Exceptions                 : None                                         */
/*  Use of Recursion           : None                                         */
/*  Returns                    : None                                         */
/******************************************************************************/

INT4
FsNpNatEnableOnIntf (INT4 i4Intf)
{
    UNUSED_PARAM (i4Intf);
    /* This Function is Obsoleted and is replaced by Function 
     * FsNatDisableOnIntf in the control plane.
     * It is retained for backward compatibility */
    return FNP_FAILURE;
}
#endif
/****************************************************************************
 Function    :  FsNpIpv4IntfStatus
 Description :  This function is used to indicate change in the operational
                status of interface associated with the Virtual Router
                (u4VrId) operational status.
 Input       :  u4VrId      - Virtual Router Identifier
                u4IfIndex   - Interface Index value
                u4IfStatus  - Interface's Operational Status. The values are:
                                    NP_IP_IF_UP - 1 
                                    NP_IP_IF_DOWN - 2
 Output      :  None.
 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpIpv4IntfStatus (UINT4 u4VrId, UINT4 u4IfStatus,
                    tFsNpIp4IntInfo * pIpIntInfo)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4IfStatus);
    UNUSED_PARAM (pIpIntInfo);

#ifdef USER_HW_API
#endif //USER_HW_API
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name             :  FsNpIpv4ArpGet                              */
/*  Description               :  Gets ARP entry from the Fast Path Table     */
/*  Input(s)                  :  u4IpAddr   -- IpAddress                     */
/*  Output(s)                 :  pHwAddr - MAC address                       */
/*                               pi1State    -- ARP_STATIC / ARP_DYNAMIC     */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions                :  None                                        */
/*  Use of Recursion          :  None                                        */
/*  Returns                   :  FNP_FAILURE(0) on failure                   */
/*                               FNP_SUCCESS(1) on success                   */
/*****************************************************************************/
INT4
FsNpIpv4ArpGet (tNpArpInput ArpNpInParam, tNpArpOutput * pArpNpOutParam)
{
    UNUSED_PARAM (ArpNpInParam);
    UNUSED_PARAM (pArpNpOutParam);

#ifdef USER_HW_API
#endif //USER_HW_API
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name             :  FsNpIpv4ArpGetNext                          */
/*  Description               :  Gets  the next ARP entry from the FPT       */
/*  Input(s)                  :  u4IpAddr   -- IpAddress                     */
/*  Output(s)                 :  pHwAddr - MAC address                       */
/*                               u4IpAddr   -- IpAddress                     */
/*                               pu4CfaIfIndex -- Pointer to IfIndex         */
/*                               pi1State    -- ARP_STATIC / ARP_DYNAMIC     */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions                :  None                                        */
/*  Use of Recursion          :  None                                        */
/*  Returns                   :  FNP_FAILURE(0) on failure                   */
/*                               FNP_SUCCESS(1) on success                   */
/*****************************************************************************/
INT4
FsNpIpv4ArpGetNext (tNpArpInput ArpNpInParam, tNpArpOutput * pArpNpOutParam)
{
    UNUSED_PARAM (ArpNpInParam);
    UNUSED_PARAM (pArpNpOutParam);

#ifdef USER_HW_API
#endif //USER_HW_API
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name             :  FsNpIpv4UcGetRoute                          */
/*  Description               :  Gets the next hop info for the given dest   */
/*  Input(s)                  :  u4DstIp   -- IpAddress                      */
/*  Output(s)                 :  u4NextHop  -- Next hop address              */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions                :  None                                        */
/*  Use of Recursion          :  None                                        */
/*  Returns                   :  FNP_FAILURE(0) on failure                   */
/*                               FNP_SUCCESS(1) on success                   */
/*****************************************************************************/
INT4
FsNpIpv4UcGetRoute (tNpRtmInput RtmNpInParam, tNpRtmOutput * pRtmNpOutParam)
{
    UNUSED_PARAM (RtmNpInParam);
    UNUSED_PARAM (pRtmNpOutParam);
#ifdef USER_HW_API
#endif //USER_HW_API
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsMbsmVrrpHwProgram                              */
/*                                                                          */
/*    Description        : This function is to indicate the Hardware with   */
/*                         VRRP Informations                                */
/*                                                                          */
/*    Input(s)           : pSlotInfo  - Mbsm Slot Inforamtion               */
/*                         VrrpNwIntf - Vrrp Network Interface              */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/

INT4
FsMbsmVrrpHwProgram (tMbsmSlotInfo * pSlotInfo, tVrrpNwIntf * pVrrpNwIntf)
{

    UNUSED_PARAM (pSlotInfo);
    UNUSED_PARAM (pVrrpNwIntf);
#ifdef USER_HW_API
#endif //USER_HW_API
    return FNP_SUCCESS;

}

/******************************************************************************
*  Function Name             : FsNpIpv4DeleteSecIpInterface
*  Description               : Deletes the secondary IPv4 Interface for
*                              the specified prefix.
*  Input(s)                  : u4VrId   -  The virtual router
*                                          identifier.
*                              pu1IfName - Interface name.
*                              u4IfIndex - Interface Index
*                              u4IpAddr  - Secondary Ip Address of the
*                                          Interface to be deleted
*                              u2VlanId  - Vlan Identifier
*  Output(s)                 : None
*  Global Variables Referred : None
*  Global variables Modified : None
*  Exceptions                : None
*  Use of Recursion          : None
*  Returns                   : FNP_SUCCESS/FNP_FAILURE
*****************************************************************************/
UINT4 FsNpIpv4DeleteSecIpInterface (UINT4 u4VrId, UINT1 *pu1IfName,
                               UINT4 u4IfIndex, UINT4 u4IpAddr, UINT2 u2VlanId)
{
     UNUSED_PARAM(u4VrId);
     UNUSED_PARAM(pu1IfName);
     UNUSED_PARAM(u4IfIndex);
     UNUSED_PARAM(u4IpAddr);
     UNUSED_PARAM(u2VlanId);

     return FNP_SUCCESS;
}
/****************************************************************************
 Function    :  FsNpIpv4CreateL3SubInterface
 Description :  This function is used to create the L3Subinterface in hw.
 Input       :  NONE
 Output      :  None.
 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
UINT4
FsNpIpv4CreateL3SubInterface (UINT4 u4VrId, UINT1 *pu1IfName,
                           UINT4 u4CfaIfIndex,
                           UINT4 u4IpAddr, UINT4 u4IpSubNetMask,
                           UINT2 u2VlanId, UINT1 *au1MacAddr,
                           UINT4 u4ParentIfIndex)

{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pu1IfName);
    UNUSED_PARAM (u4CfaIfIndex);
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (u4IpSubNetMask);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (au1MacAddr);
    UNUSED_PARAM (u4ParentIfIndex);
    return FNP_SUCCESS;
}
/****************************************************************************
 Function    :  FsNpIpv4DeleteL3SubInterface
 Description :  This function is used to create the L3Subinterface in hw.
 Input       :  NONE
 Output      :  None.
 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
UINT4
FsNpIpv4DeleteL3SubInterface (UINT4 u4CfaIfIndex)
{
    UNUSED_PARAM (u4CfaIfIndex);
    return FNP_SUCCESS;
}


#if defined (LNXIP4_WANTED)  && !defined (KERNEL_WANTED)
/*****************************************************************************/
/* Function     : FsNpOspfCreateAndDeleteFilter                          */
/*                                                                           */
/* Description  : Create KNET filter to trap OSPF packets to  the            */
/*                KNET interface                                             */
/*                                                                           */
/* Input        : u4CfaIndex - L3 interface index                            */
/*                                                                           */
/* Output       : NONE                                                       */
/*                                                                           */
/* Returns      : FNP_SUCCESS on SUCCESS                                     */
/*                FNP_FAILURE on FAILURE                                     */
/*****************************************************************************/
INT4
FsNpOspfCreateAndDeleteFilter(UINT4 u4CfaIfIndex, UINT1 u1Status)
{

    UNUSED_PARAM (u4CfaIfIndex);
    UNUSED_PARAM (u1Status);

    return FNP_SUCCESS;
}
#endif

/*****************************************************************/
/*  Function Name             : FsNpIpv4CreateL3IpInterface      */
/*  Description               : Create a local IP Interface for  */
/*                              the specified prefix.            */
/*  Input(s)                  : tNpIpInterface L3Interface       */
/*  Output(s)                 : None                             */
/*  Global Variables Referred : None                             */
/*  Global variables Modified : None                             */
/*  Exceptions                : None                             */
/*  Use of Recursion          : None                             */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE          */
/*****************************************************************/

UINT4
FsNpIpv4CreateL3IpInterface (tNpIpInterface *L3Interface)
{
    UINT4 ret = FNP_SUCCESS;
#ifdef USER_HW_API
    ret = EnetHal_FsNpIpv4CreateL3IpInterface ((tEnetHal_NpIpInterface *)L3Interface);
#endif //USER_HW_API
    UNUSED_PARAM(L3Interface);
    return ret;
}

/*****************************************************************/
/*  Function Name             : FsNpIpv4ModifyL3IpInterface      */
/*  Description               : Modify the specified IP          */
/*                              interface. This function will be */
/*                              invoked whenever any IP          */
/*                              parameters of the interface is   */
/*                              changed.                         */
/*  Input(s)                  : tNpIpInterface L3Interface       */
/*  Output(s)                 : None                             */
/*  Global Variables Referred : None                             */
/*  Global variables Modified : None                             */
/*  Exceptions                : None                             */
/*  Use of Recursion          : None                             */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE          */
/*****************************************************************/

UINT4
FsNpIpv4ModifyL3IpInterface (tNpIpInterface *L3Interface)
{
    UINT4 ret = FNP_SUCCESS;
#ifdef USER_HW_API
    ret = EnetHal_FsNpIpv4ModifyL3IpInterface ((tEnetHal_NpIpInterface *)L3Interface);
#endif //USER_HW_API
    UNUSED_PARAM(L3Interface);
    return ret;
}

/******************************************************************************
 *  Function Name             : FsNpIpv4DeleteL3IpInterface
 *  Description               : Delete a local IP Interface for
 *                              the specified prefix.
 *  Input(s)                  : u4VrId   - The virtual router    
 *  Output(s)                 : None
 *  Global Variables Referred : None
 *  Global variables Modified : None
 *  Exceptions                : None
 *  Use of Recursion          : None
 *  Returns                   : FNP_SUCCESS/FNP_FAILURE
 *****************************************************************************/
UINT4
FsNpIpv4DeleteL3IpInterface (tNpIpInterface *L3Interface)
{
    UINT4 ret = FNP_SUCCESS;
#ifdef USER_HW_API
    ret = EnetHal_FsNpIpv4DeleteIpInterface (L3Interface->u4VrId, (ADAP_Uint8 *)L3Interface->pu1IfName, 
			L3Interface->u4CfaIfIndex, L3Interface->u2VlanId);
#endif //USER_HW_API
    UNUSED_PARAM(L3Interface);
    return ret;
}
/* end of file */
