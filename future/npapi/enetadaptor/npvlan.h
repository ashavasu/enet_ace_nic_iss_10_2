/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/
#ifndef _NPVLAN_H
#define _NPVLAN_H

#include "vlannp.h"

#ifdef _VLANNP_C_
tVlanId gNpDefVlanId;
#else
extern tVlanId gNpDefVlanId;
#endif

#define VLAN_NP_DEF_VLAN_ID      gNpDefVlanId
#endif
