/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mbsnp.c,v 
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/

/* This file has definitions for functions used at both CFM & LM side.
 * This file will be compiled with both ISS & BCM SDK (only for LM side).
 * To differentiate between ISS & LM, the switch MBSM_WANTED is used.
 * if MBSM is defined then it is for ISS(CFM side). 
 * If not then it is for LM side.
 */
#ifdef MBSM_WANTED

#ifndef _MBSNP_C_
#define _MBSNP_C_

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "mbsm.h"
#include "npapi.h"
#include "npcfa.h"
#include "npvlan.h"
#include "mbsnp.h"

#define MBSM_STACK_MASTER_PRIORITY 10    /*There is no reason for selecting value 10,but */
                                                                      /* it should be > than Slave Priority */
#define MBSM_STACK_SLAVE_PRIORITY 1

#define  MBSM_MAX_NO_OF_SLOTS          6
#define  MBSM_MAX_MODIDS_PER_SLOT      4

#define MBSM_STK_COS 7            /*COS Q assigned for STK Traffic */
#define MBSM_STK_VLAN 4094        /*VLAN assigned for STK Traffic */
#define MBSM_STK_MAX_FABRIC_TRUNK 4    /* Maximum no. fabric trunks with */
                                    /*  HiGig ports supported */

#ifdef L2RED_WANTED

INT4
MbsmNpProcRmNodeTransition (INT4 i4Event, UINT1 u1PrevState, UINT1 u1State)
{
    UNUSED_PARAM (i4Event);
    UNUSED_PARAM (u1PrevState);
    UNUSED_PARAM (u1State);
    return FNP_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : MbsmInitNpInfoOnStandbyToActive 
 *
 *    Description          : This function is used to initializes the NP Info    
 *                           cfa mapping tables (LPortMap, IndexMap table)
 *                           for all interfaces in the given slot, NP callback
 *                           registration when node moves from standby to active.
 *
 *    Input(s)             : SlotInfo - Slot Information
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : gaCfaNpIndexMap
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : FNP_SUCCESS/FNP_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
MbsmInitNpInfoOnStandbyToActive (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : MbsmNpGetSlotInfo 
 *
 *    Description          : This function is used to get Slot Information 
 *                           for a particular Slot 
 *
 *    Input(s)             : SlotId - Slot Id
 *
 *    Output(s)            : pHwMsg - pointer to get Slot Information
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : FNP_SUCCESS/FNP_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
MbsmNpGetSlotInfo (INT4 i4SlotId, tMbsmHwMsg * pHwMsg)
{
    UNUSED_PARAM (i4SlotId);
    UNUSED_PARAM (pHwMsg);
    return FNP_FAILURE;
}

#endif /* L2RED_WANTED */

INT4
MbsmNpInitHwTopoDisc (INT4 i4SlotId, INT1 i1NodeState)
{
    UNUSED_PARAM (i4SlotId);
    UNUSED_PARAM (i1NodeState);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name             :  MbsmNpClearHwTbl                            */
/*  Description               :  Clears all hardware tables of the specified */
/*                                 SLOT                                      */
/*  Input(s)                  :  i4SlotId - SlotId to be cleared             */
/*  Output(s)                 :  None                                        */
/*  Returns                   :  FNP_SUCCESS/FNP_FAILURE                     */
/*****************************************************************************/
INT4
MbsmNpClearHwTbl (INT4 i4SlotId)
{
    UNUSED_PARAM (i4SlotId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name             :  MbsmNpProcCardInsertForLoadSharing          */
/*  Description               :  Processes the card insert event when        */
/*                               load-sharing is enabled.                    */
/*  Input(s)                  :  i4MsgType - Type of msg to be processed     */
/*  Output(s)                 :  None                                        */
/*  Returns                   :  FNP_SUCCESS/FNP_FAILURE                     */
/*****************************************************************************/
INT4
MbsmNpProcCardInsertForLoadSharing (INT4 i4MsgType)
{
    UNUSED_PARAM (i4MsgType);
    return FNP_SUCCESS;
}

/*****************************************************************************
 * Function Name : MbsmNpUpdtHwTblForLoadSharing 
 * Description   : Function to program the SrcModBlk tbl, Unicast tbl and 
 *                 ModPortMap tbl based on the load-sharing status. 
 * Input(s)      : u1Flag - LoadSharingFlag 
 * Output(s)     : None  
 * Returns       : FNP_SUCCESS/FNP_FAILURE 
 *****************************************************************************/
INT4
MbsmNpUpdtHwTblForLoadSharing (UINT1 u1Flag)
{
    UNUSED_PARAM (u1Flag);
    return (FNP_SUCCESS);
}

/*****************************************************************************
 * Function Name : MbsNpSetLoadSharingStatus
 * Description   : Function to set the Load-Sharing status in the hardware.
 * Input(s)      : i4LoadSharingFlag - Flag to enable/disable load-sharing
 * Output(s)     : None  
 * Returns       : FNP_SUCCESS/FNP_FAILURE 
 *****************************************************************************/
INT4
MbsNpSetLoadSharingStatus (INT4 i4LoadSharingFlag)
{
    UNUSED_PARAM (i4LoadSharingFlag);
    return FNP_SUCCESS;
}

/*****************************************************************************
 * Function Name : MbsmNpGetCardTypeTable
 * Description   : Function to set the Line card types supported by h/w.
 * Input(s)      : i4LoopIdx - Index for gaMbsmNpStaticCardTypeTable 
 * Output(s)     : Pointer to tMbsmCardInfo type.
 * Returns       : FNP_SUCCESS/FNP_FAILURE 
 *****************************************************************************/
INT4
MbsmNpGetCardTypeTable (INT4 i4LoopIdx, tMbsmCardInfo * pMbsmCardInfo)
{
    UNUSED_PARAM (i4LoopIdx);
    UNUSED_PARAM (pMbsmCardInfo);
    return FNP_SUCCESS;
}

/******************************************************************************
 * Function           : MbsmNpHandleNodeTransition
 * Input(s)           : event,previous state,current state to be change
 * Output(s)          : None
 * Returns            : None
 * Action             : Routine used to restart stack task in FSW for COLD
                        STANDBY
******************************************************************************/
INT4
MbsmNpHandleNodeTransition (INT4 i4Event, UINT1 u1PrevState, UINT1 u1State)
{
    UNUSED_PARAM (i4Event);
    UNUSED_PARAM (u1PrevState);
    UNUSED_PARAM (u1State);
    return (FNP_SUCCESS);
}

/*****************************************************************************
 * Function Name : MbsmNpTxOnStackInterface
 * Description   : Function to transmit the packets over stack interface.
 * Input(s)      : pu1Pkt    - Pointer to the packet
 *                 i4PktSize - Size of the packet
 * Output(s)     : None
 * Returns       : FNP_SUCCESS/FNP_FAILURE
 *****************************************************************************/
INT4
MbsmNpTxOnStackInterface (UINT1 *pu1Pkt, INT4 i4PktSize)
{
    UNUSED_PARAM (pu1Pkt);
    UNUSED_PARAM (i4PktSize);
    return (FNP_SUCCESS);
}

/******************************************************************************
 * Function           : MbsmNpGetStackMac
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : Routine used to get stack mac
******************************************************************************/
VOID
MbsmNpGetStackMac (UINT4 u4SlotId, UINT1 *pu1MacAddr)
{
    UNUSED_PARAM (u4SlotId);
    UNUSED_PARAM (pu1MacAddr);
    return;
}
#endif
#endif /*MBSM_WANTED */
