/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: nprstp.h,v 1.3 2007/02/01 14:59:31 iss Exp $ 
 *
 * Description: All prototypes for Network Processor API functions for RSTP 
 * 
 *
 *******************************************************************/
#ifndef _NPRSTP_H
#define _NPRSTP_H

#include "rstnp.h"

#endif /* _NPRSTP_H */
