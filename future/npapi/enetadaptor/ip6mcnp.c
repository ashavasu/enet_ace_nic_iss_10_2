/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipmcnp.c,v
 *
 * Description: This file contains the function implementations  of IPMC NP-API.
 ****************************************************************************/
#ifndef _IP6MCNP_C_
#define _IP6MCNP_C_

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "npapi.h"
#include "npip6mc.h"

/*****************************************************************************
 *  Function Name   : FsNpIpv6McInit
 *  Description     : This function initialises the IP Multicasting in the 
 *                    driver. This function enables the IPMC Module, enforces
 *                    source port check, and defines the search rule for IPMC
 *                    entry
 *  Input(s)        : None
 *  Output(s)       : None.
 *  Returns         : FNP_SUCCESS/FNP_FAILURE
 ***************************************************************/

PUBLIC UINT4
FsNpIpv6McInit (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : FsNpIpv6McDeInit
 *  Description     : This function deinitialises the IP Multicasting in the 
 *                    driver. This function disables the IPMC Module
 *  Input(s)        : None
 *  Output(s)       : None.
 *  Returns         : FNP_SUCCESS/FNP_FAILURE
 ***************************************************************/

PUBLIC UINT4
FsNpIpv6McDeInit (VOID)
{
    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FsNpIpv6McAddRouteEntry                  */
/*  Description     : This function adds Multicast Route Entry */
/*                    in IPMC Table. It also add a downstream  */
/*                    interface to the existing route entry.   */
/*  Input(s)        : u4VrId        :  Virtual Router ID       */
/*                                     ZERO when not supported.*/
/*                    pGrpAddr      :  Group Address           */
/*                    u4GrpPrefix   :  Group Prefix            */
/*                    pSrcIpAddr    :  Source Address          */
/*                    u4SrcIpPrefix :  Source Prefix           */
/*                    rtEntry       :  Have the route entry    */
/*                                     parameters like command */
/*                                     RPF interface and flags.*/
/*                    u2NoOfDownStreamIf:  No of associated    */
/*                                     downstream interfaces.  */
/*                    NOTE: Currently only one interface is    */
/*                          added at a time                    */
/*                    pDownStreamIf :  downstream interface    */
/*                                     to be added.            */
/*  Output(s)       : None.                                    */
/*  Returns         : FNP_SUCCESS/FNP_FAILURE                  */
/***************************************************************/

PUBLIC UINT4
FsNpIpv6McAddRouteEntry (UINT4 u4VrId,
                         UINT1 *pGrpAddr,
                         UINT4 u4GrpPrefix,
                         UINT1 *pSrcIpAddr,
                         UINT4 u4SrcIpPrefix,
                         UINT1 u1CallerId,
                         tMc6RtEntry rtEntry,
                         UINT2 u2NoOfDownStreamIf,
                         tMc6DownStreamIf * pDownStreamIf)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pGrpAddr);
    UNUSED_PARAM (u4GrpPrefix);
    UNUSED_PARAM (pSrcIpAddr);
    UNUSED_PARAM (u4SrcIpPrefix);
    UNUSED_PARAM (u1CallerId);
    UNUSED_PARAM (rtEntry);
    UNUSED_PARAM (u2NoOfDownStreamIf);
    UNUSED_PARAM (pDownStreamIf);
    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FsNpIpv6McDelRouteEntry                  */
/*  Description     : Deletes multicast route entry if         */
/*                    (u2NoOfDownStreamIf = 0) or else deletes */
/*                    the associated outgoing interface from   */
/*                    the corresponding route entry.           */
/*  Input(s)        : u4VrId        :  Virtual Router ID       */
/*                                     ZERO when not supported.*/
/*                    pGrpAddr     :  Group Address           */
/*                    u4GrpPrefix   :  Group Prefix            */
/*                    pSrcIpAddr   :  Source Address          */
/*                    u4SrcIpPrefix :  Source Prefix           */
/*                    u2NoOfDownStreamIf:  No of associated    */
/*                                     downstream interfaces.  */
/*                    pDownStreamIf :  Pointer to list of      */
/*                                     downstream interfaces   */
/*                                     to be deleted.          */
/*  Output(s)       : None.                                    */
/*  Returns         : FNP_SUCCESS/FNP_FAILURE                  */
/***************************************************************/

UINT4
FsNpIpv6McDelRouteEntry (UINT4 u4VrId,
                         UINT1 *pGrpAddr,
                         UINT4 u4GrpPrefix,
                         UINT1 *pSrcIpAddr,
                         UINT4 u4SrcIpPrefix,
                         tMc6RtEntry rtEntry,
                         UINT2 u2NoOfDownStreamIf,
                         tMc6DownStreamIf * pDownStreamIf)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pGrpAddr);
    UNUSED_PARAM (u4GrpPrefix);
    UNUSED_PARAM (pSrcIpAddr);
    UNUSED_PARAM (u4SrcIpPrefix);
    UNUSED_PARAM (rtEntry);
    UNUSED_PARAM (u2NoOfDownStreamIf);
    UNUSED_PARAM (pDownStreamIf);
    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FsNpIpv6McClearAllRoutes                 */
/*  Description     : Clears the multicast route entries       */
/*                    for the specified Vr ID.                 */
/*  Input(s)        : u4VrId        :  Virtual Router ID       */
/*                                     ZERO when not supported.*/
/*  Output(s)       : None.                                    */
/*  Returns         : None.                                    */
/***************************************************************/

PUBLIC VOID
FsNpIpv6McClearAllRoutes (UINT4 u4VrId)
{
    UNUSED_PARAM (u4VrId);
    return;
}

/***************************************************************/
/*  Function Name   : FsNpIpv6McUpdateOifVlanEntry             */
/*  Description     : This function updates the L3 bitmap      */
/*                   a route entry in the IPMC table           */
/*                                                             */
/*                                                             */
/*  Input(s)        : u4VrId        :  Virtual Router ID       */
/*                                     ZERO when not supported.*/
/*                    pGrpAddr      :  Group Address           */
/*                    u4GrpPrefix   :  Group Prefix            */
/*                    pSrcIpAddr    :  Source IP address       */
/*                    u4SrcIpPrefix :  Source Prefix           */
/*                    rtEntry       :  Have the route entry    */
/*                                     parameters like  RPF    */
/*                    pDownStreamIf :  downstream interface    */
/*                                     to be updated           */
/*  Output(s)       : None.                                    */
/*  Returns         : None                                     */
/***************************************************************/
VOID
FsNpIpv6McUpdateOifVlanEntry (UINT4 u4VrId, UINT1 *pGrpAddr, UINT4 u4GrpPrefix,
                              UINT1 *pSrcIpAddr, UINT4 u4SrcIpPrefix,
                              tMc6RtEntry rtEntry,
                              tMc6DownStreamIf downStreamIf)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pGrpAddr);
    UNUSED_PARAM (u4GrpPrefix);
    UNUSED_PARAM (pSrcIpAddr);
    UNUSED_PARAM (u4SrcIpPrefix);
    UNUSED_PARAM (rtEntry);
    UNUSED_PARAM (downStreamIf);
    return;
}

/***************************************************************/
/*  Function Name   : FsNpIpv4McUpdateIifVlanEntry             */
/*  Description     : This function updates the L3 bitmap      */
/*                   a route entry in the IPMC table           */
/*                                                             */
/*                                                             */
/*  Input(s)        : u4VrId        :  Virtual Router ID       */
/*                                     ZERO when not supported.*/
/*                    pGrpAddr      :  Group Address           */
/*                    u4GrpPrefix   :  Group Prefix            */
/*                    u4RtAddr      :  Source IP address       */
/*                    rtEntry       :  Have the route entry    */
/*                                     parameters like  RPF    */
/*  Output(s)       : None.                                    */
/*  Returns         : None                                     */
/***************************************************************/

VOID
FsNpIpv6McUpdateIifVlanEntry (UINT4 u4VrId, UINT1 *pGrpAddr, UINT4 u4GrpPrefix,
                              UINT1 *pSrcIpAddr, UINT4 u4SrcIpPrefix,
                              tMc6RtEntry rtEntry,
                              tMc6DownStreamIf downStreamIf)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pGrpAddr);
    UNUSED_PARAM (u4GrpPrefix);
    UNUSED_PARAM (pSrcIpAddr);
    UNUSED_PARAM (u4SrcIpPrefix);
    UNUSED_PARAM (rtEntry);
    UNUSED_PARAM (downStreamIf);
    return;

}

/*****************************************************************************/
/*  Function Name   : FsNpIpv6McGetHitStatus                                 */
/*                                                                           */
/*  Description     : This function gets the hit status for a particular     */
/*                    multicast group from the hardware.                     */
/*                                                                           */
/*  Input(s)        : pGrpAddr      :  Multicast group address               */
/*                    pSrcIpAddr    :  Source IP                             */
/*                    u4Iif         :  Incoming interface                    */
/*                    u2VlanId      :  Vlan Identifier                       */
/*                                                                           */
/*  Output(s)       : pu4HitStatus - Hit bit status                          */
/*                                                                           */
/*  Returns         : None                                                   */
/*****************************************************************************/

VOID
FsNpIpv6McGetHitStatus (UINT1 *pSrcIpAddr, UINT1 *pGrpAddr,
                        UINT4 u4Iif, UINT2 u2VlanId, UINT4 *pu4HitStatus)
{
    UNUSED_PARAM (pSrcIpAddr);
    UNUSED_PARAM (pGrpAddr);
    UNUSED_PARAM (u4Iif);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (pu4HitStatus);

    return;
}

/******************************************************************************/
/*  Function Name   : FsNpIpv6McAddCpuPort                      */
/*  Description     : This function sets CPU port also for              */
/*                    a particular multicast                                  */
/*                    group.                                                  */
/*                                          */
/*  Input(s)        : u4VrId        :  Virtual Router ID                      */
/*                                     ZERO when not supported.               */
/*                    pGrpAddr      :  Group Address                          */
/*                    u4GrpPrefix   :  Group Prefix                           */
/*                    pSrcIpAddr    :  Source IP address                      */
/*                    u4SrcIpPrefix :  Source Prefix                          */
/*                    rtEntry       :  Have the route entry                   */
/*                                     parameters like  RPF                   */
/*                    pDownStreamIf :  downstream interface                   */
/*                                     to be updated                          */
/******************************************************************************/
UINT4
FsNpIpv6McAddCpuPort (UINT1 u1GenRtrId,
                      UINT1 *pGrpAddr,
                      UINT4 u4GrpPrefix,
                      UINT1 *pSrcIpAddr,
                      UINT4 u4SrcIpPrefix,
                      tMc6RtEntry rtEntry,
                      UINT2 u2NoOfDownStreamIf,
                      tMc6DownStreamIf * pDownStreamIf)
{
    UNUSED_PARAM (u1GenRtrId);
    UNUSED_PARAM (pGrpAddr);
    UNUSED_PARAM (u4GrpPrefix);
    UNUSED_PARAM (pSrcIpAddr);
    UNUSED_PARAM (u4SrcIpPrefix);
    UNUSED_PARAM (rtEntry);
    UNUSED_PARAM (u2NoOfDownStreamIf);
    UNUSED_PARAM (pDownStreamIf);
    return FNP_SUCCESS;
}

/******************************************************************************
 *  Function Name   : FsNpIpv6McDelCpuPort                     
 *  Description     : This function deletes CPU port for a particular multicast
 *                    group.
 *                   
 *  Input(s)        : pGrpAddr     :  Multicast group address
 *                    u4GrpPrefix   :  Group Prefix
 *                    pSrcAddr     :  Source IP
 *                    u4SrcIpPrefix :  Source Prefix                          
 *                    rtEntry       :  Have the route entry                   
 *                                     parameters like  RPF                   
 *                    pDownStreamIf :  downstream interface                   
 *                                     to be updated        
 *  Output(s)       : None
 *  Returns         : None 
 ******************************************************************************/
UINT4
FsNpIpv6McDelCpuPort (UINT1 u1GenRtrId, UINT1 *pGrpAddr, UINT4 u4GrpPrefix,
                      UINT1 *pSrcIpAddr, UINT4 u4SrcIpPrefix,
                      tMc6RtEntry rtEntry)
{
    UNUSED_PARAM (u1GenRtrId);
    UNUSED_PARAM (pGrpAddr);
    UNUSED_PARAM (u4GrpPrefix);
    UNUSED_PARAM (pSrcIpAddr);
    UNUSED_PARAM (u4SrcIpPrefix);
    UNUSED_PARAM (rtEntry);
    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FsNpIpv6GetMRouteStats                   */
/*  Description     : This function get the muticast route     */
/*                    statistics from NP depending upon the    */
/*                    stats Type.                              */
/*  Input(s)        : u4VrId- Virtual Router ID                */
/*                    ZERO when not supported.                 */
/*                    pu1GrpAddr - Group Address               */
/*                    pu1SrcAddr - Source Address              */
/*                    i4StatType - Statistic type              */
/*  Output(s)       : pu4RetValue                              */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : FNP_SUCCESS/FNP_FAILURE                  */
/***************************************************************/
INT4
FsNpIpv6GetMRouteStats (UINT4 u4VrId, UINT1 *pu1GrpAddr, UINT1 *pu1SrcAddr,
                        INT4 i4StatType, UINT4 *pu4RetValue)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pu1GrpAddr);
    UNUSED_PARAM (pu1SrcAddr);
    UNUSED_PARAM (i4StatType);
    *pu4RetValue = 0;

    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FsNpIpv6GetMRouteHCStats                 */
/*  Description     : This function get the muticast route     */
/*                    64-bit statistics from NP depending upon */
/*                    stats Type.                              */
/*                    address.This is 64-bit version.          */
/*  Input(s)        : u4VrId- Virtual Router ID                */
/*                    ZERO when not supported.                 */
/*                    pu1GrpAddr - Group Address               */
/*                    pu1SrcAddr - Source Address              */
/*                    i4StatType -Statistic type               */
/*  Output(s)       : pu8RetValue                              */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : FNP_SUCCESS/FNP_FAILURE                  */
/***************************************************************/
INT4
FsNpIpv6GetMRouteHCStats (UINT4 u4VrId, UINT1 *pu1GrpAddr, UINT1 *pu1SrcAddr,
                          INT4 i4StatType, tSNMP_COUNTER64_TYPE * pu8RetValue)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pu1GrpAddr);
    UNUSED_PARAM (pu1SrcAddr);
    UNUSED_PARAM (i4StatType);
    pu8RetValue->lsn = 0;
    pu8RetValue->msn = 0;

    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FsNpIpv6GetMNextHopStats                 */
/*  Description     : The number of packets which have been    */
/*                    forwarded on this out going interface    */
/*  Input(s)        : u4VrId- Virtual Router ID                */
/*                    ZERO when not supported.                 */
/*                    pu1GrpAddr - Group Address               */
/*                    pu1SrcAddr - Source Address              */
/*                    i4OutIfIndex - Out going interface index */
/*                    i4StatType - Statistic type              */
/*  Output(s)       : pu4RetValue                              */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : FNP_SUCCESS/FNP_FAILURE                  */
/***************************************************************/
INT4
FsNpIpv6GetMNextHopStats (UINT4 u4VrId, UINT1 *pu1GrpAddr, UINT1 *pu1SrcAddr,
                          INT4 i4OutIfIndex, INT4 i4StatType,
                          UINT4 *pu4RetValue)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pu1GrpAddr);
    UNUSED_PARAM (pu1SrcAddr);
    UNUSED_PARAM (i4OutIfIndex);
    UNUSED_PARAM (i4StatType);
    *pu4RetValue = 0;

    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FsNpIpv6GetMIfaceStats                   */
/*  Description     : The number of octets of multicast packets*/
/*                    that have arrived or forwarded  on the   */
/*                    interface depending upon the stats type  */
/*  Input(s)        : u4VrId- Virtual Router ID                */
/*                    ZERO when not supported.                 */
/*                    i4IfIndex - Interface Index              */
/*                    i4StatType - Statistics type             */
/*  Output(s)       : pu4RetValue                              */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : FNP_SUCCESS/FNP_FAILURE                  */
/***************************************************************/
INT4
FsNpIpv6GetMIfaceStats (UINT4 u4VrId, INT4 i4IfIndex, INT4 i4StatType,
                        UINT4 *pu4RetValue)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4StatType);
    *pu4RetValue = 0;

    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FsNpIpv6GetMIfaceHCStats                 */
/*  Description     : The number of 64-bit octets of multicast */
/*                    packets that have arrived or forwarded   */
/*                    on the interface depending upon the stats*/
/*                    type                                     */
/*  Input(s)        : u4VrId- Virtual Router ID                */
/*                    ZERO when not supported.                 */
/*                    i4IfIndex - Interface Index              */
/*                    i4StatType - Statistics type             */
/*  Output(s)       : pu8RetValue                              */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : FNP_SUCCESS/FNP_FAILURE                  */
/***************************************************************/
INT4
FsNpIpv6GetMIfaceHCStats (UINT4 u4VrId, INT4 i4IfIndex, INT4 i4StatType,
                          tSNMP_COUNTER64_TYPE * pu8RetValue)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4StatType);

    pu8RetValue->lsn = 0;
    pu8RetValue->msn = 0;

    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FsNpIpv6SetMIfaceTtlTreshold             */
/*  Description     : Sets the The datagram TTL threshold for  */
/*                    the interface                            */
/*  Input(s)        : u4VrId- Virtual Router ID                */
/*                    ZERO when not supported.                 */
/*                    i4IfIndex - Interface Index              */
/*                    i4TtlTreshold - Ttl-threshold            */
/*  Output(s)       :                                          */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         :  FNP_SUCCESS/FNP_FAILURE                 */
/***************************************************************/
INT4
FsNpIpv6SetMIfaceTtlTreshold (UINT4 u4VrId, INT4 i4IfIndex, INT4 i4TtlTreshold)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TtlTreshold);

    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FsNpIpv6SetMIfaceRateLimit               */
/*  Description     : Sets the The datagram Rate limit for     */
/*                    the interface                            */
/*  Input(s)        : u4VrId- Virtual Router ID                */
/*                    ZERO when not supported.                 */
/*                    i4IfIndex - Interface Index              */
/*  Output(s)       : i4RateLimit - rate limit value           */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : FNP_SUCCESS/FNP_FAILURE                  */
/***************************************************************/
INT4
FsNpIpv6SetMIfaceRateLimit (UINT4 u4VrId, INT4 i4IfIndex, INT4 i4RateLimit)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4RateLimit);

    return FNP_SUCCESS;
}

#ifdef PIMV6_WANTED

/*****************************************************************************/
/* Function Name      : FsPimv6NpInitHw                                        */
/*                                                                           */
/* Description        : This function enables PIMv6 & enables IPMC in the      */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

UINT4
FsPimv6NpInitHw (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsPimv6NpDeInitHw                                      */
/*                                                                           */
/* Description        : This function disables PIMV6 & disables IPMC in the    */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

UINT4
FsPimv6NpDeInitHw (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : FsNpIpv6McClearHitBit                                  */
/*                                                                           */
/*  Description     : This function clears the hit status for a particular   */
/*                    multicast group from the hardware.                     */
/*                                                                           */
/*  Input(s)        : pGrpAddr      :  Multicast group address               */
/*                    pSrcIpAddr    :  Source IP                             */
/*                    u4Iif         :  Incoming interface                    */
/*                    u2VlanId      :  Vlan Identifier                       */
/*                                                                           */
/*  Output(s)       : pu4HitStatus - Hit bit status                          */
/*                                                                           */
/*  Returns         : None                                                   */
/*****************************************************************************/

UINT4
FsNpIpv6McClearHitBit (UINT2 u2VlanId, UINT1 *pSrcIpAddr, UINT1 *pGrpAddr)
{
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (pSrcIpAddr);
    UNUSED_PARAM (pGrpAddr);
    return FNP_SUCCESS;
}

#endif /* PIMV6_WANTED */
#endif
