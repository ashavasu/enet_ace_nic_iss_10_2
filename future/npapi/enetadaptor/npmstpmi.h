/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: npmstpmi.h,v 1.2 2007/02/01 14:59:31 iss Exp $ 
 *
 * Description: All prototypes for Network Processor API functions for MSTP 
 * 
 *
 *******************************************************************/
#ifndef _NPMSTPMI_H
#define _NPMSTPMI_H

#include "mstminp.h"

#define MSTP_NP_BCM_INST(u2InstId) (u2InstId + 1)

#endif /* _NPMSTPMI_H */
