/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rstnpx.c,v 1.4 2007/07/17 13:29:53 iss Exp $
 *
 * Description: All network processor functions for RSTP-NP
 *
 *******************************************************************/
#ifndef _RSTPNPX_C_
#define _RSTPNPX_C_

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "rstp.h"
#include "npapi.h"
#include "nprstp.h"

/*************************************************************************/
/* Function Name      : FsRstpMbsmNpSetPortState                         */
/*                                                                       */
/* Description        : Sets the RSTP Port State in the Hardware.        */
/*                      When RSTP is working in RSTP mode                */
/*                      or RSTP in STP compatible                        */
/*                      mode, porting should be done for this API.       */
/*                                                                       */
/*                                                                       */
/* Input(s)           : u4IfIndex - Port Number.                         */
/*                      u1Status  - Status to be set.                    */
/*                                Values can be AST_PORT_STATE_DISCARDING*/
/*                                  or AST_PORT_STATE_LEARNING           */
/*                                  or AST_PORT_STATE_FORWARDING          
 *                      pSlotInfo - Pointer to the slot information 
 *                                   Structure                           */
/*                                                                       */
/* Output(s)          : None                                             */
/*                                                                       */
/* Global Variables                                                      */
/* Referred           : None                                             */
/*                                                                       */
/* Global Variables                                                      */
/* Modified           : None                                             */
/*                                                                       */
/* Return Value(s)    : FNP_SUCCESS - On successful set (or)             */
/*                      FNP_FAILURE - Error during setting               */
/*************************************************************************/
INT1
FsRstpMbsmNpSetPortState (UINT4 u4IfIndex, UINT1 u1Status,
                          tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*************************************************************************
 * Function Name                  :FsRstpMbsmNpInitHw                    *
 *                                                                       *
 * DESCRIPTION                    :This function performs any necessary  * 
 *                                 RSTP related initialisation in the    *
 *                                 Hardware                              *
 *                                                                       *
 * INPUTS                         : pSlotInfo - Slot Information         *
 *                                                                       *
 *                                                                       *
 * OUTPUTS                        : None                                 *
 *                                                                       *
 * RETURNS                        : FNP_SUCCESS - On Success             *
 *                                  FNP_FAILURE - On Failure             *
 *                                                                       *
 *************************************************************************/

INT1
FsRstpMbsmNpInitHw (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}
#endif
