/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: nprstpmi.h,v 1.2 2007/02/01 14:59:31 iss Exp $ 
 *
 * Description: All prototypes for Network Processor API functions for RSTP 
 * 
 *
 *******************************************************************/
#ifndef _NPRSTPMI_H
#define _NPRSTPMI_H

#include "rstminp.h"

#endif /* _NPRSTPMI_H */
