/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: issunp.c,v 1.1 2015/09/15 06:39:07 siva Exp $
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/

#ifndef _ISSUNP_C_
#define _ISSUNP_C_

#include "lr.h"
#include "iss.h"
#include "npapi.h"
#include "issunp.h"

/*****************************************************************************/
/*    Function Name       : FsNpIssuLoadVersionUpgrade                       */
/*                                                                           */
/*    Description         : This function loads new software image           */
/*                                                                           */
/*    Input(s)            : tIssuHwStatusInfo		                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns                 : FNP_SUCCESS/FNP_FAILURE                      */
/*****************************************************************************/

INT4
FsNpIssuLoadVersionUpgrade (tIssuHwStatusInfo * pIssuHwStatusInfo)
{
    UNUSED_PARAM (pIssuHwStatusInfo);
    return FNP_SUCCESS;
}
#endif /*ISSUNP_C*/
