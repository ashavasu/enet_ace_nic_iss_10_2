/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfanpx.c,v 1.8 2009/09/24 14:21:46 prabuc Exp $
 *
 * Description: This file contains the function implementations  of CFA NP-API.
 ****************************************************************************/
#ifndef _CFANPX_C_
#define _CFANPX_C_

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "la.h"
#include "npapi.h"
#include "npcfa.h"

/*****************************************************************************
 *
 *    Function Name        : CfaMbsmNpSlotInit
 *
 *    Description          : This function is used to initializes the driver 
 *                           to cfa mapping tables (LPortMap, IndexMap table)
 *                           for all interfaces in the given slot.
 *
 *    Input(s)             : None
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : gaCfaNpIndexMap
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : FNP_SUCCESS/FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
CfaMbsmNpSlotInit (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaMbsmNpSlotDeInit
 *
 *    Description          : This function is used to set default values of the 
 *                           driver to cfa mapping tables (LPortMap, IndexMap table)
 *                           for all interfaces in the given slot and 
 *                           initialize slot specific global variables.
 *
 *    Input(s)             : None
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : gaUnitInfo, gaCfaNpIndexMap
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : FNP_SUCCESS/FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
CfaMbsmNpSlotDeInit (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaMbsmRegisterWithNpDrv
 *
 *    Description          : This function is used to register with hardware
 *
 *    Input(s)             : None
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None,
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : FNP_SUCCESS/FNP_FAILURE
 *
 *****************************************************************************/
INT4
CfaMbsmRegisterWithNpDrv (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************
 *    Function Name      : CfaMbsmNpEnablePort()
 *    Description        : This function is invoked to functionally Enable the port.  
 *    Input(s)           : u4UnitId - Unit Number
 *                         i4Port - Port Number 
 *    Output(s)          : None.
 *
 *    Globals Referred         : None
 *    Globals Modified         : None
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Returns                  : FNP_SUCCESS/FNP_FAILURE
 *****************************************************************************/
INT4
CfaMbsmNpEnablePort (UINT4 u4UnitId, INT4 i4Port)
{
    UNUSED_PARAM (u4UnitId);
    UNUSED_PARAM (i4Port);
    return FNP_SUCCESS;
}

/* This routine registers in the hardware for receiving the specified protocol 
 * packets.
 */
/*****************************************************************************
 *    Function Name      : CfaMbsmNpRegisterProtocol 
 *    Description        : This function registers a protocol by creating
 *                         filter based on protocol ID in the Hw
 *    Input(s)           : u1Protocol - Protocol Number
 *    Output(s)          : None
 *    Globals Referred         : None
 *    Globals Modified         : None
 *    Exceptions or Operating
 *    System Error Handling    : None
 *    Returns                  : FNP_SUCCESS/FNP_FAILURE
 *****************************************************************************/

INT4
CfaMbsmNpRegisterProtocol (UINT1 u1Protocol, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u1Protocol);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/********************************************************************
 * Function Name        -  FsHwMbsmSetCustIfParams                  *
 * Function Description -  To set custom interface parameters       * 
 *                         in the hardware.                         *
 * Input description    -  u4IfIndex - Interface index of the port  *
 *                         HwCustParamType - Type of the custom     * 
 *                         parameter passed.                        *
 *                         CustIfParamVal - The custom parameter    *
 *                         passed                                   *
 *                         EntryAction - Create or Delete entry     *
 *                         pSlotInfo  -  Slot Information           *
 * Output               -  FNP_SUCCESS/FNP_FAILURE                  * 
 ********************************************************************/
INT4
FsHwMbsmSetCustIfParams (UINT4 u4IfIndex, tHwCustIfParamType HwCustParamType,
                         tHwCustIfParamVal CustIfParamVal,
                         tNpEntryAction EntryAction, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (HwCustParamType);
    UNUSED_PARAM (CustIfParamVal);
    UNUSED_PARAM (EntryAction);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/**************************************************************************
 Function Name             : FsCfaMbsmSetMacAddr                       *
 Description               : Configures the given Macaddress         *
 Input(s)                  : InterfaceId, MacAddress                 *
 Output(s)                 : None                                    *
 Global Variables Referred : None                                    *
 Global Variables Modified : None                                    *
 Exceptions or Operating                                             *
 System Error Handling     : None.                                   *
 Use of Recursion          : None.                                   *
 Returns                   : FNP_SUCCESS/FNP_FAILURE                 *
 **************************************************************************/
INT4
FsCfaMbsmSetMacAddr (UINT4 u4IfIndex, tMacAddr PortMac,
                     tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (PortMac);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/****************************************************************************************
 *    Function Name      : CfaMbsmUpdatePortMacAddress
 *    Description        : This function will update port mac address
 *    Input(s)           : u1SlotId - slot number
 *                         u4PortIndex - port number
 *                         u2IfIndex - Interface index
 *                         pu1SwitchMac - Switch hw address
 *    Output(s)          : None
 *    Returns            : FNP_SUCCESS/FNP_FAILURE
 *****************************************************************************************/
VOID
CfaMbsmUpdatePortMacAddress (UINT1 u1SlotId, UINT4 u4PortIndex,
                             UINT4 u4IfIndex, UINT1 *pu1SwitchMac)
{
    UNUSED_PARAM (u1SlotId);
    UNUSED_PARAM (u4PortIndex);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1SwitchMac);
    return;
}
#endif
