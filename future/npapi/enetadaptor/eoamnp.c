/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: eoamnp.c,v 1.6 2007/02/01 14:59:31 iss Exp $
 *
 * Description: This file contains the Ethernet OAM related NPAPI.
 *              This is a portable file based on the type of chipset.
 *****************************************************************************/

#include "lr.h"
#include "cfa.h"
#include "eoam.h"
#include "npapi.h"
#include "npeoam.h"

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamNpInit
 *                                                                          
 *    DESCRIPTION      : This function is called by EOAM to initialize 
 *                       EOAM NP data structures
 *                                                                          
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : FNP_SUCCESS / FNP_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
EoamNpInit (VOID)
{
    return FNP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamNpDeInit
 *                                                                          
 *    DESCRIPTION      : This function is called by EOAM to de-initialize 
 *                       EOAM NP data structures
 *                                                                          
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : FNP_SUCCESS / FNP_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
EoamNpDeInit (VOID)
{
    return FNP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamNpGetUniDirectionalCapability                  
 *                                                                          
 *    DESCRIPTION      : This function is called by EOAM to check if a 
 *                       given interface supports unidirectional 
 *                       transmission capability or not.
 *                                                                          
 *    INPUT            : u4IfIndex - interface index
 *                                                                          
 *    OUTPUT           :  *pu1Capability - pointer to truth value 
 *                             FNP_TRUE - if unidirectional capability is 
 *                             supported by the interface
 *                             FNP_FALSE - otherwise.
 *                                                                          
 *    RETURNS          : FNP_SUCCESS / FNP_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
EoamNpGetUniDirectionalCapability (UINT4 u4IfIndex, UINT1 *pu1Capability)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1Capability);
    return FNP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  EoamNpHandleRemoteLoopBack                        
 *                                                                          
 *    DESCRIPTION      :  This function is called when the local EOAM entity
 *                        has put the remote peer in remote loopback mode and
 *                        when it is reset to normal mode from remote loopback
 *                        mode.
 *
 *                        When Remote loopback is enabled, this function 
 *                        should take the following actions:
 *                        - only OAMPDUs and loopback test data can go out of 
 *                          the interface. Hardware switching should also be
 *                          disabled on this interface
 *                        - Install hardware filter to copy looped back test 
 *                          packets upto the CPU so that it will be given to 
 *                          the Fault Management module for verification. 
 *
 *                        When Remote loopback is disabled, the installed 
 *                        filters should be removed and transmission of packets 
 *                        should be returned to normal. 
 *                                                                          
 *    INPUT            :  u4IfIndex - Interface index
 *                        SrcMac    - Source MAC address of the interface
 *                                    for filter installation
 *                        DestMac   - Destination MAC for filter installation
 *                        u1Status  - EOAM_ENABLED or EOAM_DISABLED
 *                                    Denotes whether Remote Loopback is being
 *                                    'enabled'  or 'disabled'
 *                                                                          
 *    OUTPUT           :  None.                                                 
 *                                                                          
 *    RETURNS          :  FNP_SUCCESS / FNP_FAILURE
 *                                                                          
 ******************************************************************************/
INT4
EoamNpHandleRemoteLoopBack (UINT4 u4IfIndex, tMacAddr SrcMac,
                            tMacAddr PeerMac, UINT1 u1Status)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SrcMac);
    UNUSED_PARAM (PeerMac);
    UNUSED_PARAM (u1Status);
    return FNP_SUCCESS;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  EoamNpHandleLocalLoopBack                        
 *                                                                          
 *    DESCRIPTION      :  This function is called when remote loopback command
 *                        is received from the peer to put this interface in 
 *                        the loop back mode.
 *
 *                        If data link layer frame level loopback
 *                        is supported by the hardware, appropriate harware API 
 *                        can be called in this function to put the interface 
 *                        in loopback mode.
 *
 *                        If the hardware DOES NOT support the data link layer 
 *                        loop back, this feature can be achieved by installing
 *                        filter in the hardware so as to get all incoming 
 *                        packets on the mentioned interface up to the CPU. 
 *                        In this case, EOAM software will perform the loopback.
 *
 *                        This is assumed that at the peer which initiated the 
 *                        loop back command, would make sure that only test 
 *                        packets and EOAM packets are transmitted out on the 
 *                        link.
 *
 *                        This function should also take appropriate actions to
 *                        make sure that only OAMPDUs and looped back test data
 *                        are transmitted out of the interface.  
 *                                                                          
 *    INPUT            :  u4IfIndex - Interface index
 *                        u1Status  - Denotes whether Local Loopback is enabled
 *                        or disabled
 *                                                                          
 *    OUTPUT           :  None.                                                 
 *                                                                          
 *    RETURNS          : FNP_SUCCESS / FNP_FAILURE
 *                                                                          
 ******************************************************************************/
INT4
EoamNpHandleLocalLoopBack (UINT4 u4IfIndex, tMacAddr SrcMac,
                           tMacAddr DestMac, UINT1 u1Status)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SrcMac);
    UNUSED_PARAM (DestMac);
    UNUSED_PARAM (u1Status);
    return FNP_SUCCESS;
}

/*************** Link Monitoring NPAPIs *************************************/

/*****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamLmNpRegisterLinkMonitor
 *                                                                          
 *    DESCRIPTION      : This function is called at Link Monotoring 
 *                       initialization time so as to register the call back
 *                       funtion for posting the Link monitoring threshold 
 *                       crossing event details to the Ethernet OAM task.
 *
 *                       This would be required ONLY when the hardware itself
 *                       supports detection of threshold crossing events.
 *                       In that case the Link Monitoring task will NOT be run
 *                       in the software.
 *
 *    INPUT            : None
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : FNP_SUCCESS / FNP_FAILURE
 *                                                                          
 *****************************************************************************/
INT4
EoamLmNpRegisterLinkMonitor (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamLmNpConfigureParams  
 *                                                                          
 *    DESCRIPTION      : This function is called to configure the window and 
 *                       threshold parameters for Link Monitoring events on a 
 *                       particular port. 
 *
 *    INPUT            : u4IfIndex - Interface index
 *                       u2Event   - Type of Link Monitoring event for which 
 *                                   the configuration is applicable. 
 *                                   This can be 
 *                                   - symbol period event
 *                                   - errored frame period event 
 *                                   - error frame event 
 *                                   - errored second summary event
 *                       Window    - Configured  window for the event
 *                       Threshold - Configured threshold for the event
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : FNP_SUCCESS / FNP_FAILURE
 *                                                                          
 *****************************************************************************/
INT4
EoamLmNpConfigureParams (UINT4 u4IfIndex, UINT2 u2Event,
                         FS_UINT8 Window, FS_UINT8 Threshold)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2Event);
    UNUSED_PARAM (Window);
    UNUSED_PARAM (Threshold);
    return FNP_SUCCESS;
}

/*****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamLmNpGetStat
 *                                                                          
 *    DESCRIPTION      : This function gets the statistics such as 
 *                       erroed frames/received frames/total for the given port.
 *
 *    INPUT            : u4IfIndex - interface index
 *                       u1StatType - Type of statistic value can be
 *                                    NP_STAT_IF_IN_ERRORS
 *                                    NP_STAT_ETHER_PKTS
 *                                                                          
 *    OUTPUT           : *pu4Value - returned value for the requested statistics
 *                                                                          
 *    RETURNS          : FNP_SUCCESS / FNP_FAILURE
 *                                                                          
 *****************************************************************************/
INT4
EoamLmNpGetStat (UINT4 u4IfIndex, UINT1 u1StatType, UINT4 *pu4Value)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1StatType);
    UNUSED_PARAM (pu4Value);
    return FNP_SUCCESS;
}

/*****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamLmNpGetStat64
 *                                                                          
 *    DESCRIPTION      : This function gets the statistics such as 
 *                       symbols/symbol errors for the given port.
 *
 *    INPUT            : u4IfIndex - interface index
 *                       u1StatType - Type of statistic value can be
 *                                    NP_STAT_IF_IN_SYMBOLS
 *                                    NP_STAT_DOT3_SYMBOL_ERRORS
 *                                                                          
 *    OUTPUT           : *pu8Value - returned value for the requested statistics
 *                                                                          
 *    RETURNS          : FNP_SUCCESS / FNP_FAILURE
 *                                                                          
 *****************************************************************************/
INT4
EoamLmNpGetStat64 (UINT4 u4IfIndex, UINT1 u1StatType, FS_UINT8 * pu8Value)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1StatType);
    UNUSED_PARAM (pu8Value);
    return FNP_SUCCESS;
}
