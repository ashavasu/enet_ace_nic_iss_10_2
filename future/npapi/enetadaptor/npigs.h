/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: npigs.h,v 1.3 2007/02/01 14:59:31 iss Exp $
 *
 * Description: Prototypes for IGMP Snooping module's NP-API.
 *
 *******************************************************************/

#ifndef _NPIGS_H
#define _NPIGS_H
#include "igsnp.h"

#endif
