/***************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                    */
/*                                                                         */
/*  FILE NAME             : rstpbnpx.c                                     */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*  SUBSYSTEM NAME        : RSTP Module                                    */
/*  MODULE NAME           : RSTP                                           */
/*  LANGUAGE              : C                                              */
/*  TARGET ENVIRONMENT    : Any                                            */
/*  DATE OF FIRST RELEASE : 07 DEC 2006                                    */
/*  AUTHOR                : Aricent Inc.                                   */
/*  DESCRIPTION           : This file contains RSTP NP Routine             */
/*                                                                         */
/***************************************************************************/

#ifndef _RSTPBNPX_C_
#define _RSTPBNPX_C_

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fssnmp.h"
#include "fsvlan.h"
#include "pnac.h"
#include "rstp.h"
#include "mstp.h"
#include "la.h"

#include "npapi.h"
#include "nprstp.h"
#include "npcfa.h"

/*****************************************************************************/
/* Function Name      : FsPbRstpMbsmNpSetPortState                           */
/*                                                                           */
/* Description        : Sets the RSTP Port State in the Hardware.            */
/*                      When RSTP is working in RSTP mode                    */
/*                      or RSTP in STP compatible                            */
/*                      mode, porting should be done for this API.           */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Number (CEP IfIndex).               */
/*                      Svid      - S-Vlan Id correspoding to C-VLAN port 
 *                                  ranges <1-4094>.                         */
/*                      u1Status  - Status to be set.                        */
/*                                  Values can be AST_PORT_STATE_DISCARDING  */
/*                                  or AST_PORT_STATE_LEARNING               */
/*                                  or AST_PORT_STATE_FORWARDING             */
/*                      pSlotInfo - Pointer to the slot information 
 *                                  structure.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On successful set (or)                 */
/*                      FNP_FAILURE - Error during setting                   */
/*****************************************************************************/
INT1
FsPbRstpMbsmNpSetPortState (UINT4 u4IfIndex, tVlanId Svid, UINT1 u1Status,
                            tMbsmSlotInfo * pSlotInfo)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    UNUSED_PARAM (Svid);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}
#endif
