/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: difsrnpx.c,v 1.2 2007/02/01 14:59:31 iss Exp $
 *
 * Description: This file contains the function implementations  of DiffServ
 *              System related NP-API.
 ****************************************************************************/
#ifndef _DIFSRVNPX_C
#define _DIFSRVNPX_C

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "npapi.h"
#ifdef SWC
#include "dscxe.h"
#else
#include "diffsrv.h"
#endif
#include "npdifsrv.h"

/*****************************************************************************/
/* Function Name      : DsMbsmHwInit                                         */
/*                                                                           */
/* Description        : This function intialises the DiffServ in the device  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
DsMbsmHwInit (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsMbsmHwClassifierAdd                                */
/*                                                                           */
/* Description        : This function intialises the DiffServ in the device  */
/*                                                                           */
/* Input(s)           : tDiffServClfrEntry *pDsClfrEntry                     */
/*                      tDiffServMultiFieldClfrEntry *pDsMultiFieldClfrEntry */
/*                                                                           */
/*                      tDiffServInProfileActionEntry *                      */
/*                      pDsInProfileActionEntry                              */
/*                                                                           */
/*                      tDiffServOutProfileActionEntry *                     */
/*                      pDsOutProfileActionEntry                             */
/*                                                                           */
/*                      tDiffServMeterEntry * pDsMeterEntry                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
DsMbsmHwClassifierAdd (tDiffServClfrEntry * pDsClfrEntry,
                       tDiffServMultiFieldClfrEntry * pDsMultiFieldClfrEntry,
                       tDiffServInProfileActionEntry * pDsInProfileActionEntry,
                       tDiffServOutProfileActionEntry *
                       pDsOutProfileActionEntry,
                       tDiffServMeterEntry * pDsMeterEntry,
                       tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pDsClfrEntry);
    UNUSED_PARAM (pDsMultiFieldClfrEntry);
    UNUSED_PARAM (pDsInProfileActionEntry);
    UNUSED_PARAM (pDsOutProfileActionEntry);
    UNUSED_PARAM (pDsMeterEntry);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsMbsmHwSchedulerAdd                                 */
/*                                                                           */
/* Description        : This function intialises the DiffServ in the device  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
DsMbsmHwSchedulerAdd (tDiffServSchedulerEntry * pDsSchedulerEntry,
                      tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pDsSchedulerEntry);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}
#endif
