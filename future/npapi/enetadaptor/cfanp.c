/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfanp.c,v 1.38 2015/05/20 11:32:13 siva Exp $
 *
 * Description: This file contains the function implementations  of CFA NP-API.
 ****************************************************************************/
#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "la.h"
#include "fsvlan.h"
#include "npapi.h"
#include "npcfa.h"


#include "custnp.h"
#include "gddmain.h"
#include "iss.h"
#include "cfainc.h"
//#include "cfaport.h"
//#include "cfaifutl.h"
//#include "ifmmacs.h"

#include <fcntl.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <stdio.h>


#include "adap_types.h"
#include "adap_logger.h"
#include "EnetHal_L2_Api.h"

PRIVATE INT4        NpCfaGetPortsForTrunk (UINT2 u2Port, UINT2 *pu2PortList,
                                           UINT2 *pu2NumPorts);

#define NP_MAX_PACKET_SIZE 1600

//extern tOsixQId            			gCfaPktMuxQId;
//extern UINT1            			gu1CfaInitialised;
//#define   CFA_INITIALISED           gu1CfaInitialised

//extern    tOsixTaskId         		gCfaTaskId;
//#define   CFA_TASK_ID               gCfaTaskId
//#define   CFA_GDD_INTERRUPT_EVENT   0x00010000
/*************************************************************************
 *    Function Name      : FsCfaHwClearStats()                           *  
 *    Description        : This function clears the statistics collected *
 *                        on the Port.                                   *
 *                         This function must be called for the Physical *
 *                         Ports ONLY.                                   *
 *    Input(s)           : u4Port - Port Number.                         *
 *    Output(s)          : None.                                         *
 *                                                                       *
 *    Globals Referred         : None                                    *
 *    Globals Modified         : None                                    * 
 *    Exceptions or Operating
 *    System Error Handling    : None.                                   *
 *    Returns                  : CFA_NP_SUCCESS/CFA_NP_FAILURE           *
 *************************************************************************/
INT4
FsCfaHwClearStats (UINT4 u4Port)
{
	INT4 ret = FNP_SUCCESS;

    UNUSED_PARAM (u4Port);

#ifdef USER_HW_API	
    ret = EnetHal_FsCfaHwClearStats (u4Port);
#endif	
    return ret;
}

/*************************************************************************
 *                                                                       *
 *    Function Name        : FsHwUpdateAdminStatusChange                 *
 *                                                                       *
 *    Description          : This function is used to synchronise the    *
 *                           interface state of the data plane with that *
 *                           of  control plane.                          *       *
 *
 *    Input(s)             : u4IfIndex - Interface Index for which the    *
 *                                       Data Plane to be controlled.     *
 *                           u1AdminEnable - FNP_TRUE if Enabled otherwise*
 *                                           FNP_FALSE                    *
 *
 *    Output(s)            : None.                                        *
 *
 *    Global Variables Referred : None,                                   *
 *
 *    Global Variables Modified : None                                    *
 *
 *    Exceptions or Operating                                             * 
 *    System Error Handling   : None.                                     *
 *
 *    Use of Recursion        : None.                                     *
 *
 *    Returns                 : FNP_SUCCESS if succeeded, otherwise       * 
 *                              FNP_FAILURE                               *
 *                                                                        *
 **************************************************************************/
INT4
FsHwUpdateAdminStatusChange (UINT4 u4IfIndex, UINT1 u1AdminEnable)
{
	INT4 ret =FNP_SUCCESS;
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1AdminEnable);
#ifdef USER_HW_API	
    if(u1AdminEnable == FNP_TRUE)
    {
    	ret = EnetHal_FsHwUpdateAdminStatusChange (u4IfIndex,ADAP_ENABLE);
    }
    else
    {
    	ret = EnetHal_FsHwUpdateAdminStatusChange (u4IfIndex,ADAP_DISABLE);
    }
#endif	
    return ret;
}

UINT4 CfaGddSendIntrEvent (VOID)
{
    if (CFA_INITIALISED != TRUE)
    {
        return (CFA_FAILURE);
    }
    OsixEvtSend (CFA_TASK_ID, CFA_GDD_INTERRUPT_EVENT);
    return (CFA_SUCCESS);
}
/**************************************************************************
 *                                                                        *
 *    Function Name        : CfaReceivePacket                             *
 *                                                                        *
 *    Description          : This function created by Gil, this function  *
 *    						 is callback called when packet send to ISS   *
 *                            hardware                                    *
 *																		  *
 *    Input(s)             : None                                         *
 *                                                                        *
 *    Output(s)            : None.                                        *
 *                                                                        *
 *    Global Variables Referred : None,                                   *
 *                                                                        *
 *    Global Variables Modified : None                                    *
 *
 *    Exceptions or Operating                                             *
 *    System Error Handling   : None.                                     *
 *
 *    Use of Recursion        : None.                                     *
 *                                                                        *
 *    Returns                 : FNP_SUCCESS/FNP_FAILURE                   *
 *                                                                        *
 **************************************************************************/
static int CfaReceivePacket(UINT4 IfIndex,void *Buf,UINT4 len)
{
    tCRU_BUF_CHAIN_HEADER *pCruBuf;
//    UINT2               u2IfIndex;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT1               aRcvStaticBuf[NP_MAX_PACKET_SIZE];    /*Buffer used during packet Rx */
#ifdef PRINT_PACKETS
    UINT4		idx=0;
#endif


    if (CFA_INITIALISED != TRUE)
    {
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"cfa not initialize\r\n");
        return (CFA_FAILURE);
    }

    pCruBuf = (tCRU_BUF_CHAIN_HEADER *) CRU_BUF_Allocate_MsgBufChain (len, 0);

    if (pCruBuf == NULL)
    {
        /* call the FM fault handler and increment discard count */
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate buffer\r\n");
        if (IfIndex != 0)
        {
            CFA_IF_SET_IN_DISCARD (IfIndex);
        }
        return FNP_FAILURE;
    }
    //ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"before memset\r\n");
    memset (aRcvStaticBuf, 0, sizeof (aRcvStaticBuf));
    memcpy (aRcvStaticBuf, Buf, len);
    //ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"after memset\r\n");

    CRU_BUF_Copy_OverBufChain (pCruBuf, aRcvStaticBuf, 0, len);
    //ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"after copy len:%d\r\n",len);

    /* Insert received interface index into module data */
    pCruBuf->ModuleData.InterfaceId.u4IfIndex = IfIndex;
    //ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"after ifindex:%d\r\n",IfIndex);

#ifdef PRINT_PACKETS
	 printf("send packet len:%d ifIndex:%d to CFA\r\n",len,IfIndex);
	 for(idx=0;idx<len;idx++)
	 {
		 printf("0x%x ",aRcvStaticBuf[idx]);
		 if( ( (idx%16) == 0) && (idx!= 0) )
		 {
			 printf("\r\n");
		 }
	 }

#endif

    /* post the buffer to CFA queue */
     i4RetVal = OsixQueSend (gCfaPktMuxQId, (UINT1 *) &pCruBuf,
                             OSIX_DEF_MSG_LEN);
     if (i4RetVal != OSIX_SUCCESS)
     {
    	 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to forward packet to CFA\r\n");
         CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
         CFA_IF_SET_IN_DISCARD (IfIndex);
         return CFA_FAILURE;
     }
     //ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"after OsixQueSend:%d\r\n",IfIndex);
     if(CfaGddSendIntrEvent () != CFA_SUCCESS)
     {
    	 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to send CFA event\r\n");
     }

     //ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"packet sent to CFA ifIndex:%x len:%d\r\n",IfIndex,len);

     return FNP_SUCCESS;

}

/**************************************************************************
 *                                                                        *
 *    Function Name        : CfaRegisterWithNpDrv                         * 
 *                                                                        *
 *    Description          : This function is used to register with 
 *                            hardware                                    *
 *
 *    Input(s)             : None                                         *
 *
 *    Output(s)            : None.                                        *
 *
 *    Global Variables Referred : None,                                   *
 *
 *    Global Variables Modified : None                                    * 
 *
 *    Exceptions or Operating                                             *
 *    System Error Handling   : None.                                     *
 * 
 *    Use of Recursion        : None.                                     * 
 *                                                                        *
 *    Returns                 : FNP_SUCCESS/FNP_FAILURE                   *
 *                                                                        *
 **************************************************************************/
INT4
CfaRegisterWithNpDrv (VOID)
{
	INT4 ret =FNP_SUCCESS;
#ifdef USER_HW_API		
	ret = EnetHal_CfaRegisterWithNpDrv (CfaReceivePacket);
#endif	
    return ret;
}

INT4
CfaNpPortWrite (UINT1 *pu1DataBuf, UINT2 u2IfIndex, UINT4 u4PktSize)
{
	INT4 ret=FNP_SUCCESS;

	ret = EnetHal_CfaHwL3VlanIntfWrite (pu1DataBuf,u4PktSize,0,u2IfIndex,0,0);


	return ret;
}

/**************************************************************************
 *                                                                        *
 *    Function Name        : CfaHwL3VlanIntfWrite                         *
 *    Description          : This function is used to send the packet to 
 *                           the corresponding VLAN as a synchronized 
 *                           access.                                      *
 *                           
 *
 *    Input(s)             : pu1DataBuf - Data buffer                            
 *                           VlanId     - VLAN ID                         *
 *                           u4PktSize  - size of the packet to be 
 *                            transmitted                                 *
 *                           
 *    Output(s)            : None                                         *
 *
 *    Global Variables Referred : None                                    *
 *
 *    Global Variables Modified : None                                    *
 *
 *    Exceptions or Operating                                             *
 *    System Error Handling   : None.                                     *
 *
 *    Use of Recursion        : None.                                     *
 *
 *     Returns                 :FNP_SUCCESS/FNP_FAILURE                          *
 *                                                                        *
 **************************************************************************/
INT4
CfaHwL3VlanIntfWrite (UINT1 *pu1DataBuf, UINT4 u4PktSize, tCfaVlanInfo VlanInfo)
{
#ifdef USER_HW_API	
    if (CfaL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo) != CFA_SUCCESS)
    {
        return (FNP_FAILURE);
    }
#endif	// USER_HW_API	
    return FNP_SUCCESS;
}

/**************************************************************************/
/*                                                                        */
/*    Function Name        : FsCfaHwMacLookup                             */
/*                                                                        * 
 *    Description          : This function returns the port on which the  */
/*                           given mac address is learned.                */
/*
 *    Input(s)             : pu1MacAddr - Mac Address                     */
/*                           VlanId     - VLAN ID                         */
/*                           
 *    Output(s)            : pu1Port    - port on which pu1MacAddr is     */
/*                           learned. This port can also be a port 
 *                            channel.                                    */
/*                                                                        */
/*    Global Variables Referred : None                                    */
/*                                                                        */
/*    Global Variables Modified : None                                    */
/*                                                                        */
/*    Exceptions or Operating                                             */
/*    System Error Handling   : None.                                     */
/*                                                                        */
/*    Use of Recursion        : None.                                     */
/*    Returns                 :FNP_SUCCESS/FNP_FAILURE                    */
/*                                                                        */
/**************************************************************************/

INT4
FsCfaHwMacLookup (UINT1 *pu1MacAddr, tVlanIfaceVlanId VlanId, UINT2 *pu2Port)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (pu1MacAddr);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pu2Port);
#ifdef USER_HW_API	
#endif
    return ret;
}

/**************************************************************************/
/*                                                                        */
/*    Function Name      : FsCfaHwGetIfFlowControl                        */
/*                                                                        */
/*    Description        : This function is used to get the Flow Control  */
/*                         Status from the Hardware.                      */
/*                                                                        */
/*    Input(s)           : u4IfIndex - Interface index of the port        */
/*                         to get the flow control                        */
/*                                                                        */
/*    Output(s)          : pi4PortFlowControl - can take                  */
/*                         ENET_FLOW_ON,                                  */
/*                         ENET_FLOW_OFF.                                 */
/*                                                                        */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                        */
/**************************************************************************/
INT4
FsCfaHwGetIfFlowControl (UINT4 u4IfIndex, UINT4 *pu4PortFlowControl)
{
    /* NOTE: Call the required Driver API to get the FlowControl status. 
     * If Flow Control is enabled on the Port, assign the value ENET_FLOW_ON
     * to pu4PortFlowControl. Else Assign ENET_FLOW_OFF. 
     */
	INT4 ret =FNP_SUCCESS;
	
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4PortFlowControl);
#ifdef USER_HW_API	
#endif
    return ret;
}

/**************************************************************************
 *                                                                        *
 *    Function Name             : FsCfaHwSetMtu                           *
 *    Description               : Configures the given MTU                * 
 *    Input(s)                  : InterfaceId, MTU size can take          *
 *                                                < 90-9202 >             *
 *    Output(s)                 : None                                    *
 *    Global Variables Referred : None                                    *
 *    Global Variables Modified : None                                    *
 *    Exceptions or Operating                                             *
 *    System Error Handling     : None.                                   *
 *    Use of Recursion          : None.                                   *  
 *    Returns                   : FNP_SUCCESS/FNP_FAILURE                 *
 *                                                                        *
 **************************************************************************/
INT4
FsCfaHwSetMtu (UINT4 u4IfIndex, UINT4 u4MtuSize)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4MtuSize);
#ifdef USER_HW_API	
	ret = EnetHal_FsCfaHwSetMtu (u4IfIndex,u4MtuSize);
#endif
    return ret;
}

/**************************************************************************
 *                                                                        *
 *    Function Name             : FsCfaHwSetMacAddr                       *
 *    Description               : Configures the given Macaddress         * 
 *    Input(s)                  : InterfaceId, MacAddress                 *
 *    Output(s)                 : None                                    *
 *    Global Variables Referred : None                                    *
 *    Global Variables Modified : None                                    *
 *    Exceptions or Operating                                             *
 *    System Error Handling     : None.                                   *
 *    Use of Recursion          : None.                                   *  
 *    Returns                   : FNP_SUCCESS/FNP_FAILURE                 *
 *                                                                        *
 **************************************************************************/
INT4
FsCfaHwSetMacAddr (UINT4 u4IfIndex, tMacAddr PortMac)
{
	INT4 ret=FNP_SUCCESS;

#ifdef USER_HW_API
	struct ifreq		ifr;	/* struct which contains status of interface */
	ADAP_Int32			i4CommandStatus;
	tEnetHal_MacAddr    enet_mac;

	if(u4IfIndex == 0)
	{
		/* set MAC address */
		memcpy((char *) &ifr.ifr_hwaddr, PortMac, sizeof(tEnetHal_MacAddr));

		if ((i4CommandStatus = ioctl (CFA_GDD_PORT_DESC (u4IfIndex),
									  SIOCSIFHWADDR, &ifr)) < 0)
		{
			printf ("Error in setting MAC address status:%d\n",i4CommandStatus);
			return (CFA_FAILURE);
		}
	}
	else
	{
		adap_mac_from_arr(&enet_mac, PortMac);
		ret = EnetHal_FsCfaHwSetMacAddr(u4IfIndex,&enet_mac);
	}
#endif
    return ret;
}

PUBLIC VOID
CfaNpUpdateSwitchMac (tMacAddr pSwitchMac)
{
#ifdef USER_HW_API
	tEnetHal_MacAddr enet_mac;
	adap_mac_from_arr(&enet_mac, pSwitchMac);
	EnetHal_CfaNpUpdateSwitchMac(&enet_mac);
#else
	UNUSED_PARAM(pSwitchMac);
#endif
}

/***************************************************************************
 *    Function Name      : FsHwGetStat                                     *
 *    Description        : To retrieve info from a specified port (Used 
 *                         by RMON and Bridge module)                      *
 *    Input(s)           : i4IfIndex - Interface index of the port         *
 *                         StatType  - can take value NP_STAT_IF_IN_OCTETS *
 *                                     etc.                                *
 *    Output(s)          : pu4Value  - Requested info                      *
 *    Globals Referred         : None                                      *
 *    Globals Modified         : None                                      *
 *    Exceptions or Operating                                              *
 *    System Error Handling    : None                                      *
 *    Returns                  : FNP_SUCCESS / FNP_FAILURE                 *
 ***************************************************************************/
INT4
FsHwGetStat (UINT4 u4IfIndex, INT1 i1StatType, UINT4 *pu4Value)
{

    UINT2               au2ConfPorts[LA_MAX_PORTS];
    UINT2               u2NumPorts;
    INT4 				ret=FNP_SUCCESS;
	
	
#ifdef USER_HW_API	
    if (i1StatType == NP_STAT_UNSUPPORTED)
    {
        /* Unsupported Statistics in the BCM */
        return (FNP_FAILURE);
    }
    /* This function gets the member port list if the u4IfIndex is a trunk
     * else just the u4IfIndex
     */
    if (NpCfaGetPortsForTrunk ((UINT2) u4IfIndex, au2ConfPorts, &u2NumPorts) ==
        FNP_FAILURE)
    {
        return (FNP_FAILURE);
    }
    ret = EnetHal_FsHwGetStat (u4IfIndex,i1StatType,pu4Value);
    UNUSED_PARAM (pu4Value);

#endif	
    return ret;
    /* Device specific code to get the statistics for each port in au2ConfPorts */
}

/***************************************************************************
 *    Function Name      : FsHwGetStat64                                   *
 *    Description        : To retrieve info from a specified port (Used by *
 *                         RMON and Bridge module)                         * 
 *    Input(s)           : i4IfIndex - Interface index of the port         *
 *                         StatType  - can take value NP_STAT_IF_IN_OCTETS *
 *                         etc.                                            *
 *    Output(s)          : pu4Value  - Requested info                      *
 *    Globals Referred         : None                                      *
 *    Globals Modified         : None                                      * 
 *    Exceptions or Operating                                              *
 *    System Error Handling    : None                                      *
 *    Returns                  : FNP_SUCCESS / FNP_FAILURE                 *
 ***************************************************************************/
INT4
FsHwGetStat64 (UINT4 u4IfIndex, INT1 i1StatType, tSNMP_COUNTER64_TYPE * pValue)
{

    UINT2               au2ConfPorts[LA_MAX_PORTS];
    UINT2               u2NumPorts;
    INT4				ret=FNP_SUCCESS;

#ifdef USER_HW_API		
    if (i1StatType == NP_STAT_UNSUPPORTED)
    {
        /* Unsupported Statistics in the BCM */
        return (FNP_FAILURE);
    }

    /* This function gets the member port list if the u4IfIndex is a trunk
     * else just the u4IfIndex
     */

    if (NpCfaGetPortsForTrunk ((UINT2) u4IfIndex, au2ConfPorts, &u2NumPorts) ==
        FNP_FAILURE)
    {
        return (FNP_FAILURE);
    }

    UNUSED_PARAM (pValue);
    /* Device specific code to get the statistics for each port in au2ConfPorts */

    ret = EnetHal_FsHwGetStat64 (u4IfIndex,i1StatType,(tEnetHal_COUNTER64_TYPE *)pValue);
#endif
    return ret;
}

/***************************************************************************
 *    Function Name        : CfaNpGetLinkStatus                            *
 *    Description         : This function  retrives the status of the Link *
 *    Input(s)            : None.                                          *
 *    Output(s)            : None.                                         *
 *    Global Variables Referred : FdTable                                  *
 *    Global Variables Modified : FdTable                                  *
 *    Exceptions or Operating                                              *
 *    System Error Handling    : None.                                     *
 *    Use of Recursion        : None.                                      *
 *    Returns            : CFA_IF_DOWN if the Link is Down                 *
 *                         else, CFA_IF_UP.                                *
 *                                                                         *
 ***************************************************************************/
UINT1
CfaNpGetLinkStatus (UINT4 u4IfIndex)
{
	UINT1 ret=CFA_IF_UP;

#ifdef USER_HW_API
	ret = EnetHal_CfaNpGetLinkStatus (u4IfIndex);
#else
	UNUSED_PARAM(u4IfIndex);
#endif

    return ret;
}

/****************************************************************************
 * Description:            PRIVATE FUNCTIONS 
 ****************************************************************************/

/****************************************************************************
 *                                                                          * 
 *    Function Name        : NpCfaGetPortsForTrunk                          *
 *                                                                          *
 *    Description          : This function checks whether the given port is * 
 *                           a port channel, if so returns the ports in the *
 *                           port channel otherwise returns the same port   *
 *                                                                          *
 *                                                                          *
 *    Input(s)             : u2Port - port number                           *
 *                                                                          * 
 *    Output(s)            : pu2PortList - member ports in u2Port if it is  *
 *                           port channel                                   *
 *                           otherwise u2Port itself                        *
 *                         : pu2NumPorts - Number of ports returned.        *
 *                                                                          *
 *    Global Variables Referred : None                                      *
 *
 *     Global Variables Modified : None                                     * 
 *
 *    Exceptions or Operating                                               *
 *    System Error Handling   : None.                                       *
 *                                                                          *
 *    Use of Recursion        : None.                                       *
 *                                                                          *
 *    Returns                 : FNP_SUCCESS on Success or                   *
 *                              FNP_FAILURE on Failure                      *
 *                                                                          *
 ****************************************************************************/

PRIVATE INT4
NpCfaGetPortsForTrunk (UINT2 u2Port, UINT2 *pu2PortList, UINT2 *pu2NumPorts)
{
#ifdef LA_WANTED

    if (CFA_NP_IS_PORT_CHANNEL (u2Port) == FNP_TRUE)

    {
        /* Get Ports for the given aggindex. */
    }
    else
#endif /* LA_WANTED */
    {
        *pu2PortList = u2Port;
        *pu2NumPorts = 1;
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                         *
 *    Function Name   : FsHwGetEthernetType                                *
 *                                                                         *
 *    Description     : This function will get the ethernet type of a      *
 *                      particular IfIndex                                 *
 *                                                                         *
 *    Input(s)        : u2IfIndex - Interface Index.                       *
 *                                                                         *
 *    Output(s)       : pu1EtherType can take CFA_FA_ENET,CFA_GI_ENET.     *
 *                                                                         * 
 *    Returns         : FNP_SUCCESS or FNP_FAILURE                         *
 *                                                                         *
 ***************************************************************************/
INT4
FsHwGetEthernetType (UINT4 u4IfIndex, UINT1 *pu1EtherType)
{
    UNUSED_PARAM (u4IfIndex);
#ifdef USER_HW_API	
    INT4 ret=FNP_SUCCESS;
    INT4 i4PortSpeed = 0;

    ret = EnetHal_HwGetPortSpeed (u4IfIndex,&i4PortSpeed);
    if (i4PortSpeed == ENET_1GB) 
    {
        *pu1EtherType = CFA_GI_ENET;
    }
    else if (i4PortSpeed == ENET_10GB)
    {
        *pu1EtherType = CFA_XE_ENET;
    }
    else if (i4PortSpeed == ENET_40GB)
    {
        *pu1EtherType = CFA_XL_ENET;
    }
#endif	
    return ret;
}

/***************************************************************************
 *    Function Name             : FsNpCfaSetDlfStatus                      *
 *    Description               : This function registers/de-registers to  *
 *                                get the L3 the DLF packets to CPU        *
 *    Input(s)                  : True/False                               *
 *    Output(s)                 : None.                                    *
 *    Global Variables Referred : None                                     *   
 *    Global Variables Modified : None                                     * 
 *    Exceptions or Operating                                              *
 *    System Error Handling     : None.                                    *
 *    Use of Recursion          : None.                                    *
 *    Returns                   : FNP_FAILURE on Failure.                  *
 *                                FNP_SUCCESS on Success.                  *
 *                                                                         *
 ***************************************************************************/
PUBLIC INT4
FsNpCfaSetDlfStatus (UINT1 u1Status)
{
	INT4 ret=FNP_FAILURE;

    UNUSED_PARAM (u1Status);
#ifdef USER_HW_API	
#endif	
    return ret;
}

/********************************************************************
 * Function Name        -  FsHwSetCustIfParams                      *
 * Function Description -  To set custom interface parameters       *
 *                         in the hardware.                         *
 * Input description    -  u4IfIndex - Interface index of the port  *
 *                         HwCustParamType - Type of the custom     *
 *                         parameter passed.                        *
 *                         CustIfParamVal - The custom parameter    *
 *                         passed                                   *
 *                         EntryAction - Create or Delete entry     *
 * Output               -  FNP_SUCCESS/FNP_FAILURE                  *
 ********************************************************************/
INT4
FsHwSetCustIfParams (UINT4 u4IfIndex, tHwCustIfParamType HwCustParamType,
                     tHwCustIfParamVal CustIfParamVal,
                     tNpEntryAction EntryAction)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (HwCustParamType);
    UNUSED_PARAM (CustIfParamVal);
    UNUSED_PARAM (EntryAction);
#ifdef USER_HW_API	
#endif
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsCfaHwCreateILan                                    */
/*                                                                           */
/* Description        : This function is called to create the I-LAN with     */
/*                      of ports in the hardware.                            */
/*                                                                           */
/* Input(s)           : u4ILanIndex - I-LAN Identifier.                      */
/*                      ILanPortArray - Array of I-LAN Ports                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsCfaHwCreateILan (UINT4 u4ILanIndex, tHwPortArray ILanPortArray)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ILanIndex);
    UNUSED_PARAM (ILanPortArray);
#ifdef USER_HW_API	
#endif	
    return ret;
}

/*****************************************************************************/
/* Function Name      : FsCfaHwDeleteILan                                    */
/*                                                                           */
/* Description        : This function is called to delete the created I-LAN  */
/*                      created for PBB module.                              */
/*                                                                           */
/* Input(s)           : u4ILanIndex - I-LAN Identifier.                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsCfaHwDeleteILan (UINT4 u4ILanIndex)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ILanIndex);
#ifdef USER_HW_API	
#endif	
    return ret;
}

/*****************************************************************************/
/* Function Name      : FsCfaHwAddPortToILan                                 */
/*                                                                           */
/* Description        : This function is called to add a port to I-LAN       */
/*                      created for PBB module.                              */
/*                                                                           */
/* Input(s)           : u4ILanIndex - I-LAN Identifier.                      */
/*                      u4PortIfIndex - Port Identifier                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsCfaHwAddPortToILan (UINT4 u4ILanIndex, UINT4 u4PortIndex)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ILanIndex);
    UNUSED_PARAM (u4PortIndex);
#ifdef USER_HW_API	
#endif	
    return ret;
}

/*****************************************************************************/
/* Function Name      : FsCfaHwRemovePortFromILan                            */
/*                                                                           */
/* Description        : This function is called to remove the port from I-LAN*/
/*                                                                           */
/* Input(s)           : u4ILanIndex - I-LAN Identifier.                      */
/*                      u4PortIfIndex - Port Identifier                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsCfaHwRemovePortFromILan (UINT4 u4ILanIndex, UINT4 u4PortIndex)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ILanIndex);
    UNUSED_PARAM (u4PortIndex);
#ifdef USER_HW_API		
#endif	
    return ret;
}

/*****************************************************************************/
/* Function Name      : FsCfaHwCreateInternalPort                            */
/*                                                                           */
/* Description        : This function is called to create the Internal Ports */
/*                      in the hardware.                                     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Identifier.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsCfaHwCreateInternalPort (UINT4 u4IfIndex)
{
	INT4 ret=FNP_SUCCESS;

    UNUSED_PARAM (u4IfIndex);
#ifdef USER_HW_API	
#endif	
    return ret;
}

/*****************************************************************************/
/* Function Name      : FsCfaHwDeleteInternalPort                            */
/*                                                                           */
/* Description        : This function is called to delete the Internal Ports */
/*                      in the hardware.                                     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Identifier.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsCfaHwDeleteInternalPort (UINT4 u4IfIndex)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4IfIndex);
#ifdef USER_HW_API	
#endif	
    return ret;
}

/*****************************************************************************
 *    Function Name        : CfaNpGetPhyAndLinkStatus
 *    Description         : This function  retrives the status of the Phy
 *                          and Link.
 *    Input(s)            : None.
 *    Output(s)            : None.
 *    Global Variables Referred : FdTable
 *    Global Variables Modified : FdTable
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Use of Recursion        : None.
 *    Returns            : CFA_IF_DOWN if the Link is Down or PHY is DOWN
 *                         else, CFA_IF_UP if Link is UP and PHY is up.
 *
 *****************************************************************************/
PUBLIC UINT1
CfaNpGetPhyAndLinkStatus (UINT2 u2IfIndex)
{
	UINT1 ret=CFA_IF_DOWN;
    UNUSED_PARAM (u2IfIndex);
#ifdef USER_HW_API	
	ret = EnetHal_CfaNpGetPhyAndLinkStatus (u2IfIndex);
#endif	
    return ret;
}

/*****************************************************************************
 *    Function Name             : FsCfaHwGetMtu
 *    Description               : Gets the MTU pf an Interface
 *    Input(s)                  : IfIndex: Port Interface index
 *    Output(s)                 : pu4MtuSize: MTU value
 *    Returns                   : FNP_SUCCESS/FNP_FAILURE 
 *****************************************************************************/
INT4
FsCfaHwGetMtu (UINT2 u2IfIndex, UINT4 *pu4MtuSize)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u2IfIndex);
    UNUSED_PARAM (pu4MtuSize);
#ifdef USER_HW_API	
#endif	
    return ret;
}

/***************************************************************************
*    Function Name        : NpCfaFrontPanelPorts                          *
*    Description          : This function  sets the maximum front panel   *
*                           ports                                         *
*    Input(s)             : u4MaxFrontPanelPorts                          *
*    Output(s)            : None.                                         *
*    Global Variables Referred : None                                     *
*    Global Variables Modified : gIssMaxFrontPanelPorts                   *
*    Returns                   : FNP_SUCCESS - On Success                 *
*                                                                         *
***************************************************************************/
INT4
NpCfaFrontPanelPorts (UINT4 u4MaxFrontPanelPorts)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4MaxFrontPanelPorts);
#ifdef USER_HW_API	
#endif	
    return ret;
}

/*****************************************************************************
 *
 *    Function Name   : FsCfaHwCreatePktFilter 
 *
 *    Description     : This function will create the Filters in hardware.
 *
 *    Input(s)        : pFilterInfo  - Filter information 
 *    
 *    Output(s)       : pu4FilterId  - Filter ID, created in the hardware
 *
 *    Returns         : FNP_SUCCESS or FNP_FAILURE
 *
 *****************************************************************************/

INT4
FsCfaHwCreatePktFilter (tHwCfaFilterInfo * pFilterInfo, UINT4 *pu4FilterId)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (pFilterInfo);
    UNUSED_PARAM (pu4FilterId);
#ifdef USER_HW_API	
#endif	
    return ret;
}

/*****************************************************************************
 *
 *    Function Name   : FsCfaHwDeletePktFilter 
 *
 *    Description     : This function will delete the Filters in hardware.
 *    Output(s)       : None                                                 
 *
 *    Returns         : FNP_SUCCESS or FNP_FAILURE
 *
 *****************************************************************************/

INT4
FsCfaHwDeletePktFilter (UINT4 u4FilterId)
{
    UNUSED_PARAM (u4FilterId);
#ifdef USER_HW_API	
#endif	
    return FNP_SUCCESS;
}

/******************************************************************************
 *
 *    Function Name   : FsCfaHwSetWanTye
 *
 *    Description     : This function will set the port as WAN/LAN in hardware.
 *
 *    Input(s)        : pCfaWanInfo - Port Info
 *
 *    Output(s)       : None
 *
 *    Returns         : FNP_SUCCESS or FNP_FAILURE
 *
 *****************************************************************************/
INT4
FsCfaHwSetWanTye (tIfWanInfo * pCfaWanInfo)
{
    INT4 ret=FNP_SUCCESS;
#ifdef USER_HW_API	
    tEnetHal_IfWanInfo CfaWanInfo;

    adap_mac_from_arr(&CfaWanInfo.MacAddr, pCfaWanInfo->MacAddr);
    CfaWanInfo.u1Opcode = pCfaWanInfo->u1Opcode;
    CfaWanInfo.u4IfIndex = pCfaWanInfo->u4IfIndex;
	ret = EnetHal_FsCfaHwSetWanTye (&CfaWanInfo);
#else
	UNUSED_PARAM(pCfaWanInfo);
#endif	
    return ret;
}

/******************************************************************************
 *
 *    Function Name   : CfaNpSetHwPortInfo
 *
 *    Description     : This function sets the Hardware port Information
 *
 *
 *    Input(s)        : pHwPortInfo
 *
 *    Output(s)       : None
 *                     
 *                     
 *
 *
 *    Returns         : None
 *
 *****************************************************************************/
VOID
CfaNpSetHwPortInfo (tHwPortInfo HwPortInfo)
{
    UNUSED_PARAM (HwPortInfo);
#ifdef USER_HW_API	
#endif	
    return;
}

/******************************************************************************
 *
 *    Function Name   : CfaNpGetHwPortInfo
 *
 *    Description     : This function is used to retrieve the Hardware port Information
 *
 *
 *    Input(s)        : pHwPortInfo
 *
 *    Output(s)       : None
 *
 *
 *    Returns         : None
 *
 *****************************************************************************/
VOID
CfaNpGetHwPortInfo (tHwPortInfo * pHwPortInfo)
{
    UNUSED_PARAM (pHwPortInfo);
#ifdef USER_HW_API
        INT4                i4PortSpeed = 0;
        if (pHwPortInfo->u1MsgType == ISS_NP_GET_DEFAULT_SPEED)
        {
            if (EnetHal_HwGetPortSpeed (pHwPortInfo->u4StartIfIndex,&i4PortSpeed) == ENET_SUCCESS)
            {
                pHwPortInfo->i4MaxPortSpeed = i4PortSpeed;
            }
        }
#endif
    return;
}

/******************************************************************************
 *
 *    Function Name   : CfaNpSetStackingModel
 *
 *    Description     : This function updates the stacking model to NP layer.
 *
 *
 *    Input(s)        : u4StackingModel - Stacking Model.
 *
 *    Output(s)       : None
 *
 *
 *    Returns         : FNP_SUCCESS
 *
 *****************************************************************************/
INT4
CfaNpSetStackingModel (UINT4 u4StackingModel)
{
    UNUSED_PARAM (u4StackingModel);
#ifdef USER_HW_API	
#endif
    return FNP_SUCCESS;
}

/******************************************************************************
 *
 *    Function Name   : CfaNpGetHwInfo
 *
 *    Description     : This function is used to retrieve the Hardware
 *                      Information
 *
 *
 *    Input(s)        : pHwInfo
 *
 *    Output(s)       : None
 *
 *
 *    Returns         : None
 *
 *****************************************************************************/
INT4
CfaNpGetHwInfo (tHwIdInfo * pHwIdInfo)
{
    UNUSED_PARAM (pHwIdInfo);
#ifdef USER_HW_API	
#endif	
    return FNP_SUCCESS;
}

/******************************************************************************
 *
 *    Function Name   : CfaNpRemoteSetHwInfo
 *
 *    Description     : This function is used to set Remote's Hardware
 *                      Information.
 *
 *
 *    Input(s)        : pHwInfo
 *
 *    Output(s)       : None
 *
 *
 *    Returns         : None
 *
 *****************************************************************************/
INT4
CfaNpRemoteSetHwInfo (tHwIdInfo * pHwIdInfo)
{
    UNUSED_PARAM (pHwIdInfo);
#ifdef USER_HW_API	
#endif	
    return FNP_SUCCESS;
}

/*****************************************************************************
 *    Function Name            : FsCfaHwSendIPCMsg
 *    Description              : Transmits packet to the peer CPU through the
 *                               connecting Interface.
 *    Input(s)                 : pHwInfo - Pointer to Hardware Info
 *                                         It contains all required Information
 *                                         Message Type,If-Index and other
 *                                        parameters based on Mesage Type
 *
 *    Output(s)                : None
 *    Globals Referred         : None
 *    Globals Modified         : None
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Returns                  : FNP_SUCCESS/FNP_FAILURE
 *****************************************************************************/
INT4
FsCfaHwSendIPCMsg (tHwInfo * pHwInfo)
{
    UNUSED_PARAM (pHwInfo);
#ifdef USER_HW_API	
#endif	
    return FNP_SUCCESS;
}

/*****************************************************************************
*    Function Name      : FsHwGetVlanIntfStats
*    Description        : To retrieve info from a specified L3 interface
*    Input(s)           : u4IfIndex - Interface index of the port
*                       : i1StatType  - Type of info needed
*    Output(s)          : pu4Value  - Requested info
*    Globals Referred         : None
*    Globals Modified         : gaL3VlanHwStatsCntrId
*    Exceptions or Operating
*    System Error Handling    : None
*    Returns                  : FNP_SUCCESS / FNP_FAILURE
*******************************************************************************/
INT4
FsHwGetVlanIntfStats (UINT4 u4IfIndex, INT1 i1StatType, UINT4 *pu4Value)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i1StatType);
    UNUSED_PARAM (pu4Value);
#ifdef USER_HW_API	
#endif	
    return ret;
}
/******************************************************************************
 *
 *    Function Name   : FsCfaHwRemoveIpNetRcvdDlfInHash
 *
 *    Description     : This function deletes the Hash Entry for the provided
 *                      IP address and mask
 *
 *    Input(s)        : u4ContextId - Context Id
 *                      u4IpAddress - IP address
 *                      u4IpSubNet - Subnet mask
 *
 *    Output(s)       : None
 *
 *
 *    Returns         : FNP_SUCCESS/FNP_FAILURE
 *
 *****************************************************************************/
INT4  FsCfaHwRemoveIpNetRcvdDlfInHash (UINT4 u4ContextId, UINT4 u4IpNet, UINT4 u4IpMask)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IpNet);
    UNUSED_PARAM (u4IpMask);
#ifdef USER_HW_API	
#endif	
    return ret;
}

#ifdef IP6_WANTED
/******************************************************************************
 *
 *    Function Name   : FsCfaHwRemoveIp6NetRcvdDlfInHash
 *
 *    Description     : This function deletes the Hash Entry for the provided
 *                      IP6 address and prefix
 *
 *    Input(s)        : u4ContextId - Context Id
 *                      Ip6Addr - IP6 address
 *                      u4Prefix - Prefix
 *
 *    Output(s)       : None
 *
 *
 *    Returns         : FNP_SUCCESS/FNP_FAILURE
 *
 *****************************************************************************/
INT4  FsCfaHwRemoveIp6NetRcvdDlfInHash (UINT4 u4ContextId, tIp6Addr Ip6Addr, UINT4 u4Prefix)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (Ip6Addr);
    UNUSED_PARAM (u4Prefix);
    return FNP_SUCCESS;
}
#endif
/***************************************************************************
 *    Function Name      : IssHwGetStat64                                  *
 *    Description        : To retrieve info from a specified port (Used by *
 *                         customerized counter such as CRC or symbol error*
 *    Input(s)           : i4IfIndex - Interface index of the port         *
 *    Output(s)          : pPortCounters  - Requested counters             *
 *    Globals Referred         : None                                      *
 *    Globals Modified         : None                                      *
 *    Exceptions or Operating                                              *
 *    System Error Handling    : None                                      *
 *    Returns                  : FNP_SUCCESS / FNP_FAILURE                 *
 ***************************************************************************/

INT4
IssHwGetStat64 (UINT4 u4IfIndex, tIssPortCounters *pPortCounters)
{
	INT4 ret=FNP_SUCCESS;
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pPortCounters);
#ifdef USER_HW_API	
	ret = EnetHal_FsHwGetStat64 (u4IfIndex,ENET_NP_COUNT_RX_FCS_ERRORS, (tEnetHal_COUNTER64_TYPE *)&pPortCounters->cntRxFCSErrors);
	ret = EnetHal_FsHwGetStat64 (u4IfIndex,ENET_NP_COUNT_RX_FRAME_SIZE_ERRORS, (tEnetHal_COUNTER64_TYPE *)&pPortCounters->cntRxFrameSizeErrors);
	ret = EnetHal_FsHwGetStat64 (u4IfIndex,ENET_NP_COUNT_RX_JABBER_PACKETS, (tEnetHal_COUNTER64_TYPE *)&pPortCounters->cntRxJabberPkts);
	ret = EnetHal_FsHwGetStat64 (u4IfIndex,ENET_NP_COUNT_RX_SYMBOLS_ERRORS, (tEnetHal_COUNTER64_TYPE *)&pPortCounters->cntRxSymbolErrors);
	ret = EnetHal_FsHwGetStat64 (u4IfIndex,ENET_NP_COUNT_TX_DROP_PACKETS, (tEnetHal_COUNTER64_TYPE *)&pPortCounters->cntTxErrorDropPkts);
	ret = EnetHal_FsHwGetStat64 (u4IfIndex,ENET_NP_COUNT_TX_FCS_ERRORS, (tEnetHal_COUNTER64_TYPE *)&pPortCounters->cntTxFCSErroredPkts);
	ret = EnetHal_FsHwGetStat64 (u4IfIndex,ENET_NP_COUNT_TX_TIMEOUT_PACKETS, (tEnetHal_COUNTER64_TYPE *)&pPortCounters->cntTxTimeOutPkts);
#endif	
    return ret;
}
/*****************************************************************************
 *
 *    Function Name             : FsCfaHwL3SetMtu
 *    Description               : Configures the given MTU for a L3 Interface
 *    Input(s)                  : Structure tL3MtuInfo
 *    Output(s)                 : None
 *    Global Variables Referred : None
 *    Global Variables Modified : None
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *    Use of Recursion          : None.
 *    Returns                   : FNP_SUCCESS/FNP_FAILURE 
 *
 *****************************************************************************/

INT4
FsCfaHwL3SetMtu (tL3MtuInfo L3MtuInfo)
{
    UNUSED_PARAM (L3MtuInfo);
    return FNP_SUCCESS;
}
