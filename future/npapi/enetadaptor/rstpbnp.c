
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rstpbnp.c,v 1.5 2007/07/17 13:29:53 iss Exp $
 *
 * Description: All network processor function  given here
 *******************************************************************/

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "rstp.h"
#include "npapi.h"
#include "nprstpb.h"
#include "npcfa.h"

/*****************************************************************************/
/* Function Name      : FsPbRstNpSetPortState                                */
/*                                                                           */
/* Description        : This function is used to set  the PEP Port State.    */
/*                      PEP is Part of the CVLAN component.This NPAPI is used*/
/*                      only when the bridge is operation in provider bridge */
/*                      Mode.                                                */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u4IfIndex   - CEP Port Number.                       */
/*                      SVlanId     - Service VLAN id ranges <1-4094>        */
/*                      u1Status  - Values can be AST_PORT_STATE_DISCARDING  */
/*                                  or AST_PORT_STATE_LEARNING               */
/*                                  or AST_PORT_STATE_FORWARDING             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On successful set (or)                 */
/*                      FNP_FAILURE - Error during setting                   */
/*****************************************************************************/

INT1
FsPbRstpNpSetPortState (UINT4 u4IfIndex, tVlanId SVlanId, UINT1 u1Status)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (u1Status);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsPbRstNpGetPortState                                */
/*                                                                           */
/* Description        : This function is used to get the PEP port state from */
/*                      the hardware. This function will be called only in   */
/*                      Provider Edge Bridge and only for PEPs.              */
/*                                                                           */
/* Input(s)           : u4IfIndex   - CEP IfIndex                            */
/*                      SVlanId     - Service VLAN id ranges <1-4094>        */
/*                                                                           */
/* Output(s)          : pu1Status - Port state in hardware. It can be:       */
/*                                  AST_PORT_STATE_DISCARDING                */
/*                                  or AST_PORT_STATE_LEARNING               */
/*                                  or AST_PORT_STATE_FORWARDING             */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On successful get (or)                 */
/*                      FNP_FAILURE - Error during get operation             */
/*****************************************************************************/
INT1
FsPbRstNpGetPortState (UINT4 u4IfIndex, tVlanId SVlanId, UINT1 *pu1Status)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (pu1Status);
    return FNP_SUCCESS;
}
