/*******************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: pbbnp.c,v 1.3 2009/09/24 14:21:46 prabuc Exp $
 *
 * Description: This file contains Npapi Stub wrappers used in PBB moodule.
 *
 *******************************************************************/
#ifdef PBB_WANTED

#ifndef _PBBNP_C_
#define _PBBNP_C_

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "npapi.h"
#include "pbbnp.h"

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsMiPbbHwDelBackboneServiceInstEntry             */
/*                                                                          */
/*    Description        : This function is used to del Bacbone service Instance*/
/*                                                                          */
/*    Input(s)           : Context id
                           i4IfIndex
                           u4Isid
*/
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT4
FsMiPbbHwDelBackboneServiceInstEntry (UINT4 u4ContextId,
                                      UINT4 u4IfIndex, UINT4 u4Isid)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4Isid);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsMiPbbHwDelVipAttributes                        */
/*                                                                          */
/*    Description        : This function is used to Bacbone service Instance */
/*                                                                          */
/*    Input(s)           : Context id
                           i4IfIndex
*/
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT4
FsMiPbbHwDelVipAttributes (UINT4 u4ContextId, UINT4 u4IfMainIndex)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfMainIndex);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsMiPbbHwSetBackboneServiceInstEntry                        */
/*                                                                          */
/*    Description        : This function is used to Bacbone service Instance */
/*                                                                          */
/*    Input(s)           : Context id
                           i4IfIndex
                           u4Isid
                           ServiceInst
*/
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT4
FsMiPbbHwSetBackboneServiceInstEntry (UINT4 u4ContextId,
                                      UINT4 u4ifIndex,
                                      UINT4 u4Isid,
                                      tBackboneServiceInstEntry ServiceInst)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4ifIndex);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (ServiceInst);
    return FNP_SUCCESS;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsMiPbbHwSetVipAttributes                        */
/*                                                                          */
/*    Description        : This function is used to Set Vip Attributes      */
/*                                                                          */
/*    Input(s)           : Context id
                           i4IfIndex
                           ServiceInst
*/
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT4
FsMiPbbHwSetVipAttributes (UINT4 u4ContextId, UINT4 u4ifIndex,
                           tVipAttribute ServiceInst)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4ifIndex);
    UNUSED_PARAM (ServiceInst);
    return FNP_SUCCESS;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsMiPbbNpDeInitHw                                */
/*                                                                          */
/*    Description        : This function is used to De Init H/W             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

VOID
FsMiPbbNpDeInitHw ()
{
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsMiPbbNpInitHw                                  */
/*                                                                          */
/*    Description        : This function is used to De Init H/W             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT4
FsMiPbbNpInitHw ()
{
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsMiPbbHwSetAllToOneBundlingService                 */
/*                                                                          */
/*    Description        : This function is used to Set PISID Value         */
/*                                                                          */
/*    Input(s)           : Context id
                           i4IfIndex
                           u4Pisid
*/
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT4
FsMiPbbHwSetAllToOneBundlingService (UINT4 u4ContextId,
                                     UINT4 u4ifIndex, UINT4 u4Pisid)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4ifIndex);
    UNUSED_PARAM (u4Pisid);
    return FNP_SUCCESS;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsMiPbbHwDelAllToOneBundlingService                 */
/*                                                                          */
/*    Description        : This function is used to Delete PISID Value      */
/*                                                                          */
/*    Input(s)           : Context id
                           i4IfIndex
                           u4Isid
                           ServiceInst
*/
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT4
FsMiPbbHwDelAllToOneBundlingService (UINT4 u4ContextId,
                                     UINT4 u4ifIndex, UINT4 u4Pisid)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4ifIndex);
    UNUSED_PARAM (u4Pisid);
    return FNP_SUCCESS;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsMiVlanHwSetProviderBridgePortType                 */
/*                                                                          */
/*    Description        : This function is used to Set Bridge Port Type      */
/*                                                                          */
/*    Input(s)           : Context id
                           i4VipIfIndex
*/
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

/*INT4 FsMiVlanHwSetProviderBridgePortType (UINT4 u4ContextId,
                            UINT4 u4IfIndex, UINT4 u4PortType)
{
    UNUSED_PARAM(u4ContextId);
    UNUSED_PARAM(u4IfIndex);
    UNUSED_PARAM(u4PortType);
    return FNP_SUCCESS;

}*/
/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsMiPbbHwDelVipPipMap                            */
/*                                                                          */
/*    Description        : This function is used to Delete PIP VIP map      */
/*                                                                          */
/*    Input(s)           : Context id
                           i4VipIfIndex
*/
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT4
FsMiPbbHwDelVipPipMap (UINT4 u4ContextId, UINT4 u4VipIfIndex)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4VipIfIndex);
    return FNP_SUCCESS;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsMiPbbHwSetVipPipMap                 */
/*                                                                          */
/*    Description        : This function is used to stub Vip Vip map         */
/*                                                                          */
/*    Input(s)           : Context id
                           i4VipIfIndex
*/
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT4
FsMiPbbHwSetVipPipMap (UINT4 u4ContextId, UINT4 u4VipIfIndex,
                       tVipPipMap vippipmap)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4VipIfIndex);
    UNUSED_PARAM (vippipmap);
    return FNP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : FsMiBrgHwCreateControlPktFilter                      */
/*                                                                           */
/* Description        : This function is called to install filters for       */
/*                      copying the control packets to CPU that are from the */
/*                      customer network across the PBBN.                    */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      FilterEntry - Filter information                     */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsMiBrgHwCreateControlPktFilter (UINT4 u4ContextId, tFilterEntry FilterEntry)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (FilterEntry);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiBrgHwDeleteControlPktFilter                      */
/*                                                                           */
/* Description        : This function is called to delete specified filters. */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      FilterEntry - Filter information                     */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsMiBrgHwDeleteControlPktFilter (UINT4 u4ContextId, tFilterEntry FilterEntry)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (FilterEntry);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiPbbHwSetPortIsidLckStatus                        */
/*                                                                           */
/* Description        : This function is called to set the lock status of a  */
/*                      and a isid                                           */
/*                                                                           */
/* Input(s)           : u4ContextId:  Context identifier                     */
/*                      u4IfIndex: Interface Index                           */
/*                      u4Isid: Isid for which status to be updated          */
/*                      u1Lockstatus: Lock Status                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiPbbHwSetPortIsidLckStatus (UINT4 u4ContextId,
                               UINT4 u4IfIndex,
                               UINT4 u4Isid, UINT1 u1Lockstatus)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (u1Lockstatus);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiPbbHwGetPortIsidStats                            */
/*                                                                           */
/* Description        : This function is called to get the number  of        */
/*                      recieved and transmitted frames on a port for an isid*/
/*                                                                           */
/* Input(s)           : u4ContextId:  Context identifier                     */
/*                      u4IfIndex: Interface Index                           */
/*                      u4Isid: Isid for which status to be updated          */
/*                      pu4TxFCl: pointer to number of transmitted frames    */
/*                      pu4RxFCl: pointer to number of recieved frames       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiPbbHwGetPortIsidStats (UINT4 u4ContextId,
                           UINT4 u4IfIndex,
                           UINT4 u4Isid, UINT1 u1StatsType, UINT4 *pu4Stats)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (u1StatsType);
    UNUSED_PARAM (pu4Stats);
    return FNP_SUCCESS;
}
#endif
#endif
