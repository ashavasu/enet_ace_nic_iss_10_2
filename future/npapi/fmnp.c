
/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fmnp.c,v 1.5 2008/06/03 14:49:49 iss Exp $
 *
 * Description: This file contains the function implementations of Fault
 *              Management NPAPI. This is a portable file based on the type of 
 *              chipset.
 *****************************************************************************/

#include "lr.h"
#include "cfa.h"
#include "eoam.h"
#include "eoamfm.h"
#include "npapi.h"
#include "npfm.h"
/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : FmNpRegisterForFailureIndications                 
 *                                                                          
 *    DESCRIPTION      : This function is called by FM to register with hardware
 *                       for the notification of the following failures:
 *                       1) Link Fault (Loss of signal detected by receiver)
 *                       2) Dying Gasp
 *                       3) Critical Event
 *                       When the appropriate callback function is called from 
 *                       hardware, a message is posted to EOAM task by calling 
 *                       function EoamSendEventsToPeer ()
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None.                                                  
 *                                                                          
 *    RETURNS          : FNP_SUCCESS / FNP_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
FmNpRegisterForFailureIndications (VOID)
{
    return FNP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : NpFmFailureIndicationCallbackFunc                  
 *                                                                          
 *    DESCRIPTION      : This function is called by the hardware when 
 *                       the following failures  are detected:
 *                       1) Link Fault (Loss of signal detected by receiver)
 *                       2) Dying Gasp
 *                       3) Critical Event
 *                       The function EoamSendEventsToPeer () can be called from this 
 *                       function to post a message to EOAM task 
 *                                                                          
 *    INPUT            : u4IfIndex - interface index
 *                       u1EventType - Type of the event as Link fault / 
 *                                     Dying gasp / critical event
 *                       u1Status - Status as 'up' or 'down'
 *                                                                          
 *    OUTPUT           : None.                                                  
 *                                                                          
 *    RETURNS          : FNP_SUCCESS / FNP_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
NpFmFailureIndicationCallbackFunc (UINT4 u4IfIndex, UINT1 u1EventType,
                                   UINT1 u1Status)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1EventType);
    UNUSED_PARAM (u1Status);
    return FNP_SUCCESS;
}
