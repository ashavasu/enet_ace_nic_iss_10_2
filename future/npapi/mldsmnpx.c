/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldsmnpx.c,v 1.1 2007/03/23 13:39:48 iss Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/
#ifndef _MLDSMNPX_C_
#define _MLDSMNPX_C_

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "snp.h"
#include "npapi.h"
#include "npmimlds.h"

/*****************************************************************************/
/* Function Name      : FsMiMldsMbsmHwEnableMldSnooping                      */
/*                                                                           */
/* Description        : This function enables MLD snooping feature in the    */
/*                      hardware. Filters are added for receiving MLD        */
/*                      L2MC table is initialized for building MAC based     */
/*                      Multicast data forwarding                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      pSlotInfo   - Pointer to Slot information structure  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiMldsMbsmHwEnableMldSnooping (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiMldsMbsmHwEnableIpMldSnooping                    */
/*                                                                           */
/* Description        : This function enables MLD snooping feature in the    */
/*                      hardware. Filters are added for receiving MLD        */
/*                      IPMC table is initialized for building IP based      */
/*                      Multicast data forwarding                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      pSlotInfo   - Pointer to Slot information structure  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiMldsMbsmHwEnableIpMldSnooping (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

#endif
