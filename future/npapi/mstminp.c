/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mstminp.c,v 1.6 2009/09/24 14:21:46 prabuc Exp $ fsbrgnp.c
 *
 * Description: All network processor function  given here
 *
 *******************************************************************/

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fssnmp.h"
#include "fsvlan.h"
#include "rstp.h"
#include "mstp.h"
#include "npapi.h"
#include "npmstpmi.h"
#include "npcfa.h"

/***************************************************************************/
/* FUNCTION NAME : FsMiMstpNpCreateInstance                                */
/*                                                                         */
/* DESCRIPTION   : Creates a Spanning Tree instance in the Hardware.       */
/*                                                                         */
/* INPUTS        : ContextId - Virtual Switch ID                           */
/*                 u2InstId - Instance id of the Spanning tree to be 
 *                 created.   ranges <1-64>                                */
/*                                                                         */
/* OUTPUTS       : None                                                    */
/*                                                                         */
/* RETURNS       : FNP_SUCCESS - success                                   */
/*                 FNP_FAILURE - Error during creating                     */
/*                                                                         */
/* COMMENTS      : None                                                    */
/*                                                                         */
/***************************************************************************/

PUBLIC INT1
FsMiMstpNpCreateInstance (UINT4 u4ContextId, UINT2 u2InstId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2InstId);
    return FNP_SUCCESS;
}

/**************************************************************************/
/* FUNCTION NAME : FsMiMstpNpAddVlanInstMapping                           */
/*                                                                        */
/* DESCRIPTION   : Map a VLAN to the Spanning Tree Instance in                   *                 the hardware.                                          */
/* INPUTS        : ContextId - Virtual Switch ID                          */
/*                 u2VlanId - VLAN Id that has to be mapped.              */
/*                 ranges <1-4094>                                        */
/*                 u2InstId - Spanning tree instance Id.ranges<1-64>      */
/*                                                                        */
/* OUTPUTS      : None                                                    */
/*                                                                        */
/* RETURNS      : FNP_SUCCESS - success                                   */
/*                FNP_FAILURE - Error during mapping                      */
/*                                                                        */
/* COMMENTS     : None                                                    */
/*                                                                        */
/**************************************************************************/

PUBLIC INT1
FsMiMstpNpAddVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2InstId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2InstId);
    return FNP_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME : FsMiMstpNpAddVlanListInstMapping                      */
/*                                                                       */
/* DESCRIPTION   : Map a set of VLANs to the Spanning                    */
/*                   Tree Instance in the hardware.                      */
/*                                                                       */
/* INPUTS        : ContextId   - Virtual Switch ID                       */
/*                 pu1VlanList - List of VLANs to be mapped.             */
/*                 u2InstId    - Spanning tree instance Id ranges <1-64>.*/
/*                 u2NumVlans  - Number of VLANs to be mapped            */
/*                               ranges <1-4094>.                        */
/*                 pu2LastVlan - VLAN ID of last vlan mapped succesfully */
/*                                                                       */
/* OUTPUTS       : None                                                  */
/*                                                                       */
/* RETURNS       : FNP_SUCCESS - success                                 */
/*                 FNP_FAILURE - Error during mapping                    */
/*                                                                       */
/* COMMENTS      : None                                                  */
/*************************************************************************/

PUBLIC INT1
FsMiMstpNpAddVlanListInstMapping (UINT4 u4ContextId, UINT1 *pu1VlanList,
                                  UINT2 u2InstId, UINT2 u2NumVlans,
                                  UINT2 *pu2LastVlan)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pu1VlanList);
    UNUSED_PARAM (u2InstId);
    UNUSED_PARAM (u2NumVlans);
    UNUSED_PARAM (pu2LastVlan);
    return FNP_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME : FsMiMstpNpDeleteInstance                              */
/*                                                                       */
/* DESCRIPTION   : Delete the Spanning Tree instance in the hardware.    */
/*                 Hardware should take of UnMapping the Vlan to to      */
/*                 Instance Mapping and should delete the Instance.      */
/*                                                                       */
/* INPUTS        : ContextId - Virtual Switch ID                         */
/*                 u2InstId - Instance id of the Spanning tree to be     */
/*                 deleted                                               */
/*                 ranges <1-64>                                         */
/*                                                                       */
/* OUTPUTS       : None                                                  */
/*                                                                       */
/* RETURNS       : FNP_SUCCESS - success                                 */
/*                 FNP_FAILURE - Error during deleting                   */
/*                                                                       */
/* COMMENTS      : None                                                  */
/*                                                                       */
/*************************************************************************/

PUBLIC INT1
FsMiMstpNpDeleteInstance (UINT4 u4ContextId, UINT2 u2InstId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2InstId);
    return FNP_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME : FsMiMstpNpDelVlanInstanceMapping                      */
/*                                                                       */
/* DESCRIPTION   : Delete the VLAN-Spanning Tree instance mapping in     */
/*                 the hardware.                                         */
/*                                                                       */
/* INPUTS        : ContextId - Virtual Switch ID                         */
/*                 u2VlanId - VLAN Id that has to be unmapped ranges     */
/*                            <1-4094>                                   */
/*                 u2InstId - Spanning tree instance Id ranges <1-64>    */
/*                                                                       */
/* OUTPUTS       : None                                                  */
/*                                                                       */
/* RETURNS       : FNP_SUCCESS - success                                 */
/*                 FNP_FAILURE - Error during deleting the mapping       */
/*                                                                       */
/* COMMENTS      : None                                                  */
/*                                                                       */
/*************************************************************************/

PUBLIC INT1
FsMiMstpNpDelVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2InstId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2InstId);
    return FNP_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME : FsMiMstpNpDelVlanListInstanceMapping                  */
/*                                                                       */
/* DESCRIPTION   : Delete the VLAN-Spanning Tree instance mapping in     */
/*                 the hardware for the given set of VLANs               */
/*                                                                       */
/* INPUTS        : ContextId   - Virtual Switch ID                       */
/*                 pu1VlanList - List of VLANs to be unmapped.           */
/*                 u2InstId    - Spanning tree instance Id ranges <1-64>.*/
/*                 u2NumVlans  - Number of VLANs to be unmapped ranges   */
/*                                <1-4094>.                              */
/*                 pu2LastVlan - VLAN ID of last vlan unmapped           */
/*                 successfully.                                         */
/*                                                                       */
/* OUTPUTS       : None                                                  */
/*                                                                       */
/* RETURNS       : FNP_SUCCESS - success                                 */
/*                 FNP_FAILURE - Error during deleting the mapping       */
/*                                                                       */
/* COMMENTS      : None                                                  */
/*                                                                       */
/*************************************************************************/

PUBLIC INT1
FsMiMstpNpDelVlanListInstMapping (UINT4 u4ContextId, UINT1 *pu1VlanList,
                                  UINT2 u2InstId, UINT2 u2NumVlans,
                                  UINT2 *pu2LastVlan)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pu1VlanList);
    UNUSED_PARAM (u2InstId);
    UNUSED_PARAM (u2NumVlans);
    UNUSED_PARAM (pu2LastVlan);
    return FNP_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME : FsMiMstpNpSetInstancePortState                        */
/*                                                                       */
/* DESCRIPTION   : Sets the MSTP Port State in the Hardware for given    */
/*                 INSTANCE. When MSTP is operating in MSTP mode or MSTP */
/*                 in RSTP compatible mode or MSTP in STP compatible mode*/
/* INPUTS        : ContextId - Virtual Switch ID                         */
/*                 u4IfIndex   - Interface index of whose STP state      */
/*                 is to be  updated                                     */
/*                 u2InstanceId- Instance ID ranges <1-64>.              */
/*                 u1PortState - Value of Port State can take any of the */
/*                 following                                             */
/*                 AST_PORT_STATE_FORWARDING,                            */
/*                 AST_PORT_STATE_LEARNING,                              */
/*                 AST_PORT_STATE_DISCARDING,                            */
/*                 AST_PORT_STATE_DISABLED                               */
/*                                                                       */
/* OUTPUTS       : None                                                  */
/*                                                                       */
/* RETURNS       : FNP_SUCCESS - success                                 */
/*                 FNP_FAILURE - Error during setting                    */
/*                                                                       */
/* COMMENTS      : None                                                  */
/*                                                                       */
/*************************************************************************/
INT4
FsMiMstpNpSetInstancePortState (UINT4 u4ContextId, UINT4 u4IfIndex,
                                UINT2 u2InstanceId, UINT1 u1PortState)
{
    /* SISP feature enables a physical port to be mapped to more than one
     * context. These ports will not be present in the same MSTI instance 
     * across  two different contexts. If the underlying hardware supports 
     * context based vlan or instance port state, then there will be no problem
     * But, if the hardware does not support so, then the following should be 
     * ensured.
     * 1) The port state should be programmed only for the vlans that are 
     *    mapped to this context, for which this port is member of in this 
     *    context. [H/W has vlan based port state programming]
     * 2) In doing so, if u4ContextId represents primary context identifier,
     *    then this port state should be programmed, if the PVID for this port
     *    is mapped to this instance. [For the vlan represented by PVID only]/
     * 3) For all other vlans this should not be programmed.   
     *    */
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2InstanceId);
    UNUSED_PARAM (u1PortState);
    return FNP_SUCCESS;
}

/*************************************************************************/
/* FUNCTION NAME : FsMiMstpNpInitHw                                      */
/*                                                                       */
/* DESCRIPTION   : This function performs any necessary MSTP related     */
/*                 initialisation in the Hardware                        */
/*                                                                       */
/* INPUTS        : ContextId - Virtual Switch ID                         */
/*                                                                       */
/* OUTPUTS       : None                                                  */
/*                                                                       */
/* RETURNS       : None                                                  */
/*                                                                       */
/*************************************************************************/

VOID
FsMiMstpNpInitHw (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return;
}

/*************************************************************************/
/* FUNCTION NAME : FsMiMstpNpDeInitHw                                    */
/*                                                                       */
/* DESCRIPTION   : Sets the MSTP Disable Option  in the Hardware.        */
/*                 This function removes MSTP related initialisation     */
/*                 (done in FsMiMstpNpInitHw) from the Hardware          */
/*                                                                       */
/* INPUTS        : ContextId - Virtual Switch ID                         */
/*                                                                       */
/* OUTPUTS       : None                                                  */
/*                                                                       */
/* RETURNS       : None                                                  */
/*                                                                       */
/*************************************************************************/

VOID
FsMiMstpNpDeInitHw (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return;                        /* stub */
}

/*************************************************************************/
/* Function Name      : FsMiMstNpGetPortState                            */
/*                                                                       */
/* Description        : Gets the MSTP Port State in the Hardware.        */
/*                                                                       */
/*                                                                       */
/* Input(s)           : u4ContextId - Virtual Switch ID                  */
/*                      u2InstanceId - Mst Instance id                   */
/*                      u4IfIndex - Port Number.                         */
/*                      pu1Status  - Status returned from Hardware.      */
/*                              Values can be AST_PORT_STATE_DISCARDING  */
/*                                  or AST_PORT_STATE_LEARNING           */
/*                                  or AST_PORT_STATE_FORWARDING         */
/*                                                                       */
/* Output(s)          : None                                             */
/*                                                                       */
/* Global Variables                                                      */
/* Referred           : None                                             */
/*                                                                       */
/* Global Variables                                                      */
/* Modified           : None                                             */
/*                                                                       */
/* Return Value(s)    : FNP_SUCCESS - On successful set (or)             */
/*                      FNP_FAILURE - Error during setting               */
/*************************************************************************/
INT1
FsMiMstNpGetPortState (UINT4 u4ContextId, UINT2 u2InstanceId, UINT4 u4IfIndex,
                       UINT1 *pu1Status)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2InstanceId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1Status);
    return FNP_SUCCESS;
}
