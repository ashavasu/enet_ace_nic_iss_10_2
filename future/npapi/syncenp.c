/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id $
 *
 * Description: This file contains the SyncE NPAPIs that will be used for
 *              programming the hardware chipsets.
 *****************************************************************************/

#include "lr.h"
#include "cfa.h"
#include "npapi.h"
#include "syncenp.h"

/***************************************************************************
 * Function Name             : FsNpHwConfigSynceInfo
 *
 * Description               : This function is invoked to set
 *                             SyncE information in hardware
 *
 * Input                     : u1InfoTye - type of information needs to be
 *                             set in from hardware.
 *                             tSynceHwInfo - pointer to hardware information
 *                             structure
 *
 * Output                    : None
 * Global Variables Referred : None
 * Global variables Modified : None
 * Use of Recursion          : None
 * Returns                   : FNP_SUCCESS/FNP_FAILURE
 *
 **************************************************************************/
INT4
FsNpHwConfigSynceInfo (tSynceHwInfo * pSynceHwSynceInfo)
{
    switch (pSynceHwSynceInfo->u4InfoType)
    {
        case SYNCE_HW_SET_SYNCE_STATUS:
            return (FNP_SUCCESS);

        case SYNCE_HW_SET_ESMC_MODE:
            return (FNP_SUCCESS);

        case SYNCE_HW_SET_HW_CLK:
            return (FNP_SUCCESS);
    }

    return FNP_SUCCESS;
}
