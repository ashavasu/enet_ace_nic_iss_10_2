/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: npvcm.h,v 1.2 2007/02/01 14:59:31 iss Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 * 
 *
 *******************************************************************/
#ifndef _NPVCM_H
#define _NPVCM_H

#include "vcmnp.h"

#endif /* _NPVCM_H */
