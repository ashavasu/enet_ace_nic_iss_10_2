/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: nppoe.h,v 1.3 2007/02/01 14:59:31 iss Exp $
 *
 * Description: Prototypes for Link Aggregation module's NP-API.
 *
 *******************************************************************/

#ifndef _NPPOE_H
#define _NPPOE_H

#include "poenp.h"

#endif /* _NPPOE_H */
