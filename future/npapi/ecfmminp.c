/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ecfmminp.c,v 1.8 2016/03/08 12:03:55 siva Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/
#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#ifdef PBB_WANTED
#include "fsvlan.h"
#include "pbb.h"
#include "l2iwf.h"
#endif
#include "ecfm.h"
#include "npapi.h"
#include "npecfmmi.h"

#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
#include "ecfmminp.h"
#endif
/***********************************************************************
 * Function Name      : FsMiEcfmHwInit                                       
 *                                                                       
 * Description        : This function takes care of initialising the     
 *                      hardware related parameters.
 *                                                                       
 * Input(s)           : u4ContextId                                             
 *                                                                       
 * Output(s)          : None.                                            
 *                                                                       
 * Return Value(s)    : FNP_SUCCESS/FNP_FAILURE                        
 ***********************************************************************/
INT4
FsMiEcfmHwInit (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return FNP_SUCCESS;
}

/***********************************************************************
 * Function Name      : FsMiEcfmHwDeInit                                    
 *                                                                     
 * Description        : This function takes care of de-initialising the   
 *                       hardware related parameters.
 *                                                                     
 * Input(s)           : u4ContextId                                      
 *                                                                     
 * Output(s)          : None.                                          
 *                                                                     
 * Return Value(s)    : FNP_SUCCESS/FNP_FAILURE                      
 ***********************************************************************/
INT4
FsMiEcfmHwDeInit (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return FNP_SUCCESS;
}

/***********************************************************************
 * Function Name      : FsMiEcfmTransmitDmm                                    
 *                                                                     
 * Description        : This function is used to trigger the HW to transmit 
 *                      a DMM PDU.
 * Input(s)           : u4ContextId - Context Idenifier
 *                      u4IfIndex   - Interface Index
 *                      pu1DmmPdu   - Pointer to the DMM-PDU.
 *                      u2PduLength - Length of the DMM-PDU
 *                      VlanTag     - Information to be filled in Vlan-Tag
 *                      u1Direction - Direction of the transmitting MEP
 *                                                                     
 * Output(s)          : None.                                          
 *                                                                     
 * Return Value(s)    : FNP_SUCCESS/FNP_FAILURE                      
 ***********************************************************************/
INT4
FsMiEcfmTransmitDmm (UINT4 u4ContextId, UINT4 u4IfIndex,
                     UINT1 *pu1DmmPdu, UINT2 u2PduLength,
                     tVlanTag VlanTag, UINT1 u1Direction)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1DmmPdu);
    UNUSED_PARAM (u2PduLength);
    UNUSED_PARAM (VlanTag);
    UNUSED_PARAM (u1Direction);
    return FNP_SUCCESS;
}

/***********************************************************************
 * Function Name      : FsMiEcfmTransmitDmr
 *                                                                     
 * Description        : This function is used to trigger the HW to transmit 
 *                      a DMR PDU.
 * Input(s)           : u4ContextId - Context Idenifier
 *                      u4IfIndex   - Interface Index
 *                      pu1DmrPdu   - Pointer to the DMR-PDU.
 *                      u2PduLength - Length of the DMR-PDU.
 *                      VlanTag     - Information to be filled in Vlan-Tag
 *                      u1Direction - Direction of the transmitting MEP
 *                                                                     
 * Output(s)          : None.                                          
 *                                                                     
 * Return Value(s)    : FNP_SUCCESS/FNP_FAILURE                      
 ***********************************************************************/
INT4
FsMiEcfmTransmitDmr (UINT4 u4ContextId, UINT4 u4IfIndex,
                     UINT1 *pu1DmrPdu, UINT2 u2PduLength,
                     tVlanTag VlanTag, UINT1 u1Direction)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1DmrPdu);
    UNUSED_PARAM (u2PduLength);
    UNUSED_PARAM (VlanTag);
    UNUSED_PARAM (u1Direction);
    return FNP_SUCCESS;
}

/***********************************************************************
 * Function Name      : FsMiEcfmTransmitLmm                                    
 *                                                                     
 * Description        : This function is used to trigger the HW to transmit 
 *                      a LMM PDU.
 * Input(s)           : u4ContextId - Context Idenifier
 *                      u4IfIndex   - Interface Index
 *                      pu1LmmPdu   - Pointer to the LMM-PDU.
 *                      u2PduLength - Length of the LMM-PDU
 *                      VlanTag     - Information to be filled in Vlan-Tag
 *                      u1Direction - Direction of the transmitting MEP
 *                                                                     
 * Output(s)          : None.                                          
 *                                                                     
 * Return Value(s)    : FNP_SUCCESS/FNP_FAILURE                      
 ***********************************************************************/
INT4
FsMiEcfmTransmitLmm (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 *pu1LmmPdu,
                     UINT2 u2PduLength, tVlanTagInfo VlanTag, UINT1 u1Direction)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1LmmPdu);
    UNUSED_PARAM (u2PduLength);
    UNUSED_PARAM (VlanTag);
    UNUSED_PARAM (u1Direction);
    return FNP_SUCCESS;
}

/***********************************************************************
 * Function Name      : FsMiEcfmTransmitLmr
 *                                                                     
 * Description        : This function is used to trigger the HW to transmit 
 *                      a LMR PDU.
 * Input(s)           : u4ContextId - Context Idenifier
 *                      u4IfIndex   - Interface Index
 *                      pu1LmrPdu   - Pointer to the LMR-PDU.
 *                      u2PduLength - Length of the LMR-PDU.
 *                      VlanTag     - Information to be filled in Vlan-Tag
 *                      u1Direction - Direction of the transmitting MEP
 *                                                                     
 * Output(s)          : None.                                          
 *                                                                     
 * Return Value(s)    : FNP_SUCCESS/FNP_FAILURE                      
 ***********************************************************************/
INT4
FsMiEcfmTransmitLmr (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 *pu1LmrPdu,
                     UINT2 u2PduLength, tVlanTagInfo * pVlanTag,
                     UINT1 u1Direction)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1LmrPdu);
    UNUSED_PARAM (u2PduLength);
    UNUSED_PARAM (pVlanTag);
    UNUSED_PARAM (u1Direction);
    return FNP_SUCCESS;
}

/***********************************************************************
 * Function Name      : FsMiEcfmTransmit1Dm
 *                                                                     
 * Description        : This function is used to trigger the HW to transmit 
 *                      a 1DM PDU.
 * Input(s)           : u4ContextId - Context Idenifier
 *                      u4IfIndex   - Interface Index
 *                      pu1DmPdu    - Pointer to the DM-PDU.
 *                      u2PduLength - Length of the DM-PDU.
 *                      VlanTag     - Information to be filled in Vlan-Tag
 *                      u1Direction - Direction of the transmitting MEP
 *                                                                     
 * Output(s)          : None.                                          
 *                                                                     
 * Return Value(s)    : FNP_SUCCESS/FNP_FAILURE                      
 ***********************************************************************/
INT4
FsMiEcfmTransmit1Dm (UINT4 u4ContextId, UINT4 u4IfIndex,
                     UINT1 *pu1DmPdu, UINT2 u2PduLength,
                     tVlanTag VlanTag, UINT1 u1Direction)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1DmPdu);
    UNUSED_PARAM (u2PduLength);
    UNUSED_PARAM (VlanTag);
    UNUSED_PARAM (u1Direction);
    return FNP_SUCCESS;
}

/***********************************************************************
 * Function Name      : FsMiEcfmStartLbmTransaction
 *                                                                     
 * Description        : This function is used to trigger the HW to start 
 *                      Lbm Transmission.
 * Input(s)           : pEcfmMepInfoParams - pointer to MepInfo 
 *                      pEcfmConfigLbmInfo - pointer to Lbm Info 
 *                                                                     
 * Output(s)          : None.                                          
 *                                                                     
 * Return Value(s)    : FNP_SUCCESS/FNP_FAILURE                      
 ***********************************************************************/
INT4
FsMiEcfmStartLbmTransaction (tEcfmMepInfoParams * pEcfmMepInfoParams,
                             tEcfmConfigLbmInfo * pEcfmConfigLbmInfo)
{
    UNUSED_PARAM (pEcfmMepInfoParams);
    UNUSED_PARAM (pEcfmConfigLbmInfo);
    return FNP_SUCCESS;
}

/***********************************************************************
 * Function Name      : FsMiEcfmStopLbmTransaction
 *                                                                     
 * Description        : This function is used to trigger the HW to stop 
 *                      Lbm Transmission.
 * Input(s)           : pEcfmMepInfoParams - pointer to MepInfo 
 *                                                                     
 * Output(s)          : None.                                          
 *                                                                     
 * Return Value(s)    : FNP_SUCCESS/FNP_FAILURE                      
 ***********************************************************************/
INT4
FsMiEcfmStopLbmTransaction (tEcfmMepInfoParams * pEcfmMepInfoParams)
{
    UNUSED_PARAM (pEcfmMepInfoParams);
    return FNP_SUCCESS;
}

/***********************************************************************
 * Function Name      : FsMiEcfmStartTstTransaction
 *                                                                     
 * Description        : This function is used to trigger the HW to start 
 *                      Tst Transmission.
 * Input(s)           : pEcfmMepInfoParams - pointer to MepInfo 
 *                      pEcfmConfigTstInfo - pointer to Tst Info 
 *                                                                     
 * Output(s)          : None.                                          
 *                                                                     
 * Return Value(s)    : FNP_SUCCESS/FNP_FAILURE                      
 ***********************************************************************/
INT4
FsMiEcfmStartTstTransaction (tEcfmMepInfoParams * pEcfmMepInfoParams,
                             tEcfmConfigTstInfo * pEcfmConfigTstInfo)
{
    UNUSED_PARAM (pEcfmMepInfoParams);
    UNUSED_PARAM (pEcfmConfigTstInfo);
    return FNP_SUCCESS;
}

/***********************************************************************
 * Function Name      : FsMiEcfmStopTstTransaction
 *                                                                     
 * Description        : This function is used to trigger the HW to stop 
 *                      Tst Transmission.
 * Input(s)           : pEcfmMepInfoParams - pointer to MepInfo 
 *                                                                     
 * Output(s)          : None.                                          
 *                                                                     
 * Return Value(s)    : FNP_SUCCESS/FNP_FAILURE                      
 ***********************************************************************/
INT4
FsMiEcfmStopTstTransaction (tEcfmMepInfoParams * pEcfmMepInfoParams)
{
    UNUSED_PARAM (pEcfmMepInfoParams);
    return FNP_SUCCESS;
}

/***********************************************************************
 * Function Name      : FsMiEcfmClearRcvdLbrCounter
 *                                                                     
 * Description        : This function is used to trigger the HW to clear 
 *                      Lbr Rcvd Counter.
 * Input(s)           : pEcfmMepInfoParams - pointer to MepInfo 
 *                                                                     
 * Output(s)          : None.                                          
 *                                                                     
 * Return Value(s)    : FNP_SUCCESS/FNP_FAILURE                      
 ***********************************************************************/
INT4
FsMiEcfmClearRcvdLbrCounter (tEcfmMepInfoParams * pEcfmMepInfoParams)
{
    UNUSED_PARAM (pEcfmMepInfoParams);
    return FNP_SUCCESS;
}

/***********************************************************************
 * Function Name      : FsMiEcfmGetRcvdLbrCounter
 *                                                                     
 * Description        : This function is used to get the Rcvd LBR counter 
 *                      value for HW.
 * Input(s)           : pEcfmMepInfoParams - pointer to MepInfo 
 *                    : pu4LbrIn - Recieved Lbr Counter value
 *                                                                     
 * Output(s)          : None.                                          
 *                                                                     
 * Return Value(s)    : FNP_SUCCESS/FNP_FAILURE                      
 ***********************************************************************/
INT4
FsMiEcfmGetRcvdLbrCounter (tEcfmMepInfoParams * pEcfmMepInfoParams,
                           UINT4 *pu4LbrIn)
{
    UNUSED_PARAM (pEcfmMepInfoParams);
    UNUSED_PARAM (pu4LbrIn);
    return FNP_SUCCESS;
}

/***********************************************************************
 * Function Name      : FsMiEcfmClearRcvdTstCounter
 *                                                                     
 * Description        : This function is used to trigger the HW to clear 
 *                      Tst Rcvd Counter.
 * Input(s)           : pEcfmMepInfoParams - pointer to MepInfo 
 *                                                                     
 * Output(s)          : None.                                          
 *                                                                     
 * Return Value(s)    : FNP_SUCCESS/FNP_FAILURE                      
 ***********************************************************************/
INT4
FsMiEcfmClearRcvdTstCounter (tEcfmMepInfoParams * pEcfmMepInfoParams)
{
    UNUSED_PARAM (pEcfmMepInfoParams);
    return FNP_SUCCESS;
}

/***********************************************************************
 * Function Name      : FsMiEcfmGetRcvdTstCounter
 *                                                                     
 * Description        : This function is used to get the Rcvd TST counter 
 *                      value for HW.
 * Input(s)           : pEcfmMepInfoParams - pointer to MepInfo 
 *                    : pu4TstIn - Recieved Tst Counter value
 *                                                                     
 * Output(s)          : None.                                          
 *                                                                     
 * Return Value(s)    : FNP_SUCCESS/FNP_FAILURE                      
 ***********************************************************************/
INT4
FsMiEcfmGetRcvdTstCounter (tEcfmMepInfoParams * pEcfmMepInfoParams,
                           UINT4 *pu4TstIn)
{
    UNUSED_PARAM (pEcfmMepInfoParams);
    UNUSED_PARAM (pu4TstIn);
    return FNP_SUCCESS;
}

/***********************************************************************
 * Function Name      : FsMiEcfmHwGetCapability
 *
 * Description        : This function is used to get the hardware
 *                      capabilities
 * Input(s)           : UINT4 ContextId

 *
 * Output(s)          : pu4HwCapability - HW capabilities

 *                      BIT 0 - LBM transmission and LBR reception
 *                      capability
 *                      BIT 1 - DMM and DMR transmission, reception
 *                      capability
 *                      BIT 2 - 1DM tranmission and reception
 *                      capability
 *                      BIT 3 - LMM and LMR transmission,
 *                      reception capability
 *                      BIT 4 - TST transmission, reception
 *                      capability
 *                      BIT 5 - CCM Offloading capability
 *
 * Return Value(s)    : FNP_SUCCESS/FNP_FAILURE
 ************************************************************************/
INT4
FsMiEcfmHwGetCapability (UINT4 u4ContextId, UINT4 *pu4HwCapability)
{
    UNUSED_PARAM (u4ContextId);
    *pu4HwCapability = 0;
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FsMiEcfmStartLm                                            */
/*                                                                           */
/* Description  : This function is used to install  filters in both ingress  */
/*                Egress dirction and counters are attached to filters       */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                           */
/* Output       :              */
/*                                                                           */
/* Returns      : FNP_SUCCESS/FNP_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/

INT4
FsMiEcfmStartLm (tEcfmHwLmParams * pHwLmInfo)
{

    UNUSED_PARAM (pHwLmInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FsMiEcfmStopLm                                             */
/*                                                                           */
/* Description  : This function is used to delete  filters in both ingress   */
/*                Egress dirction and to clear counters are attached to      */
/*                filters                                                    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       :                                                            */
/*                                                                           */
/* Returns      : FNP_SUCCESS/FNP_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/

INT4
FsMiEcfmStopLm (tEcfmHwLmParams * pHwLmInfo)
{

    UNUSED_PARAM (pHwLmInfo);
    return FNP_SUCCESS;

}


#if defined (Y1564_WANTED) || (RFC2544_WANTED)

/*****************************************************************************
 * Function Name   : FsNpHwConfigEsat                                        *
 *                                                                           *
 * Description     ; This is an interface function for SLA test related      *
 *                   communication                                           *
 * Input           : tHwTestInfo - structure which has all the necessaery    *
 *                   Details to trigger HW calls.                            *
 *                   u1Command   - option for which Hw call to init          *
 * Output          : NONE                                                    *
 * Return Value(s) : FNP_SUCCESS - On success                                *
 *                   FNP_FAILURE - On failure                                *
 *****************************************************************************/
INT4
FsMiEcfmSlaTest (tHwTestInfo * pHwTestInfo)
{
	UNUSED_PARAM (pHwTestInfo);
	return FNP_SUCCESS;
}

/*****************************************************************************
 * Function Name   : FsMiEcfmAddHwLoopbackInfo                               *
 *                                                                           *
 * Description     : Add the Loopback Information                            *
 *                                                                           *
 * Input           : tHwLoopbackInfo- structure which has all the necessaery *
 *                   Details of Loopback                                     *
 *                                                                           *
 * Output          : NONE                                                    *
 *                                                                           *
 * Return Value(s) : FNP_SUCCESS - On success                                *
 *                   FNP_FAILURE - On failure                                *
 *****************************************************************************/
UINT1
FsMiEcfmAddHwLoopbackInfo (tHwLoopbackInfo * pHwLoopbackInfo)
{
	UNUSED_PARAM (pHwLoopbackInfo);
	return FNP_SUCCESS;
}

/*****************************************************************************
 * Function Name   : FsMiEcfmDelHwLoopbackInfo                               *
 *                                                                           *
 * Description     : Delete the Loopback Information                         *
 *                                                                           *
 * Input           : tHwLoopbackInfo- structure which has all the necessaery *
 *                   Details of Loopback                                     *
 *                                                                           *
 * Output          : NONE                                                    *
 *                                                                           *
 * Return Value(s) : FNP_SUCCESS - On success                                *
 *                   FNP_FAILURE - On failure                                *
 *****************************************************************************/
UINT1
FsMiEcfmDelHwLoopbackInfo (tHwLoopbackInfo * pHwLoopbackInfo)
{
	UNUSED_PARAM (pHwLoopbackInfo);
	return FNP_SUCCESS;
}
#endif
/******************************************************************************/
/*                           End  of file ecfmnp.c                            */
/******************************************************************************/
