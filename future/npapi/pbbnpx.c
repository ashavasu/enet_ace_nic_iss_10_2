/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pbbnpx.c,v 1.3 2009/09/24 14:21:46 prabuc Exp $
 *
 * Description: All prototypes for PBB MBSM Network Processor API functions *              done here
 *******************************************************************/
#ifdef PBB_WANTED

#ifndef _PBBNPX_C_
#define _PBBNPX_C_
#ifdef MBSM_WANTED
#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "npapi.h"
#include "pbbnpx.h"

/*************************************************************************/
/* Function Name       : FsMiPbbMbsmHwSetVipPipMap                       */
/*                                                                       */
/* Description         : This function sets vip pip map for the slot     */
/*                                                                       */
/* Input(s)            : Context Id                                      
 *                       Vip If Index
 *                       Vip Pip Map
 *                       pSlotInfo - Slot Information                    */
/*                                                                       */
/* Output(s)           : None                                            */
/*                                                                       */
/* Global Variables Referred : None                                      */
/*                                                                       */
/* Global Variables Modified : None.                                     */
/*                                                                       */
/* Exceptions or Operating                                               */
/* System Error Handling    : None.                                      */
/*                                                                       */
/* Use of Recursion        : None.                                       */
/*                                                                       */
/* Returns            : FNP_SUCCESS OR FNP_FAILURE                       */
/*************************************************************************/
INT4
FsMiPbbMbsmHwSetVipPipMap (UINT4 u4ContextId, UINT4 u4VipIfIndex,
                           tVipPipMap VipPipMap, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4VipIfIndex);
    UNUSED_PARAM (VipPipMap);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*************************************************************************/
/* Function Name       : FsMiPbbMbsmHwDelVipPipMap                       */
/*                                                                       */
/* Description         : This function deletes vip pip map for a slot    */
/*                                                                       */
/* Input(s)            : Context Id                                      
 *                       Vip If Index
 *                       pSlotInfo - Slot Information                    */
/*                                                                       */
/* Output(s)           : None                                            */
/*                                                                       */
/* Global Variables Referred : None                                      */
/*                                                                       */
/* Global Variables Modified : None.                                     */
/*                                                                       */
/* Exceptions or Operating                                               */
/* System Error Handling    : None.                                      */
/*                                                                       */
/* Use of Recursion        : None.                                       */
/*                                                                       */
/* Returns            : FNP_SUCCESS OR FNP_FAILURE                       */
/*************************************************************************/
INT4
FsMiPbbMbsmHwDelVipPipMap (UINT4 u4ContextId, UINT4 u4VipIfIndex,
                           tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4VipIfIndex);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*************************************************************************/
/* Function Name       : FsMiPbbMbsmHwSetVipAttributes                   */
/*                                                                       */
/* Description         : This function set vip attributes for given slot */
/*                                                                       */
/* Input(s)            : Context Id                                      
 *                       Vip If Index
 *                       Vip Attributes
 *                       pSlotInfo - Slot Information                    */
/*                                                                       */
/* Output(s)           : None                                            */
/*                                                                       */
/* Global Variables Referred : None                                      */
/*                                                                       */
/* Global Variables Modified : None.                                     */
/*                                                                       */
/* Exceptions or Operating                                               */
/* System Error Handling    : None.                                      */
/*                                                                       */
/* Use of Recursion        : None.                                       */
/*                                                                       */
/* Returns            : FNP_SUCCESS OR FNP_FAILURE                       */
/*************************************************************************/
INT4
FsMiPbbMbsmHwSetVipAttributes (UINT4 u4ContextId, UINT4 u4VipIndex,
                               tVipAttribute VipAttribute,
                               tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4VipIndex);
    UNUSED_PARAM (VipAttribute);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*************************************************************************/
/* Function Name       : FsMiPbbMbsmHwDelVipAttributes                   */
/*                                                                       */
/* Description         : This function deletes vip attributes for a slot */
/*                                                                       */
/* Input(s)            : Context Id                                      
 *                       Vip If Index
 *                       pSlotInfo - Slot Information                    */
/*                                                                       */
/* Output(s)           : None                                            */
/*                                                                       */
/* Global Variables Referred : None                                      */
/*                                                                       */
/* Global Variables Modified : None.                                     */
/*                                                                       */
/* Exceptions or Operating                                               */
/* System Error Handling    : None.                                      */
/*                                                                       */
/* Use of Recursion        : None.                                       */
/*                                                                       */
/* Returns            : FNP_SUCCESS OR FNP_FAILURE                       */
/*************************************************************************/
INT4
FsMiPbbMbsmHwDelVipAttributes (UINT4 u4ContextId, UINT4 u4VipIndex,
                               tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4VipIndex);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*************************************************************************/
/* Function Name       : FsMiPbbMbsmHwSetBackboneServiceInstEntry        */
/*                                                                       */
/* Description         : This function sets Backbone Service Instance 
 *                       Entry for a slot                                */
/*                                                                       */
/* Input(s)            : Context Id                                      
 *                       Cbp If Index
 *                       Backbone Isid
 *                       Backbone Service Instance entry
 *                       pSlotInfo - Slot Information                    */
/*                                                                       */
/* Output(s)           : None                                            */
/*                                                                       */
/* Global Variables Referred : None                                      */
/*                                                                       */
/* Global Variables Modified : None.                                     */
/*                                                                       */
/* Exceptions or Operating                                               */
/* System Error Handling    : None.                                      */
/*                                                                       */
/* Use of Recursion        : None.                                       */
/*                                                                       */
/* Returns            : FNP_SUCCESS OR FNP_FAILURE                       */
/*************************************************************************/
INT4
FsMiPbbMbsmHwSetBackboneServiceInstEntry (UINT4 u4ContextId,
                                          UINT4 u4CbpIfIndex,
                                          UINT4 u4BSid,
                                          tBackboneServiceInstEntry
                                          BackboneServiceInstEntry,
                                          tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4CbpIfIndex);
    UNUSED_PARAM (u4BSid);
    UNUSED_PARAM (BackboneServiceInstEntry);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*************************************************************************/
/* Function Name       : FsMiPbbMbsmHwDelBackBoneServiceInstEntry        */
/*                                                                       */
/* Description         : This function deletes Backbone Service Instance 
 *                       Entry for a slot                                */
/*                                                                       */
/* Input(s)            : Context Id                                      
 *                       Cbp If Index
 *                       Backbone Isid
 *                       pSlotInfo - Slot Information                    */
/*                                                                       */
/* Output(s)           : None                                            */
/*                                                                       */
/* Global Variables Referred : None                                      */
/*                                                                       */
/* Global Variables Modified : None.                                     */
/*                                                                       */
/* Exceptions or Operating                                               */
/* System Error Handling    : None.                                      */
/*                                                                       */
/* Use of Recursion        : None.                                       */
/*                                                                       */
/* Returns            : FNP_SUCCESS OR FNP_FAILURE                       */
/*************************************************************************/
INT4
FsMiPbbMbsmHwDelBackBoneServiceInstEntry (UINT4 u4ContextId,
                                          UINT4 u4CbpIfIndex,
                                          UINT4 u4BSid,
                                          tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4CbpIfIndex);
    UNUSED_PARAM (u4BSid);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*************************************************************************/
/* Function Name       : FsMiPbbMbsmHwSetAllToOneBundlingService         */
/*                                                                       */
/* Description         : This function sets pisid for the ports 
 *                       for a slot                                      */
/*                                                                       */
/* Input(s)            : Context Id                                      
 *                       Cnp If Index
 *                       Isid
 *                       pSlotInfo - Slot Information                    */
/*                                                                       */
/* Output(s)           : None                                            */
/*                                                                       */
/* Global Variables Referred : None                                      */
/*                                                                       */
/* Global Variables Modified : None.                                     */
/*                                                                       */
/* Exceptions or Operating                                               */
/* System Error Handling    : None.                                      */
/*                                                                       */
/* Use of Recursion        : None.                                       */
/*                                                                       */
/* Returns            : FNP_SUCCESS OR FNP_FAILURE                       */
/*************************************************************************/
INT4
FsMiPbbMbsmHwSetAllToOneBundlingService (UINT4 u4ContextId,
                                         UINT4 u4IfIndex,
                                         UINT4 u4Isid,
                                         tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*************************************************************************/
/* Function Name       : FsMiPbbMbsmHwDelAllToOneBundlingService         */
/*                                                                       */
/* Description         : This function deletes pisid for the ports
 *                       for a slot                                      */
/*                                                                       */
/* Input(s)            : Context Id                                      
 *                       Cnp If Index
 *                       Isid
 *                       pSlotInfo - Slot Information                    */
/*                                                                       */
/* Output(s)           : None                                            */
/*                                                                       */
/* Global Variables Referred : None                                      */
/*                                                                       */
/* Global Variables Modified : None.                                     */
/*                                                                       */
/* Exceptions or Operating                                               */
/* System Error Handling    : None.                                      */
/*                                                                       */
/* Use of Recursion        : None.                                       */
/*                                                                       */
/* Returns            : FNP_SUCCESS OR FNP_FAILURE                       */
/*************************************************************************/
INT4
FsMiPbbMbsmHwDelAllToOneBundlingService (UINT4 u4ContextId,
                                         UINT4 u4IfIndex,
                                         UINT4 u4Isid,
                                         tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*************************************************************************/
/* Function Name       : FsMiPbbMbsmHwNpInit                             */
/*                                                                       */
/* Description         : This function initialised the PBB MBSM          */
/*                                                                       */
/* Input(s)            :pSlotInfo - Slot Information                     */
/*                                                                       */
/* Output(s)           : None                                            */
/*                                                                       */
/* Global Variables Referred : None                                      */
/*                                                                       */
/* Global Variables Modified : None.                                     */
/*                                                                       */
/* Exceptions or Operating                                               */
/* System Error Handling    : None.                                      */
/*                                                                       */
/* Use of Recursion        : None.                                       */
/*                                                                       */
/* Returns            : FNP_SUCCESS OR FNP_FAILURE                       */
/*************************************************************************/
INT4
FsMiPbbMbsmHwNpInit (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*************************************************************************/
/* Function Name       : FsMiBrgMbsmHwCreateControlPktFilter             */
/*                                                                       */
/* Description         : This function initialised the PBB MBSM filters  */
/*                                                                       */
/* Input(s)            :u4ContextId - Current Context Id                 */
/*                      pAstFilterEntry - Filter Structure              */
/*                      pSlotInfo - Slot Information                     */
/*                                                                       */
/* Output(s)           : None                                            */
/*                                                                       */
/* Global Variables Referred : None                                      */
/*                                                                       */
/* Global Variables Modified : None.                                     */
/*                                                                       */
/* Exceptions or Operating                                               */
/* System Error Handling    : None.                                      */
/*                                                                       */
/* Use of Recursion        : None.                                       */
/*                                                                       */
/* Returns            : FNP_SUCCESS OR FNP_FAILURE                       */
/*************************************************************************/

INT4
FsMiBrgMbsmHwCreateControlPktFilter (UINT4 u4ContextId,
                                     tPbbCtrlPktFilterEntry * pAstFilterEntry,
                                     tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pAstFilterEntry);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

#endif /* End of MBSM Wanted flag */
#endif
#endif
