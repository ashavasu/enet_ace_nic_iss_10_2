/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipnpx.c,v 1.7 2014/11/11 12:58:24 siva Exp $
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/
#ifndef _IPNPX_C_
#define _IPNPX_C_

#include "lr.h"
#include "cfa.h"
#include "npip.h"

/******************************************************************/
/*  Function Name             : FsNpMbsmIpInit                    */
/*  Description               : This is the first function to be  */
/*                              called for initialising           */
/*                              any IP specific operation         */
/*  Input(s)                  : None.                             */
/*  Output(s)                 : None                              */
/*  Global Variables Referred : None                              */
/*  Global variables Modified : None                              */
/*  Exceptions                : None                              */
/*  Use of Recursion          : None                              */
/*  Returns                   : VOID                              */
/******************************************************************/

VOID
FsNpMbsmIpInit (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
}

/******************************************************************/
/*  Function Name             : FsNpMbsmIsisHwProgram             */
/*  Description               : This function for Enable / Disable*/
/*                               Isis                             */
/*                                                                */
/*                                                                */
/*  Input(s)                  : u1Status - Enable / Disable       */
/*  Output(s)                 : None                              */
/*  Global Variables Referred : None                              */
/*  Global variables Modified : None                              */
/*  Exceptions                : None                              */
/*  Use of Recursion          : None                              */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE           */
/******************************************************************/

UINT1                                                                                                                                                        FsNpMbsmIsisHwProgram (tMbsmSlotInfo * pSlotInfo, UINT1 u1Status)
{
    UNUSED_PARAM (pSlotInfo);
    UNUSED_PARAM (u1Status);
    return FNP_SUCCESS;
}

/*****************************************************************/
/*  Function Name             : FsMbsmNpOspfInit                 */
/*  Description               : This fucntion does the initializa*/
/*                              -ation in hardware to receive    */
/*                              OSPF packets                     */
/*  Input(s)                  : None.                            */
/*  Output(s)                 : None                             */
/*  Global Variables Referred : None                             */
/*  Global variables Modified : None                             */
/*  Exceptions                : None                             */
/*  Use of Recursion          : None                             */
/*  Returns                   : VOID                             */
/*****************************************************************/
INT1
FsNpMbsmOspfInit (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************/
/*  Function Name             : FsNpMbsmDhcpSrvInit              */
/*  Description               : This fucntion does the initializa*/
/*                              -ation in hardware to receive    */
/*                              DHCP server packets              */
/*  Input(s)                  : None.                            */
/*  Output(s)                 : None                             */
/*  Global Variables Referred : None                             */
/*  Global variables Modified : None                             */
/*  Exceptions                : None                             */
/*  Use of Recursion          : None                             */
/*  Returns                   : VOID                             */
/*****************************************************************/
INT1
FsNpMbsmDhcpSrvInit (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************/
/*  Function Name             : FsNpMbsmDhcpRlyInit              */
/*  Description               : This fucntion does the initializa*/
/*                              -ation in hardware to receive    */
/*                              DHCP relay packets               */
/*  Input(s)                  : None.                            */
/*  Output(s)                 : None                             */
/*  Global Variables Referred : None                             */
/*  Global variables Modified : None                             */
/*  Exceptions                : None                             */
/*  Use of Recursion          : None                             */
/*  Returns                   : VOID                             */
/*****************************************************************/
INT1
FsNpMbsmDhcpRlyInit (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************/
/*  Function Name             : FsNpMbsmIpv4CreateIpInterface    */
/*  Description               : Create a local IP Interface for  */
/*                              the specified prefix.            */
/*  Input(s)                  : u4VrId   - The virtual router    */
/*                                         identifier.           */
/*                              u4IfIndex -Interface Index       */
/*                              u4IpAddr  -Ip address to be      */
/*                                         configured for the    */
/*                                         interface             */
/*                              u4IpSubnetMask -Network Mask     */
/*                                              for this         */
/*                                              interface        */
/*                VlanId - Vlan Identifier     */
/*                au1MacAddr - Mac address of this */
/*  Output(s)                 : None                             */
/*  Global Variables Referred : None                             */
/*  Global variables Modified : None                             */
/*  Exceptions                : None                             */
/*  Use of Recursion          : None                             */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE          */
/*****************************************************************/

UINT4
FsNpMbsmIpv4CreateIpInterface (UINT4 u4VrId,
                               UINT4 u4CfaIfIndex,
                               UINT4 u4IpAddr, UINT4 u4IpSubNetMask,
                               UINT2 u2VlanId, UINT1 *au1MacAddr,
                               tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4CfaIfIndex);
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (u4IpSubNetMask);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (au1MacAddr);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/******************************************************************************/
/*   Function Name             : FsNpMbsmIpv4ArpAdd                           */
/*   Description               : Adds the Specified IP address and MAC address*/
/*   Input(s)                  : u4CfaIfIndex - CFA Vlan If Index             */
/*   Output(s)                 : None                                         */
/*   Global Variables Referred : None                                         */
/*   Global variables Modified : None                                         */
/*   Exceptions                : None                                         */
/*   Use of Recursion          : None                                         */
/*   Returns                   : FNP_SUCCESS/FNP_FAILURE                      */
/******************************************************************************/
UINT4
FsNpMbsmIpv4ArpAdd (tNpVlanId u2VlanId, UINT4 u4IpAddr,
                    UINT1 *pMacAddr, UINT1 *pbu1TblFull,
                    tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (pMacAddr);
    UNUSED_PARAM (pbu1TblFull);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/***********************************************************************/
/*  Function Name             :FsNpMbsmIpv4UcAddRoute                  */
/*  Description               : This function adds an route            */
/*                              entry to ip unicast route table        */
/*                              of Network Processort(Fast path)       */
/*  Input(s)                  : u4VrId   - The virtual router          */
/*                                         identifier.                 */
/*                              u4IpDestAddr - Destination IP          */
/*                                             Address                 */
/*                              u4IpSubNetMask-Destination             */
/*                                             Subnet Mask             */
/*                              routeEntry    -Unicast Route           */
/*                                             Entry containing        */
/*                                             Gateway IP              */
/*                                             Address.                */
/*                              u4RouteType   -Unicast Route           */
/*                                             Type - static/dy        */
/*  Output(s)                 : None                                   */
/*  Global Variables Referred : None                                   */
/*  Global variables Modified : None                                   */
/*  Exceptions                : None                                   */
/*  Use of Recursion          : None                                   */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE                */
/***********************************************************************/
UINT4
FsNpMbsmIpv4UcAddRoute (UINT4 u4VrId, UINT4 u4IpDestAddr, UINT4 u4IpSubNetMask,
                        tFsNpNextHopInfo routeEntry, UINT1 *pbu1TblFull,
                        tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4IpDestAddr);
    UNUSED_PARAM (u4IpSubNetMask);
    UNUSED_PARAM (routeEntry);
    UNUSED_PARAM (pbu1TblFull);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

#ifdef VRRP_WANTED
/***************************************************************/
/*  Function Name             : FsNpMbsmIpv4CreateVrrpInterface*/
/*  Description               : Create Vrrp Interface          */
/*  Input(s)                  : u4IfIndex -Interface Index     */
/*                              u4IpAddr  - Master Ip address  */
/*                              u1MacAddr - Virtual Mac Address*/
/*  Output(s)                 : None                           */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions                : None                           */
/*  Use of Recursion          : None                           */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE        */
/***************************************************************/

UINT4
FsNpMbsmIpv4CreateVrrpInterface (tNpVlanId u2VlanId, UINT4 u4IpAddr,
                                 UINT1 *au1MacAddr, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (au1MacAddr);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FsNpMbsmv4VrrpInstallFilter                      */
/*                                                                          */
/*    Description        : This function is used to install the filters     */
/*                         in the hardware.                                 */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
FsNpMbsmIpv4VrrpInstallFilter (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

#endif /* VRRP_WANTED */

/***************************************************************/
/*  Function Name             : FsNpMbsmIpv4VrmEnableVr        */
/*  Description               : Enable/Disable the specified   */
/*                              Virtual Router in the IPv4     */
/*                              Routing engine.                */
/*  Input(s)                  : u4VrId   - The virtual router  */
/*                                         identifier.         */
/*                              u1Status - FNP_TRUE, enable the*/
/*                                         specified Virtual   */
/*                                         Router              */
/*                                         FNP_FALSE, disable  */
/*                                         the specified       */
/*                                         Virtual Router      */
/*  Output(s)                 : None                           */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions                : None                           */
/*  Use of Recursion          : None                           */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE        */
/***************************************************************/

UINT4
FsNpMbsmIpv4VrmEnableVr (UINT4 u4VrId, UINT1 u1Status,
                         tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u1Status);

    return FNP_SUCCESS;
}

/***************************************************************/
/*  Function Name             : FsNpMbsmIpv4BindIfToVrId       */
/*  Description               : Bind an interface to a Virtual */
/*                              Router                         */
/*  Input(s)                  : u4VrId   - The virtual router  */
/*                                         identifier.         */
/*                              u4IfIndex -Interface Index     */
/*  Output(s)                 : None                           */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions                : None                           */
/*  Use of Recursion          : None                           */
/*  Returns                   : FNP_SUCCESS/FNP_FAILURE        */
/***************************************************************/

UINT4
FsNpMbsmIpv4BindIfToVrId (UINT4 u4IfIndex, UINT4 u4VrId,
                          tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4VrId);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name             :  FsNpMbsmIpv4UcAddTrap                       */
/*  Description               :  Creates a Trap entry in the Fast Path Table */
/*  Input(s)                  :  u4VrId     -- Virtual Router Id             */
/*                               u4Dest     -- Destination Network           */
/*                               u4DestMask -- Destination Mask              */
/*                               nextHopInfo-- Next Hop Information          */
/*  Output(s)                 :  None                                        */
/*  Output(s)                 :  None                                        */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions                :  None                                        */
/*  Use of Recursion          :  None                                        */
/*  Returns                   :  FNP_FAILURE(0) on failure                   */
/*                               FNP_SUCCESS(1) on success                   */
/*****************************************************************************/
UINT4
FsNpMbsmIpv4UcAddTrap (UINT4 u4VrId, UINT4 u4Dest, UINT4 u4DestMask,
                       tFsNpNextHopInfo nextHopInfo, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4Dest);
    UNUSED_PARAM (u4DestMask);
    UNUSED_PARAM (nextHopInfo);

    return FNP_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name   : FsNpMbsmIpv4MapVlansToIpInterface
 *
 *    Description     : Map list of vlan to IP interface
 *
 *    Input(s)        : pPvlanMappingInfo, pSlotInfo                      
 *
 *    Output(s)       : None                                                 
 *
 *    Returns         : FNP_SUCCESS or FNP_FAILURE
 *
 *****************************************************************************/

INT4
FsNpMbsmIpv4MapVlansToIpInterface (tNpIpVlanMappingInfo * pPvlanMappingInfo,
                                   tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pPvlanMappingInfo);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

/******************************************************************************/
/*   Function Name             : FsNpMbsmIpv4ArpAddition                      */
/*   Description               : Adds the Specified IP address and MAC address*/
/*                               to the H/W L3 IP Table and Returns Full if no*/
/*                               free entry exist in the L3 Ip Table          */
/*                               This function is an upgrade of MBSM call     */
/*                               FsNpMbsmIpv4ArpAdd with additional           */
/*                               arguments i1State/u4IfIndex/pu1IfName        */
/*   Input(s)                  : u2VlanId  - L2 Vlan Id                       */
/*                               u4IfIndex - CFA L3 Vlan If Index             */
/*                               u4IpAddr  - IP address for which ARP is added*/
/*                               pMacAddr  - MAC Address                      */
/*                               pu1IfName - Interface name                   */
/*                               i1State   - State of the ARP entry           */
/*                               pSlotInfo - Slot Information                 */
/*   Output(s)                 : pbu1TblFull - Hw Table Full/not              */
/*                                            (FNP_FALSE/FNP_TRUE)            */
/*   Global Variables Referred : None                                         */
/*   Global variables Modified : None                                         */
/*   Exceptions                : None                                         */
/*   Use of Recursion          : None                                         */
/*   Returns                   : FNP_SUCCESS/FNP_FAILURE                      */
/******************************************************************************/
UINT4
FsNpMbsmIpv4ArpAddition (tNpVlanId u2VlanId, UINT4 u4IpAddr,
                         UINT1 *pMacAddr, UINT1 *pbu1TblFull,
                         UINT4 u4IfIndex, UINT1 *pu1IfName,
                         INT1 i1State, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (pMacAddr);
    UNUSED_PARAM (pbu1TblFull);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1IfName);
    UNUSED_PARAM (i1State);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}

#endif
/* end of file */
