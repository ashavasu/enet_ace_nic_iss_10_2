/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpnp.c,v 1.3 2014/12/10 12:53:49 siva Exp $
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/
#ifndef _IGMPNP_C_
#define _IGMPNP_C_

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "npapi.h"
#include "igmpnp.h"

/*****************************************************************************/
/* Function Name      : FsIgmpHwEnableIgmp                                   */
/*                                                                           */
/* Description        : This function is called when IGMP is enabled. It     */
/*                      sets up a filter in the hardware so that IGMP packets*/
/*                      are received in the cPU.                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                                    */
/*****************************************************************************/

INT4
FsIgmpHwEnableIgmp (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsIgmpHwDisableIgmp                                  */
/*                                                                           */
/* Description        : This function is called when IGMP is disabled. It is */
/*                      called to removed the installed filters for IGMP     */
/*                      packets.                                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsIgmpHwDisableIgmp (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsNpIpv4SetIgmpIfaceJoinRate                         */
/*                                                                           */
/* Description        : This function is called when the rate for IGMP join  */
/*                      packets are configured in the interface.             */
/*                      It sets the rate at which the join packets must be   */
/*                      accepted in an IGMP interface                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsNpIpv4SetIgmpIfaceJoinRate (INT4 i4IfIndex, INT4 i4RateLimit)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4RateLimit);
    return FNP_SUCCESS;
}

#endif
