/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpnp.c,v 1.4 2009/09/22 14:09:59 prabuc Exp $
 *
 * Description: All prototypes for Network Processor API functions
 *              done here
 *******************************************************************/

#include "lr.h"
#include "cfa.h"
#include "npapi.h"
#include "mrp.h"
#include "npmrp.h"

/*****************************************************************************
*    Function Name       : FsMiMrpHwSetMvrpStatus                              
*                                                                           
*    Description         : This function will be used to enable/disable MVRP 
*                          in H/W                                         
*    Input(s)            : pMrpHwInfo - Pointer to MrpHwInfo structure
*                                                                           
*    Output(s)           : None                                             
*                                                                           
*    Returns            : FNP_SUCCESS / FNP_FAILURE                         
*****************************************************************************/
INT4
FsMiMrpHwSetMvrpStatus (tMrpHwInfo * pMrpHwInfo)
{
    UNUSED_PARAM (pMrpHwInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************
*    Function Name       : FsMiMrpHwSetMmrpStatus                              
*                                                                           
*    Description         : This function will be used to enable/disable MMRP
*                          in H/W                                         
*    Input(s)            : pMrpHwInfo - Pointer to MrpHwInfo structure 
*                                                                           
*    Output(s)           : None                                             
*                                                                           
*    Returns            : FNP_SUCCESS / FNP_FAILURE                         
*****************************************************************************/
INT4
FsMiMrpHwSetMmrpStatus (tMrpHwInfo * pMrpHwInfo)
{
    UNUSED_PARAM (pMrpHwInfo);
    return FNP_SUCCESS;
}
