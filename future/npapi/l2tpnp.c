#ifndef _L2TPNP_C_
#define _L2TPNP_C_
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l2tpnp.c,v 1.1 2016/07/12 11:02:12 siva Exp $
 *
 * Description: All prototypes for Network Processor API functions
 *              done here
 *******************************************************************/
#include "lr.h"
#include "npl2tp.h"

INT4
FsHwL2tpFilterCreate (tL2tpFilterInfo *pL2tpFilterInfo)
{
    UNUSED_PARAM (pL2tpFilterInfo);
    return FNP_SUCCESS;
}

/********************************************************************************/
/*                                                                              */
/*    FUNCTION NAME    : L2tpHandleFilterIdFromXConnectId                       */
/*                                                                              */
/*    DESCRIPTION      : Function to Store Filter Id based on the XConnect      */
/*                       Id attachment circuit                                  */
/*                                                                              */
/*    INPUT            : u4XConnectId   - X Connect Id                          */
/*                                                                              */
/*    OUTPUT           : None                                                   */
/*                                                                              */
/*    RETURNS          : None                                                   */
/*                                                                              */
/********************************************************************************/

INT4
L2tpHandleFilterIdFromXConnectId(UINT4 u4XConnectId,UINT1 u1Opcode,INT4 i4L2tpFilterNo)
{
    UNUSED_PARAM (u4XConnectId);
    UNUSED_PARAM (u1Opcode);
    UNUSED_PARAM (i4L2tpFilterNo);
    return L2TP_NP_SUCCESS; 
}
/********************************************************************************/
/*                                                                              */
/*    FUNCTION NAME    : L2tpCalcFilterIdFromXConnectId                        */
/*                                                                              */
/*    DESCRIPTION      : Function to Store Filter Id based on the XConnect      */
/*                       Id attachment circuit                                  */
/*                                                                              */
/*    INPUT            : u4XConnectId   - X Connect Id                          */
/*                                                                              */
/*    OUTPUT           : None                                                   */
/*                                                                              */
/*    RETURNS          : None                                                   */
/*                                                                              */
/********************************************************************************/

VOID
L2tpCalcFilterIdFromXConnectId (UINT4 u4XConnectId,INT4 *pi4L2tpFilterNo)
{
    UNUSED_PARAM (u4XConnectId);
    UNUSED_PARAM (pi4L2tpFilterNo);
    return; 
}
/********************************************************************************/
/*                                                                              */
/*    FUNCTION NAME    : L2tpFetchFilterIdFromXConnectId                        */
/*                                                                              */
/*    DESCRIPTION      : Function to Store Filter Id based on the XConnect      */
/*                       Id attachment circuit                                  */
/*                                                                              */
/*    INPUT            : u4XConnectId - X Connect Id                            */
/*                                                                              */
/*    OUTPUT           : None                                                   */
/*                                                                              */
/*    RETURNS          : None                                                   */
/*                                                                              */
/********************************************************************************/

INT4
L2tpFetchFilterIdFromXConnectId(UINT4 u4XConnectId,INT4 *pi4L2tpFilterNo)
{
    UNUSED_PARAM (u4XConnectId);
    UNUSED_PARAM (pi4L2tpFilterNo);
    return L2TP_NP_SUCCESS;
}
#endif
