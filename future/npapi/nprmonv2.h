/*****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: nprmonv2.h,v 1.2 2009/09/18 12:28:27 prabuc Exp $
 *
 * Description: This file contains the function implementations  of
 *              RMON NP-API.
 ****************************************************************************/

#ifndef _NPRMONV2_H
#define _NPRMONV2_H

#include "rmon2np.h" 
#include "npcfa.h" 
#endif
