/*$Id: qosxnp.c,v 1.14 2016/05/25 10:04:39 siva Exp $*/
/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qosxnp.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-NPAPI_STUBS                                 */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This file contains the NPAPI stub functions     */
/*                          implementations for Aricent QoS Module.         */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/

#ifndef __QOSX_NP_C__
#define __QOSX_NP_C__
#include "npqosx.h"

/*****************************************************************************/
/* Function Name      : QoSHwInit                                            */
/*                                                                           */
/* Description        : This function intialises the QoS Engine in the       */
/*                       Hardware device                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwInit (VOID)
{
    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwMapClassToPolicy                                */
/*                                                                           */
/* Description        : This function used to configure the set traffic into */
/*                       a CLASS and Policy for it.                          */
/*                                                                           */
/* Input(s)           : pClsMapEntry -  Ptr for a ClassMapEntry              */
/*                    : pPlyMapEntry -  Ptr for a PolicyMapEntry             */
/*                    : pInProActEntry -Ptr for a In Profile Action Entry    */
/*                    : pOutProActEntry-Ptr for a Out Profile Action Entry   */
/*                    : pMeterEntry    -Ptr for a Meter entry                */
/*                    : u1Flag         -  QOS_PLY_ADD / QOS_PLY_MAP          */
/*                      QOS_PLY_ADD - Policy needs to be created and map     */
/*                                    the Class and Policy                   */
/*                      QOS_PLY_MAP - Map the Class with already created     */
/*                                    Policy.                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwMapClassToPolicy (tQoSClassMapEntry * pClsMapEntry,
                       tQoSPolicyMapEntry * pPlyMapEntry,
                       tQoSInProfileActionEntry * pInProActEntry,
                       tQoSOutProfileActionEntry * pOutProActEntry,
                       tQoSMeterEntry * pMeterEntry, UINT1 u1Flag)
{
    UNUSED_PARAM (pClsMapEntry);
    UNUSED_PARAM (pPlyMapEntry);
    UNUSED_PARAM (pInProActEntry);
    UNUSED_PARAM (pOutProActEntry);
    UNUSED_PARAM (pMeterEntry);
    UNUSED_PARAM (u1Flag);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwUpdatePolicyMapForClass                         */
/*                                                                           */
/* Description        : This function used to update the Policy Parameters   */
/*                      for a CLASS of traffic.                              */
/*                                                                           */
/* Input(s)           : pClsMapEntry -  Ptr for a ClassMapEntry              */
/*                    : pPlyMapEntry -  Ptr for a PolicyMapEntry             */
/*                    : pInProActEntry -Ptr for a In Profile Action Entry    */
/*                    : pOutProActEntry-Ptr for a Out Profile Action Entry   */
/*                    : pMeterEntry    -Ptr for a Meter entry                */
/*                    : u1Flag         -Updated Field in the In pPlyMapEntry,*/
/*                                      pInProActEntry, pOutProActEntry      */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwUpdatePolicyMapForClass (tQoSClassMapEntry * pClsMapEntry,
                              tQoSPolicyMapEntry * pPlyMapEntry,
                              tQoSInProfileActionEntry * pInProActEntry,
                              tQoSOutProfileActionEntry * pOutProActEntry,
                              tQoSMeterEntry * pMeterEntry, UINT1 u1Flag)
{
    UNUSED_PARAM (pClsMapEntry);
    UNUSED_PARAM (pPlyMapEntry);
    UNUSED_PARAM (pInProActEntry);
    UNUSED_PARAM (pOutProActEntry);
    UNUSED_PARAM (pMeterEntry);
    UNUSED_PARAM (u1Flag);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwUnmapClassFromPolicy                            */
/*                                                                           */
/* Description        : This function is used to Remove the Policy for a     */
/*                      CLASS of traffic.                                    */
/*                                                                           */
/* Input(s)           : pClsMapEntry -  Ptr for a ClassMapEntry              */
/*                    : pPlyMapEntry -  Ptr for a PolicyMapEntry             */
/*                    : pMeterEntry    -Ptr for a Meter entry                */
/*                    : u1Flag     -  QOS_PLY_UNMAP / QOS_PLY_DEL            */
/*                      QOS_PLY_DEL  -  Unmap Class from Policy and delete   */
/*                                      the Policy                           */
/*                      QOS_PLY_UNMAP - Unmap the Class from Policy.         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwUnmapClassFromPolicy (tQoSClassMapEntry * pClsMapEntry,
                           tQoSPolicyMapEntry * pPlyMapEntry,
                           tQoSMeterEntry * pMeterEntry, UINT1 u1Flag)
{
    UNUSED_PARAM (pClsMapEntry);
    UNUSED_PARAM (pPlyMapEntry);
    UNUSED_PARAM (pMeterEntry);
    UNUSED_PARAM (u1Flag);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwDeleteClassMapEntry                             */
/*                                                                           */
/* Description        : This function Delete the Filter From a CLASS         */
/*                                                                           */
/* Input(s)           : pClsMapEntry -  Ptr for a ClassMapEntry              */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwDeleteClassMapEntry (tQoSClassMapEntry * pClsMapEntry)
{
    UNUSED_PARAM (pClsMapEntry);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwMeterCreate                                     */
/*                                                                           */
/* Description        : This function is used to Create Meter in the device  */
/*                                                                           */
/* Input(s)           : pMeterEntry    -Ptr for a Meter entry                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwMeterCreate (tQoSMeterEntry * pMeterEntry)
{
    UNUSED_PARAM (pMeterEntry);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwMeterDelete                                     */
/*                                                                           */
/* Description        : This function is used to Delate a  Meter form the    */
/*                      Hardware device.                                     */
/*                                                                           */
/* Input(s)           : i4MeterId  - Meter Id                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwMeterDelete (INT4 i4MeterId)
{
    UNUSED_PARAM (i4MeterId);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwMeterStatsUpdate                                */
/*                                                                           */
/* Description        : This function is used to Create Meter in the device  */
/*                                                                           */
/* Input(s)           : pMeterEntry    -Ptr for a Meter entry                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwMeterStatsUpdate (UINT4 u4HwFilterId,tQoSMeterEntry * pMeterEntry)
{
    UNUSED_PARAM (u4HwFilterId);
    UNUSED_PARAM (pMeterEntry);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSHwMeterStatsClear                                 */
/*                                                                           */
/* Description        : This function is used to Create Meter in the device  */
/*                                                                           */
/* Input(s)           : u4MererId    -Meter ID                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwMeterStatsClear (UINT4 u4MeterId)
{
    UNUSED_PARAM (u4MeterId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSHwSchedulerAdd                                    */
/*                                                                           */
/* Description        : This function is used to create a Scheduler in the   */
/*                      Hardware device.                                     */
/*                                                                           */
/* Input(s)           : pSchedEntry - Ptr to 'tQoSSchedulerEntry'            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwSchedulerAdd (tQoSSchedulerEntry * pSchedEntry)
{
    UNUSED_PARAM (pSchedEntry);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwSchedulerUpdateParams                           */
/*                                                                           */
/* Description        : This function is used to Update a Scheduler in the   */
/*                      Hardware device.                                     */
/*                                                                           */
/* Input(s)           : pSchedEntry   - Ptr to 'tQoSSchedulerEntry'          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwSchedulerUpdateParams (tQoSSchedulerEntry * pSchedEntry)
{
    UNUSED_PARAM (pSchedEntry);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwSchedulerDelete                                 */
/*                                                                           */
/* Description        : This function is used to Delete a Scheduler in the   */
/*                      Hardware device.                                     */
/*                                                                           */
/* Input(s)           : i4IfIndex - Interface Index                          */
/*                    : u4SchedId - Scheduler Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwSchedulerDelete (INT4 i4IfIndex, UINT4 u4SchedId)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SchedId);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwQueueCreate                                     */
/*                                                                           */
/* Description        : This function is used to Create a Queue in the HW    */
/*                                                                           */
/* Input(s)           : i4IfIndex   - Scheduler and Q Interface.             */
/*                    : u4QId       - Q id                                   */
/*                    : pQEntry     - Ptr to 'tQoSQEntry'                    */
/*                    : pQTypeEntry - Ptr to 'tQoSQtypeEntry'                */
/*                    : papRDCfgEntry - Ptr to  array of ptr type            */
/*                                     'tQoSREDCfgEntry'                     */
/*                                     3 Prts papRDCfgEntry[0-2] for DP      */
/*                                     Low(0), Medium(1), high (2)           */
/*                    : i2HL        - Scheduler Hierarchy level              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwQueueCreate (INT4 i4IfIndex, UINT4 u4QId, tQoSQEntry * pQEntry,
                  tQoSQtypeEntry * pQTypeEntry,
                  tQoSREDCfgEntry * papRDCfgEntry[], INT2 i2HL)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4QId);
    UNUSED_PARAM (pQEntry);
    UNUSED_PARAM (pQTypeEntry);
    UNUSED_PARAM (papRDCfgEntry);
    UNUSED_PARAM (i2HL);
    return (FNP_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : QoSHwQueueDelete                                     */
/*                                                                           */
/* Description        : This function is used to Delete a Queue in the HW    */
/*                                                                           */
/* Input(s)           : i4IfIndex   - Scheduler and Q Interface.             */
/*                    : u4QId       - Q id                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwQueueDelete (INT4 i4IfIndex, UINT4 u4Id)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4Id);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwMapClassToQueue                                 */
/*                                                                           */
/* Description        : This function is used to Map or UnMap a Q and a      */
/*                      Scheduler in the Hardware device.                    */
/*                                                                           */
/* Input(s)           : i4IfIndex   - Scheduler and Q Interface.             */
/*                    : i4ClsOrPriType - Map Type of the 'u4ClsOrPri'        */
/*                               NONE  means CLASS  otherwise                */
/*                               VLAN_PRI/IP_TOS/IP_DSCP/MPLS_EXP/VLAN_DEI   */
/*                    : u4ClsOrPri  - Map Type Value                         */
/*                    : u4QId       - Q id                                   */
/*                    : u1Flag      - Map / Unmap                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwMapClassToQueue (INT4 i4IfIndex, INT4 i4ClsOrPriType, UINT4 u4ClsOrPri,
                      UINT4 u4QId, UINT1 u1Flag)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4ClsOrPriType);
    UNUSED_PARAM (u4ClsOrPri);
    UNUSED_PARAM (u4QId);
    UNUSED_PARAM (u1Flag);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwMapClassToQueueId                               */
/*                                                                           */
/* Description        : This function is used to Map or UnMap a Q and a      */
/*                      Scheduler in the Hardware device.This is the          */
/*                      replication of the function QoSHwMapClassToQueue with*/
/*                      additional paramter pClsMapEntry to get the          */
/*                      hardware handle infromation                          */
/*                                                                           */
/* Input(s)           : i4IfIndex   - Scheduler and Q Interface.             */
/*                    : i4ClsOrPriType - Map Type of the 'u4ClsOrPri'        */
/*                               NONE  means CLASS  otherwise                */
/*                               VLAN_PRI/IP_TOS/IP_DSCP/MPLS_EXP/VLAN_DEI   */
/*                    : u4ClsOrPri  - Map Type Value                         */
/*                    : u4QId       - Q id                                   */
/*                    : u1Flag      - Map / Unmap                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwMapClassToQueueId (tQoSClassMapEntry * pClsMapEntry, INT4 i4IfIndex,
                        INT4 i4ClsOrPriType, UINT4 u4ClsOrPri,
                        UINT4 u4QId, UINT1 u1Flag)
{
    UNUSED_PARAM (pClsMapEntry);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4ClsOrPriType);
    UNUSED_PARAM (u4ClsOrPri);
    UNUSED_PARAM (u4QId);
    UNUSED_PARAM (u1Flag);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwSchedulerHierarchyMap                           */
/*                                                                           */
/* Description        : This function is used to Map Hierarchy Scheduler in  */
/*                      Hardware device.                                     */
/*                                                                           */
/* Input(s)           : i4IfIndex   - Interface Index                        */
/*                    : i4SchedId   - Scheduler ID                           */
/*                    : u2Sweight   - Weight of the Scheduler Hierarchy      */
/*                    : u1Spriority - Priority of the Scheduler Hierarchy    */
/*                    : i4NextSchedId - Next Level Scheduler Id              */
/*                    : i4NextQId   - Next Level Q Id                        */
/*                    : i2HL        - Scheduler Hierarchy level              */
/*                    : u1Flag      - Map / Unmap                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwSchedulerHierarchyMap (INT4 i4IfIndex, UINT4 u4SchedId, UINT2 u2Sweight,
                            UINT1 u1Spriority, UINT4 u4NextSchedId,
                            UINT4 u4NextQId, INT2 i2HL, UINT1 u1Flag)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SchedId);
    UNUSED_PARAM (u2Sweight);
    UNUSED_PARAM (u1Spriority);
    UNUSED_PARAM (u4NextSchedId);
    UNUSED_PARAM (u4NextQId);
    UNUSED_PARAM (i2HL);
    UNUSED_PARAM (u1Flag);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwSetDefUserPriority                              */
/*                                                                           */
/* Description        : This function is used set Port Default User Priority */
/*                      in Hardware device.                                  */
/*                                                                           */
/* Input(s)           : i4Port        - Interface Index                      */
/*                    : i4DefPriority - Default Priority for the given Port  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwSetDefUserPriority (INT4 i4Port, INT4 i4DefPriority)
{
    UNUSED_PARAM (i4Port);
    UNUSED_PARAM (i4DefPriority);
    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwGetMeterStats                                   */
/*                                                                           */
/* Description        : This function is used to Get the Meter Statistics    */
/*                     from the Hardware device.                             */
/*                                                                           */
/* Input(s)           : u4MeterId - Meter Id                                 */
/*                    : u4StatsType - Statistics Type                        */
/*                    : pu8MeterStatsCounter - Ptr to the Stats Value        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwGetMeterStats (UINT4 u4MeterId, UINT4 u4StatsType,
                    tSNMP_COUNTER64_TYPE * pu8MeterStatsCounter)
{
    UNUSED_PARAM (u4MeterId);
    UNUSED_PARAM (u4StatsType);
    UNUSED_PARAM (pu8MeterStatsCounter);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwGetCoSQStats                                    */
/*                                                                           */
/* Description        : This function is used to Get the Queue Statistics    */
/*                      from the Hardware device.                            */
/*                                                                           */
/* Input(s)           : i4IfIndex - Interface Index                          */
/*                    : u4QId       - Q Id                                   */
/*                    : u4StatsType - Statistics Type                        */
/*                    : pu8CoSQStatsCounter  - Ptr to the Stats Value        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwGetCoSQStats (INT4 i4IfIndex, UINT4 u4QId, UINT4 u4StatsType,
                   tSNMP_COUNTER64_TYPE * pu8CoSQStatsCounter)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4QId);
    UNUSED_PARAM (u4StatsType);
    UNUSED_PARAM (pu8CoSQStatsCounter);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : FsQosHwConfigPfc                                     */
/*                                                                           */
/* Description        : This function is used to configure the PFC in HW     */
/*                                                                           */
/* Input(s)           : pQosPfcHwEntry - PFC Hw Entry                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsQosHwConfigPfc (tQosPfcHwEntry * pQosPfcHwEntry)
{

    UNUSED_PARAM (pQosPfcHwEntry);
    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwGetCountActStats                                */
/*                                                                           */
/* Description        : This function gets the CountAct statistics from the  */
/*                      Hardware device                                      */
/*                                                                           */
/* Input(s)           : u4DiffServId                                         */
/*                    : u4StatsType                                          */
/*                                                                           */
/* Output             : pu8RetValDiffServCounter                             */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwGetCountActStats (UINT4 u4DiffServId,
                       UINT4 u4StatsType,
                       tSNMP_COUNTER64_TYPE * pu8RetValDiffServCounter)
{
    UNUSED_PARAM (u4DiffServId);
    UNUSED_PARAM (u4StatsType);
    UNUSED_PARAM (pu8RetValDiffServCounter);
    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwGetAlgDropStats                                 */
/*                                                                           */
/* Description        : This function gets the AlgDrop  statistics from the  */
/*                      Hardware device                                      */
/*                                                                           */
/* Input(s)           : u4DiffServId                                         */
/*                    : u4StatsType                                          */
/*                                                                           */
/* Output             : pu8RetValDiffServCounter                             */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwGetAlgDropStats (UINT4 u4DiffServId,
                      UINT4 u4StatsType,
                      tSNMP_COUNTER64_TYPE * pu8RetValDiffServCounter)
{
    UNUSED_PARAM (u4DiffServId);
    UNUSED_PARAM (u4StatsType);
    UNUSED_PARAM (pu8RetValDiffServCounter);
    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwGetRandomDropStats                              */
/*                                                                           */
/* Description        : This function gets the RandomDrop statistics from the*/
/*                      Hardware device                                      */
/*                                                                           */
/* Input(s)           : u4DiffServId                                         */
/*                    : u4StatsType                                          */
/*                                                                           */
/* Output             : pu8RetValDiffServCounter                             */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwGetRandomDropStats (UINT4 u4DiffServId,
                         UINT4 u4StatsType,
                         tSNMP_COUNTER64_TYPE * pu8RetValDiffServCounter)
{
    UNUSED_PARAM (u4DiffServId);
    UNUSED_PARAM (u4StatsType);
    UNUSED_PARAM (pu8RetValDiffServCounter);
    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwSetPbitPreferenceOverDscp                       */
/*                                                                           */
/* Description        : This function sets the pbit preference over DSCP if  */
/*                      the frame contains both pbit and DSCP                */
/*                                                                           */
/* Input(s)           : i4Port                                               */
/*                    : i4PbitPref                                           */
/*                                                                           */
/* Output             : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
QoSHwSetPbitPreferenceOverDscp (INT4 i4Port, INT4 i4PbitPref)
{
    UNUSED_PARAM (i4Port);
    UNUSED_PARAM (i4PbitPref);
    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwSetCpuRateLimit                                 */
/*                                                                           */
/* Description        : This function is used set the CPU rate limiting      */
/*                      in Hardware device.                                  */
/*                                                                           */
/* Input(s)           : i4CpuQueueId   - CPU Queue Id                        */
/*                    : u4MinRate      - Mininum transmission rate           */
/*                    : u4MaxRate      - Maximum transmission rate           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwSetCpuRateLimit (INT4 i4CpuQueueId, UINT4 u4MinRate, UINT4 u4MaxRate)
{
    UNUSED_PARAM (i4CpuQueueId);
    UNUSED_PARAM (u4MinRate);
    UNUSED_PARAM (u4MaxRate);

    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwMapClasstoPriMap                                */
/*                                                                           */
/* Description        : This function map vlanpri/dscp/exp values to         */
/*                        internal priority                                  */
/*                                                                           */
/* Input(s)           : pQosClassToPriMapEntry                               */
/*                                                                           */
/* Output             : Field processor entry id if prio type is exp         */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QoSHwMapClasstoPriMap (tQosClassToPriMapEntry * pQosClassToPriMapEntry)
{
    UNUSED_PARAM (pQosClassToPriMapEntry);
    return (FNP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosHwMapClassToIntPriority                           */
/*                                                                           */
/* Description        : This function associatess L2/L3 filter classified    */
/*                          traffic to internal priority                     */
/*                                                                           */
/* Input(s)           : pQosClassToIntPriEntryi                              */
/*                                                                           */
/* Output             : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
QosHwMapClassToIntPriority (tQosClassToIntPriEntry * pQosClassToIntPriEntry)
{
    UNUSED_PARAM (pQosClassToIntPriEntry);
    return (FNP_SUCCESS);
}
/*****************************************************************************/
/* Function Name      : QosHwGetDscpQueueMap                                 */
/*                                                                           */
/* Description        : This function used to get the DSCP to Queue mapping  */
/*                       information configured in the hardware              */
/*                                                                           */
/* Input(s)           : i4QosDscpVal - DscpValue                             */
/*                    : i4QosQueID   - Queue ID                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT1
QosHwGetDscpQueueMap (INT4  i4QosDscpVal, INT4 * i4QosQueID)
{

    UNUSED_PARAM(i4QosDscpVal);
    UNUSED_PARAM(i4QosQueID);

    return FNP_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : QoSHwSetTrafficClassToPcp                            */
/*                                                                           */
/* Description        : This function sets the Traffic class to Pcp in h/w   */
/*                                                                           */
/*                                                                           */
/* Input(s)           : i4IfIndex                                            */
/*                    : i4UserPriority                                       */
/*                    : i4TrafficClass                                       */
/* Output             : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT1
QoSHwSetTrafficClassToPcp (INT4 i4IfIndex, INT4 i4Priority, INT4 i4TrafficClass)
{
    UNUSED_PARAM(i4IfIndex);
    UNUSED_PARAM(i4Priority);
    UNUSED_PARAM(i4TrafficClass);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSHwSetVlanQueuingStatus                            */
/*                                                                           */
/* Description        : This function Creates the Hierarchical Scheduling    */
/*                      Schema to perform VLAN based Subscriber Queue-Mapping*/
/*                                                                           */
/*                                                                           */
/* Input(s)           : i4IfIndex                                            */
/*                    : i4UserPriority                                       */
/*                    : i4TrafficClass                                       */
/* Output             : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT1
QoSHwSetVlanQueuingStatus (INT4 i4IfIndex, tQoSQEntry  *pQEntrySubscriber)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pQEntrySubscriber);

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsQosHwGetPfcStats                                   */
/*                                                                           */
/* Description        : This function is used to Get PFC Statistics          */
/*                                                                           */
/* Input(s)           : QosPfcHwStats                                        */
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
UINT1 FsQosHwGetPfcStats (tQosPfcHwStats *pPfcHwStats) 
{
    UNUSED_PARAM(pPfcHwStats);
    return FNP_SUCCESS;
}
#endif /* __QOSX_NP_C__ */
