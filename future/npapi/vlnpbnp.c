/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnpbnp.c,v 1.12 2012/07/26 09:42:24 siva Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/
#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "npapi.h"
#include "npvlnpb.h"

/*****************************************************************************/
/* Function Name      : FsVlanHwProviderBridgePortType.                      */
/*                                                                           */
/* Description        : This function is called when the port type is        */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u4PortType - CUSTOMER_NETWORK_PORT/CUSTOMER_EDGE_PORT*/
/*                      PROVIDER_NETWORK_PORT                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanHwSetProviderBridgePortType (UINT4 u4IfIndex, UINT4 u4PortType)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PortType);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwSetPortIngressEtherType.                     */
/*                                                                           */
/* Description        : This function is called when the ingress ether type  */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u2EtherType - Configured Ingress Ether type can take
 *                      <1-65535>                                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanHwSetPortIngressEtherType (UINT4 u4IfIndex, UINT2 u2EtherType)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2EtherType);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwSetPortEgressEtherType.                      */
/*                                                                           */
/* Description        : This function is called when the Egress ether type   */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u2EtherType - Configured Egress Ether type can take
 *                      <1-65535>                                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanHwSetPortEgressEtherType (UINT4 u4IfIndex, UINT2 u2EtherType)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2EtherType);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwSetPortSVlanTranslationStatus.               */
/*                                                                           */
/* Description        : This function is called when the SVLAN Translation   */
/*                      status onfigured for a port.                         */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanHwSetPortSVlanTranslationStatus (UINT4 u4IfIndex, UINT1 u1Status)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwAddSVlanTranslationEntry.                    */
/*                                                                           */
/* Description        : This function is called when an entry is configured  */
/*                      for an VID translation table                         */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u2LocalSVlan - SVlan received in the packet can take 
 *                      < 1-4094 >                                           */
/*                      u2RelaySVlan - SVlan used in the bridge can take 
 *                      < 1-4094 >                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanHwAddSVlanTranslationEntry (UINT4 u4IfIndex, UINT2 u2LocalSVlan,
                                  UINT2 u2RelaySVlan)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalSVlan);
    UNUSED_PARAM (u2RelaySVlan);

    /*
     * The following should be taken care or considered before calling 
     * the HW-API.
     * This API can be called with the following inputs for local and 
     * relay VID.
     -Local and relay VID - valid values (Non-zero values)
     -Local VID:0,        relay VID:nonzero
     -Local VID:non-zero, relay VID:0

     * If the HW maintains ingress and egress VID translation tables,
     * entry must be added in ingress VLAN translation table when the 
     * local VID is not 0. 
     * Entry must be added in Egress VLAN translation table when relay 
     * VLAN-ID is not 0.
     * When both local and relay VLANIDs are not zero, entry must be 
     * added in Ingress and Egress VLAN translation tables.
     */
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwDelSVlanTranslationEntry.                    */
/*                                                                           */
/* Description        : This function is called when an entry is deleted     */
/*                      from an VID translation table                        */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u2LocalSVlan - SVlan received in the packet can 
 *                      take < 1-4094 >                                      */
/*                      u2RelaySVlan - SVlan used in the bridge can take
 *                      < 1-4094 >                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanHwDelSVlanTranslationEntry (UINT4 u4IfIndex, UINT2 u2LocalSVlan,
                                  UINT2 u2RelaySVlan)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalSVlan);
    UNUSED_PARAM (u2RelaySVlan);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwSetPortEtherTypeSwapStatus.                  */
/*                                                                           */
/* Description        : This function is called when the SVLAN Ether type    */
/*                      status onfigured for a port.                         */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanHwSetPortEtherTypeSwapStatus (UINT4 u4IfIndex, UINT1 u1Status)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwAddEtherTypeSwapEntry.                       */
/*                                                                           */
/* Description        : This function is called when an entry is added       */
/*                      to the ether type swap table which is applied in     */
/*                      ingress of a packet.                                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u2LocalEtherType- Ether type received in the packet
 *                      can take < 1-65535 >                                 */
/*                      u2RelayEtherType- Ether type understand by the bridge
 *                      can take < 1-65535 >                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanHwAddEtherTypeSwapEntry (UINT4 u4IfIndex, UINT2 u2LocalEtherType,
                               UINT2 u2RelayEtherType)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalEtherType);
    UNUSED_PARAM (u2RelayEtherType);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwDelEtherTypeSwapEntry.                       */
/*                                                                           */
/* Description        : This function is called when an entry is deleted     */
/*                      to the ether type swap table which is applied in     */
/*                      ingress of a packet.                                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u2LocalEtherType- Ether type received in the packet 
 *                      can take < 1-65535 > 
 *                      u2RelayEtherType - Can take vale <1-65536>           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanHwDelEtherTypeSwapEntry (UINT4 u4IfIndex, UINT2 u2LocalEtherType,
                               UINT2 u2RelayEtherType)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalEtherType);
    UNUSED_PARAM (u2RelayEtherType);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwAddSVlanMap.                                 */
/*                                                                           */
/* Description        : This function is called when an entry is added for   */
/*                      Port in the SVLAN classification tables which is     */
/*                      assigned for that port                               */
/*                                                                           */
/* Input(s)           : VlanSVlanMap-Structure containing info for the       */
/*                      Configured SVlan classification entry.               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanHwAddSVlanMap (tVlanSVlanMap VlanSVlanMap)
{
    UNUSED_PARAM (VlanSVlanMap);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwDelSVlanMap.                                 */
/*                                                                           */
/* Description        : This function is called when an entry is deleted for */
/*                      Port in the SVLAN classification tables which is     */
/*                      assigned for that port                               */
/*                                                                           */
/* Input(s)           : VlanSVlanMap-Structure containing info for the       */
/*                      deleted SVlan classification entry.                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanHwDeleteSVlanMap (tVlanSVlanMap VlanSVlanMap)
{
    UNUSED_PARAM (VlanSVlanMap);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwSetPortSVlanTableType.                       */
/*                                                                           */
/* Description        : This function is called when a port is configured    */
/*                      for a type of SVLAN classification tables which is   */
/*                      used in assigning SVLAN for untagged packets         */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index                               */
/*                      u1TableType - Type of table assigned for that port 
 *                      can take values from the tVlanSVlanTableType enum    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanHwSetPortSVlanClassifyMethod (UINT4 u4IfIndex, UINT1 u1TableType)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1TableType);
    return FNP_SUCCESS;
}



/*****************************************************************************/
/* Function Name      : FsVlanHwPortMacLearningLimit.                        */
/*                                                                           */
/* Description        : This function is called for configuring the Unicast  */
/*                      mac limit for a Port                                 */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Port identifier.                        */
/*                      u4MacLimit - Mac Limit of the Unicast Table can take
 *                      <0-4294967295>                                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanHwPortMacLearningLimit (UINT4 u4IfIndex, UINT4 u4MacLimit)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4MacLimit);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwStaticMulticastMacTableLimit .               */
/*                                                                           */
/* Description        : This function is called for configuring the Multicast*/
/*                      mac limit  for the bridge                            */
/*                                                                           */
/* Input(s)           : u4MacLimit - Mac Limit of the Multicast Table can take
 *                                    <0-4294967295> */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanHwMulticastMacTableLimit (UINT4 u4MacLimit)
{
    UNUSED_PARAM (u4MacLimit);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwResetPortCustomerVlan.                       */
/*                                                                           */
/* Description        : This function is called for resetting the ports      */
/*                      configured customer vlan                             */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanHwResetPortCustomerVlan (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwSetPortCustomerVlan.                         */
/*                                                                           */
/* Description        : This function is called for setting the port         */
/*                      configured customer vlan                             */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                      CVlanId - Configured Customer VLAN ID can 
 *                      take <1-4094>                                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanHwSetPortCustomerVlan (UINT4 u4IfIndex, tVlanId CVlanId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (CVlanId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwCreateProviderEdgePort                       */
/*                                                                           */
/* Description        : This function creates a Provider Edge Port (logical  */
/*                      port) in hardware.                                   */
/*                                                                           */
/* Input(s)           : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId   - Service Vlan Id can take <1-4094>.       */
/*                      PepConfig - Contains values to be used for PVID,     */
/*                                  Default user priority, Acceptable        */
/*                                  frame types, enable ingress filtering    */
/*                                  on port creation.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanHwCreateProviderEdgePort (UINT4 u4IfIndex, tVlanId SVlanId,
                                tHwVlanPbPepInfo PepConfig)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (PepConfig);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwDelProviderEdgePort                          */
/*                                                                           */
/* Description        : This function deletes the Provider Edge Port from    */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id can take <1-4094>.      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanHwDelProviderEdgePort (UINT4 u4IfIndex, tVlanId SVlanId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwSetPepPvid                                   */
/*                                                                           */
/* Description        : This function sets the PVID for the given Provider   */
/*                      Edge Port in hardware.                               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id can take <1-4094>.      */
/*                      Pvid       - PVID to be setcan take <1-4094>.        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanHwSetPepPvid (UINT4 u4IfIndex, tVlanId SVlanId, tVlanId Pvid)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (Pvid);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwSetPepAccFrameType                           */
/*                                                                           */
/* Description        : This function sets the Acceptable frame type for the */
/*                      given Provider Edge Port in hardware.                */
/*                                                                           */
/* Input(s)           : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id can take <1-4094>.      */
/*                      u1AccepFrameType - Acceptable frame type to be set   */
/*                                         for this PEP.Frame types are
 *                      VLAN_ADMIT_ALL_FRAMES,
 *                      VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES,
 *                      VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES.
 *                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanHwSetPepAccFrameType (UINT4 u4IfIndex, tVlanId SVlanId,
                            UINT1 u1AccepFrameType)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (u1AccepFrameType);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwSetPepDefUserPriority                        */
/*                                                                           */
/* Description        : This function sets the Default User priority for the */
/*                      given Provider Edge Port in hardware.                */
/*                                                                           */
/* Input(s)           : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id can take <1-4094>.      */
/*                      i4DefUsrPri  - Default user priority for this PEP 
 *                      can take <0-7>.                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanHwSetPepDefUserPriority (UINT4 u4IfIndex, tVlanId SVlanId,
                               INT4 i4DefUsrPri)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (i4DefUsrPri);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwSetPepIngFiltering                           */
/*                                                                           */
/* Description        : This function sets the Enable Ingress Filetering     */
/*                      parameter for the given Provider Edge Port in        */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id can take <1-4094>.      */
/*                      u1IngFilterEnable - Enable / Disable Ingress         */
/*                                          filtering can take 
 *                                          VLAN_ENABLED/VLAN_DISABLED.      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanHwSetPepIngFiltering (UINT4 u4IfIndex, tVlanId SVlanId,
                            UINT1 u1IngFilterEnable)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (u1IngFilterEnable);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwSetCvidUntagPep                              */
/*                                                                           */
/* Description        : This function sets the UntagPep value to true/false  */
/*                      in CVID registration table.                          */
/*                                                                           */
/* Input(s)           : VlanSVlanMap - Structure filled with C-VID reg       */
/*                      table parameters.                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanHwSetCvidUntagPep (tVlanSVlanMap VlanSVlanMap)
{
    UNUSED_PARAM (VlanSVlanMap);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwSetCvidUntagCep                              */
/*                                                                           */
/* Description        : This function sets the UntagCep value to true/false  */
/*                      in CVID registration table.                          */
/*                                                                           */
/* Input(s)           : VlanSVlanMap - Structure filled with C-VID reg       */
/*                      table parameters.                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanHwSetCvidUntagCep (tVlanSVlanMap VlanSVlanMap)
{
    UNUSED_PARAM (VlanSVlanMap);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwSetPcpEncodTbl.                              */
/*                                                                           */
/* Description        : This function sets/modifies PCP encoding table entry */
/*                      for the given port, given PCP selection row, given   */
/*                      priority, given drop_eligible value.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      NpPbVlanPcpInfo - Entry containing the following 
 *                                                                   fields: */
/*                          u2PcpSelRow - PCP selection row where            */
/*                                        modification will be done can take
 *                                        VLAN_8P0D_SEL_ROW,
 *                                        VLAN_7P1D_SEL_ROW,
 *                                        VLAN_6P2D_SEL_ROW,
 *                                        VLAN_5P3D_SEL_ROW.
 *                          u2Priority  - Priority value can take <0-7>      */
/*                          u1DropEligible - Drop Eligible value should be 
 *                                          VLAN_SNMP_TRUE,VLAN_SNMP_FALSE.
 *                          u2PcpVlaue  - PCP value to be set for the above  */
/*                                        input can take <0-7>.              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanHwSetPcpEncodTbl (UINT4 u4IfIndex, tHwVlanPbPcpInfo NpPbVlanPcpInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (NpPbVlanPcpInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwSetPcpDecodTbl.                              */
/*                                                                           */
/* Description        : This function sets/modifies PCP decoding table entry */
/*                      for the given port, given PCP selection row, given   */
/*                      PCP value.                                           */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      NpPbVlanPcpInfo - Entry containing the following 
 *                                                                   fields: */
/*                          u2PcpSelRow - PCP selection row where            */
/*                                        modification will be done can take
 *                                        VLAN_8P0D_SEL_ROW,
 *                                        VLAN_7P1D_SEL_ROW,
 *                                        VLAN_6P2D_SEL_ROW,
 *                                        VLAN_5P3D_SEL_ROW.
 *                          u2PcpVlaue  - PCP value can take <0-7>           */
/*                          u2Priority  - Priority value to be set for the   */
/*                                        about input (port, PcpSel, pcp).   */
/*                          u1DropEligible - Drop Eligible value to be set   */
/*                                           for the above input             */
/*                                           (port, PcpSel, Pcp). can take   */
/*                                          VLAN_SNMP_TRUE,VLAN_SNMP_FALSE.  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanHwSetPcpDecodTbl (UINT4 u4IfIndex, tHwVlanPbPcpInfo NpPbVlanPcpInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (NpPbVlanPcpInfo);
    return FNP_SUCCESS;
}

/* Use-DEI parameter. */
/*****************************************************************************/
/* Function Name      : FsVlanHwSetPortUseDei.                               */
/*                                                                           */
/* Description        : This function sets the Use_DEI parameter for the     */
/*                      given port.                                          */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u1UseDei  - True/False for Use_DEI parameter for     */
/*                                  this port.can take
 *                                  VLAN_SNMP_TRUE,VLAN_SNMP_FALSE           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanHwSetPortUseDei (UINT4 u4IfIndex, UINT1 u1UseDei)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1UseDei);
    return FNP_SUCCESS;
}

/* Require Drop Encoding parameter. */
/*****************************************************************************/
/* Function Name      : FsVlanHwSetPortReqDropEncoding.                      */
/*                                                                           */
/* Description        : This function sets the Require Drop Encoding         */
/*                      parameter for the given port.                        */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u1ReqDropEncoding  - True/False value for Require    */
/*                                           Drop encoding parameter for     */
/*                                           this port.can take
 *                                           VLAN_SNMP_TRUE, VLAN_SNMP_FALSE */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanHwSetPortReqDropEncoding (UINT4 u4IfIndex, UINT1 u1ReqDrpEncoding)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1ReqDrpEncoding);
    return FNP_SUCCESS;
}

/* Port Priority Code Point Selection Row. */
/*****************************************************************************/
/* Function Name      : FsVlanHwSetPortPcpSelRow.                            */
/*                                                                           */
/* Description        : This function sets the Port Priority Code Point      */
/*                      Selection for the given port.                        */
/*                                                                           */
/* Input(s)           : u4IfIndex       - Port Index.                        */
/*                      u2PcpSelection  - It can take the following values:  */
/*                                        VLAN_8P0D_SEL_ROW,                 */
/*                                        VLAN_7P1D_SEL_ROW,                 */
/*                                        VLAN_6P2D_SEL_ROW,                 */
/*                                        VLAN_5P3D_SEL_ROW.                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanHwSetPortPcpSelection (UINT4 u4IfIndex, UINT2 u2PcpSelection)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2PcpSelection);
    return FNP_SUCCESS;
}

/* Internal CNP - Service Priority Regeneration Table NPAPIs */
/*****************************************************************************/
/* Function Name      : FsVlanHwSetServicePriRegenEntry                      */
/*                                                                           */
/* Description        : This function sets the service priority              */
/*                      regeneration table entry.                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id can take <1-4094>.      */
/*                      i4RecvPriority - Receive Priority can take <0-7>     */
/*                      i4RegenPriority - Regenerated Priority can take <0-7>*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanHwSetServicePriRegenEntry (UINT4 u4IfIndex, tVlanId SVlanId,
                                 INT4 i4RecvPriority, INT4 i4RegenPriority)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (i4RecvPriority);
    UNUSED_PARAM (i4RegenPriority);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwSetTunnelMacAddress                          */
/*                                                                           */
/* Description        : This function creates filters for receiving the      */
/*                      packets destined to proprietary tunnel MAC           */
/*                      address configured for different L2 protocols.       */
/*                                                                           */
/* Input(s)           : MacAddr - MAC address used for protocol tunneling.   */
/*                      u2Protocol - L2 protocol for which the tunnel MAC    */
/*                                   address is configured for tunneling can */
/*                                   take any of the folling.                */
/*                                   VLAN_NP_DOT1X_PROTO_ID,                 */
/*                                   VLAN_NP_LACP_PROTO_ID,                  */
/*                                   VLAN_NP_STP_PROTO_ID,                   */
/*                                   VLAN_NP_GVRP_PROTO_ID,                  */
/*                                   VLAN_NP_GMRP_PROTO_ID,                  */
/*                                   VLAN_NP_IGMP_PROTO_ID                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanHwSetTunnelMacAddress (tMacAddr MacAddr, UINT2 u2Protocol)
{
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u2Protocol);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwGetNextSVlanMap                              */
/*                                                                           */
/* Description        : This function is called to get Cvid Entry for CVlan  */
/*                      for Port in the SVLAN classification tables          */
/*                                                                           */
/* Input(s)           : VlanSVlanMap-Structure containing info for the       */
/*                      deleted SVlan classification entry.                  */
/*                      CvlanId - CVlan Id can take <1-4094>                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanHwGetNextSVlanMap (tVlanSVlanMap VlanSVlanMap,
                         tVlanSVlanMap * pRetVlanSVlanMap)
{
    UNUSED_PARAM (VlanSVlanMap);
    UNUSED_PARAM (pRetVlanSVlanMap);

    /* This stub function should return FNP_FAILURE, otherwise current and
     * next entry will always same (0), in turn fall in to indefinite loop,
     * if call for current entry */

    return FNP_FAILURE;

}

/*****************************************************************************/
/* Function Name      : FsVlanHwGetNextSVlanTranslationEntry                 */
/*                                                                           */
/* Description        : This function is called to get entry                 */
/*                      from a  VID translation table                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2LocalSVlan - SVlan received in the packet can take 
 *                                                                   <1-4094>*/
/*                      pVidTransEntryInfo - Vid Translation Entry           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On VLAN_SUCCESS                       */
/*                      VLAN_FAILURE - On failure                            */
/*****************************************************************************/
INT4
FsVlanHwGetNextSVlanTranslationEntry (UINT4 u4IfIndex,
                                      tVlanId u2LocalSVlan,
                                      tVidTransEntryInfo * pVidTransEntryInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalSVlan);
    UNUSED_PARAM (pVidTransEntryInfo);

    /* This stub function should return FNP_FAILURE, otherwise current and
     * next entry will always same (0), in turn fall in to indefinite loop,
     * if call for current entry */

    return FNP_FAILURE;

}
