/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: esatnp.c,v 1.1 2014/08/14 12:52:04 siva Exp $
 *
 * Description: All prototypes for Network Processor API functions
 *              done here
 *******************************************************************/
#ifndef _ESATNP_C_
#define _ESATNP_C_

#include "lr.h"
#include "npapi.h"
#include "esatnp.h"

/*****************************************************************************/
/* Function Name   : FsNpHwConfigEsat                                        */
/*                                                                           */
/* Description     ; This is an interface function for all hardware          */
/*                   communication                                           */
/* Input           : tEsatHwInfo - structure which has all the necessaery    */
/*                   Details to trigger HW calls.                            */
/*                   u1Command   - option for which Hw call to init          */
/* Output          : NONE                                                    */
/* Return Value(s) : FNP_SUCCESS - On success                                */
/*                   FNP_FAILURE - On failure                                */
/*****************************************************************************/
INT4
FsNpHwConfigEsat (tEsatHwInfo * pEsatHwInfo)
{
    UNUSED_PARAM (pEsatHwInfo);

    return FNP_SUCCESS;
}

#endif
 /*_ESATNP_C_*/
