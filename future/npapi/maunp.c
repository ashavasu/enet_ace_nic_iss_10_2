/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: maunp.c,v 1.5 2007/02/01 14:59:31 iss Exp $
 *
 * Description: This file contains the function implementations of MAU
 *              MIB NP-API.
 ****************************************************************************/

#include "lr.h"
#include "npmau.h"
#include "npapi.h"
#include "cfa.h"

/****************************************************************************
 Function    : FsMauHwGetMauType
 Input       : u4IfIndex : the interface to which the MAU is connected 
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pu4Val         : pointer to the information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwGetMauType (UINT4 u4IfIndex, INT4 i4IfMauId, UINT4 *pu4Val)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);

    *pu4Val = MAU_TYPE_AUI;

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwSetMauType
 Input       : u4IfIndex : the interface to which the MAU is connected 
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information to be set
 Output      : The hardware routine takes the indicies & sets
               the requested value in the hardware.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwSetMauStatus (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwGetStatus
 Input       : u4IfIndex : the interface to which the MAU is connected 
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwGetStatus (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwGetMediaAvailable
 Input       : u4IfIndex : the interface to which the MAU is connected 
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwGetMediaAvailable (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwGetMediaAvailStateExits
 Input       : u4IfIndex : the interface to which the MAU is connected 
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwGetMediaAvailStateExits (UINT4 u4IfIndex,
                                INT4 i4IfMauId, UINT4 *pElement)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwGetJabberState
 Input       : u4IfIndex : the interface to which the MAU is connected 
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwGetJabberState (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwGetJabberingStateEnters
 Input       : u4IfIndex : the interface to which the MAU is connected 
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwGetJabberingStateEnters (UINT4 u4IfIndex,
                                INT4 i4IfMauId, UINT4 *pElement)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwGetFalseCarriers
 Input       : u4IfIndex : the interface to which the MAU is connected 
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwGetFalseCarriers (UINT4 u4IfIndex, INT4 i4IfMauId, UINT4 *pElement)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwSetDefaultType
 Input       : u4IfIndex : the interface to which the MAU is connected 
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               u4Val          : the value to be set
 Output      : The hardware routine takes the indicies & sets
               the requested value in the hardware.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwSetDefaultType (UINT4 u4IfIndex, INT4 i4IfMauId, UINT4 u4Val)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (u4Val);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwGetDefaultType
 Input       : u4IfIndex : the interface to which the MAU is connected 
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwGetDefaultType (UINT4 u4IfIndex, INT4 i4IfMauId, UINT4 *pu4Val)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pu4Val);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwGetAutoNegSupported
 Input       : u4IfIndex : the interface to which the MAU is connected 
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwGetAutoNegSupported (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwGetTypeListBits
 Input       : u4IfIndex : the interface to which the MAU is connected 
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwGetTypeListBits (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwGetJackType
 Input       : u4IfIndex : the interface to which the MAU is connected 
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwGetJackType (UINT4 u4IfIndex,
                    INT4 i4IfMauId, INT4 i4ifJackIndex, INT4 *pElement)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (i4ifJackIndex);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwSetAutoNegAdminStatus
 Input       : u4IfIndex : the interface to which the MAU is connected 
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information to be set
 Output      : The hardware routine takes the indicies & sets
               the requested value in hardware.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwSetAutoNegAdminStatus (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwGetAutoNegAdminStatus
 Input       : u4IfIndex : the interface to which the MAU is connected 
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwGetAutoNegAdminStatus (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{

    /* NOTE: Call the required Driver API to get the Port's Auto Negotiation
     * Status. If Auto Negotiation is Enabled on the Port, assign 
     * ENET_AUTO_ENABLE to pElement, else assign ENET_AUTO_DISABLE.
     */

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwGetAutoNegRemoteSignaling
 Input       : u4IfIndex : the interface to which the MAU is connected 
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwGetAutoNegRemoteSignaling (UINT4 u4IfIndex,
                                  INT4 i4IfMauId, INT4 *pElement)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwGetAutoNegAdminConfig
 Input       : u4IfIndex : the interface to which the MAU is connected 
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwGetAutoNegAdminConfig (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwSetAutoNegRestart
 Input       : u4IfIndex : the interface to which the MAU is connected 
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information to be set
 Output      : The hardware routine takes the indicies & sets
               the requested value in the hardware.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwSetAutoNegRestart (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwGetAutoNegRestart
 Input       : u4IfIndex : the interface to which the MAU is connected 
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwGetAutoNegRestart (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwGetAutoNegCapBits
 Input       : u4IfIndex : the interface to which the MAU is connected
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwGetAutoNegCapBits (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwSetAutoNegCapAdvtBits
 Input       : u4IfIndex : the interface to which the MAU is connected
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information to be set
 Output      : The hardware routine takes the indicies & sets
               the requested value in hardware.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/
INT4
FsMauHwSetAutoNegCapAdvtBits (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwGetAutoNegCapAdvtBits
 Input       : u4IfIndex : the interface to which the MAU is connected
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwGetAutoNegCapAdvtBits (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwGetAutoNegCapRcvdBits
 Input       : u4IfIndex : the interface to which the MAU is connected
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/
INT4
FsMauHwGetAutoNegCapRcvdBits (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwGetAutoNegRemFltAdvt
 Input       : u4IfIndex : the interface to which the MAU is connected
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwGetAutoNegRemFltAdvt (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwSetAutoNegRemFltAdvt
 Input       : u4IfIndex : the interface to which the MAU is connected
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information to be set
 Output      : The hardware routine takes the indicies & sets
               the requested value in hardware.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwSetAutoNegRemFltAdvt (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}

/****************************************************************************
 Function    : FsMauHwGetAutoNegRemFltRcvd
 Input       : u4IfIndex : the interface to which the MAU is connected
                                (= IfIndex).
               i4IfMauId   : the MAU identifier
               pElement       : pointer to the information requested
 Output      : The hardware routine takes the indicies & stores
               the requested value as the content of pElement.
 Returns     : FNP_SUCESS or FNP_FAILURE
****************************************************************************/

INT4
FsMauHwGetAutoNegRemFltRcvd (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfMauId);
    UNUSED_PARAM (pElement);

    return (FNP_SUCCESS);
}
