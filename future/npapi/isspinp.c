/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : isspinp.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Port Isolation                                 */
/*    MODULE NAME           : ISS                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE : 23 July 2010                                   */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains definitions for  ISS Port   */
/*                            isolation related NP-API.                      */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    23 July 2010           Initial Creation                        */
/*---------------------------------------------------------------------------*/
#ifndef ISSPINP_C
#define ISSPINP_C

#include "lr.h"
#include "iss.h"
#include "npapi.h"
#include "npiss.h"
#include "isspinp.h"

/*****************************************************************************/
/*    Function Name       : IssHwConfigPortIsolationEntry                    */
/*                                                                           */
/*    Description         : This functions creates the Port Isolation entry  */
/*                        : in the hardware                                  */
/*                                                                           */
/*    Input(s)            : pPortIsolation - ptr to update Port Isolation    */
/*                          details in PI table.                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
IssHwConfigPortIsolationEntry (tIssHwUpdtPortIsolation * pPortIsolation)
{
    UNUSED_PARAM (pPortIsolation);
    return FNP_SUCCESS;
}
#endif /* ISSPINP_C */
