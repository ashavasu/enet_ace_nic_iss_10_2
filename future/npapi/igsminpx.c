/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igsminpx.c,v 1.4 2010/10/20 13:19:12 prabuc Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/
#ifndef _IGSMINPX_C_
#define _IGSMINPX_C_

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "snp.h"
#include "npapi.h"
#include "npigsmi.h"

/*****************************************************************************/
/* Function Name      : FsMiIgsMbsmHwEnableIgmpSnooping                      */
/*                                                                           */
/* Description        : This function enables IGMP snooping feature in the   */
/*                      hardware. Filters are added for receiving IGMP       */
/*                      L2MC table is initialized for building MAC based     */
/*                      Multicast data forwarding                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      pSlotInfo   - Pointer to Slot information structure  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiIgsMbsmHwEnableIgmpSnooping (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiIgsMbsmHwEnableIpIgmpSnooping                    */
/*                                                                           */
/* Description        : This function enables IGMP snooping feature in the   */
/*                      hardware. Filters are added for receiving IGMP       */
/*                      IPMC table is initialized for building IP based      */
/*                      Multicast data forwarding                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      pSlotInfo   - Pointer to Slot information structure  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiIgsMbsmHwEnableIpIgmpSnooping (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiIgsMbsmHwEnhancedMode                            */
/*                                                                           */
/* Description        : This function enables IGMP Enhanced mode in the      */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Indentifer                     */
/*                      u1EnhStatus - Enhanced Mode Status                   */
/*                      pSlotInfo   - Pointer to Slot information structure  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiIgsMbsmHwEnhancedMode (UINT4 u4ContextId, UINT1 u1EnhStatus,
                           tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1EnhStatus);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiIgsMbsmHwPortRateLimit                           */
/*                                                                           */
/* Description        : This function configures IGMP Rate Limit for a       */
/*                      downstream interface in hardware. In default mode,   */
/*                      the downstream interface corresponds to only port and*/
/*                      inner-vlan will be 0. When enhanced mode is          */
/*                      enabled, downstream interface corresponds to port and*/
/*                      inner-vlan.In this mode inner-vlan may be within the */
/*                      valid vlan range or 0.When rate limit is             */
/*                      configured for an interface, this API will be invoked*/
/*                      with action as .ADD. and u4Rate as the required rate.*/
/*                      When rate limit is configured as zero (rate limit all*/
/*                      packets on this interface) this API will be invoked  */
/*                      with action as .ADD. and u4Rate as zero. When rate   */
/*                      limit is configured to default value ( no rate       */
/*                      limiting to be applied on this interface) this API   */
/*                      will be invoked with action as .REMOVE. and the      */
/*                      u4Rate can be ignored.                               */
/*                                                                           */
/* Input(s)           : UINT4 u4Instance        :  u4Instance                */
/*                    : Struct tIgsHwRateLmt - It contains                   */
/*                      UINT4 u4Port            :  Port Number               */
/*                      tVlanId InnerVlanId     :  Inner-Vlan                */
/*                      UINT4 u4Rate            :  No of packets per second  */
/*                                                                           */
/*                    : UINT1 u1Action - Type of action to be performed.     */
/*                                        ( Add/Remove Rate-limit for port ) */
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      /*                    : pSlotInfo   - Pointer to Slot information structure  *//*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiIgsMbsmHwPortRateLimit (UINT4 u4Instance, tIgsHwRateLmt * pIgsHwRateLmt,
                            UINT1 u1Action, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4Instance);
    UNUSED_PARAM (pIgsHwRateLmt);
    UNUSED_PARAM (u1Action);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiIgsMbsmHwSparseMode                              */
/*                                                                           */
/* Description        : This function enables/disables IGMP sparse mode in   */
/*                      the hardware.                                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      u1SparseStatus - Sparse Mode Status                  */
/*                      pSlotInfo   - Pointer to Slot information structure  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiIgsMbsmHwSparseMode (UINT4 u4ContextId, UINT1 u1SparseStatus,
                         tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1SparseStatus);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

#endif
