/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pnacnp.c,v 1.9 2007/07/17 13:29:53 iss Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/

#include "lr.h"
#include "cfa.h"
#include "pnac.h"
#include "npapi.h"
#include "nppnac.h"

/*************************************************************************/
/* Function Name      : PnacHwEnable                                     */
/*                                                                       */
/* Description        : This function is called when PNAC module is 
 *                      enabled through Manager routines. It does the 
 *                      following:                                       */
/*                      1. Flushes all existing address entries in h/w 
 *                      table                                            */
/*                      2. Registers for PAE group address in h/w        */
/*                                                                       */
/* Input(s)           : None                                             */
/*                                                                       */
/* Output(s)          : None.                                            */
/*                                                                       */
/* Global Variables                                                      */
/* Referred           : None.                                            */
/*                                                                       */
/* Global Variables                                                      */
/* Modified           : None.                                            */
/*                                                                       */
/* Return Value(s)    : FNP_SUCCESS or FNP_FAILURE.                      */
/*************************************************************************/
INT4
PnacHwEnable (VOID)
{
    return FNP_SUCCESS;
}

/*************************************************************************/
/* Function Name      : PnacHwDisable                                    */
/*                                                                       */
/* Description        : This function is called when PNAC module is 
 *                      disabled                                         */
/*                      through Manager routines. It does the following: */
/*                      1. Unregisters PAE group address in h/w          */
/*                                                                       */
/* Input(s)           : None                                             */
/*                                                                       */
/* Output(s)          : None.                                            */
/*                                                                       */
/* Global Variables                                                      */
/* Referred           : None.                                            */
/*                                                                       */
/* Global Variables                                                      */
/* Modified           : None.                                            */
/*                                                                       */
/* Return Value(s)    : FNP_SUCCESS or FNP_FAILURE.                      */
/*************************************************************************/
INT4
PnacHwDisable (VOID)
{
    return FNP_SUCCESS;
}

/*************************************************************************/
/* Function Name      : PnacHwSetAuthStatus                              */
/*                                                                       */
/* Description        : This function is called when hardware module is  */
/*                      present and the port authorization status changes*/
/*                      Called to set the authorization status in the    */
/*                      Hardware for the Port or MAC concerned. This 
 *                      routine                                          */
/*                      configures the hardware to control the traffic of*/
/*                      packets.                                         */
/* Input(s)           : u2PortNum - Port Number                          */
/*                      pu1SuppAddr - Supplicant MAC address 
 *                      (in port-based authentication case, this argument 
 *                      is NULL)                                         */
/*                      u1AuthMode - Authentication mode (Values can be  */
/*                               PNAC_PORT_AUTHMODE_PORTBASED or         */
/*                               PNAC_PORT_AUTHMODE_MACBASED)            */
/*                      u1AuthStatus - Authorization Status              */
/*                           ( Values can be PNAC_PORTSTATUS_AUTHORIZED 
 *                           or PNAC_PORTSTATUS_UNAUTHORIZED)            */
/*                      u1CtrlDir - Oper Controlled directions 
 *                          ( PNAC_CNTRLD_DIR_BOTH or PNAC_CNTRLD_DIR_IN)*/
/*                                                                       */
/* Output(s)          : None.                                            */
/*                                                                       */
/* Global Variables                                                      */
/* Referred           : None.                                            */
/*                                                                       */
/* Global Variables                                                      */
/* Modified           : None.                                            */
/*                                                                       */
/* Return Value(s)    : FNP_SUCCESS or FNP_FAILURE.                      */
/*************************************************************************/
INT4
PnacHwSetAuthStatus (UINT2 u2PortNum, UINT1 *pu1SuppAddr,
                     UINT1 u1AuthMode, UINT1 u1AuthStatus, UINT1 u1CtrlDir)
{
    UNUSED_PARAM (u2PortNum);
    UNUSED_PARAM (pu1SuppAddr);
    UNUSED_PARAM (u1AuthMode);
    UNUSED_PARAM (u1AuthStatus);
    UNUSED_PARAM (u1CtrlDir);
    return FNP_SUCCESS;
}

/* PNAC_COUNTERS */

/*************************************************************************/
/* Function Name      : PnacHwStartSessionCounters                       */
/*                                                                       */
/* Description        : This function starts the session counters of the */
/*                      authenticated session, once the session time 
 *                      starts.                                          */
/* Input(s)           : u2PortNum - Port Number                          */
/*                      pu1SuppAddr - Supplicant MAC address 
 *                      (in port-based authentication case, this argument 
 *                      is NULL )                                        */
/*                                                                       */
/* Output(s)          : None.                                            */
/*                                                                       */
/* Global Variables                                                      */
/* Referred           : None.                                            */
/*                                                                       */
/* Global Variables                                                      */
/* Modified           : None.                                            */
/*                                                                       */
/* Return Value(s)    : FNP_SUCCESS or FNP_FAILURE.                      */
/*************************************************************************/
INT4
PnacHwStartSessionCounters (UINT2 u2PortNum, UINT1 *pu1SuppAddr)
{
    UNUSED_PARAM (u2PortNum);
    UNUSED_PARAM (pu1SuppAddr);
    return FNP_SUCCESS;

}

/*************************************************************************/
/* Function Name      : PnacHwGetSessionCounter                          */
/*                                                                       */
/* Description        : This function gets the session counter value of 
 *                     the latest authenticated session, one counter at 
 *                     a time                                            */
/* Input(s)           : u2PortNum - Port Number                          */
/*                      pu1SuppAddr - Supplicant MAC address 
 *                      (in port-based authentication case, this 
 *                      argument is not  valid)                          */
/*                      u1CounterType - PNAC_COUNT_SESS_OCTETS_RX  or    */
/*                              PNAC_COUNT_SESS_OCTETS_TX  or            */
/*                              PNAC_COUNT_SESS_FRAMES_RX  or            */
/*                              PNAC_COUNT_SESS_FRAMES_TX.               */
/*                                                                       */
/* Output(s)          : pu4HiCounter - higher 4 bytes of counter's value */
/*                      pu4LoCounter - lower 4 bytes of counter's value  */
/*                                                                       */
/* Global Variables                                                      */
/* Referred           : None.                                            */
/*                                                                       */
/* Global Variables                                                      */
/* Modified           : None.                                            */
/*                                                                       */
/* Return Value(s)    : FNP_SUCCESS or FNP_FAILURE.                      */
/*************************************************************************/
INT4
PnacHwGetSessionCounter (UINT2 u2PortNum, UINT1 *pu1SuppAddr,
                         UINT1 u1CounterType, UINT4 *pu4HiCounter,
                         UINT4 *pu4LoCounter)
{
    UNUSED_PARAM (u2PortNum);
    UNUSED_PARAM (pu1SuppAddr);
    UNUSED_PARAM (u1CounterType);
    UNUSED_PARAM (pu4HiCounter);
    UNUSED_PARAM (pu4LoCounter);
    return FNP_SUCCESS;

}

/**************************************************************************/
/* Function Name      : PnacHwStopSessionCounters                         */
/*                                                                        */
/* Description        : This function stops the session counters of the   */
/*                      authenticated session, once the session time ends.*/
/* Input(s)           : u2PortNum - Port Number                           */
/*                      pu1SuppAddr - Supplicant MAC address (in 
 *                      port-based authentication case, this argument is 
 *                      not valid)                                        */
/*                                                                        */
/* Output(s)          : None.                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Referred           : None.                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified           : None.                                             */
/*                                                                        */
/* Return Value(s)    : FNP_SUCCESS or FNP_FAILURE.                       */
/**************************************************************************/
INT4
PnacHwStopSessionCounters (UINT2 u2PortNum, UINT1 *pu1SuppAddr)
{
    UNUSED_PARAM (u2PortNum);
    UNUSED_PARAM (pu1SuppAddr);
    return FNP_SUCCESS;

}

/**************************************************************************/
/* Function Name      : PnacHwGetAuthStatus                               */
/*                                                                        */
/* Description        : This function is called when hardware module is   */
/*                      present and the port authorization status changes.*/
/*                      Called to set the authorization status in the     */
/*                      Hardware for the Port or MAC concerned. This 
 *                      routine configures the hardware to control the 
 *                      traffic of packets.                               */
/* Input(s)           : u2PortNum - Port Num                              */
/*                      pu1SuppAddr - Supplicant MAC address (in 
 *                      port-based authentication case, this argument is 
 *                      not  valid)                                       */
/*                      u1AuthMode - Authentication mode (Values can be   */
/*                               PNAC_PORT_AUTHMODE_PORTBASED or          */
/*                               PNAC_PORT_AUTHMODE_MACBASED)             */
/*                      u1AuthStatus - Authorization Status               */
/*                           ( Values can be PNAC_PORTSTATUS_AUTHORIZED or*/
/*                             PNAC_PORTSTATUS_UNAUTHORIZED)              */
/*                      u1CtrlDir - Oper Controlled directions            */
/*                          ( PNAC_CNTRLD_DIR_BOTH or PNAC_CNTRLD_DIR_IN )*/
/* Output(s)          : None.                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Referred           : None.                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified           : None.                                             */
/*                                                                        */
/* Return Value(s)    : FNP_SUCCESS or FNP_FAILURE.                       */
/**************************************************************************/
INT4
PnacHwGetAuthStatus (UINT2 u2PortNum, UINT1 *pu1SuppAddr,
                     UINT1 u1AuthMode, UINT2 *pu2AuthStatus, UINT2 *pu2CtrlDir)
{
    UNUSED_PARAM (u2PortNum);
    UNUSED_PARAM (pu1SuppAddr);
    UNUSED_PARAM (u1AuthMode);
    UNUSED_PARAM (pu2AuthStatus);
    UNUSED_PARAM (pu2CtrlDir);
    return FNP_SUCCESS;
}

/*************************************************************************/
/* Function Name      : PnacHwAddOrDelMacSess                            */
/*                                                                       */
/* Description        : This function is called to add or delete a mac   */
/*                      session in hardware.                             */
/*                                                                       */
/* Input(s)           : u2PortNum - Port Number                          */
/*                      pu1SuppAddr - Supplicant MAC address             */
/*                      u1SessStatus - Session Status- PNAC_MAC_SESS_DEL */
/*                                                                       */
/* Output(s)          : None.                                            */
/*                                                                       */
/* Return Value(s)    : FNP_SUCCESS or FNP_FAILURE.                      */
/*************************************************************************/
INT4
PnacHwAddOrDelMacSess (UINT2 u2Port, UINT1 *pu1SuppAddr, UINT1 u1SessStatus)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (pu1SuppAddr);
    UNUSED_PARAM (u1SessStatus);

    return FNP_SUCCESS;
}

#ifdef L2RED_WANTED
/*************************************************************************/
/* Function Name      : FsPnacRedHwUpdateDB                              */
/*                                                                       */
/* Description        : This function is used to synchornize the NPAPI   */
/*                      datastructures for a port during switchover      */
/*                      from Standby to active.                          */
/*                                                                       */
/* Input(s)           : u2Port - Port Number                             */
/*                      pu1SuppAddr - Supplicant MAC address (in 
 *                      port-based authentication case, this argument    */
/*                                    is not valid)                      */
/*                      u1AuthMode - Authentication mode (Values can be  */
/*                                   PNAC_PORT_AUTHMODE_PORTBASED or     */
/*                                   PNAC_PORT_AUTHMODE_MACBASED)        */
/*                      u1AuthStatus - Authorization Status (Values can 
 *                      be PNAC_PORTSTATUS_AUTHORIZED or                 */
/*                                     PNAC_PORTSTATUS_UNAUTHORIZED)     */
/*                      u1CtrlDir - Oper Controlled directions           */
/*                          ( PNAC_CNTRLD_DIR_BOTH or PNAC_CNTRLD_DIR_IN)*/
/*                                                                       */
/*                                                                       */
/* Output(s)          : None                                             */
/*                                                                       */
/* Return Value(s)    : FNP_SUCCESS - On success                         */
/*                      FNP_FAILURE - On failure                         */
/*************************************************************************/
INT4
FsPnacRedHwUpdateDB (UINT2 u2Port, UINT1 *pu1SuppAddr,
                     UINT1 u1AuthMode, UINT1 u1AuthStatus, UINT1 u1CtrlDir)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (pu1SuppAddr);
    UNUSED_PARAM (u1AuthMode);
    UNUSED_PARAM (u1AuthStatus);
    UNUSED_PARAM (u1CtrlDir);
    return FNP_SUCCESS;
}
#endif
