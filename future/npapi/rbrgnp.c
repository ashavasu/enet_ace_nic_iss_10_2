/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rbrgnp.c,v 1.1 2012/01/27 13:11:08 siva Exp $
 *
 * *******************************************************************/

#include "lr.h"
#include "cfa.h"
#include "npapi.h"
#include "rbrgnp.h"
#include "rbridge.h"

/****************************************************************************
*
*    FUNCTION NAME    : RbrgNpProgramEntry
*
*    DESCRIPTION      : This function is invoked by R-Bridge module to 
*                       configure the control plane entries on hardware 
*
*    INPUT            : pRBrgEncapInfo - pointer to tRBrgEntry
*
*    OUTPUT           : Configures the control plane entries on hardware
*
*    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
*
****************************************************************************/

PUBLIC INT4 RbrgNpProgramEntry(tRBrgNpEntry *pRBrgEncapInfo)
{
   UNUSED_PARAM (pRBrgEncapInfo);
   return FNP_SUCCESS;
} 
