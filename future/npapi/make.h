#
# $Id
# Description: Auxillary file for NP makefile.
#

# Top level make include files
# ----------------------------

include ../LR/make.h
include ../LR/make.rule

# Compilation switches
# --------------------

NPAPI_COMPILATION_SWITCHES =

NPAPI_FINAL_COMPILATION_SWITCHES = ${NPAPI_COMPILATION_SWITCHES}   \
                                   ${GENERAL_COMPILATION_SWITCHES} \
                                   ${SYSTEM_COMPILATION_SWITCHES}

# Library archive utility
# -----------------------

AR            = ar
ARFLAGS       = rsv


# Directories
# -----------

NPAPI_BASE_DIR    = ${BASE_DIR}/npapi
NPAPI_INC_DIR     = ${NPAPI_BASE_DIR}
NPAPI_SRC_DIR     = ${NPAPI_BASE_DIR}
NPAPI_OBJ_DIR     = ${NPAPI_BASE_DIR}
NPAPI_LIB_DIR     = ${NPAPI_BASE_DIR}
RMON_INC_DIR      = ${BASE_DIR}/rmon/inc


# Include files
# -------------

NPAPI_INCLUDE_FILES =
                
NPAPI_FINAL_INCLUDE_FILES = ${NPAPI_INCLUDE_FILES}


# Include directories
# -------------------

NPAPI_INCLUDE_DIRS = -I${NPAPI_INC_DIR} -I$(BASE_DIR)/inc -I$(RMON_INC_DIR)

NPAPI_FINAL_INCLUDE_DIRS = ${NPAPI_INCLUDE_DIRS} \
                           ${COMMON_INCLUDE_DIRS}

# Project dependencies
# --------------------

NPAPI_DEPENDENCIES = $(COMMON_DEPENDENCIES)       \
                     $(NPAPI_FINAL_INCLUDE_FILES) \
                     $(NPAPI_BASE_DIR)/Makefile   \
                     $(NPAPI_BASE_DIR)/make.h

# -----------------------------  END OF FILE  -------------------------------
