/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmoffnp.c,v 1.14 2014/11/12 11:27:56 siva Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/
#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "ecfm.h"
#include "npapi.h"
#include "npcfmoff.h"

/*****************************************************************************
 * FsEcfmHwStartCcmTx
 *
 * DESCRIPTION:  This NPAPI updates the offloaded module to transmit CCM PDU 
 *               with the interval provided as one of the argument. It also 
 *               provides interface number on which this CCM PDU is to be 
 *               transmitted.
 *
 * INPUTS:       u4IfIndex - - Interface Number on which the PDU should be 
 *                             transmitted.
 *               pCCM_PDU - Formatted CCM PDU to be transmitted by the 
 *                          hardware
 *               u2Length - Length of the CCM PDU
 *               u4TxInterval - Transmission interval
 *               u2TxFilterId - Non Zero value if transmission needs update.
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
INT4
FsEcfmHwStartCcmTx (UINT4 u4IfIndex,
                    UINT1 *pCCM_PDU,
                    UINT2 u2Length, UINT4 u4TxInterval, UINT2 u2TxFilterId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pCCM_PDU);
    UNUSED_PARAM (u2Length);
    UNUSED_PARAM (u4TxInterval);
    UNUSED_PARAM (u2TxFilterId);

    return FNP_SUCCESS;
}

/*****************************************************************************
 * FsEcfmHwStartCcmRx
 *
 * DESCRIPTION:  This NPAPI updates the offloaded module to receive CCM PDU 
 *               with the interval provided as one of the argument. It also 
 *               provides interface number on which CCM PDU is to be received.
 *
 * INPUTS:       u4IfIndex - Interface Number on which the PDU should be 
 *                           transmitted.
 *               u2MEPID - MEP ID for which this receives call is raised
 *               pEcfmCcOffRxInfo - Information to receive and consume CCM PDU
 *               u2RxFilterId - Non Zero value if reception needs update.
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
INT4
FsEcfmHwStartCcmRx (UINT4 u4IfIndex,
                    UINT2 u2MEPID,
                    tEcfmCcOffRxInfo * pEcfmCcOffRxInfo, UINT2 u2RxFilterId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2MEPID);
    UNUSED_PARAM (pEcfmCcOffRxInfo);
    UNUSED_PARAM (u2RxFilterId);

    return FNP_SUCCESS;
}

/****************************************************************************
 * Function Name      : FsMiEcfmHwSetAisStatus
 *
 * Description        : This rotuine is used to intimate HW about AIS
 *                      condition on MPLS-TP based MEPs. 
 * 
 * Input(s)           : pAisOffParams - Pointer to AIS information.
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
INT4
FsMiEcfmHwSetAisStatus (tEcfmMplstpAisOffParams * pAisOffParams)
{
    UNUSED_PARAM (pAisOffParams);
    return FNP_SUCCESS;
}

/*****************************************************************************
 * FsEcfmHwStartCcmTxOnPortList
 *
 * DESCRIPTION:  This NPAPI updates the offloaded module to transmit CCM PDU 
 *               with the interval provided as one of the argument. It 
 *               provides list of interface number on which this CCM PDU is 
 *               to be transmitted.
 *
 * INPUTS:       PortList - List of Interface Number on which the PDU should be
 *                          transmitted
 *               UntagPortList - List of Interface Number on which the PDU 
*                           should be transmitted untagged
 *               pCCM_PDU - Formatted CCM PDU to be transmitted by the 
 *                          hardware
 *               u2Length - Length of the CCM PDU
 *               u4TxInterval - Transmission interval
 *               u4ContextId - Virtual Switch ID
 *               u2TxFilterId - Non Zero value if transmission needs update.
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
INT4
FsEcfmHwStartCcmTxOnPortList (tHwPortArray PortList,
                              tHwPortArray UntagPortList,
                              UINT1 *pCCM_PDU,
                              UINT2 u2Length,
                              UINT4 u4TxInterval,
                              UINT4 u4ContextId, UINT2 u2TxFilterId)
{

    UNUSED_PARAM (PortList);
    UNUSED_PARAM (UntagPortList);
    UNUSED_PARAM (pCCM_PDU);
    UNUSED_PARAM (u2Length);
    UNUSED_PARAM (u4TxInterval);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2TxFilterId);

    return FNP_SUCCESS;
}

/*****************************************************************************
 * FsEcfmHwStartCcmRxOnPortList
 *
 * DESCRIPTION:  This NPAPI updates the offloaded module to transmit CCM PDU 
 *               with the interval provided as one of the argument. It 
 *               provides list of interface number on which this CCM PDU is 
 *               to be transmitted.
 *
 * INPUTS:       PortList - List of Interface Number on which the PDU should be
 *                          received
 *               UntagPortList - List of Interface Number on which the PDU 
*                           should be received untagged
 *               u2MEPID - MEP ID for which this receives call is raised
 *               pEcfmCcOffRxInfo - Information to receive and consume CCM PDU
 *               u4ContextId - Virtual switch Id
 *               u2RxFilterId - Non Zero value if reception needs update.
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
INT4
FsEcfmHwStartCcmRxOnPortList (tHwPortArray PortList,
                              tHwPortArray UntagPortList,
                              UINT2 u2MEPID,
                              tEcfmCcOffRxInfo * pEcfmCcOffRxInfo,
                              UINT4 u4ContextId, UINT2 u2RxFilterId)
{

    UNUSED_PARAM (PortList);
    UNUSED_PARAM (UntagPortList);
    UNUSED_PARAM (u2MEPID);
    UNUSED_PARAM (pEcfmCcOffRxInfo);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2RxFilterId);

    return FNP_SUCCESS;
}

/*****************************************************************************
 * FsEcfmHwStopCcmTx
 *
 * DESCRIPTION:  This NPAPI removes Transmission entry from the offloaded 
 *               module for the filter id passed as argument
 *
 * INPUTS:       u4ContextId - Virtual switch Id
 *               u2TxFilterId - Handle to stop Transmission in offloaded 
 *                              module
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
VOID
FsEcfmHwStopCcmTx (UINT4 u4ContextId, UINT2 u2TxFilterId)
{

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2TxFilterId);

    return;
}

/*****************************************************************************
 * FsEcfmHwStopCcmRx
 *
 * DESCRIPTION:  This NPAPI removes reception entry from the offloaded 
 *               module for the filter id passed as argument
 *
 * INPUTS:       u4ContextId - Virtual switch Id
 *               u2RxFilterId - Handle to stop reception in offloaded 
 *                              module
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
VOID
FsEcfmHwStopCcmRx (UINT4 u4ContextId, UINT2 u2RxFilterId)
{

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2RxFilterId);

    return;
}

/*****************************************************************************
 * FsEcfmHwGetCcmTxStatistics
 *
 * DESCRIPTION:  This routine gets CCM PDU transmission statistics from the 
 *               offloaded module
 *
 * INPUTS:       u4ContextId - Virtual switch Id
 *               u2TxFilterId - Handle to get Transmission statistices from 
 *                              offloaded module
 *               pEcfmCcOffMepTxStats - CCM transmission information in 
 *                                      offloaded module
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
INT4
FsEcfmHwGetCcmTxStatistics (UINT4 u4ContextId,
                            UINT2 u2TxFilterId,
                            tEcfmCcOffMepTxStats * pEcfmCcOffMepTxStats)
{

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2TxFilterId);
    UNUSED_PARAM (pEcfmCcOffMepTxStats);

    return FNP_SUCCESS;
}

/*****************************************************************************
 * FsEcfmHwGetCcmRxStatistics
 *
 * DESCRIPTION:  This routine gets CCM PDU reception statistics from the 
 *               offloaded module
 *
 * INPUTS:       u4ContextId - Virtual switch Id
 *               u2RxFilterId - Handle to get Reception statistices from 
 *                              offloaded module
 *               pEcfmCcOffMepTxStats - CCM transmission information in 
 *                                      offloaded module
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
INT4
FsEcfmHwGetCcmRxStatistics (UINT4 u4ContextId,
                            UINT2 u2RxFilterId,
                            tEcfmCcOffMepRxStats * pEcfmCcOffMepRxStats)
{

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2RxFilterId);
    UNUSED_PARAM (pEcfmCcOffMepRxStats);

    return FNP_SUCCESS;
}

/*****************************************************************************
 * FsEcfmHwRegister
 *
 * DESCRIPTION:  Registers functions to handle Transmission failure and CCM 
 *               reception timeout callback
 *
 * INPUTS:       u4ContextId - Virtual switch Id
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
INT4
FsEcfmHwRegister (UINT4 u4ContextId)
{

    UNUSED_PARAM (u4ContextId);
    return FNP_SUCCESS;
}

/*****************************************************************************
 * FsEcfmHwGetPortCcStats
 *
 * DESCRIPTION:  Registers functions to handle Transmission failure and CCM 
 *               reception timeout callback
 *
 * INPUTS:       u4ContextId - Virtual switch Id
 *               u4IfIndex - - Interface Number on which the CCM Rx/Tx 
 *                             statistics are required. 
*                pEcfmCcOffPortStats- CCM transmission/reception information in 
 *                                      offloaded module for a port
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
INT4
FsEcfmHwGetPortCcStats (UINT4 u4ContextId, UINT4 u4IfIndex,
                        tEcfmCcOffPortStats * pEcfmCcOffPortStats)
{

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pEcfmCcOffPortStats);

    return FNP_SUCCESS;
}

/*Old NPAPIs end */

/*****************************************************************************
 * FsMiEcfmHwStartCcmTx
 *
 * DESCRIPTION:  This NPAPI updates the offloaded module to transmit CCM PDU 
 *               with the interval provided as one of the argument. It also 
 *               provides interface number on which this CCM PDU is to be 
 *               transmitted.
 *
 * INPUTS:       u4ContextId - Virtual switch Id
 *               u4IfIndex - - Interface Number on which the PDU should be 
 *                             transmitted.
 *               pCCM_PDU - Formatted CCM PDU to be transmitted by the 
 *                          hardware
 *               u2Length - Length of the CCM PDU
 *               u4TxInterval - Transmission interval
 *               u2TxFilterId - Non Zero value if transmission needs update.
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
INT4
FsMiEcfmHwStartCcmTx (UINT4 u4ContextId, UINT4 u4IfIndex,
                      UINT1 *pCCM_PDU,
                      UINT2 u2Length, UINT4 u4TxInterval, UINT2 u2TxFilterId)
{
    UNUSED_PARAM (u4ContextId);
    return FsEcfmHwStartCcmTx (u4IfIndex, pCCM_PDU,
                               u2Length, u4TxInterval, u2TxFilterId);
}

/*****************************************************************************
 * FsMiEcfmHwStartCcmRx
 *
 * DESCRIPTION:  This NPAPI updates the offloaded module to receive CCM PDU 
 *               with the interval provided as one of the argument. It also 
 *               provides interface number on which CCM PDU is to be received.
 *
 * INPUTS:       u4ContextId - Virtual switch Id
 *               u4IfIndex - Interface Number on which the PDU should be 
 *                           transmitted.
 *               u2MEPID - MEP ID for which this receives call is raised
 *               pEcfmCcOffRxInfo - Information to receive and consume CCM PDU
 *               u2RxFilterId - Non Zero value if reception needs update.
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
INT4
FsMiEcfmHwStartCcmRx (UINT4 u4ContextId, UINT4 u4IfIndex,
                      UINT2 u2MEPID,
                      tEcfmCcOffRxInfo * pEcfmCcOffRxInfo, UINT2 u2RxFilterId)
{
    UNUSED_PARAM (u4ContextId);
    return FsEcfmHwStartCcmRx (u4IfIndex, u2MEPID,
                               pEcfmCcOffRxInfo, u2RxFilterId);
}

/*****************************************************************************
 * FsMiEcfmHwStrtCcmTxOnPrtLst
 *
 * DESCRIPTION:  This NPAPI updates the offloaded module to transmit CCM PDU 
 *               with the interval provided as one of the argument. It 
 *               provides list of interface number on which this CCM PDU is 
 *               to be transmitted.
 *
 * INPUTS:       u4ContextId - Virtual Switch ID
 *               PortList - List of Interface Number on which the PDU should be
 *                          transmitted
 *               UntagPortList - List of Interface Number on which the PDU 
*                           should be transmitted untagged
 *               pCCM_PDU - Formatted CCM PDU to be transmitted by the 
 *                          hardware
 *               u2Length - Length of the CCM PDU
 *               u4TxInterval - Transmission interval
 *               u2TxFilterId - Non Zero value if transmission needs update.
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
INT4
FsMiEcfmHwStrtCcmTxOnPrtLst (UINT4 u4ContextId,
                             tHwPortArray PortList,
                             tHwPortArray UntagPortList,
                             UINT1 *pCCM_PDU,
                             UINT2 u2Length,
                             UINT4 u4TxInterval, UINT2 u2TxFilterId)
{

    return FsEcfmHwStartCcmTxOnPortList (PortList,
                                         UntagPortList, pCCM_PDU,
                                         u2Length, u4TxInterval,
                                         u4ContextId, u2TxFilterId);
}

/*****************************************************************************
 * FsMiEcfmHwStrtCcmRxOnPrtLst
 *
 * DESCRIPTION:  This NPAPI updates the offloaded module to transmit CCM PDU 
 *               with the interval provided as one of the argument. It 
 *               provides list of interface number on which this CCM PDU is 
 *               to be transmitted.
 *
 * INPUTS:       u4ContextId - Virtual switch Id
 *               PortList - List of Interface Number on which the PDU should be
 *                          received
 *               UntagPortList - List of Interface Number on which the PDU 
*                           should be received untagged
 *               u2MEPID - MEP ID for which this receives call is raised
 *               pEcfmCcOffRxInfo - Information to receive and consume CCM PDU
 *               u2RxFilterId - Non Zero value if reception needs update.
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
INT4
FsMiEcfmHwStrtCcmRxOnPrtLst (UINT4 u4ContextId,
                             tHwPortArray PortList,
                             tHwPortArray UntagPortList,
                             UINT2 u2MEPID,
                             tEcfmCcOffRxInfo * pEcfmCcOffRxInfo,
                             UINT2 u2RxFilterId)
{

    return FsEcfmHwStartCcmRxOnPortList (PortList,
                                         UntagPortList, u2MEPID,
                                         pEcfmCcOffRxInfo, u4ContextId,
                                         u2RxFilterId);
}

/*****************************************************************************
 * FsMiEcfmHwStopCcmTx
 *
 * DESCRIPTION:  This NPAPI removes Transmission entry from the offloaded 
 *               module for the filter id passed as argument
 *
 * INPUTS:       u4ContextId - Virtual switch Id
 *               u2TxFilterId - Handle to stop Transmission in offloaded 
 *                              module
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
VOID
FsMiEcfmHwStopCcmTx (UINT4 u4ContextId, UINT2 u2TxFilterId)
{

    FsEcfmHwStopCcmTx (u4ContextId, u2TxFilterId);
    return;
}

/*****************************************************************************
 * FsMiEcfmHwStopCcmRx
 *
 * DESCRIPTION:  This NPAPI removes reception entry from the offloaded 
 *               module for the filter id passed as argument
 *
 * INPUTS:       u4ContextId - Virtual switch Id
 *               u2RxFilterId - Handle to stop reception in offloaded 
 *                              module
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
VOID
FsMiEcfmHwStopCcmRx (UINT4 u4ContextId, UINT2 u2RxFilterId)
{

    FsEcfmHwStopCcmRx (u4ContextId, u2RxFilterId);

    return;
}

/*****************************************************************************
 * FsMiEcfmHwGetCcmTxStatistics
 *
 * DESCRIPTION:  This routine gets CCM PDU transmission statistics from the 
 *               offloaded module
 *
 * INPUTS:       u4ContextId - Virtual switch Id
 *               u2TxFilterId - Handle to get Transmission statistices from 
 *                              offloaded module
 *               pEcfmCcOffMepTxStats - CCM transmission information in 
 *                                      offloaded module
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
INT4
FsMiEcfmHwGetCcmTxStatistics (UINT4 u4ContextId,
                              UINT2 u2TxFilterId,
                              tEcfmCcOffMepTxStats * pEcfmCcOffMepTxStats)
{

    return FsEcfmHwGetCcmTxStatistics (u4ContextId,
                                       u2TxFilterId, pEcfmCcOffMepTxStats);
}

/*****************************************************************************
 * FsMiEcfmHwGetCcmRxStatistics
 *
 * DESCRIPTION:  This routine gets CCM PDU reception statistics from the 
 *               offloaded module
 *
 * INPUTS:       u4ContextId - Virtual switch Id
 *               u2RxFilterId - Handle to get Reception statistices from 
 *                              offloaded module
 *               pEcfmCcOffMepTxStats - CCM transmission information in 
 *                                      offloaded module
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
INT4
FsMiEcfmHwGetCcmRxStatistics (UINT4 u4ContextId,
                              UINT2 u2RxFilterId,
                              tEcfmCcOffMepRxStats * pEcfmCcOffMepRxStats)
{

    return FsEcfmHwGetCcmRxStatistics (u4ContextId,
                                       u2RxFilterId, pEcfmCcOffMepRxStats);

}

/*****************************************************************************
 * FsMiEcfmHwRegister
 *
 * DESCRIPTION:  Registers functions to handle Transmission failure and CCM 
 *               reception timeout callback
 *
 * INPUTS:       u4ContextId - Virtual switch Id
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
INT4
FsMiEcfmHwRegister (UINT4 u4ContextId)
{

    return FsEcfmHwRegister (u4ContextId);
}

/*****************************************************************************
 * FsMiEcfmHwGetPortCcStats
 *
 * DESCRIPTION:  Registers functions to handle Transmission failure and CCM 
 *               reception timeout callback
 *
 * INPUTS:       u4ContextId - Virtual switch Id
 *               u4IfIndex - - Interface Number on which the CCM Rx/Tx 
 *                             statistics are required. 
*                pEcfmCcOffPortStats- CCM transmission/reception information in 
 *                                      offloaded module for a port
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
INT4
FsMiEcfmHwGetPortCcStats (UINT4 u4ContextId, UINT4 u4IfIndex,
                          tEcfmCcOffPortStats * pEcfmCcOffPortStats)
{

    return FsEcfmHwGetPortCcStats (u4ContextId, u4IfIndex, pEcfmCcOffPortStats);
}

/*****************************************************************************
* FsMiEcfmHwSetVlanEtherType
*
* DESCRIPTION:  This NPAPI sets the Egress Ether Type for the Offloaded Module
*
* INPUTS:       u4ContextId - Virtual switch Id
*               u4IfIndex - - Interface Number on which the CCM Rx/Tx 
*                             statistics are required. 
*               u2EtherTypeValue - New VLAN Ether Type Value to be set
*                u1EtherType - Egress or Ingress Ether Type 
*
* OUTPUTS:      None
*
* Return Value(s)    : FNP_SUCCESS - On successful set (or)
*                      FNP_FAILURE - Error during setting
 *****************************************************************************/

INT4
FsMiEcfmHwSetVlanEtherType (UINT4 u4ContextId, UINT4 u4IfIndex,
                            UINT2 u2EtherTypeValue, UINT1 u1EtherType)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2EtherTypeValue);
    UNUSED_PARAM (u1EtherType);
    return FNP_SUCCESS;
}

/*****************************************************************************
 * FsMiEcfmHwHandleIntQFailure
 *
 * DESCRIPTION:  This routine get the List RxHandles for which RMEP defects  
 *               is present on the offloaded Meps
 *
 * INPUTS:       u4ContextId - Virtual Context Id
 *               pEcfmCcOffRxHandleInfo->u2StartRxHandle - To get list of 
 *               RxHandles start after this RxHandle value. 
 *               pEcfmCcOffRxHandleInfo->u2Length - Specifies number of 
 *               RxHandles to get 
 *
 * OUTPUTS:      pu2RxHandle - Contains list of
 *               RxHandles and info for offloaded MEPs
 *               pb1More - States whether more RxHandles with RMEP
 *               error present or not
 *
 * Return Value(s)    : FNP_SUCCESS - On successful get (or)
 *                      FNP_FAILURE - Error during getting
 *****************************************************************************/

INT4
FsMiEcfmHwHandleIntQFailure (UINT4 u4ContextId,
                             tEcfmCcOffRxHandleInfo * pEcfmCcOffRxHandleInfo,
                             UINT2 *pu2RxHandle, BOOL1 * pb1More)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pEcfmCcOffRxHandleInfo);
    UNUSED_PARAM (pu2RxHandle);
    UNUSED_PARAM (pb1More);
    return FNP_SUCCESS;
}

/****************************************************************************
 * Function Name      : FsMiEcfmHwCallNpApi
 *
 * Description        : This generic api will call NPAPIs depending on the Type
 *                      passed as an argument. 
 * 
 * Input(s)           : u1Type - Type of Call to NPAPI
 *                      pEcfmHwInfo - Structure pointer to the 
 *                                    structure tEcfmHwParams
 *  
 * Output(s)          : None
 * 
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE
 *****************************************************************************/
PUBLIC INT4
FsMiEcfmHwCallNpApi (UINT1 u1Type, tEcfmHwParams * pEcfmHwInfo)
{
    switch (u1Type)
    {
        case ECFM_NP_MEP_CREATE:
            pEcfmHwInfo->au1HwHandler[0] = 1;
            pEcfmHwInfo->au1HwHandler[1] = 1;
            pEcfmHwInfo->au1HwHandler[2] = 1;
            break;
        case ECFM_NP_MEP_DELETE:
            pEcfmHwInfo->au1HwHandler[0] = 0;
            pEcfmHwInfo->au1HwHandler[1] = 0;
            pEcfmHwInfo->au1HwHandler[2] = 0;
            break;
        default:
            break;
    }
    return FNP_SUCCESS;
}
