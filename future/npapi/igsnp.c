/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igsnp.c,v 1.15 2010/05/13 11:53:39 prabuc Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "snp.h"
#include "npapi.h"
#include "npigs.h"

/*****************************************************************************/
/* Function Name      : IgsHwEnable                                          */
/*                                                                           */
/* Description        : This function enables IGMP snooping feature in the   */
/*                      hardware. This creates and installs filter needed    */
/*                      for snooping feature                                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsIgsHwEnableIgmpSnooping (VOID)
{

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgsHwDisable                                         */
/*                                                                           */
/* Description        : This function disables IGMP snooping feature in the  */
/*                      hardware. This removes & destroys filter needed      */
/*                      for snooping feature                                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsIgsHwDisableIgmpSnooping (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsIgsHwSparseMode                                    */
/*                                                                           */
/* Description        : This function enable/disable IGMP Sparse mode in the */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : u1SparseStatus - Sparse Mode Status                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsIgsHwSparseMode (UINT1 u1SparseStatus)
{
    UNUSED_PARAM (u1SparseStatus);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsIgsHwEnhancedMode                                  */
/*                                                                           */
/* Description        : This function enables IGMP Enhanced mode in the      */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : u1EnhStatus - Enhanced Mode Status                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsIgsHwEnhancedMode (UINT1 u1EnhStatus)
{
    UNUSED_PARAM (u1EnhStatus);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsIgsHwPortRateLimit                                 */
/*                                                                           */
/* Description        : This function configures IGMP Rate Limit for a       */
/*                      downstream interface in hardware. In default mode,   */
/*                      the downstream interface corresponds to only port and*/
/*                      inner-vlan will be 0. When enhanced mode is          */
/*                      enabled, downstream interface corresponds to port and*/
/*                      inner-vlan.In this mode inner-vlan may be within the */
/*                      valid vlan range or 0.When rate limit is             */
/*                      configured for an interface, this API will be invoked*/
/*                      with action as .ADD. and u4Rate as the required rate.*/
/*                      When rate limit is configured as zero (rate limit all*/
/*                      packets on this interface) this API will be invoked  */
/*                      with action as .ADD. and u4Rate as zero. When rate   */
/*                      limit is configured to default value ( no rate       */
/*                      limiting to be applied on this interface) this API   */
/*                      will be invoked with action as .REMOVE. and the      */
/*                      u4Rate can be ignored.                               */
/*                                                                           */
/* Input(s)           : Struct tIgsHwRateLmt - It contains                   */
/*                      UINT4 u4Port            :  Port Number               */
/*                      tVlanId InnerVlanId     :  Inner-Vlan                */
/*                      UINT4 u4Rate            :  No of packets per second  */
/*                                                                           */
/*                    : UINT1 u1Action - Type of action to be performed.     */
/*                                        ( Add/Remove Rate-limit for port ) */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsIgsHwPortRateLimit (tIgsHwRateLmt * pIgsHwRateLmt, UINT1 u1Action)
{
    UNUSED_PARAM (pIgsHwRateLmt);
    UNUSED_PARAM (u1Action);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsNpUpdateIpmcFwdEntries                             */
/*                                                                           */
/* Description        : This function Updates IP Multicast forwarding entries*/
/*                      in the hardwarei based on the EventType.             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsNpUpdateIpmcFwdEntries (tSnoopVlanId VlanId, UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                          tPortList PortList, tPortList UntagPortList,
                          UINT1 u1EventType)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4GrpAddr);
    UNUSED_PARAM (u4SrcAddr);
    UNUSED_PARAM (PortList);
    UNUSED_PARAM (UntagPortList);
    UNUSED_PARAM (u1EventType);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsNpGetFwdEntryHitBitStatus                          */
/*                                                                           */
/* Description        : This function gets how many times this forwarding    */
/*                      entry is used for sending data traffic               */
/*                                                                           */
/* Input(s)           : VlanId - vlan id                                     */
/*                      u4GrpAddr - Group Address                            */
/*                      u4SrcAddr - Source Address                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsNpGetFwdEntryHitBitStatus (tSnoopVlanId VlanId, UINT4 u4GrpAddr,
                             UINT4 u4SrcAddr)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4GrpAddr);
    UNUSED_PARAM (u4SrcAddr);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsIgsHwAddRtrPort                                      */
/*                                                                           */
/* Description        : This function adds router Port to router Portlsit    */
/*                                                                           */
/* Input(s)           : VlanId - Vlan ID to which the MAC address should be  */
/*                               associated. This will be zero if no VLAN    */
/*                      u2Port - Port number to be added                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsIgsHwAddRtrPort (tSnoopVlanId VlanId, UINT2 u2Port)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2Port);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsIgsHwDelRtrPort                                      */
/*                                                                           */
/* Description        : This function deletes Port from router Portlsit      */
/*                                                                           */
/* Input(s)           : VlanId - Vlan ID to which the MAC address should be  */
/*                               associated. This will be zero if no VLAN    */
/*                      u2Port - Port number to be deleted                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsIgsHwDelRtrPort (tSnoopVlanId VlanId, UINT2 u2Port)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2Port);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsIgsHwEnableIpIgmpSnooping                          */
/*                                                                           */
/* Description        : This function is called when IP  IGMP is enabled     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsIgsHwEnableIpIgmpSnooping (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsIgsHwDisableIpIgmpSnooping                         */
/*                                                                           */
/* Description        : This function is called when IP IGMP is disabled.    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsIgsHwDisableIpIgmpSnooping (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsIgsHwUpdateIpmcEntry                               */
/*                                                                           */
/* Description        : This function Updates IP Multicast forwarding entries*/
/*                      in the hardware for a given instance based on the    */
/*                      EventType.                                           */
/*                                                                           */
/* Input(s)           : pIgsHwIpFwdInfo - IP Fwd information                 */
/*                      u1Action        - SNOOP_HW_CREATE_ENTRY /            */
/*                                        SNOOP_HW_DELETE_ENTRY /            */
/*                                        SNOOP_HW_ADD_PORT /                */
/*                                        SNOOP_HW_DEL_PORT /                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsIgsHwUpdateIpmcEntry (tIgsHwIpFwdInfo * pIgsHwIpFwdInfo, UINT1 u1Action)
{
    UNUSED_PARAM (pIgsHwIpFwdInfo);
    UNUSED_PARAM (u1Action);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsIgsHwGetIpFwdEntryHitBitStatus                     */
/*                                                                           */
/* Description        : This function is used to check whether the forwarding*/
/*                      entry for a given instance is used for sending data  */
/*                      traffic                                              */
/*                                                                           */
/* Input(s)           : pIgsHwIpFwdInfo - IP Fwd information                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsIgsHwGetIpFwdEntryHitBitStatus (tIgsHwIpFwdInfo * pIgsHwIpFwdInfo)
{
    UNUSED_PARAM (pIgsHwIpFwdInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FsIgsHwClearIpmcFwdEntries                       */
/*                                                                           */
/*    Description         : This function deletes all the multicast          */
/*                          entries for a context form the hardware. (IPMC   */
/*                          table).                                          */
/*                                                                           */
/*    Input(s)            : u4Instance - Insatnce Id                         */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*****************************************************************************/
VOID
FsIgsHwClearIpmcFwdEntries (VOID)
{
    return;
}
