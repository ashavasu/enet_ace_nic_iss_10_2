/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pbtnp.c,v 1.2 2008/11/14 08:28:55 premap-iss Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/
#include "lr.h"
#include "fsvlan.h"
#include "npapi.h"
#include "pbtnp.h"

/*****************************************************************************/
/* Function Name      : FsMiPbbTeHwSetEspVid                                 */
/*                                                                           */
/* Description        : This function is called to configure a Vlan as       */
/*                      ESP-VLAN in PBB-TE.                                  */
/*                      This function should disable flooding of unknown     */
/*                      unicast & multicast packets in the particular VLAN.  */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      tVlanId     - Esp VLAN Identifier                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiPbbTeHwSetEspVid (UINT4 u4ContextId, tVlanId EspVlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (EspVlanId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsMiPbbTeHwResetEspVid                               */
/*                                                                           */
/* Description        : This function is called to reset a Vlan as           */
/*                      ESP-VLAN in PBB-TE.                                  */
/*                      This function should enable flooding of unknown      */
/*                      unicast & multicast packets in the particular VLAN.  */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      tVlanId     - Esp VLAN Identifier                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On Success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiPbbTeHwResetEspVid (UINT4 u4ContextId, tVlanId EspVlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (EspVlanId);
    return FNP_SUCCESS;
}
