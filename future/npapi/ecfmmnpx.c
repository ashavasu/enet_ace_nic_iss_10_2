/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ecfmmnpx.c,v 1.5 2012/03/05 13:17:03 siva Exp $
 *
 * Description: All network processor function  given here
 *
 *******************************************************************/
#include "lr.h"
#include "cfa.h"
#include "rmgr.h"
#include "ecfm.h"
#include "npapi.h"
#include "ecfmminp.h"

/*****************************************************************************
 * FsMiEcfmMbsmNpInitHw
 *
 * DESCRIPTION:  This function takes care of initialising the
 *               hardware related parameters for a particular slot.
 *
 * INPUTS:       u4ContextId - Virtual Switch ID
 *               pSlotInfo - Slot information
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/

INT4
FsMiEcfmMbsmNpInitHw (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************
 * FsMiEcfmMbsmHwStartCcmTx
 *
 * DESCRIPTION:  This NPAPI updates the offloaded module to transmit CCM PDU 
 *               with the interval provided as one of the argument. It also 
 *               provides interface number on which this CCM PDU is to be 
 *               transmitted.
 *
 * INPUTS:       u4ContextId  - Virtual Switch ID
 *               u4IfIndex - - Interface Number on which the PDU should be 
 *                             transmitted.
 *               pCCM_PDU - Formatted CCM PDU to be transmitted by the 
 *                          hardware
 *               u2Length - Length of the CCM PDU
 *               u4TxInterval - Transmission interval
 *               u2TxFilterId - Non Zero value if transmission needs update.
 *               pSlotInfo - Slot information
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
INT4
FsMiEcfmMbsmHwStartCcmTx (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 *pCCM_PDU,
                          UINT2 u2Length, UINT4 u4TxInterval,
                          UINT2 u2TxFilterId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pCCM_PDU);
    UNUSED_PARAM (u2Length);
    UNUSED_PARAM (u4TxInterval);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2TxFilterId);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************
 * FsMiEcfmMbsmHwStartCcmRx
 *
 * DESCRIPTION:  This NPAPI updates the offloaded module to receive CCM PDU 
 *               with the interval provided as one of the argument. It also 
 *               provides interface number on which CCM PDU is to be received.
 *
 * INPUTS:       u4ContextId - Virtual Switch ID
 *               u4IfIndex - Interface Number on which the PDU should be 
 *                           transmitted.
 *               u2MEPID - MEP ID for which this receives call is raised
 *               pEcfmCcOffRxInfo - Information to receive and consume CCM PDU
 *               u2RxFilterId - Non Zero value if reception needs update.
 *               pSlotInfo - Slot information
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
INT4
FsMiEcfmMbsmHwStartCcmRx (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2MEPID,
                          tEcfmCcOffRxInfo * pEcfmCcOffRxInfo,
                          UINT2 u2RxFilterId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2MEPID);
    UNUSED_PARAM (pEcfmCcOffRxInfo);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2RxFilterId);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************
 * FsMiEcfmMbsmHwStrtCcmTxOnPrtLst
 *
 * DESCRIPTION:  This NPAPI updates the offloaded module to transmit CCM PDU 
 *               with the interval provided as one of the argument. It 
 *               provides list of interface number on which this CCM PDU is 
 *               to be transmitted.
 *
 * INPUTS:       u4ContextId - Virtual Switch ID
 *               PortList - List of Interface Number on which the PDU should be
 *                          transmitted
 *               UntagPortList - List of Interface Number on which the PDU 
*                           should be transmitted untagged
 *               pCCM_PDU - Formatted CCM PDU to be transmitted by the 
 *                          hardware
 *               u2Length - Length of the CCM PDU
 *               u4TxInterval - Transmission interval
 *               u2TxFilterId - Non Zero value if transmission needs update.
 *               pSlotInfo - Slot information
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
INT4
FsMiEcfmMbsmHwStrtCcmTxOnPrtLst (UINT4 u4ContextId,
                                 tHwPortArray PortList,
                                 tHwPortArray UntagPortList, UINT1 *pCCM_PDU,
                                 UINT2 u2Length, UINT4 u4TxInterval,
                                 UINT2 u2TxFilterId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (PortList);
    UNUSED_PARAM (UntagPortList);
    UNUSED_PARAM (pCCM_PDU);
    UNUSED_PARAM (u2Length);
    UNUSED_PARAM (u4TxInterval);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2TxFilterId);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************
 * FsMiEcfmMbsmHwStrtCcmRxOnPrtLst
 *
 * DESCRIPTION:  This NPAPI updates the offloaded module to transmit CCM PDU 
 *               with the interval provided as one of the argument. It 
 *               provides list of interface number on which this CCM PDU is 
 *               to be transmitted.
 *
 * INPUTS:       u4ContextId,
 *               PortList - List of Interface Number on which the PDU should be
 *                          received
 *               UntagPortList - List of Interface Number on which the PDU 
*                           should be received untagged
 *               u2MEPID - MEP ID for which this receives call is raised
 *               pEcfmCcOffRxInfo - Information to receive and consume CCM PDU
 *               u2RxFilterId - Non Zero value if reception needs update.
 *               pSlotInfo - Slot information
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
INT4
FsMiEcfmMbsmHwStrtCcmRxOnPrtLst (UINT4 u4ContextId, tHwPortArray PortList,
                                 tHwPortArray UntagPortList, UINT2 u2MEPID,
                                 tEcfmCcOffRxInfo * pEcfmCcOffRxInfo,
                                 UINT2 u2RxFilterId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (PortList);
    UNUSED_PARAM (UntagPortList);
    UNUSED_PARAM (u2MEPID);
    UNUSED_PARAM (pEcfmCcOffRxInfo);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2RxFilterId);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************
 * FsEcfmMbsmHwStartCcmTx
 *
 * DESCRIPTION:  Stub provided for backward compatibility with MI-unaware
 *               hardware.
 *
 * INPUTS:       u4IfIndex
 *               u2MEPID
 *               pEcfmCcOffRxInfo
 *               u2RxFilterId
 *
 * OUTPUTS:      None
 *
 * Return Value(s)    : FNP_SUCCESS - On successful set (or)
 *                      FNP_FAILURE - Error during setting
 *****************************************************************************/
INT4
FsEcfmMbsmHwStartCcmRx (UINT4 u4IfIndex, UINT2 u2MEPID,
                        tEcfmCcOffRxInfo * pEcfmCcOffRxInfo,
                        UINT2 u2RxFilterId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2MEPID);
    UNUSED_PARAM (pEcfmCcOffRxInfo);
    UNUSED_PARAM (u2RxFilterId);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/****************************************************************************
 * Function Name      : FsMiEcfmMbsmHwCallNpApi
 *
 * Description        : This generic API calla H/W APIs depending on the Type
 *                      passed as an argument.
 *
 * Input(s)           : u1Type - Type of Call to NPAPI
 *                      pEcfmHwInfo - Structure pointer to the
 *                                    structure tEcfmHwParams
 *                      pSlotInfo - Structure pointer to the
 *                                  structure tMbsmSlotInfo
 *
 * Output(s)          : None
 *
 * Return Value(s)    : FNP_SUCCESS / FNP_FAILURE
 *****************************************************************************/
PUBLIC INT4
FsMiEcfmMbsmHwCallNpApi (UINT1 u1Type, tEcfmHwParams * pEcfmHwInfo,
                         tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u1Type);
    UNUSED_PARAM (pEcfmHwInfo);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}
