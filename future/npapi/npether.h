/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * Description: Exported file for etherMIB NP-API.
 *
 *******************************************************************/
#ifndef __NPETHER_H
#define __NPETHER_H

/* Add following header files before including this file
 * #include future/inc/ethernp.h
 * */
#include "ethernp.h"

#define ETH_STATS_DUP_ST_UNKNOWN            1
#define ETH_STATS_DUP_ST_HALFDUP            2
#define ETH_STATS_DUP_ST_FULLDUP            3

#endif /* __NPETHER_H */
