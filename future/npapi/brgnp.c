/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: brgnp.c,v 1.7 2007/02/01 14:59:31 iss Exp $
 *
 * Description: All network processor function  given here
 *
 *******************************************************************/

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "npapi.h"
#include "npbrg.h"

/*******************************************************************************
* FsBrgGetMaxInfo
*
* DESCRIPTION:
*  Get the maximum information the interface can support. 
*
* INPUTS:
* u4IfIndex- logical interface number
*  
* 
*
* OUTPUTS:
* pi4MaxVal - pointer to the stored value
*
* RETURNS:
* FNP_SUCCESS - on success
* FNP_FAILURE - on error
*
*
* COMMENTS:
*  None
*
*******************************************************************************/
INT1
FsBrgGetMaxInfo (UINT4 u4IfIndex, INT4 *pi4MaxVal)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pi4MaxVal);
    return FNP_SUCCESS;
}

/*******************************************************************************
* FsBrgSetAgingTime
*
* DESCRIPTION:
* Set the FDB aging time value. This will be called either during the STAP
* operation during reconfiguration to time out entries learned during this 
* period. Also can be used to set aging time other than default value 300secs 
* supported by the 802.1D/802.1Q standard. 
*
* INPUTS:
* i4Aging - Aging Time
* OUTPUTS:
* None.
*
* RETURNS:
* FNP_SUCCESS - on success
* FNP_FAILURE - on error
*
*
* COMMENTS:
* None 

*
*******************************************************************************/
INT1
FsBrgSetAgingTime (INT4 i4AgingTime)
{
    UNUSED_PARAM (i4AgingTime);
    return FNP_SUCCESS;
}

/*******************************************************************************
* FsBrgFindFdbMacEntry
*
* DESCRIPTION:
*  This will return the logical port for the MAC address in FDB Tbl. 
*  This index can be used by ARP module for referring the MAC address 
*  corresponding to the IP Address.
*
* INPUTS:
* macAddr - MAC address of the FDB entry
* OUTPUTS:
* pu4IfIndex - pointer to the o/p Interface Index for MAC address in FDB Tbl
* None.
*
* RETURNS:
* FNP_SUCCESS - on success
* FNP_FAILURE - on error
*
*
* COMMENTS:
*  None.

*
*******************************************************************************/

INT1
FsBrgFindFdbMacEntry (tMacAddr macAddr, UINT4 *pu4IfIndex)
{
    UNUSED_PARAM (macAddr);
    UNUSED_PARAM (pu4IfIndex);
    return FNP_SUCCESS;
}

/*******************************************************************************
* FsBrgGetFdbEntryFirst
*
* DESCRIPTION:
* This will return the first entry MAC Address of FDB Table. This will be 
* used for MIB view of the FDB table.  
* INPUTS:
* None
* OUTPUTS:
* macAddr - MAC Address of the first entry 
*
* RETURNS:
*
* FNP_SUCCESS - on success
* FNP_FAILURE - on error
*
*
* COMMENTS:
*  None

*
*******************************************************************************/
INT1
FsBrgGetFdbEntryFirst (tMacAddr macAddr)
{
    UNUSED_PARAM (macAddr);
    return FNP_SUCCESS;
}

/*******************************************************************************
* FsBrgGetFdbNextEntry
*
* DESCRIPTION:
* This will return the next entry to the index of FDB Table. 
* This will be used for MIB view of the FDB table. This is similar to the get 
* next index of the MIB.
*
* INPUTS:
* macAddr - current MAC Address
* OUTPUTS:
* nextMacAddr - Next MAC address
*
* RETURNS:
* FNP_SUCCESS - on success
* FNP_FAILURE - on error
*
*
* COMMENTS:
*  None

*
*******************************************************************************/
INT1
FsBrgGetFdbNextEntry (tMacAddr macAddr, tMacAddr nextMacAddr)
{
    UNUSED_PARAM (macAddr);
    UNUSED_PARAM (nextMacAddr);
    return FNP_SUCCESS;
}

/*******************************************************************************
* FsBrgFlushPort
*
* DESCRIPTION:
* This will delete all the FDB entries learned in this logical port.
 *
* INPUTS:
* u4IfIndex  - logical Interface Index
* OUTPUTS:
* None
*
* RETURNS:
* FNP_SUCCESS - on success
* FNP_FAILURE - on error
*
* COMMENTS:
*  None

*
*******************************************************************************/
INT1
FsBrgFlushPort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return FNP_SUCCESS;
}

/*******************************************************************************
* FsBrgAddMacEntry
*
* DESCRIPTION:
* This will add the FDB entry into FDB Table.
 *
* INPUTS:
* u4IfIndex - logical port 
* macAddr -  MAC Address
* OUTPUTS:
* None
*
* RETURNS:
* FNP_SUCCESS- success
* FNP_FAILURE - Error during addition
*
* COMMENTS:
*  None

*
*******************************************************************************/
INT1
FsBrgAddMacEntry (UINT4 u4IfIndex, tMacAddr macAddr)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (macAddr);
    return FNP_SUCCESS;
}

/*******************************************************************************
* FsBrgDelMacEntry
*
* DESCRIPTION:
* This will deletes the FDB MAC entry from the FDB Table.
*
* INPUTS:
* FDB_ENTRY- FDB entry referred with MAC Address and u4IfIndex
* OUTPUTS:
* None
* 
* RETURNS:
* FNP_SUCCESS- success
* FNP_FAILURE - Error during addition
*
* COMMENTS:
*  None

*
*******************************************************************************/
INT1
FsBrgDelMacEntry (UINT4 u4IfIndex, tMacAddr macAddr)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (macAddr);
    return FNP_SUCCESS;
}

/*******************************************************************************
* FsBrgSetPortIngStFilt
*
* DESCRIPTION:
*  Sets/clears ports to be included for forwarding on ports for this 
*  static addressed packet. This will add the ports to the list of ports 
*  to be included for forwarding. Also, multi-cast address can also be 
*  included as a static address.
*
* INPUTS:
*  u4IfIndex- logical Interface Index to be included in this port list.
*  macAddr - static MAC address 
*  au1Member - members of the port lists
* 
*
* OUTPUTS:
* FNP_SUCCESS- success
* FNP_FAILURE - Error during addition 
* RETURNS:
* None
*
*
* COMMENTS:
*  None
*
*******************************************************************************/
INT1
FsBrgSetPortIngStFilt (UINT4 u4IfIndex, tMacAddr macAddr,
                       UINT1 au1Member[BRG_PORT_LIST_SIZE])
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (macAddr);
    UNUSED_PARAM (au1Member);
    return FNP_SUCCESS;
}

/*******************************************************************************
 * FsBrgSetPortIngStFiltFlags
 *
 * DESCRIPTION:
 * Sets the status of the Static entry in the hardware Address table
 *
 * INPUTS:
 *  u4RcvIfIndex -  received  Interface Index
 *  staticAddress - static MAC address 
 *  i4StaticFlags - static entry status
 * 
 *
 * OUTPUTS:
 * FNP_SUCCESS- success
 * FNP_FAILURE - Error during setting 
 * RETURNS:
 * None
 *
 *
 * COMMENTS:
 *  None
 *
 *******************************************************************************/
INT1
FsBrgSetPortIngStFiltFlags (UINT4 u4RcvIfIndex, tMacAddr staticAddress,
                            INT4 i4StaticFlags)
{
    UNUSED_PARAM (u4RcvIfIndex);
    UNUSED_PARAM (staticAddress);
    UNUSED_PARAM (i4StaticFlags);
    return FNP_SUCCESS;
}

/*******************************************************************************
* FsBrgMcAddEntry
*
* DESCRIPTION:
* Create a multicast entry and add to the multi-cast group.
 *
* INPUTS:
* mcastMacAddr- Multicast address
* i4GrpId - Group ID
* OUTPUTS:
* None
*
* RETURNS:
* FNP_SUCCESS - success
*
* COMMENTS:
*  None

*
*******************************************************************************/
INT1
FsBrgMcAddEntry (INT4 i4GrpID, tMacAddr mcastMacAddr)
{
    UNUSED_PARAM (i4GrpID);
    UNUSED_PARAM (mcastMacAddr);
    return FNP_SUCCESS;
}

/*******************************************************************************
* FsBrgMcDeleteEntry
*
* DESCRIPTION:
* Delete a multicast entry from the multi-cast group.
 *
* INPUTS:
* mcastMacAddr- Multicast address
* i4GrpId - Group ID
* OUTPUTS:
* None
*
* RETURNS:
* FNP_SUCCESS- success
* FNP_FAILURE - Error during deletion
*
* COMMENTS:
*  None

*
*******************************************************************************/
INT1
FsBrgMcDeleteEntry (INT4 i4GrpID, tMacAddr mcastMacAddr)
{
    UNUSED_PARAM (i4GrpID);
    UNUSED_PARAM (mcastMacAddr);
    return FNP_SUCCESS;
}

/*******************************************************************************
* FsBrgSetMcGrp2Port
*
* DESCRIPTION:
* Set/Clears the multicast group ID to the logical port.
 *
* INPUTS:
* i4GrpId - Group ID
* u4IfIndex - logical Interface Index
* OUTPUTS:
* None
*
* RETURNS:
* FNP_SUCCESS- success
* FNP_FAILURE - Error during setting
*
* COMMENTS:
*  None

*
*******************************************************************************/
INT1
FsBrgSetMcGrp2Port (UINT4 u4IfIndex, INT4 i4GrpId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4GrpId);
    return FNP_SUCCESS;
}

/*******************************************************************************
* FsMcEnAddrInGrp 
*
* DESCRIPTION:
* Enable /disable the multicast entry for allowing or for blocking condition.
*
* INPUTS:
* i4GrpId - Group ID
* i4EnableMcast - Enable/disable
* mcastMacAddr - multicast address
* OUTPUTS:
* None
*
* RETURNS:
* FNP_SUCCESS- success
* FNP_FAILURE - Error during setting
*
* COMMENTS:
*  None

*
*******************************************************************************/
INT1
FsBrgMcEnAddrInGrp (INT4 i4GrpId, tMacAddr mcastMacAddr, INT4 i4EnableMcast)
{
    UNUSED_PARAM (i4GrpId);
    UNUSED_PARAM (mcastMacAddr);
    UNUSED_PARAM (i4EnableMcast);
    return FNP_SUCCESS;
}

/*******************************************************************************
* FsBrgGetLearnedEntryDisc 
*
* DESCRIPTION:
* Gets the total number of forwarding database entries which would 
* have been learnt, but have been discarded due to lack of space to store them 
* in the forwarding database.
* INPUTS:
* None
* OUTPUTS:
* pu4Val - stored value
*
* RETURNS:
* FNP_SUCCESS- success
* FNP_FAILURE - Error 
*
* COMMENTS:
*  None 

*
*******************************************************************************/
INT1
FsBrgGetLearnedEntryDisc (UINT4 *pu4Val)
{
    UNUSED_PARAM (pu4Val);
    return FNP_SUCCESS;
}
