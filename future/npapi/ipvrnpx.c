/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipvrnpx.c,v 1.3 2013/07/02 13:29:44 siva Exp $
 *
 * Description: This file contains the function implementations  of new IP/ARP
 * VRF NP MBSM calls           
 ****************************************************************************/
#include "lr.h"
#include "cfa.h"
#include "npip.h"

/******************************************************************************/
/*   Function Name             : FsNpMbsmIpv4vrfArpAdd                           */
/*   Description               : Adds the Specified IP address and MAC address*/
/*   Input(s)                  : u4CfaIfIndex - CFA Vlan If Index             */
/*   Output(s)                 : None                                         */
/*   Global Variables Referred : None                                         */
/*   Global variables Modified : None                                         */
/*   Exceptions                : None                                         */
/*   Use of Recursion          : None                                         */
/*   Returns                   : FNP_SUCCESS/FNP_FAILURE                      */
/******************************************************************************/
UINT4
FsNpMbsmIpv4VrfArpAdd (UINT4 u4VrId, tNpVlanId u2VlanId, UINT4 u4IpAddr,
                       UINT1 *pMacAddr, UINT1 *pbu1TblFull,
                       tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (pMacAddr);
    UNUSED_PARAM (pbu1TblFull);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/******************************************************************************/
/*   Function Name             : FsNpMbsmIpv4VrfArpAddition                   */
/*   Description               : Adds the Specified IP address and MAC address*/
/*                               to the H/W L3 IP Table and Returns Full if no*/
/*                               free entry exist in the L3 Ip Table          */
/*                               This function is an upgrade of MBSM call     */
/*                               FsNpMbsmIpv4VrfArpAdd with additional        */
/*                               arguments i1State/u4IfIndex/pu1IfName        */
/*   Input(s)                  : u4VrId    - The virtual router Identifier    */
/*                               u2VlanId  - L2 Vlan Id                       */
/*                               u4IfIndex - CFA L3 Vlan If Index             */
/*                               u4IpAddr  - IP address for which ARP is added*/
/*                               pMacAddr  - MAC Address                      */
/*                               pu1IfName - Interface name                   */
/*                               i1State   - State of the ARP entry           */
/*                               pSlotInfo - Slot Information                 */
/*   Output(s)                 : pbu1TblFull - Hw Table Full/not              */
/*                                            (FNP_FALSE/FNP_TRUE)            */
/*   Global Variables Referred : None                                         */
/*   Global variables Modified : None                                         */
/*   Exceptions                : None                                         */
/*   Use of Recursion          : None                                         */
/*   Returns                   : FNP_SUCCESS/FNP_FAILURE                      */
/******************************************************************************/
UINT4
FsNpMbsmIpv4VrfArpAddition (UINT4 u4VrId, tNpVlanId u2VlanId, UINT4 u4IpAddr,
                            UINT1 *pMacAddr, UINT1 *pbu1TblFull,
                            UINT4 u4IfIndex, UINT1 *pu1IfName,
                            INT1 i1State, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (pMacAddr);
    UNUSED_PARAM (pbu1TblFull);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1IfName);
    UNUSED_PARAM (i1State);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}
