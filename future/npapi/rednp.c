/*****************************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: rednp.c,v 1.1 2008/06/06 13:53:17 premap-iss Exp $
 *
 * Description: This file contains the function implementations  of NP
 *              redundancy.
 ****************************************************************************/

#ifndef _REDNP_C_
#define _REDNP_C_

#include "lr.h"
#include "rednp.h"
#include "npapi.h"

/*****************************************************************************/
/* Function Name      : NpRedTaskMain                                        */
/*                                                                           */
/* Description        : This function is the entry point function of the NP  */
/*                      RED Task.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
NpRedTaskMain (INT1 *pi1Param)
{
    UNUSED_PARAM (pi1Param);
    lrInitComplete (OSIX_SUCCESS);
    return;
}

/*****************************************************************************/
/* Function Name      : NpRedInit                                            */
/*                                                                           */
/* Description        : Registers NP with RM by providing an application     */
/*                      ID for NP and a call back function to be called      */
/*                      whenever RM needs to send an event to NP.            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS/FNP_FAILURE                              */
/*****************************************************************************/
INT4
NpRedInit (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : NpRedDeInit                                          */
/*                                                                           */
/* Description        : It dequeues from NPRED queue and deregisters         */
/*                      NP with RM and intialize the NP RED RM state.         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS/FNP_FAILURE                              */
/*****************************************************************************/
INT4
NpRedDeInit (VOID)
{
    return FNP_SUCCESS;
}

#endif /*_REDNP_C_*/
