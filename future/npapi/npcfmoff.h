/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: npcfmoff.h,v 1.1 2008/06/05 09:54:26 iss Exp $
 *
 * Description: Exported file for ECFM NP-API.
 *
 *******************************************************************/
 
#ifndef _NPCFMOFF_H
#define _NPCFMOFF_H

#include "cfmoffnp.h"

#endif /* _NPCFMOFF_H */
