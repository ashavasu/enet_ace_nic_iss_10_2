/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmonnp.c,v 1.5 2007/02/01 14:59:31 iss Exp $
 *
 * Description: This file contains the function implementations  of 
 *              RMON NP-API.
 ****************************************************************************/

#include "lr.h"
#include "cfa.h"
#include "rmon.h"
#include "npapi.h"
#include "nprmon.h"

/****************************************************************************/
/*   Function Name           : FsRmonHwSetEtherStatsTable                   */
/*                                                                          */
/*   Description             : This API call configures ether statistics    */
/*   Input (s)               : u4IfIndex - Interface Index                  */
/*                             u1EtherStatsEnable - FNP_TRUE if enabling    */
/*                                                  EtherStatics, Otherwise */
/*                                                  Disable it              */
/*                                                                          */
/*   Output (s)              : None                                         */
/*                                                                          */
/*   Global Variables Referred  :                                           */
/*                                                                          */
/*   Global Variables Modified  :                                           */
/*                                                                          */
/*   Returns                 : FNP_SUCCESS/FNP_FAILURE                      */
/*                                                                          */
/****************************************************************************/
INT4
FsRmonHwSetEtherStatsTable (UINT4 u4IfIndex, UINT1 u1EtherStatsEnable)
{

    /* Here we will have to extract the table configuration parameters 
     * for the given index and pass the same to the specific HW API.
     */
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1EtherStatsEnable);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*   Function Name           :  FsRmonHwGetEthStatsTable                    */
/*                                                                          */
/*   Description             : This API call configures ether statistics    */
/*   Input (s)               : u4IfIndex - Interface Index                  */
/*                                                                          */
/*                             pEthStatsEntry- Rmon Ehernet Statistics      */
/*                                             structure which needs to     */
/*                                             be updated from hardware.    */
/*                                                                          */
/*   Output (s)              : None                                         */
/*                                                                          */
/*   Global Variables Referred  :                                           */
/*                                                                          */
/*   Global Variables Modified  :                                           */
/*                                                                          */
/*   Returns                 : NONE                                         */
/*                                                                          */
/****************************************************************************/
INT4
FsRmonHwGetEthStatsTable (UINT4 u4IfIndex, tRmonEtherStatsNode * pEthStatsEntry)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pEthStatsEntry);
    return FNP_SUCCESS;
}
