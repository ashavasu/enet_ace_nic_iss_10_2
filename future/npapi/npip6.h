/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: npip6.h,v 1.3 2007/02/01 14:59:31 iss Exp $
 *
 * Description: All prototypes for Network Processor API functions done here
 *
 *******************************************************************/
#ifndef _NPIP6_H
#define _NPIP6_H

#include "ip6np.h"
#endif /* _NPIP6_H */
