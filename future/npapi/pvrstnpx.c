/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pvrstnpx.c,v 1.1 2007/10/24 09:55:37 iss Exp $ 
 *
 * Description: All network processor function  given here
 *
 *******************************************************************/
#ifdef  PVRST_WANTED

#ifndef _PVRSTNPX_C_
#define _PVRSTNPX_C_

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fssnmp.h"
#include "fsvlan.h"
#include "pvrst.h"
#include "npapi.h"
#include "nppvrst.h"

/* Program port state for Vlan */

/*******************************************************************************
 * FsPvrstMbsmNpSetVlanPortState
 *
 * DESCRIPTION:
 * Sets the PVRST Port State in the Hardware for given Vlan. When PVRST is
 * operating in PVRST mode or PVRST in RSTP compatible mode or PVRST in STP
 * compatible mode.
 *
 * INPUTS:
 * u4IfIndex   - Interface index of whose STP state is to be updated
 * VlanId- Vlan ID.     
 * u1PortState - Value of Port State.
 * 
 * OUTPUTS:
 * None
 *
 * RETURNS:
 * FNP_SUCCESS - success
 * FNP_FAILURE - Error during setting
 *
 * COMMENTS:
 *  None
 *
 *******************************************************************************/
INT1
FsPvrstMbsmNpSetVlanPortState (UINT4 u4IfIndex, tVlanId VlanId,
                               UINT1 u1PortState, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1PortState);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************
 * Function Name                  :FsPvrstMbsmNpInitHw                       *
 *                                                                           *
 * DESCRIPTION                    :This function performs any necessary      * 
 *                                 PVRST related initialisation in the       *
 *                                 Hardware                                  *
 *                                                                           *
 * INPUTS                         : pSlotInfo - Slot Information             *
 *                                                                           *
 *                                                                           *
 * OUTPUTS                        : None                                     *
 *                                                                           *
 * RETURNS                        : FNP_SUCCESS - On Success                 *
 *                                  FNP_FAILURE - On Failure                 *
 *                                                                           *
 *****************************************************************************/

INT1
FsPvrstMbsmNpInitHw (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

#endif /* _PVRSTNPX_C_ */
#endif /* PVRST_WANTED */
