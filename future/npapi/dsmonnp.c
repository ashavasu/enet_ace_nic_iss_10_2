/*****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved                      *
 *                                                                           *
 * $Id: dsmonnp.c,v 1.4 2010/07/30 13:53:21 prabuc Exp $                        *
 *                                                                           *
 * Description: This file contains the function implementations  of          *
 *              DSMON NP-API.                                                *
 *****************************************************************************/

#ifndef _DSMONNP_C_
#define _DSMONNP_C_

#include "lr.h"
#include "npapi.h"
#include "cfa.h"
#include "npcfa.h"
#include "dsmon.h"
#include "rmon2.h"
#include "rmon2np.h"
#include "nprmonv2.h"
#include "npdsmon.h"
#include "dsmonnp.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : FsDSMONHwSetStatus                                         */
/*                                                                           */
/* Description  : Updates the DSMON status                                   */
/*                                                                           */
/* Input        : DSMON_ENABLE/DSMON_DISABLE                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : FNP_SUCCESS / FNP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
FsDSMONHwSetStatus (UINT4 u4DSMONStatus)
{
    UNUSED_PARAM (u4DSMONStatus);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FsDSMONHwGetStatus                                         */
/*                                                                           */
/* Description  : Retrieves the DSMON status                                 */
/*                                                                           */
/* Input        :  NONE                                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : FNP_SUCCESS / FNP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
UINT4
FsDSMONHwGetStatus (VOID)
{
    /* Status maintained in NPAPI data structure
       Need to be associated with NP if NP
       has such support */

    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FsDSMONEnableProbe                                         */
/*                                                                           */
/* Description  : Enables the DSMON interface to be monitored                */
/*                                                                           */
/* Input        : Interface Index, Vlan Id and port list to be monitored     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : FNP_SUCCESS / FNP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
FsDSMONEnableProbe (UINT4 u4InterfaceIdx, UINT2 u2VlanId, tPortList PortList)
{
    UNUSED_PARAM (u4InterfaceIdx);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (PortList);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FsDSMONDisableProbe                                        */
/*                                                                           */
/* Description  : Disables the DSMON interface to be monitored               */
/*                                                                           */
/* Input        : Interface Index, Vlan Id and port list                     */
/*                                   to be stopped from monitoring           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : FNP_SUCCESS / FNP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
FsDSMONDisableProbe (UINT4 u4InterfaceIdx, UINT2 u2VlanId, tPortList PortList)
{
    UNUSED_PARAM (u4InterfaceIdx);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (PortList);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FsDSMONAddFlowStats                                        */
/*                                                                           */
/* Description  : Adds a Flow Stats to be monitored                          */
/*                                                                           */
/* Input        : tPktHeader - Packet Info                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : FNP_SUCCESS / FNP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
FsDSMONAddFlowStats (tPktHeader FlowStatsTuple)
{
    UNUSED_PARAM (FlowStatsTuple);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FsDSMONRemoveFlowStats                                     */
/*                                                                           */
/* Description  : Updates the Flow Stats tree                                */
/*                                                                           */
/* Input        : tPktHeader - Packet Info                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : FNP_SUCCESS / FNP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
FsDSMONRemoveFlowStats (tPktHeader FlowStatsTuple)
{
    UNUSED_PARAM (FlowStatsTuple);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FsDSMONCollectStats                                        */
/*                                                                           */
/* Description  : Updates the Flow Stats tree                                */
/*                                                                           */
/* Input        : tPktHeader - Packet Info                                   */
/*                                                                           */
/* Output       :  tRmon2Stats for packet count                              */
/*                                                                           */
/* Returns      : FNP_SUCCESS / FNP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
FsDSMONCollectStats (tPktHeader FlowStatsTuple, tRmon2Stats * pStatsCnt)
{
    UNUSED_PARAM (FlowStatsTuple);
    UNUSED_PARAM (pStatsCnt);
    return FNP_SUCCESS;
}

#endif
