/*************************************************************************
* Copyright (C) 2007-2012 Aricent Group . All Rights Reserved
*
* $Id: fsbnpx.c,v 1.1 2015/10/01 12:32:44 siva Exp $
*
* Description: This file contains the function implementations of FSB NP-API.
*
*************************************************************************/
#ifndef _FSBNP_C_
#define _FSBNP_C_

#include "lr.h"
#include "npapi.h"
#include "fsb.h"
#include "fsbnp.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsMiNpFsbMbsmHwInit                              */
/*                                                                           */
/*    Description         : The NPAPI installs the Default VLAN ACL          */
/*                          required for switching & copying the             */
/*                          FIP VLAN Discovery frames to the FIP Snooping    */
/*                          module, executing in the control plane CPU.      */
/*                          This is called while FSB module is enabled.      */
/*                                                                           */
/*    Input(s)            : pFsbHwFilterEntry - Inputs to install filter     */
/*                                                                           */
/*    Output(s)           : pFsbLocRemFilterId - Pointer to                  */
/*                          Filter ID structure                              */
/*                                                                           */
/*    Returns             : FNP_SUCCESS / FNP_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsMiNpFsbMbsmHwInit (tFsbHwFilterEntry * pFsbHwFilterEntry,
        tFsbLocRemFilterId *pFsbLocRemFilterId)
{
    UNUSED_PARAM (pFsbHwFilterEntry);
    UNUSED_PARAM (pFsbLocRemFilterId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsFsbMbsmHwCreateFilter                          */
/*                                                                           */
/*    Description         : This NP call installs filters for default ACLS   */
/*                          (for the FIP Snooping Enabled FCOE Vlans)        */
/*                          and ACL's required for the FIP sessions          */
/*                                                                           */
/*    Input(s)            : pFsbHwFilterEntry - Inputs to install filter     */
/*                                                                           */
/*    Output(s)           : pu4FilterId - Filter ID                          */
/*                                                                           */
/*    Returns             : FNP_SUCCESS / FNP_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
FsFsbMbsmHwCreateFilter(tFsbHwFilterEntry *pFsbHwFilterEntry, UINT4 *pu4FilterId)
{
    UNUSED_PARAM (pFsbHwFilterEntry);
    UNUSED_PARAM (pu4FilterId);
    return FNP_SUCCESS;
}
#endif
