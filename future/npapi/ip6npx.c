/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6npx.c,v 1.3 2010/10/20 13:19:12 prabuc Exp $
 *
 * Description: Network Processor API functions definitions for IPv6
 *
 *******************************************************************/
#ifndef _IP6NPX_C_
#define _IP6NPX_C_

#include "lr.h"
#include "cfa.h"
#include "fssnmp.h"
#include "ipv6.h"
#include "npapi.h"
#include "npip6.h"

extern UINT1       *Ip6PrintAddr PROTO ((tIp6Addr * pAddr));

/****************************************************************************
 Function    :  FsNpMbsmIpv6Init
 
 Description :  This function adds filter to send Ipv6 Miss and ERROR 
                packets to CPU
                
 Input       :  None.
 
 Output      :  None.

 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpMbsmIpv6Init (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

#ifdef RIP6_WANTED
/****************************************************************************
 Function    :  FsNpMbsmRip6Init
 
 Description :  This function adds filter to send Ipv6 Miss and ERROR 
                packets to CPU
                
 Input       :  None.
 
 Output      :  None.

 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpMbsmRip6Init (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}
#endif
/*****************************************************************/
/*  Function Name             : FsMbsmNpOspf3Init                 */
/*  Description               : This fucntion does the initializa*/
/*                              -ation in hardware to receive    */
/*                              OSPF3 packets                     */
/*  Input(s)                  : None.                            */
/*  Output(s)                 : None                             */
/*  Global Variables Referred : None                             */
/*  Global variables Modified : None                             */
/*  Exceptions                : None                             */
/*  Use of Recursion          : None                             */
/*  Returns                   : VOID                             */
/*****************************************************************/
INT1
FsNpMbsmOspf3Init (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/****************************************************************************
 Function    :  FsNpMbsmIpv6NeighCacheAdd

 Description :  This function adds or deletes an entry in the Host Table
                for the specified Ip6 Address (pu1Ip6Addr) Mac Address 
                (pu1HwAddr)
                
 Input       :  u4VrId        - Virtual Router Identifier.
                pu1Ip6Addr    - Ipv6 Destination Address whose Link-Layer
                                address is resolved.
                u4IfIndex     - Interface through which specified Ipv6
                                Address is reachable.
                pu1HwAddr     - Link-Layer Address of the specified Ipv6
                                address.
                u1HwAddrLen   - Link-Layer Address Length.
                u1ReachStatus - The Reachability Status of this entry.
                                NP_IPV6_NH_INCOMPLETE - 1
                                NP_IPV6_NH_REACHABLE  - 2
                                NP_IPV6_NH_STALE      - 3
 Output      :  None.

 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpMbsmIpv6NeighCacheAdd (UINT4 u4VrId, UINT1 *pu1Ip6Addr, UINT4 u4IfIndex,
                           UINT1 *pu1HwAddr, UINT1 u1HwAddrLen,
                           UINT1 u1ReachStatus, UINT2 u2VlanId,
                           tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pu1Ip6Addr);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1HwAddr);
    UNUSED_PARAM (u1HwAddrLen);
    UNUSED_PARAM (u1ReachStatus);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/****************************************************************************
 Function    :  FsNpMbsmIpv6NeighCacheDel
 
 Description :  This function deletes an Ipv6 entry in the Host Table 
                matching the specified Ip6 Address (pu1Ip6Addr).

 Input       :  u4VrId       - Virtual Router Identifier.
                pu1Ip6Addr   - Ipv6 Destination Address whose Link-Layer
                               address needs to be deleted.
                u4IfIndex    - Interface through which specified Ipv6
                               Address is reachable.
 Output      :  None.

 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpMbsmIpv6NeighCacheDel (UINT4 u4VrId, UINT1 *pu1Ip6Addr, UINT4 u4IfIndex,
                           tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pu1Ip6Addr);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/****************************************************************************
 Function    :  FsNpMbsmIpv6UcRouteAdd
 
 Description :  This function is used to add route entries into the HW route 
                table associated with the Virtual Router Id (u4VrId).
                
 Input       :  u4VrId       - Virtual Router Identifier.
                pu1Ip6Prefix - Ipv6 Route Prefix
                u1PrefixLen  - Ipv6 Route Prefix Length.
                pu1NextHop   - Next-Hop address.
                u4IfIndex    - Interface through with the next-hop is
                               reachable.
                u4NHType     - Next Hop Type, Not used in BCM.
                
 Output      :  None.
 
 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpMbsmIpv6UcRouteAdd (UINT4 u4VrId, UINT1 *pu1Ip6Prefix, UINT1 u1PrefixLen,
                        UINT1 *pu1NextHop, UINT4 u4NHType,
                        tFsNpIntInfo * pIntInfo, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pu1Ip6Prefix);
    UNUSED_PARAM (u1PrefixLen);
    UNUSED_PARAM (pu1NextHop);
    UNUSED_PARAM (u4NHType);
    UNUSED_PARAM (pIntInfo);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/****************************************************************************
 Function    :  FsNpMbsmIpv6IsRtPresentInFastPath
 Description :  This function is used to query whether the given route is
                present in the HW Route Table
 Input       :  u4VrId       - Virtual Router Identifier
                pu1Ip6Prefix - Ipv6 Route Prefix
                u1PrefixLen  - Ipv6 Route Prefix Length
                pu1NextHop   - Next-Hop address
                u4IfIndex    - Interface through with the next-hop is 
                reachable
 Output      :  None.
 Returns     :  FNP_SUCCESS or FNP_FAILURE
L3RtInfo.l3a_flags |= BCM_L3_IP6;
L3RtInfo.l3a_flags |= BCM_L3_IP6;
****************************************************************************/
INT4
FsNpMbsmIpv6RtPresentInFastPath (UINT4 u4VrId, UINT1 *pu1Ip6Prefix,
                                 UINT1 u1PrefixLen, UINT1 *pu1NextHop,
                                 UINT4 u4Index, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pu1Ip6Prefix);
    UNUSED_PARAM (u1PrefixLen);
    UNUSED_PARAM (pu1NextHop);
    UNUSED_PARAM (u4Index);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/****************************************************************************
 Function    :  FsNpMbsmIpv6IntfCreate
 
 Description :  This function is used to create an interface for the
                Virtual Router (u4VrId).
                
 Input       :  u4VrId    - Virtual Router Identifier
                u4IfIndex - Interface Index value
                
 Output      :  None.

 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpMbsmIpv6IntfCreate (UINT4 u4VrId, UINT4 u4IfIndex,
                        tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/****************************************************************************
 Function    :  FsNpMbsmIpv6IntfStatus
 
 Description :  This function is used to indicate change in the operational
                status of interface associated with the Virtual Router
                (u4VrId) operational status.
                
 Input       :  u4VrId      - Virtual Router Identifier
                u4IfIndex   - Interface Index value
                u4IfStatus  - Interface's Operational Status. The values are:
                                    NP_IP6_IF_UP - 1 
                                    NP_IP6_IF_DOWN - 2
                                    
 Output      :  None.

 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpMbsmIpv6IntfStatus (UINT4 u4VrId, UINT4 u4IfStatus, tFsNpIntInfo * pIntInfo,
                        tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4IfStatus);
    UNUSED_PARAM (pIntInfo);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}
#endif /* _IP6NPX_C_ */
