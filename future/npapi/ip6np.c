/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6np.c,v 1.9 2015/01/28 12:37:08 siva Exp $
 *
 * Description: Network Processor API functions definitions for IPv6
 *
 *******************************************************************/

#include "lr.h"
#include "cfa.h"
#include "fssnmp.h"
#include "ipv6.h"
#include "npapi.h"
#include "npip6.h"

/****************************************************************************
 Function    :  FsNpIpv6Init
 Description :  This function initialises all the required data structures
                associated with Ipv6 NPAPI Implementation.
 Input       :  None.
 Output      :  None.
 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpIpv6Init (VOID)
{
    return FNP_SUCCESS;
}

/****************************************************************************
 Function    :  FsNpIpv6Deinit
 Description :  This function deinitialises all the required data structures
                associated with Ipv6 NPAPI Implementation.
 Input       :  None.
 Output      :  None.
 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpIpv6Deinit (VOID)
{
    return FNP_SUCCESS;
}

/****************************************************************************
 Function    :  FsNpIpv6NeighCacheAdd
 Description :  This function adds or updates an entry in the Neighbor Cache
                Table for the specified Ip6 Address (pu1Ip6Addr) and
                Interface Index Combination.
 Input       :  u4VrId       - Virtual Router Identifier.
                pu1Ip6Addr - Ipv6 Destination Address whose Link-Layer
                             address needs to be resolved.
                u4IfIndex  - Interface through which specified Ipv6
                             Address is reachable.
                pu1HwAddr  - Link-Layer Address of the specified Ipv6
                             address.
                u1HwAddrLen - Link-Layer Address Length.
                u1ReachStatus - The Reachability Status of this entry.
                                NP_IPV6_NH_INCOMPLETE - 1
                                NP_IPV6_NH_REACHABLE  - 2
                                NP_IPV6_NH_STALE      - 3
                u2VlanId - VlanID
 Output      :  None.
 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpIpv6NeighCacheAdd (UINT4 u4VrId, UINT1 *pu1Ip6Addr, UINT4 u4IfIndex,
                       UINT1 *pu1HwAddr, UINT1 u1HwAddrLen,
                       UINT1 u1ReachStatus, UINT2 u2VlanId)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pu1Ip6Addr);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1HwAddr);
    UNUSED_PARAM (u1HwAddrLen);
    UNUSED_PARAM (u1ReachStatus);
    UNUSED_PARAM (u2VlanId);
    return FNP_SUCCESS;
}

/****************************************************************************
 Function    :  FsNpIpv6NeighCacheDel
 Description :  This function deletes an entry in the Neighbor Cache
                Table matching the specified Ip6 Address (pu1Ip6Addr) and
                Interface Index Combination.
 Input       :  u4VrId       - Virtual Router Identifier.
                pu1Ip6Addr - Ipv6 Destination Address whose Link-Layer
                             address needs to be resolved.
                u4IfIndex  - Interface through which specified Ipv6
                             Address is reachable.
 Output      :  None.
 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpIpv6NeighCacheDel (UINT4 u4VrId, UINT1 *pu1Ip6Addr, UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pu1Ip6Addr);
    UNUSED_PARAM (u4IfIndex);
    return FNP_SUCCESS;
}

/****************************************************************************
 Function    :  FsNpIpv6CheckHitOnNDCacheEntry
 Description :  This function reads the hit bit on the ND Cache entry and
                clears the bit after reading
 Input       :  u4VrId       - Virtual Router Identifier.
                pu1Ip6Addr - Ipv6 Destination Address whose Cache has to be
                checked
                u4IfIndex  - Interface through which specified Ipv6
                             Address is reachable.
 Output      :  None.
 Returns     :  FNP_TRUE or FNP_FALSE
****************************************************************************/
UINT1
FsNpIpv6CheckHitOnNDCacheEntry (UINT4 u4VrId, UINT1 *pu1Ip6Addr,
                                UINT4 u4IfIndex)
{

    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pu1Ip6Addr);
    UNUSED_PARAM (u4IfIndex);
    return FNP_FALSE;
}

/****************************************************************************
 Function    :  FsNpIpv6UcRouteAdd
 Description :  This function is used to add route entries into the FIB table
                associated with the Virtual Router Id (u4VrId). If no route
                entry exists for the given Prefix (pu1Ip6Prefix) /
                Prefix Length (u1PrefixLen) / Next-Hop combination, then an
                entry should be added in the FIB table. If an entry already
                exists, then the interface index and next-hop type
                information should be updated.
 Input       :  u4VrId       - Virtual Router Identifier.
                pu1Ip6Prefix - Ipv6 Route Prefix
                u1PrefixLen  - Ipv6 Route Prefix Length.
                pu1NextHop   - Next-Hop address.
                u4NHType     - Describes the action to be performed when
                               this route entry is chosen for forwarding.
                               The value are:
                                 NH_DIRECT (1) - Destination Address belongs
                                                 to Directly Attached Subnet.
                                 NH_REMOTE (2) - Destination Address belongs
                                                 to different subnet and packet
                                                 needs to be forwarded to the
                                                 next-hop.
                                 NH_SENDTO_CP (3) - Send the packet to the
                                                    control plane.
                                 NH_DISCARD (4)- Drop the packet.
                                 NH_TUNNEL (5) - The next-hop address is the
                                                 address of the next-hop in the
                                                 tunnel. The tunnel exit node
                                                 address, to which the
                                                 encapsulated packets are sent,
                                                 is either derived from this
                                                 next-hop or from the tunnel
                                                 interface structure.
                pIntInfo - IPv6 Interface Information
 Output      :  None.
 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpIpv6UcRouteAdd (UINT4 u4VrId, UINT1 *pu1Ip6Prefix, UINT1 u1PrefixLen,
                    UINT1 *pu1NextHop, UINT4 u4NHType, tFsNpIntInfo * pIntInfo)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pu1Ip6Prefix);
    UNUSED_PARAM (u1PrefixLen);
    UNUSED_PARAM (pu1NextHop);
    UNUSED_PARAM (u4NHType);
    UNUSED_PARAM (pIntInfo);
    return FNP_SUCCESS;
}

/****************************************************************************
 Function    :  FsNpIpv6UcRouteDel
 Description :  This function is used to delete route entries from the FIB
                table associated with the Virtual Router Id (u4VrId).
 Input       :  u4VrId       - Virtual Router Identifier.
                pu1Ip6Prefix - Ipv6 Route Prefix
                u1PrefixLen  - Ipv6 Route Prefix Length.
                pu1NextHop   - Next-Hop address.
                u4IfIndex    - Interface through with the next-hop is
                               reachable.
 Output      :  None.
 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpIpv6UcRouteDelete (UINT4 u4VrId, UINT1 *pu1Ip6Prefix, UINT1 u1PrefixLen,
                       UINT1 *pu1NextHop, UINT4 u4IfIndex,
                       tFsNpRouteInfo * pRouteInfo)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pu1Ip6Prefix);
    UNUSED_PARAM (u1PrefixLen);
    UNUSED_PARAM (pu1NextHop);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pRouteInfo);
    return FNP_SUCCESS;
}

/****************************************************************************
 Function    :  FsNpIpv6RtPresentInFastPath
 Description :  This function is used to query whether the given route is
                present in the FIB table associated with the Virtual Router
                Id (u4VrId).
 Input       :  u4VrId       - Virtual Router Identifier
                pu1Ip6Prefix - Ipv6 Route Prefix
                u1PrefixLen  - Ipv6 Route Prefix Length
                pu1NextHop   - Next-Hop address
                u4IfIndex    - Interface through with the next-hop is reachable
 Output      :  None.
 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpIpv6RtPresentInFastPath (UINT4 u4VrId, UINT1 *pu1Ip6Prefix,
                             UINT1 u1PrefixLen, UINT1 *pu1NextHop,
                             UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pu1Ip6Prefix);
    UNUSED_PARAM (u1PrefixLen);
    UNUSED_PARAM (pu1NextHop);
    UNUSED_PARAM (u4IfIndex);
    return FNP_SUCCESS;
}

/****************************************************************************
 Function    :  FsNpIpv6UcRouting
 Description :  This function is used to enable or disable the uni-cast
                routing on Virtual Router (u4VrId).
 Input       :  u4VrId          - Virtual Router Identifier
                u4RoutingStatus - Uni-Cast Routing Status. The value are
                                      NP_IP6_FORW_ENABLE (1)
                                      NP_IP6_FORW_DISABLE (2)
 Output      :  None.
 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpIpv6UcRouting (UINT4 u4VrId, UINT4 u4RoutingStatus)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4RoutingStatus);
    return FNP_SUCCESS;
}

/****************************************************************************
 Function    :  FsNpIpv6GetStats
 Description :  This function is used to query the required statistics
                information from the tables associated with the Virtual
                Router Id (u4VrId).
 Input       :  u4VrId      - Virtual Router Identifier
                u4IfIndex   - Interface Index
                u4StatsFlag - Describes the entity whose statistic needs to
                              be retrieved. The values are
                                    NP_STAT_IP6_IN_HDR_ERRORS     - 1
                                    NP_STAT_IP6_IN_DISCARDS       - 2
                                    NP_STAT_IP6_IN_FORW_DATAGRAMS - 3
                                    NP_STAT_IP6_IN_RECIEVES       - 4 
 Output      :  pu4StatsValue - Statistical value of the entity defined by
                                u4StatsFlag .
 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpIpv6GetStats (UINT4 u4VrId, UINT4 u4IfIndex, UINT4 u4StatsFlag,
                  UINT4 *pu4StatsValue)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4StatsFlag);
    UNUSED_PARAM (pu4StatsValue);
    return FNP_SUCCESS;
}

/****************************************************************************
 Function    :  FsNpIpv6IntfStatus
 Description :  This function is used to indicate change in the operational
                status of interface associated with the Virtual Router
                (u4VrId) operational status.
 Input       :  u4VrId      - Virtual Router Identifier
                pIntfInfo - Ipv6 Interface Information
                u4IfStatus  - Interface's Operational Status. The values are:
                                    NP_IP6_IF_UP - 1 
                                    NP_IP6_IF_DOWN - 2
 Output      :  None.
 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpIpv6IntfStatus (UINT4 u4VrId, UINT4 u4IfStatus, tFsNpIntInfo * pIntfInfo)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (pIntfInfo);
    UNUSED_PARAM (u4IfStatus);
    return FNP_SUCCESS;
}

/****************************************************************************
 Function    :  FsNpIpv6AddrCreate
 Description :  This function is used to add an ipv6 address over the
                interface for the Virtual Router (u4VrId).
 Input       :  u4VrId      - Virtual Router Identifier
                u4IfIndex   - Interface Index value
                u4AddrType  - Address Type. The values are:
                                NP_IP6_UNI_ADDR - 1
                                NP_IP6_MULTI_ADDR - 2
                pu1Ip6Addr   - Ipv6 Address assigned to the interface
                u1PrefixLen  - Prefix Length
 Output      :  None.
 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpIpv6AddrCreate (UINT4 u4VrId, UINT4 u4IfIndex, UINT4 u4AddrType,
                    UINT1 *pu1Ip6Addr, UINT1 u1PrefixLen)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4AddrType);
    UNUSED_PARAM (pu1Ip6Addr);
    UNUSED_PARAM (u1PrefixLen);
    return FNP_SUCCESS;
}

/****************************************************************************
 Function    :  FsNpIpv6AddrDelete
 Description :  This function is used to delete an ipv6 address from the
                interface for the Virtual Router (u4VrId).
 Input       :  u4VrId       - Virtual Router Identifier
                u4IfIndex    - Interface Index value
                u4AddrType   - Address Type. The values are:
                                    NP_IP6_UNI_ADDR - 1 
                                    NP_IP6_MULTI_ADDR - 2 
                pu1Ip6Addr   - Ipv6 Address assigned to the interface
                u1PrefixLen  - Prefix Length
 Output      :  None.
 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpIpv6AddrDelete (UINT4 u4VrId, UINT4 u4IfIndex, UINT4 u4AddrType,
                    UINT1 *pu1Ip6Addr, UINT1 u1PrefixLen)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4AddrType);
    UNUSED_PARAM (pu1Ip6Addr);
    UNUSED_PARAM (u1PrefixLen);
    return FNP_SUCCESS;
}

/****************************************************************************
 Function    :  FsNpIpv6TunlParamSet
 Description :  This function is used to set the parameters associated with
                the tunnel interface for the Virtual Router (u4VrId).
 Input       :  u4VrId       - Virtual Router Identifier
                u4IfIndex    - Tunnel Interface Index value
                u4TunlType   - Tunnel's Encapsulation Method
                u4SrcAddr    - Tunnel's Ipv4 Source Address
                u4DstAddr    - Tunnel's Ipv4 Destination Address
 Output      :  None.
 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpIpv6TunlParamSet (UINT4 u4VrId, UINT4 u4IfIndex, UINT4 u4TunlType,
                      UINT4 u4SrcAddr, UINT4 u4DstAddr)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4TunlType);
    UNUSED_PARAM (u4SrcAddr);
    UNUSED_PARAM (u4DstAddr);
    return FNP_SUCCESS;
}

/****************************************************************************
 Function    :  FsNpIpv6AddMcastMAC
 Description :  This function is used to add the MAC Address associated the
                Multicast address registered over the interface for the
                Virtual Router (u4VrId).
 Input       :  u4VrId       - Virtual Router Identifier
                u4IfIndex    - Tunnel Interface Index value
                pu1MacAddr   - Mcast MAC Address
                u1MacAddrLen - MAC Address Length
 Output      :  None.
 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpIpv6AddMcastMAC (UINT4 u4VrId, UINT4 u4IfIndex, UINT1 *pu1MacAddr,
                     UINT1 u1MacAddrLen)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1MacAddr);
    UNUSED_PARAM (u1MacAddrLen);
    return FNP_SUCCESS;
}

/****************************************************************************
 Function    :  FsNpIpv6DelMcastMAC
 Description :  This function is used to delete the MAC Address associated the
                Multicast address registered over the interface for the
                Virtual Router (u4VrId).
 Input       :  u4VrId       - Virtual Router Identifier
                u4IfIndex    - Tunnel Interface Index value
                pu1MacAddr   - Mcast MAC Address
                u1MacAddrLen - MAC Address Length
 Output      :  None.
 Returns     :  FNP_SUCCESS or FNP_FAILURE
****************************************************************************/
INT4
FsNpIpv6DelMcastMAC (UINT4 u4VrId, UINT4 u4IfIndex, UINT1 *pu1MacAddr,
                     UINT1 u1MacAddrLen)
{
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1MacAddr);
    UNUSED_PARAM (u1MacAddrLen);
    return FNP_SUCCESS;
}

/****************************************************************************
 Function    :  FsNpIpv6HandleFdbMacEntryChange
 Description :  This function is used to handle the change in the status of
                the MAC entry in VLAN FDB table.
 Input       :  pu1L3Nexthop - L3 NextHop Address corresponding to the
                               MAC Address.
                u4IfIndex    - L3 Interface Index.
                pu1MacAddr   - MAC Address changed.
                u4IsRemoved  - Status indicating whether the MAC entry is
                               added or deleted.
 Output      :  None.
 Returns     :  FNP_SUCCESS or FNP_FAILURE
*****************************************************************************/
INT4
FsNpIpv6HandleFdbMacEntryChange (UINT1 *pu1L3Nexthop, UINT4 u4IfIndex,
                                 UINT1 *pu1MacAddr, UINT4 u4IsRemoved)
{
    UNUSED_PARAM (pu1L3Nexthop);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1MacAddr);
    UNUSED_PARAM (u4IsRemoved);
    return FNP_SUCCESS;
}

#ifdef OSPF3_WANTED
/*****************************************************************/
/*  Function Name             : FsNpOspf3Init                    */
/*  Description               : This fucntion does the initializa*/
/*                              -ation in hardware to receive    */
/*                              OSPF3 packets                    */
/*  Input(s)                  : None.                            */
/*  Output(s)                 : None                             */
/*  Global Variables Referred : None                             */
/*  Global variables Modified : None                             */
/*  Exceptions                : None                             */
/*  Use of Recursion          : None                             */
/*  Returns                   : FNP_SUCCESS                      */
/*****************************************************************/
INT4
FsNpOspf3Init (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************/
/*  Function Name             : FsNpOspf3DeInit                  */
/*  Description               : This function does the de-init   */
/*                              in hardware                      */
/*  Input(s)                  : None                             */
/*  Output(s)                 : None                             */
/*  Global Variables Referred : None                             */
/*  Global variables Modified : None                             */
/*  Exceptions                : None                             */
/*  Use of Recursion          : None                             */
/*  Returns                   : VOID                             */
/*****************************************************************/
VOID
FsNpOspf3DeInit (VOID)
{
    return;
}
#endif /* OSPF3_WANTED */

#ifdef RIP6_WANTED
/****************************************************************************
 Function    :  FsNpRip6Init
 
 Description :  This function adds filter to send Ipv6 Miss and ERROR 
                packets to CPU
                
 Input       :  None.
 
 Output      :  None.

 Returns     :  FNP_SUCCESS
****************************************************************************/
INT4
FsNpRip6Init ()
{
    return FNP_SUCCESS;
}
#endif /* RIP6_WANTED */

/*****************************************************************************
 *  Function Name             :  FsNpIpv6NeighCacheGet                       
 *  Description               :  Gets ND6 entry from the Fast Path Table     
 *  Input(s)                  :  u4VrId - The virtual router identifier      
 *                               pu1Ip6Addr   -- IpAddress                   
 *  Output(s)                 :  pHwAddr - MAC address                       
 *                               pu4CfaIfIndex -- Pointer to IfIndex         
 *                               pi1ReachState -- Reachability state of the  
 *                                                nd cache entry             
 *  Global Variables Referred :  None                                        
 *  Global variables Modified :  None                                        
 *  Exceptions                :  None                                        
 *  Use of Recursion          :  None                                        
 *  Returns                   :  FNP_FAILURE(0) on failure                   
 *                               FNP_SUCCESS(1) on success                   
 *****************************************************************************/

INT4
FsNpIpv6NeighCacheGet (tNpNDCacheInput Nd6NpInParam,
                       tNpNDCacheOutput * pNd6NpOutParam)
{
    UNUSED_PARAM (Nd6NpInParam);
    UNUSED_PARAM (pNd6NpOutParam);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name             :  FsNpIpv6NeighCacheGetNext                   */
/*  Description               :  Gets  the next ND6 entry from the FPT       */
/*  Input(s)                  :  u4VrId - The virtual router identifier      */
/*                               pu1Ip6Addr -- IpAddress                     */
/*  Output(s)                 :  pHwAddr - MAC address                       */
/*                               pu4CfaIfIndex -- Pointer to IfIndex         */
/*                               pReachState -- Reachability state of the    */
/*                                                nd cache entry             */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions                :  None                                        */
/*  Use of Recursion          :  None                                        */
/*  Returns                   :  FNP_FAILURE(0) on failure                   */
/*                               FNP_SUCCESS(1) on success                   */
/*****************************************************************************/
INT4
FsNpIpv6NeighCacheGetNext (tNpNDCacheInput Nd6NpInParam,
                           tNpNDCacheOutput * pNd6NpOutParam)
{
    UNUSED_PARAM (Nd6NpInParam);
    UNUSED_PARAM (pNd6NpOutParam);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name             :  FsNpIpv6UcGetRoute                          */
/*  Description               :  Gets the next hop info for the given dest   */
/*  Input(s)                  :  DstIp   -- Ipv6Address                      */
/*  Output(s)                 :  NextHop  -- Next hop v6 address             */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions                :  None                                        */
/*  Use of Recursion          :  None                                        */
/*  Returns                   :  FNP_FAILURE(0) on failure                   */
/*                               FNP_SUCCESS(1) on success                   */
/*****************************************************************************/
INT4
FsNpIpv6UcGetRoute (tNpRtm6Input Rtm6NpInParam, tNpRtm6Output * pRtm6NpOutParam)
{
    UNUSED_PARAM (Rtm6NpInParam);
    UNUSED_PARAM (pRtm6NpOutParam);
    return FNP_SUCCESS;
}
/*****************************************************************************/
/*  Function Name             :  FsNpIpv6Enable                              */
/*  Description               :  Enable IPv6 on the Interface                */
/*  Input(s)                  :  u4Index  -- Interface Index                 */
/*                               u2VlanId -- Vlan ID                         */
/*  Output(s)                 :  NextHop  -- Next hop v6 address             */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions                :  None                                        */
/*  Use of Recursion          :  None                                        */
/*  Returns                   :  FNP_FAILURE(0) on failure                   */
/*                               FNP_SUCCESS(1) on success                   */
/*****************************************************************************/
INT4
FsNpIpv6Enable (UINT4 u4Index, UINT2  u2VlanId)
{

    UNUSED_PARAM (u4Index);
    UNUSED_PARAM (u2VlanId);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name             :  FsNpIpv6Disable                             */
/*  Description               :  Disable IPv6 on the Interface               */
/*  Input(s)                  :  u4Index  -- Interface Index                 */
/*                               u2VlanId -- Vlan ID                         */
/*  Output(s)                 :  NextHop  -- Next hop v6 address             */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions                :  None                                        */
/*  Use of Recursion          :  None                                        */
/*  Returns                   :  FNP_FAILURE(0) on failure                   */
/*                               FNP_SUCCESS(1) on success                   */
/*****************************************************************************/
INT4
 FsNpIpv6Disable (UINT4 u4Index,UINT2  u2VlanId)
{

    UNUSED_PARAM (u4Index);
    UNUSED_PARAM (u2VlanId);
    return FNP_SUCCESS;
}

