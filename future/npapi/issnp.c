/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: issnp.c,v 1.45 2017/01/27 12:27:27 siva Exp $
 *
 * Description: This file contains the function implementations  of ISS
 *              System related NP-API.
 ****************************************************************************/
#include "lr.h"
#include "iss.h"
#include "npapi.h"
#include "npiss.h"
/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwSetPortEgressStatus                         */
/*                                                                          */
/*    Description        : This function sets the egress status of the      */
/*                         given port. In case if the IfIndex is 0, then    */
/*                         it will be applied for all ports.                */
/*                                                                          */
/*    Input(s)           : u4IfIndex - Port interface index.                */
/*                         u1EgressEnable - ISS_ENABLE if Egress enabled    */
/*                                          otherwise ISS_DISABLE           */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwSetPortEgressStatus (UINT4 u4IfIndex, UINT1 u1EgressEnable)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1EgressEnable);
    return FNP_SUCCESS;
}

/*******************************************************************************/
/* Function Name      : NpUtilAccessHwConsole                                  */
/*                                                                             */
/* Description        : This function access the hardware CLI commands.        */
/*                                                                             */
/* Input(s)           : pu1HwCmd - hardware command                            */
/*                                                                             */
/* Output(s)          : None                                                   */
/*                                                                             */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                              */
/*******************************************************************************/

PUBLIC UINT1
NpUtilAccessHwConsole (UINT1 *pu1HwCmd)
{
    UNUSED_PARAM (pu1HwCmd);
    return (FNP_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwSetPortStatsCollection                      */
/*                                                                          */
/*    Description        : This function enables/disables the port stats    */
/*                         collection of the given port. In case if the     */
/*                         IfIndex is 0, then it will be applied for all    */
/*                         ports.                                           */
/*                                                                          */
/*    Input(s)           : u4IfIndex - Port interface index.                */
/*                         i4Status - Status collection enabled / disabled. */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwSetPortStatsCollection (UINT4 u4IfIndex, UINT1 u1StatsEnable)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1StatsEnable);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwSetPortMode                                 */
/*                                                                          */
/*    Description        : This function sets the port mode to auto or      */
/*                         negotiation.                                     */
/*                                                                          */
/*    Input(s)           : u4IfIndex - Port interface index.                */
/*                         i4Value - Port Mode value (auto/noNegotiation)   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwSetPortMode (UINT4 u4IfIndex, INT4 i4Value)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4Value);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwSetPortDuplex                               */
/*                                                                          */
/*    Description        : This function sets the port as half/full duplex. */
/*                                                                          */
/*    Input(s)           : u4IfIndex - Port interface index.                */
/*                         i4Value - half/full duplex.                      */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwSetPortDuplex (UINT4 u4IfIndex, INT4 i4Value)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4Value);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwSetPortSpeed                                */
/*                                                                          */
/*    Description        : This function sets the port speed to either of   */
/*                         10/100MBPS  or 1/10 GB.                          */
/*                                                                          */
/*    Input(s)           : u4IfIndex - Port interface index.                */
/*                         i4Value - Port speed value(10/100MBPS or 1/10 GB)*/
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwSetPortSpeed (UINT4 u4IfIndex, INT4 i4Value)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4Value);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwSetPortFlowControl                          */
/*                                                                          */
/*    Description        : This function enables/disables Flow Control      */
/*                         in the hardware.                                 */
/*                                                                          */
/*    Input(s)           : u4IfIndex - Port interface index.                */
/*                         u1FlowCtrlEnable - Flow control enabled          */
/*                                            / disabled.                   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwSetPortFlowControl (UINT4 u4IfIndex, UINT1 u1FlowCtrlEnable)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1FlowCtrlEnable);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwSetPortRenegotiate                          */
/*                                                                          */
/*    Description        : This function starts the renegotiation if the    */
/*                         interface PortCtrlMode is configured as 'auto'.  */
/*                                                                          */
/*    Input(s)           : u4IfIndex - Port interface index.                */
/*                         i4Value - Renegotiate true / false.              */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwSetPortRenegotiate (UINT4 u4IfIndex, INT4 i4Value)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4Value);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwSetPortMaxMacAddr                           */
/*                                                                          */
/*    Description        : This function sets the Maximum number new of     */
/*                         Mac Addresses that can be learnt over this       */
/*                         interface.                                       */
/*                                                                          */
/*    Input(s)           : u4IfIndex - Port interface index.                */
/*                         i4Value - MaxMacAddr Value.                      */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwSetPortMaxMacAddr (UINT4 u4IfIndex, INT4 i4Value)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4Value);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwSetPortMaxMacAction                         */
/*                                                                          */
/*    Description        : This function sets Action to be taken when the   */
/*                         Maximum number of address learnt on the interface*/
/*                         exceeds the MaxMacAddr value.                    */
/*                                                                          */
/*    Input(s)           : u4IfIndex - Port interface index.                */
/*                         i4Value - MaxMacAction .                         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwSetPortMaxMacAction (UINT4 u4IfIndex, INT4 i4Value)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4Value);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwSetPortMirroringStatus                      */
/*                                                                          */
/*    Description        : This function enables/disables the port          */
/*                         mirroring in hardware.                           */
/*                                                                          */
/*    Input(s)           : i4MirrorStatus - Port mirroring status.          */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwSetPortMirroringStatus (INT4 i4MirrorStatus)
{
    UNUSED_PARAM (i4MirrorStatus);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwSetMirrorToPort                             */
/*                                                                          */
/*    Description        : This function sets the port on which the         */
/*                         mirrored packets has to be sent in hardware.     */
/*                                                                          */
/*    Input(s)           : u4IfIndex - Mirror to port.                      */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwSetMirrorToPort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwSetIngressMirroring                         */
/*                                                                          */
/*    Description        : This function enables/disables the ingress       */
/*                         mirroring  on the given port. In case if the     */
/*                         IfIndex is 0, then it will be applied for all    */
/*                         ports.                                           */
/*                                                                          */
/*    Input(s)           : u4IfIndex - Port interface index.                */
/*                         i4IngMirroring- Ingress mirroning status.        */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwSetIngressMirroring (UINT4 u4IfIndex, INT4 i4IngMirroring)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IngMirroring);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwSetEgressMirroring                          */
/*                                                                          */
/*    Description        : This function enables/disables the egress        */
/*                         mirroring  on the given port. In case if the     */
/*                         IfIndex is 0, then it will be applied for all    */
/*                         ports.                                           */
/*                                                                          */
/*    Input(s)           : u4IfIndex - Port interface index.                */
/*                         i4EgrMirroring- egress  mirroring status.        */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwSetEgressMirroring (UINT4 u4IfIndex, INT4 i4EgrMirroring)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4EgrMirroring);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwSetRateLimitingValue                        */
/*                                                                          */
/*    Description        : This function sets the rate limiting value for   */
/*                         for all packets of the given port. In case       */
/*                         if the IfIndex is 0, then it will be applied for */
/*                         all ports.                                       */
/*                                                                          */
/*    Input(s)           : u4IfIndex - Port interface index.                */
/*                         u1PacketType - DLF/Broadcast/multicast           */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwSetRateLimitingValue (UINT4 u4IfIndex, UINT1 u1PacketType,
                           INT4 i4RateLimitVal)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1PacketType);
    UNUSED_PARAM (i4RateLimitVal);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwSetPortEgressPktRate                        */
/*                                                                          */
/*    Description        : This function sets the port pkt rate and port    */
/*                         maximum burst size.                              */
/*                                                                          */
/*    Input(s)           : u4IfIndex - Port interface index.                */
/*                         i4PktRate - Packet Rate                          */
/*                         i4BurstRate - Packet Burst Rate                  */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwSetPortEgressPktRate (UINT4 u4IfIndex, INT4 i4PktRate, INT4 i4BurstRate)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4PktRate);
    UNUSED_PARAM (i4BurstRate);

    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwGetPortEgressPktRate                        */
/*                                                                          */
/*    Description        : This function is used to get the Port Egress Rate*/
/*                         from the Hardware.                               */
/*                                                                          */
/*    Input(s)           : u4Port       - Port Index                        */
/*                                                                          */
/*    Output(s)          : pi4PortPktRate - Port Pkt Rate                   */
/*                         pi4PortBurstRate - Port Burst Rate               */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwGetPortEgressPktRate (UINT4 u4Port, INT4 *pi4PortPktRate,
                           INT4 *pi4PortBurstRate)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (pi4PortPktRate);
    UNUSED_PARAM (pi4PortBurstRate);

    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwUpdateL2Filter                              */
/*                                                                          */
/*    Description        : This function is used to update the hardware L2  */
/*                         filter.                                          */
/*                                                                          */
/*    Input(s)           : pIssL2FilterEntry - L2Filter Entry to be updated.*/
/*                         i4Value - Flag for the operation                 */
/*                                  (ISS_L2FILTER_ADD                       */
/*                                   or ISS_L2FILTER_DELETE).               */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
INT4
IssHwUpdateL2Filter (tIssL2FilterEntry * pIssL2FilterEntry, INT4 i4Value)
{
    UNUSED_PARAM (pIssL2FilterEntry);
    UNUSED_PARAM (i4Value);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwUpdateL3Filter                              */
/*                                                                          */
/*    Description        : This function is used to update the hardware L3  */
/*                         filter.                                          */
/*                                                                          */
/*    Input(s)           : pIssL3FilterEntry - L3Filter Entry to be updated.*/
/*                         i4Value - Flag for the operation                 */
/*                                  (ISS_L3FILTER_ADD                       */
/*                                   or ISS_L3FILTER_DELETE).               */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
INT4
IssHwUpdateL3Filter (tIssL3FilterEntry * pIssL3FilterEntry, INT4 i4Value)
{
    UNUSED_PARAM (pIssL3FilterEntry);
    UNUSED_PARAM (i4Value);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwRestartSystem                               */
/*                                                                          */
/*    Description        : This function is used to reset the system.       */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssHwRestartSystem (VOID)
{
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwInitFilter                                  */
/*                                                                          */
/*    Description        : This function is used to initialise the filters  */
/*                         in the hardware.                                 */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwInitFilter (VOID)
{
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwGetPortDuplex                               */
/*                                                                          */
/*    Description        : This function is used to get the Port Data       */
/*                         Transfer Mode from the Hardware.                 */
/*                                                                          */
/*    Input(s)           : UINT4 u4IfIndex                                  */
/*                                                                          */
/*    Output(s)          : INT4 *pi4PortDuplexStatus                        */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwGetPortDuplex (UINT4 u4IfIndex, INT4 *pi4PortDuplexStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pi4PortDuplexStatus);

    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwGetPortSpeed                                */
/*                                                                          */
/*    Description        : This function is used to get the Port Speed      */
/*                         from the Hardware.                               */
/*                                                                          */
/*    Input(s)           : UINT4 u4IfIndex                                  */
/*                                                                          */
/*    Output(s)          : INT4 *pi4PortSpeed                               */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwGetPortSpeed (UINT4 u4IfIndex, INT4 *pi4PortSpeed)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pi4PortSpeed);

    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwGetPortFlowControl                          */
/*                                                                          */
/*    Description        : This function is used to get the Flow Control    */
/*                         Status from the Hardware.                        */
/*                                                                          */
/*    Input(s)           : UINT4 u4IfIndex                                  */
/*                                                                          */
/*    Output(s)          : INT4 *pi4PortFlowControl                         */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwGetPortFlowControl (UINT4 u4IfIndex, INT4 *pi4PortFlowControl)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pi4PortFlowControl);

    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwSetPortHOLBlockPrevention                   */
/*                                                                          */
/*    Description        : This function enables or disables the            */
/*                         Head-Of-Line Blocking prevention for the given   */
/*                         port. Incase of enabling HOL Blocking prevention */
/*                         this function sets the HOL threshold value for   */
/*                         this port.                                       */
/*                                                                          */
/*    Input(s)           : u4IfIndex - Port interface index.                */
/*                         i4Value - Enable/Disable HOL Blocking prevention */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwSetPortHOLBlockPrevention (UINT4 u4IfIndex, INT4 i4Value)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4Value);

    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwGetPortHOLBlockPrevention                   */
/*                                                                          */
/*    Description        : This function reads from hardware whether        */
/*                         Head-Of-Line Blocking prevention is enabled on   */
/*                         the given port or not.                           */
/*                                                                          */
/*    Input(s)           : u4IfIndex - Port interface index.                */
/*                                                                          */
/*    Output(s)          : *pi4Value - ISS_ENABLE / ISS_DISABLE.            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwGetPortHOLBlockPrevention (UINT4 u4IfIndex, INT4 *pi4Value)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pi4Value);

    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwUpdateL4SFilter                              */
/*                                                                          */
/*    Description        : This function is used to update the hardware L4S */
/*                         filter.                                          */
/*                                                                          */
/*    Input(s)           : pIssL4SFilterEntry - L4SFilter Entry to be uptd. */
/*                         i4Value - Flag for the operation                 */
/*                                  (ISS_L4SFILTER_ADD                      */
/*                                   or ISS_L4SFILTER_DELETE).              */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/

INT4
IssHwUpdateL4SFilter (tIssL4SFilterEntry * pIssL4SFilterEntry, INT4 i4Value)
{

    UNUSED_PARAM (pIssL4SFilterEntry);
    UNUSED_PARAM (i4Value);

    return FNP_SUCCESS;
}
/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwSetResrvFrameFilter                         */
/*                                                                          */
/*    Description        : This function is used to update the hardware L4S */
/*                         filter.                                          */
/*                                                                          */
/*    Input(s)           : pIssReservFrmCtrlInfo - Reserved Fram Table      */
/*                    Entry to be uptdated.                                 */
/*                         i4Value - Flag for the operation                 */
/*                                  (ISS_L4SFILTER_ADD                      */
/*                                   or ISS_L4SFILTER_DELETE).              */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
INT4
IssHwSetResrvFrameFilter (tIssReservFrmCtrlTable * pIssReservFrmCtrlTable,INT4 i4Value)
{
        UNUSED_PARAM(pIssReservFrmCtrlTable);
        UNUSED_PARAM(i4Value);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwSendBufferToLinux                           */
/*                                                                          */
/*    Description        : This function gets the Buffer from User Space    */
/*                                                                          */
/*    Input(s)           : pBuffer - Pointer to Buffer                      */
/*                         i4Len - Length of Buffer                         */
/*                         i4ImageType - Normal / FallBack                  */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwSendBufferToLinux (UINT1 *pBuffer, UINT4 u4Len, INT4 i4ImageType)
{
    UNUSED_PARAM (pBuffer);
    UNUSED_PARAM (i4ImageType);
    UNUSED_PARAM (u4Len);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwGetCurrTemperature                          */
/*                                                                          */
/*    Description        : This function gets the current temperature of    */
/*                         the switch.                                      */
/*                                                                          */
/*    Input(s)           : pi4CurrTemperature - Current switch Temperature  */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwGetCurrTemperature (INT4 *pi4CurrTemperature)
{
    *pi4CurrTemperature = ISS_SWITCH_DEFAULT_TEMPERATURE;

    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwGetCurrPowerSupply                          */
/*                                                                          */
/*    Description        : This function gets the current power supply      */
/*                         of the switch.                                   */
/*                                                                          */
/*    Input(s)           : pu4CurrPowerSupply - Current power supply        */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwGetCurrPowerSupply (UINT4 *pu4CurrPowerSupply)
{
    *pu4CurrPowerSupply = ISS_SWITCH_DEFAULT_POWER_SUPPLY;

    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwGetFanStatus                                */
/*                                                                          */
/*    Description        : This function gets the fan status of the switch. */
/*                                                                          */
/*    Input(s)           : u4FanIndex - Fan index.                          */
/*                         pu4FanStatus - Fan Status                        */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwGetFanStatus (UINT4 u4FanIndex, UINT4 *pu4FanStatus)
{
    UNUSED_PARAM (u4FanIndex);
    *pu4FanStatus = ISS_FAN_UP;

    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssHwInitMirrDataBase                                */
/*                                                                           */
/* Description        : This function is called at ISS module intialization. */
/*                      It initializes default Mirroring session parameters. */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - On success                             */
/*                      ISS_FAILURE - On failure                             */
/*****************************************************************************/
INT4
IssHwInitMirrDataBase ()
{
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwSetMirroring                                */
/*                                                                          */
/*    Description        : This API will be called by Mirroring interface   */
/*                         to update hardware for various mirroring options */
/*                         The information passed in the Mirroring Info as  */
/*                         argument includes type of mirroring to be enabled*/
/*                         in the hardware, based on this other arguments   */
/*                         will be used to enable mirroring for various     */
/*                         source entities                                  */
/*                                                                          */
/*    Input(s)           : IssHwMirrorInfo                                  */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwSetMirroring (tIssHwMirrorInfo * pMirrorInfo)
{
    UNUSED_PARAM (pMirrorInfo);

    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwMirrorAddRemovePort                          */
/*                                                                          */
/*    Description        : This API will be called by when a port is added  */
/*                         or removed from a portchannel interface.         */
/*                                                                          */
/*    Input(s)           : u4SrcIndex - Src Port added/removed from LAGG    */
/*                         u4DestIndex - Destination port to Mirror         */
/*                         u1Mode      - Ingress/Egress/Both                */
/*                         u1MirrCfg   - Mirror ADD/REMOVE                  */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwMirrorAddRemovePort (UINT4 u4SrcIndex, UINT4 u4DestIndex, UINT1 u1Mode,
                          UINT1 u1MirrCfg)
{
    UNUSED_PARAM (u4SrcIndex);
    UNUSED_PARAM (u4DestIndex);
    UNUSED_PARAM (u1Mode);
    UNUSED_PARAM (u1MirrCfg);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : IssHwSetMacLearningRateLimit                     */
/*                                                                           */
/*    Description         : This functions sets the learn limit in NP        */
/*                                                                           */
/*    Input(s)            :                                                  */
/*                                                                           */
/*    Output(s)           :                                                  */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
IssHwSetMacLearningRateLimit (INT4 i4IssMacLearnLimitVal)
{
    UNUSED_PARAM (i4IssMacLearnLimitVal);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : IssHwGetLearnedMacAddrCount                      */
/*                                                                           */
/*    Description         : This functions gets the Dynamic Unicast MAC addr */
/*                        : of the switch                                    */
/*                                                                           */
/*    Input(s)            :                                                  */
/*                                                                           */
/*    Output(s)           :                                                  */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
IssHwGetLearnedMacAddrCount (INT4 *pi4LearnedMacAddrCount)
{
    *pi4LearnedMacAddrCount = ISS_ZERO_ENTRY;
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwUpdateUserDefinedFilter                     */
/*                                                                          */
/*    Description        : This function is used to update the hardware UDB  */
/*                         filter.                                          */
/*                                                                          */
/*    Input(s)           :                                                  */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
INT4
IssHwUpdateUserDefinedFilter (tIssUDBFilterEntry * pAccessFilterEntry,
                              tIssL2FilterEntry * pL2AccessFilterEntry,
                              tIssL3FilterEntry * pL3AccessFilterEntry,
                              INT4 i4FilterAction)
{
    UNUSED_PARAM (pAccessFilterEntry);
    UNUSED_PARAM (pL2AccessFilterEntry);
    UNUSED_PARAM (pL3AccessFilterEntry);
    UNUSED_PARAM (i4FilterAction);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwSetPortAutoNegAdvtCapBits                   */
/*                                                                          */
/*    Description        : This function to set the AutoNegAdvtCapBit for   */
/*                          the given port.                                 */
/*                                                                          */
/*    Input(s)           : u4IfIndex - Port interface index.                */
/*                         *pi4MauCapBits , *pi4IssCapBits                  */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwSetPortAutoNegAdvtCapBits (UINT4 u4IfIndex, INT4 *pi4MauCapBits,
                                INT4 *pi4IssCapBits)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pi4MauCapBits);
    UNUSED_PARAM (pi4IssCapBits);

    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwGetPortAutoNegAdvtCapBits                   */
/*                                                                          */
/*    Description        : This function reads from hardware whether        */
/*                         AutoNegAdvtCap Bits  is set on the port          */
/*                                                                          */
/*                                                                          */
/*    Input(s)           : u4IfIndex - Port interface index.                */
/*                                                                          */
/*    Output(s)          : *pElemen -                                       */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE/i4RetVal                 */
/****************************************************************************/
INT4
IssHwGetPortAutoNegAdvtCapBits (UINT4 u4IfIndex, INT4 *pElement)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pElement);

    return FNP_SUCCESS;
}

/********************************************************************/
/*                                                                  */
/* Function Name : IssHwSetLearningMode()                           */
/*                                                                  */
/* Description   : This function removes mac entries from the port  */
/*                 and configures as CPU learning or HW learning.   */
/*                                                                  */
/* Input(s)      : u4IfIndex - Interface Index                      */
/*                 i4Status - CPU controlled learning status        */
/*                                                                  */
/* Output(s)     : None.                                            */
/*                                                                  */
/* Returns       : FNP_SUCCESS / FNP_FAILURE.                       */
/********************************************************************/

INT4
IssHwSetLearningMode (UINT4 u4IfIndex, INT4 i4Status)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4Status);

    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwSetPortMdiOrMdixCap                         */
/*                                                                          */
/*    Description        : This function sets the port as Auto/MDI/MDIX     */
/*                                                                          */
/*    Input(s)           : u4IfIndex - Port interface index.                */
/*                         i4Value - Port type (Auto/Mdi/Mdix)              */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwSetPortMdiOrMdixCap (UINT4 u4IfIndex, INT4 i4Value)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4Value);
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssHwGetPortMdiOrMdixCap                         */
/*                                                                          */
/*    Description        : This function is used to get the Port type       */
/*                         as Auto/MDI/MDIX.                                */
/*                                                                          */
/*    Input(s)           : UINT4 u4IfIndex                                  */
/*                                                                          */
/*    Output(s)          : INT4 *pi4PortStatus                              */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
INT4
IssHwGetPortMdiOrMdixCap (UINT4 u4IfIndex, INT4 *pi4PortStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pi4PortStatus);
    return FNP_SUCCESS;
}

/********************************************************************/
/* Function Name : IssHwSetPortFlowControlRate                      */
/*                                                                  */
/* Description   : This function configures the pause threshold and */
/*                 resume rate. Threshold rate above which pause    */
/*                 frames are transmitted and resume rate below which*/
/*                 pause transmission stops. If the feature is not  */
/*                 supported in the chipset then the ingress rate of*/
/*                 a particular port is configured above which pause*/
/*                 frames are transmitted.                          */
/*                                                                  */
/* Input(s)      : u4IfIndex - Interface Index                      */
/*                 i4PortMaxRate - threshold rate limit in Kbps     */
/*                 i4PortMinRate - resume rate limit in Kbps        */
/*                                                                  */
/* Output(s)     : None.                                            */
/*                                                                  */
/* Returns       : FNP_SUCCESS / FNP_FAILURE.                       */
/********************************************************************/

INT4
IssHwSetPortFlowControlRate (UINT4 u4IfIndex,
                             INT4 i4PortMaxRate, INT4 i4PortMinRate)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4PortMaxRate);
    UNUSED_PARAM (i4PortMinRate);
    return FNP_SUCCESS;
}

/******************************************************************/
/*  Function Name             : NpSetTrace                        */
/*  Description               : This fucntion sets trace level    */
/*                                                                */
/*  Input(s)                  : u1TraceLevel                      */
/*  Output(s)                 : None                              */
/*  Global Variables Referred : gu2TraceLevel                     */
/*  Global variables Modified : gu2TraceLevel                     */
/*  Exceptions                : None                              */
/*  Use of Recursion          : None                              */
/*  Returns                   : VOID                              */
/******************************************************************/
VOID
NpSetTrace (UINT1 u1TraceModule, UINT1 u1TraceLevel)
{
    UNUSED_PARAM (u1TraceLevel);
    UNUSED_PARAM (u1TraceModule);
    return;
}

/******************************************************************/
/*  Function Name             : NpGetTrace                        */
/*  Description               : This fucntion gets trace level    */
/*                                                                */
/*  Input(s)                  : pu1TraceLevel                     */
/*  Output(s)                 : None                              */
/*  Global Variables Referred : gu2TraceLevel                     */
/*  Global variables Modified : None                              */
/*  Exceptions                : None                              */
/*  Use of Recursion          : None                              */
/*  Returns                   : VOID                              */
/******************************************************************/
VOID
NpGetTrace (UINT1 u1TraceModule, UINT1 *pu1TraceLevel)
{
    UNUSED_PARAM (u1TraceModule);
    UNUSED_PARAM (pu1TraceLevel);
    return;
}
/******************************************************************/
/*  Function Name             : NpGiSetTraceLevel                 */
/*  Description               : This fucntion gets trace level    */
/*                                                                */
/*  Input(s)                  : u2TraceLevel                      */
/*  Output(s)                 : None                              */
/*  Global Variables Referred : gu2TraceLevel                     */
/*  Global variables Modified : None                              */
/*  Exceptions                : None                              */
/*  Use of Recursion          : None                              */
/*  Returns                   : VOID                              */
/******************************************************************/
VOID
NpSetTraceLevel (UINT2 u2TraceLevel)
{
    UNUSED_PARAM (u2TraceLevel);
    return;
}

/******************************************************************/
/*  Function Name             : NpGetTraceLevel                   */
/*  Description               : This fucntion gets trace level    */
/*                                                                */
/*  Input(s)                  : pu2TraceLevel                     */
/*  Output(s)                 : None                              */
/*  Global Variables Referred : gu2TraceLevel                     */
/*  Global variables Modified : None                              */
/*  Exceptions                : None                              */
/*  Use of Recursion          : None                              */
/*  Returns                   : VOID                              */
/******************************************************************/
VOID
NpGetTraceLevel (UINT2 *pu2TraceLevel)
{
    UNUSED_PARAM (pu2TraceLevel);
    return;
}

/******************************************************************/
/*  Function Name             : IssNpapiShowDebugging             */
/*  Description               : This fucntion displays the        */
/*                              npapi debugging list              */
/*                                                                */
/*  Input(s)                  : CliHandle                         */
/*  Output(s)                 : None                              */
/*  Global Variables Referred : None                              */
/*  Global variables Modified : None                              */
/*  Exceptions                : None                              */
/*  Use of Recursion          : None                              */
/*  Returns                   : VOID                              */
/******************************************************************/
VOID
IssNpapiShowDebugging (tCliHandle CliHandle)
{

    UNUSED_PARAM (CliHandle);
    return;
}

/*****************************************************************************
 *    Function Name             : IssHwSetSetTrafficSeperationCtrl
 *    Description               : This function for set Traffic seperation based on
 *                                control status.
 *    Input(s)                  : Traffic Seperation status
 *    Output(s)                 : None.
 *    Global Variables Referred : None
 *    Global Variables Modified : None
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *    Use of Recursion          : None.
 *    Returns                   : FNP_SUCCESS/FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
IssHwSetSetTrafficSeperationCtrl (INT4 i4CtrlStatus)
{
    UNUSED_PARAM (i4CtrlStatus);
    return FNP_SUCCESS;
}
/******************************************************************************
 *
 *    Function Name   : IssNpHwGetCapabilities
 *
 *    Description     : This function is used to retrieve the Hardware Supported
 *                      informations.
 *
 *    Input(s)        : pHwGetCapabilities
 *
 *    Output(s)       : None
 *
 *
 *    Returns         : None
 *
 *****************************************************************************/
VOID
IssNpHwGetCapabilities (tNpIssHwGetCapabilities * pHwGetCapabilities)
{
    /* Kernel Interface is not supported  */
    pHwGetCapabilities->u1HwKnetIntf = FNP_FALSE;
    /* Creation of LAG between ports that are operating in different speeds */
    pHwGetCapabilities->u1HwLaWithDiffPortSpeed = FNP_TRUE;
    /* Keeping a port as untagged in more than one VLAN */
    pHwGetCapabilities->u1HwUntaggedPortsForVlans = FNP_TRUE;
    /* Support for Unicase mac learing limit */
    pHwGetCapabilities->u1HwUnicastMacLearningLimit = FNP_TRUE;
    /* Support for Queue configurations for aggregated ports */
    pHwGetCapabilities->u1HwQueueConfigOnLaPort = FNP_TRUE;
    /* Support for creation of port channel for all ports */
    pHwGetCapabilities->u1HwLagOnAllPorts  = FNP_TRUE;
    /* Support for LAG on CEP ports   */
    pHwGetCapabilities->u1HwLagOnCep = FNP_TRUE;
    /* Support for Self MAC in MAC address table   */
    pHwGetCapabilities->u1HwSelfMacInMacTable = FNP_FALSE;
    /* Support for More than one scheduler(Inaddition to Default schedular)*/
    pHwGetCapabilities->u1HwMoreScheduler = FNP_TRUE;
    /* HighCapacity counter supported */
    pHwGetCapabilities->u1HwHighCapacityCntr = FNP_FALSE;
    /* Support for Setting TPID configurations For customer Ports */
    pHwGetCapabilities->u1HwCustomerTpidAllow = FNP_TRUE;
    /* Shape template param EIR is supported */
    pHwGetCapabilities->u1ShapeParamEIR = FNP_TRUE;
    /* Shape template param EBS is supported */
    pHwGetCapabilities->u1ShapeParamEBS = FNP_TRUE;
    /* Meter Color Blind Mode supported */
    pHwGetCapabilities->u1HwMeterColorBlind = FNP_TRUE;
    /* Untagged CEP and untagged PEP as FALSE is supported */
    pHwGetCapabilities->u1HwUntagCepPep = FNP_TRUE;

    return;
}
/*****************************************************************************/
/*    Function Name       : IssHwSetSwitchModeType                           */
/*                                                                           */
/*    Description         : This functions sets the mode to Cutthrough or    */
/*                          store-forward                                    */
/*                                                                           */
/*    Input(s)           :  i4SwitchModeType  - SwitchMode to be configured  */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            :    FNP_SUCCESS/FNP_FAILURE                        */
/*****************************************************************************/
INT4
IssHwSetSwitchModeType (INT4 i4SwitchModeType)
{
    UNUSED_PARAM (i4SwitchModeType);
    return FNP_SUCCESS;
}
/****************************************************************************/
  /*                                                                          */
  /*    Function Name      : IssHwSetCpuMirroring                             */
  /*                                                                          */
  /*    Description        : This function is used to configure mirroring     */
  /*                         parameters for CPU ingress packets.              */
  /*                                                                          */
  /*    Input(s)           : u4MirrorToPort -  Destination port for mirroring.*/
  /*                         i4MirrorType - Ingress/Egress/Both/Disabled      */
  /*    Output(s)                 : None.                                     */
  /*    Global Variables Referred : gu4IssCpuMirrorToPort                     */
  /*                                gi4IssCpuMirrorType                       */
  /*    Global Variables Modified : None.                                     */
  /*    Exceptions or Operating                                               */
  /*    System Error Handling     : None.                                     */
  /*    Use of Recursion          : None.                                     */
  /*    Returns                   : FNP_SUCCESS/FNP_FAILURE                   */
  /*                                                                          */
  /*****************************************************************************/
  PUBLIC INT4
  IssHwSetCpuMirroring (INT4 i4MirrorType ,UINT4 u4MirrorToPort)
  {
      UNUSED_PARAM (i4MirrorType);
      UNUSED_PARAM (u4MirrorToPort);
      return FNP_SUCCESS;
  }

    /* END OF FILE */
