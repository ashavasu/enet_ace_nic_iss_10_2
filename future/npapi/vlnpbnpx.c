/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnpbnpx.c,v 1.5 2012/07/26 09:42:25 siva Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *              done here
 *******************************************************************/
#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "npapi.h"
#include "npvlnpb.h"

/*****************************************************************************/
/* Function Name      : FsVlanMbsmHwProviderBridgePortType.                  */
/*                                                                           */
/* Description        : This function is called when the port type is        */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u4PortType - CUSTOMER_NETWORK_PORT/CUSTOMER_EDGE_PORT*/
/*                      PROVIDER_NETWORK_PORT                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanMbsmHwSetProviderBridgePortType (UINT4 u4IfIndex, UINT4 u4PortType,
                                       tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PortType);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanMbsmHwSetPortIngressEtherType.                 */
/*                                                                           */
/* Description        : This function is called when the ingress ether type  */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u2EtherType - Configured Ingress Ether type can 
 *                      take values ranges <1-65536>                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanMbsmHwSetPortIngressEtherType (UINT4 u4IfIndex, UINT2 u2EtherType,
                                     tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2EtherType);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanMbsmHwSetPortEgressEtherType.                  */
/*                                                                           */
/* Description        : This function is called when the Egress ether type   */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u2EtherType - Configured Egress Ether type can take
 *                      value ranges <1-65535>                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanMbsmHwSetPortEgressEtherType (UINT4 u4IfIndex, UINT2 u2EtherType,
                                    tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2EtherType);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanMbsmHwSetPortSVlanTranslationStatus.           */
/*                                                                           */
/* Description        : This function is called when the SVLAN Translation   */
/*                      status onfigured for a port.                         */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanMbsmHwSetPortSVlanTranslationStatus (UINT4 u4IfIndex, UINT1 u1Status,
                                           tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanMbsmHwAddSVlanTranslationEntry.                */
/*                                                                           */
/* Description        : This function is called when an entry is configured  */
/*                      for an VID translation table                         */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u2LocalSVlan - SVlan received in the packet can take 
 *                                                                   <1-4094>*/
/*                      u2RelaySVlan - SVlan used in the bridge can 
 *                      take <1-4094>                                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanMbsmHwAddSVlanTranslationEntry (UINT4 u4IfIndex, UINT2 u2LocalSVlan,
                                      UINT2 u2RelaySVlan,
                                      tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalSVlan);
    UNUSED_PARAM (u2RelaySVlan);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanMbsmHwSetPortEtherTypeSwapStatus.              */
/*                                                                           */
/* Description        : This function is called when the SVLAN Ether type    */
/*                      status onfigured for a port.                         */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanMbsmHwSetPortEtherTypeSwapStatus (UINT4 u4IfIndex, UINT1 u1Status,
                                        tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanMbsmHwAddEtherTypeSwapEntry.                   */
/*                                                                           */
/* Description        : This function is called when an entry is added       */
/*                      to the ether type swap table which is applied in     */
/*                      ingress of a packet.                                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u2LocalEtherType- Ether type received in the packet
 *                      can take <1-65535>                                   */
/*                      u2RelayEtherType- Ether type understand by the bridge
 *                      can take <1-65535>                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanMbsmHwAddEtherTypeSwapEntry (UINT4 u4IfIndex, UINT2 u2LocalEtherType,
                                   UINT2 u2RelayEtherType,
                                   tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalEtherType);
    UNUSED_PARAM (u2RelayEtherType);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanMbsmHwAddSVlanMap.                             */
/*                                                                           */
/* Description        : This function is called when an entry is added for   */
/*                      Port in the SVLAN classification tables which is     */
/*                      assigned for that port                               */
/*                                                                           */
/* Input(s)           : VlanSVlanMap-Structure containing info for the       */
/*                      Configured SVlan classification entry.               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanMbsmHwAddSVlanMap (tVlanSVlanMap VlanSVlanMap, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (VlanSVlanMap);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanMbsmHwSetPortSVlanTableType.                   */
/*                                                                           */
/* Description        : This function is called when a port is configured    */
/*                      for a type of SVLAN classification tables which is   */
/*                      used in assigning SVLAN for untagged packets         */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index                               */
/*                      u1TableType - Type of table assigned for that port 
 *                      can take values from the tVlanSVlanTableType enum    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanMbsmHwSetPortSVlanClassifyMethod (UINT4 u4IfIndex, UINT1 u1TableType,
                                        tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1TableType);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}



/*****************************************************************************/
/* Function Name      : FsVlanMbsmHwPortMacLearningLimit.                    */
/*                                                                           */
/* Description        : This function is called for configuring the Unicast  */
/*                      mac limit for a Port                                 */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Port identifier.                        */
/*                      u4MacLimit - Mac Limit of the Unicast Table can take
 *                      <0-4294967295>                                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanMbsmHwPortMacLearningLimit (UINT4 u4IfIndex, UINT4 u4MacLimit,
                                  tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4MacLimit);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanMbsmHwSetPortCustomerVlan.                     */
/*                                                                           */
/* Description        : This function is called for setting the port         */
/*                      configured customer vlan                             */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                      CVlanId - Configured Customer VLAN ID can take 
 *                      <1-4094>                                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsVlanMbsmHwSetPortCustomerVlan (UINT4 u4IfIndex, tVlanId CVlanId,
                                 tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (CVlanId);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanMbsmHwCreateProviderEdgePort                   */
/*                                                                           */
/* Description        : This function creates a Provider Edge Port (logical  */
/*                      port) in hardware.                                   */
/*                                                                           */
/* Input(s)           : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId   - Service Vlan Id can take <1-4094>.       */
/*                      PepConfig - Contains values to be used for PVID,     */
/*                                  Default user priority, Acceptable        */
/*                                  frame types, enable ingress filtering    */
/*                                  on port creation.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanMbsmHwCreateProviderEdgePort (UINT4 u4IfIndex, tVlanId SVlanId,
                                    tHwVlanPbPepInfo PepConfig,
                                    tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (PepConfig);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanMbsmHwSetPcpEncodTbl.                          */
/*                                                                           */
/* Description        : This function sets/modifies PCP encoding table entry */
/*                      for the given port, given PCP selection row, given   */
/*                      priority, given drop_eligible value.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      NpPbVlanPcpInfo - Entry containing the following     */
/*                                        fields:                            */
/*                          u2PcpSelRow - PCP selection row where            */
/*                                        modification will be done can take */
/*                                        VLAN_8P0D_SEL_ROW,                 */
/*                                        VLAN_7P1D_SEL_ROW,                 */
/*                                        VLAN_6P2D_SEL_ROW,                 */
/*                                        VLAN_5P3D_SEL_ROW.                 */
/*                          u2Priority  - Priority value can take <0-7>      */
/*                          u1DropEligible - Drop Eligible value can         */
/*                                              take VLAN_SNMP_TRUE,         */
/*                                              VLAN_SNMP_FALSE.             */
/*                          u2PcpVlaue  - PCP value to be set for the above  */
/*                                        input can take <0-7>.              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanMbsmHwSetPcpEncodTbl (UINT4 u4IfIndex, tHwVlanPbPcpInfo NpPbVlanPcpInfo,
                            tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (NpPbVlanPcpInfo);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanMbsmHwSetPcpDecodTbl.                          */
/*                                                                           */
/* Description        : This function sets/modifies PCP decoding table entry */
/*                      for the given port, given PCP selection row, given   */
/*                      PCP value.                                           */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      PNpPbVlanPcpInfo - Entry containing the following    */
/*                                                                   fields: */
/*                          u2PcpSelRow - PCP selection row where            */
/*                                        modification will be done can take */
/*                                        VLAN_8P0D_SEL_ROW,                 */
/*                                        VLAN_7P1D_SEL_ROW,                 */
/*                                        VLAN_6P2D_SEL_ROW,                 */
/*                                        VLAN_5P3D_SEL_ROW.                 */
/*                          u2PcpVlaue  - PCP value can take <0-7>           */
/*                          u2Priority  - Priority value to be set for the   */
/*                                        about input (port, PcpSel, pcp).   */
/*                          u1DropEligible - Drop Eligible value to be set   */
/*                                           for the above input             */
/*                                           (port, PcpSel, Pcp).can take    */
/*                                           VLAN_SNMP_TRUE,VLAN_SNMP_FALSE  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanMbsmHwSetPcpDecodTbl (UINT4 u4IfIndex, tHwVlanPbPcpInfo NpPbVlanPcpInfo,
                            tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (NpPbVlanPcpInfo);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/* Use-DEI parameter. */
/*****************************************************************************/
/* Function Name      : FsVlanMbsmHwSetPortUseDei.                           */
/*                                                                           */
/* Description        : This function sets the Use_DEI parameter for the     */
/*                      given port.                                          */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u1UseDei  - True/False for Use_DEI parameter for     */
/*                                  this port. can take VLAN_SNMP_TRUE,
 *                                  VLAN_SNMP_FALSE                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanMbsmHwSetPortUseDei (UINT4 u4IfIndex, UINT1 u1UseDei,
                           tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1UseDei);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/* Require Drop Encoding parameter. */
/*****************************************************************************/
/* Function Name      : FsVlanMbsmHwSetPortReqDropEncoding.                  */
/*                                                                           */
/* Description        : This function sets the Require Drop Encoding         */
/*                      parameter for the given port.                        */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u1ReqDropEncoding  - True/False value for Require    */
/*                                           Drop encoding parameter for     */
/*                                           this port.can take 
 *                                           VALN_SNMP_TRUE,VLAN_SNMP_FALSE  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanMbsmHwSetPortReqDropEncoding (UINT4 u4IfIndex, UINT1 u1ReqDrpEncoding,
                                    tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1ReqDrpEncoding);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/* Port Priority Code Point Selection Row. */
/*****************************************************************************/
/* Function Name      : FsVlanMbsmHwSetPortPcpSelRow.                        */
/*                                                                           */
/* Description        : This function sets the Port Priority Code Point      */
/*                      Selection for the given port.                        */
/*                                                                           */
/* Input(s)           : u4IfIndex       - Port Index.                        */
/*                      u2PcpSelection  - It can take the following values:  */
/*                                        VLAN_8P0D_SEL_ROW,                 */
/*                                        VLAN_7P1D_SEL_ROW,                 */
/*                                        VLAN_6P2D_SEL_ROW,                 */
/*                                        VLAN_5P3D_SEL_ROW.                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanMbsmHwSetPortPcpSelection (UINT4 u4IfIndex, UINT2 u2PcpSelection,
                                 tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2PcpSelection);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/* Internal CNP - Service Priority Regeneration Table NPAPIs */
/*****************************************************************************/
/* Function Name      : FsVlanMbsmHwSetServicePriRegenEntry                  */
/*                                                                           */
/* Description        : This function sets the service priority              */
/*                      regeneration table entry.                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Customer Edge Port Index.                */
/*                      SVlanId    - Service Vlan Id.                        */
/*                      i4RecvPriority - Receive Priority can take <0-7>     */
/*                      i4RegenPriority - Regenerated Priority can take <0-7>*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanMbsmHwSetServicePriRegenEntry (UINT4 u4IfIndex, tVlanId SVlanId,
                                     INT4 i4RecvPriority, INT4 i4RegenPriority,
                                     tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (i4RecvPriority);
    UNUSED_PARAM (i4RegenPriority);
    UNUSED_PARAM (pSlotInfo);
    return FNP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FsVlanHwMulticastMacTableLimit                       */
/*                                                                           */
/* Description        : This function is called for configuring the Multicast*/
/*                      mac limit  for the bridge                            */
/*                                                                           */
/* Input(s)           : u4MacLimit - Mac Limit of the Multicast Table        */
/*                      pSlotInfo  - Slot Information                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsVlanMbsmHwMulticastMacTableLimit (UINT4 u4MacLimit, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4MacLimit);
    UNUSED_PARAM (pSlotInfo);

    return FNP_SUCCESS;
}
