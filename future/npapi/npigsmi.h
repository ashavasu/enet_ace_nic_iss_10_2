/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: npigsmi.h,v 1.2 2007/02/01 14:59:31 iss Exp $
 *
 * Description: Prototypes for IGMP Snooping module's NP-API.
 *
 *******************************************************************/

#ifndef _NPIGSMI_H
#define _NPIGSMI_H
#include "igsminp.h"

#endif
