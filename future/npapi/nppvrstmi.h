/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: nppvrstmi.h,v 1.2 2008/01/07 14:42:27 iss Exp $ 
 *
 * Description: All prototypes for Network Processor API functions for PVRST 
 * 
 *
 *******************************************************************/
#ifndef _NPPVRSTMI_H
#define _NPPVRSTMI_H

#include "pvrstminp.h"

INT4 FsMiPvrstCreateFilter (VOID);
INT4 FsMiPvrstHwCreateFilter (UINT1 *pFilterAddr, UINT4 u4PktType);
INT4 FsMiPvrstDestroyFilter (VOID);
INT4 FsMiPvrstHwDestroyFilter (UINT4 u4PktType);
INT4 FsMiPvrstCreateFP (VOID);
INT4 FsMiPvrstDestroyFP (VOID);
INT4 FsMiPvrstHwCreateFP (UINT1 *pFilterAddr, UINT4 u4PktType);
INT4 FsMiPvrstHwDestroyFP (UINT4 u4PktType);


#endif /* _NPPVRSTMI_H */

