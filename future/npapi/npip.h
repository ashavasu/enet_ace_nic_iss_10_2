/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: npip.h,v 1.3 2007/02/01 14:59:31 iss Exp $
 *
 * Description: Exported file for IP NP-API.
 *
 *******************************************************************/
#ifndef _NPIP_H
#define _NPIP_H

#include "ipnp.h"

/* Mapping the following macros to the constants given by Broadcom.
 * This may vary depends on the target platform. The caller should call
 * with the constants defined here for a particular stats.
 */

#endif
