
/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: utf8tstcli.h,v 1.1.1.1 2015/04/28 12:27:59 siva Exp $
 **
 ** Description: UTF8  UT Test cases.
 ** NOTE: This file should not be included in release packaging.
 ** ***********************************************************************/
#include "lr.h"
#include "cli.h"
#include "radcli.h"
#include "radcom.h"

INT4 cli_process_utf8_test_cmd (tCliHandle, UINT4,...);
