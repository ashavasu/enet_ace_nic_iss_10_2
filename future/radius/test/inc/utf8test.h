/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: utf8test.h,v 1.1.1.1 2015/04/28 12:27:59 siva Exp $
 **
 ** Description: UTF8 UT Test cases.
 ** NOTE: This file should not be included in release packaging.
 ** ***********************************************************************/

#ifndef _UTF8TEST_H_
#define _UTF8TEST_H_
#include "radcom.h"

VOID Utf8ExecuteUtCase(UINT4 , UINT4 );
VOID Utf8ExecuteUtAll(VOID);
VOID Utf8ExecuteUtFile(UINT4);

typedef struct
{
    UINT1     u1File;
    UINT1     u1Case;
}tTestCase;

#define FILE_COUNT     1

tTestCase gTestCase[] = {
    {1,20} /* utf8api.c */
};

/* file1 UT cases */
INT4 Utf8UtApi1_1 (VOID);
INT4 Utf8UtApi1_2 (VOID);
INT4 Utf8UtApi1_3 (VOID);
INT4 Utf8UtApi1_4 (VOID);
INT4 Utf8UtApi1_5 (VOID);
INT4 Utf8UtApi1_6 (VOID);
INT4 Utf8UtApi1_7 (VOID);
INT4 Utf8UtApi1_8 (VOID);
INT4 Utf8UtApi1_9 (VOID);
INT4 Utf8UtApi1_10 (VOID);
INT4 Utf8UtApi1_11 (VOID);
INT4 Utf8UtApi1_12 (VOID);
INT4 Utf8UtApi1_13 (VOID);
INT4 Utf8UtApi1_14 (VOID);
INT4 Utf8UtApi1_15 (VOID);
INT4 Utf8UtApi1_16 (VOID);
INT4 Utf8UtApi1_17 (VOID);
INT4 Utf8UtApi1_18 (VOID);
INT4 Utf8UtApi1_19 (VOID);
INT4 Utf8UtApi1_20 (VOID);
#endif /* _UTF8TEST_H_ */ 
