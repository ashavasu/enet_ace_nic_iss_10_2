##########################################################################
# Copyright (C) Future Software Limited,2010			         #
#                                                                        #
# $Id: make.h,v 1.1 2015/04/28 12:28:38 siva Exp $                     #
#								         #
# Description : Contains information fro creating the make file          #
#		for this utf8ule      				         #
#								         #
##########################################################################

#include the make.h and make.rule from LR
include ../../LR/make.h
include ../../LR/make.rule

#Compilation switches
COMP_SWITCHES = $(GENERAL_COMPILATION_SWITCHES)\
                $(SYSTEM_COMPILATION_SWITCHES)

#project directories
UTF8_INCL_DIR           = $(BASE_DIR)/radius/inc

UTF8_TEST_BASE_DIR  = ${BASE_DIR}/radius/test
UTF8_TEST_SRC_DIR   = ${UTF8_TEST_BASE_DIR}/src
UTF8_TEST_INC_DIR   = ${UTF8_TEST_BASE_DIR}/inc
UTF8_TEST_OBJ_DIR   = ${UTF8_TEST_BASE_DIR}/obj

UTF8_TEST_INCLUDES  = -I$(UTF8_TEST_INC_DIR) \
                     -I$(UTF8_INCL_DIR) \
                     $(COMMON_INCLUDE_DIRS)


##########################################################################
#                    End of file make.h					 #
##########################################################################
