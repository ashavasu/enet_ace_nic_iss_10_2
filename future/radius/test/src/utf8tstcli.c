/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: utf8tstcli.c,v 1.1.1.1 2015/04/28 12:28:08 siva Exp $
 **
 ** Description: UTF8 UT Test cases cli file.
 ** NOTE: This file should not be include in release packaging.
 ** ***********************************************************************/
#include "utf8tstcli.h"

#define MAX_ARGS 3

extern VOID Utf8ExecuteUtCase (UINT4 u4FileNumber, UINT4 u4TestNumber);
extern VOID Utf8ExecuteUtAll (VOID);
extern VOID Utf8ExecuteUtFile (UINT4 u4File);

/*  Function is called from utf8tstcmd.def file */

INT4
cli_process_utf8_test_cmd (tCliHandle CliHandle, UINT4 u4Command,...)
{
    va_list             ap;
    UINT4              *args[MAX_ARGS];
    INT4                i4Inst = 0;
    INT1                argno = 0;

    CliRegisterLock (CliHandle, RadiusLock, RadiusUnLock);
    RADIUS_LOCK();

    va_start (ap, u4Command);
    UNUSED_PARAM (CliHandle);
    i4Inst = va_arg (ap, INT4);

    /* Walk through the arguments and store in args array.
     */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == MAX_ARGS)
            break;
    }

    va_end (ap);

    if (args[0] == NULL)
    {
        /* specified case on specified file */
        if (args[2] != NULL)
        {
            Utf8ExecuteUtCase (*(UINT4 *) (args[1]), *(UINT4 *) (args[2]));
        }
        /* all cases on specified file */
        else
        {
            Utf8ExecuteUtFile (*(UINT4 *) (args[1]));
        }
    }
    else
    {
        /* all cases in all the files */
        Utf8ExecuteUtAll ();
    }

    CliUnRegisterLock (CliHandle);
    RADIUS_UNLOCK();
    return CLI_SUCCESS;
}
