/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: utf8test.c,v 1.1.1.1 2015/04/28 12:28:08 siva Exp $
 **
 ** Description: UTF8  UT Test cases file.
 ** NOTE: This file should not be include in release packaging.
 ** ***********************************************************************/
#include "utf8test.h"

VOID
Utf8ExecuteUtCase (UINT4 u4FileNumber, UINT4 u4TestNumber)
{
    INT4 i4RetVal = -1;

    switch (u4FileNumber)
    {
        /* utf8api.c */
        case 1:
            switch (u4TestNumber)
            {
                case 1:
                   i4RetVal = Utf8UtApi1_1 ();
                   break;
                case 2:
                   i4RetVal = Utf8UtApi1_2 ();
                   break;
                case 3:
                   i4RetVal = Utf8UtApi1_3 ();
                   break;
                case 4:
                   i4RetVal = Utf8UtApi1_4 ();
                   break;
                case 5:
                   i4RetVal = Utf8UtApi1_5 ();
                   break;
                case 6:
                   i4RetVal = Utf8UtApi1_6 ();
                   break;
                case 7:
                   i4RetVal = Utf8UtApi1_7 ();
                   break;
                case 8:
                   i4RetVal = Utf8UtApi1_8 ();
                   break;
                case 9:
                   i4RetVal = Utf8UtApi1_9 ();
                   break;
                case 10:
                   i4RetVal = Utf8UtApi1_10 ();
                   break;
                case 11:
                   i4RetVal = Utf8UtApi1_11 ();
                   break;
                case 12:
                   i4RetVal = Utf8UtApi1_12 ();
                   break;
                case 13:
                   i4RetVal = Utf8UtApi1_13 ();
                   break;
                case 14:
                   i4RetVal = Utf8UtApi1_14 ();
                   break;
                case 15:
                   i4RetVal = Utf8UtApi1_15 ();
                   break;
                case 16:
                   i4RetVal = Utf8UtApi1_16 ();
                   break;
                case 17:
                   i4RetVal = Utf8UtApi1_17 ();
                   break;
                case 18:
                   i4RetVal = Utf8UtApi1_18 ();
                   break;
                case 19:
                   i4RetVal = Utf8UtApi1_19 ();
                   break;
                case 20:
                   i4RetVal = Utf8UtApi1_20 ();
                   break;
                default:
                    printf ("Invalid Test for file utf8api.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of utf8api.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of utf8api.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
    }
    return;
}

VOID
Utf8ExecuteUtAll (VOID)
{
    UINT1 u1File = 0;

    for (u1File = 1; u1File <= FILE_COUNT; u1File++)
    {
        Utf8ExecuteUtFile (u1File);
    }
}

VOID
Utf8ExecuteUtFile (UINT4 u4File)
{
    UINT1               u1Case = 0;

    for (u1Case = 1; u1Case <= gTestCase[u4File - 1].u1Case; u1Case++)
    {
        Utf8ExecuteUtCase (u4File, u1Case);
    }
}


/* utf8api UT cases */
INT4 
Utf8UtApi1_1 (VOID)
{ 
    UINT4   a_u1Utf8UserName[LEN_UTF8_USER_NAME] = {0}; 
    UINT1   a_u1TempArray[LEN_UTF8_USER_NAME] = {0};
    UINT4   u4_Index = 0;

    /* TestCase Aim: To Test UTF-8 Encoder Properly encodes the 
     * multilingual pattern or not */
    
    /* Fill the multilingual pattern for sending as a input */
    a_u1Utf8UserName[0] = 0x221e;
    a_u1Utf8UserName[1] = L'\0';

    /* Encode mulilingual byte sequences into UTF-8 */
    if(UnicodeToUtf8
    (a_u1Utf8UserName,1, a_u1TempArray,LEN_UTF8_USER_NAME) != UTF8_SUCCESS)
    {
        printf(" Error in UTF-8 encoding.\n");
        return OSIX_FAILURE;
    }

    /* Compare the output with the valid UTF8 pattern */
    for(u4_Index = 0;
    (a_u1TempArray[u4_Index] != L'\0')&&
    (u4_Index <= LEN_UTF8_USER_NAME);
     u4_Index++)
    {
       if(u4_Index == 0x0)
       {
           if(a_u1TempArray[u4_Index] != 0xe2)
           {
               printf("Encoder output is wrong.\n");
               return OSIX_FAILURE;
           }
       }
       else if(u4_Index == 0x1)
       { 
           if(a_u1TempArray[u4_Index] != 0x88)
           {
               printf("Encoder output is wrong.\n");
               return OSIX_FAILURE;
           }
       }
       else if(u4_Index == 0x2)
       {
           if(a_u1TempArray[u4_Index] != 0x9e)
           {
               printf("Encoder output is wrong.\n");
               return OSIX_FAILURE;
           }
       }
       else
       {
           printf("Encoder output is wrong.\n");
           return OSIX_FAILURE;
       }
    }
    return OSIX_SUCCESS;
}
INT4 
Utf8UtApi1_2 (VOID)
{
    UINT4   a_u1Utf8UserName[LEN_UTF8_USER_NAME] = {0}; 
    UINT1   a_u1TempArray[LEN_UTF8_USER_NAME] = {0};
    UINT4   u4_Index = 0;

    /* TestCase Aim: To Test UTF-8 Decoder Properly 
     * Decodes the Bytes or not */
    
    /* Fill the UTF-8 pattern for sending as a input */      
    a_u1TempArray[0] = 0xe2;
    a_u1TempArray[1] = 0x88;
    a_u1TempArray[2] = 0x9e;
    a_u1TempArray[3] = L'\0';

    /* Decode UTF-8 encoded string into mulilingual byte sequences */
    if(Utf8ToUnicode
    (a_u1TempArray,3, a_u1Utf8UserName,LEN_UTF8_USER_NAME) != UTF8_SUCCESS)
    {
        printf(" Error in UTF-8 encoding.\n");
        return OSIX_FAILURE;
    }

    /* Compare the output with the valid UTF8 pattern */
    for(u4_Index = 0;
    (a_u1Utf8UserName[u4_Index] != L'\0')&&
    (u4_Index <= LEN_UTF8_USER_NAME);
     u4_Index++)
    {
       if(u4_Index == 0x0)
       {
           if(a_u1Utf8UserName[u4_Index] != 0x221e)
           {
               printf("Encoder output is wrong.\n");
               return OSIX_FAILURE;
           }
       }
       else
       {
           printf("Decoder output is wrong.\n");
           return OSIX_FAILURE;
       }
    }
    return OSIX_SUCCESS;
}
INT4 
Utf8UtApi1_3 (VOID)
{
    UINT4   *a_u1Utf8UserName = NULL;
    UINT1   a_u1TempArray[LEN_UTF8_USER_NAME] = {0};
    
    /* TestCaseAim: To Test Encoder's NULL checking condition */
 
    /* Passing the NULL args */
    if(UnicodeToUtf8
    (a_u1Utf8UserName, 0,a_u1TempArray,LEN_UTF8_USER_NAME) == UTF8_SUCCESS)
    {
	printf("NULL checking is not done.\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
INT4 
Utf8UtApi1_4 (VOID)
{
    UINT4   *a_u1Utf8UserName = NULL;
    UINT1   a_u1TempArray[LEN_UTF8_USER_NAME] = {0};
    
    /* TestCaseAim: To Test Decoder's NULL checking condition */ 
    
    /* Passing the NULL args */
    if(Utf8ToUnicode
    (a_u1TempArray,0, a_u1Utf8UserName,LEN_UTF8_USER_NAME) == UTF8_SUCCESS)
    {
        printf("NULL checking is not done.\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4 
Utf8UtApi1_5 (VOID)
{
    UINT4   a_u1Utf8UserName[LEN_UTF8_USER_NAME] = {0};
    UINT1   a_u1TempArray[LEN_UTF8_USER_NAME] = {0};

    /* TestCaseAim: To Test Encoder's <0x80 checking condition */
    a_u1Utf8UserName[0] = 0x7f;
    a_u1Utf8UserName[1] = L'\0';

    if(UnicodeToUtf8
    (a_u1Utf8UserName, 1,a_u1TempArray,LEN_UTF8_USER_NAME) != UTF8_SUCCESS)
    {
        printf("<0x80 Condtion is not properly written.\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
INT4 
Utf8UtApi1_6 (VOID)
{
    UINT4   a_u1Utf8UserName[LEN_UTF8_USER_NAME] = {0};
    UINT1   a_u1TempArray[LEN_UTF8_USER_NAME] = {0};

    /* TestCaseAim: To Test Encoder's <0x800 checking condition */
    a_u1Utf8UserName[0] = 0x7ff;
    a_u1Utf8UserName[1] = L'\0';

    if(UnicodeToUtf8
    (a_u1Utf8UserName, 1,a_u1TempArray,LEN_UTF8_USER_NAME) != UTF8_SUCCESS)
    {
        printf("<0x800 Condtion is not properly written.\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
INT4 
Utf8UtApi1_7 (VOID)
{
    UINT4   a_u1Utf8UserName[LEN_UTF8_USER_NAME] = {0};
    UINT1   a_u1TempArray[LEN_UTF8_USER_NAME] = {0};

    /* TestCaseAim: To Test Encoder's <0x10000 checking condition */
    a_u1Utf8UserName[0] = 0xffff;
    a_u1Utf8UserName[1] = L'\0';

    if(UnicodeToUtf8
    (a_u1Utf8UserName, 1,a_u1TempArray,LEN_UTF8_USER_NAME) != UTF8_SUCCESS)
    {
        printf("<0x10000 Condtion is not properly written.\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
INT4 
Utf8UtApi1_8 (VOID)
{
    UINT4   a_u1Utf8UserName[LEN_UTF8_USER_NAME] = {0};
    UINT1   a_u1TempArray[LEN_UTF8_USER_NAME] = {0};

    /* TestCaseAim: To Test Encoder's <0x200000 checking condition */
    a_u1Utf8UserName[0] = 0x1fffff;
    a_u1Utf8UserName[1] = L'\0';

    if(UnicodeToUtf8
    (a_u1Utf8UserName, 1,a_u1TempArray,LEN_UTF8_USER_NAME) != UTF8_SUCCESS)
    {
        printf("<0x200000 Condtion is not properly written.\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
INT4 
Utf8UtApi1_9 (VOID)
{
    UINT4   a_u1Utf8UserName[LEN_UTF8_USER_NAME] = {0};
    UINT1   a_u1TempArray[LEN_UTF8_USER_NAME] = {0};

    /* TestCaseAim: To Test Encoder's <0x4000000 checking condition */
    a_u1Utf8UserName[0] = 0x3ffffff;
    a_u1Utf8UserName[1] = L'\0';

    if(UnicodeToUtf8
    (a_u1Utf8UserName,1, a_u1TempArray,LEN_UTF8_USER_NAME) != UTF8_SUCCESS)
    {
        printf("0x3ffffff Condtion is not properly written.\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
INT4 
Utf8UtApi1_10 (VOID)
{ 
    UINT4   a_u1Utf8UserName[LEN_UTF8_USER_NAME] = {0};
    UINT1   a_u1TempArray[LEN_UTF8_USER_NAME] = {0};

    /* TestCaseAim: To Test Encoder's <0x8000000 checking condition */
    a_u1Utf8UserName[0] = 0x7ffffff;
    a_u1Utf8UserName[1] = L'\0';

    if(UnicodeToUtf8
    (a_u1Utf8UserName, 1,a_u1TempArray,LEN_UTF8_USER_NAME) != UTF8_SUCCESS)
    {
        printf("<0x8000000 Condtion is not properly written.\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
INT4 
Utf8UtApi1_11 (VOID)
{
    UINT4   a_u1Utf8UserName[LEN_UTF8_USER_NAME] = {0};
    UINT1   a_u1TempArray[LEN_UTF8_USER_NAME] = {0};

    /* TestCaseAim: To Test Decoder's ==0xFC checking condition */
    a_u1TempArray[0] = 0xfd;
    a_u1TempArray[1] = '\0';

    if(Utf8ToUnicode
    (a_u1TempArray, 1,a_u1Utf8UserName,LEN_UTF8_USER_NAME) != UTF8_SUCCESS)
    {
        printf("==0xFC Condtion is not properly written.\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
INT4
Utf8UtApi1_12 (VOID)
{
    UINT4   a_u1Utf8UserName[LEN_UTF8_USER_NAME] = {0};
    UINT1   a_u1TempArray[LEN_UTF8_USER_NAME] = {0};

    /* TestCaseAim: To Test Decoder's ==0xF8 checking condition */
    a_u1TempArray[0] = 0xfb;
    a_u1TempArray[1] = '\0';

    if(Utf8ToUnicode
    (a_u1TempArray, 1,a_u1Utf8UserName,LEN_UTF8_USER_NAME) != UTF8_SUCCESS)
    {
        printf("==0xF8 Condtion is not properly written.\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
INT4
Utf8UtApi1_13 (VOID)
{
    UINT4   a_u1Utf8UserName[LEN_UTF8_USER_NAME] = {0};
    UINT1   a_u1TempArray[LEN_UTF8_USER_NAME] = {0};
 
    /* TestCaseAim: To Test Decoder's ==0xF0 checking condition */
    a_u1TempArray[0] = 0xf7;
    a_u1TempArray[1] = '\0';

    if(Utf8ToUnicode
    (a_u1TempArray,1,a_u1Utf8UserName,LEN_UTF8_USER_NAME) != UTF8_SUCCESS)
    {
        printf("==0xF0 Condtion is not properly written.\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
INT4
Utf8UtApi1_14 (VOID)
{
    UINT4   a_u1Utf8UserName[LEN_UTF8_USER_NAME] = {0};
    UINT1   a_u1TempArray[LEN_UTF8_USER_NAME] = {0};
 
    /* TestCaseAim: To Test Decoder's ==0xE0 checking condition */
    a_u1TempArray[0] = 0xef;
    a_u1TempArray[1] = '\0';

    if(Utf8ToUnicode
    (a_u1TempArray,1, a_u1Utf8UserName,LEN_UTF8_USER_NAME) != UTF8_SUCCESS)
    {
        printf("==0xE0 Condtion is not properly written.\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
INT4
Utf8UtApi1_15 (VOID)
{
    UINT4   a_u1Utf8UserName[LEN_UTF8_USER_NAME] = {0};
    UINT1   a_u1TempArray[LEN_UTF8_USER_NAME] = {0};

    /* TestCaseAim: To Test Decoder's ==0xc0 checking condition */
    a_u1TempArray[0] = 0xdf;
    a_u1TempArray[1] = '\0';

    if(Utf8ToUnicode
    (a_u1TempArray, 1,a_u1Utf8UserName,LEN_UTF8_USER_NAME) != UTF8_SUCCESS)
    {
        printf("==0xc0 Condtion is not properly written.\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
INT4
Utf8UtApi1_16 (VOID)
{
    UINT4   a_u1Utf8UserName[LEN_UTF8_USER_NAME] = {0};
    UINT1   a_u1TempArray[LEN_UTF8_USER_NAME] = {0};

    /* TestCaseAim: To Test Decoder's !=0x80 checking condition */
    a_u1TempArray[0] = 0x7f;
    a_u1TempArray[1] = '\0';

    if(Utf8ToUnicode
    (a_u1TempArray, 1,a_u1Utf8UserName,LEN_UTF8_USER_NAME) != UTF8_SUCCESS)
    {
        printf("!=0x80 Condtion is not properly written.\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
INT4
Utf8UtApi1_17 (VOID)
{
    UINT4   a_u1Utf8UserName[LEN_UTF8_USER_NAME] = {0};
    UINT1   *a_u1TempArray = NULL;

    /* TestCaseAim: To Test Encoder's NULL checking condition */

    /* Passing the NULL args */
    if(UnicodeToUtf8
    (a_u1Utf8UserName, LEN_UTF8_USER_NAME,a_u1TempArray,0) == UTF8_SUCCESS)
    {
        printf("NULL checking is not done.\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
INT4
Utf8UtApi1_18 (VOID)
{
    UINT4   a_u1Utf8UserName[LEN_UTF8_USER_NAME] = {0};
    UINT1   *a_u1TempArray = NULL;

    /* TestCaseAim: To Test Decoder's NULL checking condition */

    /* Passing the NULL args */
    if(Utf8ToUnicode
    (a_u1TempArray,0, a_u1Utf8UserName,LEN_UTF8_USER_NAME) == UTF8_SUCCESS)
    {
        printf("NULL checking is not done.\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
INT4
Utf8UtApi1_19 (VOID)
{
    UINT4   a_u1Utf8UserName[LEN_UTF8_USER_NAME] = {0};
    UINT1   a_u1TempArray[LEN_UTF8_USER_NAME] = {0};

    /* TestCaseAim: Negative Test for Decoder's !=0x80 checking condition */
    a_u1TempArray[0] = 0x80;
    a_u1TempArray[1] = '\0';

    if(Utf8ToUnicode
    (a_u1TempArray,1, a_u1Utf8UserName,LEN_UTF8_USER_NAME) == UTF8_SUCCESS)
    {
        printf("!=0x80 Condtion is not properly written.\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
INT4
Utf8UtApi1_20 (VOID)
{
    UINT4   a_u1Utf8UserName[LEN_UTF8_USER_NAME] = {0};
    UINT1   a_u1TempArray[LEN_UTF8_USER_NAME] = {0};

    /* TestCaseAim: Negative Test for Encoder's <0x8000000 checking condition */
    a_u1Utf8UserName[0] = 0x8000000;
    a_u1Utf8UserName[1] = L'\0';

    if(UnicodeToUtf8
    (a_u1Utf8UserName, 1,a_u1TempArray,LEN_UTF8_USER_NAME) == UTF8_SUCCESS)
    {
        printf("<0x8000000 Condtion is not properly written.\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}   

