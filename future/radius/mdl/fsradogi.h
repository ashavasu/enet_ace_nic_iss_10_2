
# ifndef fsrad_OGP_H
# define fsrad_OGP_H

 /* The Definitions of the OGP Index Constants.  */
# define SNMP_OGP_INDEX_RADIUSEXTSERVERTABLE               (0)
# define SNMP_OGP_INDEX_RADIUSEXTCLIENT               (1)

#endif /*  fsradius_OGP_H  */
