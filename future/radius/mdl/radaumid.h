
/*  Prototype for Get Test & Set for radiusAuthServerTable.  */
tSNMP_VAR_BIND*
radiusAuthServerEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));


/*  Prototype for Get Test & Set for radiusAuthClient.  */
tSNMP_VAR_BIND*
radiusAuthClientGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));

