
/*****
  snmpmbdb.h . This File Contains the Mib DataBase.
*****/

#ifndef RADACC_SNMP_MBDB_H
#define RADACC_SNMP_MBDB_H
/*  The NULL FUNCTION POINTER.  */
# define NULLF ARG_LIST(((INT4 (*)(tSNMP_OID_TYPE *,\
                                          tSNMP_OID_TYPE *,\
                                          UINT1,\
                                          tSNMP_MULTI_DATA_TYPE *)) NULL))
/*  NULL VARBIND FUNCTION POINTER . */
# define NULLVBF  ARG_LIST(((tSNMP_VAR_BIND* (*)(tSNMP_OID_TYPE *,\
                                           tSNMP_OID_TYPE *,\
                                           UINT1,\
                                           UINT1))NULL))
# include "radacmid.h"
# include "radaccon.h"
# include "radacogi.h"

/*  The Declaration of the Group Arrays. */

UINT4 au4_radacc_snmp_TABLE1[] = {1,3,6,1,2,1,67,2,2,1,1};

/*  The Declaration of the SubGroup Arrays. */

UINT4  au4_SNMP_OGP_RADIUSACCSERVERTABLE_OID [] ={3,1};
UINT4  au4_SNMP_OGP_RADIUSACCCLIENT_OID [] ={0};

/*  Declaration of Group OID Table. */
/* Each entry contains Length of Group OID, Pointer to the Group OID,
   Priority of the registering subagent, Timeout for response,
   and Number of Subgroups in that order */

const tSNMP_GroupOIDType radacc_FMAS_GroupOIDTable[] =
{
   {11 , au4_radacc_snmp_TABLE1 , 1 , 10 , 2}
};
/*  Declaration of Base OID Table. */
/* Each entry contains Length of Base OID, Pointer to the Base OID,
   Middle level get function pointer, Middle level test function pointer,
   Middle level set function pointer and Number of objects in that table
   in that order */

const tSNMP_BaseOIDType  radacc_FMAS_BaseOIDTable[] = {
{
0,
au4_SNMP_OGP_RADIUSACCCLIENT_OID,
radiusAccClientGet,
NULLF,
NULLF,
2
},
{
2,
au4_SNMP_OGP_RADIUSACCSERVERTABLE_OID,
radiusAccServerEntryGet,
NULLF,
NULLF,
13
}
};
/* Declaration of MIB Object Table. */
/* Each entry contains Name of the table, Permissions for
   the object and Object name in that order */

const tSNMP_MIBObjectDescrType  radacc_FMAS_MIBObjectTable[] = {
{
 SNMP_OGP_INDEX_RADIUSACCCLIENT,
 READ_ONLY,
 RADIUSACCCLIENTINVALIDSERVERADDRESSES
},
{
 SNMP_OGP_INDEX_RADIUSACCCLIENT,
 READ_ONLY,
 RADIUSACCCLIENTIDENTIFIER
},
{
 SNMP_OGP_INDEX_RADIUSACCSERVERTABLE,
 NO_ACCESS,
 RADIUSACCSERVERINDEX
},
{
 SNMP_OGP_INDEX_RADIUSACCSERVERTABLE,
 READ_ONLY,
 RADIUSACCSERVERADDRESS
},
{
 SNMP_OGP_INDEX_RADIUSACCSERVERTABLE,
 READ_ONLY,
 RADIUSACCCLIENTSERVERPORTNUMBER
},
{
 SNMP_OGP_INDEX_RADIUSACCSERVERTABLE,
 READ_ONLY,
 RADIUSACCCLIENTROUNDTRIPTIME
},
{
 SNMP_OGP_INDEX_RADIUSACCSERVERTABLE,
 READ_ONLY,
 RADIUSACCCLIENTREQUESTS
},
{
 SNMP_OGP_INDEX_RADIUSACCSERVERTABLE,
 READ_ONLY,
 RADIUSACCCLIENTRETRANSMISSIONS
},
{
 SNMP_OGP_INDEX_RADIUSACCSERVERTABLE,
 READ_ONLY,
 RADIUSACCCLIENTRESPONSES
},
{
 SNMP_OGP_INDEX_RADIUSACCSERVERTABLE,
 READ_ONLY,
 RADIUSACCCLIENTMALFORMEDRESPONSES
},
{
 SNMP_OGP_INDEX_RADIUSACCSERVERTABLE,
 READ_ONLY,
 RADIUSACCCLIENTBADAUTHENTICATORS
},
{
 SNMP_OGP_INDEX_RADIUSACCSERVERTABLE,
 READ_ONLY,
 RADIUSACCCLIENTPENDINGREQUESTS
},
{
 SNMP_OGP_INDEX_RADIUSACCSERVERTABLE,
 READ_ONLY,
 RADIUSACCCLIENTTIMEOUTS
},
{
 SNMP_OGP_INDEX_RADIUSACCSERVERTABLE,
 READ_ONLY,
 RADIUSACCCLIENTUNKNOWNTYPES
},
{
 SNMP_OGP_INDEX_RADIUSACCSERVERTABLE,
 READ_ONLY,
 RADIUSACCCLIENTPACKETSDROPPED
}
};

const tSNMP_GLOBAL_STRUCT radacc_FMAS_Global_data =
{sizeof (radacc_FMAS_GroupOIDTable) / sizeof (tSNMP_GroupOIDType),
 sizeof (radacc_FMAS_BaseOIDTable) / sizeof (tSNMP_BaseOIDType)};

const int radacc_MAX_OBJECTS =
(sizeof (radacc_FMAS_MIBObjectTable) / sizeof (tSNMP_MIBObjectDescrType));

#endif  /* RADACC_SNMP_MBDB_H. */

