# include  "include.h"
# include  "radaumid.h"
# include  "radaulow.h"
# include  "radaucon.h"
# include  "radauogi.h"
# include  "extern.h"
# include  "midconst.h"

/****************************************************************************
 Function   : radiusAuthServerEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
#ifdef __STDC__
tSNMP_VAR_BIND     *
radiusAuthServerEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                          UINT1 u1_arg, UINT1 u1_search_type)
#else
tSNMP_VAR_BIND     *
radiusAuthServerEntryGet (p_in_db, p_incoming, u1_arg, u1_search_type)
     tSNMP_OID_TYPE     *p_in_db;
     tSNMP_OID_TYPE     *p_incoming;
     UINT1               u1_arg;
     UINT1               u1_search_type;
#endif
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_radiusAuthServerTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val;
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_radiusAuthServerIndex = FALSE;
    INT4                i4_next_radiusAuthServerIndex = FALSE;

    UINT4               u4_addr_ret_val_radiusAuthServerAddress;
   /*** $$TRACE_LOG (ENTRY,"radiusAuthServerTableGet Routine\n"); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_arg = %d\n",u1_arg); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_search_type = %d\n",u1_search_type); ***/

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_radiusAuthServerTable_INDEX = p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_radiusAuthServerTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_radiusAuthServerIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceRadiusAuthServerTable
                     (i4_radiusAuthServerIndex)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_radiusAuthServerIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexRadiusAuthServerTable
                     (&i4_radiusAuthServerIndex)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_radiusAuthServerIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_radiusAuthServerIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexRadiusAuthServerTable
                     (i4_radiusAuthServerIndex,
                      &i4_next_radiusAuthServerIndex)) == SNMP_SUCCESS)
                {
                    i4_radiusAuthServerIndex = i4_next_radiusAuthServerIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_radiusAuthServerIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case RADIUSAUTHSERVERINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == EXACT) || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_radiusAuthServerIndex;
            }
            else
            {
                i4_return_val = i4_next_radiusAuthServerIndex;
            }
            break;
        }
        case RADIUSAUTHSERVERADDRESS:
        {
            i1_ret_val =
                nmhGetRadiusAuthServerAddress (i4_radiusAuthServerIndex,
                                               &u4_addr_ret_val_radiusAuthServerAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                CRU_BMC_DWTOPDU (u1_octet_string,
                                 u4_addr_ret_val_radiusAuthServerAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case RADIUSAUTHCLIENTSERVERPORTNUMBER:
        {
            i1_ret_val =
                nmhGetRadiusAuthClientServerPortNumber
                (i4_radiusAuthServerIndex, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case RADIUSAUTHCLIENTROUNDTRIPTIME:
        {
            i1_ret_val =
                nmhGetRadiusAuthClientRoundTripTime (i4_radiusAuthServerIndex,
                                                     &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case RADIUSAUTHCLIENTACCESSREQUESTS:
        {
            i1_ret_val =
                nmhGetRadiusAuthClientAccessRequests (i4_radiusAuthServerIndex,
                                                      &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case RADIUSAUTHCLIENTACCESSRETRANSMISSIONS:
        {
            i1_ret_val =
                nmhGetRadiusAuthClientAccessRetransmissions
                (i4_radiusAuthServerIndex, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case RADIUSAUTHCLIENTACCESSACCEPTS:
        {
            i1_ret_val =
                nmhGetRadiusAuthClientAccessAccepts (i4_radiusAuthServerIndex,
                                                     &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case RADIUSAUTHCLIENTACCESSREJECTS:
        {
            i1_ret_val =
                nmhGetRadiusAuthClientAccessRejects (i4_radiusAuthServerIndex,
                                                     &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case RADIUSAUTHCLIENTACCESSCHALLENGES:
        {
            i1_ret_val =
                nmhGetRadiusAuthClientAccessChallenges
                (i4_radiusAuthServerIndex, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case RADIUSAUTHCLIENTMALFORMEDACCESSRESPONSES:
        {
            i1_ret_val =
                nmhGetRadiusAuthClientMalformedAccessResponses
                (i4_radiusAuthServerIndex, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case RADIUSAUTHCLIENTBADAUTHENTICATORS:
        {
            i1_ret_val =
                nmhGetRadiusAuthClientBadAuthenticators
                (i4_radiusAuthServerIndex, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case RADIUSAUTHCLIENTPENDINGREQUESTS:
        {
            i1_ret_val =
                nmhGetRadiusAuthClientPendingRequests (i4_radiusAuthServerIndex,
                                                       &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_GAUGE32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case RADIUSAUTHCLIENTTIMEOUTS:
        {
            i1_ret_val =
                nmhGetRadiusAuthClientTimeouts (i4_radiusAuthServerIndex,
                                                &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case RADIUSAUTHCLIENTUNKNOWNTYPES:
        {
            i1_ret_val =
                nmhGetRadiusAuthClientUnknownTypes (i4_radiusAuthServerIndex,
                                                    &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case RADIUSAUTHCLIENTPACKETSDROPPED:
        {
            i1_ret_val =
                nmhGetRadiusAuthClientPacketsDropped (i4_radiusAuthServerIndex,
                                                      &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

   /*** $$TRACE_LOG (EXIT,"  i2_type = %d\n",i2_type); ***/
   /*** $$TRACE_LOG (EXIT,"  u4_counter_val = %u\n",u4_counter_val); ***/
   /*** $$TRACE_LOG (EXIT,"  i2_type = %d\n",i4_return_val); ***/

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : radiusAuthClientGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
#ifdef __STDC__
tSNMP_VAR_BIND     *
radiusAuthClientGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                     UINT1 u1_arg, UINT1 u1_search_type)
#else
tSNMP_VAR_BIND     *
radiusAuthClientGet (p_in_db, p_incoming, u1_arg, u1_search_type)
     tSNMP_OID_TYPE     *p_in_db;
     tSNMP_OID_TYPE     *p_incoming;
     UINT1               u1_arg;
     UINT1               u1_search_type;
#endif
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Scalar get routine. */

    UINT1               i1_ret_val = FALSE;
    INT4                LEN_radiusAuthClient_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val;
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  This Variable is declared for being used in the
     *  FOR Loop for extracting Indices from OID Given.
     */

    tSNMP_OCTET_STRING_TYPE *poctet_retval_radiusAuthClientIdentifier = NULL;
   /*** $$TRACE_LOG (ENTRY,"radiusAuthClientGet Routine\n"); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_arg = %d\n",u1_arg); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_search_type = %d\n",u1_search_type); ***/

/*** DECLARATION_END ***/

    LEN_radiusAuthClient_INDEX = p_in_db->u4_Length;

    /*  Incrementing the Length for the Extract of Scalar Tables. */
    LEN_radiusAuthClient_INDEX++;
    if (u1_search_type == EXACT)
    {
        if ((LEN_radiusAuthClient_INDEX != (INT4) p_incoming->u4_Length)
            || (p_incoming->pu4_OidList[p_incoming->u4_Length - 1] != 0))
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    else
    {
        /*  Get Next Operation on the Scalar Variable.  */
        if ((INT4) p_incoming->u4_Length >= LEN_radiusAuthClient_INDEX)
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    switch (u1_arg)
    {
        case RADIUSAUTHCLIENTINVALIDSERVERADDRESSES:
        {
            i1_ret_val =
                nmhGetRadiusAuthClientInvalidServerAddresses (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case RADIUSAUTHCLIENTIDENTIFIER:
        {
            poctet_retval_radiusAuthClientIdentifier =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (256);
            if (poctet_retval_radiusAuthClientIdentifier == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetRadiusAuthClientIdentifier
                (poctet_retval_radiusAuthClientIdentifier);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_radiusAuthClientIdentifier;
            }
            else
            {
                free_octetstring (poctet_retval_radiusAuthClientIdentifier);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    /* Incrementing the Length of the p_in_db. */
    p_in_db->u4_Length++;
    /* Adding the .0 to the p_in_db for scalar Objects. */
    p_in_db->pu4_OidList[p_in_db->u4_Length - 1] = ZERO;
   /*** $$TRACE_LOG (EXIT,"  i2_type = %d\n",i2_type); ***/
   /*** $$TRACE_LOG (EXIT,"  u4_counter_val = %u\n",u4_counter_val); ***/
   /*** $$TRACE_LOG (EXIT,"  i2_type = %d\n",i4_return_val); ***/

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */
