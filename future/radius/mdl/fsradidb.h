/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsradidb.h,v 1.5 2015/04/28 12:26:21 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSRADIDB_H
#define _FSRADIDB_H

UINT1 RadiusExtServerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 fsradi [] ={1,3,6,1,4,1,2076,25};
tSNMP_OID_TYPE fsradiOID = {8, fsradi};


UINT4 RadiusExtDebugMask [ ] ={1,3,6,1,4,1,2076,25,1,1};
UINT4 RadiusMaxNoOfUserEntries [ ] ={1,3,6,1,4,1,2076,25,1,2};
UINT4 RadiusExtServerIndex [ ] ={1,3,6,1,4,1,2076,25,1,3,1,1};
UINT4 RadiusExtServerAddress [ ] ={1,3,6,1,4,1,2076,25,1,3,1,2};
UINT4 RadiusExtServerType [ ] ={1,3,6,1,4,1,2076,25,1,3,1,3};
UINT4 RadiusExtServerSharedSecret [ ] ={1,3,6,1,4,1,2076,25,1,3,1,4};
UINT4 RadiusExtServerEnabled [ ] ={1,3,6,1,4,1,2076,25,1,3,1,5};
UINT4 RadiusExtServerResponseTime [ ] ={1,3,6,1,4,1,2076,25,1,3,1,6};
UINT4 RadiusExtServerMaximumRetransmission [ ] ={1,3,6,1,4,1,2076,25,1,3,1,7};
UINT4 RadiusExtServerEntryStatus [ ] ={1,3,6,1,4,1,2076,25,1,3,1,8};


tMbDbEntry fsradiMibEntry[]= {

{{10,RadiusExtDebugMask}, NULL, RadiusExtDebugMaskGet, RadiusExtDebugMaskSet, RadiusExtDebugMaskTest, RadiusExtDebugMaskDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,RadiusMaxNoOfUserEntries}, NULL, RadiusMaxNoOfUserEntriesGet, RadiusMaxNoOfUserEntriesSet, RadiusMaxNoOfUserEntriesTest, RadiusMaxNoOfUserEntriesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,RadiusExtServerIndex}, GetNextIndexRadiusExtServerTable, RadiusExtServerIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, RadiusExtServerTableINDEX, 1, 0, 0, NULL},

{{12,RadiusExtServerAddress}, GetNextIndexRadiusExtServerTable, RadiusExtServerAddressGet, RadiusExtServerAddressSet, RadiusExtServerAddressTest, RadiusExtServerTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, RadiusExtServerTableINDEX, 1, 0, 0, NULL},

{{12,RadiusExtServerType}, GetNextIndexRadiusExtServerTable, RadiusExtServerTypeGet, RadiusExtServerTypeSet, RadiusExtServerTypeTest, RadiusExtServerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, RadiusExtServerTableINDEX, 1, 0, 0, NULL},

{{12,RadiusExtServerSharedSecret}, GetNextIndexRadiusExtServerTable, RadiusExtServerSharedSecretGet, RadiusExtServerSharedSecretSet, RadiusExtServerSharedSecretTest, RadiusExtServerTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, RadiusExtServerTableINDEX, 1, 0, 0, NULL},

{{12,RadiusExtServerEnabled}, GetNextIndexRadiusExtServerTable, RadiusExtServerEnabledGet, RadiusExtServerEnabledSet, RadiusExtServerEnabledTest, RadiusExtServerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, RadiusExtServerTableINDEX, 1, 0, 0, "1"},

{{12,RadiusExtServerResponseTime}, GetNextIndexRadiusExtServerTable, RadiusExtServerResponseTimeGet, RadiusExtServerResponseTimeSet, RadiusExtServerResponseTimeTest, RadiusExtServerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, RadiusExtServerTableINDEX, 1, 0, 0, NULL},

{{12,RadiusExtServerMaximumRetransmission}, GetNextIndexRadiusExtServerTable, RadiusExtServerMaximumRetransmissionGet, RadiusExtServerMaximumRetransmissionSet, RadiusExtServerMaximumRetransmissionTest, RadiusExtServerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, RadiusExtServerTableINDEX, 1, 0, 0, NULL},

{{12,RadiusExtServerEntryStatus}, GetNextIndexRadiusExtServerTable, RadiusExtServerEntryStatusGet, RadiusExtServerEntryStatusSet, RadiusExtServerEntryStatusTest, RadiusExtServerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, RadiusExtServerTableINDEX, 1, 0, 1, NULL},
};
tMibData fsradiEntry = { 10, fsradiMibEntry };
#endif /* _FSRADIDB_H */

