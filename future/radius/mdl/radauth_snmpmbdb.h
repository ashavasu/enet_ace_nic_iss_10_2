
/*****
  snmpmbdb.h . This File Contains the Mib DataBase.
*****/

#ifndef RADAUTH_SNMP_MBDB_H
#define RADAUTH_SNMP_MBDB_H
/*  The NULL FUNCTION POINTER.  */
# define NULLF ARG_LIST(((INT4 (*)(tSNMP_OID_TYPE *,\
                                          tSNMP_OID_TYPE *,\
                                          UINT1,\
                                          tSNMP_MULTI_DATA_TYPE *)) NULL))
/*  NULL VARBIND FUNCTION POINTER . */
# define NULLVBF  ARG_LIST(((tSNMP_VAR_BIND* (*)(tSNMP_OID_TYPE *,\
                                           tSNMP_OID_TYPE *,\
                                           UINT1,\
                                           UINT1))NULL))
# include "radaumid.h"
# include "radaucon.h"
# include "radauogi.h"

/*  The Declaration of the Group Arrays. */

UINT4 au4_radauth_snmp_TABLE1[] = {1,3,6,1,2,1,67,1,2,1,1};

/*  The Declaration of the SubGroup Arrays. */

UINT4  au4_SNMP_OGP_RADIUSAUTHSERVERTABLE_OID [] ={3,1};
UINT4  au4_SNMP_OGP_RADIUSAUTHCLIENT_OID [] ={0};

/*  Declaration of Group OID Table. */
/* Each entry contains Length of Group OID, Pointer to the Group OID,
   Priority of the registering subagent, Timeout for response,
   and Number of Subgroups in that order */

const tSNMP_GroupOIDType radauth_FMAS_GroupOIDTable[] =
{
   {11 , au4_radauth_snmp_TABLE1 , 1 , 10 , 2}
};
/*  Declaration of Base OID Table. */
/* Each entry contains Length of Base OID, Pointer to the Base OID,
   Middle level get function pointer, Middle level test function pointer,
   Middle level set function pointer and Number of objects in that table
   in that order */

const tSNMP_BaseOIDType  radauth_FMAS_BaseOIDTable[] = {
{
0,
au4_SNMP_OGP_RADIUSAUTHCLIENT_OID,
radiusAuthClientGet,
NULLF,
NULLF,
2
},
{
2,
au4_SNMP_OGP_RADIUSAUTHSERVERTABLE_OID,
radiusAuthServerEntryGet,
NULLF,
NULLF,
15
}
};
/* Declaration of MIB Object Table. */
/* Each entry contains Name of the table, Permissions for
   the object and Object name in that order */

const tSNMP_MIBObjectDescrType  radauth_FMAS_MIBObjectTable[] = {
{
 SNMP_OGP_INDEX_RADIUSAUTHCLIENT,
 READ_ONLY,
 RADIUSAUTHCLIENTINVALIDSERVERADDRESSES
},
{
 SNMP_OGP_INDEX_RADIUSAUTHCLIENT,
 READ_ONLY,
 RADIUSAUTHCLIENTIDENTIFIER
},
{
 SNMP_OGP_INDEX_RADIUSAUTHSERVERTABLE,
 NO_ACCESS,
 RADIUSAUTHSERVERINDEX
},
{
 SNMP_OGP_INDEX_RADIUSAUTHSERVERTABLE,
 READ_ONLY,
 RADIUSAUTHSERVERADDRESS
},
{
 SNMP_OGP_INDEX_RADIUSAUTHSERVERTABLE,
 READ_ONLY,
 RADIUSAUTHCLIENTSERVERPORTNUMBER
},
{
 SNMP_OGP_INDEX_RADIUSAUTHSERVERTABLE,
 READ_ONLY,
 RADIUSAUTHCLIENTROUNDTRIPTIME
},
{
 SNMP_OGP_INDEX_RADIUSAUTHSERVERTABLE,
 READ_ONLY,
 RADIUSAUTHCLIENTACCESSREQUESTS
},
{
 SNMP_OGP_INDEX_RADIUSAUTHSERVERTABLE,
 READ_ONLY,
 RADIUSAUTHCLIENTACCESSRETRANSMISSIONS
},
{
 SNMP_OGP_INDEX_RADIUSAUTHSERVERTABLE,
 READ_ONLY,
 RADIUSAUTHCLIENTACCESSACCEPTS
},
{
 SNMP_OGP_INDEX_RADIUSAUTHSERVERTABLE,
 READ_ONLY,
 RADIUSAUTHCLIENTACCESSREJECTS
},
{
 SNMP_OGP_INDEX_RADIUSAUTHSERVERTABLE,
 READ_ONLY,
 RADIUSAUTHCLIENTACCESSCHALLENGES
},
{
 SNMP_OGP_INDEX_RADIUSAUTHSERVERTABLE,
 READ_ONLY,
 RADIUSAUTHCLIENTMALFORMEDACCESSRESPONSES
},
{
 SNMP_OGP_INDEX_RADIUSAUTHSERVERTABLE,
 READ_ONLY,
 RADIUSAUTHCLIENTBADAUTHENTICATORS
},
{
 SNMP_OGP_INDEX_RADIUSAUTHSERVERTABLE,
 READ_ONLY,
 RADIUSAUTHCLIENTPENDINGREQUESTS
},
{
 SNMP_OGP_INDEX_RADIUSAUTHSERVERTABLE,
 READ_ONLY,
 RADIUSAUTHCLIENTTIMEOUTS
},
{
 SNMP_OGP_INDEX_RADIUSAUTHSERVERTABLE,
 READ_ONLY,
 RADIUSAUTHCLIENTUNKNOWNTYPES
},
{
 SNMP_OGP_INDEX_RADIUSAUTHSERVERTABLE,
 READ_ONLY,
 RADIUSAUTHCLIENTPACKETSDROPPED
}
};

const tSNMP_GLOBAL_STRUCT radauth_FMAS_Global_data =
{sizeof (radauth_FMAS_GroupOIDTable) / sizeof (tSNMP_GroupOIDType),
 sizeof (radauth_FMAS_BaseOIDTable) / sizeof (tSNMP_BaseOIDType)};

const int radauth_MAX_OBJECTS =
(sizeof (radauth_FMAS_MIBObjectTable) / sizeof (tSNMP_MIBObjectDescrType));

#endif  /* RADAUTH_SNMP_MBDB_H. */

