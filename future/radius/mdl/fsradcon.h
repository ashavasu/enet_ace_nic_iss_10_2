
# ifndef fsrad_OCON_H
# define fsrad_OCON_H
/*
 *  The Constant Declarations for
 *  radiusExtServerTable
 */
# define RADIUSEXTSERVERINDEX               (1)
# define RADIUSEXTSERVERADDRESS               (2)
# define RADIUSEXTSERVERTYPE               (3)
# define RADIUSEXTSERVERSHAREDSECRET               (4)
# define RADIUSEXTSERVERENABLED               (5)
# define RADIUSEXTSERVERRESPONSETIME               (6)
# define RADIUSEXTSERVERMAXIMUMRETRANSMISSION               (7)
# define RADIUSEXTSERVERENTRYSTATUS               (8)

/*
 *  The Constant Declarations for
 *  radiusExtClient
 */
# define RADIUSEXTDEBUGMASK               (1)
# define RADIUSMAXNOOFUSERENTRIES               (2)

#endif /*  fsradius_OCON_H  */
