/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: radautdb.h,v 1.5 2015/04/28 12:26:21 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _RADAUTDB_H
#define _RADAUTDB_H

UINT1 RadiusAuthServerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 radaut [] ={1,3,6,1,2,1,67,1,2};
tSNMP_OID_TYPE radautOID = {9, radaut};


UINT4 RadiusAuthClientInvalidServerAddresses [ ] ={1,3,6,1,2,1,67,1,2,1,1,1};
UINT4 RadiusAuthClientIdentifier [ ] ={1,3,6,1,2,1,67,1,2,1,1,2};
UINT4 RadiusAuthServerIndex [ ] ={1,3,6,1,2,1,67,1,2,1,1,3,1,1};
UINT4 RadiusAuthServerAddress [ ] ={1,3,6,1,2,1,67,1,2,1,1,3,1,2};
UINT4 RadiusAuthClientServerPortNumber [ ] ={1,3,6,1,2,1,67,1,2,1,1,3,1,3};
UINT4 RadiusAuthClientRoundTripTime [ ] ={1,3,6,1,2,1,67,1,2,1,1,3,1,4};
UINT4 RadiusAuthClientAccessRequests [ ] ={1,3,6,1,2,1,67,1,2,1,1,3,1,5};
UINT4 RadiusAuthClientAccessRetransmissions [ ] ={1,3,6,1,2,1,67,1,2,1,1,3,1,6};
UINT4 RadiusAuthClientAccessAccepts [ ] ={1,3,6,1,2,1,67,1,2,1,1,3,1,7};
UINT4 RadiusAuthClientAccessRejects [ ] ={1,3,6,1,2,1,67,1,2,1,1,3,1,8};
UINT4 RadiusAuthClientAccessChallenges [ ] ={1,3,6,1,2,1,67,1,2,1,1,3,1,9};
UINT4 RadiusAuthClientMalformedAccessResponses [ ] ={1,3,6,1,2,1,67,1,2,1,1,3,1,10};
UINT4 RadiusAuthClientBadAuthenticators [ ] ={1,3,6,1,2,1,67,1,2,1,1,3,1,11};
UINT4 RadiusAuthClientPendingRequests [ ] ={1,3,6,1,2,1,67,1,2,1,1,3,1,12};
UINT4 RadiusAuthClientTimeouts [ ] ={1,3,6,1,2,1,67,1,2,1,1,3,1,13};
UINT4 RadiusAuthClientUnknownTypes [ ] ={1,3,6,1,2,1,67,1,2,1,1,3,1,14};
UINT4 RadiusAuthClientPacketsDropped [ ] ={1,3,6,1,2,1,67,1,2,1,1,3,1,15};


tMbDbEntry radautMibEntry[]= {

{{12,RadiusAuthClientInvalidServerAddresses}, NULL, RadiusAuthClientInvalidServerAddressesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,RadiusAuthClientIdentifier}, NULL, RadiusAuthClientIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{14,RadiusAuthServerIndex}, GetNextIndexRadiusAuthServerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, RadiusAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAuthServerAddress}, GetNextIndexRadiusAuthServerTable, RadiusAuthServerAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, RadiusAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAuthClientServerPortNumber}, GetNextIndexRadiusAuthServerTable, RadiusAuthClientServerPortNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, RadiusAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAuthClientRoundTripTime}, GetNextIndexRadiusAuthServerTable, RadiusAuthClientRoundTripTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, RadiusAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAuthClientAccessRequests}, GetNextIndexRadiusAuthServerTable, RadiusAuthClientAccessRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, RadiusAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAuthClientAccessRetransmissions}, GetNextIndexRadiusAuthServerTable, RadiusAuthClientAccessRetransmissionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, RadiusAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAuthClientAccessAccepts}, GetNextIndexRadiusAuthServerTable, RadiusAuthClientAccessAcceptsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, RadiusAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAuthClientAccessRejects}, GetNextIndexRadiusAuthServerTable, RadiusAuthClientAccessRejectsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, RadiusAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAuthClientAccessChallenges}, GetNextIndexRadiusAuthServerTable, RadiusAuthClientAccessChallengesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, RadiusAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAuthClientMalformedAccessResponses}, GetNextIndexRadiusAuthServerTable, RadiusAuthClientMalformedAccessResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, RadiusAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAuthClientBadAuthenticators}, GetNextIndexRadiusAuthServerTable, RadiusAuthClientBadAuthenticatorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, RadiusAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAuthClientPendingRequests}, GetNextIndexRadiusAuthServerTable, RadiusAuthClientPendingRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, RadiusAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAuthClientTimeouts}, GetNextIndexRadiusAuthServerTable, RadiusAuthClientTimeoutsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, RadiusAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAuthClientUnknownTypes}, GetNextIndexRadiusAuthServerTable, RadiusAuthClientUnknownTypesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, RadiusAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAuthClientPacketsDropped}, GetNextIndexRadiusAuthServerTable, RadiusAuthClientPacketsDroppedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, RadiusAuthServerTableINDEX, 1, 0, 0, NULL},
};
tMibData radautEntry = { 17, radautMibEntry };
#endif /* _RADAUTDB_H */

