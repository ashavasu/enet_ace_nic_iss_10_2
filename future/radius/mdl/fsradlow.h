/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsradlow.h,v 1.3 2015/04/28 12:26:21 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/

/* Proto Validate Index Instance for RadiusExtServerTable. */
INT1
nmhValidateIndexInstanceRadiusExtServerTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for RadiusExtServerTable  */

INT1
nmhGetFirstIndexRadiusExtServerTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexRadiusExtServerTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetRadiusExtServerAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusExtServerType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetRadiusExtServerSharedSecret ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetRadiusExtServerEnabled ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetRadiusExtServerResponseTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetRadiusExtServerMaximumRetransmission ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetRadiusExtServerEntryStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetRadiusExtServerAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetRadiusExtServerType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetRadiusExtServerSharedSecret ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetRadiusExtServerEnabled ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetRadiusExtServerResponseTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetRadiusExtServerMaximumRetransmission ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetRadiusExtServerEntryStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2RadiusExtServerAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2RadiusExtServerType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2RadiusExtServerSharedSecret ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2RadiusExtServerEnabled ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2RadiusExtServerResponseTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2RadiusExtServerMaximumRetransmission ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2RadiusExtServerEntryStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2RadiusExtServerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetRadiusExtDebugMask ARG_LIST((INT4 *));

INT1
nmhGetRadiusMaxNoOfUserEntries ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetRadiusExtDebugMask ARG_LIST((INT4 ));

INT1
nmhSetRadiusMaxNoOfUserEntries ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2RadiusExtDebugMask ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2RadiusMaxNoOfUserEntries ARG_LIST((UINT4 *  ,INT4 ));



/* Low Level DEP Routines for.  */

INT1
nmhDepv2RadiusExtDebugMask ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2RadiusMaxNoOfUserEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
