
# ifndef radac_OCON_H
# define radac_OCON_H
/*
 *  The Constant Declarations for
 *  radiusAccServerTable
 */
# define RADIUSACCSERVERINDEX               (1)
# define RADIUSACCSERVERADDRESS               (2)
# define RADIUSACCCLIENTSERVERPORTNUMBER               (3)
# define RADIUSACCCLIENTROUNDTRIPTIME               (4)
# define RADIUSACCCLIENTREQUESTS               (5)
# define RADIUSACCCLIENTRETRANSMISSIONS               (6)
# define RADIUSACCCLIENTRESPONSES               (7)
# define RADIUSACCCLIENTMALFORMEDRESPONSES               (8)
# define RADIUSACCCLIENTBADAUTHENTICATORS               (9)
# define RADIUSACCCLIENTPENDINGREQUESTS               (10)
# define RADIUSACCCLIENTTIMEOUTS               (11)
# define RADIUSACCCLIENTUNKNOWNTYPES               (12)
# define RADIUSACCCLIENTPACKETSDROPPED               (13)

/*
 *  The Constant Declarations for
 *  radiusAccClient
 */
# define RADIUSACCCLIENTINVALIDSERVERADDRESSES               (1)
# define RADIUSACCCLIENTIDENTIFIER               (2)

#endif /*  radacc_OCON_H  */
