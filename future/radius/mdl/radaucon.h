
# ifndef radau_OCON_H
# define radau_OCON_H
/*
 *  The Constant Declarations for
 *  radiusAuthServerTable
 */
# define RADIUSAUTHSERVERINDEX               (1)
# define RADIUSAUTHSERVERADDRESS               (2)
# define RADIUSAUTHCLIENTSERVERPORTNUMBER               (3)
# define RADIUSAUTHCLIENTROUNDTRIPTIME               (4)
# define RADIUSAUTHCLIENTACCESSREQUESTS               (5)
# define RADIUSAUTHCLIENTACCESSRETRANSMISSIONS               (6)
# define RADIUSAUTHCLIENTACCESSACCEPTS               (7)
# define RADIUSAUTHCLIENTACCESSREJECTS               (8)
# define RADIUSAUTHCLIENTACCESSCHALLENGES               (9)
# define RADIUSAUTHCLIENTMALFORMEDACCESSRESPONSES               (10)
# define RADIUSAUTHCLIENTBADAUTHENTICATORS               (11)
# define RADIUSAUTHCLIENTPENDINGREQUESTS               (12)
# define RADIUSAUTHCLIENTTIMEOUTS               (13)
# define RADIUSAUTHCLIENTUNKNOWNTYPES               (14)
# define RADIUSAUTHCLIENTPACKETSDROPPED               (15)

/*
 *  The Constant Declarations for
 *  radiusAuthClient
 */
# define RADIUSAUTHCLIENTINVALIDSERVERADDRESSES               (1)
# define RADIUSAUTHCLIENTIDENTIFIER               (2)

#endif /*  radauth_OCON_H  */
