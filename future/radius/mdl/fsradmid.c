# include  "include.h"
# include  "fsradmid.h"
# include  "fsradlow.h"
# include  "fsradcon.h"
# include  "fsradogi.h"
# include  "extern.h"
# include  "midconst.h"
# include  "fsradicli.h"
#include "fsradiwr.h"

/****************************************************************************
 Function   : radiusExtServerEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
#ifdef __STDC__
tSNMP_VAR_BIND     *
radiusExtServerEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                         UINT1 u1_arg, UINT1 u1_search_type)
#else
tSNMP_VAR_BIND     *
radiusExtServerEntryGet (p_in_db, p_incoming, u1_arg, u1_search_type)
     tSNMP_OID_TYPE     *p_in_db;
     tSNMP_OID_TYPE     *p_incoming;
     UINT1               u1_arg;
     UINT1               u1_search_type;
#endif
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_radiusExtServerTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val;
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_radiusExtServerIndex = FALSE;
    INT4                i4_next_radiusExtServerIndex = FALSE;

    UINT4               u4_addr_ret_val_radiusExtServerAddress;
    tSNMP_OCTET_STRING_TYPE *poctet_retval_radiusExtServerSharedSecret = NULL;
   /*** $$TRACE_LOG (ENTRY,"radiusExtServerTableGet Routine\n"); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_arg = %d\n",u1_arg); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_search_type = %d\n",u1_search_type); ***/

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_radiusExtServerTable_INDEX = p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_radiusExtServerTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_radiusExtServerIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceRadiusExtServerTable
                     (i4_radiusExtServerIndex)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_radiusExtServerIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexRadiusExtServerTable
                     (&i4_radiusExtServerIndex)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_radiusExtServerIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_radiusExtServerIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexRadiusExtServerTable
                     (i4_radiusExtServerIndex,
                      &i4_next_radiusExtServerIndex)) == SNMP_SUCCESS)
                {
                    i4_radiusExtServerIndex = i4_next_radiusExtServerIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_radiusExtServerIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case RADIUSEXTSERVERINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == EXACT) || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_radiusExtServerIndex;
            }
            else
            {
                i4_return_val = i4_next_radiusExtServerIndex;
            }
            break;
        }
        case RADIUSEXTSERVERADDRESS:
        {
            i1_ret_val =
                nmhGetRadiusExtServerAddress (i4_radiusExtServerIndex,
                                              &u4_addr_ret_val_radiusExtServerAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                CRU_BMC_DWTOPDU (u1_octet_string,
                                 u4_addr_ret_val_radiusExtServerAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case RADIUSEXTSERVERTYPE:
        {
            i1_ret_val =
                nmhGetRadiusExtServerType (i4_radiusExtServerIndex,
                                           &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case RADIUSEXTSERVERSHAREDSECRET:
        {
            poctet_retval_radiusExtServerSharedSecret =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (256);
            if (poctet_retval_radiusExtServerSharedSecret == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetRadiusExtServerSharedSecret (i4_radiusExtServerIndex,
                                                   poctet_retval_radiusExtServerSharedSecret);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_radiusExtServerSharedSecret;
            }
            else
            {
                free_octetstring (poctet_retval_radiusExtServerSharedSecret);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case RADIUSEXTSERVERENABLED:
        {
            i1_ret_val =
                nmhGetRadiusExtServerEnabled (i4_radiusExtServerIndex,
                                              &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case RADIUSEXTSERVERRESPONSETIME:
        {
            i1_ret_val =
                nmhGetRadiusExtServerResponseTime (i4_radiusExtServerIndex,
                                                   &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case RADIUSEXTSERVERMAXIMUMRETRANSMISSION:
        {
            i1_ret_val =
                nmhGetRadiusExtServerMaximumRetransmission
                (i4_radiusExtServerIndex, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case RADIUSEXTSERVERENTRYSTATUS:
        {
            i1_ret_val =
                nmhGetRadiusExtServerEntryStatus (i4_radiusExtServerIndex,
                                                  &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

   /*** $$TRACE_LOG (EXIT,"  i2_type = %d\n",i2_type); ***/
   /*** $$TRACE_LOG (EXIT,"  u4_counter_val = %u\n",u4_counter_val); ***/
   /*** $$TRACE_LOG (EXIT,"  i2_type = %d\n",i4_return_val); ***/

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : radiusExtServerEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
#ifdef __STDC__
INT4
radiusExtServerEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                         UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
#else
INT4
radiusExtServerEntrySet (p_in_db, p_incoming, u1_arg, p_value)
     tSNMP_OID_TYPE     *p_in_db;
     tSNMP_OID_TYPE     *p_incoming;
     UINT1               u1_arg;
     tSNMP_MULTI_DATA_TYPE *p_value;
#endif
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    INT4                i4_radiusExtServerIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
    UINT4               u4_addr_val_radiusExtServerAddress;
   /*** $$TRACE_LOG (ENTRY,"radiusExtServerTableSet Routine\n"); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_arg = %d\n",u1_arg); ***/

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_INCONSISTENT_NAME);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_radiusExtServerIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case RADIUSEXTSERVERADDRESS:
        {
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_radiusExtServerAddress =
                CRU_BMC_DWFROMPDU (u1_octet_string);
            i1_ret_val =
                nmhSetRadiusExtServerAddress (i4_radiusExtServerIndex,
                                              u4_addr_val_radiusExtServerAddress);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case RADIUSEXTSERVERTYPE:
        {
            i1_ret_val =
                nmhSetRadiusExtServerType (i4_radiusExtServerIndex,
                                           p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case RADIUSEXTSERVERSHAREDSECRET:
        {
            i1_ret_val =
                nmhSetRadiusExtServerSharedSecret (i4_radiusExtServerIndex,
                                                   p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case RADIUSEXTSERVERENABLED:
        {
            i1_ret_val =
                nmhSetRadiusExtServerEnabled (i4_radiusExtServerIndex,
                                              p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case RADIUSEXTSERVERRESPONSETIME:
        {
            i1_ret_val =
                nmhSetRadiusExtServerResponseTime (i4_radiusExtServerIndex,
                                                   p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case RADIUSEXTSERVERMAXIMUMRETRANSMISSION:
        {
            i1_ret_val =
                nmhSetRadiusExtServerMaximumRetransmission
                (i4_radiusExtServerIndex, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case RADIUSEXTSERVERENTRYSTATUS:
        {
            i1_ret_val =
                nmhSetRadiusExtServerEntryStatus (i4_radiusExtServerIndex,
                                                  p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case RADIUSEXTSERVERINDEX:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : radiusExtServerEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

#ifdef __STDC__
INT4
radiusExtServerEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                          UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
#else
INT4
radiusExtServerEntryTest (p_in_db, p_incoming, u1_arg, p_value)
     tSNMP_OID_TYPE     *p_in_db;
     tSNMP_OID_TYPE     *p_incoming;
     UINT1               u1_arg;

     tSNMP_MULTI_DATA_TYPE *p_value;
#endif
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    INT4                i4_radiusExtServerIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
    UINT4               u4_addr_val_radiusExtServerAddress;
   /*** $$TRACE_LOG (ENTRY,"radiusExtServerTableTest Routine\n"); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_arg = %d\n",u1_arg); ***/

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_INCONSISTENT_NAME);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_radiusExtServerIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION */
    /*

       if ((i1_ret_val =
       nmhValidateIndexInstanceRadiusExtServerTable (i4_radiusExtServerIndex))
       != SNMP_SUCCESS)
       {
       return SNMP_ERR_INCONSISTENT_NAME;
       }
     */
    switch (u1_arg)
    {

        case RADIUSEXTSERVERADDRESS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_IP_ADDR_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_radiusExtServerAddress =
                CRU_BMC_DWFROMPDU (u1_octet_string);

            i1_ret_val =
                nmhTestv2RadiusExtServerAddress (&u4ErrorCode,
                                                 i4_radiusExtServerIndex,
                                                 u4_addr_val_radiusExtServerAddress);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case RADIUSEXTSERVERTYPE:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2RadiusExtServerType (&u4ErrorCode,
                                              i4_radiusExtServerIndex,
                                              p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case RADIUSEXTSERVERSHAREDSECRET:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_OCTET_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2RadiusExtServerSharedSecret (&u4ErrorCode,
                                                      i4_radiusExtServerIndex,
                                                      p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case RADIUSEXTSERVERENABLED:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2RadiusExtServerEnabled (&u4ErrorCode,
                                                 i4_radiusExtServerIndex,
                                                 p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case RADIUSEXTSERVERRESPONSETIME:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2RadiusExtServerResponseTime (&u4ErrorCode,
                                                      i4_radiusExtServerIndex,
                                                      p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case RADIUSEXTSERVERMAXIMUMRETRANSMISSION:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2RadiusExtServerMaximumRetransmission (&u4ErrorCode,
                                                               i4_radiusExtServerIndex,
                                                               p_value->
                                                               i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case RADIUSEXTSERVERENTRYSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2RadiusExtServerEntryStatus (&u4ErrorCode,
                                                     i4_radiusExtServerIndex,
                                                     p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case RADIUSEXTSERVERINDEX:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : radiusExtClientGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
#ifdef __STDC__
tSNMP_VAR_BIND     *
radiusExtClientGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                    UINT1 u1_arg, UINT1 u1_search_type)
#else
tSNMP_VAR_BIND     *
radiusExtClientGet (p_in_db, p_incoming, u1_arg, u1_search_type)
     tSNMP_OID_TYPE     *p_in_db;
     tSNMP_OID_TYPE     *p_incoming;
     UINT1               u1_arg;
     UINT1               u1_search_type;
#endif
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Scalar get routine. */

    UINT1               i1_ret_val = FALSE;
    INT4                LEN_radiusExtClient_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val;
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  This Variable is declared for being used in the
     *  FOR Loop for extracting Indices from OID Given.
     */

   /*** $$TRACE_LOG (ENTRY,"radiusExtClientGet Routine\n"); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_arg = %d\n",u1_arg); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_search_type = %d\n",u1_search_type); ***/

/*** DECLARATION_END ***/

    LEN_radiusExtClient_INDEX = p_in_db->u4_Length;

    /*  Incrementing the Length for the Extract of Scalar Tables. */
    LEN_radiusExtClient_INDEX++;
    if (u1_search_type == EXACT)
    {
        if ((LEN_radiusExtClient_INDEX != (INT4) p_incoming->u4_Length)
            || (p_incoming->pu4_OidList[p_incoming->u4_Length - 1] != 0))
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    else
    {
        /*  Get Next Operation on the Scalar Variable.  */
        if ((INT4) p_incoming->u4_Length >= LEN_radiusExtClient_INDEX)
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    switch (u1_arg)
    {
        case RADIUSEXTDEBUGMASK:
        {
            i1_ret_val = nmhGetRadiusExtDebugMask (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case RADIUSMAXNOOFUSERENTRIES:
        {
            i1_ret_val = nmhGetRadiusMaxNoOfUserEntries (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    /* Incrementing the Length of the p_in_db. */
    p_in_db->u4_Length++;
    /* Adding the .0 to the p_in_db for scalar Objects. */
    p_in_db->pu4_OidList[p_in_db->u4_Length - 1] = ZERO;
   /*** $$TRACE_LOG (EXIT,"  i2_type = %d\n",i2_type); ***/
   /*** $$TRACE_LOG (EXIT,"  u4_counter_val = %u\n",u4_counter_val); ***/
   /*** $$TRACE_LOG (EXIT,"  i2_type = %d\n",i4_return_val); ***/

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : radiusExtClientSet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
#ifdef __STDC__
INT4
radiusExtClientSet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                    UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
#else
INT4
radiusExtClientSet (p_in_db, p_incoming, u1_arg, p_value)
     tSNMP_OID_TYPE     *p_in_db;
     tSNMP_OID_TYPE     *p_incoming;
     UINT1               u1_arg;
     tSNMP_MULTI_DATA_TYPE *p_value;
#endif
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
   /*** $$TRACE_LOG (ENTRY,"radiusExtClientSet Routine\n"); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_arg = %d\n",u1_arg); ***/

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_INCONSISTENT_NAME);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case RADIUSEXTDEBUGMASK:
        {
            i1_ret_val = nmhSetRadiusExtDebugMask (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case RADIUSMAXNOOFUSERENTRIES:
        {
            i1_ret_val =
                nmhSetRadiusMaxNoOfUserEntries (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : radiusExtClientTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

#ifdef __STDC__
INT4
radiusExtClientTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                     UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
#else
INT4
radiusExtClientTest (p_in_db, p_incoming, u1_arg, p_value)
     tSNMP_OID_TYPE     *p_in_db;
     tSNMP_OID_TYPE     *p_incoming;
     UINT1               u1_arg;

     tSNMP_MULTI_DATA_TYPE *p_value;
#endif
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
   /*** $$TRACE_LOG (ENTRY,"radiusExtClientTest Routine\n"); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_arg = %d\n",u1_arg); ***/

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_INCONSISTENT_NAME);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }
    switch (u1_arg)
    {

        case RADIUSEXTDEBUGMASK:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2RadiusExtDebugMask (&u4ErrorCode,
                                             p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case RADIUSMAXNOOFUSERENTRIES:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2RadiusMaxNoOfUserEntries (&u4ErrorCode,
                                                   p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */
