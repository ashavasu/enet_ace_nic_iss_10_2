
/* Proto Validate Index Instance for RadiusAccServerTable. */
INT1
nmhValidateIndexInstanceRadiusAccServerTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for RadiusAccServerTable  */

INT1
nmhGetFirstIndexRadiusAccServerTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexRadiusAccServerTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetRadiusAccServerAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusAccClientServerPortNumber ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetRadiusAccClientRoundTripTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusAccClientRequests ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusAccClientRetransmissions ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusAccClientResponses ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusAccClientMalformedResponses ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusAccClientBadAuthenticators ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusAccClientPendingRequests ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusAccClientTimeouts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusAccClientUnknownTypes ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusAccClientPacketsDropped ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetRadiusAccClientInvalidServerAddresses ARG_LIST((UINT4 *));

INT1
nmhGetRadiusAccClientIdentifier ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
