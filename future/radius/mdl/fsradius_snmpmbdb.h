
/*****
  snmpmbdb.h . This File Contains the Mib DataBase.
*****/

#ifndef FSRADIUS_SNMP_MBDB_H
#define FSRADIUS_SNMP_MBDB_H
/*  The NULL FUNCTION POINTER.  */
# define NULLF ARG_LIST(((INT4 (*)(tSNMP_OID_TYPE *,\
                                          tSNMP_OID_TYPE *,\
                                          UINT1,\
                                          tSNMP_MULTI_DATA_TYPE *)) NULL))
/*  NULL VARBIND FUNCTION POINTER . */
# define NULLVBF  ARG_LIST(((tSNMP_VAR_BIND* (*)(tSNMP_OID_TYPE *,\
                                           tSNMP_OID_TYPE *,\
                                           UINT1,\
                                           UINT1))NULL))
# include "fsradmid.h"
# include "fsradcon.h"
# include "fsradogi.h"

/*  The Declaration of the Group Arrays. */

UINT4 au4_fsradius_snmp_TABLE1[] = {1,3,6,1,4,1,2076,25,1};

/*  The Declaration of the SubGroup Arrays. */

UINT4  au4_SNMP_OGP_RADIUSEXTSERVERTABLE_OID [] ={3,1};
UINT4  au4_SNMP_OGP_RADIUSEXTCLIENT_OID [] ={0};

/*  Declaration of Group OID Table. */
/* Each entry contains Length of Group OID, Pointer to the Group OID,
   Priority of the registering subagent, Timeout for response,
   and Number of Subgroups in that order */

const tSNMP_GroupOIDType fsradius_FMAS_GroupOIDTable[] =
{
   {9 , au4_fsradius_snmp_TABLE1 , 1 , 10 , 2}
};
/*  Declaration of Base OID Table. */
/* Each entry contains Length of Base OID, Pointer to the Base OID,
   Middle level get function pointer, Middle level test function pointer,
   Middle level set function pointer and Number of objects in that table
   in that order */

const tSNMP_BaseOIDType  fsradius_FMAS_BaseOIDTable[] = {
{
0,
au4_SNMP_OGP_RADIUSEXTCLIENT_OID,
radiusExtClientGet,
radiusExtClientTest,
radiusExtClientSet,
2
},
{
2,
au4_SNMP_OGP_RADIUSEXTSERVERTABLE_OID,
radiusExtServerEntryGet,
radiusExtServerEntryTest,
radiusExtServerEntrySet,
8
}
};
/* Declaration of MIB Object Table. */
/* Each entry contains Name of the table, Permissions for
   the object and Object name in that order */

const tSNMP_MIBObjectDescrType  fsradius_FMAS_MIBObjectTable[] = {
{
 SNMP_OGP_INDEX_RADIUSEXTCLIENT,
 READ_WRITE,
 RADIUSEXTDEBUGMASK
},
{
 SNMP_OGP_INDEX_RADIUSEXTCLIENT,
 READ_WRITE,
 RADIUSMAXNOOFUSERENTRIES
},
{
 SNMP_OGP_INDEX_RADIUSEXTSERVERTABLE,
 READ_ONLY,
 RADIUSEXTSERVERINDEX
},
{
 SNMP_OGP_INDEX_RADIUSEXTSERVERTABLE,
 READ_WRITE,
 RADIUSEXTSERVERADDRESS
},
{
 SNMP_OGP_INDEX_RADIUSEXTSERVERTABLE,
 READ_WRITE,
 RADIUSEXTSERVERTYPE
},
{
 SNMP_OGP_INDEX_RADIUSEXTSERVERTABLE,
 READ_WRITE,
 RADIUSEXTSERVERSHAREDSECRET
},
{
 SNMP_OGP_INDEX_RADIUSEXTSERVERTABLE,
 READ_WRITE,
 RADIUSEXTSERVERENABLED
},
{
 SNMP_OGP_INDEX_RADIUSEXTSERVERTABLE,
 READ_WRITE,
 RADIUSEXTSERVERRESPONSETIME
},
{
 SNMP_OGP_INDEX_RADIUSEXTSERVERTABLE,
 READ_WRITE,
 RADIUSEXTSERVERMAXIMUMRETRANSMISSION
},
{
 SNMP_OGP_INDEX_RADIUSEXTSERVERTABLE,
 READ_WRITE,
 RADIUSEXTSERVERENTRYSTATUS
}
};

const tSNMP_GLOBAL_STRUCT fsradius_FMAS_Global_data =
{sizeof (fsradius_FMAS_GroupOIDTable) / sizeof (tSNMP_GroupOIDType),
 sizeof (fsradius_FMAS_BaseOIDTable) / sizeof (tSNMP_BaseOIDType)};

const int fsradius_MAX_OBJECTS =
(sizeof (fsradius_FMAS_MIBObjectTable) / sizeof (tSNMP_MIBObjectDescrType));

#endif  /* FSRADIUS_SNMP_MBDB_H. */

