#ifndef _RADACCWR_H
#define _RADACCWR_H

VOID RegisterRADACC(VOID);
INT4 RadiusAccClientInvalidServerAddressesGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAccClientIdentifierGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexRadiusAccServerTable(tSnmpIndex *, tSnmpIndex *);
INT4 RadiusAccServerIndexGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAccServerAddressGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAccClientServerPortNumberGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAccClientRoundTripTimeGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAccClientRequestsGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAccClientRetransmissionsGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAccClientResponsesGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAccClientMalformedResponsesGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAccClientBadAuthenticatorsGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAccClientPendingRequestsGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAccClientTimeoutsGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAccClientUnknownTypesGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAccClientPacketsDroppedGet(tSnmpIndex *, tRetVal *);
#endif /* _RADACCWR_H */
