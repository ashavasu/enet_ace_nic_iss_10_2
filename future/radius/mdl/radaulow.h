
/* Proto Validate Index Instance for RadiusAuthServerTable. */
INT1
nmhValidateIndexInstanceRadiusAuthServerTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for RadiusAuthServerTable  */

INT1
nmhGetFirstIndexRadiusAuthServerTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexRadiusAuthServerTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetRadiusAuthServerAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusAuthClientServerPortNumber ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetRadiusAuthClientRoundTripTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusAuthClientAccessRequests ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusAuthClientAccessRetransmissions ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusAuthClientAccessAccepts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusAuthClientAccessRejects ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusAuthClientAccessChallenges ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusAuthClientMalformedAccessResponses ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusAuthClientBadAuthenticators ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusAuthClientPendingRequests ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusAuthClientTimeouts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusAuthClientUnknownTypes ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRadiusAuthClientPacketsDropped ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetRadiusAuthClientInvalidServerAddresses ARG_LIST((UINT4 *));

INT1
nmhGetRadiusAuthClientIdentifier ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
