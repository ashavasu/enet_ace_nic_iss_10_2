#ifndef _FSRADIWR_H
#define _FSRADIWR_H

VOID RegisterFSRADI(VOID);
INT4 RadiusExtDebugMaskGet(tSnmpIndex *, tRetVal *);
INT4 RadiusMaxNoOfUserEntriesGet(tSnmpIndex *, tRetVal *);
INT4 RadiusExtDebugMaskSet(tSnmpIndex *, tRetVal *);
INT4 RadiusMaxNoOfUserEntriesSet(tSnmpIndex *, tRetVal *);
INT4 RadiusExtDebugMaskTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 RadiusMaxNoOfUserEntriesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 RadiusExtDebugMaskDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 RadiusMaxNoOfUserEntriesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);


INT4 GetNextIndexRadiusExtServerTable(tSnmpIndex *, tSnmpIndex *);
INT4 RadiusExtServerIndexGet(tSnmpIndex *, tRetVal *);
INT4 RadiusExtServerAddressGet(tSnmpIndex *, tRetVal *);
INT4 RadiusExtServerTypeGet(tSnmpIndex *, tRetVal *);
INT4 RadiusExtServerSharedSecretGet(tSnmpIndex *, tRetVal *);
INT4 RadiusExtServerEnabledGet(tSnmpIndex *, tRetVal *);
INT4 RadiusExtServerResponseTimeGet(tSnmpIndex *, tRetVal *);
INT4 RadiusExtServerMaximumRetransmissionGet(tSnmpIndex *, tRetVal *);
INT4 RadiusExtServerEntryStatusGet(tSnmpIndex *, tRetVal *);
INT4 RadiusExtServerAddressSet(tSnmpIndex *, tRetVal *);
INT4 RadiusExtServerTypeSet(tSnmpIndex *, tRetVal *);
INT4 RadiusExtServerSharedSecretSet(tSnmpIndex *, tRetVal *);
INT4 RadiusExtServerEnabledSet(tSnmpIndex *, tRetVal *);
INT4 RadiusExtServerResponseTimeSet(tSnmpIndex *, tRetVal *);
INT4 RadiusExtServerMaximumRetransmissionSet(tSnmpIndex *, tRetVal *);
INT4 RadiusExtServerEntryStatusSet(tSnmpIndex *, tRetVal *);
INT4 RadiusExtServerAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 RadiusExtServerTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 RadiusExtServerSharedSecretTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 RadiusExtServerEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 RadiusExtServerResponseTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 RadiusExtServerMaximumRetransmissionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 RadiusExtServerEntryStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 RadiusExtServerTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);







#endif /* _FSRADIWR_H */
