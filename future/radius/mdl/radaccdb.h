/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: radaccdb.h,v 1.5 2015/04/28 12:26:21 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _RADACCDB_H
#define _RADACCDB_H

UINT1 RadiusAccServerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 radacc [] ={1,3,6,1,2,1,67,2,2};
tSNMP_OID_TYPE radaccOID = {9, radacc};


UINT4 RadiusAccClientInvalidServerAddresses [ ] ={1,3,6,1,2,1,67,2,2,1,1,1};
UINT4 RadiusAccClientIdentifier [ ] ={1,3,6,1,2,1,67,2,2,1,1,2};
UINT4 RadiusAccServerIndex [ ] ={1,3,6,1,2,1,67,2,2,1,1,3,1,1};
UINT4 RadiusAccServerAddress [ ] ={1,3,6,1,2,1,67,2,2,1,1,3,1,2};
UINT4 RadiusAccClientServerPortNumber [ ] ={1,3,6,1,2,1,67,2,2,1,1,3,1,3};
UINT4 RadiusAccClientRoundTripTime [ ] ={1,3,6,1,2,1,67,2,2,1,1,3,1,4};
UINT4 RadiusAccClientRequests [ ] ={1,3,6,1,2,1,67,2,2,1,1,3,1,5};
UINT4 RadiusAccClientRetransmissions [ ] ={1,3,6,1,2,1,67,2,2,1,1,3,1,6};
UINT4 RadiusAccClientResponses [ ] ={1,3,6,1,2,1,67,2,2,1,1,3,1,7};
UINT4 RadiusAccClientMalformedResponses [ ] ={1,3,6,1,2,1,67,2,2,1,1,3,1,8};
UINT4 RadiusAccClientBadAuthenticators [ ] ={1,3,6,1,2,1,67,2,2,1,1,3,1,9};
UINT4 RadiusAccClientPendingRequests [ ] ={1,3,6,1,2,1,67,2,2,1,1,3,1,10};
UINT4 RadiusAccClientTimeouts [ ] ={1,3,6,1,2,1,67,2,2,1,1,3,1,11};
UINT4 RadiusAccClientUnknownTypes [ ] ={1,3,6,1,2,1,67,2,2,1,1,3,1,12};
UINT4 RadiusAccClientPacketsDropped [ ] ={1,3,6,1,2,1,67,2,2,1,1,3,1,13};


tMbDbEntry radaccMibEntry[]= {

{{12,RadiusAccClientInvalidServerAddresses}, NULL, RadiusAccClientInvalidServerAddressesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,RadiusAccClientIdentifier}, NULL, RadiusAccClientIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{14,RadiusAccServerIndex}, GetNextIndexRadiusAccServerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, RadiusAccServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAccServerAddress}, GetNextIndexRadiusAccServerTable, RadiusAccServerAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, RadiusAccServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAccClientServerPortNumber}, GetNextIndexRadiusAccServerTable, RadiusAccClientServerPortNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, RadiusAccServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAccClientRoundTripTime}, GetNextIndexRadiusAccServerTable, RadiusAccClientRoundTripTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, RadiusAccServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAccClientRequests}, GetNextIndexRadiusAccServerTable, RadiusAccClientRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, RadiusAccServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAccClientRetransmissions}, GetNextIndexRadiusAccServerTable, RadiusAccClientRetransmissionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, RadiusAccServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAccClientResponses}, GetNextIndexRadiusAccServerTable, RadiusAccClientResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, RadiusAccServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAccClientMalformedResponses}, GetNextIndexRadiusAccServerTable, RadiusAccClientMalformedResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, RadiusAccServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAccClientBadAuthenticators}, GetNextIndexRadiusAccServerTable, RadiusAccClientBadAuthenticatorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, RadiusAccServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAccClientPendingRequests}, GetNextIndexRadiusAccServerTable, RadiusAccClientPendingRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, RadiusAccServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAccClientTimeouts}, GetNextIndexRadiusAccServerTable, RadiusAccClientTimeoutsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, RadiusAccServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAccClientUnknownTypes}, GetNextIndexRadiusAccServerTable, RadiusAccClientUnknownTypesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, RadiusAccServerTableINDEX, 1, 0, 0, NULL},

{{14,RadiusAccClientPacketsDropped}, GetNextIndexRadiusAccServerTable, RadiusAccClientPacketsDroppedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, RadiusAccServerTableINDEX, 1, 0, 0, NULL},
};
tMibData radaccEntry = { 15, radaccMibEntry };
#endif /* _RADACCDB_H */

