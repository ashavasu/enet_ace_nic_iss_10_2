
/*  Prototype for Get Test & Set for radiusAccServerTable.  */
tSNMP_VAR_BIND*
radiusAccServerEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));


/*  Prototype for Get Test & Set for radiusAccClient.  */
tSNMP_VAR_BIND*
radiusAccClientGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));

