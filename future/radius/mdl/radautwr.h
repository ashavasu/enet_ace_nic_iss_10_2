#ifndef _RADAUTWR_H
#define _RADAUTWR_H

VOID RegisterRADAUT(VOID);
INT4 RadiusAuthClientInvalidServerAddressesGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAuthClientIdentifierGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexRadiusAuthServerTable(tSnmpIndex *, tSnmpIndex *);
INT4 RadiusAuthServerIndexGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAuthServerAddressGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAuthClientServerPortNumberGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAuthClientRoundTripTimeGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAuthClientAccessRequestsGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAuthClientAccessRetransmissionsGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAuthClientAccessAcceptsGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAuthClientAccessRejectsGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAuthClientAccessChallengesGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAuthClientMalformedAccessResponsesGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAuthClientBadAuthenticatorsGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAuthClientPendingRequestsGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAuthClientTimeoutsGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAuthClientUnknownTypesGet(tSnmpIndex *, tRetVal *);
INT4 RadiusAuthClientPacketsDroppedGet(tSnmpIndex *, tRetVal *);
#endif /* _RADAUTWR_H */
