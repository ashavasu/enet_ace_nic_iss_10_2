/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radmac.h,v 1.16 2015/04/28 12:26:21 siva Exp $ 
 *
 * Description: This file contains the macro definitions
 *
 *******************************************************************/
 
#ifndef RADMAC_H
#define RADMAC_H

#define EXP01_EVENT            (0x01000000)


#define RAD_EAP_CODE_REQUEST   1 
#define RAD_EAP_CODE_RESPONSE  2 
#define RAD_EAP_CODE_SUCCESS   3 
#define RAD_EAP_CODE_FAILURE   4 

#define RAD_MIN_UDP_PORT       0 
#define RAD_MAX_UDP_PORT       65535
#define RAD_RECV_BUF_SIZE   4096
extern tTimerListId    p_TimerListId; /* Timer List ID */


#define RAD_ERROR                 0   
#define RAD_FOREVER()         for(;;) 

#define RADIUS_MESSAGE_DIGEST(out,in,len) md5_calc(out,in,len)
extern UINT1       gau1ZeroHostName[DNS_MAX_QUERY_LEN];
extern UINT4       gRadDebugMask;
extern INT4       gi4RadiusProxyStatus;
#define RAD_DEB_MASK    gRadDebugMask
#define RADIUS_PROXY_STATUS()    gi4RadiusProxyStatus

#define RAD_NO_DEBUG        0x00000000
#define RAD_DEBUG_ALL       0x000fffff
#define RAD_ERR_MASK        0x00000001
#define RAD_TIMER_MASK      0x00000002
#define RAD_RESP_MASK       0x00000004
#define RAD_PACKET_MASK     0x00000008
#define RAD_EVENT_MASK      0x00000010


#define RAD_EVENT_NAME      "\nRAD"
#define RAD_ERR_NAME        "\nRAD!!!"
#define RAD_TIMER_NAME      "\nRAD"
#define RAD_RESP_NAME       "\nRAD"
#define RAD_PACKET_NAME     ""

#ifdef TRACE_WANTED 


#define RAD_EVENT_PRINT(format)\
    RAD_MACRO_PRINT(RAD_EVENT_MASK,RAD_EVENT_NAME,format)
#define RAD_EVENT_PRINT1(format,arg1)\
    RAD_MACRO_PRINT1(RAD_EVENT_MASK,RAD_EVENT_NAME,format,arg1)
#define RAD_EVENT_PRINT2(format,arg1,arg2)\
    RAD_MACRO_PRINT2(RAD_EVENT_MASK,RAD_EVENT_NAME,format,arg1,arg2)
#define RAD_EVENT_PRINT3(format,arg1,arg2,arg3)\
    RAD_MACRO_PRINT3(RAD_EVENT_MASK,RAD_EVENT_NAME,format,arg1,arg2,arg3)
    

#define RAD_ERR_PRINT(format)\
    RAD_MACRO_PRINT(RAD_ERR_MASK,RAD_ERR_NAME,format)
#define RAD_ERR_PRINT1(format,arg1)\
    RAD_MACRO_PRINT1(RAD_ERR_MASK,RAD_ERR_NAME,format,arg1)
#define RAD_ERR_PRINT2(format,arg1,arg2)\
    RAD_MACRO_PRINT2(RAD_ERR_MASK,RAD_ERR_NAME,format,arg1,arg2)
#define RAD_ERR_PRINT3(format,arg1,arg2,arg3)\
    RAD_MACRO_PRINT3(RAD_ERR_MASK,RAD_ERR_NAME,format,arg1,arg2,arg3)

#define RAD_TIMER_PRINT(format)\
    RAD_MACRO_PRINT(RAD_TIMER_MASK,RAD_TIMER_NAME,format)
#define RAD_TIMER_PRINT1(format,arg1)\
    RAD_MACRO_PRINT1(RAD_TIMER_MASK,RAD_TIMER_NAME,format,arg1)
#define RAD_TIMER_PRINT2(format,arg1,arg2)\
    RAD_MACRO_PRINT2(RAD_TIMER_MASK,RAD_TIMER_NAME,format,arg1,arg2)
#define RAD_TIMER_PRINT3(format,arg1,arg2,arg3)\
    RAD_MACRO_PRINT3(RAD_TIMER_MASK,RAD_TIMER_NAME,format,arg1,arg2,arg3)


#define RAD_PACKET_PRINT    if(RAD_PACKET_MASK & RAD_DEB_MASK) printf
#define RAD_PACKET_PRINT1   if(RAD_PACKET_MASK & RAD_DEB_MASK) printf
#define RAD_PACKET_PRINT2   if(RAD_PACKET_MASK & RAD_DEB_MASK) printf
#define RAD_PACKET_PRINT3   if(RAD_PACKET_MASK & RAD_DEB_MASK) printf

#define RAD_RESPONSE_PRINT(format)\
    RAD_MACRO_PRINT(RAD_RESP_MASK,RAD_RESP_NAME,format)
#define RAD_RESPONSE_PRINT1(format,arg1)\
    RAD_MACRO_PRINT1(RAD_RESP_MASK,RAD_RESP_NAME,format,arg1)
#define RAD_RESPONSE_PRINT2(format,arg1,arg2)\
    RAD_MACRO_PRINT2(RAD_RESP_MASK,RAD_RESP_NAME,format,arg1,arg2)
#define RAD_RESPONSE_PRINT3(format,arg1,arg2,arg3)\
    RAD_MACRO_PRINT3(RAD_RESP_MASK,RAD_RESP_NAME,format,arg1,arg2,arg3)
/**************************************/

#define RAD_MACRO_PRINT(MASK,NAME,format)\
    UtlTrcLog(RAD_DEB_MASK,MASK,NAME,format)

#define RAD_MACRO_PRINT1(MASK,NAME,format,arg1)\
    UtlTrcLog(RAD_DEB_MASK,MASK,NAME,format,arg1)

#define RAD_MACRO_PRINT2(MASK,NAME,format,arg1,arg2)\
    UtlTrcLog(RAD_DEB_MASK,MASK,NAME,format,arg1,arg2)

#define RAD_MACRO_PRINT3(MASK,NAME,format,arg1,arg2,arg3)\
    UtlTrcLog(RAD_DEB_MASK,MASK,NAME,format,arg1,arg2,arg3)

    

#else   /* For disabling debug statements */
#define RAD_EVENT_PRINT(a)  
#define RAD_EVENT_PRINT1(a,b) \
{ \
  UNUSED_PARAM (b); \
}
#define RAD_EVENT_PRINT2(a,b,c) \
{ \
  UNUSED_PARAM (b); \
  UNUSED_PARAM (c); \
}
#define RAD_EVENT_PRINT3(a,b,c,d)  \
{ \
  UNUSED_PARAM (b); \
  UNUSED_PARAM (c); \
  UNUSED_PARAM (d); \
}
#define RAD_ERR_PRINT(a)
#define RAD_ERR_PRINT1(a,b)
#define RAD_ERR_PRINT2(a,b,c)
#define RAD_ERR_PRINT3(a,b,c,d)
#define RAD_TIMER_PRINT(a)
#define RAD_TIMER_PRINT1(a,b)
#define RAD_TIMER_PRINT2(a,b,c)
#define RAD_TIMER_PRINT3(a,b,c,d)
#define RAD_PACKET_PRINT(a)
#define RAD_PACKET_PRINT1(a,b)\
{ \
  UNUSED_PARAM (b);\
}
#define RAD_PACKET_PRINT2(a,b,c)
#define RAD_PACKET_PRINT3(a,b,c,d)
#define RAD_RESPONSE_PRINT(a)
#define RAD_RESPONSE_PRINT1(a,b)
#define RAD_RESPONSE_PRINT2(a,b,c)
#define RAD_RESPONSE_PRINT3(a,b,c,d)
#endif  /* For disabling debug statements */

#define RAD_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)
#define RAD_HEX_DUMP(p,len)   RadUtilHexDump(p,len) 

#define RADIUS_IPVX_LENGTH_FROM_TYPE(t) \
    (((t) == IPVX_ADDR_FMLY_IPV4 )?\
     IPVX_IPV4_ADDR_LEN:IPVX_IPV6_ADDR_LEN)
     
#define RADIUS_IPVX_TYPE_FROM_LENGTH(t) \
    (((t) == IPVX_IPV4_ADDR_LEN)?\
     IPVX_ADDR_FMLY_IPV4:IPVX_ADDR_FMLY_IPV6)


#endif  /* RADMAC_H */
