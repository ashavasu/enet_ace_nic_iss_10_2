/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radtdfs.h,v 1.1 2015/04/28 12:26:21 siva Exp $
 *
 * Description:This file includes all the type definitions for   
 *             RADIUS Client Software 
 *                    
 *******************************************************************/
/* Data Structure for RADIUS input for Authentication */

#include "radius.h"


typedef struct userq {
   tRADIUS_SERVER   *ptRadiusServer;              /* Pointer of the server used
                                                     provided by the user
                                                     module.*/
   tRADIUS_INPUT_AUTH userInfoAuth;               /* Structure to store the Authentication info of the user */
   tRADIUS_INPUT_ACC userInfoAcc;                 /* Structure to store the Accouting info of the user*/
   void    (*CallBackfPtr)(void *);               /* Call Back function pointer for this user */
   UINT1   *p_u1PacketTransmitted;                /* Pointer to the RADIUS packet transmitted */
   UINT4    ifIndex;                              /* ifIndex or the identification on number of the user module */
   UINT4    u4SendTime;                           /* send time of the request*/

   UINT4    a_u4Utf8UserName[LEN_UTF8_USER_NAME];  /* Used when multilingual characters are available
                                                    * '\0' is must at the end of multilingual string
                                                    */
   UINT4    u4NumServersContacted;                  /* Number of Radius Servers
                                                    * contacted to transmit the
                                                    * request*/
   
   UINT1    a_u1UserName[LEN_USER_NAME];           /* Name of the user, alligned 
                                                     to 4 bytes boundary*/
   UINT1    a_u1RequestAuth[LEN_REQ_AUTH_AUTH];    /* The request authenticator
                                                     transmitted, aligned to 4
                                                     bytes boundary */
   UINT1   u1_ReTxCount;                          /* No of maximum re
                                                     transmissions allowed */
   UINT1   u1_PacketIdentifier;                   /* Identifier of the packet
                                                     transmitted */
  
   UINT1   u1_IsUtf8EncodingReqd;                /* When a_u1Utf8UserName is used at
                                                  * that time this flag is must to be set
                                                  * 1:UTF-8 Encoding is required
                                                  * 0:UTF-8 Encoding isn't required
                                                  */
   UINT1   au1StaMac[MAC_ADDR_LEN];                          /*Staion Mac Address */
   UINT1   au1Align[3];

}tRADIUS_REQ_ENTRY;



typedef struct errorlog {
   UINT4   u4_ErrorPacketIdentifier;
   UINT4   u4_ErrorPacketLength;
   UINT4   u4_ErrorQueueCreationAuth;
   UINT4   u4_ErrorQueueCreationAcc;
   UINT4   u4_ErrorPacketCreationAuth;
   UINT4   u4_ErrorPacketCreationAcc;
   UINT4   u4_ErrorServerDisabledAuth;
   UINT4   u4_ErrorServerDisabledAcc;
   UINT4   u4_ErrorInputInsufficientAuth;
   UINT4   u4_ErrorInputInsufficientAcc;
   UINT4   u4_ErrorTransmissionAuth;
   UINT4   u4_ErrorTransmissionAcc;
   UINT4   u4_ErrorTimerNotStartedAuth;
   UINT4   u4_ErrorTimerNotStartedAcc;
   UINT4   u4_ErrorTimerNotStoped;
   UINT4   u4_ErrorMemPoolCreation;
   UINT4   u4_ErrorAuthSocketOpening;
   UINT4   u4_ErrorAccSocketOpening;
   UINT4   u4_ErrorAuthSocketBinding;
   UINT4   u4_ErrorAccSocketBinding;
}tRADIUS_ERROR_LOG;

typedef struct faultoperand {
   UINT4   u4_Item1;
   UINT4   u4_Item2;
   UINT4   u4_Item3;
   UINT4   u4_Item4;
   UINT4   u4_Item5;
   UINT4   u4_Item6;
   UINT4   u4_Item7;
   UINT4   u4_Item8;
   UINT4   u4_Item9;
   UINT4   u4_Item10;
   UINT4   u4_Item11;
   UINT4   u4_Item12;
   UINT4   u4_Item13;
   UINT4   u4_Item14;
   UINT4   u4_Item15;
   UINT4   u4_Item16;
   UINT4   u4_Item17;
   UINT4   u4_Item18;
   UINT4   u4_Item19;
   UINT4   u4_Item20;
   UINT4   u4_Item21;
   UINT2   u2_Item22;
   UINT2   u2Pad;
}tFAULT_OPERANDS;

typedef struct RadMempoolId {
    UINT4   u4UserEntryPoolId;
    UINT4   u4PacketPoolId;
    UINT4   u4RadiusServerPoolId; 
    UINT4   u4RadiusIfPoolId;
    UINT4   u4Radu2PoolId;
    UINT4   u4ServicePoolId;
    UINT4   u4UserEapPoolId;
    UINT4   u4UserChapPoolId;
    UINT4   u4UserPapPoolId;
    UINT4   u4UserOthersPoolId;
    UINT4   u4UserMsChapPoolId;
    UINT4   u4UserChapCpw1PoolId;
    UINT4   u4UserChapCpw2PoolId;
    UINT4   u4RadKeyPoolId;
    UINT4   u4RadTimerPoolId;
    UINT4   u4RadConcatPoolId;
    UINT4   u4RadPacketPoolId;
}tRadMemPoolId;

/* To handle CallBack for Radius module */

typedef union RadCallBackEntry {

    INT4 (*pRadUtilReadSecretFromNVRAM) (VOID);
    INT4 (*pRadUtilWriteSecretToNVRAM) (VOID);
    INT4 (*pRadCustSecureMemAccess) (UINT4, UINT4, ...);
}unRadCallBackEntry;

typedef struct a_u1Concatenated
{
UINT1 a_u1Concatenated[LEN_RX_PKT + LEN_SECRET];
}tu1Concatenated;
   
typedef struct a_u1RadiusPacketAuth
{
UINT1               a_u1RadiusPacketAuth[LEN_TX_PKT];
}tu1RadiusPacketAuth;

typedef struct RadiusGlobals 
{
    tRadHandleRadAuthCB    pRadHandleRadAuthCB;
    tRadHandleRadResCB     pRadHandleRadResCB;
}tRadiusGlobals;

#ifdef _RADMAIN_C
tRadiusGlobals gRadiusGlobals;
#else
extern tRadiusGlobals gRadiusGlobals;
#endif

 
/*************************** END OF FILE "radtdfs.h" ***********************/


