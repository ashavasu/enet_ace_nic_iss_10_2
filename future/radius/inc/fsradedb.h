/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsradedb.h,v 1.1 2015/04/28 12:26:21 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSRADEDB_H
#define _FSRADEDB_H

UINT1 FsRadExtServerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsRadExtAuthServerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsRadExtAccServerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsrade [] ={1,3,6,1,4,1,29601,2,30};
tSNMP_OID_TYPE fsradeOID = {9, fsrade};


UINT4 FsRadExtDebugMask [ ] ={1,3,6,1,4,1,29601,2,30,1,1,1};
UINT4 FsRadExtMaxNoOfUserEntries [ ] ={1,3,6,1,4,1,29601,2,30,1,1,2};
UINT4 FsRadExtPrimaryServerAddressType [ ] ={1,3,6,1,4,1,29601,2,30,1,1,3};
UINT4 FsRadExtPrimaryServer [ ] ={1,3,6,1,4,1,29601,2,30,1,1,4};
UINT4 FsRadExtServerIndex [ ] ={1,3,6,1,4,1,29601,2,30,1,1,5,1,1};
UINT4 FsRadExtServerAddrType [ ] ={1,3,6,1,4,1,29601,2,30,1,1,5,1,2};
UINT4 FsRadExtServerAddress [ ] ={1,3,6,1,4,1,29601,2,30,1,1,5,1,3};
UINT4 FsRadExtServerType [ ] ={1,3,6,1,4,1,29601,2,30,1,1,5,1,4};
UINT4 FsRadExtServerSharedSecret [ ] ={1,3,6,1,4,1,29601,2,30,1,1,5,1,5};
UINT4 FsRadExtServerEnabled [ ] ={1,3,6,1,4,1,29601,2,30,1,1,5,1,6};
UINT4 FsRadExtServerResponseTime [ ] ={1,3,6,1,4,1,29601,2,30,1,1,5,1,7};
UINT4 FsRadExtServerMaximumRetransmission [ ] ={1,3,6,1,4,1,29601,2,30,1,1,5,1,8};
UINT4 FsRadExtServerEntryStatus [ ] ={1,3,6,1,4,1,29601,2,30,1,1,5,1,9};
UINT4 FsRadExtAuthClientInvalidServerAddresses [ ] ={1,3,6,1,4,1,29601,2,30,1,2,1};
UINT4 FsRadExtAuthClientIdentifier [ ] ={1,3,6,1,4,1,29601,2,30,1,2,2};
UINT4 FsRadExtAuthServerIndex [ ] ={1,3,6,1,4,1,29601,2,30,1,2,3,1,1};
UINT4 FsRadExtAuthServerAddressType [ ] ={1,3,6,1,4,1,29601,2,30,1,2,3,1,2};
UINT4 FsRadExtAuthServerAddress [ ] ={1,3,6,1,4,1,29601,2,30,1,2,3,1,3};
UINT4 FsRadExtAuthClientServerPortNumber [ ] ={1,3,6,1,4,1,29601,2,30,1,2,3,1,4};
UINT4 FsRadExtAuthClientRoundTripTime [ ] ={1,3,6,1,4,1,29601,2,30,1,2,3,1,5};
UINT4 FsRadExtAuthClientAccessRequests [ ] ={1,3,6,1,4,1,29601,2,30,1,2,3,1,6};
UINT4 FsRadExtAuthClientAccessRetransmissions [ ] ={1,3,6,1,4,1,29601,2,30,1,2,3,1,7};
UINT4 FsRadExtAuthClientAccessAccepts [ ] ={1,3,6,1,4,1,29601,2,30,1,2,3,1,8};
UINT4 FsRadExtAuthClientAccessRejects [ ] ={1,3,6,1,4,1,29601,2,30,1,2,3,1,9};
UINT4 FsRadExtAuthClientAccessChallenges [ ] ={1,3,6,1,4,1,29601,2,30,1,2,3,1,10};
UINT4 FsRadExtAuthClientMalformedAccessResponses [ ] ={1,3,6,1,4,1,29601,2,30,1,2,3,1,11};
UINT4 FsRadExtAuthClientBadAuthenticators [ ] ={1,3,6,1,4,1,29601,2,30,1,2,3,1,12};
UINT4 FsRadExtAuthClientPendingRequests [ ] ={1,3,6,1,4,1,29601,2,30,1,2,3,1,13};
UINT4 FsRadExtAuthClientTimeouts [ ] ={1,3,6,1,4,1,29601,2,30,1,2,3,1,14};
UINT4 FsRadExtAuthClientUnknownTypes [ ] ={1,3,6,1,4,1,29601,2,30,1,2,3,1,15};
UINT4 FsRadExtAuthClientPacketsDropped [ ] ={1,3,6,1,4,1,29601,2,30,1,2,3,1,16};
UINT4 FsRadExtAccClientInvalidServerAddresses [ ] ={1,3,6,1,4,1,29601,2,30,1,3,1};
UINT4 FsRadExtAccClientIdentifier [ ] ={1,3,6,1,4,1,29601,2,30,1,3,2};
UINT4 FsRadExtAccServerIndex [ ] ={1,3,6,1,4,1,29601,2,30,1,3,3,1,1};
UINT4 FsRadExtAccServerAddressType [ ] ={1,3,6,1,4,1,29601,2,30,1,3,3,1,2};
UINT4 FsRadExtAccServerAddress [ ] ={1,3,6,1,4,1,29601,2,30,1,3,3,1,3};
UINT4 FsRadExtAccClientServerPortNumber [ ] ={1,3,6,1,4,1,29601,2,30,1,3,3,1,4};
UINT4 FsRadExtAccClientRoundTripTime [ ] ={1,3,6,1,4,1,29601,2,30,1,3,3,1,5};
UINT4 FsRadExtAccClientRequests [ ] ={1,3,6,1,4,1,29601,2,30,1,3,3,1,6};
UINT4 FsRadExtAccClientRetransmissions [ ] ={1,3,6,1,4,1,29601,2,30,1,3,3,1,7};
UINT4 FsRadExtAccClientResponses [ ] ={1,3,6,1,4,1,29601,2,30,1,3,3,1,8};
UINT4 FsRadExtAccClientMalformedResponses [ ] ={1,3,6,1,4,1,29601,2,30,1,3,3,1,9};
UINT4 FsRadExtAccClientBadAuthenticators [ ] ={1,3,6,1,4,1,29601,2,30,1,3,3,1,10};
UINT4 FsRadExtAccClientPendingRequests [ ] ={1,3,6,1,4,1,29601,2,30,1,3,3,1,11};
UINT4 FsRadExtAccClientTimeouts [ ] ={1,3,6,1,4,1,29601,2,30,1,3,3,1,12};
UINT4 FsRadExtAccClientUnknownTypes [ ] ={1,3,6,1,4,1,29601,2,30,1,3,3,1,13};
UINT4 FsRadExtAccClientPacketsDropped [ ] ={1,3,6,1,4,1,29601,2,30,1,3,3,1,14};


tMbDbEntry fsradeMibEntry[]= {

{{12,FsRadExtDebugMask}, NULL, FsRadExtDebugMaskGet, FsRadExtDebugMaskSet, FsRadExtDebugMaskTest, FsRadExtDebugMaskDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsRadExtMaxNoOfUserEntries}, NULL, FsRadExtMaxNoOfUserEntriesGet, FsRadExtMaxNoOfUserEntriesSet, FsRadExtMaxNoOfUserEntriesTest, FsRadExtMaxNoOfUserEntriesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsRadExtPrimaryServerAddressType}, NULL, FsRadExtPrimaryServerAddressTypeGet, FsRadExtPrimaryServerAddressTypeSet, FsRadExtPrimaryServerAddressTypeTest, FsRadExtPrimaryServerAddressTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsRadExtPrimaryServer}, NULL, FsRadExtPrimaryServerGet, FsRadExtPrimaryServerSet, FsRadExtPrimaryServerTest, FsRadExtPrimaryServerDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{14,FsRadExtServerIndex}, GetNextIndexFsRadExtServerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsRadExtServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtServerAddrType}, GetNextIndexFsRadExtServerTable, FsRadExtServerAddrTypeGet, FsRadExtServerAddrTypeSet, FsRadExtServerAddrTypeTest, FsRadExtServerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRadExtServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtServerAddress}, GetNextIndexFsRadExtServerTable, FsRadExtServerAddressGet, FsRadExtServerAddressSet, FsRadExtServerAddressTest, FsRadExtServerTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsRadExtServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtServerType}, GetNextIndexFsRadExtServerTable, FsRadExtServerTypeGet, FsRadExtServerTypeSet, FsRadExtServerTypeTest, FsRadExtServerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRadExtServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtServerSharedSecret}, GetNextIndexFsRadExtServerTable, FsRadExtServerSharedSecretGet, FsRadExtServerSharedSecretSet, FsRadExtServerSharedSecretTest, FsRadExtServerTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsRadExtServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtServerEnabled}, GetNextIndexFsRadExtServerTable, FsRadExtServerEnabledGet, FsRadExtServerEnabledSet, FsRadExtServerEnabledTest, FsRadExtServerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRadExtServerTableINDEX, 1, 0, 0, "1"},

{{14,FsRadExtServerResponseTime}, GetNextIndexFsRadExtServerTable, FsRadExtServerResponseTimeGet, FsRadExtServerResponseTimeSet, FsRadExtServerResponseTimeTest, FsRadExtServerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRadExtServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtServerMaximumRetransmission}, GetNextIndexFsRadExtServerTable, FsRadExtServerMaximumRetransmissionGet, FsRadExtServerMaximumRetransmissionSet, FsRadExtServerMaximumRetransmissionTest, FsRadExtServerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRadExtServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtServerEntryStatus}, GetNextIndexFsRadExtServerTable, FsRadExtServerEntryStatusGet, FsRadExtServerEntryStatusSet, FsRadExtServerEntryStatusTest, FsRadExtServerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRadExtServerTableINDEX, 1, 0, 1, NULL},

{{12,FsRadExtAuthClientInvalidServerAddresses}, NULL, FsRadExtAuthClientInvalidServerAddressesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsRadExtAuthClientIdentifier}, NULL, FsRadExtAuthClientIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{14,FsRadExtAuthServerIndex}, GetNextIndexFsRadExtAuthServerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsRadExtAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAuthServerAddressType}, GetNextIndexFsRadExtAuthServerTable, FsRadExtAuthServerAddressTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRadExtAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAuthServerAddress}, GetNextIndexFsRadExtAuthServerTable, FsRadExtAuthServerAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsRadExtAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAuthClientServerPortNumber}, GetNextIndexFsRadExtAuthServerTable, FsRadExtAuthClientServerPortNumberGet, FsRadExtAuthClientServerPortNumberSet, FsRadExtAuthClientServerPortNumberTest, FsRadExtAuthServerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRadExtAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAuthClientRoundTripTime}, GetNextIndexFsRadExtAuthServerTable, FsRadExtAuthClientRoundTripTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRadExtAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAuthClientAccessRequests}, GetNextIndexFsRadExtAuthServerTable, FsRadExtAuthClientAccessRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRadExtAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAuthClientAccessRetransmissions}, GetNextIndexFsRadExtAuthServerTable, FsRadExtAuthClientAccessRetransmissionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRadExtAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAuthClientAccessAccepts}, GetNextIndexFsRadExtAuthServerTable, FsRadExtAuthClientAccessAcceptsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRadExtAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAuthClientAccessRejects}, GetNextIndexFsRadExtAuthServerTable, FsRadExtAuthClientAccessRejectsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRadExtAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAuthClientAccessChallenges}, GetNextIndexFsRadExtAuthServerTable, FsRadExtAuthClientAccessChallengesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRadExtAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAuthClientMalformedAccessResponses}, GetNextIndexFsRadExtAuthServerTable, FsRadExtAuthClientMalformedAccessResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRadExtAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAuthClientBadAuthenticators}, GetNextIndexFsRadExtAuthServerTable, FsRadExtAuthClientBadAuthenticatorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRadExtAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAuthClientPendingRequests}, GetNextIndexFsRadExtAuthServerTable, FsRadExtAuthClientPendingRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsRadExtAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAuthClientTimeouts}, GetNextIndexFsRadExtAuthServerTable, FsRadExtAuthClientTimeoutsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRadExtAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAuthClientUnknownTypes}, GetNextIndexFsRadExtAuthServerTable, FsRadExtAuthClientUnknownTypesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRadExtAuthServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAuthClientPacketsDropped}, GetNextIndexFsRadExtAuthServerTable, FsRadExtAuthClientPacketsDroppedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRadExtAuthServerTableINDEX, 1, 0, 0, NULL},

{{12,FsRadExtAccClientInvalidServerAddresses}, NULL, FsRadExtAccClientInvalidServerAddressesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsRadExtAccClientIdentifier}, NULL, FsRadExtAccClientIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{14,FsRadExtAccServerIndex}, GetNextIndexFsRadExtAccServerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsRadExtAccServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAccServerAddressType}, GetNextIndexFsRadExtAccServerTable, FsRadExtAccServerAddressTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRadExtAccServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAccServerAddress}, GetNextIndexFsRadExtAccServerTable, FsRadExtAccServerAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsRadExtAccServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAccClientServerPortNumber}, GetNextIndexFsRadExtAccServerTable, FsRadExtAccClientServerPortNumberGet, FsRadExtAccClientServerPortNumberSet, FsRadExtAccClientServerPortNumberTest, FsRadExtAccServerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRadExtAccServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAccClientRoundTripTime}, GetNextIndexFsRadExtAccServerTable, FsRadExtAccClientRoundTripTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRadExtAccServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAccClientRequests}, GetNextIndexFsRadExtAccServerTable, FsRadExtAccClientRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRadExtAccServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAccClientRetransmissions}, GetNextIndexFsRadExtAccServerTable, FsRadExtAccClientRetransmissionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRadExtAccServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAccClientResponses}, GetNextIndexFsRadExtAccServerTable, FsRadExtAccClientResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRadExtAccServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAccClientMalformedResponses}, GetNextIndexFsRadExtAccServerTable, FsRadExtAccClientMalformedResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRadExtAccServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAccClientBadAuthenticators}, GetNextIndexFsRadExtAccServerTable, FsRadExtAccClientBadAuthenticatorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRadExtAccServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAccClientPendingRequests}, GetNextIndexFsRadExtAccServerTable, FsRadExtAccClientPendingRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsRadExtAccServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAccClientTimeouts}, GetNextIndexFsRadExtAccServerTable, FsRadExtAccClientTimeoutsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRadExtAccServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAccClientUnknownTypes}, GetNextIndexFsRadExtAccServerTable, FsRadExtAccClientUnknownTypesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRadExtAccServerTableINDEX, 1, 0, 0, NULL},

{{14,FsRadExtAccClientPacketsDropped}, GetNextIndexFsRadExtAccServerTable, FsRadExtAccClientPacketsDroppedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRadExtAccServerTableINDEX, 1, 0, 0, NULL},
};
tMibData fsradeEntry = { 47, fsradeMibEntry };
#endif /* _FSRADEDB_H */

