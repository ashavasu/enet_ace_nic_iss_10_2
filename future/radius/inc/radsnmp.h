/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radsnmp.h,v 1.10 2015/04/28 12:26:21 siva Exp $ 
 *
 * Description: This file contains the prototype declarations of the 
 *              low level routines.
 *
 *******************************************************************/


/*********** Low Level Routines Prototypes ********************/
INT1 nmhValidateIndexInstanceRadiusAuthServerTable(INT4 i4RadiusAuthServerIndex);
INT1 nmhGetFirstIndexRadiusAuthServerTable(INT4 *pi4RadiusAuthServerIndex);
INT1 nmhGetNextIndexRadiusAuthServerTable(INT4 i4RadiusAuthServerIndex ,INT4 *pi4NextRadiusAuthServerIndex);
INT1 nmhGetRadiusAuthServerAddress(INT4 i4RadiusAuthServerIndex , UINT4 *pu4RetValRadiusAuthServerAddress);
INT1 nmhGetRadiusAuthClientServerPortNumber(INT4 i4RadiusAuthServerIndex , INT4 *pi4RetValRadiusAuthClientServerPortNumber);
INT1 nmhGetRadiusAuthClientRoundTripTime(INT4 i4RadiusAuthServerIndex , UINT4 *pu4RetValRadiusAuthClientRoundTripTime);
INT1 nmhGetRadiusAuthClientAccessRequests(INT4 i4RadiusAuthServerIndex , UINT4 *pu4RetValRadiusAuthClientAccessRequests);
INT1 nmhGetRadiusAuthClientAccessRetransmissions(INT4 i4RadiusAuthServerIndex , UINT4 *pu4RetValRadiusAuthClientAccessRetransmissions);
INT1 nmhGetRadiusAuthClientAccessAccepts(INT4 i4RadiusAuthServerIndex , UINT4 *pu4RetValRadiusAuthClientAccessAccepts);
INT1 nmhGetRadiusAuthClientAccessRejects(INT4 i4RadiusAuthServerIndex , UINT4 *pu4RetValRadiusAuthClientAccessRejects);
INT1 nmhGetRadiusAuthClientAccessChallenges(INT4 i4RadiusAuthServerIndex , UINT4 *pu4RetValRadiusAuthClientAccessChallenges);
INT1 nmhGetRadiusAuthClientMalformedAccessResponses(INT4 i4RadiusAuthServerIndex , UINT4 *pu4RetValRadiusAuthClientMalformedAccessResponses);
INT1 nmhGetRadiusAuthClientBadAuthenticators(INT4 i4RadiusAuthServerIndex , UINT4 *pu4RetValRadiusAuthClientBadAuthenticators);
INT1 nmhGetRadiusAuthClientPendingRequests(INT4 i4RadiusAuthServerIndex , UINT4 *pu4RetValRadiusAuthClientPendingRequests);
INT1 nmhGetRadiusAuthClientTimeouts(INT4 i4RadiusAuthServerIndex , UINT4 *pu4RetValRadiusAuthClientTimeouts);
INT1 nmhGetRadiusAuthClientUnknownTypes(INT4 i4RadiusAuthServerIndex , UINT4 *pu4RetValRadiusAuthClientUnknownTypes);
INT1 nmhGetRadiusAuthClientPacketsDropped(INT4 i4RadiusAuthServerIndex , UINT4 *pu4RetValRadiusAuthClientPacketsDropped);
INT1 nmhGetRadiusAuthClientInvalidServerAddresses(UINT4 *pu4RetValRadiusAuthClientInvalidServerAddresses);
INT1 nmhValidateIndexInstanceRadiusAccServerTable(INT4 i4RadiusAccServerIndex);
INT1 nmhGetFirstIndexRadiusAccServerTable(INT4 *pi4RadiusAccServerIndex);
INT1 nmhGetNextIndexRadiusAccServerTable(INT4 i4RadiusAccServerIndex ,INT4 *pi4NextRadiusAccServerIndex);
INT1 nmhGetRadiusAccServerAddress(INT4 i4RadiusAccServerIndex , UINT4 *pu4RetValRadiusAccServerAddress);
INT1 nmhGetRadiusAccClientServerPortNumber(INT4 i4RadiusAccServerIndex , INT4 *pi4RetValRadiusAccClientServerPortNumber);
INT1 nmhGetRadiusAccClientRoundTripTime(INT4 i4RadiusAccServerIndex , UINT4 *pu4RetValRadiusAccClientRoundTripTime);
INT1 nmhGetRadiusAccClientRequests(INT4 i4RadiusAccServerIndex , UINT4 *pu4RetValRadiusAccClientRequests);
INT1 nmhGetRadiusAccClientRetransmissions(INT4 i4RadiusAccServerIndex , UINT4 *pu4RetValRadiusAccClientRetransmissions);
INT1 nmhGetRadiusAccClientResponses(INT4 i4RadiusAccServerIndex , UINT4 *pu4RetValRadiusAccClientResponses);
INT1 nmhGetRadiusAccClientMalformedResponses(INT4 i4RadiusAccServerIndex , UINT4 *pu4RetValRadiusAccClientMalformedResponses);
INT1 nmhGetRadiusAccClientBadAuthenticators(INT4 i4RadiusAccServerIndex , UINT4 *pu4RetValRadiusAccClientBadAuthenticators);
INT1 nmhGetRadiusAccClientPendingRequests(INT4 i4RadiusAccServerIndex , UINT4 *pu4RetValRadiusAccClientPendingRequests);
INT1 nmhGetRadiusAccClientTimeouts(INT4 i4RadiusAccServerIndex , UINT4 *pu4RetValRadiusAccClientTimeouts);
INT1 nmhGetRadiusAccClientUnknownTypes(INT4 i4RadiusAccServerIndex , UINT4 *pu4RetValRadiusAccClientUnknownTypes);
INT1 nmhGetRadiusAccClientPacketsDropped(INT4 i4RadiusAccServerIndex , UINT4 *pu4RetValRadiusAccClientPacketsDropped);
INT1 nmhGetRadiusAccClientInvalidServerAddresses(UINT4 *pu4RetValRadiusAccClientInvalidServerAddresses);
INT1 nmhValidateIndexInstanceRadiusExtServerTable(INT4 i4RadiusExtServerIndex);
INT1 nmhGetFirstIndexRadiusExtServerTable(INT4 *pi4RadiusExtServerIndex);
INT1 nmhGetNextIndexRadiusExtServerTable(INT4 i4RadiusExtServerIndex ,INT4 *pi4NextRadiusExtServerIndex);
INT1 nmhGetRadiusExtServerAddress(INT4 i4RadiusExtServerIndex , UINT4 *pu4RetValRadiusExtServerAddress);
INT1 nmhGetRadiusExtServerType(INT4 i4RadiusExtServerIndex , INT4 *pi4RetValRadiusExtServerType);
INT1 nmhGetRadiusExtServerEnabled(INT4 i4RadiusExtServerIndex , INT4 *pi4RetValRadiusExtServerEnabled);
INT1 nmhGetRadiusExtServerResponseTime(INT4 i4RadiusExtServerIndex , INT4 *pi4RetValRadiusExtServerResponseTime);
INT1 nmhGetRadiusExtServerMaximumRetransmission(INT4 i4RadiusExtServerIndex , INT4 *pi4RetValRadiusExtServerMaximumRetransmission);
INT1 nmhGetRadiusExtServerEntryStatus(INT4 i4RadiusExtServerIndex , INT4 *pi4RetValRadiusExtServerEntryStatus);
INT1 nmhSetRadiusExtServerAddress(INT4 i4RadiusExtServerIndex , UINT4 u4SetValRadiusExtServerAddress);
INT1 nmhSetRadiusExtServerType(INT4 i4RadiusExtServerIndex , INT4 i4SetValRadiusExtServerType);
INT1 nmhSetRadiusExtServerEnabled(INT4 i4RadiusExtServerIndex , INT4 i4SetValRadiusExtServerEnabled);
INT1 nmhSetRadiusExtServerResponseTime(INT4 i4RadiusExtServerIndex , INT4 i4SetValRadiusExtServerResponseTime);
INT1 nmhSetRadiusExtServerMaximumRetransmission(INT4 i4RadiusExtServerIndex , INT4 i4SetValRadiusExtServerMaximumRetransmission);
INT1 nmhSetRadiusExtServerEntryStatus(INT4 i4RadiusExtServerIndex , INT4 i4SetValRadiusExtServerEntryStatus);
INT1 nmhTestv2RadiusExtServerAddress(UINT4 *pu4ErrorCode , INT4 i4RadiusExtServerIndex , UINT4 u4TestValRadiusExtServerAddress);
INT1 nmhTestv2RadiusExtServerType(UINT4 *pu4ErrorCode , INT4 i4RadiusExtServerIndex , INT4 i4TestValRadiusExtServerType);
INT1 nmhTestv2RadiusExtServerEnabled(UINT4 *pu4ErrorCode , INT4 i4RadiusExtServerIndex , INT4 i4TestValRadiusExtServerEnabled);
INT1 nmhTestv2RadiusExtServerResponseTime(UINT4 *pu4ErrorCode , INT4 i4RadiusExtServerIndex , INT4 i4TestValRadiusExtServerResponseTime);
INT1 nmhTestv2RadiusExtServerMaximumRetransmission(UINT4 *pu4ErrorCode , INT4 i4RadiusExtServerIndex , INT4 i4TestValRadiusExtServerMaximumRetransmission);
INT1 nmhTestv2RadiusExtServerEntryStatus(UINT4 *pu4ErrorCode , INT4 i4RadiusExtServerIndex , INT4 i4TestValRadiusExtServerEntryStatus);

INT1 nmhGetRadiusExtServerSharedSecret(INT4 i4RadiusExtServerIndex , tSNMP_OCTET_STRING_TYPE * pRetValRadiusExtServerSharedSecret);
INT1 nmhSetRadiusExtServerSharedSecret(INT4 i4RadiusExtServerIndex , tSNMP_OCTET_STRING_TYPE *pSetValRadiusExtServerSharedSecret);
INT1 nmhTestv2RadiusExtServerSharedSecret(UINT4 *pu4ErrorCode , INT4 i4RadiusExtServerIndex , tSNMP_OCTET_STRING_TYPE *pTestValRadiusExtServerSharedSecret);

INT1 nmhGetRadiusExtDebugMask(INT4 *pi4RetValRadiusExtDebugMask);
INT1 nmhSetRadiusExtDebugMask(INT4 i4SetValRadiusExtDebugMask);
INT1 nmhTestv2RadiusExtDebugMask(UINT4 *pu4ErrorCode , INT4 i4TestValRadiusExtDebugMask);
INT1 nmhGetRadiusMaxNoOfUserEntries(INT4 *pi4RetValRadiusExtDebugMask);
INT1 nmhSetRadiusMaxNoOfUserEntries(INT4 i4SetValRadiusExtDebugMask);
INT1 nmhTestv2RadiusMaxNoOfUserEntries(UINT4 *pu4ErrorCode , INT4 i4TestValRadiusExtDebugMask);

INT1 nmhGetRadiusAuthClientIdentifier(tSNMP_OCTET_STRING_TYPE * pRetValRadiusAuthClientIdentifier);
INT1 nmhGetRadiusAccClientIdentifier(tSNMP_OCTET_STRING_TYPE * pRetValRadiusAccClientIdentifier);

INT1 nmhGetRadiusProxyStatus (INT4 *pi4RetValRadiusProxyStatus);
INT1 nmhSetRadiusProxyStatus (INT4 i4SetValRadiusProxyStatus);
INT1 nmhTestv2RadiusProxyStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValRadiusProxyStatus);

INT1 nmhDepv2RadiusExtServerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1 nmhDepv2RadiusExtDebugMask ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1 nmhDepv2RadiusMaxNoOfUserEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));



INT1
nmhGetFsRadExtPrimaryServerAddressType ARG_LIST((INT4 *));

INT1
nmhGetFsRadExtPrimaryServer ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));



INT1
nmhTestv2FsRadExtPrimaryServerAddressType ARG_LIST((UINT4 *  ,INT4 ));




/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRadExtAuthServerTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */






/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRadExtAuthClientServerPortNumber ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRadExtAuthServerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


