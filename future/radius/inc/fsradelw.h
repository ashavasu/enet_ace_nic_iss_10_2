/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsradelw.h,v 1.1 2015/04/28 12:26:21 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRadExtDebugMask ARG_LIST((INT4 *));

INT1
nmhGetFsRadExtMaxNoOfUserEntries ARG_LIST((INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRadExtDebugMask ARG_LIST((INT4 ));

INT1
nmhSetFsRadExtMaxNoOfUserEntries ARG_LIST((INT4 ));

INT1
nmhSetFsRadExtPrimaryServerAddressType ARG_LIST((INT4 ));

INT1
nmhSetFsRadExtPrimaryServer ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRadExtDebugMask ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRadExtMaxNoOfUserEntries ARG_LIST((UINT4 *  ,INT4 ));


INT1
nmhTestv2FsRadExtPrimaryServer ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRadExtDebugMask ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRadExtMaxNoOfUserEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRadExtPrimaryServerAddressType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRadExtPrimaryServer ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRadExtServerTable. */
INT1
nmhValidateIndexInstanceFsRadExtServerTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRadExtServerTable  */

INT1
nmhGetFirstIndexFsRadExtServerTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRadExtServerTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRadExtServerAddrType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRadExtServerAddress ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRadExtServerType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRadExtServerSharedSecret ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRadExtServerEnabled ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRadExtServerResponseTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRadExtServerMaximumRetransmission ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRadExtServerEntryStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRadExtServerAddrType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRadExtServerAddress ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsRadExtServerType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRadExtServerSharedSecret ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsRadExtServerEnabled ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRadExtServerResponseTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRadExtServerMaximumRetransmission ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRadExtServerEntryStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRadExtServerAddrType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRadExtServerAddress ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsRadExtServerType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRadExtServerSharedSecret ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsRadExtServerEnabled ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRadExtServerResponseTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRadExtServerMaximumRetransmission ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRadExtServerEntryStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRadExtServerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRadExtAuthClientInvalidServerAddresses ARG_LIST((UINT4 *));

INT1
nmhGetFsRadExtAuthClientIdentifier ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsRadExtAuthServerTable. */
INT1
nmhValidateIndexInstanceFsRadExtAuthServerTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRadExtAuthServerTable  */

INT1
nmhGetFirstIndexFsRadExtAuthServerTable ARG_LIST((INT4 *));



/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRadExtAuthServerAddressType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRadExtAuthServerAddress ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRadExtAuthClientServerPortNumber ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRadExtAuthClientRoundTripTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRadExtAuthClientAccessRequests ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRadExtAuthClientAccessRetransmissions ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRadExtAuthClientAccessAccepts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRadExtAuthClientAccessRejects ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRadExtAuthClientAccessChallenges ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRadExtAuthClientMalformedAccessResponses ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRadExtAuthClientBadAuthenticators ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRadExtAuthClientPendingRequests ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRadExtAuthClientTimeouts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRadExtAuthClientUnknownTypes ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRadExtAuthClientPacketsDropped ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRadExtAuthClientServerPortNumber ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */


/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRadExtAccClientInvalidServerAddresses ARG_LIST((UINT4 *));

INT1
nmhGetFsRadExtAccClientIdentifier ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsRadExtAccServerTable. */
INT1
nmhValidateIndexInstanceFsRadExtAccServerTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRadExtAccServerTable  */

INT1
nmhGetFirstIndexFsRadExtAccServerTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRadExtAccServerTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRadExtAccServerAddressType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRadExtAccServerAddress ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRadExtAccClientServerPortNumber ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRadExtAccClientRoundTripTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRadExtAccClientRequests ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRadExtAccClientRetransmissions ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRadExtAccClientResponses ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRadExtAccClientMalformedResponses ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRadExtAccClientBadAuthenticators ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRadExtAccClientPendingRequests ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRadExtAccClientTimeouts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRadExtAccClientUnknownTypes ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRadExtAccClientPacketsDropped ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRadExtAccClientServerPortNumber ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRadExtAccClientServerPortNumber ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRadExtAccServerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
