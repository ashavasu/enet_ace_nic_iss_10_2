/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radvar.h,v 1.14 2015/04/28 12:26:21 siva Exp $ 
 *
 * Description : This file contains the extern declarations 
 *               for all the data structures that are present 
 *               in the system.
 *
 *******************************************************************/


 #ifndef RADVAR_H

#define RADVAR_H

/******** Global Configurable Variable Decleration ******************/


/**************** Global Variable Decleration ***********************/

#include "utilipvx.h"

#ifdef RADINIT_C
#define EXTERN
#else
#define EXTERN extern
#endif



EXTERN tRADIUS_ERROR_LOG   *p_gRadiusErrorLog;   /* Pointer to a structure to
                            hold the error events */
EXTERN tRADIUS_ERROR_LOG   g_RadiusErrors;   /* Structure of Radius 
                                                    Errors */   
EXTERN UINT1   u1_gRadiusPacketIdentifier;   /* Packet Identifier */

EXTERN INT4   i4_gRadiusAuthSocketId;   /* Ipv4 and Ipv6 Socket Identifiers */
EXTERN INT4   i4_gRadiusAccSocketId;    /* Ipv4 and IPv6 Socket Identifiers */
EXTERN INT4   gi4ProxyToClientSocketId;
EXTERN INT4   gi4ProxyToServerSocketId;
#ifdef IP6_WANTED
EXTERN INT4   i4_gRadiusAccSocketId6;   /* for the transmission of acc packet */
EXTERN INT4   i4_gRadiusAuthSocketId6;  /* for the transmission of auth packet */
#endif

    /* A table to hold the pointers of the Timer requests */
    EXTERN tTmrAppTimer *a_gUserTable[Q_TABLE_SIZE]; 

extern tRadMemPoolId  gRadMemPoolId;

/* A table to hold the pointers of the servers */
EXTERN tRADIUS_SERVER   *a_gServerTable[MAX_RAD_SERVERS_LIMIT];

EXTERN UINT4   u4_gRadAuthClientInvalidServers;
EXTERN UINT4   u4_gRadAcctClientInvalidServers;
extern UINT1   gau1RadiusClientIdentifier[64];
extern INT4   gi4RadMaxNoofUserEntries;
EXTERN tRADIUS_SERVER *p_gPrimaryRadServer;
extern tIPvXAddr gRadActiveServer;
extern UINT1 gau1RadActiveServerHostName[DNS_MAX_QUERY_LEN];
EXTERN UINT1   gu1RadConfigRestored;
#define TIMER_FREE_RUN          1
#define TIMER_00        00
#define TASK_NAME       (UINT1 *)"RAD"
#define RADIUS_SEM_NAME (const UINT1 *) "RDSM" 
#define ERRNO           errno
 
#endif /* RADVAR_H */

