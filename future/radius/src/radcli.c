
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radcli.c,v 1.24 2018/02/14 10:04:08 siva Exp $
 *
 * Description: Action routines for set/get objects in fsradius.mib, 
 *              radauth.mib             
 *
 *******************************************************************/
#ifndef __RADCLI_C__
#define __RADCLI_C__

#ifdef RADIUS_WANTED
#include "lr.h"
#include "cfa.h"
#include "radcli.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "radsnmp.h"
#include "radcom.h"
#include "fsradicli.h"
#include "fsradecli.h"
#include "ip6util.h"
#endif

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_process_radius_cmd                             */
/*                                                                           */
/*     DESCRIPTION      : This function takes in variable no. of arguments   */
/*                        and process the commands for the RADIUS module as  */
/*                        defined in radcli.h                                */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Command -  Command Identifier                    */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
cli_process_radius_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[RAD_CLI_MAX_COMMANDS];
    UINT1              *pu1Inst = NULL;
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4RespTime = 0;
    UINT4               u4ReTransmit = 0;
    UINT4               u4IpAddr = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    INT4                i4PrimaryServer = FALSE;
    tIPvXAddr           ServerAddress;
    tIp6Addr            Ip6ServerAddr;
    INT4                i4AuthPort = 0;
    INT4                i4AccPort = 0;
    UINT1               au1RadServAddr[DNS_MAX_QUERY_LEN];

    UNUSED_PARAM (pu1Inst);

    MEMSET (&ServerAddress, 0, sizeof (tIPvXAddr));
    MEMSET (au1RadServAddr, 0, DNS_MAX_QUERY_LEN);
    va_start (ap, u4Command);

    /* Third arguement is always interface name/index */

    pu1Inst = va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguements and store in args array. 
     * Store RAD_CLI_MAX_ARGS arguements at the max. 
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == RAD_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    CliRegisterLock (CliHandle, RadiusLock, RadiusUnLock);

    RADIUS_LOCK ();

    switch (u4Command)
    {
        case CLI_RADIUS_SERVER_HOST:
            if ((args[1]) != NULL)
            {
                u4RespTime = *(UINT4 *) (args[1]);
            }
            if ((args[2]) != NULL)
            {
                u4ReTransmit = *(UINT4 *) (args[2]);
            }

            if (((INT4 *) args[6]) != NULL)
            {
                i4AuthPort = *(INT4 *) (args[6]);
            }

            if (((INT4 *) args[7]) != NULL)
            {
                i4AccPort = *(INT4 *) (args[7]);
            }

            i4PrimaryServer = (*(INT4 *) (args[5]));

            if (*(INT4 *) (args[4]) == IPVX_ADDR_FMLY_IPV4)
            {
                u4IpAddr = *(args[0]);
                u4IpAddr = OSIX_NTOHL (u4IpAddr);

                MEMCPY (au1RadServAddr, (UINT1 *) &u4IpAddr,
                        IPVX_IPV4_ADDR_LEN);

                i4RetStatus = RadSetServerHost (CliHandle,
                                                au1RadServAddr,
                                                u4RespTime,
                                                u4ReTransmit,
                                                (UINT1 *) (args[3]),
                                                i4PrimaryServer, i4AuthPort,
                                                i4AccPort, IPVX_ADDR_FMLY_IPV4);
            }
            else if (*(INT4 *) (args[4]) == IPVX_ADDR_FMLY_IPV6)
            {
                /* get the string in args[3], remove ":" or "." from the
                 *  *                    addess string and copy it to the variable below */
                if (INET_ATON6 (args[0], au1RadServAddr) == 0)
                {
                    break;
                }

                i4RetStatus = RadSetServerHost (CliHandle,
                                                au1RadServAddr,
                                                u4RespTime,
                                                u4ReTransmit,
                                                (UINT1 *) (args[3]),
                                                i4PrimaryServer, i4AuthPort,
                                                i4AccPort, IPVX_ADDR_FMLY_IPV6);

            }
            else
            {
                if (STRLEN (args[0]) <= DNS_MAX_QUERY_LEN)
                {
                    STRNCPY (au1RadServAddr, (UINT1 *) args[0],
                             STRLEN (args[0]));
                    au1RadServAddr[STRLEN (args[0])] = '\0';
                    *au1RadServAddr = UtilStrToLower ((UINT1 *) au1RadServAddr);
                    i4RetStatus = RadSetServerHost (CliHandle,
                                                    au1RadServAddr,
                                                    u4RespTime,
                                                    u4ReTransmit,
                                                    (UINT1 *) (args[3]),
                                                    i4PrimaryServer, i4AuthPort,
                                                    i4AccPort, RAD_DNS_FAMILY);
                }
            }

            break;

        case CLI_RADIUS_NO_SERVER_HOST:

            i4PrimaryServer = (*(INT4 *) (args[2]));

            if (*(UINT4 *) (args[1]) == IPVX_ADDR_FMLY_IPV4)
            {

                u4IpAddr = *(args[0]);
                u4IpAddr = OSIX_NTOHL (u4IpAddr);

                MEMCPY (au1RadServAddr, (UINT1 *) &u4IpAddr,
                        IPVX_IPV4_ADDR_LEN);

                i4RetStatus =
                    RadSetNoServerHost (CliHandle, au1RadServAddr,
                                        i4PrimaryServer, IPVX_ADDR_FMLY_IPV4);
            }
            else if (*(UINT4 *) (args[1]) == IPVX_ADDR_FMLY_IPV6)
            {
                /* get the string in args[3], remove ":" or "." from the
                 *  *                    addess string and copy it to the variable below */
                if (INET_ATON6 (args[0], au1RadServAddr) == 0)
                {
                    break;
                }

                i4RetStatus =
                    RadSetNoServerHost (CliHandle, au1RadServAddr,
                                        i4PrimaryServer, IPVX_ADDR_FMLY_IPV6);
            }
            else
            {
                STRNCPY (au1RadServAddr, (UINT1 *) args[0], STRLEN (args[0]));

                au1RadServAddr[STRLEN (args[0])] = '\0';

                *au1RadServAddr = UtilStrToLower ((UINT1 *) au1RadServAddr);

                i4RetStatus =
                    RadSetNoServerHost (CliHandle, au1RadServAddr,
                                        i4PrimaryServer, RAD_DNS_FAMILY);

            }

            break;

        case CLI_ENABLE_RADIUS_PROXY:
            i4RetStatus = RadSetProxyStatus (CliHandle, RADIUS_PROXY_ENABLED);
            break;

        case CLI_DISABLE_RADIUS_PROXY:
            i4RetStatus = RadSetProxyStatus (CliHandle, RADIUS_PROXY_DISABLED);
            break;

        case CLI_RADIUS_DEBUG:
            i4RetStatus = RadSetDebug (CliHandle, CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_RADIUS_NO_DEBUG:
            i4RetStatus = RadSetDebug (CliHandle, RAD_CLI_NO_DEBUG);
            break;

        case CLI_SHOW_RADIUS_SERVER:
            if (CLI_PTR_TO_I4 (args[1]) == IPVX_ADDR_FMLY_IPV4)
            {
                u4IpAddr = *(args[0]);
                u4IpAddr = OSIX_NTOHL (u4IpAddr);
                IPVX_ADDR_INIT_FROMV4 (ServerAddress, u4IpAddr);
                i4RetStatus = RadShowServerInfo (CliHandle, &ServerAddress,
                                                 gau1ZeroHostName);
            }
            else if (CLI_PTR_TO_I4 (args[1]) == IPVX_ADDR_FMLY_IPV6)
            {
                MEMSET (&Ip6ServerAddr, 0, sizeof (tIp6Addr));
                INET_ATON6 (args[0], &Ip6ServerAddr);
                IPVX_ADDR_INIT_FROMV6 (ServerAddress, (VOID *) &Ip6ServerAddr);
                i4RetStatus = RadShowServerInfo (CliHandle, &ServerAddress,
                                                 gau1ZeroHostName);
            }
            else if (CLI_PTR_TO_I4 (args[1]) == RAD_DNS_FAMILY)
            {
                STRNCPY (au1RadServAddr, (UINT1 *) args[0], STRLEN (args[0]));

                au1RadServAddr[STRLEN (args[0])] = '\0';

                ServerAddress.u1Afi = (UINT1) RAD_DNS_FAMILY;
                *au1RadServAddr = UtilStrToLower ((UINT1 *) au1RadServAddr);

                i4RetStatus = RadShowServerInfo (CliHandle, &ServerAddress,
                                                 au1RadServAddr);
            }
            else
            {
                ServerAddress.u1Afi = (UINT1) RAD_ZERO;
                i4RetStatus = RadShowServerInfo (CliHandle, &ServerAddress,
                                                 gau1ZeroHostName);
            }
            break;

        case CLI_SHOW_RADIUS_STATS:
            i4RetStatus = RadShowStats (CliHandle);
            break;
        case CLI_SHOW_RADIUS_PROXY:
            i4RetStatus = RadShowProxyInfo (CliHandle);
            break;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_RAD_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", RadCliErrString[u4ErrCode]);
        }

        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);

    RADIUS_UNLOCK ();

    return i4RetStatus;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RadSetServerHost                                   */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Radius Server Host          */
/*                        Information                                        */
/*                                                                           */
/*     INPUT            : u4IpAddr -  Radius Server IpAddress                */
/*                        u4ResponseTime - Response Time                     */
/*                        u4MaxReTrans - Maximum Retransmission              */
/*                        pu1Password - Server SharedSecret                  */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
RadSetServerHost (tCliHandle CliHandle, UINT1 *au1IpAddress,
                  UINT4 u4ResponseTime, UINT4 u4MaxReTrans, UINT1 *pu1Password,
                  INT4 i4PrimaryServer, INT4 i4AuthPort, INT4 i4AccPort,
                  UINT4 u4AddrType)
{
    INT4                i4CurServEntryStatus = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4EntryStatus = CLI_SUCCESS;
    UINT4               u4Iface = 0;
    tSNMP_OCTET_STRING_TYPE Password;
    tSNMP_OCTET_STRING_TYPE ServerIpAddress;
    UINT1               u4Flag = SERVER_AUTH;
    INT1                i1RetVal = 0;
    tIPvXAddr           ServerAddr;
    INT4                i4AddrType = (INT4) u4AddrType;

    if (i4AccPort != 0)
    {
        u4Flag |= SERVER_ACC;
    }

    MEMSET (&ServerAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&ServerIpAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    /* Copy the Server address into the tSNMP_OCTET_STRING_TYPE
     * structure */

    if (i4AddrType == (INT4) IPVX_ADDR_FMLY_IPV4)
    {
        ServerIpAddress.pu1_OctetList = au1IpAddress;
        ServerIpAddress.i4_Length = (INT4) IPVX_IPV4_ADDR_LEN;

    }
    else if (i4AddrType == (INT4) IPVX_ADDR_FMLY_IPV6)
    {
        ServerIpAddress.pu1_OctetList = au1IpAddress;
        ServerIpAddress.i4_Length = (INT4) IPVX_IPV6_ADDR_LEN;
    }
    else if (u4AddrType == (INT4) RAD_DNS_FAMILY)
    {
        ServerIpAddress.pu1_OctetList = au1IpAddress;
        ServerIpAddress.i4_Length = (INT4) STRLEN (au1IpAddress);
    }

    if (u4AddrType != RAD_DNS_FAMILY)
    {
        IPVX_ADDR_INIT (ServerAddr, (UINT1) u4AddrType,
                        ServerIpAddress.pu1_OctetList);

        u4Iface = RadGetServerIndex (&ServerAddr);
    }
    else
    {
        u4Iface = RadGetServerHostIndex (au1IpAddress);
    }
    /* use this for deleting the entry which is created here */

    Password.i4_Length = (STRLEN (pu1Password)) ?
        STRLEN (pu1Password) : RAD_CLI_NAME_SIZE;

    Password.pu1_OctetList = pu1Password;

    if (u4Iface == (UINT4) RAD_ERROR)
    {
        /* No row exist with this interface, create a new row */

        u4Iface = GetNewRadServerIndex ();

        if ((u4Iface == 0) || (u4Iface > MAX_RAD_SERVERS_LIMIT))
        {
            CliPrintf (CliHandle,
                       "  %% Maximum number of RADIUS servers supported"
                       " are %d only !\r\n ", MAX_RAD_SERVERS_LIMIT);
            return CLI_FAILURE;
        }

        u4EntryStatus = CLI_FAILURE;

        if (nmhTestv2FsRadExtServerEntryStatus (&u4ErrCode,
                                                u4Iface,
                                                CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhSetFsRadExtServerEntryStatus (u4Iface,
                                             CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_RAD_MAX_SERVER_ERR);
            return (CLI_FAILURE);
        }
    }
    else
    {
        /* Row entry exists, put the row in not-in-service and update
         *  the fields. Here no need for test routines as test routines 
         *  always return success */
        /* Store the current status of the entry */
        if (SNMP_FAILURE ==
            nmhGetFsRadExtServerEntryStatus ((INT4) u4Iface,
                                             &i4CurServEntryStatus))
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetFsRadExtServerEntryStatus (u4Iface,
                                             NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (nmhTestv2FsRadExtServerType (&u4ErrCode, u4Iface,
                                     u4Flag) == SNMP_FAILURE)

    {
        if (u4EntryStatus == CLI_FAILURE)
        {
            nmhSetFsRadExtServerEntryStatus (u4Iface, DESTROY);
        }
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (nmhSetFsRadExtServerAddrType
        (u4Iface, (INT4) u4AddrType) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_RAD_INVALID_SERVER);
        return (CLI_FAILURE);
    }

    if (nmhTestv2FsRadExtServerAddress (&u4ErrCode, u4Iface,
                                        &ServerIpAddress) == SNMP_FAILURE)
    {
        if (u4EntryStatus == CLI_FAILURE)
        {
            nmhSetFsRadExtServerEntryStatus (u4Iface, DESTROY);
        }
        /* Set the current status back to the entry */
        if (0 != i4CurServEntryStatus)
        {
            if (SNMP_FAILURE ==
                nmhSetFsRadExtServerEntryStatus ((INT4) u4Iface,
                                                 i4CurServEntryStatus))
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }
        CLI_SET_ERR (CLI_RAD_INVALID_SERVER_IP);
        return (CLI_FAILURE);
    }

    if (u4ResponseTime)
    {
        if (nmhTestv2FsRadExtServerResponseTime (&u4ErrCode,
                                                 u4Iface,
                                                 u4ResponseTime)
            == SNMP_FAILURE)
        {
            if (u4EntryStatus == CLI_FAILURE)
            {
                nmhSetFsRadExtServerEntryStatus (u4Iface, DESTROY);
            }
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (u4MaxReTrans)
    {
        if (nmhTestv2FsRadExtServerMaximumRetransmission (&u4ErrCode,
                                                          u4Iface,
                                                          u4MaxReTrans)
            == SNMP_FAILURE)
        {
            if (u4EntryStatus == CLI_FAILURE)
            {
                nmhSetFsRadExtServerEntryStatus (u4Iface, DESTROY);
            }
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (nmhTestv2FsRadExtServerSharedSecret (&u4ErrCode,
                                             u4Iface,
                                             &Password) == SNMP_FAILURE)
    {
        if (u4EntryStatus == CLI_FAILURE)
        {
            nmhSetFsRadExtServerEntryStatus (u4Iface, DESTROY);
        }
        CLI_SET_ERR (CLI_RAD_NVT_ERR);
        return (CLI_FAILURE);
    }

    if (nmhSetFsRadExtServerType (u4Iface, u4Flag) == SNMP_FAILURE)
    {
        if (u4EntryStatus == CLI_FAILURE)
        {
            nmhSetFsRadExtServerEntryStatus (u4Iface, DESTROY);
        }
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetFsRadExtServerAddress (u4Iface, &ServerIpAddress) == SNMP_FAILURE)
    {
        if (u4EntryStatus == CLI_FAILURE)
        {
            nmhSetFsRadExtServerEntryStatus (u4Iface, DESTROY);
        }
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    /* Server Respose time is a optional parameter
     * So, call the corresonponding set routine, only when this input
     * is given by the user
     */
    if (u4ResponseTime)
    {
        if (nmhSetFsRadExtServerResponseTime (u4Iface, u4ResponseTime)
            == SNMP_FAILURE)
        {
            if (u4EntryStatus == CLI_FAILURE)
            {
                nmhSetFsRadExtServerEntryStatus (u4Iface, DESTROY);
            }
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* Maximum Retransmission setting is a optional parameter
     * So, call the corresonponding set routine, only when 
     * this input is given by the user 
     */
    if (u4MaxReTrans)
    {
        if (nmhSetFsRadExtServerMaximumRetransmission (u4Iface,
                                                       u4MaxReTrans)
            == SNMP_FAILURE)
        {
            if (u4EntryStatus == CLI_FAILURE)
            {
                nmhSetFsRadExtServerEntryStatus (u4Iface, DESTROY);
            }
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (nmhSetFsRadExtServerSharedSecret (u4Iface, &Password) == SNMP_FAILURE)
    {
        if (u4EntryStatus == CLI_FAILURE)
        {
            nmhSetFsRadExtServerEntryStatus (u4Iface, DESTROY);
        }
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2FsRadExtServerEntryStatus (&u4ErrCode,
                                            u4Iface, ACTIVE) == SNMP_FAILURE)
    {
        if (u4EntryStatus == CLI_FAILURE)
        {
            nmhSetFsRadExtServerEntryStatus (u4Iface, DESTROY);
        }
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (i4AuthPort != 0)
    {
        if (nmhTestv2FsRadExtAuthClientServerPortNumber (&u4ErrCode,
                                                         u4Iface, i4AuthPort)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_RAD_NVT_ERR);
            return CLI_FAILURE;
        }

        if (nmhSetFsRadExtAuthClientServerPortNumber (u4Iface, i4AuthPort)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_RAD_NVT_ERR);
            return CLI_FAILURE;
        }
    }

    if (i4AccPort != 0)
    {
        if (nmhTestv2FsRadExtAccClientServerPortNumber (&u4ErrCode,
                                                        u4Iface, i4AccPort)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_RAD_NVT_ERR);
            return CLI_FAILURE;
        }

        if (nmhSetFsRadExtAccClientServerPortNumber (u4Iface, i4AccPort)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_RAD_NVT_ERR);
            return CLI_FAILURE;
        }
    }

    if (nmhSetFsRadExtServerEntryStatus (u4Iface, ACTIVE) == SNMP_FAILURE)
    {
        if (u4EntryStatus == CLI_FAILURE)
        {
            nmhSetFsRadExtServerEntryStatus (u4Iface, DESTROY);
        }
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (i4PrimaryServer == TRUE)

    {
        /* This server is to be configured as the primary server */
        i1RetVal = nmhSetFsRadExtPrimaryServerAddressType (((INT4) u4AddrType));
        i1RetVal = nmhSetFsRadExtPrimaryServer (&ServerIpAddress);
        UNUSED_PARAM (i1RetVal);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RadSetNoServerHost                                 */
/*                                                                           */
/*     DESCRIPTION      : This function deletes the Radius Server Host       */
/*                        Information                                        */
/*                                                                           */
/*     INPUT            : u4IpAddr -  Radius Server IpAddress                */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
RadSetNoServerHost (tCliHandle CliHandle, UINT1 *au1IpAddress,
                    INT4 i4PrimaryAddress, UINT4 u4AddrType)
{
    UINT4               u4ErrCode;
    UINT4               u4Iface;
    tSNMP_OCTET_STRING_TYPE ActiveServerIpAddress;
    tIPvXAddr           ServerAddr;
    UINT1               au1RadServAddr[DNS_MAX_QUERY_LEN];

    MEMSET (&ServerAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&au1RadServAddr, 0, DNS_MAX_QUERY_LEN);
    MEMSET (&ActiveServerIpAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    ActiveServerIpAddress.pu1_OctetList = au1RadServAddr;
    STRNCPY (ActiveServerIpAddress.pu1_OctetList, &au1IpAddress,
             MEM_MAX_BYTES (STRLEN (au1IpAddress), DNS_MAX_QUERY_LEN - 1));

    if (u4AddrType != RAD_DNS_FAMILY)
    {
        IPVX_ADDR_INIT (ServerAddr, (UINT1) u4AddrType, au1IpAddress);
        u4Iface = RadGetServerIndex (&ServerAddr);
    }
    else
    {
        u4Iface = RadGetServerHostIndex (au1IpAddress);
    }

    if (u4Iface == (UINT4) RAD_ERROR)
    {
        CLI_SET_ERR (CLI_RAD_INVALID_SERVER_IP);
        return (CLI_FAILURE);
    }

    if (i4PrimaryAddress == FALSE)
    {
        /* Check whether the server is configured as
         * primary server. If yes, remove it as the primary server
         * else, do nothing. return */
        nmhGetFsRadExtPrimaryServer (&ActiveServerIpAddress);
        if (MEMCMP (ActiveServerIpAddress.pu1_OctetList,
                    au1IpAddress, DNS_MAX_QUERY_LEN) == 0)
        {
            MEMSET (ActiveServerIpAddress.pu1_OctetList, 0, DNS_MAX_QUERY_LEN);
            ActiveServerIpAddress.i4_Length = 0;

            if (nmhSetFsRadExtPrimaryServerAddressType (0) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsRadExtPrimaryServer (&ActiveServerIpAddress)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r%%The mentioned server is not a primary server\r\n");
        }

        return CLI_SUCCESS;

    }

    if (nmhTestv2FsRadExtServerEntryStatus (&u4ErrCode,
                                            u4Iface, DESTROY) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_RAD_ENTRY_IN_USE);
        return (CLI_FAILURE);
    }

    if (nmhSetFsRadExtServerEntryStatus (u4Iface, DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RadSetDebug                                        */
/*                                                                           */
/*     DESCRIPTION      : This function sets/resets Debug Value for Radius   */
/*                                                                           */
/*     INPUT            : u4DebugMask - Radius Debug Value                   */
/*                                      RAD_CLI_NO_DEBUG      0              */
/*                                      RAD_CLI_DEBUG_ALL     1              */
/*                                      RAD_CLI_ERR_MASK      2              */
/*                                      RAD_CLI_EVENT_MASK    3              */
/*                                      RAD_CLI_PACKET_MASK   4              */
/*                                      RAD_CLI_RESP_MASK     5              */
/*                                      RAD_CLI_TIMER_MASK    6              */
/*                                                                           */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
RadSetDebug (tCliHandle CliHandle, UINT4 u4DebugMask)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4Mask;

    nmhGetRadiusExtDebugMask ((INT4 *) &u4Mask);
    if (u4DebugMask != RAD_CLI_NO_DEBUG)
    {
        u4DebugMask = u4DebugMask | u4Mask;
    }

    if (nmhTestv2FsRadExtDebugMask (&u4ErrCode, u4DebugMask) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetFsRadExtDebugMask (u4DebugMask) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RadShowServerInfo                                  */
/*                                                                           */
/*     DESCRIPTION      : This function shows the Radius Server Host         */
/*                        Information                                        */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
RadShowServerInfo (tCliHandle CliHandle, tIPvXAddr * pServerAddress,
                   UINT1 *au1HostName)
{

    UINT4               u4CurrentIndex = 0;
    UINT4               u4NextIndex = 0;
    UINT4               u4OutCome = 0;
    UINT4               u4EntryStatus = 0;
    UINT4               u4ResponseTime = 0;
    UINT4               u4MaxRetransmit = 0;
    UINT4               u4Address = 0;
    CHR1               *pu1String = NULL;
    UINT1               au1TmpSecret[LEN_SECRET - 2];
    INT4                i4CliRetVal = 0;
    INT4                i4AddrType = 0;
    INT4                i4AuthPort = 0;
    INT4                i4AccPort = 0;
    tSNMP_OCTET_STRING_TYPE Password;
    tSNMP_OCTET_STRING_TYPE IfAddr;
    UINT1               au1TempAddr[DNS_MAX_QUERY_LEN];
    UINT1               u1Flag = 0;

    MEMSET (&IfAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TempAddr, 0, DNS_MAX_QUERY_LEN);
    IfAddr.pu1_OctetList = au1TempAddr;
    IfAddr.i4_Length = 0;

    MEMSET (au1TmpSecret, 0, (LEN_SECRET - 2));

    if (pServerAddress->u1Afi == 0)
    {
        /* Server address is not menioned, Display all servers
         * including primary server */

        nmhGetFsRadExtPrimaryServerAddressType (&i4AddrType);
        nmhGetFsRadExtPrimaryServer (&IfAddr);

        if (i4AddrType == (INT4) IPVX_ADDR_FMLY_IPV4)
        {
            PTR_FETCH4 (u4Address, IfAddr.pu1_OctetList);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
            CliPrintf (CliHandle, "\rPrimary Server           : %-17s\r\n",
                       pu1String);
        }

        if (i4AddrType == (INT4) IPVX_ADDR_FMLY_IPV6)
        {
            CliPrintf (CliHandle, "\rPrimary Server           : %-17s\r\n",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) IfAddr.
                                     pu1_OctetList));
        }

        if (i4AddrType == (INT4) RAD_DNS_FAMILY)
        {
            CliPrintf (CliHandle, "\rPrimary Server           : %s\r\n",
                       IfAddr.pu1_OctetList);
        }

        u4OutCome =
            (INT4) (nmhGetFirstIndexFsRadExtServerTable
                    ((INT4 *) &u4NextIndex));

        if (u4OutCome == SNMP_FAILURE)
        {
            return (CLI_SUCCESS);
        }
    }
    else
    {
        u1Flag = 1;
        if (pServerAddress->u1Afi != (UINT1) RAD_DNS_FAMILY)
        {
            u4NextIndex = RadGetServerIndex (pServerAddress);
        }
        else
        {
            u4NextIndex = RadGetServerHostIndex (au1HostName);
        }

        if (u4NextIndex == (UINT4) RAD_ERROR)
        {
            CliPrintf (CliHandle, "\r%%No Such Server is configured!!\r\n");
            return (CLI_SUCCESS);
        }
    }

    CliPrintf (CliHandle, "\r\nRadius Server Host Information \r\n");
    CliPrintf (CliHandle, "\r------------------------------\r\n");

    do
    {
        u4CurrentIndex = u4NextIndex;
        nmhGetFsRadExtServerEntryStatus ((INT4) u4NextIndex,
                                         (INT4 *) &u4EntryStatus);
        nmhGetFsRadExtServerAddress (u4NextIndex, &IfAddr);
        nmhGetFsRadExtServerAddrType (u4NextIndex, &i4AddrType);
        Password.pu1_OctetList = (UINT1 *) &au1TmpSecret;
        nmhGetFsRadExtServerSharedSecret (u4NextIndex, &Password);
        nmhGetFsRadExtServerResponseTime ((INT4) u4NextIndex,
                                          (INT4 *) &u4ResponseTime);
        nmhGetFsRadExtServerMaximumRetransmission ((INT4) u4NextIndex,
                                                   (INT4 *) &u4MaxRetransmit);

        nmhGetFsRadExtAuthClientServerPortNumber ((INT4) u4NextIndex,
                                                  &i4AuthPort);
        nmhGetFsRadExtAccClientServerPortNumber ((INT4) u4NextIndex,
                                                 &i4AccPort);

        /* Port number */
        CliPrintf (CliHandle, "\rIndex                    : %d\r\n",
                   u4NextIndex);

        /*address */
        if (i4AddrType == (INT4) IPVX_ADDR_FMLY_IPV4)
        {
            PTR_FETCH4 (u4Address, IfAddr.pu1_OctetList);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
            CliPrintf (CliHandle, "\rServer address           : %-17s\r\n",
                       pu1String);
        }

        if (i4AddrType == (INT4) IPVX_ADDR_FMLY_IPV6)
        {
            CliPrintf (CliHandle, "\rServer address           : %-17s\r\n",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) IfAddr.
                                     pu1_OctetList));
        }

        if (i4AddrType == (INT4) RAD_DNS_FAMILY)
        {
            CliPrintf (CliHandle, "\rPrimary Server           : %-17s\r\n",
                       IfAddr.pu1_OctetList);
        }

        /*password */
        Password.pu1_OctetList = (UINT1 *) &au1TmpSecret;
        CliPrintf (CliHandle,
                   "\rShared secret            : %s\r\n",
                   Password.pu1_OctetList);

        /*Status */
        if (u4EntryStatus == ACTIVE)
        {
            CliPrintf (CliHandle, "\rRadius Server Status     : Enabled \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "\rRadius Server Status     : Disabled \r\n");
        }

        /*response time */
        CliPrintf (CliHandle, "\rResponse Time            : %d\r\n",
                   u4ResponseTime);

        /*maximum retransmission */
        CliPrintf (CliHandle, "\rMaximum Retransmission   : %d\r\n",
                   u4MaxRetransmit);

        CliPrintf (CliHandle, "\rAuthentication Port      : %d\r\n",
                   i4AuthPort);

        CliPrintf (CliHandle, "\rAccounting Port          : %d\r\n", i4AccPort);

        i4CliRetVal = CliPrintf (CliHandle,
                                 "\r--------------------------------------------\r\n");

        if (i4CliRetVal == CLI_FAILURE)
        {
            return CLI_SUCCESS;
        }
    }
    while ((nmhGetNextIndexFsRadExtServerTable ((INT4) u4CurrentIndex,
                                                (INT4 *) &u4NextIndex)
            != SNMP_FAILURE) && (u1Flag == 0));

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RadShowStats                                       */
/*                                                                           */
/*     DESCRIPTION      : This function shows the Statistics of the Radius   */
/*                        Server                                             */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
RadShowStats (tCliHandle CliHandle)
{
    CHR1               *pu1String = NULL;
    UINT4               u4CurrentIndex = 0;
    UINT4               u4NextIndex = 0;
    UINT4               u4OutCome = 0;
    UINT4               u4PortNum = 0;

    UINT4               u4TripTime = 0;
    UINT4               u4AccessRequest = 0;
    UINT4               u4Address = 0;

    UINT4               u4AccessAccept = 0;
    UINT4               u4AccessReject = 0;
    UINT4               u4AccessChallenge = 0;
    UINT4               u4AccessResponse = 0;
    UINT4               u4AccessRetransmit = 0;
    UINT4               u4BadAuth = 0;
    UINT4               u4PendingRequest = 0;
    UINT4               u4AuthTimeOut = 0;
    UINT4               u4UnknownType = 0;
    INT4                i4AddrType = 0;
    tSNMP_OCTET_STRING_TYPE IfAddr;
    UINT1               au1Addr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1IfAddr[IPVX_MAX_INET_ADDR_LEN];
    IfAddr.pu1_OctetList = au1Addr;

    CliPrintf (CliHandle, "\r\nRadius Server Statistics \r\n");
    CliPrintf (CliHandle, "\r-------------------------\r\n");

    u4OutCome =
        (INT4) (nmhGetFirstIndexRadiusAuthServerTable ((INT4 *) &u4NextIndex));
    if (u4OutCome == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    do
    {

        nmhGetFsRadExtServerAddress (u4NextIndex, &IfAddr);
        nmhGetFsRadExtServerAddrType (u4NextIndex, &i4AddrType);
        nmhGetFsRadExtAuthClientServerPortNumber (u4NextIndex,
                                                  (INT4 *) &u4PortNum);

        nmhGetFsRadExtAuthClientRoundTripTime (u4NextIndex, &u4TripTime);
        nmhGetFsRadExtAuthClientAccessRequests (u4NextIndex, &u4AccessRequest);
        nmhGetFsRadExtAuthClientAccessRetransmissions (u4NextIndex,
                                                       &u4AccessRetransmit);
        nmhGetFsRadExtAuthClientAccessAccepts (u4NextIndex, &u4AccessAccept);
        nmhGetFsRadExtAuthClientAccessRejects (u4NextIndex, &u4AccessReject);
        nmhGetFsRadExtAuthClientAccessChallenges (u4NextIndex,
                                                  &u4AccessChallenge);
        nmhGetFsRadExtAuthClientMalformedAccessResponses (u4NextIndex,
                                                          &u4AccessResponse);
        nmhGetFsRadExtAuthClientBadAuthenticators (u4NextIndex, &u4BadAuth);
        nmhGetFsRadExtAuthClientPendingRequests (u4NextIndex,
                                                 &u4PendingRequest);
        nmhGetFsRadExtAuthClientTimeouts (u4NextIndex, &u4AuthTimeOut);
        nmhGetFsRadExtAuthClientUnknownTypes (u4NextIndex, &u4UnknownType);

        /* Port number */
        CliPrintf (CliHandle,
                   "\rIndex                            : %d\r\n", u4NextIndex);
        /*server address */
        if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            PTR_FETCH4 (u4Address, IfAddr.pu1_OctetList);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
            CliPrintf (CliHandle,
                       "\rServer address                   : %-17s\r\n",
                       pu1String);
        }

        if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            MEMCPY (au1IfAddr, IfAddr.pu1_OctetList, IPVX_IPV6_ADDR_LEN);
            CliPrintf (CliHandle,
                       "\rServer address                   : %-17s\r\n",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) au1IfAddr));
        }

        if (i4AddrType == IPVX_DNS_FAMILY)
        {
            CliPrintf (CliHandle,
                       "\rServer address                   : %-17s\r\n",
                       IfAddr.pu1_OctetList);

        }

        /*port number */
        CliPrintf (CliHandle,
                   "\rUDP port number                  : %d\r\n", u4PortNum);

        /*round trip time */
        CliPrintf (CliHandle,
                   "\rRound trip time                  : %d\r\n", u4TripTime);

        /*requests */
        CliPrintf (CliHandle,
                   "\rNo of request packets            : %d\r\n",
                   u4AccessRequest);

        /*retransmissions */
        CliPrintf (CliHandle,
                   "\rNo of retransmitted packets      : %d\r\n",
                   u4AccessRetransmit);

        /*accepts */
        CliPrintf (CliHandle,
                   "\rNo of access-accept packets      : %d\r\n",
                   u4AccessAccept);

        /*rejects */
        CliPrintf (CliHandle,
                   "\rNo of access-reject packets      : %d\r\n",
                   u4AccessReject);

        /*challenges */
        CliPrintf (CliHandle,
                   "\rNo of access-challenge packets   : %d\r\n",
                   u4AccessChallenge);
        /*mal performed access responses */
        CliPrintf (CliHandle,
                   "\rNo of malformed access responses : %d\r\n",
                   u4AccessResponse);

        /*bad authenticators */
        CliPrintf (CliHandle,
                   "\rNo of bad authenticators         : %d\r\n", u4BadAuth);

        /*pending requests */
        CliPrintf (CliHandle,
                   "\rNo of pending requests           : %d\r\n",
                   u4PendingRequest);

        /*time outs */
        CliPrintf (CliHandle,
                   "\rNo of time outs                  : %d\r\n",
                   u4AuthTimeOut);

        /*unknown types */
        CliPrintf (CliHandle,
                   "\rNo of unknown types              : %d\r\n",
                   u4UnknownType);

        CliPrintf (CliHandle,
                   "\r--------------------------------------------\r\n");
        u4CurrentIndex = u4NextIndex;
    }
    while (nmhGetNextIndexRadiusAuthServerTable
           ((INT4) u4CurrentIndex, (INT4 *) &u4NextIndex) != SNMP_FAILURE);

    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssRadiusShowDebugging                             */
/*                                                                           */
/*     DESCRIPTION      : This function prints the Radius debug level        */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
IssRadiusShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DbgLevel = 0;

    nmhGetFsRadExtDebugMask (&i4DbgLevel);
    if (i4DbgLevel == 0)
    {
        return;
    }
    CliPrintf (CliHandle, "\rRADIUS :");

    if ((i4DbgLevel & RAD_ERR_MASK) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RADIUS error debugging is on");
    }
    if ((i4DbgLevel & RAD_EVENT_MASK) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RADIUS event debugging is on");
    }
    if ((i4DbgLevel & RAD_PACKET_MASK) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RADIUS packet debugging is on");
    }
    if ((i4DbgLevel & RAD_RESP_MASK) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RADIUS response debugging is on");
    }
    if ((i4DbgLevel & RAD_TIMER_MASK) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RADIUS timer debugging is on");
    }

    CliPrintf (CliHandle, "\r\n");
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : RadiusShowRunningConfig                            */
/*                                                                           */
/*     DESCRIPTION      : This function displays current configuration       */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
RadiusShowRunningConfig (tCliHandle CliHandle)
{
    CHR1               *pu1String = NULL;
    INT4                i4RowStatus = 0;
    INT4                i4AddrType = 0;
    INT4                i4AuthPort = 0;
    INT4                i4AccPort = 0;
    UINT1               au1TmpSecret[LEN_SECRET - 2];
    UINT1               u1isShowAll = TRUE;
    UINT4               u4CurrentIndex = 0;
    UINT4               u4NextIndex = 0;
    UINT4               u4ResponseTime = 0;
    UINT4               u4MaxRetransmit = 0;
    UINT4               u4Address = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    tSNMP_OCTET_STRING_TYPE Password;
    tSNMP_OCTET_STRING_TYPE ServerIpAddress;
    tSNMP_OCTET_STRING_TYPE PrimaryServerIpAddress;
    UINT1               au1TmpArray[DNS_MAX_QUERY_LEN];
    UINT1               au1Tmp1Array[DNS_MAX_QUERY_LEN];
    INT4                i4ServerIpLength = 0;

    MEMSET (au1TmpArray, 0, DNS_MAX_QUERY_LEN);
    MEMSET (au1Tmp1Array, 0, DNS_MAX_QUERY_LEN);

    ServerIpAddress.pu1_OctetList = au1TmpArray;
    PrimaryServerIpAddress.pu1_OctetList = au1Tmp1Array;

    CliRegisterLock (CliHandle, RadiusLock, RadiusUnLock);
    RADIUS_LOCK ();
    if (nmhGetFirstIndexFsRadExtServerTable ((INT4 *) &u4NextIndex)
        == SNMP_SUCCESS)
    {
        do
        {
            MEMSET (au1TmpSecret, 0, (LEN_SECRET - 2));
            MEMSET (ServerIpAddress.pu1_OctetList, 0, DNS_MAX_QUERY_LEN);
            nmhGetFsRadExtServerEntryStatus (u4NextIndex, &i4RowStatus);
            if (i4RowStatus == ACTIVE)
            {
                nmhGetFsRadExtServerAddress (u4NextIndex, &ServerIpAddress);
                nmhGetFsRadExtServerAddrType (u4NextIndex, &i4AddrType);

                Password.pu1_OctetList = (UINT1 *) &au1TmpSecret;
                nmhGetFsRadExtServerSharedSecret (u4NextIndex, &Password);
                nmhGetFsRadExtServerResponseTime ((INT4) u4NextIndex,
                                                  (INT4 *) &u4ResponseTime);
                nmhGetFsRadExtServerMaximumRetransmission ((INT4) u4NextIndex,
                                                           (INT4 *)
                                                           &u4MaxRetransmit);

                nmhGetFsRadExtAuthClientServerPortNumber (u4NextIndex,
                                                          &i4AuthPort);

                nmhGetFsRadExtAccClientServerPortNumber (u4NextIndex,
                                                         &i4AccPort);
                CliPrintf (CliHandle, "\r\n%s", "radius-server host");
                /*address */

                if (i4AddrType == (INT4) IPVX_ADDR_FMLY_IPV4)
                {
                    PTR_FETCH4 (u4Address, ServerIpAddress.pu1_OctetList);
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
                    CliPrintf (CliHandle, " %s", pu1String);
                }

                if (i4AddrType == (INT4) IPVX_ADDR_FMLY_IPV6)
                {
                    CliPrintf (CliHandle, " %s",
                               Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                             ServerIpAddress.pu1_OctetList));
                }

                if (i4AddrType == (INT4) RAD_DNS_FAMILY)
                {
                    CliPrintf (CliHandle, " %s", ServerIpAddress.pu1_OctetList);
                }

                if (i4AuthPort != (INT4) RADIUS_SERVER_PORT_AUTH)
                {
                    CliPrintf (CliHandle, " auth-port %d", i4AuthPort);
                }

                if (i4AccPort != (INT4) RADIUS_SERVER_PORT_ACC)
                {
                    CliPrintf (CliHandle, " acct-port %d", i4AccPort);
                }

                /*response time */
                if (u4ResponseTime != DEFAULT_RESPONSE_TIME)
                {
                    CliPrintf (CliHandle, " timeout %d", u4ResponseTime);
                }

                /*maximum retransmission */
                if (u4MaxRetransmit != DEFAULT_MAX_RETRANS)
                {
                    CliPrintf (CliHandle, " retransmit %d", u4MaxRetransmit);
                }

                if (Password.i4_Length > 0)
                {
                    CliPrintf (CliHandle, " %s", "key");
                    /*password */
                    Password.pu1_OctetList = (UINT1 *) &au1TmpSecret;
                    u4PagingStatus = CliPrintf (CliHandle,
                                                " %s", Password.pu1_OctetList);
                }
                nmhGetFsRadExtPrimaryServer (&PrimaryServerIpAddress);

                if (PrimaryServerIpAddress.i4_Length != 0)
                {
                    if (PrimaryServerIpAddress.i4_Length !=
                        ServerIpAddress.i4_Length)
                    {
                        i4ServerIpLength =
                            MEM_MAX_BYTES (PrimaryServerIpAddress.i4_Length,
                                           ServerIpAddress.i4_Length);
                    }
                    else
                    {
                        i4ServerIpLength = PrimaryServerIpAddress.i4_Length;
                    }

                    if (MEMCMP (PrimaryServerIpAddress.pu1_OctetList,
                                ServerIpAddress.pu1_OctetList,
                                i4ServerIpLength) == 0)
                    {
                        CliPrintf (CliHandle, " %s", "primary");
                    }
                }
                CliPrintf (CliHandle, "\r\n");
            }
            if (u4PagingStatus == CLI_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            u4CurrentIndex = u4NextIndex;

            if (nmhGetNextIndexFsRadExtServerTable ((INT4) u4CurrentIndex,
                                                    (INT4 *) &u4NextIndex)
                == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);

    }

    RADIUS_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RadSetProxyStatus                                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets/resets the radius proxy status  */
/*                                                                           */
/*     INPUT            : u1StatusValue - Radius Proxy Status                */
/*                                                                           */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
RadSetProxyStatus (tCliHandle CliHandle, UINT1 u1StatusVal)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2RadiusProxyStatus (&u4ErrCode, u1StatusVal) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    nmhSetRadiusProxyStatus (u1StatusVal);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RadShowRadiusProxyInfo                             */
/*                                                                           */
/*     DESCRIPTION      : This function shows the Radius Proxy Information   */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
RadShowProxyInfo (tCliHandle CliHandle)
{
    INT4                i4RadiusProxyStatus = 0;

    nmhGetRadiusProxyStatus (&i4RadiusProxyStatus);

    /*Status */
    if (i4RadiusProxyStatus == RADIUS_PROXY_ENABLED)
    {
        CliPrintf (CliHandle, "\rRadius Proxy Status     : Enabled \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\rRadius Proxy  Status     : Disabled \r\n");
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}
#endif
