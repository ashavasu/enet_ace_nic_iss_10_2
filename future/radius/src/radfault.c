/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radfault.c,v 1.11 2015/04/28 12:26:24 siva Exp $ 
 *
 * Description: This file contains the module for fault information.
 *
 *******************************************************************/

#include "radcom.h"

/*----------------------------------------------------------------------------
Procdure    : radiusInformFault
Description    : This module infroms the fault event to the fault manager.
Input(s)    : The fault type (event type)
Output(s)    : None.
Returns        : None
Called By    : 
Calling        : fault_report_to_fm()
Author        : G.Vedavinayagam
Date        : 09/03/98 (dd/mm/yy)
odifications    :
-----------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
radiusInformFault (FS_ULONG ul_Fault_Event_Type)
#else
VOID
radiusInformFault (ul_Fault_Event_Type)
     FS_ULONG            ul_Fault_Event_Type;
#endif
{
#ifndef TRACE_WANTED
    UNUSED_PARAM (ul_Fault_Event_Type);
#endif
    RAD_EVENT_PRINT ((char *) ul_Fault_Event_Type);
}

/**************************** End of file "radfault.c" ***********************/
