/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radapi.c,v 1.1 2015/04/28 12:26:24 siva Exp $ 
 *
 * Description: This file contains utility function for radius
 *
 *******************************************************************/
#ifndef RADAPI_C
#define RADAPI_C

#include "radcom.h"

/*****************************************************************************/
/* Function Name      : RadApiRestoreSharedSecretFromNvRam                   */
/*                                                                           */
/* Description        : This function is invoked to retrieve shared secret   */
/*                      from  radiuskey file.                                */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                      pFsCbInfo - Call Back Function                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RAD__SUCCESS/RAD__FAILURE                            */
/*****************************************************************************/
INT4
RadApiRestoreSharedSecretFromNvRam (VOID)
{
    INT4                i4RetVal = RAD_SUCCESS;
    RADIUS_LOCK ();
    i4RetVal = RadUtilCallBack (RADIUS_READ_SECRET_FROM_NVRAM);
    RADIUS_UNLOCK ();
    return i4RetVal;
}


/*****************************************************************************/
/* Function Name      : RadApiHandleRadAuthCB                                */
/*                                                                           */
/* Description        : This function is invoked for wss pnac module         */ 
/* 			Interaction 		  			     */
/*                                                                           */
/* Input(s)           : pRadiusInputAuth ,pRadiusServerAuth,u4IfIndex,       */
/*		        u1ReqId 					     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Call Back Function Pointer                           */
/*****************************************************************************/

tRadHandleRadAuthCB
RadApiHandleRadAuthCB  (tRADIUS_INPUT_AUTH * pRadiusInputAuth,
                        tRADIUS_SERVER * pRadiusServerAuth,
                        UINT4 u4IfIndex, UINT1 u1ReqId, INT4 *pi4Ret)
{
    tRadHandleRadAuthCB pHandleRadAuthCB = NULL;

    UNUSED_PARAM (pRadiusServerAuth);


    pHandleRadAuthCB = gRadiusGlobals.pRadHandleRadAuthCB;

     *pi4Ret = NOT_OK;

    if (pHandleRadAuthCB != NULL)
    {
        *pi4Ret = pHandleRadAuthCB (pRadiusInputAuth, NULL,
                            gRadiusGlobals.pRadHandleRadResCB,
                            u4IfIndex, u1ReqId);
	return pHandleRadAuthCB;
    }

    return pHandleRadAuthCB;
}


/*****************************************************************************/
/* Function Name      : RadApiRegisterHandleRadAuthResCB                     */
/*                                                                           */
/* Description        : This function is invoked to register the wss-pnac    */
/*                      Interaction function                                 */
/*                                                                           */
/* Input(s)           : RadAuthCB, RadAuthCB  			             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None			                             */
/*****************************************************************************/

VOID 
RadApiRegisterHandleRadAuthResCB (tRadHandleRadAuthCB  RadAuthCB, 
                                  tRadHandleRadResCB RadResCB)
{
     RADIUS_LOCK ();
     gRadiusGlobals.pRadHandleRadAuthCB = RadAuthCB;
     gRadiusGlobals.pRadHandleRadResCB = RadResCB;
     RADIUS_UNLOCK ();

}

/*****************************************************************************/
/* Function Name      : RadApiDeRegisterHandleRadAuthResCB                   */
/*                                                                           */
/* Description        : This function is invoked to Deregister the wss-pnac  */
/*                      Interaction function                                 */
/*                                                                           */
/* Input(s)           : None 		                                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID RadApiDeRegisterHandleRadAuthResCB (VOID)
{
     RADIUS_LOCK ();
     gRadiusGlobals.pRadHandleRadAuthCB = NULL;
     gRadiusGlobals.pRadHandleRadResCB = NULL;;
     RADIUS_UNLOCK ();
}

#endif
