/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radauth.c,v 1.28 2018/02/14 10:04:08 siva Exp $ 
 *
 * Description: This file contains all the modules for
 *              framing and transmitting a packet for
 *              authentication request.
 *
 *******************************************************************/

#include "radcom.h"
#include "arHmac_api.h"
#include "arMD5_inc.h"

/*-----------------------------------------------------------------------------
Procedure    : radiusAuthentication()
Description    : This module frames packet for authentication request, and
              transmits the packet to the server. Then the user request is
              entered in the User queue. It first validates the radius
              input given to it. If any error is found the packet framing
              is abandoned. The user password is hidden using Message
              digest algorithm. If the protocol used for authentication of
              the user is CHAP, then the password is not hidden. The CHAP
              response and the CHAP Identifier are taken as the Password.
              Then all the attributes are filled. After all attributes
              filled, the packet is transmitted.
Input(s)    : p_RadiusInputAuth
              p_RadiusServerAuth
OutPut(s)    : None
Returns        : The Identifier of the packet if packet is transmitted
              successfully
              -1, if there is failure in transmitting the packet.
Called By    : The User
Calling        : radiusValidateInputAuth()
              radiusFramePrePacketAuth()
              radiusHidePassword()
              radiusFillAttributesAuth()
              radiusTransmitPacket()
              radiusEnterUserQAuth()
Author        : G.Vedavinayagam
Date        : 11/11/97
odificaions    :
-----------------------------------------------------------------------------*/
#ifdef __STDC__
INT4
radiusAuthentication (tRADIUS_INPUT_AUTH * p_RadiusInputAuth,
                      tRADIUS_SERVER * p_RadiusServerAuth,
                      VOID (*CallBackfPtr) (VOID *),
                      UINT4 ifIndex, UINT1 u1ReqId)
#else
INT4
radiusAuthentication (p_RadiusInputAuth, p_RadiusServerAuth, CallBackfPtr,
                      ifIndex, u1ReqId)
     tRADIUS_INPUT_AUTH *p_RadiusInputAuth;
     tRADIUS_SERVER     *p_RadiusServerAuth;
     VOID                (*CallBackfPtr) (VOID *);
     UINT4               ifIndex;
#endif
{

    UINT1              *pa_u1RadiusPacketAuth = NULL;
    UINT2               u2_RadiusPacketLength = 0;
    UINT4               i4_Index = 0;
    UINT4               u4NoOfServers = 0;
    UINT4               u4ServersContacted = 0;
    UINT4               u4_NasIPAddr = 0;
    UINT4               u4DestIp = 0;

    /* If the radius server address is passed, check whether the server
     * is configured in Radius client's external server table */
    pa_u1RadiusPacketAuth = MemAllocMemBlk (gRadMemPoolId.u4RadPacketPoolId);

    if (NULL == pa_u1RadiusPacketAuth)
    {
        RAD_ERR_PRINT
            ("Error in allocating memory for pa_u1RadiusPacketAuth\n");
        return NOT_OK;
    }
    MEMSET (pa_u1RadiusPacketAuth, 0, sizeof (tu1RadiusPacketAuth));

    RADIUS_LOCK ();

    if (p_RadiusServerAuth != NULL)
    {
        for (i4_Index = 0; i4_Index < MAX_RAD_SERVERS_LIMIT; i4_Index++)
        {
            if (a_gServerTable[i4_Index])
            {
                if (IPVX_ADDR_COMPARE (p_RadiusServerAuth->ServerAddress,
                                       a_gServerTable[i4_Index]->
                                       ServerAddress) == 0)
                    break;
            }
        }

        /* Given Radius Server is not in the Radius Client's External 
         * Server table */
        if (i4_Index == MAX_RAD_SERVERS_LIMIT)
        {
            RAD_ERR_PRINT
                ("\n Server is not in the External Server Table...\n");

            RADIUS_UNLOCK ();
            MemReleaseMemBlock (gRadMemPoolId.u4RadPacketPoolId,
                                (UINT1 *) pa_u1RadiusPacketAuth);
            return NOT_OK;
        }
    }
    else
    {
        /* Check if the primary server is in the external server table */
        if (p_gPrimaryRadServer != NULL)
        {
            for (i4_Index = 0; i4_Index < MAX_RAD_SERVERS_LIMIT; i4_Index++)
            {
                if (a_gServerTable[i4_Index])
                {
                    if (IPVX_ADDR_COMPARE (p_gPrimaryRadServer->ServerAddress,
                                           a_gServerTable[i4_Index]->
                                           ServerAddress) == 0)
                        break;
                }
            }
        }

        /* If the primary server is NULL or the primary server is not in the
           external server table, get the primary server */
        if ((p_gPrimaryRadServer == NULL)
            || (i4_Index == MAX_RAD_SERVERS_LIMIT))
        {
            p_gPrimaryRadServer = RadGetAuthServer ();
            if (p_gPrimaryRadServer == NULL)
            {
                RAD_ERR_PRINT ("\n No Server Available for the Request...\n");

                RADIUS_UNLOCK ();
                MemReleaseMemBlock (gRadMemPoolId.u4RadPacketPoolId,
                                    (UINT1 *) pa_u1RadiusPacketAuth);
                return NOT_OK;
            }
        }
        MEMCPY (&u4DestIp, p_gPrimaryRadServer->ServerAddress.au1Addr,
                sizeof (UINT4));
        if (p_gPrimaryRadServer->ServerAddress.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            u4DestIp = OSIX_NTOHL (u4DestIp);
            ip_src_addr_to_use_for_dest (u4DestIp, &u4_NasIPAddr);
            p_RadiusInputAuth->u4_NasIPAddress = u4_NasIPAddr;
        }
        p_RadiusServerAuth = p_gPrimaryRadServer;
    }

    RAD_EVENT_PRINT (" Sending Authentication Request to the Server.\n");

    if (radiusValidateInputAuth (p_RadiusInputAuth) == NOT_OK)
    {
        p_gRadiusErrorLog->u4_ErrorInputInsufficientAuth++;
        RAD_ERR_PRINT ("Input not sufficient for authentication.\n");

        RADIUS_UNLOCK ();
        MemReleaseMemBlock (gRadMemPoolId.u4RadPacketPoolId,
                            (UINT1 *) pa_u1RadiusPacketAuth);

        return (NOT_OK);
    }

    if (radiusFramePrePacketAuth (pa_u1RadiusPacketAuth,
                                  &u2_RadiusPacketLength, u1ReqId) == NOT_OK)
    {
        RAD_ERR_PRINT
            ("\r\n Auth request not processed,  Packet Id not available.\n");

        RADIUS_UNLOCK ();
        MemReleaseMemBlock (gRadMemPoolId.u4RadPacketPoolId,
                            (UINT1 *) pa_u1RadiusPacketAuth);

        return (NOT_OK);

    }

    /* Get the number of Auth servers present in the external server table */
    u4NoOfServers = RadGetNumAuthServers ();

    /* Retransmit the request until the number of Radius servers contacted
     * is equal to the number of Radius servers configured  or pointer to the
     * Radius Server is NULL */
    for (u4ServersContacted = 1;
         (p_RadiusServerAuth != NULL && u4ServersContacted <= u4NoOfServers);
         p_RadiusServerAuth =
         RadGetNextServerEntry (p_RadiusServerAuth->ifIndex),
         u4ServersContacted++)
    {

        /* Form the Radius Auth Packet and Transmit the Request */
        if (RadiusTransmitAuth (p_RadiusInputAuth, pa_u1RadiusPacketAuth,
                                p_RadiusServerAuth, CallBackfPtr, ifIndex,
                                u4ServersContacted) == OK)
        {
            RADIUS_UNLOCK ();
            MemReleaseMemBlock (gRadMemPoolId.u4RadPacketPoolId,
                                (UINT1 *) pa_u1RadiusPacketAuth);
            return OK;
        }
    }

    RADIUS_UNLOCK ();

    MemReleaseMemBlock (gRadMemPoolId.u4RadPacketPoolId,
                        (UINT1 *) pa_u1RadiusPacketAuth);
    return NOT_OK;
}

/*----------------------------------------------------------------------------
Procedure    : radiusValidateInputAuth()
Description    : This module checks the presence of required inputs for a
              purticular protocol type of authentication. If the protocol
              type is CHAP, the presence of CHAP challenge, CHAP user
              Identifier, CHAP response and CHAP user name are checked. The
              length of the CHAP challenge is also validated. Its length
              should be >=5 bytes. If the protocol type is PAP, the 
              presence of PAP user name and PAP user password are checked.
              If the protocol type is other than these two mentioned, the 
              presence of other user name and other user password are 
              checked.
Input(s)    : p_RadiusInputAuth
Output(s)    : None
Returns        : 0, if the validation passes
              -1, if the validaion fails
Called By    : radiusAuthentication()
Calling        : None
Author        : G.Vedavinayagam
Date        : 11/11/97
odifications    :
-----------------------------------------------------------------------------*/
#ifdef __STDC__
INT1
radiusValidateInputAuth (tRADIUS_INPUT_AUTH * p_RadiusInputAuth)
#else
INT1
radiusValidateInputAuth (p_RadiusInputAuth)
     tRADIUS_INPUT_AUTH *p_RadiusInputAuth;
#endif
{

    tUSER_INFO_CHAP    *p_chapuser = NULL;
    tUSER_INFO_EAP     *p_eapuser = NULL;

    tUSER_INFO_PAP     *p_papuser = NULL;
    tUSER_INFO_OTHERS  *p_otheruser = NULL;
    tUSER_INFO_MS_CHAP *p_mschapuser = NULL;
    tMS_CHAP_CPW1      *p_mschapcpw1 = NULL;
    tMS_CHAP_CPW2      *p_mschapcpw2 = NULL;

    if (p_RadiusInputAuth->u1_ProtocolType == PRO_EAP)
    {
        p_eapuser = p_RadiusInputAuth->p_UserInfoEAP;
        if ((p_eapuser == NULL) ||
            (STRLEN ((char *) p_eapuser->a_u1Response) == 0))
        {
            return (NOT_OK);
        }

        if (p_eapuser->u1_IsUtf8EncodingReqd == UTF8_ENABLE)
        {
            if (radiusCalcUsrNameLen (p_eapuser->a_u4Utf8UserName,
                                      LEN_UTF8_USER_NAME) == 0)
            {
                RAD_ERR_PRINT ("UserName Field is empty\n");
                return (NOT_OK);
            }
        }
        else
        {
            if (STRLEN ((char *) p_eapuser->a_u1UserName) == 0)
            {
                RAD_ERR_PRINT ("UserName Field is empty\n");
                return (NOT_OK);
            }
        }

        return (OK);
    }
    else if (p_RadiusInputAuth->u1_ProtocolType == PRO_CHAP)
    {
        p_chapuser = p_RadiusInputAuth->p_UserInfoCHAP;
        if (p_chapuser == NULL)
        {
            return (NOT_OK);
        }
        if (p_chapuser->u1_IsUtf8EncodingReqd == UTF8_ENABLE)
        {
            if (radiusCalcUsrNameLen (p_chapuser->a_u4Utf8UserName,
                                      LEN_UTF8_USER_NAME) == 0)
            {
                RAD_ERR_PRINT ("UserName Field is empty\n");
                return (NOT_OK);
            }
        }
        else
        {
            if (STRLEN ((char *) p_chapuser->a_u1UserName) == 0)
            {
                RAD_ERR_PRINT ("UserName Field is empty\n");
                return (NOT_OK);
            }
        }

        if (STRLEN ((char *) p_chapuser->a_u1Challenge) < 5 ||
            STRLEN ((char *) p_chapuser->a_u1Response) == 0)
        {
            return (NOT_OK);
        }
        return (OK);
    }
    else if (p_RadiusInputAuth->u1_ProtocolType == PRO_PAP)
    {
        p_papuser = p_RadiusInputAuth->p_UserInfoPAP;
        if (p_papuser == NULL)
        {
            return (NOT_OK);
        }

        if (p_papuser->u1_IsUtf8EncodingReqd == UTF8_ENABLE)
        {
            if (radiusCalcUsrNameLen (p_eapuser->a_u4Utf8UserName,
                                      LEN_UTF8_USER_NAME) == 0)
            {
                RAD_ERR_PRINT ("UserName Field is empty\n");
                return (NOT_OK);
            }
        }
        else
        {
            if (STRLEN ((char *) p_papuser->a_u1UserName) == 0)
            {
                RAD_ERR_PRINT ("UserName Field is empty\n");
                return (NOT_OK);
            }
        }

        if (STRLEN ((char *) p_papuser->a_u1UserPasswd) == 0)
        {
            RAD_ERR_PRINT ("No UserName, strlen =0! \n");
            return (NOT_OK);
        }
        return (OK);
    }
    else if (p_RadiusInputAuth->u1_ProtocolType == PRO_OTHERS)
    {
        p_otheruser = p_RadiusInputAuth->p_UserInfoOTHERS;
        if (p_otheruser == NULL)
        {
            return (NOT_OK);
        }

        if (p_otheruser->u1_IsUtf8EncodingReqd == UTF8_ENABLE)
        {
            if (radiusCalcUsrNameLen (p_otheruser->a_u4Utf8UserName,
                                      LEN_UTF8_USER_NAME) == 0)
            {
                RAD_ERR_PRINT ("UserName Field is empty\n");
                return (NOT_OK);
            }
        }
        else
        {
            if (STRLEN ((char *) p_otheruser->a_u1UserName) == 0)
            {
                RAD_ERR_PRINT ("UserName Field is empty\n");
                return (NOT_OK);
            }
        }

        if (STRLEN ((char *) p_otheruser->a_u1UserPasswd) == 0)
        {
            return (NOT_OK);
        }
        return (OK);
    }
    else if (p_RadiusInputAuth->u1_ProtocolType == PRO_MS_CHAP)
    {
        p_mschapuser = p_RadiusInputAuth->p_UserInfoMschap;
        if (p_mschapuser == NULL)
        {
            return (NOT_OK);
        }
        if (p_mschapuser->u1_IsUtf8EncodingReqd == UTF8_ENABLE)
        {
            if (radiusCalcUsrNameLen (p_mschapuser->a_u4Utf8UserName,
                                      LEN_UTF8_USER_NAME) == 0)
            {
                RAD_ERR_PRINT ("UserName Field is empty\n");
                return (NOT_OK);
            }
        }
        else
        {
            if (STRLEN ((char *) p_mschapuser->a_u1UserName) == 0)
            {
                RAD_ERR_PRINT ("UserName Field is empty\n");
                return (NOT_OK);
            }
        }

        if (STRLEN ((char *) p_mschapuser->a_u1UserPasswd) == 0)
        {
            return (NOT_OK);
        }
        return (OK);
    }
    else if (p_RadiusInputAuth->u1_ProtocolType == MS_CHAP_CPW1)
    {
        p_mschapcpw1 = p_RadiusInputAuth->p_MsChapChgPw1;
        if (p_mschapcpw1 == NULL)
        {
            return (NOT_OK);
        }

        if (p_mschapcpw1->u1_IsUtf8EncodingReqd == UTF8_ENABLE)
        {
            if (radiusCalcUsrNameLen (p_mschapcpw1->a_u4Utf8UserName,
                                      LEN_UTF8_USER_NAME) == 0)
            {
                RAD_ERR_PRINT ("UserName Field is empty\n");
                return (NOT_OK);
            }
        }
        else
        {
            if (STRLEN ((char *) p_mschapcpw1->a_u1UserName) == 0)
            {
                RAD_ERR_PRINT ("UserName Field is empty\n");
                return (NOT_OK);
            }
        }

        if (STRLEN ((char *) p_mschapcpw1->a_u1UserPasswd) == 0)
        {
            return (NOT_OK);
        }
        return (OK);
    }
    else if (p_RadiusInputAuth->u1_ProtocolType == MS_CHAP_CPW2)
    {
        p_mschapcpw2 = p_RadiusInputAuth->p_MsChapChgPw2;
        if (p_mschapcpw2 == NULL)
        {
            return (NOT_OK);
        }

        if (p_mschapcpw2->u1_IsUtf8EncodingReqd == UTF8_ENABLE)
        {
            if (radiusCalcUsrNameLen (p_mschapcpw1->a_u4Utf8UserName,
                                      LEN_UTF8_USER_NAME) == 0)
            {
                RAD_ERR_PRINT ("UserName Field is empty\n");
                return (NOT_OK);
            }
        }
        else
        {
            if (STRLEN ((char *) p_mschapcpw2->a_u1UserName) == 0)
            {
                RAD_ERR_PRINT ("UserName Field is empty\n");
                return (NOT_OK);
            }
        }

        if (STRLEN ((char *) p_mschapcpw2->a_u1UserPasswd) == 0)
        {
            return (NOT_OK);
        }
        return (OK);
    }
    return (NOT_OK);
}

/*---------------------------------------------------------------------------
Procedure    : radiusFramePrePacketAuth()
Description    : This module frames the preliminary packet for authentication
              request. It fills the Code, Request authenticator and
              Identifier.
Input(s)    : a_u1RadiusPacketAuth
              &u2_RadiusPacketLength
Output(s)    : a_u1RadiusReqAuthAuth
Returns        :0, in case of success
                -1, in the case of failure.
Called By    : radiusAuthentication()
Calling        : radiusGenReqAuthenticatorAuth()
Author        : G.Vedavinayagam
Date        : 11/11/97
odifications    :
-----------------------------------------------------------------------------*/
#ifdef __STDC__
INT1
radiusFramePrePacketAuth (UINT1 *a_u1PacketAuth, UINT2 *u2_PacketLength,
                          UINT1 u1PacketId)
#else
INT1
radiusFramePrePacketAuth (a_u1PacketAuth, u2_PacketLength, u1PacketId)
     UINT1               a_u1PacketAuth[];
     UINT2              *u2_PacketLength;
#endif
{

    UINT1               a_u1ReqAuthAuth[LEN_REQ_AUTH_AUTH] = { 0 };
    UINT1               u1PktId = 0;
    INT4                i4UserQSize = 0;

    MEMSET (a_u1PacketAuth, 0, LEN_TX_PKT);

    a_u1PacketAuth[PKT_TYPE] = ACCESS_REQUEST;

    /* Get the packet free packet id, which shouldn't be 0, and should not be
     *  used by other auth request packet */
    u1PktId = u1PacketId;
    for (i4UserQSize = 1; i4UserQSize < Q_TABLE_SIZE; i4UserQSize++, u1PktId++)
    {
        /* if the packet id is 0, increment the id since the packet id
           of 0 is not allowed */
        if (u1PktId == 0)
            u1PktId++;

        if (a_gUserTable[u1PktId] == NULL)
            break;
    }
    /* if the packet id reaches Q_TABLE_SIZE , return NOT OK */
    if (i4UserQSize == Q_TABLE_SIZE)
        return NOT_OK;

    a_u1PacketAuth[PKT_ID] = u1PktId;

    radiusGenReqAuthenticatorAuth (a_u1ReqAuthAuth);
    MEMCPY (a_u1PacketAuth + PKT_REQA, a_u1ReqAuthAuth, LEN_REQ_AUTH_AUTH);

    *u2_PacketLength = LEN_PKT_MIN;

    return OK;
}

/*----------------------------------------------------------------------------
Procedure    : radiusGenReqAuthenticatorAuth()
Description    : This module generates a random unpredictable 16 byte no.
Input(s)    : ReqAuthAuth
Output(s)    : ReqAuthAuth
Returns        : None
Called By    : radiusFramePrePacketAuth()
Calling        : None
Author        : G.Vedavinayagam
Date        : 11/11/97
odifcations    :
-----------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
radiusGenReqAuthenticatorAuth (UINT1 *a_u1ReqAuthAuth)
#else
VOID
radiusGenReqAuthenticatorAuth (a_u1ReqAuthAuth)
     UINT1               a_u1ReqAuthAuth[];
#endif
{
    INT4                i4Index = 0, i4RandNo = 0;
    INT4                i4Temp = 0;

    OsixGetSysTime ((tOsixSysTime *) & i4Temp);
    srand (i4Temp);

    for (i4Index = 0; i4Index < LEN_REQ_AUTH_AUTH; i4Index++)
    {
        i4RandNo = rand ();
        MEMCPY (a_u1ReqAuthAuth, &i4RandNo, sizeof (int));
        a_u1ReqAuthAuth += sizeof (int);
        i4Index += sizeof (int);
    }
}

/*----------------------------------------------------------------------------
Procedure    : radiusHidePassword()
Description    : This module hides the user password using the Message Digest
              algorithm. It first intialises an array of size 
              "LEN_PASSWD+16" to NULLs. Then it copies the password from 
              the user password array to this array. Then the length is
              adjusted such that is multiples of 16. This is done for
              padding the password with NULLs. Then the first 16 bytes of 
              padded password is XORed with MD5 hash value of string of
              shared secret and Request Authenticator. The XORed value is
              the hidden password if the password if size is <= 16 bytes.
              If the paddes password size is > 16 bytes, the next 16 bytes
              of padded password is XORed with MD5 has value of string of
              shared secret and result of previous XOR operation. This
              operation is repeated till all 16 bytes of password are
              hidden. The results of all XOR operation are concatenated to
              get the full hidden password. 
Input(s)    : p_u1UserPassword
              p_u1RadiusReqAuthAuth
              p_RadiusServerAuth
Output(s)    : a_u1RadHiddenPassword
              i4_Length
Returns        : None
Called By    : radiusAuthentication()
Calling        : radiusMakeStringAuth()
              RADIUS_MESSAGE_DIGEST()
Author        : G.Vedavinayagam
Date        : 11/11/97
odifications    :
-----------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
radiusHidePassword (UINT1 *a_u1UserPassword,
                    UINT1 *a_u1RequestAuth,
                    UINT1 *a_u1HiddenPassword,
                    INT4 *i4_Length, tRADIUS_SERVER * p_RadiusServerAuth)
#else
VOID
radiusHidePassword (a_u1UserPassword, a_u1RequestAuth,
                    a_u1HiddenPassword, i4_Length, p_RadiusServerAuth)
     UINT1               a_u1UserPassword[];
     UINT1               a_u1RequestAuth[];
     UINT1               a_u1HiddenPassword[];
     INT4               *i4_Length;
     tRADIUS_SERVER     *p_RadiusServerAuth;
#endif
{
    INT4                i4_PwdLen = 0, i4_StrLen = 0, i4_index = 0,
        i4_index1 = 0, i4_index2 = 0;
    UINT1               a_u1Concatenated[LEN_SECRET + LEN_REQ_AUTH_AUTH] =
        { 0 };
    UINT1               a_u1Digest[LEN_DIGEST] = { 0 };
    UINT1               a_u1PreResult[LEN_DIGEST] = { 0 };

    MEMSET (a_u1HiddenPassword, 0, LEN_PASSWD + LEN_PWD_PAD);
    MEMSET (a_u1Digest, 0, LEN_DIGEST);

    i4_PwdLen = STRLEN ((char *) a_u1UserPassword);
    MEMCPY (a_u1HiddenPassword, a_u1UserPassword, i4_PwdLen);

    if (i4_PwdLen % 16)
    {
        i4_PwdLen = i4_PwdLen + (16 - (i4_PwdLen % 16));
    }

    MEMCPY (a_u1PreResult, a_u1RequestAuth, LEN_DIGEST);

    i4_index1 = 0;
    for (i4_index = 1; i4_index <= i4_PwdLen / 16; i4_index++)
    {
        radiusMakeStringAuth (a_u1PreResult, a_u1Concatenated, &i4_StrLen,
                              p_RadiusServerAuth);

        RADIUS_MESSAGE_DIGEST (a_u1Digest, a_u1Concatenated, i4_StrLen);

        for (i4_index2 = 0; i4_index2 < LEN_DIGEST; i4_index2++)
        {
            a_u1HiddenPassword[i4_index2 + i4_index1] ^= a_u1Digest[i4_index2];
            a_u1PreResult[i4_index2] =
                a_u1HiddenPassword[i4_index2 + i4_index1];
        }

        i4_index1 += LEN_DIGEST;
    }                            /* for i4_index */

    *i4_Length = i4_PwdLen;
}

/*--------------------------------------------------------------------------
Procedure    : radiusMakeStringAuth()
Description    : This module concatenates the shared secret and the another
              string.
Input(s)    : a_u1PreResult
              p_RadiusServerAuth->a_u1Secret
Output(s)    : a_u1Concatenated
              i4_StrLen
Returns        : None
Called By    : radiusHidePassword()
Calling        : None
Author        : G.Vedavinayagam
Date        : 11/11/97
odifications    :
----------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
radiusMakeStringAuth (UINT1 *a_u1InString,
                      UINT1 *a_u1OutString,
                      INT4 *i4_StrLen, tRADIUS_SERVER * p_RadiusServerAuth)
#else
VOID
radiusMakeStringAuth (a_u1InString, a_u1OutString, i4_StrLen,
                      p_RadiusServerAuth)
     UINT1               a_u1InString[];
     UINT1               a_u1OutString[];
     INT4               *i4_StrLen;
     tRADIUS_SERVER     *p_RadiusServerAuth;
#endif
{
    INT4                i4_seclen = 0;

    MEMSET (a_u1OutString, 0, LEN_SECRET + LEN_REQ_AUTH_AUTH);

    i4_seclen = STRLEN ((char *) p_RadiusServerAuth->a_u1Secret);
    MEMCPY (a_u1OutString, p_RadiusServerAuth->a_u1Secret, i4_seclen);

    MEMCPY (a_u1OutString + i4_seclen, a_u1InString, LEN_DIGEST);

    *i4_StrLen = i4_seclen + LEN_DIGEST;
}

/*----------------------------------------------------------------------------
Procedure    : radiusFillAttributesAuth()
Description    : This module fills the attributes corresponding to the input
              present in the Radius input.
Input(s)    : a_u1RadHiddenPassword
              i4_PLength
              &u2_RadiusPacketLength
              p_u1RadiusPacketAuth
              p_RadiusInputAuth
Output(s)    : p_u1RadiusPacketAuth
Returns        : 0, if successfull
                -1, in the case of failure   
Called By    : radiusAuthentication
Calling        : None
Author        : G.Vedavinayagam
Date        : 11/11/97
odifications    :
-----------------------------------------------------------------------------*/
#ifdef __STDC__
INT1
radiusFillAttributesAuth (tRADIUS_INPUT_AUTH * p_RadiusInputAuth,
                          UINT1 a_u1Password[],
                          INT4 i4_PLength,
                          UINT2 *u2_PacketLength, UINT1 a_u1RadiusPacket[],
                          tRADIUS_SERVER * p_RadiusServerAuth)
#else
INT1
radiusFillAttributesAuth (p_RadiusInputAuth, a_u1Password, i4_PLength,
                          u2_PacketLength, a_u1RadiusPacket, p_RadiusServerAuth)
     tRADIUS_INPUT_AUTH *p_RadiusInputAuth;
     UINT1               a_u1Password[];
     INT4                i4_PLength;
     UINT2              *u2_PacketLength;
     UINT1               a_u1RadiusPacket[];
     tRADIUS_SERVER     *p_RadiusServerAuth
#endif
     {
         UINT1               u1_EapType = 0;
         UINT1               u1_Userlen = 0;
         UINT1               u1_strlen = 0;
         UINT2               u2_pktlen = 0;
         UINT2               u2_NetByte = 0;

         tUSER_INFO_EAP     *p_eapuser = NULL;
         UINT2               u2_TmpOffset = 0;
         UINT1               a_u1Signature[LEN_MESSAGE_AUTHENTICATOR] = { 0 };
         INT4                i4_Seclen = 0;
         UINT1               u1_StateLen;

         tUSER_INFO_CHAP    *p_chapuser = NULL;
         tUSER_INFO_PAP     *p_papuser = NULL;
         tUSER_INFO_OTHERS  *p_otheruser = NULL;
         tUSER_INFO_MS_CHAP *p_mschapuser = NULL;
         tMS_CHAP_CPW1      *p_mschapcpw1 = NULL;
         tMS_CHAP_CPW2      *p_mschapcpw2 = NULL;
         tSERVICES          *p_Services = NULL;
         UINT4               u4_NetByte = 0;
         UINT1               a_u1TempArray[LEN_UTF8_USER_NAME] = { 0 };
         UINT4               u4_Count = 0;

         u2_pktlen = *u2_PacketLength;

         MEMSET (a_u1Signature, 0, LEN_MESSAGE_AUTHENTICATOR);
         MEMSET (a_u1TempArray, 0, LEN_UTF8_USER_NAME);

         if (p_RadiusInputAuth->u1_ProtocolType == PRO_EAP)
         {
             p_eapuser = p_RadiusInputAuth->p_UserInfoEAP;

             /* Fill the EAP User Name */
             a_u1RadiusPacket[u2_pktlen] = A_USER_NAME;
             if (p_eapuser->u1_IsUtf8EncodingReqd == UTF8_ENABLE)
             {
                 u4_Count = radiusCalcUsrNameLen
                     (p_eapuser->a_u4Utf8UserName, LEN_UTF8_USER_NAME);

                 /* Convert mulilingual byte sequences into UTF-8 encoding */
                 if (UnicodeToUtf8
                     (p_eapuser->a_u4Utf8UserName, u4_Count,
                      a_u1TempArray, LEN_UTF8_USER_NAME) != UTF8_SUCCESS)
                 {
                     RAD_ERR_PRINT (" Error in UTF-8 encoding.\n");
                     return (NOT_OK);
                 }
                 u1_strlen = (UINT1) STRLEN ((char *) a_u1TempArray);
                 u1_Userlen = u1_strlen;
                 a_u1RadiusPacket[u2_pktlen + 1] =
                     (UINT1) (u1_strlen + LEN_RAD_ATTRIBUTE);

                 MEMCPY (a_u1RadiusPacket + u2_pktlen + LEN_RAD_ATTRIBUTE,
                         a_u1TempArray, u1_strlen);
             }
             else
             {
                 u1_strlen = (UINT1) STRLEN ((char *) p_eapuser->a_u1UserName);
                 u1_Userlen = u1_strlen;
                 a_u1RadiusPacket[u2_pktlen + 1] =
                     (UINT1) (u1_strlen + LEN_RAD_ATTRIBUTE);

                 MEMCPY (a_u1RadiusPacket + u2_pktlen + LEN_RAD_ATTRIBUTE,
                         p_eapuser->a_u1UserName, u1_strlen);
             }
             u2_pktlen += u1_strlen + LEN_RAD_ATTRIBUTE;
             radiusFillAttrEapuser (p_eapuser, a_u1RadiusPacket, &u2_pktlen);

             u2_TmpOffset = (UINT2) (u2_pktlen - (LEN_MESSAGE_AUTHENTICATOR));
         }

         else if (p_RadiusInputAuth->u1_ProtocolType == PRO_CHAP)
         {
             p_chapuser = p_RadiusInputAuth->p_UserInfoCHAP;

             /* RFC 001 */
             MEMSET ((a_u1RadiusPacket + PKT_REQA), 0, LEN_REQ_AUTH_AUTH);
             MEMCPY ((a_u1RadiusPacket + PKT_REQA), p_chapuser->a_u1Challenge,
                     LEN_REQ_AUTH_AUTH);

             a_u1RadiusPacket[u2_pktlen] = A_USER_NAME;    /* Fill the CHAP User Name */

             if (p_chapuser->u1_IsUtf8EncodingReqd == UTF8_ENABLE)
             {
                 u4_Count = radiusCalcUsrNameLen
                     (p_chapuser->a_u4Utf8UserName, LEN_UTF8_USER_NAME);

                 /* Convert mulilingual byte sequences into UTF-8 encoding */
                 if (UnicodeToUtf8
                     (p_chapuser->a_u4Utf8UserName, u4_Count,
                      a_u1TempArray, LEN_UTF8_USER_NAME) != UTF8_SUCCESS)
                 {
                     RAD_ERR_PRINT (" Error in UTF-8 encoding.\n");
                     return (NOT_OK);
                 }

                 u1_strlen = (UINT1) STRLEN ((char *) a_u1TempArray);
                 a_u1RadiusPacket[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);

                 MEMCPY (a_u1RadiusPacket + u2_pktlen + 2, a_u1TempArray,
                         u1_strlen);
             }
             else
             {
                 u1_strlen = (UINT1) STRLEN ((char *) p_chapuser->a_u1UserName);
                 a_u1RadiusPacket[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);

                 MEMCPY (a_u1RadiusPacket + u2_pktlen + 2,
                         p_chapuser->a_u1UserName, u1_strlen);
             }

             u2_pktlen += u1_strlen + 2;
             a_u1RadiusPacket[u2_pktlen] = A_CHAP_PASSWORD;    /* Fill the CHAP
                                                               Identifier and 
                                                               CHAP response as 
                                                               CHAP password */

             a_u1RadiusPacket[u2_pktlen + 1] = LEN_CHAP_PASSWORD;
             a_u1RadiusPacket[u2_pktlen + 2] = p_chapuser->u1_Identifier;

             MEMCPY (a_u1RadiusPacket + u2_pktlen + 3,
                     p_chapuser->a_u1Response, LEN_CHAP_RESPONSE);
             u2_pktlen += LEN_CHAP_PASSWORD;

             a_u1RadiusPacket[u2_pktlen] = A_CHAP_CHALLENGE;    /* Fill the CHAP
                                                                   Challenge */
             u1_strlen = (UINT1) STRLEN ((char *) p_chapuser->a_u1Challenge);
             a_u1RadiusPacket[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);

             MEMCPY (a_u1RadiusPacket + u2_pktlen + 2,
                     p_chapuser->a_u1Challenge, u1_strlen);
             u2_pktlen += u1_strlen + 2;

         }

         /* if PRO_PAP */
         else if (p_RadiusInputAuth->u1_ProtocolType == PRO_PAP)
         {
             p_papuser = p_RadiusInputAuth->p_UserInfoPAP;

             a_u1RadiusPacket[u2_pktlen] = A_USER_NAME;

             if (p_papuser->u1_IsUtf8EncodingReqd == UTF8_ENABLE)
             {
                 u4_Count = radiusCalcUsrNameLen
                     (p_papuser->a_u4Utf8UserName, LEN_UTF8_USER_NAME);

                 /* Convert mulilingual byte sequences into UTF-8 encoding */
                 if (UnicodeToUtf8
                     (p_papuser->a_u4Utf8UserName, u4_Count,
                      a_u1TempArray, LEN_UTF8_USER_NAME) != UTF8_SUCCESS)
                 {
                     RAD_ERR_PRINT (" Error in UTF-8 encoding.\n");
                     return (NOT_OK);
                 }

                 u1_strlen = (UINT1) STRLEN ((char *) a_u1TempArray);
                 a_u1RadiusPacket[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);

                 MEMCPY (a_u1RadiusPacket + u2_pktlen + 2, a_u1TempArray,
                         u1_strlen);
             }
             else
             {
                 u1_strlen = (UINT1) STRLEN ((char *) p_papuser->a_u1UserName);
                 a_u1RadiusPacket[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);

                 MEMCPY (a_u1RadiusPacket + u2_pktlen + 2,
                         p_papuser->a_u1UserName, u1_strlen);
             }

             u2_pktlen += u1_strlen + 2;

             a_u1RadiusPacket[u2_pktlen] = A_USER_PASSWORD;

             u1_strlen = (UINT1) i4_PLength;
             a_u1RadiusPacket[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);

             MEMCPY (a_u1RadiusPacket + u2_pktlen + 2, a_u1Password, u1_strlen);
             u2_pktlen += u1_strlen + 2;

         }

         /* if PRO_OTHERS */
         else if (p_RadiusInputAuth->u1_ProtocolType == PRO_OTHERS)
         {
             p_otheruser = p_RadiusInputAuth->p_UserInfoOTHERS;

             a_u1RadiusPacket[u2_pktlen] = A_USER_NAME;

             if (p_otheruser->u1_IsUtf8EncodingReqd == UTF8_ENABLE)
             {
                 u4_Count = radiusCalcUsrNameLen
                     (p_otheruser->a_u4Utf8UserName, LEN_UTF8_USER_NAME);

                 /* Convert mulilingual byte sequences into UTF-8 encoding */
                 if (UnicodeToUtf8
                     (p_otheruser->a_u4Utf8UserName, u4_Count,
                      a_u1TempArray, LEN_UTF8_USER_NAME) != UTF8_SUCCESS)
                 {
                     RAD_ERR_PRINT (" Error in UTF-8 encoding.\n");
                     return (NOT_OK);
                 }

                 u1_strlen = (UINT1) STRLEN ((char *) a_u1TempArray);
                 a_u1RadiusPacket[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);

                 MEMCPY (a_u1RadiusPacket + u2_pktlen + 2, a_u1TempArray,
                         u1_strlen);
             }
             else
             {
                 u1_strlen =
                     (UINT1) STRLEN ((char *) p_otheruser->a_u1UserName);
                 a_u1RadiusPacket[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);

                 MEMCPY (a_u1RadiusPacket + u2_pktlen + 2,
                         p_otheruser->a_u1UserName, u1_strlen);
             }
             u2_pktlen += u1_strlen + 2;

             a_u1RadiusPacket[u2_pktlen] = A_USER_PASSWORD;

             u1_strlen = (UINT1) i4_PLength;
             a_u1RadiusPacket[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);

             MEMCPY (a_u1RadiusPacket + u2_pktlen + 2, a_u1Password, u1_strlen);
             u2_pktlen += u1_strlen + 2;

         }

         /* if PRO_MS_CHAP */
         else if (p_RadiusInputAuth->u1_ProtocolType == PRO_MS_CHAP)
         {
             p_mschapuser = p_RadiusInputAuth->p_UserInfoMschap;
             a_u1RadiusPacket[u2_pktlen] = A_USER_NAME;

             if (p_mschapuser->u1_IsUtf8EncodingReqd == UTF8_ENABLE)
             {
                 u4_Count = radiusCalcUsrNameLen
                     (p_mschapuser->a_u4Utf8UserName, LEN_UTF8_USER_NAME);

                 /* Convert mulilingual byte sequences into UTF-8 encoding */
                 if (UnicodeToUtf8
                     (p_mschapuser->a_u4Utf8UserName, u4_Count,
                      a_u1TempArray, LEN_UTF8_USER_NAME) != UTF8_SUCCESS)
                 {
                     RAD_ERR_PRINT (" Error in UTF-8 encoding.\n");
                     return (NOT_OK);
                 }

                 u1_strlen = (UINT1) STRLEN ((char *) a_u1TempArray);
                 a_u1RadiusPacket[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);

                 MEMCPY (a_u1RadiusPacket + u2_pktlen + 2, a_u1TempArray,
                         u1_strlen);
             }
             else
             {
                 u1_strlen =
                     (UINT1) STRLEN ((char *) p_mschapuser->a_u1UserName);
                 a_u1RadiusPacket[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);

                 MEMCPY (a_u1RadiusPacket + u2_pktlen + 2,
                         p_mschapuser->a_u1UserName, u1_strlen);
             }
             u2_pktlen += u1_strlen + 2;

             a_u1RadiusPacket[u2_pktlen] = A_USER_PASSWORD;

             u1_strlen = (UINT1) i4_PLength;
             a_u1RadiusPacket[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);

             MEMCPY (a_u1RadiusPacket + u2_pktlen + 2, a_u1Password, u1_strlen);
             u2_pktlen += u1_strlen + 2;

             radiusFillAttrMschapuser (p_mschapuser, a_u1RadiusPacket,
                                       &u2_pktlen);
         }

         else if (p_RadiusInputAuth->u1_ProtocolType == MS_CHAP_CPW1)
         {
             p_mschapcpw1 = p_RadiusInputAuth->p_MsChapChgPw1;
             a_u1RadiusPacket[u2_pktlen] = A_USER_NAME;

             if (p_mschapcpw1->u1_IsUtf8EncodingReqd == UTF8_ENABLE)
             {
                 u4_Count = radiusCalcUsrNameLen
                     (p_mschapcpw1->a_u4Utf8UserName, LEN_UTF8_USER_NAME);

                 /* Convert mulilingual byte sequences into UTF-8 encoding */
                 if (UnicodeToUtf8
                     (p_mschapcpw1->a_u4Utf8UserName, u4_Count,
                      a_u1TempArray, LEN_UTF8_USER_NAME) != UTF8_SUCCESS)
                 {
                     RAD_ERR_PRINT (" Error in UTF-8 encoding.\n");
                     return (NOT_OK);
                 }

                 u1_strlen = (UINT1) STRLEN ((char *) a_u1TempArray);
                 a_u1RadiusPacket[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);

                 MEMCPY (a_u1RadiusPacket + u2_pktlen + 2, a_u1TempArray,
                         u1_strlen);
             }
             else
             {
                 u1_strlen =
                     (UINT1) STRLEN ((char *) p_mschapcpw1->a_u1UserName);
                 a_u1RadiusPacket[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);

                 MEMCPY (a_u1RadiusPacket + u2_pktlen + 2,
                         p_mschapcpw1->a_u1UserName, u1_strlen);
             }
             u2_pktlen += u1_strlen + 2;

             a_u1RadiusPacket[u2_pktlen] = A_USER_PASSWORD;

             u1_strlen = (UINT1) i4_PLength;
             a_u1RadiusPacket[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);

             MEMCPY (a_u1RadiusPacket + u2_pktlen + 2, a_u1Password, u1_strlen);
             u2_pktlen += u1_strlen + 2;

             radiusFillAttrMschapcpw1 (p_mschapcpw1, a_u1RadiusPacket,
                                       &u2_pktlen);
         }

         else if (p_RadiusInputAuth->u1_ProtocolType == MS_CHAP_CPW2)
         {
             p_mschapcpw2 = p_RadiusInputAuth->p_MsChapChgPw2;
             a_u1RadiusPacket[u2_pktlen] = A_USER_NAME;

             if (p_mschapcpw2->u1_IsUtf8EncodingReqd == UTF8_ENABLE)
             {
                 u4_Count = radiusCalcUsrNameLen
                     (p_mschapcpw2->a_u4Utf8UserName, LEN_UTF8_USER_NAME);

                 /* Convert mulilingual byte sequences into UTF-8 encoding */
                 if (UnicodeToUtf8
                     (p_mschapcpw2->a_u4Utf8UserName, u4_Count,
                      a_u1TempArray, LEN_UTF8_USER_NAME) != UTF8_SUCCESS)
                 {
                     RAD_ERR_PRINT (" Error in UTF-8 encoding.\n");
                     return (NOT_OK);
                 }

                 u1_strlen = (UINT1) STRLEN ((char *) a_u1TempArray);
                 a_u1RadiusPacket[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);

                 MEMCPY (a_u1RadiusPacket + u2_pktlen + 2, a_u1TempArray,
                         u1_strlen);
             }
             else
             {
                 u1_strlen =
                     (UINT1) STRLEN ((char *) p_mschapcpw2->a_u1UserName);
                 a_u1RadiusPacket[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);

                 MEMCPY (a_u1RadiusPacket + u2_pktlen + 2,
                         p_mschapcpw2->a_u1UserName, u1_strlen);
             }
             u2_pktlen += u1_strlen + 2;

             a_u1RadiusPacket[u2_pktlen] = A_USER_PASSWORD;

             u1_strlen = (UINT1) i4_PLength;
             a_u1RadiusPacket[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);

             MEMCPY (a_u1RadiusPacket + u2_pktlen + 2, a_u1Password, u1_strlen);
             u2_pktlen += u1_strlen + 2;

             radiusFillAttrMschapcpw2 (p_mschapcpw2, a_u1RadiusPacket,
                                       &u2_pktlen);
         }
         if (((u1_strlen =
               (UINT1) STRLEN ((char *) p_RadiusInputAuth->a_u1NasId)) != 0) &&
             (u1_strlen < LEN_NAS_ID))
         {
             a_u1RadiusPacket[u2_pktlen] = A_NAS_IDENTIFIER;
             a_u1RadiusPacket[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);

             MEMCPY (a_u1RadiusPacket + u2_pktlen + 2,
                     p_RadiusInputAuth->a_u1NasId, u1_strlen);
             u2_pktlen += u1_strlen + 2;
         }                        /* NAS Identifier */
         else
         {
             if (p_RadiusInputAuth->u4_NasIPAddress != 0x00000000 &&
                 p_RadiusInputAuth->u4_NasIPAddress != 0xffffffff)
             {
                 a_u1RadiusPacket[u2_pktlen] = A_NAS_IP_ADDRESS;
                 a_u1RadiusPacket[u2_pktlen + 1] = LEN_NAS_IP_ADDRESS;
                 u4_NetByte = OSIX_HTONL (p_RadiusInputAuth->u4_NasIPAddress);

                 MEMCPY (a_u1RadiusPacket + u2_pktlen + 2, &u4_NetByte, 4);
                 u2_pktlen += LEN_NAS_IP_ADDRESS;
             }
         }                        /* NAS IP Address */

         if (p_RadiusInputAuth->u4_NasPort != NO_ATTRIBUTE)
         {
             a_u1RadiusPacket[u2_pktlen] = A_NAS_PORT;
             a_u1RadiusPacket[u2_pktlen + 1] = LEN_NAS_PORT;
             u4_NetByte = OSIX_HTONL (p_RadiusInputAuth->u4_NasPort);

             MEMCPY (a_u1RadiusPacket + u2_pktlen + 2, &u4_NetByte, 4);
             u2_pktlen += LEN_NAS_PORT;
         }                        /* NAS Port */
         if (p_RadiusInputAuth->u4_NasPortType != NO_ATTRIBUTE)
         {
             a_u1RadiusPacket[u2_pktlen] = A_NAS_PORT_TYPE;
             a_u1RadiusPacket[u2_pktlen + 1] = LEN_NAS_PORT_TYPE;
             u4_NetByte = OSIX_HTONL (p_RadiusInputAuth->u4_NasPortType);

             MEMCPY (a_u1RadiusPacket + u2_pktlen + 2, &u4_NetByte, 4);
             u2_pktlen += LEN_NAS_PORT_TYPE;
         }                        /* NAS Port Type */
         /* Called station id */
         if (p_RadiusInputAuth->bCalledStationFlag == OSIX_TRUE)
         {
             u1_strlen = STRLEN (p_RadiusInputAuth->au1CalledStationId);
             a_u1RadiusPacket[u2_pktlen] = CALLED_STATION_ID_ATTRIBUTE;
             a_u1RadiusPacket[u2_pktlen + 1] =
                 STRLEN (p_RadiusInputAuth->au1CalledStationId) + 2;

             MEMCPY (a_u1RadiusPacket + u2_pktlen + 2,
                     p_RadiusInputAuth->au1CalledStationId, u1_strlen);

             u2_pktlen += u1_strlen + 2;
         }

         /* Calling station ID */
         if (p_RadiusInputAuth->bCallingStationFlag == OSIX_TRUE)
         {
             u1_strlen = STRLEN (p_RadiusInputAuth->au1CallingStationId);
             a_u1RadiusPacket[u2_pktlen] = CALLING_STATION_ID_ATTRIBUTE;
             a_u1RadiusPacket[u2_pktlen + 1] =
                 STRLEN (p_RadiusInputAuth->au1CallingStationId) + 2;

             MEMCPY (a_u1RadiusPacket + u2_pktlen + 2,
                     p_RadiusInputAuth->au1CallingStationId, u1_strlen);

             u2_pktlen += u1_strlen + 2;
         }

         p_Services = p_RadiusInputAuth->p_ServicesAuth;

         for (;;)
         {
             if (p_Services == NULL)
             {
                 break;
             }

             a_u1RadiusPacket[u2_pktlen] = p_Services->u1_ServiceType;

             if (p_Services->u1_IsUtf8EncodingReqd == UTF8_ENABLE)
             {
                 u4_Count = radiusCalcUsrNameLen
                     (p_Services->a_u4Utf8StringVal, LEN_UTF8_USER_NAME);

                 /* Convert mulilingual byte sequences into UTF-8 encoding */
                 if (UnicodeToUtf8
                     (p_Services->a_u4Utf8StringVal, u4_Count,
                      a_u1TempArray, LEN_UTF8_USER_NAME) != UTF8_SUCCESS)
                 {
                     RAD_ERR_PRINT (" Error in UTF-8 encoding.\n");
                     return (NOT_OK);
                 }

                 if (((u1_strlen =
                       (UINT1) STRLEN ((char *) a_u1TempArray)) != 0)
                     && (u1_strlen < (LEN_SERVICE_STRING - 1)))
                 {
                     a_u1RadiusPacket[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);
                     /* Adding below check to avoid Coverity warning */
                     if (u1_strlen > sizeof (a_u1TempArray) - 1)
                     {
                         RAD_ERR_PRINT (" Error in UTF-8 encoding.\n");
                         return (NOT_OK);
                     }
                     MEMCPY (a_u1RadiusPacket + u2_pktlen + 2,
                             a_u1TempArray, u1_strlen);
                     u2_pktlen += u1_strlen + 2;
                 }
             }
             else if (((u1_strlen =
                        (UINT1) STRLEN ((char *) p_Services->
                                        a_u1StringValue)) != 0)
                      && (u1_strlen < (LEN_SERVICE_STRING - 1)))
             {
                 a_u1RadiusPacket[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);
                 MEMCPY (a_u1RadiusPacket + u2_pktlen + 2,
                         p_Services->a_u1StringValue, u1_strlen);
                 u2_pktlen += u1_strlen + 2;
             }
             else
             {
                 a_u1RadiusPacket[u2_pktlen + 1] = LEN_NUM_ATTR;
                 u4_NetByte = OSIX_HTONL (p_Services->u4_NumericalValue);
                 MEMCPY (a_u1RadiusPacket + u2_pktlen + 2, &u4_NetByte, 4);
                 u2_pktlen += LEN_NUM_ATTR;
             }
             p_Services = p_Services->p_NextService;
         }

         if (p_RadiusInputAuth->u1_ProtocolType == PRO_EAP)
         {
             u1_EapType =
                 a_u1RadiusPacket[LEN_PKT_MIN + LEN_RAD_ATTRIBUTE + u1_Userlen +
                                  LEN_RAD_ATTRIBUTE + EAP_HDR_LEN];
             if ((u1_EapType != EAP_TYPE_IDENTITY) &&
                 (p_RadiusInputAuth->u1_StateLen != 0))
             {
                 a_u1RadiusPacket[u2_pktlen] = A_STATE;

                 u1_StateLen = p_RadiusInputAuth->u1_StateLen;

                 a_u1RadiusPacket[u2_pktlen + 1] =
                     (UINT1) (LEN_RAD_ATTRIBUTE + u1_StateLen);

                 MEMCPY (a_u1RadiusPacket + u2_pktlen + LEN_RAD_ATTRIBUTE,
                         p_RadiusInputAuth->a_u1State, u1_StateLen);

                 u2_pktlen += LEN_RAD_ATTRIBUTE + u1_StateLen;
             }
         }

         *u2_PacketLength = u2_pktlen;
         u2_NetByte = OSIX_HTONS (u2_pktlen);

         /* Fill the length of th ePacket */
         MEMCPY (a_u1RadiusPacket + PKT_LEN, &u2_NetByte, 2);

         if (p_RadiusInputAuth->u1_ProtocolType == PRO_EAP)
         {
             /* Calculate Message Authentication Using arHmac_MD5 */
             MEMCPY (a_u1RadiusPacket + u2_TmpOffset, a_u1Signature,
                     LEN_MESSAGE_AUTHENTICATOR);
             i4_Seclen =
                 (UINT1) STRLEN ((char *) p_RadiusServerAuth->a_u1Secret);
             arHmac_MD5 ((unsigned char *) a_u1RadiusPacket, u2_pktlen,
                         p_RadiusServerAuth->a_u1Secret, i4_Seclen,
                         (unsigned char *) &a_u1Signature);
             MEMCPY (a_u1RadiusPacket + u2_TmpOffset, a_u1Signature,
                     LEN_MESSAGE_AUTHENTICATOR);
         }
         return (OK);
     }

/*---------------------------------------------------------------------------
Procedure    : radiusFillAttrEapuser()
Description    : This module fills the EAP authentication attributes
Input(s)    : p_eapuser
Output(s)    : a_u1RadiusPacket,
                  &u2_pktlen
Returns        : None
Called By    : radiusFillAttributesAuth()
Calling        : None
Author        : Jeeva Sethuraman
Date        : 11/02/2002
odifcations    :
---------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
radiusFillAttrEapuser (tUSER_INFO_EAP * p_eapuser,
                       UINT1 a_u1RadiusPacket[], UINT2 *u2_pktlen)
#else
VOID
radiusFillAttrEapuser (p_eapuser, a_u1RadiusPacket, u2_pktlen)
     tUSER_INFO_EAP     *p_eapuser;
     UINT1               a_u1RadiusPacket[];
     UINT2              *u2_pktlen;
#endif
{
    UINT2               u2_Offset = 0;
    UINT1               a_u1Signature[LEN_MESSAGE_AUTHENTICATOR] = { 0 };
    UINT2               u2_EapMsgLen = 0;
    UINT2               u2_EapPktOffset = 0;
    INT1                i1_FirstFlag = FALSE;

    u2_Offset = *u2_pktlen;

    MEMSET (a_u1Signature, 0, LEN_MESSAGE_AUTHENTICATOR);

    a_u1RadiusPacket[u2_Offset] = A_EAP_MESSAGE;    /* Fill the EAP Message */
    u2_Offset += 1;

    MEMCPY (&u2_EapMsgLen, &p_eapuser->a_u1Response[2], 2);
    u2_EapMsgLen = OSIX_NTOHS (u2_EapMsgLen);

    while (u2_EapMsgLen != 0)    /* As long as there is data left */
    {
        if (u2_EapMsgLen > EAP_FRAGMENT_SIZE)
        {
            if (i1_FirstFlag == FALSE)
            {
                a_u1RadiusPacket[u2_Offset] = EAP_FRAGMENT_SIZE;

                u2_Offset = (UINT2) (u2_Offset + 1);
                i1_FirstFlag = TRUE;
            }
            else
            {
                a_u1RadiusPacket[u2_Offset] = A_EAP_MESSAGE;

                a_u1RadiusPacket[u2_Offset + 1] = EAP_FRAGMENT_SIZE;

                u2_Offset = (UINT2) (u2_Offset + 2);
            }

            MEMCPY (a_u1RadiusPacket + u2_Offset,
                    p_eapuser->a_u1Response + u2_EapPktOffset,
                    (EAP_FRAGMENT_SIZE - LEN_EAP_MESSAGE));

            u2_EapMsgLen = (UINT2) (u2_EapMsgLen - (EAP_FRAGMENT_SIZE -
                                                    LEN_EAP_MESSAGE));

            u2_Offset = (UINT2) (u2_Offset + (EAP_FRAGMENT_SIZE -
                                              LEN_EAP_MESSAGE));

            u2_EapPktOffset = (UINT2) (u2_EapPktOffset + (EAP_FRAGMENT_SIZE -
                                                          LEN_EAP_MESSAGE));
        }
        else
        {
            if (i1_FirstFlag == TRUE)
            {
                a_u1RadiusPacket[u2_Offset] = A_EAP_MESSAGE;

                u2_Offset = (UINT2) (u2_Offset + 1);
            }

            a_u1RadiusPacket[u2_Offset] = (UINT1)
                (u2_EapMsgLen + LEN_EAP_MESSAGE);

            u2_Offset = (UINT2) (u2_Offset + 1);

            MEMCPY (a_u1RadiusPacket + u2_Offset,
                    p_eapuser->a_u1Response + u2_EapPktOffset, u2_EapMsgLen);

            u2_Offset = (UINT2) (u2_Offset + u2_EapMsgLen);

            break;
        }
    }

    /* Fill the Message Authenticator */
    a_u1RadiusPacket[u2_Offset] = A_MESSAGE_AUTHENTICATOR;

    a_u1RadiusPacket[u2_Offset + 1] =
        LEN_MESSAGE_AUTHENTICATOR + LEN_RAD_ATTRIBUTE;

    MEMCPY (a_u1RadiusPacket + u2_Offset + LEN_RAD_ATTRIBUTE, a_u1Signature,
            LEN_MESSAGE_AUTHENTICATOR);

    u2_Offset += LEN_RAD_ATTRIBUTE;
    u2_Offset += LEN_MESSAGE_AUTHENTICATOR;

    *u2_pktlen = u2_Offset;

}

/*---------------------------------------------------------------------------
Procedure    : radiusFillAttrMschapuser()
Description    : This module fills the MS CHAP authentication attributes
Input(s)    : p_mschapuser
Output(s)    : a_u1RadiusPacket,
              &u2_pktlen
Returns        : None
Called By    : radiusFillAttributesAuth()
Calling        : None
Author        : G.Vedavinayagam
Date        : 11/11/97
odifcations    :
---------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
radiusFillAttrMschapuser (tUSER_INFO_MS_CHAP * p_mschapuser,
                          UINT1 a_u1RadiusPacket[], UINT2 *u2_pktlen)
#else
VOID
radiusFillAttrMschapuser (p_mschapuser, a_u1RadiusPacket, u2_pktlen)
     tUSER_INFO_MS_CHAP *p_mschapuser;
     UINT1               a_u1RadiusPacket[];
     UINT2              *u2_pktlen;
#endif
{
    UINT1               u1_StrLen = 0, u1_AttrLen = 0;
    UINT2               u2_Offset = 0;
    UINT4               u4_VendorId = 0;

    u2_Offset = *u2_pktlen;
    u1_AttrLen = 6;

    a_u1RadiusPacket[u2_Offset] = A_VENDOR_SPECIFIC;
    u4_VendorId = MS_CHAP_VENDOR_ID;
    u4_VendorId = OSIX_HTONL (u4_VendorId);
    MEMCPY (a_u1RadiusPacket + u2_Offset + 2, &u4_VendorId, 4);
    u2_Offset += 6;

    if ((u1_StrLen = (UINT1) STRLEN ((char *) p_mschapuser->a_u1Challenge)))
    {
        a_u1RadiusPacket[u2_Offset] = SA_MS_CHAP_CHALLENGE;
        u1_StrLen = (UINT1) STRLEN ((char *) p_mschapuser->a_u1Challenge);
        a_u1RadiusPacket[u2_Offset + 1] = (UINT1) (u1_StrLen + 2);
        MEMCPY (a_u1RadiusPacket + u2_Offset + 2, p_mschapuser->a_u1Challenge,
                u1_StrLen);
        u2_Offset += u1_StrLen + 2;
        u1_AttrLen += u1_StrLen + 2;
    }

    a_u1RadiusPacket[u2_Offset] = SA_MS_CHAP_RESPONSE;
    a_u1RadiusPacket[u2_Offset + 1] = LEN_SA_MS_CHAP_RESPONSE;
    a_u1RadiusPacket[u2_Offset + 2] = p_mschapuser->u1_Identifier;
    a_u1RadiusPacket[u2_Offset + 3] = MS_CHAP_RESPONSE_FLAG;
    MEMCPY (a_u1RadiusPacket + u2_Offset + 4, p_mschapuser->a_u1ResponseLM,
            LEN_MS_CHAP_RESPONSE_LM);
    MEMCPY (a_u1RadiusPacket + u2_Offset + 4 + LEN_MS_CHAP_RESPONSE_LM,
            p_mschapuser->a_u1ResponseNT, LEN_MS_CHAP_RESPONSE_NT);

    u2_Offset += LEN_SA_MS_CHAP_RESPONSE;
    u1_AttrLen += LEN_SA_MS_CHAP_RESPONSE;

    a_u1RadiusPacket[(*u2_pktlen) + 1] = u1_AttrLen;

    *u2_pktlen = u2_Offset;

}

/*---------------------------------------------------------------------------
Procedure    : radiusFillAttrMschapcpw1()
Description    : This module fills the attributes for changing the MS CHAP
              password if the password has exired (version 1).
Input(s)    : p_mschapcpw1
Output(s)    : a_u1RadiusPacket,
              &u2_pktlen
Returns        : None
Called By    : radiusFillAttributesAuth()
Calling        : None
Author        : G.Vedavinayagam
Date        : 11/11/97
odifcations    :
---------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
radiusFillAttrMschapcpw1 (tMS_CHAP_CPW1 * p_mschapcpw1,
                          UINT1 a_u1RadiusPacket[], UINT2 *u2_pktlen)
#else
VOID
radiusFillAttrMschapcpw1 (p_mschapcpw1, a_u1RadiusPacket, u2_pktlen)
     tMS_CHAP_CPW1      *p_mschapcpw1;
     UINT1               a_u1RadiusPacket[];
     UINT2              *u2_pktlen;
#endif
{
    UINT1               u1_AttrLen = 0;
    UINT2               u2_Offset = 0, u2_NetByte = 0;
    UINT4               u4_VendorId = 0;

    u2_Offset = *u2_pktlen;
    u1_AttrLen = 6;

    a_u1RadiusPacket[u2_Offset] = A_VENDOR_SPECIFIC;
    u4_VendorId = MS_CHAP_VENDOR_ID;
    u4_VendorId = OSIX_HTONL (u4_VendorId);
    MEMCPY (a_u1RadiusPacket + u2_Offset + 2, &u4_VendorId, 4);
    u2_Offset += 6;

    a_u1RadiusPacket[u2_Offset] = SA_MS_CHAP_CPW1;
    a_u1RadiusPacket[u2_Offset + 1] = LEN_SA_MS_CHAP_CPW1;
    a_u1RadiusPacket[u2_Offset + 2] = MS_CHAP_CPW1_CODE;
    a_u1RadiusPacket[u2_Offset + 3] = p_mschapcpw1->u1_Identifier;

    MEMCPY (a_u1RadiusPacket + u2_Offset + 4, p_mschapcpw1->a_u1OldPwLM,
            LEN_MS_CHAP_OLD_PW_LM);
    MEMCPY (a_u1RadiusPacket + u2_Offset + 4 + LEN_MS_CHAP_OLD_PW_LM,
            p_mschapcpw1->a_u1NewPwLM, LEN_MS_CHAP_NEW_PW_LM);
    MEMCPY (a_u1RadiusPacket + u2_Offset + 4 + LEN_MS_CHAP_OLD_PW_LM +
            LEN_MS_CHAP_NEW_PW_LM, p_mschapcpw1->a_u1OldPwNT,
            LEN_MS_CHAP_OLD_PW_NT);
    MEMCPY (a_u1RadiusPacket + u2_Offset + 4 + LEN_MS_CHAP_OLD_PW_LM +
            LEN_MS_CHAP_NEW_PW_LM + LEN_MS_CHAP_OLD_PW_NT,
            p_mschapcpw1->a_u1NewPwNT, LEN_MS_CHAP_NEW_PW_NT);

    u2_NetByte = p_mschapcpw1->u2_NewLMPwLength;
    u2_NetByte = OSIX_HTONS (u2_NetByte);
    MEMCPY (a_u1RadiusPacket + u2_Offset + 4 + LEN_MS_CHAP_OLD_PW_LM +
            LEN_MS_CHAP_NEW_PW_LM + LEN_MS_CHAP_OLD_PW_NT +
            LEN_MS_CHAP_NEW_PW_NT, &u2_NetByte, 2);

    u2_NetByte = MS_CHAP_CPW1_FLAG;
    u2_NetByte = OSIX_HTONS (u2_NetByte);
    MEMCPY (a_u1RadiusPacket + u2_Offset + 4 + LEN_MS_CHAP_OLD_PW_LM +
            LEN_MS_CHAP_NEW_PW_LM + LEN_MS_CHAP_OLD_PW_NT +
            LEN_MS_CHAP_NEW_PW_NT + 2, &u2_NetByte, 2);

    u2_Offset += LEN_SA_MS_CHAP_CPW1;
    u1_AttrLen += LEN_SA_MS_CHAP_CPW1;

    a_u1RadiusPacket[(*u2_pktlen) + 1] = u1_AttrLen;

    *u2_pktlen = u2_Offset;
}

/*---------------------------------------------------------------------------
Procedure    : radiusFillAttrMschapcpw2()
Description    : This module fills the attributes for changing the MS CHAP
              password if the password has exired (version 2).
Input(s)    : p_mschapcpw2
Output(s)    : a_u1RadiusPacket,
              &u2_pktlen
Returns        : None
Called By    : radiusFillAttributesAuth()
Calling        : None
Author        : G.Vedavinayagam
Date        : 11/11/97
odifcations    :
---------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
radiusFillAttrMschapcpw2 (tMS_CHAP_CPW2 * p_mschapcpw2,
                          UINT1 a_u1RadiusPacket[], UINT2 *u2_pktlen)
#else
VOID
radiusFillAttrMschapcpw2 (p_mschapcpw2, a_u1RadiusPacket, u2_pktlen)
     tMS_CHAP_CPW2      *p_mschapcpw2;
     UINT1               a_u1RadiusPacket[];
     UINT2              *u2_pktlen;
#endif
{
    UINT1               u1_AttrLen = 0, u1_OffsetEnc = 0;
    UINT2               u2_Offset = 0, u2_NetByte = 0, u2_SeqNum = 0;
    UINT4               u4_VendorId = 0;
    INT4                i4_index = 0;

    u2_Offset = *u2_pktlen;
    u1_AttrLen = 6;

    a_u1RadiusPacket[u2_Offset] = A_VENDOR_SPECIFIC;
    u4_VendorId = MS_CHAP_VENDOR_ID;
    u4_VendorId = OSIX_HTONL (u4_VendorId);
    MEMCPY (a_u1RadiusPacket + u2_Offset + 2, &u4_VendorId, 4);
    u2_Offset += 6;

    a_u1RadiusPacket[u2_Offset] = SA_MS_CHAP_CPW2;
    a_u1RadiusPacket[u2_Offset + 1] = LEN_SA_MS_CHAP_CPW2;
    a_u1RadiusPacket[u2_Offset + 2] = MS_CHAP_CPW2_CODE;
    a_u1RadiusPacket[u2_Offset + 3] = p_mschapcpw2->u1_Identifier;

    MEMCPY (a_u1RadiusPacket + u2_Offset + 4, p_mschapcpw2->a_u1OldNTHash,
            LEN_MS_CHAP_OLD_HASH_NT);
    MEMCPY (a_u1RadiusPacket + u2_Offset + 4 + LEN_MS_CHAP_OLD_HASH_NT,
            p_mschapcpw2->a_u1OldLMHash, LEN_MS_CHAP_OLD_HASH_LM);
    MEMCPY (a_u1RadiusPacket + u2_Offset + 4 + LEN_MS_CHAP_OLD_HASH_NT +
            LEN_MS_CHAP_OLD_HASH_LM, p_mschapcpw2->a_u1ResponseLM,
            LEN_MS_CHAP_RESPONSE_LM);
    MEMCPY (a_u1RadiusPacket + u2_Offset + 4 + LEN_MS_CHAP_OLD_HASH_NT +
            LEN_MS_CHAP_OLD_HASH_LM + LEN_MS_CHAP_RESPONSE_LM,
            p_mschapcpw2->a_u1ResponseNT, LEN_MS_CHAP_RESPONSE_NT);

    u2_NetByte = MS_CHAP_CPW2_FLAG;
    u2_NetByte = OSIX_HTONS (u2_NetByte);
    MEMCPY (a_u1RadiusPacket + u2_Offset + 4 + LEN_MS_CHAP_OLD_HASH_NT +
            LEN_MS_CHAP_OLD_HASH_LM + LEN_MS_CHAP_RESPONSE_LM +
            LEN_MS_CHAP_RESPONSE_NT, &u2_NetByte, 2);

    u2_Offset += LEN_SA_MS_CHAP_CPW2;
    u1_AttrLen += LEN_SA_MS_CHAP_CPW2;

    a_u1RadiusPacket[(*u2_pktlen) + 1] = u1_AttrLen;

    *u2_pktlen = u2_Offset;

    u1_OffsetEnc = 0;
    u2_SeqNum = 0;

    for (i4_index = 0; i4_index < NO_OF_FRAGS_MS_CHAP_ENC_PW; i4_index++)
    {

        u2_Offset = *u2_pktlen;
        u1_AttrLen = 6;
        u2_SeqNum++;

        a_u1RadiusPacket[u2_Offset] = A_VENDOR_SPECIFIC;
        u4_VendorId = MS_CHAP_VENDOR_ID;
        u4_VendorId = OSIX_HTONL (u4_VendorId);
        MEMCPY (a_u1RadiusPacket + u2_Offset + 2, &u4_VendorId, 4);
        u2_Offset += 6;
        a_u1RadiusPacket[u2_Offset] = SA_MS_CHAP_LM_ENC_PW;
        a_u1RadiusPacket[u2_Offset + 1] = LEN_SA_MS_CHAP_LM_ENC_PW;
        a_u1RadiusPacket[u2_Offset + 2] = MS_CHAP_LM_ENC_PW_CODE;
        a_u1RadiusPacket[u2_Offset + 3] = p_mschapcpw2->u1_Identifier;

        u2_NetByte = OSIX_HTONS (u2_SeqNum);
        MEMCPY (a_u1RadiusPacket + u2_Offset + 4, &u2_NetByte, 2);

        MEMCPY (a_u1RadiusPacket + u2_Offset + 6,
                p_mschapcpw2->a_u1EncriptedPwLM + u1_OffsetEnc,
                LEN_MS_CHAP_ENC_PW_FRAG);

        u2_Offset += LEN_SA_MS_CHAP_LM_ENC_PW;
        u1_AttrLen += LEN_SA_MS_CHAP_LM_ENC_PW;

        a_u1RadiusPacket[(*u2_pktlen) + 1] = u1_AttrLen;

        *u2_pktlen = u2_Offset;

        u1_OffsetEnc += LEN_MS_CHAP_ENC_PW_FRAG;
    }                            /* for */

    u1_OffsetEnc = 0;
    u2_SeqNum = 0;

    for (i4_index = 0; i4_index < NO_OF_FRAGS_MS_CHAP_ENC_PW; i4_index++)
    {

        u2_Offset = *u2_pktlen;
        u1_AttrLen = 6;
        u2_SeqNum++;

        a_u1RadiusPacket[u2_Offset] = A_VENDOR_SPECIFIC;
        u4_VendorId = MS_CHAP_VENDOR_ID;
        u4_VendorId = OSIX_HTONL (u4_VendorId);
        MEMCPY (a_u1RadiusPacket + u2_Offset + 2, &u4_VendorId, 4);
        u2_Offset += 6;
        a_u1RadiusPacket[u2_Offset] = SA_MS_CHAP_NT_ENC_PW;
        a_u1RadiusPacket[u2_Offset + 1] = LEN_SA_MS_CHAP_NT_ENC_PW;
        a_u1RadiusPacket[u2_Offset + 2] = MS_CHAP_NT_ENC_PW_CODE;
        a_u1RadiusPacket[u2_Offset + 3] = p_mschapcpw2->u1_Identifier;

        u2_NetByte = OSIX_HTONS (u2_SeqNum);
        MEMCPY (a_u1RadiusPacket + u2_Offset + 4, &u2_NetByte, 2);

        MEMCPY (a_u1RadiusPacket + u2_Offset + 6,
                p_mschapcpw2->a_u1EncriptedPwNT + u1_OffsetEnc,
                LEN_MS_CHAP_ENC_PW_FRAG);

        u2_Offset += LEN_SA_MS_CHAP_NT_ENC_PW;
        u1_AttrLen += LEN_SA_MS_CHAP_NT_ENC_PW;

        a_u1RadiusPacket[(*u2_pktlen) + 1] = u1_AttrLen;

        *u2_pktlen = u2_Offset;

        u1_OffsetEnc += LEN_MS_CHAP_ENC_PW_FRAG;
    }                            /* for */
}

/*---------------------------------------------------------------------------
Procedure    : radiusEnterUserQAuth()
Description    : This module creates a user entry and enters it in the user 
              queue.
Input(s)    : p_RadiusInputAuth
              a_u1RadiusPacketAuth
              p_RadiusServerAuth
Output(s)    : None
Returns      : 0, if successfull
              -1, in the case of failure   
Called By    : radiusAuthentication()
Calling        : None
Author        : G.Vedavinayagam
Date        : 11/17/97
odifications    :
-----------------------------------------------------------------------------*/
#ifdef __STDC__
INT1
radiusEnterUserQAuth (tRADIUS_INPUT_AUTH * p_RadiusInputAuth,
                      UINT1 a_u1RadiusPacketAuth[],
                      tRADIUS_REQ_ENTRY ** p_QEntry,
                      tRADIUS_SERVER * p_RadiusServerAuth,
                      VOID (*CallBackfPtr) (VOID *), UINT4 ifIndex)
#else
INT1
radiusEnterUserQAuth (p_RadiusInputAuth, a_u1RadiusPacketAuth,
                      p_QEntry, p_RadiusServerAuth, CallBackfPtr, ifIndex)
     tRADIUS_INPUT_AUTH *p_RadiusInputAuth;
     UINT1               a_u1RadiusPacketAuth[];
     tRADIUS_REQ_ENTRY **p_QEntry;
     tRADIUS_SERVER     *p_RadiusServerAuth;
     VOID                (*CallBackfPtr) (VOID *);
     UINT4               ifIndex;
#endif
{
    UINT4               u4_strlen = 0;
    UINT2               u2_PktLen = 0;
    tRADIUS_REQ_ENTRY  *p_UserQEntry = NULL;

    MEMCPY (&u2_PktLen, a_u1RadiusPacketAuth + PKT_LEN, 2);
    u2_PktLen = OSIX_NTOHS (u2_PktLen);

    p_UserQEntry = radiusCreateUserEntry (gRadMemPoolId.u4UserEntryPoolId);
    if (p_UserQEntry == NULL)
    {
        p_gRadiusErrorLog->u4_ErrorQueueCreationAuth++;
        return NOT_OK;
    }
    else
    {
        p_UserQEntry->CallBackfPtr = CallBackfPtr;
        MEMCPY (p_UserQEntry->au1StaMac, p_RadiusInputAuth->au1StaMac,
                MAC_ADDR_LEN);
        p_UserQEntry->userInfoAuth.u4_NasPort = p_RadiusInputAuth->u4_NasPort;
        p_UserQEntry->ifIndex = ifIndex;
        OsixGetSysTime ((tOsixSysTime *) & (p_UserQEntry->u4SendTime));

        /* if PRO_CHAP */
        if (p_RadiusInputAuth->u1_ProtocolType == PRO_CHAP)
        {
            if (p_RadiusInputAuth->p_UserInfoCHAP->u1_IsUtf8EncodingReqd ==
                UTF8_ENABLE)
            {
                u4_strlen = radiusCalcUsrNameLen
                    (p_RadiusInputAuth->p_UserInfoCHAP->a_u4Utf8UserName,
                     LEN_UTF8_USER_NAME);

                MEMCPY (p_UserQEntry->a_u4Utf8UserName,
                        p_RadiusInputAuth->p_UserInfoCHAP->a_u4Utf8UserName,
                        u4_strlen);
                *(p_UserQEntry->a_u4Utf8UserName + u4_strlen) = (INT4) 0;
                p_UserQEntry->u1_IsUtf8EncodingReqd = UTF8_ENABLE;
            }
            else
            {
                u4_strlen =
                    (UINT1) STRLEN ((char *) p_RadiusInputAuth->p_UserInfoCHAP->
                                    a_u1UserName);
                MEMCPY (p_UserQEntry->a_u1UserName,
                        p_RadiusInputAuth->p_UserInfoCHAP->a_u1UserName,
                        u4_strlen);

                *(p_UserQEntry->a_u1UserName + u4_strlen) = (INT4) 0;
            }
        }

        /* if PRO_EAP */
        else if (p_RadiusInputAuth->u1_ProtocolType == PRO_EAP)
        {
            if (p_RadiusInputAuth->p_UserInfoEAP->u1_IsUtf8EncodingReqd ==
                UTF8_ENABLE)
            {
                u4_strlen = radiusCalcUsrNameLen
                    (p_RadiusInputAuth->p_UserInfoEAP->a_u4Utf8UserName,
                     LEN_UTF8_USER_NAME);

                MEMCPY (p_UserQEntry->a_u4Utf8UserName,
                        p_RadiusInputAuth->p_UserInfoEAP->a_u4Utf8UserName,
                        u4_strlen);
                *(p_UserQEntry->a_u4Utf8UserName + u4_strlen) = (INT4) 0;
                p_UserQEntry->u1_IsUtf8EncodingReqd = UTF8_ENABLE;
            }
            else
            {
                u4_strlen =
                    (UINT1) STRLEN ((char *) p_RadiusInputAuth->p_UserInfoEAP->
                                    a_u1UserName);
                MEMCPY (p_UserQEntry->a_u1UserName,
                        p_RadiusInputAuth->p_UserInfoEAP->a_u1UserName,
                        u4_strlen);
                *(p_UserQEntry->a_u1UserName + u4_strlen) = (INT4) 0;
            }
        }

        /* if PRO_PAP */
        else if (p_RadiusInputAuth->u1_ProtocolType == PRO_PAP)
        {
            if (p_RadiusInputAuth->p_UserInfoPAP->u1_IsUtf8EncodingReqd ==
                UTF8_ENABLE)
            {
                u4_strlen = radiusCalcUsrNameLen
                    (p_RadiusInputAuth->p_UserInfoPAP->a_u4Utf8UserName,
                     LEN_UTF8_USER_NAME);

                MEMCPY (p_UserQEntry->a_u4Utf8UserName,
                        p_RadiusInputAuth->p_UserInfoPAP->a_u4Utf8UserName,
                        u4_strlen);
                *(p_UserQEntry->a_u4Utf8UserName + u4_strlen) = (INT4) 0;
                p_UserQEntry->u1_IsUtf8EncodingReqd = UTF8_ENABLE;
            }
            else
            {
                u4_strlen =
                    (UINT1) STRLEN ((char *) p_RadiusInputAuth->p_UserInfoPAP->
                                    a_u1UserName);
                MEMCPY (p_UserQEntry->a_u1UserName,
                        p_RadiusInputAuth->p_UserInfoPAP->a_u1UserName,
                        u4_strlen);

                *(p_UserQEntry->a_u1UserName + u4_strlen) = (INT4) 0;
            }
        }

        /* if PRO_OTHERS */
        else if (p_RadiusInputAuth->u1_ProtocolType == PRO_OTHERS)
        {
            if (p_RadiusInputAuth->p_UserInfoOTHERS->u1_IsUtf8EncodingReqd ==
                UTF8_ENABLE)
            {
                u4_strlen = radiusCalcUsrNameLen
                    (p_RadiusInputAuth->p_UserInfoOTHERS->a_u4Utf8UserName,
                     LEN_UTF8_USER_NAME);

                MEMCPY (p_UserQEntry->a_u4Utf8UserName,
                        p_RadiusInputAuth->p_UserInfoOTHERS->a_u4Utf8UserName,
                        u4_strlen);
                *(p_UserQEntry->a_u4Utf8UserName + u4_strlen) = (INT4) 0;
                p_UserQEntry->u1_IsUtf8EncodingReqd = UTF8_ENABLE;
            }
            else
            {
                u4_strlen =
                    (UINT1) STRLEN ((char *) p_RadiusInputAuth->
                                    p_UserInfoOTHERS->a_u1UserName);
                MEMCPY (p_UserQEntry->a_u1UserName,
                        p_RadiusInputAuth->p_UserInfoOTHERS->a_u1UserName,
                        u4_strlen);

                *(p_UserQEntry->a_u1UserName + u4_strlen) = (INT4) 0;
            }
        }

        p_UserQEntry->u1_PacketIdentifier = *(a_u1RadiusPacketAuth + PKT_ID);
        MEMCPY (p_UserQEntry->a_u1RequestAuth, a_u1RadiusPacketAuth + PKT_REQA,
                LEN_REQ_AUTH_AUTH);

        p_UserQEntry->p_u1PacketTransmitted =
            radiusCreatePacket (gRadMemPoolId.u4PacketPoolId);

        if (p_UserQEntry->p_u1PacketTransmitted == NULL)
        {
            radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                                    (UINT1 *) p_UserQEntry);
            p_gRadiusErrorLog->u4_ErrorPacketCreationAuth++;
            return NOT_OK;
        }
        else
        {
            MEMCPY (p_UserQEntry->p_u1PacketTransmitted, a_u1RadiusPacketAuth,
                    u2_PktLen);
            *(p_UserQEntry->p_u1PacketTransmitted + u2_PktLen) = '\0';
        }

        /* Copy User auth Info from Input Packet in UserQ entry so that
         * the information can be reused for retransmitting to multiple
         * radius servers */

        if (RadiusCopyUserInfoAuth (p_RadiusInputAuth, &(p_UserQEntry->
                                                         userInfoAuth)) ==
            NOT_OK)
        {
            radiusReleasePacket (gRadMemPoolId.u4PacketPoolId,
                                 p_UserQEntry->p_u1PacketTransmitted);

            radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                                    (UINT1 *) p_UserQEntry);
            p_gRadiusErrorLog->u4_ErrorPacketCreationAuth++;
            return NOT_OK;
        }

        p_UserQEntry->u1_ReTxCount = 0;

        p_UserQEntry->ptRadiusServer = p_RadiusServerAuth;

        *p_QEntry = p_UserQEntry;

        return OK;
    }                            /* if p_UserQEntry == NULL */
}

/*---------------------------------------------------------------------------
Procedure    : RadiusTransmitAuth()
Description  : This function Transmits authentication request to Radius Server,
               creates a user entry and enters it in the user queue and starts 
               timer for response time
Input(s)     : p_RadiusInputAuth  - Pointer to structure passed from
               user module
               a_u1RadiusPacketAuth - Authentication Request Packet.
               p_RadiusServerAuth - Pointer to the stucture which contains
               Radius server parameters.
               CallBackfPtr - Call back function pointer registered by user 
               module  
               ifIndex - Interface index
                       
Output(s)    : None
Returns      : 0, if successfull
              -1, in the case of failure   
Called By    : radiusAuthentication() and radiusTimeOutHandler 
Calling      : radiusHidePassword, radiusFillAttributesAuth, 
               radiusTransmitPacket, radiusenterUserQAuth.
Date         : 21/08/03
-----------------------------------------------------------------------------*/
#ifdef __STDC__
INT1
RadiusTransmitAuth (tRADIUS_INPUT_AUTH * p_RadiusInputAuth,
                    UINT1 *a_u1RadiusPacketAuth,
                    tRADIUS_SERVER * p_RadiusServerAuth,
                    VOID (*CallBackfPtr) (VOID *), UINT4 ifIndex,
                    UINT4 u4NumServers)
#else
INT1
RadiusTransmitAuth (p_RadiusInputAuth, a_u1RadiusPacketAuth
                    p_RadiusServerAuth, CallBackfPtr, ifIndex)
     tRADIUS_INPUT_AUTH *p_RadiusInputAuth;
     UINT1              *a_u1RadiusPacketAuth;
     tRADIUS_SERVER     *p_RadiusServerAuth;
     VOID                (*CallBackfPtr) (VOID *);
     UINT4               ifIndex;
     UINT4               u4NumServers;
#endif

{
    UINT1              *p_u1UserPassword = NULL;
    UINT1               a_u1RadHiddenPassword[LEN_PASSWD + LEN_PWD_PAD] = { 0 };
    UINT1               a_u1RadiusReqAuthAuth[LEN_REQ_AUTH_AUTH] = { 0 };
    UINT2               u2_RadiusPacketLength = LEN_PKT_MIN;
    INT4                i4_HiddenPasswordLength = 0;
    tRADIUS_REQ_ENTRY  *p_UserQEntry = NULL;
    tTmrAppTimer       *pAppTimer = NULL;
    tDNSResolvedIpInfo  ResolvedIpInfo;
    INT4                i4RetVal = 0;
    tIPvXAddr           ZeroIpvxAddr;

    MEMSET (&ZeroIpvxAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&ResolvedIpInfo, 0, sizeof (tDNSResolvedIpInfo));

    switch (p_RadiusInputAuth->u1_ProtocolType)
    {
        case PRO_PAP:
            p_u1UserPassword = p_RadiusInputAuth->p_UserInfoPAP->a_u1UserPasswd;
            break;

        case PRO_MS_CHAP:
            p_u1UserPassword =
                p_RadiusInputAuth->p_UserInfoMschap->a_u1UserPasswd;
            break;

        case MS_CHAP_CPW1:
            p_u1UserPassword =
                p_RadiusInputAuth->p_MsChapChgPw1->a_u1UserPasswd;
            break;

        case MS_CHAP_CPW2:
            p_u1UserPassword =
                p_RadiusInputAuth->p_MsChapChgPw2->a_u1UserPasswd;
            break;

        case PRO_OTHERS:
            p_u1UserPassword =
                p_RadiusInputAuth->p_UserInfoOTHERS->a_u1UserPasswd;
            break;

        default:
            p_u1UserPassword = NULL;
            break;
    }
    /* Copy the Request Authenticator */
    MEMCPY (a_u1RadiusReqAuthAuth, a_u1RadiusPacketAuth + PKT_REQA,
            LEN_REQ_AUTH_AUTH);

    if (p_u1UserPassword != NULL)
    {
        radiusHidePassword (p_u1UserPassword, a_u1RadiusReqAuthAuth,
                            a_u1RadHiddenPassword, &i4_HiddenPasswordLength,
                            p_RadiusServerAuth);
    }

    if (radiusFillAttributesAuth (p_RadiusInputAuth, a_u1RadHiddenPassword,
                                  i4_HiddenPasswordLength,
                                  &u2_RadiusPacketLength, a_u1RadiusPacketAuth,
                                  p_RadiusServerAuth) == NOT_OK)
    {
        p_gRadiusErrorLog->u4_ErrorTransmissionAuth++;

        RAD_ERR_PRINT (" Error in transmitting the packet.\n");
        return (NOT_OK);
    }

    if (p_RadiusServerAuth->b1HostFlag == OSIX_TRUE)
    {
        i4RetVal = FsUtlIPvXResolveHostName (p_RadiusServerAuth->au1HostName,
                                             DNS_NONBLOCK, &ResolvedIpInfo);
        if (i4RetVal == DNS_NOT_RESOLVED)
        {
            RAD_ERR_PRINT ("Cannot resolve the host name ");
            return (NOT_OK);
        }
        else if (i4RetVal == DNS_IN_PROGRESS)
        {
            RAD_ERR_PRINT ("Host name resolution in Progress ");
            return (NOT_OK);
        }
        else if (i4RetVal == DNS_CACHE_FULL)
        {
            RAD_ERR_PRINT ("DNS cache full, cannot resolve at the moment ");
            return (NOT_OK);
        }

        if (MEMCMP (&ResolvedIpInfo.Resolv6Addr, &ZeroIpvxAddr,
                    sizeof (tIPvXAddr)) == 0)
        {
            p_RadiusServerAuth->ServerAddress.u1Afi = IPVX_ADDR_FMLY_IPV4;
            MEMCPY (p_RadiusServerAuth->ServerAddress.au1Addr,
                    &ResolvedIpInfo.Resolv4Addr.au1Addr,
                    ResolvedIpInfo.Resolv4Addr.u1AddrLen);
            p_RadiusServerAuth->ServerAddress.u1AddrLen =
                ResolvedIpInfo.Resolv4Addr.u1AddrLen;
        }
        else
        {
            p_RadiusServerAuth->ServerAddress.u1Afi = IPVX_ADDR_FMLY_IPV6;
            MEMCPY (p_RadiusServerAuth->ServerAddress.au1Addr,
                    &ResolvedIpInfo.Resolv6Addr.au1Addr,
                    ResolvedIpInfo.Resolv6Addr.u1AddrLen);
            p_RadiusServerAuth->ServerAddress.u1AddrLen =
                ResolvedIpInfo.Resolv6Addr.u1AddrLen;

        }
    }

    if (p_RadiusServerAuth->ServerAddress.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        if ((radiusTransmitPacket (a_u1RadiusPacketAuth,
                                   i4_gRadiusAuthSocketId,
                                   i4_gRadiusAccSocketId,
                                   p_RadiusServerAuth)) == NOT_OK)
        {
            p_gRadiusErrorLog->u4_ErrorTransmissionAuth++;

            RAD_ERR_PRINT (" Error in transmitting the packet.\n");
            return (NOT_OK);
        }
    }
#ifdef IP6_WANTED
    else if (p_RadiusServerAuth->ServerAddress.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        if ((radiusTransmitPacket (a_u1RadiusPacketAuth,
                                   i4_gRadiusAuthSocketId6,
                                   i4_gRadiusAccSocketId6,
                                   p_RadiusServerAuth)) == NOT_OK)
        {
            p_gRadiusErrorLog->u4_ErrorTransmissionAuth++;

            RAD_ERR_PRINT (" Error in transmitting the packet.\n");
            return (NOT_OK);
        }

    }
#endif

    RAD_EVENT_PRINT (" Access Request transmitted successfully...\n");
    RAD_EVENT_PRINT (" \tWaiting For The Response From The Server.\n");

    if ((radiusEnterUserQAuth (p_RadiusInputAuth, a_u1RadiusPacketAuth,
                               &p_UserQEntry, p_RadiusServerAuth, CallBackfPtr,
                               ifIndex)) == NOT_OK)
    {
        RAD_ERR_PRINT (" Error in UserQ/Pkt Creation - Auth\n");
        return (NOT_OK);
    }

    /* copy the number of Radius Servers contacted */
    p_UserQEntry->u4NumServersContacted = u4NumServers;

    /* Check if the Timer is already present */
    pAppTimer = a_gUserTable[*(a_u1RadiusPacketAuth + PKT_ID)];

    if (pAppTimer == NULL)
    {
        /* To be freed after timer expiry */

        pAppTimer = (tTmrAppTimer *)
            MemAllocMemBlk (gRadMemPoolId.u4RadTimerPoolId);

        if (pAppTimer == NULL)
        {
            RAD_ERR_PRINT ("Insufficient memory.\n");

            /* release the user entry when the memory allocation for timer 
             * fails */
            radiusReleasePacket (gRadMemPoolId.u4PacketPoolId,
                                 p_UserQEntry->p_u1PacketTransmitted);

            RadiusReleaseUserInfoAuth (&(p_UserQEntry->userInfoAuth));

            radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                                    (UINT1 *) p_UserQEntry);
            return (NOT_OK);
        }
        /* Initialise the allocated memory for apptimer */
        MEMSET ((UINT1 *) pAppTimer, 0, sizeof (tTmrAppTimer));
    }
    else
    {
        if (rad_tmrh_stop_timer (pAppTimer) == NOT_OK)
        {
            RAD_TIMER_PRINT ("\nError in Stopping TIMER");
            p_gRadiusErrorLog->u4_ErrorTimerNotStoped++;
        }
    }

    pAppTimer->u4Data = (FS_ULONG) p_UserQEntry;

    if (rad_tmrh_start_timer (pAppTimer, p_RadiusServerAuth->u4_ResponseTime)
        == RAD_FAILURE)
    {
        radiusReleasePacket (gRadMemPoolId.u4PacketPoolId,
                             p_UserQEntry->p_u1PacketTransmitted);

        RadiusReleaseUserInfoAuth (&(p_UserQEntry->userInfoAuth));
        radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                                (UINT1 *) p_UserQEntry);

        MemReleaseMemBlock (gRadMemPoolId.u4RadTimerPoolId,
                            (UINT1 *) pAppTimer);
        pAppTimer = NULL;

        p_gRadiusErrorLog->u4_ErrorTimerNotStartedAuth++;
        radiusInformFault (FAULT_TIMER_START);
        a_gUserTable[*(a_u1RadiusPacketAuth + PKT_ID)] = NULL;
        return (NOT_OK);
    }

    /* Add to the User table after starting the timer */
    a_gUserTable[*(a_u1RadiusPacketAuth + PKT_ID)] = pAppTimer;

    p_RadiusServerAuth->u4_AccessReqs++;
    p_RadiusServerAuth->u4_PendingRequests++;

    return OK;
}

/*---------------------------------------------------------------------------
Procedure    : RadiusCopyUserInfoAuth()
Description  : This module copies data from the packet (packet from User)
               to structure in p_UserQEntry (p_UserQEntry->userInfoAuth)
Input(s)     : p_RadiusInputAuth
               p_UserInfoAuth
Output(s)    : None
Returns      : 0, if successfull
              -1, in the case of failure   
Called By    : radiusEnterUserqAuth()
Calling      : None
Date         : 21/08/03
-----------------------------------------------------------------------------*/
#ifdef __STDC__
INT1
RadiusCopyUserInfoAuth (tRADIUS_INPUT_AUTH * p_RadiusInputAuth,
                        tRADIUS_INPUT_AUTH * p_UserInfoAuth)
#else
INT1
RadiusCopyUserInfoAuth (p_RadiusInputAuth, userInfoAuth)
     tRADIUS_INPUT_AUTH *p_RadiusInputAuth;
     tRADIUS_INPUT_AUTH *p_UserInfoAuth;

#endif
{
    UINT1               u1_strlen = 0;
    INT1                serviceFlag = 0;
    tSERVICES          *pServicesList = NULL;
    tSERVICES          *pServices = NULL;

    /* Copy CHAP Information */
    if (p_RadiusInputAuth->u1_ProtocolType == PRO_CHAP)
    {
        p_UserInfoAuth->u1_ProtocolType = PRO_CHAP;

        p_UserInfoAuth->p_UserInfoCHAP =
            (tUSER_INFO_CHAP *) MemAllocMemBlk (gRadMemPoolId.u4UserChapPoolId);

        if ((p_UserInfoAuth->p_UserInfoCHAP) == NULL)
        {
            return NOT_OK;
        }

        MEMSET ((UINT1 *) p_UserInfoAuth->p_UserInfoCHAP, 0,
                sizeof (tUSER_INFO_CHAP));

        MEMCPY (p_UserInfoAuth->p_UserInfoCHAP,
                p_RadiusInputAuth->p_UserInfoCHAP, sizeof (tUSER_INFO_CHAP));
    }

    /* Copy EAP Information */
    if (p_RadiusInputAuth->u1_ProtocolType == PRO_EAP)
    {
        p_UserInfoAuth->u1_ProtocolType = PRO_EAP;

        p_UserInfoAuth->p_UserInfoEAP =
            (tUSER_INFO_EAP *) MemAllocMemBlk (gRadMemPoolId.u4UserEapPoolId);

        if ((p_UserInfoAuth->p_UserInfoEAP) == NULL)
        {
            return NOT_OK;
        }

        MEMSET ((UINT1 *) p_UserInfoAuth->p_UserInfoEAP, 0,
                sizeof (tUSER_INFO_EAP));

        MEMCPY (p_UserInfoAuth->p_UserInfoEAP,
                p_RadiusInputAuth->p_UserInfoEAP, sizeof (tUSER_INFO_EAP));

    }

    /* Copy PAP Information */
    if (p_RadiusInputAuth->u1_ProtocolType == PRO_PAP)
    {
        p_UserInfoAuth->u1_ProtocolType = PRO_PAP;

        p_UserInfoAuth->p_UserInfoPAP =
            (tUSER_INFO_PAP *) MemAllocMemBlk (gRadMemPoolId.u4UserPapPoolId);

        if ((p_UserInfoAuth->p_UserInfoPAP) == NULL)
        {
            return NOT_OK;
        }
        MEMSET ((UINT1 *) p_UserInfoAuth->p_UserInfoPAP, 0,
                sizeof (tUSER_INFO_PAP));

        MEMCPY (p_UserInfoAuth->p_UserInfoPAP,
                p_RadiusInputAuth->p_UserInfoPAP, sizeof (tUSER_INFO_PAP));
    }

    /* Copy MS_CHAP Information */
    if (p_RadiusInputAuth->u1_ProtocolType == PRO_MS_CHAP)
    {

        p_UserInfoAuth->u1_ProtocolType = PRO_MS_CHAP;

        p_UserInfoAuth->p_UserInfoMschap =
            (tUSER_INFO_MS_CHAP *) MemAllocMemBlk (gRadMemPoolId.
                                                   u4UserMsChapPoolId);

        if ((p_UserInfoAuth->p_UserInfoMschap) == NULL)
        {
            return NOT_OK;
        }

        MEMSET ((UINT1 *) p_UserInfoAuth->p_UserInfoMschap, 0,
                sizeof (tUSER_INFO_MS_CHAP));

        MEMCPY (p_UserInfoAuth->p_UserInfoMschap,
                p_RadiusInputAuth->p_UserInfoMschap,
                sizeof (tUSER_INFO_MS_CHAP));

    }

    /* Copy MS_CHAP_CPW1 Information */
    if (p_RadiusInputAuth->u1_ProtocolType == MS_CHAP_CPW1)
    {

        p_UserInfoAuth->u1_ProtocolType = MS_CHAP_CPW1;

        p_UserInfoAuth->p_MsChapChgPw1 =
            (tMS_CHAP_CPW1 *) MemAllocMemBlk (gRadMemPoolId.
                                              u4UserChapCpw1PoolId);

        if ((p_UserInfoAuth->p_MsChapChgPw1) == NULL)
        {
            return NOT_OK;
        }

        MEMSET ((UINT1 *) p_UserInfoAuth->p_MsChapChgPw1, 0,
                sizeof (tMS_CHAP_CPW1));

        MEMCPY (p_UserInfoAuth->p_MsChapChgPw1,
                p_RadiusInputAuth->p_MsChapChgPw1, sizeof (tMS_CHAP_CPW1));

    }

    /* Copy MS_CHAP_CPW2 Information */
    if (p_RadiusInputAuth->u1_ProtocolType == MS_CHAP_CPW2)
    {
        p_UserInfoAuth->u1_ProtocolType = MS_CHAP_CPW2;

        p_UserInfoAuth->p_MsChapChgPw2 =
            (tMS_CHAP_CPW2 *) MemAllocMemBlk (gRadMemPoolId.
                                              u4UserChapCpw2PoolId);
        if ((p_UserInfoAuth->p_MsChapChgPw2) == NULL)
        {
            return NOT_OK;
        }

        MEMSET ((UINT1 *) p_UserInfoAuth->p_MsChapChgPw2, 0,
                sizeof (tMS_CHAP_CPW2));

        MEMCPY (p_UserInfoAuth->p_MsChapChgPw2,
                p_RadiusInputAuth->p_MsChapChgPw2, sizeof (tMS_CHAP_CPW2));

    }

    /* Copy other authentication protocol Information */
    if (p_RadiusInputAuth->u1_ProtocolType == PRO_OTHERS)
    {
        p_UserInfoAuth->u1_ProtocolType = PRO_OTHERS;

        p_UserInfoAuth->p_UserInfoOTHERS =
            (tUSER_INFO_OTHERS *) MemAllocMemBlk (gRadMemPoolId.
                                                  u4UserOthersPoolId);

        if ((p_UserInfoAuth->p_UserInfoOTHERS) == NULL)
        {
            return NOT_OK;
        }

        MEMSET ((UINT1 *) p_UserInfoAuth->p_UserInfoOTHERS, 0,
                sizeof (tUSER_INFO_OTHERS));

        MEMCPY (p_UserInfoAuth->p_UserInfoOTHERS,
                p_RadiusInputAuth->p_UserInfoOTHERS,
                sizeof (tUSER_INFO_OTHERS));
    }

    /* Copy the Services list */
    RAD_FOREVER ()
    {
        if (p_RadiusInputAuth->p_ServicesAuth == NULL)
        {
            pServicesList = NULL;
            break;
        }

        pServices =
            (tSERVICES *) MemAllocMemBlk (gRadMemPoolId.u4ServicePoolId);

        if (pServices == NULL)
        {
            /* Release the allocated memory for 
             * specific Authentication protocol */
            RadiusReleaseUserInfoAuth (p_UserInfoAuth);
            return NOT_OK;
        }

        MEMSET ((UINT1 *) pServices, 0, sizeof (tSERVICES));

        MEMCPY (pServices, p_RadiusInputAuth->p_ServicesAuth,
                sizeof (tSERVICES));

        if (serviceFlag == 0)
        {
            p_UserInfoAuth->p_ServicesAuth = pServices;
            pServicesList = pServices;
            serviceFlag = 1;
        }
        else
        {
            pServicesList->p_NextService = pServices;
            pServicesList = pServicesList->p_NextService;
        }

        p_RadiusInputAuth->p_ServicesAuth =
            p_RadiusInputAuth->p_ServicesAuth->p_NextService;

    }                            /* RAD_FOREVER() */

    /* Copy NasId, Nas IP address, Nas Type */
    u1_strlen = (UINT1) STRLEN ((char *) p_RadiusInputAuth->a_u1NasId);
    if (u1_strlen != 0)
    {
        MEMSET (p_UserInfoAuth->a_u1NasId, 0,
                sizeof (p_UserInfoAuth->a_u1NasId));
        MEMCPY (p_UserInfoAuth->a_u1NasId,
                p_RadiusInputAuth->a_u1NasId, u1_strlen);
    }                            /* NAS Identifier */

    if (p_RadiusInputAuth->TaskId != 0)
    {
        p_UserInfoAuth->TaskId = p_RadiusInputAuth->TaskId;
    }

    p_UserInfoAuth->u4_NasIPAddress = p_RadiusInputAuth->u4_NasIPAddress;

    p_UserInfoAuth->u4_NasPort = p_RadiusInputAuth->u4_NasPort;

    p_UserInfoAuth->u4_NasPortType = p_RadiusInputAuth->u4_NasPortType;

    p_UserInfoAuth->u1_StateLen = p_RadiusInputAuth->u1_StateLen;
    MEMCPY (p_UserInfoAuth->a_u1State, p_RadiusInputAuth->a_u1State, p_RadiusInputAuth->u1_StateLen);    /* NAS Port */
    UNUSED_PARAM (pServicesList);
    return OK;
}

/*---------------------------------------------------------------------------
Procedure    : RadiusReleaseUserInfoAuth()
Description  : This module releases a user info auth structure  
Input(s)     : p_UserInfoAuth 
Output(s)    : None
Returns      : 0, if successfull
              -1, in the case of failure   
Called By    : radiusCopyUserInfoAuth()
Calling      : None
Date         : 21/08/03
-----------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
RadiusReleaseUserInfoAuth (tRADIUS_INPUT_AUTH * p_UserInfoAuth)
#else
VOID
RadiusReleaseUserInfoAuth (p_UserInfoAuth)
     tRADIUS_INPUT_AUTH *p_UserInfoAuth;
#endif
{
    tSERVICES          *pServicesAuth = NULL, *pServicesAuthList = NULL;
    UINT4               u4ReturnVal = MEM_SUCCESS;

    if (p_UserInfoAuth->u1_ProtocolType == PRO_CHAP)
    {
        u4ReturnVal =
            MemReleaseMemBlock (gRadMemPoolId.u4UserChapPoolId,
                                (UINT1 *) p_UserInfoAuth->p_UserInfoCHAP);
    }                            /* if PRO_CHAP */

    if (p_UserInfoAuth->u1_ProtocolType == PRO_EAP)
    {
        u4ReturnVal =
            MemReleaseMemBlock (gRadMemPoolId.u4UserEapPoolId,
                                (UINT1 *) p_UserInfoAuth->p_UserInfoEAP);
    }                            /* if PRO_EAP */

    if (p_UserInfoAuth->u1_ProtocolType == PRO_PAP)
    {
        u4ReturnVal =
            MemReleaseMemBlock (gRadMemPoolId.u4UserPapPoolId,
                                (UINT1 *) p_UserInfoAuth->p_UserInfoPAP);
    }                            /* if PRO_PAP */

    if (p_UserInfoAuth->u1_ProtocolType == PRO_MS_CHAP)
    {
        u4ReturnVal =
            MemReleaseMemBlock (gRadMemPoolId.u4UserMsChapPoolId,
                                (UINT1 *) p_UserInfoAuth->p_UserInfoMschap);
    }

    if (p_UserInfoAuth->u1_ProtocolType == MS_CHAP_CPW1)
    {
        u4ReturnVal =
            MemReleaseMemBlock (gRadMemPoolId.u4UserChapCpw1PoolId,
                                (UINT1 *) p_UserInfoAuth->p_MsChapChgPw1);
    }

    if (p_UserInfoAuth->u1_ProtocolType == MS_CHAP_CPW2)
    {
        u4ReturnVal =
            MemReleaseMemBlock (gRadMemPoolId.u4UserChapCpw2PoolId,
                                (UINT1 *) p_UserInfoAuth->p_MsChapChgPw2);
    }

    if (p_UserInfoAuth->u1_ProtocolType == PRO_OTHERS)
    {
        u4ReturnVal =
            MemReleaseMemBlock (gRadMemPoolId.u4UserOthersPoolId,
                                (UINT1 *) p_UserInfoAuth->p_UserInfoOTHERS);
    }

    /* Release the Services list */
    pServicesAuth = p_UserInfoAuth->p_ServicesAuth;

    for (;;)
    {
        if (pServicesAuth == NULL)
            break;
        pServicesAuthList = pServicesAuth->p_NextService;

        u4ReturnVal =
            MemReleaseMemBlock (gRadMemPoolId.u4ServicePoolId,
                                (UINT1 *) pServicesAuth);
        if (u4ReturnVal == MEM_FAILURE)
        {
            break;
        }
        pServicesAuth = pServicesAuthList;

    }
    if (u4ReturnVal == MEM_FAILURE)
    {
        radiusInformFault (FAULT_MEM_RELEASE);
    }
    p_UserInfoAuth->p_ServicesAuth = NULL;

}

/*-----------------------------------------------------------------------------
 * Procedure    : radiusCalcUsrNameLen()
 * Description  : This module calculates the size of the UserName
 *                Input(s): pu4Name = pointer to Username array
 *                          u4UsrNameMaxSize = Max size of Username array
 * Output(s)    : None
 * Returns      : Length of the username
 * Called By    : radiusEnterUserqAuth(),
 *                radiusValidateInputAuth(), 
 *                radiusFillAttributesAuth()               
 * Calling      : None
 * Date         : 03/03/2011
 * ---------------------------------------------------------------------------*/

UINT4
radiusCalcUsrNameLen (UINT4 *pu4Name, UINT4 u4UsrNameMaxSize)
{
    UINT4               u4_Index = 0;
    UINT4               u4_Count = 0;

    if (pu4Name == NULL)
    {
        RAD_ERR_PRINT ("pu4Name is NULL\n");
        return 0;
    }

    for (u4_Index = 0;
         (pu4Name[u4_Index] != L'\0') &&
         (u4_Index < u4UsrNameMaxSize); u4_Index++)
    {
        u4_Count++;
    }
    return u4_Count;
}

/************************** END OF FILE "radauth.c" *************************/
