/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsradelw.c,v 1.1 2015/04/28 12:26:23 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"

# include "radsnmp.h"
# include "radcom.h"
# include "ip.h"
# include "radcli.h"
# include "rmgr.h"

#ifdef WLAN_WANTED
#include "ipcnp.h"
#include "wlannp.h"
#include "wlan.h"

#endif

UINT4               u4ExtTempProp;
UINT4               u4ExtTempAuth;
UINT4               u4ExtTempAcc;

extern UINT4        FsRadExtPrimaryServer[12];
extern UINT4        FsRadExtPrimaryServerAddressType[12];

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRadExtDebugMask
 Input       :  The Indices

                The Object 
                retValFsRadExtDebugMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtDebugMask (INT4 *pi4RetValFsRadExtDebugMask)
{
    *pi4RetValFsRadExtDebugMask = (INT4) gRadDebugMask;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRadExtMaxNoOfUserEntries
 Input       :  The Indices

                The Object 
                retValFsRadExtMaxNoOfUserEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtMaxNoOfUserEntries (INT4 *pi4RetValFsRadExtMaxNoOfUserEntries)
{
    *pi4RetValFsRadExtMaxNoOfUserEntries = gi4RadMaxNoofUserEntries;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRadExtPrimaryServerAddressType
 Input       :  The Indices

                The Object 
                retValFsRadExtPrimaryServerAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtPrimaryServerAddressType (INT4
                                        *pi4RetValFsRadExtPrimaryServerAddressType)
{
    *pi4RetValFsRadExtPrimaryServerAddressType = gRadActiveServer.u1Afi;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRadExtPrimaryServer
 Input       :  The Indices

                The Object 
                retValFsRadExtPrimaryServer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtPrimaryServer (tSNMP_OCTET_STRING_TYPE *
                             pRetValFsRadExtPrimaryServer)
{
    if (gRadActiveServer.u1Afi != (UINT1) RAD_DNS_FAMILY)
    {
        MEMCPY (pRetValFsRadExtPrimaryServer->pu1_OctetList,
                gRadActiveServer.au1Addr, gRadActiveServer.u1AddrLen);

        pRetValFsRadExtPrimaryServer->i4_Length = gRadActiveServer.u1AddrLen;
    }
    else
    {
        STRNCPY (pRetValFsRadExtPrimaryServer->pu1_OctetList,
                 gau1RadActiveServerHostName, 
                 STRLEN (gau1RadActiveServerHostName));
        pRetValFsRadExtPrimaryServer->i4_Length = 
            (INT4)STRLEN (gau1RadActiveServerHostName);

        pRetValFsRadExtPrimaryServer->pu1_OctetList
            [STRLEN (gau1RadActiveServerHostName)] = '\0';
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRadExtDebugMask
 Input       :  The Indices

                The Object 
                setValFsRadExtDebugMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRadExtDebugMask (INT4 i4SetValFsRadExtDebugMask)
{

    gRadDebugMask = (UINT4)i4SetValFsRadExtDebugMask;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRadExtMaxNoOfUserEntries
 Input       :  The Indices

                The Object 
                setValFsRadExtMaxNoOfUserEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRadExtMaxNoOfUserEntries (INT4 i4SetValFsRadExtMaxNoOfUserEntries)
{
    gi4RadMaxNoofUserEntries = i4SetValFsRadExtMaxNoOfUserEntries;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRadExtPrimaryServerAddressType
 Input       :  The Indices

                The Object 
                setValFsRadExtPrimaryServerAddressType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRadExtPrimaryServerAddressType (INT4
                                        i4SetValFsRadExtPrimaryServerAddressType)
{
    gRadActiveServer.u1Afi = (UINT1) i4SetValFsRadExtPrimaryServerAddressType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRadExtPrimaryServer
 Input       :  The Indices

                The Object 
                setValFsRadExtPrimaryServer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRadExtPrimaryServer (tSNMP_OCTET_STRING_TYPE *
                             pSetValFsRadExtPrimaryServer)
{
    INT4                i4Index;
    UINT1               au1PrimaryServer[DNS_MAX_QUERY_LEN];

    MEMSET (au1PrimaryServer, 0, DNS_MAX_QUERY_LEN);

    if (gRadActiveServer.u1Afi != (UINT1) RAD_DNS_FAMILY)
    {
        if ((pSetValFsRadExtPrimaryServer->i4_Length !=
             RADIUS_IPVX_LENGTH_FROM_TYPE (gRadActiveServer.u1Afi)) &&
            (gRadActiveServer.u1Afi != 0))
        {
            /* The addres type configured does not match the
             * Address being configured */
            return SNMP_FAILURE;
        }


        MEMSET (gRadActiveServer.au1Addr, 0, IPVX_MAX_INET_ADDR_LEN);
        MEMSET (au1PrimaryServer, 0, DNS_MAX_QUERY_LEN);

        MEMCPY (gRadActiveServer.au1Addr,
                pSetValFsRadExtPrimaryServer->pu1_OctetList,
                pSetValFsRadExtPrimaryServer->i4_Length);

        gRadActiveServer.u1AddrLen =
            (UINT1) pSetValFsRadExtPrimaryServer->i4_Length;
    }
    else
    {
        MEMSET (gau1RadActiveServerHostName, 0, DNS_MAX_QUERY_LEN);
        STRNCPY (gau1RadActiveServerHostName,
                pSetValFsRadExtPrimaryServer->pu1_OctetList,
                pSetValFsRadExtPrimaryServer->i4_Length);

        gau1RadActiveServerHostName
            [pSetValFsRadExtPrimaryServer->i4_Length] = '\0';

    }

        /* When we are resetting the Primary Server
         * p_gPrimaryRadServer has to be updated */
    if (gRadActiveServer.u1Afi != (UINT1) RAD_DNS_FAMILY)
    {
        if ((pSetValFsRadExtPrimaryServer->i4_Length == 0) ||
            (MEMCMP (pSetValFsRadExtPrimaryServer->pu1_OctetList,
                     au1PrimaryServer,
                     pSetValFsRadExtPrimaryServer->i4_Length) == 0))
        {
            p_gPrimaryRadServer = NULL;
            gRadActiveServer.u1Afi = 0;
            gRadActiveServer.u1AddrLen = 0;
            return SNMP_SUCCESS;
        }
    }
    else
    {
        if ((pSetValFsRadExtPrimaryServer->i4_Length == 0) ||
            (MEMCMP (pSetValFsRadExtPrimaryServer->pu1_OctetList,
                     au1PrimaryServer,
                     pSetValFsRadExtPrimaryServer->i4_Length) == 0))
        {
            p_gPrimaryRadServer = NULL;
            gRadActiveServer.u1Afi = 0;
            MEMSET (gau1RadActiveServerHostName, 0, DNS_MAX_QUERY_LEN);
            return SNMP_SUCCESS;

        }


    }

    if (gRadActiveServer.u1Afi != (UINT1) RAD_DNS_FAMILY)
    {
        for (i4Index = 0; i4Index < MAX_RAD_SERVERS_LIMIT; i4Index++)
        {
            if (a_gServerTable[i4Index])
            {
                if (MEMCMP (pSetValFsRadExtPrimaryServer->pu1_OctetList,
                            a_gServerTable[i4Index]->ServerAddress.au1Addr,
                            pSetValFsRadExtPrimaryServer->i4_Length) == 0)
                {
                    /* Copy the configured primary server parameters into 
                     * the primary server structure */
                    p_gPrimaryRadServer = a_gServerTable[i4Index];
                }
            }
        }
    }
    else
    {
        for (i4Index = 0; i4Index < MAX_RAD_SERVERS_LIMIT; i4Index++)
        {
            if (a_gServerTable[i4Index])
            {
                if (MEMCMP (pSetValFsRadExtPrimaryServer->pu1_OctetList,
                            a_gServerTable[i4Index]->au1HostName,
                            pSetValFsRadExtPrimaryServer->i4_Length) == 0)
                {
                    /* Copy the configured primary server parameters into
                     * the primary server structure */
                    p_gPrimaryRadServer = a_gServerTable[i4Index];
                }
            }
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRadExtDebugMask
 Input       :  The Indices

                The Object 
                testValFsRadExtDebugMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRadExtDebugMask (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsRadExtDebugMask)
{
    if (i4TestValFsRadExtDebugMask < RAD_NO_DEBUG ||
        i4TestValFsRadExtDebugMask > RAD_DEBUG_ALL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRadExtMaxNoOfUserEntries
 Input       :  The Indices

                The Object 
                testValFsRadExtMaxNoOfUserEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRadExtMaxNoOfUserEntries (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsRadExtMaxNoOfUserEntries)
{
    if (i4TestValFsRadExtMaxNoOfUserEntries < 1 ||
        i4TestValFsRadExtMaxNoOfUserEntries > 100)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRadExtPrimaryServerAddressType
 Input       :  The Indices

                The Object 
                testValFsRadExtPrimaryServerAddressType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRadExtPrimaryServerAddressType (UINT4 *pu4ErrorCode,
                                           INT4
                                           i4TestValFsRadExtPrimaryServerAddressType)
{
    if ((i4TestValFsRadExtPrimaryServerAddressType == IPVX_ADDR_FMLY_IPV4) ||
        (i4TestValFsRadExtPrimaryServerAddressType == IPVX_ZERO))
    {
        return SNMP_SUCCESS;
    }

#ifdef IP6_WANTED
    if (i4TestValFsRadExtPrimaryServerAddressType == IPVX_ADDR_FMLY_IPV6)
    {
        return SNMP_SUCCESS;
    }
#endif

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsRadExtPrimaryServer
 Input       :  The Indices

                The Object 
                testValFsRadExtPrimaryServer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRadExtPrimaryServer (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsRadExtPrimaryServer)
{
    tRADIUS_SERVER     *ptRadiusServer;
    UINT4               u4Index = 0;
    UINT1               au1PrimaryServer[IPVX_MAX_INET_ADDR_LEN];

    MEMSET (au1PrimaryServer, 0, IPVX_MAX_INET_ADDR_LEN);

    /* When we are resetting the Primary Server
     * The Ip Address Does not exist in the server table
     * So return Success*/
    if ((pTestValFsRadExtPrimaryServer->i4_Length == 0) ||
        (MEMCMP (pTestValFsRadExtPrimaryServer->pu1_OctetList,
                 au1PrimaryServer,
                 pTestValFsRadExtPrimaryServer->i4_Length) == 0))
    {
        return SNMP_SUCCESS;
    }

    if (gRadActiveServer.u1Afi != (UINT1) RAD_DNS_FAMILY)
    {
        if (pTestValFsRadExtPrimaryServer->i4_Length !=
            RADIUS_IPVX_LENGTH_FROM_TYPE (gRadActiveServer.u1Afi))
        {
            /* The addres type configured does not match the
             * Address being configured */
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (pTestValFsRadExtPrimaryServer->i4_Length > DNS_MAX_QUERY_LEN)
        {
            /* The addres type configured does not match the
             * Address being configured */
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return SNMP_FAILURE;

        }
    }

    if (gRadActiveServer.u1Afi != (UINT1) RAD_DNS_FAMILY)
    {
        for (u4Index = 0; u4Index < MAX_RAD_SERVERS_LIMIT; u4Index++)
        {
            if (a_gServerTable[u4Index])
            {
                ptRadiusServer = a_gServerTable[u4Index];
                if ((ptRadiusServer->ServerAddress.u1Afi == 
                     gRadActiveServer.u1Afi) &&
                    (MEMCMP
                     (pTestValFsRadExtPrimaryServer->pu1_OctetList,
                      ptRadiusServer->ServerAddress.au1Addr,
                      pTestValFsRadExtPrimaryServer->i4_Length) == 0))
                {
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    else
    {
        for (u4Index = 0; u4Index < MAX_RAD_SERVERS_LIMIT; u4Index++)
        {
            if (a_gServerTable[u4Index])
            {
                ptRadiusServer = a_gServerTable[u4Index];
                if ((ptRadiusServer->ServerAddress.u1Afi == 
                     gRadActiveServer.u1Afi)
                    &&
                    (STRNCMP
                     (pTestValFsRadExtPrimaryServer->pu1_OctetList,
                      ptRadiusServer->au1HostName,
                      pTestValFsRadExtPrimaryServer->i4_Length) == 0))
                {
                    return SNMP_SUCCESS;
                }
            }
        }
    }

    /* The Ip Address Does not exist in the server table */

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRadExtDebugMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRadExtDebugMask (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRadExtMaxNoOfUserEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRadExtMaxNoOfUserEntries (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRadExtPrimaryServerAddressType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRadExtPrimaryServerAddressType (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRadExtPrimaryServer
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRadExtPrimaryServer (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRadExtServerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRadExtServerTable
 Input       :  The Indices
                FsRadExtServerIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRadExtServerTable (INT4 i4FsRadExtServerIndex)
{
    if (RadGetServerEntry ((UINT4)i4FsRadExtServerIndex, &u4ExtTempProp) == NULL)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRadExtServerTable
 Input       :  The Indices
                FsRadExtServerIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRadExtServerTable (INT4 *pi4FsRadExtServerIndex)
{
    tRADIUS_SERVER     *ptRadiusServer;
    UINT4               u4Index;
    INT4                i4FirstIndex = MAX_INTEGER;
    INT4                Flag;
    Flag = 0;
    for (u4Index = 0; u4Index < MAX_RAD_SERVERS_LIMIT; u4Index++)
    {
        ptRadiusServer = a_gServerTable[u4Index];
        if (ptRadiusServer)
        {
            Flag = 1;
            if (ptRadiusServer->ifIndex < (UINT4) i4FirstIndex)
            {
                i4FirstIndex = (INT4)ptRadiusServer->ifIndex;
            }
        }
    }

    if (!Flag)
    {
        return SNMP_FAILURE;
    }

    *pi4FsRadExtServerIndex = i4FirstIndex;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRadExtServerTable
 Input       :  The Indices
                FsRadExtServerIndex
                nextFsRadExtServerIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRadExtServerTable (INT4 i4FsRadExtServerIndex,
                                    INT4 *pi4NextFsRadExtServerIndex)
{
    tRADIUS_SERVER     *ptRadiusServer;
    UINT4               u4Index;
    INT4                i4NextIndex = MAX_INTEGER;
    INT4                Flag;
    Flag = 0;

    for (u4Index = 0; u4Index < MAX_RAD_SERVERS_LIMIT; u4Index++)
    {
        ptRadiusServer = a_gServerTable[u4Index];
        if (ptRadiusServer)
        {
            if (ptRadiusServer->ifIndex > (UINT4) i4FsRadExtServerIndex)
            {
                Flag = 1;
                if (ptRadiusServer->ifIndex < (UINT4) i4NextIndex)
                {
                    i4NextIndex = (INT4)ptRadiusServer->ifIndex;
                }
            }
        }
    }

    if (!Flag)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsRadExtServerIndex = i4NextIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRadExtServerAddrType
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                retValFsRadExtServerAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtServerAddrType (INT4 i4FsRadExtServerIndex,
                              INT4 *pi4RetValFsRadExtServerAddrType)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry ((UINT4)i4FsRadExtServerIndex, &u4ExtTempProp)) != NULL)
    {
        if (ptRadiusServer->b1HostFlag == OSIX_FALSE)
        {
            *pi4RetValFsRadExtServerAddrType = ptRadiusServer->ServerAddress.u1Afi;
        }
        else
        {
            *pi4RetValFsRadExtServerAddrType = IPVX_DNS_FAMILY;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsRadExtServerAddress
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                retValFsRadExtServerAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtServerAddress (INT4 i4FsRadExtServerIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsRadExtServerAddress)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry ((UINT4)i4FsRadExtServerIndex, &u4ExtTempProp)) != NULL)
    {
        if (ptRadiusServer->b1HostFlag == OSIX_FALSE)
        {
            MEMCPY (pRetValFsRadExtServerAddress->pu1_OctetList,
                    ptRadiusServer->ServerAddress.au1Addr,
                    ptRadiusServer->ServerAddress.u1AddrLen);

            pRetValFsRadExtServerAddress->i4_Length =
                (INT4) ptRadiusServer->ServerAddress.u1AddrLen;
        }
        else if(ptRadiusServer->b1HostFlag == OSIX_TRUE)
        {
            STRNCPY (pRetValFsRadExtServerAddress->pu1_OctetList,
                     ptRadiusServer->au1HostName,
                     STRLEN(ptRadiusServer->au1HostName));

            pRetValFsRadExtServerAddress->i4_Length = 
                (INT4) STRLEN(ptRadiusServer->au1HostName);

            pRetValFsRadExtServerAddress->pu1_OctetList
                [STRLEN(ptRadiusServer->au1HostName)] = '\0';
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsRadExtServerType
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                retValFsRadExtServerType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtServerType (INT4 i4FsRadExtServerIndex,
                          INT4 *pi4RetValFsRadExtServerType)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry ((UINT4)i4FsRadExtServerIndex, &u4ExtTempProp)) != NULL)
    {
        *pi4RetValFsRadExtServerType = (INT4)ptRadiusServer->u4_ServerType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsRadExtServerSharedSecret
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                retValFsRadExtServerSharedSecret
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtServerSharedSecret (INT4 i4FsRadExtServerIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsRadExtServerSharedSecret)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((MsrGetSaveStatus () == ISS_TRUE)
#ifdef RM_WANTED
        /* If configurations are synced to standby node
           get the key
         */
        || (RmGetStaticConfigStatus () == RM_STATIC_CONFIG_IN_PROGRESS)
#endif
       )
    {
        if ((ptRadiusServer =
             RadGetServerEntry ((UINT4)i4FsRadExtServerIndex, &u4ExtTempProp)) != NULL)
        {

            pRetValFsRadExtServerSharedSecret->i4_Length =
                (INT4)STRLEN (ptRadiusServer->a_u1Secret);
            STRNCPY (pRetValFsRadExtServerSharedSecret->pu1_OctetList,	    
                    ptRadiusServer->a_u1Secret, STRLEN(ptRadiusServer->a_u1Secret));
	    pRetValFsRadExtServerSharedSecret->pu1_OctetList[STRLEN(ptRadiusServer->a_u1Secret)] = '\0';
            return SNMP_SUCCESS;
        }
    }
    else
    {
        pRetValFsRadExtServerSharedSecret->i4_Length = 0;
        return SNMP_SUCCESS;

    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsRadExtServerEnabled
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                retValFsRadExtServerEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtServerEnabled (INT4 i4FsRadExtServerIndex,
                             INT4 *pi4RetValFsRadExtServerEnabled)
{
    tRADIUS_SERVER     *ptRadiusServer;
    if ((ptRadiusServer =
         RadGetServerEntry ((UINT4)i4FsRadExtServerIndex, &u4ExtTempProp)) != NULL)
    {
        *pi4RetValFsRadExtServerEnabled = ptRadiusServer->u1_ServerEnabled;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsRadExtServerResponseTime
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                retValFsRadExtServerResponseTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtServerResponseTime (INT4 i4FsRadExtServerIndex,
                                  INT4 *pi4RetValFsRadExtServerResponseTime)
{
    tRADIUS_SERVER     *ptRadiusServer;
    if ((ptRadiusServer =
         RadGetServerEntry ((UINT4)i4FsRadExtServerIndex, &u4ExtTempProp)) != NULL)
    {
        *pi4RetValFsRadExtServerResponseTime = (INT4)ptRadiusServer->u4_ResponseTime;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsRadExtServerMaximumRetransmission
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                retValFsRadExtServerMaximumRetransmission
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtServerMaximumRetransmission (INT4 i4FsRadExtServerIndex,
                                           INT4
                                           *pi4RetValFsRadExtServerMaximumRetransmission)
{
    tRADIUS_SERVER     *ptRadiusServer;
    if ((ptRadiusServer =
         RadGetServerEntry ((UINT4)i4FsRadExtServerIndex, &u4ExtTempProp)) != NULL)
    {
        *pi4RetValFsRadExtServerMaximumRetransmission =
            (INT4) ptRadiusServer->u1_MaxRetransmissions;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsRadExtServerEntryStatus
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                retValFsRadExtServerEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtServerEntryStatus (INT4 i4FsRadExtServerIndex,
                                 INT4 *pi4RetValFsRadExtServerEntryStatus)
{
    tRADIUS_SERVER     *ptRadiusServer;
    if ((ptRadiusServer =
         RadGetServerEntry ((UINT4)i4FsRadExtServerIndex, &u4ExtTempProp)) != NULL)
    {
        *pi4RetValFsRadExtServerEntryStatus = ptRadiusServer->Status;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRadExtServerAddrType
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                setValFsRadExtServerAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRadExtServerAddrType (INT4 i4FsRadExtServerIndex,
                              INT4 i4SetValFsRadExtServerAddrType)
{
    tRADIUS_SERVER     *ptRadiusServer;
    if ((ptRadiusServer =
         RadGetServerEntry ((UINT4)i4FsRadExtServerIndex, &u4ExtTempProp)) != NULL)
    {
        ptRadiusServer->ServerAddress.u1Afi = 
            (UINT1) i4SetValFsRadExtServerAddrType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsRadExtServerAddress
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                setValFsRadExtServerAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRadExtServerAddress (INT4 i4FsRadExtServerIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsRadExtServerAddress)
{
    tRADIUS_SERVER     *ptRadiusServer;
    if ((ptRadiusServer =
         RadGetServerEntry ((UINT4)i4FsRadExtServerIndex, &u4ExtTempProp)) == NULL)
    {
        return SNMP_FAILURE;
    }

    if(ptRadiusServer->ServerAddress.u1Afi != (UINT1) RAD_DNS_FAMILY)
    {
        if (pSetValFsRadExtServerAddress->i4_Length !=
            RADIUS_IPVX_LENGTH_FROM_TYPE (ptRadiusServer->ServerAddress.u1Afi))
        {
            /* Adddress type configured does not match the type of address being
             * configured*/
            return SNMP_FAILURE;
        }

        MEMCPY (ptRadiusServer->ServerAddress.au1Addr,
                pSetValFsRadExtServerAddress->pu1_OctetList,
                pSetValFsRadExtServerAddress->i4_Length);

        ptRadiusServer->ServerAddress.u1AddrLen =
            (UINT1) pSetValFsRadExtServerAddress->i4_Length;
        ptRadiusServer->b1HostFlag = OSIX_FALSE;
        
    }
    else if(ptRadiusServer->ServerAddress.u1Afi == (UINT1) RAD_DNS_FAMILY)
    {
        STRNCPY (ptRadiusServer->au1HostName, 
                pSetValFsRadExtServerAddress->pu1_OctetList,
                pSetValFsRadExtServerAddress->i4_Length);

        ptRadiusServer->au1HostName
            [pSetValFsRadExtServerAddress->i4_Length] = '\0';
        ptRadiusServer->b1HostFlag = OSIX_TRUE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRadExtServerType
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                setValFsRadExtServerType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRadExtServerType (INT4 i4FsRadExtServerIndex,
                          INT4 i4SetValFsRadExtServerType)
{
    tRADIUS_SERVER     *ptRadiusServer;
    if ((ptRadiusServer =
         RadGetServerEntry ((UINT4)i4FsRadExtServerIndex, &u4ExtTempProp)) != NULL)
    {
        ptRadiusServer->u4_ServerType = (UINT4)i4SetValFsRadExtServerType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsRadExtServerSharedSecret
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                setValFsRadExtServerSharedSecret
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRadExtServerSharedSecret (INT4 i4FsRadExtServerIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValFsRadExtServerSharedSecret)
{
    tRADIUS_SERVER     *ptRadiusServer;
    if ((ptRadiusServer =
         RadGetServerEntry ((UINT4)i4FsRadExtServerIndex, &u4ExtTempProp)) != NULL)
    {
        MEMSET (ptRadiusServer->a_u1Secret, 0, LEN_SECRET);

        MEMCPY (ptRadiusServer->a_u1Secret,
                pSetValFsRadExtServerSharedSecret->pu1_OctetList,
                pSetValFsRadExtServerSharedSecret->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsRadExtServerEnabled
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                setValFsRadExtServerEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRadExtServerEnabled (INT4 i4FsRadExtServerIndex,
                             INT4 i4SetValFsRadExtServerEnabled)
{
    tRADIUS_SERVER     *ptRadiusServer;
    if ((ptRadiusServer =
         RadGetServerEntry ((UINT4)i4FsRadExtServerIndex, &u4ExtTempProp)) != NULL)
    {
        ptRadiusServer->u1_ServerEnabled =
            (UINT1) i4SetValFsRadExtServerEnabled;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsRadExtServerResponseTime
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                setValFsRadExtServerResponseTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRadExtServerResponseTime (INT4 i4FsRadExtServerIndex,
                                  INT4 i4SetValFsRadExtServerResponseTime)
{
    tRADIUS_SERVER     *ptRadiusServer;
    if ((ptRadiusServer =
         RadGetServerEntry ((UINT4)i4FsRadExtServerIndex, &u4ExtTempProp)) != NULL)
    {
        ptRadiusServer->u4_ResponseTime =
            (UINT4) i4SetValFsRadExtServerResponseTime;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsRadExtServerMaximumRetransmission
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                setValFsRadExtServerMaximumRetransmission
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRadExtServerMaximumRetransmission (INT4 i4FsRadExtServerIndex,
                                           INT4
                                           i4SetValFsRadExtServerMaximumRetransmission)
{
    tRADIUS_SERVER     *ptRadiusServer;
    if ((ptRadiusServer =
         RadGetServerEntry ((UINT4)i4FsRadExtServerIndex, &u4ExtTempProp)) != NULL)
    {
        ptRadiusServer->u1_MaxRetransmissions =
            (UINT1) i4SetValFsRadExtServerMaximumRetransmission;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsRadExtServerEntryStatus
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                setValFsRadExtServerEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRadExtServerEntryStatus (INT4 i4FsRadExtServerIndex,
                                 INT4 i4SetValFsRadExtServerEntryStatus)
{
    tRADIUS_SERVER     *ptRadiusServer;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4TableIndex;
    UINT4               u4SeqNum = 0;
#ifdef  WLAN_WANTED
    UINT4               u4InternalIpAddress = 0;
#endif

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    ptRadiusServer = RadGetServerEntry ((UINT4)i4FsRadExtServerIndex, &u4TableIndex);

    switch (i4SetValFsRadExtServerEntryStatus)
    {

        case ACTIVE:
        case NOT_IN_SERVICE:
        case NOT_READY:

            if (ptRadiusServer == NULL)
            {
                return SNMP_FAILURE;
            }

            ptRadiusServer->Status = (UINT1) i4SetValFsRadExtServerEntryStatus;
            ptRadiusServer->u1_ServerEnabled =
                (UINT1) i4SetValFsRadExtServerEntryStatus;
#ifdef WLAN_WANTED
            u4InternalIpAddress = IssGetBaseRemoteIpFromNvRam ();
            /* Wlan utility function to set  the radius server configurations */
            if (RADIUS_PROXY_STATUS () == RADIUS_PROXY_DISABLED)
            {
                FsWlanUtilsRadiusConfg ((UINT4) i4FsRadExtServerIndex,
                                        ptRadiusServer->u4_ServerIPAddress,
                                        ptRadiusServer->a_u1Secret,
                                        LEN_SECRET,
                                        (UINT4)
                                        i4SetValFsRadExtServerEntryStatus);
            }
            else
            {
                FsWlanUtilsRadiusConfg ((UINT4) i4FsRadExtServerIndex,
                                        u4InternalIpAddress,
                                        "FutureWLANRD",
                                        STRLEN ("FutureWLANRD"),
                                        (UINT4)
                                        i4SetValFsRadExtServerEntryStatus);
            }
#endif
            if ((i4SetValFsRadExtServerEntryStatus == ACTIVE) &&
                (gu1RadConfigRestored == RADIUS_TRUE))
            {
                if (RadUtilCallBack
                    (RADIUS_WRITE_SECRET_TO_NVRAM) == RAD_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }

            break;

        case CREATE_AND_GO:

            ptRadiusServer = RadCreateServerEntry ((UINT4)i4FsRadExtServerIndex);
            if (ptRadiusServer == NULL)
                return SNMP_FAILURE;
            else
            {
                RadInitRadServer (ptRadiusServer);
                if (gu1RadConfigRestored == RADIUS_TRUE)
                {
                    if (RadUtilCallBack
                        (RADIUS_WRITE_SECRET_TO_NVRAM) == RAD_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }
                }
                ptRadiusServer->Status = ACTIVE;
                ptRadiusServer->u1_ServerEnabled = RADIUS_ENABLED;
            }
            break;

        case CREATE_AND_WAIT:

            ptRadiusServer = RadCreateServerEntry ((UINT4)i4FsRadExtServerIndex);
            if (ptRadiusServer == NULL)
                return SNMP_FAILURE;
            else
            {
                RadInitRadServer (ptRadiusServer);
                ptRadiusServer->Status = NOT_READY;
            }
            break;

        case DESTROY:

            if (ptRadiusServer == NULL)
            {
                return SNMP_FAILURE;
            }

#ifdef WLAN_WANTED
            /* Wlan utility function to set  the radius server configurations */
            FsWlanUtilsRadiusConfg ((UINT4) i4FsRadExtServerIndex,
                                    ptRadiusServer->u4_ServerIPAddress,
                                    ptRadiusServer->a_u1Secret,
                                    LEN_SECRET,
                                    (UINT4) i4SetValFsRadExtServerEntryStatus);
#endif
            if ((p_gPrimaryRadServer != NULL) &&
                (IPVX_ADDR_COMPARE (gRadActiveServer,
                                    a_gServerTable[u4TableIndex]->ServerAddress)
                 == 0))
            {
                p_gPrimaryRadServer = NULL;
                gRadActiveServer.u1Afi = 0;
                gRadActiveServer.u1AddrLen = 0;

                MEMSET (gRadActiveServer.au1Addr, 0, IPVX_IPV6_ADDR_LEN);

                RM_GET_SEQ_NUM (&u4SeqNum);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsRadExtPrimaryServerAddressType,
                                      u4SeqNum, FALSE, RadiusLock, RadiusUnLock,
                                      0, SNMP_SUCCESS);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%c",
                                  gRadActiveServer.u1Afi));

                RM_GET_SEQ_NUM (&u4SeqNum);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsRadExtPrimaryServer, u4SeqNum,
                                      FALSE, RadiusLock, RadiusUnLock, 0,
                                      SNMP_SUCCESS);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s",
                                  gRadActiveServer.au1Addr));

            }

            radiusReleaseRadiusServer (gRadMemPoolId.u4RadiusServerPoolId,
                                       (UINT1 *) ptRadiusServer);
            a_gServerTable[u4TableIndex] = NULL;
            if (gu1RadConfigRestored == RADIUS_TRUE)
            {
                if (RadUtilCallBack
                    (RADIUS_WRITE_SECRET_TO_NVRAM) == RAD_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRadExtServerAddrType
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                testValFsRadExtServerAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRadExtServerAddrType (UINT4 *pu4ErrorCode,
                                 INT4 i4FsRadExtServerIndex,
                                 INT4 i4TestValFsRadExtServerAddrType)
{
    UNUSED_PARAM (i4FsRadExtServerIndex);

    if (i4TestValFsRadExtServerAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        return SNMP_SUCCESS;
    }

#ifdef IP6_WANTED
    if (i4TestValFsRadExtServerAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        return SNMP_SUCCESS;
    }
#endif

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsRadExtServerAddress
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                testValFsRadExtServerAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRadExtServerAddress (UINT4 *pu4ErrorCode, INT4 i4FsRadExtServerIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsRadExtServerAddress)
{
    tRADIUS_SERVER     *ptRadiusServer = NULL;
    INT4                i4RetServIndex = RAD_ERROR;
    UINT4               u4TestValFsRadExtServerAddress = 0;
    tIPvXAddr           TestServerAddress;
    UINT1               au1HostName[DNS_MAX_QUERY_LEN];
#ifdef IP6_WANTED
    UINT4               u4IfIndex = 0;
    UINT1               au1Array[IPVX_IPV6_ADDR_LEN];
#endif

    MEMSET (au1HostName, 0, DNS_MAX_QUERY_LEN);
    ptRadiusServer = RadGetServerEntry ((UINT4)i4FsRadExtServerIndex, &u4ExtTempProp);

    if (ptRadiusServer == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if(ptRadiusServer->ServerAddress.u1Afi != (UINT1) RAD_DNS_FAMILY)
    {
        if (pTestValFsRadExtServerAddress->i4_Length !=
            RADIUS_IPVX_LENGTH_FROM_TYPE (ptRadiusServer->ServerAddress.u1Afi))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (pTestValFsRadExtServerAddress->i4_Length > (INT4) DNS_MAX_QUERY_LEN)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (ptRadiusServer->ServerAddress.u1Afi != (UINT1) IPVX_DNS_FAMILY)
    {
    IPVX_ADDR_INIT (TestServerAddress, ptRadiusServer->ServerAddress.u1Afi,
                    pTestValFsRadExtServerAddress->pu1_OctetList);
    }
    if (ptRadiusServer->ServerAddress.u1Afi == (UINT1) IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&u4TestValFsRadExtServerAddress,
                TestServerAddress.au1Addr, IPVX_IPV4_ADDR_LEN);
        u4TestValFsRadExtServerAddress =
            OSIX_NTOHL (u4TestValFsRadExtServerAddress);

        if ((u4TestValFsRadExtServerAddress == 0xffffffff))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        /*Checks whether IP belongs to Local Interface .
           If RADIUS PROXY is to be activated in the future this should be taken into 
           account */

        if (NetIpv4IfIsOurAddress (u4TestValFsRadExtServerAddress) ==
            NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_RAD_INVALID_SERVER_IP);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        /* Checks whether IP is multicast */
        if (((IP_IS_ADDR_CLASS_D (u4TestValFsRadExtServerAddress) == TRUE)) ||
            /* Checks whether IP is reserved */
            ((IP_IS_ADDR_CLASS_E (u4TestValFsRadExtServerAddress) == TRUE)))

        {
            CLI_SET_ERR (CLI_RAD_INVALID_SERVER_IP);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
#ifdef IP6_WANTED
    else if (ptRadiusServer->ServerAddress.u1Afi == (UINT1) IPVX_ADDR_FMLY_IPV6)
    {
        MEMSET (au1Array, 0, IPVX_IPV6_ADDR_LEN);
        if (MEMCMP (TestServerAddress.au1Addr, au1Array, IPVX_IPV6_ADDR_LEN) ==
            0)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        MEMSET (au1Array, 0xff, IPVX_IPV6_ADDR_LEN);
        if (MEMCMP (TestServerAddress.au1Addr, au1Array, IPVX_IPV6_ADDR_LEN) ==
            0)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        /*Checks whether IP belongs to Local Interface .
           If RADIUS PROXY is to be activated in the future this should be taken into account */

        if (NetIpv6IsOurAddress
            ((tIp6Addr *) (VOID *) TestServerAddress.au1Addr,
             &u4IfIndex) == NETIPV6_SUCCESS)
        {
            CLI_SET_ERR (CLI_RAD_INVALID_SERVER_IP);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

    }
#endif
    if (ACTIVE == ptRadiusServer->Status)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (ptRadiusServer->ServerAddress.u1Afi != (UINT1) IPVX_DNS_FAMILY)
    {
    i4RetServIndex = (INT4)RadGetServerIndex (&TestServerAddress);
    }
    else
    {
        MEMCPY (au1HostName, pTestValFsRadExtServerAddress->pu1_OctetList, 
                pTestValFsRadExtServerAddress->i4_Length);
        i4RetServIndex = (INT4)RadGetServerHostIndex (au1HostName);
    }
    /* Check if the same ip address is configured for any other entry */
    if ((RAD_ERROR != i4RetServIndex)
        && (i4RetServIndex != i4FsRadExtServerIndex))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsRadExtServerType
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                testValFsRadExtServerType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRadExtServerType (UINT4 *pu4ErrorCode, INT4 i4FsRadExtServerIndex,
                             INT4 i4TestValFsRadExtServerType)
{
    tRADIUS_SERVER     *ptRadiusServer;

    ptRadiusServer = RadGetServerEntry ((UINT4)i4FsRadExtServerIndex, &u4ExtTempProp);

    if (ptRadiusServer == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsRadExtServerType != SERVER_AUTH) &&
        (i4TestValFsRadExtServerType != SERVER_ACC) &&
        (i4TestValFsRadExtServerType != SERVER_BOTH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (ACTIVE == ptRadiusServer->Status)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsRadExtServerSharedSecret
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                testValFsRadExtServerSharedSecret
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRadExtServerSharedSecret (UINT4 *pu4ErrorCode,
                                     INT4 i4FsRadExtServerIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValFsRadExtServerSharedSecret)
{
    tRADIUS_SERVER     *ptRadiusServer;

    ptRadiusServer = RadGetServerEntry ((UINT4)i4FsRadExtServerIndex, &u4ExtTempProp);

    if (ptRadiusServer == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((pTestValFsRadExtServerSharedSecret->i4_Length <= 0) ||
        (pTestValFsRadExtServerSharedSecret->i4_Length > LEN_SECRET - 2))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    /* NVT_CHECK */
    if (SNMPCheckForNVTChars
        (pTestValFsRadExtServerSharedSecret->pu1_OctetList,
         pTestValFsRadExtServerSharedSecret->i4_Length) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif
    if (ACTIVE == ptRadiusServer->Status)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsRadExtServerEnabled
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                testValFsRadExtServerEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRadExtServerEnabled (UINT4 *pu4ErrorCode, INT4 i4FsRadExtServerIndex,
                                INT4 i4TestValFsRadExtServerEnabled)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((i4TestValFsRadExtServerEnabled != RADIUS_ENABLED) &&
        (i4TestValFsRadExtServerEnabled != RADIUS_DISABLED) &&
        (i4TestValFsRadExtServerEnabled != RAD_EXT_SRV_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    ptRadiusServer = RadGetServerEntry ((UINT4)i4FsRadExtServerIndex, &u4ExtTempProp);

    if (ptRadiusServer == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (ACTIVE == ptRadiusServer->Status)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRadExtServerResponseTime
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                testValFsRadExtServerResponseTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRadExtServerResponseTime (UINT4 *pu4ErrorCode,
                                     INT4 i4FsRadExtServerIndex,
                                     INT4 i4TestValFsRadExtServerResponseTime)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((i4TestValFsRadExtServerResponseTime < MIN_RESP_TIME) ||
        (i4TestValFsRadExtServerResponseTime > MAX_RESP_TIME))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    ptRadiusServer = RadGetServerEntry ((UINT4)i4FsRadExtServerIndex, &u4ExtTempProp);

    if (ptRadiusServer == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (ACTIVE == ptRadiusServer->Status)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsRadExtServerMaximumRetransmission
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                testValFsRadExtServerMaximumRetransmission
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRadExtServerMaximumRetransmission (UINT4 *pu4ErrorCode,
                                              INT4 i4FsRadExtServerIndex,
                                              INT4
                                              i4TestValFsRadExtServerMaximumRetransmission)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((i4TestValFsRadExtServerMaximumRetransmission < MIN_RETRANSMIT) ||
        (i4TestValFsRadExtServerMaximumRetransmission > MAX_RETRANSMIT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    ptRadiusServer = RadGetServerEntry (i4FsRadExtServerIndex, &u4ExtTempProp);

    if (ptRadiusServer == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (ACTIVE == ptRadiusServer->Status)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRadExtServerEntryStatus
 Input       :  The Indices
                FsRadExtServerIndex

                The Object 
                testValFsRadExtServerEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRadExtServerEntryStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4FsRadExtServerIndex,
                                    INT4 i4TestValFsRadExtServerEntryStatus)
{
    tRADIUS_SERVER     *ptRadiusServer;
    tRADIUS_REQ_ENTRY  *ptRadiusReqEntry;
    tTmrAppTimer       *pAppTimer;
    UINT4               u4Index;
    UINT1               au1Server[IPVX_MAX_INET_ADDR_LEN];
    MEMSET (au1Server, 0, IPVX_MAX_INET_ADDR_LEN);

    if ((i4FsRadExtServerIndex <= 0)
        || (i4FsRadExtServerIndex > MAX_RAD_SERVERS_LIMIT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    ptRadiusServer = RadGetServerEntry (i4FsRadExtServerIndex, &u4Index);

    switch (i4TestValFsRadExtServerEntryStatus)
    {

        case ACTIVE:
            /* FsRadExtServerEntryStatus can be made active only 
             * when Server * IP address is configured*/
            if (ptRadiusServer == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;
        case NOT_IN_SERVICE:
        case NOT_READY:
            if (ptRadiusServer == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;

        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            if (ptRadiusServer != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case DESTROY:            /* if already in use or not exists return FAIL */
            if (ptRadiusServer == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }

            for (u4Index = 0; u4Index < Q_TABLE_SIZE; u4Index++)
            {
                pAppTimer = a_gUserTable[u4Index];
                if (pAppTimer)
                {
                    ptRadiusReqEntry = (tRADIUS_REQ_ENTRY *) pAppTimer->u4Data;
                    if (ptRadiusReqEntry)
                    {
                        if (ptRadiusReqEntry->ptRadiusServer == ptRadiusServer)
                        {
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            return SNMP_FAILURE;
                        }
                    }
                }                /* if */
            }                    /* for */
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;

    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRadExtServerTable
 Input       :  The Indices
                FsRadExtServerIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRadExtServerTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRadExtAuthClientInvalidServerAddresses
 Input       :  The Indices

                The Object 
                retValFsRadExtAuthClientInvalidServerAddresses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAuthClientInvalidServerAddresses (UINT4
                                                *pu4RetValFsRadExtAuthClientInvalidServerAddresses)
{
    *pu4RetValFsRadExtAuthClientInvalidServerAddresses =
        u4_gRadAuthClientInvalidServers;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRadExtAuthClientIdentifier
 Input       :  The Indices

                The Object 
                retValFsRadExtAuthClientIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAuthClientIdentifier (tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsRadExtAuthClientIdentifier)
{
    if (STRLEN (gau1RadiusClientIdentifier) == 0)
        return SNMP_FAILURE;

    pRetValFsRadExtAuthClientIdentifier->i4_Length =
        STRLEN (gau1RadiusClientIdentifier);
    STRNCPY (pRetValFsRadExtAuthClientIdentifier->pu1_OctetList,
            gau1RadiusClientIdentifier, STRLEN(gau1RadiusClientIdentifier));
    pRetValFsRadExtAuthClientIdentifier->pu1_OctetList[STRLEN(gau1RadiusClientIdentifier)] = '\0';
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRadExtAuthServerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRadExtAuthServerTable
 Input       :  The Indices
                FsRadExtAuthServerIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRadExtAuthServerTable (INT4 i4FsRadExtAuthServerIndex)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAuthServerIndex, &u4ExtTempAuth)) == NULL)
        return SNMP_FAILURE;
    else if (ptRadiusServer->u4_ServerType & SERVER_AUTH)
        return SNMP_SUCCESS;

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRadExtAuthServerTable
 Input       :  The Indices
                FsRadExtAuthServerIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRadExtAuthServerTable (INT4 *pi4FsRadExtAuthServerIndex)
{
    tRADIUS_SERVER     *ptRadiusServer;
    UINT4               u4Index;
    INT4                i4FirstIndex = MAX_INTEGER;
    INT4                Flag;
    Flag = 0;
    for (u4Index = 0; u4Index < MAX_RAD_SERVERS_LIMIT; u4Index++)
    {
        ptRadiusServer = a_gServerTable[u4Index];
        if (ptRadiusServer)
        {
            if (ptRadiusServer->u4_ServerType & SERVER_AUTH)
            {
                Flag = 1;
                if (ptRadiusServer->ifIndex < (UINT4) i4FirstIndex)
                {
                    i4FirstIndex = ptRadiusServer->ifIndex;
                }
            }
        }
    }

    if (!Flag)
    {
        return SNMP_FAILURE;
    }
    *pi4FsRadExtAuthServerIndex = i4FirstIndex;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRadExtAuthServerTable
 Input       :  The Indices
                FsRadExtAuthServerIndex
                nextFsRadExtAuthServerIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRadExtAuthServerTable (INT4 i4FsRadExtAuthServerIndex,
                                        INT4 *pi4NextFsRadExtAuthServerIndex)
{
    tRADIUS_SERVER     *ptRadiusServer;
    UINT4               u4Index;
    INT4                i4NextIndex = MAX_INTEGER;
    INT4                Flag;
    Flag = 0;

    for (u4Index = 0; u4Index < MAX_RAD_SERVERS_LIMIT; u4Index++)
    {
        ptRadiusServer = a_gServerTable[u4Index];
        if (ptRadiusServer)
        {
            if (ptRadiusServer->u4_ServerType & SERVER_AUTH)
            {
                if (ptRadiusServer->ifIndex > (UINT4) i4FsRadExtAuthServerIndex)
                {
                    Flag = 1;
                    if (ptRadiusServer->ifIndex < (UINT4) i4NextIndex)
                    {
                        i4NextIndex = ptRadiusServer->ifIndex;
                    }
                }
            }
        }
    }

    if (!Flag)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsRadExtAuthServerIndex = i4NextIndex;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRadExtAuthServerAddressType
 Input       :  The Indices
                FsRadExtAuthServerIndex

                The Object 
                retValFsRadExtAuthServerAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAuthServerAddressType (INT4 i4FsRadExtAuthServerIndex,
                                     INT4
                                     *pi4RetValFsRadExtAuthServerAddressType)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAuthServerIndex, &u4ExtTempProp)) != NULL)
    {
        *pi4RetValFsRadExtAuthServerAddressType =
            ptRadiusServer->ServerAddress.u1Afi;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsRadExtAuthServerAddress
 Input       :  The Indices
                FsRadExtAuthServerIndex

                The Object 
                retValFsRadExtAuthServerAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAuthServerAddress (INT4 i4FsRadExtAuthServerIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsRadExtAuthServerAddress)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAuthServerIndex, &u4ExtTempProp)) != NULL)
    {
        MEMCPY (pRetValFsRadExtAuthServerAddress->pu1_OctetList,
                ptRadiusServer->ServerAddress.au1Addr,
                ptRadiusServer->ServerAddress.u1AddrLen);

        pRetValFsRadExtAuthServerAddress->i4_Length =
            ptRadiusServer->ServerAddress.u1AddrLen;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsRadExtAuthClientServerPortNumber
 Input       :  The Indices
                FsRadExtAuthServerIndex

                The Object 
                retValFsRadExtAuthClientServerPortNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAuthClientServerPortNumber (INT4 i4FsRadExtAuthServerIndex,
                                          INT4
                                          *pi4RetValFsRadExtAuthClientServerPortNumber)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAuthServerIndex, &u4ExtTempAuth)) != NULL)
    {
        *pi4RetValFsRadExtAuthClientServerPortNumber =
            ptRadiusServer->u4_DestinationPortAuth;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsRadExtAuthClientRoundTripTime
 Input       :  The Indices
                FsRadExtAuthServerIndex

                The Object 
                retValFsRadExtAuthClientRoundTripTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAuthClientRoundTripTime (INT4 i4FsRadExtAuthServerIndex,
                                       UINT4
                                       *pu4RetValFsRadExtAuthClientRoundTripTime)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAuthServerIndex, &u4ExtTempAuth)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_AUTH)
        {
            *pu4RetValFsRadExtAuthClientRoundTripTime =
                ptRadiusServer->u4_RoundTripTime;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsRadExtAuthClientAccessRequests
 Input       :  The Indices
                FsRadExtAuthServerIndex

                The Object 
                retValFsRadExtAuthClientAccessRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAuthClientAccessRequests (INT4 i4FsRadExtAuthServerIndex,
                                        UINT4
                                        *pu4RetValFsRadExtAuthClientAccessRequests)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAuthServerIndex, &u4ExtTempAuth)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_AUTH)
        {
            *pu4RetValFsRadExtAuthClientAccessRequests =
                ptRadiusServer->u4_AccessReqs;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsRadExtAuthClientAccessRetransmissions
 Input       :  The Indices
                FsRadExtAuthServerIndex

                The Object 
                retValFsRadExtAuthClientAccessRetransmissions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAuthClientAccessRetransmissions (INT4 i4FsRadExtAuthServerIndex,
                                               UINT4
                                               *pu4RetValFsRadExtAuthClientAccessRetransmissions)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAuthServerIndex, &u4ExtTempAuth)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_AUTH)
        {
            *pu4RetValFsRadExtAuthClientAccessRetransmissions =
                ptRadiusServer->u4_AccessReTxs;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsRadExtAuthClientAccessAccepts
 Input       :  The Indices
                FsRadExtAuthServerIndex

                The Object 
                retValFsRadExtAuthClientAccessAccepts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAuthClientAccessAccepts (INT4 i4FsRadExtAuthServerIndex,
                                       UINT4
                                       *pu4RetValFsRadExtAuthClientAccessAccepts)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAuthServerIndex, &u4ExtTempAuth)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_AUTH)
        {
            *pu4RetValFsRadExtAuthClientAccessAccepts =
                ptRadiusServer->u4_AccessAccepts;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsRadExtAuthClientAccessRejects
 Input       :  The Indices
                FsRadExtAuthServerIndex

                The Object 
                retValFsRadExtAuthClientAccessRejects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAuthClientAccessRejects (INT4 i4FsRadExtAuthServerIndex,
                                       UINT4
                                       *pu4RetValFsRadExtAuthClientAccessRejects)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAuthServerIndex, &u4ExtTempAuth)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_AUTH)
        {
            *pu4RetValFsRadExtAuthClientAccessRejects =
                ptRadiusServer->u4_AccessRejects;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsRadExtAuthClientAccessChallenges
 Input       :  The Indices
                FsRadExtAuthServerIndex

                The Object 
                retValFsRadExtAuthClientAccessChallenges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAuthClientAccessChallenges (INT4 i4FsRadExtAuthServerIndex,
                                          UINT4
                                          *pu4RetValFsRadExtAuthClientAccessChallenges)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAuthServerIndex, &u4ExtTempAuth)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_AUTH)
        {
            *pu4RetValFsRadExtAuthClientAccessChallenges =
                ptRadiusServer->u4_AccessChallenges;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsRadExtAuthClientMalformedAccessResponses
 Input       :  The Indices
                FsRadExtAuthServerIndex

                The Object 
                retValFsRadExtAuthClientMalformedAccessResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAuthClientMalformedAccessResponses (INT4
                                                  i4FsRadExtAuthServerIndex,
                                                  UINT4
                                                  *pu4RetValFsRadExtAuthClientMalformedAccessResponses)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAuthServerIndex, &u4ExtTempAuth)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_AUTH)
        {
            *pu4RetValFsRadExtAuthClientMalformedAccessResponses =
                ptRadiusServer->u4_MalAccessResps;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsRadExtAuthClientBadAuthenticators
 Input       :  The Indices
                FsRadExtAuthServerIndex

                The Object 
                retValFsRadExtAuthClientBadAuthenticators
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAuthClientBadAuthenticators (INT4 i4FsRadExtAuthServerIndex,
                                           UINT4
                                           *pu4RetValFsRadExtAuthClientBadAuthenticators)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAuthServerIndex, &u4ExtTempAuth)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_AUTH)
        {
            *pu4RetValFsRadExtAuthClientBadAuthenticators =
                ptRadiusServer->u4_AuthBadAuths;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsRadExtAuthClientPendingRequests
 Input       :  The Indices
                FsRadExtAuthServerIndex

                The Object 
                retValFsRadExtAuthClientPendingRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAuthClientPendingRequests (INT4 i4FsRadExtAuthServerIndex,
                                         UINT4
                                         *pu4RetValFsRadExtAuthClientPendingRequests)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAuthServerIndex, &u4ExtTempAuth)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_AUTH)
        {
            *pu4RetValFsRadExtAuthClientPendingRequests =
                ptRadiusServer->u4_PendingRequests;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsRadExtAuthClientTimeouts
 Input       :  The Indices
                FsRadExtAuthServerIndex

                The Object 
                retValFsRadExtAuthClientTimeouts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAuthClientTimeouts (INT4 i4FsRadExtAuthServerIndex,
                                  UINT4 *pu4RetValFsRadExtAuthClientTimeouts)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAuthServerIndex, &u4ExtTempAuth)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_AUTH)
        {
            *pu4RetValFsRadExtAuthClientTimeouts = ptRadiusServer->u4_TimeOuts;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsRadExtAuthClientUnknownTypes
 Input       :  The Indices
                FsRadExtAuthServerIndex

                The Object 
                retValFsRadExtAuthClientUnknownTypes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAuthClientUnknownTypes (INT4 i4FsRadExtAuthServerIndex,
                                      UINT4
                                      *pu4RetValFsRadExtAuthClientUnknownTypes)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAuthServerIndex, &u4ExtTempAuth)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_AUTH)
        {
            *pu4RetValFsRadExtAuthClientUnknownTypes =
                ptRadiusServer->u4_UnknownTypes;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsRadExtAuthClientPacketsDropped
 Input       :  The Indices
                FsRadExtAuthServerIndex

                The Object 
                retValFsRadExtAuthClientPacketsDropped
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAuthClientPacketsDropped (INT4 i4FsRadExtAuthServerIndex,
                                        UINT4
                                        *pu4RetValFsRadExtAuthClientPacketsDropped)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAuthServerIndex, &u4ExtTempAuth)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_AUTH)
        {
            *pu4RetValFsRadExtAuthClientPacketsDropped =
                ptRadiusServer->u4_PktsDropped;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsRadExtAuthClientServerPortNumber
 Input       :  The Indices
                FsRadExtAuthServerIndex

                The Object 
                setValFsRadExtAuthClientServerPortNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRadExtAuthClientServerPortNumber (INT4 i4FsRadExtAuthServerIndex,
                                          INT4
                                          i4SetValFsRadExtAuthClientServerPortNumber)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAuthServerIndex, &u4ExtTempAuth)) != NULL)
    {
        ptRadiusServer->u4_DestinationPortAuth =
            i4SetValFsRadExtAuthClientServerPortNumber;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRadExtAuthClientServerPortNumber
 Input       :  The Indices
                FsRadExtAuthServerIndex

                The Object 
                testValFsRadExtAuthClientServerPortNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRadExtAuthClientServerPortNumber (UINT4 *pu4ErrorCode,
                                             INT4 i4FsRadExtAuthServerIndex,
                                             INT4
                                             i4TestValFsRadExtAuthClientServerPortNumber)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((i4TestValFsRadExtAuthClientServerPortNumber <= RAD_MIN_UDP_PORT) ||
        (i4TestValFsRadExtAuthClientServerPortNumber > RAD_MAX_UDP_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    ptRadiusServer =
        RadGetServerEntry (i4FsRadExtAuthServerIndex, &u4ExtTempProp);

    if (ptRadiusServer == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (ACTIVE == ptRadiusServer->Status)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRadExtAuthServerTable
 Input       :  The Indices
                FsRadExtAuthServerIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRadExtAuthServerTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRadExtAccClientInvalidServerAddresses
 Input       :  The Indices

                The Object 
                retValFsRadExtAccClientInvalidServerAddresses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAccClientInvalidServerAddresses (UINT4
                                               *pu4RetValFsRadExtAccClientInvalidServerAddresses)
{
    *pu4RetValFsRadExtAccClientInvalidServerAddresses =
        u4_gRadAcctClientInvalidServers;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRadExtAccClientIdentifier
 Input       :  The Indices

                The Object 
                retValFsRadExtAccClientIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAccClientIdentifier (tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsRadExtAccClientIdentifier)
{
    if (STRLEN (gau1RadiusClientIdentifier) == 0)
        return SNMP_FAILURE;

    pRetValFsRadExtAccClientIdentifier->i4_Length =
        STRLEN (gau1RadiusClientIdentifier);
    STRNCPY (pRetValFsRadExtAccClientIdentifier->pu1_OctetList,
            gau1RadiusClientIdentifier, STRLEN(gau1RadiusClientIdentifier));
    pRetValFsRadExtAccClientIdentifier->pu1_OctetList[STRLEN(gau1RadiusClientIdentifier)] = '\0';
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : FsRadExtAccServerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRadExtAccServerTable
 Input       :  The Indices
                FsRadExtAccServerIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRadExtAccServerTable (INT4 i4FsRadExtAccServerIndex)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAccServerIndex, &u4ExtTempAcc)) == NULL)
        return SNMP_FAILURE;
    else if (ptRadiusServer->u4_ServerType & SERVER_ACC)
        return SNMP_SUCCESS;

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRadExtAccServerTable
 Input       :  The Indices
                FsRadExtAccServerIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRadExtAccServerTable (INT4 *pi4FsRadExtAccServerIndex)
{
    tRADIUS_SERVER     *ptRadiusServer;
    UINT4               u4Index;
    INT4                i4FirstIndex = MAX_INTEGER;
    INT4                Flag;
    Flag = 0;
    for (u4Index = 0; u4Index < MAX_RAD_SERVERS_LIMIT; u4Index++)
    {
        ptRadiusServer = a_gServerTable[u4Index];
        if (ptRadiusServer)
        {
            if (ptRadiusServer->u4_ServerType & SERVER_ACC)
            {
                Flag = 1;
                if (ptRadiusServer->ifIndex < (UINT4) i4FirstIndex)
                {
                    i4FirstIndex = ptRadiusServer->ifIndex;
                }
            }
        }
    }

    if (!Flag)
    {
        return SNMP_FAILURE;
    }
    *pi4FsRadExtAccServerIndex = i4FirstIndex;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRadExtAccServerTable
 Input       :  The Indices
                FsRadExtAccServerIndex
                nextFsRadExtAccServerIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRadExtAccServerTable (INT4 i4FsRadExtAccServerIndex,
                                       INT4 *pi4NextFsRadExtAccServerIndex)
{
    tRADIUS_SERVER     *ptRadiusServer;
    UINT4               u4Index;
    INT4                i4NextIndex = MAX_INTEGER;
    INT4                Flag;
    Flag = 0;

    for (u4Index = 0; u4Index < MAX_RAD_SERVERS_LIMIT; u4Index++)
    {
        ptRadiusServer = a_gServerTable[u4Index];
        if (ptRadiusServer)
        {
            if (ptRadiusServer->u4_ServerType & SERVER_ACC)
            {
                if (ptRadiusServer->ifIndex > (UINT4) i4FsRadExtAccServerIndex)
                {
                    Flag = 1;
                    if (ptRadiusServer->ifIndex < (UINT4) i4NextIndex)
                    {
                        i4NextIndex = ptRadiusServer->ifIndex;
                    }
                }
            }
        }
    }

    if (!Flag)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsRadExtAccServerIndex = i4NextIndex;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRadExtAccServerAddressType
 Input       :  The Indices
                FsRadExtAccServerIndex

                The Object 
                retValFsRadExtAccServerAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAccServerAddressType (INT4 i4FsRadExtAccServerIndex,
                                    INT4 *pi4RetValFsRadExtAccServerAddressType)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAccServerIndex, &u4ExtTempProp)) != NULL)
    {
        *pi4RetValFsRadExtAccServerAddressType =
            ptRadiusServer->ServerAddress.u1Afi;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsRadExtAccServerAddress
 Input       :  The Indices
                FsRadExtAccServerIndex

                The Object 
                retValFsRadExtAccServerAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAccServerAddress (INT4 i4FsRadExtAccServerIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsRadExtAccServerAddress)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAccServerIndex, &u4ExtTempProp)) != NULL)
    {
        MEMCPY (pRetValFsRadExtAccServerAddress->pu1_OctetList,
                ptRadiusServer->ServerAddress.au1Addr,
                ptRadiusServer->ServerAddress.u1AddrLen);

        pRetValFsRadExtAccServerAddress->i4_Length =
            ptRadiusServer->ServerAddress.u1AddrLen;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsRadExtAccClientServerPortNumber
 Input       :  The Indices
                FsRadExtAccServerIndex

                The Object 
                retValFsRadExtAccClientServerPortNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAccClientServerPortNumber (INT4 i4FsRadExtAccServerIndex,
                                         INT4
                                         *pi4RetValFsRadExtAccClientServerPortNumber)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAccServerIndex, &u4ExtTempAcc)) != NULL)
    {
        *pi4RetValFsRadExtAccClientServerPortNumber =
            ptRadiusServer->u4_DestinationPortAcc;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsRadExtAccClientRoundTripTime
 Input       :  The Indices
                FsRadExtAccServerIndex

                The Object 
                retValFsRadExtAccClientRoundTripTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAccClientRoundTripTime (INT4 i4FsRadExtAccServerIndex,
                                      UINT4
                                      *pu4RetValFsRadExtAccClientRoundTripTime)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAccServerIndex, &u4ExtTempAcc)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_ACC)
        {
            *pu4RetValFsRadExtAccClientRoundTripTime =
                ptRadiusServer->u4_RoundTripTime;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsRadExtAccClientRequests
 Input       :  The Indices
                FsRadExtAccServerIndex

                The Object 
                retValFsRadExtAccClientRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAccClientRequests (INT4 i4FsRadExtAccServerIndex,
                                 UINT4 *pu4RetValFsRadExtAccClientRequests)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAccServerIndex, &u4ExtTempAcc)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_ACC)
        {
            *pu4RetValFsRadExtAccClientRequests = ptRadiusServer->u4_AcctReqs;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsRadExtAccClientRetransmissions
 Input       :  The Indices
                FsRadExtAccServerIndex

                The Object 
                retValFsRadExtAccClientRetransmissions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAccClientRetransmissions (INT4 i4FsRadExtAccServerIndex,
                                        UINT4
                                        *pu4RetValFsRadExtAccClientRetransmissions)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAccServerIndex, &u4ExtTempAcc)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_ACC)
        {
            *pu4RetValFsRadExtAccClientRetransmissions =
                ptRadiusServer->u4_AccessReTxs;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsRadExtAccClientResponses
 Input       :  The Indices
                FsRadExtAccServerIndex

                The Object 
                retValFsRadExtAccClientResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAccClientResponses (INT4 i4FsRadExtAccServerIndex,
                                  UINT4 *pu4RetValFsRadExtAccClientResponses)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAccServerIndex, &u4ExtTempAcc)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_ACC)
        {
            *pu4RetValFsRadExtAccClientResponses = ptRadiusServer->u4_AcctResps;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsRadExtAccClientMalformedResponses
 Input       :  The Indices
                FsRadExtAccServerIndex

                The Object 
                retValFsRadExtAccClientMalformedResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAccClientMalformedResponses (INT4 i4FsRadExtAccServerIndex,
                                           UINT4
                                           *pu4RetValFsRadExtAccClientMalformedResponses)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAccServerIndex, &u4ExtTempAcc)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_ACC)
        {
            *pu4RetValFsRadExtAccClientMalformedResponses =
                ptRadiusServer->u4_MalAcctAccessResps;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsRadExtAccClientBadAuthenticators
 Input       :  The Indices
                FsRadExtAccServerIndex

                The Object 
                retValFsRadExtAccClientBadAuthenticators
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAccClientBadAuthenticators (INT4 i4FsRadExtAccServerIndex,
                                          UINT4
                                          *pu4RetValFsRadExtAccClientBadAuthenticators)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAccServerIndex, &u4ExtTempAcc)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_ACC)
        {
            *pu4RetValFsRadExtAccClientBadAuthenticators =
                ptRadiusServer->u4_AcctBadAuths;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsRadExtAccClientPendingRequests
 Input       :  The Indices
                FsRadExtAccServerIndex

                The Object 
                retValFsRadExtAccClientPendingRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAccClientPendingRequests (INT4 i4FsRadExtAccServerIndex,
                                        UINT4
                                        *pu4RetValFsRadExtAccClientPendingRequests)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAccServerIndex, &u4ExtTempAcc)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_ACC)
        {
            *pu4RetValFsRadExtAccClientPendingRequests =
                ptRadiusServer->u4_PendingRequests;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsRadExtAccClientTimeouts
 Input       :  The Indices
                FsRadExtAccServerIndex

                The Object 
                retValFsRadExtAccClientTimeouts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAccClientTimeouts (INT4 i4FsRadExtAccServerIndex,
                                 UINT4 *pu4RetValFsRadExtAccClientTimeouts)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAccServerIndex, &u4ExtTempAcc)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_ACC)
        {
            *pu4RetValFsRadExtAccClientTimeouts = ptRadiusServer->u4_TimeOuts;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsRadExtAccClientUnknownTypes
 Input       :  The Indices
                FsRadExtAccServerIndex

                The Object 
                retValFsRadExtAccClientUnknownTypes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAccClientUnknownTypes (INT4 i4FsRadExtAccServerIndex,
                                     UINT4
                                     *pu4RetValFsRadExtAccClientUnknownTypes)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry (i4FsRadExtAccServerIndex, &u4ExtTempAcc)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_ACC)
        {
            *pu4RetValFsRadExtAccClientUnknownTypes =
                ptRadiusServer->u4_UnknownTypes;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsRadExtAccClientPacketsDropped
 Input       :  The Indices
                FsRadExtAccServerIndex

                The Object 
                retValFsRadExtAccClientPacketsDropped
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRadExtAccClientPacketsDropped (INT4 i4FsRadExtAccServerIndex,
                                       UINT4
                                       *pu4RetValFsRadExtAccClientPacketsDropped)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry ((UINT4)i4FsRadExtAccServerIndex, &u4ExtTempAcc)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_ACC)
        {
            *pu4RetValFsRadExtAccClientPacketsDropped =
                ptRadiusServer->u4_PktsDropped;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRadExtAccClientServerPortNumber
 Input       :  The Indices
                FsRadExtAccServerIndex

                The Object 
                setValFsRadExtAccClientServerPortNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRadExtAccClientServerPortNumber (INT4 i4FsRadExtAccServerIndex,
                                         INT4
                                         i4SetValFsRadExtAccClientServerPortNumber)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((ptRadiusServer =
         RadGetServerEntry ((UINT4) i4FsRadExtAccServerIndex, &u4ExtTempAcc)) != NULL)
    {
        ptRadiusServer->u4_DestinationPortAcc =
            (UINT4) i4SetValFsRadExtAccClientServerPortNumber;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRadExtAccClientServerPortNumber
 Input       :  The Indices
                FsRadExtAccServerIndex

                The Object 
                testValFsRadExtAccClientServerPortNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRadExtAccClientServerPortNumber (UINT4 *pu4ErrorCode,
                                            INT4 i4FsRadExtAccServerIndex,
                                            INT4
                                            i4TestValFsRadExtAccClientServerPortNumber)
{
    tRADIUS_SERVER     *ptRadiusServer;

    if ((i4TestValFsRadExtAccClientServerPortNumber < RAD_MIN_UDP_PORT) ||
        (i4TestValFsRadExtAccClientServerPortNumber > RAD_MAX_UDP_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    ptRadiusServer =
        RadGetServerEntry ((UINT4)i4FsRadExtAccServerIndex, &u4ExtTempProp);

    if (ptRadiusServer == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (ACTIVE == ptRadiusServer->Status)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRadExtAccServerTable
 Input       :  The Indices
                FsRadExtAccServerIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRadExtAccServerTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
