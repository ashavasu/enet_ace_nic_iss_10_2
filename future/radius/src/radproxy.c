/********************************************************************
 * Copyright (C) Future Software Limited, 2007-2008
 *
 * $Id: radproxy.c,v 1.1 2015/04/28 12:26:24 siva Exp $ 
 *
 * Description: This file contains the radius proxy related definitions.
 *
 *******************************************************************/

#define RADPROXY_C

#include "radcom.h"
#include "radsnmp.h"
#include "ip6util.h"
/*----------------------------------------------------------------------------
Procedure    : RadProxyProcessPktFromClient
Description  : Thie routine receives the packet from radius client in AP
               and sends the packet to reachable radius server.
Input(s)     : pu1RadiusPkt:Radius Packet
             : RadClientSoc:Radius Client socket
Output(s)    : None
Returns      : Nones
Called By    : RadiusMain 
-----------------------------------------------------------------------------*/
VOID
RadProxyProcessPktFromClient (UINT1 *pu1RadiusPkt,
                              struct sockaddr_in RadClientSoc)
{

    UINT2               u2PktLen = 0;
    UINT2               u2TempNewPktLen = 0;
    UINT2               u2NewPktLen = 0;
    tIPvXAddr           IpAddress;
    struct sockaddr_in  RadiusServerAddrAuth;
    INT4                i4NoOfWrittenCh = 0;
    UINT1               au1ProxyState[24];
    UINT1              *pu1Secret;
    UINT4               u4Rc = OSIX_FAILURE;
    UINT4               u4_ServerIPAddress = 0;

    /*Length of received radius packet from client */
    MEMCPY (&u2PktLen, pu1RadiusPkt + PKT_LEN, 2);
    u2PktLen = OSIX_NTOHS (u2PktLen);

    /*Since proxy state attribute remains unchanged during the entire
     * authentication process this is used for deciding the ClientIpAddress
     * and udp destport to which packet received from server is to be sent
     * and request authenticator is used for re-calculation of authenticator
     * and message authenticator. Assumption is that there is no proxy state
     * already existing in the packet.If exists then Proxy should add its own
     * proxy state after the existing one*/

    /* Form and Fill the Proxy State Attribute in the Packet. Format is
     * AttrType(1) + Len of Attr(1) + Attribute value */

    /* Attribute value is 
     * u2DestPort(2) + u4ClientIpAddr(4) + Request Authenticator(16) = 22 Bytes 
     * So total length of Proxy state attribute is always 1 + 1 + 22 = 24bytes*/

    MEMSET (au1ProxyState, 0, LEN_PROXY_STATE_ATTR);
    MEMCPY (au1ProxyState, &RadClientSoc.sin_port, 2);
    MEMCPY (au1ProxyState + BYTE_LEN_2, &RadClientSoc.sin_addr, 4);
    MEMCPY (au1ProxyState + BYTE_LEN_2 + BYTE_LEN_4,
            pu1RadiusPkt + PKT_REQA, LEN_REQ_AUTH_AUTH);

    /*Start adding the proxy state attribute in the packet */
    u2TempNewPktLen =
        (UINT2) (u2PktLen + 2 + sizeof (au1ProxyState) +
                 LEN_MESSAGE_AUTHENTICATOR);
    if (u2TempNewPktLen > 4096)
    {
        p_gRadiusErrorLog->u4_ErrorTransmissionAuth++;
        RAD_ERR_PRINT ("\nSENDTO Radius server failure\n");
        return;
    }

    *(pu1RadiusPkt + u2PktLen) = A_PROXY_STATE;
    *(pu1RadiusPkt + u2PktLen + 1) = sizeof (au1ProxyState);

    MEMCPY ((pu1RadiusPkt + u2PktLen + 2), au1ProxyState,
            sizeof (au1ProxyState));

    /*New Length of radius packet after filling the proxy state */
    u2NewPktLen = u2TempNewPktLen;
    u2NewPktLen = OSIX_HTONS (u2NewPktLen);
    MEMSET (pu1RadiusPkt + PKT_LEN, 0, 2);
    MEMCPY (pu1RadiusPkt + PKT_LEN, &u2NewPktLen, 2);

    /*Get the radius server Ip address and secret shared to which 
     * the access-request is to be sent.*/
    u4Rc = RadProxyGetRadSrvAddrAndSecret (&IpAddress, &pu1Secret);
    if (u4Rc == OSIX_FAILURE)
    {
        p_gRadiusErrorLog->u4_ErrorTransmissionAuth++;
        RAD_ERR_PRINT ("\nSENDTO Radius server failure\n");
        return;
    }

    /*Re-Calulate Authenticator and Message-Authenticator */
    RadClientToProxyCalculateMsgAuth (pu1RadiusPkt, pu1Secret);

    if (IpAddress.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {

        MEMSET ((UINT1 *) &RadiusServerAddrAuth, 0,
                sizeof (RadiusServerAddrAuth));

        RadiusServerAddrAuth.sin_family = AF_INET;
        RadiusServerAddrAuth.sin_port = OSIX_HTONS (RADIUS_SERVER_PORT_AUTH);
        PTR_FETCH4 (u4_ServerIPAddress, &(IpAddress.au1Addr));
        RadiusServerAddrAuth.sin_addr.s_addr = OSIX_HTONL (u4_ServerIPAddress);

        RAD_EVENT_PRINT2 ("\nSending to Server Address: %x, Port No = %d",
                          OSIX_NTOHL (RadiusServerAddrAuth.sin_addr.s_addr),
                          OSIX_NTOHS (RadiusServerAddrAuth.sin_port));

        i4NoOfWrittenCh = sendto ((INT4) gi4ProxyToServerSocketId,
                                  (UINT1 *) pu1RadiusPkt,
                                  (INT4) u2NewPktLen, (INT4) 0,
                                  (struct sockaddr *)
                                  &RadiusServerAddrAuth,
                                  (INT4) sizeof (RadiusServerAddrAuth));
    }

    if (i4NoOfWrittenCh < 0)
    {
        p_gRadiusErrorLog->u4_ErrorTransmissionAuth++;
        RAD_ERR_PRINT ("\nSENDTO Radius server failure\n");
        return;
    }
    return;
}

/*----------------------------------------------------------------------------
Procedure    : RadProxyProcessPktFromServer
Description  : Thie routine receives the packet from radius server
               and sends the packet to appropriate client.
Input(s)     : pu1RadiusPkt
Output(s)    : None
Returns      : Nones
-----------------------------------------------------------------------------*/
VOID
RadProxyProcessPktFromServer (UINT1 *pRadiusPkt)
{
    UINT2               u2DestPort = 0;
    UINT2               u2PktLen = 0;
    UINT2               u2NewPktLen = 0;
    UINT4               u4ClientIpAddress = 0;
    tIPvXAddr           IpAddress;
    struct sockaddr_in  RadiusServerAddrAuth;
    UINT1               au1ReqAuth[LEN_REQ_AUTH_AUTH];
    INT4                i4NoOfWrittenCh;
    UINT1              *pu1NewRadiusPkt;
    UINT1              *pu1Secret;
    UINT4               u4Rc;

    /*Length of received radius packet from server */
    MEMSET (au1ReqAuth, 0, sizeof (au1ReqAuth));
    MEMCPY (&u2PktLen, pRadiusPkt + PKT_LEN, 2);
    u2PktLen = OSIX_NTOHS (u2PktLen);

    /*Since proxy state attribute remains unchanged during the entire
     * authentication process this is used for deciding the ClientIpAddress
     * and udp destport to which packet received from server is to be sent
     * and request authenticator is used for re-calculation of authenticator
     * and message authenticator. Apart from this since the proxy state is 
     * added by Proxy this must be removed and required parameters need to
     * be recalculated before sending to the client.*/

    if (u2PktLen <= LEN_PROXY_STATE_ATTR)
    {
        RAD_ERR_PRINT ("\nMemory allocation failed\n");
        return;
    }
    u2NewPktLen = (UINT2) (u2PktLen - LEN_PROXY_STATE_ATTR);
    pu1NewRadiusPkt = (UINT1 *) MemAllocMemBlk (gRadMemPoolId.u4Radu2PoolId);

    if (pu1NewRadiusPkt == NULL)
    {
        RAD_ERR_PRINT ("\nMemory allocation failed\n");
        return;
    }

    MEMSET (pu1NewRadiusPkt, 0, u2NewPktLen);

    /*Construct a new radius packet with the proxy-state attribute 
     *removed*/
    RadFormNewPacket (pRadiusPkt, pu1NewRadiusPkt, &u2DestPort,
                      &u4ClientIpAddress, &au1ReqAuth[0]);

    u4Rc = RadProxyGetRadSrvAddrAndSecret (&IpAddress, &pu1Secret);

    if (u4Rc == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gRadMemPoolId.u4Radu2PoolId,
                            (UINT1 *) pu1NewRadiusPkt);
        RAD_ERR_PRINT ("\nSending to client failed\n");
        return;
    }
    if (*(pu1NewRadiusPkt + PKT_TYPE) != ACCESS_ACCEPT)
    {
        RadServerToProxyCalculateMsgAuth (pu1NewRadiusPkt, &au1ReqAuth[0]);
    }
    if (*(pu1NewRadiusPkt + PKT_TYPE) == ACCESS_ACCEPT)
    {
        /*Recalculate MS-MPPE-Send/Receive keys and authenticator 
         * and message authenticator*/

        RadProxyFillInterfaceStructure (pu1NewRadiusPkt, &au1ReqAuth[0],
                                        pu1Secret);
        RadServerToProxyCalculateMsgAuth (pu1NewRadiusPkt, &au1ReqAuth[0]);
    }

    MEMSET ((UINT1 *) &RadiusServerAddrAuth, 0, sizeof (RadiusServerAddrAuth));

    RadiusServerAddrAuth.sin_family = AF_INET;
    RadiusServerAddrAuth.sin_port = u2DestPort;
    RadiusServerAddrAuth.sin_addr.s_addr = u4ClientIpAddress;

    i4NoOfWrittenCh = sendto ((INT4) gi4ProxyToClientSocketId,
                              (UINT1 *) pu1NewRadiusPkt,
                              u2NewPktLen, (INT4) 0,
                              (struct sockaddr *)
                              &RadiusServerAddrAuth,
                              (INT4) sizeof (RadiusServerAddrAuth));
    MemReleaseMemBlock (gRadMemPoolId.u4Radu2PoolId, (UINT1 *) pu1NewRadiusPkt);
    if (i4NoOfWrittenCh < 0)
    {
        RAD_ERR_PRINT ("\nSending to client failed\n");
        return;
    }
}

/*----------------------------------------------------------------------------
Procedure    : RadProxyGetRadSrvAddrAndSecret
Description  : Thie routine receives the packet from radius server
               and sends the packet to appropriate client.
Input(s)     : None
Output(s)    : None
Returns      : Nones
-----------------------------------------------------------------------------*/
UINT4
RadProxyGetRadSrvAddrAndSecret (tIPvXAddr * pIpAddress, UINT1 **pu1Secret)
{
    tRADIUS_SERVER     *ptRadiusServer;
    UINT4               u4Index;
    UINT4               u4TempAuth;
    INT4                i4Index = MAX_INTEGER;
    INT4                Flag;
    Flag = 0;
    for (u4Index = 0; u4Index < MAX_RAD_SERVERS_LIMIT; u4Index++)
    {
        ptRadiusServer = a_gServerTable[u4Index];
        if (ptRadiusServer)
        {
            if (ptRadiusServer->u4_ServerType & SERVER_AUTH)
            {
                Flag = 1;
                if (ptRadiusServer->ifIndex < (UINT4) i4Index)
                {
                    i4Index = ptRadiusServer->ifIndex;
                }
            }
        }
    }

    if (Flag == 0)
    {
        return OSIX_FAILURE;
    }
    if ((ptRadiusServer = RadGetServerEntry (i4Index, &u4TempAuth)) != NULL)
    {
        if (ptRadiusServer->u4_ServerType & SERVER_AUTH)
        {
            MEMCPY (pIpAddress, &(ptRadiusServer->ServerAddress),
                    sizeof (tIPvXAddr));
            *pu1Secret = ptRadiusServer->a_u1Secret;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}
