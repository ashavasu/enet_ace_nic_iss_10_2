/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radsnmp.c,v 1.12 2015/07/03 09:45:05 siva Exp $ 
 *
 * Description: This file contains modules to access the servers.
 *
 *******************************************************************/

#define RADSNMPH_C

#include "radcom.h"

extern UINT4        FsRadExtServerSharedSecret[14];

/*************************shijith*********************/

/****************************************************************************
 Function    :     RadCreateServerEntry 
 Input       :  None 
 Output      : get a servr entry with the specified index
****************************************************************************/
tRADIUS_SERVER     *
RadGetServerEntry (UINT4 ifIndex, UINT4 *u4TableIndex)
{
    tRADIUS_SERVER     *ptRadiusServer;
    UINT4               u4Index;
    ptRadiusServer = NULL;

    for (u4Index = 0; u4Index < MAX_RAD_SERVERS_LIMIT; u4Index++)
    {
        ptRadiusServer = a_gServerTable[u4Index];
        if (ptRadiusServer)
            if (ptRadiusServer->ifIndex == ifIndex)
            {
                *u4TableIndex = u4Index;
                break;
            }
    }

    if (u4Index >= MAX_RAD_SERVERS_LIMIT)
    {
        ptRadiusServer = NULL;
    }

    return ptRadiusServer;

}

/****************************************************************************
 Function    :     RadCreateServerEntry 
 Input       :  None 
 Output      : create a servr entry    
****************************************************************************/
tRADIUS_SERVER     *
RadCreateServerEntry (UINT4 ifIndex)
{
    tRADIUS_SERVER     *ptRadiusServer;
    UINT4               u4Index;
    ptRadiusServer = NULL;

    for (u4Index = 0; u4Index < MAX_RAD_SERVERS_LIMIT; u4Index++)
    {

        if (a_gServerTable[u4Index] == NULL)
        {
            a_gServerTable[u4Index] =
                radiusCreateRadiusServer (gRadMemPoolId.u4RadiusServerPoolId);
            ptRadiusServer = a_gServerTable[u4Index];
            ptRadiusServer->ifIndex = ifIndex;
            break;
        }

    }

    if (u4Index >= MAX_RAD_SERVERS_LIMIT)
    {
        ptRadiusServer = NULL;
    }

    return ptRadiusServer;

}

/****************************************************************************
 Function    :  RadGetAuthServer 
 Input       :  None 
 Output      :     Returns an authentication server, if present or NULL. 
****************************************************************************/
tRADIUS_SERVER     *
RadGetAuthServer (VOID)
{
    tRADIUS_SERVER     *ptRadiusServer;
    UINT4               u4Index;
    ptRadiusServer = NULL;

    for (u4Index = 0; u4Index < MAX_RAD_SERVERS_LIMIT; u4Index++)
    {
        ptRadiusServer = a_gServerTable[u4Index];
        if (ptRadiusServer)
            if (ptRadiusServer->u4_ServerType & SERVER_AUTH)
            {
                return ptRadiusServer;
            }
    }
    return NULL;
}

/****************************************************************************
 Function    :  RadGetAcctServer 
 Input       :  None 
 Output      :     Returns an accounting server, if present or NULL. 
****************************************************************************/
tRADIUS_SERVER     *
RadGetAcctServer (VOID)
{
    tRADIUS_SERVER     *ptRadiusServer;
    UINT4               u4Index;
    ptRadiusServer = NULL;

    for (u4Index = 0; u4Index < MAX_RAD_SERVERS_LIMIT; u4Index++)
    {
        ptRadiusServer = a_gServerTable[u4Index];
        if (ptRadiusServer)
            if (ptRadiusServer->u4_ServerType & SERVER_ACC)
            {
                return ptRadiusServer;
            }
    }
    return NULL;
}

/****************************************************************************
 Function    :     RadGetNextServerEntry 
 Input       :  None 
 Output      : get a servr entry with the specified index
****************************************************************************/
tRADIUS_SERVER     *
RadGetNextServerEntry (UINT4 ifIndex)
{
    tRADIUS_SERVER     *ptRadiusServer;
    UINT4               u4Index;
    UINT4               u4NoOfServers = 0;

    ptRadiusServer = NULL;

    for (u4Index = 0; u4Index < MAX_RAD_SERVERS_LIMIT; u4Index++)
    {
        ptRadiusServer = a_gServerTable[u4Index];
        if (ptRadiusServer)
            if (ptRadiusServer->ifIndex == ifIndex)
            {
                break;
            }
    }
    /* Get the next Radius Server Ptr, if the search hits the bottom 
     * of the table wrap around */
    for (u4Index = u4Index + 1; u4NoOfServers < (MAX_RAD_SERVERS_LIMIT - 1);
         u4Index++, u4NoOfServers++)
    {
        if (u4Index >= MAX_RAD_SERVERS_LIMIT)
            u4Index = 0;

        ptRadiusServer = a_gServerTable[u4Index];

        if (ptRadiusServer)
        {
            if (ptRadiusServer->u4_ServerType & SERVER_AUTH)
                break;
        }
    }
    return ptRadiusServer;
}

/****************************************************************************
 Function    :  RadGetNumAuthServers 
 Input       :  None 
 Output      :  Returns Number of Auth servers present in the external 
                servers table. 
****************************************************************************/
UINT4
RadGetNumAuthServers (VOID)
{
    tRADIUS_SERVER     *ptRadiusServer;
    UINT4               u4Index;
    UINT4               u4NoOfServers = 0;
    ptRadiusServer = NULL;

    for (u4Index = 0; u4Index < MAX_RAD_SERVERS_LIMIT; u4Index++)
    {
        ptRadiusServer = a_gServerTable[u4Index];
        if ((ptRadiusServer) && (ptRadiusServer->u4_ServerType & SERVER_AUTH))
            u4NoOfServers++;
    }
    return u4NoOfServers;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RadGetServerIndex                                  */
/*                                                                           */
/*     DESCRIPTION      : This function gets index of the radius server in   */
/*                        server  table                                      */
/*                                                                           */
/*     INPUT            : u4RadServerAddr - Radius server addr               */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : u4Index                                            */
/*                                                                           */
/*****************************************************************************/
UINT4
RadGetServerIndex (tIPvXAddr * pRadServerAddr)
{
    UINT4               u4Index;
    tRADIUS_SERVER     *ptRadiusServer;

    for (u4Index = 0; u4Index < MAX_RAD_SERVERS_LIMIT; u4Index++)
    {
        ptRadiusServer = a_gServerTable[u4Index];
        if ((ptRadiusServer != 0)  && (ptRadiusServer->b1HostFlag == OSIX_FALSE))
        {
            if (MEMCMP (ptRadiusServer->ServerAddress.au1Addr,
                        pRadServerAddr->au1Addr,
                        pRadServerAddr->u1AddrLen) == 0)
            {
                return ptRadiusServer->ifIndex;
            }
        }
    }
    return RAD_ERROR;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RadGetServerHostIndex                              */
/*                                                                           */
/*     DESCRIPTION      : This function gets index of the radius server in   */
/*                        server  table incase of Host Name is given         */
/*                                                                           */
/*     INPUT            : au1HostName - Radius server Host Name              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : u4Index                                            */
/*                                                                           */
/*****************************************************************************/
UINT4
RadGetServerHostIndex (UINT1 *au1HostName)
{
    UINT4               u4Index = 0;
    tRADIUS_SERVER     *ptRadiusServer;

    for (u4Index = 0; u4Index < MAX_RAD_SERVERS_LIMIT; u4Index++)
    {
        ptRadiusServer = a_gServerTable[u4Index];
        if ((ptRadiusServer != 0)  && (ptRadiusServer->b1HostFlag == OSIX_TRUE))
        {
            if (STRNCMP (ptRadiusServer->au1HostName,
                        au1HostName,
                        DNS_MAX_QUERY_LEN) == 0)
            {
                return ptRadiusServer->ifIndex;
            }
        }
    }
    return RAD_ERROR;
}


/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RadDeleteSharedSecretKey                           */
/*                                                                           */
/*     DESCRIPTION      : This function deletes the shared secret key        */
/*                        which is specific to the client and server.        */
/*                                                                           */
/*     INPUT            : NONE.                                              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
RadDeleteSharedSecretKey (VOID)
{
    tRADIUS_SERVER     *ptRadiusServer = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tSNMP_OCTET_STRING_TYPE SharedSecret;
    UINT4               u4Index = 0;

    RADIUS_LOCK ();
    for (u4Index = 0; u4Index < MAX_RAD_SERVERS_LIMIT; u4Index++)
    {
        MEMSET (&SharedSecret, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

        ptRadiusServer = a_gServerTable[u4Index];
        if (ptRadiusServer != NULL)
        {
            MEMSET (ptRadiusServer->a_u1Secret, 0, LEN_SECRET);

            SharedSecret.pu1_OctetList = ptRadiusServer->a_u1Secret;
            SharedSecret.i4_Length = 0;

            MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

            SnmpNotifyInfo.pu4ObjectId = FsRadExtServerSharedSecret;
            SnmpNotifyInfo.u4OidLen =
                sizeof (FsRadExtServerSharedSecret) / sizeof (UINT4);
            SnmpNotifyInfo.u1RowStatus = FALSE;
            SnmpNotifyInfo.pLockPointer = RadiusLock;
            SnmpNotifyInfo.pUnLockPointer = RadiusUnLock;
            SnmpNotifyInfo.u4Indices = 1;
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

            SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s ",
                                  ptRadiusServer->ifIndex, &SharedSecret));

        }
    }

    /* If file exists, Keys written in the file are removed */

    if (RAD_FAILURE == RadUtilCallBack (RAD_ACCESS_SECURE_MEMORY,
                                        ISS_SECURE_MEM_DELETE,
                                        RADIUS_KEY_SAVE_FILE))
    {
        RADIUS_UNLOCK ();
        return SNMP_FAILURE;
    }
    RADIUS_UNLOCK ();
    return SNMP_SUCCESS;
}
