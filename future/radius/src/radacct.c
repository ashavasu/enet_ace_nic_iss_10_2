/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radacct.c,v 1.13 2015/04/28 12:26:24 siva Exp $ 
 *
 * Description: This file contains all the modules for
 *                framing and transmitting a packet for
 *                accounting request. 
 *******************************************************************/

#include   "radcom.h"

/*----------------------------------------------------------------------------
Procedure    : radiusAccounting()
Description    : This module Frames the packet for an accounting request and
              it transmits the framed packet. It validates the input for
              the presence of all required inputs for framing a genuine
              accounting request packet. If input validation fails the 
              packet will not be framed. If the inpus are valid different
              modules are called for framing a packet and transmitting it.
Input(s)    : p_RadiusInputAcc
                  p_RadiusServerAcc
Output(s)    : None
Returns        : The Identifier of the packet transmitted if the packet is
              framed and transmitted successfully.
              -1, if failure in framing or transmitting the packet oberved.
Called By    : The User
Calling        : radiusValidateInputAcc()
              radiusFramePrePacketAcc()
              radiusFillAttributesAcc()
              radiusMakeFinalPacketAcc()
              radiusTransmitPacket()
              radiusEnterUserQAcc()
Author        : G.Vedavinayagam
Date        : 11/12/97
odifications    :
-----------------------------------------------------------------------------*/
#ifdef __STDC__
INT4
radiusAccounting (tRADIUS_INPUT_ACC * p_RadiusInputAcc,
                  tRADIUS_SERVER * p_RadiusServerAcc,
                  VOID (*CallBackfPtr) (VOID *), UINT4 ifIndex)
#else
INT4
radiusAccounting (p_RadiusInputAcc, p_RadiusServerAcc)
     tRADIUS_INPUT_ACC  *p_RadiusInputAcc;
     tRADIUS_SERVER     *p_RadiusServerAcc;
     VOID                (*CallBackfPtr) (VOID *);
     UINT4               ifIndex;
#endif
{
    tRADIUS_REQ_ENTRY  *p_UserQEntry = NULL;
    UINT1              *pa_u1RadiusPacketAcc = NULL;
    UINT2               u2_RadiusPacketLength;
    INT4                i4_ID = 0;
    UINT4               u4_Index = 0;
    UINT4               u4_NasIPAddr = 0;
    tTmrAppTimer       *pAppTimer = NULL;
    UINT4               u4DestIp = 0;

    pa_u1RadiusPacketAcc = MemAllocMemBlk (gRadMemPoolId.u4RadPacketPoolId);

    if (NULL == pa_u1RadiusPacketAcc)
    {
        RAD_ERR_PRINT ("Error in allocating memory for pa_u1RadiusPacketAcc\n");
        return NOT_OK;
    }
    MEMSET (pa_u1RadiusPacketAcc, 0, sizeof (tu1RadiusPacketAuth));


    if (p_RadiusServerAcc != NULL)
    {
        for (u4_Index = 0; u4_Index < MAX_RAD_SERVERS_LIMIT; u4_Index++)
        {
            if (a_gServerTable[u4_Index])
            {
                if (IPVX_ADDR_COMPARE (p_RadiusServerAcc->ServerAddress,
                                       a_gServerTable[u4_Index]->
                                       ServerAddress) == 0)
                    break;
            }
        }

        /* Given Radius Server is not in the Radius Client's External
 *          * Server table */
        if (u4_Index == MAX_RAD_SERVERS_LIMIT)
        {
            RAD_ERR_PRINT
                ("\n Server is not in the External Server Table...\n");

            RADIUS_UNLOCK ();
            MemReleaseMemBlock (gRadMemPoolId.u4RadPacketPoolId,
                                (UINT1 *) pa_u1RadiusPacketAcc);
            return NOT_OK;
        }
    }
    else
    {
        /* Check if the primary server is in the external server table */
        if (p_gPrimaryRadServer != NULL)
        {   
            for (u4_Index = 0; u4_Index < MAX_RAD_SERVERS_LIMIT; u4_Index++)
            {  
                if (a_gServerTable[u4_Index])
                {  
                    if (IPVX_ADDR_COMPARE (p_gPrimaryRadServer->ServerAddress,
                                           a_gServerTable[u4_Index]->
                                           ServerAddress) == 0)
                        break;
                }
            }
         
        }
        if (p_gPrimaryRadServer == NULL || (u4_Index == MAX_RAD_SERVERS_LIMIT))
        { 
            p_gPrimaryRadServer = RadGetAcctServer ();
            if (p_gPrimaryRadServer == NULL)
            { 
                RAD_ERR_PRINT ("\n No Server Available for the Request...\n");
                MemReleaseMemBlock (gRadMemPoolId.u4RadPacketPoolId,
                                    (UINT1 *) pa_u1RadiusPacketAcc);
                return NOT_OK;
            }
        }

        if (p_gPrimaryRadServer->ServerAddress.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            u4DestIp = OSIX_NTOHL (u4DestIp);
            ip_src_addr_to_use_for_dest (u4DestIp, &u4_NasIPAddr);
            p_RadiusInputAcc->u4_NasIPAddress = u4_NasIPAddr;
        }

         p_RadiusServerAcc = p_gPrimaryRadServer;
    }
    RAD_EVENT_PRINT (" Sending Accounting Request to the Server.\n");

    if (radiusValidateInputAcc (p_RadiusInputAcc) == NOT_OK)
    {
        p_gRadiusErrorLog->u4_ErrorInputInsufficientAcc++;
        RAD_ERR_PRINT ("Input not sufficient for accounting request.\n");
        MemReleaseMemBlock (gRadMemPoolId.u4RadPacketPoolId,
                            (UINT1 *) pa_u1RadiusPacketAcc);
        return (NOT_OK);
    }

    radiusFramePrePacketAcc (pa_u1RadiusPacketAcc, &u2_RadiusPacketLength);

    radiusFillAttributesAcc (p_RadiusInputAcc, pa_u1RadiusPacketAcc,
                             &u2_RadiusPacketLength);

    radiusMakeFinalPacketAcc (pa_u1RadiusPacketAcc, &u2_RadiusPacketLength,
                              p_RadiusServerAcc);

    if ((i4_ID = radiusTransmitPacket (pa_u1RadiusPacketAcc,
                                       i4_gRadiusAuthSocketId,
                                       i4_gRadiusAccSocketId,
                                       p_RadiusServerAcc)) == NOT_OK)
    {
        p_gRadiusErrorLog->u4_ErrorTransmissionAcc++;
        RAD_ERR_PRINT (" Error in transmitting the packet.\n");
        MemReleaseMemBlock (gRadMemPoolId.u4RadPacketPoolId,
                            (UINT1 *) pa_u1RadiusPacketAcc);
        return (NOT_OK);
    }
    RAD_EVENT_PRINT (" Accounting Request transmitted successfully...\n");
    RAD_EVENT_PRINT (" \tWaiting For The Response From The Server.\n");

    if ((radiusEnterUserQAcc (p_RadiusInputAcc, pa_u1RadiusPacketAcc,
                              &p_UserQEntry, p_RadiusServerAcc,
                              CallBackfPtr, ifIndex)) == NOT_OK)
    {
        RAD_ERR_PRINT (" Error in UserQ/Pkt Creation - Accounting.\n");
        MemReleaseMemBlock (gRadMemPoolId.u4RadPacketPoolId,
                            (UINT1 *) pa_u1RadiusPacketAcc);
        return (NOT_OK);
    }

    /* To be freed after timer expiry */

    pAppTimer =
        (tTmrAppTimer *) MemAllocMemBlk (gRadMemPoolId.u4RadTimerPoolId);
    if (pAppTimer == NULL)
    {
        RAD_ERR_PRINT (" Error in malloc for timer node.\n");
        radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                                (UINT1 *) p_UserQEntry);
        MemReleaseMemBlock (gRadMemPoolId.u4RadPacketPoolId,
                            (UINT1 *) pa_u1RadiusPacketAcc);
        return (NOT_OK);
    }

    /* Initialise */
    MEMSET ((UINT1 *) pAppTimer, 0, sizeof (tTmrAppTimer));

    pAppTimer->u4Data = (FS_ULONG) p_UserQEntry;

    if (rad_tmrh_start_timer (pAppTimer,
                              p_RadiusServerAcc->u4_ResponseTime) ==
        RAD_FAILURE)
    {
        radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                                (UINT1 *) p_UserQEntry);
        p_gRadiusErrorLog->u4_ErrorTimerNotStartedAcc++;

        MemReleaseMemBlock (gRadMemPoolId.u4RadTimerPoolId,
                            (UINT1 *) pAppTimer);

        pAppTimer = NULL;

        radiusInformFault (FAULT_TIMER_START);
    }

    /* Add to the User table after starting the timer */
    a_gUserTable[*(pa_u1RadiusPacketAcc + PKT_ID)] = pAppTimer;

    p_RadiusServerAcc->u4_AcctReqs++;
    MemReleaseMemBlock (gRadMemPoolId.u4RadPacketPoolId,
                        (UINT1 *) pa_u1RadiusPacketAcc);

    return (i4_ID);

}

/*--------------------------------------------------------------------------
Procedure    : radiusValidateInputAcc()
Description    : This module validate the input whether it has all required
              parameters for framing an accounting request. It checks the
              presence of user name or the presence of session Id. If both
              are missing it abandons framing packet. It also checks for
              presence of  NAS IP address or NAS Identifier. If both are 
              missing it abandons framing a packet.
Input(s)    : p_RadiusInputAcc
Output(s)    : None
Returns        : 0, if the validation passes
              -1, if the validation fails
Called By    : radiusAccounting()
Calling        : None
Author        : G.Vedavinayagam
Date        : 11/12/97
odifications    :
---------------------------------------------------------------------------*/
#ifdef __STDC__
INT1
radiusValidateInputAcc (tRADIUS_INPUT_ACC * p_RadiusInputAcc)
#else
INT1
radiusValidateInputAcc (p_RadiusInputAcc)
     tRADIUS_INPUT_ACC  *p_RadiusInputAcc;
#endif
{

    if (STRLEN ((char *) p_RadiusInputAcc->a_u1UserName) == 0 &&
        STRLEN ((char *) p_RadiusInputAcc->a_u1SessionId) == 0)
        return (NOT_OK);
    if (p_RadiusInputAcc->u4_NasIPAddress == 0 &&
        STRLEN ((char *) p_RadiusInputAcc->a_u1NasId) == 0)
        return (NOT_OK);
    return (OK);
}

/*---------------------------------------------------------------------------
Procedure    : radiusFramePrePacketAcc()
Description    : This module prepare a preliminary Packet for accounting
              request. It fills the Code, New Identifier, dummy request
              authenticator. The dummy request authentictor is series of
              16 zero octets.
Input(s)    : a_u1RadiusPacketAcc
Output(s)    : u2_RadiusPacketLength
Returns        : None
Called By    : radiusAccounting()
Calling        : None
Author        : G.Vedavinayagam
Date        : 11/12/97
odifications    :
---------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
radiusFramePrePacketAcc (UINT1 *a_u1PacketAcc, UINT2 *u2_PacketLength)
#else
VOID
radiusFramePrePacketAcc (a_u1PacketAcc, u2_PacketLength)
     UINT1               a_u1PacketAcc[];
     UINT2              *u2_PacketLength;
#endif
{
    MEMSET (a_u1PacketAcc, 0, LEN_TX_PKT);

    a_u1PacketAcc[PKT_TYPE] = ACCOUNTING_REQUEST;
    u1_gRadiusPacketIdentifier++;
    a_u1PacketAcc[PKT_ID] = u1_gRadiusPacketIdentifier;
    MEMSET (a_u1PacketAcc + PKT_REQA, 0, LEN_REQ_AUTH_ACC);
    *u2_PacketLength = 20;
}

/*---------------------------------------------------------------------------
Procedure    : radiusFillAttributesAcc()
Description    : This module fills the attributes corresponding to the data
              present in the Input given by the user.
Input(s)    : a_u1RadiusPacketAcc
              p_RadiusInputAcc
              u2_RadiusPacketLength
Output(s)    : a_u1RadiusPacketAcc
          u2_RadiusPacketLength
Returns        : None
Called By    : radiusAccounting()
Calling        : None
Author        : G.Vedavinayagam
Date        : 11/12/97
odifications    :
----------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
radiusFillAttributesAcc (tRADIUS_INPUT_ACC * p_RadiusInputAcc,
                         UINT1 *a_u1PacketAcc, UINT2 *u2_PacketLength)
#else
VOID
radiusFillAttributesAcc (p_RadiusInputAcc, a_u1PacketAcc, u2_PacketLength)
     tRADIUS_INPUT_ACC  *p_RadiusInputAcc;
     UINT1               a_u1PacketAcc[];
     UINT2              *u2_PacketLength;
#endif
{
    UINT1               u1_strlen;
    UINT2               u2_pktlen, u2_NetByte;
    UINT4               u4_status, u4_NetByte;
    tACCOUNT_STAT      *p_acctstat;
    tSERVICES          *p_Services;

    u2_pktlen = *u2_PacketLength;

    if (((u1_strlen =
          (UINT1) STRLEN ((char *) p_RadiusInputAcc->a_u1UserName)) != 0) &&
        (u1_strlen < LEN_USER_NAME))
    {
        a_u1PacketAcc[u2_pktlen] = A_USER_NAME;
        a_u1PacketAcc[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);
        MEMCPY (a_u1PacketAcc + u2_pktlen + 2,
                p_RadiusInputAcc->a_u1UserName, u1_strlen);
        u2_pktlen += u1_strlen + 2;

    }

    if (((u1_strlen =
          (UINT1) STRLEN ((char *) p_RadiusInputAcc->a_u1SessionId)) != 0) &&
        (u1_strlen < LEN_SESSION_ID))
    {
        a_u1PacketAcc[u2_pktlen] = A_ACCT_SESSION_ID;
        a_u1PacketAcc[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);
        MEMCPY (a_u1PacketAcc + u2_pktlen + 2,
                p_RadiusInputAcc->a_u1SessionId, u1_strlen);
        u2_pktlen += u1_strlen + 2;

    }

    if (((u1_strlen =
          (UINT1) STRLEN ((char *) p_RadiusInputAcc->a_u1NasId)) != 0) &&
        (u1_strlen < LEN_NAS_ID))
    {
        a_u1PacketAcc[u2_pktlen] = A_NAS_IDENTIFIER;
        a_u1PacketAcc[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);
        MEMCPY (a_u1PacketAcc + u2_pktlen + 2,
                p_RadiusInputAcc->a_u1NasId, u1_strlen);
        u2_pktlen += u1_strlen + 2;
    }
    else
    {
        if (p_RadiusInputAcc->u4_NasIPAddress != 0x00000000 &&
            p_RadiusInputAcc->u4_NasIPAddress != 0xffffffff)
        {
            a_u1PacketAcc[u2_pktlen] = A_NAS_IP_ADDRESS;
            a_u1PacketAcc[u2_pktlen + 1] = LEN_NAS_IP_ADDRESS;
            u4_NetByte = OSIX_HTONL (p_RadiusInputAcc->u4_NasIPAddress);
            MEMCPY (a_u1PacketAcc + u2_pktlen + 2, &u4_NetByte, 4);
            u2_pktlen += LEN_NAS_IP_ADDRESS;
        }
    }

    if (p_RadiusInputAcc->u4_NasPort != NO_ATTRIBUTE)
    {
        a_u1PacketAcc[u2_pktlen] = A_NAS_PORT;
        a_u1PacketAcc[u2_pktlen + 1] = LEN_NAS_PORT;
        u4_NetByte = OSIX_HTONL (p_RadiusInputAcc->u4_NasPort);
        MEMCPY (a_u1PacketAcc + u2_pktlen + 2, &u4_NetByte, 4);
        u2_pktlen += LEN_NAS_PORT;
    }
    if (p_RadiusInputAcc->u4_NasPortType != NO_ATTRIBUTE)
    {
        a_u1PacketAcc[u2_pktlen] = A_NAS_PORT_TYPE;
        a_u1PacketAcc[u2_pktlen + 1] = LEN_NAS_PORT_TYPE;
        u4_NetByte = OSIX_HTONL (p_RadiusInputAcc->u4_NasPortType);
        MEMCPY (a_u1PacketAcc + u2_pktlen + 2, &u4_NetByte, 4);
        u2_pktlen += LEN_NAS_PORT_TYPE;
    }

    if (p_RadiusInputAcc->bCalledStationFlag == OSIX_TRUE)
    {
        u1_strlen = STRLEN (p_RadiusInputAcc->au1CalledStationId);
        a_u1PacketAcc[u2_pktlen] = A_CALLED_STATION_ID;
        a_u1PacketAcc[u2_pktlen + 1] = STRLEN (p_RadiusInputAcc->au1CalledStationId) + 2;
        MEMCPY (a_u1PacketAcc + u2_pktlen + 2,  p_RadiusInputAcc->au1CalledStationId , u1_strlen);
        u2_pktlen += u1_strlen + 2;;
    }

    if (p_RadiusInputAcc->bCallingStationFlag == OSIX_TRUE)
    {
        u1_strlen = STRLEN (p_RadiusInputAcc->au1CallingStationId);
        a_u1PacketAcc[u2_pktlen] = A_CALLING_STATION_ID;
        a_u1PacketAcc[u2_pktlen + 1] = STRLEN (p_RadiusInputAcc->au1CallingStationId) + 2;
        MEMCPY (a_u1PacketAcc + u2_pktlen + 2,  p_RadiusInputAcc->au1CallingStationId , u1_strlen);
        u2_pktlen += u1_strlen + 2;;
    }

    if (p_RadiusInputAcc->u4_AuthType != 0 &&
        p_RadiusInputAcc->u4_AuthType <= AUTH_REMOTE)
    {
        a_u1PacketAcc[u2_pktlen] = A_ACCT_AUTHENTIC;
        a_u1PacketAcc[u2_pktlen + 1] = LEN_ACCT_AUTHENTIC;
        u4_NetByte = OSIX_HTONL (p_RadiusInputAcc->u4_AuthType);
        MEMCPY (a_u1PacketAcc + u2_pktlen + 2, &u4_NetByte, 4);
        u2_pktlen += LEN_ACCT_AUTHENTIC;
    }

    if (p_RadiusInputAcc->p_AccountStat == NULL)
    {
        a_u1PacketAcc[u2_pktlen] = A_ACCT_STATUS_TYPE;
        a_u1PacketAcc[u2_pktlen + 1] = LEN_ACCT_STATUS_TYPE;
        u4_status = ACCT_START;
        u4_NetByte = OSIX_HTONL (u4_status);
        MEMCPY (a_u1PacketAcc + u2_pktlen + 2, &u4_NetByte, 4);
        u2_pktlen += LEN_ACCT_STATUS_TYPE;
    }
    else
    {

        if (p_RadiusInputAcc->p_AccountStat->u4_TerminateCause == 0)
        {
            a_u1PacketAcc[u2_pktlen] = A_ACCT_STATUS_TYPE;
            a_u1PacketAcc[u2_pktlen + 1] = LEN_ACCT_STATUS_TYPE;
            u4_status = ACCT_INTERIM_UPDATE;
            u4_NetByte = OSIX_HTONL (u4_status);
            MEMCPY (a_u1PacketAcc + u2_pktlen + 2, &u4_NetByte, 4);
            u2_pktlen += LEN_ACCT_STATUS_TYPE;          
        }
        else
        {
            a_u1PacketAcc[u2_pktlen] = A_ACCT_STATUS_TYPE;
            a_u1PacketAcc[u2_pktlen + 1] = LEN_ACCT_STATUS_TYPE;
            u4_status = ACCT_STOP;
            u4_NetByte = OSIX_HTONL (u4_status);
            MEMCPY (a_u1PacketAcc + u2_pktlen + 2, &u4_NetByte, 4);
            u2_pktlen += LEN_ACCT_STATUS_TYPE;
        }

        p_acctstat = p_RadiusInputAcc->p_AccountStat;

        if (p_acctstat->u4_InputGigaWords != 0)
        {
            a_u1PacketAcc[u2_pktlen] = A_ACCT_INPUT_GIGAWORDS;
            a_u1PacketAcc[u2_pktlen + 1] = LEN_ACCT_INPUT_GIGAWORDS;
            u4_NetByte = OSIX_HTONL (p_acctstat->u4_InputGigaWords);
            MEMCPY (a_u1PacketAcc + u2_pktlen + 2, &u4_NetByte, 4);
            u2_pktlen += LEN_ACCT_INPUT_GIGAWORDS;
        }

        if (p_acctstat->u4_OutputGigaWords != 0)
        {
            a_u1PacketAcc[u2_pktlen] = A_ACCT_OUTPUT_GIGAWORDS;
            a_u1PacketAcc[u2_pktlen + 1] = LEN_ACCT_OUTPUT_GIGAWORDS;
            u4_NetByte = OSIX_HTONL (p_acctstat->u4_OutputGigaWords);
            MEMCPY (a_u1PacketAcc + u2_pktlen + 2, &u4_NetByte, 4);
            u2_pktlen += LEN_ACCT_OUTPUT_GIGAWORDS;
        }

        if (p_acctstat->u4_InputOctets != 0)
        {
            a_u1PacketAcc[u2_pktlen] = A_ACCT_INPUT_OCTETS;
            a_u1PacketAcc[u2_pktlen + 1] = LEN_ACCT_INPUT_OCTETS;
            u4_NetByte = OSIX_HTONL (p_acctstat->u4_InputOctets);
            MEMCPY (a_u1PacketAcc + u2_pktlen + 2, &u4_NetByte, 4);
            u2_pktlen += LEN_ACCT_INPUT_OCTETS;
        }

        if (p_acctstat->u4_OutputOctets != 0)
        {
            a_u1PacketAcc[u2_pktlen] = A_ACCT_OUTPUT_OCTETS;
            a_u1PacketAcc[u2_pktlen + 1] = LEN_ACCT_OUTPUT_OCTETS;
            u4_NetByte = OSIX_HTONL (p_acctstat->u4_OutputOctets);
            MEMCPY (a_u1PacketAcc + u2_pktlen + 2, &u4_NetByte, 4);
            u2_pktlen += LEN_ACCT_OUTPUT_OCTETS;
        }

        if (p_acctstat->u4_SessionTime != 0)
        {
            a_u1PacketAcc[u2_pktlen] = A_ACCT_SESSION_TIME;
            a_u1PacketAcc[u2_pktlen + 1] = LEN_ACCT_SESSION_TIME;
            u4_NetByte = OSIX_HTONL (p_acctstat->u4_SessionTime);
            MEMCPY (a_u1PacketAcc + u2_pktlen + 2, &u4_NetByte, 4);
            u2_pktlen += LEN_ACCT_SESSION_TIME;
        }

        if (p_acctstat->u4_InputPackets != 0)
        {
            a_u1PacketAcc[u2_pktlen] = A_ACCT_INPUT_PACKETS;
            a_u1PacketAcc[u2_pktlen + 1] = LEN_ACCT_INPUT_PACKETS;
            u4_NetByte = OSIX_HTONL (p_acctstat->u4_InputPackets);
            MEMCPY (a_u1PacketAcc + u2_pktlen + 2, &u4_NetByte, 4);
            u2_pktlen += LEN_ACCT_INPUT_PACKETS;
        }

        if (p_acctstat->u4_OutputPackets != 0)
        {
            a_u1PacketAcc[u2_pktlen] = A_ACCT_OUTPUT_PACKETS;
            a_u1PacketAcc[u2_pktlen + 1] = LEN_ACCT_OUTPUT_PACKETS;
            u4_NetByte = OSIX_HTONL (p_acctstat->u4_OutputPackets);
            MEMCPY (a_u1PacketAcc + u2_pktlen + 2, &u4_NetByte, 4);
            u2_pktlen += LEN_ACCT_OUTPUT_PACKETS;
        }

        if (p_acctstat->u4_TerminateCause != 0)
        {
            a_u1PacketAcc[u2_pktlen] = A_ACCT_TERMINATE_CAUSE;
            a_u1PacketAcc[u2_pktlen + 1] = LEN_ACCT_TERMINATE_CAUSE;
            u4_NetByte = OSIX_HTONL (p_acctstat->u4_TerminateCause);
            MEMCPY (a_u1PacketAcc + u2_pktlen + 2, &u4_NetByte, 4);
            u2_pktlen += LEN_ACCT_TERMINATE_CAUSE;
        }

    }

    p_Services = p_RadiusInputAcc->p_ServicesAcc;

    for (;;)
    {
        if (p_Services == NULL)
            break;
        a_u1PacketAcc[u2_pktlen] = p_Services->u1_ServiceType;
        if (((u1_strlen =
              (UINT1) STRLEN ((char *) p_Services->a_u1StringValue)) != 0) &&
            (u1_strlen < (LEN_SERVICE_STRING - 1)))
        {
            a_u1PacketAcc[u2_pktlen + 1] = (UINT1) (u1_strlen + 2);
            MEMCPY (a_u1PacketAcc + u2_pktlen + 2,
                    p_Services->a_u1StringValue, u1_strlen);
            u2_pktlen += u1_strlen + 2;
        }
        else
        {
            a_u1PacketAcc[u2_pktlen + 1] = LEN_NUM_ATTR;
            u4_NetByte = OSIX_HTONL (p_Services->u4_NumericalValue);
            MEMCPY (a_u1PacketAcc + u2_pktlen + 2, &u4_NetByte, 4);
            u2_pktlen += LEN_NUM_ATTR;
        }
        p_Services = p_Services->p_NextService;
    }                            /* for(;;) */

    *u2_PacketLength = u2_pktlen;
    u2_NetByte = OSIX_HTONS (u2_pktlen);
    MEMCPY (a_u1PacketAcc + PKT_LEN, &u2_NetByte, 2);

}

/*----------------------------------------------------------------------------
Procedure    : radiusMakeFinalPacketAcc()
Description    : This module makes the final Packet to be sent. This module
              calculates the Request authenticator and fills it in the 
              middle level accounting request packet to get the final
              packet.
Input(s)    : a_u1RadiusPacketAcc
              u2_RadiusPacketLength
              p_RadiusServerAcc
Output(s)    : a_u1RadiusPacketAcc
Returns        : None
Called By    : radiusAccounting()
Calling        : radiusMakeReqAuthAcc()
              radiusFillReqAuthAcc()
Author        : G.Vedavinayagam
Date        : 11/12/97
odifications    :
-----------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
radiusMakeFinalPacketAcc (UINT1 *a_u1PacketAcc,
                          UINT2 *u2_PacketLength,
                          tRADIUS_SERVER * p_RadiusServerAcc)
#else
VOID
radiusMakeFinalPacketAcc (a_u1PacketAcc, u2_PacketLength, p_RadiusServerAcc)
     UINT1               a_u1PacketAcc[];
     UINT2              *u2_PacketLength;
     tRADIUS_SERVER     *p_RadiusServerAcc;
#endif
{
    UINT1               a_u1RadiusReqAuthAcc[LEN_REQ_AUTH_ACC];

    MEMSET (a_u1RadiusReqAuthAcc, 0, LEN_REQ_AUTH_ACC);

    RAD_EVENT_PRINT1 ("\nPacket Length:%d\t\n", *u2_PacketLength);
    radiusMakeReqAuthAcc (a_u1PacketAcc, a_u1RadiusReqAuthAcc,
                          p_RadiusServerAcc);
    radiusFillReqAuthAcc (a_u1PacketAcc, a_u1RadiusReqAuthAcc);
}

/*-----------------------------------------------------------------------------
Procedure    : radiusMakeReqAuthAcc()
Description    : This module calculates the request authenticator from the
              packet with dummy request authenticator and the attributes
              code, Identifier and length.
Input(s)    : a_u1PacketAcc
              p_RadiusServerAcc
Output(s)    : a_u1RadiusReqAuthAcc
Returns        : None
Called By    : radiusMakeFinalPacketAcc()
Calling        : radiusMakeStringAcc()
              RADIUS_MESSAGE_DIGEST() 
Author        : G.Vedavinayagam
Date        : 11/12/97
odifications    :
-----------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
radiusMakeReqAuthAcc (UINT1 *a_u1PacketAcc,
                      UINT1 *a_u1ReqAuthAcc, tRADIUS_SERVER * p_RadiusServerAcc)
#else
VOID
radiusMakeReqAuthAcc (a_u1PacketAcc, a_u1ReqAuthAcc, p_RadiusServerAcc)
     UINT1               a_u1PacketAcc[];
     UINT1               a_u1ReqAuthAcc[];
     tRADIUS_SERVER     *p_RadiusServerAcc;
#endif
{

    UINT1              *pa_u1Concatenated = NULL;
    INT4                i4_StrLen;

    pa_u1Concatenated = MemAllocMemBlk (gRadMemPoolId.u4RadConcatPoolId);

    if (NULL == pa_u1Concatenated)
    {
        RAD_ERR_PRINT ("Error in allocating memory for pa_u1Concatenated\n");
        return;
    }
    MEMSET (pa_u1Concatenated, 0, sizeof (tu1Concatenated));

    radiusMakeStringAcc (a_u1PacketAcc, pa_u1Concatenated, &i4_StrLen,
                         p_RadiusServerAcc);
    RADIUS_MESSAGE_DIGEST (a_u1ReqAuthAcc, pa_u1Concatenated, i4_StrLen);
    MemReleaseMemBlock (gRadMemPoolId.u4RadConcatPoolId,
                        (UINT1 *) pa_u1Concatenated);

}

/*---------------------------------------------------------------------------
Procedure    : radiusMakeStringAcc()
Description    : This module conacatenates the partially filled accounting 
              request packet (with dummy request authenticator) and the
              secret shared between the server and the client.
Input(s)    : a_u1PacketAcc
              p_RadiusServerAcc
Output(s)    : a_u1Concatenated
              i4_StrLen
Returns        : None
Called By    : radiusMakeReqAuthAcc()
Calling        : None
Author        : G.Vedavinayagam
Date        : 11/12/97
odifications    :
---------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
radiusMakeStringAcc (UINT1 *a_u1PacketAcc,
                     UINT1 *a_u1Concatenated,
                     INT4 *i4_StrLen, tRADIUS_SERVER * p_RadiusServerAcc)
#else
VOID
radiusMakeStringAcc (a_u1PacketAcc, a_u1Concatenated, i4_StrLen,
                     p_RadiusServerAcc)
     UINT1               a_u1PacketAcc[];
     UINT1               a_u1Concatenated[];
     INT4               *i4_StrLen;
     tRADIUS_SERVER     *p_RadiusServerAcc;
#endif
{
    INT4                i4_seclen;
    UINT2               u2_pktlen;

    MEMSET (a_u1Concatenated, 0, LEN_TX_PKT + LEN_SECRET);

    MEMCPY (&u2_pktlen, a_u1PacketAcc + PKT_LEN, 2);
    u2_pktlen = OSIX_NTOHS (u2_pktlen);

    i4_seclen = STRLEN ((char *) p_RadiusServerAcc->a_u1Secret);
    MEMCPY (a_u1Concatenated, a_u1PacketAcc, u2_pktlen);
    MEMCPY (a_u1Concatenated + u2_pktlen, p_RadiusServerAcc->a_u1Secret,
            i4_seclen);
    *i4_StrLen = u2_pktlen + i4_seclen;
}

/*--------------------------------------------------------------------------
Procedure    : radiusFillReqAuthAcc()
Description    : This module fills the request authenticator calculated from
              accounting request packet.
Input(s)    : a_u1PacketAcc
              a_u1RadiusReqAuthAcc
Output(s)    : a_u1PacketAcc
Returns        : None
Called By    : radiusMakeFinalPacketAcc()
Calling        : None
Author        : G.Vedavinayagam
Date        : 11/12/97
odifications    :
---------------------------------------------------------------------------*/
#ifdef __STDC__
VOID
radiusFillReqAuthAcc (UINT1 *a_u1PacketAcc, UINT1 *a_u1RadiusReqAuthAcc)
#else
VOID
radiusFillReqAuthAcc (a_u1PacketAcc, a_u1RadiusReqAuthAcc)
     UINT1               a_u1PacketAcc[];
     UINT1               a_u1RadiusReqAuthAcc[];
#endif
{
    MEMCPY (a_u1PacketAcc + PKT_REQA, a_u1RadiusReqAuthAcc, LEN_REQ_AUTH_ACC);
}

/*---------------------------------------------------------------------------
Procedure    : radiusEnterUserQAcc()
Description    : This module creates a user entry and enters it in the user 
              queue.
Input(s)    : p_RadiusInputAcc
              a_u1RadiusPacketAcc
              p_RadiusServerAcc
Output(s)    : None
Returns      : 0, if successfull
              -1, in the case of failure 
Called By    : radiusAccounting()
Calling        : None
Author        : G.Vedavinayagam
Date        : 11/17/97
odifications    :
-----------------------------------------------------------------------------*/
#ifdef __STDC__

INT1
radiusEnterUserQAcc (tRADIUS_INPUT_ACC * p_RadiusInputAcc,
                     UINT1 *a_u1RadiusPacketAcc,
                     tRADIUS_REQ_ENTRY ** p_QEntry,
                     tRADIUS_SERVER * p_RadiusServerAcc,
                     VOID (*CallBackfPtr) (VOID *), UINT4 ifIndex)
#else

INT1
radiusEnterUserQAcc (p_RadiusInputAcc, a_u1RadiusPacketAcc, p_QEntry,
                     p_RadiusServerAcc, CallBackfPtr, ifIndex)
     tRADIUS_INPUT_ACC  *p_RadiusInputAcc;
     UINT1               a_u1RadiusPacketAcc[];
     tRADIUS_REQ_ENTRY **p_QEntry;
     tRADIUS_SERVER     *p_RadiusServerAcc;
     VOID                (*CallBackfPtr) (VOID *);
     UINT4               ifIndex;

#endif

{
    INT4                i4_strlen;
    UINT2               u2_PktLen;
    tRADIUS_REQ_ENTRY  *p_UserQEntry;

    MEMCPY (&u2_PktLen, a_u1RadiusPacketAcc + PKT_LEN, 2);
    u2_PktLen = OSIX_NTOHS (u2_PktLen);

    p_UserQEntry = radiusCreateUserEntry (gRadMemPoolId.u4UserEntryPoolId);

    if (p_UserQEntry == NULL)
    {
        p_gRadiusErrorLog->u4_ErrorQueueCreationAcc++;
        return NOT_OK;
    }
    else
    {
        p_UserQEntry->CallBackfPtr = CallBackfPtr;
        p_UserQEntry->ifIndex = ifIndex;
        OsixGetSysTime ((tOsixSysTime *) & (p_UserQEntry->u4SendTime));
        i4_strlen = STRLEN ((char *) p_RadiusInputAcc->a_u1UserName);
        MEMCPY (p_UserQEntry->a_u1UserName, p_RadiusInputAcc->a_u1UserName,
                i4_strlen);
        *(p_UserQEntry->a_u1UserName + i4_strlen) = (INT4) 0;
        p_UserQEntry->u1_PacketIdentifier = *(a_u1RadiusPacketAcc + PKT_ID);
        MEMCPY (p_UserQEntry->a_u1RequestAuth, a_u1RadiusPacketAcc + PKT_REQA,
                LEN_REQ_AUTH_ACC);

        p_UserQEntry->p_u1PacketTransmitted =
            radiusCreatePacket (gRadMemPoolId.u4PacketPoolId);

        if (p_UserQEntry->p_u1PacketTransmitted == NULL)
        {
            radiusReleaseUserEntry (gRadMemPoolId.u4UserEntryPoolId,
                                    (UINT1 *) p_UserQEntry);
            p_gRadiusErrorLog->u4_ErrorPacketCreationAcc++;
            return NOT_OK;
        }
        else
            MEMCPY (p_UserQEntry->p_u1PacketTransmitted, a_u1RadiusPacketAcc,
                    u2_PktLen);
        *(p_UserQEntry->p_u1PacketTransmitted + u2_PktLen) = '\0';
        p_UserQEntry->u1_ReTxCount = 0;

        p_UserQEntry->ptRadiusServer = p_RadiusServerAcc;

        *p_QEntry = p_UserQEntry;
        return OK;

    }                            /* if p_UserQEntry == NULL */
}

/************************** END OF FILE "radacct.c" *************************/
