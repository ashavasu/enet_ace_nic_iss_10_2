/***************************************************************************
 * Copyright (C) Future Software Limited, 2007-2008
 *
 * $Id: raproxlw.c,v 1.1 2015/04/28 12:26:24 siva Exp $ 
 *
 * Description: This file contains the low level routines for radius proxy.
 *
 ***************************************************************************/
# include "lr.h"
# include "fssnmp.h"
# include "snmccons.h"

# include "radsnmp.h"
# include "radcom.h"
/* Low Level GET Routine for Radius Proxy Object  */

/****************************************************************************
 Function    :  nmhGetRadiusProxyStatus
 Input       :  The Indices

                The Object 
                retValRadiusProxyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRadiusProxyStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetRadiusProxyStatus (INT4 *pi4RetValRadiusProxyStatus)
#else /*  */
INT1
nmhGetRadiusProxyStatus (pi4RetValRadiusProxyStatus)
     INT4               *pi4RetValRadiusProxyStatus;

#endif /*  */
{
    *pi4RetValRadiusProxyStatus = (INT4) RADIUS_PROXY_STATUS ();
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetRadiusProxyStatus
 Input       :  The Indices

                The Object 
                setValRadiusProxyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS 
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetRadiusProxyStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetRadiusProxyStatus (INT4 i4SetValRadiusProxyStatus)
#else /*  */
INT1
nmhSetRadiusProxyStatus (i4SetValRadiusProxyStatus)
     INT4                i4SetValRadiusProxyStatus;

#endif /*  */
{
    /* Compare the existing configured value and given set value. */
    if (i4SetValRadiusProxyStatus == RADIUS_PROXY_STATUS ())
    {
        return SNMP_SUCCESS;
    }

    RADIUS_PROXY_STATUS () = (UINT1) i4SetValRadiusProxyStatus;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "RadiusProxyStatus = %d\n", i4SetValRadiusProxyStatus); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2RadiusProxyStatus
 Input       :  The Indices

                The Object 
                testValRadiusProxyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2RadiusProxyStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2RadiusProxyStatus (UINT4 *pu4ErrorCode,
                            INT4 i4TestValRadiusProxyStatus)
#else /*  */
INT1
nmhTestv2RadiusProxyStatus (pu4ErrorCode, i4TestValRadiusProxyStatus)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValRadiusProxyStatus;

#endif /*  */
{
    if ((i4TestValRadiusProxyStatus != RADIUS_PROXY_ENABLED) &&
        (i4TestValRadiusProxyStatus != RADIUS_PROXY_DISABLED))

    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "RadiusProxyStatus = %d\n", i4TestValRadiusProxyStatus); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n\n"); ***/
}
